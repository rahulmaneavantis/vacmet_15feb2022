﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Web.Security;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class ComplianceDashboard : System.Web.UI.Page
    {
        public static string rahulflag = "N";
        int checkInternalapplicable = 0;
      

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                lblErrorMessage.Text = string.Empty;
                Tab1.CssClass = "Clicked";
                MainView.ActiveViewIndex = 0;

                if (Session["userID"] != null)
                {
                    int userID = Convert.ToInt32(Session["userID"]);
                    User user = UserManagement.GetByID(userID);
                    
                    if (user.CustomerID == null)
                    {
                        checkInternalapplicable = 2;
                    }
                    else
                    {
                        Customer customer = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                        if (customer.IComplianceApplicable != null)
                        {
                            checkInternalapplicable = Convert.ToInt32(customer.IComplianceApplicable);
                        }
                    }
                    if (checkInternalapplicable == 1)
                    {
                        //Tab2.Visible = true;
                    }
                    else if (checkInternalapplicable == 0)
                    {
                        //Tab1.Visible = true;
                        //Tab2.Visible = false;
                    }


                    //if (Session["RahulSessionFlag"].ToString().Trim() == "N")
                    //{
                    //    //SetRole();
                                            
                    //}
                    Tab1_Click(sender, e);                                       
                }                
            }

            if (Session["ViewDetailsBack"] == "true")
            {
                divStatusTab.Visible = false;
                //ucApproverDashboard.Visible = false;
                rdApproverReport.Visible = false;
                //ucApproverGrading.Visible = false;
                //ucSummaryStatusReport.Visible = false;
                divEventstatusTab.Visible = true;
                divActiveEventstatusTab.Visible = true;
                //ucEventDashboards.Visible = false;
                //ucActiveEventDashboards.Visible = false;

                btnAssignedEvents_Click(sender, e);
                Session["ViewDetailsBack"] = "";
            }

            if (Session["MyEventBack"] == "true")
            {
                Tab1.CssClass = "Initial";
                Tab2.CssClass = "Initial";
                divStatusTab.Visible = false;
                //ucApproverDashboard.Visible = false;
                rdApproverReport.Visible = false;
                //ucApproverGrading.Visible = false;
                //ucSummaryStatusReport.Visible = false;
                divEventstatusTab.Visible = true;
                divActiveEventstatusTab.Visible = true;
                //ucEventDashboards.Visible = false;
                //ucActiveEventDashboards.Visible = false;
                Session["MyEventBack"] = "";
            }
        }

        protected void rblRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rblRole.SelectedValue.Equals("3"))
                {
                    divPendingForReview.Visible = false;
                    divUpcomingCompliances.Visible = true;
                    divOverdueCompliances.Visible = true;
                    setCompliancesCounts();
                    divStatusTab.Visible = true;
                    //ucApproverDashboard.Visible = false;
                    rdApproverReport.Visible = false;
                    //ucApproverGrading.Visible = false;
                    //ucSummaryStatusReport.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                    //ucEventDashboards.Visible = false;
                    //ucActiveEventDashboards.Visible = false;
                }
                else if (rblRole.SelectedValue.Equals("4"))
                {
                    divPendingForReview.Visible = true;
                    divUpcomingCompliances.Visible = false;
                    divOverdueCompliances.Visible = true;
                    setCompliancesCounts();
                    divStatusTab.Visible = true;
                    //ucApproverDashboard.Visible = false;
                    rdApproverReport.Visible = false;
                    //ucApproverGrading.Visible = false;
                    //ucSummaryStatusReport.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                    //ucEventDashboards.Visible = false;
                    //ucActiveEventDashboards.Visible = false;
                }                 
                else if (rblRole.SelectedValue.Equals("10") || rblRole.SelectedValue.Equals("11"))
                {
                    divStatusTab.Visible = false;
                    //ucApproverDashboard.Visible = false;
                    rdApproverReport.Visible = false;
                    //ucApproverGrading.Visible = false;
                    //ucSummaryStatusReport.Visible = false;
                    divEventstatusTab.Visible = true;
                    divActiveEventstatusTab.Visible = true;
                    //ucEventDashboards.Visible = false;
                    //ucActiveEventDashboards.Visible = false;
                }

                else if (rblRole.SelectedValue.Equals("6"))
                {
                    divStatusTab.Visible = false;
                    //ucApproverDashboard.Visible = true;
                    rdApproverReport.Visible = true;
                    //ucApproverGrading.Visible = false;
                    //ucSummaryStatusReport.Visible = false;
                    rdApproverReport.SelectedIndex = 0;
                    //ucApproverDashboard.DrowGraphs(true);
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                    //ucEventDashboards.Visible = false;
                    //ucActiveEventDashboards.Visible = false;
                }
                else
                {
                    divStatusTab.Visible = true;
                    //ucApproverDashboard.Visible = false;
                    //ucEventDashboards.Visible = false;
                    rdApproverReport.Visible = false;
                    //ucApproverGrading.Visible = false;
                    //ucSummaryStatusReport.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                    //ucApproverDashboard.Visible = false;
                    //ucEventDashboards.Visible = false;
                }                             
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }

        protected void rdInternalaRoleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);

                if (rdInternalaRoleList.SelectedValue.Equals("3"))
                {
                    divPendingForReviewI.Visible = false;
                    divUpcomingCompliancesI.Visible = true;
                    divOverdueCompliancesI.Visible = true;
                    setInternalCompliancesCounts();
                    divStatusTabI.Visible = true;
                    //UCApproverDashboardInternal.Visible = false;
                    rdApproverReportInternal.Visible = false;
                    //UCApproverGradingInternal.Visible = false;
                    //UCSummaryStatusReportInternal.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                    //ucEventDashboards.Visible = false;
                    //ucActiveEventDashboards.Visible = false;
                }
                else if (rdInternalaRoleList.SelectedValue.Equals("4"))
                {                    
                    divPendingForReviewI.Visible = true;
                    divUpcomingCompliancesI.Visible = false;
                    divOverdueCompliancesI.Visible = true;
                    setInternalCompliancesCounts();
                    divStatusTabI.Visible = true;
                    //UCApproverDashboardInternal.Visible = false;
                    rdApproverReportInternal.Visible = false;
                    //UCApproverGradingInternal.Visible = false;
                    //UCSummaryStatusReportInternal.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                    //ucEventDashboards.Visible = false;
                    //ucActiveEventDashboards.Visible = false;
                }                 
                else if (rdInternalaRoleList.SelectedValue.Equals("10") || rdInternalaRoleList.SelectedValue.Equals("11"))
                {

                    divStatusTabI.Visible = false;
                    //UCpproverDashboardInternal.Visible = false;
                    rdApproverReportInternal.Visible = false;
                    //UCApproverGradingInternal.Visible = false;
                    //UCSummaryStatusReportInternal.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                    //ucEventDashboards.Visible = false;
                    //ucActiveEventDashboards.Visible = false;
                }
                else if (rdInternalaRoleList.SelectedValue.Equals("6"))
                {
                    divStatusTabI.Visible = false;                 
                    //UCApproverDashboardInternal.Visible = true;
                    rdApproverReportInternal.Visible = true;
                    //UCApproverGradingInternal.Visible = false;
                    //UCSummaryStatusReportInternal.Visible = false;
                    rdApproverReportInternal.SelectedIndex = 0;
                    //UCApproverDashboardInternal.DrowGraphs(customerid,true);
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                    //ucEventDashboards.Visible = false;
                    //ucActiveEventDashboards.Visible = false;
                }
                else
                {
                    divStatusTabI.Visible = true;
                    //UCApproverDashboardInternal.Visible = false;
                    rdApproverReportInternal.Visible = false;
                    //UCApproverGradingInternal.Visible = false;
                    //UCSummaryStatusReportInternal.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                    //ucEventDashboards.Visible = false;
                    //ucActiveEventDashboards.Visible = false;
                }               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessageInternal.Text = "Server Error Occured. Please try again.";
            }
        }
        protected void setCompliancesCounts()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {


                    entities.Database.CommandTimeout = 180;
                    List<SP_GetCheckListCannedReportCompliancesSummary_Result> MasterStatuoryChecklistTransactionsQuery = new List<SP_GetCheckListCannedReportCompliancesSummary_Result>();

                    MasterStatuoryChecklistTransactionsQuery = (entities.SP_GetCheckListCannedReportCompliancesSummary(AuthenticationHelper.UserID,Convert.ToInt32(AuthenticationHelper.CustomerID), "PERF")).ToList();
                    
                    if (rblRole.SelectedValue.Equals("3"))
                    {                       
                        btnUpcomingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForPerformerUpcomingDisplayCountOldDesign(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        btnPendingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForPerformerOverDueDisplayCountOldDesign(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        btnPendingForApprovels.Text = Convert.ToString(DashboardManagement.DashboardDataForPerformerDisplayCountOldDesign(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        btnChecklistCompliances.Text = Convert.ToString(Business.ComplianceManagement.GetMappedComplianceCheckListRahul(AuthenticationHelper.UserID, MasterStatuoryChecklistTransactionsQuery, true, null).Count);
                        btnRejectedCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForPerformerUpcomingDisplayCountOldDesign(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Rejected).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        btnAssignedEvents.Text = Convert.ToString(DashboardManagement.DashboardDataAssignedEventCount(AuthenticationHelper.UserID).Count);
                        btnActiveEvents.Text = Convert.ToString(DashboardManagement.DashboardDataActiveEventCount(AuthenticationHelper.UserID).Count);


                        LinkButton2.Text = "Overdue Compliances";
                        LinkButton2.ToolTip = "Click here to see Overdue Compliances.";
                        btnPendingForApproverTitle.Text = "Pending for review";
                        btnPendingForApproverTitle.ToolTip = "Click here to see Pending for Review Compliances.";
                    }
                    else if (rblRole.SelectedValue.Equals("4") || rblRole.SelectedValue.Equals("5"))
                    {
                        //btnPendingForReview.Text = Convert.ToString(DashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //btnPendingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue, true).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //btnChecklistCompliances.Text = Convert.ToString(Business.ComplianceManagement.GetMappedComplianceCheckListRahul(AuthenticationHelper.UserID, null).Count);
                        //btnPendingForApprovels.Text = Convert.ToString(DashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForApproval).Where(entry => entry.ComplianceInstanceID != -1).Count());


                        btnPendingForReview.Text = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCountOldDesign(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        btnPendingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCountOldDesign(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue, true).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        btnChecklistCompliances.Text = Convert.ToString(Business.ComplianceManagement.GetMappedComplianceCheckListRahul(AuthenticationHelper.UserID, MasterStatuoryChecklistTransactionsQuery, false, null).Count);
                        btnRejectedCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCountOldDesign(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Rejected).Where(entry => entry.ComplianceInstanceID != -1).Count());

                        btnPendingForApprovels.Text = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCountOldDesign(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        btnAssignedEvents.Text = Convert.ToString(DashboardManagement.DashboardDataAssignedEventCount(AuthenticationHelper.UserID).Count);
                        btnActiveEvents.Text = Convert.ToString(DashboardManagement.DashboardDataActiveEventCount(AuthenticationHelper.UserID).Count);

                        LinkButton2.Text = "Due but not submited";
                        LinkButton2.ToolTip = "Click here to see not submited compliances.";
                        btnPendingForApproverTitle.Text = "Pending for approval";
                        btnPendingForApproverTitle.ToolTip = "Click here to see Pending for Approval Compliances.";
                    }
                    else if (rblRole.SelectedValue.Equals("6"))
                    {
                        //btnUpcomingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //btnPendingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        //btnPendingForApprovels.Text = Convert.ToString(DashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForApproval).Where(entry => entry.ComplianceInstanceID != -1).Count());


                        btnUpcomingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForApproverDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        btnPendingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForApproverDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue).Where(entry => entry.ComplianceInstanceID != -1).Count());
                        btnPendingForApprovels.Text = Convert.ToString(DashboardManagement.DashboardDataForApproverDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.ComplianceInstanceID != -1).Count());

                        btnPendingForApproverTitle.Text = "Pending for approval";
                        btnPendingForApproverTitle.ToolTip = "Click here to see Pending for Approval Compliances.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }
        //protected void setCompliancesCounts()
        //{
        //    try
        //    {
        //        if (rblRole.SelectedValue.Equals("3"))
        //        {

        //            //btnUpcomingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForPerformerUpcoming(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.ComplianceInstanceID != -1).Count());
        //            //btnPendingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForPerformerOverDue(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue).Where(entry => entry.ComplianceInstanceID != -1).Count());
        //            //btnPendingForApprovels.Text = Convert.ToString(DashboardManagement.DashboardDataForPerformer(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForApproval).Where(entry => entry.ComplianceInstanceID != -1).Count());
        //            //btnChecklistCompliances.Text = Convert.ToString(Business.ComplianceManagement.GetMappedComplianceCheckListRahul(AuthenticationHelper.UserID, null).Count);     

        //            btnUpcomingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForPerformerUpcomingDisplayCountOldDesign(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.ComplianceInstanceID != -1).Count());
        //            btnPendingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForPerformerOverDueDisplayCountOldDesign(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue).Where(entry => entry.ComplianceInstanceID != -1).Count());
        //            btnPendingForApprovels.Text = Convert.ToString(DashboardManagement.DashboardDataForPerformerDisplayCountOldDesign(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.ComplianceInstanceID != -1).Count());
        //            btnChecklistCompliances.Text = Convert.ToString(Business.ComplianceManagement.GetMappedComplianceCheckListRahul(AuthenticationHelper.UserID,true, null).Count);
        //            btnRejectedCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForPerformerUpcomingDisplayCountOldDesign(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Rejected).Where(entry => entry.ComplianceInstanceID != -1).Count());
        //            btnAssignedEvents.Text = Convert.ToString(DashboardManagement.DashboardDataAssignedEventCount(AuthenticationHelper.UserID).Count);
        //            btnActiveEvents.Text = Convert.ToString(DashboardManagement.DashboardDataActiveEventCount(AuthenticationHelper.UserID).Count);


        //            LinkButton2.Text = "Overdue Compliances";
        //            LinkButton2.ToolTip = "Click here to see Overdue Compliances.";
        //            btnPendingForApproverTitle.Text = "Pending for review";
        //            btnPendingForApproverTitle.ToolTip = "Click here to see Pending for Review Compliances.";                 
        //        }
        //        else if (rblRole.SelectedValue.Equals("4") || rblRole.SelectedValue.Equals("5"))
        //        {
        //            //btnPendingForReview.Text = Convert.ToString(DashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.ComplianceInstanceID != -1).Count());
        //            //btnPendingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue, true).Where(entry => entry.ComplianceInstanceID != -1).Count());
        //            //btnChecklistCompliances.Text = Convert.ToString(Business.ComplianceManagement.GetMappedComplianceCheckListRahul(AuthenticationHelper.UserID, null).Count);
        //            //btnPendingForApprovels.Text = Convert.ToString(DashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForApproval).Where(entry => entry.ComplianceInstanceID != -1).Count());


        //            btnPendingForReview.Text = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCountOldDesign(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.ComplianceInstanceID != -1).Count());
        //            btnPendingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCountOldDesign(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue, true).Where(entry => entry.ComplianceInstanceID != -1).Count());
        //            btnChecklistCompliances.Text = Convert.ToString(Business.ComplianceManagement.GetMappedComplianceCheckListRahul(AuthenticationHelper.UserID,false, null).Count);
        //            btnRejectedCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCountOldDesign(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Rejected).Where(entry => entry.ComplianceInstanceID != -1).Count());

        //            btnPendingForApprovels.Text = Convert.ToString(DashboardManagement.DashboardDataForReviewerDisplayCountOldDesign(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.ComplianceInstanceID != -1).Count());
        //            btnAssignedEvents.Text = Convert.ToString(DashboardManagement.DashboardDataAssignedEventCount(AuthenticationHelper.UserID).Count);
        //            btnActiveEvents.Text = Convert.ToString(DashboardManagement.DashboardDataActiveEventCount(AuthenticationHelper.UserID).Count);

        //            LinkButton2.Text = "Due but not submited";
        //            LinkButton2.ToolTip = "Click here to see not submited compliances.";                   
        //            btnPendingForApproverTitle.Text = "Pending for approval";
        //            btnPendingForApproverTitle.ToolTip = "Click here to see Pending for Approval Compliances.";
        //        }
        //        else if (rblRole.SelectedValue.Equals("6"))
        //        {
        //            //btnUpcomingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.ComplianceInstanceID != -1).Count());
        //            //btnPendingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue).Where(entry => entry.ComplianceInstanceID != -1).Count());
        //            //btnPendingForApprovels.Text = Convert.ToString(DashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForApproval).Where(entry => entry.ComplianceInstanceID != -1).Count());


        //            btnUpcomingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForApproverDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.ComplianceInstanceID != -1).Count());
        //            btnPendingCompliances.Text = Convert.ToString(DashboardManagement.DashboardDataForApproverDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue).Where(entry => entry.ComplianceInstanceID != -1).Count());
        //            btnPendingForApprovels.Text = Convert.ToString(DashboardManagement.DashboardDataForApproverDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.ComplianceInstanceID != -1).Count());

        //            btnPendingForApproverTitle.Text = "Pending for approval";
        //            btnPendingForApproverTitle.ToolTip = "Click here to see Pending for Approval Compliances.";
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        lblErrorMessage.Text = "Server Error Occured. Please try again.";
        //    }
        //}
        protected void setInternalCompliancesCounts()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    entities.Database.CommandTimeout = 180;
                    var MasterInternaltransactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                                           where row.UserID == AuthenticationHelper.UserID
                                                   && row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                           select row).ToList();


                    if (rdInternalaRoleList.SelectedValue.Equals("3"))
                    {

                        btnInternalUpcomingCompliances.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.InternalComplianceStatusID != -1).Count());
                        btnInternalPendingCompliances.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Overdue).Where(entry => entry.InternalComplianceStatusID != -1).Count());
                        btnInternalPendingForApprovels.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.InternalComplianceStatusID != -1).Count());
                        LinkButton7.Text = "Overdue Compliances";
                        LinkButton7.ToolTip = "Click here to see Overdue Compliances.";
                        btnInternalPendingForApproverTitle.Text = "Pending for review";
                        btnInternalPendingForApproverTitle.ToolTip = "Click here to see Pending for Review Compliances.";
                    }
                    else if (rdInternalaRoleList.SelectedValue.Equals("4") || rdInternalaRoleList.SelectedValue.Equals("5"))
                    {

                        btnInternalPendigForreview.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.InternalComplianceInstanceID != -1).Count());
                        btnInternalPendingCompliances.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.Overdue, true).Where(entry => entry.InternalComplianceInstanceID != -1).Count());
                        btnInternalPendingForApprovels.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, MasterInternaltransactionsQuery, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.InternalComplianceInstanceID != -1).Count());

                        LinkButton7.Text = "Due but not submited";
                        LinkButton7.ToolTip = "Click here to see not submited compliances.";
                        btnInternalPendingForApproverTitle.Text = "Pending for approval";
                        btnInternalPendingForApproverTitle.ToolTip = "Click here to see Pending for Approval Compliances.";
                    }
                    else if (rdInternalaRoleList.SelectedValue.Equals("6"))
                    {
                        btnInternalUpcomingCompliances.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForApproverDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.InternalComplianceInstanceID != -1).Count());
                        btnInternalPendingCompliances.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForApproverDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue).Where(entry => entry.InternalComplianceInstanceID != -1).Count());
                        btnInternalPendingForApprovels.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForApproverDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.InternalComplianceInstanceID != -1).Count());
                        btnInternalPendingForApproverTitle.Text = "Pending for approval";
                        btnInternalPendingForApproverTitle.ToolTip = "Click here to see Pending for Approval Compliances.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }
        //protected void setInternalCompliancesCounts()
        //{
        //    try
        //    {
        //        if (rdInternalaRoleList.SelectedValue.Equals("3"))
        //        {

        //            btnInternalUpcomingCompliances.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.InternalComplianceStatusID != -1).Count());
        //            btnInternalPendingCompliances.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue).Where(entry => entry.InternalComplianceStatusID != -1).Count());
        //            btnInternalPendingForApprovels.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForPerformerDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.InternalComplianceStatusID != -1).Count());                   
        //            LinkButton7.Text = "Overdue Compliances";
        //            LinkButton7.ToolTip = "Click here to see Overdue Compliances.";
        //            btnInternalPendingForApproverTitle.Text = "Pending for review";
        //            btnInternalPendingForApproverTitle.ToolTip = "Click here to see Pending for Review Compliances.";
        //        }
        //        else if (rdInternalaRoleList.SelectedValue.Equals("4") || rdInternalaRoleList.SelectedValue.Equals("5"))
        //        {

        //            btnInternalPendigForreview.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.InternalComplianceInstanceID != -1).Count());
        //            btnInternalPendingCompliances.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue, true).Where(entry => entry.InternalComplianceInstanceID != -1).Count());
        //            btnInternalPendingForApprovels.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForReviewerDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.InternalComplianceInstanceID != -1).Count());

        //            LinkButton7.Text = "Due but not submited";
        //            LinkButton7.ToolTip = "Click here to see not submited compliances.";
        //            btnInternalPendingForApproverTitle.Text = "Pending for approval";
        //            btnInternalPendingForApproverTitle.ToolTip = "Click here to see Pending for Approval Compliances.";              
        //        }
        //        else if (rdInternalaRoleList.SelectedValue.Equals("6"))
        //        {
        //            btnInternalUpcomingCompliances.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForApproverDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming).Where(entry => entry.InternalComplianceInstanceID != -1).Count());
        //            btnInternalPendingCompliances.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForApproverDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Overdue).Where(entry => entry.InternalComplianceInstanceID != -1).Count());
        //            btnInternalPendingForApprovels.Text = Convert.ToString(InternalDashboardManagement.DashboardDataForApproverDisplayCount(AuthenticationHelper.UserID, CannedReportFilterForPerformer.PendingForReview).Where(entry => entry.InternalComplianceInstanceID != -1).Count());
        //            btnInternalPendingForApproverTitle.Text = "Pending for approval";
        //            btnInternalPendingForApproverTitle.ToolTip = "Click here to see Pending for Approval Compliances.";
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        lblErrorMessage.Text = "Server Error Occured. Please try again.";
        //    }
        //}

        protected void btnPendingForReview_Click(object sender, EventArgs e)
        {
       
            try
            {
                if (rblRole.SelectedItem != null)
                {

                    Response.Redirect("~/Controls/PendingForReviewCompliance/PendingForReview/" + rblRole.SelectedItem.Text + "/Statutory", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }

        protected void btnUpcomingCompliances_Click(object sender, EventArgs e)
        {
            try
            {
                if (rblRole.SelectedItem != null)
                {

                    Response.Redirect("~/Controls/CompanyAdminUpCompliances/Upcoming/" + rblRole.SelectedItem.Text + "/Statutory", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }

       
        }

        protected void btnPendingCompliances_Click(object sender, EventArgs e)
        {
           
            try
            {
                if (rblRole.SelectedItem != null)
                {                 
                    Response.Redirect("~/Controls/OverdueCompliances/Pending/" + rblRole.SelectedItem.Text + "/Statutory", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }

        protected void btnPendingForApprovels_Click(object sender, EventArgs e)
        {
            
            try
            {
                if (rblRole.SelectedItem != null)
                {
                    Response.Redirect("~/Controls/PendingforApprovelsCompliance/PendingforApproval/" + rblRole.SelectedItem.Text + "/Statutory", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }


        }

        protected void btnPerformanceSummaryforPerformer_Click(object sender, EventArgs e)
        {
            try
            {
                //Response.Redirect("~/Controls/frmEventUpcomingOverdue.aspx", false);
                if (rblRole.SelectedItem != null)
                {
                    if (rblRole.SelectedValue == "3")
                    {
                        Response.Redirect("~/Controls/EventUpcomingOverduePerformer/UpcomingOverduePerformer/" + rblRole.SelectedItem.Text + "/Statutory", false);
                    }
                    else if (rblRole.SelectedValue == "4")
                    {
                        Response.Redirect("~/Controls/EventUpcomingReviewer/UpcomingOverdueReviewer/" + rblRole.SelectedItem.Text + "/Statutory", false);
                        
                    }

                }
                //if (rblRole.SelectedItem != null)
                //{
                //routes.MapPageRoute("Common/ComplianceDashboardDisplay/{filter}/{Role}/{Type}", "Common/ComplianceDashboardDisplay/{filter}/{Role}/{Type}", "~/Common/ComplianceDashboardDisplay.aspx");
                //routes.MapPageRoute("Common/ComplianceDashboardDisplay/{{Role}", "Common/ComplianceDashboardDisplay/{{Role}", "~/Common/ComplianceDashboardDisplay.aspx");
                //    Response.Redirect("~/Common/ComplianceDashboardDisplay/Summary/" + rblRole.SelectedItem.Text + "/Statutory", false);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
           
        }
       
        protected void btnCheckList_Click(object sender, EventArgs e)
        {
            try
            {
                if (rblRole.SelectedItem != null)
                {
                    Business.ComplianceManagement.GetMappedComplianceCheckList(AuthenticationHelper.UserID, null);
                    Response.Redirect("~/Compliances/CompanyAdminChList", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
           
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        public void SetRole()
        {
            try
            {
                var AssignedRoles = ComplianceManagement.Business.ComplianceManagement.GetUserRoles(AuthenticationHelper.UserID);
                foreach (var ar in AssignedRoles)
                {
                    string name = ar.Name;
                    if (name.Equals("Reviewer1") || name.Equals("Reviewer2"))
                    {
                        name = "Reviewer";
                    }
                    if (ar.ID.ToString().Equals("10") || ar.ID.ToString().Equals("11"))
                    {
                        name = "My Events";
                    }

                    ListItem item = new ListItem(name, ar.ID.ToString());
                    ListItem item1 = rblRole.Items.FindByText(name);
                    if (item1 == null)
                    {
                        rblRole.Items.Add(item);
                    }
                }
              
                if (AssignedRoles.Count > 0)
                {
                    rblRole.SelectedIndex = 0;
                }
                Session["RahulRoleID"] =null;
                if (rblRole.SelectedValue !=null)
                {
                    Session["RahulRoleID"] = rblRole.SelectedValue;
                }
              
                if (rblRole.SelectedValue.Equals("4"))
                {
                    divPendingForReview.Visible = true;
                    divUpcomingCompliances.Visible = false;
                    divOverdueCompliances.Visible = true;
                    //ucEventDashboards.Visible = false;
                    //ucActiveEventDashboards.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                    divEventstatusTab.Visible = false;
                }
                else
                {
                    divPendingForReview.Visible = false;
                    divUpcomingCompliances.Visible = true;
                    divOverdueCompliances.Visible = true;
                    //ucEventDashboards.Visible = false;
                    //ucActiveEventDashboards.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                    divEventstatusTab.Visible = false;
                }

                if (rblRole.SelectedValue.Equals("6"))
                {
                    divStatusTab.Visible = false;
                    //ucApproverDashboard.Visible = true;
                    rdApproverReport.Visible = true;
                    //ucApproverGrading.Visible = false;
                    //ucSummaryStatusReport.Visible = false;
                    //ucEventDashboards.Visible = false;
                    rdApproverReport.SelectedIndex = 0;
                    //ucActiveEventDashboards.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                    divEventstatusTab.Visible = false;
                }
                else
                {
                    divStatusTab.Visible = true;
                    //ucApproverDashboard.Visible = false;
                    rdApproverReport.Visible = false;
                    //ucApproverGrading.Visible = false;
                    //ucSummaryStatusReport.Visible = false;
                    //ucEventDashboards.Visible = false;
                    //ucActiveEventDashboards.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                }

                if (rblRole.SelectedValue.Equals("10") || rblRole.SelectedValue.Equals("11"))
                {
                    //ucEventDashboards.Visible = false;
                    //ucActiveEventDashboards.Visible = false;
                    divStatusTab.Visible = false;
                    //ucApproverDashboard.Visible = false;
                    rdApproverReport.Visible = false;
                    //ucApproverGrading.Visible = false;
                    //ucSummaryStatusReport.Visible = false;
                    //ucSummaryStatusReport.Visible = false;
                    divEventstatusTab.Visible = true;
                    divActiveEventstatusTab.Visible = true;
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }

        public void SetRoleForInternal()
        {
            try
            {
                var AssignedRoles = ComplianceManagement.Business.InternalComplianceManagement.GetUserRoles(AuthenticationHelper.UserID);
                foreach (var ar in AssignedRoles)
                {
                    string name = ar.Name;
                    if (name.Equals("Reviewer1") || name.Equals("Reviewer2"))
                    {
                        name = "Reviewer";
                    }
                    if (ar.ID.ToString().Equals("10") || ar.ID.ToString().Equals("11"))
                    {
                        name = "My Events";
                    }

                    ListItem item = new ListItem(name, ar.ID.ToString());
                    ListItem item1 = rdInternalaRoleList.Items.FindByText(name);
                    if (item1 == null)
                    {
                        rdInternalaRoleList.Items.Add(item);
                    }
                }

                if (AssignedRoles.Count > 0)
                {
                    rdInternalaRoleList.SelectedIndex = 0;
                }

                if (rdInternalaRoleList.SelectedValue.Equals("4"))
                {
                    divPendingForReviewI.Visible = true;
                    divUpcomingCompliancesI.Visible = false;
                    divOverdueCompliancesI.Visible = true;
                    //EventDashboardsI.Visible = false;
                    //ucActiveEventDashboards.Visible = false;
                    //ucEventDashboards.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                }
                else
                {
                    divPendingForReviewI.Visible = false;
                    divUpcomingCompliancesI.Visible = true;
                    divOverdueCompliancesI.Visible = true;
                    //EventDashboardsI.Visible = false;
                    //ucActiveEventDashboards.Visible = false;
                    //ucEventDashboards.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                }

                if (rdInternalaRoleList.SelectedValue.Equals("6"))
                {
                    divStatusTabI.Visible = false;
                    //UCApproverDashboardInternal.Visible = true;
                    rdApproverReportInternal.Visible = true;
                    //UCApproverGradingInternal.Visible = false;
                    //UCSummaryStatusReportInternal.Visible = false;
                    //ucEventDashboards.Visible = false;
                    rdApproverReportInternal.SelectedIndex = 0;
                    //ucActiveEventDashboards.Visible = false;
                    //ucEventDashboards.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;

                }
                else
                {
                    divStatusTabI.Visible = true;
                    //UCApproverDashboardInternal.Visible = false;
                    rdApproverReportInternal.Visible = false;
                    //UCApproverGradingInternal.Visible = false;
                    //UCSummaryStatusReportInternal.Visible = false;
                   // ucEventDashboards.Visible = false;
                    //setCompliancesCounts();
                    //ucActiveEventDashboards.Visible = false;
                    //ucEventDashboards.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                }

                if (rdInternalaRoleList.SelectedValue.Equals("10") || rdInternalaRoleList.SelectedValue.Equals("11"))
                {
                   // ucEventDashboards.Visible = true;
                    divStatusTabI.Visible = false;
                    //UCApproverDashboardInternal.Visible = false;
                    rdApproverReportInternal.Visible = false;
                    //UCApproverGradingInternal.Visible = false;
                    //UCSummaryStatusReportInternal.Visible = false;
                    //ucActiveEventDashboards.Visible = false;
                    //ucEventDashboards.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }

        protected void rdApproverReport_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdApproverReport.SelectedValue.Equals("dashboard"))
                {
                    //ucApproverDashboard.Visible = true;
                    rdApproverReport.Visible = true;
                    //ucApproverGrading.Visible = false;
                    //ucSummaryStatusReport.Visible = false;
                    //ucActiveEventDashboards.Visible = false;
                    //ucEventDashboards.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                }
                else if (rdApproverReport.SelectedValue.Equals("gradingReport"))
                {
                    //ucApproverDashboard.Visible = false;
                    //ucSummaryStatusReport.Visible = false;
                    rdApproverReport.Visible = true;
                    //ucApproverGrading.Visible = true;
                    //ucActiveEventDashboards.Visible = false;
                    //ucEventDashboards.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                }
                else
                {
                    //ucApproverGrading.Visible = false;
                    //ucApproverDashboard.Visible = false;
                    //ucSummaryStatusReport.Visible = true;
                    rdApproverReport.Visible = true;
                    //ucActiveEventDashboards.Visible = false;
                    //ucEventDashboards.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }
        //Internal
        protected void rdApproverReportInternal_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdApproverReportInternal.SelectedValue.Equals("dashboard"))
                {
                    //UCApproverDashboardInternal.Visible = true;
                    rdApproverReportInternal.Visible = true;
                    ////UCApproverGradingInternal.Visible = false;
                    ////UCSummaryStatusReportInternal.Visible = false;
                    ////ucActiveEventDashboards.Visible = false;
                    ////ucEventDashboards.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                }
                else if (rdApproverReportInternal.SelectedValue.Equals("gradingReport"))
                {
                    //UCApproverDashboardInternal.Visible = false;
                    //UCSummaryStatusReportInternal.Visible = false;
                    rdApproverReportInternal.Visible = true;
                    //UCApproverGradingInternal.Visible = true;
                    //ucActiveEventDashboards.Visible = false;
                    //ucEventDashboards.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                }
                else
                {
                    //UCApproverGradingInternal.Visible = false;
                    //UCApproverDashboardInternal.Visible = false;
                    //UCSummaryStatusReportInternal.Visible = true;
                    rdApproverReportInternal.Visible = true;
                    //ucActiveEventDashboards.Visible = false;
                    //ucEventDashboards.Visible = false;
                    divEventstatusTab.Visible = false;
                    divActiveEventstatusTab.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessageInternal.Text = "Server Error Occured. Please try again.";
            }
        }
        protected void Tab1_Click(object sender, EventArgs e)
        {
            rahulflag = "Y";
            Tab1.CssClass = "Clicked";
            Tab2.CssClass = "Initial";
            MainView.ActiveViewIndex = 0;
            SetRole();
            setCompliancesCounts();
            if (rblRole.SelectedValue.Equals("6"))
            {
                divStatusTab.Visible = false;
                //ucEventDashboards.Visible = false;
                //ucApproverDashboard.Visible = true;
                rdApproverReport.Visible = true;
                //ucApproverGrading.Visible = false;
                //ucSummaryStatusReport.Visible = false;
                rdApproverReport.SelectedIndex = 0;
                //ucApproverDashboard.DrowGraphs(true);
                //ucActiveEventDashboards.Visible = false;
                //ucEventDashboards.Visible = false;
                divActiveEventstatusTab.Visible = false;
                divStatusTabI.Visible = false; 
            }
            Session["RahulSessionFlag"] = "Y";
        }

        protected void Tab2_Click(object sender, EventArgs e)
        {
            int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);

            Tab1.CssClass = "Initial";
            Tab2.CssClass = "Clicked";
            MainView.ActiveViewIndex = 1;
            SetRoleForInternal();
             setInternalCompliancesCounts();

            if (rdInternalaRoleList.SelectedValue.Equals("6"))
            {
                divStatusTabI.Visible = false;       
                //UCApproverDashboardInternal.Visible = true;
                rdApproverReportInternal.Visible = true;
                //UCApproverGradingInternal.Visible = false;
                //UCSummaryStatusReportInternal.Visible = false;
                rdApproverReportInternal.SelectedIndex = 0;
                //UCApproverDashboardInternal.DrowGraphs(customerid,true);
                //ucActiveEventDashboards.Visible = false;
                //ucEventDashboards.Visible = false;
                divActiveEventstatusTab.Visible = false;
                divStatusTabI.Visible = false; 
            }
        }

        protected void btnInternalPendingCompliances_Click(object sender, EventArgs e)
        {                      
            try
            {
                if (rdInternalaRoleList.SelectedItem != null)
                {                    
                    Response.Redirect("~/Controls/OverdueInternalCompliances/Pending/" + rdInternalaRoleList.SelectedItem.Text + "/Internal", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }

        protected void btnInternalUpcomingCompliances_Click(object sender, EventArgs e)
        {
            try
            {
                if (rdInternalaRoleList.SelectedItem != null)
                {

                    Response.Redirect("~/Controls/UpcomingInternalCompliances/Upcoming/" + rdInternalaRoleList.SelectedItem.Text + "/Internal", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }

        protected void btnInternalPendigForreview_Click(object sender, EventArgs e)
        {          
            try
            {
                if (rdInternalaRoleList.SelectedItem != null)
                {

                    Response.Redirect("~/Controls/PendingForReviewInternalCompliance/PendingForReview/" + rdInternalaRoleList.SelectedItem.Text + "/Internal", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }

        protected void btnInternalPerformanceSummary_Click(object sender, EventArgs e)
        {
            try
            {
                if (rdInternalaRoleList.SelectedItem != null)
                {
                    Response.Redirect("~/Common/ComplianceDashboardDisplay/Summary/" + rdInternalaRoleList.SelectedItem.Text + "/Internal", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }

        }

        protected void btnInternalPendingForApproval_Click(object sender, EventArgs e)
        {           
            try
            {
                if (rblRole.SelectedItem != null)
                {

                    Response.Redirect("~/Controls/PendingforApprovelsInternalCompliance/PendingforApproval/" + rdInternalaRoleList.SelectedItem.Text + "/Internal", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }

        }

        protected void btnInternalCheckList_Click(object sender, EventArgs e)
        {
            try
            {
                if (rdInternalaRoleList.SelectedItem != null)
                {
                    Business.ComplianceManagement.GetMappedComplianceCheckList(AuthenticationHelper.UserID, null);
                    Response.Redirect("~/Compliances/CheckList", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }

        }

        protected void btnAssignedEvents_Click(object sender, EventArgs e)
        {
            try
            {
                divEventstatusTab.Visible = false;
                divActiveEventstatusTab.Visible = false;
                //ucEventDashboards.Visible = true;
                //ucActiveEventDashboards.Visible = false;
                Tab1.Visible = false;
                rblRole.Visible = false;
                Tab2.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }

        protected void btnActiveEvents_Click(object sender, EventArgs e)
        {
            try
            {
                divEventstatusTab.Visible = false;
                divActiveEventstatusTab.Visible = false;
                //ucEventDashboards.Visible = false;
                //ucActiveEventDashboards.Visible = true;
                Tab1.Visible = false;
                rblRole.Visible = false;
                Tab2.Visible = false;
                Session["EventRoleID"] = 10;
                //var AssignedRoles = ComplianceManagement.Business.ComplianceManagement.GetUserRoles(AuthenticationHelper.UserID);
                //int EventOwnerRoleID = 0;
                //int EventReviewerRoleID = 0;
                //foreach (var ar in AssignedRoles)
                //{
                //    string name = ar.Name;
                //    if (name.Equals("Event Owner"))
                //    {
                //        EventOwnerRoleID = 10;
                //    }
                //    if (name.Equals("Event Reviewer"))
                //    {
                //        EventReviewerRoleID = 11;
                //    }
                //}
                //if (EventOwnerRoleID == 10 && EventReviewerRoleID == 11)
                //{
                //    Session["EventRoleID"] = 10;
                //}
                //if (EventOwnerRoleID == 10 && EventReviewerRoleID == 0)
                //{
                //    Session["EventRoleID"] = 10;
                //}
                //else
                //{
                //    Session["EventRoleID"] = 11;
                //}

                //ucActiveEventDashboards.BindEventInstances(Convert.ToInt32(Session["EventRoleID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }

        protected void btnRejected_Click(object sender, EventArgs e)
        {
            try
            {
                if (rblRole.SelectedItem != null)
                {
                    //Response.Redirect("~/Controls/UpcomingCompliances/Upcoming/" + rblRole.SelectedItem.Text + "/Statutory", false);
                    Response.Redirect("~/Controls/RejectedCompliances/Rejected/" + rblRole.SelectedItem.Text + "/Statutory", false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                lblErrorMessage.Text = "Server Error Occured. Please try again.";
            }
        }
    }
}