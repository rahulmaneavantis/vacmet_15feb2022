﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Web.Security;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class EventDashboard : System.Web.UI.Page
    {
        public static string rahulflag = "N";
        int checkInternalapplicable = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["userID"] != null)
                {
                    int userID = Convert.ToInt32(Session["userID"]);
                    User user = UserManagement.GetByID(userID);
                    
                    if (user.CustomerID == null)
                    {
                        checkInternalapplicable = 2;
                    }
                    else
                    {
                        Customer customer = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                        if (customer.IComplianceApplicable != null)
                        {
                            checkInternalapplicable = Convert.ToInt32(customer.IComplianceApplicable);
                        }
                    }
                  
                                                            
                }
                if (!string.IsNullOrEmpty(Request.QueryString["type"]))
                {
                    if (Request.QueryString["type"] == "assinged")
                    {
                        liActiveEvents.Attributes.Add("class", "");
                        liAssignedEvents.Attributes.Add("class", "active");

                        ucEventDashboards.Visible = true;
                        ucActiveEventDashboards.Visible = false;
                    }
                    else if (Request.QueryString["type"] == "active")
                    {
                        liAssignedEvents.Attributes.Add("class", "");
                        liActiveEvents.Attributes.Add("class", "active");

                        ucActiveEventDashboards.Visible = true;
                        ucEventDashboards.Visible = false;
                    }
                    else
                    {
                        liActiveEvents.Attributes.Add("class", "");
                        liAssignedEvents.Attributes.Add("class", "active");

                        ucEventDashboards.Visible = true;
                        ucActiveEventDashboards.Visible = false;
                    }
                }
                else
                {
                    liActiveEvents.Attributes.Add("class", "");
                    liAssignedEvents.Attributes.Add("class", "active");

                    ucEventDashboards.Visible = true;
                    ucActiveEventDashboards.Visible = false;
                }
                HiddenFieldType.Value = "";
            }
        }
                
        protected void btnActiveEvents_Click(object sender, EventArgs e)
        {
            try
            {
                ucEventDashboards.Visible = false;
                ucActiveEventDashboards.Visible = true;
                int EventRoleID = 10;
                ucActiveEventDashboards.BindEventInstances(-1,EventRoleID, "",-1);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void liAssignedEvents_Click(object sender, EventArgs e)
        {
            liActiveEvents.Attributes.Add("class", "");
            liAssignedEvents.Attributes.Add("class", "active");

            ucEventDashboards.ClearFields();
            HiddenFieldType.Value = "Assigned Events";

            ucEventDashboards.Visible = true;
            ucActiveEventDashboards.Visible = false;
        }

        protected void liActiveEvents_Click(object sender, EventArgs e)
        {
            liAssignedEvents.Attributes.Add("class", "");
            liActiveEvents.Attributes.Add("class", "active");
            ucEventDashboards.ClearFields();
            HiddenFieldType.Value = "Activated Events";

            ucActiveEventDashboards.Visible = true;
            ucEventDashboards.Visible = false;
        }
    }
}