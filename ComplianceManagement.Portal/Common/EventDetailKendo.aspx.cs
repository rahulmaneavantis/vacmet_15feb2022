﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
    public partial class EventDetailKendo : System.Web.UI.Page
    {
        protected static int parentID;
        protected static int branchID;
        protected static string Path;
        protected static string Flag;
        static string eventType;
        protected void Page_Load(object sender, EventArgs e)
        {
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            Flag = Convert.ToString(AuthenticationHelper.UserID);
            //int parentId = 2149;
            //int branchID = 17027;
            parentID = Convert.ToInt32(Request.QueryString["eventId"]);
            branchID = Convert.ToInt32(Request.QueryString["CustomerBranchID"]);
            lblEventName.Text = getEventName(parentID);
            eventType = Convert.ToString(Request.QueryString["eventType"]);
           //if (eventType == "AssignedEvent")
           //// if(eventType == "1")
           // {
           //     btn_BackAssigned.Visible = true;
           //     btn_BackActivated.Visible = false;
           // }
           //if (eventType == "AciveEvent")
           ////if(eventType == "2")
           // {
           //     btn_BackActivated.Visible = true;
           //     btn_BackAssigned.Visible = false;
           // }
        }
        protected void lnkPreviousSwitch_Click(object sender, EventArgs e)
        {
            try
            {
                if (eventType == "AssignedEvent")
                {
                    Response.Redirect("~/Common/AssignedEventNew.aspx", false);
                }
                if (eventType == "AciveEvent")
                {
                    Response.Redirect("~/Common/ActivatedEventNew.aspx", false);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private string getEventName(int EventID)
        {
            ComplianceDBEntities entities = new ComplianceDBEntities();
            var eventName = (from row in entities.Events
                             where row.ID == EventID
                             select row.Name).SingleOrDefault();
            return eventName;
        }
       
    }
}