﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class DownloadActOverview : System.Web.UI.Page
    {
        protected static string ActDocString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ActID"]))
                {
                    int ActID = Convert.ToInt32(Request.QueryString["ActID"]);

                    if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                    {
                        int ID = Convert.ToInt32(Request.QueryString["ID"]);
                        //BindFilePerticular(ActID, ID);
                        List<Sp_Act_Document_Result> ActDocumentlst = new List<Sp_Act_Document_Result>();
                        ActDocumentlst = ActManagement.getActDocumentIDWise(ActID, ID).ToList();
                        if (ActDocumentlst.Count > 0)
                        {
                            foreach (var file in ActDocumentlst)
                            {
                                string name = Path.GetFileName(file.FilePath);
                                string ext = Path.GetExtension(file.FilePath);
                                string type = "";
                                if (ext != null)
                                {
                                    switch (ext.ToLower())
                                    {
                                        case ".txt ":
                                            type = "text/plain";
                                            break;
                                        case ".doc":
                                        case ".rtf":
                                        case ".docx":
                                            type = "Application/msword";
                                            break;
                                        case ".xlsx":
                                        case ".xls":
                                            type = "Application/x-msexcel";
                                            break;
                                        case ".jpg":
                                        case ".jpeg":
                                            type = "image/jpeg";
                                            break;
                                        case ".gif":
                                            type = "image/GIF";
                                            break;
                                        case ".pdf":
                                            type = "application/pdf";
                                            break;
                                    }
                                }

                                HttpContext.Current.Response.Buffer = true;
                                HttpContext.Current.Response.ClearContent();
                                HttpContext.Current.Response.ClearHeaders();
                                HttpContext.Current.Response.Clear();

                                if (type != "")
                                    Response.ContentType = type;
                                else
                                    HttpContext.Current.Response.ContentType = "application/octet-stream";

                                //HttpContext.Current.Response.ContentType = "application/octet-stream";
                                HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename= " + file.FileName);
                                HttpContext.Current.Response.TransmitFile(HttpContext.Current.Server.MapPath(file.FilePath));
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                        }
                    }
                    else
                    {
                        BindFiles(ActID);
                    }
                }
            }
        }

        private void BindFiles(int ActID)
        {

            var MasterQuery = ActManagement.getFileNamebyID(ActID);
            var DistinctTransQuery = MasterQuery;
            DistinctTransQuery = DistinctTransQuery.GroupBy(x => x.DocumentTypeID).Select(x => x.FirstOrDefault()).ToList();

            //List<Sp_Act_Document_Result> CMPDocuments = Business.ActManagement.getFileNamebyID(ActID);

            if (DistinctTransQuery != null)
            {
                if (DistinctTransQuery.Count > 0)
                {
                    System.Text.StringBuilder stringbuilder = new System.Text.StringBuilder();
                    stringbuilder.Append(@"<table id='basic' width='100%'>");

                    foreach (var item in DistinctTransQuery)
                    {
                        stringbuilder.Append("<tr data-node-id=" + Convert.ToString(item.DocumentTypeID) + ">" +
                                    " <td class='locationheadLocationbg'>" + Convert.ToString(item.DocumentType) + "</td>");
                        stringbuilder.Append("</tr>");
                        var actDocData = MasterQuery.Where(entry => entry.Act_ID == ActID && entry.DocumentTypeID == item.DocumentTypeID).ToList();
                        //var actDocData = ActManagement.getActDocumentTypeWise(ActID, item.DocumentTypeID);
                        int i = 0;
                        foreach (var item1 in actDocData)
                        {
                            i += 1;
                            string id = Convert.ToString(item.DocumentTypeID) + "." + i;
                            string FileName = "";
                            string[] filename = item1.FileName.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            if (filename[0].Length > 90)
                            {
                                FileName = filename[0].Substring(1, 90) + "." + filename[1];
                            }
                            else
                            {
                                FileName = item1.FileName;
                            }

                            if (item.Act_TypeVersionDate != null)
                            {
                                FileName = FileName + " on " + Convert.ToDateTime(item1.Act_TypeVersionDate).ToString("MMM-yy");
                            }

                            stringbuilder.Append("<tr data-node-id=" + Convert.ToString(id) + " data-node-pid=" + Convert.ToString(item.DocumentTypeID) + " >" +
                                 " <td class='locationheadLocationbg'>" + Convert.ToString(FileName) + "</td>");
                            stringbuilder.Append("<td class='downloadcss' onclick ='OpenDocumentDowmloadOverviewpup(" + ActID + "," + item1.ID + ")'> Download </td>");
                            stringbuilder.Append("</tr>");
                        }
                    }
                    stringbuilder.Append("</table>");
                    ActDocString = stringbuilder.ToString().Trim();

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDownloadDocument();", true);
                }
            }
        }

        //private void BindFilePerticular(int ActID,int ID)
        //{
        //    List<Sp_Act_Document_Result> CMPDocuments = Business.ActManagement.getFileNameID(ActID,ID);

        //    if (CMPDocuments != null)
        //    {
        //        if (CMPDocuments.Count > 0)
        //        {
        //            rptActDocVersion.DataSource = CMPDocuments.OrderByDescending(entry => entry.ID);
        //            rptActDocVersion.DataBind();

        //            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDownloadDocument();", true);
        //        }
        //    }
        //}
        //protected void rptActDocVersion_ItemDataBound(object Sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnActVersionDoc");
        //        var scriptManager = ScriptManager.GetCurrent(this.Page);
        //        scriptManager.RegisterPostBackControl(lblDownLoadfile);
        //    }
        //}
        //protected void rptActDocVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        //{
        //    try
        //    {
        //        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        //        string version = Convert.ToString(commandArgs[1]);
        //        int actID = Convert.ToInt32(commandArgs[0]);
        //        lblMessageAct.Text = string.Empty;
        //        List<Sp_Act_Document_Result> ActDocumentlst = new List<Sp_Act_Document_Result>();
        //        ActDocumentlst = ActManagement.getFileNamebyID(Convert.ToInt32(actID)).ToList();

        //        if (e.CommandName.Equals("Download"))
        //        {
        //            #region Download                   
        //            var files = ActDocumentlst.Where(entry => entry.Version == version).ToList();
        //            if (files.Count > 0)
        //            {
        //                foreach (var file in files)
        //                {
        //                    Response.Buffer = true;
        //                    Response.ClearContent();
        //                    Response.ClearHeaders();
        //                    Response.Clear();
        //                    Response.ContentType = "application/octet-stream";
        //                    Response.AddHeader("content-disposition", "attachment; filename= " + file.FileName);
        //                    Response.TransmitFile(Server.MapPath(file.FilePath));
        //                    Response.Flush();
        //                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
        //                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
        //                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

        //                }
        //            }
        //            #endregion
        //        }
        //        else if (e.CommandName.Equals("View"))
        //        {
        //            #region View
        //            var ActDocument = ActDocumentlst.Where(entry => entry.Version == version).ToList();
        //            if (ActDocument.Count > 0)
        //            {
        //                foreach (var file in ActDocument)
        //                {
        //                    string filePath = Server.MapPath(file.FilePath);

        //                    if (file.FilePath != null && File.Exists(filePath))
        //                    {
        //                        rptActDocVersionView.DataSource = ActDocumentlst.OrderBy(entry => entry.Version);
        //                        rptActDocVersionView.DataBind();
        //                        UpdatePanel6.Update();

        //                        string Folder = "~/TempFiles";
        //                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

        //                        string DateFolder = Folder + "/" + File;

        //                        string extension = System.IO.Path.GetExtension(filePath);
        //                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z" || extension.ToUpper() == ".RAR")
        //                        {
        //                            lblMessageAct.Text = extension.ToUpper().Trim() + " file can't view please download it";
        //                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileActPopUp();", true);
        //                        }
        //                        else
        //                        {
        //                            Directory.CreateDirectory(Server.MapPath(DateFolder));

        //                            if (!Directory.Exists(DateFolder))
        //                            {
        //                                Directory.CreateDirectory(Server.MapPath(DateFolder));
        //                            }

        //                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

        //                            string User = AuthenticationHelper.UserID + "" + FileDate;

        //                            string FileName = DateFolder + "/" + User + "" + extension;

        //                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
        //                            BinaryWriter bw = new BinaryWriter(fs);
        //                            bw.Write(DocumentManagement.ReadDocFiles(filePath));
        //                            bw.Close();

        //                            FileName = FileName.Substring(2, FileName.Length - 2);
        //                            lblMessageAct.Text = "";
        //                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileAct('" + FileName + "');", true);

        //                        }
        //                    }
        //                    else
        //                    {
        //                        lblMessageAct.Text = "There is no file to preview";
        //                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileActPopUp();", true);
        //                        break;
        //                    }
        //                }
        //            }
        //            #endregion
        //        }
        //        //upComplianceDetails.Update();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        //protected void rptActDocVersionView_ItemDataBound(object Sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        var scriptManager = ScriptManager.GetCurrent(this.Page);
        //        LinkButton lblActDocumentVersionView = (LinkButton)e.Item.FindControl("lblActDocumentVersionView");
        //        scriptManager.RegisterAsyncPostBackControl(lblActDocumentVersionView);
        //    }
        //}
        //protected void rptActDocVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        //{
        //    try
        //    {
        //        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        //        string version = Convert.ToString(commandArgs[1]);
        //        int actID = Convert.ToInt32(commandArgs[0]);
        //        lblMessageAct.Text = string.Empty;
        //        List<Sp_Act_Document_Result> ActDocumentlst = new List<Sp_Act_Document_Result>();
        //        ActDocumentlst = ActManagement.getFileNamebyID(Convert.ToInt32(actID)).ToList();

        //        if (e.CommandName.Equals("View"))
        //        {
        //            #region View
        //            var ActDocument = ActDocumentlst.Where(entry => entry.Version == version).ToList();
        //            if (ActDocument.Count > 0)
        //            {
        //                foreach (var file in ActDocument)
        //                {
        //                    string filePath = Server.MapPath(file.FilePath);

        //                    if (file.FilePath != null && File.Exists(filePath))
        //                    {
        //                        string Folder = "~/TempFiles";
        //                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

        //                        string DateFolder = Folder + "/" + File;

        //                        string extension = System.IO.Path.GetExtension(filePath);
        //                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z" || extension.ToUpper() == ".RAR")
        //                        {
        //                            lblMessageAct.Text = extension.ToUpper().Trim() + " file can't view please download it";
        //                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileActPopUp();", true);
        //                        }
        //                        else
        //                        {
        //                            Directory.CreateDirectory(Server.MapPath(DateFolder));

        //                            if (!Directory.Exists(DateFolder))
        //                            {
        //                                Directory.CreateDirectory(Server.MapPath(DateFolder));
        //                            }

        //                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

        //                            string User = AuthenticationHelper.UserID + "" + FileDate;

        //                            string FileName = DateFolder + "/" + User + "" + extension;

        //                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
        //                            BinaryWriter bw = new BinaryWriter(fs);
        //                            bw.Write(DocumentManagement.ReadDocFiles(filePath));
        //                            bw.Close();

        //                            FileName = FileName.Substring(2, FileName.Length - 2);
        //                            lblMessageAct.Text = "";
        //                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileAct('" + FileName + "');", true);

        //                        }
        //                    }
        //                    else
        //                    {
        //                        lblMessageAct.Text = "There is no file to preview";
        //                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileActPopUp();", true);
        //                        break;
        //                    }
        //                }
        //            }
        //            #endregion
        //        }
        //        UpdatePanel6.Update();
        //        //upComplianceDetails.Update();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
    }
}