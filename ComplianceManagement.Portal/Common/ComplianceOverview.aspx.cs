﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class ComplianceOverview : System.Web.UI.Page
    {
        protected static int CustId;
        protected static int UId;
        protected static int SOnID;
        protected static int compInstanceID;
        protected static string KendoPath;
        protected static string NumberofDays;
        protected static string CDNumberofDays;
        protected static string NTNumberofDays;
        protected static string CTNumberofDays;
        protected static string NumberofMonths;
        static string sampleFormPath1 = "";
        public static string CompDocReviewPath = "";
        protected static bool IsNotCompiled;
        protected static string Authorization;
        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ComplianceScheduleID"]) && !string.IsNullOrEmpty(Request.QueryString["ComplainceInstatnceID"]))
            {
                if (!IsPostBack)
                {
                    if (AuthenticationHelper.CustomerID == 29)
                    {
                        sequenceid.Visible = true;
                    }
                    else
                    {
                        sequenceid.Visible = false;
                    }
                                                 
                    UId = Convert.ToInt32(AuthenticationHelper.UserID);
                    CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    int ScheduledOnID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);
                    int complianceInstanceID = Convert.ToInt32(Request.QueryString["ComplainceInstatnceID"]);

                    BindTransactionDetails(ScheduledOnID, complianceInstanceID);


                    KendoPath = ConfigurationManager.AppSettings["KendoPathApp"];
                    SOnID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);
                    compInstanceID = Convert.ToInt32(Request.QueryString["ComplainceInstatnceID"]);
                    BindAuditLogs(ScheduledOnID, complianceInstanceID);
                    BindUserDetail(complianceInstanceID);

                    ViewState["complianceInstanceScheduleOnID"] = ScheduledOnID;
                    ViewState["complianceInstanceAuditID"] = complianceInstanceID;

                    //ComplianceDBEntities entities = new ComplianceDBEntities();
                    //var ParentEvent = entities.SP_GetAuditLogDate(ScheduledOnID).ToList();
                    //gvParentGrid.DataSource = ParentEvent;
                    //gvParentGrid.DataBind();

                    licomplianceoverview1.Attributes.Add("class", "active");

                //       $('#licomplianceoverview').css('background-color', '#1fd9e1');
                //$('#licomplianceoverview').css('color', 'white');

                    licomplianceoverview1.Attributes.Add("class", "active");
                    lidocuments1.Attributes.Add("class", "");
                    liAudit1.Attributes.Add("class", "");
                    liUpdates1.Attributes.Add("class", "");
                    liAuditlog1.Attributes.Add("class", "");

                    complianceoverview.Attributes.Remove("class");
                    complianceoverview.Attributes.Add("class", "tab-pane active");
                    documents.Attributes.Remove("class");
                    documents.Attributes.Add("class", "tab-pane");
                    audits.Attributes.Remove("class");
                    audits.Attributes.Add("class", "tab-pane");

                    updateView.Attributes.Remove("class");
                    updateView.Attributes.Add("class", "tab-pane");

                    auditlogView.Attributes.Remove("class");
                    auditlogView.Attributes.Add("class", "tab-pane");


                    divMgmtRemrks.Visible = false;

                    if (AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.ComplianceProductType == 3)
                    {
                        divMgmtRemrks.Visible = true;
                    }
                    int customizedid = CustomerManagement.GetCustomizedCustomerid(CustId, "GSTFields");
                    if (customizedid == CustId)
                    {
                        divgstno.Visible = true;
                    }

                }
            }
        }

        public void BindUserDetail(int complianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                var lstAuditLogs = entities.SP_GetAuditLogDetailNew(complianceInstanceID).ToList();

                var selectedUserId = lstAuditLogs.Select(x => x.UserId).Distinct().ToList();
                
                var allUsers = UserManagement.GetAllUser(customerID).ToList();
                allUsers = allUsers.Where(x => selectedUserId.Contains(x.ID)).ToList();

                var users = (from row in allUsers
                             select new
                             {
                                 ID = row.ID,
                                 Name = row.FirstName + " " + row.LastName
                             }).OrderBy(entry => entry.Name).ToList<object>();

                ddlUsers_AuditLog.DataValueField = "ID";
                ddlUsers_AuditLog.DataTextField = "Name";
                ddlUsers_AuditLog.DataSource = users;
                ddlUsers_AuditLog.DataBind();
            }
        }

        public void BindAuditLogs(int ScheduledOnID,int complianceInstanceID)
        {
            try
            {
                long totalRowCount = 0;
                                
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    int userID = -1;
                    if (!(string.IsNullOrEmpty(ddlUsers_AuditLog.SelectedValue)))
                    {
                        userID = Convert.ToInt32(ddlUsers_AuditLog.SelectedValue);
                    }
                    string fromDate = string.Empty;
                    string toDate = string.Empty;
                
                if (complianceInstanceID != 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        entities.Database.CommandTimeout = 300;
                        var lstAuditLogs = entities.SP_GetAuditLogDetailNew(complianceInstanceID).ToList();

                        if (userID != -1 && userID != 0)
                            lstAuditLogs = lstAuditLogs.Where(row => row.UserId == userID).ToList();

                        if (!(string.IsNullOrEmpty(txtFromDate_AuditLog.Text.Trim())))
                        {
                            try
                            {
                                fromDate = txtFromDate_AuditLog.Text.Trim();
                                lstAuditLogs = lstAuditLogs.Where(row => row.SendDate >= Convert.ToDateTime(fromDate)).ToList();
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                        }
                        if (!(string.IsNullOrEmpty(txtToDate_AuditLog.Text.Trim())))
                        {
                            try
                            {
                                toDate = txtToDate_AuditLog.Text.Trim();
                                lstAuditLogs = lstAuditLogs.Where(row => row.SendDate <= Convert.ToDateTime(toDate)).ToList();
                            }
                            catch (Exception)
                            {

                            }
                        }
                        divAuditLog_Vertical.InnerHtml = ShowAuditLog_Centered_Clickable(lstAuditLogs);

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public string ShowAuditLog_Centered_Clickable(List<SP_GetAuditLogDetailNew_Result> lstAuditLogs)
        {
            try
            {
                StringBuilder strTimeLineHTML = new StringBuilder();

                if (lstAuditLogs.Count > 0)
                {
                    var minDate = lstAuditLogs.Select(row => row.SendDate).Min();                  
                    var maxDate = lstAuditLogs.Select(row => row.SendDate).Max();

                    if (minDate != null && maxDate != null)
                    {
                        strTimeLineHTML.Append("<ul class=\"timeline\">");

                        int accordionCount = 0;
                        var startDate = maxDate;
                        int flag = 0;
                        do
                        {
                            flag = flag + 1;
                            DateTime MonthStartDate = ContractCommonMethods.GetFirstDayOfMonth(Convert.ToDateTime(startDate));
                            DateTime MonthEndDate = ContractCommonMethods.GetLastDayOfMonth(Convert.ToDateTime(startDate));

                            var auditLogsMonthWise = lstAuditLogs.Where(row => row.SendDate >= MonthStartDate && row.SendDate <= MonthEndDate).OrderByDescending(row => row.SendDate).ToList();

                            if (auditLogsMonthWise.Count > 0)
                            {
                                accordionCount++;

                                string accordionCollapseClass = "panel-collapse collapse ";//change by Ruchi on 12th sep 2018

                                string faIcon = "fa fa-minus";
                                if (accordionCount > 1)
                                {
                                    accordionCollapseClass = "panel-collapse collapse";
                                    faIcon = "fa fa-minus";
                                }

                                if (accordionCount % 2 == 0)
                                    strTimeLineHTML.Append("<li class=\"timeline-inverted\">");
                                else
                                    strTimeLineHTML.Append("<li>");

                                strTimeLineHTML.Append("<div class=\"timeline-badge\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\"" +
                                    "data-toggle=\"tooltip\" data-placement=\"bottom\" ToolTip=\"View Detail\"><i id=\"i" + accordionCount + "\" class=\"" + faIcon + "\"></i></div>");

                                strTimeLineHTML.Append("<div class=\"timeline-panel\">");

                                strTimeLineHTML.Append("<div class=\"panel-heading clickable newProducts\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\">" +
                                    "<a class=\"newProductsLink\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse" + accordionCount + "\">" +
                                    "<h2 class=\"panel-title new-products-title\">" +
                                    CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToDateTime(startDate).Month) + "-" + Convert.ToDateTime(startDate).Year + "</h2>" +
                                    "</a>" +
                                "</div>");

                                strTimeLineHTML.Append("<div id=\"divCollapse" + accordionCount + "\" class=\"panel-body in\" style =\"max-height: 300px; overflow-y: auto; padding: 0px;\">" +
                                                        "<div id=\"collapse" + accordionCount + "\" class=" + accordionCollapseClass + "\">" +
                                                                "<ul class=newProductsList>");

                                auditLogsMonthWise.ForEach(eachAuditLog =>
                                {
                                    strTimeLineHTML.Append("<li>" +
                                         "<div class=\"timeline-body\">" +
                                          "<div class=\"col-md-12 pl0\">" +
                                             //"<p><i class=\"fa fa-clock-o mlr5\"></i>" + eachAuditLog.SendDate + "</p>" +
                                             "<p><i class=\"fa fa-clock-o mlr5\"></i>" + String.Format("{0:dd MMM yyyy} {0:t}", eachAuditLog.SendDate) + "</p>" +
                                            //"<p><i class=\"fa fa-clock-o mlr5\"></i>" + eachAuditLog.SendDate.ToString("dd-MM-yyyy h:mm tt") + "</p>" +
                                            "</div>" +
                                             "<div class=\"col-md-12 pl0\">" +
                                                "<p class=\"timeline-title\">" + eachAuditLog.UserName + "</p>" +
                                             "</div>" +
                                            "<div class=\"col-md-12 pl0\">" +
                                                "<p>" + eachAuditLog.Remarks + "</p>" +
                                            "</div>" +
                                             "<div class=\"col-md-12 pl0\">" +
                                                "<p>" + "" + "</p>" +
                                            "</div>" +
                                         "</div>" +
                                   "</li>");
                                    strTimeLineHTML.Append("<div class=\"clearfix\"></div>");

                                    //<small class=\"text-muted\"></small>
                                });

                                strTimeLineHTML.Append("</ul></div>");
                                strTimeLineHTML.Append("</div>");

                                startDate = MonthStartDate.AddDays(-1);
                            }
                            else
                            {
                                startDate = MonthStartDate.AddDays(-1);
                            }

                        }while (startDate >= minDate);

                        strTimeLineHTML.Append("</ul>");
                    }
                }


                //if (lstAuditLogs.Count > 0)
                //{
                //    //var minDate = lstAuditLogs.Select(row => row.SendDate).Min();
                //    var minDate = lstAuditLogs.Select(row => row.SendDate).Min();
                //    var maxDate = lstAuditLogs.Select(row => row.SendDate).Max();

                //    if (minDate != null && maxDate != null)
                //    {
                //        strTimeLineHTML.Append("<ul class=\"timeline\">");

                //        int accordionCount = 0;
                //        var startDate = maxDate;
                //        int flag = 0;
                //        do
                //        {
                //            flag = flag + 1;
                //            DateTime MonthStartDate = ContractCommonMethods.GetFirstDayOfMonth(Convert.ToDateTime(startDate));
                //            DateTime MonthEndDate = ContractCommonMethods.GetLastDayOfMonth(Convert.ToDateTime(startDate));

                //            var auditLogsMonthWise = lstAuditLogs.Where(row => row.SendDate >= MonthStartDate && row.SendDate <= MonthEndDate).OrderByDescending(row => row.SendDate).ToList();

                //            if (auditLogsMonthWise.Count > 0)
                //            {
                //                accordionCount++;

                //                string accordionCollapseClass = "panel-collapse collapse ";//change by Ruchi on 12th sep 2018

                //                string faIcon = "fa fa-minus";
                //                if (accordionCount > 1)
                //                {
                //                    accordionCollapseClass = "panel-collapse collapse";
                //                    faIcon = "fa fa-minus";
                //                }

                //                if (accordionCount % 2 == 0)
                //                    strTimeLineHTML.Append("<li class=\"timeline-inverted\">");
                //                else
                //                    strTimeLineHTML.Append("<li>");

                //                strTimeLineHTML.Append("<div class=\"timeline-badge\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\"" +
                //                    "data-toggle=\"tooltip\" data-placement=\"bottom\" ToolTip=\"View Detail\"><i id=\"i" + accordionCount + "\" class=\"" + faIcon + "\"></i></div>");

                //                strTimeLineHTML.Append("<div class=\"timeline-panel\">");

                //                strTimeLineHTML.Append("<div class=\"panel-heading clickable newProducts\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\">" +
                //                    "<a class=\"newProductsLink\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse" + accordionCount + "\">" +
                //                    "<h2 class=\"panel-title new-products-title\">" +
                //                    CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToDateTime(startDate).Month) + "-" + Convert.ToDateTime(startDate).Year + "</h2>" +
                //                    "</a>" +
                //                "</div>");

                //                strTimeLineHTML.Append("<div id=\"divCollapse" + accordionCount + "\" class=\"panel-body in\" style =\"max-height: 300px; overflow-y: auto; margin-top: 5px;\">" +
                //                                        "<div id=\"collapse" + accordionCount + "\" class=" + accordionCollapseClass + "\">" +
                //                                                "<ul class=newProductsList>");

                //                auditLogsMonthWise.ForEach(eachAuditLog =>
                //                {
                //                    strTimeLineHTML.Append("<li>" +
                //                         "<div class=\"timeline-body\">" +
                //                          "<div class=\"col-md-12 pl0\">" +
                //                             "<p><i class=\"fa fa-clock-o mlr5\"></i>" + eachAuditLog.SendDate + "</p>" +
                //                            //"<p><i class=\"fa fa-clock-o mlr5\"></i>" + eachAuditLog.SendDate.ToString("dd-MM-yyyy h:mm tt") + "</p>" +
                //                            "</div>" +
                //                             "<div class=\"col-md-12 pl0\">" +
                //                                "<p class=\"timeline-title\">" + eachAuditLog.UserName + "</p>" +
                //                             "</div>" +
                //                            "<div class=\"col-md-12 pl0\">" +
                //                                "<p>" + eachAuditLog.Remarks + "</p>" +
                //                            "</div>" +
                //                             "<div class=\"col-md-12 pl0\">" +
                //                                "<p>" + "" + "</p>" +
                //                            "</div>" +
                //                         "</div>" +
                //                   "</li>");
                //                    strTimeLineHTML.Append("<div class=\"clearfix\"></div>");

                //                    //<small class=\"text-muted\"></small>
                //                });

                //                strTimeLineHTML.Append("</ul></div>");
                //                strTimeLineHTML.Append("</div>");

                //                startDate = MonthStartDate.AddDays(-1);
                //            }

                //        }while(lstAuditLogs.Count > flag)
                //        //while (startDate >= minDate);

                //        strTimeLineHTML.Append("</ul>");
                //    }
                //}

                return strTimeLineHTML.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }


        private void BindTransactionDetails(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {
                lblPenalty.Text = string.Empty;
                lblRisk.Text = string.Empty;            
                int customerID =Convert.ToInt32(AuthenticationHelper.CustomerID);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {               
                    var AllComData = (from row in entities.GetDetailsbyScheduleonID(ScheduledOnID)
                                      select row).FirstOrDefault();
                  
                    var RCT = Business.ComplianceManagement.GetCurrentStatusByComplianceID(ScheduledOnID);
                    if (AllComData != null && RCT != null)
                    {
                        var masterquery = (from row in entities.ComplianceAssignments
                                           join row1 in entities.Users
                                           on row.UserID equals row1.ID
                                           where row.ComplianceInstanceID == complianceInstanceID
                                           select new
                                           {
                                               name = row1.FirstName + " " + row1.LastName,
                                               roleid = row.RoleID
                                           }).ToList();
                        var PerformerName = masterquery.Where(en => en.roleid == 3).Select(ent => ent.name).FirstOrDefault();
                        var ReviewerName = masterquery.Where(en => en.roleid == 4).Select(ent => ent.name).FirstOrDefault();
                        lblPerformer.Text = PerformerName;
                        lblReviewer.Text = ReviewerName;
                        hdnperformer.Value = PerformerName;
                        hdnreviewer.Value = ReviewerName;

                        string customer = ConfigurationManager.AppSettings["NotCompliedCustID"].ToString();


                        List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                        if (PenaltyNotDisplayCustomerList.Count > 0)
                        {
                            foreach (string PList in PenaltyNotDisplayCustomerList)
                            {
                                if (PList == customerID.ToString())
                                {
                                    IsNotCompiled = true;
                                    break;
                                }
                            }
                        }

                        string getSequenceID = Business.DocumentManagement.GetSequenceID(complianceInstanceID);
                        lblLabel.Text = getSequenceID;

                        hdnCustomerBranchId.Value = Convert.ToString(AllComData.CustomerBranchID);

                        if (RCT.Status == "Interim Rejected" || RCT.Status == "Interim Review Approved"
                            || RCT.Status == "Submitted For Interim Review")
                        {
                            lblStatus.Text = RCT.Status.ToLower();
                            hdnstatus.Value = RCT.Status.ToLower();
                            divRiskType.Attributes["style"] = "background-color:red;";
                            StatusColor.Attributes["style"] = "border-left-color:red;";
                        }
                        else if (RCT.Status.ToLower() == "in progress")
                        {
                            lblStatus.Text = RCT.Status;
                            hdnstatus.Value = RCT.Status;
                            divRiskType.Attributes["style"] = "background-color:red;";
                            StatusColor.Attributes["style"] = "border-left-color:red;";
                        }
                        else if (Convert.ToDateTime(AllComData.PerformerScheduledOn) < DateTime.Now)
                        {
                            if (RCT.Status.ToLower() == "open"
                                || RCT.Status.ToLower() == "not  complied"
                                || RCT.Status.ToLower() == "complied but pending review"
                                || RCT.Status.ToLower() == "complied delayed but pending review"
                                || RCT.Status.ToLower() == "rejected"
                                || RCT.Status.ToLower() == "not  applicable"
                                || RCT.Status.ToLower() == "complied  but document pending"
                                || RCT.Status.ToLower() == "overdue")
                            {
                                lblStatus.Text = "Overdue";
                                hdnstatus.Value = "Overdue";
                                divRiskType.Attributes["style"] = "background-color:red;";
                                StatusColor.Attributes["style"] = "border-left-color:red;";
                            }
                            else if (RCT.Status.ToLower() == "closed-timely" || RCT.Status.ToLower() == "closed-delayed"
                                || RCT.Status.ToLower() == "approved" || RCT.Status.ToLower() == "revise compliance"
                                || RCT.Status.ToLower() == "not applicable" || RCT.Status.ToLower() == "complied but document pending")
                            {
                                lblStatus.Text = "Completed";
                                hdnstatus.Value = "Completed";
                                divRiskType.Attributes["style"] = "background-color:green;";
                                StatusColor.Attributes["style"] = "border-left-color:green;";
                            }
                        }
                        else if (Convert.ToDateTime(AllComData.PerformerScheduledOn) >= DateTime.Now)
                        {
                            if (RCT.Status.ToLower() == "open" || RCT.Status.ToLower() == "not  complied" || RCT.Status.ToLower() == "complied but pending review" || RCT.Status.ToLower() == "complied delayed but pending review" || RCT.Status.ToLower() == "rejected")
                            {
                                lblStatus.Text = "Upcoming";
                                hdnstatus.Value = "Upcoming";
                                divRiskType.Attributes["style"] = "background-color:#ffbf00;";
                                StatusColor.Attributes["style"] = "border-left-color:#ffbf00;";
                            }
                            else if (RCT.Status.ToLower() == "closed-timely" || RCT.Status.ToLower() == "closed-delayed" || RCT.Status.ToLower() == "revise compliance" || RCT.Status.ToLower() == "not applicable" || RCT.Status.ToLower() == "complied but document pending")
                            {
                                lblStatus.Text = "Completed";
                                hdnstatus.Value = "Completed";
                                divRiskType.Attributes["style"] = "background-color:green;";
                                StatusColor.Attributes["style"] = "border-left-color:green;";
                            }
                        }
                        if (IsNotCompiled == true)
                        {
                            if (RCT.Status.ToLower() == "not complied")
                            {
                                lblStatus.Text = "Not Complied";
                                hdnstatus.Value = "Not Complied";
                                divRiskType.Attributes["style"] = "background-color:#2195f2;";
                                StatusColor.Attributes["style"] = "border-left-color:#2195f2;";
                            }
                        }
                        lblLocation.Text = AllComData.LocationName;
                        lblDueDate.Text = Convert.ToDateTime(AllComData.PerformerScheduledOn).ToString("dd-MMM-yyyy");
                        hdnduedate.Value = Convert.ToDateTime(AllComData.PerformerScheduledOn).ToString("dd-MMM-yyyy");
                        lblPeriod.Text = AllComData.ForMonth;
                        if (AllComData.ActualScheduleon != null)
                        {
                            TRactualduedate.Visible = true;
                            lblActualDueDate.Text = Convert.ToDateTime(AllComData.ActualScheduleon).ToString("dd-MMM-yyyy");
                        }

                        LblGstno.Text = AllComData.GSTNumber;



                        ViewState["ScheduledOnID"] = ScheduledOnID;

                        var dataChart = entities.Sp_Get_ComplianceHistory(Convert.ToInt32(AllComData.ComplianceInstanceID), Convert.ToInt32(AllComData.CustomerBranchID)).ToList();
                        if (IsNotCompiled == true)
                        {
                            List<int?> overdue = new List<int?>();
                            List<int?> ClosedTimely = new List<int?>();
                            List<int?> ClosedDelayed = new List<int?>();
                            List<int?> NotComplied = new List<int?>();
                            foreach (var item in dataChart)
                            {
                                if (!string.IsNullOrEmpty(item.Status))
                                {
                                    if (item.Status.Equals("Overdue"))
                                    {
                                        int datediff = Convert.ToInt32((DateTime.Now - item.ScheduleOn).TotalDays);
                                        overdue.Add(datediff);
                                        ClosedTimely.Add(0);
                                        ClosedDelayed.Add(0);
                                        NotComplied.Add(0);
                                    }
                                    if (item.Status.Equals("Closed-Timely") || item.Status.Equals("ClosedTimely"))
                                    {
                                        overdue.Add(0);
                                        ClosedTimely.Add(item.DaysDiff);
                                        ClosedDelayed.Add(0);
                                        NotComplied.Add(0);
                                    }
                                    if (item.Status.Equals("Closed-Delayed") || item.Status.Equals("ClosedDelayed"))
                                    {
                                        overdue.Add(0);
                                        ClosedTimely.Add(0);
                                        ClosedDelayed.Add(item.DaysDiff);
                                        NotComplied.Add(0);
                                    }
                                    if (item.Status.Equals("Not Complied"))
                                    {
                                        overdue.Add(0);
                                        ClosedTimely.Add(0);
                                        ClosedDelayed.Add(0);
                                        NotComplied.Add(item.DaysDiff);
                                    }
                                }
                            }
                            NumberofDays = String.Join(",", overdue.Select(x => x.Value).ToList());
                            CTNumberofDays = String.Join(",", ClosedTimely.Select(x => x.Value).ToList());
                            CDNumberofDays = String.Join(",", ClosedDelayed.Select(x => x.Value).ToList());
                            NTNumberofDays = String.Join(",", NotComplied.Select(x => x.Value).ToList());
                            NumberofMonths = Convert.ToString(JsonConvert.SerializeObject(dataChart.Select(x => x.MonthStatusChangedOn).ToList()));
                        }
                        else
                        {
                            List<int?> overdue = new List<int?>();
                            List<int?> ClosedTimely = new List<int?>();
                            List<int?> ClosedDelayed = new List<int?>();
                            foreach (var item in dataChart)
                            {
                                if (!string.IsNullOrEmpty(item.Status))
                                {
                                    if (item.Status.Equals("Overdue"))
                                    {
                                        int datediff = Convert.ToInt32((DateTime.Now - item.ScheduleOn).TotalDays);
                                        overdue.Add(datediff);
                                        ClosedTimely.Add(0);
                                        ClosedDelayed.Add(0);
                                    }
                                    if (item.Status.Equals("Closed-Timely") || item.Status.Equals("ClosedTimely"))
                                    {
                                        overdue.Add(0);
                                        ClosedTimely.Add(item.DaysDiff);
                                        ClosedDelayed.Add(0);
                                    }
                                    if (item.Status.Equals("Closed-Delayed") || item.Status.Equals("ClosedDelayed"))
                                    {
                                        overdue.Add(0);
                                        ClosedTimely.Add(0);
                                        ClosedDelayed.Add(item.DaysDiff);
                                    }
                                }
                            }
                            NumberofDays = String.Join(",", overdue.Select(x => x.Value).ToList());
                            CTNumberofDays = String.Join(",", ClosedTimely.Select(x => x.Value).ToList());
                            CDNumberofDays = String.Join(",", ClosedDelayed.Select(x => x.Value).ToList());
                            NumberofMonths = Convert.ToString(JsonConvert.SerializeObject(dataChart.Select(x => x.MonthStatusChangedOn).ToList()));
                        }


                        lnkSampleForm.Text = Convert.ToString(AllComData.SampleFormLink);
                        lblComplianceDiscription.Text = AllComData.ShortDescription;
                        lblDetailedDiscription.Text = AllComData.Description;


                        var cfrequency = Business.ComplianceManagement.GetClientBasedCheckListFrequency(AuthenticationHelper.CustomerID, AllComData.ComplianceID);
                        if (cfrequency != null)
                        {
                            lblFrequency.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(cfrequency.CFrequency != null ? (int)cfrequency.CFrequency : -1));
                        }
                        else
                        {
                            lblFrequency.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(AllComData.Frequency != null ? (int)AllComData.Frequency : -1));
                        }
                        lblRefrenceText.Text = WebUtility.HtmlEncode(AllComData.ReferenceMaterialText);
                        lblPenalty.Text = AllComData.PenaltyDescription;


                        var risk = string.Empty;
                        if (AllComData.ComplianceInstanceRisk != null)
                        {
                            risk = Business.ComplianceManagement.GetRiskType(-1, (int)AllComData.ComplianceInstanceRisk);
                        }
                        else
                        {
                            risk = Business.ComplianceManagement.GetRiskType((int)AllComData.ComplianceRisk, -1);
                        }
                        lblRisk.Text = risk;
                        hdnActId.Value = Convert.ToString(AllComData.ActID);
                        hdnComplianceId.Value = Convert.ToString(AllComData.ComplianceID);
                        hdnrisk.Value = risk;
                      
                        if (risk == "HIGH")
                        {
                            lblRisk.Attributes["style"] = "color:red;font-weight: bold;";
                            RiskColor.Attributes["style"] = "border-left-color:red;";
                        }
                        else if (risk == "MEDIUM")
                        {
                            lblRisk.Attributes["style"] = "color:#ffbf00;font-weight: bold;";
                            RiskColor.Attributes["style"] = "border-left-color:#ffbf00;";
                        }
                        else if (risk == "LOW")
                        {
                            lblRisk.Attributes["style"] = "color:green;font-weight: bold;";
                            RiskColor.Attributes["style"] = "border-left-color:green;";
                        }
                        else if (risk == "CRITICAL")
                        {
                            lblRisk.Attributes["style"] = "color:#CC0900;font-weight: bold;";
                            RiskColor.Attributes["style"] = "border-left-color:#CC0900;";
                        }

                        linkActName.Text = AllComData.ActName;
                        lblComplianceID.Text = Convert.ToString(AllComData.ComplianceID);
                        lblRule.Text = AllComData.Sections;
                        lblFormNumber.Text = AllComData.RequiredForms;
                        actid.Value = Convert.ToString(AllComData.ActID);

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {

                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }
        protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }

       

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {

                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                    Session["ScheduleOnID"] = commandArg[0];

                    if (CMPDocuments != null)
                    {
                        List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            if (entitiesData.Count > 0)
                            {

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                foreach (var file in CMPDocuments)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }
                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {                                       
                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                        }
                                        else
                                        {
                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            CompDocReviewPath = filePath1;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                          
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                            lblMessage.Text = "";
                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                        string DateFolder = Folder + "/" + File;
                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }
                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                            string FileName = DateFolder + "/" + User + "" + extension;
                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            CompDocReviewPath = FileName;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                            lblMessage.Text = "";
                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);
                if (file.FilePath != null)
                {
                    string filePath = string.Empty;
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                        filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        //protected void gvParentToComplianceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        Label lblUserName = (Label)e.Row.FindControl("lblUserName");
        //        string UserName = Convert.ToString(lblUserName.Text);

        //        Label lblStatus = (Label)e.Row.FindControl("lblStatus");
        //        string Status = Convert.ToString(lblStatus.Text);

        //        Label lblRemarks = (Label)e.Row.FindControl("lblRemarks");
        //        string Remarks = Convert.ToString(lblRemarks.Text);

        //        Label lblDocumentVersionView = (Label)e.Row.FindControl("lblcustomlabel");
        //        if (Status.ToLower() == "open" && Remarks.ToLower() == "new compliance assigned.")
        //        {
        //            lblDocumentVersionView.Text = "Compliance assinged to " + UserName;
        //        }
        //        if (Status.ToLower() == "not complied" || Status.ToLower() == "rejected")
        //        {
        //            lblDocumentVersionView.Text = "Compliance has been rejected by " + UserName;
        //        }
        //        if (Status.ToLower() == "complied but pending review")
        //        {
        //            if (Remarks != "")
        //                lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " in time with remark " + Remarks;
        //            else
        //                lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " in time ";
        //        }
        //        if (Status.ToLower() == "complied delayed but pending review")
        //        {
        //            if (Remarks != "")
        //                lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " after due date time with remark " + Remarks;
        //            else
        //                lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " after due date ";
        //        }
        //        if (Status.ToLower() == "complied delayed but pending review")
        //        {
        //            if (Remarks != "")
        //                lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " after due date time with remark " + Remarks;
        //            else
        //                lblDocumentVersionView.Text = "Compliance has been submitted by " + UserName + " after due date ";
        //        }

        //        if (Status.ToLower() == "closed-timely")
        //        {
        //            if (Remarks != "")
        //                lblDocumentVersionView.Text = "Compliance has been reviewed and closed by " + UserName + " in time with remark " + Remarks;
        //            else
        //                lblDocumentVersionView.Text = "Compliance has been reviewed and closed by " + UserName + " in time ";
        //        }

        //        if (Status.ToLower() == "closed-delayed")
        //        {
        //            if (Remarks != "")
        //                lblDocumentVersionView.Text = "Compliance has been reviewed and closed by " + UserName + " after due date with remark" + Remarks;
        //            else
        //                lblDocumentVersionView.Text = "Compliance has been  reviewed and closed by " + UserName + " after due date ";
        //        }

        //        if (Status.ToLower() == "approved")
        //        {
        //            lblDocumentVersionView.Text = "Compliance has been approved and closed by " + UserName;
        //        }

        //        if (Status.ToLower() == "revise compliance")
        //        {
        //            lblDocumentVersionView.Text = "Compliance has been revised by " + UserName;
        //        }
        //        if (Status.ToLower() == "submitted for interim review")
        //        {
        //            lblDocumentVersionView.Text = "Interim compliance has been reviewed by " + UserName;
        //        }

        //        if (Status.ToLower() == "interim review approved")
        //        {
        //            lblDocumentVersionView.Text = "Interim compliance has been submitted for review approved by " + UserName;
        //        }
        //        if (Status.ToLower() == "interim rejected")
        //        {
        //            lblDocumentVersionView.Text = "Interim compliance has been rejected by " + UserName;
        //        }

        //        if (Status.ToLower() == "reminder")
        //        {
        //            if (Remarks.ToLower() == "upcoming")
        //                lblDocumentVersionView.Text = "Upcoming compliance reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "overdue")
        //                lblDocumentVersionView.Text = "Overdue compliance reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "escalation notification")
        //                lblDocumentVersionView.Text = "Escalation notification compliance reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "penalty performer")
        //                lblDocumentVersionView.Text = "Penalty performer reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "penalty reviewer")
        //                lblDocumentVersionView.Text = "Penalty Reviewer reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "pending for review")
        //                lblDocumentVersionView.Text = "Pending for review compliance reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "checklist reminder")
        //                lblDocumentVersionView.Text = "Checklist compliance reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "checklist escalation notification")
        //                lblDocumentVersionView.Text = "Checklist escalation notification reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "upcoming perofrmer monthly")
        //                lblDocumentVersionView.Text = "Upcoming perofrmer monthly reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "upcoming reviewer monthly")
        //                lblDocumentVersionView.Text = "Upcoming reviewer monthly reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "upcoming management")
        //                lblDocumentVersionView.Text = "Upcoming management reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "weekly management")
        //                lblDocumentVersionView.Text = "Weekly management reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "upcoming approver")
        //                lblDocumentVersionView.Text = "Upcoming approver reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "weekly  approver")
        //                lblDocumentVersionView.Text = "Weekly approver reminder has been sent " + UserName;
        //            if (Remarks.ToLower() == "approver notification")
        //                lblDocumentVersionView.Text = "Approver notification reminder has been sent " + UserName;
        //        }
        //        if (Status.ToLower() == "managementremark")
        //        {
        //            lblDocumentVersionView.Text = UserName + " has added remark " + Remarks;
        //        }
        //    }
        //}
        //protected void gvParentGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Row.RowType == DataControlRowType.DataRow)
        //        {
        //            if (!String.IsNullOrEmpty(Request.QueryString["ComplianceScheduleID"]))
        //            {
        //                if (gvParentGrid.DataKeys[e.Row.RowIndex]["SendDate"] != null)
        //                {
        //                    DateTime? Date = Convert.ToDateTime(gvParentGrid.DataKeys[e.Row.RowIndex]["SendDate"]);

        //                    //DateTime? Date = Convert.ToDateTime(e.Row.Cells[1].Text);
        //                    int? ScheduledOnID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);

        //                    //GridView gv = (GridView)e.Row.FindControl("gvChildGrid");
        //                    //string type = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

        //                    ComplianceDBEntities entities = new ComplianceDBEntities();
        //                    GridView gv1 = (GridView)e.Row.FindControl("gvParentToComplianceGrid");
        //                    var compliance = entities.SP_GetAuditLogDetail(ScheduledOnID, Date).ToList();
        //                    gv1.DataSource = compliance;
        //                    gv1.DataBind();
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        protected void lnkSampleForm_Click(object sender, EventArgs e)
        {
            try
            {
                if (lnkSampleForm.Text != "")
                {
                    string url = lnkSampleForm.Text;

                    string fullURL = "window.open('" + url + "', '_blank', 'height=500,width=800,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
                    lnkSampleForm.Attributes.Add("OnClick", fullURL);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTypes", "UpdatedrebindTypes('Detail');", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveRemarks_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtManagementRemarks.Text != "")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        ManagementComment cm = new ManagementComment();
                        cm.remarks = txtManagementRemarks.Text;
                        cm.createdBy = AuthenticationHelper.UserID;
                        cm.createdDate = DateTime.UtcNow;
                        cm.ComplainceInstatnceID = Convert.ToInt32(Request.QueryString["ComplainceInstatnceID"]);
                        cm.ComplianceScheduleID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);
                        entities.ManagementComments.Add(cm);
                        entities.SaveChanges();
                        int instanceId = Convert.ToInt32(Request.QueryString["ComplainceInstatnceID"]);
                        var compliance= (from row in  entities.ComplianceAssignments
                                         where row.ComplianceInstanceID== instanceId
                                         select row).ToList();

                        //hdnActId.Value = Convert.ToString(complianceInfo.ActID);
                        //hdnComplianceId.Value = Convert.ToString(complianceInfo.ID);
                        string buildstringfornotification = "Management sent you remarks on compliance assigned to you for period " + lblPeriod.Text;
                        buildstringfornotification += "\n Remarks is:" + txtManagementRemarks.Text;
                        Notification ntc = new Notification();
                        ntc.Remark = buildstringfornotification;
                        ntc.UpdatedBy = AuthenticationHelper.UserID;
                        ntc.UpdatedOn = DateTime.UtcNow;
                        ntc.CreatedBy = AuthenticationHelper.UserID;
                        ntc.CreatedOn = DateTime.UtcNow;
                        ntc.Type = "Compliance";
                        ntc.ActID = Convert.ToInt32(hdnActId.Value);
                        ntc.ComplianceID = Convert.ToInt32(hdnComplianceId.Value);

                        entities.Notifications.Add(ntc);
                        entities.SaveChanges();
                      

                        var perf = (from row in compliance where row.RoleID == 3
                                    select row.UserID).FirstOrDefault();

                        var rev = (from row in compliance
                                    where row.RoleID == 4
                                    select row.UserID).FirstOrDefault();

                        try
                        {
                            if (perf != 0)
                            {
                                var User = UserManagement.GetByID(Convert.ToInt32(perf));
                                var MgmtUser = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
                                string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(AuthenticationHelper.CustomerID)).Name;
                                var ShortDescription = Business.ComplianceManagement.GetCompliance(Convert.ToInt32(hdnComplianceId.Value)).ShortDescription;

                                string portalurl = string.Empty;
                                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                if (Urloutput != null)
                                {
                                    portalurl = Urloutput.URL;
                                }
                                else
                                {
                                    portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                }

                                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ManagmentRemarkNotification
                                                    .Replace("@User", User.FirstName + " " + User.LastName)
                                                    .Replace("@MgmtUser", MgmtUser.FirstName + " " + MgmtUser.LastName)
                                                    .Replace("@ShortDescription", ShortDescription)
                                                    .Replace("@Period", lblPeriod.Text)
                                                    .Replace("@Remarks", txtManagementRemarks.Text)
                                                    .Replace("@URL", Convert.ToString(portalurl))
                                                    .Replace("@From", ReplyEmailAddressName);

                                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ManagmentRemarkNotification
                                //                    .Replace("@User", User.FirstName + " " + User.LastName)
                                //                    .Replace("@MgmtUser", MgmtUser.FirstName + " " + MgmtUser.LastName)
                                //                    .Replace("@ShortDescription", ShortDescription)
                                //                    .Replace("@Period", lblPeriod.Text)
                                //                    .Replace("@Remarks", txtManagementRemarks.Text)
                                //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                //                    .Replace("@From", ReplyEmailAddressName);

                                new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { User.Email }).ToList(), null, null, "AVACOM Notification for Management Remarks/Comment.", message); }).Start();
                            }

                            if (rev != 0)
                            {
                                string portalurl = string.Empty;
                                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                if (Urloutput != null)
                                {
                                    portalurl = Urloutput.URL;
                                }
                                else
                                {
                                    portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                }
                                var User = UserManagement.GetByID(Convert.ToInt32(rev));
                                var MgmtUser = UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID));
                                string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(AuthenticationHelper.CustomerID)).Name;
                                var ShortDescription = Business.ComplianceManagement.GetCompliance(Convert.ToInt32(hdnComplianceId.Value)).ShortDescription;
                                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ManagmentRemarkNotification
                                                .Replace("@User", User.FirstName + " " + User.LastName)
                                                .Replace("@MgmtUser", MgmtUser.FirstName + " " + MgmtUser.LastName)
                                                .Replace("@ShortDescription", ShortDescription)
                                                .Replace("@Period", lblPeriod.Text)
                                                .Replace("@Remarks", txtManagementRemarks.Text)
                                                .Replace("@URL", Convert.ToString(portalurl))
                                                .Replace("@From", ReplyEmailAddressName);
                                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ManagmentRemarkNotification
                                //                    .Replace("@User", User.FirstName + " " + User.LastName)
                                //                    .Replace("@MgmtUser", MgmtUser.FirstName + " " + MgmtUser.LastName)
                                //                    .Replace("@ShortDescription", ShortDescription)
                                //                    .Replace("@Period", lblPeriod.Text)
                                //                    .Replace("@Remarks", txtManagementRemarks.Text)
                                //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                //                    .Replace("@From", ReplyEmailAddressName);

                                new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { User.Email }).ToList(), null, null, "AVACOM Notification for Management Remarks/Comment.", message); }).Start();
                            }
                        }
                        catch (Exception)
                        {
                        }
                        
                        UserNotification unt = new UserNotification();
                        unt.NotificationID = ntc.ID;
                        unt.UserID = Convert.ToInt32(perf);
                        unt.IsRead = false;
                        entities.UserNotifications.Add(unt);
                        entities.SaveChanges();

                        unt = new UserNotification();
                        unt.NotificationID = ntc.ID;
                        unt.UserID = Convert.ToInt32(rev);
                        unt.IsRead = false;
                        entities.UserNotifications.Add(unt);
                        entities.SaveChanges();

                        CustomValidator1.IsValid = false;
                        CustomValidator1.ErrorMessage = "Management Remarks Saved Successfully.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void btnApplyFilter_AuditLog_Click(object sender, EventArgs e)
        {
            try
            {
                BindAuditLogs(Convert.ToInt32(ViewState["complianceInstanceScheduleOnID"]), Convert.ToInt32(ViewState["complianceInstanceAuditID"]));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTypes", "UpdatedrebindTypes('Audit');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearFilter_AuditLog_Click(object sender, EventArgs e)
        {
            try
            {
                ddlUsers_AuditLog.ClearSelection();

                txtFromDate_AuditLog.Text = string.Empty;
                txtToDate_AuditLog.Text = string.Empty;

                BindAuditLogs(Convert.ToInt32(ViewState["complianceInstanceScheduleOnID"]), Convert.ToInt32(ViewState["complianceInstanceAuditID"]));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTypes", "UpdatedrebindTypes('Audit');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

   protected void linkActName_Click(object sender, EventArgs e)
        {
            int ScheduledOnID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);
            ViewState["ScheduledOnID"] = ScheduledOnID;
            var complianceInfo = Business.ComplianceManagement.GetComplianceByInstanceID(ScheduledOnID);
            var ActID = Business.ActManagement.GetByID(complianceInfo.ActID);
           // linkActName.Text = ActInfo.Name;
           // int actid = Convert.ToInt32(Request.QueryString["Name"]);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "actname", "openInNewTab2('" + ActID + "');", true);
        }
    }

    internal class select
    {
    }
}