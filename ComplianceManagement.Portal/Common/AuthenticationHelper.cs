﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public class AuthenticationHelper
    {
        public static int UserID
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                    return Convert.ToInt32(HttpContext.Current.User.Identity.Name.Split(new char[] { ';' }, 13)[0]);
                else
                    return -1;
            }
        }

        public static string Role
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                    return HttpContext.Current.User.Identity.Name.Split(new char[] { ';' }, 13)[1];
                else
                    return string.Empty;
            }
        }

        public static string User
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                    return HttpContext.Current.User.Identity.Name.Split(new char[] { ';' }, 13)[2];
                else
                    return "Guest";
            }
        }

        public static string IComplilanceApplicable
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                    return Convert.ToString(HttpContext.Current.User.Identity.Name.Split(new char[] { ';' }, 13)[3]);
                else
                    return string.Empty;
            }
        }

        public static string ProductApplicableLogin
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                    return Convert.ToString(HttpContext.Current.User.Identity.Name.Split(new char[] { ';' }, 13)[4]);
                else
                    return string.Empty;
            }
        }

        public static long CustomerID
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                    return Convert.ToInt32(HttpContext.Current.User.Identity.Name.Split(new char[] { ';' }, 13)[5]);
                else
                    return -1;
            }
        }

        public static string TaskApplicable
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                    return Convert.ToString(HttpContext.Current.User.Identity.Name.Split(new char[] { ';' }, 13)[6]);
                else
                    return string.Empty;
            }
        }

        public static string ProfileID
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                    return Convert.ToString(HttpContext.Current.User.Identity.Name.Split(new char[] { ';' }, 13)[7]);
                else
                    return string.Empty;
            }
        }

        public static string AuthKey
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                    return Convert.ToString(HttpContext.Current.User.Identity.Name.Split(new char[] { ';' }, 13)[8]);
                else
                    return string.Empty;
            }
        }

        public static int IsVerticalApplicable
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                    return Convert.ToInt32(HttpContext.Current.User.Identity.Name.Split(new char[] { ';' }, 13)[9]);
                else
                    return  -1;
            }
        }

        public static bool IsPaymentCustomer
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                    if (HttpContext.Current.User.Identity.Name.Split(new char[] { ';' }, 12)[10] != null)
                        return Convert.ToBoolean(HttpContext.Current.User.Identity.Name.Split(new char[] { ';' }, 13)[10]);
                    else
                        return false;
                else
                    return false;
            }
        }

        public static int IsLabelApplicable
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                    return Convert.ToInt32(HttpContext.Current.User.Identity.Name.Split(new char[] { ';' }, 13)[11]);
                else
                    return -1;               
            }
        }

        public static int ComplianceProductType
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                    return Convert.ToInt32(HttpContext.Current.User.Identity.Name.Split(new char[] { ';' }, 13)[12]);
                else
                    return -1;
            }
        }
    }
}