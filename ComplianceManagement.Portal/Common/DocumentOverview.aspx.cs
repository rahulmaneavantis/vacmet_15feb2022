﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class DocumentOverview : System.Web.UI.Page
    {
        static string sampleFormPath1 = "";
        public static string CompDocReviewPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ComplianceScheduleID"]) && !string.IsNullOrEmpty(Request.QueryString["ComplainceTransactionID"]))
                {
                    int ScheduledOnID = 0;
                    string ComplianceSchedules = Request.QueryString["ComplianceScheduleID"];
                    string[] ScheduleArray = ComplianceSchedules.ToString().Split(',');
                    List<int> LstScheduleIds = new List<int>();
                    foreach (var item in ScheduleArray)
                        ScheduledOnID = Convert.ToInt32(item);
                    int ComplainceTransactionID = Convert.ToInt32(Request.QueryString["ComplainceTransactionID"]);
                    int IsStatutory = Convert.ToInt32(Request.QueryString["ISStatutoryflag"]);
                    BindTransactionDetails(ScheduledOnID, ComplainceTransactionID, IsStatutory);


                }
            }
        }
        protected void rptComplianceVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }
        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                    Session["ScheduleOnID"] = commandArg[0];

                    if (CMPDocuments != null)
                    {
                        List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            if (entitiesData.Count > 0)
                            {

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                foreach (var file in CMPDocuments)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }

                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string extension = System.IO.Path.GetExtension(file.FileName);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                        }
                                        else
                                        {
                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            CompDocReviewPath = filePath1;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);                                        
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                            lblMessage.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;
                                        string extension = System.IO.Path.GetExtension(filePath);

                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessage.Text = "";
                                            lblMessage.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }
                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            CompDocReviewPath = FileName;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                            lblMessage.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptIComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                List<GetInternalComplianceDocumentsView> ComplianceFileData = new List<GetInternalComplianceDocumentsView>();
                List<GetInternalComplianceDocumentsView> ComplianceDocument = new List<GetInternalComplianceDocumentsView>();
                ComplianceDocument = DocumentManagement.GetFileDataInternalView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();

                if (commandArgs[1].Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                }

                if (e.CommandName.Equals("View"))
                { int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
                    if (AWSData != null)
                    {
                        #region AWS Storage
                        if (ComplianceFileData.Count > 0)
                        {
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                            string directoryPath = "~/TempDocuments/AWS/" + User;
                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(Server.MapPath(directoryPath));
                            }

                            int i = 0;
                            foreach (var file in ComplianceFileData)
                            {
                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                    request.Key = file.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                }

                                string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessageInternal.Text = "";
                                        lblMessageInternal.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ViewInternalDocument();", true);
                                    }
                                    else
                                    {
                                        string filePath1 = directoryPath + "/" + file.FileName;
                                        CompDocReviewPath = filePath1;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        i++;
                                        lblMessageInternal.Text = "";                                      
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewInternal('" + CompDocReviewPath + "');", true);
                                    }
                                }
                                else
                                {
                                    lblMessageInternal.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ViewInternalDocument();", true);
                                }
                                break;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal Storage
                        if (ComplianceFileData.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in ComplianceFileData)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessageInternal.Text = "";
                                        lblMessageInternal.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ViewInternalDocument();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();
                                        i++;
                                        lblMessageInternal.Text = "";
                                        CompDocReviewPath = FileName;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewInternal('" + CompDocReviewPath + "');", true);
                                    }
                                }
                                else
                                {
                                    lblMessageInternal.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ViewInternalDocument();", true);
                                }
                                break;
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
               
            }
        }
        protected void rptIComplianceVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                LinkButton lblIDocumentVersionView = (LinkButton)e.Item.FindControl("lblIDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblIDocumentVersionView);

            }
        }
        public void BindTransactionDetails(long ScheduledOnID, long TransactionID,int IsStatutory)
        {           
            if (IsStatutory == -1 || IsStatutory == 1 || IsStatutory == 2 || IsStatutory == 4)
            {
                List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(ScheduledOnID), Convert.ToInt64(TransactionID));

                if (CMPDocuments != null)
                {
                    List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                    if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                    {
                        GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                        entityData.Version = "1.0";
                        entityData.ScheduledOnID = Convert.ToInt64(ScheduledOnID);
                        entitiesData.Add(entityData);
                    }
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
                    if (AWSData != null)
                    {
                        #region AWS Storage
                        if (entitiesData.Count > 0)
                        {
                            rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptComplianceVersionView.DataBind();

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                            string directoryPath = "~/TempDocuments/AWS/" + User;
                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(Server.MapPath(directoryPath));
                            }

                            foreach (var file in CMPDocuments)
                            {
                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                    request.Key = file.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                }

                                string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string extension = System.IO.Path.GetExtension(file.FileName);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessage.Text = "";
                                        lblMessage.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                    }
                                    else
                                    {
                                        string filePath1 = directoryPath + "/" + file.FileName;
                                        CompDocReviewPath = filePath1;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                        lblMessage.Text = "";
                                    }
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);

                                }
                                break;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal Storage
                        if (entitiesData.Count > 0)
                        {
                            rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptComplianceVersionView.DataBind();

                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessage.Text = "";
                                        lblMessage.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }
                                        
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();

                                        CompDocReviewPath = FileName;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                        lblMessage.Text = "";
                                    }
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);

                                }
                                break;
                            }
                        }
                        #endregion
                    }
                }
            }
            else
            {
               
                List<GetInternalComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetInternalComplianceDocuments(Convert.ToInt64(ScheduledOnID), Convert.ToInt64(TransactionID));
               
                if (CMPDocuments != null)
                {
                    //List<GetInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                    List<GetInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();

                    if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                    {
                        GetInternalComplianceDocumentsView entityData = new GetInternalComplianceDocumentsView();
                        entityData.Version = "1.0";
                        entityData.InternalComplianceScheduledOnID = Convert.ToInt64(ScheduledOnID);
                        entitiesData.Add(entityData);
                    }
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
                    if (AWSData != null)
                    {
                        #region AWS Storage
                        if (entitiesData.Count > 0)
                        {
                            rptIComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptIComplianceVersionView.DataBind();

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                            string directoryPath = "~/TempDocuments/AWS/" + User;

                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(Server.MapPath(directoryPath));
                            }

                            foreach (var item in entitiesData)
                            {
                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                    request.Key = item.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                }
                            }

                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessageInternal.Text = "";
                                        lblMessageInternal.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ViewInternalDocument();", true);
                                    }
                                    else
                                    {
                                        string filePath1 = directoryPath + "/" + file.FileName;
                                        CompDocReviewPath = filePath1;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);                                        
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewInternal('" + CompDocReviewPath + "');", true);

                                        lblMessageInternal.Text = "";
                                    }
                                }
                                else
                                {
                                    lblMessageInternal.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ViewInternalDocument();", true);

                                }
                                break;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal Storage
                        if (entitiesData.Count > 0)
                        {
                            rptIComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptIComplianceVersionView.DataBind();
                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessageInternal.Text = "";
                                        lblMessageInternal.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ViewInternalDocument();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();

                                        CompDocReviewPath = FileName;

                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewInternal('" + CompDocReviewPath + "');", true);

                                        lblMessageInternal.Text = "";
                                    }
                                }
                                else
                                {
                                    lblMessageInternal.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ViewInternalDocument();", true);

                                }
                                break;
                            }
                        }
                        #endregion
                    }
                }
            }
        }
    }
}