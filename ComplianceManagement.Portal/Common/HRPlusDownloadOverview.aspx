﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HRPlusDownloadOverview.aspx.cs" EnableEventValidation="false"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.HRPlusDownloadOverview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">
    <title></title>
    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>




    <style type="text/css">

        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: #1fd9e1;
            background-color: #f7f7f7;
        }

        .panel-heading .nav > li > a {
            font-size: 20px !important;
        }

        .panel-heading .nav > li > a {
            border-bottom: 0px;
        }

        .customDropDownCheckBoxCSS {
            height: 32px !important;
            width: 70%;
        }

        .chosen-single {
            color: #8e8e93;
        }

        .container {
            max-width: 100%;
        }

        ul.multiselect-container.dropdown-menu {
            width: 100%;
            height: 100px;
            overflow-y: auto;
        }

        button.multiselect.dropdown-toggle.btn.btn-default {
            text-align: left;
        }

        span.multiselect-selected-text {
            float: left;
            color: #444;
            font-family: 'Roboto', sans-serif !important;
        }

        b.caret {
            float: right;
            margin-top: 8px;
        }

        label {
            font-weight: 500;
            color: #666;
        }

        .fixed {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
        }
    </style>

    <style type="text/css">
        tr.spaceUnder > td {
            padding-top: 1em;
        }

        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .circle {
            width: 24px;
            height: 24px;
            border-radius: 59%;
            display: inline-block;
            margin-left: 8px;
        }
    </style>


    <script type="text/javascript">
        function ShowDownloadDocument() {
            $('#divDownloadDocument').modal('show');
            return true;
        };

    </script>

    <script type="text/javascript">
        function openInNewTab(url) {           
            var win = window.open(url, '_blank');
            win.focus();
            return false;
        }
        function ShowIntenalDownloadDocument() {
            $('#divInternalDownloadDocument').modal('show');
            return true;
        };

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <div class="modal fade" id="divDownloadDocument" style="bottom: auto; overflow: hidden;" tabindex="-1" role="dialog;" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 432px; margin-top: -11px; margin-left: -15px;">
                    <div class="modal-content">
                       <%-- <div class="modal-header">
                        </div>--%>
                        <div class="modal-body" >

                            <table width="100%" style="text-align: left; margin-left: 0%;">
                                <thead>
                                    <tr>
                                        <td valign="top">
                                            <div style="height:190px;overflow-y:scroll;">
                                            <asp:Repeater ID="rptComplianceVersion" runat="server" OnItemCommand="rptComplianceVersion_ItemCommand"
                                                OnItemDataBound="rptComplianceVersion_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblComplianceDocumnets">
                                                        <thead>
                                                            <th>Versions</th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' ID="lblDocumentVersion"
                                                                runat="server" Text='<%# Eval("Version")%>'></asp:LinkButton>

                                                        </td>
                                                        <td>
                                                             <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 210px;">
                                                              <asp:Label ID="Label1" Text='<%# Eval("FileName") %>' runat="server" data-toggle="tooltip" CssClass="control-label" ToolTip='<%# Eval("FileName") %>' ></asp:Label>
                                                                 </div>
                                                                  </td>
                                                        <td>
                                                            <asp:LinkButton class="btn btn-search" CommandName="Download" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version")+","+ Eval("FileID") %>' OnClientClick='javascript:enableControls()'
                                                                ID="btnComplinceVersionDoc" runat="server" Text="Download" Style="margin-top: 10%;">
                                                            </asp:LinkButton>
                                                              <asp:Label ID="lblCompDocpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>                                                                                                 
                                                                <asp:LinkButton CommandName="RedirectURL" ID="lblCompDocpathDownload"   CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                OnClientClick=<%# "javascript:openInNewTab('" + Eval("FilePath") + "')" %>
                                                                Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                            runat="server" Font-Underline="false" />  
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            </div>
                                        </td>
                                        <td valign="top">
                                            <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                                OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblComplianceDocumnets">
                                                        <thead>
                                                            <th>Compliance Related Documents</th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton
                                                                CommandArgument='<%# Eval("FileID")%>'
                                                                OnClientClick='javascript:enableControls()'
                                                                ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                            </asp:LinkButton></td>                                                       
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            <asp:Repeater ID="rptWorkingFiles" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                                OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblWorkingFiles">
                                                        <thead>
                                                            <th>Compliance Working Files</th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton CommandArgument='<%# Eval("FileID")%>' class="btn btn-search"
                                                                ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                            </asp:LinkButton></td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div>
            <div class="modal fade" id="divInternalDownloadDocument" style="bottom: auto; overflow: hidden;" tabindex="-1" role="dialog;" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 487px; margin-top: -57px; margin-left: -15px;">
                    <div class="modal-content">
                        <div class="modal-header">
                        </div>

                        <div class="modal-body" style="height: 400px;overflow-y: auto;">
                            <table width="100%" style="text-align: left; margin-left: 0%;">
                                <thead>
                                    <tr>
                                        <td valign="top">
                                            <asp:Repeater ID="rptIComplianceVersion" runat="server" OnItemCommand="rptIComplianceVersion_ItemCommand"
                                                OnItemDataBound="rptIComplianceVersion_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblIComplianceDocumnets">
                                                        <thead>
                                                            <th>Versions</th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("InternalComplianceScheduledOnID") + ","+ Eval("Version") %>' ID="lblDocumentVersion"
                                                                runat="server" Text='<%# Eval("Version")%>'></asp:LinkButton></td>
                                                        <td>
                                                            <asp:LinkButton class="btn btn-search" CommandName="Download" CommandArgument='<%# Eval("InternalComplianceScheduledOnID") + ","+ Eval("Version") %>' OnClientClick='javascript:enableControls()'
                                                                ID="btnComplinceVersionDoc" runat="server" Text="Download" Style="margin-top: 10%;">
                                                            </asp:LinkButton>
                                                              <asp:Label ID="lblInternalCompDocpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>                                                                                                 
                                                                <asp:LinkButton ID="lblInternalCompDocpathDownload"  CommandArgument='<%# Eval("InternalComplianceScheduledOnID") + ","+ Eval("Version") %>'
                                                                OnClientClick=<%# "javascript:openInNewTab('" + Eval("FilePath") + "')" %>
                                                                Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                            runat="server" Font-Underline="false" /> 

                                                        </td>

                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                        <td valign="top">
                                            <asp:Repeater ID="rptIComplianceDocumnets" runat="server" OnItemCommand="rptIComplianceDocumnets_ItemCommand"
                                                OnItemDataBound="rptIComplianceDocumnets_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblIComplianceDocumnets">
                                                        <thead>
                                                            <th>Compliance Related Documents</th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton
                                                                CommandArgument='<%# Eval("FileID")%>'
                                                                OnClientClick='javascript:enableControls()'
                                                                ID="btnIComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                            </asp:LinkButton></td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            <asp:Repeater ID="rptInternalWorkingFiles" runat="server" OnItemCommand="rptInternalWorkingFiles_ItemCommand"
                                                OnItemDataBound="rptInternalWorkingFiles_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblInternalWorkingFiles">
                                                        <thead>
                                                            <th>Compliance Working Files</th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton
                                                                CommandArgument='<%# Eval("FileID")%>'
                                                                ID="btnInternalWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                            </asp:LinkButton></td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
