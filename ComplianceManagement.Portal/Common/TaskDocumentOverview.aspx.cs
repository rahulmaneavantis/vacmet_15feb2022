﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class TaskDocumentOverview : System.Web.UI.Page
    {
        public static string subTaskDocViewPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["TaskScheduleID"]) && !string.IsNullOrEmpty(Request.QueryString["TaskTransactionID"]))
            {
                int taskScheduleOnID = Convert.ToInt32(Request.QueryString["TaskScheduleID"]);
                int TaskTransactionID = Convert.ToInt32(Request.QueryString["TaskTransactionID"]);
                int IsStatutory = Convert.ToInt32(Request.QueryString["ISStatutoryflag"]);

                if (!IsPostBack)
                {
                    BindTransactionDetails(taskScheduleOnID, TaskTransactionID, IsStatutory);
                }
            }
        }
        public void BindTransactionDetails(int taskScheduleOnID, int TaskTransactionID, int IsStatutory)
        {
            if (IsStatutory == 1 || IsStatutory == 2)//ForStatutory or Internal
            {
                List<GetTaskDocumentView> CMPDocuments = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
                if (CMPDocuments != null)
                {
                    List<GetTaskDocumentView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();

                    if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                    {
                        GetTaskDocumentView entityData = new GetTaskDocumentView();
                        entityData.Version = "1.0";
                        entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                        entitiesData.Add(entityData);
                    }
                    int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                    if (AWSData != null)
                    {
                        #region AWS Storage
                        if (entitiesData.Count > 0)
                        {
                            foreach (var file in CMPDocuments)
                            {
                                rptViewTaskVersion.DataSource = entitiesData.OrderBy(entry => entry.Version); //+ "_" + lastFragment.Substring(1, 5);
                                rptViewTaskVersion.DataBind();

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                    request.Key = file.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                }

                                string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string extension = System.IO.Path.GetExtension(filePath);

                                    string filePath1 = directoryPath + "/" + file.FileName;
                                    subTaskDocViewPath = filePath1;
                                    subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                    lblMessage.Text = "";
                                    upTaskDetails1.Update();
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);

                                }
                                break;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal Storage
                        if (entitiesData.Count > 0)
                        {
                            foreach (var file in CMPDocuments)
                            {
                                rptViewTaskVersion.DataSource = entitiesData.OrderBy(entry => entry.Version); //+ "_" + lastFragment.Substring(1, 5);
                                rptViewTaskVersion.DataBind();

                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);

                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }

                                    bw.Close();

                                    subTaskDocViewPath = FileName;
                                    subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                    lblMessage.Text = "";
                                    upTaskDetails1.Update();
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);

                                }
                                break;
                            }
                        }
                        #endregion
                    }
                }
            }
          
        }
        #region  View
        protected void rptViewTaskVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);
                int taskFileidID = Convert.ToInt32(commandArgs[2]);
                if (taskScheduleOnID != 0)
                {
                    if (e.CommandName.Equals("View"))
                    {
                        #region
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();
                        List<GetTaskDocumentView> taskFileData = new List<GetTaskDocumentView>();
                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
                        taskFileData = taskDocumenttoView;
                        taskDocumenttoView = taskDocumenttoView.Where(entry => entry.FileID == taskFileidID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    rptViewTaskVersion.DataSource = taskFileData.OrderBy(entry => entry.Version);
                                    rptViewTaskVersion.DataBind();
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                           
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            subTaskDocViewPath = filePath1;
                                            subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                            lblMessage.Text = "";
                                            //UpdatePanel4.Update();
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    rptViewTaskVersion.DataSource = taskFileData.OrderBy(entry => entry.Version);
                                    rptViewTaskVersion.DataBind();

                                    foreach (var file in taskDocumenttoView)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);

                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();

                                            subTaskDocViewPath = FileName;
                                            subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                            lblMessage.Text = "";
                                            //UpdatePanel4.Update();
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptViewTaskVersion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                var scriptManager = ScriptManager.GetCurrent(this.Page);
                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblViewTaskDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }
        #endregion
    }
}