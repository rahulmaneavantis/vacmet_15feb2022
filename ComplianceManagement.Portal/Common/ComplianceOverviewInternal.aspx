﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComplianceOverviewInternal.aspx.cs" EnableEventValidation="false"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.ComplianceOverviewInternal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">
    <title></title>

    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
     <link href="https://avacdn.azureedge.net/newcss/kendo.common1.2.min.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/kendo.rtl.min.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/kendo.silver.min.css" rel="stylesheet" />
     <link href="https://avacdn.azureedge.net/newcss/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <%--<script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jquery.js"></script>--%>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/kendo.all.min.js"></script>
    <script src="https://avacdn.azureedge.net/newjs/jquery.mentionable.js"></script>
    <link href="https://avacdn.azureedge.net/newcss/jquery.mentionable.css" rel="stylesheet" />
    
    <script type="text/javascript">
        function openInNewTab(url) {
            var win = window.open(url, '_blank');
            win.focus();
        }
        function fopendocfileReview(file) {
           
            $('#DocumentReviewPopUp1').modal('show');
            $('#docViewerReviewAll').attr('src', "../docviewer.aspx?docurl=" + file);
        }
        function fopendocfileReviewPopUp() {
            $('#DocumentReviewPopUp1').modal('show');
        }
        $(document).ready(function () {
            $("button[data-dismiss-modal=modal1]").click(function () {
                $('#DocumentReviewPopUp1').modal('hide');
            });
        });

        
        function fredv(type) {

            if (type == "Compliance") {

                $('#licomplianceoverview1').addClass('active');
                $('#lidocuments1').removeClass('active');
                $('#documents').removeClass('active');
                $('#liAudit1').removeClass('active');
                $('#audits').removeClass('active');
                $('#complianceoverview').addClass('active');

            } else if (type == "Document") {
                $('#lidocuments1').addClass('active');
                $('#licomplianceoverview1').removeClass('active');
                $('#complianceoverview').removeClass('active');
                $('#liAudit1').removeClass('active');
                $('#audits').removeClass('active');
                $('#documents').addClass('active');
            }
            else if (type == "Audit") {

                $('#liAudit1').addClass('active');
                $('#licomplianceoverview1').removeClass('active');
                $('#complianceoverview').removeClass('active');
                $('#lidocuments1').removeClass('active');
                $('#documents').removeClass('active');

                $('#audits').addClass('active');
            }
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }

        function fshowmore() {
            if ($('#moreItems').is(':visible')) {
                $('#moreItems').hide();
                $('#btnshowmore').text('Show more...');
            } else {
                $('#moreItems').show();
                $('#btnshowmore').text('Show less');
            }
        }

        function showDiv(pageid) {
            document.getElementById(pageid + "_div").style.display = "block";
            document.getElementById(pageid + "_div").style.marginLeft = "10px";
            document.getElementById(pageid + "_txt").value = "";
        }

        function submitData() {
            var SelectedValue = new Array();
            var inps = document.getElementsByName('mentioned_id[]');
            for (var i = 0; i < inps.length; i++) {
                if (inps[i].disabled != true) {
                    var inp = inps[i];
                    SelectedValue.push(inp.value);
                }
            }

            var commet = document.getElementById("TxtComments").value;
            var CompId = $('#hdnComplianceId').val();
            var BranchId = $('#hdnCustomerBranchId').val();
            var ActId = $('#hdnActId').val();
            var Duedate = $('#hdnduedate').val();
            var Status = $('#hdnstatus').val();
            var risk = $('#hdnrisk').val();
            var performer = $('#hdnperformer').val();
            var reviewer = $('#hdnreviewer').val();

            $.ajax({
                type: "GET",
                url: '<%=KendoPath%>/data/PostCommentdata?Uids=' + SelectedValue + '&Comments=' + commet + '&UserId=<% =UId%>&CustId=<% =CustId%>&type=I&CompId=' + CompId + '&CustomerBranchId=' + BranchId + '&Actid=' + ActId + '&duedate=' + Duedate + '&Status=' + Status + '&risk=' + risk + '&Performer=' + performer + '&Reviewer=' + reviewer + '&Comscheduleid=<% =SOnID %>+ &Cominstid=<% =compInstanceID%>',
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                crossDomain: true,
                success: function (result) {
                    GetCommentDetails();
                    $('#TxtComments').val('');
                    setTimeout(function () {
                        GetCommentDetails() // this will run after every 5 seconds
                    }, 1000);
                },
                error: function (response) {

                }
            });
        }

        function submitDiv(pageid) {
            var valuetext = document.getElementById(pageid + "_txt").value;
            if (valuetext != "") {
                var CompId = $('#hdnComplianceId').val();
                var BranchId = $('#hdnCustomerBranchId').val();

                $.ajax({
                    type: "GET",
                    url: '<% =KendoPath%>/Data/PostSubCommentdata?MsgID=' + pageid + '&Comments=' + valuetext + '&UserId=<% =UId%>&CustId=<% =CustId%>&type=I&CompId=' + CompId + '&CustomerBranchId=' + BranchId,
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    crossDomain: true,
                    success: function (result) {
                        setTimeout(function () {
                            GetCommentDetails() // this will run after every 5 seconds
                        }, 1000);
                    },
                    error: function (response) {

                    }
                });
            }
            else {
                //alert("Please Write something in comment box"); 
            }
        }

        function CloseDiv(pageid) {
            document.getElementById(pageid + "_div").style.display = "none";
        }

        function GetCommentDetails() {

            $.ajax({
                type: "GET",
                url: '<% =KendoPath%>/data/GetCommentdata?CustomerBranchId=' + $(hdnCustomerBranchId).val() + '&searchKey=' + $(hdnComplianceId).val() + '&CustomerID=<% =CustId%>&ComplianceType=I',
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                crossDomain: true,
                success: function (result) {
                    var MainCommnetId = new Array();
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].message_id == 0) {
                            MainCommnetId.push([result[i].compliance_id, result[i].comment, result[i].user_id, result[i].created_at, result[i]._id, result[i].user_name]);
                        }
                    }
                    var customers = new Array();
                    for (var i = 0; i < MainCommnetId.length; i++) {
                        customers.push('<div style="font-family: Roboto;color:black;float: left;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 234px;border-radius: 6px;">' + MainCommnetId[i][1] + '</div></br > <div style="float: left;font-size: 10px;color:black;">' + MainCommnetId[i][5] + ',' + new Date(new Date(MainCommnetId[i][3])).toLocaleString()
                            + '<a style="font-size: 12px;margin-left: 8px;color:blue;cursor: pointer;" onclick=showDiv("' + MainCommnetId[i][4] + '");>Comments</a></div></br><div style="display:none;" id=' + MainCommnetId[i][4] + '_div><input type="text" style="border-radius: 12px;" name=txtid id=' + MainCommnetId[i][4] + '_txt></br>&nbsp;&nbsp;&nbsp;<a style="cursor: pointer;color: blue;" onclick=submitDiv("' + MainCommnetId[i][4] + '");>Submit</a>&nbsp;&nbsp;&nbsp;<a style="cursor: pointer;color: blue;" onclick=CloseDiv("' + MainCommnetId[i][4] + '");>Closed</a></div></br>');

                        for (var k = 0; k < result.length; k++) {
                            if (result[k].message_id == MainCommnetId[i][4]) {
                                customers.push('<div style="font-family: Roboto;color:black;background: white;float: right;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 234px;border-radius: 6px;">' + result[k].comment + '</div></br></br><div style="float: right;font-size: 10px;color:black;">' + result[k].user_name + ',' + new Date(new Date(result[k].created_at)).toLocaleString() + '</div></br>');
                            }
                        }
                    }
                    $("#ShowDatadiv").load();
                    $('#ShowDatadiv').html('');
                    $('#ShowDatadiv').html('<div style="min-height:45px;padding-left: 10px;padding-top: 10px;">' + customers.join('</div><div style="padding-left: 10px;padding-bottom: 18px;">') + '</div>');

                },
                error: function (response) {

                }
            });

        }

        $('#TxtComments').mentionable(
             '<% =KendoPath%>/Data/GetUSerDetailbySearch?customerID=<% =CustId%>', { parameterName: "search" }
        );

        GetCommentDetails();

        imgExpandCollapse();

        $(document).ready(function () {
            $('#TxtComments').mentionable(
                '<% =KendoPath%>/Data/GetUSerDetailbySearch?customerID=<% =CustId%>', { parameterName: "search" }
            );
        });

    </script>

    <style type="text/css">
        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: #1fd9e1;
            background-color: #f7f7f7;
        }

        a:hover {
            text-decoration: underline;
            outline: 1px;
        }

        .panel-heading .nav > li > a {
            font-size: 20px !important;
        }

        .panel-heading .nav > li > a {
            border-bottom: 0px;
        }

         #mentioned-user-list {
            width:300px;
            color:black;
        }

         .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #ceced2;
            background-color: #f6f6f6;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .customDropDownCheckBoxCSS {
            height: 32px !important;
            width: 70%;
        }

        .chosen-single {
            color: #8e8e93;
        }

        .container {
            max-width: 100%;
        }

        ul.multiselect-container.dropdown-menu {
            width: 100%;
            height: 100px;
            overflow-y: auto;
        }

        button.multiselect.dropdown-toggle.btn.btn-default {
            text-align: left;
        }

        span.multiselect-selected-text {
            float: left;
            color: #444;
            font-family: 'Roboto', sans-serif !important;
        }

        b.caret {
            float: right;
            margin-top: 8px;
        }

       /*  #mentioned-user-list {
            width:300px;
        }
      */

        label {
            font-weight: 500;
            color: #666;
        }

        .fixed {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
        }

         .circle {
    width: 24px;
    height: 24px;
    border-radius: 59%;
    display: inline-block;
    margin-left: 8px;
        }
    </style>

    <style type="text/css">
        tr.spaceUnder > td {
            padding-top: 1em;
        }

        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .RiskControl {
            border: 1px solid #e4d8d8;
            border-left: 5px solid;
            border-radius: 6px;
            width: 141px;
            height: 33px;
        }

        .StatusControl {
            border: 1px solid #e4d8d8;
            border-left: 5px solid;
            border-radius: 6px;
            width:auto;
            /*width: 174px;*/
            height: 33px;
            margin-left: -31px;
        }

        .DueDateControl {
            border: 1px solid #e4d8d8;
            /*border-left: 5px solid;*/
            border-radius: 6px;
            height: 33px;
            width: 184px;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:UpdateProgress ID="updateProgress" runat="server">
                    <ProgressTemplate>
                        <div id="updateProgress1" style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                                AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                   <div style="background-color: white;">
                    <div id="divRiskType" runat="server" class="circle"></div>
                    <asp:Label runat="server" Style="margin-top: -2PX; margin-left: 5px; font-size: 17px; position: absolute; color: #333;">Status - </asp:Label>
                    <asp:Label ID="lblStatus" Style="width: 500px; margin-top: -2PX; margin-left: 69px; font-weight: bold; font-size: 17px; position: absolute; color: #333;"
                        runat="server" />
                </div>        <%--Status - --%>
               <div class="col-lg-8 col-md-8 " style="padding-left: 16px; background: white; padding-top: 9px; padding-right: 23px;">
                   <ul class="nav nav-tabs calender-li" role="tablist" style="background: white;">
                        <li id="licomplianceoverview1" style="cursor: pointer;" onclick="fredv('Compliance');" runat="server"><a id="licomplianceoverview" runat="server" aria-controls="complianceoverview" role="tab" data-toggle="tab">Compliance Overview</a></li>
                        <li id="lidocuments1" runat="server" style="cursor: pointer;" onclick="fredv('Document');"><a id="lidocuments" runat="server" aria-controls="documents" role="tab" data-toggle="tab">Documents</a></li>
                        <li id="liAudit1" runat="server" style="cursor: pointer;" onclick="fredv('Audit');"><a id="liAudit" runat="server" aria-controls="audits" role="tab" data-toggle="tab">Audits</a></li>
                    </ul>
                  <div class="tab-content" style="padding-top: 0px; border: 1px solid #e4d8d8; min-height: 457px; max-height: 457px; border-radius: 5px; overflow-x:hidden;">
                      
                        <div role="tabpanel" class="tab-pane" runat="server" id="complianceoverview">
                            <div class="tabbable-panel">
                                <div class="tabbable-line">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%; background: white;">
                                          <div class="col-lg-5 col-md-5colpadding0 fixwidth" style="width: 50%; background: white; float: left;">
                                        <table style="width: 100%; background: white; margin-left: 10px;">
                                          
                                            <tr class="spaceUnder">
                                                <td style="width: 100%;  vertical-align: top;">Title :
                                                    <asp:Label ID="lblComplianceDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                                                        maximunsize="300px" autosize="true" runat="server" />
                                                </td>
                                            </tr>
                                                <tr class="spaceUnder">
                                                <td style="width: 100%;  vertical-align: top;">Short Form :
                                                    <asp:Label ID="lblShortForm" Style="width: 300px; font-size: 13px; color: #333;"
                                                        maximunsize="300px" autosize="true" runat="server" />
                                                </td>
                                            </tr>
                                                  <tr class="spaceUnder">
                                                <td style="width: 100%; ">Frequency :
                                                    <asp:Label ID="lblFrequency" Style="width: 300px; font-size: 13px; color: #333;"
                                                        maximunsize="300px" autosize="true" runat="server" />
                                                </td>
                                            </tr>
                                             <tr class="spaceUnder">
                                                <td style="width: 100%; ">Risk :
                                                    <asp:Label ID="lblRisk" Style="width: 300px; font-size: 13px; font-weight:bold;"
                                                        maximunsize="300px" autosize="true" runat="server" />
                                                </td>
                                            </tr>
                                             <tr class="spaceUnder">
                                                <td style="width: 100%; ">Due Date :
                                                    <asp:Label ID="lblDueDate" Style="width: 300px; font-size: 13px; color: #333;"
                                                        maximunsize="300px" autosize="true" runat="server" />
                                                </td>
                                            </tr>
                                           
                                              </table>
                                        </div>
                                          <div class="col-lg-5 col-md-5colpadding0 fixwidth" style="width: 50%; background: white; float: left;">
                                            <table style="background: white; margin-left: 10px;">
                                                                           
                                          
                                                  <tr class="spaceUnder">
                                                <td style="width: 100%;  vertical-align: top;">Compliance ID :
                                                    <asp:Label ID="lblcomplianceID" Style="width: 300px; font-size: 13px; color: #333;"
                                                        maximunsize="300px" autosize="true" runat="server" />
                                                </td>
                                            </tr>
                                            <tr class="spaceUnder">
                                                <td style="width: 100%; ">Location :
                                                    <asp:Label ID="lblLocation" Style="width: 300px; font-size: 13px; color: #333;"
                                                        maximunsize="300px" autosize="true" runat="server" />
                                                </td>
                                            </tr>
                                                     <tr class="spaceUnder">
                                                <td style="width: 100%; ">Period :
                                                    <asp:Label ID="lblPeriod" Style="width: 300px; font-size: 13px; color: #333;"
                                                        maximunsize="300px" autosize="true" runat="server" />
                                                </td>
                                            </tr>
                                                 <tr class="spaceUnder">
                                                <td style="width: 100%; ">Performer :
                                                    <asp:Label ID="lblPerformer" Style="width: 300px; font-size: 13px; color: #333;"
                                                        maximunsize="300px" autosize="true" runat="server" />
                                                </td>
                                            </tr>
                                                   <tr class="spaceUnder">
                                                <td style="width: 100%; ">Reviewer :
                                                    <asp:Label ID="lblreviewer" Style="width: 300px; font-size: 13px; color: #333;"
                                                        maximunsize="300px" autosize="true" runat="server" />
                                                </td>
                                            </tr>
                                            </table>
                                        </div>
                                          <div id="divMgmtRemrks" runat="server" style="width: 100%; float: left;display:none;">
                                            <table style="width: 100%; background: white; margin-left: 13px;">
                                                <tr>
                                                    <td style="width: 100%; vertical-align: top;">
                                                        <asp:ValidationSummary runat="server" Style="color: red;" CssClass="vdsummary"
                                                            ValidationGroup="ComplianceValidationGroup" />
                                                        <asp:CustomValidator ID="CustomValidator1" Style="color: red;" runat="server" EnableClientScript="False"
                                                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 100%; vertical-align: top;">
                                                        <div style="float: left; width: 100%">
                                                            <div style="float: left; width: 100px;margin-left:10px;">Comments : </div>
                                                            <div style="float: left;">
                                                                <asp:TextBox TextMode="MultiLine" ID="txtManagementRemarks" Width="950px" Height="150px" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="Comment can not be empty."
                                                                    ControlToValidate="txtManagementRemarks" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                                    Display="None" />
                                                            </div>
                                                            <div style="clear: both; height: 5px;"></div>
                                                            <div style="float: left; width: 94%;">
                                                                <asp:Button ID="btnSaveRemarks" Style="float: right;" OnClick="btnSaveRemarks_Click" runat="server" ValidationGroup="ComplianceValidationGroup" Text="Save" CssClass="btn btn-search" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                            </table>
                                        </div>
                                    </div>
                            </div>
                        </div>
                     </div>   <%--ComplainceOverview--%>
                      
                        <div role="tabpanel" runat="server" id="documents">
                            <div class="tabbable-panel">
                                <div class="tabbable-line">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%; background: white;">
                                      <table style="width: 100%; margin-top: 20px; margin-left: 10px;">
                                       <tr>
                                        <td style="width: 15%; font-weight: bold;">Versions</td>
                                        <td style="width: 2%; font-weight: bold;">: </td>
                                        <td style="width: 83%;">
                                            <table width="100%" style="text-align: left">
                                                <thead>
                                                    <tr>
                                                        <td valign="top">
                                                            <asp:Repeater ID="rptComplianceVersion" runat="server" OnItemCommand="rptComplianceVersion_ItemCommand"
                                                                OnItemDataBound="rptComplianceVersion_ItemDataBound">
                                                                <HeaderTemplate>
                                                                    <table id="tblComplianceDocumnets">
                                                                        <thead>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("InternalComplianceScheduledOnID") + ","+ Eval("Version") %>' ID="lblDocumentVersion"
                                                                                        runat="server" Text='<%# Eval("Version")%>' Style="color: blue;"></asp:LinkButton></td>
                                                                                <td>
                                                                                    <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("InternalComplianceScheduledOnID") + ","+ Eval("Version") %>'
                                                                                        ID="btnComplinceVersionDoc" runat="server" Text="Download" Style="color: blue;">
                                                                                    </asp:LinkButton>
                                                                                    <asp:Label ID="lblSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                                    <asp:LinkButton CommandName="View" ID="lnkViewDoc" CommandArgument='<%# Eval("InternalComplianceScheduledOnID") + ","+ Eval("Version") %>' Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                                        runat="server" Font-Underline="false" />
                                                                                    <asp:Label ID="lblpathReviewDoc" runat="server" Style="display: none"></asp:Label>
                                                                                     <asp:Label ID="lblpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>                                                                                                 
                                                                                     <asp:LinkButton CommandName="version" ID="lblpathDownload"  
                                                                                         CommandArgument='<%# Eval("InternalComplianceScheduledOnID") + ","+ Eval("Version") %>'
                                                                                             Text='Click Here' Style="width: 150px; font-size: 13px; color: blue"
                                                                                            runat="server" Font-Underline="false" /> 
                                                                                </td>
                                                                            </tr>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="btnComplinceVersionDoc" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </table>
                                                                </FooterTemplate>
                                                            </asp:Repeater>

                                                        </td>
                                                        <td valign="top">
                                                            <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                                                OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                                                <HeaderTemplate>
                                                                    <table id="tblComplianceDocumnets">
                                                                        <thead>
                                                                            <th>Compliance Related Documents</th>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                      <td>
                                                                      <asp:LinkButton
                                                                          CommandArgument='<%# Eval("ID")%>'
                                                                          OnClientClick='javascript:enableControls1()'
                                                                          ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                      </asp:LinkButton>
                                                                        <asp:Label ID="lblCompDocpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>                                                                                                 
                                                                                    <asp:LinkButton CommandName="RedirectURL" ID="lblCompDocpathDownload"  
                                                                                        OnClientClick=<%# "openInNewTab('" + Eval("FilePath") + "')" %>
                                                                                        Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                                                      runat="server" Font-Underline="false" />   

                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </table>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                            <asp:Repeater ID="rptWorkingFiles" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                                                OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                                                <HeaderTemplate>
                                                                    <table id="tblWorkingFiles">
                                                                        <thead>
                                                                            <th>Compliance Working Files</th>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:LinkButton
                                                                                CommandArgument='<%# Eval("ID")%>'
                                                                                ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                                            </asp:LinkButton>
                                                                               <asp:Label ID="lblWorkCompDocpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>   
                                                                              <asp:LinkButton CommandName="RedirectURL" ID="lblWorkCompDocpathDownload"  
                                                                                              OnClientClick=<%# "openInNewTab('" + Eval("FilePath") + "')" %>
                                                                                              Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                                                            runat="server" Font-Underline="false" /> 
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </table>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </td>
                                       </tr>
                                      </table>
                                    </div>
                                </div>
                            </div>
                        </div>   <%--documents--%>

                        <div role="tabpanel" runat="server" id="audits">
                            <div class="tabbable-panel">
                                <div class="tabbable-line">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%; background: white; margin-left: 15px;">
                                        <asp:GridView ID="gvParentGrid" GridLines="None" runat="server" AutoGenerateColumns="false"
                                            ShowFooter="true" Width="900px" DataKeyNames="SendDate"
                                            OnRowDataBound="gvParentGrid_OnRowDataBound">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-Width="20px">
                                                    <ItemTemplate>
                                                        <a href="JavaScript:divexpandcollapse('div<%# Convert.ToDateTime(Eval("SendDate")).ToString("MMddyyyy") %>');">
                                                            <img id="imgdiv<%# Convert.ToDateTime(Eval("SendDate")).ToString("MMddyyyy") %>" width="9px" border="0"
                                                                src="../Images/add.png" alt="" /></a>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblSendDate" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("SendDate")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td colspan="100%">
                                                                <div id="div<%# Convert.ToDateTime(Eval("SendDate")).ToString("MMddyyyy") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: visible">
                                                                    <asp:GridView ID="gvParentToComplianceGrid" ShowHeader="false" GridLines="None" runat="server" Width="95%" DataKeyNames="SendDate"
                                                                        AutoGenerateColumns="false" OnRowDataBound="gvParentToComplianceGrid_OnRowDataBound">
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblUserName" runat="server" Visible="false" Text='<%# Eval("UserName") %>'></asp:Label>
                                                                                    <asp:Label ID="lblStatus" runat="server" Visible="false" Text='<%# Eval("Status") %>'></asp:Label>
                                                                                    <asp:Label ID="lblRemarks" runat="server" Visible="false" Text='<%# Eval("Remarks") %>'></asp:Label>
                                                                                    <asp:Label ID="lblcustomlabel" runat="server"> </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                           <%-- <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>--%>
                                        </asp:GridView>
                                    </div>
                                </div>

                            </div>
                        </div>              <%--Audits--%>
                    </div>
                 </div>
              
                 <div class="col-lg-4 col-md-4 " style="padding-left: 10px; background: white; padding-top: 9px;">

                    <div class="row" style="padding-top:3px;">
                        <div class="panel panel-default" style="border: 1px solid #e4d8d8" >
                            <div class="panel-heading" style="padding: 4px 19px; background-color: #1fd9e1; color: white">Comments</div>
                            <div class="panel-body" style="padding: 10px; overflow-y: scroll;min-height: 466px; max-height: 184px;">

                                <div id="ShowDatadiv" style=""></div>

                                <div style="padding-left: 10px;">
                                   
<%--                                    <textarea id="TxtComments" style="width: 250px;border-color: #ddd;float: left;border-radius: 12px;margin-top: 9px;" name="txtcmt"></textarea>--%>
                                    <textarea id="TxtComments" style="width: 268px;border-color: #ddd;float: left;border-radius: 12px;position: relative;" name="txtcmt"></textarea>
                                   
                                    <%--<a onclick="submitData();" style="float:right;cursor:pointer;padding-right:70px;color: blue;margin-top: 20px;" id="MainSubmit">Post</a>   --%>
                                    <a onclick="submitData();" style="float:right;cursor:pointer;padding-right:48px;margin-top: 5px;color: blue;font-size: x-large;" id="MainSubmit"><i class="fa fa-send-o" style="color: blue;" data-toggle="tooltip" title="Send"></i></a>                                  
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                 <%--<div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div class="modal-dialog" style="width: 1306px; padding: 0; position: initial;">
                        <div class="modal-content" style="width: 100%;">
                            <div class="modal-header" style="border-bottom: none;">
                                <button type="button" class="close" data-dismiss="modal" onclick="CloseClearOV();" aria-hidden="true" style="margin-top: -11px;">&times;</button>
                            </div>
                            <div class="modal-body">                                                               
                                <iframe id="OverViews" src="about:blank" width="1306px" height="100%" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>--%>
            </ContentTemplate>
        </asp:UpdatePanel>

        <div class="modal fade" id="DocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss-modal="modal1" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div style="float: left; width: 10%">
                                <table style="text-align: left; margin-left: 5%;">
                                    <thead>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Repeater ID="rptComplianceVersionView" runat="server" OnItemCommand="rptComplianceVersionView_ItemCommand"
                                                            OnItemDataBound="rptComplianceVersionView_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="tblComplianceDocumnets">
                                                                    <thead>
                                                                        <th>Versions</th>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("InternalComplianceScheduledOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblDocumentVersionView"
                                                                                    runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div style="float: left; width: 90%">
                                 <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                 <ContentTemplate>
                                    <asp:Label ID="lblMessage" runat="server" Style="color: red;"></asp:Label>
                                </ContentTemplate>
                                </asp:UpdatePanel>
                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="docViewerReviewAll" runat="server" width="100%" height="535px"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                $('#TxtComments').mentionable(
                    '<% =KendoPath%>/Data/GetUSerDetailbySearch?customerID=<% =CustId%>', { parameterName: "search" }
                );

               GetCommentDetails();

                imgExpandCollapse();
            });

            function imgExpandCollapse() {

                $("[src*=collapse]").on('click', function () {

                    $(this).attr("src", "/Images/add.png");
                    $(this).closest("tr").next().remove();
                });

                $("[src*=add]").on('click', function () {

                    //if ($(this).attr('src').indexOf('add.png') > -1) {
                    //    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
                    //    $(this).attr("src", "/Images/collapse.png");
                    //    imgExpandhide();
                    //} else if ($(this).attr('src').indexOf('collapse.png') > -1) {
                    //    $(this).attr("src", "/Images/add.png");
                    //    $(this).closest("tr").next().remove();
                    //    imgExpandhide();
                    //}
                });
            }

            function divexpandcollapse(divname) {
          
                var div = document.getElementById(divname);
                var img = document.getElementById('img' + divname);
                if (div.style.display == "none") {
                    div.style.display = "inline";
                    img.src = "../Images/collapse.png";
                } else {    
                    div.style.display = "none";
                    img.src = "../Images/add.png";
                }
            }

            $('#updateProgress1').show();
            $(document).ready(function () {
                $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
                $('#updateProgress1').hide();
                $('span.k-widget.k-datepicker.k-header.form-control').removeAttr('style');
            });
        </script>
                <asp:HiddenField runat="server" ID="hdnActId" />
        <asp:HiddenField runat="server" ID="hdnComplianceId" />
        <asp:HiddenField runat="server" ID="hdnCustomerBranchId" />
        <asp:HiddenField runat="server" ID="hdnduedate" />
        <asp:HiddenField runat="server" ID="hdnstatus" />
        <asp:HiddenField runat="server" ID="hdnrisk" />
        <asp:HiddenField runat="server" ID="hdnreviewer" />
        <asp:HiddenField runat="server" ID="hdnperformer" />
                  <asp:HiddenField runat="server" ID="HiddenField1" />
        <style>
		span.k-picker-wrap.k-state-default {
		    border: none;
		}
        </style>
    </form>
</body>
</html>