﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class DownloadOverview : System.Web.UI.Page
    {          
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["ComplianceScheduleID"]) && !string.IsNullOrEmpty(Request.QueryString["ComplainceTransactionID"]))
                {
                    int ScheduledOnID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);
                    int TransactionID = Convert.ToInt32(Request.QueryString["ComplainceTransactionID"]);
                    int IsStatutory = Convert.ToInt32(Request.QueryString["ISStatutoryflag"]);
                    BindFiles(ScheduledOnID, TransactionID, IsStatutory);
                }
            }
        }
        public class GetDocumentData
        {
            public Nullable<int> ID { get; set; }
            public int FileID { get; set; }
            public Nullable<long> ScheduledOnID { get; set; }
            public string FileName { get; set; }
            public string FilePath { get; set; }
            public string Version { get; set; }
            public Nullable<System.DateTime> VersionDate { get; set; }
            public Nullable<bool> ISLink { get; set; }
        }
        public class GetInternalDocumentData
        {
            public Nullable<int> ID { get; set; }
            public int FileID { get; set; }
            public Nullable<long> InternalComplianceScheduledOnID { get; set; }
            public string FileName { get; set; }
            public string FilePath { get; set; }
            public string Version { get; set; }
            public Nullable<System.DateTime> VersionDate { get; set; }
            public Nullable<bool> ISLink { get; set; }
        }
        private void BindFiles(int scheduledOnID, int transactionID, int IsStatutory)
        {
            
            
            if (IsStatutory == -1 || IsStatutory == 1 || IsStatutory == 2 || IsStatutory == 4)
            {
                List<GetDocumentData> Documentsoutput = new List<GetDocumentData>();
                List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(scheduledOnID), Convert.ToInt64(transactionID));
                Session["ScheduleOnID"] = scheduledOnID;
                Session["TransactionID"] = transactionID;

                if (CMPDocuments != null)
                {
                    //CMPDocuments= CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                    //List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                    //if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                    //{
                    //    GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                    //    entityData.Version = "1.0";
                    //    entityData.ScheduledOnID = Convert.ToInt64(scheduledOnID);
                    //    entitiesData.Add(entityData);
                    //}
                    Documentsoutput.Clear();
                    if (CMPDocuments.Count > 0)
                    {
                        var entitiesData1 = CMPDocuments.Where(x => x.ISLink == false).ToList();
                        var documentVersionData = entitiesData1.GroupBy(entry => new { entry.Version,entry.FileName }).Select(entry => entry.FirstOrDefault()).ToList();
                        var documentVersionData1 = CMPDocuments.Where(x => x.ISLink == true).ToList();
                        foreach (var item in documentVersionData)
                        {
                            Documentsoutput.Add(new GetDocumentData
                            {
                                ID = item.ID,
                                ScheduledOnID = item.ScheduledOnID,
                                Version = string.IsNullOrEmpty(item.Version) ? "1.0" : item.Version,
                                VersionDate = item.VersionDate,
                                FileID = item.FileID,
                                ISLink = item.ISLink,
                                FilePath = item.FilePath,
                                FileName = item.FileName
                            });
                        }
                        foreach (var item in documentVersionData1)
                        {
                            Documentsoutput.Add(new GetDocumentData
                            {
                                ID = item.ID,
                                ScheduledOnID = item.ScheduledOnID,
                                Version = string.IsNullOrEmpty(item.Version) ? "1.0" : item.Version,
                                VersionDate = item.VersionDate,
                                FileID = item.FileID,
                                ISLink = item.ISLink,
                                FilePath = item.FilePath,
                                FileName = item.FileName
                            });
                        }
                        rptComplianceVersion.DataSource = Documentsoutput;
                        rptComplianceVersion.DataBind();
                        //rptComplianceVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                        //rptComplianceVersion.DataBind();

                        //rptComplianceDocumnets.DataSource = null;
                        //rptComplianceDocumnets.DataBind();

                        //rptWorkingFiles.DataSource = null;
                        //rptWorkingFiles.DataBind();

                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDownloadDocument();", true);
                    }
                    else
                    {

                    }
                }
            }
            else
            {
                List<GetInternalDocumentData> Documentsoutput = new List<GetInternalDocumentData>();
                List<GetInternalComplianceDocumentsView> CMPDocuments = Business.InternalComplianceManagement.GetInternalDocumnetsKendo(Convert.ToInt64(scheduledOnID), Convert.ToInt64(transactionID));
              
                Session["ScheduleOnID"] = scheduledOnID;
                Session["TransactionID"] = transactionID;

                if (CMPDocuments != null)
                {
                    //CMPDocuments = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                    //List<GetInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                    //if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                    //{
                    //    GetInternalComplianceDocumentsView entityData = new GetInternalComplianceDocumentsView();
                    //    entityData.Version = "1.0";
                    //    entityData.InternalComplianceScheduledOnID = Convert.ToInt64(scheduledOnID);
                    //    entitiesData.Add(entityData);
                    //}

                    if (CMPDocuments.Count > 0)
                    {
                        var entitiesData1 = CMPDocuments.Where(x => x.ISLink == false).ToList();
                        var documentVersionData = entitiesData1.GroupBy(entry => new { entry.Version }).Select(entry => entry.FirstOrDefault()).ToList();
                        var documentVersionData1 = CMPDocuments.Where(x => x.ISLink == true).ToList();
                        foreach (var item in documentVersionData)
                        {
                            Documentsoutput.Add(new GetInternalDocumentData
                            {
                                ID = item.ID,
                                InternalComplianceScheduledOnID = item.InternalComplianceScheduledOnID,
                                Version = string.IsNullOrEmpty(item.Version) ? "1.0" : item.Version,
                                VersionDate = item.VersionDate,
                                FileID = item.FileID,
                                ISLink = item.ISLink,
                                FilePath = item.FilePath,
                                FileName = item.FileName
                            });
                        }
                        foreach (var item in documentVersionData1)
                        {
                            Documentsoutput.Add(new GetInternalDocumentData
                            {
                                ID = item.ID,
                                InternalComplianceScheduledOnID = item.InternalComplianceScheduledOnID,
                                Version = string.IsNullOrEmpty(item.Version) ? "1.0" : item.Version,
                                VersionDate = item.VersionDate,
                                FileID = item.FileID,
                                ISLink = item.ISLink,
                                FilePath = item.FilePath,
                                FileName = item.FileName
                            });
                        }
                        rptIComplianceVersion.DataSource = Documentsoutput;
                        rptIComplianceVersion.DataBind();

                        //rptIComplianceVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                        //rptIComplianceVersion.DataBind();

                        rptIComplianceDocumnets.DataSource = null;
                        rptIComplianceDocumnets.DataBind();

                        rptInternalWorkingFiles.DataSource = null;
                        rptInternalWorkingFiles.DataBind();

                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowIntenalDownloadDocument();", true);
                    }
                    else
                    {

                    }
                }

            }
        }

        protected void rptComplianceVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();
                ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();

                if (commandArgs[1].Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                }

              
                if (e.CommandName.Equals("Download"))
                {
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
                    if (AWSData != null)
                    {
                        #region AWS Storage
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            string ForMonth = string.Empty;
                            int IsStatutory1 = Convert.ToInt32(Request.QueryString["ISStatutoryflag"]);
                            if (IsStatutory1 == 2 || IsStatutory1 == 4)//Statutory Checklist or Evenet Based Checklist
                            {
                                var ComplianceData = DocumentManagement.GetForMonthChecklist(Convert.ToInt32(commandArgs[0]));
                                ForMonth = ComplianceData.ForMonth;
                            }
                            else
                            {
                                var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
                                ForMonth = ComplianceData.ForMonth;
                            }
                            ComplianceZip.AddDirectoryByName(ForMonth + "/" + commandArgs[1]);
                            ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                            string directoryPath = "~/TempDocuments/AWS/" + User;

                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(Server.MapPath(directoryPath));
                            }
                            if (ComplianceFileData.Count > 0)
                            {
                                foreach (var item in ComplianceFileData)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                        request.Key = item.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                    }
                                }

                                int i = 0;
                                foreach (var file in ComplianceFileData)
                                {
                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string ext = Path.GetExtension(file.FileName);
                                        string[] filename = file.FileName.Split('.');
                                        //string str = filename[0] + i + "." + filename[1];
                                        string str = filename[0] + i + "." + ext;
                                        ComplianceZip.AddEntry(ForMonth + "/" + commandArgs[1] + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                        i++;
                                    }
                                }
                            }
                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();

                            Response.Buffer = true;

                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument_" + commandArgs[1] + ".zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal Storage
                        using (ZipFile ComplianceZip = new ZipFile())
                        {

                            string ForMonth = string.Empty;
                            int IsStatutory1 = Convert.ToInt32(Request.QueryString["ISStatutoryflag"]);
                            if (IsStatutory1 == 2 || IsStatutory1 == 4)//Statutory Checklist or Evenet Based Checklist
                            {
                                var ComplianceData = DocumentManagement.GetForMonthChecklist(Convert.ToInt32(commandArgs[0]));
                                ForMonth = ComplianceData.ForMonth;
                            }
                            else
                            {
                                var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
                                ForMonth = ComplianceData.ForMonth;
                            }
                            ComplianceZip.AddDirectoryByName(ForMonth + "/" + commandArgs[1]);
                            ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                            if (ComplianceFileData.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in ComplianceFileData)
                                {
                                    string filePath = string.Empty;
                                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                    {
                                        filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                    }
                                    else
                                    {
                                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    }
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string ext = Path.GetExtension(file.FileName);
                                        string[] filename = file.FileName.Split('.');
                                        //string str = filename[0] + i + "." + filename[1];
                                        string str = filename[0] + i + "." + ext;
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }
                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();

                            Response.Buffer = true;

                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument_" + commandArgs[1] + ".zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                        #endregion
                    }
                }
                else if (e.CommandName.Equals("RedirectURL"))
                {
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls()", true);
                }

                int ScheduledOnID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);
                int TransactionID = Convert.ToInt32(Request.QueryString["ComplainceTransactionID"]);
                int IsStatutory = Convert.ToInt32(Request.QueryString["ISStatutoryflag"]);
                BindFiles(ScheduledOnID, TransactionID, IsStatutory);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }

        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            DownloadFile(Convert.ToInt32(e.CommandArgument));
        }
        protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }
        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptComplianceVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplinceVersionDoc");
                //var scriptManager = ScriptManager.GetCurrent(this.Page);
                //scriptManager.RegisterPostBackControl(lblDownLoadfile);

                //LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDocumentVersion");
                //scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);


                Label lblpathIlink = (Label)e.Item.FindControl("lblCompDocpathIlink");
                LinkButton lblRedirectfile = (LinkButton)e.Item.FindControl("lblCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplinceVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);

                if (lblpathIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblRedirectfile.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblRedirectfile.Visible = false;
                }
            }
        }

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = string.Empty;
                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }

                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #region Internal 

        protected void rptIComplianceVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });            
                List<GetInternalComplianceDocumentsView> ComplianceFileData = new List<GetInternalComplianceDocumentsView>();
                List<GetInternalComplianceDocumentsView> ComplianceDocument = new List<GetInternalComplianceDocumentsView>();              
                ComplianceDocument = DocumentManagement.GetFileDataInternal(Convert.ToInt32(commandArgs[0])).ToList();
                if (commandArgs[1].Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                }

                if (e.CommandName.Equals("version"))
                {
                    if (e.CommandName.Equals("version"))
                    {

                        int ScheduledOnID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);
                        int TransactionID = Convert.ToInt32(Request.QueryString["ComplainceTransactionID"]);
                        //int IsStatutory = Convert.ToInt32(Request.QueryString["ISStatutoryflag"]);


                        rptIComplianceDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                        rptIComplianceDocumnets.DataBind();

                        rptInternalWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                        rptInternalWorkingFiles.DataBind();                        
                        List<GetInternalComplianceDocumentsView> CMPDocuments = Business.InternalComplianceManagement.GetInternalDocumnets(Convert.ToInt64(ScheduledOnID), Convert.ToInt64(TransactionID));
                        if (CMPDocuments != null)
                        {                            
                            List<GetInternalComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetInternalComplianceDocumentsView entityData = new GetInternalComplianceDocumentsView();
                                entityData.Version = "1.0";
                                entityData.InternalComplianceScheduledOnID = Convert.ToInt64(ScheduledOnID);
                                entitiesData.Add(entityData);
                            }
                            rptIComplianceVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptIComplianceVersion.DataBind();
                        }                        
                    }
                }
                else if (e.CommandName.Equals("Download"))
                {
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
                    if (AWSData != null)
                    {
                        #region AWS Storage
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            int ScheduledOnID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);
                            int TransactionID = Convert.ToInt32(Request.QueryString["ComplainceTransactionID"]);

                            int IsStatutory1 = Convert.ToInt32(Request.QueryString["ISStatutoryflag"]);
                            string Formonth = string.Empty;
                            if (IsStatutory1 == 3)
                            {
                                var ComplianceData = DocumentManagement.InternalComplianceCheckListGetForMonth(Convert.ToInt32(ScheduledOnID));
                                Formonth = ComplianceData.ForMonth;
                            }
                            else
                            {
                                var ComplianceData = DocumentManagement.InternalComplianceGetForMonth(Convert.ToInt32(ScheduledOnID));
                                Formonth = ComplianceData.ForMonth;

                            }
                            ComplianceZip.AddDirectoryByName(Formonth + "/" + commandArgs[1]);
                            ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                            string directoryPath = "~/TempDocuments/AWS/" + User;

                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(Server.MapPath(directoryPath));
                            }

                            if (ComplianceFileData.Count > 0)
                            {
                                foreach (var item in ComplianceFileData)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                        request.Key = item.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                    }
                                }
                                int i = 0;
                                foreach (var file in ComplianceFileData)
                                {
                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string ext = Path.GetExtension(file.FileName);
                                        string[] filename = file.FileName.Split('.');
                                        string str = filename[0] + i + "." + ext;
                                        ComplianceZip.AddEntry(Formonth + "/" + commandArgs[1] + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                        i++;
                                    }
                                }
                            }

                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();

                            Response.Buffer = true;

                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=InternalComplianceDocument_" + commandArgs[1] + ".zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            Response.End();
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal Storage
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            int ScheduledOnID = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);
                            int TransactionID = Convert.ToInt32(Request.QueryString["ComplainceTransactionID"]);

                            int IsStatutory1 = Convert.ToInt32(Request.QueryString["ISStatutoryflag"]);
                            string Formonth = string.Empty;
                            if (IsStatutory1 == 3)
                            {
                                var ComplianceData = DocumentManagement.InternalComplianceCheckListGetForMonth(Convert.ToInt32(ScheduledOnID));
                                Formonth = ComplianceData.ForMonth;
                            }
                            else
                            {
                                var ComplianceData = DocumentManagement.InternalComplianceGetForMonth(Convert.ToInt32(ScheduledOnID));
                                Formonth = ComplianceData.ForMonth;

                            }
                            ComplianceZip.AddDirectoryByName(Formonth + "/" + commandArgs[1]);
                            ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                            if (ComplianceFileData.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in ComplianceFileData)
                                {
                                    string filePath = string.Empty;
                                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                    {
                                        filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                    }
                                    else
                                    {
                                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    }
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string ext = Path.GetExtension(file.FileName);
                                        string[] filename = file.FileName.Split('.');
                                        //string str = filename[0] + i + "." + filename[1];
                                        string str = filename[0] + i + "." + ext;
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(Formonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(Formonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }

                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();

                            Response.Buffer = true;

                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=InternalComplianceDocument_" + commandArgs[1] + ".zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            Response.End();
                        }
                        #endregion
                    }
                }
                else if (e.CommandName.Equals("RedirectURL"))
                {                    
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls()", true);
                }
                int ScheduledOnID1 = Convert.ToInt32(Request.QueryString["ComplianceScheduleID"]);
                int TransactionID1 = Convert.ToInt32(Request.QueryString["ComplainceTransactionID"]);
                int IsStatutory = Convert.ToInt32(Request.QueryString["ISStatutoryflag"]);
                BindFiles(ScheduledOnID1, TransactionID1, IsStatutory);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptIComplianceVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplinceVersionDoc");
                //var scriptManager = ScriptManager.GetCurrent(this.Page);
                //scriptManager.RegisterPostBackControl(lblDownLoadfile);

                //LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDocumentVersion");
                //scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);

                Label lblpathIlink = (Label)e.Item.FindControl("lblInternalCompDocpathIlink");
                LinkButton lblRedirectDownLoadfile = (LinkButton)e.Item.FindControl("lblInternalCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplinceVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
                if (lblpathIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblRedirectDownLoadfile.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblRedirectDownLoadfile.Visible = false;
                }
            }
        }

        protected void rptIComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {

                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptIComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnIComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }
        
        protected void rptInternalWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            DownloadFile(Convert.ToInt32(e.CommandArgument));
        }

        protected void rptInternalWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnInternalWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }

        #endregion
    }
}