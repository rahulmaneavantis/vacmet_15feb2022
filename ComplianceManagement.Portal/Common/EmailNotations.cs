﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Configuration;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public class EmailNotations
    {
        public static string RLCSSendPasswordResetNotificationEmail(RLCS_User_Mapping user, string passwordText, string url, string customerName)
        {
            string username = string.Format("{0} {1}", user.FirstName, user.LastName);
            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserPasswordReset
                                    .Replace("@Username", user.UserID)
                                    .Replace("@User", username)
                                    .Replace("@PortalURL", url)
                                    .Replace("@Password", passwordText)
                                    .Replace("@From", customerName)
                                    .Replace("@URL", Convert.ToString(url));
            //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserPasswordReset
            //                        .Replace("@Username", user.UserID)
            //                        .Replace("@User", username)
            //                        .Replace("@PortalURL", url)
            //                        .Replace("@Password", passwordText)
            //                        .Replace("@From", customerName)
            //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
            return message;
        }

        public static string SendPasswordResetNotificationEmail(User user, string passwordText,string url,string customerName)
        {
            string username = string.Format("{0} {1}", user.FirstName, user.LastName);
            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserPasswordReset
                                  .Replace("@Username", user.Email)
                                  .Replace("@User", username)
                                  .Replace("@PortalURL", url)
                                  .Replace("@Password", passwordText)
                                  .Replace("@From", customerName)
                                  .Replace("@URL", Convert.ToString(url));
            //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserPasswordReset
            //                        .Replace("@Username", user.Email)
            //                        .Replace("@User", username)
            //                        .Replace("@PortalURL", url)
            //                        .Replace("@Password", passwordText)
            //                        .Replace("@From", customerName)
            //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

            return message;
            //string temp = ConfigurationManager.AppSettings["SenderEmailAddress"];

            //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { user.Email }), null, null, "[CMP] Account Password changed :: " + username, message);
        }

        //public static string ProcessPasswordResetReminderEmail(User user,string days,string customerName)
        //{
        //    string username = string.Format("{0} {1}", user.FirstName, user.LastName);
        //    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_PasswordresetNotification
        //                            .Replace("@User", username)
        //                            .Replace("@username", user.Username)
        //                            .Replace("@days", days)
        //                            .Replace("@From", customerName);

        //    return message;
        //    //EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { user.Email }), null, null, "[CMP] Change Account Password :: " + username, message);
        //}

        //public static void ProcessNoLongerLoginReminderEmail(User user, string days, string customerName)
        //{
        //    string username = string.Format("{0} {1}", user.FirstName, user.LastName);
        //    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NoLongerLoginReminder
        //                            .Replace("@User", username)
        //                            .Replace("@days", days)
        //                            .Replace("@From", customerName);

        //    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { user.Email }), null, null,"Idle Account Notification", message);
        //}

    
    }
}