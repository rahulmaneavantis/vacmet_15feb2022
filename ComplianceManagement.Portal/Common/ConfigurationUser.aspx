﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="ConfigurationUser.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.ConfigurationUser" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>

    <script type="text/javascript">
        function fopenAlert() {
            alert("Frequency Added Successfully.");
            $('#divUserDialog').modal('hide');
            document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();            
            return false;
        }
        function fopenAlert1() {      
            alert("Frequency detail deleted successfully.");
            //$('#divUserDialog').modal('hide');
            document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
            return true;
        }

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        $(document).ready(function () {
            $("#ContentPlaceHolder1_udcInputForm_tbxBranch").unbind('click');
            $("#ContentPlaceHolder1_udcInputForm_tbxBranch").click(function () {
                $("#divBranches").toggle("blind", null, 500, function () { });
            });
        });

        function txtclick() {
            $("#divBranches").toggle("blind", null, 500, function () { });
        }
        $('#tbxFirstName').mouseenter(function () {

            alert('empty');
            //process further

        });

        $(document).ready(function () {
            //setactivemenu('leftmastermenu');
            fhead('Frequency Details');
        });

        function openModalControl() {
            $('#divUserDialog').modal('show');
            return true;
        }

        function ClosePopUser() {
            $('#divUserDialog').modal('hide');
            document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
        }

    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12">
                <div style="margin: 5px">
                    <%--<asp:ScriptManager ID="LitigationAddDept" runat="server"></asp:ScriptManager>--%>
                    <asp:UpdatePanel ID="upUserList" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 4px">
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none"
                                    class="alert alert-block alert-danger fade in" ValidationGroup="UserPageValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="UserPageValidationGroup" Display="None" />
                            </div>

                            <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0" style="margin-right: 18px;">
                               <asp:DropDownList runat="server" ID="ddlFrequencyList1" OnSelectedIndexChanged="ddlFrequencyList1_SelectedIndexChanged"
                                                       Style="padding: 0px; margin: 0px; widtfh: 238px;padding-right: 24px;"
                            class="form-control m-bot15" AutoPostBack="true" />
                            </div>
                             <div class="col-md-3 colpadding0">
                               <asp:DropDownList runat="server" OnSelectedIndexChanged="ddlFrequencyType_SelectedIndexChanged" ID="ddlFrequencyType" Style="padding: 0px; margin: 0px;  width: 246px;"
                            class="form-control m-bot15" AutoPostBack="true">
                                   <asp:ListItem Text="Upcoming Notification" Value="UN" Selected="true" />
                                   <asp:ListItem Text="Overdue Notification" Value="ON" />
                                   <asp:ListItem Text="Pending for Review Notification" Value="PRN" />

                               </asp:DropDownList>
                             </div>
                            <div class="col-md-3 colpadding0">
                              <asp:DropDownList runat="server" ID="ddlComplianceType" OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged" Style="padding: 0px; margin: 0px; width: 246px;"
                            class="form-control m-bot15" AutoPostBack="true">
                            <asp:ListItem Text="Statutory" value="S"  Selected="true"/>
                            <asp:ListItem Text="Internal" value="I"/>
                            <asp:ListItem Text="Statutory Event" Value="SE" />
                            <asp:ListItem Text="Internal Event" Value="IE" />
                        </asp:DropDownList>
                             </div>
                                <div class="col-md-2 colpadding0 text-right">                                 
                                        <asp:LinkButton ID="lnkBtn_RebindGrid" OnClick="lnkBtn_RebindGrid_Click" Style="display: none;" runat="server"></asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnAddUser" OnClientClick="openModalControl();" OnClick="btnAddUser_Click"
                                        data-toggle="tooltip" data-placement="bottom" ToolTip="Add New Frequency" CssClass="btn btn-primary">
                                        Add New</asp:LinkButton>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="row">
                                <div class="panel col-md-12 colpadding0" style="overflow: hidden;">
                                    <asp:GridView runat="server" ID="grdUser" AutoGenerateColumns="false" AllowPaging="true" AutoPostBack="true" AllowSorting="true"
                                        CssClass="table" GridLines="none" PageSize="10" OnSorting="grdUser_Sorting" Width="100%" ShowHeaderWhenEmpty="true"
                                        DataKeyNames="ID" OnRowCommand="grdUser_RowCommand" OnRowCreated="grdUser_RowCreated"
                                        OnRowDeleting="grdUser_RowDeleting">
                                        <Columns>
                                            <asp:BoundField DataField="Frequency_Name" HeaderText="Frequency" HeaderStyle-Height="20px"  />
                                            <asp:BoundField DataField="TimeInDays" HeaderText="Days" HeaderStyle-Height="20px"  />
                                            <asp:BoundField DataField="RepeatEveryDays" HeaderText="Repeat Every Days" HeaderStyle-Height="20px"  />
                                            <asp:BoundField DataField="FlagName" HeaderText="Status" HeaderStyle-Height="20px" />
                                            <asp:BoundField DataField="TypeName" HeaderText="Type" HeaderStyle-Height="20px" SortExpression="Flag" />
                                            <asp:TemplateField HeaderText="Action" HeaderStyle-Width="5%" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                       
                                                        <asp:LinkButton runat="server" CommandName="DELETE" ToolTip="Delete" data-toggle="tooltip" data-placement="bottom"
                                                            CommandArgument='<%# Eval("ID") %>' ID="lbtnDelete" OnClientClick="return confirm('Are you certain you want to delete this frequency?');">
                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete User" />
                                                        </asp:LinkButton>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerSettings Visible="false" />
                                        <EmptyDataTemplate>
                                            No Record Found
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 colpadding0">
                                    <div class="col-md-10 colpadding0">
                                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                                            <p style="padding-right: 0px !Important;">
                                                <%--<asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>--%>
                                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                    <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                    <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-1 colpadding0">
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 100%; float: right;margin-right: 6%;"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                            <asp:ListItem Text="5" />
                                            <asp:ListItem Text="10" Selected="True" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-md-1 colpadding0" style="float: right;">
                                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="35px">
                                        </asp:DropDownListChosen>

                                    </div>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                           <%-- <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0">
                                    <div class="col-md-2 colpadding0">
                                        <asp:LinkButton Text="Previous" CssClass="btn btn-primary" Visible="false" Width="100%" OnClick="lnkPreviousSwitch_Click" ToolTip="Go to Previous Master (Legal Entity)" data-toggle="tooltip" runat="server" ID="LnkbtnPrevious" />
                                    </div>
                                    <div class="col-md-10 colpadding0">
                                    </div>
                                </div>
                                <div class="col-md-6 colpadding0">
                                    <div class="col-md-10 colpadding0">
                                    </div>
                                    <div class="col-md-2 colpadding0">
                                        <asp:LinkButton Text="Next" CssClass="btn btn-primary" runat="server" OnClick="lnkNextSwitch_Click" ToolTip="Go to Next Master (Vendor)" data-toggle="tooltip" ID="LnkbtnNext" Style="float: right; width: 100%;" />
                                    </div>
                                </div>
                            </div>--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
            </div>
        </div>
    </div>

    <%-- Modal Pop-Up  --%>
    <div class="modal fade" id="divUserDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-left: 24%;">
        <div class="modal-dialog" style="width: 100%;">
            <div class="modal-content" style="width: 60%; height: auto;">
                <div class="modal-header">
                    &nbsp;
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 180px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Frequency Update</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <%--<vit:audituserdetailscontrol runat="server" id="udcInputForm" />--%>
                    <asp:UpdatePanel ID="upUsersPopup" runat="server" UpdateMode="Conditional" OnLoad="upUsersPopup_Load" style="margin-right: -225px;margin-left: 47px;">
                        <ContentTemplate>
                            <div>
                                <div class="row col-md-12">
                                    <div class="col-md-12 alert alert-block alert-success fade in" runat="server" visible="false" id="divsuccessmsgCusrlst">
                                        <asp:Label runat="server" ID="successmsgCusrlst"></asp:Label>
                                    </div>
                                    <asp:ValidationSummary ID="vsUserPopup" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="UserPopupValidationGroup" />

                                    <asp:CustomValidator ID="cvUserPopup" runat="server" EnableClientScript="False"
                                        ValidationGroup="UserPopupValidationGroup" Display="None" />

                                </div>
                                <div class="row"  style="padding: 5px;">
                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                        Compliance Type</label>
                                    <asp:DropDownList runat="server" ID="DropDownList2" Style="padding: 0px; margin: 0px;  width: 287px;"
                                       OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged" class="form-control m-bot15" AutoPostBack="true">
                                        <asp:ListItem Text="Statutory" Value="S" Selected="true" />
                                        <asp:ListItem Text="Internal" Value="I" />
                                        <asp:ListItem Text="Statutory Event" Value="SE" />
                                        <asp:ListItem Text="Internal Event" Value="IE" />
                                    </asp:DropDownList>
                                </div>
                                <div class="row" style="padding: 5px;" id="frequencydisplay" runat="server">
                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                        Frequency</label>
                                    <asp:DropDownList runat="server" ID="ddlFrequencyList" Style="padding: 0px; margin: 0px; width: 287px;"
                                        class="form-control m-bot15" AutoPostBack="true" />
                                    <%--<asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select Frequency." ControlToValidate="ddlFrequencyList"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserPopupValidationGroup"
                                        Display="None" />--%>
                                </div>
                                <div class="row"  style="padding: 5px;">
                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                        Type</label>
                                    <asp:DropDownList runat="server" ID="DropDownList1" 
                                        Style="padding: 0px; margin: 0px; width: 287px;"
                                        class="form-control m-bot15" AutoPostBack="true">
                                        <asp:ListItem Text="Upcoming Notification" Value="UN" Selected="true" />
                                        <asp:ListItem Text="Overdue Notification" Value="ON" />
                                        <asp:ListItem Text="Pending for Review Notification" Value="PRN" />

                                    </asp:DropDownList>
                                </div>
                                
                                <div class="row"  style="padding: 5px;">
                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                        Days</label>
                                     <asp:TextBox runat="server" onkeypress="return isNumber(event)" CssClass="form-control" ID="tbxTimeInDays"
                                      Style="width: 200px;" MaxLength="100" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Days can not be empty."
                            ControlToValidate="tbxTimeInDays" runat="server" ValidationGroup="UserPopupValidationGroup"
                            Display="None" /> 
                                </div>
                               <div class="row"  style="padding: 5px;">
                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                    <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                        Repeat Every(Days)</label>
                                    <asp:TextBox runat="server" ID="txtRepeatDay" CssClass="form-control" onkeypress="return isNumber(event)" Style="width: 200px;" MaxLength="100" />                      
                                </div>

                                <div class="clearfix"></div>

                                <div style="margin-bottom: 7px; margin-top: 10px; margin-top: 10px;margin-left: 194px;">
                                    <%--float: right; margin-right: 257px; --%>;
                                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                        ValidationGroup="UserPopupValidationGroup" />
                                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="ClosePopUser();" />
                                </div>

                                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 25px;">
                                    <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                </div>
                                <div class="clearfix" style="height: 50px"></div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <%--<asp:PostBackTrigger ControlID="btnSave" />--%>
                            <%--<asp:AsyncPostBackTrigger ControlID="btnUpload" EventName="Click" />--%>
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
