﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LicDocumentOverviewInternal.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.LicDocumentOverviewInternal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">
    <title></title>
    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>




    <style type="text/css">
        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: #1fd9e1;
            background-color: #f7f7f7;
        }

        .panel-heading .nav > li > a {
            font-size: 20px !important;
        }

        .panel-heading .nav > li > a {
            border-bottom: 0px;
        }

        .customDropDownCheckBoxCSS {
            height: 32px !important;
            width: 70%;
        }

        .chosen-single {
            color: #8e8e93;
        }

        .container {
            max-width: 100%;
        }

        ul.multiselect-container.dropdown-menu {
            width: 100%;
            height: 100px;
            overflow-y: auto;
        }

        button.multiselect.dropdown-toggle.btn.btn-default {
            text-align: left;
        }

        span.multiselect-selected-text {
            float: left;
            color: #444;
            font-family: 'Roboto', sans-serif !important;
        }

        b.caret {
            float: right;
            margin-top: 8px;
        }

        label {
            font-weight: 500;
            color: #666;
        }

        .fixed {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
        }
    </style>

    <style type="text/css">
        tr.spaceUnder > td {
            padding-top: 1em;
        }

        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .circle {
            width: 24px;
            height: 24px;
            border-radius: 59%;
            display: inline-block;
            margin-left: 8px;
        }
    </style>


    <script type="text/javascript">
               function openInNewTab(url) {
        var win = window.open(url, '_blank');
        win.focus();
    }
          function ShowDownloadDocument() {
            $('#divDownloadDocument').modal('show');
            return true;
        };
    </script>

    <script type="text/javascript">
        //function ShowIntenalDownloadDocument() {
        //    $('#divInternalDownloadDocument').modal('show');
        //    return true;
        //};

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <div class="modal fade" id="divDownloadDocument" style="bottom: auto; overflow: hidden;" tabindex="-1" role="dialog;" aria-labelledby="myModalLabel1" aria-hidden="true">                        
            <div class="modal-dialog" style="width: 550px; margin-top: -11px; margin-left: -15px;">
                <div class="modal-content">
                    <div class="modal-header">
                        
                    </div>
                    <div class="modal-body" style="height: 323px;">
                        <table width="100%" style="text-align: left; margin-left: 10%;">
                            <thead>
                                <tr>
                                    <td valign="top">
                                        <asp:Repeater ID="rptDownloadTaskVersion" runat="server" OnItemCommand="rptDownloadTaskVersion_ItemCommand"
                                            OnItemDataBound="rptDownloadTaskVersion_ItemDataBound">
                                            <HeaderTemplate>
                                                <table id="tblDownloadTaskVersionDocumnets">
                                                    <thead>
                                                        <th>Versions</th>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' ID="lblDownloadTaskDocumentVersion"
                                                            runat="server" Text='<%# Eval("Version")%>'></asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton class="btn btn-search" CommandName="Download" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' OnClientClick='javascript:enableControls()'
                                                            ID="btnDownloadTaskVersionDoc" runat="server" Text="Download" Style="margin-top: 10%;">
                                                        </asp:LinkButton>

                                                         <asp:Label ID="lblCompDocpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>                                                                                                 
                                                        <asp:LinkButton CommandName="RedirectURL" ID="lblCompDocpathDownload"  
                                                            OnClientClick=<%# "openInNewTab('" + Eval("FilePath") + "')" %>
                                                            Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                        runat="server" Font-Underline="false" />  
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
      

        <div>
            
        </div>
    </form>
</body>
</html>

