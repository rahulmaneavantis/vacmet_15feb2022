﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class ReviewerLockingDays : System.Web.UI.Page
    {
        protected static int CustId;
        protected static int UId;
        protected static string Path;
        protected static string Authorization;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
                string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
                Authorization = (string)HttpContext.Current.Cache[CacheName];
                if (Authorization == null)
                {
                    Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                    HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                }
                Path = ConfigurationManager.AppSettings["KendoPathApp"];
                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int CID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    long IDofDetailLocking = -1;

                    if (SelectedID.Text != "")
                    {
                        IDofDetailLocking = Convert.ToInt32(SelectedID.Text);
                    }
                    if (IDofDetailLocking != -1)
                    {
                        ComplianceUnlockStatu getDetails = (from row in entities.ComplianceUnlockStatus
                                                            where row.ID == IDofDetailLocking
                                                            && row.Unlock == false
                                                            select row).FirstOrDefault();

                        if (getDetails != null)
                        {
                            DateTime date = new DateTime();
                            if (txtUpdateDate.Text != "")
                            {
                                date = DateTime.ParseExact(txtUpdateDate.Text, new string[] { "dd.MM.yyyy", "yyyy-MM-dd", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);

                            }
                            var dateDifference = Convert.ToInt32((date - Convert.ToDateTime(getDetails.ScheduledOn)).TotalDays);

                            var getsubDetails = (from row in entities.SubDetailOfLockingDays
                                                 where row.DetailOfLockingDayID == IDofDetailLocking
                                                 select row).ToList();
                            int cnt = 0;
                            if (getsubDetails.Count > 0)
                            {
                                cnt = 1;
                            }
                            if (cnt == 0)
                            {
                                SubDetailOfLockingDay subdetail1 = new SubDetailOfLockingDay();
                                subdetail1.DetailOfLockingDayID = getDetails.ID;
                                subdetail1.ScheduleOnID = Convert.ToInt64(getDetails.ComplianceScheduleOnID);
                                subdetail1.DueDay = Convert.ToInt32((Convert.ToDateTime(DateTime.Now) - Convert.ToDateTime(getDetails.ScheduledOn)).TotalDays);
                                if (getDetails.ScheduledOn != null)
                                {
                                    subdetail1.DueDate = Convert.ToDateTime(getDetails.ScheduledOn);
                                }
                                subdetail1.CreatedBy = getDetails.CreatedBy;
                                subdetail1.CreatedOn = getDetails.Dated;
                                subdetail1.Remark = "First Time Locking";
                                entities.SubDetailOfLockingDays.Add(subdetail1);
                                entities.SaveChanges();
                            }

                            SubDetailOfLockingDay subdetail = new SubDetailOfLockingDay();
                            subdetail.DetailOfLockingDayID = getDetails.ID;
                            subdetail.ScheduleOnID = Convert.ToInt64(getDetails.ComplianceScheduleOnID);
                            if (getDetails.ScheduledOn != null)
                            {
                                subdetail.DueDate = Convert.ToDateTime(getDetails.ScheduledOn);
                            }
                            subdetail.DueDay = dateDifference;
                            subdetail.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                            subdetail.CreatedOn = DateTime.Now;
                            if (!string.IsNullOrEmpty(txtRemark.Text))
                            {
                                subdetail.Remark = txtRemark.Text;
                            }

                            entities.SubDetailOfLockingDays.Add(subdetail);
                            entities.SaveChanges();


                            getDetails.DueDay = dateDifference;
                            getDetails.Unlock = true;
                            if (txtUpdateDate != null)
                            {
                                getDetails.Dated = date;
                            }
                            entities.SaveChanges();

                            //cvCasePopUp.IsValid = false;
                            //cvCasePopUp.ErrorMessage = "This compliance is unlock.";

                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenAlert();", true);
                            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvCasePopUp.IsValid = false;
                //cvCasePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}