﻿<%@ Page Title="Management Report" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ComplianceManagementReport.aspx.cs" 
Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.ComplianceManagementReport" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<style type="text/css">
    /*.TFtable{
		width:100%; 
		border-collapse:collapse; 
	}
	.TFtable td{ 
		padding:7px; border:#4e95f4 1px solid;
	}*/

    .highlight {
        background: red;
    }

    .tvtable th {
        background: url("images/ui-bg_gloss-wave_55_5c9ccc_500x100.png") 50% 50% repeat-x;
        color: #FFFFFF;
        height: 10px;
        border: 1px solid #4297D7;
        font-weight: bold;
    }

    .tvtable tr {
        background: #E6EFF7;
    }
        /*  Define the background color for all the ODD background rows  */
        .tvtable tr:nth-child(odd) {
            background: #E6EFF7;
        }
        /*  Define the background color for all the EVEN background rows  */
        .tvtable tr:nth-child(even) {
            background: #FFFFFF;
        }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
<asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0;
                right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px;
                    position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

 <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
          <ContentTemplate>      

    <table width="100%" style="height:35px">
            <tr>
                <td align="left">
                    <asp:Label ID="lblErrorMessage" runat="server" style="color:Red"></asp:Label>
                </td>
            </tr>
          
        </table>
    <div id="OuterDiv" style="width:100%; margin:30px 20px 10px 30px">
    <table id="OuterTable" border="0" width="100%">
        <tr>
         <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
          <ContentTemplate>  
            <td style="vertical-align:top">
                <table border="0" width="100%">
                    <tr>
                            <td> 
                            <asp:TreeView runat="server" ID="tvLocation" BackColor="White" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="Black"
                                Style="overflow: auto" ShowLines="true" OnTreeNodeExpanded="tvLocation_TreeNodeExpanded" OnSelectedNodeChanged="tvLocation_SelectedNodeChanged"
                                OnTreeNodeCollapsed="tvLocation_TreeNodeCollapsed" NodeStyle-NodeSpacing="5px">
                            </asp:TreeView>
                            </td>
                            <td>
                            <div style="margin-bottom:10px">
                              
                            </div>
                            <div id="divCountTableContainer" runat="server">
                            
                             
                            </div>  
                        </td>
                     </tr>
                
                </table>
            </td>
            </ContentTemplate>
            </asp:UpdatePanel>
             <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
          <ContentTemplate>  
            <td> 
            <div style="float:left">
               <table >
                <tr>
                 <td style="text-align:center;width:300px">
                     <asp:Label ID="titleDelayedCompliances" runat="server" Text="High Risk Delayed Compliances" style="font-family:Verdana;font-size:12px;"></asp:Label>
                     <asp:Chart ID="chrtDelayedCompliances" runat="server" Width="300px" Height="300px" >
                    <Legends>
                        <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" 
                            LegendStyle="Table" />
                    </Legends>
                    <Series>
                        <asp:Series ChartType="Doughnut" XValueMember="Name" YValueMembers="Quantity" IsValueShownAsLabel="true">
                            <EmptyPointStyle IsValueShownAsLabel="false" IsVisibleInLegend="false" />
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Area3DStyle-Enable3D="false">
                        </asp:ChartArea>
                    </ChartAreas>
                   </asp:Chart>
                 </td>
                 <td style="text-align:center;width:300px";>
                      <asp:Label ID="titlePendingCompliances" runat="server" Text="High Risk Pending Compliances" style="font-family:Verdana;font-size:12px; "></asp:Label>
                     <asp:Chart ID="chrtPendingCompliances" runat="server" Width="300px" Height="300px">
                    <Legends>
                        <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" 
                            LegendStyle="Table" />
                    </Legends>
                    <Series>
                        <asp:Series ChartType="Doughnut" XValueMember="Name" YValueMembers="Quantity" IsValueShownAsLabel="true">
                            <EmptyPointStyle IsValueShownAsLabel="false" IsVisibleInLegend="false" />
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Area3DStyle-Enable3D="false">
                        </asp:ChartArea>
                    </ChartAreas>
                   </asp:Chart>
                 </td>
                </tr>
             </table>
            </div>
            
            </td>
              <asp:HiddenField ID="hdnUpdate"  runat="server" />
              <asp:Button ID="btnHidden" runat="server" Text="Button" style="display:none" OnClick="button_Click" />
            </ContentTemplate>
            

            </asp:UpdatePanel>
        </tr>
    </table>
    <asp:Label ID="lblNote" runat="server" Text="*Please click on a count to generate the graph." style="font-family:Verdana;font-size:12;float: left;
        padding-top: 10px;margin-left:10px"></asp:Label>
</div>


     </ContentTemplate>
 </asp:UpdatePanel>

    <script language="javascript" type="text/javascript">

        function RefreshChart(id) {
            $("#<%= hdnUpdate.ClientID %>").val(id);
            $("#<%= btnHidden.ClientID %>").click();
        }

        function AssignRowColor(id) {
            $("#ComplianceCountTable tr:odd").css("background-color", "#E6EFF7");
            $("#ComplianceCountTable tr:even").css("background-color", "#FFFFFF");

            $("#" + id).css('background', '#EBC79E');
        }

</script>
</asp:Content>
