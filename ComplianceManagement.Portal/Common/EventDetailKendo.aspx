﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EventDetailKendo.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Event.EventDetailKendo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css">
        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .k-checkbox-wrapper {
            display: inline-block;
            vertical-align: middle;
            margin-left: -13px;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        #gridEvent.k-grid-content {
            max-height: 100px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden !important;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-multiselect-wrap .k-input {
            /*padding-top:6px;*/
            display: inherit !important;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            BindGrid();
        });

        function BindGrid() {
            var gridexist = $('#gridEvent').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#gridEvent').empty();

            var gridEvent = $("#gridEvent").kendoGrid({
                columnMenuInit(e) {
                    e.container.find('li[role="menuitemcheckbox"]:nth-child(8)').remove();
                },
                dataSource: {
                    transport: {
                        read: "<% =Path%>Data/GetEventData?ParentEventID=<%=parentID%>&BranchID=<%=branchID%>"

                    },
                    pageSize: 5
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                // pageable: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                scrollable: true,
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [{
                    title: "Sr No",
                    template: "#= ++record #",
                    width: "4.5%"
                },
                {
                    field: "IntermediatesubEvent",
                    title: "Sub Event",
                    width: "15%",
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "ActName",
                    title: "Act Name",
                    width: "15%",
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "Sections",
                    title: "Sections",
                    width: "10%",
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "ShortDescription",
                    title: "Compliance",
                    width: "10%",
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "Description",
                    title: "Description",
                    width: "20%",
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "PenaltyDescription",
                    title: "Penalty Description",
                    width: "20%",
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }
                },
                ]
            });

            $("#gridEvent").kendoTooltip({
                //filter: ("td:nth-child(n+2)"),
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        return e.preventDefault();

                }
            }).data("kendoTooltip");

            $("#gridEvent .k-auto-scrollable").css('overflow-x', 'hidden');
            $("#gridEvent .k-auto-scrollable").css('overflow-y', 'auto');
            $("#gridEvent .k-grid-header-wrap.k-auto-scrollable").css('overflow', 'hidden');
        }


    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="smAddReminder" runat="server"></asp:ScriptManager>
        <asp:UpdateProgress ID="updateProgress" runat="server">
            <ProgressTemplate>
                <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                    <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                        AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <div>
            <asp:Label ID="lblEventName" Text="Event Name" runat="server" Style="color: #666666; font-family: 'Roboto' sans-serif; font-weight: normal; font: bold; font-size: 20px;"></asp:Label>

            <asp:Button Text="Back" CssClass="btn btn-primary" ToolTip="Go to Previous" data-toggle="tooltip"
                runat="server" ID="lnkbtnPrevious" Style="width: 50px; display: none; height: 20px; margin-left: 980px;" OnClick="lnkPreviousSwitch_Click" />
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filtersstoryboard">&nbsp;</div>
            <div id="gridEvent"></div>
        </div>

    </form>
</body>
</html>
