﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class TaskDownloadOverview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["TaskScheduleID"]) && !string.IsNullOrEmpty(Request.QueryString["TaskTransactionID"]))
            {
                int ScheduledOnID = Convert.ToInt32(Request.QueryString["TaskScheduleID"]);
                int TransactionID = Convert.ToInt32(Request.QueryString["TaskTransactionID"]);
                int IsStatutory = Convert.ToInt32(Request.QueryString["ISStatutoryflag"]);
                BindFiles(ScheduledOnID, TransactionID, IsStatutory);
            }
        }
        public class GetTaskDocumentData
        {
            public Nullable<int> ID { get; set; }
            public int FileID { get; set; }
            public Nullable<long> TaskScheduleOnID { get; set; }
            public string FileName { get; set; }
            public string FilePath { get; set; }
            public string Version { get; set; }
            public Nullable<System.DateTime> VersionDate { get; set; }
            public Nullable<bool> ISLink { get; set; }
        }
        private void BindFiles(int scheduledOnID, int transactionID, int IsStatutory)
        {
            if (IsStatutory == 1 || IsStatutory == 2)//Statutory or internal
            {
                List<GetTaskDocumentData> taskDocumentsoutput = new List<GetTaskDocumentData>();
                List<GetTaskDocumentView> CMPDocuments = TaskManagment.GetDocumnets(Convert.ToInt64(scheduledOnID), Convert.ToInt64(transactionID));
                if (CMPDocuments != null)
                {
                    //List<GetTaskDocumentView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                    //if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                    //{
                    //    GetTaskDocumentView entityData = new GetTaskDocumentView();
                    //    entityData.Version = "1.0";
                    //    entityData.TaskScheduleOnID = Convert.ToInt64(scheduledOnID);
                    //    entitiesData.Add(entityData);
                    //}

                    if (CMPDocuments.Count > 0)
                    {
                        taskDocumentsoutput.Clear();
                        var entitiesData1 = CMPDocuments.Where(x => x.ISLink == false).ToList();
                        var documentVersionData = entitiesData1.GroupBy(entry => new { entry.Version }).Select(entry => entry.FirstOrDefault()).ToList();
                        var documentVersionData1 = CMPDocuments.Where(x => x.ISLink == true).ToList();
                        foreach (var item in documentVersionData)
                        {
                            taskDocumentsoutput.Add(new GetTaskDocumentData
                            {
                                ID = item.ID,
                                TaskScheduleOnID = item.TaskScheduleOnID,
                                Version = string.IsNullOrEmpty(item.Version) ? "1.0" : item.Version,
                                VersionDate = item.VersionDate,
                                FileID = item.FileID,
                                ISLink = item.ISLink,
                                FilePath = item.FilePath,
                                FileName = item.FileName
                            });
                        }
                        foreach (var item in documentVersionData1)
                        {
                            taskDocumentsoutput.Add(new GetTaskDocumentData
                            {
                                ID = item.ID,
                                TaskScheduleOnID = item.TaskScheduleOnID,
                                Version = string.IsNullOrEmpty(item.Version) ? "1.0" : item.Version,
                                VersionDate = item.VersionDate,
                                FileID = item.FileID,
                                ISLink = item.ISLink,
                                FilePath = item.FilePath,
                                FileName = item.FileName
                            });
                        }
                        //rptDownloadTaskVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                        rptDownloadTaskVersion.DataSource = taskDocumentsoutput;
                        rptDownloadTaskVersion.DataBind();
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDownloadDocument();", true);
                    }                   
                }
            }
            else
            {
               

            }
        }

        protected void rptDownloadTaskVersion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnDownloadTaskVersionDoc");
                //var scriptManager = ScriptManager.GetCurrent(this.Page);
                //scriptManager.RegisterPostBackControl(lblDownLoadfile);

                //LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDownloadTaskDocumentVersion");
                //scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);

                Label lblpathIlink = (Label)e.Item.FindControl("lblCompDocpathIlink");
                LinkButton lblRedirectLink = (LinkButton)e.Item.FindControl("lblCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnDownloadTaskVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDownloadTaskDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);

                if (lblpathIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblRedirectLink.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblRedirectLink.Visible = false;
                }
            }
        }
        protected void rptDownloadTaskVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (commandArgs[1].Equals("1.0"))
                    {
                        taskDocument = taskDocument.Where(entry => entry.Version == commandArgs[1]).ToList();

                        if (taskDocument.Count <= 0)
                        {
                            taskDocument = taskDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        taskDocument = taskDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    }

                    if (e.CommandName.Equals("version"))
                    {
                        if (e.CommandName.Equals("version"))
                        {
                            
                            if (taskDocument.Count > 0)
                            {
                                List<GetTaskDocumentData> taskDocumentsoutput = new List<GetTaskDocumentData>();
                                taskDocumentsoutput.Clear();
                                var entitiesData1 = taskDocument.Where(x => x.ISLink == false).ToList();
                                var documentVersionData = entitiesData1.GroupBy(entry => new { entry.Version }).Select(entry => entry.FirstOrDefault()).ToList();
                                var documentVersionData1 = taskDocument.Where(x => x.ISLink == true).ToList();
                                foreach (var item in documentVersionData)
                                {
                                    taskDocumentsoutput.Add(new GetTaskDocumentData
                                    {
                                        ID = item.ID,
                                        TaskScheduleOnID = item.TaskScheduleOnID,
                                        Version = string.IsNullOrEmpty(item.Version) ? "1.0" : item.Version,
                                        VersionDate = item.VersionDate,
                                        FileID = item.FileID,
                                        ISLink = item.ISLink,
                                        FilePath = item.FilePath,
                                        FileName = item.FileName
                                    });
                                }
                                foreach (var item in documentVersionData1)
                                {
                                    taskDocumentsoutput.Add(new GetTaskDocumentData
                                    {
                                        ID = item.ID,
                                        TaskScheduleOnID = item.TaskScheduleOnID,
                                        Version = string.IsNullOrEmpty(item.Version) ? "1.0" : item.Version,
                                        VersionDate = item.VersionDate,
                                        FileID = item.FileID,
                                        ISLink = item.ISLink,
                                        FilePath = item.FilePath,
                                        FileName = item.FileName
                                    });
                                }
                                rptDownloadTaskVersion.DataSource = taskDocumentsoutput;
                                rptDownloadTaskVersion.DataBind();
                            }
                            //var documentVersionData = taskDocument.Select(x => new
                            //{
                            //    ID = x.ID,
                            //    TaskScheduleOnID = x.TaskScheduleOnID,
                            //    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                            //    VersionDate = x.VersionDate,
                            //    FileID = x.FileID,
                            //    //VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                            //}).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

                            //rptDownloadTaskVersion.DataSource = documentVersionData;
                            //rptDownloadTaskVersion.DataBind();

                            //upTaskDocument.Update();
                        }
                    }
                    else if (e.CommandName.Equals("Download"))
                    {
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                taskDocument = taskDocument.Where(entry => entry.Version == commandArgs[1]).ToList();

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                if (taskDocument.Count > 0)
                                {
                                    taskDocument = taskDocument.Where(x => x.ISLink == false).ToList();
                                    ComplianceZip.AddDirectoryByName(commandArgs[1]);

                                    int i = 0;

                                    foreach (var item in taskDocument)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }

                                    foreach (var eachFile in taskDocument)
                                    {
                                        
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), eachFile.FileName);
                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(eachFile.FileName);
                                            string[] filename = eachFile.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;

                                            ComplianceZip.AddEntry(commandArgs[1] + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                             
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                taskDocument = taskDocument.Where(entry => entry.Version == commandArgs[1]).ToList();

                                if (taskDocument.Count > 0)
                                {
                                    taskDocument = taskDocument.Where(x => x.ISLink == false).ToList();
                                    ComplianceZip.AddDirectoryByName(commandArgs[1]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(eachFile.FileName);
                                            string[] filename = eachFile.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (eachFile.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    //else if (e.CommandName.Equals("View"))
                    //{
                    //    #region
                    //    List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                    //    taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    //    Session["ScheduleOnID"] = taskScheduleOnID;

                    //    if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                    //    {
                    //        List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                    //        if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                    //        {
                    //            GetTaskDocumentView entityData = new GetTaskDocumentView();
                    //            entityData.Version = "1.0";
                    //            entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                    //            entitiesData.Add(entityData);
                    //        }

                    //        if (entitiesData.Count > 0)
                    //        {
                    //            foreach (var file in taskDocumenttoView)
                    //            {
                    //                rptViewTaskVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                    //                rptViewTaskVersion.DataBind();

                    //                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                    //                if (file.FilePath != null && File.Exists(filePath))
                    //                {
                    //                    string Folder = "~/TempFiles";
                    //                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                    //                    string DateFolder = Folder + "/" + File;

                    //                    string extension = System.IO.Path.GetExtension(filePath);

                    //                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                    //                    if (!Directory.Exists(DateFolder))
                    //                    {
                    //                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                    //                    }

                    //                    string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    //                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                    //                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                    //                    string FileName = DateFolder + "/" + User + "" + extension;

                    //                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                    //                    BinaryWriter bw = new BinaryWriter(fs);
                    //                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                    //                    bw.Close();

                    //                    subTaskDocViewPath = FileName;
                    //                    subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                    //                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                    //                    lblMessage.Text = "";
                    //                }
                    //                else
                    //                {

                    //                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                    //                }
                    //                break;
                    //            }
                    //        }
                    //    }
                    //    #endregion
                    //}
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);              
            }
        }

    }
}