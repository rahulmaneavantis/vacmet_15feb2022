﻿using com.VirtuosoITech.ComplianceManagement.Business;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Common
{
    public partial class ActivatedEventNew : System.Web.UI.Page
    {
        protected static int CustId;
        protected static int UId;
        protected static int roleId;
        protected static string Path;
        protected static string Falg;
        protected static string UserId;
        protected static string CId;
        protected static int RoleID;
        protected static int RoleFlag;
        protected static int PerformerFlagID;
        protected static int ReviewerFlagID;
        protected static List<Int32> roles;
        protected static int ComplianceTypeID;
        protected static int UserRoleID;
        protected static int StatusFlagID;
        protected static bool DisableFalg;
        protected static string Authorization;
        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            CId = Convert.ToString(AuthenticationHelper.CustomerID);
            UserId = Convert.ToString(AuthenticationHelper.UserID);
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            RoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);
            RoleFlag = 0;

            PerformerFlagID = 0;
            ReviewerFlagID = 0;
            //get role for Performer and Reviewer
            if (Session["User_comp_Roles"] != null)
            {
                roles = Session["User_comp_Roles"] as List<int>;
            }
            else
            {
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                Session["User_comp_Roles"] = roles;
            }
            if (roles.Contains(3))
                PerformerFlagID = 1;
            if (roles.Contains(4))
                ReviewerFlagID = 1;

            string InputType = Request.QueryString["type"];//Statutory,Internal,Event Based
            if (!string.IsNullOrEmpty(InputType))
            {
                if (InputType.Equals("Statutory"))
                    ComplianceTypeID = -1;
                if (InputType.Equals("Internal"))
                    ComplianceTypeID = 0;
            }
            else
            {
                ComplianceTypeID = -1;
            }

            string InputRole = Request.QueryString["role"];//Perfomer,Reviewer
            if (!string.IsNullOrEmpty(InputRole))
            {
                if (InputRole.Equals("Performer"))
                    UserRoleID = 3;
                if (InputRole.Equals("Reviewer"))
                    UserRoleID = 4;
            }
            else
            {
                if (roles.Contains(3))
                    UserRoleID = 3;
                else if (roles.Contains(4))
                    UserRoleID = 4;
            }


            string InputFilter = Request.QueryString["filter"];//Status,Upcoming,Overdue,PendingForReview,Rejected,DueButNotSubmitted
            Session["filter"] = InputFilter;

            if (!string.IsNullOrEmpty(InputFilter))
            {
                if (InputFilter.Equals("Status"))
                    StatusFlagID = -1;
                if (InputFilter.Equals("Upcoming"))
                    StatusFlagID = 0;
                if (InputFilter.Equals("Overdue"))
                    StatusFlagID = 1;
                if (InputFilter.Equals("PendingForReview"))
                    StatusFlagID = 2;
                if (InputFilter.Equals("Rejected"))
                    StatusFlagID = 3;
                if (InputFilter.Equals("DueButNotSubmitted"))
                    StatusFlagID = 4;
            }
            else
            {
                StatusFlagID = -1;
            }


            if (AuthenticationHelper.Role == "EXCT")
            {
                DisableFalg = false;
                Falg = "PRA";
                if (Session["User_comp_Roles"] != null)
                {
                    roles = Session["User_comp_Roles"] as List<int>;
                }
                else
                {
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                    Session["User_comp_Roles"] = roles;
                }
                if (roles.Contains(4))
                {
                    RoleFlag = 1;
                }
            }
            else if (AuthenticationHelper.Role == "MGMT")
            {
                Falg = "MGMT";
            }
            else if (AuthenticationHelper.Role == "CADMN")
            {
                DisableFalg = false;
                Falg = "PRA";
                if (Session["User_comp_Roles"] != null)
                {
                    roles = Session["User_comp_Roles"] as List<int>;
                }
                else
                {
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                    Session["User_comp_Roles"] = roles;
                }
                if (roles.Contains(4))
                {
                    RoleFlag = 1;
                }
            }
        }
        protected void liAssignedEvents_Click(object sender, EventArgs e)
        {
         
            Response.Redirect("AssignedEventNew.aspx");
        }

        protected void liActiveEvents_Click(object sender, EventArgs e)
        {
            liActiveEvents.Attributes.Add("class", "active");
            liAssignedEvents.Attributes.Add("class", "");
        }
    }
}