﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ComplianceDashboard.aspx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.ComplianceDashboard" EnableEventValidation="false" %>

<%--<%@ Register Src="~/Controls/ApproverDashboard.ascx" TagName="ApproverDashboard" TagPrefix="vit" %>
<%@ Register Src="~/Controls/ApproverGradingReport.ascx" TagName="ApproverGrading" TagPrefix="vit" %>
<%@ Register Src="~/Controls/ApproverSummaryStatusReport.ascx" TagName="SummaryStatusReport" TagPrefix="vit" %>

<%@ Register Src="~/Controls/InternalApproverDashboard.ascx" TagName="InternalApproverDashboard" TagPrefix="vit" %>
<%@ Register Src="~/Controls/InternalApproverGradingReport.ascx" TagName="InternalApproverGrading" TagPrefix="vit" %>
<%@ Register Src="~/Controls/InternalApproverSummaryStatusReport.ascx" TagName="InternalSummaryStatusReport" TagPrefix="vit" %>
<%@ Register Src="~/Controls/ActiveEventDashboardsCA.ascx" TagPrefix="vit" TagName="ActiveEventDashboardsCA" %>
<%@ Register Src="~/Controls/EventDashboardsCA.ascx" TagPrefix="vit" TagName="EventDashboardsCA" %>--%>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Style/Dashboard.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        $(function () {
            initializeRadioButtonsList($("#<%= rblRole.ClientID %>"));
            initializeRadioButtonsList($("#<%= rdInternalaRoleList.ClientID %>"));
            initializeRadioButtonsList($("#<%= rdApproverReport.ClientID %>"));
            initializeRadioButtonsList($("#<%= rdApproverReportInternal.ClientID %>"));
        });

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <table width="100%" align="left">
        <tr>
            <td>
                <asp:Button Text="Statutory" Visible="false" BorderStyle="None" ID="Tab1" CssClass="Initial" runat="server"
                    OnClick="Tab1_Click" />
                <asp:Button Text="Internal"  BorderStyle="None" ID="Tab2" CssClass="Initial" runat="server" Visible="false"
                    OnClick="Tab2_Click" />
                <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table style="width: 100%; display:none; border-width: 1px; border-color: #79b7e7; border-style: solid; min-height: 500px;">
                            <tr>
                                <td valign="top">
                                    <div>
                                        <div style="margin: 10px 20px 10px 105px">
                                            <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>

                                            <div>
                                                <asp:RadioButtonList runat="server" ID="rblRole" RepeatDirection="Horizontal" RepeatLayout="Flow" Width="400px"
                                                    OnSelectedIndexChanged="rblRole_SelectedIndexChanged" Visible="false" AutoPostBack="true">
                                                </asp:RadioButtonList>
                                                <asp:RadioButtonList runat="server" ID="rdApproverReport" RepeatDirection="Horizontal" Visible="false" AutoPostBack="true" Style="margin-left: 50px"
                                                    RepeatLayout="Flow" OnSelectedIndexChanged="rdApproverReport_SelectedIndexChanged">
                                                    <asp:ListItem Text="Dashboard" Value="dashboard" Selected="True" />
                                                    <asp:ListItem Text="Grading Report" Value="gradingReport" />
                                                    <asp:ListItem Text="Summary Status Report" Value="summaryStatus" />
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <asp:UpdatePanel ID="upComplianceDashboard" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div style="margin: 10px 20px 10px 10px" id="divStatusTab" runat="server">

                                                    <div class="a-btn" id="divUpcomingCompliances" style="display:none;" runat="server">

                                                        <asp:UpdatePanel ID="rahul" runat="server">

                                                            <ContentTemplate>
                                                                <span>
                                                                    <asp:LinkButton CssClass="a-btn-text" Style="text-decoration: none" Text="Upcoming Compliances" runat="server" ID="LinkButton1" OnClick="btnUpcomingCompliances_Click" ToolTip="Click here to see Upcoming Compliances" /></span>
                                                                <span id="spUpcomingCompliances" class="a-btn-icon-right" runat="server">
                                                                    <asp:LinkButton Text="" Style="text-decoration: none" runat="server" ID="btnUpcomingCompliances" ForeColor="#996633" /></span>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="LinkButton1" />
                                                                <asp:PostBackTrigger ControlID="btnUpcomingCompliances" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="a-btn" id="divOverdueCompliances" style="display:none;" runat="server">
                                                        <span>
                                                            <asp:LinkButton class="a-btn-text" Text="Overdue Compliances" Style="text-decoration: none" runat="server" ID="LinkButton2" OnClick="btnPendingCompliances_Click" ToolTip="Click here to see Overdue Compliances" /></span>
                                                        <span class="a-btn-icon-right" id="spPendingCompliances" runat="server">
                                                            <asp:LinkButton Text="" Style="text-decoration: none" runat="server" ID="btnPendingCompliances" ForeColor="#996633" />
                                                        </span>
                                                    </div>
                                                    <div class="a-btn" id="divPendingForReview" style="display:none;" runat="server" visible="false">
                                                        <span>
                                                            <asp:LinkButton ID="LinkButton3" CssClass="a-btn-text" Style="text-decoration: none" Text="Pending for review" runat="server" OnClick="btnPendingForReview_Click" ToolTip="Click here to see Compliances pending for review" /></span>
                                                        <span id="Span1" class="a-btn-icon-right" runat="server">
                                                            <asp:LinkButton Text="" Style="text-decoration: none" runat="server" ID="btnPendingForReview" ForeColor="#996633" /></span>
                                                    </div>
                                                    <div class="a-btn" style="display:none;">
                                                        <span>
                                                            <asp:LinkButton class="a-btn-text" Style="text-decoration: none" Text="Event Compliances" runat="server" ID="LinkButton4" OnClick="btnPerformanceSummaryforPerformer_Click" ToolTip="Click here to see Performance Summary of Compliances" /></span>
                                                        <span class="a-btn-icon-right" id="spPerformanceSummary"></span>
                                                    </div>
                                                    <div class="a-btn" style="display:none;">
                                                        <span>
                                                            <asp:LinkButton Text="" class="a-btn-text" Style="text-decoration: none" runat="server" ID="btnPendingForApproverTitle" OnClick="btnPendingForApprovels_Click" ToolTip="" /></span>
                                                        <span class="a-btn-icon-right" id="spPendingforApprovels" runat="server">
                                                            <asp:LinkButton Text="" runat="server" Style="text-decoration: none" ID="btnPendingForApprovels" ForeColor="#996633" />
                                                        </span>
                                                    </div>
                                                    <div class="a-btn" style="display:none;">
                                                        <span>
                                                            <asp:LinkButton class="a-btn-text" Style="text-decoration: none" Text="Checklist" runat="server" ID="btnCheckList" OnClick="btnCheckList_Click" ToolTip="Click here to see checklist compliances." /></span>
                                                        <span class="a-btn-icon-right" id="spChecklistCompliances" runat="server">
                                                            <asp:LinkButton Text="" runat="server" Style="text-decoration: none" ID="btnChecklistCompliances" ForeColor="#996633" />
                                                        </span>
                                                    </div>
                                                  <%--  <div class="a-btn">
                                                        <span class="a-btn-text">Downloads</span>
                                                        </span>
                                                           <span class="a-btn-icon-right"></span>

                                                    </div>--%>
                                                    <div class="a-btn" style="display:none;">
                                                        <span>
                                                            <asp:LinkButton class="a-btn-text" Style="text-decoration: none" Text="Rejected" runat="server" ID="btnRejected" OnClick="btnRejected_Click" ToolTip="Click here to see Rejected compliances." /></span>
                                                        <span class="a-btn-icon-right" id="Span2" runat="server">
                                                            <asp:LinkButton Text="" runat="server" Style="text-decoration: none" ID="btnRejectedCompliances" ForeColor="#996633" />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div style="margin: 10px 20px 10px 10px; display:none;" id="divEventstatusTab" runat="server">
                                                    <div class="a-btn" id="div1" runat="server">
                                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                            <ContentTemplate>
                                                                <span>
                                                                    <asp:LinkButton CssClass="a-btn-text" Style="text-decoration: none" Text="Assigned Events" runat="server" ID="linkAssignEvents" OnClick="btnAssignedEvents_Click" ToolTip="Click here to see assigned events" /></span>
                                                                <span id="Span3" class="a-btn-icon-right" runat="server">
                                                                    <asp:LinkButton Text="" Style="text-decoration: none" runat="server" ID="btnAssignedEvents" ForeColor="#996633" /></span>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="linkAssignEvents" />
                                                                <asp:PostBackTrigger ControlID="btnAssignedEvents" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <div style="margin: 10px 20px 10px 10px; display:none;" id="divActiveEventstatusTab" runat="server">
                                                    <div class="a-btn" id="div3" runat="server">
                                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                            <ContentTemplate>
                                                                <span>
                                                                    <asp:LinkButton CssClass="a-btn-text" Style="text-decoration: none" Text="Activated Events" runat="server" ID="lnkActiveEvents" OnClick="btnActiveEvents_Click" ToolTip="Click here to see active events" /></span>
                                                                <span id="Span5" class="a-btn-icon-right" runat="server">
                                                                    <asp:LinkButton Text="" Style="text-decoration: none" runat="server" ID="btnActiveEvents" ForeColor="#996633" /></span>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="lnkActiveEvents" />
                                                                <asp:PostBackTrigger ControlID="btnActiveEvents" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                               <%-- <vit:ApproverDashboard ID="ucApproverDashboard" runat="server" Visible="false"></vit:ApproverDashboard>
                                                <vit:ApproverGrading ID="ucApproverGrading" runat="server" Visible="false"></vit:ApproverGrading>
                                                <vit:SummaryStatusReport ID="ucSummaryStatusReport" runat="server" Visible="false"></vit:SummaryStatusReport>--%>
                                               <%-- <vit:EventDashboards ID="ucEventDashboards" runat="server" Visible="false"></vit:EventDashboards>--%>
                                               <%-- <vit:ActiveEventDashboardsCA ID="ucActiveEventDashboardsCA" runat="server" Visible="false"></vit:ActiveEventDashboardsCA>--%>
                                               <%-- <vit:ActiveEventDashboardsCA runat="server" ID="ucActiveEventDashboards" Visible="false" />
                                                <vit:EventDashboardsCA runat="server" ID="ucEventDashboards" Visible="false"/>--%>
                                            </ContentTemplate>
                                           
                                        </asp:UpdatePanel>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table style="width: 100%; border-width: 1px; border-color: #79b7e7; border-style: solid; min-height: 500px;">
                            <tr>
                                <td valign="top">
                                    <div style="margin: 10px 20px 10px 90px">
                                        <asp:Label ID="lblErrorMessageInternal" runat="server" Style="color: Red"></asp:Label>

                                        <div>
                                            <asp:RadioButtonList runat="server" ID="rdInternalaRoleList" RepeatDirection="Horizontal" RepeatLayout="Flow" Width="400px"
                                                OnSelectedIndexChanged="rdInternalaRoleList_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:RadioButtonList>
                                            <asp:RadioButtonList runat="server" ID="rdApproverReportInternal" RepeatDirection="Horizontal" Visible="false" AutoPostBack="true" Style="margin-left: 50px"
                                                RepeatLayout="Flow" OnSelectedIndexChanged="rdApproverReportInternal_SelectedIndexChanged">
                                                <asp:ListItem Text="Dashboard" Value="dashboard" Selected="True" />
                                                <asp:ListItem Text="Grading Report" Value="gradingReport" />
                                                <asp:ListItem Text="Summary Status Report" Value="summaryStatus" />
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="margin: 10px 20px 10px 10px" id="divStatusTabI" runat="server">
                                                <div class="a-btn" id="divUpcomingCompliancesI" runat="server">
                                                    <span>
                                                        <asp:LinkButton CssClass="a-btn-text" Style="text-decoration: none" Text="Upcoming Compliances" runat="server" ID="LinkButton5" OnClick="btnInternalUpcomingCompliances_Click" ToolTip="Click here to see Upcoming internal Compliances" /></span>
                                                    <span id="spUpcomingCompliancesI" class="a-btn-icon-right" runat="server">
                                                        <asp:LinkButton Text="" Style="text-decoration: none" runat="server" ID="btnInternalUpcomingCompliances" ForeColor="#996633" /></span>
                                                </div>
                                                <div class="a-btn" id="divOverdueCompliancesI" runat="server">
                                                    <span>
                                                        <asp:LinkButton class="a-btn-text" Text="Overdue Compliances" Style="text-decoration: none" runat="server" ID="LinkButton7" OnClick="btnInternalPendingCompliances_Click" ToolTip="Click here to see Overdue Internal Compliances" /></span>
                                                    <span class="a-btn-icon-right" id="spPendingCompliancesI" runat="server">
                                                        <asp:LinkButton Text="" Style="text-decoration: none" runat="server" ID="btnInternalPendingCompliances" ForeColor="#996633" />
                                                    </span>
                                                </div>
                                                <div class="a-btn" id="divPendingForReviewI" runat="server" visible="false">
                                                    <span>
                                                        <asp:LinkButton ID="LinkButton9" CssClass="a-btn-text" Style="text-decoration: none" Text="Pending for review" runat="server" OnClick="btnInternalPendigForreview_Click" ToolTip="Click here to see Compliances pending for review" /></span>
                                                    <span id="Span4" class="a-btn-icon-right" runat="server">
                                                        <asp:LinkButton Text="" Style="text-decoration: none" runat="server" ID="btnInternalPendigForreview" ForeColor="#996633" /></span>
                                                </div>
                                                <div class="a-btn">
                                                    <span>
                                                        <asp:LinkButton class="a-btn-text" Style="text-decoration: none" Text="Performance Summary" runat="server" ID="btnInternalPerformanceSummary" OnClick="btnInternalPerformanceSummary_Click" ToolTip="Click here to see Performance Summary of Compliances" /></span>
                                                    <span class="a-btn-icon-right" id="spPerformanceSummaryI"></span>
                                                </div>
                                                <div class="a-btn">
                                                    <span>
                                                        <asp:LinkButton Text="" class="a-btn-text" Style="text-decoration: none" runat="server" ID="btnInternalPendingForApproverTitle" OnClick="btnInternalPendingForApproval_Click" ToolTip="" /></span>
                                                    <span class="a-btn-icon-right" id="spPendingforApprovelsI" runat="server">
                                                        <asp:LinkButton Text="" runat="server" Style="text-decoration: none" ID="btnInternalPendingForApprovels" ForeColor="#996633" />
                                                    </span>
                                                </div>

                                            </div>
                                           <%-- <vit:InternalApproverDashboard ID="UCApproverDashboardInternal" runat="server" Visible="false"></vit:InternalApproverDashboard>
                                            <vit:InternalApproverGrading ID="UCApproverGradingInternal" runat="server" Visible="false"></vit:InternalApproverGrading>
                                            <vit:InternalSummaryStatusReport ID="UCSummaryStatusReportInternal" runat="server" Visible="false"></vit:InternalSummaryStatusReport>--%>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </td>
        </tr>
    </table>
</asp:Content>
