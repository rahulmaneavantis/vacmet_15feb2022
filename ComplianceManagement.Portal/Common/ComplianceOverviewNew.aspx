﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComplianceOverviewNew.aspx.cs" EnableEventValidation="false"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Common.ComplianceOverviewNew" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">
    <title></title>

    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
  <%--  <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css"/>--%>
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <!-- Bootstrap CSS -->
  <%--  <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />--%>
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

     <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
      
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>

    <script src="/Newjs/bootstrap-tagsinput.js"></script>
    <link href="/NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />

    <link href="/NewCSS/timeline.css" rel="stylesheet" />
    <!-- nice scroll -->
    <script type="text/javascript" src="/Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="/Newjs/jquery.nicescroll.js"></script>

    <script src="/Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="/NewCSS/tag-scrolling.css" rel="stylesheet" />

    <link href="../NewCSS/Newbootstrap.min.css" rel="stylesheet" />
<%--<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">--%>

    <script src="../Newjs/jquery.mentionable.js"></script>
    <link href="../NewCSS/jquery.mentionable.css" rel="stylesheet" />

    <style type="text/css">
        .k-grid-header-wrap.k-auto-scrollable {
            width: 99.9%;
        }

        table.k-selectable {
            border-right: 1px solid #ceced2;
        }
        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            /*margin-top: 5px;*/
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        /*.col-md-2 {
            width: 20%;
        }*/

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }

        .k-grid-content {
            min-height: 152px !important;
            /*min-height:200px !important;*/
        }
    </style>

    <%-- <style>
        th, td, p, input {
            font:14px Verdana;
        }
        table, th, td 
        {
            border: solid 1px #DDD;
            border-collapse: collapse;
            padding: 2px 3px;
            text-align: center;
        }
        th {
            font-weight:bold;
        }
    </style>--%>

    <style type="text/css">
        .bootstrap-tagsinput .tag [data-role="remove"]:after {
            content: "";
            padding: 0px 2px;
        }

        .bootstrap-tagsinput {
            /*border: none;
            box-shadow: none;*/
        }

        .form-group {
            margin-bottom: 10px;
        }

        .btn-group {
            width: 100%
        }
    </style>

    <script type="text/javascript">
        
        function UpdatedrebindTypes() {

            var gridUpdate = $("#gridUpdate").kendoGrid({
                dataSource: {
                    transport: {
                        read: '<% =KendoPath%>/Data/KendoComplianceOverviewUpdates?UserId=<% =UId%>&CustId=<% =CustId%>&complianceInstanceID=<% =compInstanceID%>'
                    },
                    pageSize: 10,
                },
                //height: 150,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                columns: [
                    {
                        field: "Title", title: 'Title',
                        width: "70%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }
                    },
                    {
                        field: "Date", title: 'Date',
                        type: "date",
                        template: "#= kendo.toString(kendo.parseDate(Date, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",

                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"

                                }
                            }
                        }
                    },
                    {
                        command: [
                            { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                        ], title: "Action", lock: true,// width: 150,

                    }
                ]
            });

             $("#gridUpdate").kendoTooltip({
                    filter: "td:nth-child(1)", //this filter selects the second column's cells
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        return content;
                    }
                }).data("kendoTooltip");

                $("#gridUpdate").kendoTooltip({
                    filter: "td:nth-child(2)", //this filter selects the second column's cells
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        return content;
                    }
                }).data("kendoTooltip");



            var gridHistoricaldocuments = $("#gridHistoricaldocuments").kendoGrid({
                dataSource: {
                    transport: {
                        read: '<% =KendoPath%>/Data/KendoComplianceDocuments?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=All&ScheduledOnID=<% =SOnID%>&complianceInstanceID=<% =compInstanceID%>'
                    },
                    pageSize: 10,
                },
                //height: 500,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                columns: [
                    {
                        field: "FileName", title: 'File Name',
                        width: "65%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }
                    }, {
                        field: "VersionDate", title: 'Version Date',
                        type: "date",
                        template: "#= kendo.toString(kendo.parseDate(VersionDate, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        width: "18%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                            { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                        ], title: "Action", lock: true,// width: 150,

                    }
                ]
            });

              $("#gridHistoricaldocuments").kendoTooltip({
                    filter: "td:nth-child(1)", //this filter selects the second column's cells
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        return content;
                    }
            }).data("kendoTooltip");
              $("#gridHistoricaldocuments").kendoTooltip({
                    filter: "td:nth-child(2)", //this filter selects the second column's cells
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        return content;
                    }
                }).data("kendoTooltip");

            var gridDocuments = $("#gridDocuments").kendoGrid({
                dataSource: {
                    transport: {
                        read: '<% =KendoPath%>/Data/KendoComplianceDocuments?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=Current&ScheduledOnID=<% =SOnID%>&complianceInstanceID=<% =compInstanceID%>'
                    },
                    pageSize: 5,
                },
                height: 228,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                columns: [
                    {
                        field: "FileName", title: 'File Name',
                        width: "40%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }
                    }, {
                        field: "VersionDate", title: 'Version Date',
                        type: "date",
                        template: "#= kendo.toString(kendo.parseDate(VersionDate, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        width: "34%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                            { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                        ], title: "Action", lock: true,// width: 150,

                    }
                ]
            });

            $("#gridDocuments").kendoTooltip({
                filter: "td:nth-child(1)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
                 $("#gridDocuments").kendoTooltip({
                filter: "td:nth-child(2)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            GetCommentDetails();


            function createChart() {

                $("#chart").kendoChart({
                    title: {
                        text: "Compliance Work Status"
                    },
                    legend: {
                        position: "top"
                    },
                    seriesDefaults: {
                        type: "column"
                    },
                    series: [{

                        name: "No Of Days",
                        data: [<% =NumberofDays%>],
                        color: function (point) {
                            if (point.value > 0) {
                                return " #fe0000";
                            } else {
                                return "#30ab14";
                            }
                        }
                    }],
                    valueAxis: {
                        labels: {
                            format: "{0}"
                        },
                        line: {
                            visible: false
                        },

                        axisCrossingValue: 0
                    },
                    categoryAxis: {
                        majorGridLines: {
                            visible: false
                        },
                        categories:<% =NumberofMonths%>,                      
                        line: {
                            visible: false
                        },
                        labels: {
                            padding: { top: 135 }
                        }
                    },
                    tooltip: {
                        visible: true,
                        format: "{0}",
                        template: "#= series.name #: #= value #"
                    },
                    seriesClick: function (e) {
                        //alert(e.value);
                    }
                });
            }

            $(document).ready(createChart);
            $(document).bind("kendo:skinChange", createChart);
        }

        function BindDateControls() {

            $("#txtFromDate_AuditLog").kendoDatePicker({
            });
            $("#txtToDate_AuditLog").kendoDatePicker({
            });


            //      var startDate = new Date();
            //      $(function () {
            //          $('input[id*=txtFromDate_AuditLog]').datepicker({
            //              dateFormat: 'dd-mm-yy',
            //              numberOfMonths: 1,
            //              changeMonth: true,
            //              changeYear: true,
            //          });


            //$('input[id*=txtToDate_AuditLog]').datepicker({
            //              dateFormat: 'dd-mm-yy',
            //              numberOfMonths: 1,
            //              changeMonth: true,
            //              changeYear: true,
            //          });
            //      });

        }


        function showHideAuditLog(divID, iID) {

            if ($(iID).attr('class').indexOf('fa fa-plus') > -1) {
                $(iID).attr("class", "fa fa-minus");
                $(divID).collapse('toggle');
            } else if ($(iID).attr('class').indexOf('fa fa-minus') > -1) {
                $(iID).attr("class", "fa fa-plus");
                $(divID).collapse('toggle');
            }
        }




        function GetCommentDetails() {

            $.ajax({
                type: "GET",
                url: '<% =KendoPath%>/data/GetCommentdata?searchKey=' + $(hdnComplianceId).val()+ '&CustomerID=<% =CustId%>',
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                crossDomain: true,
                success: function (result) {
                    debugger;

                    var MainCommnetId = new Array();
                    for (var i = 0; i < result.length; i++) {
                        if (result[i].message_id == 0) {
                            MainCommnetId.push([result[i].compliance_id, result[i].comment, result[i].user_id, result[i].created_at, result[i]._id, result[i].user_name]);
                        }
                    }

                    var customers = new Array();
                    for (var i = 0; i < MainCommnetId.length; i++) {
                        customers.push('<div style="font-family: Roboto;float: left;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 234px;border-radius: 6px;">' + MainCommnetId[i][1] + '</div></br > <div style="float: left;font-size: 10px;">' + MainCommnetId[i][5] + ',' + new Date(new Date(MainCommnetId[i][3])).toLocaleString()
                            + '<a style="font-size: 12px;margin-left: 8px;color:blue;" onclick=showDiv("' + MainCommnetId[i][4] + '");>Comments</a></div></br><div style="display:none;" id=' + MainCommnetId[i][4] + '_div><input type="text" name=txtid id=' + MainCommnetId[i][4] + '_txt></br>&nbsp;&nbsp;&nbsp;<a onclick=submitDiv("' + MainCommnetId[i][4] + '");>submit</a>&nbsp;&nbsp;&nbsp;<a onclick=CloseDiv("' + MainCommnetId[i][4] + '");>Closed</a></div></br>');

                        for (var k = 0; k < result.length; k++) {
                            if (result[k].message_id == MainCommnetId[i][4]) {
                                customers.push('<div style="font-family: Roboto;background: #eaeaea;float: right;padding: 4px 10px 4px 5px;border: 1px solid #e4d8d8;width: auto;min-width: 234px;border-radius: 6px;">' + result[k].comment + '</div></br><div style="float: right;font-size: 10px;">' + result[k].user_name + ',' + new Date(new Date(result[k].created_at)).toLocaleString() + '</div></br>');
                            }
                        }
                    }
                    $("#ShowDatadiv").load();
                    $('#ShowDatadiv').html('');
                    $('#ShowDatadiv').html('<div style="min-height:45px;padding-left: 10px;padding-top: 10px;">' + customers.join('</div><div style="padding-right: 10px;padding-bottom: 18px;">') + '</div>');

                },
                error: function (response) {

                }
            });

        }

        function showDiv(pageid) {
           
            document.getElementById(pageid + "_div").style.display = "block";
            document.getElementById(pageid + "_txt").value = "";
        }

        function submitData() {
            debugger;
            var SelectedValue = new Array();
            var inps = document.getElementsByName('mentioned_id[]');
            for (var i = 0; i < inps.length; i++) {
                if (inps[i].disabled != true) {
                    var inp = inps[i];
                    SelectedValue.push(inp.value);
                }
            }

            var commet = document.getElementById("TxtComments").value;
            var CompId = $('#hdnComplianceId').val();

            $.ajax({
                type: "GET",

                url: '<% =KendoPath%>/data/PostCommentdata?Uids=' + SelectedValue + '&Comments=' + commet + '&UserId=<% =UId%>&CustId=<% =CustId%>&type=C&CompId=' + CompId,
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                crossDomain: true,
                success: function (result) {
                    GetCommentDetails();
                    $('#TxtComments').val('');
                    setTimeout(function () {
                        GetCommentDetails() // this will run after every 5 seconds
                    }, 1000);             
                },
                error: function (response) {
                    
                }
            });

        }

        function submitDiv(pageid) {
            var valuetext = document.getElementById(pageid + "_txt").value;
            var CompId = $('#hdnComplianceId').val();

            $.ajax({
                type: "GET",
                url: '<% =KendoPath%>/Data/PostSubCommentdata?MsgID=' + pageid + '&Comments=' + valuetext + '&UserId=<% =UId%>&CustId=<% =CustId%>&type=C&CompId=' + CompId,
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                crossDomain: true,
                success: function (result) {
                 setTimeout(function () {
                        GetCommentDetails() // this will run after every 5 seconds
                    }, 1000);      
                },
                error: function (response) {
                 
                }
            });

        }
        function CloseDiv(pageid) {

            document.getElementById(pageid + "_div").style.display = "none";
        }

        $(document).ready(function () {

            BindDateControls();
            BindDocuments();
            DocumentUpdates();
            historicalDocuments();
           
             GetCommentDetails();
             //var myVar = setInterval(function () {
             //           GetCommentDetails() // this will run after every 5 seconds
             //       }, 5000);

            var myWindowAdv = $("#ViewUpdateDetails");
            function onClose() {
            }
            myWindowAdv.kendoWindow({
                width: "45%",
                height: "45%",
                title: "Legal Updates",
                visible: false,
                actions: [
                    "Pin",
                    "Minimize",
                    "Maximize",
                    "Close"
                ],
                close: onClose
            });

            //$("html").getNiceScroll().resize();

            function BindDocuments() {

                var gridDocuments = $("#gridDocuments").kendoGrid({
                    dataSource: {
                        transport: {
                            read: '<% =KendoPath%>/Data/KendoComplianceDocuments?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=Current&ScheduledOnID=<% =SOnID%>&complianceInstanceID=<% =compInstanceID%>'
                        },
                        pageSize: 3,
                    },
                    height: 228,
                    sortable: true,
                    filterable: true,
                    columnMenu: true,
                    pageable: true,
                    reorderable: true,
                    resizable: true,
                    multi: true,
                    columns: [
                        {
                            field: "FileName", title: 'File Name',
                            width: "40%;",
                            attributes: {
                                style: 'white-space: nowrap;'
                            }
                        }, {
                            field: "VersionDate", title: 'Version Date',
                            type: "date",
                            template: "#= kendo.toString(kendo.parseDate(VersionDate, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                            width: "34%;",
                            attributes: {
                                style: 'white-space: nowrap;'
                            }, filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            command: [
                                { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                                { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                            ], title: "Action", lock: true,// width: 150,

                        }
                    ]
                });

                $("#gridDocuments").kendoTooltip({
                    filter: "td:nth-child(1)", //this filter selects the second column's cells
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        return content;
                    }
                }).data("kendoTooltip");
                 $("#gridDocuments").kendoTooltip({
                    filter: "td:nth-child(2)", //this filter selects the second column's cells
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        return content;
                    }
                }).data("kendoTooltip");

            }
            $(document).on("click", "#gridDocuments tbody tr .ob-overview", function (e) {
                var item = $("#gridDocuments").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenDocumentOverviewpup(item.ScheduledOnID, item.TransactionID, item.FileID)
                return true;
            });
            $(document).on("click", "#gridDocuments tbody tr .ob-download", function (e) {
                var item = $("#gridDocuments").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenDocumentDownload(item.ScheduledOnID, item.Version, item.FileID)
                return true;
            });



            function historicalDocuments() {
                var gridHistoricaldocuments = $("#gridHistoricaldocuments").kendoGrid({
                    dataSource: {
                        transport: {
                            read: '<% =KendoPath%>/Data/KendoComplianceDocuments?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=All&ScheduledOnID=<% =SOnID%>&complianceInstanceID=<% =compInstanceID%>'
                        },
                        pageSize: 10,
                    },
                    //height: 500,
                    sortable: true,
                    filterable: true,
                    columnMenu: true,
                    pageable: true,
                    reorderable: true,
                    resizable: true,
                    multi: true,
                    columns: [
                        {
                            field: "FileName", title: 'File Name',
                            width: "65%;",
                            attributes: {
                                style: 'white-space: nowrap;'
                            }
                        }, {
                            field: "VersionDate", title: 'Version Date',
                            type: "date",
                            template: "#= kendo.toString(kendo.parseDate(VersionDate, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                            width: "18%;",
                            attributes: {
                                style: 'white-space: nowrap;'
                            }, filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            command: [
                                { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                                { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                            ], title: "Action", lock: true,// width: 150,

                        }
                    ]
                });

                $("#gridHistoricaldocuments").kendoTooltip({
                    filter: "td:nth-child(1)", //this filter selects the second column's cells
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        return content;
                    }
                }).data("kendoTooltip");
                 $("#gridHistoricaldocuments").kendoTooltip({
                    filter: "td:nth-child(2)", //this filter selects the second column's cells
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        return content;
                    }
                }).data("kendoTooltip");
            }

            $(document).on("click", "#gridHistoricaldocuments tbody tr .ob-overview", function (e) {
                var item = $("#gridHistoricaldocuments").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenDocumentOverviewpup(item.ScheduledOnID, item.TransactionID, item.FileID)
                return true;
            });

            function OpenDocumentOverviewpup(scheduledonid, transactionid,FileID) {
                $('#divOverView').modal('show');
                $('#OverViews').attr('width', '1150px');
                $('#OverViews').attr('height', '600px');
                $('.modal-dialog').css('width', '1200px');
                $('#OverViews').attr('src', "../Common/DocumentComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceTransactionID=" + transactionid+ "&FileID=" + FileID);
                $('#docViewerReviewAll').attr('src', "../docviewer.aspx?docurl=" + file);
            }

            $(document).on("click", "#gridHistoricaldocuments tbody tr .ob-download", function (e) {
                var item = $("#gridHistoricaldocuments").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenDocumentDownload(item.ScheduledOnID, item.Version, item.FileID)
                return true;
            });

            function OpenDocumentDownload(scheduledonid, Version, FileID) {
                $('#downloadfile').attr('src', "../Common/DownloadDocumentComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&FileID=" + FileID + "&Version=" + Version);
                return false;
            }

            function CloseClearOV() {
                $('#OverViews').attr('src', "../Common/blank.html");
            }


            function DocumentUpdates() {
                var gridUpdate = $("#gridUpdate").kendoGrid({
                    dataSource: {
                        transport: {
                            read: '<% =KendoPath%>/Data/KendoComplianceOverviewUpdates?UserId=<% =UId%>&CustId=<% =CustId%>&complianceInstanceID=<% =compInstanceID%>'
                        },
                        pageSize: 10,
                    },
                    //height: 150,
                    sortable: true,
                    filterable: true,
                    columnMenu: true,
                    pageable: true,
                    reorderable: true,
                    resizable: true,
                    multi: true,
                    columns: [
                        {
                            field: "Title", title: 'Title',
                            width: "70%;",
                            attributes: {
                                style: 'white-space: nowrap;'

                            }
                        },
                        {
                            field: "Date", title: 'Date',
                            type: "date",
                            template: "#= kendo.toString(kendo.parseDate(Date, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",

                            attributes: {
                                style: 'white-space: nowrap;'

                            }, filterable: {
                                extra: false,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"

                                    }
                                }
                            }
                        },
                        {
                            command: [
                                { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                            ], title: "Action", lock: true,// width: 150,

                        }
                    ]
                });

                $("#gridUpdate").kendoTooltip({
                    filter: "td:nth-child(1)", //this filter selects the second column's cells
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        return content;
                    }
                }).data("kendoTooltip");

                $("#gridUpdate").kendoTooltip({
                    filter: "td:nth-child(2)", //this filter selects the second column's cells
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        return content;
                    }
                }).data("kendoTooltip");
            }
        });

        $(document).on("click", "#gridUpdate tbody tr .ob-overview", function (e) {

            var item = $("#gridUpdate").data("kendoGrid").dataItem($(this).closest("tr"));
            OpendocumentsUpdates(item.Title);

            return true;
        });


        function OpendocumentsUpdates(title) {

            document.getElementById('detailUpdate').innerHTML = title;// 'your tip has been submitted!';


            var myWindowAdv = $("#ViewUpdateDetails");

            function onClose() {

            }

            myWindowAdv.kendoWindow({
                width: "85%",
                height: "825%",
                title: "Legal Updates",
                visible: false,
                actions: [
                    //"Pin",
                    //"Minimize",
                    "Maximize",
                    "Close"
                ],

                close: onClose
            });

            //$("#divAdvanceSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

            myWindowAdv.data("kendoWindow").center().open();
            //e.preventDefault();
            return false;
        }


        function fopendocfileReview(file) {

            $('#DocumentReviewPopUp1').modal('show');
            $('#docViewerReviewAll').attr('src', "../docviewer.aspx?docurl=" + file);
        }
        function fopendocfileReviewPopUp() {
            $('#DocumentReviewPopUp1').modal('show');
        }
        $(document).ready(function () {
            $("button[data-dismiss-modal=modal1]").click(function () {
                $('#DocumentReviewPopUp1').modal('hide');
            });

        });

        function fredv(type) {

            if (type == "Compliance") {

                $('#licomplianceoverview').css('background-color', '#1fd9e1');
                $('#licomplianceoverview').css('color', 'white');
                $('#licomplianceoverview').css('border', '1px solid #1fd9e1');

                $('#lidocuments').css('background-color', '');
                $('#lidocuments').css('color', 'black');
                $('#lidocuments').css('border', 'none');

                $('#liAudit').css('background-color', '');
                $('#liAudit').css('color', 'black');
                $('#liAudit').css('border', 'none');

                $('#liUpdates').css('background-color', '');
                $('#liUpdates').css('color', 'black');
                $('#liUpdates').css('border', 'none');

                $('#liAuditlog').css('background-color', '');
                $('#liAuditlog').css('color', 'black');
                $('#liAuditlog').css('border', 'none');





                $('#licomplianceoverview1').addClass('active');
                $('#lidocuments1').removeClass('active');
                $('#documents').removeClass('active');
                $('#liAudit1').removeClass('active');
                $('#audits').removeClass('active');
                $('#complianceoverview').addClass('active');
                $('#updateView').removeClass('active');
                $('#liUpdates1').removeClass('active');
                $('#auditlogView').removeClass('active');
                $('#liAuditlog1').removeClass('active');

            } else if (type == "Document") {

                $('#licomplianceoverview').css('background-color', '');
                $('#licomplianceoverview').css('color', 'black');
                $('#licomplianceoverview').css('border', 'none');

                $('#lidocuments').css('background-color', '#1fd9e1');
                $('#lidocuments').css('color', 'white');
                $('#lidocuments').css('border', '1px solid #1fd9e1');

                $('#liAudit').css('background-color', '');
                $('#liAudit').css('color', 'black');
                $('#liAudit').css('border', 'none');

                $('#liUpdates').css('background-color', '');
                $('#liUpdates').css('color', 'black');
                $('#liUpdates').css('border', 'none');

                $('#liAuditlog').css('background-color', '');
                $('#liAuditlog').css('color', 'black');
                $('#liAuditlog').css('border', 'none');



                $('#lidocuments1').addClass('active');
                $('#licomplianceoverview1').removeClass('active');
                $('#complianceoverview').removeClass('active');
                $('#liAudit1').removeClass('active');
                $('#audits').removeClass('active');
                $('#updateView').removeClass('active');
                $('#liUpdates1').removeClass('active');
                $('#auditlogView').removeClass('active');
                $('#liAuditlog1').removeClass('active');
                $('#documents').addClass('active');

            }
            else if (type == "Audit") {

                $('#licomplianceoverview').css('background-color', '');
                $('#licomplianceoverview').css('color', 'black');
                $('#licomplianceoverview').css('border', 'none');

                $('#lidocuments').css('background-color', '');
                $('#lidocuments').css('color', 'black');
                $('#lidocuments').css('border', 'none');

                $('#liAudit').css('background-color', '#1fd9e1');
                $('#liAudit').css('color', 'white');
                $('#liAudit').css('border', '1px solid #1fd9e1');

                $('#liUpdates').css('background-color', '');
                $('#liUpdates').css('color', 'black');
                $('#liUpdates').css('border', 'none');

                $('#liAuditlog').css('background-color', '');
                $('#liAuditlog').css('color', 'black');
                $('#liAuditlog').css('border', 'none');

                $('#liAudit1').addClass('active');
                $('#licomplianceoverview1').removeClass('active');
                $('#complianceoverview').removeClass('active');
                $('#lidocuments1').removeClass('active');
                $('#documents').removeClass('active');
                $('#updateView').removeClass('active');
                $('#liUpdates1').removeClass('active');
                $('#auditlogView').removeClass('active');
                $('#liAuditlog1').removeClass('active');
                $('#audits').addClass('active');

            }

            else if (type == "updates") {

                $('#licomplianceoverview').css('background-color', '');
                $('#licomplianceoverview').css('color', 'black');
                 $('#licomplianceoverview').css('border', 'none');

                $('#lidocuments').css('background-color', '');
                $('#lidocuments').css('color', 'black');
                 $('#lidocuments').css('border', 'none');

                $('#liAudit').css('background-color', '');
                $('#liAudit').css('color', 'black');
                 $('#liAudit').css('border', 'none');

                $('#liUpdates').css('background-color', '#1fd9e1');
                $('#liUpdates').css('color', 'white');
                $('#liUpdates').css('border', '1px solid #1fd9e1');

                $('#liAuditlog').css('background-color', '');
                $('#liAuditlog').css('color', 'black');
                 $('#liAuditlog').css('border', 'none');


                $('#liUpdates1').addClass('active');
                $('#licomplianceoverview1').removeClass('active');
                $('#complianceoverview').removeClass('active');
                $('#lidocuments1').removeClass('active');
                $('#documents').removeClass('active');

                $('#updateView').addClass('active');

                $('#liAudit1').removeClass('active');
                $('#audits').removeClass('active');

                $('#auditlogView').removeClass('active');
                $('#liAuditlog1').removeClass('active');
            }
            else if (type == "Auditlog") {
                $('#licomplianceoverview').css('background-color', '');
                $('#licomplianceoverview').css('color', 'black');
                $('#licomplianceoverview').css('border', 'none');

                $('#lidocuments').css('background-color', '');
                $('#lidocuments').css('color', 'black');
                $('#lidocuments').css('border', 'none');

                $('#liAudit').css('background-color', '');
                $('#liAudit').css('color', 'black');
                $('#liAudit').css('border', 'none');


                $('#liUpdates').css('background-color', '');
                $('#liUpdates').css('color', 'black');
                $('#liUpdates').css('border', 'none');

                $('#liAuditlog').css('background-color', '#1fd9e1');
                $('#liAuditlog').css('color', 'white');
                $('#liAuditlog').css('border', '1px solid #1fd9e1');



                $('#liAuditlog1').addClass('active');
                $('#licomplianceoverview1').removeClass('active');
                $('#complianceoverview').removeClass('active');
                $('#lidocuments1').removeClass('active');
                $('#documents').removeClass('active');

                $('#auditlogView').addClass('active');

                $('#audits').removeClass('active');
                $('#liAudit1').removeClass('active');

                $('#updateView').removeClass('active');
                $('#liUpdates1').removeClass('active');

            }
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }
        function fshowmore() {
            if ($('#moreItems').is(':visible')) {
                $('#moreItems').hide();
                $('#btnshowmore').text('Show more...');
            } else {
                $('#moreItems').show();
                $('#btnshowmore').text('Show less');
            }
        }
    </script>

    <style type="text/css">
        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: #1fd9e1;
            background-color: #f7f7f7;
        }

        .panel-heading .nav > li > a {
            font-size: 20px !important;
        }

        .panel-heading .nav > li > a {
            border-bottom: 0px;
        }

        .customDropDownCheckBoxCSS {
            height: 32px !important;
            width: 70%;
        }

        .chosen-single {
            color: #8e8e93;
        }

        .container {
            max-width: 100%;
        }

        ul.multiselect-container.dropdown-menu {
            width: 100%;
            height: 100px;
            overflow-y: auto;
        }

        button.multiselect.dropdown-toggle.btn.btn-default {
            text-align: left;
        }

        span.multiselect-selected-text {
            float: left;
            color: #444;
            font-family: 'Roboto', sans-serif !important;
        }

        b.caret {
            float: right;
            margin-top: 8px;
        }

        label {
            font-weight: 500;
            color: #666;
        }

        .fixed {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
        }
    </style>

    <style type="text/css">
        tr.spaceUnder > td {
            padding-top: 1em;
        }

        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .circle {
            width: 24px;
            height: 24px;
            border-radius: 59%;
            display: inline-block;
            margin-left: 8px;
        }

        .RiskControl {
            border: 1px solid #e4d8d8;
            border-left: 5px solid;
            border-radius: 6px;
            width: 141px;
            height: 33px;
        }

        .StatusControl {
            border: 1px solid #e4d8d8;
            border-left: 5px solid;
            border-radius: 6px;
            width:auto;
            /*width: 174px;*/
            height: 33px;
        }

        .DueDateControl {
            border: 1px solid #e4d8d8;
            /*border-left: 5px solid;*/
            border-radius: 6px;
            height: 33px;
            width: 184px;
        }

        .DetailHeader {
            background: white;
            float: left;
            color: black;
            font-size: 14px;
            font-weight: bold;
        }

        .dataDetail {
            width: 300px;
            font-size: 13px;
            color: #333;
        }

        .SummaryHeader {
            width: 20%;
            color: black;
            font-size: 14px;
            font-weight: bold;
        }

        .SummaryData {
            width: 80%;
            font-size: 13px;
            color: #333;
        }
    </style>


    <script type="text/javascript">
        function createChart() {

            $("#chart").kendoChart({
                title: {
                    text: "Compliance Work Status"
                },
                legend: {
                    position: "top"
                },
                seriesDefaults: {
                    type: "column"
                },
                series: [{

                    name: "No Of Days",
                    data: [<% =NumberofDays%>],
                    //data: [-9,-30,-13,-28,-29,21,29,1,10,-26,8,-7],
                    color: function (point) {
                        if (point.value > 0) {
                            return " #fe0000";
                        } else {
                            return "#30ab14";
                        }
                    }
                }],
                valueAxis: {
                    labels: {
                        format: "{0}"
                    },
                    line: {
                        visible: false
                    },

                    axisCrossingValue: 0
                },
                categoryAxis: {
                    majorGridLines: {
                        visible: false
                    },
                    categories:<% =NumberofMonths%>,
                    line: {
                        visible: false
                    },
                    labels: {
                        padding: { top: 135 }
                    }
                },
                tooltip: {
                    visible: true,
                    format: "{0}",
                    template: "#= series.name #: #= value #"
                    //template: "#= series.name #: #= value # ,#= series.name #: #= value # "
                },
                seriesClick: function (e) {
                    //alert(e.value);
                }
            });
        }

        $(document).ready(createChart);
        $(document).bind("kendo:skinChange", createChart);
    </script>


</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional">
            <%--OnLoad="upDivLocation_Load"--%>
            <ContentTemplate>
                <asp:UpdateProgress ID="updateProgress" runat="server">
                    <ProgressTemplate>
                        <div id="updateProgress1" style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                                AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <div style="padding-left: 14px;">
                <div style="background-color: #f8f8f8; height: 30px; padding: 6px;">
                    <asp:Label ID="lblComplianceDiscription" Style="padding-left: 7px; width: 300px; font-size: 16px; color: #333;"
                        maximunsize="300px" autosize="true" runat="server" />
                </div>
                    </div>
                <div class="col-md-12" style="background-color: white; padding-top: 9px; color: black;">
                    <div id="RiskColor" runat="server" class="col-md-2 col-sm-12 col-xs-12 RiskControl">
                        <div style="padding-top: 5px;">
                            Risk :                      
                    <asp:Label ID="lblRisk" Style="width: 300px; font-size: 13px;"
                        maximunsize="300px" autosize="true" runat="server" />
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-12 col-xs-12" style="width: 1px;">
                    </div>
                    <div class="col-md-2 col-sm-12 col-xs-12 DueDateControl">
                        <div style="padding-top: 5px;">
                            Due Date :
                    <asp:Label ID="lblDueDate" Style="width: 300px; font-size: 13px; color: #333;"
                        maximunsize="300px" autosize="true" runat="server" />
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-12 col-xs-12" style="width: 1px;">
                    </div>
                    <div id="StatusColor" runat="server" class="col-md-2 col-sm-12 col-xs-12 StatusControl">
                        <div style="padding-top: 5px;">
                            status :
                        <asp:Label ID="lblStatus" runat="server" />
                        </div>
                    </div>
                </div>


                <div style="background-color: white; padding-top: 16px;">
                    <div style="display: none;">
                        <div id="divRiskType" runat="server" class="circle"></div>
                    </div>
                    <asp:Label runat="server" Style="display: none; margin-top: -2PX; margin-left: 5px; font-size: 17px; position: absolute; color: #333;">Status - </asp:Label>
                </div>
                <%-- </div>--%>

                <div class="col-lg-8 col-md-8 " style="padding-left: 16px; background: white; padding-top: 9px; padding-right: 25px;">
                    <ul class="nav nav-tabs calender-li" role="tablist" style="background: white;">
                        <li id="licomplianceoverview1" style="cursor: pointer;" onclick="fredv('Compliance');" runat="server"><a id="licomplianceoverview" runat="server" aria-controls="complianceoverview" role="tab" style="background-color: #1fd9e1; color: white;" data-toggle="tab">Summary</a></li>
                        <li id="lidocuments1" runat="server" style="cursor: pointer;" onclick="fredv('Document');"><a id="lidocuments" runat="server" aria-controls="documents" role="tab" data-toggle="tab">Details</a></li>
                        <li id="liAudit1" runat="server" style="cursor: pointer;" onclick="fredv('Audit');"><a id="liAudit" runat="server" aria-controls="audits" role="tab" data-toggle="tab">Historical Documents</a></li>
                        <li id="liUpdates1" runat="server" style="cursor: pointer;" onclick="fredv('updates');"><a id="liUpdates" runat="server" aria-controls="updateView" role="tab" data-toggle="tab">Updates</a></li>
                        <li id="liAuditlog1" runat="server" style="cursor: pointer;" onclick="fredv('Auditlog');"><a id="liAuditlog" runat="server" aria-controls="auditlogView" role="tab" data-toggle="tab">Audit logs</a></li>
                    </ul>
                    <div class="tab-content" style="padding-top: 2px; border: 1px solid #1fd9e1; min-height: 446px; max-height: 446px; overflow-y: auto;">

                        <div role="tabpanel" class="tab-pane" runat="server" id="complianceoverview">
                            <div class="tabbable-panel">
                                <div class="tabbable-line">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%; background: white; color: black;padding-left: 0px;font-family: Roboto;">

                                        <div class="col-lg-12 col-md-12 col-sm-12" style="padding-top: 10px;padding-left: 0px;">
                                            <div class="col-lg-2 col-md-2 col-sm-2 SummaryHeader">
                                                Act :
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-10 SummaryData">
                                                <asp:Label ID="lblActName" Style="width: 100%; font-size: 13px; color: #333;"
                                                    autosize="true" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12" style="padding-top: 10px;padding-left: 0px;">
                                            <div class="col-lg-2 col-md-2 col-sm-2 SummaryHeader">
                                                Section /Rule :
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-10 SummaryData">
                                                <asp:Label ID="lblRule" Style="width: 88%; font-size: 13px; color: #333;"
                                                    autosize="true" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12" style="padding-top: 10px;padding-left: 0px;">
                                            <div class="col-lg-2 col-md-2 col-sm-2 SummaryHeader">
                                                Frequency :
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-10 SummaryData">
                                                <asp:Label ID="lblFrequency" Style="width: 300px; font-size: 13px; color: #333;"
                                                    maximunsize="300px" autosize="true" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12" style="padding-top: 10px;padding-left: 0px;">
                                            <div class="col-lg-2 col-md-2 col-sm-2 SummaryHeader">
                                                Location :
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-10 SummaryData">
                                                <asp:Label ID="lblLocation" Style="width: 300px; font-size: 13px; color: #333;"
                                                    maximunsize="300px" autosize="true" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12" style="padding-top: 10px;padding-left: 0px;">
                                            <div class="col-lg-2 col-md-2 col-sm-2 SummaryHeader">
                                                Period :
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-10 SummaryData">
                                                <asp:Label ID="lblPeriod" Style="width: 300px; font-size: 13px; color: #333;"
                                                    maximunsize="300px" autosize="true" runat="server" />
                                            </div>
                                        </div>

                                        <%--<div class="col-lg-5 col-md-5colpadding0 fixwidth" style="width: 50%; background: white; float: left;">
                                          
                                        </div>--%>
                                        <div class="col-lg-12 col-md-12 col-sm-12" style="padding-top: 10px;">
                                            <div class="demo-section k-content wide">
                                                <div id="chart" style="background: center no-repeat url('../content/shared/styles/world-map.png'); width: 1238px;"></div>
                                            </div>
                                        </div>


                                        <div id="divMgmtRemrks" runat="server" style="width: 100%; float: left; display: none;">
                                            <table style="width: 100%; background: white; margin-left: 13px;">
                                                <tr>
                                                    <td style="width: 100%; vertical-align: top;">
                                                        <asp:ValidationSummary runat="server" Style="color: red;" CssClass="vdsummary"
                                                            ValidationGroup="ComplianceValidationGroup" />
                                                        <asp:CustomValidator ID="CustomValidator1" Style="color: red;" runat="server" EnableClientScript="False"
                                                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 100%; vertical-align: top;">
                                                        <div style="float: left; width: 100%">
                                                            <div style="float: left; width: 100px;">Comments : </div>
                                                            <div style="float: left;">
                                                                <asp:TextBox TextMode="MultiLine" ID="txtManagementRemarks" Width="950px" Height="150px" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="Comment can not be empty."
                                                                    ControlToValidate="txtManagementRemarks" runat="server" ValidationGroup="ComplianceValidationGroup"
                                                                    Display="None" />
                                                            </div>
                                                            <div style="clear: both; height: 5px;"></div>
                                                            <div style="float: left; width: 94%;">
                                                                <asp:Button ID="btnSaveRemarks" Style="float: right;" OnClick="btnSaveRemarks_Click" runat="server" ValidationGroup="ComplianceValidationGroup" Text="Save" CssClass="btn btn-search" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div role="tabpanel" runat="server" id="documents">
                            <div class="tabbable-panel">
                                <div class="tabbable-line">

                                    <div style="font-family: Roboto;">

                                    <div class="col-lg-12 col-md-12 col-sm-12 DetailHeader" style="padding-top: 10px;">
                                        Detailed Description :
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 7px;">
                                        <asp:Label ID="lblDetailedDiscription" class="dataDetail"
                                            maximunsize="300px" autosize="true" runat="server" />
                                    </div>


                                    <div class="col-lg-12 col-md-12 col-sm-12 DetailHeader">
                                        Penalty :                                       
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 7px;">
                                        <asp:Label ID="lblPenalty" class="dataDetail"
                                            maximunsize="300px" autosize="true" runat="server" />
                                    </div>


                                    <div class="col-lg-12 col-md-12 col-sm-12 DetailHeader" style="padding-bottom: 7px;">
                                        Sample Form/Attachment :                                       
                                             <asp:Label ID="lblFormNumber" class="dataDetail"
                                                 maximunsize="300px" autosize="true" runat="server" />
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 DetailHeader" style="padding-bottom: 7px;">
                                        Regulatory website link :                                        
                                             <asp:LinkButton ID="lnkSampleForm" Text="" class="dataDetail" Style="color: blue"
                                                 runat="server" Font-Underline="false" OnClick="lnkSampleForm_Click" />
                                        <%--Style="width: 300px; font-size: 13px; color: blue"--%>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 DetailHeader">
                                        Additional/Reference Text :
                                       
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 7px;">
                                        <asp:Label ID="lblRefrenceText" class="dataDetail"
                                            maximunsize="300px" autosize="true" runat="server" />
                                    </div>

</div>
                                    <%--  <div class="col-lg-5 col-md-5colpadding0 fixwidth" style="width: 50%; background: white; float: left;">
                                            
                                        </div>--%>

                                    <%-- <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%; background: white;display:none;">
                                        <table style="width: 100%; margin-top: 20px; margin-left: 10px;">
                                            <tr>
                                                <td style="width: 15%; font-weight: bold;">Versions</td>
                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                <td style="width: 83%;">
                                                    <table width="100%" style="text-align: left">
                                                        <thead>
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Repeater ID="rptComplianceVersion" runat="server" OnItemCommand="rptComplianceVersion_ItemCommand"
                                                                        OnItemDataBound="rptComplianceVersion_ItemDataBound">
                                                                        <HeaderTemplate>
                                                                            <table id="tblComplianceDocumnets">
                                                                                <thead>
                                                                                </thead>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                                <ContentTemplate>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' ID="lblDocumentVersion"
                                                                                                runat="server" Text='<%# Eval("Version")%>' Style="color: blue;"></asp:LinkButton></td>
                                                                                        <td>
                                                                                            <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                ID="btnComplinceVersionDoc" runat="server" Text="Download" Style="color: blue;">
                                                                                            </asp:LinkButton>
                                                                                            <asp:Label ID="lblSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                                            <asp:LinkButton CommandName="View" ID="lnkViewDoc" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                                                runat="server" Font-Underline="false" />
                                                                                            <asp:Label ID="lblpathReviewDoc" runat="server" Style="display: none"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:PostBackTrigger ControlID="btnComplinceVersionDoc" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            </table>
                                                                        </FooterTemplate>
                                                                    </asp:Repeater>

                                                                </td>
                                                                <td valign="top">
                                                                    <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                                                        OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                                                        <HeaderTemplate>
                                                                            <table id="tblComplianceDocumnets">
                                                                                <thead>
                                                                                    <th>Compliance Related Documents</th>
                                                                                </thead>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:LinkButton
                                                                                        CommandArgument='<%# Eval("FileID")%>'
                                                                                        OnClientClick='javascript:enableControls1()'
                                                                                        ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                    </asp:LinkButton></td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            </table>
                                                                        </FooterTemplate>
                                                                    </asp:Repeater>
                                                                    <asp:Repeater ID="rptWorkingFiles" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                                                        OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                                                        <HeaderTemplate>
                                                                            <table id="tblWorkingFiles">
                                                                                <thead>
                                                                                    <th>Compliance Working Files</th>
                                                                                </thead>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:LinkButton
                                                                                        CommandArgument='<%# Eval("FileID")%>'
                                                                                        ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                                                    </asp:LinkButton></td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            </table>
                                                                        </FooterTemplate>
                                                                    </asp:Repeater>
                                                                </td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>--%>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" runat="server" id="audits">
                            <div class="tabbable-panel">
                                <div class="tabbable-line">

                                    <div id="gridHistoricaldocuments"></div>

                                    <%--  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%; background: white; margin-left: 15px;display:none;">
                                        <asp:GridView ID="gvParentGrid" GridLines="None" runat="server" AutoGenerateColumns="false"
                                            ShowFooter="true" Width="900px" DataKeyNames="SendDate"
                                            OnRowDataBound="gvParentGrid_OnRowDataBound">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-Width="20px">
                                                    <ItemTemplate>
                                                        <a href="JavaScript:divexpandcollapse('div<%# Convert.ToDateTime(Eval("SendDate")).ToString("MMddyyyy") %>');">
                                                            <img id="imgdiv<%# Convert.ToDateTime(Eval("SendDate")).ToString("MMddyyyy") %>" width="9px" border="0"
                                                                src="../Images/add.png" alt="" /></a>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblSendDate" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("SendDate")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td colspan="100%">
                                                                <div id="div<%# Convert.ToDateTime(Eval("SendDate")).ToString("MMddyyyy") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: visible">
                                                                    <asp:GridView ID="gvParentToComplianceGrid" ShowHeader="false" GridLines="None" runat="server" Width="95%" DataKeyNames="SendDate"
                                                                        AutoGenerateColumns="false" OnRowDataBound="gvParentToComplianceGrid_OnRowDataBound">
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblUserName" runat="server" Visible="false" Text='<%# Eval("UserName") %>'></asp:Label>
                                                                                    <asp:Label ID="lblStatus" runat="server" Visible="false" Text='<%# Eval("Status") %>'></asp:Label>
                                                                                    <asp:Label ID="lblRemarks" runat="server" Visible="false" Text='<%# Eval("Remarks") %>'></asp:Label>
                                                                                    <asp:Label ID="lblcustomlabel" runat="server"> </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                    </asp:GridView>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>                                          
                                        </asp:GridView>
                                    </div>--%>
                                </div>

                            </div>
                        </div>


                        <div role="tabpanel" runat="server" id="updateView">
                            <div class="tabbable-panel">
                                <div class="tabbable-line">

                                    <div id="gridUpdate"></div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%; background: white; margin-left: 15px;">
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div role="tabpanel" runat="server" id="auditlogView">
                            <div class="tabbable-panel">
                                <div class="tabbable-line">



                                    <div class="col-md-12" style="padding-top: 10px;padding-left: 3px;">
                                        <div class="col-md-4 colpadding0">
                                            <label for="ddlUsers_AuditLog" class="control-label">Users</label>
                                            <asp:DropDownListChosen runat="server" ID="ddlUsers_AuditLog" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                class="form-control" Width="100%" DataPlaceHolder="Select User">
                                            </asp:DropDownListChosen>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="txtFromDate_AuditLog" class="control-label">From Date</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar color-black"></span>
                                                </span>
                                                <asp:TextBox ID="txtFromDate_AuditLog" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                            </div>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label for="txtToDate_AuditLog" class="control-label">To Date</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar color-black"></span>
                                                </span>
                                                <asp:TextBox ID="txtToDate_AuditLog" runat="server" placeholder="DD-MM-YYYY" class="form-control" autocomplete="off" />
                                            </div>
                                        </div>


                                    </div>
                                    <div class="col-md-12">
                                         <div class="col-md-4 colpadding0">
                                             </div>
                                        <div class="col-md-4 colpadding0">

                                            <asp:Button ID="btnApplyFilter_AuditLog" Style="float: right;" class="btn btn-search" runat="server" Text="Apply Filter(s)" OnClick="btnApplyFilter_AuditLog_Click" />

                                        </div>
                                        <div class="col-md-4 colpadding0">
                                            <asp:Button ID="btnClearFilter_AuditLog" Style="float: right;" class="btn btn-search" runat="server" Text="Clear Filter(s)" OnClick="btnClearFilter_AuditLog_Click" />

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>


                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 95%; background: white; padding-left:19px;">

                                        <div id="divAuditLog_Vertical" class="auditlog" runat="server">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-4 col-md-4 " style="padding-left: 10px; background: white; padding-top: 9px;">


                    <div class="row">
                        <div class="panel panel-default" style="">
                            <%--border-radius: 15px;--%>
                            <div class="panel-heading" style="padding: 4px 19px; background-color: #1fd9e1; color: white">Current Documents</div>
                            <div class="panel-body" style="padding: 0px;">
                                <div id="gridDocuments"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-top:16px;">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="padding: 4px 19px; background-color: #1fd9e1; color: white">Comments</div>
                            <div class="panel-body" style="padding: 0px; overflow-y: scroll;min-height: 184px; max-height: 184px;">

                                <div id="ShowDatadiv" style=""></div>

                                <div style="padding-left: 10px;">
                                   
                                    <div>
                                    <textarea id="TxtComments" style="width: 250px;float: left;" name="txtcmt"></textarea>
                                   </div>
                                    <a onclick="submitData();" id="MainSubmit" style="margin-left: 12px;" class="btn btn-search">save</a>                                   
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <%-- </div>--%>



                <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div class="modal-dialog" style="width: 1306px; padding: 0; position: initial;">
                        <div class="modal-content" style="width: 100%;">
                            <div class="modal-header" style="border-bottom: none;">
                                <button type="button" class="close" data-dismiss="modal" onclick="CloseClearOV();" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">                                                               
                                <iframe id="OverViews" src="about:blank" width="1306px" height="100%" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="ViewUpdateDetails">


                    <label id="detailUpdate"></label>

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div>
            <iframe id="downloadfile" src="about:blank" width="0" height="0"></iframe>
        </div>
        <div class="modal fade" id="DocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss-modal="modal1" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div style="float: left; width: 10%">
                                <table style="text-align: left; margin-left: 5%;">
                                    <thead>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Repeater ID="rptComplianceVersionView" runat="server" OnItemCommand="rptComplianceVersionView_ItemCommand"
                                                            OnItemDataBound="rptComplianceVersionView_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="tblComplianceDocumnets">
                                                                    <thead>
                                                                        <th>Versions</th>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblDocumentVersionView"
                                                                                    runat="server" ToolTip='<%# Eval("FileName")%>'
                                                                                    Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div style="float: left; width: 90%">
                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Label ID="lblMessage" runat="server" Style="color: red;"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="docViewerReviewAll" runat="server" width="100%" height="535px"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>




            $(document).ready(function () {

                $('#TxtComments').mentionable(
                    '<% =KendoPath%>/Data/GetUSerDetailbySearch?customerID=<% =CustId%>', { parameterName: "search" }                  
                );

                imgExpandCollapse();
            });

            function imgExpandCollapse() {

                $("[src*=collapse]").on('click', function () {

                    $(this).attr("src", "/Images/add.png");
                    $(this).closest("tr").next().remove();
                });

                $("[src*=add]").on('click', function () {

                    //if ($(this).attr('src').indexOf('add.png') > -1) {
                    //    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
                    //    $(this).attr("src", "/Images/collapse.png");
                    //    imgExpandhide();
                    //} else if ($(this).attr('src').indexOf('collapse.png') > -1) {
                    //    $(this).attr("src", "/Images/add.png");
                    //    $(this).closest("tr").next().remove();
                    //    imgExpandhide();
                    //}
                });
            }

            function divexpandcollapse(divname) {

                var div = document.getElementById(divname);
                var img = document.getElementById('img' + divname);
                if (div.style.display == "none") {
                    div.style.display = "inline";
                    img.src = "../Images/collapse.png";
                } else {
                    div.style.display = "none";
                    img.src = "../Images/add.png";
                }
            }

            $('#updateProgress1').show();
            $(document).ready(function () {
                $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
                $('#updateProgress1').hide();
            });
        </script>
        <asp:HiddenField runat="server" ID="hdnActId" />
        <asp:HiddenField runat="server" ID="hdnComplianceId" />
    </form>
</body>
</html>
