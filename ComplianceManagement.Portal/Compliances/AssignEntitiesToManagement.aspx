﻿<%@ Page Title="Assign Entities" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="AssignEntitiesToManagement.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.AssignEntitiesToManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <script language="javascript" type="text/javascript">

        //sandesh code start
        function fCheckTree(obj) {
            var id = $(obj).attr('data-attr');
            var elm = $("#" + id);
            $(elm).trigger('click');
        }
        function FnSearch() {

            var tree = document.getElementById('BodyContent_tvFilterLocation');
            var links = tree.getElementsByTagName('a');
            var keysrch = document.getElementById('BodyContent_tbxFilterLocation').value.toLowerCase();
            var keysrchlen = keysrch.length
            if (keysrchlen > 2) {
                $('#bindelement').html('');
                for (var i = 0; i < links.length; i++) {

                    var anch = $(links[i]);
                    var twoletter = $(anch).html().toLowerCase().indexOf(keysrch);
                    var getId = $(anch).attr('id');
                    var parendNode = '#' + getId + 'Nodes';
                    var childanchor = $(parendNode).find('a');
                    if (childanchor.length == 0) {
                        if (twoletter > -1) {

                            var idchild = $($(anch).siblings('input')).attr('name');                            
                            var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  data-attr="' + idchild + '" ><a  >' + anch.html() + '</a></br>';
                            $('#bindelement').append(createanchor);
                        }
                    }

                }
                $(tree).hide();
                $('#bindelement').show();
            } else {
                $('#bindelement').html('');
                $('#bindelement').hide();
                $(tree).show();
            }

        }

        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }

        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }

        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }
        //added by sandesh end

</script>

     <script type="text/javascript">
        $(function () {
            $('#divDeleteEntitiesDialog').dialog({
                height: 450,
                width: 700,
                autoOpen: false,
                draggable: true,
                title: "Delete Assign Entities to Management",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

           initializeCombobox();
        });

        function initializeJQueryUI(textBoxID, divID) {

            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });


            $('#BodyContent_tbxFilterLocation').keyup(function () {
                FnSearch();
            });
        }

    </script>

     <script type="text/javascript">
        $(function () {
            $('#divAssignEntitiesDialog').dialog({
                height: 450,
                width: 700,
                autoOpen: false,
                draggable: true,
                title: "Assign Entities to Management",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });

            initializeCombobox();
        });

        function initializeCombobox() {
            $("#<%= ddlFilterUsers.ClientID %>").combobox();
             $("#<%= ddlUsers.ClientID %>").combobox();
            $("#<%= ddlComplianceCatagory.ClientID %>").combobox();
             $("#<%= delddlcomcatagory.ClientID %>").combobox();
             $("#<%= delddluser.ClientID %>").combobox();
         
         }


         //function initializeJQueryUI(textBoxID, divID) {

         //    $("#" + textBoxID).unbind('click');

         //    $("#" + textBoxID).click(function () {
         //        $("#" + divID).toggle("blind", null, 500, function () { });
         //    });

         //}


    </script>

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <script type="text/javascript">
         function initializeCombobox() {
        }
        
    </script>


    <%--<script src="../js/jquery-1.8.3.min.js"></script>--%>
 <%-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>--%>
     <script type="text/javascript">
    $(function () {
        $("[id*=tvBranches] input[type=checkbox]").bind("click", function () {
            var table = $(this).closest("table");
            if (table.next().length > 0 && table.next()[0].tagName == "DIV") {
                //Is Parent CheckBox
                var childDiv = table.next();
                var isChecked = $(this).is(":checked");
                $("input[type=checkbox]", childDiv).each(function () {
                    if (isChecked) {
                        $(this).attr("checked", "checked");
                    } else {
                        $(this).removeAttr("checked");
                    }
                });
            } else {
                //Is Child CheckBox
                var parentDIV = $(this).closest("DIV");
                if ($("input[type=checkbox]", parentDIV).length == $("input[type=checkbox]:checked", parentDIV).length) {
                    $("input[type=checkbox]", parentDIV.prev()).attr("checked", "checked");
                } else {
                    $("input[type=checkbox]", parentDIV.prev()).removeAttr("checked");
                }
            }
        });


        $('#BodyContent_tbxFilterLocation').keyup(function () {
            FnSearch();
        });
    })
</script>

      <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
        .excelgriddisplay{
            display:none;
        }
               .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }

        .dd_chk_select {
            height: 30px !important;  
            text-align: center;          
            border-radius: 5px;
        }

        div.dd_chk_select div#caption
        {
            margin-top: 5px;
        }
    </style>

      <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });


            $('#BodyContent_tbxFilterLocation').keyup(function () {
                FnSearch();
            });

        }
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    
        
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            // setactivemenu('Import / Export Utility');
            fhead('Import / Export Utility');
        });

      </script>


      <script type="text/javascript">
 
           function showProgress() {              
            var updateProgress = $get("<%# updateProgress.ClientID %>");
            updateProgress.style.display = "block";
        }
    </script>

      <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style>
    
  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div style="width: 100%">
                <div id="divcustomer" runat="server" visible="false" style="float: left; margin-top: 5px; margin-left: 20px;">
                    <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left;">
                        Select Customer:</label>
                    <asp:DropDownList runat="server" ID="ddlFiltercustomer" Height="22px" Width="220px" CssClass="txtbox" OnSelectedIndexChanged="ddlFiltercustomer_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                </div>
                <div id="FilterLocationdiv" runat="server" style="float: left; margin-left: 20px; margin-top: 5px;">
                    <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                        Select Location:</label>

                    <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 295px;"
                        CssClass="txtbox" />
                    <div style="margin-left: 100px; position: absolute; z-index: 10;" id="divFilterLocation">
                        <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                            BorderWidth="1" SelectedNodeStyle-Font-Bold="true"   Height="200px" Width="295px" 
                            Style="overflow: auto" ShowLines="true"  ShowCheckBoxes="All" onclick="OnTreeClick(event)">
                            <%--OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">--%>
                        </asp:TreeView>

                           <div id="bindelement" style="background: white; height: 292px; display: none; width: 390px; border: 1px solid; overflow: auto;"></div>

                        <asp:Button ID="btnlocation" runat="server" OnClick="btnlocation_Click" Text="Select" />
                        <asp:Button ID="btnClear1" Visible="true" runat="server" OnClick="btnClear1_Click" Text="Clear" />
                    </div>
                </div>
                <div runat="server" id="divFilterUsers" style="margin-left: 771px; margin-top: -24px; float: left;">
                    <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                        Select User:
                    </label>
                    <asp:DropDownList runat="server" ID="ddlFilterUsers" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterUsers_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                
                 <div style="float: right; margin-top: -21px; margin-right: 20px">
                    <asp:LinkButton Text="Delete entities" runat="server" ID="LinkButton1" OnClick="btnDeleteComplianceType_Click" />
                </div>
                <div style="float: right; margin-top: -21px; margin-right: 134px">
                    <asp:LinkButton Text="Add New" runat="server" ID="btnAddComplianceType" OnClick="btnAddComplianceType_Click" />
                </div>
                <br />
                <br />
                <br />

                <asp:GridView runat="server" ID="grdAssignEntities" AutoGenerateColumns="false" GridLines="Vertical"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdAssignEntities_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%" OnSorting="grdAssignEntities_Sorting"
                    Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdAssignEntities_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="Location" SortExpression="Branch">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category Name" SortExpression="Category">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Category") %>' ToolTip='<%# Eval("Category") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="UserName" HeaderText="User Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="UserName" />
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>

            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divAssignEntitiesDialog">
        <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional" OnLoad="upCompliance_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" Style="color: Red"></asp:Label>
                    </div>
                     <div style="margin-bottom: 7px" id="divassigncustomer" runat="server" visible="false">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Customer</label>
                        <asp:DropDownList ID="ddlcustomer" runat="server" Style="padding: 0px; margin: 0px; height: 26px; width: 300px;" OnSelectedIndexChanged="ddlcustomer_SelectedIndexChanged"
                         AutoPostBack="true"   CssClass="txtbox">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select Customer." ControlToValidate="ddlcustomer"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                            Display="None" />
                    </div>
                   
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;margin-right: -1px;">
                                    Location</label>
                                <asp:TextBox runat="server" ID="tbxBranch" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                                    CssClass="txtbox" />
                                <div style="margin-left: 160px; position: absolute; z-index: 10" id="divBranches">
                                    <asp:TreeView runat="server" ID="tvBranches"  BackColor="White" BorderColor="Black"
                                        BorderWidth="1" SelectedNodeStyle-Font-Bold="true" ShowCheckBoxes="All"  Height="200px" Width="300px"   
                                        Style="overflow: auto" ShowLines="true"
                                        OnSelectedNodeChanged="tvBranches_SelectedNodeChanged" onclick="OnTreeClick(event)">
                                    </asp:TreeView>
                                </div>
                               <%-- <asp:CompareValidator ID="CompareValidator2" ControlToValidate="tbxBranch" ErrorMessage="Please select Location."
                                    runat="server" ValueToCompare="< Select Location >" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />--%>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                     <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;margin-right: -1px;">
                            User</label>
                        <asp:DropDownList ID="ddlUsers" runat="server" Style="padding: 0px; margin: 0px; height: 25px; width: 300px;"
                            CssClass="txtbox">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select User." ControlToValidate="ddlUsers"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                            Display="None" />
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Compliance Category</label>
                                <asp:DropDownList runat="server" ID="ddlComplianceCatagory" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                                    CssClass="txtbox" AutoPostBack="false">
                                </asp:DropDownList>
                                <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select Compliance Category." ControlToValidate="ddlComplianceCatagory"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="margin-bottom: 7px; float: right; margin-right: 256px; margin-top: 10px; clear: both">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ComplianceInstanceValidationGroup" />
                        
                        <%-- <asp:Button Text="Delete" runat="server" ID="Button1"  OnClick="btnDelete_Click"  CssClass="button" ValidationGroup="ComplianceInstanceValidationGroup"
                            OnClientClick="return confirm('Are you sure!! You want to Delete entity assignment ?');" />--%>
                        <asp:Button Text="Close" runat="server" ID="btnClose" CssClass="button" OnClientClick="$('#divAssignEntitiesDialog').dialog('close');" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 64px; width: 260px;">

                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>


                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="divDeleteEntitiesDialog">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional" OnLoad="upCompliance_LoadDelete">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup1" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="Label3" Style="color: Red"></asp:Label>
                    </div>
                     <div style="margin-bottom: 7px" id="divDeletecustomer" runat="server" visible="false">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Customer</label>
                        <asp:DropDownList ID="delddlCustomer" OnSelectedIndexChanged="delddlCustomer_SelectedIndexChanged" runat="server" Style="padding: 0px;  margin: 0px; height: 22px; width: 300px;"
                          AutoPostBack="true"  CssClass="txtbox">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator5" ErrorMessage="Please select Customer." ControlToValidate="delddlCustomer"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup1"
                            Display="None" />
                    </div>
                   
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Location</label>
                                <asp:TextBox runat="server" ID="TextBox1" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                                    CssClass="txtbox" />
                                <div style="margin-left: 160px; position: absolute; z-index: 10" id="divBranches1">
                                    <asp:TreeView runat="server" ID="TreeView1" BackColor="White"  BorderColor="Black"
                                        BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="300px" ShowCheckBoxes="All"  
                                        Style="overflow: auto" ShowLines="true"  OnSelectedNodeChanged="TreeView1_SelectedNodeChanged" onclick="OnTreeClick(event)">
                                    </asp:TreeView>
                                </div>
                               <%-- <asp:CompareValidator ID="CompareValidator5" ControlToValidate="TextBox1" ErrorMessage="Please select Location."
                                    runat="server" ValueToCompare="< Select Location >" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup1"
                                    Display="None" />--%>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                     <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            User</label>
                        <asp:DropDownList ID="delddluser" runat="server" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                            CssClass="txtbox">
                        </asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator4" ErrorMessage="Please select User." ControlToValidate="delddluser"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup1"
                            Display="None" />
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Compliance Category</label>
                                <asp:DropDownList runat="server" ID="delddlcomcatagory" Style="padding: 0px; margin: 0px; height: 22px; width: 300px;"
                                    CssClass="txtbox"  AutoPostBack="false">
                                </asp:DropDownList>
                                <asp:CompareValidator ID="CompareValidator6" ErrorMessage="Please select Compliance Category." ControlToValidate="delddlcomcatagory"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup1"
                                    Display="None" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="margin-bottom: 7px; float: right; margin-right: 256px; margin-top: 10px; clear: both">
                        
                        <asp:Button Text="Delete" runat="server" ID="Button2"  OnClick="btnDelete_Click"  CssClass="button" ValidationGroup="ComplianceInstanceValidationGroup1"
                            OnClientClick="return confirm('Are you sure!! You want to Delete entity assignment ?');" />
                        
                        <asp:Button Text="Close" runat="server" ID="Button3" CssClass="button" OnClientClick="$('#divDeleteEntitiesDialog').dialog('close');" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 64px; width: 260px;">

                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>


                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

     
</asp:Content>
