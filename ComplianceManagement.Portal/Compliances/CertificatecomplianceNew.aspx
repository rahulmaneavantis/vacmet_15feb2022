﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" validateRequest ="false" AutoEventWireup="true" CodeBehind="CertificatecomplianceNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.CertificatecomplianceNew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
     <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

    <%-- <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>--%>
    <script src="../Scripts/KendoPage/ComCertificate.js"></script>
    <title></title>

    <style type="text/css">
        input[type=checkbox], input[type=radio] {
            margin: 4px 4px 0;
            line-height: normal;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }
    

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
        }

        #grid1 .k-grid-content {
            min-height: 320px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
           // float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        #grid .k-grouping-header {
            font-style: italic;
            margin-top: -7px;
            background-color: white;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-multiselect-wrap .k-input {
               /*padding-top:6px;*/
               display: inherit !important;
           }

        .rade_toolbar.Default .CustomPrint {
            background-image: url(https://www.telerik.com/DEMOS/ASPNET/RadControls/Editor/Skins/Default/buttons/CustomDialog.gif);
        }
        div.k-window-content {
            position: relative;
            height: 100%;
            padding: .58em;
            overflow: hidden;
            outline: 0;
        }

    </style>
    <title></title>

<%--    <script type="text/x-kendo-template" id="template"> 
      
    </script>--%>


     <script type="text/javascript">
         $(document).ready(function () {

             fhead('Compliance Certificate');
             setactivemenu('Myreport');

         });

         function OnClientSelectionChange(sender, args) {
             range = editor.getSelection().getRange(true);
             range1 = editor1.getSelection().getRange(true);
         }

         function OnClientLoad1(sender, args) {
             editor1 = sender;


             editor1.add_modeChange(function (editor, args) {
                 var mode = editor1.get_mode();
                 if (mode == 2) {
                     var htmlMode = editor1.get_textArea();
                     htmlMode.disabled = "true";
                 }
             });


         }

         function OnClientModeChange(editor, args) {
             var mode = editor.get_mode();
             switch (mode) {
                 case 1:
                     //alert( "We are in Design mode");
                     //do something
                     break;
                 case 2:
                     //alert("We are in HTML mode");
                     break;
                 case 4:
                     setTimeout(function () {
                         var tool = editor.getToolByName("Print");
                         tool.setState(0);
                     }, 0);
                     //alert( "We are in Preview mode");
                     //do something
                     break;
             }
         }
    </script>

    <script type="text/javascript">
    Telerik.Web.UI.Editor.CommandList["CustomPrint"] = function (commandName, editor, args)
    {
        var printIframe = document.createElement("IFRAME");
        document.body.appendChild(printIframe);
        var printDocument = printIframe.contentWindow.document;
        printDocument.designMode = "on";
        printDocument.open();
        var currentLocation = document.location.href;
        currentLocation = currentLocation.substring(0, currentLocation.lastIndexOf("/") + 1);
        printDocument.write("<html><head></head><body>" + editor.get_html() + "</body></html>");
        printDocument.close();
        try
        {
            if (document.all)
            {
                var oLink = printDocument.createElement("link");
                oLink.setAttribute("href", currentLocation + "PrintWithStyles.css", 0);
                oLink.setAttribute("type", "text/css");
                oLink.setAttribute("rel", "stylesheet", 0);
                printDocument.getElementsByTagName("head")[0].appendChild(oLink);
                printDocument.execCommand("Print");
            }
            else
            {
                printDocument.body.innerHTML = "<link rel='stylesheet' type='text/css' href='" + currentLocation + "PrintWithStyles.css'></link>" + printDocument.body.innerHTML;
                printIframe.contentWindow.print();
            }
        }
        catch (ex)
        {
        }
        document.body.removeChild(printIframe);
    };
</script> 

    <script type="text/javascript">

        function GetAllComplianceCertificates()
        {

        }


        function onChange() {

            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != undefined && $("#Startdatepicker").val() != "") {
                $('#ClearfilterMain').css('display', 'block');
                $("#dropdownPastData").data("kendoDropDownList").select(4);
            }
        }

        $("#Lastdatepicker").kendoDatePicker({
            change: onChange1
        });
        function onChange1() {
            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != undefined && $("#Lastdatepicker").val() != "") {
                $("#dropdownPastData").data("kendoDropDownList").select(4);
                $('#ClearfilterMain').css('display', 'block');
            }
        }

        function BindGridApply(e) {
            var setStartDate = $("#Startdatepicker").val();
            var setEndDate = $("#Lastdatepicker").val();


           // BindGrid();
            //Added by Mrunali on 24 jan 2020
            FilterGridDemo();

            if (setStartDate != null) {
                $("#Startdatepicker").data("kendoDatePicker").value(setStartDate);
            }
            if (setEndDate != null) {
                $("#Lastdatepicker").data("kendoDatePicker").value(setEndDate);
            }

            //comment by rahul on 24 jan 2020
            //FilterGrid();

            e.preventDefault();
        }

        function onChangeSD() {

        }
        function onChangeLD() {

        }
        function FilterGridDemo() {

            var Riskdetails = [];
            if ($("#dropdownlistRisk").data("kendoDropDownTree") != undefined) {
                Riskdetails = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
            }

            //location details
            var locationsdetails = [];
            if ($("#dropdowntree").data("kendoDropDownTree") != undefined) {
                locationsdetails = $("#dropdowntree").data("kendoDropDownTree")._values;
            }

            //Category details
            var categorydetails = [];
            if ($("#dropdownfunction").data("kendoDropDownTree") != undefined) {
                categorydetails = $("#dropdownfunction").data("kendoDropDownTree")._values;
            }

            //User details
            var userdetails = [];
            if ($("#dropdownUser").data("kendoDropDownTree") != undefined) {
                userdetails = $("#dropdownUser").data("kendoDropDownTree")._values;
            }

            //Dept details
            var deptdetails = [];
            if ($("#dropdownDept").data("kendoDropDownTree") != undefined) {
                deptdetails = $("#dropdownDept").data("kendoDropDownTree")._values;
            }

            //datefilter
            var datedetails = [];
            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                datedetails.push({
                    field: "ScheduledOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'dd-MM-yyyy')
                });
            }
            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                datedetails.push({
                    field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'dd-MM-yyyy')
                });
            }

           <%-- <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "MGMT") {%> 
                

                if ($("#dropdownUser").val() != "" && $("#dropdownUser").val() != null && $("#dropdownUser").val() != undefined) {
                    var UserFilter = { logic: "or", filters: [] };
                    UserFilter.filters.push({
                        field: "UserID", operator: "eq", value: $("#dropdownUser").val()
                    });
                    finalSelectedfilter.filters.push(UserFilter);
                    $('#ClearfilterMain').css('display', 'block');

                }

                     <%}%>--%>
        }

        $(document).ready(function () {
      

            $("#Startdatepicker").kendoDatePicker({
                format:"dd-MM-yyyy",
                change: onChangeSD
            });
            $("#Lastdatepicker").kendoDatePicker({
                change: onChangeLD,
                format: "dd-MM-yyyy",
            });


               $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterGridDemo();
                  
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                    //window.parent.forchild($("body").height() + 15);
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =roles%>&IsStatutoryInternal=S',
                                    dataType: "json",
                                    beforeSend: function (request) {
                                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                                    },
                                }
                                //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=MGMT&IsStatutoryInternal=S"
                            },
                            schema: {
                                data: function (response) {
                                    return response[0].locationList;
                                },
                                model: {
                                    children: "Children"
                                }
                            }
                        }
            });

        $("#dropdownlistRisk").kendoDropDownTree({
            placeholder: "Risk",
            checkboxes: true,
            checkAll: true,
            autoClose: true,
            checkAllTemplate: "Select All",
            autoWidth: true,
            dataTextField: "text",
            dataValueField: "value",
            change: function () {
                FilterGridDemo();
                fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
                //window.parent.forchild($("body").height() + 15);
            },
            dataSource: [
                { text: "Critical", value: "3" },
                { text: "High", value: "0" },
                { text: "Medium", value: "1" },
                { text: "Low", value: "2" }
            ]
        });

        $("#dropdownfunction").kendoDropDownTree({
            placeholder: "Category",
            checkboxes: true,
            checkAll: true,
            autoClose: true,
            autoWidth: true,
            dataTextField: "CategoryName",
            dataValueField: "CategoryID",
            checkAllTemplate: "Select All",
            change: function (e) {
                FilterGridDemo();
                fCreateStoryBoard('dropdownfunction', 'filterfunction', 'Catfunction');
            },
            dataSource: {
                severFiltering: true,
                transport: {
                    read: {
                        url: '<% =Path%>Data/BindCertificateFunctionList?customerID=<% =CustId%>&UserID=<% =UId%>',
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                        },
                    }
                }
            }
        });


            $("#dropdownUser").kendoDropDownTree({
                placeholder: "User",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                dataTextField: "UserName",
                dataValueField: "UserID",
                checkAllTemplate: "Select All",
                change: function (e) {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                          
                            url: '<% =Path%>/Data/BindCertificateUserList?CustId=<% =CustId%>&UserId=<% =UId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                 request.setRequestHeader('Authorization', '<% =Authorization%>');
                             },
                         }
                      
                       }
                   }
               });

            $("#dropdownToUser").kendoComboBox({
                placeholder: "To User",
                //checkboxes: true,
                //checkAll: true,
                //autoClose: true,
                //autoWidth: true,
                dataTextField: "FullName",
                dataValueField: "UID",
                //checkAllTemplate: "Select All",

                filter: "contains",
                suggest: true,
                change: function (e) {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownToUser', 'filterToUser', 'user');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {

                            url: '<% =Path%>/Data/KendoCustomerWiseUserList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }

                    }
                }
            });

            $("#dropdownFromUser").kendoComboBox({
                placeholder: "From User",
                //checkboxes: true,
                //checkAll: true,
                //autoClose: true,
                //autoWidth: true,
                dataTextField: "FullName",
                dataValueField: "UID",
                //checkAllTemplate: "Select All", 
                filter: "contains",
                suggest: true,
                change: function (e) {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownFromUser', 'filterFromUser', 'user');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {

                            url: '<% =Path%>/Data/KendoCustomerWiseUserList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }

                    }
                }
            });

            $("#dropdownDept").kendoDropDownTree({
                placeholder: "Department",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                dataTextField: "DeptName",
                dataValueField: "DeptID",
                checkAllTemplate: "Select All",

                change: function (e) {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownDept', 'filterDept', 'Dept');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindCertificateDeptList?CId=<% =CustId%>&UId=<% =UId%>',
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                    },
                }
                
            }
        }
               });


            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    if ($("#dropdownPastData").val() != "All") {
                        $("#dropdownFY").data("kendoDropDownList").select(0);
                        fCreateStoryBoard('dropdownPastData', 'filterpstData1', 'pastdata');
                    }
                },
                index: 4,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });

        });//Document.ready close

        function exportReport(e) {
            e.preventDefault();


            var isTagVisible = document.getElementById('IsTagVisible').value;
            var Roles = document.getElementById('roles').value;
            var FromUserID;
            if (isTagVisible === "True") {
                if (Roles === "MGMT") {
                    FromUserID = $("#dropdownFromUser").val()
                }
                else {
                    FromUserID = document.getElementById('UId').value;
                }
            } 

            var validationflag = true;            
            if (validationflag === true)
            {
                var UId = document.getElementById('UId').value;
                var customerId = document.getElementById('CustomerId').value;
                var PathName = document.getElementById('Path').value;

                //location details
                var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
                var locationsdetails = [];
                $.each(list1, function (i, v) {
                    locationsdetails.push(v);
                });

                //Category details
                var list1 = $("#dropdownfunction").data("kendoDropDownTree")._values;
                var categorydetails = [];
                $.each(list1, function (i, v) {
                    categorydetails.push(v);
                });

                var userdetails = [];
                var isTagVisible = document.getElementById('IsTagVisible').value;
                var Roles = document.getElementById('roles').value;
                var FromUserID;
                if (isTagVisible === "False") {
                    //User details
                    var list1 = $("#dropdownUser").data("kendoDropDownTree")._values;
                    $.each(list1, function (i, v) {
                        userdetails.push(v);
                    });
                }

                //Dept details
                var list1 = $("#dropdownDept").data("kendoDropDownTree")._values;
                var deptdetails = [];
                $.each(list1, function (i, v) {
                    deptdetails.push(v);
                });

                //risk Details
                var Riskdetails = [];
                var list3 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Riskdetails.push(v);
                });

                $.ajax({
                    type: "GET",
                    url: '' + PathName + '//ExportReport/CompliancecertificateNew',
                    data: {
                        UserId: UId, CustomerID: customerId,
                        MonthId: $("#dropdownPastData").val(),
                        location: JSON.stringify(locationsdetails),
                        risk: JSON.stringify(Riskdetails),
                        StartDateDetail: $("#Startdatepicker").val(),
                        EndDateDetail: $("#Lastdatepicker").val(),
                        DeptID: JSON.stringify(deptdetails),
                        CatID: JSON.stringify(categorydetails),
                        DdlUserID: JSON.stringify(userdetails),
                        toUser: $("#dropdownToUser").val(),
                        fromUser: FromUserID,
                    },
                    success: function (response) {
                        if (response != "Error" && response != "No Record Found" && response != "") {
                            window.location.href = '' + PathName + '/ExportReport/GetFile?userpath=' + response + '';
                        }
                        if (response == "" || response == "No Record Found") {
                            alert("No Record Found");
                        }
                    }
                });
            }
            e.preventDefault();
            return false;
        }


        function exportReportpdf(e) {
            e.preventDefault();

            var isTagVisible = document.getElementById('IsTagVisible').value;
            var Roles = document.getElementById('roles').value;
            var FromUserID;
            if (isTagVisible === "True") {
                if (Roles === "MGMT") {
                    FromUserID = $("#dropdownFromUser").val()
                }
                else {
                    FromUserID = document.getElementById('UId').value;
                }
            } 

            var validationflag = true;

            if (validationflag === true) {
                var UId = document.getElementById('UId').value;
                var customerId = document.getElementById('CustomerId').value;
                var PathName = document.getElementById('Path').value;

                //location details
                var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
                var locationsdetails = [];
                $.each(list1, function (i, v) {
                    locationsdetails.push(v);
                });

                //Category details
                var list1 = $("#dropdownfunction").data("kendoDropDownTree")._values;
                var categorydetails = [];
                $.each(list1, function (i, v) {
                    categorydetails.push(v);
                });

                var userdetails = [];
                var isTagVisible = document.getElementById('IsTagVisible').value;
                var Roles = document.getElementById('roles').value;
                var FromUserID;
                if (isTagVisible === "False") {
                    //User details
                    var list1 = $("#dropdownUser").data("kendoDropDownTree")._values;
                    $.each(list1, function (i, v) {
                        userdetails.push(v);
                    });
                }
                //Dept details
                var list1 = $("#dropdownDept").data("kendoDropDownTree")._values;
                var deptdetails = [];
                $.each(list1, function (i, v) {
                    deptdetails.push(v);
                });

                //risk Details
                var Riskdetails = [];
                var list3 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Riskdetails.push(v);
                });

                $.ajax({
                    type: "GET",
                    url: '' + PathName + '//ExportReport/CompliancecertificatepdfNew',
                    data: {
                        UserId: UId, CustomerID: customerId,
                        MonthId: $("#dropdownPastData").val(),
                        location: JSON.stringify(locationsdetails),
                        risk: JSON.stringify(Riskdetails),
                        StartDateDetail: $("#Startdatepicker").val(),
                        EndDateDetail: $("#Lastdatepicker").val(),
                        DeptID: JSON.stringify(deptdetails),
                        CatID: JSON.stringify(categorydetails),
                        DdlUserID: JSON.stringify(userdetails),
                        toUser: $("#dropdownToUser").val(),
                        fromUser: FromUserID,
                    },
                    success: function (response) {
                        if (response != "Error" && response != "No Record Found" && response != "") {
                            window.location.href = '' + PathName + '/ExportReport/GetFile?userpath=' + response + '';
                        }
                        if (response == "" || response == "No Record Found") {
                            alert("No Record Found");
                        }
                    }
                });
            }
            e.preventDefault();
            return false;
        }


        function previewDoc(e) {
            debugger;
            e.preventDefault();
       
           
            var isTagVisible = document.getElementById('IsTagVisible').value;
            var Roles = document.getElementById('roles').value;
            var FromUserID;
            if (isTagVisible === "True") {
                if (Roles === "MGMT") {
                    FromUserID = $("#dropdownFromUser").val()
                }
                else {
                    FromUserID = document.getElementById('UId').value;
                }
            } 

            var validationflag = true;
            if (validationflag === true) {
                var UId = document.getElementById('UId').value;
                var customerId = document.getElementById('CustomerId').value;
                var PathName = document.getElementById('Path').value;

                //location details
                var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
                var locationsdetails = [];
                $.each(list1, function (i, v) {
                    locationsdetails.push(v);
                });

                //Category details
                var list1 = $("#dropdownfunction").data("kendoDropDownTree")._values;
                var categorydetails = [];
                $.each(list1, function (i, v) {
                    categorydetails.push(v);
                });

                var userdetails = [];
                var isTagVisible = document.getElementById('IsTagVisible').value;
                var Roles = document.getElementById('roles').value;
                var FromUserID;
                if (isTagVisible === "False") {
                    //User details
                    var list1 = $("#dropdownUser").data("kendoDropDownTree")._values;                  
                    $.each(list1, function (i, v) {
                        userdetails.push(v);
                    });
                }

                //Dept details
                var list1 = $("#dropdownDept").data("kendoDropDownTree")._values;
                var deptdetails = [];
                $.each(list1, function (i, v) {
                    deptdetails.push(v);
                });

                //risk Details
                var Riskdetails = [];
                var list3 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Riskdetails.push(v);
                });
                
                $.ajax({
                    type: "GET",
                    url: '' + PathName + '//ExportReport/CompliancecePreview',
                    data: {
                        UserId: UId, CustomerID: customerId,
                        MonthId: $("#dropdownPastData").val(),
                        location: JSON.stringify(locationsdetails),
                        risk: JSON.stringify(Riskdetails),
                        StartDateDetail: $("#Startdatepicker").val(),
                        EndDateDetail: $("#Lastdatepicker").val(),
                        DeptID: JSON.stringify(deptdetails),
                        CatID: JSON.stringify(categorydetails),
                        DdlUserID: JSON.stringify(userdetails),
                        toUser: $("#dropdownToUser").val(),
                        fromUser: FromUserID,
                    },
                    success: function (response) {
                        debugger
                        $("#<%=datastring.ClientID%>").val(response);
                   
                        $("#ContentPlaceHolder1_btnView").click();

                        OpenPopupCCCustomized(this);

                        

                        //if (response != "Error" && response != "No Record Found" && response != "") {
                        //    window.location.href = '' + PathName + '/ExportReport/GetFile?userpath=' + response + '';
                        //}
                        //if (response == "" || response == "No Record Found") {
                        //    alert("No Record Found");
                        //}
                    }
                });
            }
            e.preventDefault();
            return false;
        }

        function OpenPopupCCCustomized(e) {
            $("#MoreReports").kendoWindow({
                width: "90%",
                height: "90%",
                title: "Preview",
                visible: false,
               // modal:true,
                refresh: true,
                //pinned: true,
                actions: [
                    "Close"
                ]

            }).data("kendoWindow").open().center();
            e.preventDefault();

            var windowWidget = $("#MoreReports").data("kendoWindow");

            kendo.ui.progress(windowWidget.element, true);

            settimeout(function () { kendo.ui.progress(windowWidget.element, false); }, 5000)
        }

        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
            $("#Startdatepicker").data("kendoDatePicker").value(null);
            $("#Lastdatepicker").data("kendoDatePicker").value(null);
            $("#dropdownfunction").data("kendoDropDownTree").value([]);
            $("#dropdownDept").data("kendoDropDownTree").value([]);
            $("#dropdownPastData").data("kendoDropDownList").select(4);             
            $("#dropdownUser").data("kendoDropDownTree").value([]);
             e.preventDefault();
        }

    </script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div id="example">
        <div>
            <input id="Path" type="hidden" value="<% =Path%>" />
            <input id="CustomerId" type="hidden" value="<% =CustId%>" /> 
            <input id="RoleFlagCHK" type="hidden" value="<% =RoleFlag%>" />
            <input id="UId" type="hidden" value="<% =UId%>" />
             <input id="roles" type="hidden" value="<% =roles%>" />
             <input id="IsTagVisible" type="hidden" value="<% =IsTagVisible%>" />
           

            <div class="row" style="padding-bottom: 5px;">
                <div >
                    <div style="margin: 0.5% 0 0.5% 1%;">
                        <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 25%; margin-right: 10px;"/>
                        <input id="dropdownfunction" data-placeholder="Category" style="width: 20%; margin-right: 10px;"/>
                        <input id="dropdownlistRisk" data-placeholder="Risk" style="width: 15%; margin-right: 10px;"/>                        
                        <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" style="width: 15%; margin-right: 0.8%;" />
                        <input id="Lastdatepicker" placeholder="End Date"  cssclass="clsROWgrid" style="width: 15%;  margin-right: 0.8%;" />

                    </div>

                    <div style="margin: 0.5% 0 0.5% 1%;">
                        <input id="dropdownDept" data-placeholder="Risk" style="width: 25%; margin-right: 10px;"/>  
                          <%if (IsTagVisible == false)%>
                        <%{%>
                         <input id="dropdownUser" data-placeholder="User" style="width: 20%; margin-right: 10px;" />
                           <%}%>
                        <input id="dropdownPastData" data-placeholder="Period" style="width: 15%; margin-right: 10px;"/>
                         <%if (IsTagVisible == true)%>
                        <%{%>
                         <input id="dropdownToUser" data-placeholder="To User" style="width: 15%; margin-right: 10px;" />  
                        <%if (roles == "MGMT")%>
                        <%{%>
                         <input id="dropdownFromUser" data-placeholder="From User" style="width: 15%; margin-right: 10px;" />
                        <%}%>
                          <%}%>
                        <button type="button" id="Preview" data-toggle="tooltip" title="Preview" style="height:30px;float: right;margin-right: 13px; margin-top: 10px;" onclick="previewDoc(event)"><span class="k-icon k-i-pdf k-grid-edit3" style="margin-right: 2px;"></span>Preview</button>
                        <button type="button" id="exportpdf" data-toggle="tooltip" title="Export pdf format" style="height:30px;float: right;margin-right: 13px; margin-top: 10px; " onclick="exportReportpdf(event)"><span class="k-icon k-i-pdf k-grid-edit3" style="margin-right: 2px;"></span>Export</button>
                        <button type="button" id="export" data-toggle="tooltip" title="Export doc format" style="height:30px;float: right;margin-right: 13px;margin-top: 10px; " onclick="exportReport(event)"><span class="k-icon k-i-excel k-grid-edit3" style="margin-right: 2px;"></span>Export</button>
                        <button id="ClearfilterMain" style="display: block; float: right; height: 30px; margin-right: 10px;margin-top: 10px;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
                    </div>
                     

                     <div class="clearfix"></div>
                        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; padding-left: 10px;color: #3e3434;" id="filtersstoryboard">&nbsp;</div>
                        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; padding-left: 10px;color: #3e3434;" id="filterrisk">&nbsp;</div> 
                        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; padding-left: 10px;color: #3e3434;" id="filterlocation">&nbsp;</div>   
                        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; padding-left: 10px;color: #3e3434;" id="filterfunction">&nbsp;</div>   
                        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; padding-left: 10px;color: #3e3434;" id="filterUser">&nbsp;</div>  
                      
                           <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; padding-left: 10px;color: #3e3434;" id="filterDept">&nbsp;</div>  
                    <div id="grid" style="margin-bottom:10px;"></div>

                 
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                      <asp:Button ID="btnView" OnClick="btn_Click" style="display:none" runat="server" Text="Button" />
                  </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnView" />
                            </Triggers>
        </asp:UpdatePanel>
                     <asp:TextBox ID="datastring" style="display:none" runat="server"></asp:TextBox>

                    
                </div>
            </div>
 


        </div>

             </div>

    <div id="MoreReports" style="display: none">
           <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
         <div class="demo-containers" style="margin-top: 10px; margin-left: 90px;">
                <div class="demo-container">
                   <%-- <telerik:RadEditor RenderMode="Lightweight" ID="RadDisplayControl" EnableTrackChanges="true" runat="server" Width="850px"
                        OnClientLoad="OnClientLoad1" Height="450px" ToolsFile="~/ToolsFile.xml" OnExportContent="RadDisplayControl_ExportContent" ContentFilters="DefaultFilters, PdfExportFilter">
                        <ExportSettings>
                            <Docx DefaultFontName="Arial" DefaultFontSizeInPoints="12" HeaderFontSizeInPoints="8" PageHeader="Some header text for DOCX documents" />
                            <Rtf DefaultFontName="Times New Roman" DefaultFontSizeInPoints="13" HeaderFontSizeInPoints="9" PageHeader="Some header text for RTF documents" />
                        </ExportSettings>
                        <TrackChangesSettings Author="RadEditorUser" CanAcceptTrackChanges="true" UserCssId="reU0"></TrackChangesSettings>
                        <Tools>
        <telerik:EditorToolGroup>
            <telerik:EditorTool Name="CustomPrint" />
        </telerik:EditorToolGroup>
    </Tools>
                    </telerik:RadEditor>--%>

                    <telerik:RadEditor RenderMode="Lightweight" runat="server" ID="RadDisplayControl" SkinID="DefaultSetOfTools"
                Height="535px" Width="100%" style="margin-left:-41px" OnClientModeChange="OnClientModeChange" EditModes="Preview">
                <ImageManager ViewPaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                    UploadPaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                    DeletePaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                    EnableAsyncUpload="true"></ImageManager>
                        <Tools>
                            <telerik:EditorToolGroup>
                                <telerik:EditorTool Name="print" />
                            </telerik:EditorToolGroup>
                        </Tools>
            </telerik:RadEditor>
                </div>
            </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <%--<telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
                    <AjaxSettings>
                        <telerik:AjaxSetting AjaxControlID="ConfiguratorPanel1">
                            <UpdatedControls>
                                <telerik:AjaxUpdatedControl ControlID="RadDisplayControl" LoadingPanelID="RadAjaxLoadingPanel1" />
                                <telerik:AjaxUpdatedControl ControlID="RadExportControl" LoadingPanelID="RadAjaxLoadingPanel1" />
                                <telerik:AjaxUpdatedControl ControlID="ConfiguratorPanel1" />
                            </UpdatedControls>
                        </telerik:AjaxSetting>
                    </AjaxSettings>
                </telerik:RadAjaxManager>
                <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1">
                </telerik:RadAjaxLoadingPanel>--%>

                <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="CheckBoxListEditMode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadEditor1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="CheckBoxListEditMode" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadioButtonToolsFile">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadEditor1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadioButtonToolsFile" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="CheckBoxListModules">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadEditor1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="CheckBoxListModules" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadioButtonListEnabled">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadEditor1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadioButtonListEnabled" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="NewLineModeButtonList">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadEditor1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="NewLineModeButtonList" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="CheckBoxListModules">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadEditor1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="CheckBoxListModules" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
            </ContentTemplate>
        </asp:UpdatePanel>
                       </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>
