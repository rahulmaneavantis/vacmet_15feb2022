﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class CheckListKendo : System.Web.UI.Page
    {
        public static string CompDocReviewPath = "";
        protected static string CId;
        protected static string UserId;
        protected static int CustId;
        protected static int UId;
        protected static List<Int32> roles;
        protected static int RoleID;
        protected static int UserRoleID;
        protected static int StatusFlagID;       
        protected static int ComplianceTypeID;
        protected static int RoleFlag;
        protected static string Falg;
        protected static bool DisableFalg;
        protected static string Path;
        protected static int PerformerFlagID;
        protected static int ReviewerFlagID;
        protected static bool IsNotCompiled;
        protected static string Authorization;
        protected static int MultipleApprove;
        protected static string UserName;
        public static bool DisplayFixedPerRev;
        protected static string Period;

        protected void Page_Load(object sender, EventArgs e)
        {
            DisplayFixedPerRev = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "ShowPerRevColumn");
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            IsNotCompiled = false;
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            CId = Convert.ToString(AuthenticationHelper.CustomerID);
            UserId = Convert.ToString(AuthenticationHelper.UserID);
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            RoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);
            RoleFlag = 0;
            UserName = AuthenticationHelper.User;
            MultipleApprove = 0;

            string CY = Request.QueryString["CY"];//Statutory,Internal,Event Based
            if (!string.IsNullOrEmpty(CY))
            {
                if (CY == "4")
                {
                    Period = "All";
                }
                else
                {
                    Period = CY;
                }
            }
            else
            {
                Period = "All";
            }


            var details = Business.ComplianceManagement.GetCustomerID(Convert.ToInt32(AuthenticationHelper.CustomerID));
            if (details != null)
            {
                MultipleApprove = 1;
            }
            PerformerFlagID = 0;
            ReviewerFlagID = 0;
            roles = Session["User_comp_Roles"] as List<int>;//get role for Performer and Reviewer
            if (roles.Contains(3))
                PerformerFlagID = 1;
            if (roles.Contains(4))
                ReviewerFlagID = 1;

            string InputType = Request.QueryString["type"];//Statutory,Internal,Event Based
            if (!string.IsNullOrEmpty(InputType))
            {
                if (InputType.Equals("Statutory"))
                    ComplianceTypeID = -1;
                if (InputType.Equals("Internal"))
                    ComplianceTypeID = 0;
            }
            else
            {
                ComplianceTypeID = -1;
            }

            string InputRole = Request.QueryString["role"];//Perfomer,Reviewer
            if (!string.IsNullOrEmpty(InputRole))
            {
                if (InputRole.Equals("Performer"))
                    UserRoleID = 3;
                if (InputRole.Equals("Reviewer"))
                    UserRoleID = 4;
            }
            else
            {
                if (roles.Contains(3))
                    UserRoleID = 3;
                else if (roles.Contains(4))
                    UserRoleID = 4;
            }


            string InputFilter = Request.QueryString["filter"];//Status,Upcoming,Overdue,PendingForReview,Rejected,DueButNotSubmitted
            if (!string.IsNullOrEmpty(InputFilter))
            {
                if (InputFilter.Equals("Status"))
                    StatusFlagID = -1;
                if (InputFilter.Equals("Overdue"))
                    StatusFlagID = 0;
                if (InputFilter.Equals("Closed-Timely"))
                    StatusFlagID = 1;
            }
            else {
                StatusFlagID = -1;
            }
                                 
            //if (AuthenticationHelper.Role == "MGMT")
            //{
            //    Falg = "MGMT";
            //    RoleFlag = 1;
            //    DisableFalg = false;
            //}
            //if (AuthenticationHelper.Role == "AUDT")
            //{               
            //    Falg = "AUD";
            //    DisableFalg = true;
            //}
            if (AuthenticationHelper.Role == "EXCT")
            {
                DisableFalg = false;
                Falg = "PRA";
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

                if (roles.Contains(4))
                {
                    RoleFlag = 1;
                }
            }

            // STT Change- Add Status
            string customer = ConfigurationManager.AppSettings["NotCompliedCustID"].ToString();

            long customerID = AuthenticationHelper.CustomerID;
            List<string> NotCompliedCustIDList = customer.Split(',').ToList();
            if (NotCompliedCustIDList.Count > 0)
            {
                foreach (string PList in NotCompliedCustIDList)
                {
                    if (PList == customerID.ToString())
                    {
                        IsNotCompiled = true;
                        break;
                    }
                }
            }

            //else if (AuthenticationHelper.Role == "CADMN")
            //{
            //    DisableFalg = false;
            //    Falg = "PRA";
            //    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

            //    if (roles.Contains(4))
            //    {
            //        RoleFlag = 1;
            //    }
            //}
        }
    }
}