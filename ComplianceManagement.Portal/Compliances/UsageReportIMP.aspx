﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="UsageReportIMP.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.UsageReportIMP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <title></title>
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftmastermenu');
            fhead('Usage Report');
        });


        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        function setDate() {
            $(".StartDate").datepicker();
        }

        function initializeJQueryUI() {

            $("#<%= tbxBranch.ClientID %>").unbind('click');

            $("#<%= tbxBranch.ClientID %>").click(function () {
                $("#divBranches").toggle("blind", null, 500, function () { });
            });
        }

        function initializeDatePicker(date) {
            var startDate = new Date();
            $('#<%= txtStartDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:2025',
            });
        }

        function initializeDatePicker1(date) {
            var startDate = new Date();
            $('#<%= txtEndDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:2025',
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" OnLoad="UpdatePanel1_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                                <div class="panel-body">
                                    <div class="col-md-12 colpadding0">
                <asp:ValidationSummary ID="ValidationSummary" runat="server"
                    ValidationGroup="ValidationUsageReport" class="alert alert-block alert-danger fade in" ForeColor="Red" />
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ValidationUsageReport" Display="None" class="alert alert-block alert-danger fade in" ForeColor="Red" />
                <asp:Label ID="Label1" runat="server" Style="color: Red"></asp:Label>
            </div>
                                   <table>
                                       <tr>
                                           <td>
                                               <%--<% if (user_Roles.Contains("IMPT"))%>
                                               <%{%>    <%}%>  --%>       
                            
                                               <div class="col-md-3 colpadding0" id="DivCustomer" runat="server" visible="false">
                                                <label style=" display: block; float: left; font-size: 13px; color: red;margin-left: 40px;">*</label>
                                                                <label style="display: block; float: left; font-size: 13px; color: #333;">
                                                                    Customer</label>
                                                                    <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px;margin: 0px;height: 26px;width: 289px;margin-left: 41px;"
                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" />

                                            </div>
                                              
                                           </td>

                                           <td>
                                               <div class="col-md-3 colpadding0 entrycount" style="float:left;margin-right: -1%; width:33.667% !important">
                                                      <label style=" display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="display: block; float: left; font-size: 13px; color: #333;">
                                                                    Customer Branch</label>
                                                     <%--<asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" Style="padding: 0px;padding-left: 10px; margin: 0px; height: 35px; width: 320px; border: 1px solid #c7c7cc;border-radius: 4px;color:#8e8e93"
                                                        CssClass="txtbox" />   
                                                    <div style="margin-left: 1px; position: absolute; z-index: 10;display: inherit;" id="divFilterLocation">
                                                        <asp:TreeView runat="server" ID="tvFilterLocation"   SelectedNodeStyle-Font-Bold="true"  Width="320px"   NodeStyle-ForeColor="#8e8e93"
                                                        Style="overflow: auto; border-left:1px solid #c7c7cc; border-right:1px solid #c7c7cc; border-bottom:1px solid #c7c7cc; background-color: #ffffff; color:#8e8e93 !important;" ShowLines="true" 
                                                            OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                                        </asp:TreeView>
                                                    </div>--%>  
                                                   <asp:TextBox runat="server" ID="tbxBranch" Style="padding: 0px; margin: 0px; height: 22px; width: 290px;"
                        CssClass="txtbox" />
                    <div style="position: absolute; z-index: 10" id="divBranches">
                        <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                            BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="290px"
                            Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                        </asp:TreeView>
                    </div>
                                               </div>
                                           </td>

                                           <td>
                                                <div id="Div3" runat="server" class="table-advanceSearch-selectOpt">
                                                <label style=" display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="display: block; float: left; font-size: 13px; color: #333;">
                                                From Date</label>
                                                <asp:TextBox runat="server" Style="padding-left: 7px;" placeholder="From Date"
                                                ID="txtStartDate" CssClass="StartDate form-group form-control" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="Previous Date can not be empty."
                                                ControlToValidate="txtStartDate" runat="server" ValidationGroup="ValidationUsageReport"
                                                Display="None" />
                                                </div>
                                           </td>

                                           <td>
                                                <div id="Div4" runat="server" class="table-advanceSearch-selectOpt">
                                                <label style=" display: block; float: left; font-size: 13px; color: red;">*</label>
                                                <label style="display: block; float: left; font-size: 13px; color: #333;">
                                                To Date</label>
                                                <asp:TextBox runat="server"
                                                Style="padding-left: 7px;" placeholder="To Date"
                                                CssClass="StartDate form-group form-control" ID="txtEndDate" />

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="To Date can not be empty."
                                                ControlToValidate="txtEndDate" runat="server" ValidationGroup="ValidationUsageReport"
                                                Display="None" />
                                                </div>
                                           </td>

                                           <td> <label style="display: block; float: left; font-size: 13px; color: #333;">
                                                                    &nbsp;</label>                                                  
                                                    <asp:Button ID="btnExportExcel" OnClick="btnExportExcel_Click" CausesValidation="false" Style="margin-left:33px !important;margin-right: 35px; float:left;" runat="server" Text="Export To Excel" />
                                                
                                           </td>
                                       </tr>
                                   </table>  
                                 </div>                        
                        </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

