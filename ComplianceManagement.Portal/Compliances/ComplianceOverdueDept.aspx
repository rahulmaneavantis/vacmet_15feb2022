﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="ComplianceOverdueDept.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ComplianceOverdueDept" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>

    <style type="text/css">                                       

        input[type=checkbox], input[type=radio] {
            margin: 4px 4px 0;
            line-height: normal;
        }
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
        }

        #grid1 .k-grid-content {
            min-height: 320px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        #grid .k-grouping-header {
            font-style: italic;
            margin-top: -7px;
            background-color: white;
        }
        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-multiselect-wrap .k-input {
               /*padding-top:6px;*/
               display: inherit !important;
           }
    </style>
    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>

    <script type="text/x-kendo-template" id="template">      
               
    </script>
    <script type="text/javascript">
        var CTypeID = -1;

        function BindGrid() {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();
            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            <%--  url: '<% =Path%>Data/GetMGMTOverdueComplianceDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&IsFlag=0&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val() + '&Isdept=1&IsApprover=<% =isapprover%>&RiskId=-1',--%>
                            url: '<% =Path%>Data/GetDeptOverdueComplianceDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&FlagStatus=-1&FlagIsApp=PRA',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        model: {
                            fields: {
                                ComplianceID: { type: "string", },
                                ScheduledOn: { type: "date" },
                                CloseDate: { type: "date" }
                            }
                        },
                        data: function (response) {
                            return response[0].StatutoryList;
                        },
                        total: function (response) {
                            return response[0].StatutoryList.length;
                        } 
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },

                toolbar: kendo.template($("#template").html()),
            //    height: 513,
                sortable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                groupable:true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBound: function (e) {
                    record = 0;
                    total = this.dataSource._total;

                    if (this.dataSource.pageSize() == undefined || this.dataSource.pageSize() == null) {
                        this.dataSource.pageSize(this.dataSource._total);
                    }
                    if (this.dataSource.pageSize() > 10 && this.dataSource.pageSize() > 20 && this.dataSource.pageSize() != this.dataSource._total) {
                        if (this.dataSource._total <= 10) {
                            this.dataSource.pageSize(10)

                        }
                        else if (this.dataSource._total >= 10 && this.dataSource._total <= 20) {
                            this.dataSource.pageSize(20)
                        }
                        else {
                            this.dataSource.pageSize(total);
                        }
                    }
                    if (this.dataSource.pageSize() < 10 && this.dataSource.pageSize() != total) {
                        this.dataSource.pageSize(10)
                    }
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                   
                },
                columns: [
                        {
                            template: "<input name='sel_chkbx' id='sel_chkbx' type='checkbox' value=#=ComplianceInstanceID# >",
                            filterable: false, sortable: false,
                            headerTemplate: "<input type='checkbox' id='chkAll'  />",
                            width: "4%;", //lock: true                         
                        },

              {
                  hidden:true,
                  field: "RiskCategory", title: 'Risk',
                  width: "5s%;",
                  attributes: {
                      style: 'white-space: nowrap;'

                  }, filterable: {
                      multi: true,
                      extra: false,
                      search: true,
                      operators: {
                          string: {
                              eq: "Is equal to",
                              neq: "Is not equal to",
                              contains: "Contains"
                          }
                      }
                  }
              },
                    {
                        field: "Branch", title: 'Location',
                        width: "17%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "18%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                       {
                           hidden:true,
                           field: "ComplianceID", title: 'Compliance ID',
                           width: "5%",
                           attributes: {
                               style: 'white-space: nowrap;'
                           },
                           filterable: {
                               multi: true,
                               extra: false,
                               search: true,
                               operators: {
                                   string: {
                                       eq: "Is equal to",
                                       neq: "Is not equal to",
                                       contains: "Contains"
                                   }
                               }
                           }
                       },
                
                  {
                      field: "ScheduledOn", title: 'Due Date',
                      type: "date",
                      format: "{0:dd-MMM-yyyy}",
                      filterable: {
                          multi: true,
                          extra: false,
                          search: true,
                          operators: {
                              string: {
                                  type: "date",
                                  format: "{0:dd-MMM-yyyy}",
                                  eq: "Is equal to",
                                  neq: "Is not equal to",
                                  contains: "Contains"
                              }
                          }
                      },width:"13%"
                  },
                    {
                        field: "OverdueBy", title: 'OverdueBy(Days)', filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                      {
                          field: "Status", title: 'Status', filterable: {
                              multi: true,
                              extra: false,
                              search: true,
                              width: "18%",
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                      },
                    {
                        field: "ForMonth", title: 'Period', filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            width: "25%",
                            // width: 120,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                       {
                           field: "DepartmentName", title: 'Department',
                           width: "10%",
                           attributes: {
                               style: 'white-space: nowrap;'
                           },
                           filterable: {
                               multi: true,
                               extra: false,
                               search: true,
                               operators: {
                                   string: {
                                       eq: "Is equal to",
                                       neq: "Is not equal to",
                                       contains: "Contains"
                                   }
                               }
                           }
                       },
                       {
                           hidden:true,
                           field: "Remark", title: 'Remark',
                           width: "10%",
                           attributes: {
                               style: 'white-space: nowrap;'
                           },
                           filterable: {
                               multi: true,
                               extra: false,
                               search: true,
                               operators: {
                                   string: {
                                       eq: "Is equal to",
                                       neq: "Is not equal to",
                                       contains: "Contains"
                                   }
                               }
                           }
                       },
                         {
                             hidden:true,
                             field: "PerformerName", title: 'Performer',
                             width: "10%",
                             attributes: {
                                 style: 'white-space: nowrap;'
                             },
                             filterable: {
                                 multi: true,
                                 extra: false,
                                 search: true,
                                 operators: {
                                     string: {
                                         eq: "Is equal to",
                                         neq: "Is not equal to",
                                         contains: "Contains"
                                     }
                                 }
                             }
                         },
                           {
                               hidden:true,
                               field: "ReviewerName", title: 'Reviewer',
                               width: "10%",
                               attributes: {
                                   style: 'white-space: nowrap;'
                               },
                               filterable: {
                                   multi: true,
                                   extra: false,
                                   search: true,
                                   operators: {
                                       string: {
                                           eq: "Is equal to",
                                           neq: "Is not equal to",
                                           contains: "Contains"
                                       }
                                   }
                               }
                           },
                    {
                        command: [
                            {
                                name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview",
                            }
                        ], title: "Action", lock: true, width: "7%;",// width: 150,
                    }
                ]
            });

                $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
                    debugger;
                    var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                    OpenOverViewpupMain(item.ScheduledOnID, item.ComplianceInstanceID);
                    return true;
                });
                $("#grid").kendoTooltip({
                    filter: ".k-grid-edit2",
                    content: function (e) {
                        return "Overview";
                    }
                });
                $('#grid').kendoTooltip({
                    filter: "td[role=gridcell]",  //what element
                    content: function (e) {
                        var content = e.target.context.textContent;
                        if (content != "") {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                        else
                            e.preventDefault();
                    },
                    beforeShow: function (e) {
                        var target = e.target;

                        var isTextBiggerThanElement = findOutSize(target);
                        if (!isTextBiggerThanElement) {
                            e.preventDefault();
                        }
                    },
                    position: 'down'
                }).data("kendoTooltip");

                $('#grid').kendoTooltip({
                    filter: "th[role=columnheader]",  //what element
                    content: function (e) {
                        var content = e.target.context.textContent;
                        if (content != "") {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                        else
                            e.preventDefault();
                    },
                    beforeShow: function (e) {
                        var target = e.target;

                        var isTextBiggerThanElement = findOutSize(target);
                        if (!isTextBiggerThanElement) {
                            e.preventDefault();
                        }
                    },
                    position: 'down'
                }).data("kendoTooltip");

                $('#grid').kendoTooltip({
                    filter: "th[data-role=droptarget]",  //what element
                    content: function (e) {
                        var content = e.target.context.textContent;
                        if (content != "") {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                        else
                            e.preventDefault();
                    },
                    beforeShow: function (e) {
                        var target = e.target;

                        var isTextBiggerThanElement = findOutSize(target);
                        if (!isTextBiggerThanElement) {
                            e.preventDefault();
                        }
                    },
                    position: 'down'
                }).data("kendoTooltip");

                $('#grid').kendoTooltip({
                    filter: "td[colspan=7]",  //what element
                    content: function (e) {
                        var content = e.target.context.textContent;
                        if (content != "") {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                        else
                            e.preventDefault();
                    },
                    position: 'down'
                }).data("kendoTooltip");

                $('#grid').kendoTooltip({
                    filter: "td[colspan=8]",  //what element
                    content: function (e) {
                        var content = e.target.context.textContent;
                        if (content != "") {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                        else
                            e.preventDefault();
                    },
                    position: 'down'
                }).data("kendoTooltip");

                $('#grid').kendoTooltip({
                    filter: "td[colspan=9]",  //what element
                    content: function (e) {
                        var content = e.target.context.textContent;
                        if (content != "") {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                        else
                            e.preventDefault();
                    },
                    position: 'down'
                }).data("kendoTooltip");

                $('#grid').kendoTooltip({
                    filter: "td[colspan=10]",  //what element
                    content: function (e) {
                        var content = e.target.context.textContent;
                        if (content != "" && content != "  ") {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                        else
                            e.preventDefault();
                    },
                    position: 'down'
                }).data("kendoTooltip");

                $('#grid').kendoTooltip({
                    filter: "td[colspan=11]",  //what element
                    content: function (e) {
                        var content = e.target.context.textContent;
                        if (content != "") {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                        else
                            e.preventDefault();
                    },
                    position: 'down'
                }).data("kendoTooltip");

                $('#grid').kendoTooltip({
                    filter: "td[colspan=12]",  //what element
                    content: function (e) {
                        var content = e.target.context.textContent;
                        if (content != "") {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                        else
                            e.preventDefault();
                    },
                    position: 'down'
                }).data("kendoTooltip");

                $('#grid').kendoTooltip({
                    filter: "td[colspan=13]",  //what element
                    content: function (e) {
                        var content = e.target.context.textContent;
                        if (content != "") {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                        else
                            e.preventDefault();
                    },
                    position: 'down'
                }).data("kendoTooltip");
                $('#grid').kendoTooltip({
                    filter: "td[colspan=14]",  //what element
                    content: function (e) {
                        var content = e.target.context.textContent;
                        if (content != "") {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                        else
                            e.preventDefault();
                    },
                    position: 'down'
                }).data("kendoTooltip");
                $('#grid').kendoTooltip({
                    filter: "td[colspan=15]",  //what element
                    content: function (e) {
                        var content = e.target.context.textContent;
                        if (content != "") {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                        else
                            e.preventDefault();
                    },
                    position: 'down'
                }).data("kendoTooltip");
                $('#grid').kendoTooltip({
                    filter: "td[colspan=16]",  //what element
                    content: function (e) {
                        var content = e.target.context.textContent;
                        if (content != "") {
                            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                        }
                        else
                            e.preventDefault();
                    },
                    position: 'down'
                }).data("kendoTooltip");

                $(document).on("click", "#chkAll", function (e) {

                    if ($('input[id=chkAll]').prop('checked')) {

                        $('input[name="sel_chkbx"]').each(function (i, e) {
                            e.click();
                        });
                    }
                    else {
                        $('input[name="sel_chkbx"]').attr("checked", false);
                    }
                    if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                        $('#dvbtnSubmit').css('display', 'none');
                        $('#dvbtnNotApplicableSubmit').css('display', 'none');
                        $('#dvbtnNotCompliedSubmit').css('display', 'none');
                    }
                    else {
                        if ($("#dropdownlistUserRole").val() != "4") {
                            $('#dvbtnSubmit').css('display', 'block');
                            $('#dvbtnNotApplicableSubmit').css('display', 'block');
                            $('#dvbtnNotCompliedSubmit').css('display', 'none');
                        }
                    }
                    return true;
                });

                $(document).on("click", "#chkAllMain", function (e) {

                    if ($('input[id=chkAllMain]').prop('checked')) {
                        $('input[name="sel_chkbxMain"]').each(function (i, e) {
                            e.click();
                        });
                    }
                    else {

                        $('input[name="sel_chkbxMain"]').attr("checked", false);
                    }
                    if (($('input[name="sel_chkbxMain"]:checked').length) == 0) {
                        $('#dvbtnSubmitMain').css('display', 'none');
                        $('#dvbtnNotApplicableMain').css('display', 'none');
                        $('#dvbtnNotCompliedMain').css('display', 'none');
                    }
                    else {
                        if ($("#dropdownlistUserRole1").val() != "4") {
                            $('#dvbtnSubmitMain').css('display', 'block');
                            $('#dvbtnNotApplicableMain').css('display', 'block');
                            $('#dvbtnNotCompliedMain').css('display', 'none');

                        }
                    }
                    return true;
                });

                $(document).on("click", "#sel_chkbx", function (e) {
                    if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                        $('#dvbtnSubmit').css('display', 'none');
                        $('#dvbtnNotApplicableSubmit').css('display', 'none');
                        $('#dvbtnNotCompliedSubmit').css('display', 'none');
                    }
                    else {
                        if ($("#dropdownlistUserRole").val() != "4") {
                            $('#dvbtnSubmit').css('display', 'block');
                            $('#dvbtnNotApplicableSubmit').css('display', 'block');
                            $('#dvbtnNotCompliedSubmit').css('display', 'none');

                        }
                    }
                    return true;
                });

                $(document).on("click", "#sel_chkbxMain", function (e) {
                    if (($('input[name="sel_chkbxMain"]:checked').length) == 0) {
                        $('#dvbtnSubmitMain').css('display', 'none');
                        $('#dvbtnNotApplicableMain').css('display', 'none');
                        $('#dvbtnNotCompliedMain').css('display', 'none');
                    }
                    else {
                        if ($("#dropdownlistUserRole1").val() != "4") {
                            $('#dvbtnSubmitMain').css('display', 'block');
                            $('#dvbtnNotApplicableMain').css('display', 'block');
                            $('#dvbtnNotCompliedMain').css('display', 'none');

                        }
                    }
                    return true;
                });

            }
        
           
            //    });
        
           
      <%--   <%if (isapprover == 1)%><%{%>--%>
        function youFunction()
        {
            editAll();
            var theGrid = $("#gridremark").data("kendoGrid");
            var gridData = theGrid.dataSource._data;
            debugger;
            for (var i = 0; i < gridData.length; i++) {
                gridData[i].Remark = $("#txtremark").val();
            }
            $("#gridremark").data("kendoGrid").refresh();
        }
    <%--     <%}%>--%>
        
        function Bindremarkgrid(prm)
        {  
            debugger;
            $("#txtCompliaceType").val('');
            if (prm == 1)
            {                
                CTypeID = $('#dropdownlistComplianceType').val();
            }
          
            var record = 0;
            var total = 0;

                             
            var ddlDataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: '<% =Path%>Data/GetComplianceRemark',
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                    },
                }
            }
        });
      
            var grid = $("#gridremark").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetDeptOverdueComplianceDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&FlagStatus='+ CTypeID+ '&FlagIsApp=' + $("#dropdownlistUserRole").val(),
                           
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }                        
                    },
                    schema: {
                      
                        model: {
                            id: "ID",
                            fields: {
                                Branch:{ editable:false },
                                ShortDescription:{ editable:false },
                                ForMonth: { editable:false },
                                Remark: { validation: { required: true, } },
                            }
                        },
                        data: function (response) {
                            if (CTypeID == -1) {
                                return response[0].StatutoryList;
                            }
                            else if (CTypeID == 0)
                            {
                                return response[0].InternalList;
                            }
                        },
                        total: function (response) {
                            if (CTypeID == -1) {
                                return response[0].StatutoryList.length;
                            }
                            else if (CTypeID == 0) {
                                return response[0].InternalList.length;
                            }
                           
                        }
                  
                    },
                    pageSize: 10
                },
                dataBound: function (e) {
                    editAll();
                    var grid = e.sender;
                    var items = e.sender.items();

                    items.each(function (e) {
                        var dataItem = grid.dataItem(this);
                        var ddt = $(this).find('.dropDownTemplate');

                        $(ddt).kendoDropDownList({
                            value: dataItem.value,
                            optionLabel:"Select",
                            dataSource: ddlDataSource,
                            dataTextField: "Name",
                            dataValueField: "ID",
                            change: onDDLChange
                        });
                    });
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
               {
                   field: "Branch", title: 'Location',
                   width: "20%",
                   attributes: {
                       style: 'white-space: nowrap;'
                   },
                   filterable: {
                       multi: true,
                       extra: false,
                       search: true,
                       operators: {
                           string: {
                               eq: "Is equal to",
                               neq: "Is not equal to",
                               contains: "Contains"
                           }
                       }
                   }
               },
                       {
                           field: "ShortDescription", title: 'Compliance',
                           width: "20%",
                           attributes: {
                               style: 'white-space: nowrap;'
                           },
                           filterable: {
                               multi: true,
                               extra: false,
                               search: true,
                               operators: {
                                   string: {
                                       eq: "Is equal to",
                                       neq: "Is not equal to",
                                       contains: "Contains"
                                   }
                               }
                           }
                       },
                       
               {
                   field: "ForMonth", title: 'Period',
                   width: "20%",
                   attributes: {
                       style: 'white-space: nowrap;'
                   },
                   filterable: {
                       multi: true,
                       extra: false,
                       search: true,
                       operators: {
                           string: {
                               eq: "Is equal to",
                               neq: "Is not equal to",
                               contains: "Contains"
                           }
                       }
                   }
               },
                   {
                       // field: "Remark",
                       template: "<input data-bind='value:Remark' style='width: 100%;'  class='k-textbox' />",
                       title: "Remark",
                       width: "18.7%;",
                   },
           
                ]
            });
            $('#gridremark').kendoTooltip({
                filter: "td[role=gridcell]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                beforeShow: function (e) {
                    var target = e.target;

                    var isTextBiggerThanElement = findOutSize(target);
                    if (!isTextBiggerThanElement) {
                        e.preventDefault();
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#gridremark').kendoTooltip({
                filter: "th[role=columnheader]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                beforeShow: function (e) {
                    var target = e.target;

                    var isTextBiggerThanElement = findOutSize(target);
                    if (!isTextBiggerThanElement) {
                        e.preventDefault();
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

            $('#gridremark').kendoTooltip({
                filter: "th[data-role=droptarget]",  //what element
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                },
                beforeShow: function (e) {
                    var target = e.target;

                    var isTextBiggerThanElement = findOutSize(target);
                    if (!isTextBiggerThanElement) {
                        e.preventDefault();
                    }
                },
                position: 'down'
            }).data("kendoTooltip");

                function columnTemplateFunction(dataItem) {
                    var input = '<input class="dropDownTemplate"/>'

                    return input
                };
                function onDDLChange(e) {
                    var element = e.sender.element;
                    var row = element.closest("tr");
                    var grid = $("#gridremark").data("kendoGrid");
                    var dataItem = grid.dataItem(row);

                    dataItem.set("value", e.sender.value());
                };

                var items = []; 
                if (prm == 1) 
                {
                    var grid = $("#grid").data("kendoGrid");
                    // Get selected rows
                    var sel = $("input:checked", grid.tbody).closest("tr");
                    // Get data item for each              
                    $.each (sel, function(idx, row) {
                        var item = grid.dataItem(row);
                        items.push(item.ScheduledOnID);
                    });
                }
                else
                {
                    var grid = $("#grid1").data("kendoGrid");
                    // Get selected rows
                    var sel = $("input:checked", grid.tbody).closest("tr");
                    // Get data item for each              
                    $.each (sel, function(idx, row) {
                        var item = grid.dataItem(row);
                        items.push(item.ScheduledOnID);
                    });
                }
         
                var finalSelectedfilter = { logic: "and", filters: [] };
                var InstancecIdFilter = { logic: "or", filters: [] };
                $.each(items, function (i, v) {
                    InstancecIdFilter.filters.push({
                        field: "ScheduledOnID", operator: "eq", value: parseInt(v)
                    });
                });
                finalSelectedfilter.filters.push(InstancecIdFilter);
                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#gridremark").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#gridremark").data("kendoGrid").dataSource.filter({});
                }
            
<%--             <%if (isapprover == 1)%><%{%>--%>
            $("#txtremark").val('');
<%--             <%}%>--%>

        }

   

        //function brandName(Remark1) {
        //    debugger
        //    for (var i = 0; i < ddlDataSource.length; i++) {
        //        debugger
        //        if (ddlDataSource[i].Name == Remark1) {
        //            return ddlDataSource[i].Name;
        //        }
        //    }
        //}
        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();
            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1');
            fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk');
            fCreateStoryBoard('dropdownlistStatus1', 'filterstatus', 'status');
            fCreateStoryBoard('dropdownDept', 'filterdept', 'dept');


        };
        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

           
            if (div == 'filtersstoryboard1') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
         
            else if (div == 'filterrisk1') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterdept') {
                $('#' + div).append('Department&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterstatus') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
           
        
            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB;height: 20px;Color:Gray;margin-left:5px;margin-bottom: 4px;border-radius:10px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="Clear" aria-label="Clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" title="Clear" aria-label="Clear" style="font-size: 12px;"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }


            CheckFilterClearorNot();

            CheckFilterClearorNotMain();
        }
        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistRisk1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistStatus1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) && 
                ($($($('#dropdownDept').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }

        function CheckFilterClearorNot() {
            if (($($($('#dropdowntree1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                //($($($('#dropdownACT').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistRisk1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownUser').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                //($($($('#dropdownFY').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                //($($($('#dropdownPastData').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                //($($($('#dropdownlistComplianceType1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistStatus1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                   ($($($('#dropdownDept').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#Clearfilter').css('display', 'none');
            }
        }
        function ClearAllFilter() {
            debugger;
            $("#dropdowntree1").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk1").data("kendoDropDownTree").value([]);
            $("#dropdownDept").data("kendoDropDownTree").value([]);
            $("#dropdownlistStatus1").data("kendoDropDownTree").value([]);
            $('#Clearfilter').css('display', 'none');
            $("#grid").data("kendoGrid").dataSource.filter({});
            $('input[id=chkAll]').prop('checked', false);
            var dataSource = $("#grid").data("kendoGrid").dataSource;
            dataSource.pageSize(10);
        }


        function editAll() {
            var theGrid = $("#gridremark").data("kendoGrid");
            $("#gridremark tbody").find('tr').each(function () {
                var model = theGrid.dataItem(this);
                kendo.bind(this, model);
            });
            $("#gridremark").focus();
        }
        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }
      
        function selectedToSubmit(e) {
            debugger;
       <%--  <%if (isapprover == 1)%><%{%>--%>
           
            
            $('#dvsubmit').css('display', 'block');

            var myWindowAdv = $("#divApproveSearchModel");

            function onClose() {
                //   ClearAllFilter();
                $('input[name="sel_chkbx"]').attr("checked", false);
                $('input[id=chkAll]').prop('checked', false);
                $('#dvbtnSubmit').css('display', 'none');

            }

            myWindowAdv.kendoWindow({
                width: "60%",
                height: "60%",
                title: "Add Remark",
                visible: false,
                actions: [
                    //"Pin",
                    //"Minimize",
                    "Maximize",
                    "Close"
                ],
                refresh: true,
                close: onClose
            });
            $("#divApproveSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

            myWindowAdv.data("kendoWindow").center().open();
            Bindremarkgrid(1);
            e.preventDefault();
            return false;
         <%--  <%}%>--%>
           <%-- <%else%><%{%> 

            if (($('input[name="sel_chkbx"]:checked').length) == 0) {

                return;
            }
            var checkboxlist = [];
            $('input[name="sel_chkbx"]').each(function (i, e) {
                if ($(e).is(':checked')) {
                    checkboxlist.push(e.value);
                }
            });
            console.log(checkboxlist.join(","));
            alert("Saved Successfully.");
            //$('#downloadfile').attr('src', "../Compliances/ChecklistPerform.aspx?ComplianceScheduleID=" + checkboxlist.join(",") + "&IsFlag=" + $("#dropdownlistComplianceType").val() + "&IsApplicable=1");
            return false;
              <%}%>--%>
        }

        function selectedToNotApplicableSubmit(e) {
           
            <%if (isapprover == 1)%><%{%>
           
            
            $('#dvsubmit').css('display', 'none');
            $('#dvNotApplicable').css('display', 'block');         

            var myWindowAdv = $("#divApproveSearchModel");

            function onClose() {
                $('input[id=chkAllMain]').prop('checked', false);
                $('input[id=chkAll]').prop('checked', false);
               // ClearAllFilter();
            }

            myWindowAdv.kendoWindow({
                width: "60%",
                height: "60%",
                title: "Add Remark",
                visible: false,
                actions: [
                    //"Pin",
                    //"Minimize",
                    "Maximize",
                    "Close"
                ],
                refresh: true,
                close: onClose
            });
            $("#divApproveSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

            myWindowAdv.data("kendoWindow").center().open();
            Bindremarkgrid(1);
            //BindCategory();
            e.preventDefault();
            return false;
            <%}%>
            <%else%><%{%> 


            if (($('input[name="sel_chkbx"]:checked').length) == 0) {

                return;
            }
            var checkboxlist = [];
            $('input[name="sel_chkbx"]').each(function (i, e) {
                if ($(e).is(':checked')) {
                    checkboxlist.push(e.value);
                }
            });
            console.log(checkboxlist.join(","));
            alert("Saved Successfully.");
            //$('#downloadfile').attr('src', "../Compliances/ChecklistPerform.aspx?ComplianceScheduleID=" + checkboxlist.join(",") + "&IsFlag=" + $("#dropdownlistComplianceType").val() + "&IsApplicable=0");
            return false;
               <%}%>
        }
        function selectedsubmited(e)
        {
            debugger;
            e.preventDefault();
            var dataSource = $("#gridremark").data("kendoGrid").dataSource;
            var filters = dataSource.filter();
            var allData = dataSource.data();
            var query = new kendo.data.Query(allData);
            var data = query.filter(filters).data;
            var validationflag=1;
            var things = [];
            for (var i = 0; i < data.length; i++) 
            {
                data[i].UserID=<% =UId%>;
                if (dataSource._view[i].remark == "")
                {
                    validationflag=0;
                }
                data[i].remark=dataSource._view[i].remark;
                // data[i].ComplianceTypeID = $("#txtCompliaceType").val();
                things.push(data[i]);
            }   
            if (validationflag == 1) 
            {
                $.ajax({
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: 'POST',
                    url: '<% =Path%>/Data/MultipleComplianceRemark?IsFlag=' + $("#dropdownlistComplianceType").val(),
                    data: JSON.stringify(things),
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                    },
                    success: function (data) {
                        debugger;
                        if (data[0].Message == "Save") {
                            alert('Record Saved Successfully.');  
                            $('#divApproveSearchModel').data("kendoWindow").close();                        
                             $("#grid").data("kendoGrid").dataSource.read();
                               $("#grid").data("kendoGrid").refresh();
                            //   $('#grid').data('kendoGrid')._selectedIds = {};
                               $('#grid').data('kendoGrid').clearSelection();  
                               $('#dvbtnSubmit').css('display', 'none');
                        }
                        else  if (data[0].Message == "Error") {
                            alert('Please select Remark for all rows.');
                            $('#divApproveSearchModel').data("kendoWindow").open();  
                        }
                    },
                    failure: function (response) {
                        $('#result').html(response);
                    }
                });
                }
                else
                {
                    alert('Please select Remark for all rows.');
                }
            }

            function selectedToNotCompliedSubmit(e) {
                if (($('input[name="sel_chkbx"]:checked').length) == 0) {
                    return;
                }
                var checkboxlist = [];
                $('input[name="sel_chkbx"]').each(function (i, e) {
                    if ($(e).is(':checked')) {
                        checkboxlist.push(e.value);
                    }
                });
                console.log(checkboxlist.join(","));

                //  $('#downloadfile').attr('src', "../Compliances/ChecklistPerform.aspx?ComplianceScheduleID=" + checkboxlist.join(",") + "&IsFlag=" + $("#dropdownlistComplianceType").val() + "&IsApplicable=2");
                return false;
            }
            function OpenOverViewpupMain(scheduledonid, instanceid) {
                debugger;
                $('#divApiOverView').modal('show');
                $('#APIOverView').attr('width', '1000px');
                $('#APIOverView').attr('height', '600px');
                $('.modal-dialog').css('width', '1130px');
                if ($("#dropdownlistComplianceType").val() == -1) 
                {
                    $('#APIOverView').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                }
                else
                {
                    $('#APIOverView').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
                }

            }
            function DataBindDaynamicKendoGriddMain() {
                if ($("#dropdownlistComplianceType").val() == -1) {
                    var dataSource = new kendo.data.DataSource({
                        transport: {
                            read: {
                                url: '<% =Path%>Data/GetDeptOverdueComplianceDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&FlagStatus=-1&FlagIsApp=' + $("#dropdownlistUserRole").val(),
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                        },
                        schema:
                       {
                           model: {
                               fields: {
                                   ComplianceID: { type: "string", },
                                   ScheduledOn: { type: "date" },
                                   CloseDate: { type: "date" }
                               }
                           },
                           data: function (response) {
                               return response[0].StatutoryList;
                           },
                           total: function (response) {
                               return response[0].StatutoryList.length;
                           }
                       },
                       
                        pageSize: 10
                    });
                        var grid = $('#grid').data("kendoGrid");
                //    dataSource.read();
                        grid.setDataSource(dataSource);
                    }
                    if ($("#dropdownlistComplianceType").val() == 0) {
                        var dataSource = new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: '<% =Path%>Data/GetDeptOverdueComplianceDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&FlagStatus=0&FlagIsApp=' + $("#dropdownlistUserRole").val(),
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                        },
                        schema: {
                            model: {
                                fields: {
                                    ComplianceID: { type: "string", },
                                    ScheduledOn: { type: "date" },
                                    CloseDate: { type: "date" }
                                }
                            },
                            data: function (response) {
                                return response[0].InternalList;
                            },
                            total: function (response) {
                                return response[0].InternalList.length;
                            }
                        },
                     
                        pageSize: 10                                                                                                                    
                    });
                        var grid = $('#grid').data("kendoGrid");
                   //    dataSource.read();
                        grid.setDataSource(dataSource);
                    }
                }
                function FilterGrid() {
                    debugger;
                    //risk Details
                    var Riskdetails = [];
                    if ($("#dropdownlistRisk1").data("kendoDropDownTree") != undefined) {
                        Riskdetails = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                    }

                    //location details
                    var locationsdetails = [];
                    if ($("#dropdowntree1").data("kendoDropDownTree") != undefined) {
                        locationsdetails = $("#dropdowntree1").data("kendoDropDownTree")._values;
                    }     
                    var Departmentdetails = [];
                    if ($("#dropdownDept").data("kendoDropDownTree") != undefined) {
                        Departmentdetails = $("#dropdownDept").data("kendoDropDownTree")._values;
                    }  
                    var Statusdetails = [];
                    if ($("#dropdownlistStatus1").data("kendoDropDownTree") != undefined) {
                        Statusdetails = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                    }  
                    var finalSelectedfilter = { logic: "and", filters: [] };

                    if (Riskdetails.length > 0 || locationsdetails.length > 0 || Departmentdetails.length > 0 || Statusdetails.length > 0) {

                        if (locationsdetails.length > 0) {
                            var LocationFilter = { logic: "or", filters: [] };

                            $.each(locationsdetails, function (i, v) {
                                LocationFilter.filters.push({
                                    field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                                });
                            });

                            finalSelectedfilter.filters.push(LocationFilter);
                        }
                        if (Riskdetails.length > 0) {
                            var CategoryFilter = { logic: "or", filters: [] };

                            $.each(Riskdetails, function (i, v) {

                                CategoryFilter.filters.push({
                                    field: "Risk", operator: "eq", value: parseInt(v)
                                });
                            });

                            finalSelectedfilter.filters.push(CategoryFilter);
                        }
                        if (Departmentdetails.length > 0) {
                            var DepartmentFilter = { logic: "or", filters: [] };

                            $.each(Departmentdetails, function (i, v) {
                                DepartmentFilter.filters.push({
                                    field: "DepartmentID", operator: "eq", value: parseInt(v)
                                });
                            });
                            finalSelectedfilter.filters.push(DepartmentFilter);
                        }
                        if (Statusdetails.length > 0) {
                            var StatusFilter = { logic: "or", filters: [] };

                            $.each(Statusdetails, function (i, v) {
                                StatusFilter.filters.push({
                                    field: "Status", operator: "eq", value: v
                                });
                            });
                            finalSelectedfilter.filters.push(StatusFilter);
                        }
                        if (finalSelectedfilter.filters.length > 0) {
                            var dataSource = $("#grid").data("kendoGrid").dataSource;
                            dataSource.filter(finalSelectedfilter);
                        }
                        else {
                            $("#grid").data("kendoGrid").dataSource.filter({});
                        }
                    }
                    else {
                        $("#grid").data("kendoGrid").dataSource.filter({});
                    }
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    if (dataSource._total > 20 && dataSource.pageSize == undefined) {
                        dataSource.pageSize(total);
                    }

                }
                function exportReport(e) {
     
                    var ReportName = "Compliance Overdue Report";
                       var customerName = document.getElementById('CustName').value;
                       var todayDate = moment().format('DD-MMM-YYYY');
                    //   var todayDate = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
                    var grid = $("#grid").getKendoGrid();
                    debugger;
                    var rows =
                        [
                              {
                                  cells: [
                                      { value: "Entity/ Location:", bold: true },
                                      { value: customerName }
                                  ]
                              },
                         {
                             cells: [
                                 { value: "Report Name:", bold: true },
                                 { value: ReportName }
                             ]
                         },
                         {
                             cells: [
                                 { value: "Report Generated On:", bold: true },
                                 { value: todayDate }
                             ]
                         },
                         {
                             cells: [
                                 { value: "" }
                             ]
                         },
                        {
                            cells: [
                                { value: "Sr.No.", bold: true },
                                { value: "Compliance ID", bold: true },
                                { value: "Location", bold: true },
                                { value: "Short Description", bold: true },
                                { value: "Due Date", bold: true },
                                { value: "Overdue By(days)", bold: true },
                                { value: "Department", bold: true },
                                { value: "Status", bold: true },
                                { value: "Risk", bold: true },           
                                { value: "Period", bold: true },
                                { value: "Performer", bold: true },
                                { value: "Reviewer", bold: true },
                                { value: "Remark", bold: true },
                            ]
                        }
                        ];

                    var trs = grid.dataSource;
                    var filteredDataSource = new kendo.data.DataSource({
                        data: trs.data(),
                        filter: trs.filter()
                    });

                    filteredDataSource.read();
                    var data = filteredDataSource.view();
                    for (var i = 0; i < data.length; i++) {
                        var dataItem = data[i];
                        rows.push({
                            cells: [ // dataItem."Whatever Your Attributes Are"
                                { value: '' },
                                { value: dataItem.ComplianceID },
                                { value: dataItem.Branch },
                                { value: dataItem.ShortDescription },
                                { value: dataItem.ScheduledOn,format: "dd-MMM-yyyy" },
                                { value: dataItem.OverdueBy },
                                { value: dataItem.DepartmentName },
                                { value: dataItem.Status },
                                { value: dataItem.RiskCategory },
                                { value: dataItem.ForMonth },
                                { value: dataItem.PerformerName },
                                { value: dataItem.ReviewerName },
                                { value: dataItem.Remark },
                        
                            ]
                        });
                    }
                    for (var i = 4; i < rows.length; i++) {
                        for (var j = 0; j < 13; j++) {
                            rows[i].cells[j].borderBottom = "#000000";
                            rows[i].cells[j].borderLeft = "#000000";
                            rows[i].cells[j].borderRight = "#000000";
                            rows[i].cells[j].borderTop = "#000000";
                            rows[i].cells[j].hAlign = "left";
                            rows[i].cells[j].vAlign = "top";
                            rows[i].cells[j].wrap = true;

                            if (i != 4) {
                                rows[i].cells[0].value = i - 4;
                            }
                            if (i == 4) {
                                rows[4].cells[j].background = "#A9A9A9";
                            }
                        }
                    }
                    excelExport(rows, ReportName);
                    e.preventDefault();
                    return false;
                }

                function excelExport(rows, ReportName) {

                    var FileName = "Overdue Compliance Report";
                    var workbook = new kendo.ooxml.Workbook({
                        sheets: [
                            {
                                columns: [
                                    { width: 100 },
                                    { autoWidth: true },
                                    { width: 250 },
                                    { width: 200 },
                                    { width: 200 },
                                    { width: 200 },
                                    { width: 200 },
                                    { width: 200 },
                                    { width: 200 },
                                    { width: 200 },
                                    { width: 200 },
                                    { width: 200 },
                                    { width: 200 }
                                ],
                                title: FileName,
                                rows: rows
                            },
                        ]
                    });

                    var nameOfPage = FileName;
                    //var nameOfPage = "Test-1"; // insert here however you are getting name of screen
                    kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });
                    return false;
                }
     
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            fhead('Overdue Compliance');
            BindGrid();
            $("#dropdownlistUserRole").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                    ClearAllFilter();
                    settracknew('Detailed report', 'Filtering', 'Role', '');
                },
                dataSource: [
                 
                    { text: "Performer", value: "PRA" },
                    { text: "Reviewer", value: "REV" },
                    
             
                ]
            });
            $("#dropdownlistStatus1").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                change: function (e) {
                    FilterGrid();
                    fCreateStoryBoard('dropdownlistStatus1', 'filterstatus', 'status');
                   
                    $('input[id=chkAllMain]').prop('checked', false);
                },
                dataSource: [
                    { text: "Overdue", value: "Overdue" },
                    { text: "Pending For Review", value: "Pending For Review" }
                ]
            });
            $("#dropdowntree1").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //filter: "contains",
                checkAll: true,
                checkAllTemplate: "Select All",
                autoClose: false,
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterGrid();
                    // FilterAllAdvancedSearch();
                    fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1') ;
                    $('input[id=chkAll]').prop('checked', false);
               
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Flag%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }, dataBound: function (e) {
                    e.sender.list.width("350");
                }
             });
            $("#dropdownlistComplianceType").kendoDropDownList({
                placeholder: "Compliance Type",
                dataTextField: "text",
                dataValueField: "value",
                autoClose: true,
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Internal", value: "0" },
                ],
                index: 0,
                change: function (e) {
                    DataBindDaynamicKendoGriddMain();
                    ClearAllFilter();
                }
            });
            $("#dropdownDept").kendoDropDownTree({
                placeholder: "Department",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                checkAllTemplate: "Select All",

                change: function (e) {
                    //DataBindDaynamicKendoGriddMain();
                    FilterGrid();
                    fCreateStoryBoard('dropdownDept', 'filterdept', 'dept');
                    $('input[id=chkAllMain]').prop('checked', false);
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindDeptListNew?UId=<% =UId%>&CId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                    },
                        }
                
                    }
                }
                  });
            $("#dropdownlistRisk1").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    //   FilterAllAdvancedSearch();
                    FilterGrid();
                    fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtnSubmit').css('display', 'none');
                 
                },
                dataSource: [
                     { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });
            $("#dropdownlistComplianceType1").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    DataBindDaynamicKendoGrid();
                    ClearAllFilter();
                },
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Internal", value: "0" },
                    { text: "Event Based", value: "1" }
                ]
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example">
           <input id="CustName" type="hidden" value="<% =CustomerName%>" />

        <div id="divApproveSearchModel" style="display: none;">

            <div class="row">
                <div class="">
                    <div class="col-md-2" style="display: none;">
                        <input id="txtCompliaceType" type="text" class="k-textbox" />
                    </div>
                    <div class="col-md-6"></div>
                    <div class="col-md-4">
                        <%--                          <%if (isapprover == 1)%><%{%>--%>
                        <input id="txtremark" type="text" style="width: 129%;" onchange="youFunction();" class="k-textbox" />
                        <%--                           <%}%>--%>
                    </div>
                </div>
            </div>


            <div class="row" style="padding-top: 5px;">
                <div id="gridremark" style="width: 99%;"></div>
            </div>
            <div style="margin: 1% -0.2% 0.7%; width: 99%;">
                <button id="dvsubmit" class="k-button" onclick="selectedsubmited(event)" style="margin-left: 46%;">Save</button>
            </div>
        </div>

        <div class="row" style="margin-left: -9px;margin-bottom:10px">
            <div class="col-md-12 colpadding0">
                 <%if (DisplayOverviewIcon == false)%><%{ %> 
                 <input id="dropdownlistUserRole" data-placeholder="Role"   style="width: 237px;margin-right:10px;float:left;margin-left:10px;"/>
           
                <%}%> 
                 
                      <div class="col-md-2" id="dvdropdowntree1" style="width: 20%; padding-left: 9px;">
                    <input id="dropdowntree1" data-placeholder="Entity/Sub-Entity/Location" style="width: 100%;" />
                </div>

                <div class="col-md-2" id="divdropdownlistComplianceType" style="width: 15%; padding-left: 0px;">
                    <input id="dropdownlistComplianceType" data-placeholder="Type" style="width: 102%;" />
                </div>
                <div class="col-md-2" id="dvdropdownUser" style="width: 13%; padding-left: 4px;">
                    <input id="dropdownlistStatus1" data-placeholder="Status" style="width: 100%;" />
                </div>
                <div class="col-md-2" id="dvdropdownlistRisk1" style="width: 15%; padding-left: 0px;">
                    <input id="dropdownlistRisk1" data-placeholder="Risk" style="width: 100%;" />
                </div>
                <input id="dropdownDept" data-placeholder="Department" style="width: 145px; margin-left: 8px" />
                <button type="button" id="export" style="height: 30px; float: right; margin-top:10px" onclick="exportReport(event)"><span class="k-icon k-i-excel k-grid-edit3" style="margin-right: 2px;"></span>Export</button>
                <button type="button" id="Clearfilter" style="float: right; margin-right: 25px; height: 30px; display: none;margin-top:10px;margin-right:15px" onclick="ClearAllFilter()"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>

            </div>
        </div>
        <div class="col-md-1" style="width: 37%; float: right; margin-left: -6px;">
            <button id="dvbtnSubmit" style="float: right; display: none; margin-left: 15px;" onclick="selectedToSubmit(event)">Submit</button>
        </div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboard1">&nbsp;</div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterrisk1">&nbsp;</div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterdept">&nbsp;</div>
        <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterstatus">&nbsp;</div>
        <div id="grid" style="margin-top:40px"></div>

        <div class="modal fade" id="divApiOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 1150px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header" style="border-bottom: none;">
                        <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <iframe id="APIOverView" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>

    </div>
       <script type="text/javascript">
           $(document).ready(function () {
<%--               $("#dropdownlistUserRole").data("kendoDropDownList").value('<% =RoleKey%>');--%>

           });
           </script>
</asp:Content>
