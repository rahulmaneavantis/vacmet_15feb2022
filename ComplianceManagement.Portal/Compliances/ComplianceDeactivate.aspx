﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ComplianceDeactivate.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ComplianceDeactivate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        $(function () {
            $(document).tooltip();
        });

        function initializeDatePicker1(date) {
            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }

    </script>
    <style type="text/css">
        .label {
            display: inline-block;
            font-weight: normal;
            font-size: 12px;
        }

        .ui-tooltip {
            max-width: 700px;
            font-weight: normal;
            font-size: 12px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlComplinceCatagory" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplinceCatagory_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlFilterComplianceType" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterComplianceType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlFilterFrequencies" Style="padding: 0px; margin: 0px; height: 22px; width: 220px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterComplianceType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:RadioButton ID="rdFunctionBased" Text="Function Based" AutoPostBack="true" Width="110px" GroupName="ComplianceType" runat="server"
                            OnCheckedChanged="rdFunctionBased_CheckedChanged" />
                    </td>
                    <td>
                        <asp:RadioButton ID="rdChecklist" Text="Checklist" AutoPostBack="true" Width="100px" GroupName="ComplianceType" runat="server"
                            OnCheckedChanged="rdChecklist_CheckedChanged" />
                    </td>

                    <td style="width: 25%; padding-right: 20px;" align="right">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel1" Width="100%" Height="500px" ScrollBars="Vertical" runat="server">
                   <%--OnRowDataBound="grdCompliances_RowDataBound"--%>

                <asp:GridView runat="server" ID="grdCompliances" AutoGenerateColumns="false" GridLines="Vertical" 
                 
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" 
                    OnRowCreated="grdCompliances_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%" 
                    OnSorting="grdCompliances_Sorting"
                    Font-Size="12px" DataKeyNames="ID"
                     OnRowCommand="grdCompliances_RowCommand" OnPageIndexChanging="grdCompliances_PageIndexChanging">
                    <Columns>
                        <asp:BoundField DataField="ID" HeaderText="ID" ItemStyle-Width="50px" SortExpression="ID" />
                        <asp:TemplateField HeaderText="Act Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" ItemStyle-Width="100px" SortExpression="ActName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" Text='<%# Eval("ActName") %>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" SortExpression="ShortDescription">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>' CssClass="label"></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Sections" HeaderText="Sections" ItemStyle-Width="200px" SortExpression="Sections" />
                        <asp:TemplateField HeaderText="Upload Document" ItemStyle-Width="140px" SortExpression="UploadDocument">
                            <ItemTemplate>
                                <%# Convert.ToBoolean(Eval("UploadDocument")) ? "Yes" : "No" %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Risk" HeaderText="Risk Type" ItemStyle-Width="150px" SortExpression="Risk" />
                        <asp:BoundField DataField="FrequencyName" HeaderText="Frequency" ItemStyle-Width="180px" SortExpression="Frequency" />
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Status" ItemStyle-Width="50px">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# ComplianceActiveOrInActive(Convert.ToInt32(Eval("ID"))) %>' CommandArgument='<%# Eval("ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkStatus" runat="server" CommandName="STATUS" CommandArgument='<%# Eval("ID") %>'><img src="../Images/change_status_icon.png" alt="Status change"/></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divComplianceStatusDialog">
        <asp:UpdatePanel ID="upComplianceStatusDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetailsStatus_Load">
            <ContentTemplate>
                <div style="margin: 5px 5px 80px;">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup1" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" />
                        <asp:Label runat="server" ID="Label1" ForeColor="Red"></asp:Label>
                    </div>
                    <div style="margin-bottom: 7px; position: relative; display: inline-block;">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Act Name</label>
                        <asp:DropDownList runat="server" ID="ddlStatusAct" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                            CssClass="txtbox" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Section(s) / Rule(s)</label>
                        <asp:TextBox runat="server" ID="tbxSectionsStatus" Style="height: 16px; width: 390px;" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Short Description</label>
                        <asp:TextBox runat="server" ID="txtShortDescriptionStatus" TextMode="MultiLine" MaxLength="100" Style="height: 30px; width: 390px;" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Detailed Description</label>
                        <asp:TextBox runat="server" ID="tbxDescriptionStatus" TextMode="MultiLine" Style="height: 50px; width: 390px;" Enabled="false" />
                    </div>
                    <div style="margin-bottom: 7px" id="div2" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 190px; display: block; float: left; font-size: 13px; color: #333;">
                            Deactivate Date</label>
                        <asp:TextBox runat="server" CssClass="StartDate" ID="txtDeactivateDate" Style="height: 16px; width: 150px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" ErrorMessage="Please enter deactivate Date."
                            ControlToValidate="txtDeactivateDate" runat="server" ValidationGroup="ComplianceValidationGroup1"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">
                            *</label>
                        <label style="width: 190px; display: block; float: left; font-size: 13px; color: #333;">
                            Description</label>
                        <asp:TextBox runat="server" ID="txtDeactivateDesc" TextMode="MultiLine" Style="height: 50px; width: 390px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ErrorMessage="Deactivate Description can not be empty."
                            ControlToValidate="txtDeactivateDesc" runat="server" ValidationGroup="ComplianceValidationGroup1"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px" id="Div1" visible="false" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 190px; display: block; float: left; font-size: 13px; color: #333;">
                            Document</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        <asp:GridView runat="server" ID="grdFile" AutoGenerateColumns="false" AllowSorting="false"
                            GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" ShowFooter="true"
                            BorderWidth="1px" ForeColor="Black" AllowPaging="True" PageSize="5" DataKeyNames="FilePath,ComplianceID,Name"
                            Width="400px" Font-Size="12px">
                            <Columns>
                                <asp:BoundField DataField="Name" ItemStyle-Width="300px" HeaderText="Document" />
                                <asp:TemplateField HeaderText="" ItemStyle-Width="100px">
                                    <ItemTemplate>
                                        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkDownload" style="color: blue;" runat="server" Text="Download" OnClick="lnkDownload_Click"></asp:LinkButton>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkDownload" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                            <PagerSettings Position="Top" />
                            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                            <AlternatingRowStyle BackColor="#E6EFF7" />
                        </asp:GridView>
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 203px; margin-top: 70px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <asp:Button Text="Approve" runat="server" ID="btnSaveDeactivate" OnClick="btnSaveDeactivate_Click" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup1" OnClientClick="return confirm('Are you sure you want to deactivate this Compliance?');" />
                        <asp:Button Text="Reject" runat="server" ID="btnReject" OnClientClick="return confirm('Are you sure you want to reject deactivate this Compliance?');" OnClick="btnReject_Click" CssClass="button"
                            ValidationGroup="ComplianceValidationGroup1" />
                        <asp:Button Text="Close" runat="server" ID="Button3" CssClass="button" OnClientClick="$('#divComplianceStatusDialog').dialog('close');" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 30px;">
                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSaveDeactivate" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">

        $('#divComplianceStatusDialog').dialog({
            height: 550,
            width: 800,
            autoOpen: false,
            draggable: true,
            title: "Compliance Details",
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });

        function initializeCombobox() {
            $("#<%= ddlStatusAct.ClientID %>").combobox();
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkIndustry") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkIndustry']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkIndustry']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='IndustrySelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        function checkAllET(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkEntityType") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeaderET() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkEntityType']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkEntityType']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='EntityTypeSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
</asp:Content>

