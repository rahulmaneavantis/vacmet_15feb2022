﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RevComplianceStatusTransaction.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.RevComplianceStatusTransaction" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href='../NewCSS/bootstrap.min.css' rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href='../NewCSS/bootstrap-theme.css' rel="stylesheet" type="text/css" />

    <link href='../NewCSS/responsive-calendar.css' rel="stylesheet" type="text/css" />

    <link href='../NewCSS/font-awesome.min.css' rel="stylesheet" type="text/css" />
    <!-- full calendar css-->
    <link href='../assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css' rel="stylesheet" type="text/css" />
    <!-- owl carousel -->
    <link rel="stylesheet" href='../NewCSS/owl.carousel.css>' type="text/css" />
    <!-- Custom styles -->
    <link rel="stylesheet" href='../NewCSS/fullcalendar.css' type="text/css" />
    <link href='../NewCSS/stylenew.css' rel="stylesheet" type="text/css" />
    <link href='../NewCSS/jquery-ui-1.10.4.min.css' rel="stylesheet" type="text/css" />
    <link href='../Newjs/bxslider/jquery.bxslider.css' rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-ui-1.9.2.custom.min.js"></script>

      <link href="https://avacdn.azureedge.net/newcss/kendo.common1.2.min.css" rel="stylesheet" />
        <link href="https://avacdn.azureedge.net/newcss/kendo.rtl.min.css" rel="stylesheet" />
        <link href="https://avacdn.azureedge.net/newcss/kendo.silver.min.css" rel="stylesheet" />
        <link href="https://avacdn.azureedge.net/newcss/kendo.mobile.all.min.css" rel="stylesheet" />

        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jszip.min.js"></script>
        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/kendo.all.min.js"></script>

    <style type="text/css">
        .reviewdoc {
            height: 100%;
            width: auto;
            display: inline-block;
            font-size: 22px;
            position: relative;
            margin: 0;
            line-height: 34px;
            font-weight: 400;
            letter-spacing: 0;
            color: #666;
        }

        .circle {
            /*background-color: green;*/
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }

        tr.spaceUnder > td {
            padding-bottom: 1em;
        }
        .input-disabled {
            background-color: #f7f7f7;
            border: 1px solid #c7c7cc;            
            padding: 6px 12px;
        }
    </style>

        <style type="text/css">
            /*.k-window div.k-window-content {
                overflow: hidden;
            }*/

            div.k-widget.k-window {
                top: 724px !important;
            }

            .k-combobox-clearable .k-input, .k-dropdowntree-clearable .k-dropdown-wrap .k-input, .k-dropdowntree-clearable .k-multiselect-wrap, .k-multiselect-clearable .k-multiselect-wrap {
                padding-right: 2em;
                text-align: center;
                display: grid;
            }

            .k-dropdown, .k-textbox {
                text-align: center;
            }
            /*.div.k-grid-footer, div.k-grid-header {
            border-top-style: solid;
            border-top-width: 1px;
        }*/
            .k-grid-content {
                min-height: 150px !important;
            }

            html {
                color: #666666;
                font-size: 15px;
                font-weight: normal;
                font-family: 'Roboto',sans-serif;
            }

            .k-checkbox-label, .k-radio-label {
                display: inline;
            }

            .myKendoCustomClass {
                z-index: 999 !important;
            }

            .k-grid-toolbar {
                background: white;
            }

            #grid .k-grid-toolbar {
                padding: .6em 1.3em .6em .4em;
            }

            .k-grid td {
                line-height: 1em;
                border-bottom-width: 1px;
            }

            .k-i-more-vertical:before {
                content: "\e006";
            }

            .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
                background-color: #1fd9e1;
                background-image: none;
            }

            k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
                color: #000000;
                border-color: #1fd9e1;
                background-color: #f6f6f6;
            }

            .k-pager-wrap > .k-link > .k-icon {
                /*margin-top: 5px;*/
                color: inherit;
            }

            .toolbar {
                float: left;
            }

            html .k-grid tr:hover {
                background: #E4F7FB;
            }

            html .k-grid tr.k-alt:hover {
                background: #E4F7FB;
                min-height: 30px;
            }

            .k-grid tbody .k-button {
                min-width: 30px;
                min-height: 30px;
                border-radius: 35px;
            }

            .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
                margin-right: 0px;
                margin-right: 0px;
                margin-left: 0px;
                margin-left: 0px;
            }

            .k-auto-scrollable {
                overflow: hidden;
            }

            .k-grid-header {
                padding-right: 0px !important;
            }

            /*.k-multicheck-wrap .k-item {
                line-height: 1.2em;
                font-size: 14px;
                margin-bottom: 5px;
            }

            label {
                display: inline-block;
                margin-bottom: 0px;
            }*/

            .k-filter-menu .k-button {
                width: 27%;
            }

            .k-label input[type="checkbox"] {
                margin: 0px 5px 0 !important;
            }

            .k-filter-row th, .k-grid-header th.k-header {
                font-size: 15px;
                background: #f8f8f8;
                font-family: 'Roboto',sans-serif;
                color: #212121;
                font-weight: 400;
            }

            .k-primary {
                border-color: #1fd9e1;
                background-color: #1fd9e1;
            }

            .k-pager-wrap {
                background-color: #f8f8f8;
                color: #2b2b2b;
            }

            /*td.k-command-cell {
                border-width: 0 1px 1px 1px;
            }

            .k-grid-pager {
                border-width: 1px 1px 1px 1px;
            }*/

            span.k-icon.k-i-calendar {
                margin-top: 6px;
            }

            .col-md-2 {
                width: 20%;
            }

            /*.k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
                border-left-width: 1px;
                line-height: 14px;
            }*/

            .k-grid tbody tr {
                height: 38px;
            }

            .k-textbox .k-icon {
                top: 50%;
                margin: -8px 0 0;
                margin-left: 56px;
                position: absolute;
            }
              label.k-label {
                font-family: roboto,sans-serif !important;
                color: #515967;
                /* font-stretch: 100%; */
                font-style: normal;
                font-weight: 400;
                min-width: 362px;
                white-space: pre-wrap;
            }

            .k-multicheck-wrap .k-item {
                line-height: 1.2em;
                font-size: 14px;
                margin-bottom: 5px;
            }
            label {
                display: flex;
                margin-bottom: 0px;
            }
            .k-animation-container{

               width: 400px !important;
                white-space: pre-wrap;
                 /*height: 255px !important;*/
                /*left: 735px !important;*/
           
           }

          k-filter-menu k-popup k-group k-reset k-state-border-up {
                height: 255px !important;
           }

           .k-multicheck-wrap{
          max-height:125px !important;
      }
          
        </style>

      <script type="text/javascript">

    $(document).ready(function () {

        kendogridupdate();
       
    })

      function kendogridupdate() {
        debugger;
        var gridUpdate = $("#gridUpdate").kendoGrid({
            dataSource: {
                transport: {
                    read: {
                        url: '<% =KendoPath%>/Data/KendoComplianceOverviewUpdates?UserId=<% =UId%>&CustId=<% =CustId%>&complianceInstanceID=<% =compInstanceID%>',
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                            //read: '<% =KendoPath%>/Data/KendoComplianceOverviewUpdates?UserId=<% =UId%>&CustId=<% =CustId%>&complianceInstanceID=<% =compInstanceID%>'
                        },
                        pageSize: 5,
                    },
                    //height: 150,
                    sortable: true,
                    filterable: true,
                    columnMenu: false,
                    pageable: true,
                    reorderable: true,
                    resizable: true,
                    multi: true,

                    noRecords: {
                        template: "No records available"
                    },
                    columns: [
                        {
                            field: "Title", title: 'Title',
                            width: "65%;",
                            attributes: {
                                style: 'white-space: nowrap;'
                            }, filterable: { multi: true, search: true }
                        },
                        //{
                        //    field: "Date", title: 'Date',
                        //    type: "date",
                        //    template: "#= kendo.toString(kendo.parseDate(Date, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        //    attributes: {
                        //        style: 'white-space: nowrap;'
                        //        //}, filterable: { multi: true, search: true }
                        //    }, filterable: {
                        //        extra: false,
                        //        operators: {
                        //            string: {
                        //                eq: "Is equal to",
                        //                neq: "Is not equal to",
                        //                contains: "Contains"
                        //            }
                        //        }
                        //    },
                        //},
                         {
                             field: "Date", title: 'Date',
                             type: "date",
                             format: "{0:dd-MMM-yyyy}",
                             template: "#= kendo.toString(kendo.parseDate(Date, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                             attributes: {
                                 style: 'white-space: nowrap;'
                                 //}, filterable: { multi: true, search: true }
                             },
                             filterable: {
                                 multi: true,
                                 extra: false,
                                 search: true,
                                 operators: {
                                     string: {
                                         type: "date",
                                         format: "{0:dd-MMM-yyyy}",
                                         eq: "Is equal to",
                                         neq: "Is not equal to",
                                         contains: "Contains"
                                     }
                                 },
                             }
                         },
                        {
                            command: [
                                { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                            ], title: "Action", lock: true,// width: 150,
                        }
                    ]
                });

                $("#gridUpdate").kendoTooltip({
                    filter: ".k-grid-edit2",
                    content: function (e) {
                        return "Overview";
                    }
                });

                $("#gridUpdate").kendoTooltip({
                    filter: "td:nth-child(1)", //this filter selects the second column's cells
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        return content;
                    }
                }).data("kendoTooltip");

                $("#gridUpdate").kendoTooltip({
                    filter: "td:nth-child(2)", //this filter selects the second column's cells
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        return content;
                    }
                }).data("kendoTooltip");
    }

            $(document).on("click", "#gridUpdate tbody tr .ob-overview", function (e) {

                var item = $("#gridUpdate").data("kendoGrid").dataItem($(this).closest("tr"));
                //OpendocumentsUpdates(item.Title);
                OpendocumentsUpdates(item.Description);

                return true;
            });


            function OpendocumentsUpdates(title) {

                document.getElementById('detailUpdate').innerHTML = title;// 'your tip has been submitted!';


                var myWindowAdv = $("#ViewUpdateDetails");

                function onClose() {

                }

                myWindowAdv.kendoWindow({
                    width: "85%",
                    height: "50%",
                    title: "Legal Updates",
                    visible: false,
                    actions: [
                        //"Pin",
                        //"Minimize",
                        //"Maximize",
                        "Close"
                    ],

                    close: onClose
                });

                //$("#divAdvanceSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

                myWindowAdv.data("kendoWindow").center().open();
                e.preventDefault();
                return false;
            }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="divReviewerComplianceDetailsDialog">
            <asp:ScriptManager ID="Isdf" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upComplianceDetails1" runat="server" UpdateMode="Conditional"
                OnLoad="upComplianceDetails1_Load">
                <ContentTemplate>
                    <div style="margin: 5px">
                        <div style="margin-bottom: 4px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Style="padding-left: 5%" class="alert alert-block alert-danger fade in" ValidationGroup="ReviewerComplianceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" class="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                                ValidationGroup="ReviewerComplianceValidationGroup" Display="None" />
                            <asp:Label ID="Labelmsgrev" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
                            <asp:HiddenField runat="server" ID="hdnlicremindbeforenoofdays" />
                            <asp:HiddenField runat="server" ID="hdnlicensetypeid" />
                            <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />
                        </div>
                        <div>
                            <asp:Label ID="Label2" Text="This is a  " Style="width: 300px; font-weight: bold; font-size: 13px; color: #333;"
                                maximunsize="300px" autosize="true" runat="server" />
                            <div id="divRiskType1" runat="server" class="circle"></div>
                            <asp:Label ID="lblRiskType1" Style="width: 300px; margin-left: -17px; font-weight: bold; font-size: 13px; color: #333;"
                                maximunsize="300px" autosize="true" runat="server" />
                        </div>
                        <div id="ActDetails1" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails1">
                                                    <h2>Compliance Details</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails1"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseActDetails1" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                    <table style="width: 100%;">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Act Name</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblActName1" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Section /Rule</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblRule1" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Compliance Id</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblComplianceID1" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                         <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Category</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblCategory" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Short Description</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblComplianceDiscription1" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Detailed Description</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblDetailedDiscription1" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Penalty</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblPenalty1" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Frequency</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblFrequency1" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Risk Type</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblRisk1" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Sample Form/Attachment</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">

                                                                <asp:UpdatePanel ID="upsample1" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Label ID="lblFormNumber1" Style="width: 300px; font-size: 13px; color: #333;"
                                                                            maximunsize="300px" autosize="true" runat="server" />
                                                                        <asp:LinkButton ID="lbDownloadSample1" Style="width: 300px; font-size: 13px; color: blue"
                                                                            runat="server" Font-Underline="false" OnClick="lbDownloadSample1_Click" />
                                                                        <asp:Label ID="lblSlash1" Text="/" Style="color: blue;" runat="server" />
                                                                        <asp:LinkButton ID="lnkViewSampleForm1" Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                            runat="server" Font-Underline="false" OnClientClick="fopendocfileSampleForm();" />
                                                                        <asp:Label ID="lblpathsample1" runat="server" Style="display: none"></asp:Label>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Regulatory website link</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:LinkButton ID="lnkSampleForm1" Text="" Style="width: 300px; font-size: 13px; color: blue"
                                                                    runat="server" Font-Underline="false" OnClick="lnkSampleForm_Click" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Additional/Reference Text </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblRefrenceText1" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Location</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblLocation" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder" style="display: none;">
                                                            <td style="width: 25%; font-weight: bold;">Period</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblPeriod" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder" style="display: none;">
                                                            <td style="width: 25%; font-weight: bold;">Due Date</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblDueDate" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="ComplianceDetails1" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails1">
                                                    <h2>License Details</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails1"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="collapseComplianceDetails1" class="collapse">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                    <table style="width: 100%">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">License Type</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblLicenseType" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">License Number</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblLicenseNumber" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">License Title</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblLicenseTitle" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">Application Due Date</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblApplicationDate" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">Start Date</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblStartdate" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">End Date</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblEndDate" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 25%; font-weight: bold;">Versions</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <table width="100%" style="text-align: left">
                                                                    <thead>
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <asp:Repeater ID="rptLicenseVersion" runat="server"
                                                                                    OnItemCommand="rptLicenseVersion_ItemCommand"
                                                                                    OnItemDataBound="rptLicenseVersion_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <table id="tblldoc1">
                                                                                            <thead>
                                                                                                <%-- <th>Versions</th>--%>
                                                                                            </thead>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                                                            <ContentTemplate>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                            ID="lblLicenseDocumentVersion" runat="server" Text='<%# Eval("Version")%>' Style="color: blue;"></asp:LinkButton></td>
                                                                                                    <td>
                                                                                                        <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                            ID="btnLicenseVersionDoc" runat="server" Text="Download" Style="color: blue;">
                                                                                                        </asp:LinkButton>
                                                                                                        <asp:Label ID="lblSlashReview1" Text="/" Style="color: blue;" runat="server" />
                                                                                                        <asp:LinkButton CommandName="View" ID="lnkViewDoc1" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                            Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                                                            runat="server" Font-Underline="false" />
                                                                                                        <asp:Label ID="lblpathReviewDoc1" runat="server" Style="display: none"></asp:Label>
                                                                                                        <asp:Label ID="lblpathIlink1" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>
                                                                                                                <asp:LinkButton ID="lblpathDownload1" CommandName="version" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                                    OnClientClick='javascript:enableControls1()' Text='Click Here' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                                    runat="server" Font-Underline="false" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="btnLicenseVersionDoc" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>

                                                                            </td>
                                                                            <td valign="top">
                                                                                <asp:Repeater ID="rptLicenseDocumnets" runat="server" OnItemCommand="rptLicenseDocumnets_ItemCommand"
                                                                                    OnItemDataBound="rptLicenseDocumnets_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <table id="tblLicenseDocumnets">
                                                                                            <thead>
                                                                                                <th>Compliance Related Documents</th>
                                                                                            </thead>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:LinkButton
                                                                                                    CommandArgument='<%# Eval("FileID")%>'
                                                                                                    ID="btnLicenseDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                                </asp:LinkButton>
                                                                                                   <asp:Label ID="lblCompDocpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>                                                                                                 
                                                                                                  <asp:LinkButton CommandName="RedirectURL" ID="lblCompDocpathDownload"  CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>'                                                                                                    
                                                                                                      OnClientClick=<%# "openInNewTab('" + Eval("FilePath") + "')" %>
                                                                                                      Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                    runat="server" Font-Underline="false" />  

                                                                                            </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>
                                                                                <asp:Repeater ID="rptLicenseWorkingFiles" runat="server" OnItemCommand="rptLicenseWorkingFiles_ItemCommand"
                                                                                    OnItemDataBound="rptLicenseWorkingFiles_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <table id="tblLicenseWorkingFiles">
                                                                                            <thead>
                                                                                                <th>Compliance Working Files</th>
                                                                                            </thead>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:LinkButton
                                                                                                    CommandArgument='<%# Eval("FileID")%>'
                                                                                                    ID="btnLicenseWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                                                                </asp:LinkButton>
                                                                                                   <asp:Label ID="lblWorkCompDocpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>                                                                                                 
                                                                                                  <asp:LinkButton CommandName="RedirectURL" ID="lblWorkCompDocpathDownload"  CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>'                                                                                                    
                                                                                                      OnClientClick=<%# "openInNewTab('" + Eval("FilePath") + "')" %>
                                                                                                      Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                    runat="server" Font-Underline="false" />

                                                                                            </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>
                                                                            </td>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>



                        <div runat="server" id="divTask" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                                <h2>Main Task Details</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>

                                        <div id="collapseTaskSubTask" class="collapse in">
                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                <asp:GridView runat="server" ID="gridSubTaskReviewer" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                    AllowPaging="false" PageSize="50" CssClass="table" GridLines="None" BorderWidth="0px" DataKeyNames="TaskID"
                                                    OnRowCommand="gridSubTaskReviewer_RowCommand" OnRowDataBound="gridSubTaskReviewer_RowDataBound" AutoPostBack="true">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                                <asp:Label ID="lblIsTaskClose" Visible="false" runat="server" Text='<%# Eval("IsTaskClose") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Task">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblTaskTitle" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Performer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblPerformer" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'
                                                                        Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Reviewer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'
                                                                        Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Due Date">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                    <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="upSubTaskDownloadView" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="btnSubTaskDocDownload" runat="server" CommandName="Download" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Download Documents" Text="Download" Style="color: blue;">
                                                                        </asp:LinkButton>
                                                                        <asp:Label ID="lblTaskSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                        <asp:LinkButton CommandName="View" runat="server" ID="btnSubTaskDocView" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="View Documents"
                                                                            Text="View" Style="width: 150px; font-size: 13px; color: blue" />
                                                                        <asp:Label ID="CompDocReviewPath" runat="server" Style="display: none"></asp:Label>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnSubTaskDocDownload" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="Step1Download" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Step1DownloadReview1">
                                                    <asp:Label ID="lblReviewDoc" runat="server" CssClass="reviewdoc" Text="Review License Document"></asp:Label>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#Step1DownloadReview1"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="Step1DownloadReview1" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;" id="fieldsetdownloadreview" runat="server">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%; font-weight: bold;">Versions</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <table width="100%" style="text-align: left">
                                                                    <thead>
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <asp:Repeater ID="rptComplianceVersion" runat="server" OnItemCommand="rptComplianceVersion_ItemCommand"
                                                                                    OnItemDataBound="rptComplianceVersion_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <table id="tblComplianceDocumnets">
                                                                                            <thead>
                                                                                                <%-- <th>Versions</th>--%>
                                                                                            </thead>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                                                            <ContentTemplate>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' ID="lblDocumentVersion"
                                                                                                            runat="server" Text='<%# Eval("Version")%>' Style="color: blue;"></asp:LinkButton></td>
                                                                                                    <td>
                                                                                                        <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>'  OnClientClick='javascript:enableControls1()'
                                                                                                            ID="btnComplinceVersionDoc" runat="server" Text="Download" Style="color: blue;">
                                                                                                        </asp:LinkButton>
                                                                                                        <asp:Label ID="lblSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                                                        <asp:LinkButton CommandName="View" ID="lnkViewDoc" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                                                            runat="server" Font-Underline="false" />
                                                                                                        <asp:Label ID="lblpathReviewDoc" runat="server" Style="display: none"></asp:Label>
                                                                                                           <asp:Label ID="lblpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>
                                                                                                                <asp:LinkButton ID="lblpathDownload" CommandName="version" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                                    OnClientClick='javascript:enableControls1()' Text='Click Here' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                                    runat="server" Font-Underline="false" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="btnComplinceVersionDoc" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>

                                                                            </td>
                                                                            <td valign="top">
                                                                                <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                                                                    OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <table id="tblComplianceDocumnets">
                                                                                            <thead>
                                                                                                <th>Compliance Related Documents</th>
                                                                                            </thead>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:LinkButton
                                                                                                    CommandArgument='<%# Eval("FileID")%>'
                                                                                                    OnClientClick='javascript:enableControls1()'
                                                                                                    ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                                </asp:LinkButton>

                                                                                                   <asp:Label ID="lblCompDocpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>                                                                                                 
                                                                                                  <asp:LinkButton CommandName="RedirectURL" ID="lblCompDocpathDownload"  CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>'                                                                                                    
                                                                                                      OnClientClick=<%# "openInNewTab('" + Eval("FilePath") + "')" %>
                                                                                                      Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                    runat="server" Font-Underline="false" />  

                                                                                            </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>
                                                                                <asp:Repeater ID="rptWorkingFiles" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                                                                    OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <table id="tblWorkingFiles">
                                                                                            <thead>
                                                                                                <th>Compliance Working Files</th>
                                                                                            </thead>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:LinkButton
                                                                                                    CommandArgument='<%# Eval("FileID")%>'
                                                                                                    ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                                                                </asp:LinkButton>
                                                                                                      <asp:Label ID="lblWorkCompDocpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>                                                                                                 
                                                                                                  <asp:LinkButton CommandName="RedirectURL" ID="lblWorkCompDocpathDownload"  CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>'                                                                                                    
                                                                                                      OnClientClick=<%# "openInNewTab('" + Eval("FilePath") + "')" %>
                                                                                                      Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                    runat="server" Font-Underline="false" />

                                                                                            </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>
                                                                            </td>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="Step1UpdateCompliance" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Step1UpdateComplianceStatus">
                                                    <h2>Update License Status </h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#Step1UpdateComplianceStatus"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="Step1UpdateComplianceStatus" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div id="fieldsetRenewal" runat="server" visible="false" style="margin-bottom: 7px">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td style="width: 25%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">License Number</label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                                <td style="width: 73%;">
                                                                    <asp:TextBox runat="server" ID="txtLicenseNo" CssClass="form-control" autocomplete="off" Width="280px" />
                                                                    <asp:RequiredFieldValidator ID="rfvLicenseNo" ErrorMessage="Please Enter License Number."
                                                                        ControlToValidate="txtLicenseNo" runat="server" ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" style="height: 13px;"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 25%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">License Title</label>
                                                                </td>

                                                                <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                                <td style="width: 73%;">
                                                                    <asp:TextBox runat="server" ID="txtLicenseTitle" CssClass="form-control" autocomplete="off" Width="280px" />
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Enter License Title."
                                                                        ControlToValidate="txtLicenseTitle" runat="server" ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" style="height: 13px;"></td>
                                                            </tr>
                                                            <tr style="margin-bottom: 7px">
                                                                <td style="width: 25%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">Start Date</label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                                <td style="width: 73%;">
                                                                    <asp:TextBox runat="server" ID="txtStartDate" placeholder="DD-MM-YYYY"
                                                                        class="form-control" Style="width: 115px;" />
                                                                    <asp:RequiredFieldValidator ErrorMessage="Please Enter Start Date." ControlToValidate="txtStartDate"
                                                                        runat="server" ID="RequiredFieldValidator4" ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" style="height: 13px;"></td>
                                                            </tr>
                                                            <tr style="margin-bottom: 7px">
                                                                <td style="width: 25%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">End Date</label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                                <td style="width: 73%;">
                                                                    <asp:TextBox runat="server" ID="txtEndDate" placeholder="DD-MM-YYYY"
                                                                        class="form-control" Style="width: 115px;" />
                                                                    <asp:RequiredFieldValidator ErrorMessage="Please Enter End Date." ControlToValidate="txtEndDate"
                                                                        runat="server" ID="RequiredFieldValidator1" ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" style="height: 13px;"></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr style="display: none;">
                                                            <label id="lblStatus1" runat="server" style="width: 200px; display: block; float: left; font-size: 13px; color: #333;" />
                                                            <td style="width: 21%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Status</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:RequiredFieldValidator Style="display: none" ID="RequiredFieldValidator90" CssClass="alert alert-block alert-danger fade in" runat="server"
                                                                    ValidationGroup="ReviewerComplianceValidationGroup" ControlToValidate="rdbtnStatus1"
                                                                    ErrorMessage="Please select Status."> </asp:RequiredFieldValidator>
                                                                <asp:HiddenField ID="hdnfieldPanaltyDisplay" runat="server" />
                                                                <asp:RadioButtonList ID="rdbtnStatus1" onchange="hideDiv1()" runat="server" RepeatDirection="Horizontal">
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <div style="margin-bottom: 7px" id="divDated1" runat="server">
                                                                <td style="width: 25%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">Date</label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 83%;">
                                                                    <asp:TextBox runat="server" ID="tbxDate1" placeholder="DD-MM-YYYY" class="form-control" Style="width: 115px;" />
                                                                    <asp:RequiredFieldValidator ErrorMessage="Please select Date." ControlToValidate="tbxDate1"
                                                                        runat="server" ID="RequiredFieldValidator3" ValidationGroup="ReviewerComplianceValidationGroup" Display="None" />
                                                                </td>
                                                            </div>
                                                        </tr>
                                                    </table>
                                                </div>
                                                 <%--     Added by renuka--%>
                                                 <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Physical Location of document</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="txtphysicalLocation" Width="280px"  class="form-control" Rows="1" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                 <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;"> File Number</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="txtFileno" Width="280px"  class="form-control" Rows="1" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            <%--    End--%>
                                                <div style="margin-bottom: 7px" id="divCost1" runat="server">
                                                    <table style="width: 100%">
                                                        <tr class="spaceUnder1">
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Cost</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="txtCost" placeholder="Cost" class="form-control" Style="width: 115px;" />

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div style="margin-bottom: 7px" runat="server" id="divChkIsActive" visible="false">
                                                      <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp; </label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Is Permanent Active</label>
                                                            </td>

                                                            <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                            <td style="width: 73%;">
                                                               <asp:CheckBox runat="server" ID="ChkIsActive"  />
                                                   
                                                            </td>
                                                        </tr>
                                                      </table>
                                                 </div>


                                                <div style="margin-bottom: 7px">
                                                    <fieldset id="fieldsetpenalty" runat="server" style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                        <table style="width: 100%">
                                                            <tr id="trPenalty" runat="server">
                                                                <td style="width: 11%; font-weight: bold;">
                                                                    <asp:CheckBox ID="chkPenaltySaveReview" class="clspenaltysave" onclick="fillValuesInTextBoxesReview1()"
                                                                        Text="Value is not known at this moment" runat="server" />
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;"></td>
                                                                <td style="width: 10%;"></td>

                                                                <td style="width: 9%;"></td>
                                                                <td style="width: 2%; font-weight: bold;"></td>
                                                                <td style="width: 10%;"></td>
                                                            </tr>

                                                            <tr>
                                                                <td style="width: 11%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">Interest(INR) </label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 10%;">
                                                                    <asp:TextBox runat="server" ID="txtInterestReview" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                        TargetControlID="txtInterestReview" />
                                                                </td>

                                                                <td style="width: 9%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label id="lblpenaltytextchange" runat="server" style="font-weight: bold; vertical-align: text-top;">Penalty Amount(INR) </label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 10%;">
                                                                    <asp:TextBox runat="server" ID="txtPenaltyReview" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                        TargetControlID="txtPenaltyReview" />
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </fieldset>
                                                    <br />
                                                    <fieldset id="fieldsetTDS" runat="server" visible="false" style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                        <table style="width: 100%">
                                                            <tr class="spaceUnder">
                                                                <td style="width: 11%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">Nature of compliance </label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 10%;">
                                                                    <asp:Label ID="lblNatureofcompliance" Style="width: 88%; font-size: 13px; color: #333;" autosize="true" runat="server" />
                                                                    <asp:Label ID="lblNatureofcomplianceID" Visible="false" runat="server" />
                                                                </td>
                                                                <td style="width: 9%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label id="lblValueAsPerSystem" runat="server" style="font-weight: bold; vertical-align: text-top;">Liability as per system (A) </label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 10%;">
                                                                    <asp:TextBox runat="server" ID="txtValueAsPerSystem" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                        TargetControlID="txtValueAsPerSystem" />
                                                                </td>
                                                            </tr>
                                                            <tr class="spaceUnder">
                                                                <td style="width: 11%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label id="lblValueAsPerReturn" runat="server" style="font-weight: bold; vertical-align: text-top;">Liability as per return (B) </label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 10%;">
                                                                    <asp:TextBox runat="server" ID="txtValueAsPerReturn" onkeyup="Diff1();" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                        TargetControlID="txtValueAsPerReturn" />
                                                                </td>
                                                                <td style="width: 9%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label id="lbldiffAB" runat="server" style="font-weight: bold; vertical-align: text-top;">Difference (A)-(B) </label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">:</td>
                                                                <td style="width: 10%;">
                                                                    <asp:TextBox runat="server" ID="txtDiffAB" ReadOnly="true" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                </td>
                                                            </tr>

                                                            <tr id="trreturn" runat="server" class="spaceUnder">
                                                                <td style="width: 11%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label id="lblLiabilityPaid" runat="server" style="font-weight: bold; vertical-align: text-top;">Liability Paid (C) </label>
                                                                </td>
                                                                <td id="tdLiabilityPaidSC" runat="server" style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 10%;">
                                                                    <asp:TextBox ID="txtLiabilityPaid" runat="server" onkeyup="Diff1();" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                        TargetControlID="txtLiabilityPaid" />
                                                                </td>
                                                                <td style="width: 9%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label id="lbldiffBC" runat="server" style="font-weight: bold; vertical-align: text-top;">Difference (B)-(C) </label>
                                                                </td>
                                                                <td id="tddiffBCSC" runat="server" style="width: 2%; font-weight: bold;">:</td>
                                                                <td style="width: 10%;">
                                                                    <asp:TextBox runat="server" ID="txtDiffBC" ReadOnly="true" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </div>
                                                <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Remarks</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:TextBox runat="server" ID="tbxRemarks1" TextMode="MultiLine" class="form-control" Rows="2" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                               
                                                <div style="margin-bottom: 7px; margin-left: 34%; margin-top: 10px;">
                                                    <asp:Button Text="Approve" runat="server" ID="btnSave1" OnClientClick="javascript:return PenaltyValidateReview1()" OnClick="btnSave1_Click" CssClass="btn btn-search"
                                                        ValidationGroup="ReviewerComplianceValidationGroup" />
                                                    <asp:Button Text="Reject" runat="server" ID="btnReject1" Style="margin-left: 15px" OnClick="btnReject1_Click" CssClass="btn btn-search"
                                                        ValidationGroup="ReviewerComplianceValidationGroup" CausesValidation="false" />
                                                    <asp:Button Text="Close" runat="server"  Style="margin-left: 15px; display:none;" ID="Button3" CssClass="btn btn-search" data-dismiss="modal" />

                                                    <asp:Button Text="Go To Portal" ID="lnkgotoportal"  Visible="false" OnClick="lnkgotoportal_Click"
                                                        Style="margin-left: 15px;" CssClass="btn btn-search" data-dismiss="modal" runat="server" />
                                                </div>

                                                </label>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div id="LegalUpdates" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 12px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseLegalUpdates">
                                                    <h2>Legal Updates</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseLegalUpdates"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="collapseLegalUpdates" class="collapse">

                                                <div id="gridUpdate"></div>
                                       
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="Log1" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#AuditLog1">
                                                    <h2>Audit Log</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#AuditLog1"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="AuditLog1" class="collapse">
                                            <div runat="server" id="log" style="text-align: left;">
                                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                                    <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                        <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false" AllowSorting="true"
                                                            AllowPaging="true" PageSize="5" GridLines="none" OnPageIndexChanging="grdTransactionHistory_OnPageIndexChanging"
                                                            OnRowCreated="grdTransactionHistory_RowCreated" BorderWidth="0px" CssClass="table" OnSorting="grdTransactionHistory_Sorting"
                                                            DataKeyNames="ComplianceTransactionID" OnRowCommand="grdTransactionHistory_RowCommand">
                                                            <Columns>
                                                                <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />
                                                                <asp:TemplateField HeaderText="Date">
                                                                    <ItemTemplate>
                                                                        <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString("dd-MM-yyyy") : ""%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Interest" HeaderText="Interest" />
                                                                <asp:BoundField DataField="Penalty" HeaderText="Penalty" />
                                                                <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                                                <asp:BoundField DataField="Status" HeaderText="Status" />
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Right" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </div>
                                                </fieldset>
                                                <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-size: 10px; color: #666;" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                        <asp:Button ID="btnDownload" runat="server" Style="display: none" OnClick="btnDownload_Click" />
                           <div id="ViewUpdateDetails">
                     <label id="detailUpdate"></label>
                     </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnDownload" />
                    <asp:PostBackTrigger ControlID="btnSave1" />
                    <asp:PostBackTrigger ControlID="lbDownloadSample1" />
                    <asp:AsyncPostBackTrigger ControlID="txtInterestReview" />
                </Triggers>
            </asp:UpdatePanel>


        </div>
        <div>
            <div class="modal fade" id="DocumentPopUpSampleForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                <div class="modal-dialog" style="width: 100%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <%--  Document="AvacomDocuments/Infrastructure.docx"     --%>
                            <%--   <GleamTech:DocumentViewer runat="server"  Width="100%" id="doccontrol"   Height="500px"  />  --%>
                            <iframe src="about:blank" id="docViewerAll1" runat="server" width="100%" height="550px"></iframe>
                            <%-- <GleamTech:DocumentViewer ID="docViewer" runat="server"
                        Width="800"
                        Height="600"
                        documentpath="~/ComplianceFiles/accountsrule.jpg" />--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="modal fade" id="DocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                <div class="modal-dialog" style="width: 100%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="height: 570px;">
                            <div style="width: 100%;">
                                <div style="float: left; width: 10%">
                                    <table width="100%" style="text-align: left; margin-left: 5%;">
                                        <thead>
                                            <tr>
                                                <td valign="top">
                                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Repeater ID="rptComplianceVersionView" runat="server" OnItemCommand="rptComplianceVersionView_ItemCommand"
                                                                OnItemDataBound="rptComplianceVersionView_ItemDataBound">
                                                                <HeaderTemplate>
                                                                    <table id="tblComplianceDocumnets">
                                                                        <thead>
                                                                            <th>Versions</th>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblDocumentVersionView"
                                                                                        runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </table>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div style="float: left; width: 90%">
                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Label ID="lblMessageReviewer1" runat="server" Style="color: red;"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                        <iframe src="about:blank" id="docViewerReviewAll" runat="server" width="100%" height="535px"></iframe>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div>
            <div class="modal fade" id="modalDocumentReviewerViewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                <div class="modal-dialog" style="width: 100%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="height: 570px;">
                            <div style="width: 100%;">
                                <div style="float: left; width: 10%">
                                    <table width="100%" style="text-align: left; margin-left: 5%;">
                                        <thead>
                                            <tr>
                                                <td valign="top">
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Repeater ID="rptTaskVersionView" runat="server" OnItemCommand="rptTaskVersionView_ItemCommand">
                                                                <HeaderTemplate>
                                                                    <table id="tblComplianceDocumnets">
                                                                        <thead>
                                                                            <th>Versions</th>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblTaskDocumentVersionView"
                                                                                        runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="lblTaskDocumentVersionView" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </table>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="rptTaskVersionView" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div style="float: left; width: 90%">
                                    <asp:Label runat="server" ID="lblMessagetask" Style="color: red;"></asp:Label>
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                        <iframe src="about:blank" id="docTaskViewerReviewAll" runat="server" width="100%" height="535px"></iframe>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <div class="modal fade" id="DocumentShowPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                <div class="modal-dialog" style="width: 100%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="height: 570px;">
                            <div style="width: 100%;">
                                <div style="float: left; width: 10%">
                                    <table width="100%" style="text-align: left; margin-left: 5%;">
                                        <thead>
                                            <tr>
                                                <td valign="top">
                                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdatleMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Repeater ID="rptLicenseVersionView" runat="server"
                                                                OnItemCommand="rptLicenseVersionView_ItemCommand"
                                                                OnItemDataBound="rptLicenseVersionView_ItemDataBound">
                                                                <HeaderTemplate>
                                                                    <table id="tblLicenseDocumnets">
                                                                        <thead>
                                                                            <th>Versions</th>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("LicenseID") + ","+ Eval("Version") + ","+ Eval("FileID") %>'
                                                                                        ID="lblLicenseVersionView"
                                                                                        runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="lblLicenseVersionView" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </table>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="rptLicenseVersionView" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div style="float: left; width: 90%">
                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="lblMessageReviewer2" runat="server" Style="color: red;"></asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                        <iframe src="about:blank" id="docViewerLicenseAll" runat="server" width="100%" height="535px"></iframe>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../../Newjs/bxslider/jquery.bxslider.js"></script>
        <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
        <!-- nice scroll -->
        <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
        <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
        <!-- charts scripts -->
        <script type="text/javascript" src="../../assets/jquery-knob/js/jquery.knob.js"></script>
        <script type="text/javascript" src="../../Newjs/owl.carousel.js"></script>
        <!-- jQuery full calendar -->
        <script type="text/javascript" src="../../Newjs/fullcalendar.min.js"></script>
        <!--script for this page only-->
        <script type="text/javascript" src="../../Newjs/calendar-custom.js"></script>
        <script type="text/javascript" src="../../Newjs/jquery.rateit.min.js"></script>
        <!-- custom select -->
        <script type="text/javascript" src="../../Newjs/jquery.customSelect.min.js"></script>
        <!--custome script for all page-->
        <script type="text/javascript" src="../../Newjs/scripts.js"></script>

        <script type="text/javascript">
            function openInNewTab(url) {
                var win = window.open(url, '_blank');
                win.focus();
            }
            function InitializeRequest(sender, args) { }
            function EndRequest(sender, args) { BindControls(); }

            function BindControls() {
                var enddates = new Date($("#lblEndDate").text());
                if (enddates != null) {
                    var startDate = new Date();
                    $(function () {
                        $('input[id*=txtStartDate]').datepicker({
                            dateFormat: 'dd-mm-yy',
                            numberOfMonths: 1,
                            minDate: enddates,
                            changeMonth: true,
                            changeYear: true,
                        });
                        $('input[id*=txtEndDate]').datepicker({
                            dateFormat: 'dd-mm-yy',
                            numberOfMonths: 1,
                            minDate: enddates,
                            changeMonth: true,
                            changeYear: true,
                        });
                        $('input[id*=tbxDate]').datepicker({
                            dateFormat: 'dd-mm-yy',
                            maxDate: enddates,
                            numberOfMonths: 1,
                            //changeMonth: true,
                            //changeYear: true,
                        });
                    });

                }
                else
                {
                    var startDate = new Date();
                    $(function () {
                        $('input[id*=txtStartDate]').datepicker({
                            dateFormat: 'dd-mm-yy',
                            numberOfMonths: 1,
                            minDate: startDate,
                            changeMonth: true,
                            changeYear: true,
                        });
                        $('input[id*=txtEndDate]').datepicker({
                            dateFormat: 'dd-mm-yy',
                            numberOfMonths: 1,
                            minDate: startDate,
                            changeMonth: true,
                            changeYear: true,
                        });
                        $('input[id*=tbxDate]').datepicker({
                            dateFormat: 'dd-mm-yy',
                            maxDate: startDate,
                            numberOfMonths: 1,
                            //changeMonth: true,
                            //changeYear: true,
                        });
                    });
                }
            }


            function enableControls1() {
                debugger;
               
                if ("<%= status %>" == "Validity Expired" || "<%= status %>" == "Registered") {
                  
                }
                else {
                 
                    $("#<%= btnSave1.ClientID %>").removeAttr("disabled");
                    $("#<%= rdbtnStatus1.ClientID %>").removeAttr("disabled");
                    $("#<%= tbxRemarks1.ClientID %>").removeAttr("disabled");
                    $("#<%= tbxDate1.ClientID %>").removeAttr("disabled");
                    $("#<%= btnReject1.ClientID %>").removeAttr("disabled");
                }
            }

            function enableControlswithstatus(Status) {
                debugger;
                alert(status);
                $("#<%= btnSave1.ClientID %>").removeAttr("disabled");
                $("#<%= rdbtnStatus1.ClientID %>").removeAttr("disabled");
                $("#<%= tbxRemarks1.ClientID %>").removeAttr("disabled");
                $("#<%= tbxDate1.ClientID %>").removeAttr("disabled");
                $("#<%= btnReject1.ClientID %>").removeAttr("disabled");
            }

            function fopendocfileSampleForm() {
                $('#DocumentPopUpSampleForm').modal();
                $('#docViewerAll1').attr('src', "../docviewer.aspx?docurl=" + $("#<%= lblpathsample1.ClientID %>").text());
            }

            function fopendocfileReviewReviewer(file) {
                $('#DocumentReviewPopUp1').modal('show'); enableControls1();
                $('#docViewerReviewAll').attr('src', "../docviewer.aspx?docurl=" + file);
            }

            function fopendoctaskfileReview1(file) {
                $('#modalDocumentReviewerViewer').modal('show');
                $('#docTaskViewerReviewAll').attr('src', "../docviewer.aspx?docurl=" + file);
            }
            function fopendoctaskfileReview1PopUp() {
                $('#modalDocumentReviewerViewer').modal('show');
            }
            function fopendocfileReviewReviewerPopUp() {
                $('#DocumentReviewPopUp1').modal('show');
            }

            function fopendocfilelicense(file) {
                $('#DocumentShowPopUp').modal('show');
                $('#docViewerLicenseAll').attr('src', "../docviewer.aspx?docurl=" + file);
            }
            function fopendocfileLicenseAllShowPopUp() {
                $('#DocumentShowPopUp').modal('show');
            }

            $(document).ready(function () {
                //$('#txtEndDate').attr('readonly', true);
                $('#txtEndDate').addClass('input-disabled')
                $('#txtStartDate').addClass('input-disabled')
            });

            $(document).ready(function () {
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_initializeRequest(InitializeRequest);
                prm.add_endRequest(EndRequest);


                $('.btn-minimize').click(function () {
                    var s1 = $(this).find('i');
                    if ($(this).hasClass('collapsed')) {
                        $(s1).removeClass('fa-chevron-down');
                        $(s1).addClass('fa-chevron-up');
                    } else {
                        $(s1).removeClass('fa-chevron-up');
                        $(s1).addClass('fa-chevron-down');
                    }
                });

                function btnminimize(obj) {
                    var s1 = $(obj).find('i');
                    if ($(obj).hasClass('collapsed')) {

                        $(s1).removeClass('fa-chevron-up');
                        $(s1).addClass('fa-chevron-down');
                    } else {
                        $(s1).removeClass('fa-chevron-down');
                        $(s1).addClass('fa-chevron-up');
                    }
                }

                BindControls();
                $("button[data-dismiss-modal=modal2]").click(function () {
                    $('#DocumentPopUpSampleForm').modal('hide');
                    $('#DocumentReviewPopUp1').modal('hide');
                    $('#DocumentShowPopUp').modal('hide');
                    $('#modalDocumentReviewerViewer').modal('hide');
                });
            });

            function PenaltyValidateReview1() {
                var RB1 = document.getElementById("<%=rdbtnStatus1.ClientID%>");
                var radio = RB1.getElementsByTagName("input");
                var isChecked = false;
                for (var i = 0; i < radio.length; i++) {
                    if (radio[i].checked) {
                        isChecked = true;
                        break;
                    }
                }
                if (isChecked == false) {
                    $("#Labelmsgrev").css('display', 'block');
                    $('#Labelmsgrev').text("Please select Status.");
                    $('#ValidationSummary1').IsValid = false;
                    document.getElementById("ValidationSummary1").value = "Please select Status.";
                    return false;
                }
                var ispenaltyDisplay = document.getElementById("<%=hdnfieldPanaltyDisplay.ClientID%>").value;
                if (ispenaltyDisplay == "true") {
                    if (document.getElementById('rdbtnStatus1_0') != null) {
                        if (document.getElementById('rdbtnStatus1_0').checked) {
                            {
                                var checkedRadio = $("#rdbtnStatus1 input[type=radio]:checked");
                                if (checkedRadio.length > 0) {
                                    var selectedText = checkedRadio.next().html();
                                    if (selectedText == "Closed-Delayed") {

                                        var chk = $("#chkPenaltySaveReview").is(":checked");
                                        if (chk == false) {
                                            var txtInterestReview = $("#txtInterestReview").val();
                                            var txtPenaltyReview = $("#txtPenaltyReviewReview").val();
                                            if (txtInterestReview == "" || txtPenaltyReview == "" || txtInterestReview == "" || txtPenaltyReview == "0") {
                                                $("#Labelmsgrev").css('display', 'block');
                                                $('#Labelmsgrev').text("Please enter interest and penalty");
                                                $('#ValidationSummary1').IsValid = false;
                                                document.getElementById("ValidationSummary1").value = "Please enter interest and penalty";
                                                return false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            function hideDiv1() {

                var ispenaltyDisplay = document.getElementById("<%=hdnfieldPanaltyDisplay.ClientID%>").value;
                if (ispenaltyDisplay == "true") {
                    if (document.getElementById('fieldsetpenalty') != null) {
                        document.getElementById('fieldsetpenalty').style.display = 'none';
                    }

                    if (document.getElementById('rdbtnStatus1_1') != null) {
                        if (document.getElementById('rdbtnStatus1_1').checked) {
                            var checkedRadio = $("#rdbtnStatus1 input[type=radio]:checked");
                            if (checkedRadio.length > 0) {
                                var selectedText = checkedRadio.next().html();
                                if (selectedText == "Closed-Timely") {
                                    $("#chkPenaltySaveReview").prop('checked', false);
                                }
                            }
                        }
                    }

                    if (document.getElementById('rdbtnStatus1_0') != null) {
                        if (document.getElementById('rdbtnStatus1_0').checked) {
                            var checkedRadio = $("#rdbtnStatus1 input[type=radio]:checked");
                            var chk = $("#chkPenaltySaveReview").is(":checked");
                            if (checkedRadio.length > 0) {
                                var selectedText = checkedRadio.next().html();
                                if (selectedText == "Closed-Delayed" && chk == false) {
                                    $("#txtInterestReview").attr("disabled", false);
                                    $("#txtPenaltyReview").attr("disabled", false);
                                    document.getElementById('fieldsetpenalty').style.display = 'block';
                                }
                                else {
                                    document.getElementById('fieldsetpenalty').style.display = 'block';
                                    $("#txtInterestReview").attr("disabled", true);
                                    $("#txtPenaltyReview").attr("disabled", true);
                                }
                            }
                        }
                        else {
                            document.getElementById("txtInterestReview").value = "0";
                            document.getElementById("txtPenaltyReview").value = "0";
                        }
                    }
                }
                else {
                    document.getElementById('fieldsetpenalty').style.display = 'none';
                }
            }

            function fillValuesInTextBoxesReview1() {
                var chk = $("#chkPenaltySaveReview").is(":checked");
                if (chk == true) {
                    document.getElementById("txtInterestReview").value = "0";
                    document.getElementById("txtPenaltyReview").value = "0";
                    $("#txtInterestReview").attr("disabled", true);
                    $("#txtPenaltyReview").attr("disabled", true);
                }
                else {
                    document.getElementById("txtInterestReview").value = "";
                    document.getElementById("txtPenaltyReview").value = "";
                    $("#txtInterestReview").attr("disabled", false);
                    $("#txtPenaltyReview").attr("disabled", false);
                }
            }

            function Diff1() {
                document.getElementById('txtDiffAB').value = "";
                var ValueAsPerSystem = document.getElementById('txtValueAsPerSystem').value;
                var ValueAsPerReturn = document.getElementById('txtValueAsPerReturn').value;
                var AB = parseInt(ValueAsPerSystem) - parseInt(ValueAsPerReturn);
                if (!isNaN(AB)) {
                    document.getElementById('txtDiffAB').value = AB;
                }
                else {
                    document.getElementById('txtDiffAB').value = "";
                }
                document.getElementById('txtDiffBC').value = ""
                var LiabilityPaid = document.getElementById('txtLiabilityPaid').value;
                var BC = parseInt(ValueAsPerReturn) - parseInt(LiabilityPaid);
                if (!isNaN(BC)) {
                    document.getElementById('txtDiffBC').value = BC;
                }
                else {
                    document.getElementById('txtDiffBC').value = "";
                }
            }
        </script>

  

    </form>
</body>
</html>
