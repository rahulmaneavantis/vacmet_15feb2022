﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="AssignComplianceToUser.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.AssignComplianceToUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">                               
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        // For Enter Only Number Only.
        function validatenumerics(key) {
            //getting key code of pressed key
            var keycode = (key.which) ? key.which : key.keyCode;
            //comparing pressed keycodes
            if (keycode > 31 && (keycode < 48 || keycode > 57) && keycode != 46) {
                return false;
            }
            else return true;
        }
        function ValidateDaysAndIntermDays(obj) {
            var esc = $(obj).parent('td').parent('tr').find('.Esclare');
            var escI = $(obj).parent('td').parent('tr').find('.EsclareI');
            if ($(escI).val() == "" || $(esc).val() == "") {
                alert("Please enter value of days and interimdays.!");
                return false;
            }
            else {
                return true;
            }
        }

    </script>
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upDocumentDownload" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <div style="margin-bottom: 4px">
                            <asp:CustomValidator ID="cvDuplicateEntry" CssClass="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblMsg1" CssClass="alert alert-block alert-danger fade in" runat="server" Visible="false"></asp:Label>
                        </div>
                        <section class="panel">
                           <header class="panel-heading tab-bg-primary ">
                                                          </header>
                            <div class="clearfix"></div>
                            <div class="panel-body">
                                   <div class="col-md-12 colpadding0">
                                        <div class="col-md-12 colpadding0" style="text-align: right; float: right">
                                            <div class="col-md-10 colpadding0" style="width:69.666667%;">
                                                <div class="col-md-2 colpadding0 entrycount">
                                            <div class="col-md-4 colpadding0">
                                                <p style="color:#999; margin-top:5px"> Show </p>
                                            </div>
                                          <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >
                                        <asp:ListItem Text="5" Selected="True"/>
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                        </asp:DropDownList>     
                                        </div>
                                                 <div style="float:left;margin-right: 2%;">
                                        <asp:DropDownList runat="server" ID="ddlType" class="form-control m-bot15" Style="width: 180px; float: left" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" >
                                        <asp:ListItem Text="Statutory" Value="S" Selected="True"/>
                                        <asp:ListItem Text="Statutory CheckList" Value="C"/>
                                        <asp:ListItem Text="EventBased" Value="E"/>
                                        <asp:ListItem Text="Internal" Value="I"/>
                                        <asp:ListItem Text="Internal CheckList" Value="IC" />
                                    </asp:DropDownList>    
                                        </div>
                                                  <div class="col-md-5 colpadding0 entrycount" style="float:left;margin-right: -2%; width:33.667% !important">
                                                       <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation"
                                                Style="padding: 0px;padding-left: 10px; margin: 0px; height: 35px; width: 325px; 
                                                border: 1px solid #c7c7cc;border-radius: 4px;color:#8e8e93"
                                                CssClass="txtbox" />                                                       
                                            <div style="margin-left: 1px; position: absolute; z-index: 10;display: inherit;" id="divFilterLocation">
                                                <asp:TreeView runat="server" ID="tvFilterLocation"   SelectedNodeStyle-Font-Bold="true"  Width="325px"   NodeStyle-ForeColor="#8e8e93"
                                                Style="overflow: auto; border-left:1px solid #c7c7cc; border-right:1px solid #c7c7cc; border-bottom:1px solid #c7c7cc; 
                                                background-color: #ffffff; color:#8e8e93 !important;" ShowLines="true"  
                                                OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                            </asp:TreeView>
                                                    </div>  
                                               </div>
                                            </div>    
                                             <div class="col-md-2 colpadding0" style="width: 22.333333%; float:right">
                                                <div class="col-md-6 colpadding0" style="margin-left: 10px;width: 41%;">               
                                        <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search"  runat="server" Text="Apply" OnClick="btnSearch_Click"/> 
                                        </div>
                                        <div class="col-md-6 colpadding0">  
                                            <asp:Button ID="btnExport" CausesValidation="false" class="btn btn-search"  runat="server" Text="Export to Excel" OnClick="btnExport_Click"/> 
                                        </div>
                                            </div>              
                                        </div>
                                         <div class="clearfix"> 
                                    </div>
                                </div>

                                 <!-- Advance Search scrum-->
                                 <div class="clearfix"></div>
                            <div class="col-md-12 AdvanceSearchScrum" style="display:none;">
                                 
                                    <div runat="server" id="DivRecordsScrum" style="float: right;">
                                        <p style="padding-right: 0px !Important;">
                                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                            <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                            </div>
                            </div> 

                          <%--  <div class="col-md-12 colpadding0">
                                
                                <div class="col-md-10 colpadding0" style="text-align: right; float: left">
                                    <div class="col-md-2 colpadding0 entrycount">
                                    <div class="col-md-3 colpadding0" >
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                    </div>
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >
                                        <asp:ListItem Text="5" Selected="True"/>
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>      
                                </div>
                                    <div class="col-md-8 colpadding0">              
                                        <div style="float:left;margin-right: 2%;">
                                        <asp:DropDownList runat="server" ID="ddlType" class="form-control m-bot15" Style="width: 180px; float: left" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" >
                                        <asp:ListItem Text="Statutory" Value="S" Selected="True"/>
                                        <asp:ListItem Text="Statutory CheckList" Value="C"/>
                                        <asp:ListItem Text="EventBased" Value="E"/>
                                        <asp:ListItem Text="Internal" Value="I"/>
                                        <asp:ListItem Text="Internal CheckList" Value="IC" />
                                    </asp:DropDownList>    
                                        </div>
                                        <div style="float:left;margin-right: 2%;">
                                                <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation"
                                                Style="padding: 0px;padding-left: 10px; margin: 0px; height: 35px; width: 325px; 
                                                border: 1px solid #c7c7cc;border-radius: 4px;color:#8e8e93"
                                                CssClass="txtbox" />                                                       
                                            <div style="margin-left: 1px; position: absolute; z-index: 10;display: inherit;" id="divFilterLocation">
                                                <asp:TreeView runat="server" ID="tvFilterLocation"   SelectedNodeStyle-Font-Bold="true"  Width="325px"   NodeStyle-ForeColor="#8e8e93"
                                                Style="overflow: auto; border-left:1px solid #c7c7cc; border-right:1px solid #c7c7cc; border-bottom:1px solid #c7c7cc; 
                                                background-color: #ffffff; color:#8e8e93 !important;" ShowLines="true"  
                                                OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                            </asp:TreeView>
                                        </div>  
                                        </div>           
                                    </div>
                                    <div class="col-md-4 colpadding0">
                                        <div class="col-md-6 colpadding0" style="margin-left: 10px;width: 41%;">               
                                        <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search"  runat="server" Text="Apply" OnClick="btnSearch_Click"/> 
                                        </div>
                                        <div class="col-md-6 colpadding0">  
                                            <asp:Button ID="btnExport" CausesValidation="false" class="btn btn-search"  runat="server" Text="Export to Excel" OnClick="btnExport_Click"/> 
                                        </div>
                                    </div>
                                </div>  
                             
                               
                                <!-- Advance Search scrum-->
                                 <div class="clearfix"></div>
                            <div class="col-md-12 AdvanceSearchScrum" style="display:none;">
                                 
                                    <div runat="server" id="DivRecordsScrum" style="float: right;">
                                        <p style="padding-right: 0px !Important;">
                                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                            <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                            </div>
                            </div>--%>
                            </div>
                        <div class="clearfix"></div>                                                                      
                        
                        <div id="ReviewerGrids" runat="server">                               
                            <div style="margin-bottom: 4px">
                                <asp:GridView runat="server" ID="grdReviewerComplianceDocument" AutoGenerateColumns="false" CssClass="table" GridLines="None" BorderWidth="0px"
                                    CellPadding="4" Width="100%" 
                                    OnPageIndexChanging="grdReviewerComplianceDocument_PageIndexChanging" DataKeyNames="ComplianceId" AllowPaging="True" PageSize="5" AutoPostBack="true">
                                    <Columns>  
                                           <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                            <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                                              
                                    <asp:BoundField DataField="ComplianceID" HeaderText="Compliance ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="ComplianceID"/>
                                    <asp:TemplateField HeaderText="Location" SortExpression="Branch">
                                    <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom"  Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                    </div>
                                    </ItemTemplate>
                                    </asp:TemplateField>                                          
                                    <asp:TemplateField HeaderText="Description" SortExpression="ShortDescription">
                                    <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom"  Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                    </div>
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                      
                            <asp:TemplateField HeaderText="Risk" ItemStyle-Width="10%" SortExpression="Risk" ItemStyle-Height="20px" HeaderStyle-Height="20px" Visible="false">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                        <asp:Label ID="lblRisk" runat="server" Text='<%# Eval("Risk") %>' ToolTip='<%# Eval("Risk") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Frequency" ItemStyle-Width="10%" SortExpression="Frequency" ItemStyle-Height="20px" HeaderStyle-Height="20px" Visible="false">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                        <asp:Label ID="lblFrequency" runat="server" Text='<%# Eval("Frequency") %>' ToolTip='<%# Eval("Frequency") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Performer" ItemStyle-Width="10%" SortExpression="Performer" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                        <asp:Label ID="lblPerformer" data-toggle="tooltip" data-placement="bottom"  runat="server" Text='<%# Eval("Performer") %>' ToolTip='<%# Eval("Performer") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Reviewer" ItemStyle-Width="10%" SortExpression="Reviewer" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                        <asp:Label ID="lblReviewer" data-toggle="tooltip" data-placement="bottom"  runat="server" Text='<%# Eval("Reviewer") %>' ToolTip='<%# Eval("Reviewer") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Start Date" ItemStyle-Width="10%" SortExpression="StartDate" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                           <%# Eval("StartDate")!= null?((DateTime)Eval("StartDate")).ToString("dd-MMM-yyyy"):""%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>                               
                            </div>                                                     
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 colpadding0">
                            <div class="col-md-6 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 15px;"></asp:Label></p>
                                    </div>                                    
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0">
                                <div class="table-paging" style="margin-bottom: 20px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="Previous_Click"/>
                                  
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>

                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="Next_Click" />                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                         </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
       <Triggers>
            <asp:PostBackTrigger ControlID="btnExport" />
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">
        $(document).ready(function () {
            fhead('Assign Compliance Report');
            setactivemenu('Myreport1');
            fmaters1();
        });
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
</asp:Content>
