﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Configuration;
using Ionic.Zip;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3;
using Amazon;
using Amazon.S3.Model;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class PerCompliancestatusTransaction : System.Web.UI.Page
    {
        public static string LicenseDocPath = "";
        static string sampleFormPath = "";
        public static string subTaskDocViewPath = "";
        public static List<long> Tasklist = new List<long>();
        protected string UploadDocumentLink;
        protected String Date1;
        protected static string KendoPath;
        protected static int CustId;
        protected static int UId;
        protected static int compInstanceID;
        protected static string Authorization;
        //protected static string status;
        public string ispenaltytext;
        public string ISmail;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
                string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
                Authorization = (string)HttpContext.Current.Cache[CacheName];
                if (Authorization == null)
                {
                    Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                    HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                }
                ispenaltytext = ConfigurationManager.AppSettings["IsPenaltyTextchanged"];
                if (ispenaltytext == Convert.ToString(AuthenticationHelper.CustomerID))
                {
                    lblpenaltytextchange.InnerText = "Penalty/Additional Fee(INR)";
                }
                KendoPath = ConfigurationManager.AppSettings["KendoPathApp"];
                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                compInstanceID = Convert.ToInt32(Request.QueryString["CID"].ToString());
                //status = Convert.ToString(Request.QueryString["status"].ToString());

                string IsActiveCustomer = Convert.ToString(ConfigurationManager.AppSettings["IsLicensePermanantActive"]);

                if (IsActiveCustomer == Convert.ToString(AuthenticationHelper.CustomerID))
                {
                    divChkIsActive.Visible = true;
                }
                ISmail = "0";
                try
                {
                    ISmail = Convert.ToString(Request.QueryString["ISM"]);
                    if (!string.IsNullOrEmpty(ISmail) && ISmail == "1")
                    {
                        lnkgotoportal.Visible = true;
                    }
                    else
                    {
                        lnkgotoportal.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "ISmail", MethodBase.GetCurrentMethod().Name);
                    ISmail = "0";
                }

                string listCustomer = ConfigurationManager.AppSettings["UploadUsingLinkCustomerlist"];
                string[] listCust = new string[] { };
                if (!string.IsNullOrEmpty(listCustomer))
                    listCust = listCustomer.Split(',');

                if (listCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                    UploadDocumentLink = "True";
                else
                    UploadDocumentLink = "False";
                if (!Page.IsPostBack)
                {                                         
                    OpenTransactionPage(Convert.ToInt32(Request.QueryString["SOID"].ToString()), Convert.ToInt32(Request.QueryString["CID"].ToString()));
                    
                    string listLockingCustomer = ConfigurationManager.AppSettings["LockingDaysCustomerlist"];
                    string[] listLockCust = new string[] { };
                    if (!string.IsNullOrEmpty(listLockingCustomer))
                        listLockCust = listLockingCustomer.Split(',');

                    if (listLockCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                    {
                        if (Business.ComplianceManagement.GetCurrentStatusByScheduleONID(Convert.ToInt32(Request.QueryString["SOID"].ToString()), "S", Convert.ToInt32(AuthenticationHelper.CustomerID)))
                            btnSave.Enabled = false;
                        else
                            btnSave.Enabled = true;
                    }

                   // int LicID = Convert.ToInt32(Session["LicenseID"]);
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var LicID = (from row in entities.Lic_tbl_LicenseComplianceInstanceMapping
                                     where row.ComplianceInstanceID == compInstanceID
                                     select row.LicenseID).FirstOrDefault();

                        var LicRecentStatus = (from row in entities.RecentLicenseTransactionViews
                                             where row.LicenseID == LicID
                                             select row.StatusID).FirstOrDefault();
                       
                        if ((LicRecentStatus == 9 || LicRecentStatus == 11) || (LicRecentStatus == 2 && Convert.ToInt32(Session["IsPermanantActive"]) == 1))
                        {
                            if (LicRecentStatus == 2 && Convert.ToInt32(Session["IsPermanantActive"]) == 1)
                            {
                                ChkIsActive.Checked = true;
                            }
                            
                            ddlStatus.SelectedValue = Convert.ToString(LicRecentStatus);
                            ddlStatus.Attributes.Add("disabled", "disabled");
                            tbxDate.Attributes.Add("disabled", "disabled");
                            tbxRemarks.Attributes.Add("disabled", "disabled");
                            txtFileno.Attributes.Add("disabled", "disabled");
                            txtphysicalLocation.Attributes.Add("disabled", "disabled");
                            btnSave.Attributes.Add("disabled", "disabled");
                            fuSampleFile.Attributes.Add("disabled", "disabled");
                            FileUpload1.Attributes.Add("disabled", "disabled");
                            txtCost.Attributes.Add("disabled", "disabled");
                            ChkIsActive.Enabled = false;
                            btnSaveDOCNotCompulsory.Visible = false;
                            btnSave.Enabled = false;

                        }
                        else
                        {
                            ddlStatus.Attributes.Remove("disabled");
                            tbxDate.Attributes.Remove("disabled");
                            tbxRemarks.Attributes.Remove("disabled");
                            txtFileno.Attributes.Remove("disabled");
                            txtphysicalLocation.Attributes.Remove("disabled");
                            btnSave.Attributes.Remove("disabled");
                            fuSampleFile.Attributes.Remove("disabled");
                            FileUpload1.Attributes.Remove("disabled");
                            ChkIsActive.Attributes.Remove("disabled");
                            txtCost.Attributes.Remove("disabled");
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkgotoportal_Click(object sender, EventArgs e)
        {
            try
            {
                ProductMappingStructure _obj = new ProductMappingStructure();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _obj.ReAuthenticate_UserNew();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void UploadDocument_Click(object sender, EventArgs e)
        {
            try
            {
                grdDocument.DataSource = null;
                grdDocument.DataBind();
                long ScheduledOnID = Convert.ToInt64(ViewState["ScheduledOnID"]);
                long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

                TempComplianceDocument tempComplianceDocument = null;
                HttpFileCollection fileCollection = Request.Files;
                if (fileCollection.Count > 0)
                {
                    #region file upload
                    bool isBlankFile = false;
                    string[] InvalidvalidFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                    for (int i = 0; i < fileCollection.Count; i++)
                    {
                        HttpPostedFile uploadfile = null;
                        uploadfile = fileCollection[i];
                        int filelength = uploadfile.ContentLength;
                        string fileName = Path.GetFileName(uploadfile.FileName);
                        string ext = System.IO.Path.GetExtension(uploadfile.FileName);                    
                        if (!string.IsNullOrEmpty(fileName))
                        {                          
                            if (filelength == 0)
                            {
                                isBlankFile = true;
                                break;
                            }
                            else if (ext == "")
                            {
                                isBlankFile = true;
                                break;
                            }
                            else
                            {
                                if (ext != "")
                                {
                                    for (int j = 0; j < InvalidvalidFileTypes.Length; j++)
                                    {
                                        if (ext == "." + InvalidvalidFileTypes[j])
                                        {
                                            isBlankFile = true;
                                            break;
                                        }
                                    }
                                }

                            }
                        }
                    }
                    if (isBlankFile == false)
                    {
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            string directoryPath = null;

                            if (!string.IsNullOrEmpty(fileName))
                            {
                                string[] keys = fileCollection.Keys[i].Split('$');
                                if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                {
                                    directoryPath = Server.MapPath("~/TempDocuments/Statutory/");
                                }
                                else
                                {
                                    directoryPath = Server.MapPath("~/TempDocuments/Working/");
                                }

                                DocumentManagement.CreateDirectory(directoryPath);
                                string finalPath = Path.Combine(directoryPath, fileName);
                                finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                                fileCollection[i].SaveAs(Server.MapPath(finalPath));

                                Stream fs = uploadfile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                tempComplianceDocument = new TempComplianceDocument()
                                {
                                    ScheduleOnID = ScheduledOnID,
                                    ComplianceInstanceID = complianceInstanceID,
                                    DocPath = finalPath,
                                    DocData = bytes,
                                    DocName = fileCollection[i].FileName,
                                    FileSize = uploadfile.ContentLength,
                                };

                                if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                {
                                    tempComplianceDocument.DocType = "S";
                                }
                                else
                                {
                                    tempComplianceDocument.DocType = "W";
                                }

                                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                                if (_objTempDocumentID > 0)
                                {
                                    BindTempDocumentData(ScheduledOnID);
                                }

                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;                      
                        cvDuplicateEntry.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                    }
                    #endregion
                }

                BindTempDocumentData(ScheduledOnID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindTempDocumentData(long ScheduledOnID)
        {
            try
            {
                grdDocument.DataSource = null;
                grdDocument.DataBind();
                var DocData = DocumentManagement.GetTempComplianceDocumentData(ScheduledOnID);
                if (DocData.Count > 0)
                {
                    grdDocument.Visible = true;
                    grdDocument.DataSource = DocData;
                    grdDocument.DataBind();
                }
                else
                {
                    grdDocument.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Delete Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        if (Business.ComplianceManagement.DeleteTempDocumentFile(FileID))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);

                            BindTempDocumentData(Convert.ToInt64(ViewState["ScheduledOnID"]));
                        }
                    }
                }
                else if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
                            if (file != null)
                            {
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename= " + file.DocName);
                                Response.TransmitFile(Server.MapPath(file.DocPath));
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                        }
                        BindTempDocumentData(Convert.ToInt64(ViewState["ScheduledOnID"]));
                    }
                }
                else if (e.CommandName.Equals("View Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);

                    if (FileID > 0)
                    {
                        TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
                        if (file != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.DocPath));
                            if (file.DocName != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Zip file can't view please download it.')", true);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Zip file can't view please download it.";
                                }
                                else
                                {
                                    string CompDocReviewPath = file.DocPath;

                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenDocumentPriview('" + CompDocReviewPath + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('There is no file to preview.')", true);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "There is no file to preview.";
                            }
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fCalltreeCollapsed();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again."; ;
            }
        }
        protected void grdDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblDocumentName = (Label)e.Row.FindControl("lblDocumentName");
                    Label lblIsLinkTrue = (Label)e.Row.FindControl("lblIsLinkTrue");
                    Label lblDocType = (Label)e.Row.FindControl("lblDocType");

                    if (lblDocType.Text.Trim() == "S")
                    {
                        lblDocType.Text = "Compliance Document";
                    }
                    else
                    {
                        lblDocType.Text = "Working Files";
                    }

                    LinkButton lnkRedirectDocument = (LinkButton)e.Row.FindControl("lnkRedirectDocument");
                    LinkButton lnkDownloadDocument = (LinkButton)e.Row.FindControl("lnkDownloadDocument");
                    LinkButton lnkViewDocument = (LinkButton)e.Row.FindControl("lnkViewDocument");
                    //LinkButton lnkDeleteDocument = (LinkButton)e.Row.FindControl("lnkDeleteDocument");

                    if (lblIsLinkTrue.Text == "True")
                    {
                        lnkDownloadDocument.Visible = false;
                        lnkViewDocument.Visible = false;
                        lnkRedirectDocument.Visible = true;
                        lblDocumentName.Visible = false;
                        //lnkDeleteDocument.Visible = false;
                    }
                    else
                    {
                        lnkDownloadDocument.Visible = true;
                        lnkViewDocument.Visible = true;
                        lnkRedirectDocument.Visible = false;
                        lblDocumentName.Visible = true;
                    }
                }              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void UploadlinkWorkingfile_Click(object sender, EventArgs e)
        {
            grdDocument.DataSource = null;
            grdDocument.DataBind();
            long ScheduledOnID = Convert.ToInt64(ViewState["ScheduledOnID"]);
            long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

            string url = txtboxuploadworkingfiles.Text;
            string fileName = DocumentManagement.getFileName(url);
            if (!string.IsNullOrEmpty(txtboxuploadworkingfiles.Text))
            {
                TempComplianceDocument tempComplianceDocument = null;
                var bytes = new byte[] { };// { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };               
                tempComplianceDocument = new TempComplianceDocument()
                {
                    ScheduleOnID = ScheduledOnID,
                    ComplianceInstanceID = complianceInstanceID,
                    DocPath = url,//userpath,
                    DocData = bytes,
                    DocName = fileName,
                    DocType = "S",
                    ISLink = true
                };

                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                if (_objTempDocumentID > 0)
                {
                    BindTempDocumentData(ScheduledOnID);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
            txtboxuploadworkingfiles.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
        }

        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        private void BindTransactionDetails(int ScheduledOnID, int complianceInstanceID)
        {
            var CSOID = Convert.ToInt64(ScheduledOnID);
            var RCT = Business.ComplianceManagement.GetCurrentStatusByComplianceID((int)CSOID);
            var LRCT = Business.ComplianceManagement.GetCurrentStatusOfLicenseByScheduleOnID((int)CSOID);
            
            if (RCT.ComplianceStatusID == 2 || RCT.ComplianceStatusID == 3 || RCT.ComplianceStatusID == 4 || RCT.ComplianceStatusID == 5)
            {
                if (LRCT.Status == "Registered" || LRCT.Status == "Validity Expired")
                {
                }
                else
                {
                    btnSave.Visible = false;
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "This compliance is already submitted for review or already closed.";
                }
            }           
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    grdDocument.DataSource = null;
                    grdDocument.DataBind();
                    lblLicenseType.Text = string.Empty;
                    lblLicenseNumber.Text = string.Empty;
                    lblLicenseTitle.Text = string.Empty;
                    txtLicenseTitle.Text = string.Empty;
                    lblApplicationDate.Text = string.Empty;
                    lblStartdate.Text = string.Empty;
                    lblEndDate.Text = string.Empty;

                    lblPenalty.Text = string.Empty;
                    lblRisk.Text = string.Empty;
                    lbDownloadSample.Text = string.Empty;

                    chkPenaltySave.Checked = false;
                    txtInterest.Text = "";
                    txtPenalty.Text = "";
                    txtCost.Text = "";
                    txtInterest.ReadOnly = false;
                    txtPenalty.ReadOnly = false;

                    int customerID = -1;
                    int customerbranchID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var AllComData = Business.ComplianceManagement.GetPeriodBranchLocation(ScheduledOnID, complianceInstanceID);
                    if (AllComData != null)
                    {
                        customerbranchID = AllComData.CustomerBranchID;
                        lblLocation.Text = AllComData.Branch;
                        lblDueDate.Text = Convert.ToDateTime(AllComData.PerformerScheduledOn).ToString("dd-MMM-yyyy");
                        lblPeriod.Text = AllComData.ForMonth;
                    }
                    var IsAfter = TaskManagment.IsTaskAfter(ScheduledOnID, 3, lblPeriod.Text, customerbranchID, customerID);
                    var showHideButton = false;
                    if (IsAfter == false)
                    {
                        showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, customerbranchID);
                        btnSave.Enabled = showHideButton;
                    }
                    else
                    {
                        showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, customerbranchID);
                        btnSave.Enabled = true;
                    }

                    BindTempDocumentData(ScheduledOnID);
                    ViewState["IsAfter"] = IsAfter;
                    ViewState["Period"] = lblPeriod.Text;
                    ViewState["CustomerBrachID"] = customerbranchID;
                    ViewState["complianceInstanceID"] = complianceInstanceID;
                    ViewState["ScheduledOnID"] = ScheduledOnID;
                    var complianceInfo = Business.ComplianceManagement.GetComplianceByInstanceID(ScheduledOnID);
                   
                    var complianceForm = Business.ComplianceManagement.GetComplianceFormByID(complianceInfo.ID);
                    var RecentComplianceTransaction = Business.ComplianceManagement.GetCurrentStatusByComplianceID(ScheduledOnID);
                    if (complianceInfo != null)
                    {
                        var complianceCategoryInfo = Business.ComplianceManagement.GetComplianceCategoryByInstanceID(complianceInfo.ActID);
                        var LicenseID = (from row in entities.Lic_tbl_LicenseComplianceInstanceMapping
                                         where row.ComplianceInstanceID == complianceInstanceID
                                         select row.LicenseID).FirstOrDefault();

                        var MasterTransction = LicenseMgmt.GetPreviousLicenseDetail(LicenseID);
                        if (MasterTransction != null)
                        {
                            //ViewState["LicenseType"] = MasterTransction.LicensetypeName;
                            Session["LicenseType"] = MasterTransction.LicensetypeName;
                            Session["LicenseTypeID"] = MasterTransction.LicenseTypeID;
                            Session["LicenseID"] = MasterTransction.LicenseID;
                            Session["IsPermanantActive"] = MasterTransction.IsPermanantActive;
                            lblLicenseType.Text = MasterTransction.LicensetypeName;
                            lblLicenseNumber.Text = MasterTransction.LicenseNo;
                            lblLicenseTitle.Text = MasterTransction.Licensetitle;
                            txtLicenseTitle.Text = MasterTransction.Licensetitle;
                            lblApplicationDate.Text = Convert.ToDateTime(MasterTransction.ApplicationDate).ToString("dd-MMM-yyyy");
                            lblStartdate.Text = Convert.ToDateTime(MasterTransction.StartDate).ToString("dd-MMM-yyyy");
                            lblEndDate.Text = Convert.ToDateTime(MasterTransction.EndDate).ToString("dd-MMM-yyyy");
                            txtCost.Text = Convert.ToString(MasterTransction.Cost);
                            ChkIsActive.Checked = MasterTransction.IsPermanantActive;

                            var documentVersionData = LicenseDocumentManagement.GetFileData(LicenseID, -1).Select(x => new
                            {
                                ID = x.ID,
                                ScheduledOnID = x.ScheduledOnID,
                                LicenseID = x.LicenseID,
                                FileID = x.FileID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                                ISLink = x.ISLink,
                                FilePath = x.FilePath,
                                FileName = x.FileName
                            }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();


                            rptLicenseVersion.DataSource = documentVersionData;
                            rptLicenseVersion.DataBind();

                            rptLicenseDocumnets.DataSource = null;
                            rptLicenseDocumnets.DataBind();
                        }
                        if (complianceInfo.NonComplianceType != null)
                        {
                            //lblMonetoryNonMonetory.Text =Convert.ToString(complianceInfo.NonComplianceType);
                            mont.Value = Convert.ToString(complianceInfo.NonComplianceType);
                        }

                        lblComplianceID.Text = Convert.ToString(complianceInfo.ID);
                        lblCategory.Text = Convert.ToString(complianceCategoryInfo.Name);
                        lnkSampleForm.Text = Convert.ToString(complianceInfo.SampleFormLink);
                        lblComplianceDiscription.Text = complianceInfo.ShortDescription;
                        lblDetailedDiscription.Text = complianceInfo.Description;
                        lblFrequency.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1));
                        lblRefrenceText.Text = WebUtility.HtmlEncode(complianceInfo.ReferenceMaterialText);
                        lblPenalty.Text = complianceInfo.PenaltyDescription;
                        string risk = Business.ComplianceManagement.GetRiskType(complianceInfo, complianceInstanceID);
                        lblRisk.Text = risk;
                        lblRiskType.Text = Business.ComplianceManagement.GetRisk(complianceInfo, complianceInstanceID);
                        var LICInfo = Business.ActManagement.GetByLICID(LicenseID);
                        txtFileno.Text = LICInfo.FileNO;
                        txtphysicalLocation.Text = LICInfo.PhysicalLocation;

                        var ActInfo = Business.ActManagement.GetByID(complianceInfo.ActID);

                        if (ActInfo != null)
                        {
                            lblActName.Text = ActInfo.Name;
                        }

                        lblNatureofcompliance.Text = "";
                        txtValueAsPerSystem.Text = "";
                        txtValueAsPerReturn.Text = "";
                        txtLiabilityPaid.Text = "";
                        txtDiffAB.Text = "";
                        txtDiffBC.Text = "";

                        if (complianceInfo.NatureOfCompliance != null)
                        {
                            lblNatureofcomplianceID.Text = Convert.ToString(complianceInfo.NatureOfCompliance);
                            string Nature = Business.ComplianceManagement.GetNatureOfComplianceFromID(Convert.ToInt32(complianceInfo.NatureOfCompliance));
                            lblNatureofcompliance.Text = Nature.ToString();

                            if (complianceInfo.NatureOfCompliance == 1)
                            {
                                trreturn.Visible = true;
                                lblValueAsPerSystem.InnerText = "Liability as per system (A)";
                                lblValueAsPerReturn.InnerText = "Liability as per return (B)";
                            }
                            else if (complianceInfo.NatureOfCompliance == 0)
                            {
                                fieldsetTDS.Visible = true;
                                lblValueAsPerSystem.InnerText = "Values as per system (A)";
                                lblValueAsPerReturn.InnerText = "Values as per return (B)";
                                trreturn.Visible = false;
                            }
                            else
                            {
                                fieldsetTDS.Visible = false;
                            }
                        }
                        else
                        {
                            fieldsetTDS.Visible = false;
                        }

                        if (risk == "HIGH")
                        {
                            divRiskType.Attributes["style"] = "background-color:red;";
                        }
                        else if (risk == "CRITICAL")
                        {
                            divRiskType.Attributes["style"] = "background-color:red;";
                        }
                        else if (risk == "MEDIUM")
                        {
                            divRiskType.Attributes["style"] = "background-color:yellow;";
                        }
                        else if (risk == "LOW")
                        {
                            divRiskType.Attributes["style"] = "background-color:green;";
                        }
                        lblRule.Text = complianceInfo.Sections;
                        lblFormNumber.Text = complianceInfo.RequiredForms;
                    }
                    if (RecentComplianceTransaction.ComplianceStatusID == 10)
                    {                        
                        BindDocument(ScheduledOnID);
                    }
                    else
                    {                        
                        divDeleteDocument.Visible = false;
                        rptComplianceDocumnets.DataSource = null;
                        rptComplianceDocumnets.DataBind();

                        rptWorkingFiles.DataSource = null;
                        rptWorkingFiles.DataBind();
                    }

                    if (complianceInfo.UploadDocument == true && complianceForm != null)
                    {
                        lbDownloadSample.Text = "Download";
                        lbDownloadSample.CommandArgument = complianceForm.ComplianceID.ToString();
                        sampleFormPath = complianceForm.FilePath;
                        sampleFormPath = sampleFormPath.Substring(2, sampleFormPath.Length - 2);

                        lblNote.Visible = true;
                        lnkViewSampleForm.Visible = true;
                        lblSlash.Visible = true;
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            lblpathsample.Text = sampleFormPath;
                        }
                        else
                        {
                            lblpathsample.Text = sampleFormPath;
                        }
                    }
                    else
                    {
                        lblNote.Visible = false;
                        lblSlash.Visible = false;
                        lnkViewSampleForm.Visible = false;
                        sampleFormPath = "";
                    }

                    btnSave.Attributes.Remove("disabled");
                    if (RecentComplianceTransaction.ComplianceStatusID == 1 || RecentComplianceTransaction.ComplianceStatusID == 6 || RecentComplianceTransaction.ComplianceStatusID == 10 || RecentComplianceTransaction.ComplianceStatusID == 13 || RecentComplianceTransaction.ComplianceStatusID == 14)
                    {
                        if (complianceInfo.EventFlag == true)
                        {
                            if (complianceInfo.UpDocs == true)
                            {
                                divUploadDocument.Visible = true;
                                divWorkingfiles.Visible = true;
                                btnSave.Visible = true;
                                btnSaveDOCNotCompulsory.Visible = false;
                                lblDocComplasary.Visible = true;
                            }
                            else
                            {                               
                                btnSaveDOCNotCompulsory.Visible = true;
                                btnSave.Visible = false;
                                divUploadDocument.Visible = true;
                                divWorkingfiles.Visible = true;
                                lblDocComplasary.Visible = false;
                            }
                        }
                        else
                        {
                            divUploadDocument.Visible = true;
                            divWorkingfiles.Visible = true;
                            btnSave.Visible = true;
                            btnSaveDOCNotCompulsory.Visible = false;
                            lblDocComplasary.Visible = true;
                        }
                    }
                    else if (RecentComplianceTransaction.ComplianceStatusID != 1)
                    {
                        divUploadDocument.Visible = false;
                        divWorkingfiles.Visible = false;
                        if ((complianceInfo.UploadDocument ?? false))
                        {
                            btnSave.Attributes.Add("disabled", "disabled");
                        }
                    }                   
                    fuSampleFile.Enabled = true;
                    FileUpload1.Enabled = true;
                    BindStatusList(Convert.ToInt32(RecentComplianceTransaction.ComplianceStatusID));                  
                    BindTransactions(ScheduledOnID);
                    tbxRemarks.Text = string.Empty;
                    hdnComplianceInstanceID.Value = complianceInstanceID.ToString();
                    hdnComplianceScheduledOnId.Value = ScheduledOnID.ToString();
                    upComplianceDetails.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }           
        }        
        public void BindDocument(int ScheduledOnID)
        {
            try
            {
                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();
                divDeleteDocument.Visible = false;
                ComplianceDocument = DocumentManagement.GetFileData1(ScheduledOnID).Where(entry => entry.Version.Equals("1.0")).ToList();
                rptComplianceDocumnets.DataSource = ComplianceDocument.Where(entry => entry.FileType == 1).ToList();
                rptComplianceDocumnets.DataBind();

                rptWorkingFiles.DataSource = ComplianceDocument.Where(entry => entry.FileType == 2).ToList();
                rptWorkingFiles.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }       
        protected void grdTransactionHistory_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var complianceTransactionHistory = Business.ComplianceManagement.GetAllTransactionLog(Convert.ToInt32(ViewState["ScheduledOnID"]));
                if (direction == SortDirection.Ascending)
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }
                foreach (DataControlField field in grdTransactionHistory.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndexHistory"] = grdTransactionHistory.Columns.IndexOf(field);
                    }
                }
                grdTransactionHistory.DataSource = complianceTransactionHistory;
                grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTransactionHistory.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["ScheduledOnID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactions(int ScheduledOnID)
        {
            try
            {
                grdTransactionHistory.DataSource = Business.ComplianceManagement.GetAllTransactionLog(ScheduledOnID);
                grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindEscalationStatusList()
        {
            try
            {
                ddlStatus.DataSource = null;
                ddlStatus.DataBind();
                ddlStatus.ClearSelection();

                ddlStatus.DataTextField = "Name";
                ddlStatus.DataValueField = "ID";

                var statusList = ComplianceStatusManagement.GetEscalationStatusList();

                ddlStatus.DataSource = statusList;
                ddlStatus.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindStatusList(int statusID)
        {
            try
            {
                ddlStatus.DataSource = null;
                ddlStatus.DataBind();
                ddlStatus.ClearSelection();            
                ddlStatus.DataTextField = "StatusName";
                ddlStatus.DataValueField = "ID";
               
                string LicTypeName = Convert.ToString(Session["LicenseType"]);
                long LicTypeID = Convert.ToInt32(Session["LicenseTypeID"]);
                
                List<Lic_tbl_StatusMaster> statusList;
                string status = string.Empty;
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                     status = (from row in entities.Lic_tbl_type
                               where row.CustomerID == customerID &&
                               LicTypeName.Contains(row.Status)
                               select row.Status).FirstOrDefault();

                }
                if (status != null)
                {
                    if (LicTypeName == status)
                    {
                        statusList = LicenseMgmt.GetStatusListIPR();
                    }
                    else
                    {
                        statusList = LicenseMgmt.GetStatusList();
                    }
                }
                else
                {
                    statusList = LicenseMgmt.GetStatusList();
                }
                              
                ddlStatus.DataSource = statusList.OrderBy(entry => entry.StatusName).ToList(); ;
                ddlStatus.DataBind();

                ddlStatus.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public int comparedate(DateTime DateA, DateTime DateB)
        {
            DateTime dt1 = DateTime.ParseExact(DateA.ToString("dd-MM-yyyy"), "dd-MM-yyyy", null);
            DateTime dt2 = DateTime.ParseExact(DateB.ToString("dd-MM-yyyy"), "dd-MM-yyyy", null);

            //if (momentA > momentB) return 1;
            //else if (momentA < momentB) return -1;
            //else return 0;

            int cmp = dt1.CompareTo(dt2);

            if (cmp > 0)
            {
                return cmp;
                // date1 is greater means date1 is comes after date2
            }
            else if (cmp < 0)
            {
                return cmp;
                // date2 is greater means date1 is comes after date1
            }
            else
            {
                return cmp;
                // date1 is same as date2
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            var CSOID = Convert.ToInt64(hdnComplianceScheduledOnId.Value);
            var RCT = Business.ComplianceManagement.GetCurrentStatusByComplianceID((int)CSOID);
            if (RCT.ComplianceStatusID == 2 || RCT.ComplianceStatusID == 3 || RCT.ComplianceStatusID == 4 || RCT.ComplianceStatusID == 5)
            {
                cvDuplicateEntry.IsValid = false;           
                cvDuplicateEntry.ErrorMessage = "This compliance is already submitted for review or already closed.";
            }
            else
            {
                #region Save Code
                List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                try
                {
                    bool isValidFile = true;                  
                    if (isValidFile == true)
                    {
                        if (txtInterest.Text == "")
                            txtInterest.Text = "0";
                        if (txtPenalty.Text == "")
                            txtPenalty.Text = "0";
                        if (txtValueAsPerSystem.Text == "")
                            txtValueAsPerSystem.Text = "0";
                        if (txtValueAsPerReturn.Text == "")
                            txtValueAsPerReturn.Text = "0";
                        if (txtLiabilityPaid.Text == "")
                            txtLiabilityPaid.Text = "0";
                        if (lblNatureofcomplianceID.Text == "")
                            lblNatureofcomplianceID.Text = "8";
                        
                        long? StatusID = ComplianceManagement.Business.ComplianceManagement.GetCurrentStatusByComplianceID(Convert.ToInt32(hdnComplianceScheduledOnId.Value)).ComplianceStatusID;
                        var PenaltySubmit = "";
                        int lblStatus = -1;

                        var a = comparedate(Convert.ToDateTime(lblApplicationDate.Text.Trim()), DateTimeExtensions.GetDate(tbxDate.Text.Trim()));
                        if (a == 1)
                        {
                            lblStatus = 2;
                        }
                        else if (a == -1)
                        {
                            lblStatus = 3;
                        }
                        else if (a == 0)
                        {
                            lblStatus = 2;
                        }
                        if (lblStatus == 3)
                        {
                            PenaltySubmit = "P";
                        }
                        if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                        {
                            if (Convert.ToInt32(ddlStatus.SelectedValue) == 12)
                            {
                                lblStatus = 4;
                            }
                        }
                      
                        ComplianceTransaction transaction = new ComplianceTransaction()
                        {
                            ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                            ComplianceInstanceId = Convert.ToInt64(hdnComplianceInstanceID.Value),
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedByText = AuthenticationHelper.User,                       
                            StatusId = Convert.ToInt32(lblStatus),
                            StatusChangedOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                            Remarks = tbxRemarks.Text,
                            Interest = Convert.ToDecimal(txtInterest.Text),
                            Penalty = Convert.ToDecimal(txtPenalty.Text),
                            ComplianceSubTypeID = Convert.ToInt32(lblNatureofcomplianceID.Text),
                            ValuesAsPerSystem = Convert.ToDecimal(txtValueAsPerSystem.Text),
                            ValuesAsPerReturn = Convert.ToDecimal(txtValueAsPerReturn.Text),
                            LiabilityPaid = Convert.ToDecimal(txtLiabilityPaid.Text),
                            IsPenaltySave = chkPenaltySave.Checked,
                            PenaltySubmit = PenaltySubmit,
                            PhysicalLocation=txtphysicalLocation.Text,
                            FileNO=txtFileno.Text,
                        };
                        var leavedetails = Business.ComplianceManagement.GetUserLeavePeriodExists(AuthenticationHelper.UserID, "P");
                        if (leavedetails != null)
                        {
                            transaction.OUserID = leavedetails.OldPerformerID;
                        }
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            long ComplianceInstanceID = Convert.ToInt64(hdnComplianceInstanceID.Value);
                            var LicenseID = (from row in entities.Lic_tbl_LicenseComplianceInstanceMapping
                                             where row.ComplianceInstanceID == ComplianceInstanceID
                                             select row.LicenseID).FirstOrDefault();

                            if (ddlStatus.SelectedValue == "7") //renewed
                            {
                                #region Save License Instance 
                                Lic_tbl_LicenseInstance licenseRecord = new Lic_tbl_LicenseInstance()
                                {
                                    UpdatedBy = AuthenticationHelper.UserID
                                };

                                if (!string.IsNullOrEmpty(txtLicenseNo.Text))
                                    licenseRecord.LicenseNo = Convert.ToString(txtLicenseNo.Text);

                                if (!string.IsNullOrEmpty(txtLicenseTitle.Text))
                                    licenseRecord.LicenseTitle = Convert.ToString(txtLicenseTitle.Text);

                                if (!string.IsNullOrEmpty(txtStartDate.Text))
                                    licenseRecord.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);
                                
                                if (!string.IsNullOrEmpty(txtEndDate.Text))
                                licenseRecord.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);

                                if (ChkIsActive.Checked)
                                {
                                    licenseRecord.IsPermanantActive = true;
                                }
                                else
                                {
                                    licenseRecord.IsPermanantActive = false;
                                }
                                if (!string.IsNullOrEmpty(txtCost.Text))
                                    licenseRecord.Cost = Convert.ToDecimal(txtCost.Text);

                                if (!string.IsNullOrEmpty(txtFileno.Text))
                                {
                                    licenseRecord.FileNO = Convert.ToString(txtFileno.Text);
                                }


                                if (!string.IsNullOrEmpty(txtphysicalLocation.Text))
                                    licenseRecord.PhysicalLocation = Convert.ToString(txtphysicalLocation.Text);


                                licenseRecord.ID = LicenseID;

                                LicenseMgmt.UpdateLicenseNew(licenseRecord);


                                
                                #endregion
                            }

                            Lic_tbl_LicenseInstance licenseRecord1 = new Lic_tbl_LicenseInstance()
                            {
                                UpdatedBy = AuthenticationHelper.UserID
                            };
                            if (ChkIsActive.Checked)
                            {
                                licenseRecord1.IsPermanantActive = true;
                            }
                            else
                            {
                                licenseRecord1.IsPermanantActive = false;
                            }
                            if (!string.IsNullOrEmpty(txtCost.Text))
                                licenseRecord1.Cost = Convert.ToDecimal(txtCost.Text);

                            if (!string.IsNullOrEmpty(txtFileno.Text))
                                licenseRecord1.FileNO = Convert.ToString(txtFileno.Text);

                            if (!string.IsNullOrEmpty(txtphysicalLocation.Text))
                                licenseRecord1.PhysicalLocation = Convert.ToString(txtphysicalLocation.Text);

                            licenseRecord1.ID = LicenseID;
                            LicenseMgmt.UpdateLicenseCost(licenseRecord1);

                            Lic_tbl_LicenseInstance_Log licenseRecordLog = new Lic_tbl_LicenseInstance_Log()
                            {
                                UpdatedBy = AuthenticationHelper.UserID,
                                UpdatedOn = DateTime.Now,
                            };
                            if (ChkIsActive.Checked)
                            {
                                licenseRecordLog.IsPermanantActive = true;
                            }
                            else
                            {
                                licenseRecordLog.IsPermanantActive = false;
                            }

                            if (!string.IsNullOrEmpty(txtCost.Text))
                                licenseRecordLog.Cost = Convert.ToDecimal(txtCost.Text);

                            licenseRecordLog.LicenseID = LicenseID;
                            LicenseMgmt.UpdateLicenseLogNew(licenseRecordLog);
                            Lic_tbl_LicenseStatusTransaction license = new Lic_tbl_LicenseStatusTransaction()
                            {
                                CustomerID = (int)AuthenticationHelper.CustomerID,
                                LicenseID = LicenseID,
                                StatusID = Convert.ToInt32(ddlStatus.SelectedValue),
                                StatusChangeOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                IsActive = true,
                                CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                CreatedOn = DateTime.Now,
                                UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                UpdatedOn = DateTime.Now,
                                Remark = "Change license status to " + ddlStatus.SelectedItem.Text + ".",
                                ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                            };
                            entities.Lic_tbl_LicenseStatusTransaction.Add(license);
                            entities.SaveChanges();


                            Lic_tbl_LicenseAudit_Log newLicenseInstance_Log = new Lic_tbl_LicenseAudit_Log()
                            {
                                CustomerID = (int)AuthenticationHelper.CustomerID,
                                LicenseID = LicenseID,
                                StatusID = Convert.ToInt32(ddlStatus.SelectedValue),
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                Remark = "Change license status to " + ddlStatus.SelectedItem.Text + "."
                            };
                            if (!string.IsNullOrEmpty(txtStartDate.Text))
                                newLicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);

                            if (!string.IsNullOrEmpty(txtEndDate.Text))
                                newLicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                            entities.Lic_tbl_LicenseAudit_Log.Add(newLicenseInstance_Log);
                            entities.SaveChanges();

                        }
                        

                        List<FileData> files = new List<FileData>();
                        List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                        bool blankfileCount = true;
                        string directoryPath = null;
                        var TempDocData = Business.ComplianceManagement.GetTempComplianceDocumentData(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                        if (TempDocData.Count > 0)
                        {
                            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));

                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                           
                                string version = null;
                                if (StatusID == 6) //if previous status is rejected(not complied) than for uploading new documents new version is created
                                {
                                    version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(hdnComplianceScheduledOnId.Value));
                                    directoryPath = "avacomdocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                                }
                                else
                                {
                                    version = "1.0";
                                    directoryPath = "avacomdocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                                }
                                foreach (var item in TempDocData)
                                {
                                    if (item.ISLink)
                                    {
                                        String fileName = "";
                                        if (item.DocType == "S")
                                        {
                                            fileName = "ComplianceDoc_" + item.DocName;
                                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                                        }
                                        else
                                        {
                                            fileName = "WorkingFiles_" + item.DocName;
                                            list.Add(new KeyValuePair<string, int>(fileName, 2));
                                        }
                                        FileData file = new FileData()
                                        {
                                            Name = fileName,
                                            FilePath = item.DocPath,
                                            VersionDate = DateTime.UtcNow,
                                            ISLink = true,
                                            Version = version
                                        };
                                        files.Add(file);
                                    }
                                    else
                                    {
                                        String fileName = "";
                                        if (item.DocType == "S")
                                        {
                                            fileName = "ComplianceDoc_" + item.DocName;
                                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                                        }
                                        else
                                        {
                                            fileName = "WorkingFiles_" + item.DocName;
                                            list.Add(new KeyValuePair<string, int>(fileName, 2));
                                        }
                                        Guid fileKey = Guid.NewGuid();
                                        string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));
                                        Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));
                                        if (item.DocData.Length > 0)
                                        {
                                            string filepathvalue = string.Empty;
                                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                            filepathvalue = vale.Replace(@"\", "/");


                                            FileData file = new FileData()
                                            {
                                                Name = fileName,
                                                FilePath = filepathvalue,
                                                FileKey = fileKey.ToString(),
                                                //Version = "1.0",
                                                Version = version,
                                                VersionDate = DateTime.UtcNow,
                                                FileSize = item.FileSize,
                                            };
                                            files.Add(file);
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(item.DocName))
                                                blankfileCount = false;
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                string version = null;
                                if (StatusID == 6) //if previous status is rejected(not complied) than for uploading new documents new version is created
                                {
                                    version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(hdnComplianceScheduledOnId.Value));
                                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                    {
                                        directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                                    }
                                    else
                                    {
                                        directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/" + version);
                                    }
                                }
                                else
                                {
                                    version = "1.0";
                                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                    {
                                        directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                                    }
                                    else
                                    {
                                        directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/" + version);
                                    }
                                }
                                DocumentManagement.CreateDirectory(directoryPath);
                                foreach (var item in TempDocData)
                                {
                                    if (item.ISLink)
                                    {
                                        String fileName = "";
                                        if (item.DocType == "S")
                                        {
                                            fileName = "ComplianceDoc_" + item.DocName;
                                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                                        }
                                        else
                                        {
                                            fileName = "WorkingFiles_" + item.DocName;
                                            list.Add(new KeyValuePair<string, int>(fileName, 2));
                                        }
                                        FileData file = new FileData()
                                        {
                                            Name = fileName,
                                            FilePath = item.DocPath,
                                            VersionDate = DateTime.UtcNow,
                                            ISLink = true,
                                            Version = version
                                        };
                                        files.Add(file);
                                    }
                                    else
                                    {
                                        String fileName = "";
                                        if (item.DocType == "S")
                                        {
                                            fileName = "ComplianceDoc_" + item.DocName;
                                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                                        }
                                        else
                                        {
                                            fileName = "WorkingFiles_" + item.DocName;
                                            list.Add(new KeyValuePair<string, int>(fileName, 2));
                                        }
                                        Guid fileKey = Guid.NewGuid();
                                        string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));
                                        Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));
                                        if (item.DocData.Length > 0)
                                        {
                                            string filepathvalue = string.Empty;
                                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                            {
                                                string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                filepathvalue = vale.Replace(@"\", "/");
                                            }
                                            else
                                            {
                                                filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                            }


                                            FileData file = new FileData()
                                            {
                                                Name = fileName,
                                                FilePath = filepathvalue,
                                                FileKey = fileKey.ToString(),
                                                //Version = "1.0",
                                                Version = version,
                                                VersionDate = DateTime.UtcNow,
                                                FileSize = item.FileSize,
                                            };
                                            files.Add(file);
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(item.DocName))
                                                blankfileCount = false;
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                        bool flag = false;
                        if (blankfileCount)
                        {
                            flag = Business.ComplianceManagement.CreateTransaction(transaction, files, list, Filelist, directoryPath, Convert.ToInt64(hdnComplianceScheduledOnId.Value), CustId);
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                        }
                        //bool flag = true;
                        if (flag != true)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                        }
                        if (flag)
                        {
                            try
                            {
                                Business.ComplianceManagement.BindDatainRedis(Convert.ToInt64(hdnComplianceInstanceID.Value), Convert.ToInt32(AuthenticationHelper.CustomerID));
                            }
                            catch { }
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Saved Sucessfully.";
                            ValidationSummary1.CssClass = "alert alert-success";
                        }
                        if (OnSaved != null)
                        {
                            OnSaved(this, null);
                        }
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fcloseandcallcal();", true);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Invalid file uploded. .exe,.bat,.dll formats not supported.";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
                #endregion

            }
        }

        protected void grdTransactionHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DOWNLOAD_FILE"))
                {
                    int fileID = Convert.ToInt32(e.CommandArgument);
                    var file = Business.ComplianceManagement.GetFile(fileID);
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                    //Response.BinaryWrite(file.Data); // create the file
                    Response.Flush(); // send it to the client to download
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(Convert.ToInt32(hdlSelectedDocumentID.Value));

                Response.Buffer = true;
                //Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.Data); // create the file
                Response.End();
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void lbDownloadSample_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.ComplianceManagement.GetComplianceFormByID(Convert.ToInt32(lbDownloadSample.CommandArgument));
                Response.Buffer = true;
                Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                Response.BinaryWrite(file.FileData); // create the file
                Response.Flush(); // send it to the client to download

                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {

                //DateTime date1 = DateTime.MinValue;
                //if (DateTime.TryParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date1))
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer1", string.Format("initializeDatePickerforPerformer1(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer1", "initializeDatePickerforPerformer1(null);", true);
                //}

                // ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Script", "initializeComboboxUpcoming();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_RowCreated(object sender, GridViewRowEventArgs e)
        {           
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        public void OpenTransactionPage(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {
                ViewState["ScheduledOnID"] = ScheduledOnID;
                ViewState["complianceInstanceID"] = complianceInstanceID;
                tbxRemarks.Text = tbxDate.Text = string.Empty;
                BindTransactionDetails(ScheduledOnID, complianceInstanceID);             
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public event EventHandler OnSaved;

        protected void btnUpload_Click(object sender, EventArgs e)
        {

            try
            {
                HttpFileCollection fileCollection = Request.Files;
                for (int i = 0; i < fileCollection.Count; i++)
                {
                    HttpPostedFile uploadfile = fileCollection[i];
                    string fileName = Path.GetFileName(uploadfile.FileName);
                    if (uploadfile.ContentLength > 0)
                    {                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    DeleteFile(Convert.ToInt32(e.CommandArgument));
                    BindDocument(Convert.ToInt32(ViewState["ScheduledOnID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    DeleteFile(Convert.ToInt32(e.CommandArgument));
                    BindDocument(Convert.ToInt32(ViewState["ScheduledOnID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = string.Empty;
                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {

                        string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                        filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);

                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void DeleteFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);
                if (file != null)
                {
                    string path = string.Empty;
                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        string pathvalue = file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name);
                        path = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);

                        // path = file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name);
                    }
                    else
                    {
                        path = Server.MapPath(file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name));
                    }
                    DocumentManagement.DeleteFile(path, fileId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lbtLinkDocbutton = (LinkButton)e.Item.FindControl("lbtLinkDocbutton");
                scriptManager.RegisterAsyncPostBackControl(lbtLinkDocbutton);

            }
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lbtLinkbutton = (LinkButton)e.Item.FindControl("lbtLinkbutton");
                scriptManager.RegisterAsyncPostBackControl(lbtLinkbutton);
            }
        }

        protected void lnkSampleForm_Click(object sender, EventArgs e)
        {
            try
            {
                if (lnkSampleForm.Text != "")
                {
                    string url = lnkSampleForm.Text;

                    string fullURL = "window.open('" + url + "', '_blank', 'height=500,width=800,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
                    lnkSampleForm.Attributes.Add("OnClick", fullURL);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkViewSampleForm_Click(object sender, EventArgs e)
        {
        }        
        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                chkPenaltySave.Checked = false;
                txtInterest.Text = "";
                txtPenalty.Text = "";
                txtLicenseNo.Text = "";
                //txtLicenseTitle.Text = "";
                txtStartDate.Text = "";
                txtEndDate.Text = "";
                txtInterest.ReadOnly = false;
                txtPenalty.ReadOnly = false;

                if (ddlStatus.SelectedValue == "7")
                {
                    fieldsetRenewal.Visible = true;                    
                }
                else {
                    fieldsetRenewal.Visible = false;
                }
                if(ddlStatus.SelectedValue=="12")
                {
                    lblDocComplasary.Visible = false;
                    Label3.Visible = false;
                    lblDate.Visible = false;
                    tbxDate.Text = Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy");
                   
                }
                else
                {
                    lblDocComplasary.Visible = true;
                    Label3.Visible = true;
                    lblDate.Visible = true;
                    tbxDate.Text = "";

                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "initchag();", true);
                 
                if (!string.IsNullOrEmpty(tbxDate.Text))
                {
                   
                    int lblStatus = -1;
                    var a = comparedate(Convert.ToDateTime(lblApplicationDate.Text.Trim()), DateTimeExtensions.GetDate(tbxDate.Text.Trim()));
                    if (a == 1)
                    {
                        lblStatus = 2;
                    }
                    else if (a == -1)
                    {
                        lblStatus = 3;
                    }
                    //comment by Rahul on 16 JAN 2019
                    if (lblStatus == 3)
                    {
                        var data = Business.ComplianceManagement.CheckMonetary(Convert.ToInt32(lblComplianceID.Text));
                        if (data == null)
                        {
                        }
                        else
                        {
                            if (data.NonComplianceType == 1)
                            {
                            }
                            else
                            {
                                //style = "border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%; display: none;"
                                fieldsetpenalty.Attributes["style"] = "border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%; Display:block;";
                            }
                        }
                    }
                }
                
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private bool BindSubTasks(long ComplianceScheduleOnID, int roleID, string period, int CustomerBranchid)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var documentData = (from row in entities.SP_TaskInstanceTransactionStatutoryView(customerID)
                                        where row.ParentID == null
                                        && row.ForMonth == period
                                        && row.RoleID == roleID
                                        && row.CustomerBranchID == CustomerBranchid
                                        && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                        select row).ToList();

                    if (documentData.Count != 0)
                    {
                        divTask.Visible = true;
                        gridSubTask.DataSource = documentData;
                        gridSubTask.DataBind();

                        var closedSubTaskCount = documentData.Where(entry => (entry.TaskStatusID == 4 || entry.TaskStatusID == 5)).Count();

                        if (documentData.Count == closedSubTaskCount)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        divTask.Visible = false;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }


        protected string GetUserName(long taskInstanceID, long taskScheduleOnID, int roleID, byte taskType)
        {
            try
            {
                string result = "";

                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                result = TaskManagment.GetTaskAssignedUser(customerID, taskType, taskInstanceID, taskScheduleOnID, roleID);

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cv.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }
        }

        protected void gridSubTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (e.CommandName.Equals("Download"))
                    {
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                if (taskDocument.Count > 0)
                                {
                                    foreach (var item in taskDocument)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), eachFile.FileName);
                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = eachFile.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str,  DocumentManagement.ReadDocFiles(filePath));
                                            
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                if (taskDocument.Count > 0)
                                {
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        //comment by rahul on 20 JAN 2017
                                        string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                        //string filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));

                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = eachFile.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            if (eachFile.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }

                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptComplianceVersionView.DataBind();

                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessage.Text = "";
                                                lblMessage.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                subTaskDocViewPath = filePath1;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview('" + subTaskDocViewPath + "');", true);
                                                lblMessage.Text = "";
                                                UpdatePanel4.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                               
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptComplianceVersionView.DataBind();

                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessage.Text = "";
                                                lblMessage.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();

                                                subTaskDocViewPath = FileName;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview('" + subTaskDocViewPath + "');", true);
                                                lblMessage.Text = "";
                                                UpdatePanel4.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                        #region

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);
                int taskFileidID = Convert.ToInt32(commandArgs[2]);
                if (taskScheduleOnID != 0)
                {
                    if (e.CommandName.Equals("View"))
                    {
                        #region
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();
                        List<GetTaskDocumentView> taskFileData = new List<GetTaskDocumentView>();
                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
                        taskFileData = taskDocumenttoView;
                        taskDocumenttoView = taskDocumenttoView.Where(entry => entry.FileID == taskFileidID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    rptComplianceVersionView.DataSource = taskFileData.OrderBy(entry => entry.Version);
                                    rptComplianceVersionView.DataBind();

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }

                                    foreach (var file in taskDocumenttoView)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessage.Text = "";
                                                lblMessage.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                subTaskDocViewPath = filePath1;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);
                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview('" + subTaskDocViewPath + "');", true);
                                                lblMessage.Text = "";
                                                //UpdatePanel4.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    rptComplianceVersionView.DataSource = taskFileData.OrderBy(entry => entry.Version);
                                    rptComplianceVersionView.DataBind();

                                    foreach (var file in taskDocumenttoView)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessage.Text = "";
                                                lblMessage.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();

                                                subTaskDocViewPath = FileName;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview('" + subTaskDocViewPath + "');", true);
                                                lblMessage.Text = "";
                                                //UpdatePanel4.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                        #endregion                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gridSubTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    Label lblSlashReview = (Label)e.Row.FindControl("lblSlashReview");
                    LinkButton btnSubTaskDocView = (LinkButton)e.Row.FindControl("btnSubTaskDocView");
                    LinkButton btnSubTaskDocDownload = (LinkButton)e.Row.FindControl("btnSubTaskDocDownload");
                    CheckBox chkTask = (CheckBox)e.Row.FindControl("chkTask");
                    Label lblIsTaskClose = (Label)e.Row.FindControl("lblIsTaskClose");

                    if (lblStatus != null && btnSubTaskDocDownload != null && lblSlashReview != null && btnSubTaskDocView != null) /*&& lblSlashReview != null*/
                    {
                        if (lblStatus.Text != "")
                        {
                            if (lblStatus.Text == "Open")
                            {
                                btnSubTaskDocDownload.Visible = false;
                                lblSlashReview.Visible = false;
                                btnSubTaskDocView.Visible = false;
                            }
                            else
                            {
                                if (lblIsTaskClose.Text == "True")
                                {
                                    chkTask.Enabled = false;
                                    chkTask.Checked = true;
                                    btnSubTaskDocDownload.Visible = false;
                                    lblSlashReview.Visible = false;
                                    btnSubTaskDocView.Visible = false;
                                }
                                else
                                {
                                    btnSubTaskDocDownload.Visible = true;
                                    lblSlashReview.Visible = true;
                                    btnSubTaskDocView.Visible = true;
                                }
                            }
                        }
                    }
                }

                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    Label lblStatus = (Label) e.Row.FindControl("lblStatus");
                //    Label lblSlashReview = (Label) e.Row.FindControl("lblSlashReview");
                //    LinkButton btnSubTaskDocView = (LinkButton) e.Row.FindControl("btnSubTaskDocView");
                //    LinkButton btnSubTaskDocDownload = (LinkButton) e.Row.FindControl("btnSubTaskDocDownload");

                //    if (lblStatus != null && btnSubTaskDocDownload != null && lblSlashReview != null && btnSubTaskDocView != null)
                //    {
                //        if (lblStatus.Text != "")
                //        {
                //            if (lblStatus.Text == "Open")
                //            {
                //                btnSubTaskDocDownload.Visible = false;
                //                lblSlashReview.Visible = false;
                //                btnSubTaskDocView.Visible = false;
                //            }
                //            else
                //            {
                //                btnSubTaskDocDownload.Visible = true;
                //                lblSlashReview.Visible = true;
                //                btnSubTaskDocView.Visible = true;
                //            }
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        public static List<NameValueHierarchy> GetAllHierarchyTask(int TaskID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Tasks
                             where row.ID == TaskID
                             select row);

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Title }).OrderBy(entry => entry.Name).ToList();
                foreach (var item in hierarchy)
                {
                    Tasklist.Add(item.ID);
                    LoadSubEntitiesTask(item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntitiesTask(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            var query = (from row in entities.Tasks
                         where row.ParentID != null
                         && row.ParentID == nvp.ID
                         select row);

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Title }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                Tasklist.Add(item.ID);
                LoadSubEntitiesTask(item, false, entities);
            }
        }

        protected void lblNo_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow gvrow in gridSubTask.Rows)
                {
                    CheckBox chk = (CheckBox)gvrow.FindControl("chkTask");
                    if (chk != null & chk.Checked)
                    {
                        int TaskID = Convert.ToInt32(gridSubTask.DataKeys[gvrow.RowIndex].Value.ToString());
                        string str = gridSubTask.DataKeys[gvrow.RowIndex].Value.ToString();
                        Label lblTaskScheduledOnID = (Label)gvrow.FindControl("lblTaskScheduledOnID");
                        int TaskScheduledOnID = Convert.ToInt32(lblTaskScheduledOnID.Text);
                        Label lblComplianceScheduleOnID = (Label)gvrow.FindControl("lblComplianceScheduleOnID");
                        int ComplianceScheduleOnID = Convert.ToInt32(lblComplianceScheduleOnID.Text);
                        Label lbllblTaskInstanceID = (Label)gvrow.FindControl("lblTaskInstanceID");
                        int TaskInstanceID = Convert.ToInt32(lbllblTaskInstanceID.Text);
                        Label lblMainTaskID = (Label)gvrow.FindControl("lblMainTaskID");
                        int MainTaskID = Convert.ToInt32(lblMainTaskID.Text);
                        Label lblForMonth = (Label)gvrow.FindControl("lblForMonth");
                        string ForMonth = Convert.ToString(lblForMonth.Text);


                        if (TaskID != -1)
                        {
                            long? parentID = TaskID;
                            List<NameValueHierarchy> hierarchy = new List<NameValueHierarchy>();
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                Tasklist.Clear();
                                GetAllHierarchyTask(TaskID);
                                int customerID = -1;
                                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                                var documentData = (from row in entities.TaskInstanceTransactionViews
                                                    where Tasklist.Contains(row.TaskID)
                                                    && row.ForMonth == ForMonth
                                                    && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                                    && row.RoleID == 4
                                                    && row.CustomerID == customerID
                                                    select row).ToList();

                                for (int i = 0; i < documentData.Count; i++)
                                {
                                    var IsTaskTransactionPresent = TaskManagment.CheckTaskTransacion(Convert.ToInt64(documentData[i].TaskInstanceID), Convert.ToInt64(documentData[i].TaskScheduledOnID), Convert.ToInt64(documentData[i].ComplianceScheduleOnID));
                                    if (IsTaskTransactionPresent == false)
                                    {
                                        int StatusID = 4;
                                        TaskTransaction transaction = new TaskTransaction()
                                        {
                                            ComplianceScheduleOnID = Convert.ToInt64(documentData[i].ComplianceScheduleOnID),
                                            TaskScheduleOnID = Convert.ToInt64(documentData[i].TaskScheduledOnID),
                                            TaskInstanceId = Convert.ToInt64(documentData[i].TaskInstanceID),
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedByText = AuthenticationHelper.User,
                                            StatusId = StatusID,
                                            StatusChangedOn = DateTime.ParseExact(DateTime.Now.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                            Remarks = "Not applicable",
                                            IsTaskClose = true,
                                        };
                                        bool sucess = TaskManagment.CreateTaskTransaction(transaction);
                                    }
                                }
                            }
                        }
                    }
                }
                var IsAfter = "";
                var Period = "";
                int CustomerBrachID = -1;
                int ScheduledOnID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["IsAfter"])))
                {
                    IsAfter = Convert.ToString(ViewState["IsAfter"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Period"])))
                {
                    Period = Convert.ToString(ViewState["Period"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomerBrachID"])))
                {
                    CustomerBrachID = Convert.ToInt32(ViewState["CustomerBrachID"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ScheduledOnID"])))
                {
                    ScheduledOnID = Convert.ToInt32(ViewState["ScheduledOnID"]);
                }

                var showHideButton = false;
                if (IsAfter == "false")
                {
                    showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, CustomerBrachID);
                    btnSave.Enabled = showHideButton;
                }
                else
                {
                    showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, CustomerBrachID);
                    btnSave.Enabled = true;
                }
                upComplianceDetails.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        #region License Documents
        protected void rptLicenseVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblSlashReview = (Label)e.Item.FindControl("lblSlashReviewnew");
                Label lblpathIlink = (Label)e.Item.FindControl("lblpathIlink");
                LinkButton lblViewfile = (LinkButton)e.Item.FindControl("lnkViewDoc");
                LinkButton lblfilePath = (LinkButton)e.Item.FindControl("lblpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblLicenseDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
                if (lblpathIlink.Text == "True")
                {
                    lblViewfile.Visible = false;
                    lblfilePath.Visible = true;
                    lblDownLoadfile.Visible = false;
                    lblSlashReview.Visible = false;
                }
                else
                {
                    lblViewfile.Visible = true;
                    lblfilePath.Visible = false;
                    lblDownLoadfile.Visible = true;
                    lblSlashReview.Visible = true;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseVersionDoc");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);

            //    LinkButton lblLicenseDocumentVersion = (LinkButton)e.Item.FindControl("lblLicenseDocumentVersion");
            //    scriptManager.RegisterAsyncPostBackControl(lblLicenseDocumentVersion);
            //}
        }
        protected void rptLicenseVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                }
                else
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    List<SP_GetLicenseDocument_Result> ComplianceFileData = new List<SP_GetLicenseDocument_Result>();
                    List<SP_GetLicenseDocument_Result> ComplianceDocument = new List<SP_GetLicenseDocument_Result>();
                    ComplianceDocument = LicenseDocumentManagement.GetFileData(Convert.ToInt32(commandArgs[0]), -1).ToList();

                    if (commandArgs[2].Equals("1.0"))
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                        if (ComplianceFileData.Count <= 0)
                        {
                            ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                    }

                    if (e.CommandName.Equals("version"))
                    {
                        if (e.CommandName.Equals("version"))
                        {
                            rptLicenseDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                            rptLicenseDocumnets.DataBind();

                            rptLicenseWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                            rptLicenseWorkingFiles.DataBind();

                            var documentVersionData = ComplianceDocument.Select(x => new
                            {
                                ID = x.ID,
                                ScheduledOnID = x.ScheduledOnID,
                                LicenseID = x.LicenseID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                                FileID = x.FileID,
                                ISLink = x.ISLink,
                                FilePath = x.FilePath,
                                FileName = x.FileName
                            }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();

                            rptLicenseVersion.DataSource = documentVersionData;
                            rptLicenseVersion.DataBind();

                            //upComplianceDetails1.Update();                    
                        }
                    }
                    else if (e.CommandName.Equals("Download"))
                    {
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                var LicenseData = LicenseDocumentManagement.GetLicense(Convert.ToInt32(commandArgs[0]));

                                ComplianceZip.AddDirectoryByName(LicenseData.LicenseNo);
                                ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                                if (ComplianceFileData.Count > 0)
                                {
                                    foreach (var item in ComplianceFileData)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }

                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = file.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];                                            
                                            ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                            i++;
                                        }
                                    }
                                }
                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=LicenseDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                var LicenseData = LicenseDocumentManagement.GetLicense(Convert.ToInt32(commandArgs[0]));

                                ComplianceZip.AddDirectoryByName(LicenseData.LicenseNo);
                                ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                                if (ComplianceFileData.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = file.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }
                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=LicenseDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        
                        string[] commandArg = e.CommandArgument.ToString().Split(',');
                        List<SP_GetLicenseDocument_Result> CMPDocuments = LicenseDocumentManagement.GetFileData(Convert.ToInt32(commandArgs[0]), -1).ToList();
                        Session["LicenseID"] = commandArg[0];
                        if (CMPDocuments != null)
                        {
                            List<SP_GetLicenseDocument_Result> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                SP_GetLicenseDocument_Result entityData = new SP_GetLicenseDocument_Result();
                                entityData.Version = "1.0";
                                entityData.LicenseID = Convert.ToInt64(commandArg[0]);
                                entitiesData.Add(entityData);
                            }
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {

                                    CMPDocuments = CMPDocuments.Where(x => x.ISLink == false).ToList();

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }

                                    foreach (var file in CMPDocuments)
                                    {
                                        rptLicenseVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptLicenseVersionView.DataBind();

                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageReviewer1.Text = "";
                                                lblMessageReviewer1.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                LicenseDocPath = filePath1;
                                                LicenseDocPath = LicenseDocPath.Substring(2, LicenseDocPath.Length - 2);

                                                lblMessageReviewer1.Text = "";

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + LicenseDocPath + "');", true);
                                            }
                                        }
                                        else
                                        {
                                            lblMessageReviewer1.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                              
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    CMPDocuments = CMPDocuments.Where(x => x.ISLink == false).ToList();
                                    foreach (var file in CMPDocuments)
                                    {
                                        rptLicenseVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptLicenseVersionView.DataBind();
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageReviewer1.Text = "";
                                                lblMessageReviewer1.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();
                                                LicenseDocPath = FileName;

                                                LicenseDocPath = LicenseDocPath.Substring(2, LicenseDocPath.Length - 2);

                                                lblMessageReviewer1.Text = "";

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + LicenseDocPath + "');", true);
                                            }
                                        }
                                        else
                                        {
                                            lblMessageReviewer1.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptLicenseDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblpathCompIlink = (Label)e.Item.FindControl("lblCompDocpathIlink");
                LinkButton lblRedirectfile = (LinkButton)e.Item.FindControl("lblCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
                if (lblpathCompIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblRedirectfile.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblRedirectfile.Visible = false;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseDocumnets");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }
        protected void rptLicenseDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                else
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptLicenseWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblWorkCompDocpathIlink = (Label)e.Item.FindControl("lblWorkCompDocpathIlink");
                LinkButton lblWorkCompDocpathDownload = (LinkButton)e.Item.FindControl("lblWorkCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                if (lblWorkCompDocpathIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblWorkCompDocpathDownload.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblWorkCompDocpathDownload.Visible = false;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseWorkingFiles");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }
        protected void rptLicenseWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                else
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void rptLicenseVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblLicenseVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }
        protected void rptLicenseVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {

                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<SP_GetLicenseDocument_Result> CMPDocuments = LicenseDocumentManagement.GetFileData(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                    Session["LicenseID"] = commandArg[0];

                    if (CMPDocuments != null)
                    {
                        List<SP_GetLicenseDocument_Result> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            SP_GetLicenseDocument_Result entityData = new SP_GetLicenseDocument_Result();
                            entityData.Version = "1.0";
                            entityData.LicenseID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWs Storage
                            if (entitiesData.Count > 0)
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                foreach (var file in CMPDocuments)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }

                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessageReviewer1.Text = "";
                                            lblMessageReviewer1.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                        }
                                        else
                                        {
                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            LicenseDocPath = filePath1;
                                            LicenseDocPath = LicenseDocPath.Substring(2, LicenseDocPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + LicenseDocPath + "');", true);
                                            lblMessageReviewer1.Text = "";
                                        }
                                    }
                                    else
                                    {
                                        lblMessageReviewer1.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                    }
                                    break;
                                }
                            }
                            #endregion
             
                        }
                        else
                        {
                            #region Normal Storage
                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;
                                        string extension = System.IO.Path.GetExtension(filePath);

                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessageReviewer1.Text = "";
                                            lblMessageReviewer1.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }
                                            string customerID = Convert.ToString(AuthenticationHelper.CustomerID);
                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                            string FileName = DateFolder + "/" + User + "" + extension;
                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            LicenseDocPath = FileName;
                                            LicenseDocPath = LicenseDocPath.Substring(2, LicenseDocPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + LicenseDocPath + "');", true);
                                            lblMessageReviewer1.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessageReviewer1.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                    }
                                    break;
                                }
                            }
                            #endregion

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        protected void UploadlinkCompliancefile_Click(object sender, EventArgs e)
        {
            grdDocument.DataSource = null;
            grdDocument.DataBind();
            long ScheduledOnID = Convert.ToInt64(ViewState["ScheduledOnID"]);
            long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

            string url = TxtUploadocumentlnkLIc.Text;
            string fileName = DocumentManagement.getFileName(url);
            if (!string.IsNullOrEmpty(TxtUploadocumentlnkLIc.Text))
            {
                TempComplianceDocument tempComplianceDocument = null;
                var bytes = new byte[] { };// { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };  
                tempComplianceDocument = new TempComplianceDocument()
                {
                    ScheduleOnID = ScheduledOnID,
                    ComplianceInstanceID = complianceInstanceID,
                    DocPath = url,//userpath,
                    DocData = bytes,
                    DocName = fileName,
                    DocType = "S",
                    ISLink = true
                };

                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                if (_objTempDocumentID > 0)
                {
                    BindTempDocumentData(ScheduledOnID);
                }
            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalPer();", true);
            }
            TxtUploadocumentlnkLIc.Text = "";
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalPer();", true);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            grdDocument.DataSource = null;
            grdDocument.DataBind();
            long ScheduledOnID = Convert.ToInt64(ViewState["ScheduledOnID"]);
            long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

            string url = txtboxuploadworkingfiles.Text;
            string fileName = DocumentManagement.getFileName(url);
            if (!string.IsNullOrEmpty(txtboxuploadworkingfiles.Text))
            {
                TempComplianceDocument tempComplianceDocument = null;
                var bytes = new byte[] { };// { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };  
                tempComplianceDocument = new TempComplianceDocument()
                {
                    ScheduleOnID = ScheduledOnID,
                    ComplianceInstanceID = complianceInstanceID,
                    DocPath = url,//userpath,
                    DocData = bytes,
                    DocName = fileName,
                    DocType = "W",
                    ISLink = true
                };

                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                if (_objTempDocumentID > 0)
                {
                    BindTempDocumentData(ScheduledOnID);
                }
            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalPer();", true);
            }
            txtboxuploadworkingfiles.Text = "";
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalPer();", true);
        }
    }
}