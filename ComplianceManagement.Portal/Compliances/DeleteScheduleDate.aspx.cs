﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class DeleteScheduleDate : System.Web.UI.Page
    {
        public DateTime? OldscheduleOnDate { get; private set; }
        string scheduleid1 = "";
        string scheduleid = "";
        public static int userID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                userID = Convert.ToInt32(AuthenticationHelper.UserID);
                BindCustomersList(userID);
                tbxFilterLocation.Text = "< Select >";
                tbxFilterLocation.Attributes.Add("readonly", "readonly");
                ddlAct.SelectedValue = "-1";
                ddlCompliance.SelectedValue = "-1";

            }
        }
        private void BindCustomersList(int Userid)
        {
            ddlCustomer.DataTextField = "Name";
            ddlCustomer.DataValueField = "ID";
          //  ddlCustomer.DataSource = CustomerManagement.GetAll(-1);
            ddlCustomer.DataSource = CustomerManagement.GetAllCustomer(Userid);
            ddlCustomer.DataBind();
            ddlCustomer.Items.Insert(0, new ListItem("< Select Customer>", "-1"));
        }
        private void BindCompliances(long actID,long BranchID)
        {
            try
            {
                ddlCompliance.DataTextField = "Name";
                ddlCompliance.DataValueField = "ID";

                ddlCompliance.DataSource = GetAllAssignedCompliances(actID, BranchID);
                ddlCompliance.DataBind();

                ddlCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindActs(long Locbranchid)
        {
            ddlAct.DataTextField = "Name";
            ddlAct.DataValueField = "ID";
            ddlAct.DataSource = GetAllActs(Locbranchid);
            ddlAct.DataBind();
            ddlAct.Items.Insert(0, new ListItem("<Select Act>", "-1"));
        }
        protected void upAct_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<object> GetAllAssignedCompliances(long ActID,long BranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Acts
                            join row1 in entities.Compliances
                            on row.ID equals row1.ActID
                            join row2 in entities.ComplianceInstances
                            on row1.ID equals row2.ComplianceId
                            where row.IsDeleted == false && row1.IsDeleted == false && row1.ActID == ActID
                            && row2.CustomerBranchID== BranchID && row1.EventFlag == null
                            orderby row.Name ascending
                            select new { ID = row1.ID, Name = row1.ShortDescription }).Distinct().OrderBy(entry => entry.Name).ToList<object>();

                return acts;
            }
        }
       
        public static List<object> GetAllActs(long Locbranchid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var acts = (from row in entities.Acts
                            join row1 in entities.Compliances
                            on row.ID equals row1.ActID
                            join row2 in entities.ComplianceInstances
                            on row1.ID equals row2.ComplianceId
                            where row.IsDeleted == false && row2.CustomerBranchID == Locbranchid
                            orderby row.Name ascending
                            select new { ID = row.ID, Name = row.Name }).Distinct().OrderBy(entry => entry.Name).ToList<object>();

                return acts;
            }
        }
        protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        {           
            if (!string.IsNullOrEmpty(ddlAct.SelectedValue) && !string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
            {
                BindCompliances(Convert.ToInt32(ddlAct.SelectedValue), Convert.ToInt32(tvFilterLocation.SelectedValue));
            }                     
        }
        protected void ddlCompliance_SelectedIndexChanges(object sender, EventArgs e)
        {
            grdCompliances.DataSource = null;
            grdCompliances.DataBind();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (!string.IsNullOrEmpty(ddlAct.SelectedValue) && !string.IsNullOrEmpty(ddlCompliance.SelectedValue) && !string.IsNullOrEmpty(ddlCustomer.SelectedValue) && !string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                {               
                    long actid = Convert.ToInt32(ddlAct.SelectedValue);
                    long complianced = Convert.ToInt32(ddlCompliance.SelectedValue);
                    long customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    long Locbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
                                        
                    var ScheduleList = (from row in entities.Acts
                                        join row1 in entities.Compliances
                                        on row.ID equals row1.ActID
                                        join row2 in entities.ComplianceInstances
                                        on row1.ID equals row2.ComplianceId
                                        join row4 in entities.ComplianceScheduleOns
                                        on row2.ID equals row4.ComplianceInstanceID
                                        join row5 in entities.CustomerBranches
                                        on row2.CustomerBranchID equals row5.ID
                                        join row6 in entities.Customers
                                        on row5.CustomerID equals row6.ID
                                        where row1.IsDeleted == false && row1.ActID == actid
                                        && row1.ID == complianced && row5.ID == Locbranchid
                                        && row.ID == actid && row6.ID == customerID
                                        && row4.IsUpcomingNotDeleted == true && row4.IsActive == true                                        
                                        select new
                                        {
                                            CustomerName = row6.Name,
                                            CustomerBranchName = row5.Name,
                                            ActName = row.Name,
                                            row1.ShortDescription,
                                            row4.ScheduleOn
                                        }).Distinct();

                    var compliances = ScheduleList.ToList();
                    grdCompliances.DataSource = compliances;
                    grdCompliances.DataBind();
                }
            }
        }
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {           
            tvFilterLocation.Nodes.Clear();
            BindLocationFilter();
        }
        private void BindLocationFilter()
        {
            try
            {

                long customerID = Convert.ToInt32(ddlCustomer.SelectedValue);             
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(Convert.ToInt32(customerID));                
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {        
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "< Select Location >";
            int Locbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
            BindActs(Locbranchid);

        }
        public static void DeleteComplianceScheduleOn(long scheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceScheduleOn complianceScheduleOn = (from row in entities.ComplianceScheduleOns
                                                             where row.ID == scheduleOnID// && row.IsActive==true &&
                                                             //row.IsUpcomingNotDeleted==true
                                                             select row).FirstOrDefault();

                complianceScheduleOn.IsActive =false;
                complianceScheduleOn.IsUpcomingNotDeleted = false;
                entities.SaveChanges();

            }

        }
        public void CreateLogForScheduleOn(string ScheduleIDList, DateTime? OldscheduleOnDate,DateTime NewscheduleOnDate)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    Log_ComplianceInstance logchangedate = new Log_ComplianceInstance()
                    {
                        Schedule_On_ID = Convert.ToString(ScheduleIDList),
                        Previous_Start_Date = OldscheduleOnDate,
                        New_Instance_Start_Date = NewscheduleOnDate,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                    };

                    entities.Log_ComplianceInstance.Add(logchangedate);
                    entities.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btndelete_Click(object sender, EventArgs e)
        {
            try
            {
                long actid = -1;
                long complianceid = -1;
                long Locbranchid = -1;
                if (!string.IsNullOrEmpty(ddlAct.SelectedValue))
                {
                    actid = Convert.ToInt32(ddlAct.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlCompliance.SelectedValue))
                {
                    complianceid = Convert.ToInt32(ddlCompliance.SelectedValue);
                }
                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                {
                    Locbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                if (actid != -1 && complianceid != -1 && Locbranchid != -1)
                {

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        DateTime NewscheduleOnDate = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        var complianceInstances = (from row in entities.ComplianceInstances
                                                   where row.IsDeleted == false && row.ComplianceId == complianceid && row.CustomerBranchID == Locbranchid
                                                   select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();

                        DateTime? OldStartDate = null;
                        complianceInstances.ForEach(complianceInstancesentry =>
                        {
                            var complianceScheduleOns = (from row in entities.ComplianceScheduleOns
                                                         where row.ComplianceInstanceID == complianceInstancesentry.ID &&
                                                         row.ScheduleOn <= NewscheduleOnDate &&
                                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                         select row).ToList();

                            if (complianceScheduleOns.Count > 0)
                            {
                                complianceScheduleOns.ForEach(complianceScheduleOnssentry =>
                                {

                                    DeleteComplianceScheduleOn(complianceScheduleOnssentry.ID);
                                    scheduleid1 += complianceScheduleOnssentry.ID.ToString() + " ,";
                                });
                                if (complianceScheduleOns.Count <= 0)
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Record not found.";
                                }
                                else
                                {
                                    scheduleid1 = scheduleid1.Trim(',');
                                    scheduleid = scheduleid1;
                                    OldStartDate = complianceInstancesentry.ScheduledOn;
                                    CreateLogForScheduleOn(scheduleid, OldStartDate, NewscheduleOnDate);
                                    ChangeDateScheduleOn(complianceInstancesentry.ID, NewscheduleOnDate);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Record Updated Sucessfully.";
                                }
                            }
                            else
                            {
                                OldStartDate = complianceInstancesentry.ScheduledOn;
                                CreateLogForScheduleOn("0", OldStartDate, NewscheduleOnDate);
                                ChangeDateScheduleOn(complianceInstancesentry.ID, NewscheduleOnDate);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record Updated Sucessfully.";
                            }
                        });

                    }
                    grdCompliances.DataSource = null;
                    tbxStartDate.Text = "";
                    grdCompliances.DataBind();
                    ddlCustomer.SelectedValue = "-1";
                    ddlAct.SelectedValue = "-1";
                    ddlCompliance.SelectedValue = "-1";
                    tbxFilterLocation.Text = "< Select >";
                }
            }           
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
      
        protected void btnKeep_Click(object sender, EventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    long actid = -1;
                    long complianceid = -1;
                    long Locbranchid = -1;
                    if (!string.IsNullOrEmpty(ddlAct.SelectedValue))
                    {
                        actid = Convert.ToInt32(ddlAct.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(ddlCompliance.SelectedValue))
                    {
                        complianceid = Convert.ToInt32(ddlCompliance.SelectedValue);
                    }
                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                    {
                        Locbranchid = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }                  
                    if (actid != -1 && complianceid != -1 && Locbranchid != -1)
                    {                    
                        DateTime NewscheduleOnDate = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        var complianceInstances = (from row in entities.ComplianceInstances
                                                   where row.IsDeleted == false && row.ComplianceId == complianceid && row.CustomerBranchID == Locbranchid
                                                   select row).GroupBy(entity => entity.ID).Select(entity => entity.FirstOrDefault()).ToList();
                        DateTime? OldStartDate = null;
                        
                        complianceInstances.ForEach(complianceInstancesentry =>
                        {
                            var complianceScheduleOns = (from row in entities.ComplianceScheduleOns
                                                         where row.ComplianceInstanceID == complianceInstancesentry.ID &&
                                                         row.ScheduleOn <= NewscheduleOnDate &&
                                                         row.IsActive == true && row.IsUpcomingNotDeleted == true
                                                         select row).ToList();

                            if (complianceScheduleOns.Count > 0)
                            {
                                complianceScheduleOns.ForEach(complianceScheduleOnssentry =>
                                {
                                    var complianceTransactions = (from row in entities.ComplianceTransactions
                                                                  where row.ComplianceInstanceId == complianceScheduleOnssentry.ComplianceInstanceID &&
                                                                  row.ComplianceScheduleOnID == complianceScheduleOnssentry.ID && row.StatusId != 1
                                                                  select row).ToList();

                                    if (complianceTransactions.Count == 0)
                                    {
                                        DeleteComplianceScheduleOn(complianceScheduleOnssentry.ID);
                                        scheduleid1 += complianceScheduleOnssentry.ID.ToString() + " ,";
                                    }
                                });


                                if (complianceScheduleOns.Count <= 0)
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Record not found.";
                                }
                                else
                                {
                                    scheduleid1 = scheduleid1.Trim(',');
                                    scheduleid = scheduleid1;
                                    OldStartDate = complianceInstancesentry.ScheduledOn;
                                    CreateLogForScheduleOn(scheduleid, OldStartDate, NewscheduleOnDate);
                                    ChangeDateScheduleOn(complianceInstancesentry.ID, NewscheduleOnDate);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Record Updated Sucessfully.";
                                }
                            }
                            else
                            {
                                OldStartDate = complianceInstancesentry.ScheduledOn;
                                ChangeDateScheduleOn(complianceInstancesentry.ID, NewscheduleOnDate);
                                CreateLogForScheduleOn("0", OldStartDate, NewscheduleOnDate);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record Updated Sucessfully.";
                            }
                        });
                    }
                    grdCompliances.DataSource = null;
                    tbxStartDate.Text = "";
                    grdCompliances.DataBind();
                    ddlCustomer.SelectedValue = "-1";
                    ddlAct.SelectedValue = "-1";
                    ddlCompliance.SelectedValue = "-1";
                    tbxFilterLocation.Text = "< Select >";
                }
            }
            catch(Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }


        }

        public void ChangeDateScheduleOn(long complianceInstanceID,DateTime NewscheduleOnDate)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceInstance = (from row in entities.ComplianceInstances
                                          where row.ID == complianceInstanceID
                                          select row).FirstOrDefault();

                ComplianceInstance.ScheduledOn = NewscheduleOnDate;
                entities.SaveChanges();
            }
        }      
    }
}