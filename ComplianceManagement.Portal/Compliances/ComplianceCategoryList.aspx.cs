﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Linq;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ComplianceCategoryList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindComplianceCategories();
                if ((AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("RREV")))
                {
                    btnAddComplianceCategory.Visible = true;
                }
            }
        }

        private void BindComplianceCategories()
        {
            try
            {
                grdComplianceCategory.DataSource = ComplianceCategoryManagement.GetAll(tbxFilter.Text);
                grdComplianceCategory.DataBind();
                upComplianceCategoryList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceCategory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try{
            int categoryID = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("EDIT_COMPLIANCE_CATEGORY"))
            {
                ViewState["Mode"] = 1;
                ViewState["ComplianceCategoryID"] = categoryID;

                ComplianceCategory userParameter = ComplianceCategoryManagement.GetByID(categoryID);

                tbxName.Text = userParameter.Name;
                tbxDescription.Text = userParameter.Description;
                tbxDescription.ToolTip = userParameter.Description;

                upComplianceCategory.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divComplianceCategoryDialog\").dialog('open')", true);
            }
            else if (e.CommandName.Equals("DELETE_COMPLIANCE_CATEGORY"))
            {
                if (ComplianceCategoryManagement.CanDelete(categoryID))
                {
                    ComplianceCategoryManagement.Delete(categoryID);
                    BindComplianceCategories();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "alert('Category is associated with one or more Acts, can not be deleted')", true);
                }
            }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void grdComplianceCategory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try{
            grdComplianceCategory.PageIndex = e.NewPageIndex;
            BindComplianceCategories();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void btnAddComplianceCategory_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;

                tbxName.Text = string.Empty;
                tbxDescription.Text = string.Empty;

                upComplianceCategory.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divComplianceCategoryDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdComplianceCategory.PageIndex = 0;
                BindComplianceCategories();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ComplianceCategory category = new ComplianceCategory()
                {
                    Name = tbxName.Text,
                    Description = tbxDescription.Text
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    category.ID = Convert.ToInt32(ViewState["ComplianceCategoryID"]);
                }

                if (ComplianceCategoryManagement.Exists(category))
                {
                    cvDuplicateEntry.ErrorMessage = "Category already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    ComplianceCategoryManagement.Create(category);
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    ComplianceCategoryManagement.Update(category);
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceCategoryDialog\").dialog('close')", true);
                BindComplianceCategories();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceCategory_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var complianceCategoryList = ComplianceCategoryManagement.GetAll(tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                {
                    complianceCategoryList = complianceCategoryList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceCategoryList = complianceCategoryList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdComplianceCategory.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdComplianceCategory.Columns.IndexOf(field);
                    }
                }

                grdComplianceCategory.DataSource = complianceCategoryList;
                grdComplianceCategory.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdComplianceCategory_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdComplianceCategory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.Footer || e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (!AuthenticationHelper.Role.Equals("SADMN"))
                    {
                        e.Row.Cells[2].Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


    }
}