﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PerCompliancestatusTransaction.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.PerCompliancestatusTransaction" %>

<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <!-- Bootstrap CSS -->
    <link href='../NewCSS/bootstrap.min.css' rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href='../NewCSS/bootstrap-theme.css' rel="stylesheet" type="text/css" />
    <link href='../NewCSS/responsive-calendar.css' rel="stylesheet" type="text/css" />
    <link href='../NewCSS/font-awesome.min.css' rel="stylesheet" type="text/css" />
    <!-- full calendar css-->
    <link href='../assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css' rel="stylesheet" type="text/css" />
    <!-- owl carousel -->
    <link rel="stylesheet" href='../../NewCSS/owl.carousel.css' type="text/css" />
    <!-- Custom styles -->
    <link rel="stylesheet" href='../NewCSS/fullcalendar.css' type="text/css" />
    <link href='../NewCSS/stylenew.css' rel="stylesheet" type="text/css" />
    <link href='../NewCSS/jquery-ui-1.10.4.min.css' rel="stylesheet" type="text/css" />
    <link href='../Newjs/bxslider/jquery.bxslider.css' rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="../Newjs/moment.min.js"></script>


    <link href="https://avacdn.azureedge.net/newcss/kendo.common1.2.min.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/kendo.rtl.min.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/kendo.silver.min.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jszip.min.js"></script>
    <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/kendo.all.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {

            kendogridupdate();

        })

        function kendogridupdate() {
            
            var gridUpdate = $("#gridUpdate").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =KendoPath%>/Data/KendoComplianceOverviewUpdates?UserId=<% =UId%>&CustId=<% =CustId%>&complianceInstanceID=<% =compInstanceID%>',
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                    }
                    //read: '<% =KendoPath%>/Data/KendoComplianceOverviewUpdates?UserId=<% =UId%>&CustId=<% =CustId%>&complianceInstanceID=<% =compInstanceID%>'
                },
                pageSize: 5,
            },
            //height: 150,
            sortable: true,
            filterable: true,
            columnMenu: false,
            pageable: true,
            reorderable: true,
            resizable: true,
            multi: true,

            noRecords: {
                template: "No records available"
            },
            columns: [
                {
                    field: "Title", title: 'Title',
                    width: "65%;",
                    attributes: {
                        style: 'white-space: nowrap;'
                    }, filterable: { multi: true, search: true }
                },
                //{
                //    field: "Date", title: 'Date',
                //    type: "date",
                //    template: "#= kendo.toString(kendo.parseDate(Date, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                //    attributes: {
                //        style: 'white-space: nowrap;'
                //        //}, filterable: { multi: true, search: true }
                //    }, filterable: {
                //        extra: false,
                //        operators: {
                //            string: {
                //                eq: "Is equal to",
                //                neq: "Is not equal to",
                //                contains: "Contains"
                //            }
                //        }
                //    },
                //},
                 {
                     field: "Date", title: 'Date',
                     type: "date",
                     format: "{0:dd-MMM-yyyy}",
                     template: "#= kendo.toString(kendo.parseDate(Date, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                     attributes: {
                         style: 'white-space: nowrap;'
                         //}, filterable: { multi: true, search: true }
                     },
                     filterable: {
                         multi: true,
                         extra: false,
                         search: true,
                         operators: {
                             string: {
                                 type: "date",
                                 format: "{0:dd-MMM-yyyy}",
                                 eq: "Is equal to",
                                 neq: "Is not equal to",
                                 contains: "Contains"
                             }
                         },
                     }
                 },
                {
                    command: [
                        { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                    ], title: "Action", lock: true,// width: 150,
                }
            ]
        });

        $("#gridUpdate").kendoTooltip({
            filter: ".k-grid-edit2",
            content: function (e) {
                return "Overview";
            }
        });

        $("#gridUpdate").kendoTooltip({
            filter: "td:nth-child(1)", //this filter selects the second column's cells
            position: "down",
            content: function (e) {
                var content = e.target.context.textContent;
                return content;
            }
        }).data("kendoTooltip");

        $("#gridUpdate").kendoTooltip({
            filter: "td:nth-child(2)", //this filter selects the second column's cells
            position: "down",
            content: function (e) {
                var content = e.target.context.textContent;
                return content;
            }
        }).data("kendoTooltip");
    }

    $(document).on("click", "#gridUpdate tbody tr .ob-overview", function (e) {

        var item = $("#gridUpdate").data("kendoGrid").dataItem($(this).closest("tr"));
        //OpendocumentsUpdates(item.Title);
        OpendocumentsUpdates(item.Description);

        return true;
    });


    function OpendocumentsUpdates(title) {

        document.getElementById('detailUpdate').innerHTML = title;// 'your tip has been submitted!';


        var myWindowAdv = $("#ViewUpdateDetails");

        function onClose() {

        }

        myWindowAdv.kendoWindow({
            width: "85%",
            height: "50%",
            title: "Legal Updates",
            visible: false,
            actions: [
                //"Pin",
                //"Minimize",
                //"Maximize",
                "Close"
            ],

            close: onClose
        });

        //$("#divAdvanceSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

        myWindowAdv.data("kendoWindow").center().open();
        //e.preventDefault();
        return false;
    }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

    </script>

    <script type="text/javascript">
        function openInNewTab(url) {
            var win = window.open(url, '_blank');
            win.focus();
        }
        function SelectheaderCheckboxes(headerchk) {
            var count = 0;
            var gvcheck = document.getElementById("gridSubTask");
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            var gv = document.getElementById("gridSubTask");
            var inputList = gv.getElementsByTagName("input");

            for (var i = 0; i < inputList.length; i++) {
                if (inputList[i].type == "checkbox" && inputList[i].checked) {
                    count = count + 1;
                    $('#ConfirmationModel').modal('show');

                }
            }
        }

        function callOnButtonYes1() {

            var count = 0;
            var gvcheck = document.getElementById("gridSubTask");
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            var gv = document.getElementById("gridSubTask");
            var inputList = gv.getElementsByTagName("input");
            if (inputList != undefined) {
                for (var i = 0; i < inputList.length; i++) {
                    if (inputList[i].type == "checkbox" && inputList[i].checked) {
                        inputList[i].checked = false;
                    }
                }
                $('#ConfirmationModel').modal('hide');
            }
            initializeDatePicker();
        }
        function callOnButtonNo1() {

            var count = 0;
            var gvcheck = document.getElementById("gridSubTask");
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            var gv = document.getElementById("gridSubTask");
            var inputList = gv.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                if (inputList[i].type == "checkbox" && inputList[i].checked) {
                    count = count + 1;
                }
            }
            $('#ConfirmationModel').modal('hide');
            initializeDatePicker();
        }
        $('.btn-minimize').click(function () {
            var s1 = $(this).find('i');
            if ($(this).hasClass('collapsed')) {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            } else {
                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            }
        });

        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {

            var enddates = new Date($("#lblEndDate").text());
            if (enddates != null) {
                var startDate = new Date();
                $(function () {
                    $('input[id*=txtStartDate]').datepicker({
                        dateFormat: 'dd-mm-yy',
                        numberOfMonths: 1,
                        minDate: enddates,
                        changeMonth: true,
                        changeYear: true,
                    });
                    $('input[id*=txtEndDate]').datepicker({
                        dateFormat: 'dd-mm-yy',
                        numberOfMonths: 1,
                        minDate: enddates,
                        changeMonth: true,
                        changeYear: true,
                    });
                    $('input[id*=tbxDate]').datepicker({
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        //changeMonth: true,
                        //changeYear: true,
                    });
                });

            }
            else {
                var startDate = new Date();
                $(function () {
                    $('input[id*=txtStartDate]').datepicker({
                        dateFormat: 'dd-mm-yy',
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                    });
                    $('input[id*=txtEndDate]').datepicker({
                        dateFormat: 'dd-mm-yy',
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                    });

                    $('input[id*=tbxDate]').datepicker({
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        //changeMonth: true,
                        //changeYear: true,
                    });

                });
            }
        }

        function fopenDocumentPriview(file) {
            $('#DocumentPriview').modal('show');
            $('#docPriview').attr('src', "../docviewer.aspx?docurl=" + file);
        }
        function fopendocfileReview(file) {
            $('#modalDocumentPerformerViewer').modal('show');
            $('#docViewerPerformerAll').attr('src', "../docviewer.aspx?docurl=" + file);
        }

        $(document).ready(function () {
            $("button[data-dismiss-modal=modal2]").click(function () {
                $('#modalDocumentPerformerViewer').modal('hide');
                $('#DocumentShowPopUp').modal('hide');
            });
        });
        function initializeComboboxUpcoming() {
            $("#ddlStatus").combobox();
        }
        function fopendoctaskfileReview(file) {
            $('#modalDocumentPerformerViewer').modal('show');
            $('#docViewerPerformerAll').attr('src', "../docviewer.aspx?docurl=" + file);
        }
        function fopendoctaskfileReviewPopUp() {
            $('#modalDocumentPerformerViewer').modal('show');
        }
        $(document).ready(function () {
            $("button[data-dismiss-modal=modal2]").click(function () {
                $('#modalDocumentPerformerViewer').modal('hide');
            });
        });
        $(document).ready(function () {
            $("button[data-dismiss-modal=modal2]").click(function () {
                $('#modalDocumentPerformerViewer').modal('hide');
            });
        });
        function fopendocfile() {
            $('#DocumentPopUp').modal('show');
            $('#docViewerAll').attr('src', "../docviewer.aspx?docurl=" + $("#lblpathsample").text());
        }
        function fopendocfileLicenseAllShowPopUp() {
            $('#DocumentShowPopUp').modal('show');
        }
        function fopendocfilelicense(file) {
            $('#DocumentShowPopUp').modal('show');
            $('#docViewerLicenseAll').attr('src', "../docviewer.aspx?docurl=" + file);
        }

        $(document).ready(function () {
           
            //$('#txtEndDate').attr('readonly', true);
            $('#txtEndDate').addClass('input-disabled')
            $('#txtStartDate').addClass('input-disabled')
        });


        var validFilesTypes = ["exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp"];
        function ValidateFilestatus() {

            var label = document.getElementById("Label1");
            var fuSampleFile = $("#fuSampleFile").get(0).files;
            var FileUpload1 = $("#FileUpload1").get(0).files;
            var isValidFile = true;

            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (validFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
            }


            for (var i = 0; i < FileUpload1.length; i++) {ValidationSummary1
                var fileExtension = FileUpload1[i].name.split('.').pop();
                if (validFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
            }

            if (!isValidFile) {
                $('#').IsValid = false;
                document.getElementById("ValidationSummary1").value = "Invalid file error. System does not support uploaded file.Please upload another file.";


            }
            return isValidFile;
        }

        function fillValuesInTextBoxes() {
            var chk = $("#chkPenaltySave").is(":checked");
            if (chk == true) {
                document.getElementById("txtInterest").value = "0";
                document.getElementById("txtPenalty").value = "0";
                document.getElementById("txtInterest").readOnly = true;
                document.getElementById("txtPenalty").readOnly = true;
            }
            else {
                document.getElementById("txtInterest").value = "";
                document.getElementById("txtPenalty").value = "";
                document.getElementById("txtInterest").readOnly = false;
                document.getElementById("txtPenalty").readOnly = false;
            }
        }



        function PenaltyEventValidate() {

            var tbxDate = $("#tbxDate").val();
            if (tbxDate != "") {
                var appdate = new Date($("#lblApplicationDate").text());
                var adate = new Date($.datepicker.parseDate("dd-mm-yy", $("#tbxDate").val()));
                var monnonmon = $("#mont").val();
                var lstatus = -1;
                var a = compare(appdate, adate);
                if (a == 1) {
                    lstatus = 2;
                } else if (a == -1) {
                    lstatus = 3;
                } else if (a == 0) {
                    lstatus = 2;
                }
                var ddlstatus = $('select#ddlStatus option:selected').val();
                if (ddlstatus == -1) {
                    $("#Labelmsg").css('display', 'block');
                    $('#Labelmsg').text("Please select status");
                    $('#ValidationSummary1').IsValid = false;
                    document.getElementById("ValidationSummary1").value = "Please select status";
                    return false;
                }
                //if (monnonmon != "") {
                //    if (monnonmon != 1) {
                //        if (lstatus == 3 && a == -1) {
                //            var chk = $("#chkPenaltySave").is(":checked");
                //            if (chk == false) {
                //                var txtInterest = $("#txtInterest").val();
                //                var txtPenalty = $("#txtPenalty").val();
                //                if (txtInterest == "" || txtPenalty == "" || txtInterest == "" || txtPenalty == "0") {
                //                    $("#Labelmsg").css('display', 'block');
                //                    $('#Labelmsg').text("Please enter interest and penalty");
                //                    $('#ValidationSummary1').IsValid = false;
                //                    document.getElementById("ValidationSummary1").value = "Please enter interest and penalty";
                //                    return false;
                //                }
                //            }
                //        }
                //    }
                //}
            }
            else {
                $("#Labelmsg").css('display', 'block');
                $('#Labelmsg').text("Please select date");
                $('#ValidationSummary1').IsValid = false;
                document.getElementById("ValidationSummary1").value = "Please select date";
                return false;
            }
        }
        function DiffPerformer() {
            document.getElementById('txtDiffAB').value = "";
            var ValueAsPerSystem = document.getElementById('txtValueAsPerSystem').value;
            var ValueAsPerReturn = document.getElementById('txtValueAsPerReturn').value;

            var AB = parseInt(ValueAsPerSystem) - parseInt(ValueAsPerReturn);

            if (!isNaN(AB)) {
                document.getElementById('txtDiffAB').value = AB;
            }
            else {
                document.getElementById('txtDiffAB').value = "";
            }

            document.getElementById('txtDiffBC').value = ""
            var LiabilityPaid = document.getElementById('txtLiabilityPaid').value;
            var BC = parseInt(ValueAsPerReturn) - parseInt(LiabilityPaid);
            if (!isNaN(BC)) {
                document.getElementById('txtDiffBC').value = BC;
            }
            else {
                document.getElementById('txtDiffBC').value = "";
            }
        }
        function compare(dateTimeA, dateTimeB) {
            var momentA = moment(dateTimeA, "DD/MM/YYYY");
            var momentB = moment(dateTimeB, "DD/MM/YYYY");
            if (momentA > momentB) return 1;
            else if (momentA < momentB) return -1;
            else return 0;
        }
        function PenaltyVisibleValidate() {
            debugger;
            var tbxDate = $("#tbxDate").val();
            if (tbxDate != "") {
                var appdate = new Date($("#lblApplicationDate").text());
                var adate = new Date($.datepicker.parseDate("dd-mm-yy", $("#tbxDate").val()));
                var monnonmon = $("#mont").val();
                if (monnonmon != "") {
                    if (monnonmon != 1) {
                        var a = compare(appdate, adate);
                        if (a == 1) {
                            $("#fieldsetpenalty").css("display", "none");

                        } else if (a == -1) {
                            $("#fieldsetpenalty").css("display", "block");
                        } else if (a == 0) {
                            $("#fieldsetpenalty").css("display", "none");
                        }
                    }
                }
            }
            else {
                $("#Labelmsg").css('display', 'block');
                $('#Labelmsg').text("Please select date");
                $('#ValidationSummary1').IsValid = false;
                document.getElementById("ValidationSummary1").value = "Please select date";
                return false;
            }
        }
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);

            BindControls();
            //alert(1);
            $('#tbxDate').change(function () {
                debugger;
               // PenaltyVisibleValidate()
            });

        });
        function initchag() {
            $('#tbxDate').change(function () {
              //  PenaltyVisibleValidate()
            });
        }
       
    </script>
    <script type="text/javascript">
        $(document).on("click", "#ContentPlaceHolder1_UpdatePanel1", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            }
            else if (event.target.id != "ContentPlaceHolder1_tbxFilterLocation") {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == "ContentPlaceHolder1_tbxFilterLocation") {
                $("#ContentPlaceHolder1_tbxFilterLocation").unbind('click');
                $("#ContentPlaceHolder1_tbxFilterLocation").click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });

            }
        });


        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function hidediv() {
            var div = document.getElementById('AdvanceSearch');
            div.style.display == "none" ? "block" : "none";
            $('.modal-backdrop').hide();
            return true;
        }
        function openModal() {
            $('#ComplaincePerformer').modal('show');
            return true;
        }
        function openModalInternalPer() {
            $('#ComplainceInternalPerformaer').modal('show');
            return true;
        }
        function openModalReviewer() {
            $('#ComplainceReviewer').modal('show');
            return true;
        }
        function openModalInternalReviewer() {
            $('#ComplainceInternalReviewer').modal('show');
            return true;
        }
        function openModalEventBased() {
            $('#ComplainceEventBased').modal('show');
            return true;
        }
        function openModalEventBasedReviewer() {
            $('#ComplainceEventBasedReviewer').modal('show');
            return true;
        }
    </script>

    <style type="text/css">
        .clspenaltysave {
            font-weight: bold;
            margin-left: 15px;
        }

        .input-disabled {
            background-color: #f7f7f7;
            border: 1px solid #c7c7cc;
            padding: 6px 12px;
        }

        .btnss {
            background-image: url(../Images/edit_icon_new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > thead > tr > th > a {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .Inforamative {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }

        tr.Inforamative > td {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }

        .circle {
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }
    </style>

    <style type="text/css">
        /*.k-window div.k-window-content {
                overflow: hidden;
            }*/

        div.k-widget.k-window {
            top: 643px !important;
        }

        .k-combobox-clearable .k-input, .k-dropdowntree-clearable .k-dropdown-wrap .k-input, .k-dropdowntree-clearable .k-multiselect-wrap, .k-multiselect-clearable .k-multiselect-wrap {
            padding-right: 2em;
            text-align: center;
            display: grid;
        }

        .k-dropdown, .k-textbox {
            text-align: center;
        }
        /*.div.k-grid-footer, div.k-grid-header {
            border-top-style: solid;
            border-top-width: 1px;
        }*/
        .k-grid-content {
            min-height: 150px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-grid-toolbar {
            background: white;
        }

        #grid .k-grid-toolbar {
            padding: .6em 1.3em .6em .4em;
        }

        .k-grid td {
            line-height: 1em;
            border-bottom-width: 1px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
            min-height: 30px;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 1px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #212121;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 1px 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
            line-height: 14px;
        }

        .k-grid tbody tr {
            height: 38px;
        }

        .k-textbox .k-icon {
            top: 50%;
            margin: -8px 0 0;
            margin-left: 56px;
            position: absolute;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }


        .k-animation-container {
            width: 400px !important;
            white-space: pre-wrap;
            height: 255px !important;
            /*left: 735px !important;*/
        }

        k-filter-menu k-popup k-group k-reset k-state-border-up {
            height: 255px !important;
        }
    </style>

    <script type="text/javascript">
        function setTabActive(id) {
            $('.dummyval').removeClass("active");
            $('#' + id).addClass("active");
        };
    </script>

    <style>
        tr.spaceUnder > td {
            padding-bottom: 1em;
        }
    </style>
</head>
<body>
    <form runat="server">

        <div id="divComplianceDetailsDialog">
            <asp:ScriptManager ID="Isdf" runat="server" ScriptMode="Release"></asp:ScriptManager>
            <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional"
                OnLoad="upComplianceDetails_Load">
                <ContentTemplate>
                    <div style="margin: 5px">
                        <div style="margin-bottom: 2px">
                            <asp:ValidationSummary ID="ValidationSummary1" Style="padding-left: 5%" runat="server" Display="none"
                                class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceValidationGroup1" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" class="alert alert-block alert-danger fade in"
                                EnableClientScript="true" ValidationGroup="ComplianceValidationGroup1" Style="display: none; padding-left: 40px" />
                            <asp:Label ID="Label1" class="alert alert-block alert-danger fade in" Visible="false" runat="server"></asp:Label>
                            <asp:Label ID="Labelmsg" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
                            <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />
                        </div>
                        <div class="clearfix" style="margin-bottom: 5px"></div>
                        <div>
                            <asp:Label ID="Label2" Text="This is a  " Style="width: 300px; font-weight: bold; font-size: 13px; color: #333;"
                                maximunsize="300px" autosize="true" runat="server" />
                            <div id="divRiskType" runat="server" class="circle"></div>
                            <asp:Label ID="lblRiskType" Style="width: 300px; margin-left: -17px; font-weight: bold; font-size: 13px; color: #333;"
                                maximunsize="300px" autosize="true" runat="server" />
                        </div>

                        <div id="ActDetails" class="row Dashboard-white-widget" style="margin-bottom: 10px;">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails">
                                                    <h2>Compliance Details</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="collapseActDetails" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                    <table style="width: 100%;">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Act Name</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblActName" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Section /Rule</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblRule" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Compliance Id</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblComplianceID" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Category</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblCategory" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Short Description</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblComplianceDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Detailed Description</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblDetailedDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Penalty</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblPenalty" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Frequency</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblFrequency" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Risk Type</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblRisk" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Sample Form/Attachment</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:UpdatePanel ID="upsample" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Label ID="lblFormNumber" Style="width: 150px; font-size: 13px; color: #333;"
                                                                            maximunsize="300px" autosize="true" runat="server" />
                                                                        <asp:LinkButton ID="lbDownloadSample" Style="width: 150px; font-size: 13px; color: blue"
                                                                            runat="server" Font-Underline="false" OnClick="lbDownloadSample_Click" />
                                                                        <asp:Label ID="lblSlash" Text="/" Style="color: blue;" runat="server" />
                                                                        <asp:LinkButton ID="lnkViewSampleForm" Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                            runat="server" Font-Underline="false" OnClientClick="fopendocfile();" />
                                                                        <asp:Label ID="lblpathsample" runat="server" Style="display: none"></asp:Label>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Regulatory website link</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:LinkButton ID="lnkSampleForm" Text="" Style="width: 300px; font-size: 13px; color: blue"
                                                                    runat="server" Font-Underline="false" OnClick="lnkSampleForm_Click" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Additional/Reference Text </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblRefrenceText" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Location</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblLocation" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder" style="display: none;">
                                                            <td style="width: 25%; font-weight: bold;">Period</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblPeriod" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder" style="display: none;">
                                                            <td style="width: 25%; font-weight: bold;">Due Date</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblDueDate" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="ComplianceDetails" class="row Dashboard-white-widget" style="margin-bottom: 10px;">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails">
                                                    <h2>License Details</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="collapseComplianceDetails" class="collapse">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                    <table style="width: 100%">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">License Type</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblLicenseType" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">License Number</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblLicenseNumber" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">License Title</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblLicenseTitle" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">Application Due Date</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblApplicationDate" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">Start Date</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblStartdate" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold; vertical-align: top;">End Date</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblEndDate" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td style="width: 25%; font-weight: bold;">Versions</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <table width="100%" style="text-align: left">
                                                                    <thead>
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <asp:Repeater ID="rptLicenseVersion" runat="server"
                                                                                    OnItemCommand="rptLicenseVersion_ItemCommand"
                                                                                    OnItemDataBound="rptLicenseVersion_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <table id="tblLicenseDocumnets">
                                                                                            <thead>
                                                                                                <%-- <th>Versions</th>--%>
                                                                                            </thead>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                                            <ContentTemplate>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                            ID="lblLicenseDocumentVersion" runat="server" Text='<%# Eval("Version")%>' Style="color: blue;"></asp:LinkButton></td>
                                                                                                    <td>
                                                                                                        <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                            ID="btnLicenseVersionDoc" runat="server" Text="Download" Style="color: blue;">
                                                                                                        </asp:LinkButton>
                                                                                                        <asp:Label ID="lblSlashReviewnew" Text="/" Style="color: blue;" runat="server" />
                                                                                                        <asp:LinkButton CommandName="View" ID="lnkViewDoc" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                            Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                                                            runat="server" Font-Underline="false" />
                                                                                                        <asp:Label ID="lblpathReviewDoc" runat="server" Style="display: none"></asp:Label>
                                                                                                        <asp:Label ID="lblpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>
                                                                                                        <asp:LinkButton ID="lblpathDownload" CommandName="version" CommandArgument='<%# Eval("LicenseID")+","+ Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                            OnClientClick='javascript:enableControls1()' Text='Click Here' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                            runat="server" Font-Underline="false" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="btnLicenseVersionDoc" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>

                                                                            </td>
                                                                            <td valign="top">
                                                                                <asp:Repeater ID="rptLicenseDocumnets" runat="server" OnItemCommand="rptLicenseDocumnets_ItemCommand"
                                                                                    OnItemDataBound="rptLicenseDocumnets_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <table id="tblLicenseDocumnets">
                                                                                            <thead>
                                                                                                <th>Compliance Related Documents</th>
                                                                                            </thead>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:LinkButton
                                                                                                    CommandArgument='<%# Eval("FileID")%>'
                                                                                                    ID="btnLicenseDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                                </asp:LinkButton>
                                                                                                <asp:Label ID="lblCompDocpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>
                                                                                                <asp:LinkButton CommandName="RedirectURL" ID="lblCompDocpathDownload" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                    OnClientClick=<%# "openInNewTab('" + Eval("FilePath") + "')" %>
                                                                                                    Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                    runat="server" Font-Underline="false" />

                                                                                            </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>
                                                                                <asp:Repeater ID="rptLicenseWorkingFiles" runat="server" OnItemCommand="rptLicenseWorkingFiles_ItemCommand"
                                                                                    OnItemDataBound="rptLicenseWorkingFiles_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <table id="tblLicenseWorkingFiles">
                                                                                            <thead>
                                                                                                <th>Compliance Working Files</th>
                                                                                            </thead>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:LinkButton
                                                                                                    CommandArgument='<%# Eval("FileID")%>'
                                                                                                    ID="btnLicenseWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                                                                </asp:LinkButton>
                                                                                                <asp:Label ID="lblWorkCompDocpathIlink" Text='<%# Eval("ISLink")%>' runat="server" Style="display: none"></asp:Label>
                                                                                                <asp:LinkButton CommandName="RedirectURL" ID="lblWorkCompDocpathDownload" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>'
                                                                                                    OnClientClick=<%# "openInNewTab('" + Eval("FilePath") + "')" %>
                                                                                                    Text='<%# Eval("FileName")%>' Style="width: 150px; font-size: 13px; color: blue"
                                                                                                    runat="server" Font-Underline="false" />

                                                                                            </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>
                                                                            </td>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div runat="server" id="divTask" class="row Dashboard-white-widget" style="margin-bottom: 10px;">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                                <h2>Main Task Details</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>

                                        <div id="collapseTaskSubTask" class="collapse in">
                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                <asp:GridView runat="server" ID="gridSubTask" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                    AllowPaging="false" PageSize="50" CssClass="table" GridLines="None" BorderWidth="0px" DataKeyNames="TaskID"
                                                    OnRowCommand="gridSubTask_RowCommand" OnRowDataBound="gridSubTask_RowDataBound" AutoPostBack="true">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                                <asp:Label ID="lblTaskScheduledOnID" runat="server" Visible="false" Text='<%# Eval("TaskScheduledOnID") %>'></asp:Label>
                                                                <asp:Label ID="lblIsTaskClose" Visible="false" runat="server" Text='<%# Eval("IsTaskClose") %>'></asp:Label>
                                                                <asp:Label ID="lblTaskInstanceID" runat="server" Visible="false" Text='<%# Eval("TaskInstanceID") %>'></asp:Label>
                                                                <asp:Label ID="lblMainTaskID" runat="server" Visible="false" Text='<%# Eval("MainTaskID") %>'></asp:Label>
                                                                <asp:Label ID="lblForMonth" runat="server" Visible="false" Text='<%# Eval("ForMonth") %>'></asp:Label>
                                                                <asp:Label ID="lblComplianceScheduleOnID" runat="server" Visible="false" Text='<%# Eval("ComplianceScheduleOnID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Task">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblTaskTitle" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Performer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblPerformer" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'
                                                                        Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'></asp:Label>

                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Reviewer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'
                                                                        Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Due Date">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                    <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="upSubTaskDownloadView" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="btnSubTaskDocDownload" runat="server" CommandName="Download" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Download Documents" Text="Download" Style="color: blue;">
                                                                        </asp:LinkButton>
                                                                        <asp:Label ID="lblSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                        <asp:LinkButton CommandName="View" runat="server" ID="btnSubTaskDocView" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="View Documents"
                                                                            Text="View" Style="width: 150px; font-size: 13px; color: blue" />
                                                                        <asp:Label ID="CompDocReviewPath" runat="server" Style="display: none"></asp:Label>
                                                                        <asp:CheckBox ID="chkTask" CssClass="sbtask" Style="display: none;" data-attr='<%# Eval("IsYesNo") %>' Width="30px" data-toggle="tooltip" AutoPostBack="true" ToolTip="Click to close if not applicable" runat="server" onclick="javascript:SelectheaderCheckboxes(this)" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnSubTaskDocDownload" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="UpdateComplianceStatus" class="row Dashboard-white-widget" style="margin-bottom: 10px;">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateComplianceStatus">
                                                    <h2>Update License Status</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateComplianceStatus"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseUpdateComplianceStatus" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div style="margin-bottom: 0px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Status</label>
                                                            </td>

                                                            <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:DropDownList runat="server" ID="ddlStatus" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"  class="form-control m-bot15" Style="width: 280px;" AutoPostBack="true" />
                                                                <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select Status." ControlToValidate="ddlStatus"
                                                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup1"
                                                                    Display="None" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div id="fieldsetRenewal" runat="server" visible="false" style="margin-bottom: 7px">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td style="width: 25%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">License Number</label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                                <td style="width: 73%;">
                                                                    <asp:TextBox runat="server" ID="txtLicenseNo" CssClass="form-control" autocomplete="off" Width="280px" />
                                                                    <asp:RequiredFieldValidator ID="rfvLicenseNo" ErrorMessage="Please Enter License Number."
                                                                        ControlToValidate="txtLicenseNo" runat="server" ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" style="height: 5px;"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 25%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">License Title</label>
                                                                </td>

                                                                <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                                <td style="width: 73%;">
                                                                    <asp:TextBox runat="server" ID="txtLicenseTitle" CssClass="form-control" autocomplete="off" Width="280px" />
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Please Enter License Title."
                                                                        ControlToValidate="txtLicenseTitle" runat="server" ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" style="height: 5px;"></td>
                                                            </tr>
                                                            <tr style="margin-bottom: 7px">
                                                                <td style="width: 25%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">Start Date</label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                                <td style="width: 73%;">
                                                                    <asp:TextBox runat="server" ID="txtStartDate" placeholder="DD-MM-YYYY"
                                                                        class="form-control" Style="width: 115px;" />
                                                                    <asp:RequiredFieldValidator ErrorMessage="Please Enter Start Date." ControlToValidate="txtStartDate"
                                                                        runat="server" ID="RequiredFieldValidator4" ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" style="height: 5px;"></td>
                                                            </tr>
                                                            <tr style="margin-bottom: 7px">
                                                                <td style="width: 25%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">End Date</label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                                <td style="width: 73%;">
                                                                    <asp:TextBox runat="server" ID="txtEndDate" placeholder="DD-MM-YYYY"
                                                                        class="form-control" Style="width: 115px;" />
                                                                    <asp:RequiredFieldValidator ErrorMessage="Please Enter End Date." ControlToValidate="txtEndDate"
                                                                        runat="server" ID="RequiredFieldValidator3" ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3" style="height: 5px;"></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>

                                                <div style="margin-bottom: 7px" runat="server" id="divUploadDocument">
                                                    <% if (UploadDocumentLink == "True")
                                                        {%>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <asp:Label ID="Label3" runat="server" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</asp:Label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Upload  Document(s)</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="TxtUploadocumentlnkLIc" class="form-control" />
                                                            </td>
                                                            <td>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Button ID="UploadlinkCompliancefile" runat="server" Text="Add Link" Style="" OnClick="UploadlinkCompliancefile_Click"
                                                                            class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Add Link" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="UploadlinkCompliancefile" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <%}%>
                                                    <% if (UploadDocumentLink == "False")
                                                        {%>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <asp:Label ID="lblDocComplasary" runat="server" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</asp:Label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Upload  Document(s)</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 60%;">
                                                                <asp:FileUpload ID="fuSampleFile" Multiple="Multiple" runat="server" onchange="fFilesubmit()" Style="color: black" />

                                                            </td>
                                                            <td style="width: 13%;">
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Button ID="UploadDocument" runat="server" Text="Upload Document"
                                                                            Style="display: none;" OnClick="UploadDocument_Click"
                                                                            class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Upload Document"
                                                                            CausesValidation="true" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <%}%>
                                                </div>
                                                <div style="margin-bottom: 7px" runat="server" id="divWorkingfiles">
                                                    <% if (UploadDocumentLink == "True")
                                                        {%>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Upload Working Files(s)</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="txtboxuploadworkingfiles" class="form-control" />
                                                            </td>
                                                            <td>
                                                                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Button ID="Button1" runat="server" Text="Add Link" Style="" OnClick="Button1_Click"
                                                                            class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Add Link" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="Button1" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <%}%>
                                                    <% if (UploadDocumentLink == "False")
                                                        {%>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label style="width: 5px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Upload Working Files(s)</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:FileUpload ID="FileUpload1" Multiple="Multiple" runat="server" onchange="fWorkingFilesubmit()" Style="color: black" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <%}%>
                                                </div>

                                                <div style="margin-bottom: 7px" runat="server" id="divInternalgrdFiles">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%;">
                                                                <asp:GridView runat="server" ID="grdDocument" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                    PageSize="100" AllowPaging="true" OnRowCommand="grdDocument_RowCommand" OnRowDataBound="grdDocument_RowDataBound"
                                                                    AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                                                                            <ItemTemplate>
                                                                                <%#Container.DataItemIndex+1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Document Name" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px;">
                                                                                    <asp:Label ID="lblDocumentName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DocName") %>' ToolTip='<%# Eval("DocName") %>'></asp:Label>
                                                                                    <asp:LinkButton ID="lnkRedirectDocument" runat="server" data-toggle="tooltip" Style="color: blue; text-decoration: underline;"
                                                                                        OnClientClick=<%# "openInNewTab('" + Eval("DocPath") + "')" %> Text='<%# Eval("DocName") %>' />
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Document Type" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                    <asp:Label ID="lblIsLinkTrue" Visible="false" runat="server" data-placement="bottom" Text='<%# Eval("ISLink") %>' ToolTip='<%# Eval("ISLink") %>'></asp:Label>
                                                                                    <asp:Label ID="lblDocType" runat="server" data-placement="bottom" Text='<%# Eval("DocType") %>' ToolTip='<%# Eval("DocType") %>'></asp:Label>
                                                                                    <asp:Label ID="lblScheduleOnID" runat="server" Visible="false" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ScheduleOnID") %>'></asp:Label>
                                                                                    <asp:Label ID="lblComplianceInstanceID" runat="server" Visible="false" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ComplianceInstanceID") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="upgrid" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton ID="lnkDownloadDocument" runat="server" CommandName="Download Document" ToolTip="Download Document" data-toggle="tooltip"
                                                                                            CommandArgument='<%# Eval("Id") %>'>
                                                                                     <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download Document" /></asp:LinkButton>
                                                                                        <asp:LinkButton ID="lnkViewDocument" runat="server" CommandName="View Document" ToolTip="View Document" data-toggle="tooltip"
                                                                                            CommandArgument='<%# Eval("Id") %>'>
                                                                                        <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View Document" /></asp:LinkButton>
                                                                                        <asp:LinkButton ID="lnkDeleteDocument" runat="server" CommandName="Delete Document" ToolTip="Delete Document" data-toggle="tooltip"
                                                                                            CommandArgument='<%# Eval("Id") %>'>
                                                                                           <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Document" /></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="lnkDownloadDocument" />
                                                                                        <asp:PostBackTrigger ControlID="lnkViewDocument" />

                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <PagerSettings Visible="false" />
                                                                    <PagerTemplate>
                                                                    </PagerTemplate>
                                                                    <EmptyDataTemplate>
                                                                        No Record Found
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%;">
                                                                <asp:Label ID="lblDate" runat="server" style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</asp:Label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Date</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="tbxDate" placeholder="DD-MM-YYYY"
                                                                    class="form-control" Style="width: 115px;" />

                                                                <asp:RequiredFieldValidator ErrorMessage="Please select Date." ControlToValidate="tbxDate"
                                                                    runat="server" ID="RequiredFieldValidator1" ValidationGroup="ComplianceValidationGroup1" Display="None" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr class="spaceUnder1">
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Cost</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="txtCost" placeholder="Cost" onkeypress="return isNumberKey(event)" class="form-control" Style="width: 115px;" />
                                                                <%--<asp:RegularExpressionValidator  Runat="server" ID="valNumbersOnly" ValidationGroup="LicenseListPageValidationGroup" ControlToValidate="txtCost" Display="Dynamic" ForeColor="Red" ErrorMessage="Please enter only numbers" ValidationExpression="(^([0-9]*|\d*\d{1}?\d*)$)">
                                                                                </asp:RegularExpressionValidator>--%>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <div style="margin-bottom: 7px" runat="server" id="divChkIsActive" visible="false">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp; </label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Is Permanent Active</label>
                                                            </td>

                                                            <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:CheckBox runat="server" ID="ChkIsActive" />

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div style="margin-bottom: 7px">
                                                    <%--  <fieldset id="fieldsetpenalty" runat="server" visible="false" style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">--%>

                                                    <fieldset id="fieldsetpenalty" runat="server" style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%; display: none;">
                                                        <table style="width: 100%">
                                                            <tr id="trPenalty" runat="server">
                                                                <td style="width: 11%; font-weight: bold;">

                                                                    <asp:CheckBox ID="chkPenaltySave" onclick="fillValuesInTextBoxes()" Text="Value is not known at this moment" runat="server" />

                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;"></td>
                                                                <td style="width: 10%;"></td>

                                                                <td style="width: 9%;"></td>
                                                                <td style="width: 2%; font-weight: bold;"></td>
                                                                <td style="width: 10%;"></td>
                                                            </tr>

                                                            <tr>
                                                                <td style="width: 11%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">Interest(INR) </label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 10%;">
                                                                    <asp:TextBox runat="server" ID="txtInterest" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                        TargetControlID="txtInterest" />
                                                                </td>

                                                                <td style="width: 11%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label id="lblpenaltytextchange" runat="server" style="font-weight: bold; vertical-align: text-top;">Penalty Amount(INR) </label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 10%;">
                                                                    <asp:TextBox runat="server" ID="txtPenalty" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                    <asp:CompareValidator runat="server" ID="cmpNumbers" ControlToValidate="txtPenalty" Type="Double" Operator="DataTypeCheck" ErrorMessage="Please enter proper value ofpenalty" />
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                        TargetControlID="txtPenalty" />
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </fieldset>
                                                    <br />
                                                    <fieldset id="fieldsetTDS" runat="server" visible="false" style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                        <table style="width: 100%">
                                                            <tr class="spaceUnder">
                                                                <td style="width: 11%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">Nature of compliance </label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 10%;">
                                                                    <asp:Label ID="lblNatureofcompliance" Style="width: 88%; font-size: 13px; color: #333;" autosize="true" runat="server" />
                                                                    <asp:Label ID="lblNatureofcomplianceID" Visible="false" runat="server" />

                                                                </td>
                                                                <td style="width: 9%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label id="lblValueAsPerSystem" runat="server" style="font-weight: bold; vertical-align: text-top;">Liability as per system (A) </label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 10%;">
                                                                    <%-- OnTextChanged="txtValueAsPerSystem_TextChanged" AutoPostBack="true"--%>
                                                                    <asp:TextBox runat="server" ID="txtValueAsPerSystem" onkeyup="DiffPerformer();" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                        TargetControlID="txtValueAsPerSystem" />
                                                                </td>
                                                            </tr>
                                                            <tr class="spaceUnder">
                                                                <td style="width: 11%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label id="lblValueAsPerReturn" runat="server" style="font-weight: bold; vertical-align: text-top;">Liability as per return (B) </label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 10%;">
                                                                    <%-- OnTextChanged="txtValueAsPerReturn_TextChanged" AutoPostBack="true"--%>
                                                                    <asp:TextBox runat="server" ID="txtValueAsPerReturn" onkeyup="DiffPerformer();" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                        TargetControlID="txtValueAsPerReturn" />
                                                                </td>
                                                                <td style="width: 9%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label id="lbldiffAB" runat="server" style="font-weight: bold; vertical-align: text-top;">Difference (A)-(B) </label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">:</td>
                                                                <td style="width: 10%;">
                                                                    <asp:TextBox runat="server" ID="txtDiffAB" ReadOnly="true" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                </td>
                                                            </tr>

                                                            <tr id="trreturn" runat="server" class="spaceUnder">
                                                                <td style="width: 11%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label id="lblLiabilityPaid" runat="server" style="font-weight: bold; vertical-align: text-top;">Liability Paid (C) </label>
                                                                </td>
                                                                <td id="tdLiabilityPaidSC" runat="server" style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 10%;">
                                                                    <%--  OnTextChanged="txtLiabilityPaid_TextChanged" AutoPostBack="true"--%>
                                                                    <asp:TextBox ID="txtLiabilityPaid" runat="server" onkeyup="DiffPerformer();" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                        TargetControlID="txtLiabilityPaid" />
                                                                </td>
                                                                <td style="width: 9%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label id="lbldiffBC" runat="server" style="font-weight: bold; vertical-align: text-top;">Difference (B)-(C) </label>
                                                                </td>
                                                                <td id="tddiffBCSC" runat="server" style="width: 2%; font-weight: bold;">:</td>
                                                                <td style="width: 10%;">
                                                                    <asp:TextBox runat="server" ID="txtDiffBC" ReadOnly="true" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                </td>
                                                            </tr>


                                                        </table>
                                                    </fieldset>


                                                </div>

                                   

                                                <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Physical Location of document</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="txtphysicalLocation" Width="280px"  class="form-control" Rows="1" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                 <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;"> File Number</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="txtFileno" Width="280px"  class="form-control" Rows="1" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>


                                                <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Remarks</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="tbxRemarks" TextMode="MultiLine" class="form-control" Rows="2" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>


                                                <div style="margin-bottom: 7px; margin-left: 42%; margin-top: 10px;">
                                                    <asp:Button Text="Submit" runat="server" ID="btnSave"
                                                        OnClientClick="javascript:return PenaltyValidate();" OnClick="btnSave_Click" CssClass="btn btn-search"
                                                        ValidationGroup="ComplianceValidationGroup1" />
                                                    <asp:Button Text="submit" runat="server" ID="btnSaveDOCNotCompulsory"
                                                        OnClientClick="javascript:return PenaltyEventValidate(); "
                                                        OnClick="btnSave_Click" CssClass="btn btn-search"
                                                        ValidationGroup="ComplianceValidationGroup1" />
                                                    <asp:Button Text="Close" ID="btnCancel"
                                                        Style="margin-left: 15px; display: none;" CssClass="btn btn-search" data-dismiss="modal" runat="server" />

                                                      <asp:Button Text="Go To Portal" ID="lnkgotoportal"  Visible="false" OnClick="lnkgotoportal_Click"
                                                        Style="margin-left: 15px;" CssClass="btn btn-search" data-dismiss="modal" runat="server" />

                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div runat="server" id="divDeleteDocument" visible="false" style="text-align: left;">
                            <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 50%">
                                            <asp:UpdatePanel runat="server">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                                        OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="tblComplianceDocumnets">
                                                                <thead>
                                                                    <th>Compliance Related Documents</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                        ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                    </asp:LinkButton></td>
                                                                <td>
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>' CommandName="Delete"
                                                                        OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                        ID="lbtLinkDocbutton" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                                    </asp:LinkButton></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rptWorkingFiles" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                                        OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="tblWorkingFiles">
                                                                <thead>
                                                                    <th>Compliance Working Files</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td style="width: 50%">
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                        ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                                    </asp:LinkButton></td>
                                                                <td>
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>' CommandName="Delete"
                                                                        OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                        ID="lbtLinkbutton" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                                    </asp:LinkButton></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>

                            </fieldset>
                        </div>

                        <div id="LegalUpdates" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 12px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseLegalUpdates">
                                                    <h2>Legal Updates</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseLegalUpdates"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="collapseLegalUpdates" class="collapse">

                                            <div id="gridUpdate"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="AuditLog" class="row Dashboard-white-widget" style="margin-bottom: 10px;">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog">
                                                    <h2>Audit Log</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="collapseAuditLog" class="collapse">
                                            <div runat="server" id="log" style="text-align: left;">
                                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                                    <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                        <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false" AllowSorting="true"
                                                            AllowPaging="true" PageSize="5" CssClass="table" GridLines="Horizontal" OnPageIndexChanging="grdTransactionHistory_OnPageIndexChanging"
                                                            OnRowCreated="grdTransactionHistory_RowCreated" BorderWidth="0px" OnSorting="grdTransactionHistory_Sorting"
                                                            DataKeyNames="ComplianceTransactionID" OnRowCommand="grdTransactionHistory_RowCommand">
                                                            <Columns>
                                                                <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />
                                                                <asp:TemplateField HeaderText="Date">
                                                                    <ItemTemplate>
                                                                        <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString("dd-MM-yyyy") : ""%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Interest" HeaderText="Interest" />
                                                                <asp:BoundField DataField="Penalty" HeaderText="Penalty" />
                                                                <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                                                <asp:BoundField DataField="Status" HeaderText="Status" />
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Right" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </div>
                                                </fieldset>
                                                <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-size: 10px; color: #666;" Visible="false"></asp:Label>
                                                <div></div>
                                                <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                                                <asp:Button ID="btnDownload" runat="server" Style="display: none" OnClick="btnDownload_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnDownload" />
                    <asp:PostBackTrigger ControlID="btnSave" />
                    <asp:PostBackTrigger ControlID="lbDownloadSample" />
                    <asp:PostBackTrigger ControlID="btnSaveDOCNotCompulsory" />
                    <asp:PostBackTrigger ControlID="UploadDocument" />
                </Triggers>
            </asp:UpdatePanel>



        </div>

        <div class="modal fade" id="DocumentPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
            <div class="modal-dialog" style="width: 100%;">
                <div class="modal-content">
                    <div class="modal-header">

                        <button type="button" class="close" onclick="$('#DocumentPopUp').modal('hide');" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="width: 100%;">

                        <iframe src="about:blank" id="docViewerAll" runat="server" width="100%" height="550px"></iframe>

                    </div>
                </div>
            </div>
        </div>

        <div>
            <div class="modal fade" id="modalDocumentPerformerViewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                <div class="modal-dialog" style="width: 100%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="height: 570px;">
                            <div style="width: 100%;">
                                <div style="float: left; width: 10%">
                                    <table width="100%" style="text-align: left; margin-left: 5%;">
                                        <thead>
                                            <tr>
                                                <td valign="top">
                                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Repeater ID="rptComplianceVersionView" runat="server" OnItemCommand="rptComplianceVersionView_ItemCommand">
                                                                <HeaderTemplate>
                                                                    <table id="tblComplianceDocumnets">
                                                                        <thead>
                                                                            <th>Versions</th>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblDocumentVersionView"
                                                                                        runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </table>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div style="float: left; width: 90%">
                                  <%--  <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdatleMode="Conditional">
                                        <ContentTemplate>--%>
                                            <asp:Label ID="lblMessage" runat="server" Style="color: red;"></asp:Label>
                                       <%-- </ContentTemplate>
                                    </asp:UpdatePanel>--%>
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                        <iframe src="about:blank" id="docViewerPerformerAll" runat="server" width="100%" height="535px"></iframe>
                                    </fieldset>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="DocumentPriview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="$('#DocumentPriview').modal('hide');" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                <iframe src="about:blank" id="docPriview" runat="server" width="100%" height="535px"></iframe>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="ConfirmationModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: 30%;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <div style="width: 10%; float: right">
                            <button type="button" class="close" onclick="$('#ConfirmationModel').modal('hide');" aria-hidden="true">×</button>
                        </div>
                        <div style="width: 90%; align-content: center; margin-left: 82px; float: left;">
                            <p style="font-size: 20px">Confirmation</p>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div id="DivYesNoConf">
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="height: 150px;">
                                        <div style="width: 80%; margin-left: 15%;">
                                            <p>Proceeds to subsequent checks?</p>
                                            <div style="padding: 13px; padding-left: 65px;">
                                                <asp:LinkButton ID="lblYes" runat="server" Text="Yes" CssClass="btn btn-primary" OnClientClick="callOnButtonYes1()"></asp:LinkButton>
                                                <asp:LinkButton ID="lblNo" runat="server" Text="No" CssClass="btn btn-primary" OnClick="lblNo_Click" OnClientClick="callOnButtonNo1()"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>

                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div>
            <div class="modal fade" id="DocumentShowPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                <div class="modal-dialog" style="width: 100%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="height: 570px;">
                            <div style="width: 100%;">
                                <div style="float: left; width: 10%">
                                    <table width="100%" style="text-align: left; margin-left: 5%;">
                                        <thead>
                                            <tr>
                                                <td valign="top">
                                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdatleMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Repeater ID="rptLicenseVersionView" runat="server" OnItemCommand="rptLicenseVersionView_ItemCommand"
                                                                OnItemDataBound="rptLicenseVersionView_ItemDataBound">
                                                                <HeaderTemplate>
                                                                    <table id="tblLicenseDocumnets">
                                                                        <thead>
                                                                            <th>Versions</th>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("LicenseID") + ","+ Eval("Version") + ","+ Eval("FileID") %>'
                                                                                        ID="lblLicenseVersionView"
                                                                                        runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="lblLicenseVersionView" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </table>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="rptLicenseVersionView" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div style="float: left; width: 90%">
                                   <%-- <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                        <ContentTemplate>--%>
                                            <asp:Label ID="lblMessageReviewer1" runat="server" Style="color: red;"></asp:Label>
                                        <%--</ContentTemplate>
                                    </asp:UpdatePanel>--%>
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                        <iframe src="about:blank" id="docViewerLicenseAll" runat="server" width="100%" height="535px"></iframe>
                                    </fieldset>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
        <!-- nice scroll -->
        <script type="text/javascript" src="../Newjs/jquery.scrollTo.min.js"></script>
        <script type="text/javascript" src="../Newjs/jquery.nicescroll.js"></script>
        <!-- charts scripts -->
        <script type="text/javascript" src="../assets/jquery-knob/js/jquery.knob.js"></script>
        <script type="text/javascript" src="../Newjs/owl.carousel.js"></script>
        <!-- jQuery full calendar -->
        <script type="text/javascript" src="../Newjs/fullcalendar.min.js"></script>
        <!--script for this page only-->
        <script type="text/javascript" src="../Newjs/calendar-custom.js"></script>
        <script type="text/javascript" src="../Newjs/jquery.rateit.min.js"></script>
        <!-- custom select -->
        <script type="text/javascript" src="../Newjs/jquery.customSelect.min.js"></script>
        <!--custome script for all page-->
        <script type="text/javascript" src="../Newjs/scripts.js"></script>
        <asp:HiddenField runat="server" ID="mont" />

        <script type="text/javascript">
            function fFilesubmit() {                
                if (ValidateFilestatus()) {
                    fileUpload = document.getElementById('fuSampleFile');
                    if (fileUpload.value != '') {
                        document.getElementById("<%=UploadDocument.ClientID %>").click();
                    }
                }
                else {
                    $("#Labelmsg").css('display', 'block');
                    $('#Labelmsg').text("Invalid file error. System does not support uploaded file.Please upload another file.");
                    $('#ValidationSummary1').IsValid = false;
                    document.getElementById("ValidationSummary1").value = "Invalid file error. System does not support uploaded file.Please upload another file.";
                }
            }
            function fWorkingFilesubmit() {
                if (ValidateFilestatus()) {
                    fileUpload = document.getElementById('FileUpload1');
                    if (fileUpload.value != '') {
                        document.getElementById("<%=UploadDocument.ClientID %>").click();
                }
            }
            else {
                $("#Labelmsg").css('display', 'block');
                $('#Labelmsg').text("Invalid file error. System does not support uploaded file.Please upload another file.");
                $('#ValidationSummary1').IsValid = false;
                document.getElementById("ValidationSummary1").value = "Invalid file error. System does not support uploaded file.Please upload another file.";
            }
            }

        function PenaltyValidate() {
            var tbxDate = $("#tbxDate").val();
            if (tbxDate != "") {
                var appdate = new Date($("#lblApplicationDate").text());
                var adate = new Date($.datepicker.parseDate("dd-mm-yy", $("#tbxDate").val()));
                var monnonmon = $("#mont").val();
                var lstatus = -1;
                var a = compare(appdate, adate);
                if (a == 1) {
                    lstatus = 2;
                } else if (a == -1) {
                    lstatus = 3;
                } else if (a == 0) {
                    lstatus = 2;
                }
                var ddlstatus = $('select#ddlStatus option:selected').val();
                if (ddlstatus == -1) {
                    $("#Labelmsg").css('display', 'block');
                    $('#Labelmsg').text("Please select status");
                    $('#ValidationSummary1').IsValid = false;
                    document.getElementById("ValidationSummary1").value = "Please select status";
                    return false;
                }
                if (ddlstatus == 7) {
                    var lno = $("#txtLicenseNo").val();
                    var lT = $("#txtLicenseTitle").val();
                    var lSD = $("#txtStartDate").val();
                    var lED = $("#txtEndDate").val();

                    if (lno == "") {
                        $("#Labelmsg").css('display', 'block');
                        $('#Labelmsg').text("Please enter license number");
                        $('#ValidationSummary1').IsValid = false;
                        document.getElementById("ValidationSummary1").value = "Please enter license number";
                        return false;
                    }
                    if (lT == "") {
                        $("#Labelmsg").css('display', 'block');
                        $('#Labelmsg').text("Please enter license title");
                        $('#ValidationSummary1').IsValid = false;
                        document.getElementById("ValidationSummary1").value = "Please enter license title";
                        return false;
                    }
                    if (lSD == "") {
                        $("#Labelmsg").css('display', 'block');
                        $('#Labelmsg').text("Please enter license start date");
                        $('#ValidationSummary1').IsValid = false;
                        document.getElementById("ValidationSummary1").value = "Please enter license start date";
                        return false;
                    }
                    if (lED == "") {
                        $("#Labelmsg").css('display', 'block');
                        $('#Labelmsg').text("Please enter license end date");
                        $('#ValidationSummary1').IsValid = false;
                        document.getElementById("ValidationSummary1").value = "Please enter license end date";
                        return false;
                    }
                }
                if (lstatus == 12) {
                    var filename = $("#FileUpload1").val();
                    if (filename == "") {
                        $("#Labelmsg").css('display', 'block');
                        $('#Labelmsg').text("Please select working file for upload.");
                        $('#ValidationSummary1').IsValid = false;
                        document.getElementById("ValidationSummary1").value = "Please select working file for upload.";
                        return false;
                    }
                }
                else if (ddlstatus != 12){
                    var filename = $("#fuSampleFile").val();
                    if (filename == "") {
                        var rowscount = $("#<%=grdDocument.ClientID %> tr").length;
                            if (rowscount == 0) {
                                $("#Labelmsg").css('display', 'block');
                                $('#Labelmsg').text("Please select documents for upload.");
                                $('#ValidationSummary1').IsValid = false;
                                document.getElementById("ValidationSummary1").value = "Please select documents for upload.";
                                return false;
                            }
                        }
                    }
                    //if (monnonmon != "") {
                    //    if (monnonmon != 1) {
                    //        if (lstatus == 3 && a == -1) {
                    //            var chk = $("#chkPenaltySave").is(":checked");
                    //            if (chk == false) {
                    //                var txtInterest = $("#txtInterest").val();
                    //                var txtPenalty = $("#txtPenalty").val();
                    //                if (txtInterest == "" || txtPenalty == "" || txtInterest == "" || txtPenalty == "0") {
                    //                    $("#Labelmsg").css('display', 'block');
                    //                    $('#Labelmsg').text("Please enter interest and penalty");
                    //                    $('#ValidationSummary1').IsValid = false;
                    //                    document.getElementById("ValidationSummary1").value = "Please enter interest and penalty";
                    //                    return false;
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                }
                else {
                    $("#Labelmsg").css('display', 'block');
                    $('#Labelmsg').text("Please select date");
                    $('#ValidationSummary1').IsValid = false;
                    document.getElementById("ValidationSummary1").value = "Please select date";
                    return false;
                }
            }
        </script>
    </form>
</body>
</html>


