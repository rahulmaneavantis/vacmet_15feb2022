﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class MyReport : System.Web.UI.Page
    {
        public     string CompDocReviewPath = "";
        protected  string CId;
        protected  string UserId;
        protected  int CustId;
        protected  int UId;
        protected  List<Int32> roles;
        protected  bool IsNotCompiled;     
        protected  int RoleID;
        protected  int RoleFlag;
        protected  string Falg;
        protected  bool DisableFalg;
        protected  String SDate;
        protected  String LDate;
        protected  String IsMonthID;
        protected  string Path;
        protected  string CustomerName;
        protected  int PerformerFlagID;
        protected  int ReviewerFlagID;
        protected  int ApproverFlagID;
        protected  int ManagmentFlagID;
        protected  int DepartmentFlagID;
        protected  string RoleKey;
        protected  string Authorization;
        protected  int customizedid;
        protected  int AuditorFlagID;
        public bool ReturnamtFields;
        public bool EntityField;
        public bool ComplianceCT;
        protected int DetailReportThreshold;
        public bool BankNewFields;
        public bool RemoveColumn;

        protected void Page_Load(object sender, EventArgs e)
        {
            IsNotCompiled = false;
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            CId = Convert.ToString(AuthenticationHelper.CustomerID);
            UserId = Convert.ToString(AuthenticationHelper.UserID);
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            RoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);
            CustomerName = GetCustomerName(CustId);
            RoleFlag = 0;
            IsMonthID = "3";
            PerformerFlagID = 0;
            ReviewerFlagID = 0;
            ApproverFlagID = 0;
            ManagmentFlagID = 0;
            DepartmentFlagID = 0;
            AuditorFlagID = 0;
            DetailReportThreshold = Convert.ToInt32(GetDetailReportThreshold());

            //DetailReportThreshold = Convert.ToInt32(ConfigurationManager.AppSettings["DetailReportThreshold"]);
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            BankNewFields = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "BankDetailsColumn");
            RemoveColumn = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "RemovecolumnDeReport");

            var UserDetails = UserManagement.GetByID(AuthenticationHelper.UserID);
            if (!string.IsNullOrEmpty(Convert.ToString(UserDetails.IsHead)))
            {
                if ((bool)UserDetails.IsHead)
                {
                    DepartmentFlagID = 1;
                    RoleKey = "DEPT";
                }
            }

            roles = Session["User_comp_Roles"] as List<int>;//get role for Performer and Reviewer and Approver
            if (roles.Contains(3))
            {
                PerformerFlagID = 1;
                RoleKey = "PRA";
            }
            if (roles.Contains(4))
            {
                ReviewerFlagID = 1;
                RoleKey = "REV";
            }
            if (roles.Contains(6))
            {
                ApproverFlagID = 1;
                RoleKey = "APPR";
            }
            if (AuthenticationHelper.Role == "MGMT")
            {
                ManagmentFlagID = 1;
                Falg = "MGMT";
                RoleFlag = 1;
                DisableFalg = false;
                RoleKey = "MGMT";
            }
            if (AuthenticationHelper.Role == "AUDT")
            {
                //IsMonthID = "AUD";
                SDate = Convert.ToDateTime(Session["Auditstartdate"].ToString()).Date.ToString("dd-MM-yyyy");
                LDate = Convert.ToDateTime(Session["Auditenddate"].ToString()).Date.ToString("dd-MM-yyyy");

                RoleKey = "AUD";
                Falg = "AUD";
                DisableFalg = true;
                AuditorFlagID = 1;
            }
            else if (AuthenticationHelper.Role == "EXCT")
            {
                DisableFalg = false;
                Falg = "PRA";
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

                if (roles.Contains(4))
                {
                    RoleFlag = 1;
                }
            }
            else if (AuthenticationHelper.Role == "CADMN")
            {
                DisableFalg = false;
                Falg = "PRA";
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

                if (roles.Contains(4))
                {
                    RoleFlag = 1;
                }
            }

            // STT Change- Add Status
            string customer = ConfigurationManager.AppSettings["NotCompliedCustID"].ToString();
            List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
            if (PenaltyNotDisplayCustomerList.Count > 0)
            {
                foreach (string PList in PenaltyNotDisplayCustomerList)
                {
                    if (PList == CustId.ToString())
                    {
                        IsNotCompiled = true;
                        break;
                    }
                }
            }
            ReturnamtFields = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "ReturnAmount");
            EntityField= CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "EntityDetReport");
            ComplianceCT = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "ComplianceCategoryType");
            customizedid = GetCustomizedCustomerid(CustId);
        }
        public static int GetDetailReportThreshold()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var form = (from row in entities.DetailReportThresholds
                            select row.ThresholdNo).FirstOrDefault();
                return form;
            }
        }
        public static int GetCustomizedCustomerid(int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClientCustomizations
                            where row.CustomizationName == "TaxReportFields"
                            && row.ClientID==customerid
                            select row.ClientID).FirstOrDefault();

                return data;
            }
        }

        public static string GetCustomerName(int CID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    string CName = (from row in entities.CustomerViews
                                    where row.ID == CID
                                    select row.Name).FirstOrDefault();

                    return CName;
                }
            }
            catch (Exception ex)
            {
                return "";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }

        }

    }
}