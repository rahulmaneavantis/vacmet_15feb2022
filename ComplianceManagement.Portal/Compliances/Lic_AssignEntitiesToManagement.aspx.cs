﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.License;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class Lic_AssignEntitiesToManagement : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                divFilterUsers.Visible = true;
                FilterLocationdiv.Visible = true;
                btnAddComplianceType.Visible = true;
                BindLocationFilter();
                //BindComplianceEntityInstances();
                BindLicenseType();
                BindUsers(ddlUsers);
                BindUsers(ddlFilterUsers);
                BindLocation();
                tbxBranch.Attributes.Add("readonly", "readonly");
            }
        }

        private void BindLicenseType()
        {
            try
            {
                ddlLicenseType.DataTextField = "Name";
                ddlLicenseType.DataValueField = "ID";

                ddlLicenseType.DataSource = LicenseTypeMasterManagement.GetLicenseType();
                ddlLicenseType.DataBind();

                ddlLicenseType.Items.Insert(0, new ListItem("< Select >", "-1"));
                ddlLicenseType.Items.Insert(1, new ListItem("Select All", "All"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindComplianceEntityInstances()
        {
            try
            {
                int branchID = -1;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                int userID = -1;
                if ((!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue)))
                {
                    userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }

                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                grdAssignEntities.DataSource = LIC_Assign_Entity_Management.SelectAllEntities(customerID,userID, branchID);
                grdAssignEntities.DataBind();

                upComplianceTypeList.Update();
                ForceCloseFilterBranchesTreeView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocation()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);
                TreeNode node = new TreeNode("All", "-2");
                node.SelectAction = TreeNodeSelectAction.Select;
                 
                tvBranches.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                      node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    
                    BindBranchesHierarchy(node, item);
                    tvBranches.Nodes.Add(node);
                }

                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocationFilter()
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                TreeNode node = new TreeNode("< All >", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            BindComplianceEntityInstances();
        }

        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }

        private void BindUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                
                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";

                ddlUserList.Items.Clear();
                var users = UserManagement.GetAllManagmentUser(customerID);
                users.Insert(0, new { ID = -1, Name = ddlUserList == ddlUsers ? "< Select >" : "< All >" });

                ddlUserList.DataSource = users;
                ddlUserList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceEntityInstances();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                int userID = Convert.ToInt32(ddlUsers.SelectedValue);
                int branchId = Convert.ToInt32(tvBranches.SelectedNode.Value);

                if (!ddlLicenseType.SelectedValue.Equals("All"))
                {
                    if (branchId == -2)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var customerBranches = (from row in entities.CustomerBranches
                                                    where row.IsDeleted == false && row.CustomerID==AuthenticationHelper.CustomerID
                                                    select row);
                            foreach (var node in customerBranches)
                            {
                                int licensetypeId = Convert.ToInt32(ddlLicenseType.SelectedValue);
                                var data = LIC_Assign_Entity_Management.SelectEntity(Convert.ToInt32(node.ID), userID, licensetypeId);
                                if (data != null)
                                {
                                }
                                else
                                {
                                    LIC_EntitiesAssignment objEntitiesAssignment = new LIC_EntitiesAssignment();
                                    objEntitiesAssignment.UserID = userID;
                                    objEntitiesAssignment.BranchID = Convert.ToInt32(node.ID);
                                    objEntitiesAssignment.LicenseTypeID = licensetypeId;
                                    objEntitiesAssignment.CreatedOn = DateTime.UtcNow;
                                    LIC_Assign_Entity_Management.Create(objEntitiesAssignment);
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);
                                }
                            }
                        }
                    }
                    else
                    {
                        int licensetypeId = Convert.ToInt32(ddlLicenseType.SelectedValue);
                        var data = LIC_Assign_Entity_Management.SelectEntity(branchId, userID, licensetypeId);
                        if (data != null)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Entity already assigned to location for the category.";
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                        }
                        else
                        {
                            LIC_EntitiesAssignment objEntitiesAssignment = new LIC_EntitiesAssignment();
                            objEntitiesAssignment.UserID = userID;
                            objEntitiesAssignment.BranchID = branchId;
                            objEntitiesAssignment.LicenseTypeID = licensetypeId;
                            objEntitiesAssignment.CreatedOn = DateTime.UtcNow;
                            LIC_Assign_Entity_Management.Create(objEntitiesAssignment);
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);
                        }
                    }
                }
                else
                {
                    if (branchId == -2)
                    {
                        var licensetypelist = LicenseTypeMasterManagement.GetLicenseType();
                        //var CatagoryList = ComplianceCategoryManagement.GetAll();
                        List<LIC_EntitiesAssignment> licassignmentEntities = new List<LIC_EntitiesAssignment>();
                        foreach (Lic_tbl_LicenseType_Master type in licensetypelist)
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                var customerBranches = (from row in entities.CustomerBranches
                                                        where row.IsDeleted == false && row.CustomerID == AuthenticationHelper.CustomerID
                                                        select row);
                                foreach (var node in customerBranches)
                                {
                                    int lictypeId = Convert.ToInt32(type.ID);
                                    var data = LIC_Assign_Entity_Management.SelectEntity(Convert.ToInt32(node.ID), userID, lictypeId);
                                    if (data != null)
                                    {
                                    }
                                    else
                                    {
                                        LIC_EntitiesAssignment objEntitiesAssignment = new LIC_EntitiesAssignment();
                                        objEntitiesAssignment.UserID = userID;
                                        objEntitiesAssignment.BranchID = Convert.ToInt32(node.ID);
                                        objEntitiesAssignment.LicenseTypeID = lictypeId;
                                        objEntitiesAssignment.CreatedOn = DateTime.UtcNow;
                                        LIC_Assign_Entity_Management.Create(objEntitiesAssignment);
                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);
                                    }
                                }
                            }
                        }                       
                    }
                    else
                    {
                        var licensetypelist = LicenseTypeMasterManagement.GetLicenseType();
                        //var CatagoryList = ComplianceCategoryManagement.GetAll();
                        List<LIC_EntitiesAssignment> assignmentEntities = new List<LIC_EntitiesAssignment>();
                        foreach (Lic_tbl_LicenseType_Master type in licensetypelist)
                        {
                            var data = LIC_Assign_Entity_Management.SelectEntity(branchId, userID, (int)type.ID);
                            if (!(data != null))
                            {
                                LIC_EntitiesAssignment objEntitiesAssignment = new LIC_EntitiesAssignment();
                                objEntitiesAssignment.UserID = userID;
                                objEntitiesAssignment.BranchID = branchId;
                                objEntitiesAssignment.LicenseTypeID = type.ID;
                                objEntitiesAssignment.CreatedOn = DateTime.UtcNow;
                                assignmentEntities.Add(objEntitiesAssignment);
                            }
                        }
                        LIC_Assign_Entity_Management.Create(assignmentEntities);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('close');", true);
                    }
                }               
                BindComplianceEntityInstances();                             
            }
            catch (Exception ex)
            {
               
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddComplianceType_Click(object sender, EventArgs e)
        {
            try
            {
                if (tvBranches.SelectedNode != null)
                {
                    tvBranches.SelectedNode.Selected = false;
                }
              
                lblErrorMassage.Text = "";
                ddlUsers.SelectedValue = "-1";
                ddlLicenseType.SelectedValue = "-1";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignEntitiesDialog\").dialog('open');", true);
                tbxBranch.Text = "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                ForceCloseFilterBranchesTreeView();
                upCompliance.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAssignEntities_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdAssignEntities.PageIndex = e.NewPageIndex;
            BindComplianceEntityInstances();
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdAssignEntities_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                int branchID = -1;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                int userID = -1;
                if ((!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue)))
                {
                    userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }

                int customerID =Convert.ToInt32(AuthenticationHelper.CustomerID);

                var assignmentList = LIC_Assign_Entity_Management.SelectAllEntities(customerID,userID,branchID);
                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdAssignEntities.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdAssignEntities.Columns.IndexOf(field);
                    }
                }

                grdAssignEntities.DataSource = assignmentList;
                grdAssignEntities.DataBind();
                tbxFilterLocation.Text = "< Select Location >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdAssignEntities_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

    }
}