﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Text;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Text.RegularExpressions;
using System.Data;
using System.Configuration;
using System.Web.Security;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class NotMappingComplianceList : System.Web.UI.Page
    {
        public static List<long?> CompanyTypeList = new List<long?>();
        public static List<long?> BusinessActivityTypeist = new List<long?>();
        public static List<long?> ActApplicability = new List<long?>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                {
                    if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "MGMT")
                    {
                        ViewState["SortOrder"] = "Asc";
                        ViewState["SortExpression"] = "ID";

                        BindCategories();
                        BindActGroup();
                        BindTypes();
                        BindActs();
                        //BindActs(Convert.ToInt32(ddlComplinceCatagory.SelectedValue));
                        BindDueDates();
                        BindFrequencies();
                        BindDueDays();
                        BindFilterFrequencies();
                        BindNatureOfCompliance();
                        BindCompanyType();
                        BindBusinessActivityType();
                        BindAct_Applicability();
                        BindLocationType();
                        BindIndustry();
                        BindEntityType();
                        BindIsForSecretarial();
                        txtIndustry.Attributes.Add("readonly", "readonly");
                        txtEntityType.Attributes.Add("readonly", "readonly");
                        txtIsForSecretarial.Attributes.Add("readonly", "readonly");
                        BindCompliancesNew();
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
                else
                {
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
        }
        private void BindActGroup()
        {
            try
            {
                ddlActGroup.DataTextField = "Name";
                ddlActGroup.DataValueField = "ID";

                ddlActGroup.DataSource = GetActgroupdata();
                ddlActGroup.DataBind();

                ddlActGroup.Items.Insert(0, new ListItem("< Select Act group >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<ActGroupMaster> GetActgroupdata()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ActGroupMasters
                            where row.IsActive == true
                            select row).ToList();
                return data.OrderBy(entry => entry.Name).ToList();
            }
        }
        private void BindNatureOfCompliance()
        {
            try
            {
                ddlNatureOfCompliance.DataSource = Business.ComplianceManagement.GetAllComplianceNature();
                ddlNatureOfCompliance.DataTextField = "Name";
                ddlNatureOfCompliance.DataValueField = "ID";
                ddlNatureOfCompliance.DataBind();

                ddlNatureOfCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

     

        public Boolean ButtonDisplayComplianceActiveOrInActive(string status)
        {
            try
            {
                Boolean result = true;
                if (status == "Active")
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return true;
        }
        protected void chbImprisonment_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                divImprisonmentDetails.Visible = ((CheckBox)sender).Checked;
                if (!divImprisonmentDetails.Visible)
                {
                    tbxDesignation.Text = tbxMinimumYears.Text = tbxMaximumYears.Text = string.Empty;
                    ddlMinimumYear.SelectedValue = ddlMaximumYear.SelectedValue = "-1";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddCompliance_Click(object sender, EventArgs e)
        {
            try
            {
                divSampleForm.Visible = false;
                saveopo.Value = "false";
                ViewState["Mode"] = 0;
                ViewState["ComplianceParameters"] = null;
                lblErrorMassage.Text = string.Empty;
                rdoComplianceVisible.Checked = true;

                ddlComplianceSubType.Items.Clear();

                tbxRequiredForms.Text = tbxDescription.Text = tbxNonComplianceEffects.Text = tbxSections.Text = string.Empty;
                ddlNatureOfCompliance.SelectedIndex = ddlComplianceType.SelectedIndex = ddlAct.SelectedIndex = ddlFrequency.SelectedIndex = ddlDueDate.SelectedIndex = ddlNonComplianceType.SelectedIndex = ddlRiskType.SelectedIndex;
                vivWeekDueDays.Visible = false;
                ddlWeekDueDay.SelectedValue = "-1";

                divComplianceDueDays.Visible = false;
                txtEventDueDate.Text = string.Empty;
                chkDocument.Checked = false;
                isOnline.Checked = false;
                ChkIsMapped.Checked = true;
                ddlNonComplianceType_SelectedIndexChanged(null, null);
             
                ddlComplianceType_SelectedIndexChanged(null, null);

                txtReferenceMaterial.Text = string.Empty;
                rbReminderType.SelectedValue = "0";
                txtReminderBefore.Text = string.Empty;
                txtReminderGap.Text = string.Empty;
                txtPenaltyDescription.Text = string.Empty;
                txtShortDescription.Text = string.Empty;
                txtshortform.Text = string.Empty;
                txtSampleFormLink.Text = string.Empty;
                txtCompliancetag.Text = string.Empty;
                tbxMinimumYears.Text = string.Empty;
                tbxMaximumYears.Text = string.Empty;
                ddlMaximumYear.SelectedValue = "-1";
                ddlMaximumYear.SelectedValue = "-1";
                ddlLocationType.SelectedValue = "-1";
                txtIndustry.Text = "< Select >";
                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                    chkIndustry.Checked = false;
                    CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                    IndustrySelectAll.Checked = false;
                }
                txtEntityType.Text = "< Select >";
                
                foreach (RepeaterItem aItem in rptEntityType.Items)
                {
                    CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");
                    chkEntityType.Checked = false;
                    CheckBox EntityTypeSelectAll = (CheckBox)rptEntityType.Controls[0].Controls[0].FindControl("EntityTypeSelectAll");
                    EntityTypeSelectAll.Checked = false;
                }
                txtCompanyType.Text = "< Select >";
                foreach (RepeaterItem aItem in rptCompanyType.Items)
                {
                    CheckBox chkCompanyType = (CheckBox)aItem.FindControl("chkCompanyType");
                    chkCompanyType.Checked = false;
                    CheckBox CompanyTypeSelectAll = (CheckBox)rptCompanyType.Controls[0].Controls[0].FindControl("CompanyTypeSelectAll");
                    CompanyTypeSelectAll.Checked = false;
                }


                txtBusinessActivityType.Text = "< Select >";
                foreach (RepeaterItem aItem in rptBusinessActivityType.Items)
                {
                    CheckBox chkBusinessActivityType = (CheckBox)aItem.FindControl("chkBusinessActivityType");
                    chkBusinessActivityType.Checked = false;
                    CheckBox BusinessActivityTypeSelectAll = (CheckBox)rptBusinessActivityType.Controls[0].Controls[0].FindControl("BusinessActivityTypeSelectAll");
                    BusinessActivityTypeSelectAll.Checked = false;
                }


                txtActApplicability.Text = "< Select >";
                foreach (RepeaterItem aItem in rptActApplicability.Items)
                {
                    CheckBox chkActApplicability = (CheckBox)aItem.FindControl("chkActApplicability");
                    chkActApplicability.Checked = false;
                    CheckBox ActApplicabilitySelectAll = (CheckBox)rptActApplicability.Controls[0].Controls[0].FindControl("ActApplicabilitySelectAll");
                    ActApplicabilitySelectAll.Checked = false;
                }

                txtIsForSecretarial.Text = "< Select >";

                foreach (RepeaterItem aItem in rptIsForSecretarial.Items)
                {
                    CheckBox chkIsForSecretarial = (CheckBox)aItem.FindControl("chkIsForSecretarial");
                    chkIsForSecretarial.Checked = false;
                    CheckBox IsForSecretarialSelectAll = (CheckBox)rptIsForSecretarial.Controls[0].Controls[0].FindControl("IsForSecretarialSelectAll");
                    IsForSecretarialSelectAll.Checked = false;
                }
               
                tbxOnetimeduedate.Text = string.Empty;
                divForCustome.Visible = false;
                divChecklist.Visible = false;
                divOneTime.Visible = false;
                upComplianceDetails.Update();
                rbReminderType.SelectedValue ="0";
                rbReminderType.Enabled = true;

                tbxStartDate.Text = string.Empty;
                isOnline.Checked = false;
                ChkIsMapped.Checked = true;
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePickerStart", string.Format("initializeDatePickerStart(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog1", "$(\"#divComplianceDetailsDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ChkIsForSecretarial_Changed(object sender, EventArgs e)
        {
            try
            {
                divSecretarial.Visible = ((CheckBox)sender).Checked;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool chkOneTimeValidation = false;
                bool chkexists = false;
                if (ViewState["GComplianceID"] != null)
                {
                    chkexists = Business.ComplianceManagement.ExistsSavedCompliance(tbxDescription.Text.Trim(), txtShortDescription.Text.Trim(), Convert.ToInt32(ddlAct.SelectedValue), Convert.ToInt32(ViewState["GComplianceID"]));
                }
                else
                {
                    chkexists = Business.ComplianceManagement.ExistsR(tbxDescription.Text.Trim(), txtShortDescription.Text.Trim(), Convert.ToInt32(ddlAct.SelectedValue));
                }
                Boolean ComplianceVisible = false;
                if (rdoComplianceVisible.Checked == true)
                {
                    ComplianceVisible = true;
                }
                if (ddlComplianceType.SelectedValue != "1")
                {
                    ComplianceVisible = true;
                }
                Boolean chkIndustryFlag = false;
                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                    if (chkIndustry.Checked)
                    {
                        chkIndustryFlag = true;
                    }
                }
                if (chkIndustryFlag == false)
                {
                    saveopo.Value = "true";
                    cvDuplicateEntry.ErrorMessage = "Please select at least one industry.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }

                Boolean chkLegalEntityFlag = false;
                foreach (RepeaterItem aItem in rptEntityType.Items)
                {
                    CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");
                    if (chkEntityType.Checked)
                    {
                        chkLegalEntityFlag = true;
                    }
                }

                if (chkLegalEntityFlag == false)
                {
                    saveopo.Value = "true";
                    cvDuplicateEntry.ErrorMessage = "Please select at least one Legal entity type.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }
                //if (ddlMinimumYear.SelectedItem.Text == "Year" && ddlMaximumYear.SelectedItem.Text == "Month")
                //{
                //    int? minvalue = null;
                //    int? maxvalue = null;
                //    string result = string.Empty;
                //    minvalue = Convert.ToInt32(tbxMinimumYears.Text);
                //    minvalue = minvalue * 12;
                //    maxvalue = Convert.ToInt32(tbxMaximumYears.Text);
                //    if (minvalue >= maxvalue)
                //    {
                //        saveopo.Value = "true";
                //        cvDuplicateEntry.ErrorMessage = "Please add maximum month greater than" + " " + minvalue;
                //        cvDuplicateEntry.IsValid = false;
                //        return;
                //    }
                //}
                //else if (ddlMinimumYear.SelectedItem.Text == "Month" && ddlMaximumYear.SelectedItem.Text == "Year")
                //{
                //    int? minvalue = null;
                //    int? maxvalue = null;
                //    string result = string.Empty;
                //    maxvalue = Convert.ToInt32(tbxMaximumYears.Text);
                //    minvalue = Convert.ToInt32(tbxMinimumYears.Text);
                //    maxvalue = maxvalue * 12;

                //    if (minvalue >= maxvalue)
                //    {
                //        saveopo.Value = "true";
                //        cvDuplicateEntry.ErrorMessage = "Please add maximum year greater than" + " " + minvalue;
                //        cvDuplicateEntry.IsValid = false;
                //        return;
                //    }
                //}
                //else if (ddlMinimumYear.SelectedItem.Text == "Month" && ddlMaximumYear.SelectedItem.Text == "Month")
                //{
                //    int? minvalue = null;
                //    int? maxvalue = null;
                //    string result = string.Empty;
                //    maxvalue = Convert.ToInt32(tbxMaximumYears.Text);
                //    minvalue = Convert.ToInt32(tbxMinimumYears.Text);
                //    if (maxvalue <= minvalue)
                //    {
                //        saveopo.Value = "true";
                //        cvDuplicateEntry.ErrorMessage = "Please add maximum month greater than" + " " + minvalue;
                //        cvDuplicateEntry.IsValid = false;
                //        return;
                //    }
                //}

                if (ddlComplianceType.SelectedValue == "3")
                {
                    if (string.IsNullOrEmpty(txtReminderBefore.Text))
                    {
                        cvDuplicateEntry.ErrorMessage = "Please Enter Before(In Days).";
                        cvDuplicateEntry.IsValid = false;
                        chkexists= true;
                        chkOneTimeValidation = true;
                    }
                    if (string.IsNullOrEmpty(txtReminderGap.Text))
                    {
                        cvDuplicateEntry.ErrorMessage = "Please Enter Gap(In Days).";
                        cvDuplicateEntry.IsValid = false;
                        chkexists = true;
                        chkOneTimeValidation = true;
                    }
                }
                int? DueDateType=null;
                if(isOnline.Checked==false)
                {
                    DueDateType = Convert.ToInt32(ddlDueDateType.SelectedValue);
                }
                int LocationTypeID = -1;
                if (!string.IsNullOrEmpty(ddlLocationType.SelectedValue))
                {
                    if (ddlLocationType.SelectedValue != "-1")
                    {
                        LocationTypeID = Convert.ToInt32(ddlLocationType.SelectedValue);
                    }
                }
                if (chkexists == false)
                {
                    Business.Data.Compliance compliance = new Business.Data.Compliance()
                    {
                        ActID = Convert.ToInt32(ddlAct.SelectedValue),
                        Description = tbxDescription.Text,
                        Sections = tbxSections.Text,
                        ShortDescription = txtShortDescription.Text,
                        ShortForm = txtshortform.Text,
                        UploadDocument = chkDocument.Checked,
                        ComplianceType = Convert.ToByte(ddlComplianceType.SelectedValue),
                        RequiredForms = tbxRequiredForms.Text,
                        SampleFormLink = txtSampleFormLink.Text,
                        RiskType = Convert.ToByte(ddlRiskType.SelectedValue),
                        PenaltyDescription = txtPenaltyDescription.Text,
                        ReferenceMaterialText = txtReferenceMaterial.Text,
                        CreatedBy = AuthenticationHelper.UserID,
                        UpdatedOn = DateTime.Now,
                        ComplinceVisible = ComplianceVisible,
                        onlineoffline = isOnline.Checked,
                        duedatetype = DueDateType,
                        LocationTypeID= LocationTypeID,
                        IsForSecretarial = false,
                        ScheduleType = Convert.ToInt32(ddlScheduleType.SelectedValue),
                        IsMapped = ChkIsMapped.Checked,
                    };
                    if (ChkIsForSecretarial.Checked)
                    {
                        compliance.IsForSecretarial = Convert.ToBoolean(true);
                    }
                    if (!string.IsNullOrEmpty(tbxStartDate.Text))
                        compliance.StartDate = DateTime.ParseExact(Convert.ToString(tbxStartDate.Text).Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    if (compliance.ComplianceType == 0 || compliance.ComplianceType == 2 || compliance.ComplianceType == 3)
                    {
                        #region // 0(function based) , 2(time based) and 3(one time)
                        if (ddlNatureOfCompliance.SelectedValue != "")
                        {
                            if (ddlNatureOfCompliance.SelectedValue != "-1")
                            {
                                compliance.NatureOfCompliance = Convert.ToByte(ddlNatureOfCompliance.SelectedValue);
                            }
                        }
                        if (ddlComplianceSubType.SelectedValue != "")
                        {
                            if (ddlComplianceSubType.SelectedValue != "-1")
                            {
                                compliance.ComplianceSubTypeID = Convert.ToInt32(ddlComplianceSubType.SelectedValue);
                            }
                        }
                        if (compliance.ComplianceType == 3)//One Time
                        {
                            string Onetimedate = Request[tbxOnetimeduedate.UniqueID].ToString().Trim();
                            DateTime dtOneTimeDate = new DateTime();
                            if (Onetimedate != "")
                            {
                                dtOneTimeDate = DateTime.ParseExact(Onetimedate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                compliance.OneTimeDate = dtOneTimeDate;
                            }
                        }
                        else
                        {
                            compliance.OneTimeDate = null;
                        }
                        if (compliance.ComplianceType == 2)//time based
                        {
                            if (rbTimebasedTypes.SelectedValue.Equals("0"))//fixed gap
                            {
                                compliance.EventID = null;
                                compliance.Frequency = null;
                                compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text);
                                compliance.SubComplianceType = 0;
                            }
                            else if (rbTimebasedTypes.SelectedValue.Equals("1"))//periodicallly based
                            {
                                compliance.EventID = null;
                                compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                if(compliance.Frequency == 8)
                                {
                                    compliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue); 
                                }
                                else
                                {
                                    compliance.DueDate = null;
                                }
                                compliance.SubComplianceType = 1;
                            }
                            else
                            {
                                compliance.EventID = null;
                                compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                if (compliance.Frequency == 8)
                                {
                                    compliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                                }
                                else
                                {
                                    compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text); ;
                                }
                                compliance.SubComplianceType = 2;
                            }
                        }
                        if (compliance.ComplianceType == 0)//Function Based
                        {
                            compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                            if (compliance.Frequency == 8)
                            {
                                compliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                            }
                            else
                            {
                                compliance.DueDate = Convert.ToInt32(ddlDueDate.SelectedValue);
                            }
                        }
                        compliance.NonComplianceType = ddlNonComplianceType.SelectedValue == "-1" ? (byte?) null : Convert.ToByte(ddlNonComplianceType.SelectedValue);
                        compliance.NonComplianceEffects = tbxNonComplianceEffects.Text;
                        compliance.FixedMinimum = tbxFixedMinimum.Text.Length > 0 ? Convert.ToDouble(tbxFixedMinimum.Text) : (double?) null;
                        compliance.FixedMaximum = tbxFixedMaximum.Text.Length > 0 ? Convert.ToDouble(tbxFixedMaximum.Text) : (double?) null;
                        if (ddlPerDayMonth.SelectedValue == "0")
                        {
                            compliance.VariableAmountPerDay = tbxVariableAmountPerDay.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPerDay.Text) : (double?)null;
                            compliance.VariableAmountPerMonth = null;
                            compliance.VariableAmountPerInstance = null;
                        }
                        else if (ddlPerDayMonth.SelectedValue == "1")
                        {
                            compliance.VariableAmountPerMonth = tbxVariableAmountPerDay.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPerDay.Text) : (double?)null;
                            compliance.VariableAmountPerDay = null;
                            compliance.VariableAmountPerInstance = null;
                        }
                        else
                        {
                            compliance.VariableAmountPerInstance = tbxVariableAmountPerDay.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPerDay.Text) : (double?)null;
                            compliance.VariableAmountPerDay = null;
                            compliance.VariableAmountPerMonth = null;
                        }
                        compliance.VariableAmountPerDayMax = tbxVariableAmountPerDayMax.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPerDayMax.Text) : (double?) null;
                        //compliance.VariableAmountPercent = tbxVariableAmountPercent.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPercent.Text) : (double?) null;
                        //compliance.VariableAmountPercentMax = tbxVariableAmountPercentMaximum.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPercentMaximum.Text) : (double?) null;
                        compliance.VariableAmountPercent = Convert.ToString(tbxVariableAmountPercent.Text);
                        compliance.VariableAmountPercentMax =Convert.ToString(tbxVariableAmountPercentMaximum.Text);

                        compliance.Imprisonment = chbImprisonment.Checked;
                        compliance.Designation = tbxDesignation.Text;
                        compliance.MinimumYears = tbxMinimumYears.Text.Length > 0 ? Convert.ToInt32(tbxMinimumYears.Text) : (int?) null;
                        compliance.MaximumYears = tbxMaximumYears.Text.Length > 0 ? Convert.ToInt32(tbxMaximumYears.Text) : (int?) null;

                        compliance.MinimumPeriodType = ddlMinimumYear.SelectedValue == "-1" ? (int?)null : Convert.ToByte(ddlMinimumYear.SelectedValue);
                        compliance.MaximumPeriodType = ddlMaximumYear.SelectedValue == "-1" ? (int?)null : Convert.ToByte(ddlMaximumYear.SelectedValue);

                        #endregion
                    }
                    else
                    {
                        #region CheckList
                        compliance.CheckListTypeID = Convert.ToInt32(ddlChklstType.SelectedValue);

                        if (ddlNatureOfCompliance.SelectedValue != "")
                        {
                            if (ddlNatureOfCompliance.SelectedValue != "-1")
                            {
                                compliance.NatureOfCompliance = Convert.ToByte(ddlNatureOfCompliance.SelectedValue);
                            }
                        }
                        if (ddlComplianceSubType.SelectedValue != "")
                        {
                            if (ddlComplianceSubType.SelectedValue != "-1")
                            {
                                compliance.ComplianceSubTypeID = Convert.ToInt32(ddlComplianceSubType.SelectedValue);
                            }
                        }

                        //if (chkEventBased.Checked == true)
                        //{
                        //    if (tbxSubEvent.Text.Equals("< Select Sub Event >"))
                        //    {
                        //        compliance.EventID = Convert.ToInt64(ddlEvents.SelectedValue);
                        //    }
                        //    else
                        //    {
                        //        compliance.EventID = Convert.ToInt64(ddlEvents.SelectedValue);
                        //        compliance.SubEventID = Convert.ToInt64(tvSubEvent.SelectedNode.Value);
                        //    }
                        //    compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text);
                        //}
                        compliance.NatureOfCompliance = null;
                        compliance.NonComplianceType = null;
                        compliance.NonComplianceEffects = null;
                        //------checklist------
                        if (compliance.CheckListTypeID == 1 || compliance.CheckListTypeID == 2)//function based or time based
                        {
                            if (compliance.CheckListTypeID == 2)//time based
                            {
                                if (rbTimebasedTypes.SelectedValue.Equals("0"))//fixed gap
                                {
                                    compliance.EventID = null;
                                    compliance.Frequency = null;
                                    compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text);
                                    compliance.SubComplianceType = 0;
                                }
                                else if (rbTimebasedTypes.SelectedValue.Equals("1"))//periodicallly based
                                {
                                    compliance.EventID = null;
                                    compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                    if (compliance.Frequency == 8)
                                    {
                                        compliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                                    }
                                    else
                                    {
                                        compliance.DueDate = null;
                                    }
                                    compliance.SubComplianceType = 1;
                                }
                                else
                                {
                                    compliance.EventID = null;
                                    compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                    if (compliance.Frequency == 8)
                                    {
                                        compliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                                    }
                                    else
                                    {
                                        compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text); ;
                                    }
                                    compliance.SubComplianceType = 2;
                                }
                            }
                            else
                            {
                                //if (chkEventBased.Checked == true)
                                //{
                                //    compliance.Frequency = null;
                                //    if (tbxSubEvent.Text.Equals("< Select Sub Event >"))
                                //    {
                                //        compliance.EventID = Convert.ToInt64(ddlEvents.SelectedValue);
                                //    }
                                //    else
                                //    {
                                //        compliance.EventID = Convert.ToInt64(ddlEvents.SelectedValue);
                                //        compliance.SubEventID = Convert.ToInt64(tvSubEvent.SelectedNode.Value);
                                //    }
                                //    compliance.DueDate = Convert.ToInt32(txtEventDueDate.Text);
                                //    compliance.EventComplianceType = Convert.ToByte(rbEventComplianceType.SelectedValue);
                                //}
                                //else
                                //{
                                    compliance.EventID = null;
                                    compliance.Frequency = Convert.ToByte(ddlFrequency.SelectedValue);
                                    if (compliance.Frequency == 8)
                                    {
                                        compliance.DueWeekDay = Convert.ToByte(ddlWeekDueDay.SelectedValue);
                                    }
                                    else
                                    {
                                        compliance.DueDate = Convert.ToInt32(ddlDueDate.SelectedValue);
                                    }
                                //}
                            }
                        }
                        else if (compliance.CheckListTypeID == 0 && compliance.ComplinceVisible==true )//One time 
                        {
                            string Onetimedate = Request[tbxOnetimeduedate.UniqueID].ToString().Trim();
                            DateTime dtOneTimeDate = new DateTime();
                            if (Onetimedate != "")
                            {
                                dtOneTimeDate = DateTime.ParseExact(Onetimedate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                compliance.OneTimeDate = dtOneTimeDate;
                            }
                        }


                        compliance.NonComplianceType = ddlNonComplianceType.SelectedValue == "-1" ? (byte?)null : Convert.ToByte(ddlNonComplianceType.SelectedValue);
                        compliance.NonComplianceEffects = tbxNonComplianceEffects.Text;
                        compliance.FixedMinimum = tbxFixedMinimum.Text.Length > 0 ? Convert.ToDouble(tbxFixedMinimum.Text) : (double?)null;
                        compliance.FixedMaximum = tbxFixedMaximum.Text.Length > 0 ? Convert.ToDouble(tbxFixedMaximum.Text) : (double?)null;
                        if (ddlPerDayMonth.SelectedValue == "0")
                        {
                            compliance.VariableAmountPerDay = tbxVariableAmountPerDay.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPerDay.Text) : (double?)null;
                            compliance.VariableAmountPerMonth = null;
                            compliance.VariableAmountPerInstance = null;
                        }
                        else if (ddlPerDayMonth.SelectedValue == "1")
                        {
                            compliance.VariableAmountPerMonth = tbxVariableAmountPerDay.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPerDay.Text) : (double?)null;
                            compliance.VariableAmountPerDay = null;
                            compliance.VariableAmountPerInstance = null;
                        }
                        else
                        {
                            compliance.VariableAmountPerInstance = tbxVariableAmountPerDay.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPerDay.Text) : (double?)null;
                            compliance.VariableAmountPerDay = null;
                            compliance.VariableAmountPerMonth = null;
                        }
                        compliance.VariableAmountPerDayMax = tbxVariableAmountPerDayMax.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPerDayMax.Text) : (double?)null;
                        //compliance.VariableAmountPercent = tbxVariableAmountPercent.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPercent.Text) : (double?)null;
                        //compliance.VariableAmountPercentMax = tbxVariableAmountPercentMaximum.Text.Length > 0 ? Convert.ToDouble(tbxVariableAmountPercentMaximum.Text) : (double?)null;
                        compliance.VariableAmountPercent = Convert.ToString(tbxVariableAmountPercent.Text);
                        compliance.VariableAmountPercentMax = Convert.ToString(tbxVariableAmountPercentMaximum.Text);

                        compliance.Imprisonment = chbImprisonment.Checked;
                        compliance.Designation = tbxDesignation.Text;
                        compliance.MinimumYears = tbxMinimumYears.Text.Length > 0 ? Convert.ToInt32(tbxMinimumYears.Text) : (int?)null;
                        compliance.MaximumYears = tbxMaximumYears.Text.Length > 0 ? Convert.ToInt32(tbxMaximumYears.Text) : (int?)null;

                        compliance.MinimumPeriodType = ddlMinimumYear.SelectedValue == "-1" ? (int?)null : Convert.ToByte(ddlMinimumYear.SelectedValue);
                        compliance.MaximumPeriodType = ddlMaximumYear.SelectedValue == "-1" ? (int?)null : Convert.ToByte(ddlMaximumYear.SelectedValue);


                        #endregion
                    }

                    ComplianceForm form = null;
                    List<ComplianceForm> ComplianceForms = new List<ComplianceForm>();
                    if (chkDocument.Checked)
                    {
                        if (fuSampleFile.PostedFiles.Count() > 0)                      
                        {
                            HttpFileCollection fileCollection = Request.Files;
                            if (fileCollection.Count > 0)
                            {
                                for (int i = 0; i < fileCollection.Count; i++)
                                {
                                    HttpPostedFile uploadfile = null;
                                    uploadfile = fileCollection[i];
                                    string fileName = uploadfile.FileName;
                                    if (!string.IsNullOrEmpty(fileName))
                                    {
                                        fileCollection[i].SaveAs(Server.MapPath("~/ComplianceFiles/" + fileName));
                                        Stream fs = uploadfile.InputStream;
                                        BinaryReader br = new BinaryReader(fs);
                                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                        form = new ComplianceForm()
                                        {
                                            Name = fileName,
                                            FileData = bytes,
                                            FilePath = "~/ComplianceFiles/" + fileName,
                                        };
                                        ComplianceForms.Add(form);
                                    }
                                }
                            }
                        }
                    }

                    compliance.ReminderType = Convert.ToByte(rbReminderType.SelectedValue);
                    if (rbReminderType.SelectedValue.Equals("1"))
                    {
                        compliance.ReminderBefore = Convert.ToInt32(txtReminderBefore.Text);
                        compliance.ReminderGap = Convert.ToInt32(txtReminderGap.Text);
                    }
                    List<ComplianceParameter> parameters = new List<ComplianceParameter>(); //GetComplianceParameters();
                    if ((int) ViewState["Mode"] == 1)
                    {
                        compliance.ID = Convert.ToInt32(ViewState["ComplianceID"]);
                    }
                    if ((int) ViewState["Mode"] == 0)
                    {
                        Business.ComplianceManagement.CreateMultiple(compliance, ComplianceForms);
                        // Add compliance new filed in  ComplianceDetail table SACHIN 04 Aug 2017
                        ComplianceDetail compliancedetail = new ComplianceDetail()
                        {
                            ComplianceID = compliance.ID,
                            UpdatedBy = AuthenticationHelper.UserID,
                        };
                        Business.ComplianceManagement.CreateComplianceDetail(compliancedetail);

                        //---------add Industry--------------------------------------------
                        List<int> IndustryIds = new List<int>();
                        foreach (RepeaterItem aItem in rptIndustry.Items)
                        {
                            CheckBox chkIndustry = (CheckBox) aItem.FindControl("chkIndustry");
                            if (chkIndustry.Checked)
                            {
                                IndustryIds.Add(Convert.ToInt32(((Label) aItem.FindControl("lblIndustryID")).Text.Trim()));
                                IndustryMapping IndustryMapping = new IndustryMapping()
                                {
                                    ComplianceId = compliance.ID,
                                    IndustryID = Convert.ToInt32(((Label) aItem.FindControl("lblIndustryID")).Text.Trim()),
                                    IsActive = true,
                                    EditedDate = DateTime.UtcNow,
                                    EditedBy = Convert.ToInt32(Session["userID"]),
                                };
                                Business.ComplianceManagement.CreateIndustryMapping(IndustryMapping);
                            }
                        }

                        //---------add Legal Entity type--------------------------------------------
                        List<int> EntityTypeIds = new List<int>();
                        foreach (RepeaterItem aItem in rptEntityType.Items)
                        {
                            CheckBox chkEntityType = (CheckBox) aItem.FindControl("chkEntityType");
                            if (chkEntityType.Checked)
                            {
                                EntityTypeIds.Add(Convert.ToInt32(((Label) aItem.FindControl("lblEntityTypeID")).Text.Trim()));
                                LegalEntityTypeMapping LegalEntityTypeMapping = new LegalEntityTypeMapping()
                                {
                                    ComplianceId = compliance.ID,
                                    LegalEntityTypeID = Convert.ToInt32(((Label) aItem.FindControl("lblEntityTypeID")).Text.Trim()),
                                    IsActive = true,
                                    EditedDate = DateTime.UtcNow,
                                    EditedBy = Convert.ToInt32(Session["userID"]),
                                };
                                Business.ComplianceManagement.CreateLegalEntityMapping(LegalEntityTypeMapping);
                            }
                        }
                        //----------------add IsForSecretarial----------------------------------------
                        List<int> IsForSecretarialIds = new List<int>();
                        foreach (RepeaterItem aItem in rptIsForSecretarial.Items)
                        {
                            CheckBox chkIsForSecretarial = (CheckBox)aItem.FindControl("chkIsForSecretarial");
                            if (chkIsForSecretarial.Checked)
                            {
                                IsForSecretarialIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblIsForSecretarialID")).Text.Trim()));
                                Compliance_SecretarialTagMapping SecretarialTagMapping = new Compliance_SecretarialTagMapping()
                                {
                                    ComplianceID = compliance.ID,
                                    TagID = Convert.ToInt32(((Label)aItem.FindControl("lblIsForSecretarialID")).Text.Trim()),
                                    IsActive = true,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                    CreatedOn = DateTime.UtcNow,
                                };
                                Business.ComplianceManagement.CreateSecreterialMapping(SecretarialTagMapping);
                            }
                        }

                        #region add CompanyType                            
                        List<Compliance_CompanyTypeMapping> mappingCompanyTypelist = new List<Compliance_CompanyTypeMapping>();
                        foreach (RepeaterItem aItem in rptCompanyType.Items)
                        {
                            CheckBox chkCompanyType = (CheckBox)aItem.FindControl("chkCompanyType");

                            if (chkCompanyType.Checked)
                            {
                                Compliance_CompanyTypeMapping mappingCompanyType = new Compliance_CompanyTypeMapping()
                                {
                                    ComplianceID = compliance.ID,
                                    TypeID = Convert.ToInt32(((Label)aItem.FindControl("lblCompanyTypeID")).Text.Trim()),
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                };
                                mappingCompanyTypelist.Add(mappingCompanyType);

                            }
                        }
                        ActManagement.CreateOrUpdateCompliance_CompanyTypeMapping(mappingCompanyTypelist, compliance.ID, false);
                        #endregion

                        #region add Business Activity                              
                        List<Compliance_BusinessActivityMapping> mappingBusinessActivitylist = new List<Compliance_BusinessActivityMapping>();

                        foreach (RepeaterItem aItem in rptBusinessActivityType.Items)
                        {
                            CheckBox chkBusinessActivityType = (CheckBox)aItem.FindControl("chkBusinessActivityType");

                            if (chkBusinessActivityType.Checked)
                            {
                                Compliance_BusinessActivityMapping mappingBusinessActivity = new Compliance_BusinessActivityMapping()
                                {
                                    ComplianceID = compliance.ID,
                                    BATypeID = Convert.ToInt32(((Label)aItem.FindControl("lblBusinessActivityTypeID")).Text.Trim()),
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                };
                                mappingBusinessActivitylist.Add(mappingBusinessActivity);

                            }
                        }

                        ActManagement.CreateOrUpdateCompliance_BusinessActivityMappingMapping(mappingBusinessActivitylist, compliance.ID, false);
                        #endregion

                        #region add Act Applicability                              
                        List<Compliance_ActApplicabilityMapping> mappingActApplicabilitylist = new List<Compliance_ActApplicabilityMapping>();

                        foreach (RepeaterItem aItem in rptActApplicability.Items)
                        {
                            CheckBox chkActApplicability = (CheckBox)aItem.FindControl("chkActApplicability");

                            if (chkActApplicability.Checked)
                            {
                                Compliance_ActApplicabilityMapping mappingActApplicability = new Compliance_ActApplicabilityMapping()
                                {
                                    ComplianceID = compliance.ID,
                                    Act_ApplicabilityID = Convert.ToInt32(((Label)aItem.FindControl("lblActApplicabilityID")).Text.Trim()),
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                };
                                mappingActApplicabilitylist.Add(mappingActApplicability);

                            }
                        }

                        ActManagement.CreateOrUpdateCompliance_ActApplicabilityMappingMapping(mappingActApplicabilitylist, compliance.ID, false);
                        #endregion

                    }
                    else if ((int) ViewState["Mode"] == 1)
                    {
                        bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
                        if (chkexistsComplianceschedule == true)
                        {                            
                            Business.ComplianceManagement.UpdateIFInstanceIsCreatedCompliance(compliance, ComplianceForms);                            
                        }
                        else
                        {                            
                            Business.ComplianceManagement.Update(compliance, ComplianceForms);
                        }
                        bool chkexistsComplianceDetail = Business.ComplianceManagement.ExistsComplianceDetail(Convert.ToInt64(ViewState["ComplianceID"]));
                        if (chkexistsComplianceDetail == false)
                        {
                            ComplianceDetail compliancedetail = new ComplianceDetail()
                            {
                                ComplianceID = compliance.ID,
                                UpdatedBy = AuthenticationHelper.UserID,
                            };
                            Business.ComplianceManagement.CreateComplianceDetail(compliancedetail);
                        }
                        else
                        {
                            Business.ComplianceManagement.UpdateComplianceDetail(compliance.ID, AuthenticationHelper.UserID);
                        }
                        //---------add Industry--------------------------------------------
                        List<int> IndustryIds = new List<int>();
                        Business.ComplianceManagement.UpdateIndustryMappedID(compliance.ID);
                        foreach (RepeaterItem aItem in rptIndustry.Items)
                        {
                            CheckBox chkIndustry = (CheckBox) aItem.FindControl("chkIndustry");
                            if (chkIndustry.Checked)
                            {
                                IndustryIds.Add(Convert.ToInt32(((Label) aItem.FindControl("lblIndustryID")).Text.Trim()));
                                IndustryMapping IndustryMapping = new IndustryMapping()
                                {
                                    ComplianceId = compliance.ID,
                                    IndustryID = Convert.ToInt32(((Label) aItem.FindControl("lblIndustryID")).Text.Trim()),
                                    IsActive = true,
                                    EditedDate = DateTime.UtcNow,
                                    EditedBy = Convert.ToInt32(Session["userID"]),
                                };

                                Business.ComplianceManagement.CreateIndustryMapping(IndustryMapping);
                            }
                        }

                        //---------add Legal Entity type--------------------------------------------
                        List<int> EntityTypeIds = new List<int>();
                        Business.ComplinaceManagementNew.UpdateLegalEntityMappedID(compliance.ID);
                        foreach (RepeaterItem aItem in rptEntityType.Items)
                        {
                            CheckBox chkEntityType = (CheckBox) aItem.FindControl("chkEntityType");
                            if (chkEntityType.Checked)
                            {
                                EntityTypeIds.Add(Convert.ToInt32(((Label) aItem.FindControl("lblEntityTypeID")).Text.Trim()));

                                LegalEntityTypeMapping LegalEntityTypeMapping = new LegalEntityTypeMapping()
                                {
                                    ComplianceId = compliance.ID,
                                    LegalEntityTypeID = Convert.ToInt32(((Label) aItem.FindControl("lblEntityTypeID")).Text.Trim()),

                                    IsActive = true,
                                    EditedDate = DateTime.UtcNow,
                                    EditedBy = Convert.ToInt32(Session["userID"]),

                                };
                                Business.ComplianceManagement.CreateLegalEntityMapping(LegalEntityTypeMapping);
                            }
                        }

                        //---------add Secreterial--------------------------------------------
                        List<int> IsForSecretarialIds = new List<int>();
                        Business.ComplinaceManagementNew.UpdateSecretarialMappedID(compliance.ID);
                        foreach (RepeaterItem aItem in rptIsForSecretarial.Items)
                        {
                            CheckBox chkIsForSecretarial = (CheckBox)aItem.FindControl("chkIsForSecretarial");
                            if (chkIsForSecretarial.Checked)
                            {
                                IsForSecretarialIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblIsForSecretarialID")).Text.Trim()));
                                Compliance_SecretarialTagMapping SecretarialTagMapping = new Compliance_SecretarialTagMapping()
                                {
                                    ComplianceID = compliance.ID,
                                    TagID = Convert.ToInt32(((Label)aItem.FindControl("lblIsForSecretarialID")).Text.Trim()),
                                    IsActive = true,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                    CreatedOn = DateTime.UtcNow,
                                    UpdatedBy = Convert.ToInt32(Session["userID"]),
                                    UpdatedOn = DateTime.UtcNow,

                                };
                                Business.ComplianceManagement.CreateSecreterialMapping(SecretarialTagMapping);
                            }
                        }

                        #region add CompanyType                            
                        List<Compliance_CompanyTypeMapping> mappingCompanyTypelist = new List<Compliance_CompanyTypeMapping>();
                        foreach (RepeaterItem aItem in rptCompanyType.Items)
                        {
                            CheckBox chkCompanyType = (CheckBox)aItem.FindControl("chkCompanyType");

                            if (chkCompanyType.Checked)
                            {
                                Compliance_CompanyTypeMapping mappingCompanyType = new Compliance_CompanyTypeMapping()
                                {
                                    ComplianceID = compliance.ID,
                                    TypeID = Convert.ToInt32(((Label)aItem.FindControl("lblCompanyTypeID")).Text.Trim()),
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                };
                                mappingCompanyTypelist.Add(mappingCompanyType);

                            }
                        }
                        ActManagement.CreateOrUpdateCompliance_CompanyTypeMapping(mappingCompanyTypelist, compliance.ID, true);
                        #endregion

                        #region add Business Activity                              
                        List<Compliance_BusinessActivityMapping> mappingBusinessActivitylist = new List<Compliance_BusinessActivityMapping>();

                        foreach (RepeaterItem aItem in rptBusinessActivityType.Items)
                        {
                            CheckBox chkBusinessActivityType = (CheckBox)aItem.FindControl("chkBusinessActivityType");

                            if (chkBusinessActivityType.Checked)
                            {
                                Compliance_BusinessActivityMapping mappingBusinessActivity = new Compliance_BusinessActivityMapping()
                                {
                                    ComplianceID = compliance.ID,
                                    BATypeID = Convert.ToInt32(((Label)aItem.FindControl("lblBusinessActivityTypeID")).Text.Trim()),
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                };
                                mappingBusinessActivitylist.Add(mappingBusinessActivity);

                            }
                        }

                        ActManagement.CreateOrUpdateCompliance_BusinessActivityMappingMapping(mappingBusinessActivitylist, compliance.ID, true);
                        #endregion

                        #region add Act Applicability                              
                        List<Compliance_ActApplicabilityMapping> mappingActApplicabilitylist = new List<Compliance_ActApplicabilityMapping>();

                        foreach (RepeaterItem aItem in rptActApplicability.Items)
                        {
                            CheckBox chkActApplicability = (CheckBox)aItem.FindControl("chkActApplicability");

                            if (chkActApplicability.Checked)
                            {
                                Compliance_ActApplicabilityMapping mappingActApplicability = new Compliance_ActApplicabilityMapping()
                                {
                                    ComplianceID = compliance.ID,
                                    Act_ApplicabilityID = Convert.ToInt32(((Label)aItem.FindControl("lblActApplicabilityID")).Text.Trim()),
                                    IsActive = true,
                                    CreatedDate = DateTime.Now,
                                    CreatedBy = Convert.ToInt32(Session["userID"]),
                                };
                                mappingActApplicabilitylist.Add(mappingActApplicability);

                            }
                        }

                        ActManagement.CreateOrUpdateCompliance_ActApplicabilityMappingMapping(mappingActApplicabilitylist, compliance.ID, true);
                        #endregion
                    }

                    Business.ComplianceManagement.UpdateComplianceTagMappedID(compliance.ID);

                    if (txtCompliancetag.Text != "")
                    {
                        string[] arrFileTags = txtCompliancetag.Text.Split(',');

                        if (arrFileTags.Length > 0)
                        {
                            List<ComplianceDatatagMapping> lstFiletagdataMapping = new List<ComplianceDatatagMapping>();

                            for (int j = 0; j < arrFileTags.Length; j++)
                            {
                                ComplianceDatatagMapping objFileTagMapping = new ComplianceDatatagMapping()
                                {
                                    ComplianceID = compliance.ID,
                                    FileTag = arrFileTags[j].Trim(),
                                    IsActive = true,
                                    CreatedOn = DateTime.Now,
                                    UpdatedOn = DateTime.Now,
                                    DocType = "S"
                                };

                                lstFiletagdataMapping.Add(objFileTagMapping);
                            }

                            if (lstFiletagdataMapping.Count > 0)
                            {
                                Business.ComplianceManagement.CreateUpdate_ComplianceFileTagsMappingData(lstFiletagdataMapping);
                            }
                        }
                    }
                    //BindCompliancesNew();
                    var complianceForm = Business.ComplianceManagement.GetMultipleComplianceFormByID(compliance.ID);

                    if (complianceForm.Count > 0)
                    {
                        hdnFile.Value = Convert.ToString(compliance.ID);
                        divSampleForm.Visible = true;
                        grdSampleForm.DataSource = complianceForm;
                        grdSampleForm.DataBind();
                    }
                    else
                    {
                        divSampleForm.Visible = false;
                    }
                    upCompliancesList.Update();
                    saveopo.Value = "true";
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Record Saved Sucessfully.";
                }
                else
                {
                    if (chkOneTimeValidation)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please enter reminder Before(In Days) and Gap(In Days).";
                        saveopo.Value = "true";
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Compliance Short Description and Detailed Description compliance allready present in System";
                        saveopo.Value = "true";
                    }
                   
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void OpenScheduleInformation(Business.Data.Compliance compliance)
        {
            try
            {
                saveopo.Value = "true";
                ViewState["ComplianceID"] = compliance.ID;
                ViewState["Frequency"] = compliance.Frequency.Value;
                if (compliance.DueDate != null)
                {
                    ViewState["Day"] = compliance.DueDate.Value;
                }

                if ((compliance.Frequency.Value == 0 || compliance.Frequency.Value == 1))
                    divStartMonth.Visible = false;
                else
                    divStartMonth.Visible = true;

                var scheduleList = Business.ComplianceManagement.GetScheduleByComplianceID(compliance.ID);
                if (scheduleList.Count == 0)
                {
                    Business.ComplianceManagement.GenerateDefaultScheduleForComplianceID(compliance.ID, compliance.SubComplianceType);
                    scheduleList = Business.ComplianceManagement.GetScheduleByComplianceID(compliance.ID);
                }

                int step = 0;
                if (compliance.Frequency.Value == 0)
                    step = 0;
                else if (compliance.Frequency.Value == 1)
                    step = 2;
                else if (compliance.Frequency.Value == 2)
                    step = 5;
                else if (compliance.Frequency.Value == 4)
                    step = 3;
                else
                    step = 11;

                var dataSource = scheduleList.Select(entry => new
                {
                    ID = entry.ID,
                    ForMonth = entry.ForMonth,
                    ForMonthName = compliance.Frequency.Value == 0 ? ((Month)entry.ForMonth).ToString() : ((Month)entry.ForMonth).ToString() + " - " + ((Month)((entry.ForMonth + step) > 12 ? (entry.ForMonth + step) - 12 : (entry.ForMonth + step))).ToString(),
                    SpecialDay = Convert.ToByte(entry.SpecialDate.Substring(0, 2)),
                    SpecialMonth = Convert.ToByte(entry.SpecialDate.Substring(2, 2))
                }).ToList();

                if (divStartMonth.Visible)
                {
                    if (compliance.SubComplianceType == 1)
                    {
                        if (dataSource.First().ForMonth == 6 || dataSource.First().ForMonth == 3)
                            ddlStartMonth.SelectedValue = Convert.ToString(1);
                        else
                            ddlStartMonth.SelectedValue = Convert.ToString(4);
                    }
                    else
                    {
                        ddlStartMonth.SelectedValue = dataSource.First().ForMonth.ToString();
                    }
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();

                upSchedulerRepeter.Update();
                upComplianceScheduleDialog.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenScheduleDialog", "$(\"#divComplianceScheduleDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                saveopo.Value = "true";
                var dataSource = new List<object>();
                for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                {
                    RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
                    HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
                    DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");


                    dataSource.Add(new
                    {
                        ID = hdnID.Value,
                        ForMonth = Convert.ToByte(hdnForMonth.Value),
                        ForMonthName = ((Month)Convert.ToByte(hdnForMonth.Value)).ToString(),
                        SpecialDay = ddlDays.SelectedValue,
                        SpecialMonth = ddlMonths.SelectedValue
                    });

                    Month month = (Month)Convert.ToInt32(ddlMonths.SelectedValue);
                    int totalDays = 0;
                    switch (month)
                    {
                        case Month.February:
                            totalDays = 28;
                            break;
                        case Month.January:
                        case Month.March:
                        case Month.May:
                        case Month.July:
                        case Month.August:
                        case Month.October:
                        case Month.December:
                            totalDays = 31;
                            break;
                        case Month.April:
                        case Month.June:
                        case Month.September:
                        case Month.November:
                            totalDays = 30;
                            break;
                    }

                    var daysdataSource = new List<object>();

                    for (int j = 1; j <= totalDays; j++)
                    {
                        daysdataSource.Add(new { ID = i, Name = i.ToString() });
                    }

                    ddlDays.DataSource = daysdataSource;
                    ddlDays.DataBind();
                    upSchedulerRepeter.Update();
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlStartMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Frequency frequency = (Frequency)Convert.ToByte(ViewState["Frequency"]);
                byte day = Convert.ToByte(ViewState["Day"]);
                byte startMonth = Convert.ToByte(ddlStartMonth.SelectedValue);
                byte step = 1;
                switch (frequency)
                {
                    case Frequency.Quarterly:
                        step = 3;
                        break;
                    case Frequency.FourMonthly:
                        step = 4;
                        break;
                    case Frequency.HalfYearly:
                        step = 6;
                        break;
                    case Frequency.Annual:
                        step = 12;
                        break;
                    case Frequency.TwoYearly:
                        step = 12;
                        break;
                    case Frequency.SevenYearly:
                        step = 12;
                        break;
                }

                var dataSource = new List<object>();

                for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                {
                    RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
                    HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
                    DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");

                    int month = startMonth + (step * i) <= 12 ? startMonth + (step * i) : 1;
                    int specialMonth;
                    if (step == 12)
                    {
                        specialMonth = startMonth + (step * i);
                    }
                    else
                    {
                        if (month < 10)
                        {
                            if (month > 12)
                            {
                                specialMonth = 1;
                            }
                            else
                            {
                                if (frequency == Frequency.FourMonthly)
                                    specialMonth = startMonth + (step * i) + 4;
                                else
                                    specialMonth = startMonth + (step * i) + 6;

                            }
                        }
                        else
                        {
                            if (frequency == Frequency.FourMonthly)
                                specialMonth = 4;
                            else
                                specialMonth = startMonth + (step * i) - 6;
                        }
                    }

                    dataSource.Add(new
                    {
                        ID = hdnID.Value,
                        ForMonth = month,
                        ForMonthName = frequency == Frequency.Monthly ? ((Month)month).ToString() : ((Month)month).ToString() + " - " + ((Month)((month + (step - 1)) > 12 ? (month + (step - 1)) - 12 : (month + (step - 1)))).ToString(),
                        SpecialDay = day,
                        SpecialMonth = specialMonth
                    });
                }

                repComplianceSchedule.DataSource = dataSource;
                repComplianceSchedule.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void repComplianceSchedule_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    DropDownList ddlDays = (DropDownList)e.Item.FindControl("ddlDays");
                    DropDownList ddlMonths = (DropDownList)e.Item.FindControl("ddlMonths");
                   

                    ScriptManager scriptManager = ScriptManager.GetCurrent(Page);
                    if (scriptManager != null)
                    {
                        scriptManager.RegisterAsyncPostBackControl(ddlMonths);
                    }

                    ddlMonths.DataSource = Enumerations.GetAll<Month>();
                    ddlMonths.DataBind();

                    int totalDays = 28;
                    int day = Convert.ToInt16(Convert.ToByte(e.Item.DataItem.GetType().GetProperty("SpecialDay").GetValue(e.Item.DataItem, null).ToString()));
                    Month month = (Month)Convert.ToByte(e.Item.DataItem.GetType().GetProperty("SpecialMonth").GetValue(e.Item.DataItem, null).ToString());
                    ddlMonths.SelectedValue = ((byte)month).ToString();

                    switch (month)
                    {
                        case Month.February:
                            totalDays = 28;
                            break;
                        case Month.January:
                        case Month.March:
                        case Month.May:
                        case Month.July:
                        case Month.August:
                        case Month.October:
                        case Month.December:
                            totalDays = 31;
                            break;
                        case Month.April:
                        case Month.June:
                        case Month.September:
                        case Month.November:
                            totalDays = 30;
                            break;
                    }

                    var dataSource = new List<object>();

                    for (int i = 1; i <= totalDays; i++)
                    {
                        dataSource.Add(new { ID = i, Name = i.ToString() });
                    }

                    ddlDays.DataSource = dataSource;
                    ddlDays.DataBind();

                    if (day > totalDays)
                    {
                        day = totalDays;
                    }

                    ddlDays.SelectedValue = day.ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_COMPLIANCE"))
                {
                    divSampleForm.Visible = false;
                    grdSampleForm.DataSource = null;
                    grdSampleForm.DataBind();
                    hdnFile.Value = "";
                    lblErrorMassage.Text = "";
                    int complianceID = Convert.ToInt32(e.CommandArgument);
                    ViewState["GComplianceID"] = null;
                    ViewState["GComplianceID"] = Convert.ToInt32(e.CommandArgument);
                    var compliance = Business.ComplianceManagement.GetByID(complianceID);
                    var complianceForm = Business.ComplianceManagement.GetMultipleComplianceFormByID(complianceID);
                    hdnFile.Value = Convert.ToString(complianceID);
                    if (compliance.LocationTypeID.HasValue)
                    {
                        ddlLocationType.SelectedValue = Convert.ToString(compliance.LocationTypeID);
                    }
                    #region Industry Mapping 
                    var vGetIndustryMappedIDs = Business.ComplianceManagement.GetIndustryMappedID(complianceID);
                    foreach (RepeaterItem aItem in rptIndustry.Items)
                    {
                        CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                        chkIndustry.Checked = false;
                        CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                        for (int i = 0; i <= vGetIndustryMappedIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblIndustryID")).Text.Trim() == vGetIndustryMappedIDs[i].ToString())
                            {
                                chkIndustry.Checked = true;
                            }
                        }
                        if ((rptIndustry.Items.Count) == (vGetIndustryMappedIDs.Count))
                        {
                            IndustrySelectAll.Checked = true;
                        }
                        else
                        {
                            IndustrySelectAll.Checked = false;
                        }
                    }
                    #endregion

                    #region EntityType Mapping
                    var vGetLegalEntityTypeMappedID = Business.ComplianceManagement.GetLegalEntityTypeMappedID(complianceID);
                    foreach (RepeaterItem aItem in rptEntityType.Items)
                    {
                        CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");
                        chkEntityType.Checked = false;
                        CheckBox EntityTypeSelectAll = (CheckBox)rptEntityType.Controls[0].Controls[0].FindControl("EntityTypeSelectAll");
                        for (int i = 0; i <= vGetLegalEntityTypeMappedID.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblEntityTypeID")).Text.Trim() == vGetLegalEntityTypeMappedID[i].ToString())
                            {
                                chkEntityType.Checked = true;
                            }
                        }
                        if ((rptEntityType.Items.Count) == (vGetLegalEntityTypeMappedID.Count))
                        {
                            EntityTypeSelectAll.Checked = true;
                        }
                        else
                        {
                            EntityTypeSelectAll.Checked = false;
                        }
                    }
                    #endregion


                    #region Company Type
                    txtCompanyType.Text = "< Select >";
                    var vGetActCompanyTypeIDs = ActManagement.GetComplianceCompanyTypeMappedID(complianceID);
                    foreach (RepeaterItem aItem in rptCompanyType.Items)
                    {
                        CheckBox chkCompanyType = (CheckBox)aItem.FindControl("chkCompanyType");
                        chkCompanyType.Checked = false;
                        CheckBox CompanyTypeSelectAll = (CheckBox)rptCompanyType.Controls[0].Controls[0].FindControl("CompanyTypeSelectAll");

                        for (int i = 0; i <= vGetActCompanyTypeIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblCompanyTypeID")).Text.Trim() == vGetActCompanyTypeIDs[i].ToString())
                                chkCompanyType.Checked = true;
                        }
                        if ((rptCompanyType.Items.Count) == (vGetActCompanyTypeIDs.Count))
                            CompanyTypeSelectAll.Checked = true;
                    }
                    #endregion


                    #region Business Activity Type
                    txtBusinessActivityType.Text = "< Select >";
                    var vGetActBusinessActivityIDs = ActManagement.GetComplianceBusinessActivityMappedID(complianceID);
                    foreach (RepeaterItem aItem in rptBusinessActivityType.Items)
                    {
                        CheckBox chkBusinessActivityType = (CheckBox)aItem.FindControl("chkBusinessActivityType");
                        chkBusinessActivityType.Checked = false;
                        CheckBox BusinessActivityTypeSelectAll = (CheckBox)rptBusinessActivityType.Controls[0].Controls[0].FindControl("BusinessActivityTypeSelectAll");

                        for (int i = 0; i <= vGetActBusinessActivityIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblBusinessActivityTypeID")).Text.Trim() == vGetActBusinessActivityIDs[i].ToString())
                                chkBusinessActivityType.Checked = true;
                        }
                        if ((rptBusinessActivityType.Items.Count) == (vGetActBusinessActivityIDs.Count))
                            BusinessActivityTypeSelectAll.Checked = true;
                    }
                    #endregion


                    #region Act Applicability
                    txtActApplicability.Text = "< Select >";
                    var vGetActApplicabilityIDs = ActManagement.GetComplianceApplicabilityMappedID(complianceID);
                    foreach (RepeaterItem aItem in rptActApplicability.Items)
                    {
                        CheckBox chkActApplicability = (CheckBox)aItem.FindControl("chkActApplicability");
                        chkActApplicability.Checked = false;
                        CheckBox ActApplicabilitySelectAll = (CheckBox)rptActApplicability.Controls[0].Controls[0].FindControl("ActApplicabilitySelectAll");

                        for (int i = 0; i <= vGetActBusinessActivityIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblActApplicabilityID")).Text.Trim() == vGetActApplicabilityIDs[i].ToString())
                                chkActApplicability.Checked = true;
                        }
                        if ((rptActApplicability.Items.Count) == (vGetActApplicabilityIDs.Count))
                            ActApplicabilitySelectAll.Checked = true;
                    }
                    #endregion

                    #region Secretrial tag
                    var vGetSecretrialMappedID = ActManagement.GetSecretrialTagMappedID(complianceID);
                    foreach (RepeaterItem aItem in rptIsForSecretarial.Items)
                    {
                        CheckBox chkIsForSecretarial = (CheckBox)aItem.FindControl("chkIsForSecretarial");
                        chkIsForSecretarial.Checked = false;
                        CheckBox IsForSecretarialSelectAll = (CheckBox)rptIsForSecretarial.Controls[0].Controls[0].FindControl("IsForSecretarialSelectAll");
                        for (int i = 0; i <= vGetSecretrialMappedID.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblIsForSecretarialID")).Text.Trim() == vGetSecretrialMappedID[i].ToString())
                            {
                                chkIsForSecretarial.Checked = true;
                            }
                        }
                        if ((rptIsForSecretarial.Items.Count) == (vGetSecretrialMappedID.Count))
                        {
                            IsForSecretarialSelectAll.Checked = true;
                        }
                        else
                        {
                            IsForSecretarialSelectAll.Checked = false;
                        }
                    }

                    #endregion

                    txtIndustry.Text = "< Select >";
                    txtEntityType.Text = "< Select >";
                    txtIsForSecretarial.Text = "< Select >";
                    ViewState["Mode"] = 1;
                    ViewState["ComplianceID"] = complianceID;
                    ddlAct.SelectedValue = compliance.ActID.ToString();
                    txtShortDescription.Text = compliance.ShortDescription;
                    tbxDescription.Text = compliance.Description;
                    txtshortform.Text = compliance.ShortForm;
                    tbxSections.Text = compliance.Sections;
                    chkDocument.Checked = compliance.UploadDocument ?? false;
                    ddlComplianceType.SelectedValue = compliance.ComplianceType.ToString();
                    if (compliance.ScheduleType !=null)
                    {
                        ddlScheduleType.SelectedValue = compliance.ScheduleType.ToString();
                    }
                    
                    if (compliance.IsForSecretarial == true)
                    {
                        ChkIsForSecretarial.Checked = true;
                        divSecretarial.Visible = true;
                    }
                    else
                    {
                        ChkIsForSecretarial.Checked = false;
                        divSecretarial.Visible = false;
                    }
                    if (compliance.IsMapped == true)
                    {
                        ChkIsMapped.Checked = true;
                    }
                    else
                    {
                        ChkIsMapped.Checked = false;
                    }

                    if (compliance.StartDate != null)
                        tbxStartDate.Text = Convert.ToDateTime(compliance.StartDate).ToString("dd-MM-yyyy");

                    if (vGetIndustryMappedIDs.Count <= 0)
                    {
                        ddlAct_SelectedIndexChanged(null, null);
                    }
                    if (compliance.ComplianceType == 1)
                    {
                        divActionable.Visible = true;
                    }
                    else
                    {
                        divActionable.Visible = false;
                    }
                    rdoComplianceVisible.Checked = compliance.ComplinceVisible ?? false;
                    if (rdoComplianceVisible.Checked == false)
                    {
                        rdoComplianceVisible.Checked = false;
                        rdoNotComplianceVisible.Checked = true;
                        divChecklist.Visible = false;
                        divOneTime.Visible = false;
                        divFrequency.Visible = false;
                        vivDueDate.Visible = false;
                    }
                    else
                    {
                        rdoComplianceVisible.Checked = true;
                        rdoNotComplianceVisible.Checked = false;
                    }
                    ddlComplianceType_SelectedIndexChanged(null, null);

                    var compliancetag = Business.ComplianceManagement.GetComplianceDatatagMapping(complianceID);
                    List<string> CIntertagfixvalue = new List<string>();
                    foreach (var tag in compliancetag)
                    {
                        string tag1 = tag.FileTag;
                        CIntertagfixvalue.Add(tag1);
                    }
                    string nameOfString = (string.Join(",", CIntertagfixvalue.Select(x => x.ToString()).ToArray()));
                    txtCompliancetag.Text = nameOfString;
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);
                    if (complianceForm.Count > 0)
                    {
                        divSampleForm.Visible = true;
                        grdSampleForm.DataSource = complianceForm;
                        grdSampleForm.DataBind();
                    }
                    else
                    {
                        divSampleForm.Visible = false;
                    }
                    tbxRequiredForms.Text = compliance.RequiredForms;
                    txtSampleFormLink.Text = compliance.SampleFormLink;
                    ddlRiskType.SelectedValue = compliance.RiskType.ToString();
                    if (Convert.ToString(compliance.PenaltyDescription) != null)
                    {
                        txtPenaltyDescription.Text = compliance.PenaltyDescription.ToString();
                    }
                    if (Convert.ToString(compliance.ReferenceMaterialText) != null)
                    {
                        txtReferenceMaterial.Text = compliance.ReferenceMaterialText.ToString();
                    }

                    if (compliance.onlineoffline != null)
                        isOnline.Checked = (bool)compliance.onlineoffline;

                    if (compliance.duedatetype != null)
                        ddlDueDateType.SelectedValue = Convert.ToString(compliance.duedatetype);

                    ddlDueDateType.Enabled = true;
                    if (isOnline.Checked == true)
                    {
                        ddlDueDateType.Enabled = false;
                    }

                    if (compliance.ComplianceType == 0 || compliance.ComplianceType == 2 || compliance.ComplianceType == 3)//function based or time based and one time  Statutory
                    {
                        ddlNatureOfCompliance.SelectedValue = (compliance.NatureOfCompliance ?? -1).ToString();
                        ddlNatureOfCompliance_SelectedIndexChanged(null, null);
                        try
                        {
                            if (compliance.ComplianceSubTypeID == null)
                            {
                                ddlComplianceSubType.SelectedValue = "-1";
                            }
                            else
                            {
                                ddlComplianceSubType.SelectedValue = compliance.ComplianceSubTypeID.ToString();
                            }
                        }
                        catch (Exception)
                        {
                        }

                        //chkEventBased.Checked = false;
                        //divEvent.Visible = false;
                        divComplianceDueDays.Visible = false;
                        divNonEvents.Visible = true;
                        //ddlEvents.SelectedIndex = 0;
                        txtEventDueDate.Text = string.Empty;

                        if (ddlComplianceType.SelectedValue == "3")
                        {
                            vivDueDate.Visible = false;
                            tbxOnetimeduedate.Text = compliance.OneTimeDate != null ? compliance.OneTimeDate.Value.ToString("dd-MM-yyyy") : " ";
                        }
                        else
                        {
                            ddlFrequency.SelectedValue = (compliance.Frequency ?? 0).ToString();
                            if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                            {
                                vivDueDate.Visible = false;
                            }
                            else
                            {
                                vivDueDate.Visible = true;
                            }
                            vivWeekDueDays.Visible = false;
                            if (ddlFrequency.SelectedValue == "8")
                            {
                                ddlWeekDueDay.SelectedValue = compliance.DueWeekDay.ToString();
                                vivWeekDueDays.Visible = true;
                            }
                            else
                            {
                                vivWeekDueDays.Visible = false;
                            }
                        }
                        if (compliance.DueDate != null && compliance.SubComplianceType == 0 && compliance.SubComplianceType == 2)
                        {
                            ddlDueDate.SelectedValue = (compliance.DueDate ?? 0).ToString();
                        }
                        else
                        {
                            if (compliance.DueDate != null && compliance.DueDate != -1)
                            {
                                if (compliance.DueDate <= 31)
                                {
                                    ddlDueDate.SelectedValue = (compliance.DueDate ?? 0).ToString();
                                }
                                else
                                {
                                    txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                }
                            }
                        }
                        if (compliance.ComplianceType == 2)
                        {
                            if (compliance.SubComplianceType == 0)
                            {
                                divNonEvents.Visible = false;
                                divFrequency.Visible = false;
                                vivDueDate.Visible = false;
                                rfvEventDue.Enabled = false;
                                cvfrequency.Enabled = false;
                                divComplianceDueDays.Visible = true;
                                rgexEventDueDate.Enabled = true;
                                txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                            }
                            else if (compliance.SubComplianceType == 1)
                            {
                                divNonEvents.Visible = true;
                                divFrequency.Visible = true;
                                vivDueDate.Visible = false;
                                rfvEventDue.Enabled = false;
                                cvfrequency.Enabled = true;
                                divComplianceDueDays.Visible = false;
                                rgexEventDueDate.Enabled = false;
                            }
                            else
                            {
                                divNonEvents.Visible = true;
                                divFrequency.Visible = true;
                                vivDueDate.Visible = false;
                                rfvEventDue.Enabled = false;
                                cvfrequency.Enabled = true;
                                divComplianceDueDays.Visible = true;
                                rgexEventDueDate.Enabled = true;
                                txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                            }
                            rbTimebasedTypes.SelectedValue = Convert.ToString(compliance.SubComplianceType);
                        }
                        ddlNonComplianceType.SelectedValue = (compliance.NonComplianceType ?? -1).ToString();
                        if (ddlNonComplianceType.SelectedValue == "-1")
                        {
                            divMonetary.Visible = false;
                            divNonMonetary.Visible = false;
                        }
                        if (compliance.NonComplianceType != null)
                        {
                            ddlNonComplianceType_SelectedIndexChanged(null, null);
                            tbxFixedMinimum.Text = compliance.FixedMinimum.HasValue ? compliance.FixedMinimum.Value.ToString() : string.Empty;
                            tbxFixedMaximum.Text = compliance.FixedMaximum.HasValue ? compliance.FixedMaximum.Value.ToString() : string.Empty;
                            if (compliance.VariableAmountPerDay.HasValue)
                            {
                                ddlPerDayMonth.SelectedValue = "0";
                                tbxVariableAmountPerDay.Text = compliance.VariableAmountPerDay.HasValue ? compliance.VariableAmountPerDay.Value.ToString() : string.Empty;
                            }
                            else if (compliance.VariableAmountPerMonth.HasValue)
                            {
                                ddlPerDayMonth.SelectedValue = "1";
                                tbxVariableAmountPerDay.Text = compliance.VariableAmountPerMonth.HasValue ? compliance.VariableAmountPerMonth.Value.ToString() : string.Empty;
                            }
                            else
                            {
                                ddlPerDayMonth.SelectedValue = "2";
                                tbxVariableAmountPerDay.Text = compliance.VariableAmountPerInstance.HasValue ? compliance.VariableAmountPerInstance.Value.ToString() : string.Empty;
                            }
                            tbxVariableAmountPerDayMax.Text = compliance.VariableAmountPerDayMax.HasValue ? compliance.VariableAmountPerDayMax.Value.ToString() : string.Empty;
                            //tbxVariableAmountPercent.Text = compliance.VariableAmountPercent.HasValue ? compliance.VariableAmountPercent.Value.ToString() : string.Empty;
                            //tbxVariableAmountPercentMaximum.Text = compliance.VariableAmountPercentMax.HasValue ? compliance.VariableAmountPercentMax.Value.ToString() : string.Empty;
                            tbxVariableAmountPercent.Text = compliance.VariableAmountPercent.ToString();
                            tbxVariableAmountPercentMaximum.Text = compliance.VariableAmountPercentMax.ToString();

                            chbImprisonment.Checked = compliance.Imprisonment ?? false;
                            if (chbImprisonment.Checked)
                            {
                                divImprisonmentDetails.Visible = true;
                            }
                            else
                            {
                                divImprisonmentDetails.Visible = false;
                            }
                            tbxDesignation.Text = compliance.Designation;
                            tbxMinimumYears.Text = compliance.MinimumYears.HasValue ? compliance.MinimumYears.Value.ToString() : string.Empty;
                            tbxMaximumYears.Text = compliance.MaximumYears.HasValue ? compliance.MaximumYears.Value.ToString() : string.Empty;

                            ddlMinimumYear.SelectedValue = (compliance.MinimumPeriodType ?? -1).ToString();
                            ddlMaximumYear.SelectedValue = (compliance.MaximumPeriodType ?? -1).ToString();
                        }
                        tbxNonComplianceEffects.Text = compliance.NonComplianceEffects;
                    }
                    else// checklist
                    {
                        ddlNatureOfCompliance.SelectedValue = (compliance.NatureOfCompliance ?? -1).ToString();
                        ddlNatureOfCompliance_SelectedIndexChanged(null, null);
                        try
                        {
                            if (compliance.ComplianceSubTypeID == null)
                            {
                                ddlComplianceSubType.SelectedValue = "-1";
                            }
                            else
                            {
                                ddlComplianceSubType.SelectedValue = compliance.ComplianceSubTypeID.ToString();
                            }
                        }
                        catch (Exception)
                        {
                        }

                        ddlChklstType.SelectedValue = Convert.ToString(compliance.CheckListTypeID);
                        //------checklist------
                        if (compliance.CheckListTypeID == 1 || compliance.CheckListTypeID == 2)//function based or time based checklist
                        {


                            divOneTime.Visible = false;
                            //if (compliance.EventID != null)
                            //{
                            //    //chkEventBased.Checked = true;
                            //    //divEvent.Visible = true;
                            //    divComplianceDueDays.Visible = true;
                            //    divNonEvents.Visible = false;
                            //    if (EventManagement.ExistsID(Convert.ToInt64(compliance.EventID)))
                            //    {
                            //        ddlEvents.SelectedValue = Convert.ToString(compliance.EventID);
                            //        ddlEvents_SelectedIndexChanged(null, null);

                            //        if (compliance.SubEventID != null)
                            //        {
                            //            divSubEventmode.Visible = true;
                            //            SubEvent subevent = EventManagement.GetSubEventByID(Convert.ToInt64(compliance.SubEventID));
                            //            tbxSubEvent.Text = subevent.Name;
                            //            SelectNodeByValue(tvSubEvent.Nodes[0], Convert.ToString(compliance.SubEventID));
                            //        }
                            //    }

                            //    if (compliance.DueDate != null)
                            //    {
                            //        txtEventDueDate.Text = Convert.ToString(compliance.DueDate);
                            //    }
                            //    if (compliance.EventComplianceType != null)
                            //    {
                            //        rbEventComplianceType.SelectedValue = Convert.ToString(compliance.EventComplianceType);
                            //    }
                            //}
                            //else
                            //{
                            if (compliance.ComplinceVisible == true)
                            {
                                divFunctionBased.Visible = true;
                                divFrequency.Visible = true;
                            }
                            else
                            {
                                divFunctionBased.Visible = false;
                                divFrequency.Visible = false;
                            }
                            divNonComplianceType.Visible = true;
                            //chkEventBased.Checked = false;
                            //divEvent.Visible = false;
                            divComplianceDueDays.Visible = false;
                            divNonEvents.Visible = true;
                            //ddlEvents.SelectedIndex = 0;
                            txtEventDueDate.Text = string.Empty;
                            ddlFrequency.SelectedValue = (compliance.Frequency ?? 0).ToString();
                            if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                            {
                                vivDueDate.Visible = false;
                            }
                            else
                            {
                                vivDueDate.Visible = true;
                            }
                            if (compliance.DueDate != null && compliance.SubComplianceType == 0 && compliance.SubComplianceType == 2)
                            {
                                ddlDueDate.SelectedValue = (compliance.DueDate ?? 0).ToString();
                            }
                            else
                            {
                                if (compliance.DueDate != null && compliance.DueDate != -1)
                                {
                                    if (compliance.DueDate <= 31)
                                    {
                                        ddlDueDate.SelectedValue = (compliance.DueDate ?? 0).ToString();
                                    }
                                    else
                                    {
                                        txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                    }
                                }
                            }
                            if (compliance.CheckListTypeID == 2)
                            {
                                divTimebasedTypes.Visible = true;
                                if (compliance.SubComplianceType == 0)//fixed gap -- time based
                                {
                                    divNonEvents.Visible = false;
                                    divFunctionBased.Visible = false;
                                    divNonComplianceType.Visible = true;
                                    divFrequency.Visible = false;
                                    vivDueDate.Visible = false;
                                    rfvEventDue.Enabled = false;
                                    cvfrequency.Enabled = false;
                                    divComplianceDueDays.Visible = true;
                                    rgexEventDueDate.Enabled = true;
                                    txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                }
                                else if (compliance.SubComplianceType == 1)
                                {
                                    divNonEvents.Visible = true;
                                    divFunctionBased.Visible = true;
                                    divFrequency.Visible = true;
                                    divNonComplianceType.Visible = true;
                                    vivDueDate.Visible = false;
                                    rfvEventDue.Enabled = false;
                                    cvfrequency.Enabled = true;
                                    divComplianceDueDays.Visible = false;
                                    rgexEventDueDate.Enabled = false;
                                }
                                else
                                {
                                    divNonEvents.Visible = true;
                                    divFrequency.Visible = true;
                                    vivDueDate.Visible = false;
                                    rfvEventDue.Enabled = false;
                                    cvfrequency.Enabled = true;
                                    divComplianceDueDays.Visible = true;
                                    rgexEventDueDate.Enabled = true;
                                    txtEventDueDate.Text = (compliance.DueDate ?? 0).ToString();
                                }
                                rbTimebasedTypes.SelectedValue = Convert.ToString(compliance.SubComplianceType);
                            }
                            //}
                        }
                        else//one time checklist
                        {
                            //if (compliance.EventID != null)
                            //{
                            //    //chkEventBased.Checked = true;
                            //    //divEvent.Visible = true;
                            //    divComplianceDueDays.Visible = true;
                            //    divNonEvents.Visible = false;
                            //    //divEventComplianceType.Visible = true;
                            //    if (EventManagement.ExistsID(Convert.ToInt64(compliance.EventID)))
                            //    {
                            //        //ddlEvents.SelectedValue = Convert.ToString(compliance.EventID);
                            //        ddlEvents_SelectedIndexChanged(null, null);
                            //        if (compliance.SubEventID != null)
                            //        {
                            //            //divSubEventmode.Visible = true;
                            //            SubEvent subevent = EventManagement.GetSubEventByID(Convert.ToInt64(compliance.SubEventID));
                            //            //tbxSubEvent.Text = subevent.Name;
                            //            SelectNodeByValue(tvSubEvent.Nodes[0], Convert.ToString(compliance.SubEventID));
                            //        }
                            //    }
                            //    if (compliance.DueDate != null)
                            //    {
                            //        txtEventDueDate.Text = Convert.ToString(compliance.DueDate);
                            //    }
                            //    if (compliance.EventComplianceType != null)
                            //    {
                            //        rbEventComplianceType.SelectedValue = Convert.ToString(compliance.EventComplianceType);
                            //    }
                            //}
                            //else
                            //{
                            //chkEventBased.Checked = false;
                            //divEvent.Visible = false;
                            divComplianceDueDays.Visible = false;
                            divNonEvents.Visible = true;
                            //ddlEvents.SelectedIndex = 0;
                            txtEventDueDate.Text = string.Empty;
                            divTimebasedTypes.Visible = false;
                            divFunctionBased.Visible = true;
                            divFrequency.Visible = false;
                            vivDueDate.Visible = false;
                            vivWeekDueDays.Visible = false;
                            divNonComplianceType.Visible = true;
                            rdoComplianceVisible.Checked = compliance.ComplinceVisible ?? false;
                            if (rdoComplianceVisible.Checked == false)
                            {
                                divOneTime.Visible = false;
                            }
                            else
                            {
                                divOneTime.Visible = true;
                            }
                            tbxOnetimeduedate.Text = compliance.OneTimeDate != null ? compliance.OneTimeDate.Value.ToString("dd-MM-yyyy") : " ";
                            divComplianceDueDays.Visible = false;
                            txtEventDueDate.Text = null;
                            //}
                        }

                        ddlNonComplianceType.SelectedValue = (compliance.NonComplianceType ?? -1).ToString();
                        if (ddlNonComplianceType.SelectedValue == "-1")
                        {
                            divMonetary.Visible = false;
                            divNonMonetary.Visible = false;
                        }
                        if (compliance.NonComplianceType != null)
                        {
                            ddlNonComplianceType_SelectedIndexChanged(null, null);
                            tbxFixedMinimum.Text = compliance.FixedMinimum.HasValue ? compliance.FixedMinimum.Value.ToString() : string.Empty;
                            tbxFixedMaximum.Text = compliance.FixedMaximum.HasValue ? compliance.FixedMaximum.Value.ToString() : string.Empty;
                            if (compliance.VariableAmountPerDay.HasValue)
                            {
                                ddlPerDayMonth.SelectedValue = "0";
                                tbxVariableAmountPerDay.Text = compliance.VariableAmountPerDay.HasValue ? compliance.VariableAmountPerDay.Value.ToString() : string.Empty;
                            }
                            else if (compliance.VariableAmountPerMonth.HasValue)
                            {
                                ddlPerDayMonth.SelectedValue = "1";
                                tbxVariableAmountPerDay.Text = compliance.VariableAmountPerMonth.HasValue ? compliance.VariableAmountPerMonth.Value.ToString() : string.Empty;
                            }
                            else
                            {
                                ddlPerDayMonth.SelectedValue = "2";
                                tbxVariableAmountPerDay.Text = compliance.VariableAmountPerInstance.HasValue ? compliance.VariableAmountPerInstance.Value.ToString() : string.Empty;
                            }
                            tbxVariableAmountPerDayMax.Text = compliance.VariableAmountPerDayMax.HasValue ? compliance.VariableAmountPerDayMax.Value.ToString() : string.Empty;
                            //tbxVariableAmountPercent.Text = compliance.VariableAmountPercent.HasValue ? compliance.VariableAmountPercent.Value.ToString() : string.Empty;
                            //tbxVariableAmountPercentMaximum.Text = compliance.VariableAmountPercentMax.HasValue ? compliance.VariableAmountPercentMax.Value.ToString() : string.Empty;
                            tbxVariableAmountPercent.Text = compliance.VariableAmountPercent.ToString();
                            tbxVariableAmountPercentMaximum.Text = compliance.VariableAmountPercentMax.ToString();

                            chbImprisonment.Checked = compliance.Imprisonment ?? false;
                            if (chbImprisonment.Checked)
                            {
                                divImprisonmentDetails.Visible = true;
                            }
                            else
                            {
                                divImprisonmentDetails.Visible = false;
                            }
                            tbxDesignation.Text = compliance.Designation;
                            tbxMinimumYears.Text = compliance.MinimumYears.HasValue ? compliance.MinimumYears.Value.ToString() : string.Empty;
                            tbxMaximumYears.Text = compliance.MaximumYears.HasValue ? compliance.MaximumYears.Value.ToString() : string.Empty;

                            ddlMinimumYear.SelectedValue = (compliance.MinimumPeriodType ?? -1).ToString();
                            ddlMaximumYear.SelectedValue = (compliance.MaximumPeriodType ?? -1).ToString();
                        }
                        tbxNonComplianceEffects.Text = compliance.NonComplianceEffects;

                    }
                    if (Convert.ToString(compliance.ReminderType) == "")
                    {
                        rbReminderType.SelectedValue = "0";
                        divForCustome.Visible = false;
                    }
                    else
                    {
                        rbReminderType.SelectedValue = Convert.ToString(compliance.ReminderType);
                        if (compliance.ReminderType == 0)
                        {
                            divForCustome.Visible = false;
                        }
                        else
                        {
                            divForCustome.Visible = true;
                        }
                    }
                    txtReminderBefore.Text = Convert.ToString(compliance.ReminderBefore);
                    txtReminderGap.Text = Convert.ToString(compliance.ReminderGap);

                    ddlFrequency.Enabled = true;
                    ddlDueDate.Enabled = true;

                    if (AuthenticationHelper.Role.Equals("RPER"))
                    {
                        ddlFrequency.Enabled = false;
                        ddlDueDate.Enabled = false;
                        ddlComplianceType.Enabled = false;
                    }

                    upComplianceDetails.Update();
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divComplianceDetailsDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_COMPLIANCE"))
                {
                    bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
                    if (chkexistsComplianceschedule == false)
                    {
                        int complianceID = Convert.ToInt32(e.CommandArgument);
                        Business.ComplianceManagement.Delete(complianceID);
                        BindCompliancesNew();
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Compliance assigned,can not deleted.";
                        upComplianceDetails.Update();
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divComplianceDetailsDialog\").dialog('open')", true);
                    }
                }
                else if (e.CommandName.Equals("SHOW_SCHEDULE"))
                {
                    int complianceID = Convert.ToInt32(e.CommandArgument);
                    var compliance = Business.ComplianceManagement.GetByID(complianceID);
                    OpenScheduleInformation(compliance);
                }
                else if (e.CommandName.Equals("STATUS"))
                {


                    lblErrorMassage.Text = "";
                    lblDeactivate.Text = "";
                    string deactivateOn = "";
                    txtShortDescriptionStatus.Text = "";
                    tbxDescriptionStatus.Text = "";
                    tbxDescriptionStatus.Text = "";
                    int complianceID = Convert.ToInt32(e.CommandArgument);
                    var compliance = Business.ComplianceManagement.GetByID(complianceID);
                    var compliancedeactive = Business.ComplianceManagement.GetComplianceDeactivate(complianceID);

                    ViewState["Mode"] = 1;
                    ViewState["ComplianceID"] = complianceID;
                    BindStatusActs();
                    ddlStatusAct.SelectedValue = compliance.ActID.ToString();
                    txtShortDescriptionStatus.Text = compliance.ShortDescription;
                    tbxDescriptionStatus.Text = compliance.Description;
                    txtshortform.Text = compliance.ShortForm;
                    tbxSectionsStatus.Text = compliance.Sections;
                    if (compliance.DeactivateOn != null)
                    {
                        deactivateOn = Convert.ToDateTime(compliance.DeactivateOn).ToString("dd-MM-yyyy");
                    }

                    txtDeactivateDate.Text = deactivateOn;

                    txtDeactivateDesc.Text = compliance.DeactivateDesc;

                    if (compliancedeactive != null)
                    {
                        lblDeactivate.Text = compliancedeactive.Name;
                    }
                    upComplianceStatusDetails.Update();
                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePickerStart", string.Format("initializeDatePickerStart(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divComplianceStatusDialog\").dialog('open')", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCompliances.PageIndex = e.NewPageIndex;
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindCompliancesNew();
        }

        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divFunctionBased.Visible = vaDueDate.Enabled =
                divComplianceSubType.Visible = divNatureOfCompliance.Visible =
                (ddlComplianceType.SelectedValue == "0" || ddlComplianceType.SelectedValue == "2"
                || ddlComplianceType.SelectedValue == "3" || ddlComplianceType.SelectedValue == "1");

                tbxOnetimeduedate.Text = "";
                txtEventDueDate.Text = "";
                ddlDueDate.SelectedValue = "1";
                ddlWeekDueDay.SelectedValue = "-1";
                ddlNatureOfCompliance.SelectedValue = "-1";
                ddlComplianceSubType.Items.Clear();
                ddlFrequency.SelectedValue = "-1";
                ddlNonComplianceType.SelectedValue = "-1";
                txtEventDueDate.Text = "";

                if (ddlComplianceType.SelectedValue == "0")//function based
                {
                    lblNature.Visible = true;
                    cmpValnature.Enabled = true;

                    divActionable.Visible = false;
                    divNonEvents.Visible = true;
                    divFrequency.Visible = true;
                    if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                    {
                        vivDueDate.Visible = false;
                    }
                    else
                    {
                        vivDueDate.Visible = true;
                    }
                    divTimebasedTypes.Visible = false;
                    divComplianceDueDays.Visible = false;
                    divNonComplianceType.Visible = true;

                    //-----------------------------------
                    dvUploadDoc.Visible = true;
                    dvReqForms.Visible = true;
                    dvSampleForm.Visible = true;
                    divChecklist.Visible = false;
                    divOneTime.Visible = false;

                    rbReminderType.SelectedValue = "0";
                    rbReminderType.Enabled = true;
                    rbReminderType_SelectedIndexChanged(sender, e);
                }
                else if (ddlComplianceType.SelectedValue.Equals("2"))//timebased
                {
                    lblNature.Visible = true;
                    cmpValnature.Enabled = true;
                    divActionable.Visible = false;
                    divNonEvents.Visible = false;
                    divTimebasedTypes.Visible = true;
                    divComplianceDueDays.Visible = true;
                    divFrequency.Visible = false;
                    vivDueDate.Visible = false;
                    divNonComplianceType.Visible = true;
                    rbTimebasedTypes.SelectedValue = "0";

                    //-----------------------------------
                    dvUploadDoc.Visible = true;
                    dvReqForms.Visible = true;
                    dvSampleForm.Visible = true;
                    divChecklist.Visible = false;
                    divOneTime.Visible = false;

                    rbReminderType.SelectedValue = "0";
                    rbReminderType.Enabled = true;
                    rbReminderType_SelectedIndexChanged(sender, e);

                }
                else if (ddlComplianceType.SelectedValue.Equals("3"))// statutory one time
                {
                    lblNature.Visible = true;
                    cmpValnature.Enabled = true;
                    divActionable.Visible = false;
                    divNonEvents.Visible = false;
                    divFrequency.Visible = false;
                    vivDueDate.Visible = false;
                    divTimebasedTypes.Visible = false;
                    divComplianceDueDays.Visible = false;
                    divNonComplianceType.Visible = true;
                    divChecklist.Visible = false;
                    //-----------------------------------
                    dvUploadDoc.Visible = true;
                    dvReqForms.Visible = true;
                    dvSampleForm.Visible = true;
                    divOneTime.Visible = true;

                    rbReminderType.SelectedValue = "1";
                    rbReminderType.Enabled = false;
                    rbReminderType_SelectedIndexChanged(sender, e);
                }
                else//checklist
                {
                    lblNature.Visible = false;
                    cmpValnature.Enabled = false;

                    divActionable.Visible = true;
                    divTimebasedTypes.Visible = false;
                    divFunctionBased.Visible = true;
                    divTimebasedTypes.Visible = false;
                    divComplianceDueDays.Visible = false;

                    divFrequency.Visible = false;
                    vivDueDate.Visible = false;
                    vivWeekDueDays.Visible = false;
                    divNonComplianceType.Visible = true;
                    divNatureOfCompliance.Visible = true;
                    //  divNatureOfSubCompliance.Visible = true;
                    divComplianceSubType.Visible = true;

                    //-----------------------------------
                    dvUploadDoc.Visible = true;
                    dvReqForms.Visible = true;
                    dvSampleForm.Visible = true;
                    ddlChklstType.SelectedValue = "0";

                    if (rdoComplianceVisible.Checked == true)
                    {
                        divOneTime.Visible = true;
                        divChecklist.Visible = true;
                    }
                    else
                    {
                        divOneTime.Visible = false;
                        divChecklist.Visible = false;
                    }
                    rbReminderType.SelectedValue = "0";
                    rbReminderType.Enabled = false;
                }

                ddlNonComplianceType_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlChklstType_SelectedIndexChanged(object sender, EventArgs e)
        {
            rbReminderType.SelectedValue = "0";
            rbReminderType.Enabled = false;
            divOneTime.Visible = true;
            if (ddlChklstType.SelectedValue == "0")//one time
            {

                divOneTime.Visible = true;
                divTimebasedTypes.Visible = false;
                divFunctionBased.Visible = true;
                divFrequency.Visible = false;
                vivDueDate.Visible = false;
                vivWeekDueDays.Visible = false;
                divNonComplianceType.Visible = true;
                divComplianceDueDays.Visible = false;
                divNonEvents.Visible = false;



                rbReminderType.SelectedValue = "0";
                rbReminderType.Enabled = false;

            }
            else if (ddlChklstType.SelectedValue.Equals("1"))//function based
            {

                divOneTime.Visible = false;
                divTimebasedTypes.Visible = false;

                divFunctionBased.Visible = true;
                divNonEvents.Visible = true;
                divFrequency.Visible = true;
                if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                {
                    vivDueDate.Visible = false;
                }
                else
                {
                    vivDueDate.Visible = true;
                }
                divTimebasedTypes.Visible = false;
                divComplianceDueDays.Visible = false;
                divNonComplianceType.Visible = true;
            }
            else if (ddlChklstType.SelectedValue.Equals("2"))//time based
            {

                divFunctionBased.Visible = true;
                divOneTime.Visible = false;
                divTimebasedTypes.Visible = true;
                divNonEvents.Visible = false;
                rbTimebasedTypes.SelectedValue = "0";

                divNonEvents.Visible = false;
                divTimebasedTypes.Visible = true;
                divComplianceDueDays.Visible = true;
                divFrequency.Visible = false;
                vivDueDate.Visible = false;
                divNonComplianceType.Visible = true;
                //rbReminderType.Items[0].Enabled = true;
            }
            else
            {
                divFunctionBased.Visible = true;
                divOneTime.Visible = false;
                divTimebasedTypes.Visible = true;
                divNonEvents.Visible = false;
                rbTimebasedTypes.SelectedValue = "0";

                divNonEvents.Visible = false;
                divTimebasedTypes.Visible = true;
                divComplianceDueDays.Visible = true;
                divFrequency.Visible = false;
                vivDueDate.Visible = false;
                divNonComplianceType.Visible = true;
                //rbReminderType.Items[0].Enabled = true;

            }
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);
        }

        protected void rbTimebasedTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rbTimebasedTypes.SelectedValue.Equals("0"))
                {
                    divNonEvents.Visible = false;
                    divFrequency.Visible = false;
                    vivDueDate.Visible = false;
                    rfvEventDue.Enabled = false;
                    cvfrequency.Enabled = false;
                    divComplianceDueDays.Visible = true;
                    rgexEventDueDate.Enabled = true;
                    //if (chkEventBased.Checked == true)
                    //{
                    //    divComplianceDueDays.Visible = true;

                    //}
                }
                else if (rbTimebasedTypes.SelectedValue.Equals("1"))
                {
                    divNonEvents.Visible = true;
                    divFrequency.Visible = true;
                    vivDueDate.Visible = false;
                    rfvEventDue.Enabled = false;
                    cvfrequency.Enabled = true;
                    divComplianceDueDays.Visible = false;
                    rgexEventDueDate.Enabled = false;
                    //if (chkEventBased.Checked == true)
                    //{
                    //    divComplianceDueDays.Visible = true;
                    //    divFrequency.Visible = false;
                    //    cvfrequency.Enabled = false;
                    //}
                }
                else
                {
                    divNonEvents.Visible = true;
                    divFrequency.Visible = true;
                    vivDueDate.Visible = false;
                    rfvEventDue.Enabled = false;
                    cvfrequency.Enabled = true;
                    divComplianceDueDays.Visible = true;
                    rgexEventDueDate.Enabled = true;
                    //if (chkEventBased.Checked == true)
                    //{
                    //    divComplianceDueDays.Visible = false;
                    //}
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlNonComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divMonetary.Visible = ddlNonComplianceType.SelectedValue == "0" || ddlNonComplianceType.SelectedValue == "2";
                divNonMonetary.Visible = ddlNonComplianceType.SelectedValue == "1" || ddlNonComplianceType.SelectedValue == "2";

                if (!divMonetary.Visible)
                {
                    tbxFixedMinimum.Text = tbxFixedMaximum.Text = tbxVariableAmountPerDay.Text = tbxVariableAmountPerDayMax.Text = tbxVariableAmountPercent.Text = tbxVariableAmountPercentMaximum.Text = string.Empty;
                }

                if (!divNonMonetary.Visible)
                {
                    chbImprisonment.Checked = false;
                    tbxDesignation.Text = tbxMinimumYears.Text = tbxMaximumYears.Text = string.Empty;
                    ddlMinimumYear.SelectedValue = ddlMaximumYear.SelectedValue = "-1";
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

                if (ddlComplianceType.SelectedValue == "3")
                {
                    DateTime date = DateTime.MinValue;
                    if (DateTime.TryParseExact(tbxOnetimeduedate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month, date.Day), true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                    }
                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSaveSchedule_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["ComplianceID"].ToString() != null && ViewState["ComplianceID"].ToString() != "")
                {

                    bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
                    if (chkexistsComplianceschedule == true)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Compliance assigned cannot be change Calender/Financial Year.";
                    }
                    else
                    {
                        List<ComplianceSchedule> scheduleList = new List<ComplianceSchedule>();

                        for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                        {
                            RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;

                            HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
                            HiddenField hdnForMonth = ((HiddenField)entry.FindControl("hdnForMonth"));
                            DropDownList ddlDays = (DropDownList)entry.FindControl("ddlDays");
                            DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");

                            scheduleList.Add(new ComplianceSchedule()
                            {
                                ID = Convert.ToInt32(hdnID.Value),
                                ComplianceID = Convert.ToInt64(ViewState["ComplianceID"]),
                                ForMonth = Convert.ToInt32(hdnForMonth.Value),
                                SpecialDate = string.Format("{0}{1}", Convert.ToByte(ddlDays.SelectedValue).ToString("D2"), Convert.ToByte(ddlMonths.SelectedValue).ToString("D2"))
                            });
                        }
                        Business.ComplianceManagement.UpdateComplianceUpdatedOn(Convert.ToInt64(ViewState["ComplianceID"]));
                        Business.ComplianceManagement.UpdateScheduleInformation(scheduleList);
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseScheduledDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
                        upSchedulerRepeter.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeIndustryList", string.Format("initializeJQueryUI('{0}', 'dvIndustry');", txtIndustry.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideIndustryList", "$(\"#dvIndustry\").hide(\"blind\", null, 5, function () { });", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeEntityTypeList", string.Format("initializeJQueryUI('{0}', 'dvEntityType');", txtEntityType.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideEntityTypeList", "$(\"#dvEntityType\").hide(\"blind\", null, 5, function () { });", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeCompanyTypeList", string.Format("initializeJQueryUI('{0}', 'dvCompanyType');", txtCompanyType.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideCompanyTypeList", "$(\"#dvCompanyType\").hide(\"blind\", null, 5, function () { });", true);


                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBusinessActivityTypeList", string.Format("initializeJQueryUI('{0}', 'dvBusinessActivityType');", txtBusinessActivityType.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideBusinessActivityTypeList", "$(\"#dvBusinessActivityType\").hide(\"blind\", null, 5, function () { });", true);


                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeActApplicabilityList", string.Format("initializeJQueryUI('{0}', 'dvActApplicability');", txtActApplicability.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActApplicabilityList", "$(\"#dvActApplicability\").hide(\"blind\", null, 5, function () { });", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeSecretarialList", string.Format("initializeJQueryUI('{0}', 'dvIsForSecretarial');", txtIsForSecretarial.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideSecretarialTypeList", "$(\"#dvIsForSecretarial\").hide(\"blind\", null, 5, function () { });", true);



                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxOnetimeduedate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month , date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetailsStatus_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);


                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePickerStart", string.Format("initializeDatePickerStart(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceScheduleDialog_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

                //for (int i = 0; i < repComplianceSchedule.Items.Count; i++)
                //{
                //    RepeaterItem entry = repComplianceSchedule.Items[i] as RepeaterItem;
                //    DropDownList ddlMonths = (DropDownList)entry.FindControl("ddlMonths");
                //    AsyncPostBackTrigger apbt = new AsyncPostBackTrigger();
                //    apbt.ControlID = ddlMonths.UniqueID;
                //    upComplianceScheduleDialog.Triggers.Add(apbt);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void chkDocument_CheckedChanged(object sender, EventArgs e)
        {
            //rfvFile.Enabled = chkDocument.Checked;
            if (!chkDocument.Checked)
            {
                //lblSampleForm.Text = "< Not selected >";
            }
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);

        }

        protected void grdCompliances_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int CmType = -1;
                if (rdFunctionBased.Checked)
                    CmType = 0;
                if (rdChecklist.Checked)
                    CmType = 1;

                var compliancesData = Business.ComplianceManagement.GetAll1(Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplianceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, tbxFilter.Text);
                List<object> dataSource = new List<object>();
                foreach (var complianceInfo in compliancesData)
                {
                    string risk = "";
                    if (complianceInfo.RiskType == 0)
                        risk = "High";
                    else if (complianceInfo.RiskType == 1)
                        risk = "Medium";
                    else if (complianceInfo.RiskType == 2)
                        risk = "Low";

                    //Added by Amita as on 21Jan2019 - resolve sorting error issue on ID column
                    string FrequencyName = complianceInfo.Frequency == 0 ? "Monthly" :
                                            complianceInfo.Frequency == 1 ? "Quarterly" :
                                            complianceInfo.Frequency == 2 ? "HalfYearly" :
                                            complianceInfo.Frequency == 3 ? "Annual" :
                                            complianceInfo.Frequency == 4 ? "FourMonthly" :
                                            complianceInfo.Frequency == 5 ? "TwoYearly" :
                                            complianceInfo.Frequency == 6 ? "SevenYearly" :
                                            complianceInfo.Frequency == 7 ? "Daily" :
                                            complianceInfo.Frequency == 8 ? "Weekly" : "";

                    dataSource.Add(new
                    {
                        complianceInfo.ID,
                        complianceInfo.ActName,
                        complianceInfo.Sections,
                        complianceInfo.Description,
                        complianceInfo.ComplianceType,
                        complianceInfo.EventID,
                        //NatureOfCompliance = ((NatureOfCompliance)complianceInfo.Key.NatureOfCompliance.Value).ToString(),
                        complianceInfo.UploadDocument,
                        complianceInfo.RequiredForms,
                        Frequency = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1)),
                        complianceInfo.NonComplianceEffects,
                        Risk = risk,
                        complianceInfo.ShortDescription,
                        complianceInfo.SubComplianceType,
                        complianceInfo.CheckListTypeID,
                        FrequencyName = FrequencyName
                        //Parameters = GetParameters(complianceInfo.Value)
                    });
                }

                if (direction == SortDirection.Ascending)
                {
                    dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                foreach (DataControlField field in grdCompliances.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCompliances.Columns.IndexOf(field);
                    }
                }

                grdCompliances.DataSource = dataSource;
                grdCompliances.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

      

        protected void ddlFilterComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rdChecklist_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rdFunctionBased_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind catagory of fiter catagory drop down.
        /// </summary>
        private void BindTypes()
        {
            try
            {
                ddlFilterComplianceType.DataTextField = "Name";
                ddlFilterComplianceType.DataValueField = "ID";

                ddlFilterComplianceType.DataSource = ComplianceTypeManagement.GetAll();
                ddlFilterComplianceType.DataBind();

                ddlFilterComplianceType.Items.Insert(0, new ListItem("< Select Compliance Type >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function is bind catagory for filter drodown.
        /// </summary>
        private void BindCategories()
        {
            try
            {
                ddlComplianceCatagory.DataTextField = "Name";
                ddlComplianceCatagory.DataValueField = "ID";

                ddlComplianceCatagory.DataSource = ComplianceCategoryManagement.GetAll();
                ddlComplianceCatagory.DataBind();

                ddlComplianceCatagory.Items.Insert(0, new ListItem("< Select Compliance Category>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindComplianceSubType(int NatureOfComplianceID)
        {
            try
            {
                ddlComplianceSubType.DataTextField = "Name";
                ddlComplianceSubType.DataValueField = "ID";

                ddlComplianceSubType.DataSource = ComplianceCategoryManagement.GetComplianceSubTypes(NatureOfComplianceID);
                ddlComplianceSubType.DataBind();

                ddlComplianceSubType.Items.Insert(0, new ListItem("< Nature Of Compliance Sub Type >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //private void BindEvents()
        //{
        //    try
        //    {
        //        ddlEvents.DataTextField = "Name";
        //        ddlEvents.DataValueField = "ID";

        //        ddlEvents.DataSource = EventManagement.GetAllEvents(-1, string.Empty);
        //        ddlEvents.DataBind();

        //        ddlEvents.Items.Insert(0, new ListItem("< Select Event>", "-1"));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        private void BindDueDays()
        {
            try
            {
                ddlWeekDueDay.DataTextField = "Name";
                ddlWeekDueDay.DataValueField = "ID";

                ddlWeekDueDay.DataSource = Enumerations.GetAll<Days>();
                ddlWeekDueDay.DataBind();

                ddlWeekDueDay.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindFrequencies()
        {
            try
            {
                ddlFrequency.DataTextField = "Name";
                ddlFrequency.DataValueField = "ID";

                ddlFilterFrequencies.DataTextField = "Name";
                ddlFilterFrequencies.DataValueField = "ID";

                var fre = Enumerations.GetAll<Frequency>();
                ddlFrequency.DataSource = fre;
                ddlFrequency.DataBind();

                ddlFilterFrequencies.DataSource = fre;
                ddlFilterFrequencies.DataBind();

                ddlFrequency.Items.Insert(0, new ListItem("< Select >", "-1"));
                ddlFilterFrequencies.Items.Insert(0, new ListItem("< Select Frequency>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindFilterFrequencies()
        {
            try
            {
                ddlFilterFrequencies.DataTextField = "Name";
                ddlFilterFrequencies.DataValueField = "ID";

                ddlFilterFrequencies.DataSource = Enumerations.GetAll<Frequency>();
                ddlFilterFrequencies.DataBind();

                ddlFilterFrequencies.Items.Insert(0, new ListItem("< Select Frequency>", "-1"));
                ddlFilterFrequencies.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindDueDates()
        {
            try
            {
                for (int count = 1; count < 32; count++)
                {
                    ddlDueDate.Items.Add(new ListItem() { Text = count.ToString(), Value = count.ToString() });
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCompliancesNew()
        {
            try
            {


                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int CmType = -1;
                    if (rdFunctionBased.Checked)
                        CmType = 0;
                    if (rdChecklist.Checked)
                        CmType = 1;

                    var compliancesQuery = Business.ComplianceManagement.GetNotMappedComplianceMaster();

                    int risk = -1;
                    if (tbxFilter.Text.ToUpper().Equals("HIGH"))
                        risk = 0;
                    else if (tbxFilter.Text.ToUpper().Equals("MEDIUM"))
                        risk = 1;
                    else if (tbxFilter.Text.ToUpper().Equals("LOW"))
                        risk = 2;

                    string frequency = tbxFilter.Text.ToUpper();
                    if (frequency.Equals("MONTHLY"))
                        frequency = "Monthly";
                    if (frequency.Equals("QUARTERLY"))
                        frequency = "Quarterly";
                    if (frequency.Equals("HALFYEARLY"))
                        frequency = "HalfYearly";
                    if (frequency.Equals("ANNUAL"))
                        frequency = "Annual";
                    if (frequency.Equals("FOURMONTHALY"))
                        frequency = "FourMonthly";
                    if (frequency.Equals("TWOYEARLY"))
                        frequency = "TwoYearly";
                    if (frequency.Equals("SEVENYEARLY"))
                        frequency = "SevenYearly";

                    int frequencyId = Enumerations.GetEnumByName<Frequency>(Convert.ToString(frequency));
                    if (frequencyId == 0 && frequency != "Monthly")
                    {
                        frequencyId = -1;
                    }

                    if (!string.IsNullOrEmpty(tbxFilter.Text))
                    {
                        if (CheckInt(tbxFilter.Text))
                        {
                            int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                            compliancesQuery = compliancesQuery.Where(entry => entry.ID == a).ToList();
                        }
                        else
                        {
                            //compliancesQuery = compliancesQuery.Where(entry => entry.ActName.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.ShortDescription.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.Sections.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.RequiredForms.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.Frequency == frequencyId || entry.RiskType == risk).ToList();
                            compliancesQuery = compliancesQuery.Where(entry => entry.ActName.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.ShortDescription.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.Sections.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.RequiredForms.ToUpper().Contains(tbxFilter.Text.ToUpper()) || (entry.Frequency == frequencyId && entry.Frequency != null) || entry.RiskType == risk).ToList();
                        }
                    }

                    if (Convert.ToInt32(ddlFilterComplianceType.SelectedValue) != -1)
                    {
                        int a = Convert.ToInt32(ddlFilterComplianceType.SelectedValue);
                        compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceTypeId == a).ToList();
                    }
                    if (Convert.ToInt32(ddlComplianceCatagory.SelectedValue) != -1)
                    {
                        int b = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                        compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceCategoryId == b).ToList();
                    }
                    if (CmType != -1)
                    {
                        compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceType == CmType).ToList();
                    }
                    if (Convert.ToInt32(ddlFilterFrequencies.SelectedValue) != -1)
                    {
                        int c = Convert.ToInt32(ddlFilterFrequencies.SelectedValue);
                        compliancesQuery = compliancesQuery.Where(entry => entry.Frequency == c).ToList();
                    }
                    if (ddlAct1.SelectedValue != "")
                    {
                        if (Convert.ToInt32(ddlAct1.SelectedValue) != -1)
                        {
                            int c = Convert.ToInt32(ddlAct1.SelectedValue);
                            compliancesQuery = compliancesQuery.Where(entry => entry.ActID == c).ToList();
                        }
                    }
                    var compliances = compliancesQuery.ToList();

                    if (ViewState["SortOrder"].ToString() == "Asc")
                    {
                        if (ViewState["SortExpression"].ToString() == "ID")
                        {
                            compliances = compliances.OrderBy(entry => entry.ID).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "ActName")
                        {
                            compliances = compliances.OrderBy(entry => entry.ActName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "ShortDescription")
                        {
                            compliances = compliances.OrderBy(entry => entry.ShortDescription).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Sections")
                        {
                            compliances = compliances.OrderBy(entry => entry.Sections).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "RISK")
                        {
                            compliances = compliances.OrderBy(entry => entry.RISK).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "FrequencyName")
                        {
                            compliances = compliances.OrderBy(entry => entry.FrequencyName).ToList();
                        }
                        direction = SortDirection.Descending;
                    }
                    else
                    {
                        if (ViewState["SortExpression"].ToString() == "ID")
                        {
                            compliances = compliances.OrderByDescending(entry => entry.ID).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "ActName")
                        {
                            compliances = compliances.OrderByDescending(entry => entry.ActName).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "ShortDescription")
                        {
                            compliances = compliances.OrderByDescending(entry => entry.ShortDescription).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "Sections")
                        {
                            compliances = compliances.OrderByDescending(entry => entry.Sections).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "RISK")
                        {
                            compliances = compliances.OrderByDescending(entry => entry.RISK).ToList();
                        }
                        if (ViewState["SortExpression"].ToString() == "FrequencyName")
                        {
                            compliances = compliances.OrderByDescending(entry => entry.FrequencyName).ToList();
                        }
                        direction = SortDirection.Ascending;
                    }
                    grdCompliances.DataSource = compliances;
                    grdCompliances.DataBind();

                    Session["grdData"] = (grdCompliances.DataSource as List<SP_GetNotMappedComplianceMaster_Result>).ToDataTable();
                    upCompliancesList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private void BindCompliances()
        {
            try
            {
                int CmType = -1;
                if (rdFunctionBased.Checked)
                    CmType = 0;
                if (rdChecklist.Checked)
                    CmType = 1;

                var compliancesData = Business.ComplianceManagement.GetAll1( Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplianceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, tbxFilter.Text);
                List<object> dataSource = new List<object>();
                foreach (var complianceInfo in compliancesData)
                {
                    string risk = "";
                    if (complianceInfo.RiskType == 0)
                        risk = "High";
                    else if (complianceInfo.RiskType == 1)
                        risk = "Medium";
                    else if (complianceInfo.RiskType == 2)
                        risk = "Low";

                    string ss = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency));
                    dataSource.Add(new
                    {
                        complianceInfo.ID,
                        complianceInfo.ActName,
                        complianceInfo.Sections,
                        complianceInfo.Description,
                        complianceInfo.ComplianceType,
                        complianceInfo.EventID,
                        //NatureOfCompliance = ((NatureOfCompliance)complianceInfo.Key.NatureOfCompliance.Value).ToString(),
                        complianceInfo.UploadDocument,
                        complianceInfo.RequiredForms,
                        Frequency = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1)),
                        complianceInfo.NonComplianceEffects,
                        Risk = risk,
                        complianceInfo.ShortDescription,
                        complianceInfo.SubComplianceType,
                        complianceInfo.CheckListTypeID
                        //Parameters = GetParameters(complianceInfo.Value)
                    });
                }
                grdCompliances.DataSource = dataSource;
                grdCompliances.DataBind();
                upCompliancesList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #region Comment by rahul on 16 march 2016 from view delete
        //private void BindCompliances()
        //{
        //    try
        //    {
        //        int CmType = -1;
        //        if (rdFunctionBased.Checked)
        //            CmType = 0;
        //        if (rdChecklist.Checked)
        //            CmType = 1;

        //        var compliancesData = Business.ComplianceManagement.GetAll(true, Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplinceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, tbxFilter.Text);
        //        List<object> dataSource = new List<object>();
        //        foreach (var complianceInfo in compliancesData)
        //        {
        //            string risk = "";
        //            if (complianceInfo.Key.RiskType == 0)
        //                risk = "High";
        //            else if (complianceInfo.Key.RiskType == 1)
        //                risk = "Medium";
        //            else if (complianceInfo.Key.RiskType == 2)
        //                risk = "Low";

        //            string ss = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Key.Frequency));
        //            dataSource.Add(new
        //            {
        //                complianceInfo.Key.ID,
        //                complianceInfo.Key.ActName,
        //                complianceInfo.Key.Sections,
        //                complianceInfo.Key.Description,
        //                complianceInfo.Key.ComplianceType,
        //                complianceInfo.Key.EventID,
        //                //NatureOfCompliance = ((NatureOfCompliance)complianceInfo.Key.NatureOfCompliance.Value).ToString(),
        //                complianceInfo.Key.UploadDocument,
        //                complianceInfo.Key.RequiredForms,
        //                Frequency = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Key.Frequency != null ? (int)complianceInfo.Key.Frequency : -1)),
        //                complianceInfo.Key.NonComplianceEffects,
        //                Risk = risk,
        //                complianceInfo.Key.ShortDescription,
        //                complianceInfo.Key.SubComplianceType,
        //                complianceInfo.Key.CheckListTypeID
        //                //Parameters = GetParameters(complianceInfo.Value)
        //            });
        //        }
        //        grdCompliances.DataSource = dataSource;
        //        grdCompliances.DataBind();
        //        upCompliancesList.Update();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}
        #endregion
        private string GetParameters(List<ComplianceParameter> parameters)
        {
            try
            {
                StringBuilder paramString = new StringBuilder();
                foreach (var item in parameters)
                {
                    paramString.Append(paramString.Length == 0 ? item.Name : ", " + item.Name);
                }

                return paramString.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        private void BindActs()
        {
            try
            {
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";

                var actlist = ActManagement.GetAllNVP();
                ddlAct.DataSource = actlist;
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindActsFilter(long? ActGroupID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities()) 
            {
                try
                {
                    List<object> actlist = new List<object>();
                    if (Convert.ToInt32(ddlComplianceCatagory.SelectedValue) != -1)
                    {
                        int CategoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                        actlist = (from row in entities.Acts
                                   where row.IsDeleted == false
                                   && row.ComplianceCategoryId == CategoryID
                                   && row.ActGroupId == ActGroupID
                                   select row).ToList<object>();
                    }
                    else
                    {
                        actlist = ActManagement.GetActCategoryWiseCL(ActGroupID);
                    }
                    ddlAct1.DataTextField = "Name";
                    ddlAct1.DataValueField = "ID";

                    ddlAct1.DataSource = actlist;
                    ddlAct1.DataBind();

                    ddlAct1.Items.Insert(0, new ListItem("< Select Act >", "-1"));
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
        private void BindActsFilterAsPerCategory(int CategoryID)
        {
            try
            {
                var actlist = ActManagement.GetActCategoryWise(CategoryID);

                ddlAct1.DataTextField = "Name";
                ddlAct1.DataValueField = "ID";

                ddlAct1.DataSource = actlist;
                ddlAct1.DataBind();

                ddlAct1.Items.Insert(0, new ListItem("< Select Act >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindStatusActs()
        {
            try
            {
                ddlStatusAct.DataTextField = "Name";
                ddlStatusAct.DataValueField = "ID";

                ddlStatusAct.DataSource = ActManagement.GetAllNVP();
                ddlStatusAct.DataBind();

                ddlStatusAct.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                 
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        //protected void chkEventBased_CheckedChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        vivDueDate.Visible = !chkEventBased.Checked;
        //        divEvent.Visible = chkEventBased.Checked;

        //        divComplianceDueDays.Visible = chkEventBased.Checked;
        //        cvfrequency.Enabled = !chkEventBased.Checked;
        //        divFrequency.Visible = !chkEventBased.Checked;
        //        vaDueDate.Enabled = !chkEventBased.Checked;
        //        if (!(chkEventBased.Checked) && ddlComplianceType.SelectedValue.Equals("2") && rbTimebasedTypes.SelectedValue.Equals("0"))
        //        {
        //            divComplianceDueDays.Visible = !chkEventBased.Checked;
        //        }
        //        if (!(chkEventBased.Checked) && ddlComplianceType.SelectedValue.Equals("2") && rbTimebasedTypes.SelectedValue.Equals("1"))
        //        {
        //            divComplianceDueDays.Visible = chkEventBased.Checked;
        //            rfvEventDue.Enabled = chkEventBased.Checked;
        //            vaDueDate.Enabled = chkEventBased.Checked;
        //            cvfrequency.Enabled = !chkEventBased.Checked;
        //            divFrequency.Visible = !chkEventBased.Checked;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        protected bool ViewSchedule(object frequency, object complianceType, object SubComplianceType, object CheckListTypeID)
        {
            try
            {
                if (Convert.ToString(frequency) == "7" || Convert.ToString(frequency) == "8")
                {
                    return false;
                }
                //else if(EventID != null)
                //{
                //    return false;
                //}
                else if (Convert.ToByte(complianceType) == 2)
                {
                    return false;
                }
                else
                {
                    if (Convert.ToByte(complianceType) == 1)
                    {
                        if (Convert.ToInt32(CheckListTypeID) == 1)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
            
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                saveopo.Value = "true";
                if (ViewState["ComplianceID"].ToString() != null && ViewState["ComplianceID"].ToString() != "")
                {
                    bool chkexistsComplianceschedule = Business.ComplianceManagement.ExistsComplianceScheduleAssignment(Convert.ToInt64(ViewState["ComplianceID"]));
                    if (chkexistsComplianceschedule == true)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Compliance assigned cannot be change Calender/Financial Year.";
                    }
                    else
                    {
                        Business.ComplianceManagement.ResetComplianceSchedule(Convert.ToInt32(ViewState["ComplianceID"]));
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "CloseScheduledDialog", "$(\"#divComplianceScheduleDialog\").dialog('close')", true);
                        upSchedulerRepeter.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rbReminderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rbReminderType.SelectedValue.Equals("1"))
                {
                    divForCustome.Visible = true;
                }
                else
                {
                    divForCustome.Visible = false;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //private void BindLocation(int eventID)
        //{
        //    try
        //    {
        //        //int customerID = -1;
        //        //if (AuthenticationHelper.Role == "CADMN")
        //        //{
        //        //    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
        //        //}
        //        var subEvents = EventManagement.GetAllHierarchy(eventID);
        //        if (subEvents[0].Children.Count > 0)
        //        {
        //            divSubEventmode.Visible = true;
        //            foreach (var item in subEvents)
        //            {
        //                TreeNode node = new TreeNode(item.Name, item.ID.ToString());
        //                node.SelectAction = TreeNodeSelectAction.Expand;
        //                BindBranchesHierarchy(node, item);
        //                tvSubEvent.Nodes.Add(node);
        //            }
        //        }
        //        else
        //        {
        //            divSubEventmode.Visible = false;
        //        }

        //        tvSubEvent.CollapseAll();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void ddlEvents_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    tbxSubEvent.Text = "< Select Sub Event >";
        //    tvSubEvent.Nodes.Clear();
        //    if (Convert.ToInt32(ddlEvents.SelectedValue) != -1)
        //    {
        //        divSubEventmode.Visible = true;
        //        BindLocation(Convert.ToInt32(ddlEvents.SelectedValue));
        //    }
        //    else
        //    {
        //        divSubEventmode.Visible = false;
        //    }
        //}

        //protected void tvSubEvent_SelectedNodeChanged(object sender, EventArgs e)
        //{
        //    try
        //    {

        //        if (tvSubEvent.SelectedNode.ChildNodes.Count == 0)
        //        {
        //            //divEventComplianceType.Visible = false;
        //            tbxSubEvent.Text = tvSubEvent.SelectedNode != null ? Regex.Replace(tvSubEvent.SelectedNode.Text, "<.*?>", string.Empty) : "< Select >";
        //            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView1", "$(\"#divSubevent\").hide(\"blind\", null, 500, function () { });", true);
        //            rbEventComplianceType.SelectedValue = "0";
        //        }
        //        else
        //        {
        //            divEventComplianceType.Visible = true;
        //            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView1", "$(\"#divSubevent\").show(\"true\", null, 500, function () { });", true);
        //        }



        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        //protected void TreeViewSelectedNode(string value)
        //{
        //    foreach (TreeNode node in tvSubEvent.Nodes)
        //    {

        //        if (node.ChildNodes.Count > 0)
        //        {
        //            foreach (TreeNode child in node.ChildNodes)
        //            {
        //                if (child.Value == value)
        //                {
        //                    child.Selected = true;
        //                }
        //            }
        //        }
        //        else if (node.Value == value)
        //        {
        //            node.Selected = true;
        //        }
        //    }
        //}

        protected void SelectNodeByValue(TreeNode Node, string ValueToSelect)
        {
            foreach (TreeNode n in Node.ChildNodes)
            {
                if (n.Value == ValueToSelect) { n.Select(); } else { SelectNodeByValue(n, ValueToSelect); }
            }
        }
       

        private void BindIndustry()
        {
            try
            {

                rptIndustry.DataSource = CustomerBranchManagement.GetAllIndustry();
                rptIndustry.DataBind();

                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");

                    if (!chkIndustry.Checked)
                    {
                        chkIndustry.Checked = true;
                    }
                }
                CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                IndustrySelectAll.Checked = true;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindEntityType()
        {
            try
            {
                rptEntityType.DataSource = CustomerBranchManagement.GetAllLegalEntityType();
                rptEntityType.DataBind();

                foreach (RepeaterItem aItem in rptEntityType.Items)
                {
                    CheckBox chkEntityType = (CheckBox)aItem.FindControl("chkEntityType");

                    if (!chkEntityType.Checked)
                    {
                        chkEntityType.Checked = true;
                    }
                }
                CheckBox EntityTypeSelectAll = (CheckBox)rptEntityType.Controls[0].Controls[0].FindControl("EntityTypeSelectAll");
                EntityTypeSelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindIsForSecretarial()
        {
            try
            {
                rptIsForSecretarial.DataSource = CustomerBranchManagement.GetAllIsForSecretarial();
                rptIsForSecretarial.DataBind();

                foreach (RepeaterItem aItem in rptIsForSecretarial.Items)
                {
                    CheckBox chkIsForSecretarial = (CheckBox)aItem.FindControl("chkIsForSecretarial");

                    if (!chkIsForSecretarial.Checked)
                    {
                        chkIsForSecretarial.Checked = true;
                    }
                }
                CheckBox IsForSecretarialSelectAll = (CheckBox)rptIsForSecretarial.Controls[0].Controls[0].FindControl("IsForSecretarialSelectAll");
                IsForSecretarialSelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            saveopo.Value = "true";
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);
        }

        protected void btnSaveDeactivate_Click(object sender, EventArgs e)
        {
            try
            {
                string flag = "A";
                string IsCompliance = "Compliance";
                if (!DeactivationAct.ExistsMappingRLCSorHRProduct("C", Convert.ToInt32(ViewState["ComplianceID"]))) //Check Any Compliance Under this Act tagged to HRProduct/RLCS
                {
                    DateTime Date = DateTime.ParseExact(txtDeactivateDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    Business.ComplianceManagement.ChangeStatus(Convert.ToInt32(ViewState["ComplianceID"]), Date, "D", txtDeactivateDesc.Text);
                    Business.ComplianceManagement.UpdateComplianceDate(Convert.ToInt32(ViewState["ComplianceID"]));
                    if (FileUploadDeactivateDoc.FileBytes != null && FileUploadDeactivateDoc.FileBytes.LongLength > 0)
                    {
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            FileUploadDeactivateDoc.SaveAs(ConfigurationManager.AppSettings["DriveUrl"] + "/DeactiveComplainceFiles/" + FileUploadDeactivateDoc.FileName);
                        }
                        else
                        {
                            FileUploadDeactivateDoc.SaveAs(Server.MapPath("~/DeactiveComplainceFiles/" + FileUploadDeactivateDoc.FileName));
                        }
                        ComplianceDeactive Deact = new ComplianceDeactive()
                        {
                            ComplianceID = Convert.ToInt32(ViewState["ComplianceID"]),
                            Name = FileUploadDeactivateDoc.FileName,
                            FileData = FileUploadDeactivateDoc.FileBytes,
                            FilePath = "~/DeactiveComplianceFiles/" + FileUploadDeactivateDoc.FileName
                        };
                        Business.ComplianceManagement.DeleteOldDeactiveDoc(Convert.ToInt32(ViewState["ComplianceID"]));
                        Business.ComplianceManagement.CreateDeactivateFile(Deact, true);

                    }
                    CreateLogForDeactivation(Convert.ToInt32(ViewState["ComplianceID"]),flag,IsCompliance);
                    var compliancedeactive = Business.ComplianceManagement.GetCompliance(Convert.ToInt32(ViewState["ComplianceID"]));
                    if (compliancedeactive.Status == "D")
                    {
                        string ReplyEmailAddressName = "Avantis";
                        string DeactivateDate = Convert.ToDateTime(compliancedeactive.DeactivateOn).ToString("dd-MM-yyyy");
                        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ComplainceDeactivate
                                            .Replace("@ShortDescription", compliancedeactive.ShortDescription)
                                            .Replace("@DetailDescription", compliancedeactive.Description)
                                            .Replace("@DeactivateDate", DeactivateDate)
                                            .Replace("@Requestor", AuthenticationHelper.User)
                                            .Replace("@DeactivateDescription", compliancedeactive.DeactivateDesc)
                                            .Replace("@From", ReplyEmailAddressName);

                        string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                        string CustomerCreatedEmail = ConfigurationManager.AppSettings["CustomerCreatedEmail"].ToString();

                        EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { CustomerCreatedEmail }), null, null, "Compliance Deactivation Request.", message);
                    }

                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceStatusDialog\").dialog('close')", true);
                    //BindCompliancesNew();
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Selected Compliance tagged to HR Compliance Product, Please contact IT before Compliance De-Activation";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void CreateLogForDeactivation(long complianceid,string flag,string iscompliance)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Log_Deactivation log = new Log_Deactivation()
                {
                    ComplianceID = complianceid,
                    Flag=flag,
                    IsCompliance=iscompliance,
                    CreatedBy=AuthenticationHelper.UserID,
                    CreatedOn=DateTime.Now,
                };
                entities.Log_Deactivation.Add(log);
                entities.SaveChanges();
            }
        }
        protected void ddlNatureOfCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindComplianceSubType(Convert.ToInt32(ddlNatureOfCompliance.SelectedValue));
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlActGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindActsFilter(Convert.ToInt32(ddlActGroup.SelectedValue));
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);
        }

        protected void ddlComplianceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities()) { 
                if (Convert.ToInt32(ddlActGroup.SelectedValue) == -1)
                {
                    BindActsFilterAsPerCategory(Convert.ToInt32(ddlComplianceCatagory.SelectedValue));
                }
                else
                {
                    int ActGroupID = Convert.ToInt32(ddlActGroup.SelectedValue);
                    int CategoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                    var acts = (from row in entities.Acts
                                where row.IsDeleted == false
                                && row.ActGroupId == ActGroupID
                                && row.ComplianceCategoryId == CategoryID
                                orderby row.Name ascending
                                select new { ID = row.ID, Name = row.Name }).OrderBy(entry => entry.Name).ToList<object>();

                    ddlAct1.DataTextField = "Name";
                    ddlAct1.DataValueField = "ID";

                    ddlAct1.DataSource = acts;
                    ddlAct1.DataBind();

                    ddlAct1.Items.Insert(0, new ListItem("< Select Act >", "-1"));
                }
            }
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);
        }

        protected void ddlAct1_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdCompliances.DataSource = null;
            grdCompliances.DataBind();
            if (Convert.ToInt32(ddlAct1.SelectedValue) != -1)
            {
                BindCompliancesNew();
            }
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);
        }

        protected void ddlFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlFrequency.SelectedValue == "7" || ddlFrequency.SelectedValue == "8")
                {
                    vivDueDate.Visible = false;
                }
                else
                {
                    vivDueDate.Visible = true;
                }

                if (ddlFrequency.SelectedValue == "8")
                {
                    vaDueWeekDay.Enabled = true;
                    vivWeekDueDays.Visible = true;
                }
                else
                {
                    vaDueWeekDay.Enabled = false;
                    vivWeekDueDays.Visible = false;
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "valuesJqScript", "ResolveUrl();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rdoComplianceVisible_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdoComplianceVisible.Checked == true)
                {
                    ddlChklstType.SelectedValue = "0";
                    divChecklist.Visible = true;
                    divOneTime.Visible = true;

                    divNonComplianceType.Visible = true;
                    divNatureOfCompliance.Visible = false;
                    divComplianceSubType.Visible = false;
                }
                else
                {
                    divChecklist.Visible = false;
                    divOneTime.Visible = false;
                    divTimebasedTypes.Visible = false;

                    divFrequency.Visible = false;
                    vivDueDate.Visible = false;

                    divNonComplianceType.Visible = true;
                    divNatureOfCompliance.Visible = false;
                    divComplianceSubType.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdSampleForm_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DownloadSampleForm"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int complianceID = Convert.ToInt32(commandArgs[1]);
                    int ID = Convert.ToInt32(commandArgs[0]);
                    var file = Business.ComplianceManagement.GetSelectedComplianceFileName(ID, complianceID);
                    Response.Buffer = true;

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename= " + file.Name);
                    Response.BinaryWrite(file.FileData);
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                }
                else if (e.CommandName.Equals("DeleleSampleForm"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int complianceID = Convert.ToInt32(commandArgs[1]);
                    int ID = Convert.ToInt32(commandArgs[0]);
                    Business.ComplianceManagement.DeleteComplianceForm(ID, complianceID);
                    var complianceForm = Business.ComplianceManagement.GetMultipleComplianceFormByID(complianceID);
                    grdSampleForm.DataSource = complianceForm;
                    grdSampleForm.DataBind();
                    upCompliancesList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlAct.SelectedValue) && Convert.ToString(ddlAct.SelectedValue) != "-1")
                {
                    Act act = Business.ComplianceManagement.GetStartDateFrAct(Convert.ToInt32(ddlAct.SelectedValue));

                    if (act != null && act.ShortForm != "" && act.ShortForm != null)
                        txtshortform.Text = act.ShortForm;
                    else
                        txtshortform.Text = string.Empty;

                    if (act != null && act.ID != -1 && act.StartDate != null)
                        tbxStartDate.Text = Convert.ToDateTime(act.StartDate).ToString("dd-MM-yyyy");
                    else
                        tbxStartDate.Text = string.Empty;

                    if (act != null && act.duedatetype != -1 && act.duedatetype != null)
                        ddlDueDateType.SelectedValue = act.duedatetype.Value.ToString();

                    ddlDueDateType.SelectedValue = Convert.ToString(act.duedatetype);


                    if (act.LocationTypeID.HasValue)
                    {
                        ddlLocationType.SelectedValue = Convert.ToString(act.LocationTypeID);
                    }

                    #region Industry
                    var vGetIndustryMappedIDs = ActManagement.GetActIndustryMappedID(act.ID);

                    foreach (RepeaterItem aItem in rptIndustry.Items)
                    {
                        CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                        chkIndustry.Checked = false;
                        CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");

                        for (int i = 0; i <= vGetIndustryMappedIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblIndustryID")).Text.Trim() == vGetIndustryMappedIDs[i].ToString())
                                chkIndustry.Checked = true;
                        }

                        if ((rptIndustry.Items.Count) == (vGetIndustryMappedIDs.Count))
                            IndustrySelectAll.Checked = true;
                        else
                            IndustrySelectAll.Checked = false;
                    }
                    #endregion

                    #region Company Type
                    txtCompanyType.Text = "< Select >";
                    var vGetActCompanyTypeIDs = ActManagement.GetActCompanyTypeMappedID(act.ID);
                    foreach (RepeaterItem aItem in rptCompanyType.Items)
                    {
                        CheckBox chkCompanyType = (CheckBox)aItem.FindControl("chkCompanyType");
                        chkCompanyType.Checked = false;
                        CheckBox CompanyTypeSelectAll = (CheckBox)rptCompanyType.Controls[0].Controls[0].FindControl("CompanyTypeSelectAll");

                        for (int i = 0; i <= vGetActCompanyTypeIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblCompanyTypeID")).Text.Trim() == vGetActCompanyTypeIDs[i].ToString())
                                chkCompanyType.Checked = true;
                        }
                        if ((rptCompanyType.Items.Count) == (vGetActCompanyTypeIDs.Count))
                            CompanyTypeSelectAll.Checked = true;
                    }
                    #endregion


                    #region Business Activity Type
                    txtBusinessActivityType.Text = "< Select >";
                    var vGetActBusinessActivityIDs = ActManagement.GetActBusinessActivityMappedID(act.ID);
                    foreach (RepeaterItem aItem in rptBusinessActivityType.Items)
                    {
                        CheckBox chkBusinessActivityType = (CheckBox)aItem.FindControl("chkBusinessActivityType");
                        chkBusinessActivityType.Checked = false;
                        CheckBox BusinessActivityTypeSelectAll = (CheckBox)rptBusinessActivityType.Controls[0].Controls[0].FindControl("BusinessActivityTypeSelectAll");

                        for (int i = 0; i <= vGetActBusinessActivityIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblBusinessActivityTypeID")).Text.Trim() == vGetActBusinessActivityIDs[i].ToString())
                                chkBusinessActivityType.Checked = true;
                        }
                        if ((rptBusinessActivityType.Items.Count) == (vGetActBusinessActivityIDs.Count))
                            BusinessActivityTypeSelectAll.Checked = true;
                    }
                    #endregion


                    #region Business Activity Type
                    txtActApplicability.Text = "< Select >";
                    var vGetActApplicabilityIDs = ActManagement.GetActApplicabilityMappedID(act.ID);
                    foreach (RepeaterItem aItem in rptActApplicability.Items)
                    {
                        CheckBox chkActApplicability = (CheckBox)aItem.FindControl("chkActApplicability");
                        chkActApplicability.Checked = false;
                        CheckBox ActApplicabilitySelectAll = (CheckBox)rptActApplicability.Controls[0].Controls[0].FindControl("ActApplicabilitySelectAll");

                        for (int i = 0; i <= vGetActBusinessActivityIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblActApplicabilityID")).Text.Trim() == vGetActApplicabilityIDs[i].ToString())
                                chkActApplicability.Checked = true;
                        }
                        if ((rptActApplicability.Items.Count) == (vGetActApplicabilityIDs.Count))
                            ActApplicabilitySelectAll.Checked = true;
                    }
                    #endregion


                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }



        #region Added by rahul on 18 MAy 2020
        protected void btnRepeaterCompanyType_Click(object sender, EventArgs e)
        {
            //divSubIndustry.Visible = true;
            //saveopo.Value = "true";
            CompanyTypeList.Clear();
            foreach (RepeaterItem aItem in rptCompanyType.Items)
            {
                CheckBox chk = (CheckBox)aItem.FindControl("chkCompanyType");
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (chk.Checked)
                    {
                        int CompanyTypeID = Convert.ToInt32(((Label)aItem.FindControl("lblCompanyTypeID")).Text.Trim());
                        CompanyTypeList.Add(CompanyTypeID);
                    }

                }
            }
        }
        protected void btnBusinessActivityType_Click(object sender, EventArgs e)
        {
            //divSubIndustry.Visible = true;
            //saveopo.Value = "true";
            BusinessActivityTypeist.Clear();
            foreach (RepeaterItem aItem in rptBusinessActivityType.Items)
            {
                CheckBox chk = (CheckBox)aItem.FindControl("chkBusinessActivityType");
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (chk.Checked)
                    {
                        int CompanyTypeID = Convert.ToInt32(((Label)aItem.FindControl("lblBusinessActivityTypeID")).Text.Trim());
                        BusinessActivityTypeist.Add(CompanyTypeID);
                    }

                }
            }
        }
        protected void btnActApplicability_Click(object sender, EventArgs e)
        {
            //divSubIndustry.Visible = true;
            //saveopo.Value = "true";
            ActApplicability.Clear();
            foreach (RepeaterItem aItem in rptActApplicability.Items)
            {
                CheckBox chk = (CheckBox)aItem.FindControl("chkActApplicability");
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (chk.Checked)
                    {
                        int CompanyTypeID = Convert.ToInt32(((Label)aItem.FindControl("lblActApplicabilityID")).Text.Trim());
                        ActApplicability.Add(CompanyTypeID);
                    }

                }
            }
        }
        public static List<CompanyType> GetAllCompanyType()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var industries = entities.CompanyTypes.Where(a => a.ID != 4).ToList(); ;

                return industries.OrderBy(entry => entry.Name).ToList();
            }
        }
        public static List<BusinessActivityType> GetAllBusinessActivityType()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var industries = entities.BusinessActivityTypes.ToList();

                return industries.OrderBy(entry => entry.Name).ToList();
            }
        }

        public static List<LocationType> GetAllLocationType()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var industries = entities.LocationTypes.ToList();

                return industries.OrderBy(entry => entry.Name).ToList();
            }
        }


        public static List<Act_Applicability> GetAllAct_Applicability()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var industries = entities.Act_Applicability.ToList();

                return industries.OrderBy(entry => entry.Name).ToList();
            }
        }

        private void BindCompanyType()
        {
            try
            {
                rptCompanyType.DataSource = GetAllCompanyType();
                rptCompanyType.DataBind();

                foreach (RepeaterItem aItem in rptCompanyType.Items)
                {
                    CheckBox chkCompanyType = (CheckBox)aItem.FindControl("chkCompanyType");

                    if (!chkCompanyType.Checked)
                    {
                        chkCompanyType.Checked = true;
                    }
                }
                CheckBox CompanyTypeSelectAll = (CheckBox)rptCompanyType.Controls[0].Controls[0].FindControl("CompanyTypeSelectAll");
                CompanyTypeSelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindBusinessActivityType()
        {
            try
            {
                rptBusinessActivityType.DataSource = GetAllBusinessActivityType();
                rptBusinessActivityType.DataBind();

                foreach (RepeaterItem aItem in rptBusinessActivityType.Items)
                {
                    CheckBox chkBusinessActivityType = (CheckBox)aItem.FindControl("chkBusinessActivityType");

                    if (!chkBusinessActivityType.Checked)
                    {
                        chkBusinessActivityType.Checked = true;
                    }
                }
                CheckBox BusinessActivityTypeSelectAll = (CheckBox)rptBusinessActivityType.Controls[0].Controls[0].FindControl("BusinessActivityTypeSelectAll");
                BusinessActivityTypeSelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationType()
        {
            try
            {
                ddlLocationType.DataTextField = "Name";
                ddlLocationType.DataValueField = "ID";

                ddlLocationType.DataSource = GetAllLocationType();
                ddlLocationType.DataBind();

                ddlLocationType.Items.Insert(0, new ListItem("< Select Location Type>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindAct_Applicability()
        {
            try
            {
                rptActApplicability.DataSource = GetAllAct_Applicability();
                rptActApplicability.DataBind();

                foreach (RepeaterItem aItem in rptActApplicability.Items)
                {
                    CheckBox chkActApplicability = (CheckBox)aItem.FindControl("chkActApplicability");

                    if (!chkActApplicability.Checked)
                    {
                        chkActApplicability.Checked = true;
                    }
                }
                CheckBox ActApplicabilitySelectAll = (CheckBox)rptActApplicability.Controls[0].Controls[0].FindControl("ActApplicabilitySelectAll");
                ActApplicabilitySelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("NotMappingComplianceList");
                        DataTable ExcelData = null;
                        DateTime CurrentDate = DateTime.Today.Date;
                        DataView view = new System.Data.DataView((DataTable)Session["grdData"]);

                        ExcelData = view.ToTable("Selected", false, "ID", "ActName", "ShortDescription",
                            "Sections", "Description", "FrequencyName", "RISK", "SubComplianceType");

                        exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);
                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A1"].Value = "ComplianceID";
                        exWorkSheet.Cells["A1"].AutoFitColumns(20);

                        exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B1"].Value = "ActName";
                        exWorkSheet.Cells["B1"].AutoFitColumns(50);

                        exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C1"].Value = "Short Description";
                        exWorkSheet.Cells["C1"].AutoFitColumns(50);

                        exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D1"].Value = "Sections";
                        exWorkSheet.Cells["D1"].AutoFitColumns(30);


                        exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E1"].Value = "Description";
                        exWorkSheet.Cells["E1"].AutoFitColumns(50);

                        exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F1"].Value = "Frequency";
                        exWorkSheet.Cells["F1"].AutoFitColumns(30);

                        exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G1"].Value = "RiskType";
                        exWorkSheet.Cells["G1"].AutoFitColumns(20);

                        exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["H1"].Value = "ComplianceType";
                        exWorkSheet.Cells["H1"].AutoFitColumns(30); 

                        using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 8])
                        {
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=NotMappingComplianceList.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                    catch (Exception)
                    {
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }

}