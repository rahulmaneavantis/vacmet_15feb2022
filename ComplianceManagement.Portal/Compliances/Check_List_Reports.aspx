﻿<%@ Page Title="Canned Reports" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true"
    CodeBehind="Check_List_Reports.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.Check_List_Reports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <div style="float: right; margin-top: 10px; margin-right: 7px; margin-top: 5px;">
                <asp:LinkButton runat="server" ID="lbtnExportExcel" Style="margin-top: 15px; margin-top: -5px;" OnClick="lbtnExportExcel_Click"><img src="../Images/excel.png" alt="Export to Excel"
                  title="Export to Excel" width="30px" height="30px"/></asp:LinkButton>
            </div>
            <div style="width: 100%">
                 <div id="divcustomer"  runat="server" visible="false" style="float: left; margin-top: 5px;margin-left: 15px;margin-top: 2px;">
                     <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; ">
                        Select Customer:</label>
                    <asp:DropDownList runat="server" ID="ddlcustomer" Height="22px" Width="220px" margin-left="7px" CssClass="txtbox" OnSelectedIndexChanged="ddlcustomer_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                </div>
                <div id="FilterLocationdiv" runat="server" style="margin-left: 378px; margin-top: 5px;">
                    <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                        Select Location:</label>
                    <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="txtbox" />
                    <div style="margin-left: 100px; position: absolute; z-index: 10" id="divFilterLocation">
                        <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                            BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="390px"
                            Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                        </asp:TreeView>
                    </div>
                </div>
                <div id="divNotAssigendCompliances" style="margin-top: 10px; margin-bottom: 10px">
                    <asp:GridView runat="server" ID="grdCheckListReport" AutoGenerateColumns="false" AllowSorting="true"
                        GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" OnRowCreated="grdCheckListReport_RowCreated"
                        BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="14" OnSorting="grdCheckListReport_Sorting"
                        Width="100%" Font-Size="12px" OnPageIndexChanging="grdCheckListReport_PageIndexChanging">
                          <Columns>
                            <asp:BoundField HeaderText="ComplianceID" HeaderStyle-Width="10%" DataField="ComplianceID" SortExpression="ComplianceID" />
                            <asp:TemplateField HeaderText="Branch Name" ItemStyle-Width="10%" SortExpression="Branch">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                        <asp:Label ID="lblCustomerBranchName" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Act Name" ItemStyle-Width="10%" SortExpression="ActName" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                        <asp:Label ID="lblActName" runat="server" Text='<%# Eval("ActName") %>' ToolTip='<%# Eval("ActName") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ShortDescription" ItemStyle-Width="10%" SortExpression="ShortDescription" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                        <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Section" ItemStyle-Width="10%" SortExpression="Sections" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                        <asp:Label ID="lblSections" runat="server" Text='<%# Eval("Section") %>' ToolTip='<%# Eval("Section") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Risk" ItemStyle-Width="10%" SortExpression="Risk" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                        <asp:Label ID="lblRisk" runat="server" Text='<%# Eval("Risk") %>' ToolTip='<%# Eval("Risk") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderText="ScheduleType" ItemStyle-Width="10%" SortExpression="ScheduleType" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                        <asp:Label ID="lblScheduleType" runat="server" Text='<%# Eval("ScheduleType") %>' ToolTip='<%# Eval("ScheduleType") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Frequency" ItemStyle-Width="10%" SortExpression="Frequency" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                        <asp:Label ID="lblFrequency" runat="server" Text='<%# Eval("Frequency") %>' ToolTip='<%# Eval("Frequency") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Performer" ItemStyle-Width="10%" SortExpression="Performer" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                        <asp:Label ID="lblPerformer" runat="server" Text='<%# Eval("Performer") %>' ToolTip='<%# Eval("Performer") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Reviewer" ItemStyle-Width="10%" SortExpression="Reviewer" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                        <asp:Label ID="lblReviewer" runat="server" Text='<%# Eval("Reviewer") %>' ToolTip='<%# Eval("Reviewer") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Start Date" ItemStyle-Width="10%" SortExpression="StartDate" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                           <%# Eval("StartDate")!= null?((DateTime)Eval("StartDate")).ToString("dd-MMM-yyyy"):""%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                              <asp:TemplateField HeaderText="DueDate" ItemStyle-Width="10%" SortExpression="DueDate" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                        <asp:Label ID="lblDueDate" runat="server" Text='<%# Eval("DueDate") %>' ToolTip='<%# Eval("DueDate") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <%-- <asp:TemplateField HeaderText="Penalty" ItemStyle-Width="10%" SortExpression="Penalty" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                        <asp:Label ID="lblPenalty" runat="server" Text='<%# Eval("Penalty") %>' ToolTip='<%# Eval("Penalty") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                        <PagerSettings Position="Top" />
                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                        <AlternatingRowStyle BackColor="#E6EFF7" />
                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>

            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>
</asp:Content>
