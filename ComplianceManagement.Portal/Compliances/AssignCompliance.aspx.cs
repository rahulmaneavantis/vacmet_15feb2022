﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Collections;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Event
{
    public partial class AssignCompliance : System.Web.UI.Page
    {
        public static List<long> locationList = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "ComplianceID";
                divFilterUsers.Visible = true;
                FilterLocationdiv.Visible = true;
                btnAddComplianceType.Visible = true;
                btnAddComplianceType.Visible = true;
                BindLocationFilter();
                BindComplianceInstances();
                BindComplianceCategories();
                BindTypes();
                BindUsers(ddlUsers);
                BindUsers(ddlFilterUsers);
                BindLocation();
                BindActList();
                tbxBranch.Attributes.Add("readonly", "readonly");
                txtactList.Attributes.Add("readonly", "readonly");
                AddFilter();
                tbxFilterLocation.Text = "< Select >";
                //int nCustomerBranchID = -1;
                //BindGrid(nCustomerBranchID);

            }
        }

        //sandesh code start

        protected void btnlocation_Click(object sender, EventArgs e)
        {
            try
            {
                BindComplianceInstances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }

        protected void btnClear1_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvFilterLocation.Nodes[i]);
            }
        }

        protected void ChkBoxClear(TreeNode node)
        {

            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                node.Checked = false;
            }
            foreach (TreeNode tn in node.ChildNodes)
            {

                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)              
                {
                    tn.Checked = false;
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        ChkBoxClear(tn.ChildNodes[i]);
                    }
                }
            }
        }

        protected void RetrieveNodes(TreeNode node)
        {
            try
            {
                if (node.Checked)
                {
                    if (Convert.ToInt32(node.Value) != AuthenticationHelper.CustomerID)
                    {
                        if (!locationList.Contains(Convert.ToInt32(node.Value)))
                        {
                            locationList.Add(Convert.ToInt32(node.Value));
                        }
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i]);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (Convert.ToInt32(tn.Value) != AuthenticationHelper.CustomerID)
                            {
                                if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                                {
                                    locationList.Add(Convert.ToInt32(tn.Value));
                                }
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

     

        //sandesh code end
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            //BindComplianceMatrix("N", "N");

            if (chkEvent.Checked == true)
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "Y");
                }
                else
                {
                    BindComplianceMatrix("Y", "Y");
                }
            }
            else
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "N");
                }
                else
                {
                    BindComplianceMatrix("Y", "N");
                }
            }
            if ((!string.IsNullOrEmpty(tbxStartDate.Text)) && tbxStartDate.Text != null)
            {
                setDateToGridView();
            }
        }
      
        private void BindTypes()
        {
            try
            {
                ddlComplianceType.DataTextField = "Name";
                ddlComplianceType.DataValueField = "ID";

                ddlComplianceType.DataSource = ComplianceTypeManagement.GetAll();
                ddlComplianceType.DataBind();

                ddlComplianceType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }     
        private void BindComplianceInstances(int pageIndex = 0, bool isBranchChanged = false)
        {
            try
            {
                int branchID = -1;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                int userID = -1;
                if ((!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue)))
                {
                    userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }

                if ((Convert.ToString(ViewState["pagefilter"]).Equals("self") && Convert.ToString(ViewState["pagefilter"]) != ""))
                {
                    userID = Convert.ToInt32(AuthenticationHelper.UserID);
                }
                locationList.Clear();

                for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                {
                    RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                }
                List<ComplianceAssignedInstancesView> dataSource = new List<ComplianceAssignedInstancesView>();
                if (locationList.Count > 0)
                {
                    dataSource = Business.ComplianceManagement.GetAllAssignedInstanceslist(userID, Convert.ToInt32(AuthenticationHelper.CustomerID), locationList);
                }
                else
                {
                    dataSource = Business.ComplianceManagement.GetAllAssignedInstances(branchID, userID, Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID));
                }                 
                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "Branch")
                    {
                        dataSource = dataSource.OrderBy(entry => entry.Branch).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ShortDescription")
                    {
                        dataSource = dataSource.OrderBy(entry => entry.ShortDescription).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "User")
                    {
                        dataSource = dataSource.OrderBy(entry => entry.User).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Role")
                    {
                        dataSource = dataSource.OrderBy(entry => entry.Role).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ScheduledOn")
                    {
                        dataSource = dataSource.OrderBy(entry => entry.ScheduledOn).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ComplianceID")
                    {
                        dataSource = dataSource.OrderBy(entry => entry.ComplianceID).ToList();
                    }
                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "Branch")
                    {
                        dataSource = dataSource.OrderByDescending(entry => entry.Branch).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ShortDescription")
                    {
                        dataSource = dataSource.OrderByDescending(entry => entry.ShortDescription).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "User")
                    {
                        dataSource = dataSource.OrderByDescending(entry => entry.User).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Role")
                    {
                        dataSource = dataSource.OrderByDescending(entry => entry.Role).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ScheduledOn")
                    {
                        dataSource = dataSource.OrderByDescending(entry => entry.ScheduledOn).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ComplianceID")
                    {
                        dataSource = dataSource.OrderByDescending(entry => entry.ComplianceID).ToList();
                    }
                    direction = SortDirection.Ascending;
                }
                grdComplianceType.PageIndex = pageIndex;
                grdComplianceType.DataSource = dataSource;
                grdComplianceType.DataBind();

                if (isBranchChanged)
                {
                    if (branchID != -1)
                    {
                        BindUsers(ddlFilterUsers, dataSource.Select(entry => entry.UserID).Distinct().ToList());
                    }
                    else
                    {
                        BindUsers(ddlFilterUsers);
                    }

                    ddlFilterUsers.SelectedValue = "-1";
                }
                
                upComplianceTypeList.Update();
                ForceCloseFilterBranchesTreeView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? Regex.Replace(tvBranches.SelectedNode.Text, "<.*?>", string.Empty) : "< Select >";
                TreeviewTextHighlightrd(Convert.ToInt32(tvBranches.SelectedNode.Value));
                grdComplianceRoleMatrix.PageIndex = 0;
                BindComplianceMatrix("N", "N");
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindActList();
            grdComplianceRoleMatrix.PageIndex = 0;
            //BindComplianceMatrix("N", "N");
            if (chkEvent.Checked == true)
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "Y");
                }
                else
                {
                    BindComplianceMatrix("Y", "Y");
                }
            }
            else
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "N");
                }
                else
                {
                    BindComplianceMatrix("Y", "N");
                }
            }
            Filtercompliance.Visible = true;
        }

        protected void ddlComplianceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindActList();
            grdComplianceRoleMatrix.PageIndex = 0;
            //BindComplianceMatrix("N", "N");//comment by rahul on 22 FEB 2016
            if (chkEvent.Checked == true)
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "Y");
                }
                else
                {
                    BindComplianceMatrix("Y", "Y");
                }
            }
            else
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "N");
                }
                else
                {
                    BindComplianceMatrix("Y", "N");
                }
            }
            Filtercompliance.Visible = true;
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            // BindComplianceMatrix("Y","N"); //comment by rahul on 22 FEB 2016
            if (chkEvent.Checked == true)
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "Y");
                }
                else
                {
                    BindComplianceMatrix("Y", "Y");
                }
            }
            else
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "N");
                }
                else
                {
                    BindComplianceMatrix("Y", "N");
                }
            }
        }
        protected void chkEvent_CheckedChanged(object sender, EventArgs e)
        {

            if (chkEvent.Checked == true)
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "Y");
                }
                else
                {
                    BindComplianceMatrix("Y", "Y");
                }
            }
            else
            {
                if (tbxFilter.Text.Trim() == "")
                {
                    BindComplianceMatrix("N", "N");
                }
                else
                {
                    BindComplianceMatrix("Y", "N");
                }
            }
        }
        private void BindComplianceMatrix(string flag,string IFCheckedEvent)
        {
            try
            {
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int complianceTypeID = Convert.ToInt32(ddlComplianceType.SelectedValue);
                int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                int branchID = -1;
                if (!(tbxBranch.Text.Trim().Equals("< Select >")))
                {
                    branchID = CustomerBranchManagement.GetByName(tbxBranch.Text.Trim(), customerID).ID;
                }              
                List<int> actIds = new List<int>();
                foreach (RepeaterItem aItem in rptActList.Items)
                {
                    CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                    if (chkAct.Checked)
                    {
                        actIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim()));
                    }
                }

                if (complianceTypeID != -1 || complianceCatagoryID != -1 || actIds.Count!=0)
                {
                    if (flag == "N" && IFCheckedEvent=="N")
                    {
                        grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.GetByType(complianceTypeID, complianceCatagoryID, cbApprover.Checked, branchID, actIds);
                    }
                    else if (flag == "N" && IFCheckedEvent == "Y")
                    {
                        grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.GetByTypeFilterEvent(complianceTypeID, complianceCatagoryID,"Y", cbApprover.Checked, branchID, actIds);
                    }
                    else if (flag == "Y" && IFCheckedEvent == "N")
                    {
                        //GetByTypeFilterEvent
                        grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.GetByType(complianceTypeID, complianceCatagoryID, cbApprover.Checked, branchID, actIds, tbxFilter.Text.Trim());
                    }
                    else if (flag == "Y" && IFCheckedEvent == "Y")
                    {
                        grdComplianceRoleMatrix.DataSource = Business.ComplianceManagement.GetByTypeFilterEvent(complianceTypeID, complianceCatagoryID, "Y", cbApprover.Checked, branchID, actIds, tbxFilter.Text.Trim());
                    }
                    grdComplianceRoleMatrix.DataBind();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
            
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", string.Format("initializeConfirmDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker(null);", true);
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeActList", string.Format("initializeJQueryUI('{0}', 'dvActList');", txtactList.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeConfirmDatePicker", "initializeConfirmDatePicker();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocation()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvBranches.Nodes.Add(node);
                }

                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLocationFilter()
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                //TreeNode node = new TreeNode("< All >", "-1");
                //node.Selected = true;
                //tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
              
                //int nCustomerBranchID = -1;
                //BindGrid(nCustomerBranchID);
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            BindComplianceInstances(isBranchChanged: true);
        }

        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }

        private void BindUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                ddlUserList.DataTextField = "Name";
                ddlUserList.DataValueField = "ID";
                ddlUserList.Items.Clear();
                
                var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: false);
                users.Insert(0, new { ID = -1, Name = ddlUserList == ddlUsers ? "< Select >" : "< All >" });

                ddlUserList.DataSource = users;
                ddlUserList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComplianceInstances();
        }

        protected void ddlUsers_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                User user = UserManagement.GetByID(Convert.ToInt32(ddlUsers.SelectedValue));
                int? branch=null;
                if (user.CustomerBranchID != null)
                {
                    branch = user.CustomerBranchID;

                     var customerBranch = CustomerBranchManagement.GetByID(Convert.ToInt64(user.CustomerBranchID));
                     tbxBranch.Text  = customerBranch.Name;
                }
                else
                {
                    tbxBranch.Text = "< Select >";
                }
                TreeviewTextHighlightrd(branch);
                ddlComplianceType_SelectedIndexChanged(sender, e);
            }
            catch (Exception)
            {

            }
           
        }

        public IEnumerable<TreeNode> GetChildren(TreeNode Parent)
        {
            return Parent.ChildNodes.Cast<TreeNode>().Concat(
                   Parent.ChildNodes.Cast<TreeNode>().SelectMany(GetChildren));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                var complianceList = new List<ComplianceAsignmentProperties>();
                SaveCheckedValues();
                complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                List<Tuple<ComplianceInstance, ComplianceAssignment>> assignments = new List<Tuple<ComplianceInstance, ComplianceAssignment>>();
                ComplianceAsignmentProperties rmdata = new ComplianceAsignmentProperties();
                long userID = Convert.ToInt64(ddlUsers.SelectedValue);
                //int branchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                int branchID = CustomerBranchManagement.GetByName(tbxBranch.Text.Trim(), customerID).ID;
                Dictionary<int, DateTime> startDates = new Dictionary<int, DateTime>();
                grdComplianceRoleMatrix.AllowPaging = false;
                BindComplianceMatrix("N", "N");
                grdComplianceRoleMatrix.DataBind();

                for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
                {
                    GridViewRow row = grdComplianceRoleMatrix.Rows[i];
                    int complianceID = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[row.RowIndex]["ID"]);
                    DateTime scheduledOn;
                        if (complianceList != null)
                        {
                            rmdata = complianceList.Where(t => t.ComplianceId == complianceID).FirstOrDefault();
                        }
                        else
                        {
                            rmdata = null;
                        }

                        if (rmdata != null)
                        {
                            scheduledOn = DateTime.ParseExact(rmdata.StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        }
                        else
                        {

                            if (!string.IsNullOrEmpty(tbxStartDate.Text))
                            {
                                scheduledOn = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                TextBox txt = (TextBox)grdComplianceRoleMatrix.Rows[i].FindControl("txtStartDate");
                                scheduledOn = DateTime.ParseExact(txt.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                        }
                     
                        startDates.Add(complianceID, scheduledOn);
                }

                grdComplianceRoleMatrix.AllowPaging = true;
                BindComplianceMatrix("N", "N");
                grdComplianceRoleMatrix.DataBind();
               
                if (cbApprover.Checked)
                {
                    startDates.ToList().ForEach(entry =>
                    {
                        ComplianceInstance instance = new ComplianceInstance();
                        instance.ComplianceId = entry.Key;
                        instance.CustomerBranchID = branchID;
                        instance.ScheduledOn = entry.Value;

                        ComplianceAssignment assignment = new ComplianceAssignment();
                        assignment.UserID = userID;
                        assignment.RoleID = RoleManagement.GetByCode("APPR").ID;

                        if (!(entry.Value.Date.Equals(DateTime.MinValue)))
                        {
                            assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment));
                        }
                    });
                }
                else
                {
                    if (complianceList!=null)
                    {
                        for (int i = 0; i < complianceList.Count; i++)
                        {

                            ComplianceInstance instance = new ComplianceInstance();
                            instance.ComplianceId = complianceList[i].ComplianceId;
                            instance.CustomerBranchID = branchID;
                            instance.ScheduledOn = DateTime.ParseExact(complianceList[i].StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                            if (complianceList[i].Performer)
                            {

                                ComplianceAssignment assignment = new ComplianceAssignment();
                                assignment.UserID = userID;
                                assignment.RoleID = RoleManagement.GetByCode("PERF").ID;
                                assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment));
                            }
                            if (complianceList[i].Reviewer1)
                            {

                                ComplianceAssignment assignment1 = new ComplianceAssignment();
                                assignment1.UserID = userID;
                                assignment1.RoleID = RoleManagement.GetByCode("RVW1").ID;
                                assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment1));
                            }

                            //if (complianceList[i].Reviewer2)
                            //{
                            //    ComplianceAssignment assignment2 = new ComplianceAssignment();
                            //    assignment2.UserID = userID;
                            //    assignment2.RoleID = RoleManagement.GetByCode("RVW2").ID;
                            //    assignments.Add(new Tuple<ComplianceInstance, ComplianceAssignment>(instance, assignment2));
                            //}
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please select at least one role for Compliance.";
                        setDateToGridView();
                    }
                }
                if (assignments.Count != 0)
                {
                    Business.ComplianceManagementComplianceScheduleon.CreateInstances(assignments, AuthenticationHelper.UserID, AuthenticationHelper.User, AuthenticationHelper.CustomerID);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignComplianceDialog\").dialog('close');", true);
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select at least one role for Compliance.";
                    setDateToGridView();
                }
                ViewState["CHECKED_ITEMS"] = null;
                BindComplianceInstances();
            }
            catch (Exception ex)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "This kind of assignment is not supported in current version."; 
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAddComplianceType_Click(object sender, EventArgs e)
        {
            try
            {
                if (tvBranches.SelectedNode != null)
                {
                    tvBranches.SelectedNode.Selected = false;
                }
                tbxFilter.Text = "";
                Filtercompliance.Visible = false;
                ViewState["CHECKED_ITEMS"] = null;
                ddlUsers.SelectedValue = "-1";
                ddlComplianceType.SelectedValue = "-1";
                ddlComplianceCatagory.SelectedValue = "-1";
                tbxStartDate.Text = DateTime.Now.ToString("dd-MM-yyyy");
                grdComplianceRoleMatrix.DataSource = null;
                grdComplianceRoleMatrix.DataBind();
                cbApprover.Checked = false; ;
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignComplianceDialog\").dialog('open');", true);
                tbxBranch.Text = "< Select >";
                txtactList.Text = "< Select >";
                ForceCloseFilterBranchesTreeView();
                chkEvent.Checked = false;
                upCompliance.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceType_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindComplianceInstances(pageIndex: e.NewPageIndex);
        }

        protected void AddFilter(int pageIndex = 0)
        {
            try
            {
               ViewState["pagefilter"] = Convert.ToString(Request.QueryString["Param"]);
               if (ViewState["pagefilter"] != null)
               {
                   if (Convert.ToString(ViewState["pagefilter"]).Equals("location"))
                   {
                       divFilterUsers.Visible = false;
                       FilterLocationdiv.Visible = true;
                   }
                   else if (Convert.ToString(ViewState["pagefilter"]).Equals("user"))
                   {
                       divFilterUsers.Visible = true;
                       FilterLocationdiv.Visible = false;
                   }
                   else
                   {
                       divFilterUsers.Visible = false;
                       FilterLocationdiv.Visible = false;
                       if (AuthenticationHelper.Role == "EXCT")
                       {
                           btnAddComplianceType.Visible = false;
                       }

                       BindComplianceInstances();
                   }
               }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdComplianceType_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

               
                int branchID = -1;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                int userID = -1;
                if ((!string.IsNullOrEmpty(ddlFilterUsers.SelectedValue)))
                {
                    userID = Convert.ToInt32(ddlFilterUsers.SelectedValue);
                }

                if ((Convert.ToString(ViewState["pagefilter"]).Equals("self") && Convert.ToString(ViewState["pagefilter"]) != ""))
                {
                    userID = Convert.ToInt32(AuthenticationHelper.UserID);
                }

                var assignmentList = Business.ComplianceManagement.GetAllAssignedInstances(branchID, userID, Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID));
                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }

                foreach (DataControlField field in grdComplianceType.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdComplianceType.Columns.IndexOf(field);
                    }
                }

                grdComplianceType.DataSource = assignmentList;
                grdComplianceType.DataBind();
                tbxFilterLocation.Text = "< Select >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceType_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                SaveCheckedValues();

                if (chkEvent.Checked == true)
                {
                    if (tbxFilter.Text.Trim() == "")
                    {
                        BindComplianceMatrix("N", "Y");
                    }
                    else
                    {
                        BindComplianceMatrix("Y", "Y");
                    }
                }
                else
                {
                    if (tbxFilter.Text.Trim() == "")
                    {
                        BindComplianceMatrix("N", "N");
                    }
                    else
                    {
                        BindComplianceMatrix("Y", "N");
                    }
                }

                //BindComplianceMatrix("N", "N");
                grdComplianceRoleMatrix.PageIndex = e.NewPageIndex;
                grdComplianceRoleMatrix.DataBind();
                PopulateCheckedValues();
                if ((!string.IsNullOrEmpty(tbxStartDate.Text)) && tbxStartDate.Text != null)
                {
                    setDateToGridView();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void SaveCheckedValues()
        {
            try
            {
                
                List<ComplianceAsignmentProperties> complianceList = new List<ComplianceAsignmentProperties>();
                foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                {
                    ComplianceAsignmentProperties complianceProperties = new ComplianceAsignmentProperties();
                    complianceProperties.ComplianceId = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                    complianceProperties.Performer = ((CheckBox)gvrow.FindControl("chkPerformer")).Checked;
                    complianceProperties.Reviewer1 = ((CheckBox)gvrow.FindControl("chkReviewer1")).Checked;
                    //complianceProperties.Reviewer2 = ((CheckBox)gvrow.FindControl("chkReviewer2")).Checked;
                    complianceProperties.StartDate = Request[((TextBox)gvrow.FindControl("txtStartDate")).UniqueID].ToString();
                    // Check in the Session
                    if (ViewState["CHECKED_ITEMS"] != null)
                        complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                    if (complianceProperties.Performer || complianceProperties.Reviewer1)
                    {
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                        {
                            complianceList.Remove(rmdata);
                            complianceList.Add(complianceProperties);
                        }
                        else
                        {
                            complianceList.Add(complianceProperties);
                        }
                    }
                    else
                    {
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                            complianceList.Remove(rmdata);
                    }

                }
                if (complianceList != null && complianceList.Count > 0)
                    ViewState["CHECKED_ITEMS"] = complianceList;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void PopulateCheckedValues()
        {
            try
            {
                List<ComplianceAsignmentProperties> complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                if (complianceList != null && complianceList.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                    {
                        int index = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == index).FirstOrDefault();
                        if (rmdata != null)
                        {
                            CheckBox chkPerformer = (CheckBox)gvrow.FindControl("chkPerformer");
                            CheckBox chkReviewer1 = (CheckBox)gvrow.FindControl("chkReviewer1");
                            //CheckBox chkReviewer2 = (CheckBox)gvrow.FindControl("chkReviewer2");
                            TextBox txtStartDate = (TextBox)gvrow.FindControl("txtStartDate");
                            chkPerformer.Checked = rmdata.Performer;
                            chkReviewer1.Checked = rmdata.Reviewer1;
                            //chkReviewer2.Checked = rmdata.Reviewer2;
                            txtStartDate.Text = rmdata.StartDate;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceRoleMatrix__RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    if (cbApprover.Checked)
                    {
                        CheckBox chkPerformerheader = (CheckBox)e.Row.FindControl("chkPerformerheader");
                        CheckBox chkReviewer1header = (CheckBox)e.Row.FindControl("chkReviewer1header");
                        //CheckBox chkReviewer2header = (CheckBox)e.Row.FindControl("chkReviewer2header");
                        chkPerformerheader.Enabled = false;
                        chkReviewer1header.Enabled = false;
                        //chkReviewer2header.Enabled = false;
                    }
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //int branchId = 0;
                    //if (tvBranches.SelectedNode != null)
                    //{
                    //    branchId = Convert.ToInt32(tvBranches.SelectedNode.Value);
                    //}
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    int branchID = -1;
                    if (!(tbxBranch.Text.Trim().Equals("< Select >")))
                    {
                        branchID = CustomerBranchManagement.GetByName(tbxBranch.Text.Trim(), customerID).ID;
                    }
                    long complianceId = Convert.ToInt64(grdComplianceRoleMatrix.DataKeys[e.Row.RowIndex].Values[0].ToString());
                    List<ComplianceAssignment> AssignedCompliances = ComplianceManagement.Business.ComplianceManagement.GetAssignedCompliances(complianceId, branchID);

                    DateTime startDate = DateTime.Now;
                    TextBox txtStartDate = (TextBox)e.Row.FindControl("txtStartDate");
                    txtStartDate.Text = startDate.ToString("dd-MM-yyyy");
                    CheckBox chkPerformer = (CheckBox)e.Row.FindControl("chkPerformer");
                    CheckBox chkReviewer1 = (CheckBox)e.Row.FindControl("chkReviewer1");
                    //CheckBox chkReviewer2 = (CheckBox)e.Row.FindControl("chkReviewer2");

                    if (cbApprover.Checked)
                    {
                        if (chkPerformer.Checked)
                            chkPerformer.Checked = false;
                        if (chkReviewer1.Checked)
                            chkReviewer1.Checked = false;
                        //if (chkReviewer2.Checked)
                        //    chkReviewer2.Checked = false;
                        chkPerformer.Enabled = false;
                        chkReviewer1.Enabled = false;
                        //chkReviewer2.Enabled = false;
                    }

                    CheckBox headerchk = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkPerformerheader");
                    CheckBox childchk = (CheckBox)e.Row.FindControl("chkPerformer");
                    childchk.Attributes.Add("onclick", "javascript:Selectchildcheckboxes('" + headerchk.ClientID + "')");

                    CheckBox chkReviewer1header = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkReviewer1header");
                    CheckBox chkRev1 = (CheckBox)e.Row.FindControl("chkReviewer1");
                    chkRev1.Attributes.Add("onclick", "javascript:Selectchildcheckboxes('" + chkReviewer1header.ClientID + "')");

                    //CheckBox chkReviewer2header = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkReviewer2header");
                    //CheckBox chkRev2 = (CheckBox)e.Row.FindControl("chkReviewer2");
                    //chkRev2.Attributes.Add("onclick", "javascript:Selectchildcheckboxes('" + chkReviewer2header.ClientID + "')");

                    if (AssignedCompliances != null)
                    {
                        //if (!cbApprover.Checked)
                        //{
                            ComplianceAssignment assignmentAsPerformer = AssignedCompliances.Where(entry => entry.RoleID == 3).FirstOrDefault();
                            ComplianceAssignment assignmentAsReviewer1 = AssignedCompliances.Where(entry => entry.RoleID == 4).FirstOrDefault();
                            ComplianceAssignment assignmentAsReviewer2 = AssignedCompliances.Where(entry => entry.RoleID == 5).FirstOrDefault();
                            if (assignmentAsPerformer != null)
                            {
                                User user = UserManagement.GetByID(Convert.ToInt32(assignmentAsPerformer.UserID));
                                chkPerformer.Checked = true;
                                chkPerformer.Enabled = false;
                                chkPerformer.ToolTip = "This compliance is already assigned to " + user.FirstName + " " + user.LastName + ".";
                            }


                            if (assignmentAsReviewer1 != null)
                            {
                                User user = UserManagement.GetByID(Convert.ToInt32(assignmentAsReviewer1.UserID));
                                chkReviewer1.Checked = true;
                                chkReviewer1.Enabled = false;
                                chkReviewer1.ToolTip = "This compliance is already assigned to " + user.FirstName + " " + user.LastName + ".";
                            }


                            //if (assignmentAsReviewer2 != null)
                            //{
                            //    User user = UserManagement.GetByID(Convert.ToInt32(assignmentAsReviewer2.UserID));
                            //    chkReviewer2.Checked = true;
                            //    chkReviewer2.Enabled = false;
                            //    chkReviewer2.ToolTip = "This compliance is already assigned to " + user.FirstName + " " + user.LastName + ".";
                            //}
                        //}
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceRoleMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int complianceTypeID = Convert.ToInt32(ddlComplianceType.SelectedValue);
                int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                var ComplianceRoleMatrixList = Business.ComplianceManagement.GetByType(complianceTypeID, complianceCatagoryID);
                if (direction == SortDirection.Ascending)
                {
                    ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    ComplianceRoleMatrixList = ComplianceRoleMatrixList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }


                foreach (DataControlField field in grdComplianceRoleMatrix.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["MatrixSortIndex"] = grdComplianceRoleMatrix.Columns.IndexOf(field);
                    }
                }
              

                SaveCheckedValues();
                grdComplianceRoleMatrix.DataSource = ComplianceRoleMatrixList;
                grdComplianceRoleMatrix.DataBind();
                PopulateCheckedValues();
                tbxFilterLocation.Text = "< Select >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceRoleMatrix_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["MatrixSortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddMatrixSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddMatrixSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        private void BindComplianceCategories()
        {
            try
            {
                ddlComplianceCatagory.DataTextField = "Name";
                ddlComplianceCatagory.DataValueField = "ID";
                ddlComplianceCatagory.DataSource = ComplianceCategoryManagement.GetAll();
                ddlComplianceCatagory.DataBind();
                ddlComplianceCatagory.Items.Insert(0, new ListItem("< Select >", "-1"));
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tbxStartDate_TextChanged(object sender, EventArgs e)
        {
            setDateToGridView();
        }

        private void setDateToGridView()
        {
            try
            {
                for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
                {
                    TextBox txt = (TextBox)grdComplianceRoleMatrix.Rows[i].FindControl("txtStartDate");
                    txt.Text = tbxStartDate.Text;
                }

                DateTime date = DateTime.Now;
                if (!string.IsNullOrEmpty(tbxStartDate.Text.Trim()))
                {
                    date = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindActList()
        {
            try
            {
                int complianceTypeID = Convert.ToInt32(ddlComplianceType.SelectedValue);
                int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                List<ActView> ActList = ActManagement.GetAll(complianceCatagoryID, complianceTypeID);
                if (complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        //int branchID = tvBranches.SelectedNode != null ? Convert.ToInt32(tvBranches.SelectedNode.Value) : -1;
                        int branchID = -1;
                        if (!(tbxBranch.Text.Trim().Equals("< Select >")))
                        {
                            branchID = CustomerBranchManagement.GetByName(tbxBranch.Text.Trim(), customerID).ID;
                        }
                        if (branchID != -1)
                        {
                            int StateId = CustomerBranchManagement.GetByID(branchID).StateID;
                            ActList = ActList.Where(entry => entry.StateID == StateId).ToList();
                        }
                    }
                }

                rptActList.DataSource = ActList;
                rptActList.DataBind();

                if (complianceCatagoryID != -1 || complianceTypeID != -1)
                {
                    
                    foreach (RepeaterItem aItem in rptActList.Items)
                    {
                        CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");
                       
                        if (!chkAct.Checked)
                        {
                            chkAct.Checked = true;
                        }
                    }
                    CheckBox actSelectAll = (CheckBox)rptActList.Controls[0].Controls[0].FindControl("actSelectAll");
                    actSelectAll.Checked = true;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("AssignedCompliances");
                        DataTable ExcelData=null;
                        BindComplianceInstances();
                        DataView view = new System.Data.DataView((grdComplianceType.DataSource as List<ComplianceAssignedInstancesView>).ToDataTable());
                        ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "ShortDescription", "User", "Name", "Section", "Role", "ScheduledOn");
                        exWorkSheet.Cells["A4"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["B2"].Value = "Assigned Compliances Report";
                        exWorkSheet.Cells["B2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B2"].Style.Font.Size = 15;

                       
                        exWorkSheet.Cells["A4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A4"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A4"].Value = "ID";

                        exWorkSheet.Cells["B4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B4"].Value = "Branch";
                        exWorkSheet.Cells["B4"].Style.Font.Size = 12;

                        exWorkSheet.Cells["C4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C4"].Value = "Short Description";
                        exWorkSheet.Cells["C4"].Style.Font.Size = 12;

                        exWorkSheet.Cells["D4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D4"].Value = "User";
                        exWorkSheet.Cells["D4"].Style.Font.Size = 12;

                        exWorkSheet.Cells["E4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E4"].Value = "Name";
                        exWorkSheet.Cells["E4"].Style.Font.Size = 12;

                        exWorkSheet.Cells["F4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F4"].Value = "Section";
                        exWorkSheet.Cells["F4"].Style.Font.Size = 12;

                        exWorkSheet.Cells["G4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G4"].Value = "Role";
                        exWorkSheet.Cells["G4"].Style.Font.Size = 12;
                        
                        exWorkSheet.Cells["H4"].Value = "Start Date";
                        exWorkSheet.Cells["H4"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H4"].Style.Font.Size = 12;

                        using (ExcelRange col = exWorkSheet.Cells[2, 1, 6 + ExcelData.Rows.Count, 9])
                        {
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.AutoFitColumns();
                        }
                        using (ExcelRange col = exWorkSheet.Cells[2, 5, 6 + ExcelData.Rows.Count, 9])
                        {
                            col.Style.Numberformat.Format = "dd/MM/yyyy";
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=AssignedInstancesReport.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                    }
                    catch (Exception)
                    {
                    }
                    //Response.End();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void TreeviewTextHighlightrd(int? branch)
        {
           
            
                foreach (TreeNode node in tvBranches.Nodes)
                {
                    var list = GetChildren(node);
                    List<TreeNode> tn = list.Where(nd => nd.Text.Contains("<div")).ToList();
                    if (tn != null)
                    {
                        foreach(TreeNode t in tn)
                        {
                            t.Text = Regex.Replace(t.Text, "<.*?>", string.Empty);
                        }
                       
                    }

                    TreeNode td =null;
                    if (branch != null)
                        td = list.Where(nd => nd.Value.Equals(branch.ToString())).FirstOrDefault();

                    if (td != null)
                        list.Where(nd => nd.Value.Equals(branch.ToString())).FirstOrDefault().Text = "<div style=\"font-weight:bold\">" + td.Text + "</div>";
                }
           
        }

      
     
    }
}