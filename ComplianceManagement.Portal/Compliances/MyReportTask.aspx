﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="MyReportTask.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.MyReportTask" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <style type="text/css">
        .k-list-container.k-popup-dropdowntree .k-treeview {
            box-sizing: border-box;
            padding: 10px;
            overflow-x: hidden;
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
        }

        #grid1 .k-grid-content {
            min-height: 320px !important;
        }

        html {
            color: #666666;
            /* font-size: 15px;*/
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
            font-size: 12px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            /*            font-size: 14px;*/
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        #grid .k-grouping-header {
            font-style: italic;
            margin-top: -7px;
            background-color: white;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
            font-size: 12px;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-multiselect-wrap .k-input {
            /*padding-top:6px;*/
            display: inherit !important;
        }
    </style>


    <title></title>

    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>


    <script type="text/x-kendo-template" id="template"> 
       
   
    </script>

    <script id="TypeTemplate" type="text/x-kendo-template">
                 
            #=GetRiskFileType(RiskType)# 
        
    </script>
    <script type="text/javascript">

        function GetRiskFileType(value) {
            if (value == "0") {
                return "High";
            }
            else if (value == "1") {
                return "Medium";
            }
            else if (value == "2") {
                return "Low";
            }
            else if (value == "3") {
                return "Critical";
            }
            else {
                return "";
            }

        }
        var total = 0;
        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

        $(document).ready(function () {

           <%if (Falg == "AUD")%>
           <%{%>

            $('#Startdatepicker').val('<% =SDate%>');
            $('#Lastdatepicker').val('<% =LDate%>');

            $("#Startdatepicker").attr("readonly", true);
            $("#Lastdatepicker").attr("readonly", true);
            $("#dropdownPastData").attr("readonly", true);
            $("#dropdownFY").attr("readonly", true);

            $('#dropdownPastData').val('All');
            $('#dropdownlistTypePastdata').val('All');
           <%}%>


            $("#Startdatepicker").kendoDatePicker({
                change: onChange
            });
            function onChange() {
                settracknew('Task Report', 'Filtering ', 'Start Date', '');

                $('#filterStartDate').css('display', 'none');
                $('#Clearfilter').css('display', 'none');
                $('#filterStartDate').html('');
                if (kendo.toString(this.value(), 'd') != null) {
                    $('#filterStartDate').css('display', 'block');
                    $('#Clearfilter').css('display', 'block');
                    $('#filterStartDate').append('Start Date&nbsp;&nbsp;:&nbsp;');
                    $('#filterStartDate').append('<span class="k-button" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius: 12px;">' + kendo.toString(this.value(), 'dd MMM yyyy') + '<span class="k-icon k-i-close" onclick="fclosebtn(\'filterStartDate\')"></span></span>');

                }
                DateFilterCustom();
            }

            function DateFilterCustom() {
                CheckFilterClearorNot();
                $('input[id=chkAll]').prop('checked', false);
                $('#dvbtndownloadDocument').css('display', 'none');
                FilterAllAdvancedSearch();

            }

            $("#Lastdatepicker").kendoDatePicker({
                change: onChange1
            });
            function onChange1() {
                settracknew('Task Report', 'Filtering ', 'Last Date', '');
                $('#filterLastDate').html('');
                if (kendo.toString(this.value(), 'd') != null) {
                    $('#filterLastDate').css('display', 'block');
                    $('#Clearfilter').css('display', 'block');
                    $('#filterLastDate').append('End Date&nbsp;&nbsp;:&nbsp;');

                    $('#filterLastDate').append('<span class="k-button" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius: 12px;">' + kendo.toString(this.value(), 'dd MMM yyyy') + '<span class="k-icon k-i-close" onclick="fclosebtn(\'filterStartDate\')"></span></span>');
                }
                DateFilterCustom();
            }



            $(".k-grid1-content tbody[role='rowgroup'] tr[role='row'] td:first-child").prepend('<span class="k-icon k-i-filter"</span>');

            var record = 0;
            var total = 0;
            var grid1 = $("#grid1").kendoGrid({

                dataSource: {

                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoMyReportTask?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=<% =IsMonthID%>&StatusFlag=-1&FY=0',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }

                    },
                    schema: {
                        data: function (response) {
                            return response[0].Statustory;
                        },
                        total: function (response) {
                            return response[0].Statustory.length;
                        }
                    },
                    pageSize: 10,

                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                //height: 513,
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                change: onChange,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                    debugger;
                    record = 0;
                    total = this.dataSource._total;

                    if (this.dataSource.pageSize() == undefined || this.dataSource.pageSize() == null) {
                        this.dataSource.pageSize(this.dataSource._total);
                    }
                    if (this.dataSource.pageSize() > 10 && this.dataSource.pageSize() > 20 && this.dataSource.pageSize() != this.dataSource._total) {
                        if (this.dataSource._total <= 10) {
                            this.dataSource.pageSize(10)

                        }
                        if (this.dataSource._total >= 10 && this.dataSource._total <= 20) {
                            this.dataSource.pageSize(20)
                        }
                        else {
                            this.dataSource.pageSize(total);
                        }
                    }

                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();

                },
                columns: [
                    {
                        hidden: true, field: "RiskType", title: "Risk",
                        width: "10%",
                        template: "#if(RiskType == '0') {#<span>High</span>#} else if(RiskType == '1') {#<span>Medium</span>#} else if(RiskType == '2') {#<span>Low</span>#} else {#<span>Critical</span>#}#",
                        //template: kendo.template($('#TypeTemplate').html()),
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            ui: cityFilter,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true, field: "CustomerBranchID", title: "Branch ID",
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Branch", title: 'Location',
                        width: "22%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "TaskCustomerBranchName", title: 'Reporting Location',
                        width: "19.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "TaskTitle", title: 'Task',
                        width: "35%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: 'PerformerName', title: 'Performer',
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: 'ReviewerName', title: 'Reviewer',
                        width: "10%",
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {
                        field: "ScheduledOn", title: 'Due Date',
                        width: "10%",
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",

                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ForMonth", title: 'Period', width: "10%", filterable: {
                            multi: true,
                            extra: false,
                            search: true,

                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Status", title: 'Status',
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true, field: "ActID", title: "Act ID",
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true, field: "UserID", title: "User ID",
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    }

                ]
            });

            $("#grid1").kendoTooltip({
                filter: "th",
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });

            $("#grid1").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");


            var record = 0;
            var total = 0;
            var grid = $("#grid").kendoGrid({

                dataSource: {

                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoMyReportTask?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=<% =IsMonthID%>&StatusFlag=-1&FY=0',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read:"<% =Path%>data/KendoMyReportTask?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=<% =IsMonthID%>&StatusFlag=-1&FY=0"

                    },
                    schema: {
                        data: function (response) {
                            return response[0].Statustory;
                        },
                        total: function (response) {
                            return response[0].Statustory.length;
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                toolbar: kendo.template($("#template").html()),
                //height: 513,
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {

                    record = 0;
                    total = this.dataSource._total;

                    if (this.dataSource.pageSize() == undefined || this.dataSource.pageSize() == null) {
                        this.dataSource.pageSize(this.dataSource._total);
                    }
                    if (this.dataSource.pageSize() > 10 && this.dataSource.pageSize() > 20 && this.dataSource.pageSize() != this.dataSource._total) {
                        if (this.dataSource._total <= 10) {
                            this.dataSource.pageSize(10)

                        }
                        if (this.dataSource._total >= 10 && this.dataSource._total <= 20) {
                            this.dataSource.pageSize(20)
                        }
                        else {
                            this.dataSource.pageSize(total);
                        }
                    }

                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();

                },
                columns: [

                    {
                        hidden: true, field: "RiskType", title: "Risk",
                        width: "15%",
                        template: "#if(RiskType == '0') {#<span>High</span>#} else if(RiskType == '1') {#<span>Medium</span>#} else if(RiskType == '2') {#<span>Low</span>#} else {#<span>Critical</span>#}#",
                        //template: kendo.template($('#TypeTemplate').html()),
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            ui: cityFilter,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true, field: "CustomerBranchID", title: "Branch ID",
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Branch", title: 'Location',
                        width: "22%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "TaskCustomerBranchName", title: 'Reporting Location',
                        width: "19.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "TaskTitle", title: 'Task',
                        width: "30%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: 'PerformerName', title: 'Performer',
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: 'ReviewerName', title: 'Reviewer',
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {
                        field: "ScheduledOn", title: 'Due Date',
                        type: "date",
                        width: "15%",
                        format: "{0:dd-MMM-yyyy}",
                        //template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') != null ? kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') : '' #",
                        //template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ForMonth", title: 'Period', width: "15%", filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            // width: 120,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    {
                        field: "Status", title: 'Status',
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CloseDate", title: "Close Date",
                        width: "15%",
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        hidden: true,

                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    }

                ]
            });



            $("#grid").kendoTooltip({
                filter: "th",
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });

            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");


            $("#dropdownlistComplianceType").kendoDropDownList({
                placeholder: "Compliance Type",
                dataTextField: "text",
                dataValueField: "value",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Internal", value: "0" }

                ],
                index: 0,
                change: function (e) {
                    settracknew('Task Report', 'Filtering ', 'Compliance Type', '');
                    DataBindDaynamicKendoGriddMain();
                }
            });

            function cityFilter(element) {
                element.kendoDropDownList({
                    dataSource: cities,
                    optionLabel: "--Select Value--"
                });
            }

            cities = [
                { text: "High", value: "-1" },
                { text: "Internal", value: "0" }
            ];

            function DataBindDaynamicKendoGriddMain() {


                $("#dropdowntree").data("kendoDropDownTree").value([]);
                $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
                $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
                $('#ClearfilterMain').css('display', 'none');
                $('#dvbtndownloadDocumentMain').css('display', 'none');

                if ($("#dropdownlistComplianceType").val() == -1) {
                    var dataSource = new kendo.data.DataSource({
                        transport: {
                            read: {
                                url: '<% =Path%>data/KendoMyReportTask?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=-1&FY=0',
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                            //read: '<% =Path%>data/KendoMyReportTask?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=-1&FY=0' 
                        },
                        schema: {
                            data: function (response) {
                                return response[0].Statustory;
                            },
                            total: function (response) {
                                return response[0].Statustory.length;
                            },
                            model: {
                                fields: {
                                    ScheduledOn: { type: "date" },
                                    CloseDate: { type: "date" }
                                }
                            }
                        },
                        pageSize: 10
                    });
                    var grid = $('#grid').data("kendoGrid");
                    dataSource.read();
                    grid.setDataSource(dataSource);
                }

                if ($("#dropdownlistComplianceType").val() == 0) {
                    var dataSource = new kendo.data.DataSource({
                        transport: {
                            read: {
                                url: '<% =Path%>data/KendoMyReportTask?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=0&FY=0',
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                            //read: '<% =Path%>data/KendoMyReportTask?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&StatusFlag=0&FY=0'
                        },
                        schema: {
                            data: function (response) {
                                return response[0].Internal;
                            },
                            total: function (response) {
                                return response[0].Internal.length;
                            },
                            model: {
                                fields: {
                                    ScheduledOn: { type: "date" },
                                    CloseDate: { type: "date" }
                                }
                            }
                        },
                        pageSize: 10,
                    });
                    var grid = $('#grid').data("kendoGrid");
                    dataSource.read();
                    grid.setDataSource(dataSource);
                }

                if ($("#dropdownlistComplianceType").val() == 0)//Internal and Internal Checklist
                {
                    var dataSource12 = new kendo.data.HierarchicalDataSource({
                        severFiltering: true,
                        transport: {
                            read: {
                                url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I',
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                            //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I"
                        },
                        schema: {
                            data: function (response) {
                                return response[0].locationList;
                            },
                            model: {
                                children: "Children"
                            }
                        }
                    });
                    dataSource12.read();
                    $("#dropdowntree").data("kendoDropDownTree").setDataSource(dataSource12);
                }
                else {
                    var dataSource12 = new kendo.data.HierarchicalDataSource({
                        severFiltering: true,
                        transport: {
                            read: {
                                url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                            //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                        },
                        schema: {
                            data: function (response) {
                                return response[0].locationList;
                            },
                            model: {
                                children: "Children"
                            }
                        }
                    });
                    dataSource12.read();
                    $("#dropdowntree").data("kendoDropDownTree").setDataSource(dataSource12);
                }
            }

            $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: false,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    settracknew('Task Report', 'Filtering ', 'Risk', '');
                    var filter = { logic: "or", filters: [] };
                    //  values is an array containing values to be searched
                    var values = this.value();
                    $.each(values, function (i, v) {
                        filter.filters.push({
                            field: "Risk", operator: "eq", value: parseInt(v)
                        });
                    });
                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(filter);

                    $('input[id=chkAllMain]').prop('checked', false);
                    $('#dvbtndownloadDocumentMain').css('display', 'none');
                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });

            $("#dropdownlistStatus").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                change: function (e) {
                    settracknew('Task Report', 'Filtering ', 'Status', '');
                    //FilterAllMain();
                    //fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status')

                    //$('input[id=chkAllMain]').prop('checked', false);
                    //$('#dvbtndownloadDocumentMain').css('display', 'none');

                    if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0) {

                        fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status')
                        FilterAllMain();
                        //$('input[id=chkAllMain]').prop('checked', false);
                        //$('#dvbtndownloadDocumentMain').css('display', 'none');
                    }
                    else {
                        var filter = { logic: "or", filters: [] };
                        var values = this.value();
                        $.each(values, function (i, v) {
                            //add after u get column for filter in API
                            filter.filters.push({
                                field: "Status", operator: "eq", value: v
                            });
                        });
                        fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status')
                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter(filter);

                        $('input[id=chkAllMain]').prop('checked', false);
                        $('#dvbtndownloadDocumentMain').css('display', 'none');
                    }
                },
                dataSource: [
                    { text: "Upcoming", value: "Upcoming" },
                    { text: "Overdue", value: "Overdue" },
                    { text: "Pending For Review", value: "Pending For Review" },
                    { text: "Rejected", value: "Rejected" },
                    { text: "Closed-Delayed", value: "Closed-Delayed" },
                    { text: "Closed-Timely", value: "Closed-Timely" }
                ]
            });

            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: false,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    settracknew('Task Report', 'Filtering ', 'Period', '');
                    DataBindDaynamicKendoGrid();
                },
                index: 1,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });


            function DataBindDaynamicKendoGrid() {


                if ($("#dropdownFY").val() != "0") {
                    $("#dropdownPastData").data("kendoDropDownList").select(4);

                }

                  <%if (RoleFlag == 1)%>
                            <%{%>
                $("#dropdownUser").data("kendoDropDownTree").value([]);
                            <%}%>
                $('input[id=chkAll]').prop('checked', false);

                $("#grid1").data("kendoGrid").dataSource.filter({});

                $("#dropdowntree1").data("kendoDropDownTree").value([]);
                // $("#dropdownACT").data("kendoDropDownTree").value([]);
                $("#dropdownlistRisk1").data("kendoDropDownTree").value([]);
                //$("#dropdownUser").data("kendoDropDownTree").value([]);
                $("#dropdownlistStatus1").data("kendoDropDownTree").value([]);
                $("#Startdatepicker").data("kendoDatePicker").value(null);
                $("#Lastdatepicker").data("kendoDatePicker").value(null);
                $('#filterStartDate').html('');
                $('#filterLastDate').html('');
                $('#filterStartDate').css('display', 'none');
                $('#filterLastDate').css('display', 'none');
                $('#Clearfilter').css('display', 'none');
                $('#dvbtndownloadDocument').css('display', 'none');

                $("#dvdropdownACT").css('display', 'block');
                if ($("#dropdownlistComplianceType1").val() == 0)//Internal and Internal Checklist
                {
                    $("#dvdropdownACT").css('display', 'none');

                    var dataSource12 = new kendo.data.HierarchicalDataSource({
                        severFiltering: true,
                        transport: {
                            read: {
                                url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I',
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                            //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=I"
                        },
                        schema: {
                            data: function (response) {
                                return response[0].locationList;
                            },
                            model: {
                                children: "Children"
                            }
                        }
                    });
                    dataSource12.read();
                    $("#dropdowntree1").data("kendoDropDownTree").setDataSource(dataSource12);
                }
                else {
                    var dataSource12 = new kendo.data.HierarchicalDataSource({
                        severFiltering: true,
                        transport: {
                            read: {
                                url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                            //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                        },
                        schema: {
                            data: function (response) {
                                return response[0].locationList;
                            },
                            model: {
                                children: "Children"
                            }
                        }
                    });
                    dataSource12.read();
                    $("#dropdowntree1").data("kendoDropDownTree").setDataSource(dataSource12);
                }

                if ($("#dropdownlistComplianceType1").val() == -1) {
                    var dataSource = new kendo.data.DataSource({
                        transport: {
                            read: {
                                url: '<% =Path%>data/KendoMyReportTask?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=-1&FY=' + $("#dropdownFY").val() + '',
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                            //read: '<% =Path%>data/KendoMyReportTask?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=-1&FY='+ $("#dropdownFY").val() +'' 
                        },
                        schema: {
                            data: function (response) {
                                return response[0].Statustory;
                            },
                            total: function (response) {
                                return response[0].Statustory.length;
                            },
                            model: {
                                fields: {
                                    ScheduledOn: { type: "date" }
                                }
                            }
                        },
                        pageSize: 10,
                    });
                    var grid = $('#grid1').data("kendoGrid");
                    dataSource.read();
                    grid.setDataSource(dataSource);
                }

                if ($("#dropdownlistComplianceType1").val() == 0) {
                    var dataSource = new kendo.data.DataSource({
                        transport: {
                            read: {
                                url: '<% =Path%>data/KendoMyReportTask?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=0&FY=' + $("#dropdownFY").val() + '',
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                            //read: '<% =Path%>data/KendoMyReportTask?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =Falg%>&MonthId=' + $("#dropdownPastData").val() + '&StatusFlag=0&FY='+ $("#dropdownFY").val() +''
                        },
                        schema: {
                            data: function (response) {
                                return response[0].Internal;
                            },
                            total: function (response) {
                                return response[0].Internal.length;
                            },
                            model: {
                                fields: {
                                    ScheduledOn: { type: "date" }
                                }
                            }
                        },
                        pageSize: 10,
                    });
                    var grid = $('#grid1').data("kendoGrid");
                    dataSource.read();
                    grid.setDataSource(dataSource);
                }




            }

            $("#dropdownlistTypePastdata").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: false,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    settracknew('Task Report', 'Filtering ', 'Period', '');
                    DataBindDaynamicKendoGriddMain();
                },
                index: 1,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });


            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //checkAll: true,                
                autoWidth: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",

                change: function (e) {
                    settracknew('Task Report', 'Filtering ', 'Location', '');
                    //FilterAllMain();
                    //fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')

                    //$('input[id=chkAllMain]').prop('checked', false);
                    //$('#dvbtndownloadDocumentMain').css('display', 'none');


                    if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {

                        fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                        FilterAllMain();
                    }
                    else {
                        var filter = { logic: "or", filters: [] };
                        //  values is an array containing values to be searched
                        var values = this.value();
                        $.each(values, function (i, v) {
                            filter.filters.push({
                                field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });
                        $.each(values, function (i, v) {
                            filter.filters.push({
                                field: "TaskCustomerBranchID", operator: "eq", value: parseInt(v)
                            });
                        });
                        fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                        $('input[id=chkAllMain]').prop('checked', false);
                        $('#dvbtndownloadDocumentMain').css('display', 'none');
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
            function FilterAllMain() {

                //location details
                var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
                var locationsdetails = [];
                $.each(list1, function (i, v) {
                    locationsdetails.push({
                        field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                    });
                });
                //Task Location List
                var Tlist1 = $("#dropdowntree").data("kendoDropDownTree")._values;
                var Tasklocationsdetails = [];
                $.each(Tlist1, function (i, v) {
                    Tasklocationsdetails.push({
                        field: "TaskCustomerBranchID", operator: "eq", value: parseInt(v)
                    });
                });

                //Status details
                var list2 = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
                var Statusdetails = [];
                $.each(list2, function (i, v) {
                    Statusdetails.push({
                        field: "Status", operator: "eq", value: v
                    });
                });

                var dataSource = $("#grid").data("kendoGrid").dataSource;

                if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                    && $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: locationsdetails
                                    }, {
                                        logic: "or",
                                        filters: Tasklocationsdetails
                                    }
                                ]
                            }
                        ]
                    });
                }

                else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: locationsdetails
                                    }, {
                                        logic: "or",
                                        filters: Tasklocationsdetails
                                    }
                                ]
                            }
                        ]
                    });
                }

                else if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            }
                        ]
                    });
                }
                var dataSource = $("#grid").data("kendoGrid").dataSource;
                if (dataSource._total > 20 && dataSource.pageSize == undefined) {
                    dataSource.pageSize(total);
                }
            }

            $('#Searchfilter').on('input', function (e) {
                FilterAllAdvancedSearch();
            });




            function FilterAllAdvancedSearch() {

                //location details
                var list1 = $("#dropdowntree1").data("kendoDropDownTree")._values;
                var locationsdetails = [];
                $.each(list1, function (i, v) {
                    locationsdetails.push({
                        field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                    });
                });
                //Task Location List
                var Tlist1 = $("#dropdowntree1").data("kendoDropDownTree")._values;
                var Tasklocationsdetails = [];
                $.each(Tlist1, function (i, v) {
                    Tasklocationsdetails.push({
                        field: "TaskCustomerBranchID", operator: "eq", value: parseInt(v)
                    });
                });

                //Status details
                var list2 = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                var Statusdetails = [];
                $.each(list2, function (i, v) {
                    Statusdetails.push({
                        field: "Status", operator: "eq", value: v
                    });
                });

                // type to Search
                var searchValue = $('#Searchfilter').val();

                //datefilter
                var datedetails = [];
                if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                    datedetails.push({
                        field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                    });
                }
                if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                    datedetails.push({
                        field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                    });
                }

                //Act Detail
                var Actdetails = [];
                if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
                    Actdetails.push({
                        field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
                    });
                }

                var dataSource = $("#grid1").data("kendoGrid").dataSource;

                if (locationsdetails.length > 0
                    && Statusdetails.length > 0
                    && datedetails.length > 0
                    && searchValue.length > 0
                    && Actdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: locationsdetails
                                    }, {
                                        logic: "or",
                                        filters: Tasklocationsdetails
                                    }
                                ]
                            },
                            {
                                logic: "or",
                                filters: Statusdetails

                            },
                            {
                                logic: "and",
                                filters: datedetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "or",
                                filters: [
                                    {
                                        field: "TaskTitle",
                                        operator: "contains",
                                        value: searchValue
                                    }
                                ]
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0
                    && Statusdetails.length > 0
                    && datedetails.length > 0
                    && searchValue.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: locationsdetails
                                    }, {
                                        logic: "or",
                                        filters: Tasklocationsdetails
                                    }
                                ]
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            },
                            {
                                logic: "or",
                                filters: [
                                    {
                                        field: "TaskTitle",
                                        operator: "contains",
                                        value: searchValue
                                    }
                                ]
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0
                    && datedetails.length > 0
                    && searchValue.length > 0
                    && Actdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: locationsdetails
                                    }, {
                                        logic: "or",
                                        filters: Tasklocationsdetails
                                    }
                                ]
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "or",
                                filters: [
                                    {
                                        field: "TaskTitle",
                                        operator: "contains",
                                        value: searchValue
                                    }
                                ]
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0
                    && Statusdetails.length > 0
                    && searchValue.length > 0
                    && Actdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: locationsdetails
                                    }, {
                                        logic: "or",
                                        filters: Tasklocationsdetails
                                    }
                                ]
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "or",
                                filters: [
                                    {
                                        field: "TaskTitle",
                                        operator: "contains",
                                        value: searchValue
                                    }
                                ]
                            }
                        ]
                    });
                }

                else if (Statusdetails.length > 0
                    && datedetails.length > 0
                    && searchValue.length > 0
                    && Actdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "or",
                                filters: [
                                    {
                                        field: "TaskTitle",
                                        operator: "contains",
                                        value: searchValue
                                    }
                                ]
                            }
                        ]
                    });
                }


                else if (locationsdetails.length > 0
                    && Statusdetails.length > 0
                    && searchValue.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: locationsdetails
                                    }, {
                                        logic: "or",
                                        filters: Tasklocationsdetails
                                    }
                                ]
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: [
                                    {
                                        field: "TaskTitle",
                                        operator: "contains",
                                        value: searchValue
                                    }
                                ]
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0
                    && Statusdetails.length > 0
                    && datedetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: locationsdetails
                                    }, {
                                        logic: "or",
                                        filters: Tasklocationsdetails
                                    }
                                ]
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0
                    && datedetails.length > 0
                    && Actdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: locationsdetails
                                    }, {
                                        logic: "or",
                                        filters: Tasklocationsdetails
                                    }
                                ]
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0
                    && searchValue.length > 0
                    && Actdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: locationsdetails
                                    }, {
                                        logic: "or",
                                        filters: Tasklocationsdetails
                                    }
                                ]
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "or",
                                filters: [
                                    {
                                        field: "TaskTitle",
                                        operator: "contains",
                                        value: searchValue
                                    }
                                ]
                            }
                        ]
                    });
                }

                else if (datedetails.length > 0
                    && searchValue.length > 0
                    && Actdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "and",
                                filters: datedetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "or",
                                filters: [
                                    {
                                        field: "TaskTitle",
                                        operator: "contains",
                                        value: searchValue
                                    }
                                ]
                            }
                        ]
                    });
                }

                else if (Statusdetails.length > 0
                    && datedetails.length > 0
                    && Actdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }

                else if (Statusdetails.length > 0
                    && searchValue.length > 0
                    && Actdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            },
                            {
                                logic: "or",
                                filters: [
                                    {
                                        field: "TaskTitle",
                                        operator: "contains",
                                        value: searchValue
                                    }
                                ]
                            }
                        ]
                    });
                }

                else if (Statusdetails.length > 0
                    && datedetails.length > 0
                    && searchValue.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            },
                            {
                                logic: "or",
                                filters: [
                                    {
                                        field: "TaskTitle",
                                        operator: "contains",
                                        value: searchValue
                                    }
                                ]
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0
                    && datedetails.length > 0
                    && searchValue.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: locationsdetails
                                    }, {
                                        logic: "or",
                                        filters: Tasklocationsdetails
                                    }
                                ]
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            },
                            {
                                logic: "or",
                                filters: [
                                    {
                                        field: "TaskTitle",
                                        operator: "contains",
                                        value: searchValue
                                    }
                                ]
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0
                    && Statusdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: locationsdetails
                                    }, {
                                        logic: "or",
                                        filters: Tasklocationsdetails
                                    }
                                ]
                            },
                            {
                                logic: "or",
                                filters: Statusdetails
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0
                    && datedetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: locationsdetails
                                    }, {
                                        logic: "or",
                                        filters: Tasklocationsdetails
                                    }
                                ]
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0
                    && Actdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: locationsdetails
                                    }, {
                                        logic: "or",
                                        filters: Tasklocationsdetails
                                    }
                                ]
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }

                else if (locationsdetails.length > 0
                    && searchValue.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: locationsdetails
                                    }, {
                                        logic: "or",
                                        filters: Tasklocationsdetails
                                    }
                                ]
                            },
                            {
                                logic: "or",
                                filters: [
                                    {
                                        field: "TaskTitle",
                                        operator: "contains",
                                        value: searchValue
                                    }
                                ]
                            }
                        ]
                    });
                }

                else if (Statusdetails.length > 0
                    && datedetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }

                else if (Statusdetails.length > 0
                    && Actdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }

                else if (Statusdetails.length > 0
                    && searchValue.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            },
                            {
                                logic: "or",
                                filters: [
                                    {
                                        field: "TaskTitle",
                                        operator: "contains",
                                        value: searchValue
                                    }
                                ]
                            }
                        ]
                    });
                }

                else if (datedetails.length > 0
                    && Actdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "and",
                                filters: datedetails
                            },
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }

                else if (datedetails.length > 0
                    && searchValue.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "and",
                                filters: datedetails
                            },
                            {
                                logic: "or",
                                filters: [
                                    {
                                        field: "TaskTitle",
                                        operator: "contains",
                                        value: searchValue
                                    }
                                ]
                            }
                        ]
                    });
                }


                else if (locationsdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {
                                        logic: "or",
                                        filters: locationsdetails
                                    }, {
                                        logic: "or",
                                        filters: Tasklocationsdetails
                                    }
                                ]
                            }
                        ]
                    });
                }

                else if (Statusdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Statusdetails
                            }
                        ]
                    });
                }

                else if (datedetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "and",
                                filters: datedetails
                            }
                        ]
                    });
                }

                else if (searchValue.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: [
                                    {
                                        field: "TaskTitle",
                                        operator: "contains",
                                        value: searchValue
                                    },
                                    {
                                        field: "Status",
                                        operator: "contains",
                                        value: searchValue
                                    },
                                    {
                                        field: "TaskCustomerBranchName",
                                        operator: "contains",
                                        value: searchValue
                                    },
                                    {
                                        field: "Branch",
                                        operator: "contains",
                                        value: searchValue
                                    }
                                ]
                            }
                        ]
                    });
                }
                else if (Actdetails.length > 0) {
                    dataSource.filter({
                        logic: "and",
                        filters: [
                            {
                                logic: "or",
                                filters: Actdetails
                            }
                        ]
                    });
                }
                else {
                    $("#grid1").data("kendoGrid").dataSource.filter({});
                }
                var dataSource = $("#grid1").data("kendoGrid").dataSource;
                if (dataSource._total > 20 && dataSource.pageSize == undefined) {
                    dataSource.pageSize(total);
                }
            }



            $("#dropdowntree1").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
                change: function (e) {
                    settracknew('Task Report', 'Filtering ', 'Loaction', '');
                    FilterAllAdvancedSearch();
                    fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')
                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');

                    //if ($("#dropdownlistStatus1").data("kendoDropDownTree")._values.length > 0) {
                    //    FilterAllAdvancedSearch();
                    //    fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1');

                    //    $('input[id=chkAll]').prop('checked', false);
                    //    $('#dvbtndownloadDocument').css('display', 'none');
                    //}
                    //else {

                    //    var filter = { logic: "or", filters: [] };
                    //    var values = this.value();
                    //    $.each(values, function (i, v) {
                    //        filter.filters.push({
                    //            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                    //        });
                    //    });

                    //    $.each(values, function (i, v) {
                    //        filter.filters.push({
                    //            field: "TaskCustomerBranchID", operator: "eq", value: parseInt(v)
                    //        });
                    //    });

                    //    fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')
                    //    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    //    dataSource.filter(filter);

                    //    $('input[id=chkAll]').prop('checked', false);
                    //    $('#dvbtndownloadDocument').css('display', 'none');
                    //}
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });




            $("#dropdownlistRisk1").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: false,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    settracknew('Task Report', 'Filtering ', 'Risk', '');
                    var filter = { logic: "or", filters: [] };
                    //  values is an array containing values to be searched
                    var values = this.value();
                    $.each(values, function (i, v) {
                        filter.filters.push({
                            field: "Risk", operator: "eq", value: parseInt(v)
                        });
                    });
                    fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');
                    CheckFilterClearorNot();
                    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    dataSource.filter(filter);

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');
                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });

            $("#dropdownFY").kendoDropDownList({

                autoWidth: false,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    DataBindDaynamicKendoGrid();
                    settracknew('Task Report', 'Filtering ', 'Financial Year', '');
                },
                dataSource: [
                    { text: "Financial Year", value: "0" },
                    { text: "2020-2021", value: "2020-2021" },
                    { text: "2019-2020", value: "2019-2020" },
                    { text: "2018-2019", value: "2018-2019" },
                    { text: "2017-2018", value: "2017-2018" },
                    { text: "2016-2017", value: "2016-2017" }
                ]
            });



            $("#dropdownUser").kendoDropDownTree({
                placeholder: "User",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: false,
                dataTextField: "FullName",
                dataValueField: "UID",

                change: function () {

                    settracknew('Task Report', 'Filtering ', 'User', '');
                    //alert(this.value());
                    var filter = { logic: "or", filters: [] };
                    //  values is an array containing values to be searched
                    var values = this.value();
                    $.each(values, function (i, v) {
                        filter.filters.push({
                            field: "UserID", operator: "eq", value: parseInt(v)
                        });
                    });
                    fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
                    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    dataSource.filter(filter);

                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');

                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =Falg%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                }
            });

            $("#dropdownlistStatus1").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                change: function (e) {
                    settracknew('Task Report', 'Filtering ', 'Status', '');
                    FilterAllAdvancedSearch();
                    fCreateStoryBoard('dropdownlistStatus1', 'filterstatus1', 'status1')
                    $('input[id=chkAll]').prop('checked', false);
                    $('#dvbtndownloadDocument').css('display', 'none');


                },
                dataSource: [
                    { text: "Upcoming", value: "Upcoming" },
                    { text: "Overdue", value: "Overdue" },
                    { text: "Pending For Review", value: "Pending For Review" },
                    { text: "Rejected", value: "Rejected" },
                    { text: "Closed-Delayed", value: "Closed-Delayed" },
                    { text: "Closed-Timely", value: "Closed-Timely" }
                ]
            });

            $("#dropdownlistComplianceType1").kendoDropDownList({

                autoWidth: false,

                dataTextField: "text",
                dataValueField: "value",
                index: 0,
                change: function (e) {
                    DataBindDaynamicKendoGrid();
                    settracknew('Task Report', 'Filtering ', 'Compliance Type', '');
                },
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Internal", value: "0" }

                ]
            });

            $("#dropdownType").kendoDropDownTree({
                placeholder: "Select Type",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                dataTextField: "TypeName",
                dataValueField: "TypId",
                //optionLabel: "All",
                change: function (e) {
                    settracknew('Task Report', 'Filtering ', 'Type', '');
                    var filter = { logic: "or", filters: [] };
                    var values = this.value();
                    $.each(values, function (i, v) {

                    });
                    fCreateStoryBoard('dropdownType', 'filterCompType', 'CompType');

                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindTypesAll?CId=<% =CId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }

                    }
                }
            });

            $("#dropdownCategory").kendoDropDownTree({
                placeholder: "Select Category",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                dataTextField: "CategoryName",
                dataValueField: "CategoryId",
                //optionLabel: "All",
                change: function (e) {
                    settracknew('Task Report', 'Filtering ', 'Category', '');
                    var filter = { logic: "or", filters: [] };
                    var values = this.value();
                    $.each(values, function (i, v) {

                    });
                    fCreateStoryBoard('dropdownCategory', 'filterCategory', 'Category');

                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindCategoriesAll?CId=<% =CId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }

                    }
                }
            });

            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: false,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    settracknew('Task Report', 'Filtering ', 'Act', '');
                    FilterAllAdvancedSearch();

                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =Falg%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }, dataBound: function (e) {
                    e.sender.list.width("800");
                }

            });

            $("#dropdownComplianceSubType").kendoDropDownTree({
                placeholder: "Compliance Sub Type",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
                change: function (e) {
                    settracknew('Task Report', 'Filtering ', 'Compliance Sub Type', '');
                    var filter = { logic: "or", filters: [] };
                    var values = this.value();
                    $.each(values, function (i, v) {

                    });
                    fCreateStoryBoard('dropdownComplianceSubType', 'filterCompSubType', 'CompSubType');

                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindComplianceSubTypeList?UId=<% =UserId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }

                    }
                }
            });

            $("#grid tbody").on("click", "tr", function (e) {
                var rowElement = this;
                var row = $(rowElement);
                var grid = $("#grid").getKendoGrid();
                if (row.hasClass("k-state-selected")) {
                    var selected = grid.select();
                    selected = $.grep(selected, function (x) {
                        var itemToRemove = grid.dataItem(row);
                        var currentItem = grid.dataItem(x);
                        return itemToRemove.ID != currentItem.ID
                    })
                    grid.clearSelection();
                    grid.select(selected);
                    //e.stopPropagation();
                } else {
                    grid.select(row)
                    //e.stopPropagation();
                }
            });

        });


        function ClearAllFilterMain(e) {

            $("#dropdowntree").data("kendoDropDownTree").value([]);

            $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
            $('#ClearfilterMain').css('display', 'none');

            $('#dvbtndownloadDocumentMain').css('display', 'none');


            $("#dropdownEventNature").data("kendoDropDownList").select(0);
            var dataSource = $("#grid").data("kendoGrid").dataSource;
            dataSource.pageSize(10);
            $("#grid").data("kendoGrid").dataSource.filter({});

            $('input[id=chkAllMain]').prop('checked', false);
            e.preventDefault();
        }

        function ClearAllFilter(e) {
            $("#dropdownACT").data("kendoDropDownList").select(0);
            $("#dropdowntree1").data("kendoDropDownTree").value([]);
            $("#dropdownUser").data("kendoDropDownTree").value([]);
            $("#dropdownlistStatus1").data("kendoDropDownTree").value([]);
            $("#Startdatepicker").data("kendoDatePicker").value(null);
            $("#Lastdatepicker").data("kendoDatePicker").value(null);
            $('#filterStartDate').html('');
            $('#filterLastDate').html('');
            $('#filterStartDate').css('display', 'none');
            $('#filterLastDate').css('display', 'none');
            $('#Clearfilter').css('display', 'none');

            $('#dvbtndownloadDocument').css('display', 'none');

            $('#Searchfilter').val('');

            $("#grid1").data("kendoGrid").dataSource.filter({});

            var dataSource = $("#grid1").data("kendoGrid").dataSource;
            dataSource.pageSize(10);
            $('input[id=chkAll]').prop('checked', false);
            e.preventDefault();
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');

            fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
            //for rebind if any pending filter is present (ADV Grid)
            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1');
            fCreateStoryBoard('dropdownlistStatus1', 'filterstatus1', 'status1');


            fCreateStoryBoard('dropdownUser', 'filterUser', 'user');


            CheckFilterClearorNot();

            CheckFilterClearorNotMain();
        };

        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistStatus').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }

        function CheckFilterClearorNot() {
            if (($($($('#dropdowntree1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownUser').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#dropdownlistStatus1').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {

                var flag = false;
                if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                    flag = true;
                }
                if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                    flag = true;
                }
                if (flag == false) {
                    $('#Clearfilter').css('display', 'none');
                }
                else {
                    $('#Clearfilter').css('display', 'block');
                }
            }
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filtertype') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard               
            }
            else if (div == 'filterrisk') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterstatus') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filterpstData1') {
                $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCategory') {
                $('#' + div).append('Category&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterAct') {
                $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompSubType') {
                $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompType') {
                $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtersstoryboard1') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtertype1') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterrisk1') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterFY') {
                $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterUser') {
                $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterstatus1') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top: 4px;vertical-align: baseline;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
                //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }


            CheckFilterClearorNot();

            CheckFilterClearorNotMain();
        }

        function OpenAdvanceSearch(e) {

            $("#divAdvanceSearchModel").kendoWindow({
                modal: true,
                width: "97%",
                height: "95%",
                title: "Advanced Search",
                visible: false,
                position: {
                    top: 10,
                    left: 20,
                },
                draggable: false,
                refresh: true,
                actions: [
                    "Close"
                ]

            }).data("kendoWindow").open().center();;
            e.preventDefault();
            return false;
        }

        function OpenAdvanceSearchFilter(e) {
            $('#divAdvanceSearchFilterModel').modal('show');
            e.preventDefault();
            return false;
        }

        function ChangeView() {
            $('#grid1').css('display', 'block');

            $("#grid1").data("kendoGrid").showColumn(0);//Id
            $("#grid1").data("kendoGrid").hideColumn(1);//Risk
            $("#grid1").data("kendoGrid").hideColumn(2);//CId
            $("#grid1").data("kendoGrid").hideColumn(3);//FileName
            $("#grid1").data("kendoGrid").hideColumn(4);//V
            $("#grid1").data("kendoGrid").showColumn(5);//Branch
            $("#grid1").data("kendoGrid").showColumn(6);//SD
            $("#grid1").data("kendoGrid").hideColumn(7);//Event Name
            $("#grid1").data("kendoGrid").hideColumn(8);//Event Nature
            $("#grid1").data("kendoGrid").showColumn(9);//Scheduleon
            $("#grid1").data("kendoGrid").showColumn(10);//ForMonth
            $("#grid1").data("kendoGrid").showColumn(11);//Status
            $("#grid1").data("kendoGrid").hideColumn(12);//VD
            $("#grid1").data("kendoGrid").hideColumn(13);//type
            $("#grid1").data("kendoGrid").hideColumn(14);//Uploaded Date
            $("#grid1").data("kendoGrid").hideColumn(15);//Size

            $("#grid1").data("kendoGrid").hideColumn(16);//Size

            if ($("#dropdownlistComplianceType1").val() == 1)//event based
            {
                $("#grid1").data("kendoGrid").showColumn(7);//Event Name
                $("#grid1").data("kendoGrid").showColumn(8);//Event Nature

                $("#grid1").data("kendoGrid").hideColumn(5);//Branch
                $("#grid1").data("kendoGrid").hideColumn(10);//ForMonth
            }

        }

        function ChangeListView() {
            $('#grid1').css('display', 'block');
            //  $('#grid2').css('display', 'none');
            $("#grid1").data("kendoGrid").showColumn(0);//Id
            $("#grid1").data("kendoGrid").hideColumn(1);//Risk
            $("#grid1").data("kendoGrid").hideColumn(2);//CId
            $("#grid1").data("kendoGrid").showColumn(3);//FileName
            $("#grid1").data("kendoGrid").showColumn(4);//V
            $("#grid1").data("kendoGrid").hideColumn(5);//Branch
            $("#grid1").data("kendoGrid").hideColumn(6);//SD
            $("#grid1").data("kendoGrid").hideColumn(7);//Scheduleon
            $("#grid1").data("kendoGrid").hideColumn(8);//ForMonth
            $("#grid1").data("kendoGrid").hideColumn(9);//Status
            $("#grid1").data("kendoGrid").showColumn(10);//VD
            $("#grid1").data("kendoGrid").hideColumn(11);//type
            $("#grid1").data("kendoGrid").showColumn(12);//Uploaded Date
            $("#grid1").data("kendoGrid").showColumn(13);//Size
            $("#grid1").data("kendoGrid").hideColumn(15);//Size

            $("#grid1").data("kendoGrid").hideColumn(7);//Event Name
            $("#grid1").data("kendoGrid").hideColumn(8);//Event Nature
        }

        function ChangeAuditQView() {
            $('#grid1').css('display', 'none');
            //  $('#grid2').css('display', 'block');            
        }

        $("#export").kendoTooltip({
            filter: ".k-grid-edit3",
            content: function (e) {
                return "Export to Excel";
            }
        });

        $("#exportAdvanced").kendoTooltip({
            filter: ".k-grid-edit3",
            content: function (e) {
                return "Export to Excel";
            }
        });

        function exportReportAdvanced(e) {
            settracknew('Task Report', 'Export', 'Download', '');
            var ReportName = "";
            if ($("#dropdownlistComplianceType1").val() == -1) {
                ReportName = "Statutory";
            }
            if ($("#dropdownlistComplianceType1").val() == 0) {
                ReportName = "Internal";
            }

            var ReportNameAdd = "Report of " + ReportName + " Task";

            var todayDate = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
            var grid = $("#grid1").getKendoGrid();

            //Statutory
            if ($("#dropdownlistComplianceType1").val() == -1) {

                var rows = [
                    {
                        cells: [
                            { value: "Entity/ Location:", bold: true },
                            { value: '<% =CustomerName%>' }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Name:", bold: true },
                            { value: ReportNameAdd }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Generated On:", bold: true },
                            { value: todayDate }
                        ]
                    },
                    {
                        cells: [
                            { value: "" }
                        ]
                    },
                    {
                        cells: [
                            { value: "S.No.", bold: true },
                            { value: "Compliance Location", bold: true },
                            { value: "Reporting Location", bold: true },
                            { value: "Task", bold: true },
                            { value: "Task Description", bold: true },
                            { value: "Task Type Name", bold: true },
                            { value: "Sub Task Type Name", bold: true },
                            { value: "Act", bold: true },
                            { value: "Compliance", bold: true },
                            { value: "Compliance Description", bold: true },
                            { value: "Period", bold: true },
                            { value: "Due Date", bold: true },
                            { value: "Close Date", bold: true },
                            { value: "Status", bold: true },
                            { value: "Performer", bold: true },
                            { value: "Reviewer", bold: true }
                        ]
                    }
                ];

                var trs = grid.dataSource;
                var filteredDataSource = new kendo.data.DataSource({
                    data: trs.data(),
                    filter: trs.filter()
                });

                filteredDataSource.read();
                var data = filteredDataSource.view();
                for (var i = 0; i < data.length; i++) {
                    var dataItem = data[i];
                    rows.push({
                        cells: [ // dataItem."Whatever Your Attributes Are"
                            { value: '' },
                            { value: dataItem.Branch },
                            { value: dataItem.TaskCustomerBranchName },
                            { value: dataItem.TaskTitle },
                            { value: dataItem.TaskDescription },
                            { value: '' },
                            { value: '' },
                            { value: dataItem.ActName },
                            { value: dataItem.ShortDescription },
                            { value: dataItem.DetailDescription },
                            { value: dataItem.ForMonth },
                            { value: dataItem.ScheduledOn },
                            { value: dataItem.CloseDate },
                            { value: dataItem.Status },
                            { value: dataItem.PerformerName },
                            { value: dataItem.ReviewerName }
                        ]
                    });

                }
                for (var i = 4; i < rows.length; i++) {
                    for (var j = 0; j < 16; j++) {
                        rows[i].cells[j].borderBottom = "#000000";
                        rows[i].cells[j].borderLeft = "#000000";
                        rows[i].cells[j].borderRight = "#000000";
                        rows[i].cells[j].borderTop = "#000000";
                        rows[i].cells[j].hAlign = "left";
                        rows[i].cells[j].vAlign = "top";
                        rows[i].cells[j].wrap = true;

                        if (i != 4) {
                            rows[i].cells[0].value = i - 4;
                        }
                        if (i == 4) {
                            rows[4].cells[j].background = "#A9A9A9";
                        }
                    }
                }

                excelExport(rows, ReportName);
            }

            //Internal
            if ($("#dropdownlistComplianceType1").val() == 0) {

                var rows = [
                    {
                        cells: [
                            { value: "Entity/ Location:", bold: true },
                            { value: '<% =CustomerName%>' }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Name:", bold: true },
                            { value: ReportNameAdd }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Generated On:", bold: true },
                            { value: todayDate }
                        ]
                    },
                    {
                        cells: [
                            { value: "" }
                        ]
                    },
                    {
                        cells: [
                            { value: "S.No.", bold: true },
                            { value: "Compliance Location", bold: true },
                            { value: "Reporting Location", bold: true },
                            { value: "Task", bold: true },
                            { value: "Task Description", bold: true },
                            { value: "Compliance", bold: true },
                            { value: "Period", bold: true },
                            { value: "Due Date", bold: true },
                            { value: "Close Date", bold: true },
                            { value: "Status", bold: true },
                            { value: "Remark", bold: true },
                            { value: "Performer", bold: true },
                            { value: "Reviewer", bold: true }
                        ]
                    }
                ];

                var trs = grid.dataSource;
                var filteredDataSource = new kendo.data.DataSource({
                    data: trs.data(),
                    filter: trs.filter()
                });

                filteredDataSource.read();
                var data = filteredDataSource.view();
                for (var i = 0; i < data.length; i++) {
                    var dataItem = data[i];
                    rows.push({
                        cells: [ // dataItem."Whatever Your Attributes Are"
                            { value: '' },
                            { value: dataItem.Branch },
                            { value: dataItem.TaskCustomerBranchName },
                            { value: dataItem.TaskTitle },
                            { value: dataItem.TaskDescription },
                            { value: dataItem.ShortDescription },
                            { value: dataItem.ForMonth },
                            { value: dataItem.ScheduledOn },
                            { value: dataItem.CloseDate },
                            { value: dataItem.Status },
                            { value: '' },
                            { value: dataItem.PerformerName },
                            { value: dataItem.ReviewerName }
                        ]
                    });

                }

                for (var i = 4; i < rows.length; i++) {
                    for (var j = 0; j < 13; j++) {
                        rows[i].cells[j].borderBottom = "#000000";
                        rows[i].cells[j].borderLeft = "#000000";
                        rows[i].cells[j].borderRight = "#000000";
                        rows[i].cells[j].borderTop = "#000000";
                        rows[i].cells[j].hAlign = "left";
                        rows[i].cells[j].vAlign = "top";
                        rows[i].cells[j].wrap = true;

                        //rows[i].cells[j].style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        if (i != 4) {
                            rows[i].cells[0].value = i - 4;
                        }
                        if (i == 4) {
                            rows[4].cells[j].background = "#A9A9A9";
                        }
                    }
                }

                excelExport(rows, ReportName);
            }

            e.preventDefault();
        }


        function exportReport(e) {
            settracknew('Task Report', 'Export', 'Download', '');
            var ReportName = "";
            if ($("#dropdownlistComplianceType").val() == -1) {
                ReportName = "Statutory";
            }
            if ($("#dropdownlistComplianceType").val() == 0) {
                ReportName = "Internal";
            }

            var ReportNameAdd = "Report of " + ReportName + " Task";

            var todayDate = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
            var grid = $("#grid").getKendoGrid();

            //Statutory
            if ($("#dropdownlistComplianceType").val() == -1) {

                var rows = [
                    {
                        cells: [
                            { value: "Entity/ Location:", bold: true },
                            { value: '<% =CustomerName%>' }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Name:", bold: true },
                            { value: ReportNameAdd }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Generated On:", bold: true },
                            { value: todayDate }
                        ]
                    },
                    {
                        cells: [
                            { value: "" }
                        ]
                    },
                    {
                        cells: [
                            { value: "S.No.", bold: true },
                            { value: "Compliance Location", bold: true },
                            { value: "Reporting Location", bold: true },
                            { value: "Task", bold: true },
                            { value: "Task Description", bold: true },
                            { value: "Task Type Name", bold: true },
                            { value: "Sub Task Type Name", bold: true },
                            { value: "Act", bold: true },
                            { value: "Compliance", bold: true },
                            { value: "Compliance Description", bold: true },
                            { value: "Period", bold: true },
                            { value: "Due Date", bold: true },
                            { value: "Close Date", bold: true },
                            { value: "Status", bold: true },
                            { value: "Performer", bold: true },
                            { value: "Reviewer", bold: true }
                        ]
                    }
                ];

                var trs = grid.dataSource;
                var filteredDataSource = new kendo.data.DataSource({
                    data: trs.data(),
                    filter: trs.filter()
                });

                filteredDataSource.read();
                var data = filteredDataSource.view();
                for (var i = 0; i < data.length; i++) {
                    var dataItem = data[i];
                    rows.push({
                        cells: [ // dataItem."Whatever Your Attributes Are"
                            { value: '' },
                            { value: dataItem.Branch },
                            { value: dataItem.TaskCustomerBranchName },
                            { value: dataItem.TaskTitle },
                            { value: dataItem.TaskDescription },
                            { value: '' },
                            { value: '' },
                            { value: dataItem.ActName },
                            { value: dataItem.ShortDescription },
                            { value: dataItem.DetailDescription },
                            { value: dataItem.ForMonth },
                            { value: dataItem.ScheduledOn },
                            { value: dataItem.CloseDate },
                            { value: dataItem.Status },
                            { value: dataItem.PerformerName },
                            { value: dataItem.ReviewerName }
                        ]
                    });

                }
                for (var i = 4; i < rows.length; i++) {
                    for (var j = 0; j < 16; j++) {
                        rows[i].cells[j].borderBottom = "#000000";
                        rows[i].cells[j].borderLeft = "#000000";
                        rows[i].cells[j].borderRight = "#000000";
                        rows[i].cells[j].borderTop = "#000000";
                        rows[i].cells[j].hAlign = "left";
                        rows[i].cells[j].vAlign = "top";
                        rows[i].cells[j].wrap = true;

                        if (i != 4) {
                            rows[i].cells[0].value = i - 4;
                        }
                        if (i == 4) {
                            rows[4].cells[j].background = "#A9A9A9";
                        }
                    }
                }

                excelExport(rows, ReportName);
            }

            //Internal
            if ($("#dropdownlistComplianceType").val() == 0) {

                var rows = [
                    {
                        cells: [
                            { value: "Entity/ Location:", bold: true },
                            { value: '<% =CustomerName%>' }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Name:", bold: true },
                            { value: ReportNameAdd }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Generated On:", bold: true },
                            { value: todayDate }
                        ]
                    },
                    {
                        cells: [
                            { value: "" }
                        ]
                    },
                    {
                        cells: [
                            { value: "S.No.", bold: true },
                            { value: "Compliance Location", bold: true },
                            { value: "Reporting Location", bold: true },
                            { value: "Task", bold: true },
                            { value: "Task Description", bold: true },
                            { value: "Compliance", bold: true },
                            { value: "Period", bold: true },
                            { value: "Due Date", bold: true },
                            { value: "Close Date", bold: true },
                            { value: "Status", bold: true },
                            { value: "Remark", bold: true },
                            { value: "Performer", bold: true },
                            { value: "Reviewer", bold: true }
                        ]
                    }
                ];

                var trs = grid.dataSource;
                var filteredDataSource = new kendo.data.DataSource({
                    data: trs.data(),
                    filter: trs.filter()
                });

                filteredDataSource.read();
                var data = filteredDataSource.view();
                for (var i = 0; i < data.length; i++) {
                    var dataItem = data[i];
                    rows.push({
                        cells: [ // dataItem."Whatever Your Attributes Are"
                            { value: '' },
                            { value: dataItem.Branch },
                            { value: dataItem.TaskCustomerBranchName },
                            { value: dataItem.TaskTitle },
                            { value: dataItem.TaskDescription },
                            { value: dataItem.ShortDescription },
                            { value: dataItem.ForMonth },
                            { value: dataItem.ScheduledOn },
                            { value: dataItem.CloseDate },
                            { value: dataItem.Status },
                            { value: '' },
                            { value: dataItem.PerformerName },
                            { value: dataItem.ReviewerName }
                        ]
                    });

                }

                for (var i = 4; i < rows.length; i++) {
                    for (var j = 0; j < 13; j++) {
                        rows[i].cells[j].borderBottom = "#000000";
                        rows[i].cells[j].borderLeft = "#000000";
                        rows[i].cells[j].borderRight = "#000000";
                        rows[i].cells[j].borderTop = "#000000";
                        rows[i].cells[j].hAlign = "left";
                        rows[i].cells[j].vAlign = "top";
                        rows[i].cells[j].wrap = true;

                        //rows[i].cells[j].style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        if (i != 4) {
                            rows[i].cells[0].value = i - 4;
                        }
                        if (i == 4) {
                            rows[4].cells[j].background = "#A9A9A9";
                        }
                    }
                }

                excelExport(rows, ReportName);
            }

            e.preventDefault();
            //return false;
        };

        function excelExport(rows, ReportName) {

            var workbook = new kendo.ooxml.Workbook({
                sheets: [
                    {
                        columns: [
                            { autoWidth: false },
                            { autoWidth: false },
                            { width: 300 },
                            { width: 300 },
                            { width: 250 },
                            { width: 150 },
                            { width: 150 },
                            { width: 250 },
                            { width: 300 },
                            { width: 350 },
                            { width: 250 },
                            { width: 150 },
                            { width: 150 },
                            { width: 250 },
                            { width: 250 },
                            { width: 250 }
                        ],
                        title: "TaskReport",
                        rows: rows
                    },
                ]
            });

            var nameOfPage = ReportName + "_TaskReport";
            //var nameOfPage = "Test-1"; // insert here however you are getting name of screen
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });

        }



        $("#newModelClose").on("click", function () {
            myWindow3.close();
        });


    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example">
        <div>
           <div style="margin: 0.5% 0 0.5%;">
                    <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 25%; margin-right: 0.8%;" />
                    <input id="dropdownlistComplianceType" data-placeholder="Type" style="width: 15%; margin-right: 0.8%;" />
                    <input id="dropdownlistStatus" data-placeholder="Status" style="width: 15%; margin-right: 0.8%;" />
                    <input id="dropdownlistTypePastdata" data-placeholder="Status" style="width: 15%; margin-right: 0.8%;" />
                    <button type="button" id="export" style="float: right; height: 30px;" onclick="exportReport(event)"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>                
                    <button type="button" id="AdavanceSearch" style="float: right; height: 30px; margin-right: 0.8%;" onclick="OpenAdvanceSearch(event)"><span onclick="javascript:return false;"></span>Advanced Search</button>
                </div>
           
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; color: black;" id="filtersstoryboard">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filtertype">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filterrisk">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; color: black;" id="filterstatus">&nbsp;</div>

        </div>
        <div id="grid"></div>
        <div>
            <div id="divAdvanceSearchModel" style="padding-top: 5px; display: none;">
                <div class="row">
                    <div class="col-md-10" style="padding-left: 0px; padding-bottom: 4px; display: none;">
                        <button id="primaryTextButton1" onclick="ChangeView()">Grid View</button>
                        <button id="primaryTextButton" onclick="ChangeListView()">List View</button>
                    </div>
                </div>
                <div class="row" style="margin-left: -9px;">
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-2" id="dvdropdowntree1" style="width: 20%; padding-left: 9px;">
                            <input id="dropdowntree1" data-placeholder="Entity/Sub-Entity/Location" style="width: 265px;" />
                        </div>
                        <div class="col-md-2" id="dvdropdownFY" style="width: 15%; padding-left: 0px;">
                            <input id="dropdownFY" data-placeholder="Finance Year" style="width: 102%;" />
                        </div>
                        <div class="col-md-2" id="dvdropdownUser" style="width: 13%; padding-left: 4px;">
                            <input id="dropdownlistStatus1" data-placeholder="Status" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" id="dvdropdownlistComplianceType1" style="width: 15.3%; padding-left: 0px;">
                            <input id="dropdownlistComplianceType1" data-placeholder="Type" style="width: 100%;" />
                        </div>

                        <div class="col-md-2" id="dvStartdatepicker" style="width: 15%; padding-left: 0px;">
                            <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" title="startdatepicker" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" id="dvLastdatepicker" style="width: 13%; padding-left: 0px;">
                            <input id="Lastdatepicker" placeholder="End Date" title="enddatepicker" style="width: 115%;" />
                        </div>
                    </div>
                </div>


                <div class="row" style="margin-left: -9px; margin-top: 13px; margin-bottom: 5px;">
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-2" id="dvdropdownComplianceSubType" style="width: 20%; padding-left: 9px;">
                            <input id="dropdownPastData" style="width: 100%;" />
                        </div>

                        <div class="col-md-2" style="width: 13%; padding-left: 0px; display: none;">
                            <input id="SearchTag" type="text" style="width: 100%;" class="k-textbox" placeholder="Document Tag" />
                        </div>
                        <div class="col-md-4" id="dvdropdownACT" style="width: 29.6%; padding-left: 0px;">
                            <input id="dropdownACT" data-placeholder="Act" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" id="dvsearch" style="width: 15%; padding-left: 0px;">
                            <input id='Searchfilter' class='k-textbox' placeholder="Type to Search" style="width: 100%;" />
                        </div>
                        <div class="col-md-2" style="width: 13.4%; padding-left: 0px; display: none;" id="dvdropdownlistStatus1">
                            <%if (RoleFlag == 1)%>
                            <%{%>
                            <input id="dropdownUser" data-placeholder="User" style="width: 112%;" />
                            <%}%>
                        </div>
                        <div class="col-md-2" id="dvexport" style="width: 15%; padding-left: 3px;">
                            <button id="exportAdvanced" style="height: 22px; margin-right: 10px;" onclick="exportReportAdvanced(event)"><span class="k-icon k-i-excel k-grid-edit3" style="margin-right: 2px;"></span>Export</button>
                        </div>
                    </div>
                </div>






                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterCompType">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterCategory">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterAct">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterCompSubType">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterStartDate">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterLastDate">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filtersstoryboard1">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filtertype1">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterrisk1">&nbsp;</div>

                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterpstData1">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterUser">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterFY">&nbsp;</div>
                <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: #535b6a; font-weight: bold;" id="filterstatus1">&nbsp;</div>


                <div id="grid1"></div>
            </div>

        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {

            fhead('My Reports / Task Report');
            setactivemenu('Myreporttask');
            fmaters1();
        });

    </script>
</asp:Content>
