﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ActDeactivation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.ActDeactivation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Newjs/tagging.js" type="text/javascript"></script>
    <style type="text/css">
        input.custom-combobox-input.ui-widget.ui-widget-content.ui-state-default.ui-corner-left.ui-autocomplete-input {
            width: 200px;
        }
    </style>
    <script type="text/javascript">
         function initializeCombobox() {
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;" align="center">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 54%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional" OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>

            <asp:Panel ID="vdpanel" runat="server">
                <asp:ValidationSummary ID="vsActValidationGroup" runat="server"
                    class="alert alert-block alert-danger fade in" DisplayMode="BulletList" ValidationGroup="ComplianceValidationGroup1" ForeColor="red" />
                <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceValidationGroup1" Display="None" Enabled="true" ShowSummary="true" />
                <asp:Label runat="server" ID="lblErrorMassage1" ForeColor="Red"></asp:Label>
            </asp:Panel>

            <table>
                <tr>
                    <td width="25%">
                        <asp:DropDownList runat="server" ID="ddlFilterCategory" Style="padding: 0px; margin: 0px; height: 22px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlfilterCatagory_SelectedIndexChanged">
                        </asp:DropDownList>

                    </td>
                    <td width="25%">
                        <asp:DropDownList runat="server" ID="ddlFilterType" Style="padding: 0px; margin: 0px; height: 22px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlfilterType_SelectedIndexChanged" />

                    </td>
                    <td width="25%">
                        <asp:DropDownList ID="ddlfilterState" runat="server" AutoPostBack="true" CssClass="txtbox"
                            OnSelectedIndexChanged="ddlfilterState_SelectedIndexChanged" Style="padding: 0px; margin: 0px; height: 22px;">
                        </asp:DropDownList>


                    </td>
                    <td style="width: 25%; padding-right: 20px;" align="right">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true" OnTextChanged="tbxFilter_TextChanged" />
                    </td>

                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" ID="lbldeact" Text="Select Act to Deactivate"></asp:Label>
                    </td>
                    <td>
                        <div style="width: 150px; float: left; margin-top: 0px;">
                            <asp:DropDownList runat="server" ID="ddlDeactiAct" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddldeactiveAct_SelectedIndexChanged" />
                            <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select act for Deactivation"
                                ControlToValidate="ddlDeactiAct" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                ValidationGroup="ComplianceValidationGroup1" Display="None" />
                        </div>

                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Label runat="server" ID="lblalteact" Text="Select Alternative Act"></asp:Label>
                    </td>
                    <td>
                        <div style="width: 150px; float: left; margin-top: 0px;">
                            <asp:DropDownList runat="server" ID="ddlAlterAct" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlAlternativeAct_SelectedIndexChanged" />
                            <%-- <asp:CompareValidator ID="CompareValidator4" ErrorMessage="Please select alternative act"
                            ControlToValidate="ddlAlterAct" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" />--%>
                        </div>
                    </td>
                    <td class="newlink">
                        <asp:LinkButton Text="Add New" runat="server" ID="btnAdd" OnClick="btnAdd_Click" />&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label1" Visible="false" runat="server" Text=""></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td>Deactivation Date : </td>
                    <td>
                        <asp:TextBox ID="tbxStartDate" runat="server" MaxLength="50" Width="250px" Style="padding: 0px; margin: 0px; height: 30px; width: 150px;" CssClass="StartDate" />
                        <asp:RequiredFieldValidator ID="rfvDeDate" ErrorMessage="Please select Deactivation date"
                            ControlToValidate="tbxStartDate" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" />
                    </td>
                </tr>
                <tr>
                    <td>Deactivation Description : </td>
                    <td>
                        <asp:TextBox ID="txtDedescription" runat="server" AutoPostBack="true" MaxLength="150" TextMode="MultiLine" Width="250px" />
                        <asp:RequiredFieldValidator ID="rfvDeDescription" ErrorMessage="Please Fill Deactivation description"
                            ControlToValidate="txtDedescription" runat="server" ValueToCompare="-1" Operator="NotEqual"
                            ValidationGroup="ComplianceValidationGroup1" Display="None" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>

            </table>
            <div style="margin-bottom: 7px; margin-left: 50px; margin-top: 10px;">
                <asp:Button Text="Send request" runat="server" ID="Button1" OnClick="btnSaveDeactivate_Click" CssClass="button" OnClientClick="if (!ValidateFile()) return false;"
                    ValidationGroup="ComplianceValidationGroup1" />
                <%-- <asp:Button Text="Act Deactivate" runat="server" ID="Button2" OnClick="btnSaveDeactivate_Click" CssClass="button" OnClientClick="if (!ValidateFile()) return false;"
                           ValidationGroup="ComplianceValidationGroup1"   />--%>
            </div>

            <asp:Panel ID="Panel1" runat="server">
                <asp:GridView runat="server" ID="grdAct" AutoGenerateColumns="false" GridLines="Vertical" OnRowDataBound="grdCompliances_RowDataBound"
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnRowCreated="grdCompliances_RowCreated"
                    CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" Width="100%"
                    Font-Size="12px" DataKeyNames="ComplianceID" OnRowCommand="grdCompliances_RowCommand" OnPageIndexChanging="grdCompliances_PageIndexChanging">
                    <Columns>

                        <asp:BoundField DataField="ComplianceID" HeaderText="ComplianceID" ItemStyle-Width="50px" SortExpression="ComplianceID" />
                        <asp:TemplateField HeaderText="Customer Name" ItemStyle-Height="20px" HeaderStyle-Height="20px" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="100px" SortExpression="CustomerName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" Text='<%# Eval("CustomerName") %>' ToolTip='<%# Eval("CustomerName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Customer Branch Name" ItemStyle-Width="80px" HeaderStyle-HorizontalAlign="Left" SortExpression="BranchName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label runat="server" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Section" ItemStyle-Width="80px" HeaderStyle-HorizontalAlign="Left" SortExpression="Section">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label runat="server" Text='<%# Eval("Sections") %>' ToolTip='<%# Eval("Sections") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" SortExpression="ShortDescription">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                    <asp:Label runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <%-- <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_COMPLIANCE"                                     
                                    CommandArgument='<%# Eval("ComplianceID")%>'
                                    OnClientClick="return confirm('Are you certain you want to delete this compliance?');"><img src="../Images/delete_icon.png" alt="Delete Compliance"/></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                        </asp:TemplateField>--%>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divActDeactivationDetailsDialog">
        <asp:UpdatePanel ID="upComplianceDetails1" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
            <ContentTemplate>

                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="vsActdeactivation" runat="server"
                            class="alert alert-block alert-danger fade in" DisplayMode="BulletList" ValidationGroup="ComplianceValidationGroup" ForeColor="red" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Name</label>
                        <asp:TextBox runat="server" ID="tbxName" Style="height: 30px; width: 390px;" MaxLength="100" ToolTip="" TextMode="MultiLine" />
                        <asp:RequiredFieldValidator ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                            runat="server" ValidationGroup="ComplianceValidationGroup" Display="None" />

                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Type</label>
                        <asp:DropDownList runat="server" ID="ddlType" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" />
                        <asp:CompareValidator ID="CompareValidator3" ErrorMessage="Please select Type." ControlToValidate="ddlType"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Category</label>
                        <asp:DropDownList runat="server" ID="ddlCategory" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" />
                        <asp:CompareValidator ErrorMessage="Please select Category." ControlToValidate="ddlCategory"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px" id="divState" visible="false" runat="server">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            State</label>
                        <asp:DropDownList runat="server" ID="ddlState" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" />
                        <asp:CompareValidator ID="cmpvState" ErrorMessage="Please select State." ControlToValidate="ddlState"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div runat="server" id="divCity" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            City</label>
                        <asp:DropDownList runat="server" ID="ddlCity" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" />
                    </div>
                    <div runat="server" id="div1" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Start Date</label>
                        <asp:TextBox runat="server" ID="TextBox1" Style="padding: 0px; margin: 0px; height: 30px; width: 150px;" CssClass="StartDate" />
                        <asp:RequiredFieldValidator ID="rfvStartDate" ErrorMessage="Please Select Start Date."
                            ControlToValidate="tbxStartDate" runat="server" ValidationGroup="ComplianceValidationGroup"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Industry
                        </label>
                        <asp:TextBox runat="server" ID="txtIndustry" Style="padding: 0px; margin: 0px; height: 30px; width: 390px;"
                            CssClass="txtbox" />
                        <div style="margin-left: 150px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 100px;" id="dvIndustry">
                            <asp:Repeater ID="rptIndustry" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="IndustrySelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkIndustry" runat="server" onclick="UncheckHeader();" /></td>
                                        <td style="width: 200px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                <asp:Label ID="lblIndustryID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblIndustryName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Department</label>
                        <asp:DropDownList runat="server" ID="ddlDepartment" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" />
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Ministry</label>
                        <asp:DropDownList runat="server" ID="ddlMinistry" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" />
                    </div>

                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Regulator</label>
                        <asp:DropDownList runat="server" ID="ddlRegulator" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                            CssClass="txtbox" AutoPostBack="true" />
                    </div>

                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;"></label>
                    <div runat="server" id="divfile" style="margin-bottom: 7px; margin-left: 2%;">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            File</label>
                        <asp:FileUpload runat="server" ID="ActFile_upload" AllowMultiple="true" />


                    </div>

                    <div id="DivSave" style="margin-bottom: 7px; margin-left: 152px;" runat="server">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClientClick="if (!ValidateFile()) return false;" OnClick="btnActSave_Click" CssClass="button" ValidationGroup="ComplianceValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClick="btnCancel_Click" />
                    </div>

                    <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px;">

                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>

                        <%--   <label style="width: 208px; display: block; float: left; font-size: 13px; color: red;">Note :: (*) Fields Are Compulsary</label>--%>
                    </div>
                </div>


            </ContentTemplate>

            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>

        </asp:UpdatePanel>
        <asp:HiddenField ID="saveopo" runat="server" Value="false" />
        <div style="margin-bottom: 7px; margin-left: 0px" id="Div5" runat="server">
            <asp:Label ID="Label2" runat="server" Style="color: red"></asp:Label>
        </div>
    </div>



    <script type="text/javascript">
        $(function () {
            $('#divActDeactivationDetailsDialog').dialog({
                height: 450,
                width: 900,
                autoOpen: false,
                draggable: true,
                title: "Act Deactivation",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });


            if (document.getElementById('BodyContent_saveopo').value == "true") {
                newfun();
                document.getElementById('BodyContent_saveopo').value = "false";
            }
            else {
                $("#divActDeactivationDetailsDialog").dialog('close');
            }
        });

        function newfun() {
            $("#divActDeactivationDetailsDialog").dialog('open');

        }
            //initializeCombobox();
       
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
    </script>



    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

           function initializeDatePicker(todayDate) {

            var startDate = new Date();
            var maxyr = new Date().getFullYear().toString();
            maxyr = parseInt(maxyr) + 100;

            $(".StartDate").datepicker({
                //showOn: 'button',
                // buttonImageOnly: true,
                //buttonImage: '../Images/Cal.png',
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1,
                changeYear: true,
                yearRange: '1900:' + maxyr.toString()
                ,
                beforeShow: function (input, inst) {

                    var date = $("#<%= tbxStartDate.ClientID %>").val();

                    if (date.toString().trim() != '') {

                        //startDate = date;
                        setTimeout(function () {
                            inst.dpDiv.find('a.ui-state-highlight').removeClass('ui-state-highlight');
                        }, 100);
                    }


                    // $(".StartDate").datepicker('setDate', startDate);
                    $(".StartDate").datepicker('setDate', date);

                },
                onClose: function (dateText, inst) {

                    if (dateText != null) {
                        $("#<%= tbxStartDate.ClientID %>").val(dateText);
                }
                }
            });

        $("html").on("mouseenter", ".ui-datepicker-trigger", function () {
            $(this).attr('title', 'Select Start Date');
        });
    }
    </script>
    <script type="text/javascript">
        function initializeCombobox() {
            $("#<%= ddlAlterAct.ClientID %>").combobox();
            $("#<%= ddlDeactiAct.ClientID %>").combobox();
            $("#<%= ddlFilterCategory.ClientID %>").combobox();
            $("#<%= ddlfilterState.ClientID %>").combobox();
            $("#<%= ddlFilterType.ClientID %>").combobox();
            $("#<%= ddlType.ClientID %>").combobox();
            $("#<%= ddlCategory.ClientID %>").combobox();
            $("#<%= ddlState.ClientID %>").combobox();
            $("#<%= ddlCity.ClientID %>").combobox();
            $("#<%= ddlDepartment.ClientID %>").combobox();
            $("#<%= ddlMinistry.ClientID %>").combobox();
            $("#<%= ddlRegulator.ClientID %>").combobox();
            
            
         
        }

        $(document).ready(function () {
            initializeCombobox();                
            
        });

          function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        var validFilesTypes = ["exe", "bat", "dll"];

        function ValidateFile() {

            var label = document.getElementById("<%=Label2.ClientID%>");
            var fuSampleFile = $("#<%=ActFile_upload.ClientID%>").get(0).files;
            var isValidFile = true;

            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (validFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
            }

            if (!isValidFile) {
                label.style.color = "red";
                label.innerHTML = "Invalid file uploded. .exe,.bat formats not supported.";
            }
            return isValidFile;
        }

        function fopendocfileReview(path) {

            if (path != '') {
                window.open('../docviewer.aspx?docurl=' + path);
            }
        }
        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkIndustry") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkIndustry']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkIndustry']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='IndustrySelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

      

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }


    </script>

</asp:Content>
