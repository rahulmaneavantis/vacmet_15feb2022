﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ScheduleonchangeReport : System.Web.UI.Page
    {
        public static int userID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                userID = Convert.ToInt32(AuthenticationHelper.UserID);
                Binddata();
                Bindcustomer(userID);
            }
        }

     

        #region[Export to excel]
        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {

              
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("ScheduleChangeReport");
                        DataTable ExcelData = null;
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {

                          
                            var Data = (from row in entities.Data_LogScheduleonChange()
                                        select row).ToList();


                        if (ddlCustomer.SelectedValue != "")
                        {
                            if (Convert.ToInt32(ddlCustomer.SelectedValue) != -1)
                            {
                                int c = Convert.ToInt32(ddlCustomer.SelectedValue);
                                Data = Data.Where(entry => entry.ID == c).ToList();
                            }
                        }
                        if (!string.IsNullOrEmpty(tbxFilter.Text))
                        {
                            if (CheckInt(tbxFilter.Text))
                            {
                                int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                                Data = Data.Where(entry => entry.ID == a).ToList();
                            }
                            else
                            {
                                Data = Data.Where(entry => entry.ShortDescription.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                            }
                        }

                        DataTable table = Data.ToDataTable();

                            DataView view = new System.Data.DataView(table);
                            List<Data_LogScheduleonChange_Result> cb = new List<Data_LogScheduleonChange_Result>();

                            ExcelData = view.ToTable("Selected", false, "ID", "CustomerName", "BranchName", "ShortDescription", "Period", "Olddate", "NewDate", "CreatedBy", "CreatedDate");


                        foreach (DataRow item in ExcelData.Rows)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(item["Olddate"])))
                            {
                                item["Olddate"] = Convert.ToDateTime(item["Olddate"]).ToString("dd-MMM-yyyy");
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(item["NewDate"])))
                            {
                                item["NewDate"] = Convert.ToDateTime(item["NewDate"]).ToString("dd-MMM-yyyy");
                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(item["CreatedDate"])))
                            {
                                item["CreatedDate"] = Convert.ToDateTime(item["CreatedDate"]).ToString("dd-MMM-yyyy");
                            }
                        }


                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Value = "Report Name:";

                        exWorkSheet.Cells["B1"].Merge = true;
                        exWorkSheet.Cells["B1"].Value = "Compliance Schedule Change Report";

                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Value = "Report Generated On:";

                        exWorkSheet.Cells["B2"].Merge = true;
                        exWorkSheet.Cells["B2"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                    

                            exWorkSheet.Cells["A3:I3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["A3:I3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);



                            exWorkSheet.Cells["A3"].LoadFromDataTable(ExcelData, true);
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A3"].Value = "ComplianceId";
                            exWorkSheet.Cells["A3"].AutoFitColumns(25);

                            exWorkSheet.Cells["B3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B3"].Value = "Customer Name";
                            exWorkSheet.Cells["B3"].AutoFitColumns(40);

                            exWorkSheet.Cells["C3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C3"].Value = "BranchName";
                            exWorkSheet.Cells["C3"].AutoFitColumns(40);

                            exWorkSheet.Cells["D3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D3"].Value = "ShortDescription";
                            exWorkSheet.Cells["D3"].AutoFitColumns(25);


                            exWorkSheet.Cells["E3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["E3"].Value = "Period";
                            exWorkSheet.Cells["E3"].AutoFitColumns(25);

                            exWorkSheet.Cells["F3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["F3"].Value = "Olddate";
                            exWorkSheet.Cells["F3"].AutoFitColumns(25);

                            exWorkSheet.Cells["G3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["G3"].Value = "NewDate";
                            exWorkSheet.Cells["G3"].AutoFitColumns(25);

                        exWorkSheet.Cells["H3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H3"].Style.Font.Size = 12;
                        exWorkSheet.Cells["H3"].Value = "CreatedBy";
                        exWorkSheet.Cells["H3"].AutoFitColumns(25);

                        exWorkSheet.Cells["I3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I3"].Style.Font.Size = 12;
                        exWorkSheet.Cells["I3"].Value = "CreatedDate";
                        exWorkSheet.Cells["I3"].AutoFitColumns(25);
                        

                    }
                        using (ExcelRange col = exWorkSheet.Cells[1, 1, 3 + ExcelData.Rows.Count, 9])
                        {

                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        


                    }
                    using (ExcelRange col = exWorkSheet.Cells[1, 6, 1 + ExcelData.Rows.Count, 9])
                    {
                        col.Style.Numberformat.Format = "dd/MM/yyyy";
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=ScheduleChangeReport.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        #endregion
        public  void Bindcustomer(int UserID)
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                //ddlCustomer.DataSource = GetCustomer();
                ddlCustomer.DataSource = GetAllCustomer(UserID);
                ddlCustomer.DataBind();

                ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
               
            }
        }
        public static object GetAllCustomer(int userID, string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Customers
                             join row1 in entities.CustomerAssignmentDetails
                             on row.ID equals row1.CustomerID
                             where row1.UserID == userID
                             && row.IsDeleted == false
                             && row1.IsDeleted == false 
                             select row);

                if (!string.IsNullOrEmpty(filter))
                {
                    users = users.Where(entry => entry.Name.ToUpper().Contains(filter) || entry.BuyerName.ToUpper().Contains(filter) || entry.BuyerEmail.ToUpper().Contains(filter));
                }

                return users.Distinct().ToList();
            }
        }
        public static object GetCustomer()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Customers
                            select row).ToList();
                return data;

            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            Binddata();
        }

        protected void grdReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdReport.PageIndex = e.NewPageIndex;
                Binddata();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private  void Binddata()
        {
            grdReport.DataSource = null;
            grdReport.DataBind();

          
            var comList = GetComplianceAssigned().ToList();

            if (ddlCustomer.SelectedValue != "")
            {
                if (Convert.ToInt32(ddlCustomer.SelectedValue) != -1)
                {
                    int c = Convert.ToInt32(ddlCustomer.SelectedValue);
                    comList = comList.Where(entry => entry.ID == c).ToList();
                }
            }

            if (!string.IsNullOrEmpty(tbxFilter.Text))
            {
                if (CheckInt(tbxFilter.Text))
                {
                    int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                    comList = comList.Where(entry => entry.ID == a ).ToList();
                }
                else
                {
                    comList = comList.Where(entry => entry.ShortDescription.ToUpper().Contains(tbxFilter.Text.ToUpper()) ).ToList();
                }
            }

            grdReport.DataSource = comList;
            grdReport.DataBind();
        }

        protected void grdReport_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
             
                var comList = GetComplianceAssigned().ToList();

                if (direction == SortDirection.Ascending)
                {
                    comList = comList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    comList = comList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdReport.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdReport.Columns.IndexOf(field);
                    }
                }
                grdReport.DataSource = comList;
                grdReport.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            Binddata();
        }
        public static List<Data_LogScheduleonChange_Result> GetComplianceAssigned()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.Database.CommandTimeout = 360;
                // int userid = Convert.ToInt32(AuthenticationHelper.UserID);
                var result = entities.Data_LogScheduleonChange().ToList();

                //if (branch != -1)
                //{
                //    result = result.Where(entry => entry.BranchID == branch).ToList();
                //}
                return result;
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdReport_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
      
    }
}