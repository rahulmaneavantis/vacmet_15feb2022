﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Reflection;
using System.Threading;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Collections;
using System.Data;
using System.IO;
using System.Globalization;
using Ionic.Zip;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class CheckList : System.Web.UI.Page
    {
        public string Role;
        public static string role1;
        protected static string queryStringFlag = "";
        public static string CompDocReviewPath = "";
        protected string UploadDocumentLink;
        protected void Page_Load(object sender, EventArgs e)
        {
            string listCustomer = ConfigurationManager.AppSettings["UploadUsingLinkCustomerlist"];
            string[] listCust = new string[] { };
            if (!string.IsNullOrEmpty(listCustomer))
                listCust = listCustomer.Split(',');

            if (listCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                UploadDocumentLink = "True";

            if (!IsPostBack)
            {
                try
                {
                    string type1 = Request.QueryString["type"];
                    string filter1 = Request.QueryString["filter"];
                    role1 = Request.QueryString["role"];

                    BindLocationFilter();
                    BindComplianceType();
                    BindComplianceCategory();
                    BindActList();
                    BindStatus();
                    queryStringFlag = "A";
                    BindCompliances(role1, queryStringFlag);

                    if (role1 == "Performer")
                    {
                        liPerformer.Text = "Performer";
                        lblRole.Text = "Performer";
                    }
                    else
                    {
                        liPerformer.Text = "Reviewer";
                        lblRole.Text = "Reviewer";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
            udcStatusTranscatopnChkList.OnSaved += (inputForm, args) => { BindCompliances(role1, queryStringFlag); };

        }

        #region Search

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;

                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, "EXCT", "S");
                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocationPerformer.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocationPerformer.Nodes.Add(node);
                }

                tvFilterLocationPerformer.CollapseAll();

                divFilterLocationPerformer.Style.Add("display", "none");

                tvFilterLocationPerformer_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocationPerformer_SelectedNodeChanged(object sender, EventArgs e)
        {

            try
            {
                tbxFilterLocationPerformer.Text = tvFilterLocationPerformer.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindComplianceType()
        {
            try
            {
                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";

                ddlType.DataSource = CustomerManagement.GetComplainceType();
                ddlType.DataBind();

                ddlType.Items.Insert(0, new ListItem("Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindComplianceCategory()
        {
            try
            {
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                ddlCategory.DataSource = CustomerManagement.GetComplainceCategory();
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindActList()
        {
            try
            {
                ddlAct.Items.Clear();

                int complianceTypeID = Convert.ToInt32(ddlType.SelectedValue);
                int complianceCategoryID = Convert.ToInt32(ddlCategory.SelectedValue);

                List<SP_GetActsByUserID_Result> ActList = ActManagement.GetActsByUserID(AuthenticationHelper.UserID);


                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";

                ddlAct.DataSource = ActList;
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindAct()
        {
            try
            {
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";

                ddlAct.DataSource = CustomerManagement.GetAct();
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        private void BindCompliances(string role,string queryStringFlag)
        {
            try
            {
                int customerid = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);
                int location = Convert.ToInt32(tvFilterLocationPerformer.SelectedValue);
                int type = Convert.ToInt32(ddlType.SelectedValue);
                int category = Convert.ToInt32(ddlCategory.SelectedValue);
                int ComplianceType = Convert.ToInt32(ddlComplianceType.SelectedValue);
                int ActID = Convert.ToInt32(ddlAct.SelectedValue);
                CheckListStatus Status = (CheckListStatus)Convert.ToInt32(ddlStatus.SelectedIndex);

                string StringType = "";
                StringType = txtSearchType.Text;
                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;

                if (txtStartDate.Text == "")
                {
                    dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtfrom = DateTime.ParseExact(txtStartDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                if (txtEndDate.Text == "")
                {
                    dtTo = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtTo = DateTime.ParseExact(txtEndDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                if (role1 == "Performer")
                {
                    var Checklist = DashboardManagement.DashboardDataForPerformer_ChecklistNew(AuthenticationHelper.UserID,AuthenticationHelper.CustomerID, risk, location, type, category, ActID, dtfrom, dtTo, StringType, ComplianceType, queryStringFlag, Status);
                    Session["TotalRows"] = Checklist.Count;

                    grdComplianceTransactions.DataSource = Checklist;
                    hdnCheckPageNo.Value = Convert.ToString(Checklist.Count);
                    grdComplianceTransactions.DataBind();
                    GetPageDisplaySummary();

                    if (queryStringFlag == "A")
                    {
                        grdComplianceTransactions.Columns[3].Visible = true;
                        grdComplianceTransactions.Columns[5].Visible = true;
                        grdComplianceTransactions.Columns[6].Visible = true;
                    }
                    if (queryStringFlag == "E")
                    {
                        grdComplianceTransactions.Columns[3].Visible = false;
                        grdComplianceTransactions.Columns[5].Visible = true;
                        grdComplianceTransactions.Columns[6].Visible = true;
                    }
                    if (queryStringFlag == "S")
                    {
                        grdComplianceTransactions.Columns[3].Visible = true;
                        grdComplianceTransactions.Columns[5].Visible = false;
                        grdComplianceTransactions.Columns[6].Visible = false;
                    }
                }
                else
                {
                    //var Checklist = DashboardManagement.DashboardDataForReviewer_ChecklistNew(AuthenticationHelper.UserID, risk, location, type, category, ActID, dtfrom, dtTo, StringType, ComplianceType, queryStringFlag);
                    var Checklist = DashboardManagement.DashboardDataForReviewer_ChecklistNew(AuthenticationHelper.UserID, AuthenticationHelper.CustomerID, risk, location, type, category, ActID, dtfrom, dtTo, StringType, ComplianceType, queryStringFlag, Status);

                    Session["TotalRows"] = Checklist.Count;

                    grdComplianceTransactions.DataSource = Checklist;
                    hdnCheckPageNo.Value = Convert.ToString(Checklist.Count);
                    grdComplianceTransactions.DataBind();

                    if (role == "Performer")
                    {
                        //grdComplianceTransactions.Columns[8].Visible = true;
                        grdComplianceTransactions.Columns[9].Visible = true;
                        btnSave.Visible = true;
                    }
                    else
                    {
                        //grdComplianceTransactions.Columns[8].Visible = false;
                        grdComplianceTransactions.Columns[9].Visible = false;
                        btnSave.Visible = false;
                    }

                    if (queryStringFlag == "A")
                    {
                        grdComplianceTransactions.Columns[3].Visible = true;
                        grdComplianceTransactions.Columns[5].Visible = true;
                        grdComplianceTransactions.Columns[6].Visible = true;
                    }
                    if (queryStringFlag == "E")
                    {
                        grdComplianceTransactions.Columns[3].Visible = false;
                        grdComplianceTransactions.Columns[5].Visible = true;
                        grdComplianceTransactions.Columns[6].Visible = true;
                    }
                    if (queryStringFlag == "S")
                    {
                        grdComplianceTransactions.Columns[3].Visible = true;
                        grdComplianceTransactions.Columns[5].Visible = false;
                        grdComplianceTransactions.Columns[6].Visible = false;
                    }
                    GetPageDisplaySummary();

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected bool visiblebutton(int RoleID)
        {
            try
            {
                bool result = false;
                if (RoleID == 3)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;

        }


        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {

                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                BindCompliances(role1,queryStringFlag);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliances(role1, queryStringFlag);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilterPerformer", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPerformer');", tbxFilterLocationPerformer.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterPerformer", "$(\"#divFilterLocationPerformer\").hide(\"blind\", null, 500, function () { });", true);


                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", string.Format("initializeDatePicker11(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", "initializeDatePicker11(null);", true);
                }

                DateTime date1 = DateTime.MinValue;
                if (DateTime.TryParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker12", string.Format("initializeDatePicker12(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker12", "initializeDatePicker12(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void upComplianceScheduleDialog_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdComplianceTransactions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Download")
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(commandArg[0]), Convert.ToInt64(commandArg[1]));
                    Session["ScheduleOnID"] = commandArg[0];
                    Session["TransactionID"] = commandArg[1];

                    if (CMPDocuments != null)
                    {
                        List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            rptComplianceVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptComplianceVersion.DataBind();

                            rptComplianceDocumnets.DataSource = null;
                            rptComplianceDocumnets.DataBind();

                            rptWorkingFiles.DataSource = null;
                            rptWorkingFiles.DataBind();

                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDownloadDocument();", true);
                        }
                        else
                        {
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                                List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();

                                ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();
                                ComplianceFileData = ComplianceDocument;

                                var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
                                ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                                if (ComplianceFileData.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = string.Empty;
                                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        else
                                        {
                                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = file.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument_" + commandArgs[1] + ".zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                            }
                        }
                    }
                }
                else if (e.CommandName == "View")
                {

                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(commandArg[0]), Convert.ToInt64(commandArg[1]));
                    Session["ScheduleOnID"] = commandArg[0];
                    Session["TransactionID"] = commandArg[1];

                    if (CMPDocuments != null)
                    {
                        //List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                        List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptComplianceVersionView.DataBind();

                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessage.Text = "";
                                        lblMessage.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(Convert.ToInt32(AuthenticationHelper.CustomerID));

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();

                                        CompDocReviewPath = FileName;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                        lblMessage.Text = "";
                                    }
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);

                                }
                                break;
                            }

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            //System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            //sortImage.ImageAlign = ImageAlign.AbsMiddle;

            //if (direction == SortDirection.Ascending)
            //{
            //    sortImage.ImageUrl = "../Images/SortAsc.gif";
            //    sortImage.AlternateText = "Ascending Order";
            //}
            //else
            //{
            //    sortImage.ImageUrl = "../Images/SortDesc.gif";
            //    sortImage.AlternateText = "Descending Order";
            //}
            //headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {

                Button btn = (Button) (sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                int complianceInstanceID = Convert.ToInt32(commandArgs[1]);
                string lblScheduledOn = Convert.ToString(commandArgs[2]);

                udcStatusTranscatopnChkList.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                BindCompliances(role1, queryStringFlag);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                // cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (role1 == "Performer")
                    {
                        SP_GetCheckListCannedReportCompliancesSummary_Result rowView = (SP_GetCheckListCannedReportCompliancesSummary_Result)e.Row.DataItem;
                        Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledOn");
                        CheckBox chkCompleted = (CheckBox)e.Row.FindControl("chkCompleted");
                        CheckBox chkNotCompleted = (CheckBox)e.Row.FindControl("chkNotCompleted");
                        string CheckDate = "1/1/0001 12:00:00 AM";
                        
                        Button ChangeStatus = (Button)e.Row.FindControl("btnChangeStatus");
                        ImageButton DownLoadfile = (ImageButton)e.Row.FindControl("lblDownLoadfile");
                        ImageButton Viewfile = (ImageButton)e.Row.FindControl("lblViewfile");
                        ImageButton OverView = (ImageButton)e.Row.FindControl("lblOverView1");
                        DownLoadfile.Visible = false;
                        Viewfile.Visible = false;
                        OverView.Visible = false;
                        ChangeStatus.Visible = true;
                        if (Convert.ToDateTime(lblScheduledOn.Text) == Convert.ToDateTime(CheckDate))
                        {
                            lblScheduledOn.Visible = false;
                            chkCompleted.Visible = false;
                            chkNotCompleted.Visible = false;
                        }
                        else
                        {
                            lblScheduledOn.Visible = true;
                        }
                        if (rowView.ComplianceInstanceID != -1)
                        {
                            Label lblCheckListTypeID = (Label)e.Row.FindControl("lblCheckListTypeID");
                            Label lblForMonth = (Label)e.Row.FindControl("lblForMonth");
                            if ((lblForMonth.Text == null) || (lblForMonth.Text == ""))
                            {
                                if ((lblCheckListTypeID.Text == null) || (lblCheckListTypeID.Text == ""))
                                {
                                    if (lblCheckListTypeID.Text == "0")
                                    {
                                        lblForMonth.Text = "One Time";
                                    }
                                    else if (lblCheckListTypeID.Text == "2")
                                    {
                                        lblForMonth.Text = "Time Based";
                                    }
                                }
                            }
                        }
                    }
                    else if (role1 == "Reviewer")
                    {
                        Label lblUploadedFileName = (Label)e.Row.FindControl("lblUploadedFileName");
                        
                        Button ChangeStatus = (Button)e.Row.FindControl("btnChangeStatus");
                        ImageButton DownLoadfile = (ImageButton)e.Row.FindControl("lblDownLoadfile");
                        ImageButton Viewfile = (ImageButton)e.Row.FindControl("lblViewfile");
                        ImageButton OverView = (ImageButton)e.Row.FindControl("lblOverView1");
                        DownLoadfile.Visible = true;
                        Viewfile.Visible = true;
                        OverView.Visible = true;
                        ChangeStatus.Visible = false;

                        if ((lblUploadedFileName.Text == null) || (lblUploadedFileName.Text == ""))
                        {
                            DownLoadfile.Visible = false;
                            Viewfile.Visible = false;
                        }

                        if (!string.IsNullOrEmpty(UploadDocumentLink))
                        {
                            if (UploadDocumentLink.Equals("True"))
                            {
                                DownLoadfile.Visible = false;
                                Viewfile.Visible = false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected string ShowType(long? eventID)
        {
            try
            {
                if (eventID == null)
                {
                    return "Non-Event Based";
                }
                else
                {
                    return "Event Based";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        protected void chkCompletedSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ChkBoxHeader = (CheckBox) grdComplianceTransactions.HeaderRow.FindControl("chkCompletedSelectAll");
            CheckBox chkNotCompletedSelectAll = (CheckBox) grdComplianceTransactions.HeaderRow.FindControl("chkNotCompletedSelectAll");
            foreach (GridViewRow row in grdComplianceTransactions.Rows)
            {
                CheckBox chkCompleted = (CheckBox) row.FindControl("chkCompleted");
                CheckBox chkNotCompleted = (CheckBox) row.FindControl("chkNotCompleted");
                if (ChkBoxHeader.Checked == true)
                {
                    chkCompleted.Checked = true;
                    chkNotCompletedSelectAll.Enabled = false;
                    chkNotCompleted.Enabled = false;

                }
                else
                {
                    chkCompleted.Checked = false;
                    chkNotCompletedSelectAll.Enabled = true;
                    chkNotCompleted.Enabled = true;
                }
            }
        }

        protected void chkCompleted_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox ChkBoxHeader = (CheckBox) grdComplianceTransactions.HeaderRow.FindControl("chkCompletedSelectAll");
            CheckBox chkNotCompletedSelectAll = (CheckBox) grdComplianceTransactions.HeaderRow.FindControl("chkNotCompletedSelectAll");

            int countCheckedCheckbox = 0;
            for (int i = 0; i < grdComplianceTransactions.Rows.Count; i++)
            {
                GridViewRow row = grdComplianceTransactions.Rows[i];
                CheckBox chkNotCompleted = (CheckBox) row.FindControl("chkNotCompleted");
                if (((CheckBox) row.FindControl("chkCompleted")).Checked)
                {

                    countCheckedCheckbox = countCheckedCheckbox + 1;
                    chkNotCompleted.Enabled = false;
                }
                else
                {
                    chkNotCompleted.Enabled = true; ;
                }
            }
            if (countCheckedCheckbox == grdComplianceTransactions.Rows.Count - 1)
            {
                ChkBoxHeader.Checked = true;
                chkNotCompletedSelectAll.Enabled = false;
            }
            else
            {
                ChkBoxHeader.Checked = false;
                chkNotCompletedSelectAll.Enabled = true;
            }

        }

        protected void chkNotCompletedSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkNotCompletedSelectAll = (CheckBox) grdComplianceTransactions.HeaderRow.FindControl("chkNotCompletedSelectAll");
            CheckBox chkCompletedSelectAll = (CheckBox) grdComplianceTransactions.HeaderRow.FindControl("chkCompletedSelectAll");
            foreach (GridViewRow row in grdComplianceTransactions.Rows)
            {
                CheckBox chkNotCompleted = (CheckBox) row.FindControl("chkNotCompleted");
                CheckBox chkCompleted = (CheckBox) row.FindControl("chkCompleted");
                if (chkNotCompletedSelectAll.Checked == true)
                {
                    chkNotCompleted.Checked = true;
                    chkCompletedSelectAll.Enabled = false;
                    chkCompleted.Enabled = false;
                }
                else
                {
                    chkNotCompleted.Checked = false;
                    chkCompletedSelectAll.Enabled = true;
                    chkCompleted.Enabled = true;
                }
            }
        }

        protected void chkNotCompleted_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkNotCompletedSelectAll = (CheckBox) grdComplianceTransactions.HeaderRow.FindControl("chkNotCompletedSelectAll");
            CheckBox chkCompletedSelectAll = (CheckBox) grdComplianceTransactions.HeaderRow.FindControl("chkCompletedSelectAll");

            int countCheckedCheckboxNC = 0;
            for (int i = 0; i < grdComplianceTransactions.Rows.Count; i++)
            {
                GridViewRow row = grdComplianceTransactions.Rows[i];
                CheckBox chkCompleted = (CheckBox) row.FindControl("chkCompleted");
                if (((CheckBox) row.FindControl("chkNotCompleted")).Checked)
                {
                    countCheckedCheckboxNC = countCheckedCheckboxNC + 1;
                    chkCompleted.Enabled = false;
                }
                else
                {
                    chkCompleted.Enabled = true;
                }
            }
            if (countCheckedCheckboxNC == grdComplianceTransactions.Rows.Count - 1)
            {
                chkNotCompletedSelectAll.Checked = true;
                chkCompletedSelectAll.Enabled = false;

            }
            else
            {
                chkNotCompletedSelectAll.Checked = false;
                chkCompletedSelectAll.Enabled = true;
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                for (int i = 0; i < grdComplianceTransactions.Rows.Count; i++)
                {
                    string status = "1";
                    GridViewRow row = grdComplianceTransactions.Rows[i];

                    CheckBox chkCompleted = (CheckBox) row.FindControl("chkCompleted");
                    CheckBox chkNotCompleted = (CheckBox) row.FindControl("chkNotCompleted");
                    Label lblInstanceID = (Label) row.FindControl("lblInstanceID");
                    Label lblScheduleOnID = (Label) row.FindControl("lblScheduleOnID");
                    if (chkCompleted.Checked)
                    {
                        if (lblScheduleOnID.Text != "")
                        {
                            //status = "4";//closed timely
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                ComplianceTransaction transaction = new ComplianceTransaction()
                                {
                                    ComplianceInstanceId = Convert.ToInt32(lblInstanceID.Text),
                                    ComplianceScheduleOnID = Convert.ToInt32(lblScheduleOnID.Text),
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    StatusId = 4,//closed timely
                                    Remarks = "Closed Timely"
                                };
                                transaction.Dated = DateTime.Now;
                                transaction.StatusChangedOn = DateTime.Now;
                                entities.ComplianceTransactions.Add(transaction);
                                entities.SaveChanges();
                            }
                        }
                    }
                    if (chkNotCompleted.Checked)
                    {
                        if (lblScheduleOnID.Text != "")
                        {
                            //status = "6";//not Complied
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                ComplianceTransaction transaction = new ComplianceTransaction()
                                {
                                    ComplianceInstanceId = Convert.ToInt32(lblInstanceID.Text),
                                    ComplianceScheduleOnID = Convert.ToInt32(lblScheduleOnID.Text),
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    StatusId = 6,//not Complied
                                    Remarks = "Not Complied"
                                };
                                transaction.Dated = DateTime.Now;
                                transaction.StatusChangedOn = DateTime.Now;
                                entities.ComplianceTransactions.Add(transaction);
                                entities.SaveChanges();
                            }
                        }
                    }
                }
                BindCompliances(role1, queryStringFlag);


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //This method is used to save the checkedstate of values
        private void SaveCheckedValues()
        {
            ArrayList userdetails = new ArrayList();
            long index = -1;

            foreach (GridViewRow gvrow in grdComplianceTransactions.Rows)
            {
                Label lblScheduledOn = (Label) gvrow.FindControl("lblScheduledOn");
                string CheckDate = "1/1/0001 12:00:00 AM";
                if (Convert.ToDateTime(lblScheduledOn.Text) == Convert.ToDateTime(CheckDate))
                {
                }
                else
                {
                    long aaaa = (long) grdComplianceTransactions.DataKeys[gvrow.RowIndex].Value;
                    index = (long) grdComplianceTransactions.DataKeys[gvrow.RowIndex].Value;
                    bool result = ((CheckBox) gvrow.FindControl("chkCompleted")).Checked;

                    // Check in the Session
                    if (Session["CHECKED_ITEMS"] != null)
                        userdetails = (ArrayList) Session["CHECKED_ITEMS"];
                    if (result)
                    {
                        if (!userdetails.Contains(index))
                            userdetails.Add(index);
                    }
                    else
                        userdetails.Remove(index);
                }
            }
            if (userdetails != null && userdetails.Count > 0)
                Session["CHECKED_ITEMS"] = userdetails;
        }

        private void PopulateCheckedValues()
        {
            ArrayList userdetails = (ArrayList) Session["CHECKED_ITEMS"];
            if (userdetails != null && userdetails.Count > 0)
            {
                foreach (GridViewRow gvrow in grdComplianceTransactions.Rows)
                {
                    Label lblScheduledOn = (Label) gvrow.FindControl("lblScheduledOn");
                    string CheckDate = "1/1/0001 12:00:00 AM";
                    if (Convert.ToDateTime(lblScheduledOn.Text) == Convert.ToDateTime(CheckDate))
                    {
                    }
                    else
                    {
                        long index = (long) grdComplianceTransactions.DataKeys[gvrow.RowIndex].Value;
                        if (userdetails.Contains(index))
                        {
                            CheckBox myCheckBox = (CheckBox) gvrow.FindControl("chkCompleted");
                            myCheckBox.Checked = true;
                        }
                    }
                }
            }
        }



        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkbtn = sender as LinkButton;
                GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
                Label lblFilePath = (Label) gvrow.FindControl("lblFilePath");
                Label lblFileName = (Label) gvrow.FindControl("lblFileName");

                Response.ContentType = "application/octet-stream";
                string filePath = lblFilePath.Text;

                Response.AddHeader("Content-Disposition", "attachment;filename=\"" + filePath + "\"");
                Response.TransmitFile(Server.MapPath(filePath));
                Response.End();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";

                grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                lblAdvanceSearchScrum.Text = String.Empty;

                BindCompliances(role1, queryStringFlag);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        static public void EnumToListBox(Type EnumType, ListControl TheListBox)
        {
            Array Values = System.Enum.GetValues(EnumType);

            foreach (long Value in Values)
            {
                string Display = Enum.GetName(EnumType, Value);
                ListItem Item = new ListItem(Display, Value.ToString());
                TheListBox.Items.Add(Item);
            }
        }

        protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ddlType.SelectedValue = "-1";
                ddlCategory.SelectedValue = "-1";
                ddlAct.SelectedValue = "-1";
                txtStartDate.Text = string.Empty;
                txtEndDate.Text = string.Empty;
                divAdvSearch.Visible = false;
                txtSearchType.Text = string.Empty;
                BindCompliances(role1, queryStringFlag);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlpageSize_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                grdComplianceTransactions.PageSize = int.Parse(((DropDownList) sender).SelectedValue);

                //if (!IsValid()) { return; };
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //Reload the Grid
                BindCompliances(role1, queryStringFlag);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                BindCompliances(role1, queryStringFlag);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlpageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                BindCompliances(role1, queryStringFlag);
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private bool IsValid()
        {
            try
            {
                if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                {
                    SelectedPageNo.Text = "1";
                    return false;
                }
                else if (!IsNumeric(SelectedPageNo.Text))
                {
                    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlpageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlpageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {


                TotalRows.Value = hdnCheckPageNo.Value;


                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlpageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlpageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        private void BindStatus()
        {
            try
            {
                foreach (CheckListStatus r in Enum.GetValues(typeof(CheckListStatus)))
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(CheckListStatus), r), r.ToString());
                    ddlStatus.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);
                CheckListStatus status = (CheckListStatus) Convert.ToInt16(ddlStatus.SelectedIndex);
                int location = Convert.ToInt32(tvFilterLocationPerformer.SelectedValue);
                int ComplianceType = Convert.ToInt32(ddlComplianceType.SelectedValue);
                if(ComplianceType == -1)
                {
                    queryStringFlag = "S";
                }
                else
                {
                    queryStringFlag = "E";
                }

                BindCompliances(role1, queryStringFlag);
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void btnAdvSearch_Click(object sender, EventArgs e)
        {
            try
            {
                lblAdvanceSearchScrum.Text = String.Empty;

                int type = 0;
                int Category = 0;
                int Act = 0;

                if (txtStartDate.Text != "" && txtEndDate.Text == "" || txtStartDate.Text == "" && txtEndDate.Text != "")
                {
                    if (txtStartDate.Text == "")
                        txtStartDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                    else if (txtEndDate.Text == "")
                        txtEndDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");

                    return;
                }

                if (ddlType.SelectedValue != "-1")
                {
                    type = ddlType.SelectedItem.Text.Length;

                    if (type >= 20)
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text.Substring(0, 20) + "...";
                    else
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text;
                }

                if (ddlCategory.SelectedValue != "-1")
                {
                    Category = ddlCategory.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                    else
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                }

                if (ddlAct.SelectedValue != "-1")
                {
                    Act = ddlAct.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                    else
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                }

                if (txtStartDate.Text != "" && txtEndDate.Text != "")
                {
                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        lblAdvanceSearchScrum.Text += "     " + "<b>Start Date: </b>" + txtStartDate.Text;
                        lblAdvanceSearchScrum.Text += "     " + "<b>End Date: </b>" + txtEndDate.Text;
                    }
                    else
                    {
                        lblAdvanceSearchScrum.Text += "<b>Start Date: </b>" + txtStartDate.Text;
                        lblAdvanceSearchScrum.Text += "<b> End Date: </b>" + txtEndDate.Text;
                    }
                }

                if (txtSearchType.Text != "")
                {
                    lblAdvanceSearchScrum.Text += "<b>Type To Filter:  </b>" + txtSearchType.Text;
                }

                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                    BindCompliances(role1, queryStringFlag);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnCheck_List_Click(object sender, EventArgs e)
        {
                Response.Redirect("../Compliances/Check_List_Reports_Performer.aspx?role=" + role1);
        }

        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }
        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = string.Empty;
                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }

                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            DownloadFile(Convert.ToInt32(e.CommandArgument));
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }

        protected void rptComplianceVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();
                ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();

                if (commandArgs[1].Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                }

                if (e.CommandName.Equals("version"))
                {
                    if (e.CommandName.Equals("version"))
                    {
                        rptComplianceDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                        rptComplianceDocumnets.DataBind();

                        rptWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                        rptWorkingFiles.DataBind();
                        List<GetComplianceDocumentsView> CMPDocuments = Business.ComplianceManagement.GetDocumnets(Convert.ToInt64(Session["ScheduleOnID"]), Convert.ToInt64(Session["TransactionID"]));
                        if (CMPDocuments != null)
                        {
                            List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).GroupBy(entry => entry.Version).Select(en => en.FirstOrDefault()).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                                entityData.Version = "1.0";
                                entityData.ScheduledOnID = Convert.ToInt64(Session["ScheduleOnID"]);
                                entitiesData.Add(entityData);
                            }
                            rptComplianceVersion.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptComplianceVersion.DataBind();
                        }
                    }
                }
                else if (e.CommandName.Equals("Download"))
                {
                    using (ZipFile ComplianceZip = new ZipFile())
                    {
                            var ComplianceData = DocumentManagement.GetForMonthChecklist(Convert.ToInt32(commandArgs[0]));
                            ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                        if (ComplianceFileData.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in ComplianceFileData)
                            {
                                string filePath = string.Empty;
                                //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {
                                    filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                }
                                else
                                {
                                    filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                }
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string[] filename = file.FileName.Split('.');
                                    string str = filename[0] + i + "." + filename[1];
                                    if (file.EnType == "M")
                                    {
                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    i++;
                                }
                            }
                        }
                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] data = zipMs.ToArray();

                        Response.Buffer = true;

                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument_" + commandArgs[1] + ".zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }
                else if (e.CommandName.Equals("View"))
                {
                    if (ComplianceFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in ComplianceFileData)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    lblMessage.Text = "";
                                    lblMessage.Text = "Zip file can't view please download it";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                }
                                else
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string customerID = Convert.ToString(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    i++;

                                    CompDocReviewPath = FileName;

                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);

                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplinceVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
            }
        }
        protected void rptComplianceVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                // LinkButton btnComplinceVersionDocView = (LinkButton)e.Item.FindControl("btnComplinceVersionDocView");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                //scriptManager.RegisterPostBackControl(btnComplinceVersionDocView);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                    Session["ScheduleOnID"] = commandArg[0];

                    if (CMPDocuments != null)
                    {
                        List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;
                                    string extension = System.IO.Path.GetExtension(filePath);

                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessage.Text = "";
                                        lblMessage.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();
                                        CompDocReviewPath = FileName;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                        lblMessage.Text = "";

                                    }
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

  

    }
}