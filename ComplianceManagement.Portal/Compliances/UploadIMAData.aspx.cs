﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class UploadIMAData : System.Web.UI.Page
    {
        bool sucess = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblMessage.Text = string.Empty;
                LblErormessage.Text = string.Empty;                
            }
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
           
        }
      
        #region Add Process      
        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {

                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            if (rdoAssignCompliance.Checked)
                            {
                                bool flag = IMAUploadSheetsExitsts(xlWorkbook, "IMAUpload");
                                if (flag == true)
                                {            
                                    if (HttpContext.Current.Cache.Get("IMAComplianceListData") != null)
                                    {
                                        HttpContext.Current.Cache.Remove("IMAComplianceListData");
                                    }
                                    if (HttpContext.Current.Cache.Get("IMAActListData") != null)
                                    {
                                        HttpContext.Current.Cache.Remove("IMAActListData");
                                    }
                                    if (HttpContext.Current.Cache.Get("IMALocationTypeListData") != null)
                                    {
                                        HttpContext.Current.Cache.Remove("IMALocationTypeListData");
                                    }
                                    if (HttpContext.Current.Cache.Get("IMABusinessActivityTypeListData") != null)
                                    {
                                        HttpContext.Current.Cache.Remove("IMABusinessActivityTypeListData");
                                    }
                                    if (HttpContext.Current.Cache.Get("IMAAct_ApplicabilityListData") != null)
                                    {
                                        HttpContext.Current.Cache.Remove("IMAAct_ApplicabilityListData");
                                    }

                                    if (HttpContext.Current.Cache.Get("IMACompanyTypeListData") != null)
                                    {
                                        HttpContext.Current.Cache.Remove("IMACompanyTypeListData");
                                    }
                                    if (HttpContext.Current.Cache.Get("IMAIndustryListData") != null)
                                    {
                                        HttpContext.Current.Cache.Remove("IMAIndustryListData");
                                    }
                                    if (HttpContext.Current.Cache.Get("IMALicenseTypeListData") != null)
                                    {
                                        HttpContext.Current.Cache.Remove("IMALicenseTypeListData");
                                    }
                                    if (HttpContext.Current.Cache.Get("IMACategorizationListData") != null)
                                    {
                                        HttpContext.Current.Cache.Remove("IMACategorizationListData");
                                    }
                                    if (HttpContext.Current.Cache.Get("IMASubCategorizationListData") != null)
                                    {
                                        HttpContext.Current.Cache.Remove("IMASubCategorizationListData");
                                    }                                    
                                  

                                    GetCompliance();
                                    GetACT();
                                    GetIndustry();
                                    GetCompanyType();
                                    GetBusinessActivityType();
                                    GetAct_Applicability();
                                    GetLocationType();
                                    GetLicenseTypeMaster();
                                    GetCategorization();
                                    GetSubCategorization();
                                    ComplianceAssignmentData(xlWorkbook);

                                    if (sucess  == true)
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Data uploaded successfully.";

                                        if (HttpContext.Current.Cache.Get("IMAComplianceListData") != null)
                                        {
                                            HttpContext.Current.Cache.Remove("IMAComplianceListData");
                                        }
                                        if (HttpContext.Current.Cache.Get("IMAActListData") != null)
                                        {
                                            HttpContext.Current.Cache.Remove("IMAActListData");
                                        }
                                        if (HttpContext.Current.Cache.Get("IMALocationTypeListData") != null)
                                        {
                                            HttpContext.Current.Cache.Remove("IMALocationTypeListData");
                                        }
                                        if (HttpContext.Current.Cache.Get("IMABusinessActivityTypeListData") != null)
                                        {
                                            HttpContext.Current.Cache.Remove("IMABusinessActivityTypeListData");
                                        }
                                        if (HttpContext.Current.Cache.Get("IMAAct_ApplicabilityListData") != null)
                                        {
                                            HttpContext.Current.Cache.Remove("IMAAct_ApplicabilityListData");
                                        }
                                        if (HttpContext.Current.Cache.Get("IMACompanyTypeListData") != null)
                                        {
                                            HttpContext.Current.Cache.Remove("IMACompanyTypeListData");
                                        }
                                        if (HttpContext.Current.Cache.Get("IMAIndustryListData") != null)
                                        {
                                            HttpContext.Current.Cache.Remove("IMAIndustryListData");
                                        }
                                        if (HttpContext.Current.Cache.Get("IMALicenseTypeListData") != null)
                                        {
                                            HttpContext.Current.Cache.Remove("IMALicenseTypeListData");
                                        }
                                        if (HttpContext.Current.Cache.Get("IMACategorizationListData") != null)
                                        {
                                            HttpContext.Current.Cache.Remove("IMACategorizationListData");
                                        }
                                        if (HttpContext.Current.Cache.Get("IMASubCategorizationListData") != null)
                                        {
                                            HttpContext.Current.Cache.Remove("IMASubCategorizationListData");
                                        }
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please check the sheet name, Sheet name must be 'IMAUpload'";
                                }
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
        private bool IMAUploadSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("IMAUpload"))
                    {
                        if (sheet.Name.Trim().Equals("IMAUpload"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
      
       
        public partial class TempTable
        {
            public long ComplianceId { get; set; }
            public int CustomerBranchID { get; set; }
            public long Performerid { get; set; }
            public string SequenceID { get; set; }
        }
        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }

        //public static void CreateRiskCategoryMapping(Act_CompanyTypeMapping RiskCategoryMapping)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        entities.Act_CompanyTypeMapping.Add(RiskCategoryMapping);
        //        entities.SaveChanges();
        //    }
        //}

        public long QuestionExists(int controlled,string questionname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.QuestionMasters
                             where row.IsActive == false
                             && row.ControlId == controlled
                             && row.QuestionName.ToUpper().Trim().Equals(questionname.ToUpper().Trim())
                             select row.ID).FirstOrDefault();
                return query;
            }
        }
        private void ComplianceAssignmentData(ExcelPackage xlWorkbook)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                    ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["IMAUpload"];
                    if (xlWorksheet != null)
                    {
                        int count = 1;
                        int xlrow2 = xlWorksheet.Dimension.End.Row;
                        List<Questions_ActMapping> QuestionsActMappingList = new List<Questions_ActMapping>();
                        List<Questions_ActComplianceMapping> QuestionsActComplianceMappingList = new List<Questions_ActComplianceMapping>();
                        List<Act_CompanyTypeMapping> ActCompanyTypeMappingList = new List<Act_CompanyTypeMapping>();
                        List<Act_BusinessActivityMapping> ActBusinessActivityMappingList = new List<Act_BusinessActivityMapping>();
                        List<Act_ActApplicabilityMapping> ActApplicabilityMappingList = new List<Act_ActApplicabilityMapping>();
                        List<Act_IndustryMapping> ActIndustryMappingList = new List<Act_IndustryMapping>();
                        List<Compliance_ActApplicabilityMapping> ComplianceActApplicabilityMappingList = new List<Compliance_ActApplicabilityMapping>();

                        List<Act_LoctionTypeMapping> ActLoctionTypeMappingMappingList = new List<Act_LoctionTypeMapping>();
                        
                        List<string> errorMessage = new List<string>();
                        List<TempTable> lstTemptable = new List<TempTable>();

                        #region Validations
                        int valACTID = -1;
                        int valComplianceID = -1;
                        int valCompanyTypeID = -1;
                        int valBusinessActivityTypeID = -1;
                        int valLocationTypeID = -1;
                        int valIndustryTypeID = -1;
                        int valActAppliesID = -1;
                        int valACTQuestionTypeID = -1;
                        int valComplianceQuestionTypeID = -1;
                        int valComplianceAppliesID = -1;
                        int valLicenseRegistrationTypeID = -1;
                        int valActivityCategorizationID = -1;
                        int valActivitySubCategorizationID = -1;

                        string valCompanyType = string.Empty;
                        string valBusinessActivityType = string.Empty;
                        string valLocationType = string.Empty;
                        string valIndustryType = string.Empty;
                        string valActApplies = string.Empty;
                        string valACTQuestionType = string.Empty;
                        string valActQuestion = string.Empty;
                        string valACTSingn = string.Empty;
                        string valACTAnswer = string.Empty;
                        string valActRangeFrom = string.Empty;
                        string valActRangeTo = string.Empty;
                        string valComplianceQuestionType = string.Empty;
                        string valComplianceQuestionSingn = string.Empty;
                        string valComplianceAnswer = string.Empty;
                        string valComplianceRangeFrom = string.Empty;
                        string valComplianceRangeTo = string.Empty;
                        string valComplianceApplies = string.Empty;
                        string valLinkedtoaLicenseRegistration = string.Empty;
                        string valLicenseRegistrationType = string.Empty;
                        string valActivityCategorization = string.Empty;
                        string valActivitySubCategorization = string.Empty;


                        for (int rowNum = 3; rowNum <= xlrow2; rowNum++)
                        {
                            valComplianceID = -1;
                            valACTID = -1;
                            valCompanyTypeID = -1;
                            valLocationTypeID = -1;
                            valIndustryTypeID = -1;
                            valActAppliesID = -1;
                            valACTQuestionTypeID = -1;
                            valComplianceQuestionTypeID = -1;
                            valComplianceAppliesID = -1;
                            valLicenseRegistrationTypeID = -1;
                            valActivityCategorizationID = -1;
                            valActivitySubCategorizationID = -1;
                            valCompanyType = string.Empty;
                            valBusinessActivityType = string.Empty;
                            valLocationType = string.Empty;
                            valIndustryType = string.Empty;
                            valActApplies = string.Empty;

                            valACTQuestionType = string.Empty;
                            valActQuestion = string.Empty;
                            valACTSingn = string.Empty;
                            valACTAnswer = string.Empty;
                            valActRangeFrom = string.Empty;
                            valActRangeTo = string.Empty;
                            valComplianceQuestionType = string.Empty;

                            valComplianceQuestionSingn = string.Empty;
                            valComplianceAnswer = string.Empty;
                            valComplianceRangeFrom = string.Empty;
                            valComplianceRangeTo = string.Empty;
                            valComplianceApplies = string.Empty;
                            valLinkedtoaLicenseRegistration = string.Empty;
                            valLicenseRegistrationType = string.Empty;
                            valActivityCategorization = string.Empty;
                            valActivitySubCategorization = string.Empty;

                            #region 1 ACTID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString().Trim()))
                            {
                                valACTID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 1].Text.Trim());
                            }
                            if (valACTID == -1 || valACTID == 0)
                            {
                                errorMessage.Add("Required ACTID at row number-" + rowNum);
                            }
                            else
                            {
                                if (ActExists(valACTID) == false)
                                {
                                    errorMessage.Add("ACTID not defined in the System at row number-" + rowNum);
                                }
                            }
                            #endregion

                            #region 2 ComplianceID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString().Trim()))
                            {
                                valComplianceID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 2].Text.Trim());
                            }
                            if (valComplianceID == -1 || valComplianceID == 0)
                            {
                                errorMessage.Add("Required ComplianceID at row number-" + rowNum);
                            }
                            else
                            {
                                if (ComplainceExists(valComplianceID, valACTID) == false)
                                {
                                    errorMessage.Add("ComplianceID not defined in the System or It is Event Based or Informative Compliance at row number-" + rowNum);
                                }
                            }
                            #endregion

                            #region 3 CompanyType 
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 3].Text.ToString().Trim()))
                            {
                                valCompanyType = Convert.ToString(xlWorksheet.Cells[rowNum, 3].Text).Trim();
                            }
                            if (String.IsNullOrEmpty(valCompanyType))
                            {
                                errorMessage.Add("Please Correct the CompanyType  or CompanyType Not Empty at row number -" + rowNum);
                            }
                            if (valCompanyType.ToLower().Trim() != "all")
                            {
                                string CT = xlWorksheet.Cells[rowNum, 3].Text.ToString().Trim();
                                string[] splitcttypes = CT.Split(',');
                                if (splitcttypes.Length > 0)
                                {
                                    for (int rs = 0; rs < splitcttypes.Length; rs++)
                                    {
                                        valCompanyTypeID = GetCompanyTypeByName(Regex.Replace(splitcttypes[rs].ToString().Trim(), @"\t|\n|\r", ""));
                                        if (valCompanyTypeID == 0 || valCompanyTypeID == -1)
                                        {
                                            errorMessage.Add("Please Correct the CompanyType  or CompanyType not Defined in the System at row number -" + rowNum);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 3].Text.ToString().Trim()))
                                    {
                                        valCompanyTypeID = GetCompanyTypeByName(Regex.Replace(xlWorksheet.Cells[rowNum, 3].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                    }
                                    if (valCompanyTypeID == 0 || valCompanyTypeID == -1)
                                    {
                                        errorMessage.Add("Please Correct the CompanyType  or CompanyType not Defined in the System at row number -" + rowNum);
                                    }
                                }
                            }

                            #endregion

                            #region 4 BusinessActivityType 
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 4].Text.ToString().Trim()))
                            {
                                valBusinessActivityType = Convert.ToString(xlWorksheet.Cells[rowNum, 4].Text).Trim();
                            }
                            if (String.IsNullOrEmpty(valBusinessActivityType))
                            {
                                errorMessage.Add("Please Correct the BusinessActivityType  or BusinessActivityType Not Empty at row number -" + rowNum);
                            }
                            if (valBusinessActivityType.ToLower().Trim() != "all")
                            {
                                string BAT = xlWorksheet.Cells[rowNum, 4].Text.ToString().Trim();
                                string[] splitbattypes = BAT.Split(',');
                                if (splitbattypes.Length > 0)
                                {
                                    for (int rs = 0; rs < splitbattypes.Length; rs++)
                                    {
                                        valBusinessActivityTypeID = GetBusinessActivityTypeByName(Regex.Replace(splitbattypes[rs].ToString().Trim(), @"\t|\n|\r", ""));
                                        if (valBusinessActivityTypeID == 0 || valBusinessActivityTypeID == -1)
                                        {
                                            errorMessage.Add("Please Correct the BusinessActivityType  or BusinessActivityType not Defined in the System at row number - " + rowNum);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 4].Text.ToString().Trim()))
                                    {
                                        valBusinessActivityTypeID = GetBusinessActivityTypeByName(Regex.Replace(xlWorksheet.Cells[rowNum, 4].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                    }
                                    if (valBusinessActivityTypeID == 0 || valBusinessActivityTypeID == -1)
                                    {
                                        errorMessage.Add("Please Correct the BusinessActivityType  or BusinessActivityType not Defined in the System at row number - " + rowNum);
                                    }
                                }
                            }
                            #endregion

                            #region 5 LocationType 
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim()))
                            {
                                valLocationType = Convert.ToString(xlWorksheet.Cells[rowNum, 5].Text).Trim();
                            }
                            if (String.IsNullOrEmpty(valLocationType))
                            {
                                errorMessage.Add("Please Correct the LocationType  or LocationType Not Empty at row number -" + rowNum);
                            }
                            if (valLocationType.ToLower().Trim() != "all")
                            {
                                string locationtype = xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim();
                                string[] splitloc = locationtype.Split(',');
                                if (splitloc.Length > 0)
                                {
                                    for (int rs = 0; rs < splitloc.Length; rs++)
                                    {
                                        valLocationTypeID = GetLocationTypeByName(Regex.Replace(splitloc[rs].ToString().Trim(), @"\t|\n|\r", ""));
                                        if (valLocationTypeID == 0 || valLocationTypeID == -1)
                                        {
                                            errorMessage.Add("Please Correct the LocationType  or LocationType not Defined in the System at row number - " + rowNum);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim()))
                                    {
                                        valLocationTypeID = GetLocationTypeByName(Regex.Replace(xlWorksheet.Cells[rowNum, 5].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                    }
                                    if (valLocationTypeID == 0 || valLocationTypeID == -1)
                                    {
                                        errorMessage.Add("Please Correct the LocationType  or LocationType not Defined in the System at row number - " + rowNum);
                                    }
                                }
                            }
                            #endregion

                            #region 6 Industry Type  
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 6].Text.ToString().Trim()))
                            {
                                valIndustryType = Convert.ToString(xlWorksheet.Cells[rowNum, 6].Text).Trim();
                            }
                            if (String.IsNullOrEmpty(valIndustryType))
                            {
                                errorMessage.Add("Please Correct the Industry Type  or Industry Type Not Empty at row number -" + rowNum);
                            }
                            if (valIndustryType.ToLower().Trim() != "all")
                            {
                                string industry = xlWorksheet.Cells[rowNum, 6].Text.ToString().Trim();
                                string[] splitind = industry.Split(',');
                                if (splitind.Length > 0)
                                {
                                    for (int rs = 0; rs < splitind.Length; rs++)
                                    {
                                        valIndustryTypeID = GetIndustryByName(Regex.Replace(splitind[rs].ToString().Trim(), @"\t|\n|\r", ""));
                                        if (valIndustryTypeID == 0 || valIndustryTypeID == -1)
                                        {
                                            errorMessage.Add("Please Correct the Industry Type   or Industry Type  not Defined in the System at row number - " + rowNum);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 6].Text.ToString().Trim()))
                                    {
                                        valIndustryTypeID = GetIndustryByName(Regex.Replace(xlWorksheet.Cells[rowNum, 6].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                    }
                                    if (valIndustryTypeID == 0 || valIndustryTypeID == -1)
                                    {
                                        errorMessage.Add("Please Correct the Industry Type   or Industry Type  not Defined in the System at row number - " + rowNum);
                                    }
                                }
                            }
                            #endregion

                            #region 7 Act Applies
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 7].Text.ToString().Trim()))
                            {
                                valActApplies = Convert.ToString(xlWorksheet.Cells[rowNum, 7].Text).Trim();
                            }
                            if (String.IsNullOrEmpty(valActApplies))
                            {
                                errorMessage.Add("Please Correct the Act Applies  or Act Applies Not Empty at row number -" + rowNum);
                            }
                            if (valActApplies.ToLower().Trim() != "all")
                            {
                                string AA = xlWorksheet.Cells[rowNum, 7].Text.ToString().Trim();
                                string[] splitactppli = AA.Split(',');
                                if (splitactppli.Length > 0)
                                {
                                    for (int rs = 0; rs < splitactppli.Length; rs++)
                                    {
                                        valActAppliesID = GetAct_ApplicabilityByName(Regex.Replace(splitactppli[rs].ToString().Trim(), @"\t|\n|\r", ""));
                                        if (valActAppliesID == 0 || valActAppliesID == -1)
                                        {
                                            errorMessage.Add("Please Correct the Act Applies   or Act Applies  not Defined in the System at row number - " + rowNum);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 7].Text.ToString().Trim()))
                                    {
                                        valActAppliesID = GetAct_ApplicabilityByName(Regex.Replace(xlWorksheet.Cells[rowNum, 7].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                    }
                                    if (valActAppliesID == 0 || valActAppliesID == -1)
                                    {
                                        errorMessage.Add("Please Correct the Act Applies  or Act Applies  not Defined in the System at row number - " + rowNum);
                                    }
                                }
                            }
                            #endregion

                            #region 8 ACT Question Type
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 8].Text.ToString().Trim()))
                            {
                                valACTQuestionType = Convert.ToString(xlWorksheet.Cells[rowNum, 8].Text).Trim();
                            }
                            if (valACTQuestionType.ToUpper().Trim() != "NA")
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 8].Text.ToString().Trim()))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 8].Text.ToString().Trim()))
                                    {
                                        valACTQuestionTypeID = GetQuestionAnswerControlByName(Regex.Replace(xlWorksheet.Cells[rowNum, 8].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                    }
                                    if (valACTQuestionTypeID == 0 || valACTQuestionTypeID == -1)
                                    {
                                        errorMessage.Add("Please Correct the ACT Question Type or ACT Question Type not Defined in the System at row number - " + rowNum);
                                    }
                                }
                            }
                            #endregion

                            #region 10 Act sign
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 10].Text.ToString().Trim()))
                            {
                                valACTSingn = Convert.ToString(xlWorksheet.Cells[rowNum, 10].Text).Trim();
                                if (valACTSingn.ToUpper().Trim() != "NA")
                                {
                                    if (valACTSingn != "<" && valACTSingn != ">" && valACTSingn != "=" && valACTSingn != "≥" && valACTSingn != "≤")
                                    {
                                        errorMessage.Add("Please Correct the Act sign or Act sign not Defined in the System at row number - " + rowNum);
                                    }
                                }
                            }
                            #endregion

                            #region 12 Act Range From
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 12].Text.ToString().Trim()))
                            {
                               
                                valActRangeFrom = Convert.ToString(xlWorksheet.Cells[rowNum, 12].Text).Trim();
                                if (valActRangeFrom.ToUpper().Trim() != "NA")
                                {
                                    if (!CheckInt(valActRangeFrom))
                                    {
                                        errorMessage.Add("Please Correct the Act Range From except only numeric value at row number - " + rowNum);
                                    }
                                }
                            }
                            #endregion

                            #region 13 Act Range To
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 13].Text.ToString().Trim()))
                            {
                                valActRangeTo = Convert.ToString(xlWorksheet.Cells[rowNum, 13].Text).Trim();
                                if (valActRangeTo.ToUpper().Trim() != "NA")
                                {
                                    if (!CheckInt(valActRangeTo))
                                    {
                                        errorMessage.Add("Please Correct the Act Range TO except only numeric value at row number - " + rowNum);
                                    }
                                }
                            }
                            #endregion

                            #region 14 Compliance Question Type
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 14].Text.ToString().Trim()))
                            {
                                valComplianceQuestionType = Convert.ToString(xlWorksheet.Cells[rowNum, 14].Text).Trim();
                            }
                            if (valComplianceQuestionType.ToUpper().Trim() != "NA")
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 14].Text.ToString().Trim()))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 14].Text.ToString().Trim()))
                                    {
                                        valComplianceQuestionTypeID = GetQuestionAnswerControlByName(Regex.Replace(xlWorksheet.Cells[rowNum, 14].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                    }
                                    if (valComplianceQuestionTypeID == 0 || valComplianceQuestionTypeID == -1)
                                    {
                                        errorMessage.Add("Please Correct the Compliance Question Type or  Compliance Question Type not Defined in the System at row number - " + rowNum);
                                    }
                                }
                            }
                            #endregion

                            #region 16 Compliance sign
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 16].Text.ToString().Trim()))
                            {
                                valComplianceQuestionSingn = Convert.ToString(xlWorksheet.Cells[rowNum, 16].Text).Trim();
                                if (valComplianceQuestionSingn.ToUpper().Trim() != "NA")
                                {
                                    if (valComplianceQuestionSingn != "<" && valComplianceQuestionSingn != ">" && valComplianceQuestionSingn != "=" && valComplianceQuestionSingn != "≥" && valComplianceQuestionSingn != "≤")
                                    {
                                        errorMessage.Add("Please Correct the Compliance sign or Compliance sign not Defined in the System at row number - " + rowNum);
                                    }
                                }
                            }
                            #endregion

                            #region 18 Compliance Range From
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 18].Text.ToString().Trim()))
                            {
                                valComplianceRangeFrom = Convert.ToString(xlWorksheet.Cells[rowNum, 18].Text).Trim();
                                if (valComplianceRangeFrom.ToUpper().Trim() != "NA")
                                {
                                    if (!CheckInt(valComplianceRangeFrom))
                                    {
                                        errorMessage.Add("Please Correct the Compliance Range From except only numeric value at row number - " + rowNum);
                                    }
                                }
                            }
                            #endregion

                            #region 19 Compliance Range To
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 19].Text.ToString().Trim()))
                            {
                                valComplianceRangeTo = Convert.ToString(xlWorksheet.Cells[rowNum, 19].Text).Trim();
                                if (valComplianceRangeTo.ToUpper().Trim() != "NA")
                                {
                                    if (!CheckInt(valComplianceRangeTo))
                                    {
                                        errorMessage.Add("Please Correct the Compliance Range TO except only numeric value at row number - " + rowNum);
                                    }
                                }
                            }
                            #endregion

                            #region 20 Compliance Applies
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 20].Text.ToString().Trim()))
                            {
                                valComplianceApplies = Convert.ToString(xlWorksheet.Cells[rowNum, 20].Text).Trim();
                            }
                            if (String.IsNullOrEmpty(valComplianceApplies))
                            {
                                errorMessage.Add("Please Correct the Compliance Applies  or Compliance Applies Not Empty at row number -" + rowNum);
                            }
                            if (valComplianceApplies.ToLower().Trim() != "all")
                            {
                                string AA = xlWorksheet.Cells[rowNum, 20].Text.ToString().Trim();
                                string[] splitactppli = AA.Split(',');
                                if (splitactppli.Length > 0)
                                {
                                    for (int rs = 0; rs < splitactppli.Length; rs++)
                                    {
                                        valComplianceAppliesID = GetAct_ApplicabilityByName(Regex.Replace(splitactppli[rs].ToString().Trim(), @"\t|\n|\r", ""));
                                        if (valComplianceAppliesID == 0 || valComplianceAppliesID == -1)
                                        {
                                            errorMessage.Add("Please Correct the Compliance Applies   or Compliance Applies  not Defined in the System at row number - " + rowNum);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 20].Text.ToString().Trim()))
                                    {
                                        valComplianceAppliesID = GetAct_ApplicabilityByName(Regex.Replace(xlWorksheet.Cells[rowNum, 20].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                    }
                                    if (valComplianceAppliesID == 0 || valComplianceAppliesID == -1)
                                    {
                                        errorMessage.Add("Please Correct the Compliance Applies  or Compliance Applies  not Defined in the System at row number - " + rowNum);
                                    }
                                }
                            }
                            #endregion

                            #region 21 Linked to a License / Registration
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 21].Text.ToString().Trim()))
                            {
                                valLinkedtoaLicenseRegistration = Convert.ToString(xlWorksheet.Cells[rowNum, 21].Text).Trim();
                                if (valLinkedtoaLicenseRegistration.ToUpper().Trim() != "NA")
                                {
                                    if (valLinkedtoaLicenseRegistration.ToLower().Trim() != "yes" && valLinkedtoaLicenseRegistration.ToLower().Trim() != "no")
                                    {
                                        errorMessage.Add("Please Correct the Linked to a License / Registration  at row number - " + rowNum);
                                    }
                                }
                            }
                            #endregion

                            #region 22 License / Registration Type
                            if (valLinkedtoaLicenseRegistration.ToLower().Trim() != "no")
                            {
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 22].Text.ToString().Trim()))
                                {
                                    valLicenseRegistrationType = Convert.ToString(xlWorksheet.Cells[rowNum, 22].Text).Trim();
                                }
                                if (valLicenseRegistrationType.ToUpper().Trim() != "NA")
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 22].Text.ToString().Trim()))
                                    {
                                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 22].Text.ToString().Trim()))
                                        {
                                            valLicenseRegistrationTypeID = GetLicenseTypeByName(Regex.Replace(xlWorksheet.Cells[rowNum, 22].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                        }
                                        if (valLicenseRegistrationTypeID == 0 || valLicenseRegistrationTypeID == -1)
                                        {
                                            errorMessage.Add("Please Correct the License / Registration Type  or License / Registration Type not Defined in the System at row number - " + rowNum);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (xlWorksheet.Cells[rowNum, 22].Text.ToString().ToUpper().Trim() != "NA")
                                {
                                    errorMessage.Add("Please Correct the License / Registration Type  or License / Registration Type (It should be 'NA' if Linked to a License / Registration ('no'))not Defined in the System at row number - " + rowNum);
                                }
                            }
                            #endregion

                            #region 23 Activity Categorization
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 23].Text.ToString().Trim()))
                            {
                                valActivityCategorization = Convert.ToString(xlWorksheet.Cells[rowNum, 23].Text).Trim();
                            }
                            if (String.IsNullOrEmpty(valActivityCategorization))
                            {
                                errorMessage.Add("Please Correct the Activity Categorization  or Activity Categorization Not Empty at row number -" + rowNum);
                            }
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 23].Text.ToString().Trim()))
                            {
                                if (xlWorksheet.Cells[rowNum, 23].Text.ToString().ToUpper().Trim() != "NA")
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 23].Text.ToString().Trim()))
                                    {
                                        valActivityCategorizationID = GetCategorizationByName(Regex.Replace(xlWorksheet.Cells[rowNum, 23].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                    }
                                    if (valActivityCategorizationID == 0 || valActivityCategorizationID == -1)
                                    {
                                        errorMessage.Add("Please Correct the Activity Categorization  or Activity Categorization not Defined in the System at row number - " + rowNum);
                                    }
                                }
                            }
                            #endregion

                            #region 24 Activity Sub-Categorization
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 24].Text.ToString().Trim()))
                            {
                                valActivitySubCategorization = Convert.ToString(xlWorksheet.Cells[rowNum, 24].Text).Trim();
                            }
                            if (String.IsNullOrEmpty(valActivitySubCategorization))
                            {
                                errorMessage.Add("Please Correct the Activity Sub Categorization  or Activity Sub Categorization Not Empty at row number -" + rowNum);
                            }
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 24].Text.ToString().Trim()))
                            {
                                if (xlWorksheet.Cells[rowNum, 24].Text.ToString().ToUpper().Trim() != "NA")
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 24].Text.ToString().Trim()))
                                    {
                                        valActivitySubCategorizationID = GetSubCategorizationByName(Regex.Replace(xlWorksheet.Cells[rowNum, 24].Text.ToString().Trim(), @"\t|\n|\r", ""), valActivityCategorizationID);
                                    }
                                    if (valActivitySubCategorizationID == 0 || valActivitySubCategorizationID == -1)
                                    {
                                        errorMessage.Add("Please Correct the Activity Sub Categorization  or Activity Sub Categorization not Defined in the System at row number - " + rowNum);
                                    }
                                }
                            }
                            #endregion
                        }
                        #endregion

                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                        else
                        {
                            #region Save
                            for (int rowNum1 = 3; rowNum1 <= xlrow2; rowNum1++)
                            {
                                count = count + 1;
                                int ACTID = -1;
                                int ComplianceID = -1;
                                int CompanyTypeID = -1;
                                int BusinessActivityTypeID = -1;
                                int LocationTypeID = -1;
                                int IndustryTypeID = -1;
                                int ActAppliesID = -1;
                                int ACTQuestionTypeID = -1;
                                int ComplianceQuestionTypeID = -1;
                                int ComplianceAppliesID = -1;
                                int LicenseRegistrationTypeID = -1;
                                int ActivityCategorizationID = -1;
                                int ActivitySubCategorizationID = -1;

                                string CompanyType = string.Empty;
                                string BusinessActivityType = string.Empty;
                                string LocationType = string.Empty;
                                string IndustryType = string.Empty;
                                string ActApplies = string.Empty;
                                string ACTQuestionType = string.Empty;
                                string ActQuestion = string.Empty;
                                string ACTSingn = string.Empty;
                                string ACTAnswer = string.Empty;
                                string ActRangeFrom = string.Empty;
                                string ActRangeTo = string.Empty;
                                string ComplianceQuestion = string.Empty;
                                string ComplianceQuestionType = string.Empty;
                                string ComplianceQuestionSingn = string.Empty;
                                string ComplianceAnswer = string.Empty;
                                string ComplianceRangeFrom = string.Empty;
                                string ComplianceRangeTo = string.Empty;
                                string ComplianceApplies = string.Empty;
                                string LinkedtoaLicenseRegistration = string.Empty;
                                string LicenseRegistrationType = string.Empty;
                                string ActivityCategorization = string.Empty;
                                string ActivitySubCategorization = string.Empty;


                                #region 1 ACTID
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 1].Text.ToString().Trim()))
                                {
                                    ACTID = Convert.ToInt32(xlWorksheet.Cells[rowNum1, 1].Text.Trim());
                                }
                                #endregion

                                #region 2 ComplianceID
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 2].Text.ToString().Trim()))
                                {
                                    ComplianceID = Convert.ToInt32(xlWorksheet.Cells[rowNum1, 2].Text.Trim());
                                }
                                #endregion

                                #region 3 CompanyType 
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 3].Text.ToString().Trim()))
                                {
                                    CompanyType = Convert.ToString(xlWorksheet.Cells[rowNum1, 3].Text).Trim();
                                }
                                if (CompanyType.ToLower().Trim() == "all")
                                {
                                    var Records = (List<ComplianceManagement.Business.Data.CompanyType>)Cache["IMACompanyTypeListData"];
                                    if (Records.Count > 0)
                                    {
                                        foreach (var item in Records)
                                        {
                                            Act_CompanyTypeMapping ActCompanyTypeMapping = new Act_CompanyTypeMapping()
                                            {
                                                ActID = ACTID,
                                                TypeID = item.ID,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedDate = DateTime.Now,
                                            };
                                            ActCompanyTypeMappingList.Add(ActCompanyTypeMapping);
                                        }
                                    }
                                }
                                else
                                {
                                    string CT = xlWorksheet.Cells[rowNum1, 3].Text.ToString().Trim();
                                    string[] splitcttypes = CT.Split(',');
                                    if (splitcttypes.Length > 0)
                                    {
                                        for (int rs = 0; rs < splitcttypes.Length; rs++)
                                        {
                                            CompanyTypeID = GetCompanyTypeByName(Regex.Replace(splitcttypes[rs].ToString().Trim(), @"\t|\n|\r", ""));
                                            Act_CompanyTypeMapping ActCompanyTypeMapping = new Act_CompanyTypeMapping()
                                            {
                                                ActID = ACTID,
                                                TypeID = CompanyTypeID,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedDate = DateTime.Now,
                                            };
                                            ActCompanyTypeMappingList.Add(ActCompanyTypeMapping);
                                        }
                                    }
                                }
                                #endregion

                                #region 4 BusinessActivityType 
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 4].Text.ToString().Trim()))
                                {
                                    BusinessActivityType = Convert.ToString(xlWorksheet.Cells[rowNum1, 4].Text).Trim();
                                }
                                if (BusinessActivityType.ToLower().Trim() == "all")
                                {
                                    var Records = (List<ComplianceManagement.Business.Data.BusinessActivityType>)Cache["IMABusinessActivityTypeListData"];
                                    if (Records.Count > 0)
                                    {
                                        foreach (var item in Records)
                                        {
                                            Act_BusinessActivityMapping ActBusinessActivityMapping = new Act_BusinessActivityMapping()
                                            {
                                                ActID = ACTID,
                                                BATypeID = item.ID,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedDate = DateTime.Now,
                                            };
                                            ActBusinessActivityMappingList.Add(ActBusinessActivityMapping);
                                        }
                                    }
                                }
                                else
                                {
                                    string BAT = xlWorksheet.Cells[rowNum1, 4].Text.ToString().Trim();
                                    string[] splitbattypes = BAT.Split(',');
                                    if (splitbattypes.Length > 0)
                                    {
                                        for (int rs = 0; rs < splitbattypes.Length; rs++)
                                        {
                                            BusinessActivityTypeID = GetBusinessActivityTypeByName(Regex.Replace(splitbattypes[rs].ToString().Trim(), @"\t|\n|\r", ""));
                                            Act_BusinessActivityMapping ActBusinessActivityMapping = new Act_BusinessActivityMapping()
                                            {
                                                ActID = ACTID,
                                                BATypeID = BusinessActivityTypeID,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedDate = DateTime.Now,
                                            };
                                            ActBusinessActivityMappingList.Add(ActBusinessActivityMapping);
                                        }
                                    }
                                }
                                #endregion

                                #region 5 LocationType 
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 5].Text.ToString().Trim()))
                                {
                                    LocationType = Convert.ToString(xlWorksheet.Cells[rowNum1, 5].Text).Trim();
                                }
                                if (LocationType.ToLower().Trim() == "all")
                                {

                                    var Records = (List<ComplianceManagement.Business.Data.LocationType>)Cache["IMALocationTypeListData"];
                                    if (Records.Count > 0)
                                    {
                                        foreach (var item in Records)
                                        {
                                            Act_LoctionTypeMapping ActIndustryMapping = new Act_LoctionTypeMapping()
                                            {
                                                ActID = ACTID,
                                                LoctionTypeID = item.ID,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedDate = DateTime.Now,
                                            };
                                            ActLoctionTypeMappingMappingList.Add(ActIndustryMapping);
                                        }
                                    }
                                }
                                else
                                {
                                    string locationtype = xlWorksheet.Cells[rowNum1, 5].Text.ToString().Trim();
                                    string[] splitltype = locationtype.Split(',');
                                    if (splitltype.Length > 0)
                                    {
                                        for (int rs = 0; rs < splitltype.Length; rs++)
                                        {
                                            LocationTypeID = GetLocationTypeByName(Regex.Replace(xlWorksheet.Cells[rowNum1, 5].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                            Act_LoctionTypeMapping ActIndustryMapping = new Act_LoctionTypeMapping()
                                            {
                                                ActID = ACTID,
                                                LoctionTypeID = LocationTypeID,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedDate = DateTime.Now,
                                            };
                                            ActLoctionTypeMappingMappingList.Add(ActIndustryMapping);
                                        }
                                    }
                                }

                                //if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 5].Text.ToString().Trim()))
                                //{
                                //    LocationTypeID = GetLocationTypeByName(Regex.Replace(xlWorksheet.Cells[rowNum1, 5].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                //}
                                #endregion

                                #region 6 Industry Type  
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 6].Text.ToString().Trim()))
                                {
                                    IndustryType = Convert.ToString(xlWorksheet.Cells[rowNum1, 6].Text).Trim();
                                }

                                if (IndustryType.ToLower().Trim() == "all")
                                {

                                    var Records = (List<ComplianceManagement.Business.Data.Industry>)Cache["IMAIndustryListData"];
                                    if (Records.Count > 0)
                                    {
                                        foreach (var item in Records)
                                        {
                                            Act_IndustryMapping ActIndustryMapping = new Act_IndustryMapping()
                                            {
                                                ActID = ACTID,
                                                IndustryID = item.ID,
                                                IsActive = true,
                                                EditedBy = AuthenticationHelper.UserID,
                                                EditedDate = DateTime.Now,
                                            };
                                            ActIndustryMappingList.Add(ActIndustryMapping);
                                        }
                                    }
                                }
                                else
                                {
                                    string industry = xlWorksheet.Cells[rowNum1, 6].Text.ToString().Trim();
                                    string[] splitind = industry.Split(',');
                                    if (splitind.Length > 0)
                                    {
                                        for (int rs = 0; rs < splitind.Length; rs++)
                                        {
                                            IndustryTypeID = GetIndustryByName(Regex.Replace(splitind[rs].ToString().Trim(), @"\t|\n|\r", ""));
                                            Act_IndustryMapping ActIndustryMapping = new Act_IndustryMapping()
                                            {
                                                ActID = ACTID,
                                                IndustryID = IndustryTypeID,
                                                IsActive = true,
                                                EditedBy = AuthenticationHelper.UserID,
                                                EditedDate = DateTime.Now,
                                            };
                                            ActIndustryMappingList.Add(ActIndustryMapping);
                                        }
                                    }
                                }
                                #endregion

                                #region 7 Act Applies
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 7].Text.ToString().Trim()))
                                {
                                    ActApplies = Convert.ToString(xlWorksheet.Cells[rowNum1, 7].Text).Trim();
                                }

                                if (ActApplies.ToLower().Trim() == "all")
                                {
                                    var Records = (List<ComplianceManagement.Business.Data.Act_Applicability>)Cache["IMAAct_ApplicabilityListData"];
                                    if (Records.Count > 0)
                                    {
                                        foreach (var item in Records)
                                        {
                                            Act_ActApplicabilityMapping ActApplicabilityMapping = new Act_ActApplicabilityMapping()
                                            {
                                                ActID = ACTID,
                                                Act_ApplicabilityID = item.ID,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedDate = DateTime.Now,
                                            };
                                            ActApplicabilityMappingList.Add(ActApplicabilityMapping);
                                        }
                                    }
                                }
                                else
                                {
                                    string AA = xlWorksheet.Cells[rowNum1, 7].Text.ToString().Trim();
                                    string[] splitactppli = AA.Split(',');
                                    if (splitactppli.Length > 0)
                                    {
                                        for (int rs = 0; rs < splitactppli.Length; rs++)
                                        {
                                            ActAppliesID = GetAct_ApplicabilityByName(Regex.Replace(splitactppli[rs].ToString().Trim(), @"\t|\n|\r", ""));
                                            Act_ActApplicabilityMapping ActApplicabilityMapping = new Act_ActApplicabilityMapping()
                                            {
                                                ActID = ACTID,
                                                Act_ApplicabilityID = ActAppliesID,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedDate = DateTime.Now,
                                            };
                                            ActApplicabilityMappingList.Add(ActApplicabilityMapping);
                                        }
                                    }
                                }
                                #endregion

                                #region 8 ACT Question Type                           
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 8].Text.ToString().Trim()))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 8].Text.ToString().Trim()))
                                    {
                                        ACTQuestionTypeID = GetQuestionAnswerControlByName(Regex.Replace(xlWorksheet.Cells[rowNum1, 8].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                    }
                                }
                                #endregion

                                #region 9 Act Question
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 9].Text.ToString().Trim()))
                                {
                                    ActQuestion = Convert.ToString(xlWorksheet.Cells[rowNum1, 9].Text).Trim();
                                }
                                #endregion

                                #region 10 Act sign
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 10].Text.ToString().Trim()))
                                {
                                    ACTSingn = Convert.ToString(xlWorksheet.Cells[rowNum1, 10].Text).Trim();
                                }
                                #endregion

                                #region 11 Act Answer 
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 11].Text.ToString().Trim()))
                                {
                                    if (Convert.ToString(xlWorksheet.Cells[rowNum1, 11].Text).ToUpper().Trim() != "NA")
                                    {
                                        ACTAnswer = Convert.ToString(xlWorksheet.Cells[rowNum1, 11].Text).Trim();
                                    }
                                }
                                #endregion

                                #region 12 Act Range From
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 12].Text.ToString().Trim()))
                                {
                                    if (Convert.ToString(xlWorksheet.Cells[rowNum1, 12].Text).ToUpper().Trim() != "NA")
                                    {
                                        ActRangeFrom = Convert.ToString(xlWorksheet.Cells[rowNum1, 12].Text).Trim();
                                    }
                                    else
                                    {
                                        ActRangeFrom = "0";
                                    }
                                }
                                #endregion

                                #region 13 Act Range To
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 13].Text.ToString().Trim()))
                                {
                                    if (Convert.ToString(xlWorksheet.Cells[rowNum1, 13].Text).ToUpper().Trim() != "NA")
                                    {
                                        ActRangeTo = Convert.ToString(xlWorksheet.Cells[rowNum1, 13].Text).Trim();
                                    }
                                    else
                                    {
                                        ActRangeTo = "0";
                                    }
                                }
                                #endregion

                                #region 14 Compliance Question Type                           
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 14].Text.ToString().Trim()))
                                {
                                    if (xlWorksheet.Cells[rowNum1, 14].Text.ToString().ToUpper().Trim() != "NA")
                                    {
                                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 14].Text.ToString().Trim()))
                                        {
                                            ComplianceQuestionTypeID = GetQuestionAnswerControlByName(Regex.Replace(xlWorksheet.Cells[rowNum1, 14].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                        }
                                    }

                                }
                                #endregion

                                #region 15 Compliance Question 
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 15].Text.ToString().Trim()))
                                {
                                    ComplianceQuestion = Convert.ToString(xlWorksheet.Cells[rowNum1, 15].Text).Trim();
                                }
                                #endregion
                                
                                #region 16 Compliance sign
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 16].Text.ToString().Trim()))
                                {
                                    ComplianceQuestionSingn = Convert.ToString(xlWorksheet.Cells[rowNum1, 16].Text).Trim();
                                }
                                #endregion

                                #region 17 Compliance Answer
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 17].Text.ToString().Trim()))
                                {
                                    if (Convert.ToString(xlWorksheet.Cells[rowNum1, 17].Text).ToUpper().Trim() != "NA")
                                    {
                                        ComplianceAnswer = Convert.ToString(xlWorksheet.Cells[rowNum1, 17].Text).Trim();
                                    }
                                }
                                #endregion
                                #region 18 Compliance Range From
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 18].Text.ToString().Trim()))
                                {
                                    if (Convert.ToString(xlWorksheet.Cells[rowNum1, 18].Text).ToUpper().Trim() != "NA")
                                    {
                                        ComplianceRangeFrom = Convert.ToString(xlWorksheet.Cells[rowNum1, 18].Text).Trim();
                                    }
                                    else
                                    {
                                        ComplianceRangeFrom = "0";
                                    }
                                }
                                #endregion

                                #region 19 Compliance Range To
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 19].Text.ToString().Trim()))
                                {
                                    if (Convert.ToString(xlWorksheet.Cells[rowNum1, 19].Text).ToUpper().Trim() != "NA")
                                    {
                                        ComplianceRangeTo = Convert.ToString(xlWorksheet.Cells[rowNum1, 19].Text).Trim();
                                    }
                                    else
                                    {
                                        ComplianceRangeTo = "0";
                                    }
                                }
                                #endregion

                                #region 20 Compliance Applies
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 20].Text.ToString().Trim()))
                                {
                                    ComplianceApplies = Convert.ToString(xlWorksheet.Cells[rowNum1, 20].Text).Trim();
                                }
                                if (ComplianceApplies.ToLower().Trim() == "all")
                                {
                                    var Records = (List<ComplianceManagement.Business.Data.Act_Applicability>)Cache["IMAAct_ApplicabilityListData"];
                                    if (Records.Count > 0)
                                    {
                                        foreach (var item in Records)
                                        {
                                            Compliance_ActApplicabilityMapping ComplianceActApplicabilityMapping = new Compliance_ActApplicabilityMapping()
                                            {
                                                ComplianceID = ComplianceID,
                                                Act_ApplicabilityID = item.ID,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedDate = DateTime.Now,
                                            };
                                            ComplianceActApplicabilityMappingList.Add(ComplianceActApplicabilityMapping);
                                        }
                                    }
                                }
                                else
                                {
                                    string AA = xlWorksheet.Cells[rowNum1, 20].Text.ToString().Trim();
                                    string[] splitactppli = AA.Split(',');
                                    if (splitactppli.Length > 0)
                                    {
                                        for (int rs = 0; rs < splitactppli.Length; rs++)
                                        {
                                            ComplianceAppliesID = GetAct_ApplicabilityByName(Regex.Replace(splitactppli[rs].ToString().Trim(), @"\t|\n|\r", ""));
                                            Compliance_ActApplicabilityMapping ComplianceActApplicabilityMapping = new Compliance_ActApplicabilityMapping()
                                            {
                                                ComplianceID = ComplianceID,
                                                Act_ApplicabilityID = ComplianceAppliesID,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedDate = DateTime.Now,
                                            };
                                            ComplianceActApplicabilityMappingList.Add(ComplianceActApplicabilityMapping);
                                        }
                                    }
                                }
                                #endregion

                                #region 21 Linked to a License / Registration 
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 21].Text.ToString().Trim()))
                                {
                                    LinkedtoaLicenseRegistration = Convert.ToString(xlWorksheet.Cells[rowNum1, 21].Text).Trim();
                                    if (LinkedtoaLicenseRegistration.ToLower().Trim() != "yes")
                                    {
                                        LicenseRegistrationTypeID = 1;
                                    }
                                    else
                                    {
                                        LicenseRegistrationTypeID = 2;
                                    }                                    
                                }
                                #endregion

                                #region 22 License / Registration Type                          
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 22].Text.ToString().Trim()))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 22].Text.ToString().Trim()))
                                    {
                                        if (xlWorksheet.Cells[rowNum1, 22].Text.ToString().Trim().ToUpper().Trim() != "NA")
                                        {
                                            LicenseRegistrationTypeID = GetLicenseTypeByName(Regex.Replace(xlWorksheet.Cells[rowNum1, 22].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                        }
                                    }
                                }
                                #endregion

                                #region 23 Activity Categorization                           
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 23].Text.ToString().Trim()))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 23].Text.ToString().Trim()))
                                    {
                                        if (xlWorksheet.Cells[rowNum1, 23].Text.ToString().Trim().ToUpper().Trim() != "NA")
                                        {
                                            ActivityCategorizationID = GetCategorizationByName(Regex.Replace(xlWorksheet.Cells[rowNum1, 23].Text.ToString().Trim(), @"\t|\n|\r", ""));
                                        }
                                    }
                                }
                                #endregion

                                #region 24 Activity Sub-Categorization                           
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 24].Text.ToString().Trim()))
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 24].Text.ToString().Trim()))
                                    {
                                        if (xlWorksheet.Cells[rowNum1, 24].Text.ToString().Trim().ToUpper().Trim() != "NA")
                                        {
                                            ActivitySubCategorizationID = GetSubCategorizationByName(Regex.Replace(xlWorksheet.Cells[rowNum1, 24].Text.ToString().Trim(), @"\t|\n|\r", ""), ActivityCategorizationID);
                                        }
                                    }

                                }
                                #endregion

                                #region AcT Wise
                                var quid = QuestionExists(ACTQuestionTypeID, ActQuestion);
                                if (quid == 0)
                                {
                                    QuestionMaster Qm = new QuestionMaster();
                                    Qm.QuestionName = ActQuestion;
                                    if (ACTQuestionTypeID==0)
                                    {

                                    }
                                    Qm.ControlId = ACTQuestionTypeID;
                                    if (ACTQuestionTypeID == 1 || ACTQuestionTypeID == 2)
                                    {
                                        Qm.PossibleAnswer = ACTAnswer;
                                    }
                                    else
                                    {
                                        Qm.PossibleAnswer = "";
                                    }
                                    Qm.IsQuestionType = 2;
                                    Qm.CreatedOn = DateTime.Now;
                                    Qm.CreatedBy = AuthenticationHelper.UserID;
                                    Qm.IsActive = false;
                                    entities.QuestionMasters.Add(Qm);
                                    entities.SaveChanges();
                                    quid = Qm.ID;
                                }
                                Questions_ActMapping Qmst = new Questions_ActMapping();
                                Qmst.ActId = ACTID;
                                Qmst.QuestionID = quid;
                                Qmst.ControllId = ACTQuestionTypeID;
                                var ServiceID = -1;
                                if (ACTSingn == "<")
                                {
                                    ServiceID = 1;
                                }
                                else if (ACTSingn == ">")
                                {
                                    ServiceID = 2;
                                }
                                else if (ACTSingn == "=")
                                {
                                    ServiceID = 3;
                                }
                                else if (ACTSingn == "≥")
                                {
                                    ServiceID = 4;
                                }
                                else if (ACTSingn == "≤")
                                {
                                    ServiceID = 5;
                                }
                                Qmst.AnswerTOQ = ServiceID;

                                if (ACTQuestionTypeID == 1 || ACTQuestionTypeID == 2)
                                {
                                    Qmst.AnsweSingleMulti = ACTAnswer;
                                }
                                else if (ACTQuestionTypeID == 3)
                                {
                                    Qmst.AnsweRangeMin = Convert.ToInt32(ActRangeFrom);
                                    Qmst.AnsweRangeMax = Convert.ToInt32(ActRangeTo);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(ACTAnswer))
                                    {
                                        Qmst.AnswerValue = Convert.ToInt32(ACTAnswer);
                                    }
                                    else
                                    {
                                        Qmst.AnswerValue = 0;
                                    }                                 
                                }
                                Qmst.StatusId = 2;
                                Qmst.Createdon = DateTime.Now;
                                Qmst.Createdby = AuthenticationHelper.UserID;
                                Qmst.IsActive = false;
                                QuestionsActMappingList.Add(Qmst);
                                #endregion

                                #region  Compliancewise

                                var Cquid = QuestionExists(ComplianceQuestionTypeID, ComplianceQuestion);
                                if (Cquid == 0)
                                {
                                    QuestionMaster Qm = new QuestionMaster();
                                    Qm.QuestionName = ComplianceQuestion;
                                    Qm.ControlId = ComplianceQuestionTypeID;
                                    if (ComplianceQuestionTypeID==1 || ComplianceQuestionTypeID==2)
                                    {
                                        Qm.PossibleAnswer = ComplianceAnswer;
                                    }
                                    else
                                    {
                                        Qm.PossibleAnswer = "";
                                    }
                                   
                                    Qm.IsQuestionType = 1;
                                    Qm.CreatedOn = DateTime.Now;
                                    Qm.CreatedBy = AuthenticationHelper.UserID;
                                    Qm.IsActive = false;
                                    entities.QuestionMasters.Add(Qm);
                                    entities.SaveChanges();
                                    Cquid = Qm.ID;
                                }

                                Questions_ActComplianceMapping QACmst = new Questions_ActComplianceMapping();
                                QACmst.ActId = ACTID;
                                QACmst.ComplianceId = ComplianceID;
                                QACmst.QuestionID = Cquid;
                                QACmst.ControllId = ComplianceQuestionTypeID;
                                var cServiceID = -1;
                                if (ComplianceQuestionSingn == "<")
                                {
                                    cServiceID = 1;
                                }
                                else if (ComplianceQuestionSingn == ">")
                                {
                                    cServiceID = 2;
                                }
                                else if (ComplianceQuestionSingn == "=")
                                {
                                    cServiceID = 3;
                                }
                                else if (ComplianceQuestionSingn == "≥")
                                {
                                    cServiceID = 4;
                                }
                                else if (ComplianceQuestionSingn == "≤")
                                {
                                    cServiceID = 5;
                                }
                                QACmst.AnswerTOQ = cServiceID;
                                if (xlWorksheet.Cells[rowNum1, 14].Text.ToString().ToUpper().Trim() != "NA")
                                {
                                    if (ComplianceQuestionTypeID == 1 || ComplianceQuestionTypeID == 2)
                                    {
                                        QACmst.AnsweSingleMulti = ComplianceAnswer;
                                    }
                                    else if (ComplianceQuestionTypeID == 3)
                                    {
                                        QACmst.AnsweRangeMin = Convert.ToInt32(ComplianceRangeFrom);
                                        QACmst.AnsweRangeMax = Convert.ToInt32(ComplianceRangeTo);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(ComplianceAnswer))
                                        {
                                            QACmst.AnswerValue = Convert.ToInt32(ComplianceAnswer);
                                        }
                                        else
                                        {
                                            QACmst.AnswerValue = 0;
                                        }
                                    }
                                }                           
                                QACmst.StatusId = 2;                             
                                QACmst.IslicenseID = LicenseRegistrationTypeID;
                                QACmst.LicenseTypeID = LicenseRegistrationTypeID;
                                QACmst.CategoryID = ActivityCategorizationID;
                                QACmst.SubCategoryID = ActivitySubCategorizationID;
                                QACmst.Createdon = DateTime.Now;
                                QACmst.Createdby = AuthenticationHelper.UserID;
                                QACmst.IsActive = false;
                                QuestionsActComplianceMappingList.Add(QACmst);
                                #endregion                              
                            }
                            #endregion

                            if (QuestionsActMappingList.Count>0)
                            {
                                sucess = CreateExcelQuestionsActMapping(QuestionsActMappingList);
                            }
                            if (sucess && QuestionsActComplianceMappingList.Count>0)
                            {
                                sucess = CreateExcelQuestionsActComplianceMapping(QuestionsActComplianceMappingList);                                                               
                            }
                            if (sucess && ActCompanyTypeMappingList.Count > 0)
                            {
                                sucess = CreateExcelActCompanyTypeMapping(ActCompanyTypeMappingList);                                                               
                            }
                            if (sucess && ActBusinessActivityMappingList.Count > 0)
                            {
                                sucess = CreateExcelActBusinessActivityMapping(ActBusinessActivityMappingList);                                                               
                            }
                            if (sucess && ActLoctionTypeMappingMappingList.Count > 0)
                            {
                                sucess = CreateExcelActLoctionTypeMapping(ActLoctionTypeMappingMappingList);
                            }
                            if (sucess && ActApplicabilityMappingList.Count > 0)
                            {
                                sucess = CreateExcelActApplicabilityMapping(ActApplicabilityMappingList);                                                               
                            }
                            if (sucess && ComplianceActApplicabilityMappingList.Count > 0)
                            {
                                sucess = CreateExcelComplianceActApplicabilityMapping(ComplianceActApplicabilityMappingList);
                            }
                            if (sucess && ActIndustryMappingList.Count > 0)
                            {
                                sucess = CreateExcelActIndustryMapping(ActIndustryMappingList);                           
                            }                           
                        }
                    }
                }
                catch (Exception ex)
                {
                    sucess = false;
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
        public long Compliance_ActApplicabilityMappingExists(Compliance_ActApplicabilityMapping questionactmapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Compliance_ActApplicabilityMapping
                             where row.IsActive == true
                             && row.ComplianceID == questionactmapping.ComplianceID
                             && row.Act_ApplicabilityID == questionactmapping.Act_ApplicabilityID
                             select row.ID).FirstOrDefault();
                return query;
            }
        }
        public bool CreateExcelComplianceActApplicabilityMapping(List<Compliance_ActApplicabilityMapping> TempassignmentTable)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempassignmentTable.ForEach(entry =>
                    {
                        var Cquid = Compliance_ActApplicabilityMappingExists(entry);
                        if (Cquid == 0)
                        {
                            entities.Compliance_ActApplicabilityMapping.Add(entry);
                        }
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public long Act_IndustryMappingExists(Act_IndustryMapping questionactmapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_IndustryMapping
                             where row.IsActive == true
                             && row.ActID == questionactmapping.ActID
                             && row.IndustryID == questionactmapping.IndustryID
                             select row.ID).FirstOrDefault();
                return query;
            }
        }
        public bool CreateExcelActIndustryMapping(List<Act_IndustryMapping> TempassignmentTable)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempassignmentTable.ForEach(entry =>
                    {
                        var Cquid = Act_IndustryMappingExists(entry);
                        if (Cquid == 0)
                        {
                            entities.Act_IndustryMapping.Add(entry);
                        }
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public long Act_ActApplicabilityMappingExists(Act_ActApplicabilityMapping questionactmapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_ActApplicabilityMapping
                             where row.IsActive == true
                             && row.ActID == questionactmapping.ActID
                             && row.Act_ApplicabilityID == questionactmapping.Act_ApplicabilityID
                             select row.ID).FirstOrDefault();
                return query;
            }
        }
        public bool CreateExcelActApplicabilityMapping(List<Act_ActApplicabilityMapping> TempassignmentTable)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempassignmentTable.ForEach(entry =>
                    {
                        var Cquid = Act_ActApplicabilityMappingExists(entry);
                        if (Cquid == 0)
                        {
                            entities.Act_ActApplicabilityMapping.Add(entry);
                            entities.SaveChanges();
                        }
                    });                   
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public long Act_BusinessActivityMappingExists(Act_BusinessActivityMapping questionactmapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_BusinessActivityMapping
                             where row.IsActive == true
                             && row.ActID == questionactmapping.ActID
                             && row.BATypeID == questionactmapping.BATypeID
                             select row.ID).FirstOrDefault();
                return query;
            }
        }
        public bool CreateExcelActBusinessActivityMapping(List<Act_BusinessActivityMapping> TempassignmentTable)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempassignmentTable.ForEach(entry =>
                    {
                        var Cquid = Act_BusinessActivityMappingExists(entry);
                        if (Cquid == 0)
                        {
                            entities.Act_BusinessActivityMapping.Add(entry);
                            entities.SaveChanges();
                        }
                    });
                   
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public long Act_CompanyTypeMappingExists(Act_CompanyTypeMapping questionactmapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_CompanyTypeMapping
                             where row.IsActive == true
                             && row.ActID == questionactmapping.ActID
                             && row.TypeID == questionactmapping.TypeID
                             select row.ID).FirstOrDefault();
                return query;
            }
        }
        public bool CreateExcelActCompanyTypeMapping(List<Act_CompanyTypeMapping> TempassignmentTable)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempassignmentTable.ForEach(entry =>
                    {
                        var Cquid = Act_CompanyTypeMappingExists(entry);
                        if (Cquid == 0)
                        {
                            entities.Act_CompanyTypeMapping.Add(entry);
                            entities.SaveChanges();
                        }
                    });
                  
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
       
        public long Act_LoctionTypeMappingExists(Act_LoctionTypeMapping questionactmapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Act_LoctionTypeMapping
                             where row.IsActive == true
                             && row.ActID == questionactmapping.ActID
                             && row.LoctionTypeID == questionactmapping.LoctionTypeID                          
                             select row.ID).FirstOrDefault();
                return query;
            }
        }
        public bool CreateExcelActLoctionTypeMapping(List<Act_LoctionTypeMapping> TempassignmentTable)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempassignmentTable.ForEach(entry =>
                    {                        
                        var Cquid = Act_LoctionTypeMappingExists(entry);
                        if (Cquid == 0)
                        {
                            entities.Act_LoctionTypeMapping.Add(entry);
                            entities.SaveChanges();
                        }
                    });                  
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public long Questions_ActComplianceMappingExists(Questions_ActComplianceMapping questionactcompliancemapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Questions_ActComplianceMapping
                             where row.IsActive == false
                             && row.ActId == questionactcompliancemapping.ActId
                             && row.ComplianceId == questionactcompliancemapping.ComplianceId
                             && row.ControllId == questionactcompliancemapping.ControllId
                             && row.QuestionID == questionactcompliancemapping.QuestionID
                             && row.AnswerTOQ == questionactcompliancemapping.AnswerTOQ
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public bool CreateExcelQuestionsActComplianceMapping(List<Questions_ActComplianceMapping> TempassignmentTable)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempassignmentTable.ForEach(entry =>
                    {
                        var Cquid = Questions_ActComplianceMappingExists(entry);
                        if (Cquid == 0)
                        {
                            entities.Questions_ActComplianceMapping.Add(entry);
                            entities.SaveChanges();
                        }
                    });                   
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        public long Questions_ActMappingExists(Questions_ActMapping questionactmapping)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Questions_ActMapping
                             where row.IsActive == false
                             && row.ActId == questionactmapping.ActId
                             && row.ControllId == questionactmapping.ControllId
                             && row.QuestionID == questionactmapping.QuestionID
                             && row.AnswerTOQ == questionactmapping.AnswerTOQ
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public bool CreateExcelQuestionsActMapping(List<Questions_ActMapping> TempassignmentTable)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempassignmentTable.ForEach(entry =>
                    {
                        var Cquid = Questions_ActMappingExists(entry);
                        if (Cquid ==0)
                        {
                            entities.Questions_ActMapping.Add(entry);
                            entities.SaveChanges();
                        }                        
                    });
              
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void GetCompliance()
        {
            List<ComplianceManagement.Business.Data.Compliance> Records = new List<ComplianceManagement.Business.Data.Compliance>();

            var ComplianceList = HttpContext.Current.Cache["IMAComplianceListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Compliances
                               where row.IsDeleted == false && row.EventFlag == null
                               && row.Status == null
                               && row.ComplinceVisible == true // Add 11 March 2019                                  
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("IMAComplianceListData", Records); // add it to cache
                }
            }
        }
        public static void GetACT()
        {
            List<ComplianceManagement.Business.Data.Act> Records = new List<ComplianceManagement.Business.Data.Act>();

            var ComplianceList = HttpContext.Current.Cache["IMAActListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Acts
                               where row.IsDeleted == false                                                            
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("IMAActListData", Records); // add it to cache
                }
            }
        }       
        public static void GetLocationType()
        {
            List<ComplianceManagement.Business.Data.LocationType> Records = new List<ComplianceManagement.Business.Data.LocationType>();

            var ComplianceList = HttpContext.Current.Cache["IMALocationTypeListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.LocationTypes                             
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("IMALocationTypeListData", Records); // add it to cache
                }
            }
        }
        public int GetLocationTypeByName(string Processname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int ProcessId = -1;
                var Records = (List<ComplianceManagement.Business.Data.LocationType>)Cache["IMALocationTypeListData"];
                if (Records.Count > 0)
                {
                    ProcessId = (from row in Records
                                 where row.Name.ToUpper().Trim() == Processname.ToUpper().Trim()
                                 select row.ID).FirstOrDefault();
                }
                return Convert.ToInt32(ProcessId);
            }
        }
        public static void GetBusinessActivityType()
        {
            List<ComplianceManagement.Business.Data.BusinessActivityType> Records = new List<ComplianceManagement.Business.Data.BusinessActivityType>();

            var ComplianceList = HttpContext.Current.Cache["IMABusinessActivityTypeListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.BusinessActivityTypes
                               where row.IsActive==false
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("IMABusinessActivityTypeListData", Records); // add it to cache
                }
            }
        }
        public int GetBusinessActivityTypeByName(string Processname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int ProcessId = -1;
                var Records = (List<ComplianceManagement.Business.Data.BusinessActivityType>)Cache["IMABusinessActivityTypeListData"];
                if (Records.Count > 0)
                {
                    ProcessId = (from row in Records
                                 where row.Name.ToUpper().Trim() == Processname.ToUpper().Trim()
                                 select row.ID).FirstOrDefault();
                }
                return Convert.ToInt32(ProcessId);
            }
        }
        public static void GetAct_Applicability()
        {
            List<ComplianceManagement.Business.Data.Act_Applicability> Records = new List<ComplianceManagement.Business.Data.Act_Applicability>();

            var ComplianceList = HttpContext.Current.Cache["IMAAct_ApplicabilityListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Act_Applicability                               
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("IMAAct_ApplicabilityListData", Records); // add it to cache
                }
            }
        }
        public int GetAct_ApplicabilityByName(string Processname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int ProcessId = -1;
                var Records = (List<ComplianceManagement.Business.Data.Act_Applicability>)Cache["IMAAct_ApplicabilityListData"];
                if (Records.Count > 0)
                {
                    ProcessId = (from row in Records
                                 where row.Name.ToUpper().Trim() == Processname.ToUpper().Trim()
                                 select row.ID).FirstOrDefault();
                }
                return Convert.ToInt32(ProcessId);
            }
        }
        public int GetQuestionAnswerControlByName(string Processname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int ProcessId = (from row in entities.QuestionAnswerControls
                                 where row.ControlName.ToUpper().Trim() == Processname.ToUpper().Trim()
                                 select row.ID).FirstOrDefault();
                
                return Convert.ToInt32(ProcessId);
            }
        }        
        public static void GetCompanyType()
        {
            List<ComplianceManagement.Business.Data.CompanyType> Records = new List<ComplianceManagement.Business.Data.CompanyType>();

            var ComplianceList = HttpContext.Current.Cache["IMACompanyTypeListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.CompanyTypes
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("IMACompanyTypeListData", Records); // add it to cache
                }
            }
        }
        public  int GetCompanyTypeByName(string Processname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int ProcessId = -1;
                var Records = (List<ComplianceManagement.Business.Data.CompanyType>)Cache["IMACompanyTypeListData"];
                if (Records.Count > 0)
                {
                    ProcessId = (from row in Records
                                 where row.Name.ToUpper().Trim() == Processname.ToUpper().Trim()
                                 select row.ID).FirstOrDefault();                    
                }
                return Convert.ToInt32(ProcessId);
            }
        }
        public static void GetIndustry()
        {
            List<ComplianceManagement.Business.Data.Industry> Records = new List<ComplianceManagement.Business.Data.Industry>();

            var ComplianceList = HttpContext.Current.Cache["IMAIndustryListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Industries
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("IMAIndustryListData", Records); // add it to cache
                }
            }
        }
        public int GetIndustryByName(string Processname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int ProcessId = -1;
                var Records = (List<ComplianceManagement.Business.Data.Industry>)Cache["IMAIndustryListData"];
                if (Records.Count > 0)
                {
                    ProcessId = (from row in Records
                                 where row.Name.ToUpper().Trim() == Processname.ToUpper().Trim()
                                 select row.ID).FirstOrDefault();
                }
                return Convert.ToInt32(ProcessId);
            }
        }
        public static void GetLicenseTypeMaster()
        {
            List<ComplianceManagement.Business.Data.Lic_tbl_LicenseType_Master> Records = new List<ComplianceManagement.Business.Data.Lic_tbl_LicenseType_Master>();

            var ComplianceList = HttpContext.Current.Cache["IMALicenseTypeListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Lic_tbl_LicenseType_Master
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("IMALicenseTypeListData", Records); // add it to cache
                }
            }
        }
        public int GetLicenseTypeByName(string Processname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int ProcessId = -1;
                var Records = (List<ComplianceManagement.Business.Data.Lic_tbl_LicenseType_Master>)Cache["IMALicenseTypeListData"];
                if (Records.Count > 0)
                {
                    ProcessId = (from row in Records
                                 where row.Name.ToUpper().Trim() == Processname.ToUpper().Trim()
                                 select (int)row.ID).FirstOrDefault();
                }
                return Convert.ToInt32(ProcessId);
            }
        }        
        public static void GetCategorization()
        {
            List<ComplianceManagement.Business.Data.mst_categorization> Records = new List<ComplianceManagement.Business.Data.mst_categorization>();

            var ComplianceList = HttpContext.Current.Cache["IMACategorizationListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.mst_categorization
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("IMACategorizationListData", Records); // add it to cache
                }
            }
        }
        public int GetCategorizationByName(string Processname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int ProcessId = -1;
                var Records = (List<ComplianceManagement.Business.Data.mst_categorization>)Cache["IMACategorizationListData"];
                if (Records.Count > 0)
                {
                    ProcessId = (from row in Records
                                 where row.Name.ToUpper().Trim() == Processname.ToUpper().Trim()
                                 select row.ID).FirstOrDefault();
                }
                return Convert.ToInt32(ProcessId);
            }
        }
        public static void GetSubCategorization()
        {
            List<ComplianceManagement.Business.Data.mst_Subcategorization> Records = new List<ComplianceManagement.Business.Data.mst_Subcategorization>();

            var ComplianceList = HttpContext.Current.Cache["IMASubCategorizationListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.mst_Subcategorization
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("IMASubCategorizationListData", Records); // add it to cache
                }
            }
        }
        public int GetSubCategorizationByName(string Processname, int categoryid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int ProcessId = -1;
                var Records = (List<ComplianceManagement.Business.Data.mst_Subcategorization>)Cache["IMASubCategorizationListData"];
                if (Records.Count > 0)
                {
                    ProcessId = (from row in Records
                                 where row.categorid== categoryid && row.Name.ToUpper().Trim() == Processname.ToUpper().Trim()
                                 select row.ID).FirstOrDefault();
                }
                return Convert.ToInt32(ProcessId);
            }
        }


      
        public bool ComplainceExists(int ComplianceID,int actid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.Compliance>)Cache["IMAComplianceListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ActID==actid && row.ID == ComplianceID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public bool ActExists(int ActID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.Act>)Cache["IMAActListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == ActID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public string CheckISCheckListOrComplianceExists(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.Compliance>)Cache["IMAComplianceListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == ComplianceID
                                 select row.ComplianceType).FirstOrDefault();
                    if (query == 1)
                    {
                        return "Y";
                    }
                    else
                    {
                        return "N";
                    }
                }
                else
                {
                    return "";
                }
            }
        }
        
        #endregion       
    }
}