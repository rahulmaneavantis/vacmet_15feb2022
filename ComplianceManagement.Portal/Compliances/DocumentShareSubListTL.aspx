﻿<%@ Page Title="" Language="C#" MasterPageFile="~/RLCSCompliance.master" AutoEventWireup="true" CodeBehind="DocumentShareSubListTL.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.DocumentShareSubListTL" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="DropDownListChosen" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <link href="../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <link href="../NewCSS/Document_Drive_Style.css" rel="stylesheet" />
    <script src="../Newjs/bootstrap-tagsinput.js"></script>
    <link href="../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            FetchUSerDetail();
            $("button.multiselect").on("click", function () {
                $(this).parent().addClass("open");
            });

            $('input[data-role="tagsinput"]').tagsinput();
           
        });

        $(document).ready(function () {
            $(".notification-row > ul > .dropdown").click(function () { $('.notification-row > ul > .dropdown').addClass('open') });
        });

        //$(document).ready(function () {
        //    $('ul.nav.pull-right.top-menu > .dropdown').click(function () { $(this).addClass('open') });
        //    fhead('My Documents /Critical Document(s)');
        //    setactivemenu('leftdocumentsmenu');
        //});
          $(document).ready(function () {         
             fhead('My Documents / Other Critical Documents');
              setactivemenu('DocumentShareListNew');
              //fmaters()
        });

        function FetchUSerDetail() {
            $('[id*=lstBoxUser]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 2,
                buttonWidth: '100%',
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: 'Type to Search for User..',
                nSelectedText: ' - Owner(s) selected',
            });
        }

        function BindDateControl() {
            $(function () {
                $('input[id*=TxtBxFileCreated]').datepicker({
                    dateFormat: 'mm-dd-yy',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                    maxDate: new Date()
                });
            });
        }

        //function closedUploadfilepopup() {
        //    alert();
        //    $('#divOpenNewDocumentPopup').modal('hide');
        //}

        function OpenNewFolderPopup() {
            debugger;
            
            $('#divOpenNewFolderPopup').modal('show');
        }
        function OpenNewDocumentPopup() {
            debugger;
            
            $('#divOpenNewDocumentPopup').modal('show');
        }
        function OpenNewPermissionPopup() {
            $('input[data-role="tagsinput"]').tagsinput();
            $('#divOpenPermissionPopup').modal('show');
        }
        function OpenNewInfoPopup() {
            BindDateControl();
            $('#divOpenNewInformationPopup').modal('show');
            $('input[data-role="tagsinput"]').tagsinput();
        }

        function OpenAuditChecklistPopup() {
            $('#divAuditChecklistPopup').modal('show');
            $('input[data-role="tagsinput"]').tagsinput();
        }

        
        function OpenDocumentViewPopup() {
            $('#divOpenDocumentViewPopup').modal('show');
        }

        function lnkNewTest() {
             
            $('#lnkDropdown').addClass('open');
        }

    </script>

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        .color-lable {
            color: #2b2b2b;
        }

        .folder-size {
            height: 7%;
            width: 9%;
            float: left;
            margin-left: 30%;
        }

        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        .bootstrap-tagsinput {
        display:block;
        }
        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .lnklist {
            display: inline-flex;
        }

        .table > tbody > tr > th {
            font-weight: bold;
        }

        .btn-default:active, .btn-default:hover {
            background-color: #ddd;
            border: none;
        }

        .btn {
            border: none;
        }

        .clsROWgrid {
            cursor: pointer;
        }
.child .dropdown .dropdown-menu{ display:none;}
.child .dropdown.open .dropdown-menu{display:block;}
.child .btn-group .dropdown-menu{ display:none;}

.child .btn-group.open .dropdown-menu{display:block;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upPromotorList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <section class="panel">
  <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs">
                                         
                                        <li class="" id="li1" runat="server">
                                            <asp:LinkButton ID="LinkButton1"  PostBackUrl="/ComplianceDocument/ComplianceDocumentList.aspx"   runat="server" Style="padding-bottom:0px;display:none;">Compliance Document</asp:LinkButton>                                           
                                        </li>
                                         
                                        <li class="active"  id="li2" runat="server">
                                            <asp:LinkButton ID="LinkButton2" runat="server" Style="padding-bottom:0px;display:none;">Other Critical Document</asp:LinkButton>                                        
                                        </li>
                                         
                                    </ul>
                                </header>
                            <div style="clear:both;height:15px;"></div>

                           <div class="col-lg-12 col-md-12" style="padding-left:0px;">                            
                           
                                      <div class="col-md-10" style="padding-left:0px;">
                                               <asp:LinkButton Text='My Docs' ID="lnkMyDrive" OnClick="lnkMyDrive_Click" runat="server" Style="text-decoration: none; color: Black; font-size: 18px; font-weight:200;" class="btn btn-default" />&nbsp;>&nbsp;
                                    <asp:DataList runat="server" CssClass="lnklist" ID="dlBreadcrumb" OnItemCommand="dlBreadcrumb_ItemCommand"
                                        RepeatLayout="Flow" RepeatDirection="Horizontal">
                                        <ItemTemplate>
                                            <asp:LinkButton Text='<%# Eval("Name") %>' CommandArgument='<%# Eval("ID") %>' CommandName="ITEM_CLICKED"
                                                runat="server" Style="text-decoration: none; color: Black; font-size: 18px; font-weight:200;" class="btn btn-default" />
                                        </ItemTemplate>                                   
                                        <SeparatorStyle Font-Size="16"  />
                                        <SeparatorTemplate>
                                            &gt;
                                        </SeparatorTemplate>
                                    </asp:DataList>
                                    </div> 
                                 </div>  

                        <div class="col-lg-12 col-md-12" style="padding: 0;">
                           <%-- xxx--%>
                              <div class="col-md-12" style="float: left;color:#000;margin: 10px 0 5px 0;padding: 0;">
                                    <div class="col-md-4" style="padding-left:0px;">   
                                 <asp:TextBox runat="server" ID="tbxFilter" CssClass="form-control" MaxLength="50"
                                       PlaceHolder="Type to Search Files, folders(e.g. tags, process and subprocess)"  AutoPostBack="true" OnTextChanged="tbxFilter_TextChanged" />
                            </div> 
                                  <div class="col-md-6 deleteandsharediv" style="float: left;text-align: right;display:none;">
                               <img src="../images/sharedrive.png" style="width: 45px;  height: 30px;cursor:pointer;margin-right:5px" class="sharedrive" data-toggle="tooltip" data-placement="bottom" data-original-title="Click to Share Document" />  
                                <img src="../images/downloaddoc.png" data-toggle="tooltip" data-placement="bottom" data-original-title="Click to Download" style="width: 45px;  height: 30px;cursor:pointer;margin-right:5px" class="viewdrive"  />
                                <img src="../images/sharedrive.png" style="width: 45px;  height: 30px;cursor:pointer;margin-right:5px" class="subsharedrive" data-toggle="tooltip" data-placement="bottom" data-original-title="Click to Sub Share Document" />  
                                <img src="../images/editdoc.png" style="width: 45px;  height: 30px;cursor:pointer;margin-right:5px" class="editdrive"  data-toggle="tooltip" data-placement="bottom" data-original-title="Click to edit details"  />
                                <img  class="deletedrive" data-toggle="tooltip" data-placement="bottom" data-original-title="Click to delete"   src="../images/deletedrive.png" style="width: 45px;  height: 30px;cursor:pointer;margin-right:5px" />
                               <% if (user_Roles.Contains("MGMT") && user_Audit==true ) {%>  
                                       <img  class="linkaudit" data-toggle="tooltip" data-placement="bottom" data-original-title="Link to Audit"   src="../images/attachment1.PNG" style="width:40px;  height: 25px;cursor:pointer;margin-right:5px" />
                               <%}%>  
                              </div>

                                    <div class="col-md-2 child"  style="float:right;">
                                         <li id="lnkDropdown"  class="dropdown" style="float:right;list-style:none">
                                           <button class="dropdown-toggle" onclick="lnkNewTest();" style="width: 95px;height: 42px;background-image: url(/images/Addnewicon.png);   background-color: white;    border: none;" type="button" id="menu1" data-toggle="dropdown">
                             </button>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1" style="margin: 0px -35px 0;">
                                         <li role="presentation"><a role="menuitem" tabindex="-1" >
                                            <asp:LinkButton ID="lnkAddNewFolder" runat="server" OnClientClick="OpenNewFolderPopup();" style="color:#666" >
                                             New Folder
                                            </asp:LinkButton>
                                         </a></li>
                                      <li role="presentation"><a role="menuitem" tabindex="-1" >
                                           <asp:LinkButton ID="lnkAddNewDocument" runat="server" OnClientClick="OpenNewDocumentPopup();" style="color:#666">
                                            New File </asp:LinkButton>
                                        </a></li>
                                
                                    </ul>
                                  </li>
                                    </div>                               
                                </div>                     
                        </div>
                        

                        <div class="row">
                            <asp:GridView runat="server" ID="grdFolderDetail" AutoGenerateColumns="false" AllowSorting="true"
                                ShowHeaderWhenEmpty="true"
                                OnRowCommand="grdFolderDetail_RowCommand" OnRowDataBound="grdFolderDetail_RowDataBound"
                                PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table"
                                GridLines="none" Width="100%" DataKeyNames="ID">
                                <Columns>                               
                                    <asp:TemplateField HeaderText="Sr" Visible="false">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                            <asp:TextBox ID="tbxIDFile" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("ID") %>'></asp:TextBox>
                                            <asp:TextBox ID="tbxCreatedByValue" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("Createdby") %>'></asp:TextBox>
                                            <asp:TextBox ID="tbxTypeValue" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("Type") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                
                                <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="30%" ItemStyle-Width="30%">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px">   
                                             <asp:ImageButton ID="LinkButton1" runat="server" Width="16px" CssClass="foldertogo" ToolTip="Go to sub-folder"
                                             ImageUrl="~/Images/Folder_Icon.png"
                                            data-toggle="tooltip" CommandName="Goto_Subfolder" Visible='<%# Eval("Type").ToString() != "File" ? true:false%>'
                                            CommandArgument='<%# Eval("ID") %>'></asp:ImageButton>     
                                             <asp:ImageButton ID="ImgfileType" runat="server" Width="16px" 
                                            Visible='<%# Eval("Type").ToString() != "Folder" ? true:false%>'></asp:ImageButton>                                   
                                            <asp:Label ID="lblName" runat="server" data-toggle="tooltip" data-placement="bottom" 
                                                Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Owner" HeaderStyle-Width="30%" ItemStyle-Width="30%">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" CssClass="ownerdoc" Text='<%# ShowUserName(Convert.ToString(Eval("Createdby"))) %>' ToolTip='<%# ShowUserName(Convert.ToString(Eval("Createdby"))) %>'></asp:Label>
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" style="display:none;" CssClass="ownerdocaccesss" Text='<%# UserAccess(Convert.ToString(Eval("ID")),Eval("Type").ToString()) %>' ToolTip='<%# UserAccess(Convert.ToString(Eval("Createdby")),Eval("Type").ToString()) %>'></asp:Label>
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" style="display:none;" CssClass="ownerdocTypeaccesss" Text='<%# Eval("Type").ToString() %>' ToolTip='<%# Eval("Type").ToString() %>'></asp:Label>                                                                                          
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Modified" HeaderStyle-Width="30%" ItemStyle-Width="30%">
                                        <ItemTemplate>
                                            <div style="overflow: hidden;  width: 150px">
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CreatedOn") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Size" HeaderStyle-Width="30%" ItemStyle-Width="30%">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# ShowFileSize(Convert.ToInt64(Eval("ID")),Convert.ToString(Eval("Type"))) %>'></asp:Label>                                             
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Type" HeaderStyle-Width="30%" ItemStyle-Width="30%">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# ShowFileType(Convert.ToString(Eval("Name")),Convert.ToString(Eval("Type"))) %>'></asp:Label>                                             
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField  ItemStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="LnkShare" runat="server" CssClass="sharef"  Style="float: left; padding: 0px 2px 0px 0px;display:none;" ImageUrl="~/Images/reset_password_new.png" CommandArgument='<%# Eval("ID") +","+ Eval("Type") %>' CommandName="ShareFile" data-toggle="tooltip" data-placement="bottom" ToolTip="Click to Share File"></asp:ImageButton>
                                            <asp:ImageButton ID="LnkSubShare" runat="server" CssClass="subsharef"  Style="float: left; padding: 0px 2px 0px 0px;display:none;" ImageUrl="~/Images/reset_password_new.png" CommandArgument='<%# Eval("ID") +","+ Eval("Type") %>' CommandName="SubShareFile" data-toggle="tooltip" data-placement="bottom" ToolTip="Click to Sub Share File"></asp:ImageButton>
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" Style="float: left; padding: 0px 2px 0px 0px;">
                                                <ContentTemplate>
                                                    <asp:ImageButton ID="lnkViewDoc" runat="server" style="display:none;"  CssClass="viewf"  ImageUrl="~/Images/view-icon-1.png" Visible='<%# Eval("Type").ToString() != "File" ? false:true  %>'
                                                        data-toggle="tooltip" data-placement="bottom" ToolTip="Click to View File" CommandArgument='<%# Eval("ID") %>' CommandName="DownloadDoc"></asp:ImageButton>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="lnkViewDoc" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                             <asp:ImageButton ID="lnkinfoUpload" style="display:none;" CssClass="Infof" runat="server"
                                                 Visible="true" ImageUrl="~/Images/delete_icon_new.png"
                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Click to Show Info" CommandArgument='<%# Eval("ID") +","+ Eval("Type") %>'
                                                CommandName="InfoDoc"></asp:ImageButton>
                                             <asp:ImageButton ID="Imgbtnview" style="display:none;" CssClass="Viewf" runat="server"
                                                 Visible="true" ImageUrl="~/Images/delete_icon_new.png"
                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Click to Show Info" CommandArgument='<%# Eval("ID") +","+ Eval("Type") %>'
                                                CommandName="ViewDoc"></asp:ImageButton>
                                            <asp:ImageButton ID="lnkDeleteUpload" style="display:none;" CssClass="deletef" runat="server"
                                                 Visible="true" ImageUrl="~/Images/delete_icon_new.png"
                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Click to Delete Contract" CommandArgument='<%# Eval("ID") +","+ Eval("Type") %>'
                                                CommandName="DeleteDoc"></asp:ImageButton>
                                               <%-- OnClientClick="if (!confirm('Are you sure you want delete?')){ alert(1);$('#updateProgress').hide(); return false}"></asp:ImageButton>--%>
                                              <asp:ImageButton ID="lnkAuditDetail" style="display:none;" CssClass="InfoflinkAudit" runat="server"
                                                 Visible="true" ImageUrl="~/Images/delete_icon_new.png"
                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Click to Show Info" CommandArgument='<%# Eval("ID") +","+ Eval("Type") %>'
                                                CommandName="linkAuditDetail"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                <PagerSettings Visible="false" />
                                <PagerTemplate>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                            </asp:GridView>

                        </div>

                        <div class="col-md-12 colpadding0">

                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px; ">
                                <div class="col-md-2 colpadding0" style="   margin-right: 10px;">
                                    <p style="color: #999; margin-top: 5px;margin-right: 5px;">Show </p>
                                </div>
                                <div class="col-md-6 colpadding0">
                                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                        <asp:ListItem Text="5"  />
                                        <asp:ListItem Text="10" Selected="True" />
                                        <asp:ListItem Text="20" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-8 colpadding0 entrycount" style="margin-top: 5px;">
                            </div>
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-3">
                                </div>                            
                                <div class="col-md-2 colpadding0 table-paging-text" style="width: 25%;">
                                    <p>
                                        Page                                       
                                    </p>
                                </div>
                                <div class="col-md-3 colpadding0">

                                    <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" AllowSingleDeselect="false"
                                        class="form-control m-bot15" Height="30px" OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">
                                    </asp:DropDownListChosen>
                                </div>
                            </div>
                        </div>

                        <div class=" col-md-12 colpadding0 entrycount" style="margin-top: 5px;">
                        <div class="col-md-6 colpadding0" style="float: right">
                            <div class="table-paging" style="margin-bottom: 10px;">
                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                            </div>
                        </div>
                        <div class="col-md-6 colpadding0 entrycount" style="margin-top: 5px;">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p>
                                            <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--</section>--%>
                   </section>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="divOpenNewFolderPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p5" style="width: 50%;padding-top: 142px;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        New Folder</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <asp:UpdatePanel ID="upMailDocument" runat="server">
                        <ContentTemplate>
                            <div class="row">
                              <div class="col-md-12 alert alert-block alert-success fade in" runat="server" visible="false" id="divsuccessmsgaCTemSec">
                                <asp:Label runat="server" ID="successmsgaCTemSec"></asp:Label>
                            </div>
                                <div class="form-group required col-md-12">
                                    <asp:ValidationSummary ID="FolderValidation" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="vsFolderDocumentValidationGroup" />
                                    <asp:CustomValidator ID="cvMailDocument" runat="server" EnableClientScript="False"
                                        ValidationGroup="vsFolderDocumentValidationGroup" Display="None" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <asp:TextBox runat="server" ID="txtFolderName" CssClass="form-control" />
                                    <asp:RequiredFieldValidator ID="Foldermsg" ErrorMessage="Required Name"
                                        ControlToValidate="txtFolderName" runat="server" ValidationGroup="vsFolderDocumentValidationGroup" Display="None" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <asp:Button Text="Create" runat="server" ID="btnCreateFolder" CssClass="btn btn-search"
                                        OnClick="btnCreate_Click" ValidationGroup="vsFolderDocumentValidationGroup"></asp:Button>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divOpenNewDocumentPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p5" style="width: 50%;padding-top: 142px;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Add File</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <asp:ValidationSummary ID="vsDocumentValidSummary" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="vsUploadDocumentValidationGroup" />
                                    <asp:CustomValidator ID="vsDocumentValid" runat="server" EnableClientScript="False"
                                        ValidationGroup="vsUploadDocumentValidationGroup" Display="None" />
                                </div>
                            </div>

                            <%--  <div class="row">
                                <div class="form-group required col-md-12">
                                    <label for="TxtDocHeader" class="color-lable">Header</label>
                                    <asp:TextBox runat="server" ID="TxtDocHeader" DataPlaceHolder="Document Header" CssClass="form-control" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Required Header"
                                        ControlToValidate="TxtDocHeader" runat="server" ValidationGroup="vsUploadDocumentValidationGroup" Display="None" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <label for="TxtDescription" class="color-lable">Description</label>
                                    <asp:TextBox runat="server" ID="TxtDescription" DataPlaceHolder="Document Description" CssClass="form-control" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Required Description"
                                        ControlToValidate="TxtDescription" runat="server" ValidationGroup="vsUploadDocumentValidationGroup" Display="None" />
                                </div>
                            </div>--%>
                            <div class="row">
                                <div class="form-group color-lable required col-md-12">
                                    <asp:FileUpload ID="ContractFileUpload" runat="server" AllowMultiple="true" CssClass="fileUploadClass" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="File required to upload"
                                        ControlToValidate="ContractFileUpload" runat="server" ValidationGroup="vsUploadDocumentValidationGroup" Display="None" />
                                </div>
                            </div>
                            <div class="row" style="display: none;">
                                <div class="form-group required col-md-12">
                                    <label for="ddlPermission" class="color-lable">Permission</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlPermission" DataPlaceHolder="Select Permission"
                                        AllowSingleDeselect="false" DisableSearchThreshold="5" CssClass="form-control" Width="100%">
                                        <asp:ListItem Text="View" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Write" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="View & Write" Value="3"></asp:ListItem>
                                    </asp:DropDownListChosen>
                                </div>
                            </div>
                            <%--  <div class="row">
                                <div class="form-group required col-md-12">
                                    <label for="TxtOthers" class="color-lable">Others</label>
                                    <asp:TextBox runat="server" ID="TxtOthers" DataPlaceHolder="Comment for File" CssClass="form-control" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Required Name"
                                        ControlToValidate="TxtOthers" runat="server" ValidationGroup="vsUploadDocumentValidationGroup" Display="None" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <label for="TxtProcess" class="color-lable">Process</label>
                                    <asp:TextBox runat="server" ID="TxtProcess" DataPlaceHolder="Process" CssClass="form-control" />                                 
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Required Process"
                                        ControlToValidate="TxtProcess" runat="server" ValidationGroup="vsUploadDocumentValidationGroup" Display="None" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <label for="TxtSubProcess" class="color-lable">Sub Process</label>
                                     <asp:TextBox runat="server" ID="TxtSubProcess" DataPlaceHolder="Sub Process" CssClass="form-control" />                                  
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Required Sub Process"
                                        ControlToValidate="TxtSubProcess" runat="server" ValidationGroup="vsUploadDocumentValidationGroup" Display="None" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <label for="TxtVertical" class="color-lable">Vertical</label>                                  
                                     <asp:TextBox runat="server" ID="TxtVertical" DataPlaceHolder="Vertical" CssClass="form-control" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Required Vertical"
                                        ControlToValidate="TxtVertical" runat="server" ValidationGroup="vsUploadDocumentValidationGroup" Display="None" />
                                </div>
                            </div>--%>

                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <asp:Button Text="Upload Document" runat="server" ID="btnUploadDoc" CssClass="btn btn-primary"
                                        OnClick="btnUploadFile_Click" ValidationGroup="vsUploadDocumentValidationGroup"></asp:Button>
                                </div>
                            </div>

                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnUploadDoc" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divOpenNewInformationPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p5" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Details</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-md-12 alert alert-block alert-success fade in" runat="server" visible="false" id="divEditinformation">
                                    <asp:Label runat="server" ID="successmsg"></asp:Label>
                                </div>
                                <div class="form-group required col-md-12">
                                    <asp:ValidationSummary ID="InfoValidationSummary" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="vsUploadinformationValidationGroup" />
                                    <asp:CustomValidator ID="InfoValidator" runat="server" EnableClientScript="False"
                                        ValidationGroup="vsUploadinformationValidationGroup" Display="None" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <div class="form-group required col-md-6">
                                        <label for="TxtDocName" class="color-lable">Name</label>
                                        <asp:TextBox runat="server" ID="TxtDocName" DataPlaceHolder="Document Name" CssClass="form-control" ReadOnly="true" />
                                    </div>

                                    <div class="form-group required col-md-6">
                                        <label for="TxtDocType" class="color-lable">Type</label>
                                        <asp:TextBox runat="server" ID="TxtDocType" DataPlaceHolder="Document Type" CssClass="form-control" ReadOnly="true" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <div class="form-group required col-md-6">
                                        <label for="TxtDocCreatedby" class="color-lable">Created By</label>
                                        <asp:TextBox runat="server" ID="TxtDocCreatedby" DataPlaceHolder="Document Header" CssClass="form-control" ReadOnly="true" />
                                    </div>

                                    <div class="form-group required col-md-6">
                                        <label for="TxtDocCreatedon" class="color-lable">Created Date</label>
                                        <asp:TextBox runat="server" ID="TxtDocCreatedon" DataPlaceHolder="Document Header" CssClass="form-control" ReadOnly="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <div class="form-group required col-md-6">
                                        <label for="TxtBxFileCreated" class="color-lable">File Created Date</label>
                                        <asp:TextBox runat="server" ID="TxtBxFileCreated" DataPlaceHolder="DD-MM-YYYY" CssClass="form-control" />
                                    </div>

                                    <div class="form-group required col-md-6">
                                        <label for="TxtDocModifiedDate" class="color-lable">Modified Date</label>
                                        <asp:TextBox runat="server" ID="TxtDocModifiedDate" DataPlaceHolder="Document Header" CssClass="form-control" ReadOnly="true" />
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <div class="form-group required col-md-6">
                                        <label runat="server" id="LblHeader" for="TxtDocHeader" class="color-lable">Header</label>
                                        <label style="color:red;">*</label>
                                        <asp:TextBox runat="server" ID="TxtDocHeader" DataPlaceHolder="Document Header" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ErrorMessage="Required Header"
                                            ControlToValidate="TxtDocHeader" runat="server" ValidationGroup="vsUploadinformationValidationGroup" Display="None" />
                                    </div>

                                    <div class="form-group required col-md-6">
                                        <label runat="server" id="LblDescription" for="TxtDescription" class="color-lable">Description</label>
                                        <label style="color:red;">*</label>
                                        <asp:TextBox runat="server" ID="TxtDescription" DataPlaceHolder="Document Description" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ErrorMessage="Required Description"
                                            ControlToValidate="TxtDescription" runat="server" ValidationGroup="vsUploadinformationValidationGroup" Display="None" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <div class="form-group required col-md-6">
                                        <label id="lblOthers" runat="server" for="TxtOthers" class="color-lable">Others</label>
                                        <label style="color:red;">*</label>
                                        <asp:TextBox runat="server" ID="TxtOthers" DataPlaceHolder="Comment for File" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="Required Others"
                                            ControlToValidate="TxtOthers" runat="server" ValidationGroup="vsUploadinformationValidationGroup" Display="None" />
                                    </div>

                                    <div class="form-group required col-md-6">
                                        <label id="lblProcess" runat="server" for="TxtProcess" class="color-lable">Process</label>
                                        <label style="color:red;">*</label>
                                        <asp:TextBox runat="server" ID="TxtProcess" DataPlaceHolder="Process" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ErrorMessage="Required Process"
                                            ControlToValidate="TxtProcess" runat="server" ValidationGroup="vsUploadinformationValidationGroup" Display="None" />
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <div class="form-group required col-md-6">
                                        <label id="lblSubProcess" runat="server" for="TxtSubProcess" class="color-lable">Sub Process</label>
                                        <label style="color:red;">*</label>
                                        <asp:TextBox runat="server" ID="TxtSubProcess" DataPlaceHolder="Sub Process" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ErrorMessage="Required Sub Process"
                                            ControlToValidate="TxtSubProcess" runat="server" ValidationGroup="vsUploadinformationValidationGroup" Display="None" />
                                    </div>

                                    <div class="form-group required col-md-6">
                                        <label id="lblVertical" runat="server" for="TxtVertical" class="color-lable">Vertical</label>
                                        <label style="color:red;">*</label>
                                        <asp:TextBox runat="server" ID="TxtVertical" DataPlaceHolder="Vertical" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="Required Vertical"
                                            ControlToValidate="TxtVertical" runat="server" ValidationGroup="vsUploadinformationValidationGroup" Display="None" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <div class="form-group required col-md-6">
                                        <label id="lblLocation" runat="server" for="TxtLocation" class="color-lable">Location</label>
                                        <label style="color:red;">*</label>
                                        <asp:TextBox runat="server" ID="TxtLocation" DataPlaceHolder="Vertical" CssClass="form-control" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Required Location"
                                            ControlToValidate="TxtLocation" runat="server" ValidationGroup="vsUploadinformationValidationGroup" Display="None" />
                                    </div>

                                    <div class="form-group required col-md-6">
                                        <label id="Label1" runat="server" for="txtDocTags" class="color-lable">Tags</label>
                                        <asp:TextBox runat="server" ID="txtDocTags" CssClass="form-control" ClientIDMode="Static" data-role="tagsinput"
                                            placeHolder="Document related tags" autocomplete="off" Width="100%" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group required col-md-12">
                                    <div class="form-group required col-md-6">
                                        <asp:Button Text="Update Information" runat="server" ID="btnUpdateInfo" CssClass="btn btn-search"
                                            OnClick="btnUpdateInfo_Click" ValidationGroup="vsUploadinformationValidationGroup"></asp:Button>
                                    </div>
                                </div>
                            </div>

                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnUploadDoc" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

       <div class="modal fade" id="divAuditChecklistPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p5" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Link Document</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;height: 470px;">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-md-12 alert alert-block alert-success fade in" runat="server" visible="false" id="div1">
                                    <asp:Label runat="server" ID="Label2"></asp:Label>
                                </div>
                                <div class="form-group required col-md-12">
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="vsUploadinformationValidationGroup" />
                                    <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                        ValidationGroup="vsUploadinformationValidationGroup" Display="None" />
                                </div>
                            </div>

                            <div>
                                <asp:Panel ID="pnl" ScrollBars="Vertical" Height="400px" runat="server">
                                    <asp:GridView runat="server" ID="grdChecklist" AutoGenerateColumns="false" GridLines="None" ShowHeaderWhenEmpty="true"
                                        AllowSorting="true" AllowPaging="false" DataKeyNames="ChecklistID" CssClass="table" Width="100%">  <%--OnRowDataBound="grdChecklist_RowDataBound" --%>
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="checkAll" runat="server" AutoPostBack="true" OnCheckedChanged="checkAll_CheckedChanged" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Clause" ItemStyle-Width="10px">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 110px;">
                                                        <asp:Label ID="lblSrNo" runat="server" Text='<%# Eval("SrNo")%>' ToolTip='<%# Eval("SrNo") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Checklist Name">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;width: 150px;">
                                                        <asp:Label ID="lblChecklistName" runat="server" Text='<%# Eval("ChecklistName")%>'></asp:Label>
                                                        <asp:Label ID="lblVendorID" Visible="false" runat="server" Text='<%# Eval("VendorID")%>'></asp:Label>
                                                        <asp:Label ID="lblChecklistID" Visible="false" runat="server" Text='<%# Eval("ChecklistID")%>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Vendor Name" ItemStyle-Width="15px">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 170px;">
                                                        <asp:Label ID="lblVendorName" runat="server" Text='<%# Eval("VendorName")%>' ToolTip='<%# Eval("VendorName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Right" />
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <PagerTemplate>
                                            <table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            No Records Found.
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </asp:Panel>
                            </div>

                            <div>
                                <asp:Button Text="Save" Style="margin-left: 10px;" runat="server" ID="btnSaveChecklistMapping" OnClick="btnSaveChecklistMapping_Click" CssClass="btn btn-search" />
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divOpenPermissionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p5" style="width: 50%;padding-top: 62px;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Share With Others
                    </label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <div class="row">
                        <div class="form-group required col-md-12">
                            <asp:ValidationSummary ID="vsPermission" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="vsPermissionValidationGroup" />
                            <asp:CustomValidator ID="vsPermissionSet" runat="server" EnableClientScript="False"
                                ValidationGroup="vsPermissionValidationGroup" Display="None" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group required col-md-12" style="display: block;">
                            <label for="ddlPermission1" class="color-lable">Permission</label>
                            <asp:DropDownListChosen runat="server" ID="ddlPermission1" DataPlaceHolder="Select Permission"
                                AllowSingleDeselect="false" DisableSearchThreshold="5" CssClass="form-control" Width="100%">
                                 <asp:ListItem Text="View" Value="4"></asp:ListItem>
                                <asp:ListItem Text="View & Download" Value="1"></asp:ListItem>
                                <%--<asp:ListItem Text="Write" Value="2"></asp:ListItem>--%>
                                <asp:ListItem Text="Full Control" Value="3"></asp:ListItem>
                            </asp:DropDownListChosen>
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group required col-md-12 child">
                            <label for="lstBoxUser" class="color-lable">People</label>
                            <asp:ListBox ID="lstBoxUser" CssClass="form-control" runat="server" SelectionMode="Multiple"></asp:ListBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group required col-md-12">
                            <asp:Button Text="Done" ID="btnPermission" runat="server" OnClick="btnPermission_Click" CssClass="btn btn-search"></asp:Button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group required col-md-12" style="overflow-x: auto;max-height: 212px;">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:Repeater ID="myRepeater" runat="server" OnItemCommand="myRepeater_ItemCommand">
                                        <HeaderTemplate>
                                            <div style="font-weight: bold; color: black">Shared with Users</div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="form-group required col-md-12" style="padding-left: 0px;">
                                                    <div class="form-group required col-md-6" style="padding-left: 0px;">
                                                        <asp:Label ID="myLabel" runat="server" Style="color: black; float: left" Text='<%# Eval("UserName") %>' />
                                                    </div>
                                                    <div class="form-group required col-md-1" style="padding-left: 0px;">
                                                        <asp:ImageButton ID="LnkDeletShare" runat="server" CommandName="RemoveShare" CommandArgument='<%# Eval("UserPermissionFileId") +","+ Eval("UserId") +","+ Eval("FileType") %>' Style="float: left; padding: 0px 2px 0px 0px;" ImageUrl="~/Images/delete_icon_new.png" data-toggle="tooltip" data-placement="bottom" ToolTip="Click to UnShare File"></asp:ImageButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="myRepeater" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="divOpenDocumentViewPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p5" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Document View
                    </label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 70%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="height: 570px;">
                    <div style="width: 100%;">
                        <div style="float: left; width: 100%">
                            <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 530px; width: 100%;">
                                <iframe src="about:blank" id="docViewerStatutory" runat="server" width="100%" height="510px"></iframe>
                            </fieldset>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        function fopendocfileReview(file) {
            $('#divViewDocument').modal('show');
            $('#ContentPlaceHolder1_docViewerStatutory').attr('src', "../docviewer.aspx?docurl=" + file);
        }
        function ShowViewDocument() {
            $('#divViewDocument').modal('show');
            return true;
        };
        var clickdownload = true;
        checkbuttonclick = true;
        $('.clsROWgrid').click(function () {
            debugger;
           
            setInterval(function () { }, 100);

            var owner = $(this).find('.ownerdoc');
            var owneraccsess = $(this).find('.ownerdocaccesss');
            var fileTypeowneraccsess = $(this).find('.ownerdocTypeaccesss');            
            if ($(this).css('background-color') == '#f8f8f8' || $(this).css('background-color') == 'rgb(248, 248, 248)') {
                var fclick = $(this).find('.foldertogo');
                if (  checkbuttonclick == true && $(this).find('.foldertogo').attr('id') != null && $(this).find('.foldertogo').attr('id') != undefined) {
                    document.getElementById($(fclick).attr('id')).click();                    
                }
                else {
                    if (clickdownload == true) {
                        var Viwerdrive = $('.selectedDocs').find('.Viewf');
                        document.getElementById($(Viwerdrive).attr('id')).click();
                    }
                    clickdownload = true;
                }
            } else {
                $('.viewdrive').hide();
                $('.editdrive').hide();
                $('.sharedrive').hide();
                $('.deletedrive').hide();
                $('.linkaudit').hide();
                $('.subsharedrive').hide();
                if ($(owner).text() == 'me') {
                    if ($(this).find('.foldertogo').attr('id') == null && $(this).find('.foldertogo').attr('id') == undefined) {
                        $('.viewdrive').show();
                        $('.editdrive').show();
                    }
                    $('.deletedrive').show();
                    $('.linkaudit').show();
                    $('.sharedrive').show();
                    $('.subsharedrive').hide();
                } else {
                    if ($(fileTypeowneraccsess).text() == 'File') {
                        if ($(this).find('.foldertogo').attr('id') == null && $(this).find('.foldertogo').attr('id') == undefined) {
                            if ($(owneraccsess).text() == 'Access') {
                                $('.sharedrive').show();
                                //$('.subsharedrive').show();
                                $('.viewdrive').show();
                                $('.editdrive').show();
                            }
                            else if ($(owneraccsess).text() == 'IsreadAccess') {
                                $('.viewdrive').show();
                            }
                            else {
                                $('.subsharedrive').hide();
                            }
                            //$('.sharedrive').show();
                        }
                    }
                    else
                    {
                        if ($(owneraccsess).text() == 'Access') {
                            $('.sharedrive').show();
                            //$('.subsharedrive').show();
                        }
                        else {
                            $('.subsharedrive').hide();
                        }
                    }
                }
                $('.clsROWgrid').css('background-color', 'white');
                $('.clsROWgrid').removeClass('selectedDocs');
                $('.deleteandsharediv').show();
                $(this).css('background-color', '#f8f8f8');
                $(this).addClass('selectedDocs');
                checkbuttonclick = true;
            }
        });
        $('.sharedrive').on('click', function (event) {
         
            checkbuttonclick = false;
            var shareddrive = $('.selectedDocs').find('.sharef');
            document.getElementById($(shareddrive).attr('id')).click();
            event.stopPropagation();
        });
        $('.subsharedrive').on('click', function (event) {
          
            checkbuttonclick = false;
            var shareddrive = $('.selectedDocs').find('.subsharef');
            document.getElementById($(shareddrive).attr('id')).click();
            event.stopPropagation();
        });

        $('.viewdrive').on('click', function (event) {            
            clickdownload = false;
            checkbuttonclick = false;
            var shareddrive = $('.selectedDocs').find('.viewf');
            document.getElementById($(shareddrive).attr('id')).click();
            event.stopPropagation();
        });
        $('.deletedrive').on('click', function (event) {
            checkbuttonclick = false;

            var deletedrive = $('.selectedDocs').find('.deletef');
            var r = confirm('You sure you want to delete this folder, as all sub-folders and files related to this folder also get deleted?')
            if (r == true) {
                document.getElementById($(deletedrive).attr('id')).click();
            }
            event.stopPropagation();
        });
        $('.editdrive').on('click', function (event) {
            checkbuttonclick = false;
            var deletedrive = $('.selectedDocs').find('.Infof');
            document.getElementById($(deletedrive).attr('id')).click();
            event.stopPropagation();
        });
        $('.linkaudit').on('click', function (event) {
            checkbuttonclick = false;
            var deletedrive = $('.selectedDocs').find('.InfoflinkAudit');
            document.getElementById($(deletedrive).attr('id')).click();
            event.stopPropagation();
        });
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {
                $('.clsROWgrid').click(function () {
                    debugger;
                    var owner = $(this).find('.ownerdoc');
                    var owneraccsess = $(this).find('.ownerdocaccesss');
                    var fileTypeowneraccsess = $(this).find('.ownerdocTypeaccesss');
                    if ( ($(this).css('background-color') == '#f8f8f8' || $(this).css('background-color') == 'rgb(248, 248, 248)')) {
                        var fclick = $(this).find('.foldertogo');
                        if (checkbuttonclick == true && $(this).find('.foldertogo').attr('id') != null && $(this).find('.foldertogo').attr('id') != undefined) {
                            document.getElementById($(fclick).attr('id')).click();
                          
                        }
                        else {
                            if (clickdownload == true) {
                                var Viwerdrive = $('.selectedDocs').find('.Viewf');
                                document.getElementById($(Viwerdrive).attr('id')).click();
                            }
                            clickdownload = true;
                        }
                    } else {
                        $('.viewdrive').hide();
                        $('.editdrive').hide();
                        $('.subsharedrive').hide();
                        $('.sharedrive').hide();
                        $('.deletedrive').hide();
                        $('.linkaudit').hide();
                        if ($(owner).text() == 'me') {
                            if ($(this).find('.foldertogo').attr('id') == null && $(this).find('.foldertogo').attr('id') == undefined) {
                                $('.viewdrive').show();
                                $('.editdrive').show();
                            }
                            $('.deletedrive').show();
                            $('.linkaudit').show();
                            $('.sharedrive').show();
                        } else
                        {
                            if ($(fileTypeowneraccsess).text() == 'File') {
                                if ($(this).find('.foldertogo').attr('id') == null && $(this).find('.foldertogo').attr('id') == undefined) {
                                    // $('.viewdrive').show();
                                    if ($(owneraccsess).text() == 'Access') {
                                        $('.sharedrive').show();
                                        //$('.subsharedrive').show();
                                        $('.editdrive').show();
                                        $('.viewdrive').show();
                                    }
                                    else if ($(owneraccsess).text() == 'IsreadAccess') {
                                        $('.viewdrive').show();
                                    }
                                    else {
                                        $('.subsharedrive').hide();
                                    }
                                    //$('.sharedrive').show();
                                }
                            }
                            else {
                                if ($(owneraccsess).text() == 'Access') {
                                    $('.sharedrive').show();
                                    //$('.subsharedrive').show();
                                }
                                else {
                                    $('.subsharedrive').hide();
                                }
                            }
                        }
                        $('.clsROWgrid').css('background-color', 'white');
                        $('.clsROWgrid').removeClass('selectedDocs');
                        $('.deleteandsharediv').show();
                        $(this).css('background-color', '#f8f8f8');
                        $(this).addClass('selectedDocs');
                        checkbuttonclick = true;
                    }
                });
                $('.sharedrive').on('click', function (event) {
                    checkbuttonclick = false;
                    var shareddrive = $('.selectedDocs').find('.sharef');
                    document.getElementById($(shareddrive).attr('id')).click();
                    event.stopPropagation();
                });
                $('.viewdrive').on('click', function (event) {                    
                    clickdownload = false;
                    checkbuttonclick = false;
                    var shareddrive = $('.selectedDocs').find('.viewf');
                    document.getElementById($(shareddrive).attr('id')).click();
                    event.stopPropagation();
                });
                $('.deletedrive').on('click', function (event) {
                    checkbuttonclick = false;
                    var deletedrive = $('.selectedDocs').find('.deletef');
                    var r = confirm('You sure you want to delete this folder, as all sub-folders and files related to this folder also get deleted?')
                    if (r == true) {
                        document.getElementById($(deletedrive).attr('id')).click();
                    }
                    event.stopPropagation();
                });
                $('.editdrive').on('click', function (event) {
                    checkbuttonclick = false;
                    var deletedrive = $('.selectedDocs').find('.Infof');
                    document.getElementById($(deletedrive).attr('id')).click();
                    event.stopPropagation();
                });
                $('.linkaudit').on('click', function (event) {
                    checkbuttonclick = false;
                    var deletedrive = $('.selectedDocs').find('.InfoflinkAudit');
                    document.getElementById($(deletedrive).attr('id')).click();
                    event.stopPropagation();
                });
            });
        });
    </script>
</asp:Content>





