﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="MyReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.MyReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>

    <script id="delete-confirmation" type="text/x-kendo-template">
    <p style="width:600px;  word-wrap: break-word;" class="delete-message">Your search has resulted large records. As a result, the output can not be displayed on screen. An excel sheet is being prepered and will available for download in next 60 min.Do you want to receive it on your registered email ID as well?</p>
    <br/>
         
          <P>
          <div class="democlass">
    <button class="delete-confirm k-button" style="">Yes, Please send it on my email</button> 
    <button class="delete-cancel k-button" style="margin-left: 15px;">No, I will come back and download it</button>
          </div>
    </script>
     
    <style type="text/css">
        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .democlass 
        {
                margin-left: 72px;
                padding-bottom: 10px;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: inset 0 0 1px 1px #14699f;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px #d7dae0;
            box-shadow: inset 0 0 1px 1px white;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
            overflow: hidden;
        }

        html {
            color: #666666;
           /* font-size: 15px;*/
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
            font-size:12px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 12%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-multiselect-wrap .k-input {
            /*padding-top:6px;*/
            display: inherit !important;
        }
    </style>

    <title></title>

    <script type="text/x-kendo-template" id="template"> 
      
    </script>

    <script type="text/javascript">

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

        $(document).ready(function () {

            $("input").attr("autocomplete", "off");
            fhead('My Reports / Detailed Report');
            setactivemenu('Myreport');
            fmaters1();
            BindGrid();
            BindRequestedGrid();
            BindGridFilters();
            BindLocationDataSource();
            BindAdvancedGrid();
            BindAdvancedGridFilters();
           <%if (Falg == "AUD")%>
           <%{%>

         <%--   $('#Startdatepicker').val('<% =SDate%>');
            $('#Lastdatepicker').val('<% =LDate%>');--%>

              if ('<% =SDate%>' != '') {
                var from = "<% =SDate%>".split("-");
                var sdate = (from[1]) + "-" + (from[0]) + "-" + (from[2])
                var sdate1 = new Date(sdate);
                $("#Startdatepicker").data("kendoDatePicker").value(sdate1);
            }
            if ('<% =LDate%>' != '') {
                var to = "<% =LDate%>".split("-");
                var edate = (to[1]) + "-" + (to[0]) + "-" + (to[2])
                var edate1 = new Date(edate)
                $("#Lastdatepicker").data("kendoDatePicker").value(edate1);
            }
            $("#Startdatepicker").attr("readonly", true);
            $("#Lastdatepicker").attr("readonly", true);
            $("#dropdownPastData").attr("readonly", true);
            $("#dropdownFY").attr("readonly", true);

            //$('#dropdownPastData').val('All');
            //$('#dropdownlistTypePastdata').val('All');

            $("#dropdownPastData").data("kendoDropDownList").enable('false');
            $("#dropdownPastData").data("kendoDropDownList").value('All')

            $("#dropdownlistTypePastdata").data("kendoDropDownList").enable('false');
            $("#dropdownlistTypePastdata").data("kendoDropDownList").value('All')
           <%}%>

            $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
                settracknew('Detailed report', 'Action', 'Overview', '');
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenOverViewpup(item.ScheduledOnID, item.ComplianceInstanceID, item.ReportName);
                return true;
            });

            $(document).on("click", "#grid1 tbody tr .ob-overviewMain", function (e) {
                settracknew('Detailed report', 'Action', 'Overview', '');
                var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));

                OpenOverViewpupMain(item.ScheduledOnID, item.ComplianceInstanceID, item.ReportName);
                return true;
            });

            $("#dropdownlistUserRole").data("kendoDropDownList").value('<% =RoleKey%>');

            $("#dropdownlistUserRole1").data("kendoDropDownList").value('<% =RoleKey%>');

        });
        var TotalCount1 = 0;
        function BindAdvancedGrid() {

            var pastdata = 3;
            if ($("#dropdownPastData").val() != undefined && $("#dropdownPastData").val() != "") {
                pastdata = $("#dropdownPastData").val();
            }

            var roleflag ='<% =RoleKey%>';
            if ($("#dropdownlistUserRole1").val() != undefined && $("#dropdownlistUserRole1").val() != "") {
                roleflag = $("#dropdownlistUserRole1").val();
            }

            var Typedetails = [];
            if ($("#dropdownlistComplianceType1").data("kendoDropDownTree") != undefined && $("#dropdownlistComplianceType1").data("kendoDropDownTree") != null) {

                var list3 = $("#dropdownlistComplianceType1").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Typedetails.push(v);
                });
            }
            else {
                Typedetails = ['-1'];
            }

            var Riskdetails = [];
            if ($("#dropdownlistRisk1").data("kendoDropDownTree") != undefined && $("#dropdownlistRisk1").data("kendoDropDownTree") != null) {

                var list3 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Riskdetails.push(v);
                });
            }
            else {
                // Riskdetails = ['-1'];
            }

            var Statusdetails = [];
            if ($("#dropdownlistStatus1").data("kendoDropDownTree") != undefined && $("#dropdownlistStatus1").data("kendoDropDownTree") != null) {

                var list3 = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Statusdetails.push(v);
                });
            }
            else {
                // Statusdetails = ['-1'];
            }

            var Branchdetails = [];
            if ($("#dropdowntree1").data("kendoDropDownTree") != undefined && $("#dropdowntree1").data("kendoDropDownTree") != null) {

                var list3 = $("#dropdowntree1").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Branchdetails.push(v);
                });
            }
            else {
                // Statusdetails = ['-1'];
            }



            var Deptdetails = [];
            //if ($("#dropdownDept").data("kendoDropDownTree") != undefined && $("#dropdownDept").data("kendoDropDownTree") != null) {

            //    var list3 = $("#dropdownDept").data("kendoDropDownTree")._values;
            //    $.each(list3, function (i, v) {
            //        Deptdetails.push(v);
            //    });
            //}
            //else {
            //}

            var Userdetails = [];
            if ($("#dropdownUser").data("kendoDropDownTree") != undefined && $("#dropdownUser").data("kendoDropDownTree") != null) {

                var list3 = $("#dropdownUser").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Userdetails.push(v);
                });
            }
            else {
            }

            var FYD = 0;
            if ($("#dropdownFY").val() != undefined && $("#dropdownFY").val() != "") {
                FYD = $("#dropdownFY").val();
            }


            var gridexist = $('#grid1').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid1').empty();

            var grid1 = $("#grid1").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + roleflag + '&MonthId=' + pastdata + '&StatusFlag=-1&FY=' + FYD + '&TyprDetail=' + JSON.stringify(Typedetails) + '&Riskdetail=' + JSON.stringify(Riskdetails) + '&Statusdetail=' + JSON.stringify(Statusdetails) + '&Branchdetail=' + JSON.stringify(Branchdetails) + '&ComplianceID=' + $("#txtSearchComplianceID1").val() + '&Deptdetail=' + JSON.stringify(Deptdetails) + '&Userdetail=' + JSON.stringify(Userdetails) + '&EventName=' + $("#dropdownEventName").val() + '&EventNature=' + $("#dropdownEventNature").val() + '&ActName=' + $("#dropdownACT").val() + '&StartDate=' + $("#Startdatepicker").val() + '&EndDate=' + $("#Lastdatepicker").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            var StatustoryEB1 = response[0].StatustoryEB;
                            var Internal1 = response[0].Internal;
                            var InternalChecklist1 = response[0].InternalChecklist;
                            var StatChecklist1 = response[0].StatChecklist;
                            if (response[0].TotalCount > <% =DetailReportThreshold%>) {
                                TotalCount1 = response[0].TotalCount;
                                return '';
                            }
                            else {
                                return StatustoryEB1.concat(Internal1, InternalChecklist1, StatChecklist1);
                            }
                            //return response[0].StatustoryEB;
                        },
                        total: function (response) {
                            debugger
                            var StatustoryEB1 = response[0].StatustoryEB;
                            var Internal1 = response[0].Internal;
                            var InternalChecklist1 = response[0].InternalChecklist;
                            var StatChecklist1 = response[0].StatChecklist;

                            TotalCount1 = response[0].TotalCount;
                            if (response[0].TotalCount > <% =DetailReportThreshold%>) {
                                return 0;
                            }
                            else {
                                return StatustoryEB1.concat(Internal1, InternalChecklist1, StatChecklist1).length;
                            }
                            //return response[0].StatustoryEB.length;
                        },
                        model: {
                            fields: {
                                ComplianceID: { type: "string" },
                                ScheduledOn: { type: "date" },
                                CloseDate: { type: "date" },
                                ReviewerDated: { type: "date" },
                                PerformerDated: { type: "date" },
                                Challanpaiddate: { type: "date" },
                                Reviseduedate: { type: "date" },
                            }
                        }
                    },
                    pageSize: 10,

                },
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                noRecords: {
                    template: function (e) {

                        if (TotalCount1 > <% =DetailReportThreshold%>) {
                            return "Your search criteria has resulted large records. The output can't be displayed in this grid. Please click on Export button above to initiate excel download of this data.";
                        }
                        else { return "No records found"; }

                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                sort: onSorting,
                page: onPaging,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                columns: [

                    { hidden: true, field: "RiskCategory", title: "Risk", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "CustomerBranchID", title: "Branch ID", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "Sections", title: "Section", filterable: { multi: true, search: true }, width: "10%" },
                    {
                        field: "Branch", title: 'Location',
                        width: "16.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    { hidden: true, field: "ComplianceID", title: "Compliance ID", filterable: { multi: true, extra: false, search: true, }, width: "20%" },

                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "38%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventName", title: 'Event Name',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: { multi: true, search: true }, width: "10%"
                    },
                    {
                        hidden: true,
                        field: "EventNature", title: 'Event Nature',
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: { multi: true, search: true }, width: "13%"
                    },
                    {
                        field: "ScheduledOn", title: 'Due Date',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        //template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "13%"
                    },
                    {
                        field: "ForMonth", title: 'Period', filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        field: "Status", title: 'Status',
                        width: "12%",
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CloseDate", title: 'Close Date',
                        type: "date",
                        hidden: true,
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "13%"
                    },
                    {
                        field: "ReviewerDated", title: 'Reviewer Dated',
                        type: "date",
                        hidden: true,
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "15%"
                    },
                    {
                        field: "PerformerDated", title: 'Performer Dated',
                        type: "date",
                        hidden: true,
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "15%"
                    },
                      <%if (ComplianceCT == true)
                    {%>
                    {
                        field: "ComCategoryName", title: 'Compliance Category',
                        hidden: true,
                        width: "19.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }


                    },
                    {
                        field: "ComplianceTypeName", title: 'Compliance Type',
                        hidden: true,
                        width: "19.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }


                    },
                     <%}%>
                    {
                        field: "DepartmentName", title: 'Department',
                        width: "19.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }


                    },
                    {
                        hidden: true, field: "ReportName", title: "Report Name",
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "15%"
                    },
                    {
                        field: "ShortForm", title: 'Short Form',
                        width: "21%",
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: { multi: true, search: true }, width: "10%"
                    },
                    {
                        field: "PerformerName", title: 'Performer',
                        hidden: true,
                        width: "19.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }


                    },

                    {
                        field: "ReviewerName", title: 'Reviewer',
                        hidden: true,
                        width: "19.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }


                    },
                    {
                        field: "IsDocMandatoryNonMandatory", title: 'IsDocMandatory',
                        hidden: true,
                        width: "19.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }


                    },
                    {
                        command: [
                            { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overviewMain" }
                        ], title: "Action", lock: true, width: 120, headerAttributes: {
                            style: "text-align: center;"
                        }

                    }
                ]
            });

            function onSorting(arg) {
                settracknew('Detailed report', 'Paging', arg.sort.field, '');

            }

            function onPaging(arg) {
                settracknew('Detailed report', 'Paging', arg.page, '');
            }

            $("#grid1").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Overview";
                }
            });

            $("#grid1").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid1").kendoTooltip({
                filter: "th",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }

        function BindAdvancedGridFilters() {

            $("#Startdatepicker").kendoDatePicker({
                format: "dd-MMM-yyyy",
                change: onChange
            });
            $("#Lastdatepicker").kendoDatePicker({
                format: "dd-MMM-yyyy",
                change: onChange
            });

            function onChange() {
                $("#dropdownPastData").data("kendoDropDownList").value("All");
            }

            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Period', '');
                    $("#Startdatepicker").val('');
                    $("#Lastdatepicker").val('');
                },
                index: 1,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });



            var evalEventName1 = 0;

            if ($("#dropdownEventName1").val() != '') {
                evalEventName1 = $("#dropdownEventName1").val()
            }

            $("#dropdownEventName1").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventName",
                dataValueField: "eventid",
                optionLabel: "Select Event Name",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Nature of Event', '');
                    var values = this.value();

                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid1").data("kendoGrid").dataSource;
                        dataSource.filter(filter);

                        if ($("#dropdownEventName1").val() != '') {
                            evalEventName1 = $("#dropdownEventName1").val()
                        }
                        var dataSource1 = new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>',
                                    dataType: "json",
                                    beforeSend: function (request) {
                                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                                    },
                                }
                            },
                        });
                        dataSource1.read();
                        $("#dropdownEventNature1").data("kendoDropDownList").setDataSource(dataSource1);
                    }
                    else {
                        //FilterAllAdvancedSearch();
                        //ClearAllFilter();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read:"<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>"
                    }
                }
            });

            $("#dropdownEventNature1").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventNature",
                dataValueField: "EventScheduleOnid",
                optionLabel: "Select Event Nature",
                change: function (e) {

                    settracknew('Detailed report', 'Filtering', 'Nature of Event', '');
                    var values = this.value();
                    if (values != "" && values != null) {
                        var filter = { logic: "or", filters: [] };
                        filter.filters.push({
                            field: "EventScheduleOnID", operator: "eq", value: parseInt(values)
                        });
                        var dataSource = $("#grid1").data("kendoGrid").dataSource;
                        dataSource.filter(filter);
                    }
                    else {
                        //FilterAllAdvancedSearch();

                        var values = $("#dropdownEventName1").val();

                        if (values != "" && values != null) {
                            var filter = { logic: "or", filters: [] };
                            filter.filters.push({
                                field: "EventID", operator: "eq", value: parseInt(values)
                            });
                            var dataSource = $("#grid1").data("kendoGrid").dataSource;
                            dataSource.filter(filter);
                        }
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName1 + '&RoleId=<% =RoleID%>'
                    }
                }
            });

            $("#dropdownlistUserRole1").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Role', '');
                    //DataBindDaynamicKendoGrid();
                },
                dataSource: [
                    <%if (PerformerFlagID == 1)%>
                    <%{%>
                    { text: "Performer", value: "PRA" },
                    <%}%>
                    <%if (ReviewerFlagID == 1)%>
                    <%{%>
                    { text: "Reviewer", value: "REV" },
                    <%}%>
                    <%if (ApproverFlagID == 1)%>
                    <%{%>
                    { text: "Approver", value: "APPR" },
                    <%}%>
                    <%if (ManagmentFlagID == 1 || com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "HMGMT")%>
                    <%{%>
                    { text: "Managment", value: "MGMT" },
                    <%}%>
                    <%if (DepartmentFlagID == 1)%>
                    <%{%>
                    { text: "Department", value: "DEPT" },
                    <%}%>
                    <%if (AuditorFlagID == 1)%>
                    <%{%>
                    { text: "Auditor", value: "AUD" }
                    <%}%>
                ]
            });


            $("#dropdownlistRisk1").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    //FilterAllAdvancedSearch();
                    settracknew('Detailed report', 'Filtering', 'Risk', '');
                    fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');
                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });

            $("#dropdownFY").kendoDropDownList({
                autoWidth: true,
                dataTextField: "FinancialYear",
                dataValueField: "FinancialYear",
                optionLabel: "Financial Year",
                change: function () {
                    if ($("#dropdownFY").val() != "") {
                        $("#dropdownPastData").data("kendoDropDownList").select(4);
                    }
                    //DataBindDaynamicKendoGrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetFYDetailCompliance',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }
            });

            $("#dropdownUser").kendoDropDownTree({
                placeholder: "User",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "FullName",
                dataValueField: "UID",
                change: function () {
                    settracknew('Detailed report', 'Filtering', 'User', '');
                    //FilterAllAdvancedSearch();
                    fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoUserListNew?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =RoleKey%>&status=-1',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                }
            });

            $("#dropdownlistStatus1").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                checkAllTemplate: "Select All",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Status', '');
                    //FilterAllAdvancedSearch();
                    fCreateStoryBoard('dropdownlistStatus1', 'filterstatus1', 'status1')
                },
                dataSource: [
                    { text: "Upcoming", value: "Upcoming" },
                    { text: "Overdue", value: "Overdue" },
                    { text: "Pending For Review", value: "Pending For Review" },
                    <%if (IsNotCompiled == true)%>
                    <%{%>
                    { text: "Not Complied", value: "Not Complied" },
                    <%}%>
                    { text: "Rejected", value: "Rejected" },
                    { text: "Closed-Delayed", value: "Closed-Delayed" },
                    { text: "Closed-Timely", value: "Closed-Timely" },
                    { text: "Interim Rejected", value: "Interim Rejected" },
                    { text: "Interim Review Approved", value: "Interim Review Approved" },
                    { text: "Submitted For Interim Review", value: "Submitted For Interim Review" },
                    { text: "Not Applicable", value: "Not Applicable" },
                    { text: "In Progress", value: "In Progress" }
                ]
            });

            $("#dropdownlistComplianceType1").kendoDropDownTree({
                placeholder: "Compliance Type",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                checkAllTemplate: "Select All",

                dataTextField: "text",
                dataValueField: "value",
                index: 0,
                change: function (e) {
                    if ($("#dropdownlistComplianceType1").val() == "0" || $("#dropdownlistComplianceType1").val() == "3") {
                        $("#dropdownACT").data("kendoDropDownList").select(0);
                        $("#dropdownACT").data("kendoDropDownList").enable(false);
                    }
                    else {
                        $("#dropdownACT").data("kendoDropDownList").enable(true);
                    }
                    settracknew('Detailed report', 'Filtering', 'Compliance Type', '');
                    fCreateStoryBoard('dropdownlistComplianceType1', 'filterComplianceType1', 'ComplianceTypestatus1');
                    BindSequence();

                    var CompTypedetails = [];
                    if ($("#dropdownlistComplianceType1").data("kendoDropDownTree") != undefined) {
                        CompTypedetails = $("#dropdownlistComplianceType1").data("kendoDropDownTree")._values;
                    }

                    $('#dvdropdownEventName1').css('display', 'none');

                    for (var i = 0; i < CompTypedetails.length; i++) {
                        if (CompTypedetails[i] == 1) {
                            document.getElementById('dvdropdownEventName1').style = "display: block;padding-bottom: 7px;";
                        }
                    }
                    BindLocationDataSourceAdvnc();
                },
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Internal", value: "0" },
                    { text: "Event Based", value: "1" },
                    { text: "Statutory CheckList", value: "2" },
                    { text: "Internal CheckList", value: "3" },
                    { text: "Event Based CheckList", value: "4" },
                    //{ text: "All", value: "5" },
                ]
            });


            $("#dropdownSequence").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: false,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Label",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Sequece', '');
                    //FilterAllAdvancedSearch();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>',
                    }
                }

            });

            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: false,
                loadOnDemand: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Act', '');
                    //FilterAllAdvancedSearch();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            // url: '<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =Falg%>',
                            url: '<% =Path%>Data/BindComplianceWiseActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =Falg%>',

                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =Falg%>"
                    }
                },
                dataBound: function (e) {
                    e.sender.list.width("800");
                }
            });

            $("#dropdownlistComplianceType1").data("kendoDropDownTree").value(['-1']);

        }

        var record = 0;

        function BindRequestedGrid() {
            var dataSource = new kendo.data.DataSource({
                transport: {
                    read: '<% =Path%>/Data/RequestedReportGrid?CID=<% =CustId%>&UID=<% =UId%>'
                },
                schema: {
                    model: {
                        fields: {
                            filecreatedon: { type: "date" }
                        }
                    }
                },
                pageSize: 10
            });

            var gridexist = $('#Requestedgrid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#Requestedgrid').empty();

            $("#Requestedgrid").kendoGrid({
                dataSource: dataSource,
                sortable: true,
                filterable: true,
                columnMenu: true,
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,

                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                    record = 0;
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                columns: [
                    {
                        title: "Sr. No.",
                        template: "#= ++record #",
                        width: "70px"
                    },
                    {
                        field: "filecreatedon",
                        filterable: false,
                        title: "Date/Time of Report",
                        template: "#= kendo.toString(kendo.parseDate(filecreatedon, 'yyyy-MM-dd'), 'dd-MMM-yyyy hh:mm:tt') #",
                    },
                    {
                        field: "complianceType",
                        filterable: false,
                        title: "Search Filters Applied"
                    },
                    {
                        command: [
                            { name: "edit2", text: "", iconClass: "k-icon k-i-download", className: "ob-download" }
                        ], title: "Action", lock: true, width: 120, headerAttributes: {
                            style: "text-align: center;"
                        }
                    }
                ]
            });
            $("#Requestedgrid").kendoTooltip({
                filter: ".ob-download",
                content: function (e) {
                    return "Download";
                }
            });
            $("#Requestedgrid").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#Requestedgrid").kendoTooltip({
                filter: "th",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");


        }

        $(document).on("click", "#Requestedgrid tbody tr .ob-download", function (e) {

            var item = $("#Requestedgrid").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDownloadOverviewpup(item.ID, item.Path, item.FileName)
            return false;
        });

        function OpenDownloadOverviewpup(Id, Path, FileName) {
            $('#downloadfile').attr('src', "../Common/DownloadRequestedReportDocument.aspx?Id=" + Id + "&Path=" + Path + "&FileName=" + FileName);
            return false;
        }

        var TotalCount = 0;

        function BindGrid() {

            var pastdata = 3;
            if ($("#dropdownlistTypePastdata").val() != undefined && $("#dropdownlistTypePastdata").val() != "") {
                pastdata = $("#dropdownlistTypePastdata").val();
            }

            var roleflag ='<% =RoleKey%>';
            if ($("#dropdownlistUserRole").val() != undefined && $("#dropdownlistUserRole").val() != "") {
                roleflag = $("#dropdownlistUserRole").val();
            }

            var Typedetails = [];
            if ($("#dropdownlistComplianceType").data("kendoDropDownTree") != undefined && $("#dropdownlistComplianceType").data("kendoDropDownTree") != null) {

                var list3 = $("#dropdownlistComplianceType").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Typedetails.push(v);
                });
            }
            else {
                Typedetails = ['-1'];
            }

            var Riskdetails = [];
            if ($("#dropdownlistRisk").data("kendoDropDownTree") != undefined && $("#dropdownlistRisk").data("kendoDropDownTree") != null) {

                var list3 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Riskdetails.push(v);
                });
            }
            else {
                // Riskdetails = ['-1'];
            }

            var Statusdetails = [];
            if ($("#dropdownlistStatus").data("kendoDropDownTree") != undefined && $("#dropdownlistStatus").data("kendoDropDownTree") != null) {

                var list3 = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Statusdetails.push(v);
                });
            }
            else {
                // Statusdetails = ['-1'];
            }

            var Branchdetails = [];
            if ($("#dropdowntree").data("kendoDropDownTree") != undefined && $("#dropdowntree").data("kendoDropDownTree") != null) {

                var list3 = $("#dropdowntree").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Branchdetails.push(v);
                });
            }
            else {
                // Statusdetails = ['-1'];
            }

            var Deptdetails = [];
            if ($("#dropdownDept").data("kendoDropDownTree") != undefined && $("#dropdownDept").data("kendoDropDownTree") != null) {

                var list3 = $("#dropdownDept").data("kendoDropDownTree")._values;
                $.each(list3, function (i, v) {
                    Deptdetails.push(v);
                });
            }
            else {
            }

            var Userdetails = [];


            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();


            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoMyReport?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=' + roleflag + '&MonthId=' + pastdata + '&StatusFlag=-1&FY=0&TyprDetail=' + JSON.stringify(Typedetails) + '&Riskdetail=' + JSON.stringify(Riskdetails) + '&Statusdetail=' + JSON.stringify(Statusdetails) + '&Branchdetail=' + JSON.stringify(Branchdetails) + '&ComplianceID=' + $("#txtSearchComplianceID").val() + '&Deptdetail=' + JSON.stringify(Deptdetails) + '&Userdetail=' + JSON.stringify(Userdetails) + '&EventName=' + $("#dropdownEventName").val() + '&EventNature=' + $("#dropdownEventNature").val() + '&ActName=' + '&StartDate=&EndDate=',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {

                        data: function (response) {

                            var StatustoryEB1 = response[0].StatustoryEB;
                            var Internal1 = response[0].Internal;
                            var InternalChecklist1 = response[0].InternalChecklist;
                            var StatChecklist1 = response[0].StatChecklist;

                            if (response[0].TotalCount > <% =DetailReportThreshold%>) {
                                TotalCount = response[0].TotalCount;
                                return '';
                            }
                            else {
                                return StatustoryEB1.concat(Internal1, InternalChecklist1, StatChecklist1);
                            }
                        },
                        total: function (response) {

                            var StatustoryEB1 = response[0].StatustoryEB;
                            var Internal1 = response[0].Internal;
                            var InternalChecklist1 = response[0].InternalChecklist;
                            var StatChecklist1 = response[0].StatChecklist;
                            TotalCount = response[0].TotalCount;
                            if (response[0].TotalCount > <% =DetailReportThreshold%>) {
                                return 0;
                            }
                            else {
                                return StatustoryEB1.concat(Internal1, InternalChecklist1, StatChecklist1).length;
                            }
                        },
                        //data: function (response) {
                        //    return response[0].StatustoryEB;
                        //},
                        //total: function (response) {
                        //    return response[0].StatustoryEB.length;
                        //},

                        model: {
                            fields: {
                                ComplianceID: { type: "string" },
                                ScheduledOn: { type: "date" },
                                CloseDate: { type: "date" },
                                ReviewerDated: { type: "date" },
                                PerformerDated: { type: "date" },
                                Challanpaiddate: { type: "date" },
                                Reviseduedate: { type: "date" },
                            }
                        }

                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                noRecords: {
                    template: function (e) {

                        if (TotalCount > <% =DetailReportThreshold%>) {
                            return "Your search criteria has resulted large records. The output can't be displayed in this grid. Please click on Export button above to initiate excel download of this data.";
                        }
                        else { return "No records found"; }

                    }
                },

                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                sort: onSorting,
                page: onPaging,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                columns: [
                    { hidden: true, field: "RiskCategory", title: "Risk", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "CustomerBranchID", title: "Branch ID", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "Sections", title: "Section", filterable: { multi: true, search: true }, width: "10%" },
                    {
                        field: "Branch", title: 'Location',
                        width: "19.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    { hidden: true, field: "ComplianceID", title: "Compliance ID", filterable: { multi: true, extra: false, search: true, }, width: "20%" },
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "34.7%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        hidden: true,
                        field: "EventName", title: 'Event Name',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: { multi: true, search: true }, width: "10%"
                    },
                    {
                        hidden: true,
                        field: "EventNature", title: 'Event Nature',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: { multi: true, search: true }, width: "13%"
                    },
                    {
                        field: "ScheduledOn", title: 'Due Date',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        //template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%"
                    },
                    {
                        field: "ForMonth", title: 'Period', filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            // width: 120,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%"
                    },

                    {
                        field: "Status", title: 'Status',
                        //   width: 130,
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%"
                    },
                    {
                        field: "CloseDate", title: 'Close Date',
                        type: "date",
                        hidden: true,
                        format: "{0:dd-MMM-yyyy}",
                        //template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "13%"
                    },
                    {
                        field: "PerformerDated", title: 'Performer Dated',
                        type: "date",
                        hidden: true,
                        format: "{0:dd-MMM-yyyy}",
                        //template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "15%"
                    },
                    {
                        field: "ReviewerDated", title: 'Reviewer Dated',
                        type: "date",
                        hidden: true,
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "15%"
                    },
                    {
                        field: "ShortForm", title: 'Short Form',
                        width: "34.7%",
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: { multi: true, search: true }, width: "10%"
                    },
                    {
                        hidden: true, field: "ReportName", title: "Report Name",
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%"
                    },
                    <%if (ComplianceCT == true)
                    {%>
                    {
                        field: "ComCategoryName", title: 'Compliance Category',
                        hidden: true,
                        width: "19.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }


                    },
                    {
                        field: "ComplianceTypeName", title: 'Compliance Type',
                        hidden: true,
                        width: "19.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }


                    },
                     <%}%>
                    {
                        field: "DepartmentName", title: 'Department',
                        width: "19.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "PerformerName", title: 'Performer',
                        hidden: true,
                        width: "19.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ReviewerName", title: 'Reviewer',
                        hidden: true,
                        width: "19.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "IsDocMandatoryNonMandatory", title: 'IsDocMandatory',
                        hidden: true,
                        width: "19.7%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }


                    },
                    //{ hidden: true, field: "ReportName", title: "Report Type", filterable: { multi: true, search: true }, width: "10%" },
                    {
                        command: [
                            { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                        ], title: "Action", lock: true, width: "7%;", headerAttributes: {
                            style: "text-align: center;"
                        }
                    }
                ]
            });

            function onSorting(arg) {
                settracknew('Detailed report', 'Paging', arg.sort.field, '');

            }
            function onPaging(arg) {
                settracknew('Detailed report', 'Paging', arg.page, '');
            }

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Overview";
                }
            });
            $("#grid").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "th",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }

        function BindGridFilters() {

            $("#dropdownlistComplianceType").kendoDropDownTree({
                placeholder: "Compliance Type",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                checkAllTemplate: "Select All",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Complaince Type', '');
                    fCreateStoryBoard('dropdownlistComplianceType', 'filterComplianceType', 'ComplianceTypestatus');

                    var CompTypedetails = [];
                    if ($("#dropdownlistComplianceType").data("kendoDropDownTree") != undefined) {
                        CompTypedetails = $("#dropdownlistComplianceType").data("kendoDropDownTree")._values;
                    }

                    $('#dvdropdownEventName').css('display', 'none');
                    $('#dvdropdownEventNature').css('display', 'none');

                    for (var i = 0; i < CompTypedetails.length; i++) {
                        if (CompTypedetails[i] == 1) {
                            document.getElementById('dvdropdownEventName').style = "display: block;float: left;width: 25%;margin-right: 1.2%;";
                            document.getElementById('dvdropdownEventNature').style = "display: block;float: left;width: 23.5%;margin-right: 1.2%;";
                        }
                    }
                    BindLocationDataSource();
                },
                dataSource: [
                    { text: "Statutory", value: "-1" },
                    { text: "Internal", value: "0" },
                    { text: "Event Based", value: "1" },
                    { text: "Statutory CheckList", value: "2" },
                    { text: "Internal CheckList", value: "3" },
                    { text: "Event Based CheckList", value: "4" },
                ]
            });
            $("#dropdownlistComplianceType").data("kendoDropDownTree").value(['-1']);

            $("#dropdownDept").kendoDropDownTree({
                placeholder: "Department",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                checkAllTemplate: "Select All",
                change: function (e) {
                    //FilterAllMain();
                    fCreateStoryBoard('dropdownDept', 'filterdept', 'dept')
                    $('input[id=chkAllMain]').prop('checked', false);
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindDeptUserList?UId=<% =UId%>&CId=<% =CustId%>&Flag=<% =RoleKey%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }

                    }
                }
            });

            $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    settracknew('Detailed report', 'Filtering', 'Risk', '');
                    // FilterAllMain();
                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });

            $("#dropdownlistStatus").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                checkAllTemplate: "Select All",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Status', '');
                    // FilterAllMain();
                    fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status')
                },
                dataSource: [
                    { text: "Upcoming", value: "Upcoming" },
                    { text: "Overdue", value: "Overdue" },
                    { text: "Pending For Review", value: "Pending For Review" },
                    <%if (IsNotCompiled == true)%>
                    <%{%>
                    { text: "Not Complied", value: "Not Complied" },
                    <%}%>
                    { text: "Rejected", value: "Rejected" },
                    { text: "Closed-Delayed", value: "Closed-Delayed" },
                    { text: "Closed-Timely", value: "Closed-Timely" },
                    { text: "Interim Rejected", value: "Interim Rejected" },
                    { text: "Interim Review Approved", value: "Interim Review Approved" },
                    { text: "Submitted For Interim Review", value: "Submitted For Interim Review" },
                    { text: "Not Applicable", value: "Not Applicable" },
                    { text: "In Progress", value: "In Progress" }
                ]
            });

            $("#dropdownlistTypePastdata").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    //BindGrid();
                    settracknew('Detailed report', 'Filtering', 'Period', '');
                },
                index: 1,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });

            var evalEventName = 0;

            if ($("#dropdownEventName").val() != '') {
                evalEventName = $("#dropdownEventName").val()
            }

            $("#dropdownEventName").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventName",
                dataValueField: "eventid",
                optionLabel: "Select Event Name",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Nature of Event', '');
                    var values = this.value();

                    if (values != "" && values != null) {
                        //var filter = { logic: "or", filters: [] };
                        //filter.filters.push({
                        //    field: "EventID", operator: "eq", value: parseInt(values)
                        //});
                        //var dataSource = $("#grid").data("kendoGrid").dataSource;
                        //dataSource.filter(filter);

                        if ($("#dropdownEventName").val() != '') {
                            evalEventName = $("#dropdownEventName").val()
                        }
                        var dataSource1 = new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>',
                                    dataType: "json",
                                    beforeSend: function (request) {
                                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                                    },
                                }
                            },
                        });
                        dataSource1.read();
                        $("#dropdownEventNature").data("kendoDropDownList").setDataSource(dataSource1);
                    }
                    else {
                        ClearAllFilterMain();
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventName?UId=<% =UId%>&CId=<% =CustId%>&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }
            });

            $("#dropdownEventNature").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "EventNature",
                dataValueField: "EventScheduleOnid",
                optionLabel: "Select Event Nature",
                change: function (e) {
                    settracknew('Detailed report', 'Filtering', 'Nature of Event', '');
                    var values = this.value();

                    if (values != "" && values != null) {
                        //var filter = { logic: "or", filters: [] };
                        //filter.filters.push({
                        //    field: "EventScheduleOnID", operator: "eq", value: parseInt(values)
                        //});
                        //var dataSource = $("#grid").data("kendoGrid").dataSource;
                        //dataSource.filter(filter);
                    }
                    else {
                        var values = $("#dropdownEventName").val();

                        //if (values != "" && values != null) {
                        //    var filter = { logic: "or", filters: [] };
                        //    filter.filters.push({
                        //        field: "EventID", operator: "eq", value: parseInt(values)
                        //    });
                        //    var dataSource = $("#grid").data("kendoGrid").dataSource;
                        //    dataSource.filter(filter);
                        //}
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>data/KendoGetEventNature?UId=<% =UId%>&CId=<% =CustId%>&EventID=' + evalEventName + '&RoleId=<% =RoleID%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }
            });

            $("#dropdownlistUserRole").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    //DataBindDaynamicKendoGriddMain();
                    settracknew('Detailed report', 'Filtering', 'Role', '');
                },
                dataSource: [
                    <%if (PerformerFlagID == 1)%>
                    <%{%>
                    { text: "Performer", value: "PRA" },
                    <%}%>
                    <%if (ReviewerFlagID == 1)%>
                    <%{%>
                    { text: "Reviewer", value: "REV" },
                    <%}%>
                    <%if (ApproverFlagID == 1)%>
                    <%{%>
                    { text: "Approver", value: "APPR" },
                    <%}%>
                    <%if (ManagmentFlagID == 1 || com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "HMGMT")%>
                    <%{%>
                    { text: "Managment", value: "MGMT" },
                    <%}%>
                    <%if (DepartmentFlagID == 1)%>
                    <%{%>
                    { text: "Department", value: "DEPT" },
                    <%}%>
                    <%if (AuditorFlagID == 1)%>
                    <%{%>
                    { text: "Auditor", value: "AUD" }
                    <%}%>
                ]
            });

        }

        function FilterAllMain() {

            //location details
            var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
            var locationsdetails = [];
            $.each(list1, function (i, v) {
                locationsdetails.push({
                    field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                });
            });
            var ComplianceFilter = [];
            if ($("#txtSearchComplianceID").val() != "") {
                ComplianceFilter.push({
                    field: "ComplianceID", operator: "contains", value: $("#txtSearchComplianceID").val()
                });
            }

            //ComplianceType details
            var list2 = $("#dropdownlistComplianceType").data("kendoDropDownTree")._values;
            var ComplianceTypedetails = [];
            $.each(list2, function (i, v) {
                ComplianceTypedetails.push({
                    field: "Status", operator: "eq", value: v
                });
            });

            //Status details
            var list2 = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
            var Statusdetails = [];
            $.each(list2, function (i, v) {
                Statusdetails.push({
                    field: "Status", operator: "eq", value: v
                });
            });

            //risk Details
            var Riskdetails = [];
            var list3 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
            $.each(list3, function (i, v) {
                Riskdetails.push({
                    field: "Risk", operator: "eq", value: parseInt(v)
                });
            });

            //department Details
            var Deptdetails = [];
            var list3 = $("#dropdownDept").data("kendoDropDownTree")._values;
            $.each(list3, function (i, v) {
                Deptdetails.push({
                    field: "DepartmentID", operator: "eq", value: parseInt(v)
                });
            });

            var dataSource = $("#grid").data("kendoGrid").dataSource;

            if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                && $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
                && $("#dropdownlistComplianceType").data("kendoDropDownTree")._values.length > 0
                && $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0
                && $("#dropdownDept").data("kendoDropDownTree")._values.length > 0) {

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Riskdetails
                        },
                        {
                            logic: "or",
                            filters: ComplianceTypedetails
                        },
                        {
                            logic: "or",
                            filters: Statusdetails
                        },
                        {
                            logic: "or",
                            filters: locationsdetails
                        },
                        {
                            logic: "or",
                            filters: Deptdetails
                        }
                    ]
                });
            }

            else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                && $("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Statusdetails
                        },
                        {
                            logic: "or",
                            filters: locationsdetails
                        }
                    ]
                });
            }

            else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0
                && $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Riskdetails
                        },
                        {
                            logic: "or",
                            filters: locationsdetails
                        }
                    ]
                });
            }


            else if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0
                && $("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Riskdetails
                        },
                        {
                            logic: "or",
                            filters: Statusdetails
                        }
                    ]
                });
            }

            else if ($("#dropdowntree").data("kendoDropDownTree")._values.length > 0) {

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: locationsdetails
                        }
                    ]
                });
            }

            else if ($("#dropdownlistStatus").data("kendoDropDownTree")._values.length > 0) {

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Statusdetails
                        }
                    ]
                });
            }
            else if (ComplianceFilter.length > 0) {
                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: ComplianceFilter
                        }
                    ]
                });
            }
            else if ($("#dropdownlistRisk").data("kendoDropDownTree")._values.length > 0) {

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Riskdetails
                        }
                    ]
                });
            }
            else if ($("#dropdownDept").data("kendoDropDownTree")._values.length > 0) {

                dataSource.filter({
                    logic: "and",
                    filters: [
                        {
                            logic: "or",
                            filters: Deptdetails
                        }
                    ]
                });
            }

            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
        }

        function FilterAllAdvancedSearch() {

            //location details
            var locationsdetails = [];
            if ($("#dropdowntree1").data("kendoDropDownTree") != undefined) {
                locationsdetails = $("#dropdowntree1").data("kendoDropDownTree")._values;
            }

            //Status details
            var Statusdetails = [];
            if ($("#dropdownlistStatus1").data("kendoDropDownTree") != undefined) {
                Statusdetails = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
            }

            //risk Details
            var Riskdetails = [];
            if ($("#dropdownlistRisk1").data("kendoDropDownTree") != undefined) {
                Riskdetails = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
            }

            //user Details 
            var userdetails = [];
                  <%if (RoleFlag == 1)%>
                   <%{%>
            if ($("#dropdownUser").data("kendoDropDownTree") != undefined) {
                userdetails = $("#dropdownUser").data("kendoDropDownTree")._values;
            }
                <%}%>


            //datefilter
            var datedetails = [];
            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                datedetails.push({
                    field: "ScheduledOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                });
            }
            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                datedetails.push({
                    field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                });
            }

            //dept Details
            var deptdetails = [];
            if ($("#dropdownDept").data("kendoDropDownTree") != undefined) {
                deptdetails = $("#dropdownDept").data("kendoDropDownTree")._values;
            }
            var finalSelectedfilter = { logic: "and", filters: [] };

            if (locationsdetails.length > 0
                || Statusdetails.length > 0
                //|| ComplianceTypedetails.length > 0
                || Riskdetails.length > 0
                || userdetails.length > 0
                || deptdetails.length > 0
                || $("#txtSearchComplianceID1").val() != "" && $("#txtSearchComplianceID1").val() != undefined
                || ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined)
                || ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "")
                || ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "")
                || ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "")) {

                if ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") {
                    var SeqFilter = { logic: "or", filters: [] };
                    SeqFilter.filters.push({
                        field: "SequenceID", operator: "eq", value: $("#dropdownSequence").val()
                    });
                    finalSelectedfilter.filters.push(SeqFilter);
                }

                if (locationsdetails.length > 0) {
                    var LocationFilter = { logic: "or", filters: [] };

                    $.each(locationsdetails, function (i, v) {
                        LocationFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(LocationFilter);
                }

                if (Statusdetails.length > 0) {
                    var StatusFilter = { logic: "or", filters: [] };

                    $.each(Statusdetails, function (i, v) {
                        StatusFilter.filters.push({
                            field: "Status", operator: "eq", value: v
                        });
                    });
                    finalSelectedfilter.filters.push(StatusFilter);
                }
                if (Riskdetails.length > 0) {
                    var RiskFilter = { logic: "or", filters: [] };
                    $.each(Riskdetails, function (i, v) {
                        RiskFilter.filters.push({
                            field: "Risk", operator: "eq", value: parseInt(v)
                        });
                    });
                    finalSelectedfilter.filters.push(RiskFilter);
                }
                if (userdetails.length > 0) {
                    var UserFilter = { logic: "or", filters: [] };

                    //$.each(userdetails, function (i, v) {
                    //    UserFilter.filters.push({
                    //        field: "UserID", operator: "eq", value: parseInt(v)
                    //    });
                    //});
                    $.each(userdetails, function (i, v) {
                        UserFilter.filters.push({
                            field: "PerformerID", operator: "eq", value: parseInt(v)
                        });
                    });
                    $.each(userdetails, function (i, v) {
                        UserFilter.filters.push({
                            field: "ReviewerID", operator: "eq", value: parseInt(v)
                        });
                    });
                    $.each(userdetails, function (i, v) {
                        UserFilter.filters.push({
                            field: "ApproverID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(UserFilter);
                }
                if ($("#txtSearchComplianceID1").val() != "") {
                    var RiskFilter = { logic: "or", filters: [] };
                    RiskFilter.filters.push({
                        field: "ComplianceID", operator: "contains", value: $("#txtSearchComplianceID1").val()
                    });
                    finalSelectedfilter.filters.push(RiskFilter);
                }
                if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
                    var ActFilter = { logic: "or", filters: [] };
                    ActFilter.filters.push({
                        field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
                    });
                    finalSelectedfilter.filters.push(ActFilter);
                }

                if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                    var DateFilter = { logic: "or", filters: [] };

                    DateFilter.filters.push({
                        field: "ScheduledOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'dd-MMM-yyyy')
                    });
                    finalSelectedfilter.filters.push(DateFilter);
                }
                if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                    var DateFilter = { logic: "or", filters: [] };
                    DateFilter.filters.push({
                        field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'dd-MMM-yyyy')
                    });

                    finalSelectedfilter.filters.push(DateFilter);
                }

                if (deptdetails.length > 0) {
                    var deptFilter = { logic: "or", filters: [] };
                    $.each(deptdetails, function (i, v) {
                        deptFilter.filters.push({
                            field: "DepartmentID", operator: "eq", value: parseInt(v)
                        });
                    });
                    finalSelectedfilter.filters.push(RiskFilter);
                }
                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid1").data("kendoGrid").dataSource.filter({});
                }

            }
            else {
                $("#grid1").data("kendoGrid").dataSource.filter({});
            }
        }

        function ClearAllFilterMain() {
         
            $("#txtSearchComplianceID").val('');
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
            $("#dropdownDept").data("kendoDropDownTree").value([]);
            $("#dropdownlistTypePastdata").data("kendoDropDownList").value(3);
            var CompTypedetails = [];
            if ($("#dropdownlistComplianceType").data("kendoDropDownTree") != undefined) {
                CompTypedetails = $("#dropdownlistComplianceType").data("kendoDropDownTree")._values;
            }

            //if (CompTypedetails.includes("1")) {

            //}
            //else {
                $("#dropdownlistComplianceType").data("kendoDropDownTree").value(['-1']);
                $("#dropdownEventName").data("kendoDropDownList").select(0);
                $("#dropdownEventNature").data("kendoDropDownList").select(0); 
            //}

            $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
            $("#dropdownPastData").data("kendoDropDownList").select(4);
            $("#grid").data("kendoGrid").dataSource.filter({});
            BindGrid();

        }

        function ClearAllFilter() {
            
            $("#txtSearchComplianceID1").val('');
            $("#dropdownEventName1").data("kendoDropDownList").select(0);
            $("#dropdowntree1").data("kendoDropDownTree").value([]);
            $("#dropdownEventNature1").data("kendoDropDownList").select(0);
            if ($("#dropdownFY").val() != "") {
                $("#dropdownFY").data("kendoDropDownList").select(0);
                $("#dropdownFY").data("kendoDropDownList").trigger("change");
            }
            $("#dropdownACT").data("kendoDropDownList").select(0);
            $("#dropdownlistRisk1").data("kendoDropDownTree").value([]);
            $("#dropdownlistComplianceType1").data("kendoDropDownTree").value(['-1']);
            <%if (RoleFlag == 1)%><%{%>
            $("#dropdownUser").data("kendoDropDownTree").value([]);
            <%}%>
            $("#dropdownPastData").data("kendoDropDownList").value(3);
            $("#dropdownlistStatus1").data("kendoDropDownTree").value([]);
            $("#Startdatepicker").data("kendoDatePicker").value(null);
            $("#Lastdatepicker").data("kendoDatePicker").value(null);
            $("#grid1").data("kendoGrid").dataSource.filter({});

            $("#dropdownlistComplianceType1").data("kendoDropDownTree").value(['-1']);
            $("#dropdownEventName1").data("kendoDropDownList").select(0);
            $("#dropdownEventNature1").data("kendoDropDownList").select(0); 

            BindAdvancedGrid();
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
            fCreateStoryBoard('dropdownlistComplianceType', 'filterComplianceType', 'ComplianceType')
            fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1');
            fCreateStoryBoard('dropdownlistStatus1', 'filterstatus1', 'status1');
            fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');
            fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
            fCreateStoryBoard('dropdownDept', 'filterdept', 'dept');
            fCreateStoryBoard('dropdownlistComplianceType1', 'filterComplianceType1', 'ComplianceTypestatus1');
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterrisk') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterComplianceType') {
                $('#' + div).append('ComplianceType&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterComplianceType1') {
                $('#' + div).append('ComplianceType&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterstatus') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filtersstoryboard1') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
            }
            else if (div == 'filterstatus1') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;:');
            }
            else if (div == 'filterrisk1') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;:');
            }
            else if (div == 'filterUser') {
                $('#' + div).append('User&nbsp;&nbsp;&nbsp;:');
            }
            else if (div == 'filterdept') {
                $('#' + div).append('Department&nbsp;&nbsp;&nbsp;:');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                //if (buttontest.length > 10) {
                //    buttontest = buttontest.substring(0, 10).concat("...");
                //}
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB;height: 20px;Color:Gray;margin-left:5px;margin-bottom: 4px;border-radius:10px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="Clear" aria-label="Clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" title="Clear" aria-label="Clear" style="font-size: 12px;"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
        }

        function BindLocationDataSourceAdvnc() {

            if ($("#dropdownlistComplianceType1").data("kendoDropDownTree") != undefined && $("#dropdownlistComplianceType1").data("kendoDropDownTree") != null) {
                if ($("#dropdownlistComplianceType1").data("kendoDropDownTree")._values.length > 0) {
                    var ComplianceTypeAdv = $("#dropdownlistComplianceType1").data("kendoDropDownTree")._values;

                    var LocationType = "";
                    var Flaginternallocation = 0;
                    var FlagStatlocation = 0;
                    for (var i = 0; i < ComplianceTypeAdv.length; i++) {
                        if (ComplianceTypeAdv[i] == 0 || ComplianceTypeAdv[i] == 3) {
                            Flaginternallocation = 1;
                        }
                        if (ComplianceTypeAdv[i] == -1 || ComplianceTypeAdv[i] == 1 || ComplianceTypeAdv[i] == 2 || ComplianceTypeAdv[i] == 4) {
                            FlagStatlocation = 1;
                        }
                    }

                    if (Flaginternallocation == 1 && FlagStatlocation == 1) {
                        LocationType = 'A';
                    }
                    else if (Flaginternallocation == 1) {
                        LocationType = 'I';
                    }
                    else if (FlagStatlocation == 1) {
                        LocationType = 'S';
                    }

                    if ($('#dropdowntree1').data('kendoDropDownTree')) {
                        $('#dropdowntree1').data('kendoDropDownTree').value([]);
                        $('#dropdowntree1').data('kendoDropDownTree').destroy();
                        $('#divLocationAdv').empty();
                        $('#divLocationAdv').append('<input id="dropdowntree1" style="width: 100%;" />');
                    }

                    $("#dropdowntree1").kendoDropDownTree({
                        placeholder: "Entity/Sub Entity/Location",
                        checkboxes: {
                            checkChildren: true
                        },
                        checkAll: true,
                        autoWidth: true,
                        autoClose: false,
                        checkAllTemplate: "Select All",
                        dataTextField: "Name",
                        dataValueField: "ID",
                        loadOnDemand: true,
                        change: function (e) {
                            settracknew('Detailed report', 'Filtering', 'Location', '');
                            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')
                        },
                        dataSource: {
                            severFiltering: true,
                            transport: {
                                read: {
                                    url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=' + LocationType,
                                    dataType: "json",
                                    beforeSend: function (request) {
                                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                                    },
                                }
                            },
                            schema: {
                                data: function (response) {
                                    return response[0].locationList;
                                },
                                model: {
                                    children: "Children"
                                }
                            }
                        }
                    });
                }
            }
        }

        function BindLocationDataSource() {
            if ($("#dropdownlistComplianceType").data("kendoDropDownTree") != undefined && $("#dropdownlistComplianceType").data("kendoDropDownTree") != null) {
                if ($("#dropdownlistComplianceType").data("kendoDropDownTree")._values.length > 0) {
                    var ComplianceTypeAdv = $("#dropdownlistComplianceType").data("kendoDropDownTree")._values;
                    var LocationType = "";


                    var Flaginternallocation = 0;
                    var FlagStatlocation = 0;
                    for (var i = 0; i < ComplianceTypeAdv.length; i++) {
                        if (ComplianceTypeAdv[i] == 0 || ComplianceTypeAdv[i] == 3) {
                            Flaginternallocation = 1;
                        }
                        if (ComplianceTypeAdv[i] == -1 || ComplianceTypeAdv[i] == 1 || ComplianceTypeAdv[i] == 2 || ComplianceTypeAdv[i] == 4) {
                            FlagStatlocation = 1;
                        }
                    }

                    if (Flaginternallocation == 1 && FlagStatlocation == 1) {
                        LocationType = 'A';
                    }
                    else if (Flaginternallocation == 1) {
                        LocationType = 'I';
                    }
                    else if (FlagStatlocation == 1) {
                        LocationType = 'S';
                    }
                    if ($('#dropdowntree').data('kendoDropDownTree')) {
                        $('#dropdowntree').data('kendoDropDownTree').value([]);
                        $('#dropdowntree').data('kendoDropDownTree').destroy();
                        $('#divLocationMain').empty();
                        $('#divLocationMain').append('<input id="dropdowntree" style="width: 100%;" />');
                    }

                    $("#dropdowntree").kendoDropDownTree({
                        placeholder: "Entity/Sub Entity/Location",
                        checkboxes: {
                            checkChildren: true
                        },
                        checkAll: true,
                        autoWidth: true,
                        autoClose: false,
                        checkAllTemplate: "Select All",
                        dataTextField: "Name",
                        dataValueField: "ID",
                        change: function (e) {
                            settracknew('Detailed report', 'Filtering', 'Location', '');
                            //FilterAllMain();
                            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                            $('input[id=chkAllMain]').prop('checked', false);
                        },
                        dataSource: {
                            severFiltering: true,
                            transport: {
                                read: {
                                    url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=<% =Falg%>&IsStatutoryInternal=' + LocationType,
                                    dataType: "json",
                                    beforeSend: function (request) {
                                        request.setRequestHeader('Authorization', '<% =Authorization%>');
                                    },
                                }
                            },
                            schema: {
                                data: function (response) {
                                    return response[0].locationList;
                                },
                                model: {
                                    children: "Children"
                                }
                            }
                        }
                    });
                }
            }
        }

        function BindSequence() {

            <%if (RoleFlag == 1)%>
            <%{%>
            var dataSourceUser = new kendo.data.HierarchicalDataSource({
                severFiltering: true,
                transport: {
                    read: {
                        url: '<% =Path%>Data/KendoUserListNew?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=' + $("#dropdownlistUserRole1").val() + '&status=-1',
                        <%--url: '<% =Path%>Data/KendoUserListNew?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=' + $("#dropdownlistUserRole1").val() + '&status=' + $("#dropdownlistComplianceType1").val(),--%>
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                        },
                    }
                }
            });
            dataSourceUser.read();
            $("#dropdownUser").data("kendoDropDownTree").value([]);
            $("#dropdownUser").data("kendoDropDownTree").setDataSource(dataSourceUser);
            <%}%>

            //DataBindDaynamicKendoGrid();

            <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%><%{%>
            $("#dropdownSequence").data("kendoDropDownList").select(0);
            <%}%>

            if ($("#dropdownlistComplianceType1").val() == 0 || $("#dropdownlistComplianceType1").val() == 3) {
                var dataSourceSequence = new kendo.data.DataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=I&CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                });
                dataSourceSequence.read();
              <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%><%{%>
                $("#dropdownSequence").data("kendoDropDownList").setDataSource(dataSourceSequence);
              <%}%>
            }
            else {
                var dataSourceSequence = new kendo.data.DataSource({
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                });
                dataSourceSequence.read();
                   <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%><%{%>
                $("#dropdownSequence").data("kendoDropDownList").setDataSource(dataSourceSequence);
                            <%}%>

            }
        }

        function Applybtndata(e) {
            BindGrid();
            e.preventDefault();
            return false;
        }

        function Applybtndata1(e) {
            BindAdvancedGrid();
            e.preventDefault();
            return false;
        }

        function OpenAdvanceSearch(e) {
            $("#divAdvanceSearchModel").kendoWindow({
                modal: true,
                pinned: true,
                width: "97%",
                height: "93%",
                title: "Advanced Search",
                visible: false,
                draggable: false,
                refresh: true,
                actions: [
                    "Close"
                ], close: CloseAdvancePopup

            }).data("kendoWindow").open().center();
            BindAdvancedGrid();
            e.preventDefault();
            return false;
        }

        function CloseAdvancePopup() {
            ClearAllFilter();
        }

        function OpenAdvanceSearchFilter(e) {
            $('#divAdvanceSearchFilterModel').modal('show');
            e.preventDefault();
            return false;
        }

        function exportReportAdvanced(e) {

            if (TotalCount1 > <% =DetailReportThreshold%>) {

                var kendoWindow = $("<div />").kendoWindow({
                    title: "Export Report",
                    resizable: false,
                    modal: true,
                    //actions: [
                    //    "Close"
                    //]
                });

                kendoWindow.data("kendoWindow")
                    .content($("#delete-confirmation").html())
                    .center().open();

                kendoWindow.find(".delete-confirm,.delete-cancel")
                    .click(function () {

                        if ($(this).hasClass("delete-confirm")) {


                            var FlagRole = document.getElementById('FlagDetail').value;
                            var UId = document.getElementById('UId').value;
                            var customerId = document.getElementById('CustomerId').value;
                            var PathName = document.getElementById('Path').value;
                            var CustomerName = document.getElementById('CustName').value;

                            //location details
                            var list1 = $("#dropdowntree1").data("kendoDropDownTree")._values;
                            var locationsdetails = [];
                            $.each(list1, function (i, v) {
                                locationsdetails.push(v);
                            });


                            //ComplianceType details
                            var list2 = $("#dropdownlistComplianceType1").data("kendoDropDownTree")._values;
                            var ComplianceTypedetails = [];
                            $.each(list2, function (i, v) {
                                ComplianceTypedetails.push(v);
                            });

                            //Status details
                            var list2 = $("#dropdownlistStatus1").data("kendoDropDownTree")._values;
                            var Statusdetails = [];
                            $.each(list2, function (i, v) {
                                Statusdetails.push(v);
                            });

                            //risk Details
                            var Riskdetails = [];
                            var list3 = $("#dropdownlistRisk1").data("kendoDropDownTree")._values;
                            $.each(list3, function (i, v) {
                                Riskdetails.push(v);
                            });

                            var userdetails = [];
                            var Actdetails = [];
                            var SequenceDataID = null;
                            //if ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") {
                            //    SequenceDataID = $("#dropdownSequence").val();
                            //}

                            $.ajax({
                                type: "GET",
                                url: '' + PathName + '//ExportReport/ExportReportRequest',
                                data: {
                                    UserId: UId, CustomerID: customerId,
                                    StatusFlag: JSON.stringify(ComplianceTypedetails),
                                    FlagIsApp: $("#dropdownlistUserRole1").val(),
                                    MonthId: $("#dropdownlistTypePastdata1").val(), FY: $("#dropdownFY").val(),
                                    CustomerName: CustomerName, location: JSON.stringify(locationsdetails),
                                    risk: JSON.stringify(Riskdetails),
                                    status: JSON.stringify(Statusdetails),
                                    userDetail: JSON.stringify(userdetails),
                                    actDetail: JSON.stringify(Actdetails),
                                    StartDateDetail: '',
                                    EndDateDetail: '',
                                    EventName: $("#dropdownEventName1").val(),
                                    EventNature: $("#dropdownEventNature1").val(),
                                    SequenceID: SequenceDataID,
                                    IsEmailRequestFlag: true,
                                    IsEmailSendFlag: false
                                },
                                success: function (response) {
                                    //if (response != "Error" && response != "No Record Found" && response != "") {
                                    //    window.location.href = '' + PathName + '/ExportReport/GetFile?userpath=' + response + '';
                                    //}
                                    //if (response == "No Record Found") {
                                    //    alert("No Record Found");
                                    //}
                                }
                            });

                            kendoWindow.data("kendoWindow").close();
                        }
                        else {

                            var FlagRole = document.getElementById('FlagDetail').value;
                            var UId = document.getElementById('UId').value;
                            var customerId = document.getElementById('CustomerId').value;
                            var PathName = document.getElementById('Path').value;
                            var CustomerName = document.getElementById('CustName').value;

                            //location details
                            var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
                            var locationsdetails = [];
                            $.each(list1, function (i, v) {
                                locationsdetails.push(v);
                            });


                            //ComplianceType details
                            var list2 = $("#dropdownlistComplianceType").data("kendoDropDownTree")._values;
                            var ComplianceTypedetails = [];
                            $.each(list2, function (i, v) {
                                ComplianceTypedetails.push(v);
                            });

                            //Status details
                            var list2 = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
                            var Statusdetails = [];
                            $.each(list2, function (i, v) {
                                Statusdetails.push(v);
                            });

                            //risk Details
                            var Riskdetails = [];
                            var list3 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
                            $.each(list3, function (i, v) {
                                Riskdetails.push(v);
                            });

                            var userdetails = [];
                            var Actdetails = [];
                            var SequenceDataID = null;
                            if ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") {
                                SequenceDataID = $("#dropdownSequence").val();
                            }

                            $.ajax({
                                type: "GET",
                                url: '' + PathName + '//ExportReport/ExportReportRequest',
                                data: {
                                    UserId: UId, CustomerID: customerId,
                                    //StatusFlag: $("#dropdownlistComplianceType").val(),
                                    FlagIsApp: $("#dropdownlistUserRole").val(),
                                    MonthId: $("#dropdownlistTypePastdata").val(), FY: $("#dropdownFY").val(),
                                    CustomerName: CustomerName, location: JSON.stringify(locationsdetails),
                                    StatusFlag: JSON.stringify(ComplianceTypedetails),
                                    status: JSON.stringify(ComplianceTypedetails),
                                    risk: JSON.stringify(Riskdetails),
                                    status: JSON.stringify(Statusdetails),
                                    userDetail: JSON.stringify(userdetails),
                                    actDetail: JSON.stringify(Actdetails),
                                    StartDateDetail: '',
                                    EndDateDetail: '',
                                    EventName: $("#dropdownEventName").val(),
                                    EventNature: $("#dropdownEventNature").val(),
                                    SequenceID: SequenceDataID,
                                    IsEmailRequestFlag: false,
                                    IsEmailSendFlag: false
                                },
                                success: function (response) {

                                }
                            });

                            kendoWindow.data("kendoWindow").close();
                        }

                    })
                    .end();
                e.preventDefault();
                return false;

            }
            else {

                var ReportName = "Detailed Report";
                var customerName = document.getElementById('CustName').value;
                var todayDate = moment().format('DD-MMM-YYYY');
                var grid = $("#grid1").getKendoGrid();
                var rows =
                    [
                        {
                            cells: [
                                { value: "Entity/ Location:", bold: true },
                                { value: customerName }
                            ]
                        },
                        {
                            cells: [
                                { value: "Report Name:", bold: true },
                                { value: ReportName }
                            ]
                        },
                        {
                            cells: [
                                { value: "Report Generated On:", bold: true },
                                { value: todayDate }
                            ]
                        },
                        {
                            cells: [
                                { value: "" }
                            ]
                        },
                        {
                            cells: [
                                { value: "Sr.No.", bold: true },
                                { value: "Compliance ID", bold: true },
                            <%if (EntityField == true)%><%{%>
                                { value: "Entity", bold: true },
                            <%}%>
                                { value: "Location", bold: true },
                                { value: "Act", bold: true },
                                { value: "Section", bold: true },
                                { value: "Category Name", bold: true },
                                { value: "Sub Category Name", bold: true },
                                { value: "Short Form", bold: true },
                                { value: "Penalty Description", bold: true },
                                { value: "Short Description", bold: true },
                                { value: "Detail Description", bold: true },
                                { value: "Compliance Type", bold: true },
                                { value: "Department Name", bold: true },
                                { value: "Event Name", bold: true },
                                { value: "Event Nature", bold: true },
                                { value: "Period", bold: true },
                                { value: "Due Date", bold: true },
                                { value: "Close Date", bold: true },
                                { value: "Status", bold: true },
                                { value: "Risk", bold: true },
                                { value: "Performer", bold: true },
                                { value: "Reviewer", bold: true },
                                 <%if (RemoveColumn == true)%><%{%>
                                { value: "", bold: true },
                             <%}%>
                            <%else if (RemoveColumn == false)%><%{%>
                                { value: "Approver", bold: true },
                             <%}%>
                                //{ value: "Approver", bold: true },
                          <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == customizedid)%><%{%>
                                { value: "Actual Date of Performer", bold: true },
                                { value: "Actual Date of Reviewer", bold: true },
                           <%}%>

                                <% else if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID != customizedid)%><%{%>
                                { value: "Actual Close Date Performer", bold: true },
                                { value: "Actual Close Date Reviewer", bold: true },
                           <%}%>

                                { value: "Original Performer Name", bold: true },
                                { value: "Original Reviewer Name", bold: true },
                                { value: "PerformerRemark", bold: true },
                                { value: "ReviewerRemark", bold: true },

                             <%if (RemoveColumn == true)%><%{%>
                                { value: "", bold: true },
                            <%}%>
                             <%else if (RemoveColumn == false)%><%{%>
                                { value: "Label", bold: true },
                            <%}%>
                                //{ value: "Label", bold: true },
                                { value: "Report Name", bold: true },
                      <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == customizedid)%><%{%>
                                { value: "Challan No", bold: true },
                                { value: "Challan Amount", bold: true },
                                { value: "Bank Name", bold: true },
                                { value: "Challan Paid Date", bold: true },
                                { value: "GST No.", bold: true },
                                { value: "Penalty Amount", bold: true },

                      <%}%>

                                { value: "Previous Due Date", bold: true },

                              <%if (ReturnamtFields==true)%><%{%>

                                { value: "Return Amount", bold: true },
                             <%}%>


                                <%if (ComplianceCT == true)%><%{%>
                                { value: "Compliance Type", bold: true },
                             <%}%>
                                { value: "License No.", bold: true },
                                { value: "IsDocMandatory", bold: true },
                            ]
                        }
                    ];

                var trs = grid.dataSource;
                var filteredDataSource = new kendo.data.DataSource({
                    data: trs.data(),
                    filter: trs.filter()
                });

                filteredDataSource.read();
                var data = filteredDataSource.view();
                for (var i = 0; i < data.length; i++) {
                    var dataItem = data[i];

                    if ($("#dropdownlistComplianceType1").val() == 0 || $("#dropdownlistComplianceType1").val() == 3) {
                        rows.push({
                            cells: [ // dataItem."Whatever Your Attributes Are"
                                { value: '' },
                                { value: dataItem.ComplianceID },
                             <%if (EntityField == true)%><%{%>
                                { value: dataItem.ParentName },
                            <%}%>
                                { value: dataItem.Branch },
                                { value: dataItem.ActName },
                                { value: dataItem.Sections },
                                { value: dataItem.ComCategoryName },
                                { value: dataItem.ComSubTypeName },
                                { value: dataItem.ShortForm },
                                { value: dataItem.PenaltyDescription },
                                { value: dataItem.ShortDescription },
                                { value: dataItem.Description },
                                { value: dataItem.ComplianceType },
                                { value: dataItem.DepartmentName },
                                { value: dataItem.EventName },
                                { value: dataItem.EventNature },
                                { value: dataItem.ForMonth },
                                { value: dataItem.ScheduledOn, format: "dd-MMM-yyyy" },
                                { value: dataItem.CloseDate, format: "dd-MMM-yyyy" },
                                { value: dataItem.Status },
                                { value: dataItem.RiskCategory },
                                { value: dataItem.PerformerName },
                                { value: dataItem.ReviewerName },
                                  <%if (RemoveColumn == true)%><%{%>
                                { value: '' },
                            <%}%>
                             <%else if (RemoveColumn == false)%><%{%>
                                { value: dataItem.ApproverName },
                            <%}%>
                                //{ value: dataItem.ApproverName },
                                { value: dataItem.PerformerDated, format: "dd-MMM-yyyy" },
                                { value: dataItem.ReviewerDated, format: "dd-MMM-yyyy" },
                                { value: dataItem.OriginalPerformerName },
                                { value: dataItem.OriginalReviewerName },
                                { value: dataItem.PerformerRemark },
                                { value: dataItem.ReviewerRemark },
                                  <%if (RemoveColumn == true)%><%{%>
                                { value: dataItem.Label },
                            <%}%>
                              <%else if (RemoveColumn == false)%><%{%>
                                { value: dataItem.Label },
                            <%}%>
                                { value: dataItem.ReportName },
                            <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == customizedid)%><%{%>
                                { value: '' },
                                { value: '' },
                                { value: '' },
                                { value: '' },
                                { value: '' },
                                { value: '' },

                            <%}%>
                                { value: '' },
                                  <%if (ReturnamtFields==true)%><%{%>

                                { value: '' },
                             <%}%>


                               <%if (ComplianceCT == true)%><%{%>
                                { value: dataItem.ComplianceTypeName },
                             <%}%>
                                { value: dataItem.LicenseNo },
                                { value: dataItem.IsDocMandatoryNonMandatory },//46
                            ]
                        });
                    }
                    else {
                        rows.push({
                            cells: [ // dataItem."Whatever Your Attributes Are"
                                { value: '' },
                                { value: dataItem.ComplianceID },
                             <%if (EntityField == true)%><%{%>
                                { value: dataItem.ParentName },
                            <%}%>
                                { value: dataItem.Branch },
                                { value: dataItem.ActName },
                                { value: dataItem.Sections },
                                { value: dataItem.ComCategoryName },
                                { value: dataItem.ComSubTypeName },
                                { value: dataItem.ShortForm },
                                { value: dataItem.PenaltyDescription },
                                { value: dataItem.ShortDescription },
                                { value: dataItem.Description },
                                { value: dataItem.ComplianceType },
                                { value: dataItem.DepartmentName },
                                { value: dataItem.EventName },
                                { value: dataItem.EventNature },
                                { value: dataItem.ForMonth },
                                { value: dataItem.ScheduledOn, format: "dd-MMM-yyyy" },
                                { value: dataItem.CloseDate, format: "dd-MMM-yyyy" },
                                { value: dataItem.Status },
                                { value: dataItem.RiskCategory },
                                { value: dataItem.PerformerName },
                                { value: dataItem.ReviewerName },
                                 <%if (RemoveColumn == true)%><%{%>
                                { value: '' },
                            <%}%>
                             <%else if (RemoveColumn == false)%><%{%>
                                { value: dataItem.ApproverName },
                            <%}%>

                                { value: dataItem.PerformerDated, format: "dd-MMM-yyyy" },
                                { value: dataItem.ReviewerDated, format: "dd-MMM-yyyy" },
                                { value: dataItem.OriginalPerformerName },
                                { value: dataItem.OriginalReviewerName },
                                { value: dataItem.PerformerRemark },
                                { value: dataItem.ReviewerRemark },
                                <%if (RemoveColumn == true)%><%{%>
                                { value: '' },
                            <%}%>
                            <%else if (RemoveColumn == false)%><%{%>
                                { value: dataItem.Label },
                            <%}%>

                                { value: dataItem.ReportName },
                            <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == customizedid)%><%{%>
                                { value: dataItem.ChallanNo },
                                { value: dataItem.ChallanAmount },
                                { value: dataItem.BankName },
                                { value: dataItem.Challanpaiddate, format: "dd-MMM-yyyy" },
                                { value: dataItem.GSTNumber },
                                { value: dataItem.Penalty },

                            <%}%>
                                { value: dataItem.Reviseduedate, format: "dd-MMM-yyyy" },

                           <%if (ReturnamtFields==true)%><%{%>

                                { value: dataItem.ValuesAsPerReturn },
                             <%}%>
                                <%if (ComplianceCT == true)%><%{%>
                                { value: dataItem.ComplianceTypeName },

                             <%}%>

                                { value: dataItem.LicenseNo },
                                { value: dataItem.IsDocMandatoryNonMandatory },//46
                            ]

                        });
                    }

                }
            <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == customizedid)%><%{%>

                var columnlength = rows[4].cells.length;
                for (var i = 4; i < rows.length; i++) {
                    for (var j = 0; j < columnlength; j++) {//39
                        rows[i].cells[j].borderBottom = "#000000";
                        rows[i].cells[j].borderLeft = "#000000";
                        rows[i].cells[j].borderRight = "#000000";
                        rows[i].cells[j].borderTop = "#000000";
                        rows[i].cells[j].hAlign = "left";
                        rows[i].cells[j].vAlign = "top";
                        rows[i].cells[j].wrap = true;

                        if (i != 4) {
                            rows[i].cells[0].value = i - 4;
                        }
                        if (i == 4) {
                            rows[4].cells[j].background = "#A9A9A9";
                        }
                    }
                }
            <%}%>
                <%else if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID != customizedid)%><%{%>

                var columnlength = rows[4].cells.length;
                for (var i = 4; i < rows.length; i++) {
                    for (var j = 0; j < columnlength; j++) {//33
                        rows[i].cells[j].borderBottom = "#000000";
                        rows[i].cells[j].borderLeft = "#000000";
                        rows[i].cells[j].borderRight = "#000000";
                        rows[i].cells[j].borderTop = "#000000";
                        rows[i].cells[j].hAlign = "left";
                        rows[i].cells[j].vAlign = "top";
                        rows[i].cells[j].wrap = true;

                        if (i != 4) {
                            rows[i].cells[0].value = i - 4;
                        }
                        if (i == 4) {
                            rows[4].cells[j].background = "#A9A9A9";
                        }
                    }
                }
            <%}%>

                <%else%><%{%>
                var columnlength = rows[4].cells.length;
                for (var i = 4; i < rows.length; i++) {
                    for (var j = 0; j < columnlength; j++) {//40
                        rows[i].cells[j].borderBottom = "#000000";
                        rows[i].cells[j].borderLeft = "#000000";
                        rows[i].cells[j].borderRight = "#000000";
                        rows[i].cells[j].borderTop = "#000000";
                        rows[i].cells[j].hAlign = "left";
                        rows[i].cells[j].vAlign = "top";
                        rows[i].cells[j].wrap = true;

                        if (i != 4) {
                            rows[i].cells[0].value = i - 4;
                        }
                        if (i == 4) {
                            rows[4].cells[j].background = "#A9A9A9";
                        }
                    }
                }
               <%}%>
                excelExport(rows, ReportName);
                e.preventDefault();
                return false;
            }

        }

        function exportReport(e) {
            if (TotalCount > <% =DetailReportThreshold%>) {

                var kendoWindow = $("<div />").kendoWindow({
                    title: "Export Report",
                    resizable: false,
                    modal: true,
                    //actions: [
                    //    "Close"
                    //]
                });

                kendoWindow.data("kendoWindow")
                    .content($("#delete-confirmation").html())
                    .center().open();

                kendoWindow.find(".delete-confirm,.delete-cancel")
                    .click(function () {

                        if ($(this).hasClass("delete-confirm")) {


                            var FlagRole = document.getElementById('FlagDetail').value;
                            var UId = document.getElementById('UId').value;
                            var customerId = document.getElementById('CustomerId').value;
                            var PathName = document.getElementById('Path').value;
                            var CustomerName = document.getElementById('CustName').value;

                            //location details
                            var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
                            var locationsdetails = [];
                            $.each(list1, function (i, v) {
                                locationsdetails.push(v);
                            });


                            //ComplianceType details
                            var list2 = $("#dropdownlistComplianceType").data("kendoDropDownTree")._values;
                            var ComplianceTypedetails = [];
                            $.each(list2, function (i, v) {
                                ComplianceTypedetails.push(v);
                            });

                            //Status details
                            var list2 = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
                            var Statusdetails = [];
                            $.each(list2, function (i, v) {
                                Statusdetails.push(v);
                            });

                            //risk Details
                            var Riskdetails = [];
                            var list3 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
                            $.each(list3, function (i, v) {
                                Riskdetails.push(v);
                            });

                            var userdetails = [];
                            var Actdetails = [];
                            var SequenceDataID = null;
                            if ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") {
                                SequenceDataID = $("#dropdownSequence").val();
                            }

                            $.ajax({
                                type: "GET",
                                url: '' + PathName + '//ExportReport/ExportReportRequest',
                                data: {
                                    UserId: UId, CustomerID: customerId,
                                    StatusFlag: JSON.stringify(ComplianceTypedetails),
                                    FlagIsApp: $("#dropdownlistUserRole").val(),
                                    MonthId: $("#dropdownlistTypePastdata").val(), FY: $("#dropdownFY").val(),
                                    CustomerName: CustomerName, location: JSON.stringify(locationsdetails),
                                    risk: JSON.stringify(Riskdetails),
                                    status: JSON.stringify(Statusdetails),
                                    userDetail: JSON.stringify(userdetails),
                                    actDetail: JSON.stringify(Actdetails),
                                    StartDateDetail: '',
                                    EndDateDetail: '',
                                    EventName: $("#dropdownEventName").val(),
                                    EventNature: $("#dropdownEventNature").val(),
                                    SequenceID: SequenceDataID,
                                    IsEmailRequestFlag: true,
                                    IsEmailSendFlag: false
                                },
                                success: function (response) {
                                    //if (response != "Error" && response != "No Record Found" && response != "") {
                                    //    window.location.href = '' + PathName + '/ExportReport/GetFile?userpath=' + response + '';
                                    //}
                                    //if (response == "No Record Found") {
                                    //    alert("No Record Found");
                                    //}
                                }
                            });

                            kendoWindow.data("kendoWindow").close();
                        }
                        else {

                            var FlagRole = document.getElementById('FlagDetail').value;
                            var UId = document.getElementById('UId').value;
                            var customerId = document.getElementById('CustomerId').value;
                            var PathName = document.getElementById('Path').value;
                            var CustomerName = document.getElementById('CustName').value;

                            //location details
                            var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
                            var locationsdetails = [];
                            $.each(list1, function (i, v) {
                                locationsdetails.push(v);
                            });


                            //ComplianceType details
                            var list2 = $("#dropdownlistComplianceType").data("kendoDropDownTree")._values;
                            var ComplianceTypedetails = [];
                            $.each(list2, function (i, v) {
                                ComplianceTypedetails.push(v);
                            });

                            //Status details
                            var list2 = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
                            var Statusdetails = [];
                            $.each(list2, function (i, v) {
                                Statusdetails.push(v);
                            });

                            //risk Details
                            var Riskdetails = [];
                            var list3 = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
                            $.each(list3, function (i, v) {
                                Riskdetails.push(v);
                            });

                            var userdetails = [];
                            var Actdetails = [];
                            var SequenceDataID = null;
                            if ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") {
                                SequenceDataID = $("#dropdownSequence").val();
                            }

                            $.ajax({
                                type: "GET",
                                url: '' + PathName + '//ExportReport/ExportReportRequest',
                                data: {
                                    UserId: UId, CustomerID: customerId,
                                    //StatusFlag: $("#dropdownlistComplianceType").val(),
                                    FlagIsApp: $("#dropdownlistUserRole").val(),
                                    MonthId: $("#dropdownlistTypePastdata").val(), FY: $("#dropdownFY").val(),
                                    CustomerName: CustomerName, location: JSON.stringify(locationsdetails),
                                    StatusFlag: JSON.stringify(ComplianceTypedetails),
                                    status: JSON.stringify(ComplianceTypedetails),
                                    risk: JSON.stringify(Riskdetails),
                                    status: JSON.stringify(Statusdetails),
                                    userDetail: JSON.stringify(userdetails),
                                    actDetail: JSON.stringify(Actdetails),
                                    StartDateDetail: '',
                                    EndDateDetail: '',
                                    EventName: $("#dropdownEventName").val(),
                                    EventNature: $("#dropdownEventNature").val(),
                                    SequenceID: SequenceDataID,
                                    IsEmailRequestFlag: false,
                                    IsEmailSendFlag: false
                                },
                                success: function (response) {

                                }
                            });

                            kendoWindow.data("kendoWindow").close();
                        }

                    })
                    .end();
                e.preventDefault();
                return false;

            }
            else {
                var ReportName = "Detailed Report";
                var customerName = document.getElementById('CustName').value;
                var todayDate = moment().format('DD-MMM-YYYY');
                var grid = $("#grid").getKendoGrid();
                var rows =
                    [
                        {
                            cells: [
                                { value: "Entity/ Location:", bold: true },
                                { value: customerName }
                            ]
                        },
                        {
                            cells: [
                                { value: "Report Name:", bold: true },
                                { value: ReportName }
                            ]
                        },
                        {
                            cells: [
                                { value: "Report Generated On:", bold: true },
                                { value: todayDate }
                            ]
                        },
                        {
                            cells: [
                                { value: "" }
                            ]
                        },
                        {
                            cells: [
                                { value: "Sr.No.", bold: true },
                                { value: "Compliance ID", bold: true },
                            <%if (EntityField == true)%><%{%>
                                { value: "Entity", bold: true },
                            <%}%>
                                { value: "Location", bold: true },
                                { value: "Act", bold: true },
                                { value: "Section", bold: true },
                                { value: "Category Name", bold: true },
                                { value: "Sub Category Name", bold: true },
                                { value: "Short Form", bold: true },
                                { value: "Penalty Description", bold: true },
                                { value: "Short Description", bold: true },
                                { value: "Detail Description", bold: true },
                                { value: "Compliance Type", bold: true },
                                { value: "Department Name", bold: true },
                                { value: "Event Name", bold: true },
                                { value: "Event Nature", bold: true },
                                { value: "Period", bold: true },
                                { value: "Due Date", bold: true },
                                { value: "Close Date", bold: true },
                                { value: "Status", bold: true },
                                { value: "Risk", bold: true },
                                { value: "Performer", bold: true },
                                { value: "Reviewer", bold: true },
                                  <%if (RemoveColumn == true)%><%{%>
                                { value: "", bold: true },
                             <%}%>
                            <%else if (RemoveColumn == false)%><%{%>
                                { value: "Approver", bold: true },
                             <%}%>
                                //{ value: "Approver", bold: true },
                          <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == customizedid)%><%{%>
                                { value: "Actual Date of Performer", bold: true },
                                { value: "Actual Date of Reviewer", bold: true },
                           <%}%>

                                <% else if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID != customizedid)%><%{%>
                                { value: "Actual Close Date Performer", bold: true },
                                { value: "Actual Close Date Reviewer", bold: true },
                           <%}%>

                                { value: "Original Performer Name", bold: true },
                                { value: "Original Reviewer Name", bold: true },
                                { value: "PerformerRemark", bold: true },
                                { value: "ReviewerRemark", bold: true },

                                 <%if (RemoveColumn == true)%><%{%>
                                { value: "", bold: true },
                            <%}%>
                             <%else if (RemoveColumn == false)%><%{%>
                                { value: "Label", bold: true },
                            <%}%>                               
                                //{ value: "Label", bold: true },
                                { value: "Report Name", bold: true },
                      <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == customizedid)%><%{%>
                                { value: "Challan No", bold: true },
                                { value: "Challan Amount", bold: true },
                                { value: "Bank Name", bold: true },
                                { value: "Challan Paid Date", bold: true },
                                { value: "GST No.", bold: true },
                                { value: "Penalty Amount", bold: true },

                      <%}%>

                                { value: "Previous Due Date", bold: true },

                              <%if (ReturnamtFields==true)%><%{%>

                                { value: "Return Amount", bold: true },
                             <%}%>


                                <%if (ComplianceCT == true)%><%{%>
                                { value: "Compliance Type", bold: true },
                             <%}%>

                                { value: "License No.", bold: true },
                                { value: "IsDocMandatory", bold: true },
                            ]
                        }
                    ];

                var trs = grid.dataSource;
                var filteredDataSource = new kendo.data.DataSource({
                    data: trs.data(),
                    filter: trs.filter()
                });

                filteredDataSource.read();
                var data = filteredDataSource.view();
                for (var i = 0; i < data.length; i++) {
                    var dataItem = data[i];

                    if ($("#dropdownlistComplianceType").val() == 0 || $("#dropdownlistComplianceType").val() == 3) {
                        rows.push({
                            cells: [ // dataItem."Whatever Your Attributes Are"
                                { value: '' },//1
                                { value: dataItem.ComplianceID },//2
                             <%if (EntityField == true)%><%{%>//
                                { value: dataItem.ParentName },//3
                            <%}%>
                                { value: dataItem.Branch },//5
                                { value: dataItem.ActName },//6
                                { value: dataItem.Sections },//7
                                { value: dataItem.ComCategoryName },//8
                                { value: dataItem.ComSubTypeName },//9
                                { value: dataItem.ShortForm },//10
                                { value: dataItem.PenaltyDescription },//11
                                { value: dataItem.ShortDescription },//12
                                { value: dataItem.Description },//13
                                { value: dataItem.ComplianceType },//14
                                { value: dataItem.DepartmentName },//15
                                { value: dataItem.EventName },//16
                                { value: dataItem.EventNature },//17
                                { value: dataItem.ForMonth },//18
                                { value: dataItem.ScheduledOn, format: "dd-MMM-yyyy" },//19
                                { value: dataItem.CloseDate, format: "dd-MMM-yyyy" },//20
                                { value: dataItem.Status },//21
                                { value: dataItem.RiskCategory },//22
                                { value: dataItem.PerformerName },//23
                                { value: dataItem.ReviewerName },//23
                               /* { value: dataItem.LicenseNo },*/
                               <%if (RemoveColumn == true)%><%{%>
                                { value: '' },//24
                            <%}%>
                             <%else if (RemoveColumn == false)%><%{%>
                                { value: dataItem.ApproverName },//25
                            <%}%>
                                //{ value: dataItem.ApproverName },

                                { value: dataItem.PerformerDated, format: "dd-MMM-yyyy" },//26
                                { value: dataItem.ReviewerDated, format: "dd-MMM-yyyy" },//29

                                { value: dataItem.OriginalPerformerName },//29
                                { value: dataItem.OriginalReviewerName },//30
                                { value: dataItem.PerformerRemark },//31
                                { value: dataItem.ReviewerRemark },//33
                                <%if (RemoveColumn == true)%><%{%>
                                { value: dataItem.Label },//34
                            <%}%>
                              <%else if (RemoveColumn == false)%><%{%>
                                { value: dataItem.Label },//35
                            <%}%>
                                //{ value: dataItem.Label },
                                { value: dataItem.ReportName },//36
                            <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == customizedid)%><%{%>
                                { value: '' },//37
                                { value: '' },//38
                                { value: '' },//39
                                { value: '' },//40
                                { value: '' },//41
                                { value: '' },//42

                            <%}%>
                                { value: '' },//43
                                 <%if (ReturnamtFields==true)%><%{%>

                                { value: '' },//44
                             <%}%>


                               <%if (ComplianceCT == true)%><%{%>
                                { value: dataItem.ComplianceTypeName },//45
                             <%}%>

                                { value: dataItem.LicenseNo },//46
                                { value: dataItem.IsDocMandatoryNonMandatory },//46

                            ]
                        });
                    }
                    else {
                        rows.push({
                            cells: [ // dataItem."Whatever Your Attributes Are"
                                { value: '' },
                                { value: dataItem.ComplianceID },
                             <%if (EntityField == true)%><%{%>
                                { value: dataItem.ParentName },
                            <%}%>
                                { value: dataItem.Branch },
                                { value: dataItem.ActName },
                                { value: dataItem.Sections },
                                { value: dataItem.ComCategoryName },
                                { value: dataItem.ComSubTypeName },
                                { value: dataItem.ShortForm },
                                { value: dataItem.PenaltyDescription },
                                { value: dataItem.ShortDescription },
                                { value: dataItem.Description },
                                { value: dataItem.ComplianceType },
                                { value: dataItem.DepartmentName },
                                { value: dataItem.EventName },
                                { value: dataItem.EventNature },
                                { value: dataItem.ForMonth },
                                { value: dataItem.ScheduledOn, format: "dd-MMM-yyyy" },
                                { value: dataItem.CloseDate, format: "dd-MMM-yyyy" },
                                { value: dataItem.Status },
                                { value: dataItem.RiskCategory },
                                { value: dataItem.PerformerName },
                                { value: dataItem.ReviewerName },//23
                              /*  { value: dataItem.LicenseNo },*/
                                 <%if (RemoveColumn == true)%><%{%>
                                { value: '' },//24
                            <%}%>
                             <%else if (RemoveColumn == false)%><%{%>
                                { value: dataItem.ApproverName },//25
                            <%}%>                                
                                { value: dataItem.PerformerDated, format: "dd-MMM-yyyy" },//26
                                { value: dataItem.ReviewerDated, format: "dd-MMM-yyyy" },//27
                                { value: dataItem.OriginalPerformerName },
                                { value: dataItem.OriginalReviewerName },
                                { value: dataItem.PerformerRemark },
                                { value: dataItem.ReviewerRemark },
                                <%if (RemoveColumn == true)%><%{%>
                                { value: '' },
                            <%}%>
                            <%else if (RemoveColumn == false)%><%{%>
                                { value: dataItem.Label },
                            <%}%>                              
                                { value: dataItem.ReportName },
                            <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == customizedid)%><%{%>
                                { value: dataItem.ChallanNo },
                                { value: dataItem.ChallanAmount },
                                { value: dataItem.BankName },
                                { value: dataItem.Challanpaiddate, format: "dd-MMM-yyyy" },
                                { value: dataItem.GSTNumber },
                                { value: dataItem.Penalty },

                            <%}%>
                                { value: dataItem.Reviseduedate, format: "dd-MMM-yyyy" },

                           <%if (ReturnamtFields==true)%><%{%>

                                { value: dataItem.ValuesAsPerReturn },
                                /* { value: dataItem.ParentName },*/
                             <%}%>
                                <%if (ComplianceCT == true)%><%{%>
                                { value: dataItem.ComplianceTypeName },
                                /* { value: dataItem.ParentName },*/
                             <%}%>

                                { value: dataItem.LicenseNo },
                                { value: dataItem.IsDocMandatoryNonMandatory },//46
                            ]

                        });
                    }

                }
            <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == customizedid)%><%{%>
                var columnlength = rows[4].cells.length;
                for (var i = 4; i < rows.length; i++) {
                    for (var j = 0; j < columnlength; j++) {//39
                        rows[i].cells[j].borderBottom = "#000000";
                        rows[i].cells[j].borderLeft = "#000000";
                        rows[i].cells[j].borderRight = "#000000";
                        rows[i].cells[j].borderTop = "#000000";
                        rows[i].cells[j].hAlign = "left";
                        rows[i].cells[j].vAlign = "top";
                        rows[i].cells[j].wrap = true;

                        if (i != 4) {
                            rows[i].cells[0].value = i - 4;
                        }
                        if (i == 4) {
                            rows[4].cells[j].background = "#A9A9A9";
                        }
                    }
                }
            <%}%>
                <%else if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID != customizedid)%><%{%>
                var columnlength = rows[4].cells.length;
                for (var i = 4; i < rows.length; i++) {
                    for (var j = 0; j < columnlength; j++) {//33
                        rows[i].cells[j].borderBottom = "#000000";
                        rows[i].cells[j].borderLeft = "#000000";
                        rows[i].cells[j].borderRight = "#000000";
                        rows[i].cells[j].borderTop = "#000000";
                        rows[i].cells[j].hAlign = "left";
                        rows[i].cells[j].vAlign = "top";
                        rows[i].cells[j].wrap = true;

                        if (i != 4) {
                            rows[i].cells[0].value = i - 4;
                        }
                        if (i == 4) {
                            rows[4].cells[j].background = "#A9A9A9";
                        }
                    }
                }
            <%}%>

                <%else%><%{%>
                var columnlength = rows[4].cells.length;
                for (var i = 4; i < rows.length; i++) {
                    for (var j = 0; j < columnlength; j++) {//39
                        rows[i].cells[j].borderBottom = "#000000";
                        rows[i].cells[j].borderLeft = "#000000";
                        rows[i].cells[j].borderRight = "#000000";
                        rows[i].cells[j].borderTop = "#000000";
                        rows[i].cells[j].hAlign = "left";
                        rows[i].cells[j].vAlign = "top";
                        rows[i].cells[j].wrap = true;

                        if (i != 4) {
                            rows[i].cells[0].value = i - 4;
                        }
                        if (i == 4) {
                            rows[4].cells[j].background = "#A9A9A9";
                        }
                    }
                }
               <%}%>
                excelExport(rows, ReportName);
                e.preventDefault();
                return false;
            }
        }

        function excelExport(rows, ReportName) {

            var FileName = "Detailed Report";
            var workbook = new kendo.ooxml.Workbook({
                sheets: [
                    {
                        columns: [
                            { width: 100 },
                            { autoWidth: true },
                            { width: 250 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 300 },
                            { width: 250 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                           <%if (RemoveColumn == true)%><%{%>
                            { width: 0 },
                            <%}%>
                             <%else if (RemoveColumn == true)%><%{%>
                            { width: 200 },
                            <%}%>
                            //{ width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                             <%if (RemoveColumn == true)%><%{%>
                            { width: 0 },
                             <%}%>
                             <%if (RemoveColumn == true)%><%{%>
                            { width: 200 },
                             <%}%>
                            //{ width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                            { width: 200 },
                        ],
                        title: FileName,
                        rows: rows
                    },
                ]
            });

            var nameOfPage = FileName;
            //var nameOfPage = "Test-1"; // insert here however you are getting name of screen
            kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });
            return false;
        }

        function OpenOverViewpupMain(scheduledonid, instanceid, ReportName) {

            $('#divOverView1').modal('show');
            $('#OverViews1').attr('width', '1250px');
            $('#OverViews1').attr('height', '600px');
            $('.modal-dialog').css('width', '1306px');


            if (ReportName == "Internal Checklist" || ReportName == "Internal CheckList" || ReportName == "Internal") {
                $('#OverViews1').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }
            else {
                $('#OverViews1').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }

            //if ($("#dropdownlistComplianceType1").val() == 5) {
            //    if (ReportName == "Internal Checklist" || ReportName == "Internal") {
            //        $('#OverViews1').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            //    }
            //    else {
            //        $('#OverViews1').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            //    }
            //}
            //if ($("#dropdownlistComplianceType1").val() == 0 || $("#dropdownlistComplianceType1").val() == 3) {

            //    $('#OverViews1').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            //}
            //else {
            //    $('#OverViews1').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            //}
        }

        function OpenOverViewpup(scheduledonid, instanceid, ReportName) {

            $('#divOverView').modal('show');
            $('#OverViews').attr('width', '98%');
            $('#OverViews').attr('height', '600px');
            $('.modal-dialog').css('width', '92%');
           
            if (ReportName == "Internal Checklist" || ReportName == "Internal CheckList" || ReportName == "Internal") {
                $('#OverViews').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }
            else {
                $('#OverViews').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            }

            //if ($("#dropdownlistComplianceType").val() == 5) {
            //    if (ReportName == "Internal Checklist" || ReportName == "Internal") {
            //        $('#OverViews').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            //    }
            //    else {
            //        $('#OverViews').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            //    }
            //}
            //if ($("#dropdownlistComplianceType").val() == 0 || $("#dropdownlistComplianceType").val() == 3) {

            //    $('#OverViews').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            //}
            //else {
            //    $('#OverViews').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
            //}
        }

        $("#newModelClose").on("click", function () {
            myWindow3.close();
        });

        function CloseClearPopup() {
            $('#OverViews1').attr('src', "../Common/blank.html");
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example">
        <div>
            <input id="Path" type="hidden" value="<% =Path%>" />
            <input id="CustomerId" type="hidden" value="<% =CustId%>" />
            <input id="CustName" type="hidden" value="<% =CustomerName%>" />
            <input id="RoleFlagCHK" type="hidden" value="<% =RoleFlag%>" />
            <input id="UId" type="hidden" value="<% =UId%>" />
            <input id="FlagDetail" type="hidden" value="<% =Falg%>" />

            <div style="margin: 0.5% 0 0.5%;">
             <div class="col-md-2" style="padding:0px;">
                <input id="dropdownlistUserRole" style="width: 100%;" /></div>
                <div id="divLocationMain" class="col-md-2" style="padding-left:15px;">
                <input id="dropdowntree" style="width: 100%;" /></div>
                <input id="dropdownlistComplianceType" style="width: 13%; margin-right: 0.8%;" />
                <input id="dropdownlistStatus" style="width: 13%; margin-right: 0.8%;" />
                <input id="txtSearchComplianceID" class="k-textbox" onkeydown="return (event.keyCode!=13);" placeholder="Compliance ID" style="width: 13%; margin-right: 0.8%" />
                <input id="dropdownDept" style="width: 13.5%; margin-right: 0.8%;" />
            </div>

            <div style="margin: 0.5% 0 0.5%;">
                <input id="dropdownlistTypePastdata" style="width: 15%; margin-right: 0.8%;" />
                <input id="dropdownlistRisk" style="width: 7.5%;margin-right: 5px;" />
                <div id="dvdropdownEventName" style="display: none;">
                    <input id="dropdownEventName" data-placeholder="Event Name" style="width: 100%; margin-right: 0.8%;" />
                </div>
                <div id="dvdropdownEventNature" style="display: none;">
                    <input id="dropdownEventNature" data-placeholder="Event Nature" style="width: 100%; margin-right: 0.8%;" />
                </div>
                <button type="button" id="export" style="float: right; height: 30px;" onclick="exportReport(event)"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
                <button type="button" id="Applybtn" style="float: right; height: 30px; margin-right: 0.8%;" onclick="Applybtndata(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
                <button type="button" id="ClearfilterMain" style="float: right; height: 30px; margin-right: 0.8%;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
                <button type="button" id="AdavanceSearch" style="float: right; height: 30px; margin-right: 0.8%;" onclick="OpenAdvanceSearch(event)"><span onclick="javascript:return false;"></span>Advanced Search</button>
            </div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black; display: none;" id="filtersstoryboard">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black; display: none;" id="filterrisk">&nbsp;</div>
             <div class="row" style="padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black; display: none;" id="filterComplianceType">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black; display: none;" id="filterstatus">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black; display: none;" id="filterdept">&nbsp;</div>
        </div>
    </div>

    <div id="grid"></div>

    <div class="row" style="padding-bottom: 5px;padding-top: 19px;">
        <label style="color: black;font-size: 21px;">Historical Reports Requested by You (Large records)</label>
    </div>

     <div id="Requestedgrid"></div>
    <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 1150px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div id="divAdvanceSearchModel" style="padding-top: 5px; z-index: 999; display: none;">

            <div style="margin: 0.5% 0 0.5%;">
                <div style="width: 11%;float: left;"">
                <input id="dropdownlistUserRole1" style="width: 100%;" /></div>
                <div id="divLocationAdv" style="padding-left: 15px;width: 19.3%;float: left;padding-right: 14px;">
                <input id="dropdowntree1" style="width: 100%;" /></div>
                <input id="dropdownFY" style="width: 11%; margin-right: 0.8%;" />
                <input id="dropdownlistStatus1" style="width: 13%; margin-right: 0.8%;" />
                <input id="txtSearchComplianceID1" class="k-textbox" onkeydown="return (event.keyCode!=13);" placeholder="Compliance ID" style="width: 10%; margin-right: 0.8%" />
                <input id="dropdownlistRisk1" style="width: 8%; margin-right: 0.8%;" />
                <input id="Startdatepicker" placeholder="Start Date" style="width: 10%; margin-right: 0.8%;" />
                <input id="Lastdatepicker" placeholder="End Date" style="width: 10%;" />
            </div>
            <div style="margin: 0.5% 0 0.5%;">
                <input id="dropdownlistComplianceType1" style="width: 11%; margin-right: 0.8%;" />
                <input id="dropdownACT" style="width: 19.5%; margin-right: 0.8%;" />
                <input id="dropdownPastData" style="width: 11%; margin-right: 0.8%;" />
                <%if (RoleFlag == 1)%>
                <%{%>
                <input id="dropdownUser" data-placeholder="User" style="width: 13%; margin-right: 0.8%;" />
                <%}%>
                <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%><%{%>
                <input id="dropdownSequence" style="width: 10%; margin-right: 0.8%;" />
                <%}%>
                 <button type="button" id="Applybtn1" style="float: right; height: 23px;margin-right: 0.8%;" onclick="Applybtndata1(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
               <button type="button" id="Clearfilter" style="float: right; height: 23px;margin-right: 12px;" onclick="ClearAllFilter()"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
               <button type="button" id="exportAdvanced" style="height: 23px;float: right;margin-right: 0.8%;" onclick="exportReportAdvanced(event)" data-placement="bottom"><span class="k-icon k-i-excel k-grid-edit3" style="margin-right: 2px;"></span>Export</button>
            </div>
            <div id="dvdropdownEventName1" style="display: none;padding-bottom: 7px;">
                <input id="dropdownEventName1" data-placeholder="Event Name" style="width: 16%; margin-right: 0.8%;" />
                <input id="dropdownEventNature1" data-placeholder="Event Nature" style="width: 14.5%;" />
            </div>
            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboard1">&nbsp;</div>
            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterComplianceType1">&nbsp;</div>
            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterrisk1">&nbsp;</div>
            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterUser">&nbsp;</div>
            <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterstatus1">&nbsp;</div>
            <div id="grid1"></div>
            <div class="modal fade" id="divOverView1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="OverViews1" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <iframe id="downloadfile" src="about:blank" width="0" height="0"></iframe>

</asp:Content>