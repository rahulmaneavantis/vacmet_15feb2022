﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Configuration;
using Ionic.Zip;
using com.VirtuosoITech.ComplianceManagement.Portal.Properties;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class RevComplianceStatusTransaction : System.Web.UI.Page
    {
        public static string LicenseDocPath = "";
        static string sampleFormPath1 = "";
        public static string CompDocReviewPath = "";
        public static string TaskDocViewPath = "";
        protected static string KendoPath;
        protected static int CustId;
        protected static int UId;
        protected static int compInstanceID; 
        protected static string Authorization;
        protected static string status;
        public string Penaltytextchange;
        public string ISmail;
        protected void Page_Load(object sender, EventArgs e)
        {

            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            ISmail = "0";
            try
            {
                ISmail = Convert.ToString(Request.QueryString["ISM"]);
                if (!string.IsNullOrEmpty(ISmail) && ISmail == "1")
                {
                    lnkgotoportal.Visible = true;
                }
                else
                {
                    lnkgotoportal.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "ISmail", MethodBase.GetCurrentMethod().Name);
                ISmail = "0";
            }
            if (!IsPostBack) {
                KendoPath = ConfigurationManager.AppSettings["KendoPathApp"];
                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                compInstanceID = Convert.ToInt32(Request.QueryString["CID"].ToString());
                //status = Convert.ToString(Request.QueryString["status"].ToString());
                Penaltytextchange = ConfigurationManager.AppSettings["IsPenaltyTextchanged"];
                if(Penaltytextchange==Convert.ToString(AuthenticationHelper.CustomerID))
                {
                    lblpenaltytextchange.InnerText = "Penalty/Additional Fee(INR)";
                }
                string IsActiveCustomer = Convert.ToString(ConfigurationManager.AppSettings["IsLicensePermanantActive"]);

                if (IsActiveCustomer == Convert.ToString(AuthenticationHelper.CustomerID))
                {
                    divChkIsActive.Visible = true;
                } 
                OpenTransactionPage(Convert.ToInt32(Request.QueryString["SOID"].ToString()), Convert.ToInt32(Request.QueryString["CID"].ToString()));

                //int LicID = Convert.ToInt32(Session["LicenseID"]);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var LicID = (from row in entities.Lic_tbl_LicenseComplianceInstanceMapping
                                 where row.ComplianceInstanceID == compInstanceID
                                 select row.LicenseID).FirstOrDefault();

                    var LicRecentStatus = (from row in entities.RecentLicenseTransactionViews
                                           where row.LicenseID == LicID
                                           select row.StatusID).FirstOrDefault();


                    if (LicRecentStatus == 9 || LicRecentStatus == 9)
                    {
                        btnSave1.Attributes.Add("disabled", "disabled");
                        rdbtnStatus1.Attributes.Add("disabled", "disabled");
                        tbxRemarks1.Attributes.Add("disabled", "disabled");
                        tbxDate1.Attributes.Add("disabled", "disabled");
                        txtFileno.Attributes.Add("disabled", "disabled");
                        txtphysicalLocation.Attributes.Add("disabled", "disabled");
                        txtCost.Attributes.Add("disabled", "disabled");
                        btnReject1.Attributes.Add("disabled", "disabled");
                        // btnSave.Enabled = false;
                    }
                    
                }

            }
        }
        protected void lnkgotoportal_Click(object sender, EventArgs e)
        {
            try
            {
                ProductMappingStructure _obj = new ProductMappingStructure();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _obj.ReAuthenticate_UserNew();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        private void BindTransactionDetails(int ScheduledOnID, int complianceInstanceID)
        {
            var CSOID = Convert.ToInt64(ScheduledOnID);
            var RCT = Business.ComplianceManagement.GetCurrentStatusByComplianceID((int)CSOID);
            var LRCT = Business.ComplianceManagement.GetCurrentStatusOfLicenseByScheduleOnID((int)CSOID);
            if (RCT.ComplianceStatusID == 4 || RCT.ComplianceStatusID == 5)
            {
                status = LRCT.Status;
                if (LRCT.Status == "Registered" || LRCT.Status == "Validity Expired")
                {

                }
                else
                {
                    btnSave1.Visible = false;
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "This compliance is already closed.";
                }
            }
            //else
            //{
            //    btnSave1.Visible = true;
                try
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        lblLicenseType.Text = string.Empty;
                        lblLicenseNumber.Text = string.Empty;
                        lblLicenseTitle.Text = string.Empty;
                        lblApplicationDate.Text = string.Empty;
                        lblStartdate.Text = string.Empty;
                        lblEndDate.Text = string.Empty;

                        txtLicenseTitle.Text = string.Empty;
                        txtLicenseNo.Text = string.Empty;
                        txtStartDate.Text = string.Empty;
                        txtEndDate.Text = string.Empty;

                        lblPenalty1.Text = string.Empty;
                        lblRisk1.Text = string.Empty;
                        lbDownloadSample1.Text = string.Empty;
                        hdnfieldPanaltyDisplay.Value = "true";
                        ViewState["complianceInstanceID"] = ScheduledOnID;
                        int customerbranchID = -1;

                        var AllComData = Business.ComplianceManagement.GetPeriodBranchLocation(ScheduledOnID, complianceInstanceID);
                        var complianceInfo = Business.ComplianceManagement.GetComplianceByInstanceID(ScheduledOnID);
                        var complianceForm = Business.ComplianceManagement.GetComplianceFormByID(complianceInfo.ID);
                        var RecentComplianceTransaction = Business.ComplianceManagement.GetCurrentStatusByComplianceID(ScheduledOnID);
                        if (AllComData != null)
                        {
                            customerbranchID = AllComData.CustomerBranchID;
                            lblLocation.Text = AllComData.Branch;
                            lblDueDate.Text = Convert.ToDateTime(AllComData.PerformerScheduledOn).ToString("dd-MMM-yyyy");
                            lblPeriod.Text = AllComData.ForMonth;
                        }
                        var showHideButton = BindSubTasks(ScheduledOnID, 4, customerbranchID);
                        if (complianceInfo != null)
                        {
                        #region
                        var complianceCategoryInfo = Business.ComplianceManagement.GetComplianceCategoryByInstanceID(complianceInfo.ActID);
                        var LicenseID = (from row in entities.Lic_tbl_LicenseComplianceInstanceMapping
                                             where row.ComplianceInstanceID == complianceInstanceID
                                             select row.LicenseID).FirstOrDefault();

                            var MasterTransction = LicenseMgmt.GetPreviousLicenseDetail(LicenseID);
                            if (MasterTransction != null)
                            {
                                Session["LicenseID"] = MasterTransction.LicenseID;
                                Session["IsPermanantActive"] = MasterTransction.IsPermanantActive;
                                lblLicenseType.Text = MasterTransction.LicensetypeName;
                                lblLicenseNumber.Text = MasterTransction.LicenseNo;
                                lblLicenseTitle.Text = MasterTransction.Licensetitle;
                                lblApplicationDate.Text = Convert.ToDateTime(MasterTransction.ApplicationDate).ToString("dd-MMM-yyyy");
                                lblStartdate.Text = Convert.ToDateTime(MasterTransction.StartDate).ToString("dd-MMM-yyyy");
                                lblEndDate.Text = Convert.ToDateTime(MasterTransction.EndDate).ToString("dd-MMM-yyyy");
                                txtCost.Text = Convert.ToString(MasterTransction.Cost);
                                ChkIsActive.Checked = MasterTransction.IsPermanantActive;
                            var LicensedocumentVersionData = LicenseDocumentManagement.GetFileData(LicenseID, -1).Select(x => new
                                {
                                    ID = x.ID,
                                    ScheduledOnID = x.ScheduledOnID,
                                    LicenseID = x.LicenseID,
                                    FileID = x.FileID,
                                    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                    VersionDate = x.VersionDate,
                                    VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                                    ISLink = x.ISLink,
                                    FilePath = x.FilePath,
                                    FileName = x.FileName
                                }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();


                                rptLicenseVersion.DataSource = LicensedocumentVersionData;
                                rptLicenseVersion.DataBind();

                                rptLicenseDocumnets.DataSource = null;
                                rptLicenseDocumnets.DataBind();
                            }

                            var recentlicenstransactionStatus = (from row in entities.RecentLicenseTransactionViews
                                                                 where row.LicenseID == LicenseID
                                                                 select row).FirstOrDefault();

                            if (recentlicenstransactionStatus.StatusID == 7)
                            {
                                fieldsetRenewal.Visible = true;
                                var instancedetails = (from row in entities.Lic_tbl_LicenseInstance
                                                       where row.ID == LicenseID
                                                       select row).FirstOrDefault();
                                if (instancedetails != null)
                                {
                                    txtLicenseTitle.Text = instancedetails.LicenseTitle;
                                    txtLicenseNo.Text = instancedetails.LicenseNo;
                                    txtStartDate.Text = instancedetails.StartDate != null ? instancedetails.StartDate.Value.ToString("dd-MM-yyyy") : " ";
                                    txtEndDate.Text = instancedetails.EndDate != null ? instancedetails.EndDate.Value.ToString("dd-MM-yyyy") : " ";
                                    hdnlicremindbeforenoofdays.Value = instancedetails.RemindBeforeNoOfDays.ToString();
                                    hdnlicensetypeid.Value = instancedetails.LicenseTypeID.ToString();
                                    ChkIsActive.Checked = instancedetails.IsPermanantActive;
                            }
                            }
                            else
                            {
                                fieldsetRenewal.Visible = false;
                            }
                            lblComplianceID1.Text = Convert.ToString(complianceInfo.ID);
                            lblCategory.Text = Convert.ToString(complianceCategoryInfo.Name);
                            lnkSampleForm1.Text = Convert.ToString(complianceInfo.SampleFormLink);
                            lblFrequency1.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1));
                            lblRefrenceText1.Text = WebUtility.HtmlEncode(complianceInfo.ReferenceMaterialText);
                            lblComplianceDiscription1.Text = complianceInfo.ShortDescription;
                            lblDetailedDiscription1.Text = complianceInfo.Description;
                            lblPenalty1.Text = complianceInfo.PenaltyDescription;
                            string risk = Business.ComplianceManagement.GetRiskType(complianceInfo, complianceInstanceID);
                            lblRisk1.Text = risk;
                            lblRiskType1.Text = Business.ComplianceManagement.GetRisk(complianceInfo, complianceInstanceID);
                            var LICInfo = Business.ActManagement.GetByLICID(LicenseID);
                            txtFileno.Text = LICInfo.FileNO;
                            txtphysicalLocation.Text = LICInfo.PhysicalLocation;

                            if (risk == "HIGH")
                            {
                                divRiskType1.Attributes["style"] = "background-color:red;";
                            }
                            else if (risk == "CRITICAL")
                            {
                                divRiskType1.Attributes["style"] = "background-color:red;";
                            }
                            else if (risk == "MEDIUM")
                            {
                                divRiskType1.Attributes["style"] = "background-color:yellow;";
                            }
                            else if (risk == "LOW")
                            {
                                divRiskType1.Attributes["style"] = "background-color:green;";
                            }

                            var ActInfo = Business.ActManagement.GetByID(complianceInfo.ActID);
                            if (ActInfo != null)
                            {
                                lblActName1.Text = ActInfo.Name;
                            }
                            if (RecentComplianceTransaction.IsPenaltySave == false && RecentComplianceTransaction.Status == "Complied Delayed but pending review")
                            {
                                //Intrest and penalty save                           
                                chkPenaltySaveReview.Checked = (bool)RecentComplianceTransaction.IsPenaltySave;
                                txtInterestReview.Text = Convert.ToString(RecentComplianceTransaction.Interest);
                                txtPenaltyReview.Text = Convert.ToString(RecentComplianceTransaction.Penalty);
                            }
                            else
                            {
                                if (RecentComplianceTransaction.IsPenaltySave == null)
                                {
                                    //Earlier compliance performed 
                                    chkPenaltySaveReview.Checked = false;
                                    txtInterestReview.Text = "";
                                    txtPenaltyReview.Text = "";
                                }
                                else
                                {
                                    if (RecentComplianceTransaction.Status == "Complied but pending review")
                                    {
                                        //performed in Time
                                        chkPenaltySaveReview.Checked = false;
                                        txtInterestReview.Text = "0";
                                        txtPenaltyReview.Text = "0";
                                    }
                                    //else if (RecentComplianceTransaction.Status == "Submitted For Interim Review")
                                    //{
                                    //    //performed Interim
                                    //    chkPenaltySaveReview.Checked = false;                                    
                                    //    txtInterestReview.Text = "0";
                                    //    txtPenaltyReview.Text = "0";
                                    //}
                                    else
                                    {
                                        //Penalty and intrest not known this moment
                                        chkPenaltySaveReview.Checked = (bool)RecentComplianceTransaction.IsPenaltySave;
                                        txtInterestReview.Text = "0";
                                        txtPenaltyReview.Text = "0";
                                        txtInterestReview.Attributes.Add("disabled", "disabled");
                                        txtPenaltyReview.Attributes.Add("disabled", "disabled");
                                    }
                                }
                            }
                            var data = Business.ComplianceManagement.CheckMonetary(Convert.ToInt32(lblComplianceID1.Text));
                            if (data == null)
                            {
                                fieldsetpenalty.Visible = false;
                            }
                            else
                            {
                                if (data.NonComplianceType == 1)
                                {
                                    fieldsetpenalty.Visible = false;
                                }
                                else
                                {
                                    if (RecentComplianceTransaction.Status == "Complied Delayed but pending review")
                                    {
                                        // fieldsetpenalty.Attributes.Add("style", "display:block");
                                        fieldsetpenalty.Attributes["style"] = "border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%; Display:block;";
                                    }
                                    else
                                    {
                                        fieldsetpenalty.Attributes.Add("style", "display:none");
                                    }
                                }
                            }
                            lblNatureofcompliance.Text = "";
                            txtValueAsPerSystem.Text = "";
                            txtValueAsPerReturn.Text = "";
                            txtLiabilityPaid.Text = "";
                            txtDiffAB.Text = "";
                            txtDiffBC.Text = "";

                            if (complianceInfo.NatureOfCompliance != null)
                            {
                                lblNatureofcomplianceID.Text = Convert.ToString(complianceInfo.NatureOfCompliance);
                                string Nature = Business.ComplianceManagement.GetNatureOfComplianceFromID(Convert.ToInt32(complianceInfo.NatureOfCompliance));
                                lblNatureofcompliance.Text = Nature.ToString();

                                txtValueAsPerSystem.Text = Convert.ToString(RecentComplianceTransaction.ValuesAsPerSystem);
                                txtValueAsPerReturn.Text = Convert.ToString(RecentComplianceTransaction.ValuesAsPerReturn);
                                txtLiabilityPaid.Text = Convert.ToString(RecentComplianceTransaction.LiabilityPaid);

                                if (complianceInfo.NatureOfCompliance == 1)
                                {
                                    fieldsetTDS.Visible = true;
                                    lblValueAsPerSystem.InnerText = "Liability as per system (A)";
                                    lblValueAsPerReturn.InnerText = "Liability as per return (B)";
                                    trreturn.Visible = true;

                                    try
                                    {
                                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "Diff1", "Diff1();", true);
                                    }
                                    catch (Exception ex)
                                    {
                                    }

                                }
                                else if (complianceInfo.NatureOfCompliance == 0)
                                {
                                    fieldsetTDS.Visible = true;
                                    lblValueAsPerSystem.InnerText = "Values as per system (A)";
                                    lblValueAsPerReturn.InnerText = "Values as per return (B)";
                                    trreturn.Visible = false;

                                    try
                                    {
                                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "Diff1", "Diff1();", true);
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                }
                                else
                                {
                                    fieldsetTDS.Visible = false;
                                }
                            }
                            else
                            {
                                fieldsetTDS.Visible = false;
                            }

                            lblRule1.Text = complianceInfo.Sections;
                            lblFormNumber1.Text = complianceInfo.RequiredForms;

                            #endregion
                        }
                        if (complianceInfo.UploadDocument == true && complianceForm != null)
                        {
                            lbDownloadSample1.Text = "Download";
                            lbDownloadSample1.CommandArgument = complianceForm.ComplianceID.ToString();

                            sampleFormPath1 = complianceForm.FilePath;
                            sampleFormPath1 = sampleFormPath1.Substring(2, sampleFormPath1.Length - 2);
                            lblpathsample1.Text = sampleFormPath1;
                            lnkViewSampleForm1.Visible = true;
                            lblSlash1.Visible = true;
                            lblNote.Visible = true;
                        }
                        else
                        {
                            lblNote.Visible = false;
                            lblSlash1.Visible = false;
                            lnkViewSampleForm1.Visible = false;
                            sampleFormPath1 = "";
                        }
                        //var documentVersionData = DocumentManagement.GetFileData1(Convert.ToInt32(RecentComplianceTransaction.ComplianceScheduleOnID)).Select(x => new
                        //{
                        //    ID = x.ID,
                        //    ScheduledOnID = x.ScheduledOnID,
                        //    FileID = x.FileID,
                        //    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                        //    VersionDate = x.VersionDate,
                        //    VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                        //    ISLink = x.ISLink
                        //}).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

                        var documentVersionData = DocumentManagement.GetFileData1(Convert.ToInt32(RCT.ComplianceScheduleOnID)).Select(x => new
                        {
                            ID = x.ID,
                            ScheduledOnID = x.ScheduledOnID,
                            FileID = x.FileID,
                            Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                            VersionDate = x.VersionDate,
                            VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                            ISLink = x.ISLink,
                            FilePath = x.FilePath,
                            FileName = x.FileName
                        }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();

                        rptComplianceVersion.DataSource = documentVersionData;
                        rptComplianceVersion.DataBind();

                        rptComplianceDocumnets.DataSource = null;
                        rptComplianceDocumnets.DataBind();

                        rptWorkingFiles.DataSource = null;
                        rptWorkingFiles.DataBind();

                        lblReviewDoc.Text = "Review License Document";
                        BindStatusList(Convert.ToInt32(RecentComplianceTransaction.ComplianceStatusID));

                        //added by rahul on 17 JAN 2019
                        if (RecentComplianceTransaction.Status == "Complied but pending review")
                        {
                            //Complied but pending review
                            rdbtnStatus1.SelectedValue = "4";
                        }
                        else
                        {
                            //Complied Delayed but pending review
                            rdbtnStatus1.SelectedValue = "5";
                        }
                        btnReject1.Visible = true;
                        btnSave1.Text = "Approve";
                        BindTransactions(ScheduledOnID);
                        tbxRemarks1.Text = string.Empty;
                        tbxDate1.Text = RecentComplianceTransaction.StatusChangedOn != null ? RecentComplianceTransaction.StatusChangedOn.Value.ToString("dd-MM-yyyy") : " ";
                        hdnComplianceInstanceID.Value = complianceInstanceID.ToString();
                        hdnComplianceScheduledOnId.Value = ScheduledOnID.ToString();

                        if (complianceInfo.EventFlag == true)
                        {
                            if (complianceInfo.UpDocs == true)
                            {
                                rdbtnStatus1.Attributes.Add("disabled", "disabled");
                                tbxRemarks1.Attributes.Add("disabled", "disabled");
                                tbxDate1.Attributes.Add("disabled", "disabled");
                                txtFileno.Attributes.Add("disabled", "disabled");
                                txtphysicalLocation.Attributes.Add("disabled", "disabled");
                                txtCost.Attributes.Add("disabled", "disabled");
                                btnSave1.Attributes.Add("disabled", "disabled");
                                btnReject1.Attributes.Add("disabled", "disabled");
                                fieldsetdownloadreview.Visible = true;
                            }
                            else
                            {
                                rdbtnStatus1.Attributes.Remove("disabled");
                                tbxRemarks1.Attributes.Remove("disabled");
                                tbxDate1.Attributes.Remove("disabled");
                                btnSave1.Attributes.Remove("disabled");
                                btnReject1.Attributes.Remove("disabled");
                                txtFileno.Attributes.Remove("disabled");
                                txtphysicalLocation.Attributes.Remove("disabled");
                                txtCost.Attributes.Remove("disabled");


                                tbxRemarks1.Attributes.Add("enabled", "enabled");
                                tbxRemarks1.Attributes.Add("enabled", "enabled");
                                tbxDate1.Attributes.Add("enabled", "enabled");
                                btnSave1.Attributes.Add("enabled", "enabled");
                                btnReject1.Attributes.Add("enabled", "enabled");
                                txtFileno.Attributes.Add("enabled", "enabled");
                                txtphysicalLocation.Attributes.Add("enabled", "enabled");
                                txtCost.Attributes.Add("enabled", "enabled");

                            if (documentVersionData.Count > 0)
                                {
                                    fieldsetdownloadreview.Visible = true;
                                }
                                else
                                {
                                    fieldsetdownloadreview.Visible = false;
                                }
                            }
                        }
                        else
                        {
                            rdbtnStatus1.Attributes.Add("disabled", "disabled");
                            tbxRemarks1.Attributes.Add("disabled", "disabled");
                            tbxDate1.Attributes.Add("disabled", "disabled");
                            btnSave1.Attributes.Add("disabled", "disabled");
                            btnReject1.Attributes.Add("disabled", "disabled");
                        }
                        upComplianceDetails1.Update();
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            //}
        }


        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                else
                    DownloadFile(Convert.ToInt32(e.CommandArgument));                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                }
                else
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                    List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();
                    ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();

                    if (commandArgs[1].Equals("1.0"))
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                        if (ComplianceFileData.Count <= 0)
                        {
                            ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    }

                    if (e.CommandName.Equals("version"))
                    {
                        if (e.CommandName.Equals("version"))
                        {
                            rptComplianceDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                            rptComplianceDocumnets.DataBind();

                            rptWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                            rptWorkingFiles.DataBind();

                            var documentVersionData = ComplianceDocument.Select(x => new
                            {
                                //ID = x.ID,
                                //ScheduledOnID = x.ScheduledOnID,
                                //Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                //VersionDate = x.VersionDate,
                                //VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,

                                ID = x.ID,
                                ScheduledOnID = x.ScheduledOnID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                                ISLink = x.ISLink,
                                FilePath = x.FilePath,
                                FileName = x.FileName,
                                FileID = x.FileID
                            }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();

                            rptComplianceVersion.DataSource = documentVersionData;
                            rptComplianceVersion.DataBind();

                            upComplianceDetails1.Update();


                        }
                    }
                    else if (e.CommandName.Equals("Download"))
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
                                ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);
                                ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }


                                if (ComplianceFileData.Count > 0)
                                {
                                    foreach (var item in ComplianceFileData)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }

                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);


                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            string str = filename[0] + i + "." + ext;
                                            ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                            i++;
                                        }
                                    }
                                }
                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {

                                var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
                                ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);

                                ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                                if (ComplianceFileData.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {

                                        //comment by rahul on 20 JAN 2017
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        //string filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            string[] commandArg = e.CommandArgument.ToString().Split(',');
                            List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();
                            Session["ScheduleOnID"] = commandArg[0];

                            if (CMPDocuments != null)
                            {
                                CMPDocuments = CMPDocuments.Where(x => x.ISLink == false).ToList();
                                List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();

                                if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                                    entityData.Version = "1.0";
                                    entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptComplianceVersionView.DataBind();

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }

                                    foreach (var file in ComplianceFileData)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(file.FileName);

                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageReviewer1.Text = "";
                                                lblMessageReviewer1.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                CompDocReviewPath = filePath1;
                                                CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                                lblMessageReviewer1.Text = "";

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewReviewer('" + CompDocReviewPath + "');", true);
                                            }
                                        }
                                        else
                                        {
                                            lblMessageReviewer1.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            string[] commandArg = e.CommandArgument.ToString().Split(',');
                            List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();
                            //Business.ComplianceManagement.GetDocumnetList(Convert.ToInt64(commandArg[0]), Convert.ToInt64(commandArg[1]));
                            Session["ScheduleOnID"] = commandArg[0];
                            //Session["TransactionID"] = commandArg[1];

                            if (CMPDocuments != null)
                            {
                                CMPDocuments = CMPDocuments.Where(x => x.ISLink == false).ToList();
                                List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                                if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                                    entityData.Version = "1.0";
                                    entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    rptComplianceVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptComplianceVersionView.DataBind();
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageReviewer1.Text = "";
                                                lblMessageReviewer1.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();
                                                CompDocReviewPath = FileName;

                                                CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);

                                                lblMessageReviewer1.Text = "";

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewReviewer('" + CompDocReviewPath + "');", true);
                                            }
                                        }
                                        else
                                        {
                                            lblMessageReviewer1.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected bool SaveData(string FileName, byte[] Data)
        {
            BinaryWriter Writer = null;
            //string Name = @"C:\temp\yourfile.name";

            try
            {
                // Create a new stream to write to the file
                Writer = new BinaryWriter(File.OpenWrite(FileName));

                // Writer raw data                
                Writer.Write(Data);
                Writer.Flush();
                Writer.Close();
            }
            catch
            {
                //...
                return false;
            }

            return true;
        }
        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                else
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            //System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            //sortImage.ImageAlign = ImageAlign.AbsMiddle;

            //if (direction == SortDirection.Ascending)
            //{
            //    sortImage.ImageUrl = "../Images/SortAsc.gif";
            //    sortImage.AlternateText = "Ascending Order";
            //}
            //else
            //{
            //    sortImage.ImageUrl = "../Images/SortDesc.gif";
            //    sortImage.AlternateText = "Descending Order";
            //}
            //headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdTransactionHistory_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var complianceTransactionHistory = Business.ComplianceManagement.GetAllTransactionLog(Convert.ToInt32(ViewState["complianceInstanceID"]));
                if (direction == SortDirection.Ascending)
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdTransactionHistory.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndexHistory"] = grdTransactionHistory.Columns.IndexOf(field);
                    }
                }

                grdTransactionHistory.DataSource = complianceTransactionHistory;
                grdTransactionHistory.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTransactionHistory.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["complianceInstanceID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactions(int ScheduledOnID)
        {
            try
            {
                // throw new NotImplementedException();
                grdTransactionHistory.DataSource = Business.ComplianceManagement.GetAllTransactionLog(ScheduledOnID);
                grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindEscalationRevStatusList()
        {
            try
            {
                rdbtnStatus1.Items.Clear();
                var statusList = ComplianceStatusManagement.GetEscalationRevStatusList();

                foreach (ComplianceStatu st in statusList)
                {
                    rdbtnStatus1.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindStatusList(int statusID)
        {
            try
            {

                rdbtnStatus1.Items.Clear();
                var statusList = ComplianceStatusManagement.GetStatusList();

                List<ComplianceStatu> allowedStatusList = null;

                List<ComplianceStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.Name).ToList();

                foreach (ComplianceStatu st in allowedStatusList)
                {
                    if (!(st.ID == 6))
                    {
                        rdbtnStatus1.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                    }
                }

                lblStatus1.Visible = allowedStatusList.Count > 0 ? true : false;
                divDated1.Visible = allowedStatusList.Count > 0 ? true : false;
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<ComplianceAssignment> GetAssignedUsers(int ComplianceInstanceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var AssignedUsers = (from row in entities.ComplianceAssignments
                                     where row.ComplianceInstanceID == ComplianceInstanceID
                                     select row).GroupBy(entry => entry.RoleID).Select(entry => entry.FirstOrDefault()).ToList();

                return AssignedUsers;
            }
        }
        protected void btnSave1_Click(object sender, EventArgs e)
        {
           
            var CSOID = Convert.ToInt64(hdnComplianceScheduledOnId.Value);
           
            var RecentComplianceTransaction = Business.ComplianceManagement.GetCurrentStatusByComplianceID((int)CSOID);
            if (RecentComplianceTransaction.ComplianceStatusID == 4 || RecentComplianceTransaction.ComplianceStatusID == 5)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "This compliance already performed.";
            }
            else
            {
                #region Save Code
                try
                {
                    if (txtInterestReview.Text == "")
                        txtInterestReview.Text = "0";
                    if (txtPenaltyReview.Text == "")
                        txtPenaltyReview.Text = "0";
                    if (txtValueAsPerSystem.Text == "")
                        txtValueAsPerSystem.Text = "0";
                    if (txtValueAsPerReturn.Text == "")
                        txtValueAsPerReturn.Text = "0";
                    if (txtLiabilityPaid.Text == "")
                        txtLiabilityPaid.Text = "0";
                    if (lblNatureofcomplianceID.Text == "")
                        lblNatureofcomplianceID.Text = "8";

                    int StatusID = 0;
                    if (lblStatus1.Visible)
                        StatusID = Convert.ToInt32(rdbtnStatus1.SelectedValue);
                    else
                        StatusID = Convert.ToInt32(ComplianceManagement.Business.ComplianceManagement.GetClosedTransaction(Convert.ToInt32(hdnComplianceScheduledOnId.Value)).ComplianceStatusID);

                    #region Performer Penalty Update

                    var PenaltySubmit = "";
                    if (rdbtnStatus1.SelectedValue == "5")
                    {
                        if (chkPenaltySaveReview.Checked == false)
                        {
                            //Performer Penalty Submitted
                            PenaltySubmit = "S";
                        }
                        else
                        {
                            //Performer penalty Pending
                            PenaltySubmit = "P";
                        }
                    }
                    else
                    {
                        //Performer penalty Rejected
                        PenaltySubmit = "R";
                    }

                    if (StatusID == 2 || StatusID == 3 || StatusID == 5 || StatusID == 4)
                    {
                        ComplianceTransaction transactionUpdatePerformer = new ComplianceTransaction()
                        {
                            ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                            ComplianceInstanceId = Convert.ToInt64(hdnComplianceInstanceID.Value),
                            IsPenaltySave = chkPenaltySaveReview.Checked,
                            Penalty = 0,
                            Interest = 0,
                            PenaltySubmit = PenaltySubmit,
                        };
                        Business.ComplianceManagement.UpdatePenaltyTransactionByReviewer(transactionUpdatePerformer);
                    }
                    #endregion

                    ComplianceTransaction transaction = new ComplianceTransaction()
                    {
                        ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                        ComplianceInstanceId = Convert.ToInt64(hdnComplianceInstanceID.Value),
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedByText = AuthenticationHelper.User,
                        StatusId = StatusID,
                        StatusChangedOn = DateTime.ParseExact(tbxDate1.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        Remarks = tbxRemarks1.Text,
                        Interest = Convert.ToDecimal(txtInterestReview.Text),
                        Penalty = Convert.ToDecimal(txtPenaltyReview.Text),
                        ComplianceSubTypeID = Convert.ToInt32(lblNatureofcomplianceID.Text),
                        ValuesAsPerSystem = Convert.ToDecimal(txtValueAsPerSystem.Text),
                        ValuesAsPerReturn = Convert.ToDecimal(txtValueAsPerReturn.Text),
                        LiabilityPaid = Convert.ToDecimal(txtLiabilityPaid.Text),
                        IsPenaltySave = chkPenaltySaveReview.Checked,
                        PenaltySubmit = PenaltySubmit,
                    };
                    int terminatestatusid = GetTerminateStatus(Convert.ToInt32(hdnComplianceScheduledOnId.Value));
                    var leavedetails = Business.ComplianceManagement.GetUserLeavePeriodExists(AuthenticationHelper.UserID, "R");
                    if (leavedetails != null)
                    {
                        transaction.OUserID = leavedetails.OldReviewerID;
                    }
                    bool isSucessfull = Business.ComplianceManagement.CreateTransaction(transaction);
                    //Update License Status
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        long ComplianceInstanceID = Convert.ToInt64(hdnComplianceInstanceID.Value);
                        var LicenseID = (from row in entities.Lic_tbl_LicenseComplianceInstanceMapping
                                         where row.ComplianceInstanceID == ComplianceInstanceID
                                         select row.LicenseID).FirstOrDefault();

                        var recentlicenstransactionStatus = (from row in entities.RecentLicenseTransactionViews
                                                             where row.LicenseID == LicenseID
                                                             select row).FirstOrDefault();

                        if (recentlicenstransactionStatus.StatusID == 7)
                        {
                            bool saveSuccess = false;
                            long recurringdays = 0;
                            long licensetypeid = 0;
                            if (!string.IsNullOrEmpty(hdnlicremindbeforenoofdays.Value))
                            {
                                recurringdays = Convert.ToInt64(hdnlicremindbeforenoofdays.Value);
                            }
                            if (!string.IsNullOrEmpty(hdnlicensetypeid.Value))
                            {
                                licensetypeid = Convert.ToInt64(hdnlicensetypeid.Value);
                            }

                            #region Save License Instance 
                            Lic_tbl_LicenseInstance licenseRecord = new Lic_tbl_LicenseInstance()
                            {
                                UpdatedBy = AuthenticationHelper.UserID
                            };
                            Lic_tbl_LicenseInstance_Log newLic_tbl_LicenseInstance_Log = new Lic_tbl_LicenseInstance_Log()
                            {
                                CustomerID = AuthenticationHelper.CustomerID,
                                UpdatedBy = AuthenticationHelper.UserID,
                                CreatedBy = AuthenticationHelper.UserID,
                                UpdatedOn = DateTime.Now,
                                LicenseID = LicenseID,
                                RemindBeforeNoOfDays = recurringdays,
                                LicenseTypeID = licensetypeid,
                                IsStatutory = true,
                            };
                            if (!string.IsNullOrEmpty(txtLicenseNo.Text))
                            {
                                licenseRecord.LicenseNo = Convert.ToString(txtLicenseNo.Text);
                                newLic_tbl_LicenseInstance_Log.LicenseNo = Convert.ToString(txtLicenseNo.Text);
                            }
                            if (!string.IsNullOrEmpty(txtLicenseTitle.Text))
                            {
                                licenseRecord.LicenseTitle = Convert.ToString(txtLicenseTitle.Text);
                                newLic_tbl_LicenseInstance_Log.LicenseTitle = Convert.ToString(txtLicenseTitle.Text);
                            }
                            if (!string.IsNullOrEmpty(txtStartDate.Text))
                            {
                                licenseRecord.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);
                                newLic_tbl_LicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);
                            }
                            if (!string.IsNullOrEmpty(txtEndDate.Text))
                            {
                                licenseRecord.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                                newLic_tbl_LicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                            }

                            if (ChkIsActive.Checked)
                            {
                                licenseRecord.IsPermanantActive = true;
                                newLic_tbl_LicenseInstance_Log.IsPermanantActive = true;
                            }
                            else
                            {
                                licenseRecord.IsPermanantActive = false;
                                newLic_tbl_LicenseInstance_Log.IsPermanantActive = false;
                            }
                            if (!string.IsNullOrEmpty(txtCost.Text))
                            {
                                licenseRecord.Cost = Convert.ToDecimal(txtCost.Text);
                                newLic_tbl_LicenseInstance_Log.Cost = Convert.ToDecimal(txtCost.Text);
                            }

                            if (!string.IsNullOrEmpty(txtFileno.Text))
                            {
                                licenseRecord.FileNO = Convert.ToString(txtFileno.Text);
                            }
                            

                            if (!string.IsNullOrEmpty(txtphysicalLocation.Text))
                                licenseRecord.PhysicalLocation = Convert.ToString(txtphysicalLocation.Text);


                            licenseRecord.ID = LicenseID;
                            LicenseMgmt.UpdateLicenseNew(licenseRecord);

                            LicenseMgmt.CreateLicenseLog(newLic_tbl_LicenseInstance_Log);
                            int statusId = 0;
                            if (!string.IsNullOrEmpty(Convert.ToString(txtStartDate.Text)) || !string.IsNullOrEmpty(Convert.ToString(txtEndDate.Text)))
                            {
                                if (DateTimeExtensions.GetDate(Convert.ToString(txtEndDate.Text)).Date <= DateTime.Today.Date)
                                {
                                    statusId = 3;        //Expired
                                }
                                else if (DateTimeExtensions.GetDate(Convert.ToString(txtEndDate.Text)).Date <= DateTime.Today.Date.AddDays(30))
                                {
                                    statusId = 4;            //Expiring
                                }
                                else if (DateTimeExtensions.GetDate(Convert.ToString(txtEndDate.Text)).Date > Convert.ToDateTime(DateTime.Now).Date)
                                {
                                    statusId = 2;            //Active
                                }
                                else
                                {
                                    statusId = 3;        //Expired
                                }
                            }
                            else
                                statusId = 1;       //Draft                            


                            string status = string.Empty;
                            if (statusId == 2)
                            {
                                status = "Active";
                            }
                            else if (statusId == 3)
                            {
                                status = "Expired";
                            }
                            else if (statusId == 4)
                            {
                                status = "Expiring";
                            }
                            //Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                            //{
                            //    CustomerID = (int)AuthenticationHelper.CustomerID,
                            //    LicenseID = LicenseID,
                            //    StatusID = statusId,
                            //    StatusChangeOn = DateTime.Now,
                            //    IsActive = true,
                            //    CreatedBy = AuthenticationHelper.UserID,
                            //    CreatedOn = DateTime.Now,
                            //    UpdatedBy = AuthenticationHelper.UserID,
                            //    UpdatedOn = DateTime.Now,
                            //    Remark = "Change license status to " + status + ""
                            //};
                            //saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);
                           
                            Lic_tbl_LicenseAudit_Log newLicenseInstance_Log = new Lic_tbl_LicenseAudit_Log()
                            {
                                CustomerID = (int)AuthenticationHelper.CustomerID,
                                LicenseID = LicenseID,
                                //StatusID = statusId,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                Remark = "Change license status to " + status + ""
                            };
                            if (terminatestatusid != 12)
                            {
                                newLicenseInstance_Log.StatusID = statusId;
                            }
                            else
                            {
                                newLicenseInstance_Log.StatusID = 12;
                            }
                            if (!string.IsNullOrEmpty(txtStartDate.Text))
                            {
                                newLicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);
                            }
                            if (!string.IsNullOrEmpty(txtEndDate.Text))
                            {
                                newLicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                            }
                            entities.Lic_tbl_LicenseAudit_Log.Add(newLicenseInstance_Log);
                            entities.SaveChanges();

                            #endregion

                            #region Statutory Non Statutory Compliances Entry                       
                            try
                            {
                                bool statutorySuccess = false;
                                #region Statutory
                                try
                                {
                                    long complianceScheduleOnId = 0;
                                    long complianceTransactionId = 0;
                                    statutorySuccess = false;
                                    if (ComplianceInstanceID > 0)
                                    {
                                        var AssignedRole = GetAssignedUsers((int)ComplianceInstanceID);
                                        if (recurringdays == 0)
                                        {
                                            ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                            {
                                                ScheduleOn = licenseRecord.EndDate.Value.Date,
                                                ComplianceInstanceID = ComplianceInstanceID,
                                                IsActive = true,
                                                IsUpcomingNotDeleted = true,
                                            };
                                            complianceScheduleOnId = LicenseMgmt.CreateComplianceScheduleOn(complianceScheduleOn);


                                            Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                                            {
                                                CustomerID = (int)AuthenticationHelper.CustomerID,
                                                LicenseID = LicenseID,
                                                StatusID = statusId,
                                                StatusChangeOn = DateTime.Now,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                                UpdatedOn = DateTime.Now,
                                                Remark = "Change license status to " + status + "",
                                                ComplianceScheduleOnID = complianceScheduleOnId,
                                            };
                                            saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);

                                            Lic_tbl_LicenseComplianceInstanceScheduleOnMapping lictbllicensecomplianceschedulonmapping = new Lic_tbl_LicenseComplianceInstanceScheduleOnMapping()
                                            {
                                                LicenseID = licenseRecord.ID,
                                                ComplianceInstanceID = ComplianceInstanceID,
                                                ComplianceScheduleOnID = complianceScheduleOnId,
                                                IsActivation = "Active",
                                                IsStatutoryORInternal = "S"
                                            };
                                            LicenseMgmt.CreateLicenseLicenseComplianceInstanceScheduleOnMapping(lictbllicensecomplianceschedulonmapping);


                                            ComplianceTransaction complianceTransaction = new ComplianceTransaction()
                                            {
                                                ComplianceInstanceId = ComplianceInstanceID,
                                                StatusId = 1,
                                                Remarks = "New compliance assigned.",
                                                Dated = DateTime.Now,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedByText = AuthenticationHelper.User,
                                                ComplianceScheduleOnID = complianceScheduleOnId,
                                            };
                                            complianceTransactionId = LicenseMgmt.CreateComplianceTransaction(complianceTransaction);

                                            if (AssignedRole.Count > 0)
                                            {
                                                foreach (var roles in AssignedRole)
                                                {
                                                    if (roles.RoleID != 6)
                                                    {
                                                        var reminders = (from RT in entities.ReminderTemplates
                                                                         where RT.Frequency == 2 && RT.IsSubscribed == true
                                                                         select RT).ToList();

                                                        reminders.ForEach(day =>
                                                        {
                                                            ComplianceReminder reminder = new ComplianceReminder()
                                                            {
                                                                ComplianceAssignmentID = roles.ID,
                                                                ReminderTemplateID = day.ID,
                                                                ComplianceDueDate = licenseRecord.EndDate.Value.Date,
                                                                RemindOn = licenseRecord.EndDate.Value.Date.Date.AddDays(-1 * day.TimeInDays),
                                                            };
                                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                            entities.ComplianceReminders.Add(reminder);

                                                        });
                                                    }
                                                }
                                                entities.SaveChanges();
                                            }
                                        }
                                        else
                                        {
                                            #region Application Days
                                            DateTime Applicationdate = Convert.ToDateTime(licenseRecord.EndDate);
                                            Applicationdate = Applicationdate.AddDays(-Convert.ToDouble(recurringdays));
                                            ComplianceScheduleOn complianceScheduleOn = new ComplianceScheduleOn()
                                            {
                                                ScheduleOn = Applicationdate.Date,
                                                ComplianceInstanceID = ComplianceInstanceID,
                                                IsActive = true,
                                                IsUpcomingNotDeleted = true,
                                            };
                                            complianceScheduleOnId = LicenseMgmt.CreateComplianceScheduleOn(complianceScheduleOn);


                                            Lic_tbl_LicenseStatusTransaction newStatusRecord = new Lic_tbl_LicenseStatusTransaction()
                                            {
                                                CustomerID = (int)AuthenticationHelper.CustomerID,
                                                LicenseID = LicenseID,
                                                StatusID = statusId,
                                                StatusChangeOn = DateTime.Now,
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                                UpdatedOn = DateTime.Now,
                                                Remark = "Change license status to " + status + "",
                                                ComplianceScheduleOnID = complianceScheduleOnId,
                                            };
                                            saveSuccess = LicenseMgmt.CreateLicenseStatusTransaction(newStatusRecord);

                                            Lic_tbl_LicenseComplianceInstanceScheduleOnMapping lictbllicensecomplianceschedulonmapping = new Lic_tbl_LicenseComplianceInstanceScheduleOnMapping()
                                            {
                                                LicenseID = licenseRecord.ID,
                                                ComplianceInstanceID = ComplianceInstanceID,
                                                ComplianceScheduleOnID = complianceScheduleOnId,
                                                IsActivation = "Application",
                                                IsStatutoryORInternal = "S"
                                            };
                                            LicenseMgmt.CreateLicenseLicenseComplianceInstanceScheduleOnMapping(lictbllicensecomplianceschedulonmapping);

                                            ComplianceTransaction complianceTransaction = new ComplianceTransaction()
                                            {
                                                ComplianceInstanceId = ComplianceInstanceID,
                                                StatusId = 1,
                                                Remarks = "New compliance assigned.",
                                                Dated = DateTime.Now,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedByText = AuthenticationHelper.User,
                                                ComplianceScheduleOnID = complianceScheduleOnId,
                                            };
                                            complianceTransactionId = LicenseMgmt.CreateComplianceTransaction(complianceTransaction);

                                            if (AssignedRole.Count > 0)
                                            {
                                                foreach (var roles in AssignedRole)
                                                {
                                                    if (roles.RoleID != 6)
                                                    {
                                                        var reminders = (from RT in entities.ReminderTemplates
                                                                         where RT.Frequency == 2 && RT.IsSubscribed == true
                                                                         select RT).ToList();

                                                        reminders.ForEach(day =>
                                                        {
                                                            ComplianceReminder reminder = new ComplianceReminder()
                                                            {
                                                                ComplianceAssignmentID = roles.ID,
                                                                ReminderTemplateID = day.ID,
                                                                ComplianceDueDate = Applicationdate.Date,
                                                                RemindOn = Applicationdate.Date.AddDays(-1 * day.TimeInDays),
                                                            };
                                                            reminder.Status = (byte)(reminder.RemindOn >= DateTime.Now.Date ? ReminderStatus.Pending : ReminderStatus.Cancelled);
                                                            entities.ComplianceReminders.Add(reminder);

                                                        });
                                                    }
                                                }
                                                entities.SaveChanges();
                                            }
                                            #endregion
                                        }
                                    }
                                    if (ComplianceInstanceID > 0)
                                        statutorySuccess = true;
                                    saveSuccess = statutorySuccess;

                                }
                                catch (Exception ex)
                                {
                                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    saveSuccess = false;
                                }

                                #endregion

                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                            #endregion
                        }
                        else
                        {
                            Lic_tbl_LicenseInstance licenseRecord1 = new Lic_tbl_LicenseInstance()
                            {
                                UpdatedBy = AuthenticationHelper.UserID
                            };
                            if (ChkIsActive.Checked)
                            {
                                licenseRecord1.IsPermanantActive = true;
                            }
                            else
                            {
                                licenseRecord1.IsPermanantActive = false;
                            }
                            if (!string.IsNullOrEmpty(txtCost.Text))
                                licenseRecord1.Cost = Convert.ToDecimal(txtCost.Text);

                            if (!string.IsNullOrEmpty(txtFileno.Text))
                                licenseRecord1.FileNO = Convert.ToString(txtFileno.Text);

                            if (!string.IsNullOrEmpty(txtphysicalLocation.Text))
                                licenseRecord1.PhysicalLocation = Convert.ToString(txtphysicalLocation.Text);

                            licenseRecord1.ID = LicenseID;
                            LicenseMgmt.UpdateLicenseCost(licenseRecord1);

                            Lic_tbl_LicenseInstance_Log licenseRecordLog = new Lic_tbl_LicenseInstance_Log()
                            {
                                UpdatedBy = AuthenticationHelper.UserID,
                                UpdatedOn = DateTime.Now,
                            };
                            if (ChkIsActive.Checked)
                            {
                                licenseRecordLog.IsPermanantActive = true;
                            }
                            else
                            {
                                licenseRecordLog.IsPermanantActive = false;
                            }

                            if (!string.IsNullOrEmpty(txtCost.Text))
                                licenseRecordLog.Cost = Convert.ToDecimal(txtCost.Text);

                            licenseRecordLog.LicenseID = LicenseID;
                            LicenseMgmt.UpdateLicenseLogNew(licenseRecordLog);


                            Lic_tbl_LicenseStatusTransaction license = new Lic_tbl_LicenseStatusTransaction()
                            {
                                CustomerID = (int)AuthenticationHelper.CustomerID,
                                LicenseID = LicenseID,
                                // StatusID = LicStatusID,
                                StatusChangeOn = DateTime.ParseExact(tbxDate1.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                IsActive = true,
                                CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                CreatedOn = DateTime.Now,
                                UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                UpdatedOn = DateTime.Now,
                                Remark = "Change license status to applied.",
                                ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                            };
                            if(terminatestatusid != 12)
                            {
                                license.StatusID = 5;
                            }
                            else
                            {
                                license.StatusID = 12;
                            }
                            entities.Lic_tbl_LicenseStatusTransaction.Add(license);
                            entities.SaveChanges();


                            Lic_tbl_LicenseAudit_Log newLicenseInstance_Log = new Lic_tbl_LicenseAudit_Log()
                            {
                                CustomerID = (int)AuthenticationHelper.CustomerID,
                                LicenseID = LicenseID,
                                 //StatusID = 5,
                                //StatusID = LicStatusID,
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                Remark = "Change license status to applied."
                            };
                            if (terminatestatusid != 12)
                            {
                                newLicenseInstance_Log.StatusID = 5;
                            }
                            else
                            {
                                newLicenseInstance_Log.StatusID = 12;
                            }
                            if (!string.IsNullOrEmpty(txtStartDate.Text))
                            {
                                newLicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);
                            }
                            if (!string.IsNullOrEmpty(txtEndDate.Text))
                            {
                                newLicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                            }
                            entities.Lic_tbl_LicenseAudit_Log.Add(newLicenseInstance_Log);
                            entities.SaveChanges();
                        }
                    }
                    if (OnSaved != null)
                    {
                        OnSaved(this, null);
                    }
                    if (isSucessfull)
                    {
                        try
                        {
                            Business.ComplianceManagement.BindDatainRedis(Convert.ToInt64(hdnComplianceInstanceID.Value), Convert.ToInt32(AuthenticationHelper.CustomerID));
                        }
                        catch { }
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Approved Sucessfully.";
                        ValidationSummary1.CssClass = "alert alert-success";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    ValidationSummary1.CssClass = "alert alert-danger";
                }
#endregion
            }
        }
        public static int GetTerminateStatus(long scheduleonid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var currentStatusID = (from row in entities.Lic_tbl_LicenseStatusTransaction
                                       where row.ComplianceScheduleOnID == scheduleonid
                                       && row.StatusID==12
                                       select row.StatusID).FirstOrDefault();

                return currentStatusID;
            }
        }
        protected void btnReject1_Click(object sender, EventArgs e)
        {
            var CSOID = Convert.ToInt64(hdnComplianceScheduledOnId.Value);
            var RecentComplianceTransaction = Business.ComplianceManagement.GetCurrentStatusByComplianceID((int)CSOID);
            if (RecentComplianceTransaction.ComplianceStatusID == 6 || RecentComplianceTransaction.ComplianceStatusID == 8)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "This compliance already rejected.";
            }
            else
            {
                #region Save Code
                try
                {
                    ComplianceTransaction transaction = new ComplianceTransaction()
                    {
                        ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                        ComplianceInstanceId = Convert.ToInt64(hdnComplianceInstanceID.Value),
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedByText = AuthenticationHelper.User,
                        StatusId = 6,
                        StatusChangedOn = DateTime.ParseExact(tbxDate1.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        Remarks = tbxRemarks1.Text
                    };
                    var leavedetails = Business.ComplianceManagement.GetUserLeavePeriodExists(AuthenticationHelper.UserID, "R");
                    if (leavedetails != null)
                    {
                        transaction.OUserID = leavedetails.OldReviewerID;
                    }
                    bool isSucessfull = Business.ComplianceManagement.CreateTransaction(transaction);

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        long ComplianceInstanceID = Convert.ToInt64(hdnComplianceInstanceID.Value);
                        var LicenseID = (from row in entities.Lic_tbl_LicenseComplianceInstanceMapping
                                         where row.ComplianceInstanceID == ComplianceInstanceID
                                         select row.LicenseID).FirstOrDefault();


                        Lic_tbl_LicenseStatusTransaction license = new Lic_tbl_LicenseStatusTransaction()
                        {
                            CustomerID = (int)AuthenticationHelper.CustomerID,
                            LicenseID = LicenseID,
                            StatusID = 8,
                            StatusChangeOn = DateTime.ParseExact(tbxDate1.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                            IsActive = true,
                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                            CreatedOn = DateTime.Now,
                            UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                            UpdatedOn = DateTime.Now,
                            Remark = "Change license status to Rejected.",
                            ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                        };
                        entities.Lic_tbl_LicenseStatusTransaction.Add(license);
                        entities.SaveChanges();


                        Lic_tbl_LicenseAudit_Log newLicenseInstance_Log = new Lic_tbl_LicenseAudit_Log()
                        {
                            CustomerID = (int)AuthenticationHelper.CustomerID,
                            LicenseID = LicenseID,
                            StatusID = 6,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,
                            Remark = "Change license status to Rejected."
                        };
                        if (!string.IsNullOrEmpty(txtStartDate.Text))
                        {
                            newLicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);
                        }
                        if (!string.IsNullOrEmpty(txtEndDate.Text))
                        {
                            newLicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                        }
                        entities.Lic_tbl_LicenseAudit_Log.Add(newLicenseInstance_Log);
                        entities.SaveChanges();

                    }
                    if (OnSaved != null)
                    {
                        OnSaved(this, null);
                    }

                    var Compliance = ComplianceManagement.Business.ComplianceManagement.GetInstanceTransactionCompliance(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                    if (Compliance != null)
                    {
                        int customerID = -1;
                        customerID = UserManagement.GetByID(Convert.ToInt32(Compliance.UserID)).CustomerID ?? 0;
                        string ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                        var user = UserManagement.GetByID(Convert.ToInt32(Compliance.UserID));
                        string portalurl = string.Empty;
                        URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                        if (Urloutput != null)
                        {
                            portalurl = Urloutput.URL;
                        }
                        else
                        {
                            portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                        }
                        string message = Settings.Default.EMailTemplate_RejectedCompliance
                                           .Replace("@User", Compliance.User)
                                           .Replace("@ComplianceDescription", Compliance.ShortDescription)
                                           .Replace("@Role", Compliance.Role)
                                           .Replace("@ScheduledOn", Compliance.ScheduledOn.ToString("dd-MMM-yyyy"))
                                           .Replace("@From", ReplyEmailAddressName)
                                           .Replace("@URL", Convert.ToString(portalurl));
                        //string message = Settings.Default.EMailTemplate_RejectedCompliance
                        //                    .Replace("@User", Compliance.User)
                        //                    .Replace("@ComplianceDescription", Compliance.ShortDescription)
                        //                    .Replace("@Role", Compliance.Role)
                        //                    .Replace("@ScheduledOn", Compliance.ScheduledOn.ToString("dd-MMM-yyyy"))
                        //                    .Replace("@From", ReplyEmailAddressName)
                        //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { user.Email }).ToList(), null, null, "AVACOM Notification :: Compliance has been rejected.", message);

                    }
                    if (isSucessfull)
                    {
                        try
                        {
                            Business.ComplianceManagement.BindDatainRedis(Convert.ToInt64(hdnComplianceInstanceID.Value), Convert.ToInt32(AuthenticationHelper.CustomerID));
                        }
                        catch { }
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Rejected Sucessfully.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
                #endregion
            }
        }

        protected void grdTransactionHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DOWNLOAD_FILE"))
                {
                    int fileID = Convert.ToInt32(e.CommandArgument);
                    var file = Business.ComplianceManagement.GetFile(fileID);

                    if (file.FilePath == null)
                    {
                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                            //using (FileStream fs = File.OpenRead(Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name))))
                            using (FileStream fs = File.OpenRead(pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"])))
                            {
                                int length = (int)fs.Length;
                                byte[] buffer;

                                using (BinaryReader br = new BinaryReader(fs))
                                {
                                    buffer = br.ReadBytes(length);
                                }

                                Response.Buffer = true;
                                Response.Clear();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                                if (file.EnType == "M")
                                {
                                    Response.BinaryWrite(CryptographyHandler.Decrypt(buffer)); // create the file
                                }
                                else
                                {
                                    Response.BinaryWrite(CryptographyHandler.AESDecrypt(buffer)); // create the file
                                }
                                Response.Flush(); // send it to the client to download
                            }
                        }
                        else
                        {
                            using (FileStream fs = File.OpenRead(Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name))))
                            {
                                int length = (int)fs.Length;
                                byte[] buffer;

                                using (BinaryReader br = new BinaryReader(fs))
                                {
                                    buffer = br.ReadBytes(length);
                                }

                                Response.Buffer = true;
                                Response.Clear();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                                if (file.EnType == "M")
                                {
                                    Response.BinaryWrite(CryptographyHandler.Decrypt(buffer)); // create the file
                                }
                                else
                                {
                                    Response.BinaryWrite(CryptographyHandler.AESDecrypt(buffer)); // create the file
                                }
                                Response.Flush(); // send it to the client to download
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(Convert.ToInt32(hdlSelectedDocumentID.Value));

                Response.Buffer = true;
                //Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.Data); // create the file
                Response.End();
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }


        protected void lbDownloadSample1_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.ComplianceManagement.GetComplianceFormByID(Convert.ToInt32(lbDownloadSample1.CommandArgument));

                Response.Buffer = true;
                Response.Clear();
                Response.ContentType = "application/octet-stream";

                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                Response.BinaryWrite(file.FileData); // create the file
                Response.Flush(); // send it to the client to download

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails1_Load(object sender, EventArgs e)
        {
            try
            {
                //DateTime date = DateTime.MinValue;
                //if (DateTime.TryParseExact(tbxDate1.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                //}

                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndexHistory"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        public void OpenTransactionPage(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {

                tbxRemarks1.Text = tbxDate1.Text = string.Empty;
                BindTransactionDetails(ScheduledOnID, complianceInstanceID);
                //upUsers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public event EventHandler OnSaved;

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = string.Empty;
                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        //filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));

                        string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                        filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }


            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void ViewDocument(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    //if (filePath != null && File.Exists(filePath))
                    //{
                    //    Response.Buffer = true;
                    //    Response.Clear();
                    //    Response.ClearContent();
                    //    Response.ContentType = "application/octet-stream";
                    //    Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                    //    Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                    //    Response.Flush(); // send it to the client to download
                    //    Response.End();

                    //}
                    CompDocReviewPath = filePath;
                }
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblpathCompIlink = (Label)e.Item.FindControl("lblCompDocpathIlink");
                LinkButton lblRedirectfile = (LinkButton)e.Item.FindControl("lblCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
                if (lblpathCompIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblRedirectfile.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblRedirectfile.Visible = false;
                }
            }

            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }

        protected void rptComplianceVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplinceVersionDoc");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);

            //    LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDocumentVersion");
            //    scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
            //}

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblSlashReview = (Label)e.Item.FindControl("lblSlashReview");
                Label lblpathIlink = (Label)e.Item.FindControl("lblpathIlink");
                LinkButton lblViewfile = (LinkButton)e.Item.FindControl("lnkViewDoc");
                LinkButton lblfilePath = (LinkButton)e.Item.FindControl("lblpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplinceVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
                if (lblpathIlink.Text == "True")
                {
                    lblViewfile.Visible = false;
                    lblfilePath.Visible = true;
                    lblDownLoadfile.Visible = false;
                    lblSlashReview.Visible = false;
                }
                else
                {
                    lblViewfile.Visible = true;
                    lblfilePath.Visible = false;
                    lblDownLoadfile.Visible = true;
                    lblSlashReview.Visible = true;
                }
            }
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblWorkCompDocpathIlink = (Label)e.Item.FindControl("lblWorkCompDocpathIlink");
                LinkButton lblWorkCompDocpathDownload = (LinkButton)e.Item.FindControl("lblWorkCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                if (lblWorkCompDocpathIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblWorkCompDocpathDownload.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblWorkCompDocpathDownload.Visible = false;
                }
            }

            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                HttpFileCollection fileCollection = Request.Files;
                for (int i = 0; i < fileCollection.Count; i++)
                {
                    HttpPostedFile uploadfile = fileCollection[i];
                    string fileName = Path.GetFileName(uploadfile.FileName);
                    if (uploadfile.ContentLength > 0)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnkSampleForm_Click(object sender, EventArgs e)
        {
            try
            {
                if (lnkSampleForm1.Text != "")
                {
                    string url = lnkSampleForm1.Text;

                    string fullURL = "window.open('" + url + "', '_blank', 'height=500,width=800,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
                    lnkSampleForm1.Attributes.Add("OnClick", fullURL);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void txtInterestReview_TextChanged(object sender, EventArgs e)
        {
            int val = Convert.ToInt32(txtInterestReview.Text);

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "$(\"#divComplianceDetailsDialog\").dialog('open');", true);

        }
        protected void rptComplianceVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                    if (AWSData != null)
                    {
                        #region AWS Storage
                        string[] commandArg = e.CommandArgument.ToString().Split(',');
                        List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                        Session["ScheduleOnID"] = commandArg[0];

                        if (CMPDocuments != null)
                        {
                            List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                                entityData.Version = "1.0";
                                entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                foreach (var file in CMPDocuments)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.SecretKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }

                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string extension = System.IO.Path.GetExtension(file.FileName);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessageReviewer1.Text = "";
                                            lblMessageReviewer1.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                        }
                                        else
                                        {
                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            CompDocReviewPath = filePath1;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewReviewer('" + CompDocReviewPath + "');", true);
                                            lblMessageReviewer1.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessageReviewer1.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal Storage
                        string[] commandArg = e.CommandArgument.ToString().Split(',');
                        List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                        Session["ScheduleOnID"] = commandArg[0];

                        if (CMPDocuments != null)
                        {
                            List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                                entityData.Version = "1.0";
                                entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;
                                        string extension = System.IO.Path.GetExtension(filePath);

                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessageReviewer1.Text = "";
                                            lblMessageReviewer1.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            CompDocReviewPath = FileName;
                                            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewReviewer('" + CompDocReviewPath + "');", true);
                                            lblMessageReviewer1.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessageReviewer1.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private bool BindSubTasks(long ComplianceScheduleOnID, int roleID, int CustomerBranchid)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var documentData = (from row in entities.SP_TaskInstanceTransactionStatutoryView(customerID)
                                        where row.ParentID == null
                                        //&& row.ForMonth == period
                                        && row.RoleID == roleID
                                        && row.CustomerBranchID == CustomerBranchid
                                        && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                        select row).ToList();

                    if (documentData.Count != 0)
                    {
                        divTask.Visible = true;
                        gridSubTaskReviewer.DataSource = documentData;
                        gridSubTaskReviewer.DataBind();

                        var closedSubTaskCount = documentData.Where(entry => (entry.TaskStatusID == 4 || entry.TaskStatusID == 5)).Count();

                        if (documentData.Count == closedSubTaskCount)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        divTask.Visible = false;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }
        protected string GetUserName(long taskInstanceID, long taskScheduleOnID, int roleID, byte taskType)
        {
            try
            {
                string result = "";

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                result = TaskManagment.GetTaskAssignedUser(customerID, taskType, taskInstanceID, taskScheduleOnID, roleID);

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cv.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }

        }

        protected void gridSubTaskReviewer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (e.CommandName.Equals("Download"))
                    {
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                if (taskDocument.Count > 0)
                                {
                                    foreach (var item in taskDocument)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), eachFile.FileName);
                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(eachFile.FileName);
                                            string[] filename = eachFile.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                             
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                            
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                if (taskDocument.Count > 0)
                                {
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        //comment by rahul on 20 JAN 2017
                                        string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                        //string filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));

                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(eachFile.FileName);
                                            string[] filename = eachFile.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (eachFile.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptTaskVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptTaskVersionView.DataBind();

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                        string directoryPath = "~/TempDocuments/AWS/" + User;

                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                                        }
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessagetask.Text = "";
                                                lblMessagetask.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview1PopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                TaskDocViewPath = filePath1;
                                                TaskDocViewPath = TaskDocViewPath.Substring(2, TaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview1('" + TaskDocViewPath + "');", true);
                                                lblMessagetask.Text = "";
                                                UpdatePanel2.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessagetask.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview1PopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptTaskVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptTaskVersionView.DataBind();

                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessagetask.Text = "";
                                                lblMessagetask.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview1PopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();
                                                TaskDocViewPath = FileName;
                                                TaskDocViewPath = TaskDocViewPath.Substring(2, TaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview1('" + TaskDocViewPath + "');", true);
                                                lblMessagetask.Text = "";
                                                UpdatePanel2.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessagetask.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview1PopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                        #region

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptTaskVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);
                int taskFileidID = Convert.ToInt32(commandArgs[2]);
                if (taskScheduleOnID != 0)
                {
                    if (e.CommandName.Equals("View"))
                    {
                        #region
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();
                        List<GetTaskDocumentView> taskFileData = new List<GetTaskDocumentView>();
                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
                        taskFileData = taskDocumenttoView;
                        taskDocumenttoView = taskDocumenttoView.Where(entry => entry.FileID == taskFileidID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    rptTaskVersionView.DataSource = taskFileData.OrderBy(entry => entry.Version);
                                    rptTaskVersionView.DataBind();
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }


                                    foreach (var file in taskDocumenttoView)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessagetask.Text = "";
                                                lblMessagetask.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview1PopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                TaskDocViewPath = filePath1;
                                                TaskDocViewPath = TaskDocViewPath.Substring(2, TaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview1('" + TaskDocViewPath + "');", true);
                                                lblMessagetask.Text = "";
                                                UpdatePanel2.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessagetask.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview1PopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    rptTaskVersionView.DataSource = taskFileData.OrderBy(entry => entry.Version);
                                    rptTaskVersionView.DataBind();

                                    foreach (var file in taskDocumenttoView)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);

                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessagetask.Text = "";
                                                lblMessagetask.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview1PopUp();", true);
                                            }
                                            else
                                            {
                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();
                                                TaskDocViewPath = FileName;
                                                TaskDocViewPath = TaskDocViewPath.Substring(2, TaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview1('" + TaskDocViewPath + "');", true);
                                                lblMessagetask.Text = "";
                                                UpdatePanel2.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessagetask.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReview1PopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                        #endregion                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gridSubTaskReviewer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    Label lblTaskSlashReview = (Label)e.Row.FindControl("lblTaskSlashReview");
                    LinkButton btnSubTaskDocView = (LinkButton)e.Row.FindControl("btnSubTaskDocView");
                    LinkButton btnSubTaskDocDownload = (LinkButton)e.Row.FindControl("btnSubTaskDocDownload");
                    Label lblIsTaskClose = (Label)e.Row.FindControl("lblIsTaskClose");

                    if (lblStatus != null && btnSubTaskDocDownload != null && lblTaskSlashReview != null && btnSubTaskDocView != null)
                    {
                        if (lblStatus.Text != "")
                        {
                            if (lblStatus.Text == "Open")
                            {
                                btnSubTaskDocDownload.Visible = false;
                                lblTaskSlashReview.Visible = false;
                                btnSubTaskDocView.Visible = false;
                            }
                            else
                            {
                                if (lblIsTaskClose.Text == "True")
                                {
                                    btnSubTaskDocDownload.Visible = false;
                                    lblTaskSlashReview.Visible = false;
                                    btnSubTaskDocView.Visible = false;
                                }
                                else
                                {
                                    btnSubTaskDocDownload.Visible = true;
                                    lblTaskSlashReview.Visible = true;
                                    btnSubTaskDocView.Visible = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region License Documents
        protected void rptLicenseVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblSlashReview = (Label)e.Item.FindControl("lblSlashReview1");
                Label lblpathIlink = (Label)e.Item.FindControl("lblpathIlink1");
                LinkButton lblViewfile = (LinkButton)e.Item.FindControl("lnkViewDoc1");
                LinkButton lblfilePath = (LinkButton)e.Item.FindControl("lblpathDownload1");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblLicenseDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
                if (lblpathIlink.Text == "True")
                {
                    lblViewfile.Visible = false;
                    lblfilePath.Visible = true;
                    lblDownLoadfile.Visible = false;
                    lblSlashReview.Visible = false;
                }
                else
                {
                    lblViewfile.Visible = true;
                    lblfilePath.Visible = false;
                    lblDownLoadfile.Visible = true;
                    lblSlashReview.Visible = true;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseVersionDoc");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);

            //    LinkButton lblLicenseDocumentVersion = (LinkButton)e.Item.FindControl("lblLicenseDocumentVersion");
            //    scriptManager.RegisterAsyncPostBackControl(lblLicenseDocumentVersion);
            //}
        }
        protected void rptLicenseVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                }
                else
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    List<SP_GetLicenseDocument_Result> ComplianceFileData = new List<SP_GetLicenseDocument_Result>();
                    List<SP_GetLicenseDocument_Result> ComplianceDocument = new List<SP_GetLicenseDocument_Result>();
                    ComplianceDocument = LicenseDocumentManagement.GetFileData(Convert.ToInt32(commandArgs[0]), -1).ToList();

                    if (commandArgs[2].Equals("1.0"))
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                        if (ComplianceFileData.Count <= 0)
                        {
                            ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                        }
                    }
                    else
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                    }

                    if (e.CommandName.Equals("version"))
                    {
                        if (e.CommandName.Equals("version"))
                        {
                            rptLicenseDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                            rptLicenseDocumnets.DataBind();

                            rptLicenseWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                            rptLicenseWorkingFiles.DataBind();

                            var documentVersionData = ComplianceDocument.Select(x => new
                            {
                                ID = x.ID,
                                ScheduledOnID = x.ScheduledOnID,
                                LicenseID = x.LicenseID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                                FileID = x.FileID,
                                ISLink = x.ISLink,
                                FilePath = x.FilePath,
                                FileName = x.FileName
                            }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();

                            rptLicenseVersion.DataSource = documentVersionData;
                            rptLicenseVersion.DataBind();

                            //upComplianceDetails1.Update();                    
                        }
                    }
                    else if (e.CommandName.Equals("Download"))
                    {
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage 
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                var LicenseData = LicenseDocumentManagement.GetLicense(Convert.ToInt32(commandArgs[0]));

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                ComplianceZip.AddDirectoryByName(LicenseData.LicenseNo);
                                ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                                if (ComplianceFileData.Count > 0)
                                {
                                    foreach (var item in ComplianceFileData)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            
                                                ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                           
                                            i++;
                                        }
                                    }
                                }
                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=LicenseDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage 
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                var LicenseData = LicenseDocumentManagement.GetLicense(Convert.ToInt32(commandArgs[0]));

                                ComplianceZip.AddDirectoryByName(LicenseData.LicenseNo);
                                ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                                if (ComplianceFileData.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string ext = Path.GetExtension(file.FileName);
                                            string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];
                                            string str = filename[0] + i + "." + ext;
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }
                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=LicenseDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }

                    }
                    else if (e.CommandName.Equals("View"))
                    {

                        string[] commandArg = e.CommandArgument.ToString().Split(',');
                        List<SP_GetLicenseDocument_Result> CMPDocuments = LicenseDocumentManagement.GetFileData(Convert.ToInt32(commandArgs[0]), -1).ToList();
                        Session["LicenseID"] = commandArg[0];
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            if (CMPDocuments != null)
                            {
                                CMPDocuments = CMPDocuments.Where(x => x.ISLink == false).ToList();
                                List<SP_GetLicenseDocument_Result> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                                if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    SP_GetLicenseDocument_Result entityData = new SP_GetLicenseDocument_Result();
                                    entityData.Version = "1.0";
                                    entityData.LicenseID = Convert.ToInt64(commandArg[0]);
                                    entitiesData.Add(entityData);
                                }

                                rptLicenseVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                rptLicenseVersionView.DataBind();

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }


                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in ComplianceFileData)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageReviewer2.Text = "";
                                                lblMessageReviewer2.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                LicenseDocPath = filePath1;
                                                LicenseDocPath = LicenseDocPath.Substring(2, LicenseDocPath.Length - 2);

                                                lblMessageReviewer2.Text = "";

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + LicenseDocPath + "');", true);
                                            }
                                        }
                                        else
                                        {
                                            lblMessageReviewer2.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                            }
                            #endregion                         
                        }
                        else
                        {
                            #region Normal Storage
                            if (CMPDocuments != null)
                            {
                                CMPDocuments = CMPDocuments.Where(x => x.ISLink == false).ToList();
                                List<SP_GetLicenseDocument_Result> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                                if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    SP_GetLicenseDocument_Result entityData = new SP_GetLicenseDocument_Result();
                                    entityData.Version = "1.0";
                                    entityData.LicenseID = Convert.ToInt64(commandArg[0]);
                                    entitiesData.Add(entityData);
                                }

                                rptLicenseVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                rptLicenseVersionView.DataBind();
                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageReviewer2.Text = "";
                                                lblMessageReviewer2.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();
                                                LicenseDocPath = FileName;

                                                LicenseDocPath = LicenseDocPath.Substring(2, LicenseDocPath.Length - 2);

                                                lblMessageReviewer2.Text = "";

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + LicenseDocPath + "');", true);
                                            }
                                        }
                                        else
                                        {
                                            lblMessageReviewer2.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                            }
                            #endregion

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptLicenseDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblpathCompIlink = (Label)e.Item.FindControl("lblCompDocpathIlink");
                LinkButton lblRedirectfile = (LinkButton)e.Item.FindControl("lblCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
                if (lblpathCompIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblRedirectfile.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblRedirectfile.Visible = false;
                }
            }

            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseDocumnets");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }
        protected void rptLicenseDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                else
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptLicenseWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblWorkCompDocpathIlink = (Label)e.Item.FindControl("lblWorkCompDocpathIlink");
                LinkButton lblWorkCompDocpathDownload = (LinkButton)e.Item.FindControl("lblWorkCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                if (lblWorkCompDocpathIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblWorkCompDocpathDownload.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblWorkCompDocpathDownload.Visible = false;
                }
            }

            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseWorkingFiles");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }
        protected void rptLicenseWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                else
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void rptLicenseVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblLicenseVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }
        protected void rptLicenseVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {

                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<SP_GetLicenseDocument_Result> CMPDocuments = LicenseDocumentManagement.GetFileData(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                    Session["LicenseID"] = commandArg[0];
                    int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                    if (AWSData != null)
                    {
                        #region AWS Storage

                        if (CMPDocuments != null)
                        {
                            List<SP_GetLicenseDocument_Result> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                SP_GetLicenseDocument_Result entityData = new SP_GetLicenseDocument_Result();
                                entityData.Version = "1.0";
                                entityData.LicenseID = Convert.ToInt64(commandArg[0]);
                                entitiesData.Add(entityData);
                            }

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                            string directoryPath = "~/TempDocuments/AWS/" + User;

                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(Server.MapPath(directoryPath));
                            }


                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in CMPDocuments)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }

                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessageReviewer2.Text = "";
                                            lblMessageReviewer2.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                        }
                                        else
                                        {
                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            LicenseDocPath = filePath1;
                                            LicenseDocPath = LicenseDocPath.Substring(2, LicenseDocPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + LicenseDocPath + "');", true);
                                            lblMessageReviewer2.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessageReviewer2.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal Storage

                        if (CMPDocuments != null)
                        {
                            List<SP_GetLicenseDocument_Result> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                SP_GetLicenseDocument_Result entityData = new SP_GetLicenseDocument_Result();
                                entityData.Version = "1.0";
                                entityData.LicenseID = Convert.ToInt64(commandArg[0]);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;
                                        string extension = System.IO.Path.GetExtension(filePath);

                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessageReviewer2.Text = "";
                                            lblMessageReviewer2.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(AuthenticationHelper.CustomerID);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            LicenseDocPath = FileName;
                                            LicenseDocPath = LicenseDocPath.Substring(2, LicenseDocPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfilelicense('" + LicenseDocPath + "');", true);
                                            lblMessageReviewer2.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessageReviewer2.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLicenseAllShowPopUp();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion
    }
}