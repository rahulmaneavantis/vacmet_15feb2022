﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ClientWiseDoc_Man_NoMan : System.Web.UI.Page
    {
        public static int userID;
        public static List<long> locationList = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {
           
            try
            {
                if (!IsPostBack)
                {
                    userID = Convert.ToInt32(AuthenticationHelper.UserID);
                    BindCustomersList(userID);
                }
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("ChecklistComplianceUpdate");
                    DataTable ExcelData = null;
                    DataView view = new System.Data.DataView((DataTable)Session["grdFilterDOCMAN_NONMAM"]);
                    if (view.Count > 0)
                    {
                        ExcelData = view.ToTable("Selected", false, "ComplianceInstanceID", "ComplianceId", "BranchName", "ShortDescription", "ActName", "IsDocMan_NonMan", "ChecklistFlowFrom");
                        exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);

                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A1"].Value = "InstanceID";
                        exWorkSheet.Cells["A1"].AutoFitColumns(10);

                        exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B1"].Value = "ComplianceId";
                        exWorkSheet.Cells["B1"].AutoFitColumns(10);

                        exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C1"].Value = "Branch";
                        exWorkSheet.Cells["C1"].AutoFitColumns(30);

                        exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D1"].Value = "Short Description";
                        exWorkSheet.Cells["D1"].AutoFitColumns(30);

                        exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E1"].Value = "ActName";
                        exWorkSheet.Cells["E1"].AutoFitColumns(30);

                        exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F1"].Value = "IsDocumentMandatory";
                        exWorkSheet.Cells["F1"].AutoFitColumns(20);

                        exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G1"].Value = "Activation Date (DD-MMM-YYYY)";
                        exWorkSheet.Cells["G1"].AutoFitColumns(20);


                       

                        using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 7])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                            //Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }
                        using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 7])
                        {
                            col[1, 6, 1 + ExcelData.Rows.Count, 7].Style.Numberformat.Format = "dd-MMM-yyyy";
                        }
                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=ChecklistComplianceExportForUpdate.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindComplianceFilter();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
              
            }
        }
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private void BindComplianceFilter()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerid = -1;
                    if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue) && Convert.ToString(ddlFilterCustomer.SelectedValue) != "-1")
                    {
                        customerid = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
                    }
                    int branchID = -1;

                    locationList.Clear();
                    for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
                    {
                        RetrieveNodes(this.tvFilterLocation.Nodes[i]);
                    }
                    if (customerid != -1)
                    {
                        grdClient.DataSource = null;
                        grdClient.DataBind();
                        var ClientcomplianceDetails = (from row in entities.sp_Checklist_InstanceMakerChecker_Docman_Nonman_Mapping(customerid)
                                                       select row).ToList();

                        if (ClientcomplianceDetails.Count > 0)
                        {

                            if (locationList.Count > 0)
                            {
                                ClientcomplianceDetails = SelectAllEntitiesList(locationList, customerid);
                            }
                            else
                            {
                                ClientcomplianceDetails = SelectAllEntities(branchID, customerid);
                            }
                            if (!string.IsNullOrEmpty(tbxFilter.Text))
                            {
                                if (CheckInt(tbxFilter.Text))
                                {
                                    int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                                    ClientcomplianceDetails = ClientcomplianceDetails.Where(entry => entry.ComplianceId == a).ToList();
                                }
                                else
                                {
                                    ClientcomplianceDetails = ClientcomplianceDetails.Where(entry => entry.ShortDescription.ToUpper().Contains(tbxFilter.Text.ToUpper())
                                    || entry.ActName.ToUpper().Contains(tbxFilter.Text.ToUpper())
                                    || entry.BranchName.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                                }
                            }
                            grdClient.DataSource = ClientcomplianceDetails;
                            grdClient.DataBind();
                            Session["grdFilterDOCMAN_NONMAM"] = (grdClient.DataSource as List<sp_Checklist_InstanceMakerChecker_Docman_Nonman_Mapping_Result>).ToDataTable();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindLocationFilter();
                tbxFilterLocation.Text = " Select Entity/Location";         
                grdClient.DataSource = null;
                grdClient.DataBind();
                Session["grdFilterDOCMAN_NONMAM"] = null;
                BindComplianceFilter();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCustomersList(int userid)
        {
            try
            {          
                var details = CustomerManagement.GetAllCustomer(userid);
                ddlFilterCustomer.DataTextField = "Name";
                ddlFilterCustomer.DataValueField = "ID";
                ddlFilterCustomer.DataSource = details;
                ddlFilterCustomer.DataBind();
                ddlFilterCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                ddlFilterCustomer.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }            
        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdClient.PageIndex = e.NewPageIndex;
            BindComplianceFilter();
        }

        #region Upload                
        private bool SheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("ChecklistComplianceUpdate"))
                    {
                        if (sheet.Name.Trim().Equals("ChecklistComplianceUpdate"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }


                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        private int GetDimensionRows(ExcelWorksheet sheet)
        {
            var startRow = sheet.Dimension.Start.Row;
            var endRow = sheet.Dimension.End.Row;
            return endRow - startRow;
        }
       
        public partial class CBCTempTable
        {
            public long ComplianceInstanceId { get; set; }
        }
        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry.IsValid = false;
            cvDuplicateEntry.ErrorMessage = finalErrMsg;
        }

      
        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }
        public static DateTime? CleanDateField(string DateField)
        {
            // Convert the text to DateTime and return the value or null
            DateTime? CleanDate = new DateTime();
            int intDate;
            bool DateIsInt = int.TryParse(DateField, out intDate);
            if (DateIsInt)
            {
                // If this is a serial date, convert it
                CleanDate = DateTime.FromOADate(intDate);
            }
            else if (DateField.Length != 0 && DateField != "1/1/0001 12:00:00 AM" &&
                DateField != "1/1/1753 12:00:00 AM")
            {
                // Convert from a General format
                CleanDate = (Convert.ToDateTime(DateField));
            }
            else
            {
                // Date is blank
                CleanDate = null;
            }
            return CleanDate;
        }

        protected void btnUploadSave_Click(object sender, EventArgs e)
        {

            int customerid = -1;
            if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue) && Convert.ToString(ddlFilterCustomer.SelectedValue) != "-1")
            {
                customerid = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
            }
            if (customerid != -1)
            {
                if (FU_Upload.HasFile)
                {
                    try
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            string filename = Path.GetFileName(FU_Upload.FileName);
                            FU_Upload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                            FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                            if (excelfile != null)
                            {
                                using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                                {
                                    bool flag = SheetsExitsts(xlWorkbook, "ChecklistComplianceUpdate");
                                    if (flag == true)
                                    {
                                        ExcelWorksheet xlWorksheetvalidation = xlWorkbook.Workbook.Worksheets["ChecklistComplianceUpdate"];
                                        List<string> errorMessage = new List<string>();
                                        List<string> saveerrorMessage = new List<string>();
                                        List<CBCTempTable> lstTemptable = new List<CBCTempTable>();
                                        if (xlWorksheetvalidation != null)
                                        {
                                            if (GetDimensionRows(xlWorksheetvalidation) != 0)
                                            {
                                                int xlrow2 = xlWorksheetvalidation.Dimension.End.Row;
                                                #region Validations                                           
                                                int valcomplianceinstanceid = -1;
                                                for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                                                {
                                                    valcomplianceinstanceid = -1;
                                                 

                                                    #region 1 ComplianceID
                                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 1].Text.ToString().Trim()))
                                                    {
                                                        valcomplianceinstanceid = Convert.ToInt32(xlWorksheetvalidation.Cells[rowNum, 1].Text.Trim());
                                                    }
                                                    if (valcomplianceinstanceid == 0 || valcomplianceinstanceid == -1)
                                                    {
                                                        errorMessage.Add("Required ComplianceID at row number-" + rowNum);
                                                    }
                                                    else
                                                    {
                                                        if (!lstTemptable.Any(x => x.ComplianceInstanceId == valcomplianceinstanceid))
                                                        {
                                                            CBCTempTable tt = new CBCTempTable();
                                                            tt.ComplianceInstanceId = valcomplianceinstanceid;
                                                            lstTemptable.Add(tt);
                                                        }
                                                        else
                                                        {
                                                            errorMessage.Add("Compliance with this ComplianceInstanceID (" + valcomplianceinstanceid + ") , Exists Multiple Times in the Uploaded Excel Document at Row No - " + rowNum + "");
                                                        }
                                                    }


                                                   
                                                    #endregion


                                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 6].Text.ToString().Trim()))
                                                    {
                                                        List<string> isud = new List<string>();
                                                        isud.Add("YES");
                                                        isud.Add("NO");
                                                        var isupdoc = Convert.ToString(xlWorksheetvalidation.Cells[rowNum, 6].Value).Trim().ToUpper();

                                                        if (!isud.Contains(isupdoc))
                                                        {
                                                            errorMessage.Add("IsDocumentMandatory should be (Yes, No) At Row No -" + rowNum + " ");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        errorMessage.Add("Required IsDocumentMandatory at row number-" + rowNum);
                                                        //errorMessage.Add("IsDocumentMandatory should not blank. Instead Use (Yes , No) At Row No " + rowNum );
                                                    }


                                                    #region 6 Activation Date
                                                    DateTime dt = DateTime.Today.Date;
                                                    DateTime firstDayNextMonth = dt.AddMonths(1).AddDays(-dt.Day + 1);
                                                    DateTime startDate = new DateTime();
                                                    if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 7].Text.ToString().Trim()))
                                                    {
                                                        if (string.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 7].Text))
                                                        {
                                                            errorMessage.Add("Please Check Activation Date or Date should be in DD-MMM-YYYY Format At " + rowNum + " or Activation Date can not be empty.");
                                                        }
                                                        else
                                                        {
                                                            try
                                                            {
                                                                bool check = CheckDate(Convert.ToString(xlWorksheetvalidation.Cells[rowNum, 7].Text).Trim());
                                                                if (!check)
                                                                {
                                                                    errorMessage.Add("Please Check Activation Date or Date should be in DD-MMM-YYYY Format At Row No " + rowNum);
                                                                }
                                                                else
                                                                {
                                                                    string c = Convert.ToString(xlWorksheetvalidation.Cells[rowNum, 7].Text).Trim();
                                                                    DateTime? aaaa = CleanDateField(c);
                                                                    DateTime dt1 = Convert.ToDateTime(aaaa);
                                                                    startDate = GetDate(dt1.ToString("dd/MM/yyyy"));
                                                                    if (firstDayNextMonth> startDate)
                                                                    {                                                                       
                                                                        errorMessage.Add("The activation date should be greater than upcoming month start date("+ firstDayNextMonth.ToString("dd-MMM-yyyy") + ") at Row No " + rowNum);
                                                                    }                                                                                                                                       
                                                                }
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        errorMessage.Add("Required Activation Date at row number-" + rowNum);
                                                        //errorMessage.Add("Activation Date At  - " + rowNum + " should not blank");
                                                    }
                                                    #endregion
                                                }
                                                #endregion

                                                if (errorMessage.Count > 0)
                                                {
                                                    ErrorMessages(errorMessage);
                                                }
                                                else
                                                {
                                                    DateTime ActivationDate = new DateTime();
                                                    #region Save
                                                    string noupdatedid = string.Empty;
                                                    bool DocID = false;
                                                    int complianceinstanceid = -1;
                                                    if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue) && Convert.ToString(ddlFilterCustomer.SelectedValue) != "-1")
                                                    {
                                                        customerid = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
                                                    }

                                                    for (int saverowNum = 2; saverowNum <= xlrow2; saverowNum++)
                                                    {
                                                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[saverowNum, 1].Text.ToString().Trim()))
                                                        {
                                                            complianceinstanceid = Convert.ToInt32(xlWorksheetvalidation.Cells[saverowNum, 1].Text.Trim());
                                                        }

                                                        string IsDocumentMAN = Convert.ToString(xlWorksheetvalidation.Cells[saverowNum, 6].Text).Trim().ToUpper();
                                                        if (IsDocumentMAN == "YES")
                                                        {
                                                            DocID = true;
                                                        }
                                                        else
                                                        {
                                                            DocID = false;
                                                        }
                                                        if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[saverowNum, 7].Text.ToString().Trim()))
                                                        {
                                                            string c = Convert.ToString(xlWorksheetvalidation.Cells[saverowNum, 7].Text).Trim();
                                                            DateTime? aaaa = CleanDateField(c);
                                                            DateTime dt1 = Convert.ToDateTime(aaaa);
                                                            ActivationDate = GetDate(dt1.ToString("dd/MM/yyyy"));
                                                        }

                                                        try
                                                        {
                                                            var ltlmToUpdate = (from row in entities.ComplianceInstances
                                                                                where row.ID == complianceinstanceid
                                                                                select row).FirstOrDefault();

                                                            if (ltlmToUpdate != null)
                                                            {
                                                                ltlmToUpdate.IsDocMan_NonMan = DocID;
                                                                ltlmToUpdate.ChecklistFlowFrom = ActivationDate;
                                                                entities.SaveChanges();


                                                                try
                                                                {
                                                                    ClientWiseCheckListDocument objcwcd = new ClientWiseCheckListDocument();
                                                                    objcwcd.CustomerID = customerid;
                                                                    objcwcd.ComplianceID = ltlmToUpdate.ComplianceId;
                                                                    objcwcd.ComplianceInstanceID = ltlmToUpdate.ID;
                                                                    objcwcd.LocationID = ltlmToUpdate.CustomerBranchID;
                                                                    if (DocID)
                                                                    {
                                                                        objcwcd.DocumentFlag = 1;
                                                                    }
                                                                    else
                                                                    {
                                                                        objcwcd.DocumentFlag = 0;
                                                                    }
                                                                    objcwcd.CreatedBy = ltlmToUpdate.ComplianceId;
                                                                    objcwcd.CreatedOn = DateTime.Now;
                                                                    objcwcd.UpdatedBy = null;
                                                                    objcwcd.UpdatedOn = null;
                                                                    objcwcd.IsDeleted = false;
                                                                    objcwcd.StartDate = ActivationDate;
                                                                    objcwcd.IsProcessed = false;
                                                                    //entities.SaveChanges();
                                                                    CreateClientWiseCheckListDocument(objcwcd);
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    errorMessage.Add("Error while save data in ClientWiseCheckListDocument  for ComplianceInstanceID - " + ltlmToUpdate.ID);
                                                                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                errorMessage.Add("Error data not found in ComplianceInatance  for ID - " + complianceinstanceid);
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            errorMessage.Add("Error while Update data in ComplianceInstance  for ID - " + complianceinstanceid);
                                                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                        }

                                                    }
                                                    if (saveerrorMessage.Count > 0)
                                                    {
                                                        ErrorMessages(saveerrorMessage);
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "Records uploaded successfully";
                                                    }
                                                    noupdatedid = string.Empty;
                                                    #endregion
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'ChecklistComplianceUpdate'.";
                                    }
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Error uploading file. Please try again.";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            else
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "please select Customer.";
            }
        }

        public static bool CreateClientWiseCheckListDocument(ClientWiseCheckListDocument objNewStatusRecord)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    objNewStatusRecord.CreatedOn = DateTime.Now;
                    entities.ClientWiseCheckListDocuments.Add(objNewStatusRecord);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        #endregion

        protected void RetrieveNodes(TreeNode node)
        {
            try
            {
                int customerid = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerid = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
                }
                else
                {

                    if ((!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue)) && ddlFilterCustomer.SelectedValue != "-1")
                    {
                        customerid = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
                    }

                }
                if (node.Checked)
                {
                    if (Convert.ToInt32(node.Value) != customerid)
                    {
                        if (!locationList.Contains(Convert.ToInt32(node.Value)))
                        {
                            locationList.Add(Convert.ToInt32(node.Value));
                        }
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i]);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (Convert.ToInt32(tn.Value) != customerid)
                            {
                                if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                                {
                                    locationList.Add(Convert.ToInt32(tn.Value));
                                }
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static List<sp_Checklist_InstanceMakerChecker_Docman_Nonman_Mapping_Result> SelectAllEntities(int branchId = -1, int customerid = -1)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceTransactionEntity = (from row in entities.sp_Checklist_InstanceMakerChecker_Docman_Nonman_Mapping(customerid)
                                                   select row).ToList();

                var branchIds = (from row in entities.CustomerBranches
                                 where row.IsDeleted == false
                                 select row.ID).ToList();

                if (branchIds != null)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => branchIds.Contains((int)entry.branchid)).ToList();
                }
                if (branchId != -1)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => entry.branchid == branchId).ToList();
                }


                return ComplianceTransactionEntity;
            }
        }

        public static List<sp_Checklist_InstanceMakerChecker_Docman_Nonman_Mapping_Result> SelectAllEntitiesList(List<long> CustomerBranchID, int customerid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ComplianceTransactionEntity = (from row in entities.sp_Checklist_InstanceMakerChecker_Docman_Nonman_Mapping(customerid)

                                                   select row).ToList();



                if (CustomerBranchID.Count > 0)
                {
                    ComplianceTransactionEntity = ComplianceTransactionEntity.Where(entry => CustomerBranchID.Contains((int)entry.branchid)).ToList();
                }
                return ComplianceTransactionEntity;
            }
        }

        protected void btnlocation_Click(object sender, EventArgs e)
        {
            try
            {
                BindComplianceFilter();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnClear1_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvFilterLocation.Nodes[i]);
            }
            BindComplianceFilter();          
        }

        protected void ChkBoxClear(TreeNode node)
        {

            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                node.Checked = false;
            }
            foreach (TreeNode tn in node.ChildNodes)
            {

                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)              
                {
                    tn.Checked = false;
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        ChkBoxClear(tn.ChildNodes[i]);
                    }
                }
            }
        }

        private void BindLocationFilter()
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else
                {
                    customerID = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
                }
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);
                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
                //    tvFilterLocation_SelectedNodeChanged(null, null);


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}