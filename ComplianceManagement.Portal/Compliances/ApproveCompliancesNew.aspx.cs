﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ApproveCompliancesNew : System.Web.UI.Page
    {
        protected static List<string> roles;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptCatagory.Visible = true;
                rptInternalCategory.Visible = false;
                lblMessage.Text = string.Empty;
                BindCategoryActCompliances();
                BindInternalCategoryCompliances();
                liInternal.Attributes.Add("class", "");
                liStatutory.Attributes.Add("class", "active");
            }
        }

        protected void lstCatagory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                RecentApproverTransactionView catagoryListItem = (RecentApproverTransactionView)e.Item.DataItem;

                var actList = ComplianceManagement.Business.ComplianceManagement.GetApproverAssignedCompliances(AuthenticationHelper.UserID).Where(entry=>entry.ComplianceCatagoryID==catagoryListItem.ComplianceCatagoryID)
                              .GroupBy(entry1 => entry1.ActID).Select(en => en.FirstOrDefault()).ToList();
                Repeater rptModels = (Repeater)e.Item.FindControl("repActList");
                LinkButton lbtApprover = (LinkButton)e.Item.FindControl("lbtApprover");
                lbtApprover.Attributes.Add("onclick", string.Format("Confirm('{0}')", catagoryListItem.Catagory));

                if (actList != null)
                {
                    rptModels.DataSource = actList;
                    rptModels.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lstInternalCatagory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                RecentApproverInternalTransactionView catagoryListItem = (RecentApproverInternalTransactionView)e.Item.DataItem;

                var actList = ComplianceManagement.Business.ComplianceManagement.GetApproverAssignedInternalCompliances(AuthenticationHelper.UserID).Where(entry => entry.ComplianceCatagoryID == catagoryListItem.ComplianceCatagoryID)
                              .GroupBy(entry1 => entry1.ComplianceCatagoryID).Select(en => en.FirstOrDefault()).ToList();
                Repeater rptModels = (Repeater)e.Item.FindControl("repCatagoryist");
                LinkButton lbtApprover = (LinkButton)e.Item.FindControl("lbtInternalApprover");
                lbtApprover.Attributes.Add("onclick", string.Format("Confirm('{0}')", catagoryListItem.Catagory));

                if (actList != null)
                {
                    rptModels.DataSource = actList;
                    rptModels.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void repActList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                RecentApproverTransactionView actList = (RecentApproverTransactionView)e.Item.DataItem;
                var complinaceList = ComplianceManagement.Business.ComplianceManagement.GetApproverAssignedCompliances(AuthenticationHelper.UserID).Where(entry => entry.ActID == actList.ActID)
                                     .GroupBy(entry1 => entry1.ComplianceID).Select(en => en.FirstOrDefault()).ToList();
                Repeater rptModels = (Repeater)e.Item.FindControl("rptCompliancesList");
                if (complinaceList != null)
                {
                    rptModels.DataSource = complinaceList;
                    rptModels.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void repCatagoryist_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                RecentApproverInternalTransactionView actList = (RecentApproverInternalTransactionView)e.Item.DataItem;
                var complinaceList = ComplianceManagement.Business.ComplianceManagement.GetApproverAssignedInternalCompliances(AuthenticationHelper.UserID).Where(entry => entry.ComplianceCatagoryID == actList.ComplianceCatagoryID)
                                     .GroupBy(entry1 => entry1.InternalComplianceID).Select(en => en.FirstOrDefault()).ToList();
                Repeater rptModels = (Repeater)e.Item.FindControl("rptInternalCompliancesList");
                if (complinaceList != null)
                {
                    rptModels.DataSource = complinaceList;
                    rptModels.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static ComplianceTransaction GetPenaltyTransactionReviewer(long ComplianceScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceTransaction Transaction = (from row in entities.ComplianceTransactions
                                                     where row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                                     && (row.StatusId == 5 || row.StatusId == 4)
                                                     select row).OrderByDescending(m => m.ID).FirstOrDefault();

                return Transaction;
            }
        }
        protected void lbtApprover_Command(Object sender, CommandEventArgs e)
        {
            try
            {
                string confirmValue = Request.Form["confirm_value"];

                if (confirmValue != null)
                {
                    if (confirmValue.Equals("Yes"))
                    {
                        int id = Convert.ToInt32(e.CommandArgument);
                        var ApproverTransactionList = ComplianceManagement.Business.ComplianceManagement.GetApproverAssignedCompliances(AuthenticationHelper.UserID)
                                                     .Where(entry => entry.ComplianceCatagoryID == id).ToList();

                        if (ApproverTransactionList != null)
                        {
                            List<ComplianceTransaction> transactionList = new List<ComplianceTransaction>();
                            foreach (var atl in ApproverTransactionList)
                            {

                                var transactionReviewer = GetPenaltyTransactionReviewer(atl.ScheduledOnID);

                                decimal? Interest = null;
                                decimal? Penalty = null;
                                bool? IsPenaltySave = null;
                                string PenaltySubmit = "";
                                int statusId = atl.ComplianceStatusID == 4 ? 7 : 9;

                                ComplianceTransaction transaction = new ComplianceTransaction()
                                {
                                    ComplianceScheduleOnID = atl.ScheduledOnID,
                                    ComplianceInstanceId = atl.ComplianceInstanceID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    StatusId = statusId,                                    
                                    Remarks = "",
                                   
                                };

                                if (transactionReviewer != null)
                                {
                                    if (transactionReviewer.StatusId == 5)
                                    {
                                        Interest = transactionReviewer.Interest;
                                        Penalty = transactionReviewer.Penalty;
                                        IsPenaltySave = transactionReviewer.IsPenaltySave;
                                        PenaltySubmit = transactionReviewer.PenaltySubmit;

                                        transaction.Interest = Interest;
                                        transaction.Penalty = Penalty;
                                        transaction.IsPenaltySave = IsPenaltySave;
                                        transaction.PenaltySubmit = PenaltySubmit;
                                        transaction.StatusChangedOn = transactionReviewer.StatusChangedOn;
                                    }
                                    else
                                    {
                                        transaction.StatusChangedOn = transactionReviewer.StatusChangedOn;
                                    }
                                }                                                               
                                transactionList.Add(transaction);
                            }

                            ComplianceManagement.Business.ComplianceManagement.CreateTransaction(transactionList);

                            lblMessage.Text = "Approved all Compliances of category " + Convert.ToString(e.CommandName);
                            BindCategoryActCompliances();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
           
        }
        public static InternalComplianceTransaction GetInternalComplianceTransactionReviewer(long ComplianceScheduleOnID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                InternalComplianceTransaction Transaction = (from row in entities.InternalComplianceTransactions
                                                             where row.InternalComplianceScheduledOnID == ComplianceScheduleOnID
                                                              && (row.StatusId == 5 || row.StatusId == 4)
                                                             select row).OrderByDescending(m => m.ID).FirstOrDefault();

                return Transaction;
            }
        }
        protected void lbtInternalApprover_Command(Object sender, CommandEventArgs e)
        {
            try
            {
                string confirmValue = Request.Form["confirm_value"];

                if (confirmValue != null)
                {
                    if (confirmValue.Equals("Yes"))
                    {
                        int id = Convert.ToInt32(e.CommandArgument);
                        var ApproverTransactionList = ComplianceManagement.Business.ComplianceManagement.GetApproverAssignedInternalCompliances(AuthenticationHelper.UserID)
                                                     .Where(entry => entry.ComplianceCatagoryID == id).ToList();

                        if (ApproverTransactionList != null)
                        {
                            List<InternalComplianceTransaction> transactionList = new List<InternalComplianceTransaction>();
                            foreach (var atl in ApproverTransactionList)
                            {
                                var transactionReviewer = GetInternalComplianceTransactionReviewer(atl.InternalScheduledOnID);
                                int statusId = atl.InternalComplianceStatusID == 4 ? 7 : 9;
                                InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                                {
                                    InternalComplianceScheduledOnID = atl.InternalScheduledOnID,
                                    InternalComplianceInstanceID = atl.InternalComplianceInstanceID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    StatusId = statusId,
                                    Remarks = ""
                                };
                                if (transactionReviewer != null)
                                {
                                    transaction.StatusChangedOn = transactionReviewer.StatusChangedOn;
                                }
                                transactionList.Add(transaction);
                            }

                            ComplianceManagement.Business.InternalComplianceManagement.CreateTransaction(transactionList);

                            lblMessage.Text = "Approved all Compliances of category " + Convert.ToString(e.CommandName);
                            BindInternalCategoryCompliances();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        private void BindCategoryActCompliances()
        {
            try
            {
                var catagoryList = ComplianceManagement.Business.ComplianceManagement.GetApproverAssignedCompliances(AuthenticationHelper.UserID)
                                   .GroupBy(entry => entry.ComplianceCatagoryID).Select(en => en.FirstOrDefault()).ToList();
                if (catagoryList != null)
                {
                    rptCatagory.DataSource = catagoryList;
                    rptCatagory.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindInternalCategoryCompliances()
        {
            try
            {
                var catagoryList = ComplianceManagement.Business.ComplianceManagement.GetApproverAssignedInternalCompliances(AuthenticationHelper.UserID)
                                   .GroupBy(entry => entry.ComplianceCatagoryID).Select(en => en.FirstOrDefault()).ToList();
                if (catagoryList != null)
                {
                    rptInternalCategory.DataSource = catagoryList;
                    rptInternalCategory.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void liStatutory_Click(object sender, EventArgs e)
        {
            rptCatagory.Visible = true;
            rptInternalCategory.Visible = false;

            liInternal.Attributes.Add("class", "");
            liStatutory.Attributes.Add("class", "active");
        }

        protected void liInternal_Click(object sender, EventArgs e)
        {
            rptInternalCategory.Visible = true;
            rptCatagory.Visible = false;
            liInternal.Attributes.Add("class", "active");
            liStatutory.Attributes.Add("class", "");
        }
     
    }

   


}