﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ComplianceOverviewNew : System.Web.UI.Page
    {
        public static string CompDocReviewPath = "";
        protected static string CId;
        protected static string UserId;
        protected static int CustId;
        protected static int UId;
        protected static List<Int32> roles;
        protected static int RoleID;
        protected static int UserRoleID;
        protected static int StatusFlagID;
        protected static int RoleFlag;
        protected static string Flag;
        protected static bool DisableFalg;
        protected static string Path;
        protected static int isapprover;
        protected int IsDeptHead;
        protected string RiskID;
        protected static string Authorization;
        protected static string CustomerName;
        protected static int ComplianceTypeID;
        protected static int PerformerFlagID;
        protected static int ReviewerFlagID;
        protected static string RoleKey;

        protected void Page_Load(object sender, EventArgs e)
        {

            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            RiskID = "";
            CustomerName = GetCustomerName(CustId);
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //IsDeptHead = 0;
                //if (!string.IsNullOrEmpty(Request.QueryString["IsDeptHead"]))
                //{
                //    IsDeptHead = Convert.ToInt32(Request.QueryString["IsDeptHead"]);
                //}
                //if (AuthenticationHelper.Role.Equals("MGMT") || AuthenticationHelper.ComplianceProductType == 3)
                //{
                //    isapprover = 0;
                //    Flag = "MGMT";
                //}
                //else if (IsDeptHead == 1)
                //{
                //    Flag = "DEPT";
                //}
                //else
                //{
                //    var GetApprover = (entities.Sp_GetApproverUsers(AuthenticationHelper.UserID)).ToList();
                //    if (GetApprover.Count > 0)
                //    {
                //        isapprover = 1;
                //        Flag = "APPR";
                //    }
                //}
                string InputFilter = Request.QueryString["filter"];//Status,Upcoming,Overdue,PendingForReview,Rejected,DueButNotSubmitted
                if (!string.IsNullOrEmpty(InputFilter))
                {
                    if (InputFilter.Equals("Status"))
                        StatusFlagID = -1;
                    if (InputFilter.Equals("Overdue"))
                        StatusFlagID = 0;
                    if (InputFilter.Equals("Closed-Timely"))
                        StatusFlagID = 1;
                }
                else
                {
                    StatusFlagID = -1;
                }
                roles = Session["User_comp_Roles"] as List<int>;//get role for Performer and Reviewer and Approver
                if (roles.Contains(3))
                {
                    PerformerFlagID = 1;
                    RoleKey = "PRA";
                }
                if (roles.Contains(4))
                {
                    ReviewerFlagID = 1;
                    RoleKey = "REV";
                }
                if (AuthenticationHelper.Role == "MGMT")
                {
                    //   ManagmentFlagID = 1;
                    Flag = "MGMT";
                    RoleFlag = 1;
                    DisableFalg = false;
                    RoleKey = "MGMT";
                }
                if (AuthenticationHelper.Role == "AUDT")
                {

                    RoleKey = "AUD";
                    Flag = "AUD";
                    DisableFalg = true;
                    //AuditorFlagID = 1;
                }
                else if (AuthenticationHelper.Role == "EXCT")
                {
                    DisableFalg = false;
                    Flag = "PRA";
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

                    if (roles.Contains(4))
                    {
                        RoleFlag = 1;
                    }
                }
                else if (AuthenticationHelper.Role == "CADMN")
                {
                    DisableFalg = false;
                    Flag = "PRA";
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

                    if (roles.Contains(4))
                    {
                        RoleFlag = 1;
                    }
                }
                //if (AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.Role == "AUDT" || isapprover == 1)
                //{
                Path = ConfigurationManager.AppSettings["KendoPathApp"];
                CId = Convert.ToString(AuthenticationHelper.CustomerID);
                UserId = Convert.ToString(AuthenticationHelper.UserID);
                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                RoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);
                RoleFlag = 0;

                //}

                if (!string.IsNullOrEmpty(Request.QueryString["RiskId"]))
                {
                    RiskID = Request.QueryString["RiskId"];
                }

            }
        }
        public static string GetCustomerName(int CID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                string CName = (from row in entities.CustomerViews
                                where row.ID == CID
                                select row.Name).FirstOrDefault();

                return CName;
            }

        }
    }
}