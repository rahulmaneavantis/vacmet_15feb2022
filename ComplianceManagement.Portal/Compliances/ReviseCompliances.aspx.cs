﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using System.Globalization;
using System.Collections;
using Ionic.Zip;
using System.IO;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3;
using Amazon;
using Amazon.S3.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ReviseCompliances : System.Web.UI.Page
    {
        protected string UploadDocumentLink;
        public static string CompDocReviewPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string listCustomer = ConfigurationManager.AppSettings["UploadUsingLinkCustomerlist"];
            string[] listCust = new string[] { };
            if (!string.IsNullOrEmpty(listCustomer))
                listCust = listCustomer.Split(',');

            if (listCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                UploadDocumentLink = "True";
            else
                UploadDocumentLink = "False";

            if (!IsPostBack)
            {
                try
                {                  
                    if (ScriptManager.GetCurrent(Page).IsInAsyncPostBack)
                    {
                        string controlID = Page.Request.Params["__EVENTTARGET"];
                        Control postbackControl = Page.FindControl(controlID);
                    }
                    BindTypes();
                    BindCategories();
                    BindActList();                                      
                    BindStatus();
                    BindLocationFilter();
                    FillComplianceDocuments();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }               
            }
        }
        protected void linkbutton_onclick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Controls/frmUpcomingCompliancessNew.aspx");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static List<Sp_FillReviseDocument_Result> GetReviseCompliances(int UserID,long CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var ReviceCompliances = (from row in entities.Sp_FillReviseDocument(CustomerID, UserID)                                         
                                         select row).ToList();

                return ReviceCompliances;
            }
        }

        public static List<Sp_FillReviseDocument_Result> GetFilteredReviseCompliance(int Customerid, int userID, int Risk, DocumentFilterNewStatusRevise status, String location, int ComType, int ComCategory, int Act, DateTime? StartDate, DateTime? EndDate,string StringType)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                List<Sp_FillReviseDocument_Result> MasterReviseCompliances = new List<Sp_FillReviseDocument_Result>();
                List<Sp_FillReviseDocument_Result> ReviseCompliances = new List<Sp_FillReviseDocument_Result>();
                MasterReviseCompliances = GetReviseCompliances(userID, Customerid);

                var customerlevel = MasterReviseCompliances.Select(en => en.CustomerLevel).FirstOrDefault();
                if (customerlevel==0)
                {
                    ReviseCompliances  = MasterReviseCompliances.Where(en => en.ScheduleonLevel == true).ToList();
                }
                else
                {
                    ReviseCompliances = MasterReviseCompliances.ToList();
                }
                if (Risk != -1)
                    ReviseCompliances = ReviseCompliances.Where(entry => entry.Risk == Risk).ToList();

                if (location != "Location" && location != "Entity/Sub-Entity/Location")
                    ReviseCompliances = ReviseCompliances.Where(entry => entry.Branch == location).ToList();
                                
                switch (status)
                {
                    case DocumentFilterNewStatusRevise.ClosedTimely:
                        ReviseCompliances = ReviseCompliances.Where(entry => entry.ComplianceStatusID == 4).ToList();
                        break;

                    case DocumentFilterNewStatusRevise.ClosedDelayed:
                        ReviseCompliances = ReviseCompliances.Where(entry => entry.ComplianceStatusID == 5).ToList();
                        break;

                    case DocumentFilterNewStatusRevise.PendingForReview:
                        ReviseCompliances = ReviseCompliances.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3)).ToList();
                        break;

                    //case DocumentFilterNewStatus.Rejected:
                    //    ReviseCompliances = ReviseCompliances.Where(entry => entry.ComplianceStatusID == 6).ToList();
                    //    break;
                }

                if (ComType != -1)
                    ReviseCompliances = ReviseCompliances.Where(entry => entry.ComplianceTypeId == ComType).ToList();

                if (ComCategory != -1)
                    ReviseCompliances = ReviseCompliances.Where(entry => entry.ComplianceCategoryId == ComCategory).ToList();

                if (Act != -1)
                    ReviseCompliances = ReviseCompliances.Where(entry => entry.ActID == Act).ToList();

                if (StartDate != null && EndDate != null)
                    ReviseCompliances = ReviseCompliances.Where(entry => (entry.ScheduledOn >= StartDate && entry.ScheduledOn <= EndDate)).ToList();

                // Find data through String contained in Description
                if (StringType != "")
                {
                    ReviseCompliances = ReviseCompliances.Where(entry => (entry.ShortDescription.Contains(StringType))).ToList();
                }

                return ReviseCompliances.GroupBy(entry => entry.ScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }       
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                grdRviseCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdRviseCompliances.PageIndex = 0;
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }          
        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                grdRviseCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdRviseCompliances.PageIndex = 0;
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }          
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {              
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }
                grdRviseCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdRviseCompliances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //Reload the Grid
                FillComplianceDocuments();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }      
        public void FillComplianceDocuments()
        {
            try
            {
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);
                DocumentFilterNewStatusRevise Status = (DocumentFilterNewStatusRevise)Convert.ToInt16(ddlStatus.SelectedIndex);              
                String location = tvFilterLocation.SelectedNode.Text;
                int type = Convert.ToInt32(ddlType.SelectedValue);
                int category = Convert.ToInt32(ddlCategory.SelectedValue);
                int ActID = Convert.ToInt32(ddlAct.SelectedValue);
                DateTime? DateFrom = null;
                DateTime? DateTo = null;
                if (txtAdvStartDate.Text != "")
                {
                    //DateFrom = Convert.ToDateTime(txtAdvStartDate.Text);
                    DateFrom = DateTime.ParseExact(txtAdvStartDate.Text, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                }
                if (txtAdvEndDate.Text != "")
                {
                    //  DateTo = Convert.ToDateTime(txtAdvEndDate.Text);
                    DateTo = DateTime.ParseExact(txtAdvEndDate.Text, new string[] { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" }, CultureInfo.InvariantCulture, DateTimeStyles.None);
                }
                string StringType = "";
                StringType = txtSearchType.Text;

                var ReviseComplianceDocs = GetFilteredReviseCompliance(customerid, AuthenticationHelper.UserID, risk, Status, location, type, category, ActID, DateFrom, DateTo, StringType);

                grdRviseCompliances.DataSource = ReviseComplianceDocs;
                Session["TotalRows"] = ReviseComplianceDocs.Count;
                grdRviseCompliances.DataBind();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }      
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                //DivRecordsScrum.Attributes.Remove("disabled");
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }
     
        protected string GetPerformer(long complianceinstanceid, String ComType)
        {
            try
            {
                string result = "";
                result = DocumentManagement.GetUserName(complianceinstanceid, 3, ComType);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected string GetReviewer(long complianceinstanceid, String ComType)
        {
            try
            {
                string result = "";
                result = DocumentManagement.GetUserName(complianceinstanceid, 4, ComType);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                HttpFileCollection fileCollection = Request.Files;
                bool isBlankFile = false;
                string[] InvalidvalidFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                for (int i = 0; i < fileCollection.Count; i++)
                {
                    HttpPostedFile uploadfile = null;
                    uploadfile = fileCollection[i];
                    int filelength = uploadfile.ContentLength;
                    string fileName = Path.GetFileName(uploadfile.FileName);
                    string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                    if (!string.IsNullOrEmpty(fileName))
                    {
                        if (filelength == 0)
                        {
                            isBlankFile = true;
                            break;
                        }
                        else if (ext == "")
                        {
                            isBlankFile = true;
                            break;
                        }
                        else
                        {
                            if (ext != "")
                            {
                                for (int j = 0; j < InvalidvalidFileTypes.Length; j++)
                                {
                                    if (ext == "." + InvalidvalidFileTypes[j])
                                    {
                                        isBlankFile = true;
                                        break;
                                    }
                                }
                            }

                        }
                    }
                }
                
                if (isBlankFile == false)
                {
                    List<KeyValuePair<string, int>> listGST = new List<KeyValuePair<string, int>>();
                    List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                    try
                    {
                        long? StatusID = Business.ComplianceManagement.GetCurrentStatusByComplianceID(Convert.ToInt32(ViewState["ScheduledOnID"])).ComplianceStatusID;
                        ComplianceTransaction transaction = new ComplianceTransaction()
                        {
                            ComplianceScheduleOnID = Convert.ToInt64(Convert.ToInt32(ViewState["ScheduledOnID"])),
                            ComplianceInstanceId = Convert.ToInt64(Convert.ToInt32(ViewState["ComplianceInstanceID"])),
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedByText = AuthenticationHelper.User,
                            StatusChangedOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                            Remarks = tbxRemarks.Text
                        };
                        var RCTLatest = Business.ComplianceManagement.GetCurrentLatestStatusByComplianceID(Convert.ToInt32(ViewState["ScheduledOnID"]));
                        if(RCTLatest!=null)
                        {
                            transaction.ValuesAsPerReturn = RCTLatest.ValuesAsPerReturn;
                            transaction.ValuesAsPerSystem = RCTLatest.ValuesAsPerSystem;
                        }
                     
                        if (ddlStatus.SelectedValue == "PendingForReview")
                        {
                            transaction.StatusId = (Int32)StatusID;
                        }
                        else
                        {
                            transaction.StatusId = 11;
                        }

                        var leavedetails = Business.ComplianceManagement.GetUserLeavePeriodExists(AuthenticationHelper.UserID, "P");
                        if (leavedetails != null)
                        {
                            transaction.OUserID = leavedetails.OldPerformerID;
                        }

                        List<FileData> files = new List<FileData>();
                        List<FileData> GSTfiles = new List<FileData>();
                        List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                        string fileNameGST = "";
                        string filepathvalueGST = "";
                        string fileKeyGST = "";
                        string versionGST = "";


                        bool blankfileCount = true;
                        string version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(ViewState["ScheduledOnID"]));
                        if (fileCollection.Count > 0)
                        {
                            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(ViewState["ComplianceInstanceID"]));
                            var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                string directoryPath = string.Empty;  
                                                             
                                directoryPath = "avacomdocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + Convert.ToInt32(ViewState["ScheduledOnID"]) + "\\" + version;

                                IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                                S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, directoryPath);
                                if (!di.Exists)
                                {
                                    di.Create();
                                }

                                for (int i = 0; i < fileCollection.Count; i++)
                                {
                                    HttpPostedFile uploadfile = fileCollection[i];

                                    if (uploadfile.FileName != "")
                                    {
                                        string[] keys = fileCollection.Keys[i].Split('$');
                                        String fileName = "";
                                        if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                        {
                                            fileName = "ComplianceDoc_" + uploadfile.FileName;
                                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                                        }
                                        else
                                        {
                                            fileName = "WorkingFiles_" + uploadfile.FileName;
                                            list.Add(new KeyValuePair<string, int>(fileName, 2));
                                        }

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                        Stream fs = uploadfile.InputStream;
                                        BinaryReader br = new BinaryReader(fs);
                                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                        string TdirectoryPath = "~/TempDocuments/AWS/" + User + "/" + uploadfile.FileName;
                                        string directoryPath1 = "~/TempDocuments/AWS/" + User;
                                        string TFilePath = Server.MapPath(TdirectoryPath);
                                        if (File.Exists(TFilePath))
                                            File.Delete(TFilePath);
                                        if (!Directory.Exists(directoryPath1))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath1));
                                        }

                                        FileStream objFileStrm = File.Create(TFilePath);
                                        objFileStrm.Close();
                                        File.WriteAllBytes(TFilePath, bytes);

                                        string AWSpath = "";
                                        if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                        {
                                            AWSpath = directoryPath + "\\ComplianceDoc_" + uploadfile.FileName;
                                        }
                                        else
                                        {
                                            AWSpath = directoryPath + "\\WorkingFiles_" + uploadfile.FileName;
                                        }

                                        FileInfo localFile = new FileInfo(TFilePath);
                                        S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                                        if (!s3File.Exists)
                                        {
                                            using (var s3Stream = s3File.Create()) // <-- create file in S3  
                                            {
                                                localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                                            }
                                        }

                                        Guid fileKey = Guid.NewGuid();
                                        string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                                        Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                                        if (uploadfile.ContentLength > 0)
                                        {
                                            string filepathvalue = string.Empty;
                                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                            filepathvalue = vale.Replace(@"\", "/");

                                            fileNameGST = fileName;
                                            filepathvalueGST = filepathvalue;
                                            fileKeyGST = fileKey.ToString();
                                            versionGST = version;

                                            FileData file = new FileData()
                                            {
                                                Name = fileName,
                                                FilePath = filepathvalue,
                                                FileKey = fileKey.ToString(),
                                                Version = version,
                                                VersionComment = tbxRemarks.Text,
                                                VersionDate = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                                FileSize = uploadfile.ContentLength,
                                                ISLink = false
                                            };

                                            files.Add(file);

                                            FileData gstfiles1 = new FileData()
                                            {
                                                Name = fileNameGST,
                                                FilePath = filepathvalueGST,
                                                FileKey = fileKeyGST.ToString(),
                                                Version = versionGST,
                                                VersionComment = tbxRemarks.Text,
                                                VersionDate = DateTime.UtcNow,
                                            };
                                            GSTfiles.Add(gstfiles1);

                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(uploadfile.FileName))
                                                blankfileCount = false;
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                string directoryPath = string.Empty;
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {
                                    directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + Convert.ToInt32(ViewState["ScheduledOnID"]) + "/" + version;
                                }
                                else
                                {
                                    directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + Convert.ToInt32(ViewState["ScheduledOnID"]) + "/" + version);
                                }

                                DocumentManagement.CreateDirectory(directoryPath);

                                for (int i = 0; i < fileCollection.Count; i++)
                                {
                                    HttpPostedFile uploadfile = fileCollection[i];
                                    string[] keys = fileCollection.Keys[i].Split('$');
                                    String fileName = "";
                                    if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                    {
                                        fileName = "ComplianceDoc_" + uploadfile.FileName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                    }
                                    else
                                    {
                                        fileName = "WorkingFiles_" + uploadfile.FileName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 2));
                                    }

                                    Guid fileKey = Guid.NewGuid();
                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                                    Stream fs = uploadfile.InputStream;
                                    BinaryReader br = new BinaryReader(fs);
                                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                                    if (uploadfile.ContentLength > 0)
                                    {
                                        string filepathvalue = string.Empty;
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                            filepathvalue = vale;
                                        }
                                        else
                                        {
                                            filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        }

                                        fileNameGST = fileName;
                                        filepathvalueGST = filepathvalue;
                                        fileKeyGST = fileKey.ToString();
                                        versionGST = version;

                                        FileData file = new FileData()
                                        {
                                            Name = fileName,
                                            FilePath = filepathvalue,
                                            FileKey = fileKey.ToString(),
                                            Version = version,
                                            VersionComment = tbxRemarks.Text,
                                            VersionDate = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                            FileSize = uploadfile.ContentLength,
                                            ISLink = false
                                        };

                                        files.Add(file);

                                        FileData gstfiles1 = new FileData()
                                        {
                                            Name = fileNameGST,
                                            FilePath = filepathvalueGST,
                                            FileKey = fileKeyGST.ToString(),
                                            Version = versionGST,
                                            VersionComment = tbxRemarks.Text,
                                            VersionDate = DateTime.UtcNow,
                                        };
                                        GSTfiles.Add(gstfiles1);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(uploadfile.FileName))
                                            blankfileCount = false;
                                    }
                                }
                                #endregion
                            }
                        }
                        if (!string.IsNullOrEmpty(TxtComplianceDocument.Text))
                        {
                            List<string> str = TxtComplianceDocument.Text.Trim().Split(',').ToList();
                            foreach (var item in str)
                            {
                                String fileName = "";
                                fileName = "ComplianceDoc_" + DocumentManagement.getFileName(item.ToString().Trim());
                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                listGST.Add(new KeyValuePair<string, int>(fileName, 1));
                                FileData file = new FileData()
                                {
                                    Name = fileName,
                                    Version = version,
                                    FilePath = item.ToString().Trim(),
                                    VersionDate = DateTime.Now,
                                    FileSize = 0,
                                    ISLink = true
                                };
                                files.Add(file);
                                blankfileCount = true;
                            }
                        }
                        if (!string.IsNullOrEmpty(TxtWorkingDocument.Text))
                        {
                            List<string> str = TxtWorkingDocument.Text.Trim().Split(',').ToList();
                            foreach (var item in str)
                            {
                                String fileName = "";
                                fileName = "WorkingFiles_" + DocumentManagement.getFileName(item.ToString().Trim());
                                list.Add(new KeyValuePair<string, int>(fileName, 2));
                                listGST.Add(new KeyValuePair<string, int>(fileName, 2));
                                FileData file = new FileData()
                                {
                                    Name = fileName,
                                    Version = version,
                                    FilePath = item.ToString().Trim(),
                                    VersionDate = DateTime.Now,
                                    FileSize = 0,
                                    ISLink = true
                                };
                                files.Add(file);
                                blankfileCount = true;
                            }
                        }

                        bool flag = false;
                        if (blankfileCount)
                        {
                            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            if (StatusID != null && StatusID > 1 && StatusID != 10)
                                flag = Business.ComplianceManagement.CreateTransactionRevise(transaction, files, list, Filelist, customerID);
                            else
                                flag = DocumentManagement.SaveReviseCompliances(files, list, Filelist, Convert.ToInt32(ViewState["ScheduledOnID"]), customerID);

                            #region GST Bulk Compliance Revise
                            if (flag == true)
                            {
                                foreach (GridViewRow gvrow in grdGstMappedCompliance.Rows)
                                {
                                    List<KeyValuePair<string, Byte[]>> GSTFilelist = new List<KeyValuePair<string, Byte[]>>();

                                    CheckBox chk = (CheckBox)gvrow.FindControl("chkCompliance");
                                    Label InstanceID = (Label)gvrow.FindControl("lblInstanceID");
                                    Label ComplianceScheduleOnID = (Label)gvrow.FindControl("lblComplianceScheduleOnID");
                                    Label CustomerBranchID = (Label)gvrow.FindControl("lblCustomerBranchID");
                                    if (chk != null & chk.Checked)
                                    {
                                        ComplianceTransaction GSTtransaction = new ComplianceTransaction()
                                        {
                                            ComplianceScheduleOnID = Convert.ToInt64(ComplianceScheduleOnID.Text),
                                            ComplianceInstanceId = Convert.ToInt64(InstanceID.Text),
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedByText = AuthenticationHelper.User,
                                            StatusId = 11,
                                            StatusChangedOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                            Remarks = tbxRemarks.Text
                                        };
                                        if (ddlStatus.SelectedValue == "PendingForReview")
                                        {
                                            GSTtransaction.StatusId = (Int32)StatusID;
                                        }
                                        else
                                        {
                                            GSTtransaction.StatusId = 11;
                                        }
                                        flag = Business.ComplianceManagement.CreateTransactionBulkReviseGST(GSTtransaction, GSTfiles, listGST);
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                        }

                        //bool flag = true;
                        if (flag != true)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Compliance Details Revised Successfully.";
                            FillComplianceDocuments();
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                }
                else
                {
                    CVTop.IsValid = false;
                    CVTop.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdRviseCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdRviseCompliances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdRviseCompliances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdRviseCompliances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                FillComplianceDocuments();

                //CmpPopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
       
        private void BindTransactions(int ScheduledOnID)
        {
            try
            {
                var documentVersionData = DocumentManagement.GetFileData(ScheduledOnID).Select(x => new
                {
                    ID = x.ID,
                    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                    VersionDate = x.VersionDate,
                    VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                    ScheduledOnID = x.ScheduledOnID,
                }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

                grdReviseVersionHistory.DataSource = documentVersionData;
                grdReviseVersionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ddlType.SelectedValue = "-1";
                ddlCategory.SelectedValue = "-1";
                ddlAct.SelectedValue = "-1";
                txtAdvStartDate.Text = "";
                txtAdvEndDate.Text = "";
                divAdvSearch.Visible = false;
                txtSearchType.Text = string.Empty;
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Submit(object sender, EventArgs e)
        {
            try
            {
                lblAdvanceSearchScrum.Text = String.Empty;
                ViewState["checkedCompliances"] = null;

                int type = 0;
                int Category = 0;
                int Act = 0;

                if (txtAdvStartDate.Text != "" && txtAdvEndDate.Text == "" || txtAdvStartDate.Text == "" && txtAdvEndDate.Text != "")
                {
                    if (txtAdvStartDate.Text == "")
                        txtAdvStartDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                    else if (txtAdvEndDate.Text == "")
                        txtAdvEndDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");

                    return;
                }

                if (ddlType.SelectedValue != "-1")
                {
                    type = ddlType.SelectedItem.Text.Length;

                    if (type >= 20)
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text.Substring(0, 20) + "...";
                    else
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text;
                }

                if (ddlCategory.SelectedValue != "-1")
                {
                    Category = ddlCategory.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                    else
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                }

                if (ddlAct.SelectedValue != "-1")
                {
                    Act = ddlAct.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                    else
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                }

                if (txtAdvStartDate.Text != "" && txtAdvEndDate.Text != "")
                {
                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        lblAdvanceSearchScrum.Text += "     " + "<b>Start Date: </b>" + txtAdvStartDate.Text;
                        lblAdvanceSearchScrum.Text += "     " + "<b>End Date: </b>" + txtAdvEndDate.Text;
                    }
                    else
                    {
                        lblAdvanceSearchScrum.Text += "<b>Start Date: </b>" + txtAdvStartDate.Text;
                        lblAdvanceSearchScrum.Text += "<b> End Date: </b>" + txtAdvEndDate.Text;
                    }
                }

                if (txtSearchType.Text != "")
                {
                    lblAdvanceSearchScrum.Text += "     " + "<b>Type To Filter:  </b>" + txtSearchType.Text;
                }

                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                    FillComplianceDocuments();
                }
                else
                {
                    divAdvSearch.Visible = false;
                    FillComplianceDocuments();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDocumentDownload_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }           
        }

        private void BindGSTCompliance(long ComplianceID, long ComplianceInstanceID, int roleID, string period,string GstGroupName)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);              
                var InstanceList =Business.ComplianceManagement.GetGSTCOmplianceInstanceList(customerID, ComplianceID, ComplianceInstanceID, GstGroupName);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var documentData = (from row in entities.SP_GSTComplianceStatutory(customerID)
                                        where row.ForMonth == period
                                        && row.RoleID == roleID
                                        && (row.ComplianceStatusID == 4 || row.ComplianceStatusID == 5
                                        && row.ComplianceStatusID == 3 || row.ComplianceStatusID == 2)
                                        && InstanceList.Contains(row.ComplianceInstanceID)
                                        && row.UserID == UserID
                                        select row).ToList();

                    if (documentData.Count != 0)
                    {
                        divGSTComplianceList.Visible = true;
                        grdGstMappedCompliance.DataSource = documentData;
                        grdGstMappedCompliance.DataBind();
                    }
                    else
                    {
                        divGSTComplianceList.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }



        #region All Drop Down Fill Methods
        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID =Convert.ToInt32(AuthenticationHelper.CustomerID);
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);
                string isstatutoryinternal = "";
                isstatutoryinternal = "S";
                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);
                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }
                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindStatus()
        {
            try
            {
                foreach (DocumentFilterNewStatusRevise r in Enum.GetValues(typeof(DocumentFilterNewStatusRevise)))
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(DocumentFilterNewStatusRevise), r), r.ToString());
                    ddlStatus.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
        private void BindTypes()
        {
            try
            {
                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";
                ddlType.DataSource = ComplianceTypeManagement.GetAll();
                ddlType.DataBind();
                ddlType.Items.Insert(0, new ListItem("Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }      
        private void BindCategories()
        {
            try
            {
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";
                ddlCategory.DataSource = ComplianceCategoryManagement.GetAll();
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, new ListItem("Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
        private void BindActList()
        {
            try
            {
                ddlAct.Items.Clear();
                List<SP_GetActsByUserID_Result> ActList = ActManagement.GetActsByUserID(AuthenticationHelper.UserID);
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region All Grid Events
        protected void grdRviseCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (e.CommandName == "ReviseCompliance")
                    {
                        tbxDate.Text = string.Empty;
                        tbxRemarks.Text = string.Empty;

                        string[] commandArg = e.CommandArgument.ToString().Split(',');
                        ViewState["ScheduledOnID"] = commandArg[0];
                        ViewState["ComplianceInstanceID"] = commandArg[1];

                        var AllComData = (from row in entities.GetDetailsbyScheduleonID(Convert.ToInt32(commandArg[0]))
                                          select row).FirstOrDefault();

                        if (AllComData !=null)
                        {
                            lblShorDescription.Text = AllComData.ShortDescription;

                            int customerID = -1;
                            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var GstGroupName = Business.ComplianceManagement.GetGSTCOmplianceGroupName(customerID, AllComData.ComplianceID);
                            if (!string.IsNullOrEmpty(GstGroupName))
                            {
                                BindGSTCompliance(AllComData.ComplianceID, Convert.ToInt64(commandArg[1]), 3, AllComData.ForMonth, GstGroupName);
                                divGSTComplianceList.Visible = true;
                            }
                            else
                            {
                                divGSTComplianceList.Visible = false;
                            }
                            BindTransactions(Convert.ToInt32(commandArg[0]));
                        }
                     
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenReviseDialog", "initializeReviseDate();", true);
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "divReviseCompliance", "$(\"#divReviseCompliance\").dialog('open');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdRviseCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DateTime CurrentDate = DateTime.Today.Date;

                    LinkButton btnChangeStatus = (LinkButton)e.Row.FindControl("lnkReviseCompliances");
                    e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(btnChangeStatus, "");
                    e.Row.ToolTip = "Click on row to revise Compliance";

                    Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledOn");
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    if (lblScheduledOn != null && lblStatus != null)
                    {
                        String GridStatus = lblStatus.Text;
                        if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            lblStatus.Text = "Upcoming";
                        else if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            lblStatus.Text = "Overdue";
                        else if (GridStatus == "Complied but pending review")
                            lblStatus.Text = "Pending For Review";
                        else if (GridStatus == "Complied Delayed but pending review")
                            lblStatus.Text = "Pending For Review";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            lblStatus.Text = "Upcoming";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            lblStatus.Text = "Overdue";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdReviseVersionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdReviseVersionHistory.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["ScheduledOnID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion

        protected void grdReviseVersionHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Delete_Document"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(',');
                    int ID = Convert.ToInt32(commandArgs[0]);
                    if (ddlStatus.SelectedValue == "PendingForReview")
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var documentData = (from row in entities.FileDatas
                                                where row.ID == ID
                                                select row).FirstOrDefault();
                            if (documentData != null)
                            {
                                documentData.IsDeleted = true;
                                entities.SaveChanges();
                            }
                            BindTransactions(Convert.ToInt32(ViewState["ScheduledOnID"]));
                        }
                    }
                }
                else if (e.CommandName.Equals("View"))
                {

                    string[] commandArgs = e.CommandArgument.ToString().Split(',');
                    List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataVersionWise(Convert.ToInt32(commandArgs[1]), Convert.ToString(commandArgs[2])).ToList();
                    Session["ScheduleOnID"] = commandArgs[0];
                    if (CMPDocuments != null)
                    {
                        CMPDocuments = CMPDocuments.Where(x => x.ISLink == false).ToList();
                        List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).OrderBy(entry => entry.Version).ToList();

                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.ScheduledOnID = Convert.ToInt64(commandArgs[1]);
                            entitiesData.Add(entityData);
                        }

                        rptComplianceVersionView.DataSource = entitiesData;
                        rptComplianceVersionView.DataBind();

                        if (entitiesData.Count > 0)
                        {
                            foreach (var file in entitiesData)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessageReviewer1.Text = "";
                                        lblMessageReviewer1.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();
                                        CompDocReviewPath = FileName;

                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);

                                        lblMessageReviewer1.Text = "";
                                        //UpdatePanel8.Update();
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewReviewer('" + CompDocReviewPath + "');", true);
                                    }
                                }
                                else
                                {
                                    lblMessageReviewer1.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                }

                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenReviseDialog", "initializeReviseDate();", true);

                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "Line No 560", MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected bool CanChangeStatus(string Flag)
        {
            try
            {
                bool result = false;
                if (ddlStatus.SelectedValue == "PendingForReview")
                {
                    result = true;
                }                
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {

                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<GetComplianceDocumentsView> CMPDocuments = DocumentManagement.GetFileDataView(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                    Session["ScheduleOnID"] = commandArg[0];

                    if (CMPDocuments != null)
                    {
                        List<GetComplianceDocumentsView> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            GetComplianceDocumentsView entityData = new GetComplianceDocumentsView();
                            entityData.Version = "1.0";
                            entityData.ScheduledOnID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;
                                    string extension = System.IO.Path.GetExtension(filePath);

                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessageReviewer1.Text = "";
                                        lblMessageReviewer1.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                    }
                                    else
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();
                                        CompDocReviewPath = FileName;
                                        CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReviewReviewer('" + CompDocReviewPath + "');", true);
                                        lblMessageReviewer1.Text = "";

                                    }
                                }
                                else
                                {
                                    lblMessageReviewer1.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                }
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenReviseDialog", "initializeReviseDate();", true);

                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }


    }
}

