﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class MyReportTask : System.Web.UI.Page
    {
        public static string CompDocReviewPath = "";
        protected static string CId;
        protected static string UserId;
        protected static int CustId;
        protected static int UId;
        protected static List<Int32> roles;
        protected static int RoleID;
        protected static int RoleFlag;
        protected static string Falg;
        protected static bool DisableFalg;
        protected static String SDate;
        protected static String LDate;
        protected static String IsMonthID;
        protected static string Path;
        protected static string CustomerName;
        protected static string Authorization;
        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            CId = Convert.ToString(AuthenticationHelper.CustomerID);
            UserId = Convert.ToString(AuthenticationHelper.UserID);
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            RoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);
            CustomerName = GetCustomerName(CustId);
            RoleFlag = 0;
            IsMonthID = "3";
            if (AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.ComplianceProductType == 3)
            {
                Falg = "MGMT";
                RoleFlag = 1;
                DisableFalg = false;
            }
            if (AuthenticationHelper.Role == "AUDT")
            {
                IsMonthID = "AUD";
                SDate = Convert.ToDateTime(Session["Auditstartdate"].ToString()).Date.ToString("dd-MM-yyyy");
                LDate = Convert.ToDateTime(Session["Auditenddate"].ToString()).Date.ToString("dd-MM-yyyy");
                Falg = "AUD";
                DisableFalg = true;
            }
            else if (AuthenticationHelper.Role == "EXCT")
            {
                DisableFalg = false;
                Falg = "PRA";
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

                if (roles.Contains(4))
                {
                    RoleFlag = 1;
                }
            }
            else if (AuthenticationHelper.Role == "CADMN")
            {
                DisableFalg = false;
                Falg = "PRA";
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

                if (roles.Contains(4))
                {
                    RoleFlag = 1;
                }
            }
        }

        public static string GetCustomerName(int CID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                string CName = (from row in entities.CustomerViews
                                where row.ID == CID
                                select row.Name).Single();

                return CName;
            }
        }
    }
}