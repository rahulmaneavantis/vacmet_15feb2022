﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.Editor;
using Telerik.Web.UI.Editor.Export;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Styles;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ComplianceCertificateTemplate : System.Web.UI.Page
    {
        protected int UserID = -1;
        protected void Page_Load(object sender, EventArgs e)
        {  
            if (!Page.IsPostBack)
            {
                if (AuthenticationHelper.Role == "IMPT")
                {
                    UserID = AuthenticationHelper.UserID;
                    BindCustomer(UserID);
                }
                else
                {
                    UserID = AuthenticationHelper.UserID;
                    BindCustomer(UserID);
                }

                BindGrid();                
               
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

            }
        }
        protected void btnAddOwner_Click(object sender, EventArgs e)
        {
            ViewState["Mode"] = 0;
            tbxFromDate.Enabled = true;
            tbxEndDate.Enabled = true;

            int customerid = Convert.ToInt32(ddlCustomerFilter.SelectedValue);
            ComplianceCertificateTransaction Template = ComplianceCertificate.GetCertificateTemplateLatest(customerid);
            BindTemplateFeilds();
            if (Template != null)
            {

                theEditor.Content = Template.TemplateContent.ToString();

                var version = ComplianceCertificate.GetCertificateTemplateVersion(customerid);

                lblVersion.Text = version;

                tbxFromDate.Text = Convert.ToDateTime(Template.FromDate).ToString("dd-MM-yyyy");
                tbxEndDate.Text = Convert.ToDateTime(Template.Enddate).ToString("dd-MM-yyyy");

                theEditor.TrackChangesSettings.Author = "Performer";

                theEditor.TrackChangesSettings.CanAcceptTrackChanges = true;

                theEditor.TrackChangesSettings.UserCssId = "reU9";
                theEditor.TrackChangesSettings.CanAcceptTrackChanges = false;

                theEditor.EnableTrackChanges = false;

            }
            else
            {
                theEditor.Content = "";
 
                lblVersion.Text = "1.0";

                tbxFromDate.Text = "";
                tbxEndDate.Text = "";

                theEditor.TrackChangesSettings.Author = "Performer";

                theEditor.TrackChangesSettings.CanAcceptTrackChanges = true;

                theEditor.TrackChangesSettings.UserCssId = "reU9";
                theEditor.TrackChangesSettings.CanAcceptTrackChanges = false;

                theEditor.EnableTrackChanges = false;
            }

            upcom.Update();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divOfficerdetailsDialog\").dialog('open')", true);

        }
        private void BindGrid()
        {
            int customerid = -1;
            if (AuthenticationHelper.Role == "IMPT")
            {
                customerid = Convert.ToInt32(ddlCustomerFilter.SelectedValue);
            }
            else
            {
                customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (customerid != -1)
                {
                    var data1 = (from row in entities.ComplianceCertificateTransactions
                                 where row.CustomerID == customerid
                                 && row.IsActive == true
                                 select row).ToList();


                    var details = data1.OrderByDescending(entry => entry.ID).ToList();

                    grdTemplate.DataSource = details;
                    grdTemplate.DataBind();
                }
            }
        }
        public void BindTemplateFeilds()
        {
            int customerID = -1;    
            if (AuthenticationHelper.Role == "IMPT")
            {
                customerID = Convert.ToInt32(ddlCustomerFilter.SelectedValue);
            }
            else
            {
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.tbl_masterCertificate_template
                            select row).ToList(); 

                bool IsTagVisible = CustomerManagement.CheckForClient(customerID, "FromAndToTagCertificate");

                if (IsTagVisible == false)
                { 
                    data = data.Where(a => a.ID != 14 && a.ID != 15 && a.ID != 16).Distinct().ToList();
                }

                if (data.Count > 0)
                {
                    ddlContractTemplate.DataValueField = "ContractTemplate";
                    ddlContractTemplate.DataTextField = "ContractTemplate";
                    ddlContractTemplate.DataSource = data;
                    ddlContractTemplate.DataBind();
                }
            }
        }
        protected void grdTemplate_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ViewState["TemplateID"] = null;
            ViewState["TemplateID"] = Convert.ToInt32(e.CommandArgument);
            int TemplateID = Convert.ToInt32(e.CommandArgument);
            ComplianceCertificateTransaction Template = ComplianceCertificate.GetCertificateTemplate(TemplateID);
 
            if (e.CommandName == "EDIT")
            {
                ViewState["Mode"] = 1;
               
                int customerid = Convert.ToInt32(ddlCustomerFilter.SelectedValue);
                BindTemplateFeilds();
                if (!string.IsNullOrEmpty(Template.TemplateContent))
                {
                    theEditor.Content = Template.TemplateContent.ToString();
                }

                lblVersion.Text = Template.Version;
                tbxFromDate.Text = Convert.ToDateTime(Template.FromDate).ToString("dd-MM-yyyy");
                tbxEndDate.Text = Convert.ToDateTime(Template.Enddate).ToString("dd-MM-yyyy");

                tbxFromDate.Enabled = false;
                tbxEndDate.Enabled = false;

                theEditor.TrackChangesSettings.Author = "Performer";
                
                theEditor.TrackChangesSettings.CanAcceptTrackChanges = true;

                theEditor.TrackChangesSettings.UserCssId = "reU9";
                theEditor.TrackChangesSettings.CanAcceptTrackChanges = false;

                theEditor.EnableTrackChanges = false;
                BindGrid();
                upcom.Update();
             
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divOfficerdetailsDialog\").dialog('open')", true);
                 
            }
            else if (e.CommandName == "DELETE")
            {
                  TemplateID = Convert.ToInt32(e.CommandArgument);
                Business.ComplianceCertificate.DeleteCertificateTemplate(TemplateID);
                BindGrid();
                upcom.Update();
            }
        }

        private void BindCustomer(int userid)
        {
            if (AuthenticationHelper.Role != "IMPT")
            {

                ddlCustomerFilter.DataTextField = "Name";
                ddlCustomerFilter.DataValueField = "ID";

                int customerID = -1;

                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                ddlCustomerFilter.DataSource = Assigncustomer.GetAssignCustomerData(customerID);
                ddlCustomerFilter.DataBind();
                ddlCustomerFilter.SelectedValue = Convert.ToString(customerID);
            }
            else
            {
                ddlCustomerFilter.DataTextField = "Name";
                ddlCustomerFilter.DataValueField = "ID";

                ddlCustomerFilter.DataSource = GetCustomerList(userid);
                ddlCustomerFilter.DataBind();

                ddlCustomerFilter.Items.Insert(0, new ListItem("< Select >", "-1"));

            }
            
        }
        protected void ddlCustomerFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        public static object GetCustomerList(int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.CustomerAssignmentDetails
                            join row1 in entities.Customers                           
                            on row.CustomerID equals row1.ID
                            join row2 in entities.ComplianceCertificateMappings
                            on row1.ID equals row2.CustomerID
                            where row.IsDeleted == false
                            && row.UserID == userid
                            && row1.IsDeleted == false
                            && row1.ComplianceProductType != 1
                            && row1.Status == 1
                            select row1).ToList();

                return data;
            }
        }
        private void SetDefaultConfiguratorValues()
        {
            int customerID = -1;
            if (AuthenticationHelper.Role == "IMPT")
            {
                customerID = Convert.ToInt32(ddlCustomerFilter.SelectedValue);
            }
            else
            {
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ComplianceCertificateTransactions
                            where row.CustomerID== customerID
                            select row).FirstOrDefault();
                if (data != null)
                {
                    if (!string.IsNullOrEmpty(data.TemplateContent))
                    {
                        theEditor.Content = data.TemplateContent.ToString();
                    }                    
                }
            }

            theEditor.TrackChangesSettings.Author = "Performer";
            theEditor.TrackChangesSettings.CanAcceptTrackChanges = true;

            theEditor.TrackChangesSettings.UserCssId = "reU9";
            theEditor.TrackChangesSettings.CanAcceptTrackChanges = false;

            theEditor.EnableTrackChanges = false;
        }      

        protected void RadEditor1_ExportContent(object sender, EditorExportingArgs e)
        {
            ExportType exportType = e.ExportType;

            if (exportType == ExportType.Word)
            {
                string exportedOutput = e.ExportOutput;

                Byte[] output = Encoding.Default.GetBytes(exportedOutput);

                DocxFormatProvider docxProvider = new DocxFormatProvider();
                RadFlowDocument document = docxProvider.Import(output);

                Header defaultHeader = document.Sections.First().Headers.Add();
                Paragraph defaultHeaderParagraph = defaultHeader.Blocks.AddParagraph();
                defaultHeaderParagraph.TextAlignment = Alignment.Right;
                defaultHeaderParagraph.Inlines.AddRun("This is a sample header.");

                Footer defaultFooter = document.Sections.First().Footers.Add();
                Paragraph defaultFooterParagraph = defaultFooter.Blocks.AddParagraph();
                defaultFooterParagraph.TextAlignment = Alignment.Right;
                defaultFooterParagraph.Inlines.AddRun("This is a sample footer.");

                Byte[] modifiedOutput = docxProvider.Export(document);
                string finalOutput = Encoding.Default.GetString(modifiedOutput, 0, modifiedOutput.Length);

                e.ExportOutput = finalOutput;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            theEditor.ExportToDocx();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int CustomerID = -1;
            if (AuthenticationHelper.Role == "IMPT")
            {
                CustomerID = Convert.ToInt32(ddlCustomerFilter.SelectedValue);
            }
            else
            {
                CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
 

           var version = ComplianceCertificate.GetCertificateTemplateVersion(CustomerID);

            long gettemid = checktemplteid(CustomerID);

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ComplianceCertificateTransaction obj = new ComplianceCertificateTransaction();
                obj.TemplateContent = theEditor.Content;
                obj.CreatedBy = AuthenticationHelper.UserID;
                obj.CreatedOn = DateTime.Now;
                obj.IsActive = true;
                obj.CustomerID = Convert.ToInt32(CustomerID);
                obj.Version = version;

                if (!string.IsNullOrEmpty(tbxFromDate.Text))
                    obj.FromDate = DateTime.ParseExact(Convert.ToString(tbxFromDate.Text).Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                if (!string.IsNullOrEmpty(tbxEndDate.Text))
                    obj.Enddate = DateTime.ParseExact(Convert.ToString(tbxEndDate.Text).Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture);


                if ((int)ViewState["Mode"] == 0)
                {
                    if (obj.FromDate < obj.Enddate)
                    {
                        bool isExists = ComplianceCertificate.ExistCertificateTemplate(obj);
                        if (isExists)
                        {
                            saveopo.Value = "true";
                            cvDuplicateEntry.ErrorMessage = "From date and end date in between last template version, Please select proper date.";
                            cvDuplicateEntry.IsValid = false;
                        }
                        else
                        {
                            entities.ComplianceCertificateTransactions.Add(obj);
                            entities.SaveChanges();
                            saveopo.Value = "true";
                            cvDuplicateEntry.ErrorMessage = "Record Saved Successfully.";
                            cvDuplicateEntry.IsValid = false;
                        }
                    }
                    else
                    {
                        saveopo.Value = "true";
                        cvDuplicateEntry.ErrorMessage = "From date must be less than End date.";
                        cvDuplicateEntry.IsValid = false;
                    }
                }
                else
                {
                    if (obj.FromDate < obj.Enddate)
                    {
                        int templateID = Convert.ToInt32(ViewState["TemplateID"]);
                        obj.ID = templateID;
                        Update(obj);
                        saveopo.Value = "true";
                        cvDuplicateEntry.ErrorMessage = "Record updated Sucessfully.";
                        cvDuplicateEntry.IsValid = false;
                    }
                    else
                    {
                        saveopo.Value = "true";
                        cvDuplicateEntry.ErrorMessage = "From date must be less than End date.";
                        cvDuplicateEntry.IsValid = false;
                    }
                }
                BindGrid();
                upcom.Update();
            }
        }

        public static int Update(ComplianceCertificateTransaction compliancecertt)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                ComplianceCertificateTransaction actToUpdate = (from row in entities.ComplianceCertificateTransactions
                                   where row.ID == compliancecertt.ID && row.CustomerID == compliancecertt.CustomerID
                                     select row).FirstOrDefault();

                actToUpdate.TemplateContent = compliancecertt.TemplateContent;
                actToUpdate.UpdatedBy = AuthenticationHelper.UserID;
                actToUpdate.UpdatedOn = DateTime.Now;
                actToUpdate.FromDate = compliancecertt.FromDate;
                actToUpdate.Enddate = compliancecertt.Enddate;

                entities.SaveChanges();
                return compliancecertt.ID;
            }
        }

        public static long checktemplteid(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ComplianceCertificateTransactions
                            where row.CustomerID == CustomerID
                            select row.ID).FirstOrDefault();
                return data;
    
            }
        }

        public static bool Exists(ComplianceCertificateTransaction comcert)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ComplianceCertificateTransactions
                             select row);

                if (comcert.ID > 0)
                {
                    query = query.Where(entry => entry.ID != comcert.ID);
                }

                return query.Select(entry => true).FirstOrDefault();
            }
        }

        protected void grdTemplate_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void upcom_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
    }
}