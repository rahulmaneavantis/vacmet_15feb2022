﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ReassignReviewerToPerformer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ddlNewUsers.BorderColor = Color.Empty;

            if (!IsPostBack)
            {
                //BindLocation();

                BindLocationFilter();

                btnSaveAssignment.Visible = false;
                divSelectionType.Visible = false;

                chkEvent.Checked = false;
                chkCheckList.Checked = false;
                txtAssigmnetFilter.Text = string.Empty;

              
                List<int> a = UserManagement.GetAssignedPerformerAndReviewer(AuthenticationHelper.UserID, AuthenticationHelper.CustomerID).ToList();
                if (a.Contains(4))
                {
                    divModifyAssignment.Visible = true;
                    User user = UserManagement.GetByID(AuthenticationHelper.UserID);

                    BindNewUserList(AuthenticationHelper.UserID ,AuthenticationHelper.CustomerID, ddlNewUsers);

                    if (AuthenticationHelper.CustomerID == 63)
                    {
                        ddlDocType.SelectedValue = "0";
                        ddlDocType.Items.Remove(ddlDocType.Items.FindByValue("-1"));
                        rbtSelectionType.SelectedValue = "1";
                        BindInternalComplianceInstances(AuthenticationHelper.UserID, Convert.ToInt32(user.CustomerID));
                        grdInternalComplianceInstances.Visible = true;
                        grdComplianceInstances.Visible = false;

                        ddlComType.Items.Remove(ddlComType.Items.FindByValue("0"));
                        ddlComType.Items.Remove(ddlComType.Items.FindByValue("1"));
                    }
                    else
                    {
                        ddlDocType.SelectedValue = "-1";
                        rbtSelectionType.SelectedValue = "0";
                        BindComplianceInstances(AuthenticationHelper.UserID, Convert.ToInt32(user.CustomerID));
                        grdInternalComplianceInstances.Visible = false;
                        grdComplianceInstances.Visible = true;
                    }
                    int IsIComplianceApp = 0;
                    Customer customer = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                    if (customer.IComplianceApplicable != null)
                    {
                        IsIComplianceApp = Convert.ToInt32(customer.IComplianceApplicable);
                    }
                    if (IsIComplianceApp.ToString() == "1")
                    {
                        divSelectionType.Visible = false;
                    }
                    else
                    {
                        divSelectionType.Visible = false;
                    }
                }
                else
                {
                    divModifyAssignment.Visible = false;
                }

                GetPageDisplaySummary();

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
        }

        //private void BindLocation()
        //{
        //    try
        //    {
        //        ddlLocation.DataTextField = "Name";
        //        ddlLocation.DataValueField = "ID";

        //        ddlLocation.DataSource = CustomerManagement.GetCustomerBranchList(AuthenticationHelper.UserID);
        //        ddlLocation.DataBind();

        //        ddlLocation.Items.Insert(0, new ListItem("Location", "-1"));
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        protected void linkbutton_onclick(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Controls/frmUpcomingCompliancessNew.aspx");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);
                string isstatutoryinternal = "";

                if (AuthenticationHelper.CustomerID == 63)
                {
                    ddlDocType.SelectedValue = "0";
                }

                if (ddlDocType.SelectedItem.Text == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (ddlDocType.SelectedItem.Text == "Internal")
                {
                    isstatutoryinternal = "I";
                }
                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindNewUserList(long UserID, long customerID, DropDownList ddllist)
        {
            try
            {
                var AllUsers = UserManagement.GetAllUserBranchAssigned(UserID,customerID);

                ddllist.DataTextField = "Name";
                ddllist.DataValueField = "ID";

                ddllist.DataSource = AllUsers;
                ddllist.DataBind();

                ddllist.Items.Insert(0, new ListItem("Select User to Assign", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlNewUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlNewUsers.SelectedValue != "-1")
                {
                    ddlNewUsers.BorderColor = Color.Empty;
                }
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }


        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                if (ddlDocType.SelectedValue == "-1")
                {
                    grdComplianceInstances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdComplianceInstances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                }
                else if (ddlDocType.SelectedValue == "0")
                {
                    grdInternalComplianceInstances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdInternalComplianceInstances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                FillCompliances();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        public void FillCompliances()
        {
            try
            {
                int customerid = Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID);
                User user = UserManagement.GetByID(AuthenticationHelper.UserID);

                int location = Convert.ToInt32(tvFilterLocation.SelectedValue);

                if (ddlDocType.SelectedValue == "-1")
                {
                    var ComplianceDocs = GetFilteredComplianceInstances(AuthenticationHelper.UserID, Convert.ToInt32(user.CustomerID), location);
                    grdComplianceInstances.DataSource = ComplianceDocs;
                    Session["TotalRows"] = ComplianceDocs.Count;
                    grdComplianceInstances.DataBind();

                    grdComplianceInstances.Visible = true;
                    grdInternalComplianceInstances.Visible = false;
                }
                else if (ddlDocType.SelectedValue == "0")
                {
                    var ComplianceDocs = GetFilteredInternalComplianceInstances(AuthenticationHelper.UserID, Convert.ToInt32(user.CustomerID), location);
                    grdInternalComplianceInstances.DataSource = ComplianceDocs;
                    Session["TotalRows"] = ComplianceDocs.Count;
                    grdInternalComplianceInstances.DataBind();

                    grdInternalComplianceInstances.Visible = true;
                    grdComplianceInstances.Visible = false;
                }

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                //DivRecordsScrum.Attributes.Remove("disabled");
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void txtAssigmnetFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                User user = UserManagement.GetByID(AuthenticationHelper.UserID);

                if (ddlDocType.SelectedValue == "-1")
                {
                    BindComplianceInstances(AuthenticationHelper.UserID, Convert.ToInt32(user.CustomerID));
                }
                else if (ddlDocType.SelectedValue == "0")
                {
                    BindInternalComplianceInstances(AuthenticationHelper.UserID, Convert.ToInt32(user.CustomerID));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void chkEvent_CheckedChanged(object sender, EventArgs e)
        {
            chkCheckList.Checked = false;
            if (!string.IsNullOrEmpty(Convert.ToString(AuthenticationHelper.UserID)))
            {
                User user = UserManagement.GetByID(AuthenticationHelper.UserID);
                BindComplianceInstances(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(user.CustomerID));
            }

        }

        protected void chkCheckList_CheckedChanged(object sender, EventArgs e)
        {
            chkEvent.Checked = false;
            if (!string.IsNullOrEmpty(Convert.ToString(AuthenticationHelper.UserID)))
            {
                User user = UserManagement.GetByID(AuthenticationHelper.UserID);
                BindComplianceInstances(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(user.CustomerID));
            }
        }

        protected void rbtSelectionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            User user = UserManagement.GetByID(AuthenticationHelper.UserID);

            if (rbtSelectionType.SelectedValue == "0")
            {
                BindComplianceInstances(AuthenticationHelper.UserID, Convert.ToInt32(user.CustomerID));
            }
            else
            {
                BindInternalComplianceInstances(AuthenticationHelper.UserID, Convert.ToInt32(user.CustomerID));
            }
        }

        private void BindComplianceInstances(int userID, int Customerid)
        {
            try
            {
                List<ComplianceAssignedInstancesView> datasource = new List<ComplianceAssignedInstancesView>();

                if (ddlComType.SelectedValue == "0")
                {
                    grdComplianceInstances.DataSource = null;
                    grdComplianceInstances.DataBind();

                    datasource = Business.ComplianceManagement.GetAllAssignedInstancesReassginExcludeNew("Y", userID: userID, customerId: Customerid, filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();

                    grdComplianceInstances.DataSource = datasource;
                    grdComplianceInstances.DataBind();

                    Session["TotalRows"] = datasource.Count;
                }

                else if (ddlComType.SelectedValue == "1")
                {
                    grdComplianceInstances.DataSource = null;
                    grdComplianceInstances.DataBind();

                    datasource = Business.ComplianceManagement.GetAllAssignedInstancesCheckListNew(userID: userID, customerId: Customerid, filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();

                    grdComplianceInstances.DataSource = datasource;
                    grdComplianceInstances.DataBind();

                    Session["TotalRows"] = datasource.Count;
                }
                else
                {
                    chkEvent.Checked = false;
                    chkCheckList.Checked = false;
                    grdComplianceInstances.DataSource = null;
                    grdComplianceInstances.DataBind();

                    datasource = Business.ComplianceManagement.GetAllAssignedInstancesReassginExcludeNew("N", userID: userID, customerId: Customerid, filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();

                    grdComplianceInstances.DataSource = datasource;
                    grdComplianceInstances.DataBind();

                    Session["TotalRows"] = datasource.Count;
                }

                grdInternalComplianceInstances.Visible = false;

                grdInternalComplianceInstances.DataSource = null;
                grdInternalComplianceInstances.DataBind();

                upModifyAssignment.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private List<ComplianceAssignedInstancesView> GetFilteredComplianceInstances(int userID, int Customerid, int BranchID)
        {
            List<ComplianceAssignedInstancesView> datasource = new List<ComplianceAssignedInstancesView>();

            if (ddlComType.SelectedValue == "0")
                datasource = Business.ComplianceManagement.GetAllAssignedInstancesReassginExcludeNew("Y", BranchID, userID: userID, customerId: Customerid, filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();

            else if (ddlComType.SelectedValue == "1")
                datasource = Business.ComplianceManagement.GetAllAssignedInstancesCheckListNew(BranchID, userID: userID, customerId: Customerid, filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();

            else
                datasource = Business.ComplianceManagement.GetAllAssignedInstancesReassginExcludeNew("N", BranchID, userID: userID, customerId: Customerid, filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();

            return datasource;
        }

        private List<InternalComplianceAssignedInstancesView> GetFilteredInternalComplianceInstances(int userID, int Customerid, int BranchID)
        {
            //List<InternalComplianceAssignedInstancesView> datasource = Business.InternalComplianceManagement.GetAllAssignedInstances(BranchID, userID: userID, customerId: Customerid, filter: txtAssigmnetFilter.Text).ToList();
            List<InternalComplianceAssignedInstancesView> datasource = Business.InternalComplianceManagement.GetAllAssignedInstancesReassginInternalExcludeNew(BranchID, userID: userID, customerId: Customerid, filter: txtAssigmnetFilter.Text).ToList();

            
            return datasource;
        }

        private void CheckBoxValueSaved()
        {
            List<Tuple<int, string, int, string>> chkList = new List<Tuple<int, string, int, string>>();
            int index = -1;
            foreach (GridViewRow gvrow in grdComplianceInstances.Rows)
            {
                index = Convert.ToInt32(grdComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox) gvrow.FindControl("chkCompliances")).Checked;
                string role = ((Label) gvrow.FindControl("lblRole")).Text;
                string olduserid = ((Label) gvrow.FindControl("lblUserID")).Text;
                var description = ((Label) gvrow.FindControl("lblDescription")).Text;
                //Tuple<int, string>  = new Tuple<int, string>();
                if (ViewState["AssignedCompliancesID"] != null)
                    chkList = (List<Tuple<int, string, int, string>>) ViewState["AssignedCompliancesID"];

                var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role) && entry.Item3.Equals(Convert.ToInt32(olduserid)) && entry.Item4.Equals(description)).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkList.Add(new Tuple<int, string, int, string>(index, role, Convert.ToInt32(olduserid), description));
                }
                else
                    chkList.Remove(checkedData);
            }
            if (chkList != null && chkList.Count > 0)
                ViewState["AssignedCompliancesID"] = chkList;
        }

        private void AssignCheckBoxexValue()
        {
            List<Tuple<int, string, int, string>> chkList = (List<Tuple<int, string, int, string>>) ViewState["AssignedCompliancesID"];

            if (chkList != null && chkList.Count > 0)
            {
                foreach (GridViewRow gvrow in grdComplianceInstances.Rows)
                {
                    int index = Convert.ToInt32(grdComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                    string role = ((Label) gvrow.FindControl("lblRole")).Text;
                    string olduserid = ((Label) gvrow.FindControl("lblUserID")).Text;

                    var checkedData = chkList.Where(entry => entry.Item1 == index && entry.Item2.ToString().Trim().Equals(role) && entry.Item3.Equals(Convert.ToInt32(olduserid))).FirstOrDefault();

                    if (chkList.Contains(checkedData))
                    {
                        CheckBox myCheckBox = (CheckBox) gvrow.FindControl("chkCompliances");
                        myCheckBox.Checked = true;
                    }
                }

                if (chkList.Count > 0)
                {
                    lblTotalSelected.Text = chkList.Count + " Selected";
                    btnSaveAssignment.Visible = true;
                }
                else if (chkList.Count == 0)
                {
                    lblTotalSelected.Text = "";
                    btnSaveAssignment.Visible = false;
                }
            }

            else
            {
                lblTotalSelected.Text = "";
                btnSaveAssignment.Visible = false;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SelectedPageNo.Text = "1";

            if (ddlDocType.SelectedValue == "-1")
            {
                ViewState["AssignedCompliancesID"] = null;
                grdComplianceInstances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdComplianceInstances.PageIndex = 0;
            }
            else if (ddlDocType.SelectedValue == "0")
            {
                ViewState["AssignedInternalCompliancesID"] = null;
                grdInternalComplianceInstances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdInternalComplianceInstances.PageIndex = 0;
            }

            //Reload the Grid
            FillCompliances();

            GetPageDisplaySummary();

            ShowSelectedRecords(sender, e);
        }

        protected void btnSaveAssignment_Click(object sender, EventArgs e)
        {
            try
            {
                string[] confirmValue = Request.Form["confirm_value"].Split(',');
                int customerID = -1;
                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                var AdminUser = UserManagement.GetCompanyAdminDataByCustID(customerID);
                string AdminName = string.Empty;
                string AdminEmail = string.Empty;
                User user = UserManagement.GetByID(AuthenticationHelper.UserID);
                string UserName = user.FirstName + " " + user.LastName;
                foreach (var item in AdminUser)
                {
                    AdminName = item.FirstName + " " + item.LastName;
                    AdminEmail = item.Email;
                }

                if (confirmValue[confirmValue.Length - 1] == "Yes")
                {
                    if (ddlNewUsers.SelectedValue != "-1")
                    {
                        string portalurl = string.Empty;
                        URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                        if (Urloutput != null)
                        {
                            portalurl = Urloutput.URL;
                        }
                        else
                        {
                            portalurl = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                        }

                        if (ddlDocType.SelectedValue == "-1") //Statutory
                        {
                            #region Statutory

                            CheckBoxValueSaved();

                            List<Tuple<int, string, int, string>> chkList = (List<Tuple<int, string, int, string>>) ViewState["AssignedCompliancesID"];

                            if (chkList != null)
                            {
                                Business.ComplianceManagement.ReplaceUserForComplianceAssignmentnew(Convert.ToInt32(ViewState["UserID"]), Convert.ToInt32(ddlNewUsers.SelectedValue), chkList);
                                
                                var newUser = UserManagement.GetByID(Convert.ToInt32(ddlNewUsers.SelectedValue));
                                foreach (var cil in chkList)
                                {
                                    var oldUser = UserManagement.GetByID(Convert.ToInt32(cil.Item3));
                                    //Admin approver Mail
                                    string ReplyEmailAddressName1 = CustomerManagement.GetByID(Convert.ToInt32(newUser.CustomerID)).Name;
                                    string message1 = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_ReassignPerformer
                                                       .Replace("@NewPerformer", newUser.FirstName + " " + newUser.LastName)
                                                       .Replace("@OldPerformer", oldUser.FirstName + " " + oldUser.LastName)
                                                       .Replace("@ComDescription", cil.Item4)
                                                       .Replace("@User", AdminName)
                                                       .Replace("@URL", Convert.ToString(portalurl))
                                                       .Replace("@ChangedBy", UserName)
                                                       .Replace("@From", ReplyEmailAddressName1);
                                    //string message1 = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_ReassignPerformer
                                    //                    .Replace("@NewPerformer", newUser.FirstName + " " + newUser.LastName)
                                    //                    .Replace("@OldPerformer", oldUser.FirstName + " " + oldUser.LastName)
                                    //                    .Replace("@ComDescription", cil.Item4)
                                    //                    .Replace("@User", AdminName)
                                    //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    //                    .Replace("@ChangedBy", UserName)
                                    //                    .Replace("@From", ReplyEmailAddressName1);

                                    new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { AdminEmail }).ToList(), null, null, "AVACOM Notification for re-assigning Internal Compliances.", message1); }).Start();
                                    //End Mail

                                    if (CustomerManagement.IsCustomerActive(Convert.ToInt64(newUser.CustomerID)))
                                    {
                                        string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(newUser.CustomerID)).Name;
                                        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignComplincesNewOwner
                                                          .Replace("@User", newUser.FirstName + " " + newUser.LastName)
                                                          .Replace("@oldUser", oldUser.FirstName + " " + oldUser.LastName)
                                                          .Replace("@URL", Convert.ToString(portalurl))
                                                          .Replace("@From", ReplyEmailAddressName);
                                        //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignComplincesNewOwner
                                        //                    .Replace("@User", newUser.FirstName + " " + newUser.LastName)
                                        //                    .Replace("@oldUser", oldUser.FirstName + " " + oldUser.LastName)
                                        //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        //                    .Replace("@From", ReplyEmailAddressName);

                                        new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { newUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning compliances.", message); }).Start();
                                    }


                                    if (CustomerManagement.IsCustomerActive(Convert.ToInt64(oldUser.CustomerID)))
                                    {
                                        string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(oldUser.CustomerID)).Name;
                                        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignComplincesOldOwner
                                                         .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                                         .Replace("@newUser", newUser.FirstName + " " + newUser.LastName)
                                                         .Replace("@URL", Convert.ToString(portalurl))
                                                         .Replace("@From", ReplyEmailAddressName);
                                        //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignComplincesOldOwner
                                        //                    .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                        //                    .Replace("@newUser", newUser.FirstName + " " + newUser.LastName)
                                        //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        //                    .Replace("@From", ReplyEmailAddressName);

                                        new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { oldUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning compliances.", message); }).Start();
                                    }
                                }
                                //BindUsers();
                                // ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divModifyAssignment\").dialog('close')", true);
                                ViewState["AssignedCompliancesID"] = null;
                            }
                            else
                            {
                                CustomModifyAsignment.IsValid = false;
                                CustomModifyAsignment.ErrorMessage = "Please select at list one compliance for proceed.";
                            }

                            #endregion
                        }
                        else if (ddlDocType.SelectedValue == "0") //Internal
                        {
                            #region internal

                            InternalCheckBoxValueSaved();

                            List<Tuple<int, string, int>> chkIList = (List<Tuple<int, string, int>>) ViewState["AssignedInternalCompliancesID"];

                            if (chkIList != null)
                            {
                                Business.InternalComplianceManagement.ReplaceUserForComplianceAssignmentNew(Convert.ToInt32(ViewState["UserID"]), Convert.ToInt32(ddlNewUsers.SelectedValue), chkIList);
                                var newUser = UserManagement.GetByID(Convert.ToInt32(ddlNewUsers.SelectedValue));

                                foreach (var cil in chkIList)
                                {
                                    var oldUser = UserManagement.GetByID(Convert.ToInt32(cil.Item3));

                                    string message1 = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_ReassignPerformer
                                                        .Replace("@NewPerformer", newUser.FirstName + " " + newUser.LastName)
                                                        .Replace("@OldPerformer", oldUser.FirstName + " " + oldUser.LastName)
                                                        .Replace("@ComDescription", " ")
                                                        .Replace("@User", AdminName)
                                                        .Replace("@URL", Convert.ToString(portalurl))
                                                        .Replace("@From", UserName);

                                    //string message1 = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_ReassignPerformer
                                    //                    .Replace("@NewPerformer", newUser.FirstName + " " + newUser.LastName)
                                    //                    .Replace("@OldPerformer", oldUser.FirstName + " " + oldUser.LastName)
                                    //                    .Replace("@ComDescription", " ")
                                    //                    .Replace("@User", AdminName)
                                    //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    //                    .Replace("@From", UserName);

                                    new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { AdminEmail }).ToList(), null, null, "AVACOM Notification for re-assigning Internal Compliances.", message1); }).Start();



                                    if (CustomerManagement.IsCustomerActive(Convert.ToInt64(newUser.CustomerID)))
                                    {
                                        string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(newUser.CustomerID)).Name;
                                        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignInternalComplincesNewOwner
                                                           .Replace("@User", newUser.FirstName + " " + newUser.LastName)
                                                           .Replace("@oldUser", oldUser.FirstName + " " + oldUser.LastName)
                                                           .Replace("@URL", Convert.ToString(portalurl))
                                                           .Replace("@From", ReplyEmailAddressName);
                                        //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignInternalComplincesNewOwner
                                        //                    .Replace("@User", newUser.FirstName + " " + newUser.LastName)
                                        //                    .Replace("@oldUser", oldUser.FirstName + " " + oldUser.LastName)
                                        //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        //                    .Replace("@From", ReplyEmailAddressName);

                                        new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { newUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning Internal Compliances.", message); }).Start();
                                    }


                                    if (CustomerManagement.IsCustomerActive(Convert.ToInt64(oldUser.CustomerID)))
                                    {
                                        string ReplyEmailAddressName = CustomerManagement.GetByID(Convert.ToInt32(oldUser.CustomerID)).Name;
                                        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignInternalComplincesOldOwner
                                                     .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                                     .Replace("@newUser", newUser.FirstName + " " + newUser.LastName)
                                                     .Replace("@URL", Convert.ToString(portalurl))
                                                     .Replace("@From", ReplyEmailAddressName);

                                        //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_ReassignInternalComplincesOldOwner
                                        //                    .Replace("@User", oldUser.FirstName + " " + oldUser.LastName)
                                        //                    .Replace("@newUser", newUser.FirstName + " " + newUser.LastName)
                                        //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        //                    .Replace("@From", ReplyEmailAddressName);

                                        new Thread(() => { EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { oldUser.Email }).ToList(), null, null, "AVACOM Notification for re-assigning Internal Compliances.", message); }).Start();
                                    }
                                }
                                // ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divModifyAssignment\").dialog('close')", true);
                                ViewState["AssignedInternalCompliancesID"] = null;
                            }
                            else
                            {
                                CustomModifyAsignment.IsValid = false;
                                CustomModifyAsignment.ErrorMessage = "Please select at list one compliance for proceed.";
                            }

                            #endregion
                        }

                        // User users = UserManagement.GetByID(AuthenticationHelper.UserID);

                        if (ddlDocType.SelectedValue == "-1")
                        {
                            BindComplianceInstances(AuthenticationHelper.UserID, Convert.ToInt32(user.CustomerID));
                        }
                        else if (ddlDocType.SelectedValue == "0")
                        {
                            BindInternalComplianceInstances(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(user.CustomerID));
                        }
                    }
                    else
                    {
                        ddlNewUsers.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                        cvNewUsers.IsValid = false;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void chkCompliancesHeader_CheckedChanged(object sender, EventArgs e)
        {
            if (ddlDocType.SelectedValue == "-1")
            {
                CheckBox ChkBoxHeader = (CheckBox) grdComplianceInstances.HeaderRow.FindControl("chkCompliancesHeader");

                foreach (GridViewRow row in grdComplianceInstances.Rows)
                {
                    CheckBox chkCompliances = (CheckBox) row.FindControl("chkCompliances");

                    if (ChkBoxHeader.Checked)
                        chkCompliances.Checked = true;
                    else
                        chkCompliances.Checked = false;
                }
            }

            else if (ddlDocType.SelectedValue == "0")
            {
                CheckBox ChkBoxHeader = (CheckBox) grdInternalComplianceInstances.HeaderRow.FindControl("chkICompliancesHeader");

                foreach (GridViewRow row in grdInternalComplianceInstances.Rows)
                {
                    CheckBox chkICompliances = (CheckBox) row.FindControl("chkICompliances");

                    if (ChkBoxHeader.Checked)
                        chkICompliances.Checked = true;
                    else
                        chkICompliances.Checked = false;
                }
            }

            ShowSelectedRecords(sender, e);
        }

        protected void chkCompliances_CheckedChanged(object sender, EventArgs e)
        {
            int Count = 0;

            if (ddlDocType.SelectedValue == "-1")
            {
                CheckBox ChkBoxHeader = (CheckBox) grdComplianceInstances.HeaderRow.FindControl("chkCompliancesHeader");

                foreach (GridViewRow row in grdComplianceInstances.Rows)
                {
                    CheckBox chkCompliances = (CheckBox) row.FindControl("chkCompliances");

                    if (chkCompliances.Checked)
                        Count++;
                }

                if (Count == grdComplianceInstances.Rows.Count)
                    ChkBoxHeader.Checked = true;
                else
                    ChkBoxHeader.Checked = false;
            }

            else if (ddlDocType.SelectedValue == "0")
            {
                CheckBox ChkBoxHeader = (CheckBox) grdInternalComplianceInstances.HeaderRow.FindControl("chkICompliancesHeader");

                foreach (GridViewRow row in grdInternalComplianceInstances.Rows)
                {
                    CheckBox chkCompliances = (CheckBox) row.FindControl("chkICompliances");

                    if (chkCompliances.Checked)
                        Count++;
                }

                if (Count == grdInternalComplianceInstances.Rows.Count)
                    ChkBoxHeader.Checked = true;
                else
                    ChkBoxHeader.Checked = false;
            }

            ShowSelectedRecords(sender, e);
        }

        protected void grdComplianceInstances_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                CheckBoxValueSaved();
                grdComplianceInstances.PageIndex = e.NewPageIndex;
                if (!string.IsNullOrEmpty(Convert.ToString(AuthenticationHelper.UserID)))
                {
                    User user = UserManagement.GetByID(AuthenticationHelper.UserID);
                    BindComplianceInstances(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(user.CustomerID));
                }
                AssignCheckBoxexValue();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdComplianceInstances_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                List<ComplianceAssignedInstancesView> assignmentList = new List<ComplianceAssignedInstancesView>();
                if (chkEvent.Checked == true)
                {
                    chkCheckList.Checked = false;
                    assignmentList = Business.ComplianceManagement.GetAllAssignedInstancesReassginExcludeNew("Y", userID: Convert.ToInt32(AuthenticationHelper.UserID), filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                }
                else if (chkCheckList.Checked == true)
                {
                    chkEvent.Checked = false;
                    assignmentList = Business.ComplianceManagement.GetAllAssignedInstancesCheckListNew(userID: Convert.ToInt32(AuthenticationHelper.UserID), filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                }
                else
                {
                    chkEvent.Checked = false;
                    chkCheckList.Checked = false;
                    assignmentList = Business.ComplianceManagement.GetAllAssignedInstancesReassginExcludeNew("N", userID: Convert.ToInt32(AuthenticationHelper.UserID), filter: txtAssigmnetFilter.Text).GroupBy(entry => entry.ComplianceInstanceID).Select(entry => entry.FirstOrDefault()).ToList();
                }

                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdComplianceInstances.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["InstancesSortIndex"] = grdComplianceInstances.Columns.IndexOf(field);
                    }
                }

                grdComplianceInstances.DataSource = assignmentList;
                grdComplianceInstances.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceInstances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = Convert.ToInt32(ViewState["InstancesSortIndex"]);
                    if (sortColumnIndex != 0 || sortColumnIndex != -1)
                    {
                        if (!(sortColumnIndex == 0))
                            AddInstancesSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceInstances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox headerchk = (CheckBox) grdComplianceInstances.HeaderRow.FindControl("chkCompliancesHeader");
                    CheckBox childchk = (CheckBox) e.Row.FindControl("chkCompliances");
                    childchk.CheckedChanged += new System.EventHandler(this.Childchk_CheckedChanged);

                    //childchk.Attributes.Add("onclick", "javascript:Selectchildcheckboxes('" + headerchk.ClientID + "','grdComplianceDocument')");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void Childchk_CheckedChanged(object sender, EventArgs e)
        {
            ShowSelectedRecords(sender, e);
        }

        protected void AddInstancesSortImage(int columnIndex, GridViewRow headerRow)
        {
            try
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../Images/SortAsc.gif";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../Images/SortDesc.gif";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindInternalComplianceInstances(int userID, int Customerid)
        {
            try
            {
                List<InternalComplianceAssignedInstancesView> datasource = Business.InternalComplianceManagement.GetAllAssignedInstancesReassginInternalExcludeNew(userID: userID, customerId: Customerid, filter: txtAssigmnetFilter.Text).ToList();
                grdInternalComplianceInstances.DataSource = datasource;
                grdInternalComplianceInstances.DataBind();

                Session["TotalRows"] = datasource.Count;

                grdComplianceInstances.DataSource = null;
                grdComplianceInstances.DataBind();
                upModifyAssignment.Update();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void InternalCheckBoxValueSaved()
        {
            List<Tuple<int, string, int>> chkIList = new List<Tuple<int, string, int>>();
            int index = -1;
            foreach (GridViewRow gvrow in grdInternalComplianceInstances.Rows)
            {
                index = Convert.ToInt32(grdInternalComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                bool result = ((CheckBox) gvrow.FindControl("chkICompliances")).Checked;
                string role = ((Label) gvrow.FindControl("lblIRole")).Text;
                string olduserid = ((Label) gvrow.FindControl("lblIUserID")).Text;

                if (ViewState["AssignedInternalCompliancesID"] != null)
                    chkIList = (List<Tuple<int, string, int>>) ViewState["AssignedInternalCompliancesID"];

                var checkedData = chkIList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role) && entry.Item3.Equals(Convert.ToInt32(olduserid))).FirstOrDefault();
                if (result)
                {
                    if (checkedData == null)
                        chkIList.Add(new Tuple<int, string, int>(index, role, Convert.ToInt32(olduserid)));
                }
                else
                    chkIList.Remove(checkedData);
            }
            if (chkIList != null && chkIList.Count > 0)
                ViewState["AssignedInternalCompliancesID"] = chkIList;
        }

        private void InternalAssignCheckBoxexValue()
        {
            List<Tuple<int, string, int>> chkIList = (List<Tuple<int, string, int>>) ViewState["AssignedInternalCompliancesID"];

            if (chkIList != null && chkIList.Count > 0)
            {
                foreach (GridViewRow gvrow in grdInternalComplianceInstances.Rows)
                {
                    int index = Convert.ToInt32(grdInternalComplianceInstances.DataKeys[gvrow.RowIndex].Value);
                    string role = ((Label) gvrow.FindControl("lblIRole")).Text;
                    string olduserid = ((Label) gvrow.FindControl("lblIUserID")).Text;

                    var checkedIData = chkIList.Where(entry => entry.Item1 == index && entry.Item2.Equals(role) && entry.Item3.Equals(Convert.ToInt32(olduserid))).FirstOrDefault();

                    if (chkIList.Contains(checkedIData))
                    {
                        CheckBox myCheckBox = (CheckBox) gvrow.FindControl("chkICompliances");
                        myCheckBox.Checked = true;
                    }
                }

                if (chkIList.Count > 0)
                {
                    lblTotalSelected.Text = chkIList.Count + " Selected";
                    btnSaveAssignment.Visible = true;
                }
                else if (chkIList.Count == 0)
                {
                    lblTotalSelected.Text = "";
                    btnSaveAssignment.Visible = false;
                }
            }

            else
            {
                lblTotalSelected.Text = "";
                btnSaveAssignment.Visible = false;
            }
        }

        protected void grdInternalComplianceInstances_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                InternalCheckBoxValueSaved();
                grdInternalComplianceInstances.PageIndex = e.NewPageIndex;
                User user = UserManagement.GetByID(AuthenticationHelper.UserID);
                BindInternalComplianceInstances(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(user.CustomerID));
                InternalAssignCheckBoxexValue();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdInternalComplianceInstances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = Convert.ToInt32(ViewState["InternalInstancesSortIndex"]);

                    if (sortColumnIndex != 0 || sortColumnIndex != -1)
                    {
                        if (!(sortColumnIndex == 0))
                            AddInstancesSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdInternalComplianceInstances_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var InternalassignmentList = Business.InternalComplianceManagement.GetAllAssignedInstancesReassginInternalExcludeNew(userID: Convert.ToInt32(AuthenticationHelper.UserID), filter: txtAssigmnetFilter.Text).OrderBy(entry => entry.Branch).ToList();
                if (direction == SortDirection.Ascending)
                {
                    InternalassignmentList = InternalassignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    InternalassignmentList = InternalassignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }
                foreach (DataControlField field in grdInternalComplianceInstances.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["InternalInstancesSortIndex"] = grdInternalComplianceInstances.Columns.IndexOf(field);
                    }
                }
                grdInternalComplianceInstances.DataSource = InternalassignmentList;
                grdInternalComplianceInstances.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ShowSelectedRecords(object sender, EventArgs e)
        {
            lblTotalSelected.Text = "";

            if (ddlDocType.SelectedValue == "-1")
            {
                CheckBoxValueSaved();
                AssignCheckBoxexValue();
            }
            else if (ddlDocType.SelectedValue == "0")
            {
                InternalCheckBoxValueSaved();
                InternalAssignCheckBoxexValue();
            }
        }

        protected void upModifyAssignment_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ddlDocType.SelectedValue == "-1")
                {
                    CheckBoxValueSaved();

                    grdComplianceInstances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdComplianceInstances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                    FillCompliances();

                    AssignCheckBoxexValue();
                }
                else if (ddlDocType.SelectedValue == "0")
                {
                    InternalCheckBoxValueSaved();

                    grdInternalComplianceInstances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdInternalComplianceInstances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                    FillCompliances();

                    InternalAssignCheckBoxexValue();
                }
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ddlDocType.SelectedValue == "-1")
                {
                    CheckBoxValueSaved();

                    grdComplianceInstances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdComplianceInstances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                    //Reload the Grid
                    FillCompliances();

                    AssignCheckBoxexValue();
                }
                else if (ddlDocType.SelectedValue == "0")
                {
                    InternalCheckBoxValueSaved();

                    grdInternalComplianceInstances.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdInternalComplianceInstances.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                    //Reload the Grid
                    FillCompliances();

                    InternalAssignCheckBoxexValue();
                }
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
        }
    }
}