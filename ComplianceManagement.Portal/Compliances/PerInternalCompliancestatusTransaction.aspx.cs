﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Configuration;
using Ionic.Zip;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3;
using Amazon;
using Amazon.S3.Model;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class PerInternalCompliancestatusTransaction : System.Web.UI.Page
    {
        public static string LicenseDocPath = "";
        static string sampleFormPath4 = "";
        public static string subTaskDocViewPath = "";
        public static List<long> Tasklist = new List<long>();
        public static List<SP_TaskInstanceTransactionInternalView_Result> MastersTasklistinternal = new List<SP_TaskInstanceTransactionInternalView_Result>();
        public static List<TaskDocumentsView> MastersTaskDocumentslistinternal = new List<TaskDocumentsView>();
        protected static bool IsDocumentCompulsary = false;
        protected string UploadDocumentLink;
        //protected static string status;
        protected static int compInstanceID;
        public string ISmail;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string listCustomer = ConfigurationManager.AppSettings["UploadUsingLinkCustomerlist"];
                string[] listCust = new string[] { };
                if (!string.IsNullOrEmpty(listCustomer))
                    listCust = listCustomer.Split(',');

                if (listCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                    UploadDocumentLink = "True";
                else
                    UploadDocumentLink = "False";

                ISmail = "0";
                try
                {
                    ISmail = Convert.ToString(Request.QueryString["ISM"]);
                    if (!string.IsNullOrEmpty(ISmail) && ISmail == "1")
                    {
                        lnkgotoportal.Visible = true;
                    }
                    else
                    {
                        lnkgotoportal.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "ISmail", MethodBase.GetCurrentMethod().Name);
                    ISmail = "0";
                }

                if (!Page.IsPostBack)
                {
                    compInstanceID = Convert.ToInt32(Request.QueryString["CID"].ToString());
                    //status = Convert.ToString(Request.QueryString["status"].ToString());
                    OpenTransactionPage(Convert.ToInt32(Request.QueryString["SOID"].ToString()), Convert.ToInt32(Request.QueryString["CID"].ToString()));
                    
                    string listLockingCustomer = ConfigurationManager.AppSettings["LockingDaysCustomerlist"];
                    string[] listLockCust = new string[] { };
                    if (!string.IsNullOrEmpty(listLockingCustomer))
                        listLockCust = listLockingCustomer.Split(',');

                    if (listLockCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                    {
                        if (Business.ComplianceManagement.GetCurrentStatusByScheduleONID(Convert.ToInt32(Request.QueryString["SOID"].ToString()), "I", Convert.ToInt32(AuthenticationHelper.CustomerID)))
                            btnSave2.Enabled = false;
                        else
                            btnSave2.Enabled = true;
                    }
                }

                //int LicID = Convert.ToInt32(Session["LicenseID"]);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var LicID = (from row in entities.Lic_tbl_LicenseInternalComplianceInstanceMapping
                                 where row.ComplianceInstanceID == compInstanceID
                                 select row.LicenseID).FirstOrDefault();

                    var LicRecentStatus = (from row in entities.RecentInternalLicenseTransactionViews
                                           where row.LicenseID == LicID
                                           select row.StatusID).FirstOrDefault();


                    if (LicRecentStatus == 9 || LicRecentStatus == 11)
                    {
                        //ddlStatus.SelectedItem.Text = "Registered";
                        ddlStatus2.SelectedValue = Convert.ToString(LicRecentStatus);
                        ddlStatus2.Attributes.Add("disabled", "disabled");
                        tbxDate2.Attributes.Add("disabled", "disabled");
                        tbxRemarks2.Attributes.Add("disabled", "disabled");
                        btnSave2.Attributes.Add("disabled", "disabled");
                        //btnSaveDOCNotCompulsory.Visible = false;
                        btnSave2.Enabled = false;
                    }
                    else
                    {
                        ddlStatus2.Attributes.Remove("disabled");
                        tbxDate2.Attributes.Remove("disabled");
                        tbxRemarks2.Attributes.Remove("disabled");
                        btnSave2.Attributes.Remove("disabled");

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkgotoportal_Click(object sender, EventArgs e)
        {
            try
            {
                ProductMappingStructure _obj = new ProductMappingStructure();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _obj.ReAuthenticate_UserNew();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        private void BindTransactionDetails(int ScheduledOnID, int complianceInstanceID)
        {
            var ICSOID = Convert.ToInt64(ScheduledOnID);
            var RCT = Business.InternalComplianceManagement.GetCurrentStatusByInternalComplianceID((int)ICSOID);
            var LRCT = Business.InternalComplianceManagement.GetCurrentStatusOfInternalLicenseByScheduleOnID((int)ICSOID);
            
            if (RCT.ComplianceStatusID == 2 || RCT.ComplianceStatusID == 3 || RCT.ComplianceStatusID == 4 || RCT.ComplianceStatusID == 5)
            {
                if (LRCT.Status == "Registered" || LRCT.Status == "Validity Expired")
                {

                }
                else
                {
                    btnSave2.Visible = false;
                    cvDuplicateEntry.IsValid = false;
                    //cvDuplicateEntry.ErrorMessage = "This compliance already performed.";
                    cvDuplicateEntry.ErrorMessage = "This compliance is already submitted for review or already closed.";
                }
            }
            //else
            //{
            //btnSave2.Visible = true;
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lblLicenseType.Text = string.Empty;
                    lblLicenseNumber.Text = string.Empty;
                    lblLicenseTitle.Text = string.Empty;
                    txtLicenseTitle.Text = string.Empty;
                    lblApplicationDate.Text = string.Empty;
                    lblStartdate.Text = string.Empty;
                    lblEndDate.Text = string.Empty;
                    lblRisk2.Text = string.Empty;
                    txtCost.Text = string.Empty;

                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    int customerbranchID = -1;
                    var AllComData = Business.ComplianceManagement.GetPeriodLocationPerformerInternal(ScheduledOnID, complianceInstanceID);
                    if (AllComData != null)
                    {
                        customerbranchID = AllComData.CustomerBranchID;
                        lblLocation.Text = AllComData.Branch;
                        lblDueDate.Text = Convert.ToDateTime(AllComData.InternalScheduledOn).ToString("dd-MMM-yyyy");
                        hiddenDueDateInternal.Value = Convert.ToDateTime(AllComData.InternalScheduledOn).ToString("dd-MM-yyyy");
                        if (AuthenticationHelper.CustomerID == 63)
                        {
                            lblPeriod1.Text = Business.ComplianceManagement.PeriodReplace(AllComData.ForMonth);
                            lblPeriod.Text = AllComData.ForMonth;
                        }
                        else
                        {
                            lblPeriod1.Text = AllComData.ForMonth;
                            lblPeriod.Text = AllComData.ForMonth;
                        }
                    }
                    var IsAfter = TaskManagment.IsTaskAfterInternal(ScheduledOnID, 3, lblPeriod.Text, customerbranchID, customerID);
                    var showHideButton = false;
                    if (IsAfter == false)
                    {
                        showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, customerbranchID);
                        btnSave2.Enabled = showHideButton;
                    }
                    else
                    {
                        showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, customerbranchID);
                        btnSave2.Enabled = true;
                    }

                    ViewState["IsAfter"] = IsAfter;
                    ViewState["Period"] = lblPeriod.Text;
                    ViewState["CustomerBrachID"] = customerbranchID;

                    var complianceInfo = Business.InternalComplianceManagement.GetInternalComplianceByInstanceID(ScheduledOnID);
                    var complianceCategory = Business.InternalComplianceManagement.GetInternalComplianceCategoryByInstanceID(ScheduledOnID);
                    var complianceForm = Business.InternalComplianceManagement.GetInternalComplianceFormByID(complianceInfo.ID);
                    var RecentComplianceTransaction = Business.InternalComplianceManagement.GetCurrentStatusByInternalComplianceID(ScheduledOnID);
                    if (complianceInfo != null)
                    {
                        var LicenseID = (from row in entities.Lic_tbl_LicenseInternalComplianceInstanceMapping
                                         where row.ComplianceInstanceID == complianceInstanceID
                                         select row.LicenseID).FirstOrDefault();

                        var MasterTransction = InternalLicenseMgmt.GetPreviousInternalLicenseDetail(LicenseID);
                        if (MasterTransction != null)
                        {
                            Session["LicenseType"] = MasterTransction.LicensetypeName;
                            Session["LicenseTypeID"] = MasterTransction.LicenseTypeID;
                            Session["LicenseID"] = MasterTransction.LicenseID;
                            lblLicenseType.Text = MasterTransction.LicensetypeName;
                            lblLicenseNumber.Text = MasterTransction.LicenseNo;
                            lblLicenseTitle.Text = MasterTransction.Licensetitle;
                            txtLicenseTitle.Text = MasterTransction.Licensetitle;
                            lblApplicationDate.Text = Convert.ToDateTime(MasterTransction.ApplicationDate).ToString("dd-MMM-yyyy");
                            lblStartdate.Text = Convert.ToDateTime(MasterTransction.StartDate).ToString("dd-MMM-yyyy");
                            lblEndDate.Text = Convert.ToDateTime(MasterTransction.EndDate).ToString("dd-MMM-yyyy");
                            txtCost.Text = Convert.ToString(MasterTransction.Cost);
                            var documentVersionData = InternalLicenseMgmt.GetInternalFileData(LicenseID, -1).Select(x => new
                            {
                                ID = x.ID,
                                ScheduledOnID = x.ScheduledOnID,
                                LicenseID = x.LicenseID,
                                FileID = x.FileID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                                ISLink = x.ISLink,
                                FilePath = x.FilePath,
                                FileName = x.FileName
                            }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();


                            rptLicenseVersion.DataSource = documentVersionData;
                            rptLicenseVersion.DataBind();

                            rptLicenseDocumnets.DataSource = null;
                            rptLicenseDocumnets.DataBind();
                        }
                        var LICInfo = GetByLICID(LicenseID);
                        txtFileno.Text = LICInfo.FileNO;
                        txtphysicalLocation.Text = LICInfo.PhysicalLocation;
                        lblComplianceID2.Text = Convert.ToString(complianceInfo.ID);
                        lblCategory.Text = Convert.ToString(complianceCategory.Name);
                        lblComplianceDiscription2.Text = complianceInfo.IShortDescription;
                        if (complianceInfo.IsDocumentRequired != true)
                        {
                            HDNIsDocumentCompulsary.Value = "F";
                            IsDocumentCompulsary = false;
                        }
                        else
                        {
                            HDNIsDocumentCompulsary.Value = "T";
                            IsDocumentCompulsary = true;
                        }
                        lblFrequency2.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.IFrequency != null ? (int)complianceInfo.IFrequency : -1));
                        string risk = Business.InternalComplianceManagement.GetRiskType(complianceInfo);
                        lblRiskType2.Text = Business.InternalComplianceManagement.GetRisk(complianceInfo);
                        lblRisk2.Text = risk;

                        if (risk == "HIGH")
                        {
                            divRiskType2.Attributes["style"] = "background-color:red;";
                        }
                        else if (risk == "CRITICAL")
                        {
                            divRiskType2.Attributes["style"] = "background-color:red;";
                        }
                        else if (risk == "MEDIUM")
                        {
                            divRiskType2.Attributes["style"] = "background-color:yellow;";
                        }
                        else if (risk == "LOW")
                        {
                            divRiskType2.Attributes["style"] = "background-color:green;";
                        }

                        if (RecentComplianceTransaction.ComplianceStatusID == 10)
                        {
                            BindDocument(ScheduledOnID);
                        }
                        else
                        {
                            divDeleteDocument.Visible = false;
                            rptComplianceDocumnets.DataSource = null;
                            rptComplianceDocumnets.DataBind();
                            rptWorkingFiles.DataSource = null;
                            rptWorkingFiles.DataBind();
                        }
                        grdInternalDocument.DataSource = null;
                        grdInternalDocument.DataBind();
                        BindTempDocumentData(ScheduledOnID);
                        if (complianceForm != null)
                        {
                            lnkViewSampleForm4.Visible = true;
                            lblSlash1.Visible = true;
                            lbDownloadSample4.Text = "<u style=color:blue>Click here</u> to download sample form.";
                            lbDownloadSample4.CommandArgument = complianceForm.IComplianceID.ToString();
                            lbDownloadSample4.Text = "Download";
                            lblNote.Visible = true;

                            sampleFormPath4 = complianceForm.Name;
                            lnkViewSampleForm4.Visible = true;
                            lblSlash1.Visible = true;

                            #region save samle form
                            string Filename = complianceForm.Name;
                            string filePath = sampleFormPath4;
                            if (filePath != null)
                            {
                                try
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                    string DateFolder = Folder + "/" + File;
                                    string extension = System.IO.Path.GetExtension(filePath);
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                    string FileName = DateFolder + "/" + User + "" + extension;
                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    bw.Write(complianceForm.FileData);
                                    bw.Close();
                                    lblpathsample4.Text = FileName;
                                }
                                catch (Exception ex)
                                {

                                }
                                var complianceform = Business.InternalComplianceManagement.GetMultipleInternalComplianceForm(Convert.ToInt32(lbDownloadSample4.CommandArgument));
                                rptComplianceSampleView.DataSource = complianceform;
                                rptComplianceSampleView.DataBind();
                            }
                            #endregion

                        }
                        else
                        {
                            lblNote.Visible = false;
                            lblSlash1.Visible = false;
                            lnkViewSampleForm4.Visible = false;
                            sampleFormPath4 = "";
                        }
                    }



                    btnSave2.Attributes.Remove("disabled");
                    if (RecentComplianceTransaction.ComplianceStatusID == 1 || RecentComplianceTransaction.ComplianceStatusID == 6 || RecentComplianceTransaction.ComplianceStatusID == 10)
                    {
                        divUploadDocument.Visible = true;
                        divWorkingfiles.Visible = true;
                    }
                    else if (RecentComplianceTransaction.ComplianceStatusID != 1)
                    {
                        divUploadDocument.Visible = false;
                        divWorkingfiles.Visible = false;
                        if ((complianceInfo.IUploadDocument ?? false))
                        {
                            btnSave2.Attributes.Add("disabled", "disabled");
                        }
                    }

                    BindStatusList(Convert.ToInt32(RecentComplianceTransaction.ComplianceStatusID));
                    BindTransactions(ScheduledOnID);
                    tbxRemarks2.Text = string.Empty;
                    hdnComplianceInstanceID.Value = complianceInstanceID.ToString();
                    hdnComplianceScheduledOnId.Value = ScheduledOnID.ToString();
                    upComplianceDetails.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            // }
        }
        public static Lic_tbl_InternalLicenseInstance_Log GetByLICID(long LICID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.Lic_tbl_InternalLicenseInstance_Log
                            where row.LicenseID == LICID 
                            select row).FirstOrDefault();

                return data;
            }
        }
        public void BindDocument(int ScheduledOnID)
        {
            try
            {
                List<GetInternalComplianceDocumentsView> ComplianceDocument = new List<GetInternalComplianceDocumentsView>();
                ComplianceDocument = DocumentManagement.GetFileDataInternal(ScheduledOnID).Where(entry => entry.Version.Equals("1.0")).ToList();
                rptComplianceDocumnets.DataSource = ComplianceDocument.Where(entry => entry.FileType == 1).ToList();
                rptComplianceDocumnets.DataBind();

                rptWorkingFiles.DataSource = ComplianceDocument.Where(entry => entry.FileType == 2).ToList();
                rptWorkingFiles.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory2_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var complianceTransactionHistory = Business.InternalComplianceManagement.GetAllTransactionLog(Convert.ToInt32(ViewState["InternalScheduledOnID"]));
                if (direction == SortDirection.Ascending)
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdTransactionHistory2.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndexHistory"] = grdTransactionHistory2.Columns.IndexOf(field);
                    }
                }

                grdTransactionHistory2.DataSource = complianceTransactionHistory;
                grdTransactionHistory2.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory2_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTransactionHistory2.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["InternalScheduledOnID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactions(int ScheduledOnID)
        {
            try
            {                
                grdTransactionHistory2.DataSource = Business.InternalComplianceManagement.GetAllTransactionLog(ScheduledOnID);
                grdTransactionHistory2.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindStatusList(int statusID)
        {
            try
            {
                //ddlStatus2.DataSource = null;
                //ddlStatus2.DataBind();
                //ddlStatus2.ClearSelection();

                //ddlStatus2.DataTextField = "Name";
                //ddlStatus2.DataValueField = "ID";

                //var statusList = ComplianceStatusManagement.GetStatusList();

                //List<ComplianceStatu> allowedStatusList = null;

                //List<ComplianceStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
                //List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

                //allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.Name).ToList();

                //ddlStatus2.DataSource = allowedStatusList;
                //ddlStatus2.DataBind();

                //ddlStatus2.Items.Insert(0, new ListItem("< Select >", "-1"));


                ddlStatus2.DataSource = null;
                ddlStatus2.DataBind();
                ddlStatus2.ClearSelection();
                ddlStatus2.DataTextField = "StatusName";
                ddlStatus2.DataValueField = "ID";
                // var statusList = LicenseMgmt.GetStatusList();
                string LicTypeName = Convert.ToString(Session["LicenseType"]);
                long LicTypeID = Convert.ToInt32(Session["LicenseTypeID"]);

                List<Lic_tbl_StatusMaster> statusList;
                string status = string.Empty;
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    status = (from row in entities.Lic_tbl_type
                              where row.CustomerID == customerID &&
                              LicTypeName.Contains(row.Status)
                              select row.Status).FirstOrDefault();

                }
                if (status != null)
                {
                    if (LicTypeName == status)
                    {
                        statusList = LicenseMgmt.GetStatusListIPR();
                    }
                    else
                    {
                        statusList = LicenseMgmt.GetStatusList();
                    }
                }
                else
                {
                    statusList = LicenseMgmt.GetStatusList();
                }
                ddlStatus2.DataSource = statusList.OrderBy(entry => entry.StatusName).ToList(); ;
                ddlStatus2.DataBind();
                ddlStatus2.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public int comparedate(DateTime DateA, DateTime DateB)
        {
            DateTime dt1 = DateTime.ParseExact(DateA.ToString("dd-MM-yyyy"), "dd-MM-yyyy", null);
            DateTime dt2 = DateTime.ParseExact(DateB.ToString("dd-MM-yyyy"), "dd-MM-yyyy", null);

            //if (momentA > momentB) return 1;
            //else if (momentA < momentB) return -1;
            //else return 0;

            int cmp = dt1.CompareTo(dt2);

            if (cmp > 0)
            {
                return cmp;
                // date1 is greater means date1 is comes after date2
            }
            else if (cmp < 0)
            {
                return cmp;
                // date2 is greater means date1 is comes after date1
            }
            else
            {
                return cmp;
                // date1 is same as date2
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var ICSOID = Convert.ToInt64(hdnComplianceScheduledOnId.Value);
            var RCT = Business.InternalComplianceManagement.GetCurrentStatusByInternalComplianceID((int)ICSOID);            
            if (RCT.ComplianceStatusID == 2 || RCT.ComplianceStatusID == 3 || RCT.ComplianceStatusID == 4 || RCT.ComplianceStatusID == 5)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "This compliance is already submitted for review or already closed.";
            }
            else
            {
                #region Save Code
                List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                try
                {
                    #region insert   
                    Label1.Text = "";
                    Label1.Style.Add("display", "none");
                    Label1.Style.Remove("class");


                    string tbxDate = Request.Form[tbxDate2.UniqueID];
                    Boolean isDocument = false;
                    if (IsDocumentCompulsary == true)
                    {
                        // Document mandatory
                        if (grdInternalDocument.Rows.Count > 0)
                        {
                            for (int i = 0; i < grdInternalDocument.Rows.Count; i++)
                            {
                                Label lblDocType = (Label)grdInternalDocument.Rows[i].FindControl("lblInternalDocType");
                                if (lblDocType.Text == "Compliance Document")
                                {
                                    isDocument = true;
                                }
                            }
                        }
                        else
                        {
                            isDocument = false;
                        }
                    }
                    else
                    {
                        isDocument = true;
                    }
                    int lblStatus = -1;

                    var a = comparedate(Convert.ToDateTime(lblApplicationDate.Text.Trim()), DateTimeExtensions.GetDate(tbxDate2.Text.Trim()));
                    if (a == 1)
                    {
                        lblStatus = 2;
                    }
                    else if (a == -1)
                    {
                        lblStatus = 3;
                    }
                    else if (a == 0)
                    {
                        lblStatus = 2;
                    }
                    if (!string.IsNullOrEmpty(ddlStatus2.SelectedValue))
                    {
                        if (Convert.ToInt32(ddlStatus2.SelectedValue) == 12)
                        {
                            lblStatus = 4;
                        }
                    }
                    if (isDocument == true)
                    {
                        InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                        {
                            InternalComplianceScheduledOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                            InternalComplianceInstanceID = Convert.ToInt64(hdnComplianceInstanceID.Value),
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedByText = AuthenticationHelper.User,
                            StatusId = Convert.ToInt32(lblStatus),
                            StatusChangedOn = DateTime.ParseExact(tbxDate, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                            Remarks = tbxRemarks2.Text,
                            FileNO=txtFileno.Text,
                            PhysicalLocation=txtphysicalLocation.Text
                            
                        };
                        var leavedetails = Business.ComplianceManagement.GetUserLeavePeriodExists(AuthenticationHelper.UserID, "P");
                        if (leavedetails != null)
                        {
                            transaction.OUserID = leavedetails.OldPerformerID;
                        }
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            long ComplianceInstanceID = Convert.ToInt64(hdnComplianceInstanceID.Value);
                            var LicenseID = (from row in entities.Lic_tbl_LicenseInternalComplianceInstanceMapping
                                             where row.ComplianceInstanceID == ComplianceInstanceID
                                             select row.LicenseID).FirstOrDefault();

                            if (ddlStatus2.SelectedValue == "7")
                            {
                                #region Save License Instance 
                                Lic_tbl_InternalLicenseInstance licenseRecord = new Lic_tbl_InternalLicenseInstance()
                                {
                                    UpdatedBy = AuthenticationHelper.UserID
                                };

                                if (!string.IsNullOrEmpty(txtLicenseNo.Text))
                                    licenseRecord.LicenseNo = Convert.ToString(txtLicenseNo.Text);

                                if (!string.IsNullOrEmpty(txtLicenseTitle.Text))
                                    licenseRecord.LicenseTitle = Convert.ToString(txtLicenseTitle.Text);

                                if (!string.IsNullOrEmpty(txtStartDate.Text))
                                    licenseRecord.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);

                                if (!string.IsNullOrEmpty(txtEndDate.Text))
                                    licenseRecord.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);

                                licenseRecord.ID = LicenseID;

                                if (!string.IsNullOrEmpty(txtCost.Text))
                                    licenseRecord.Cost = Convert.ToDecimal(txtCost.Text);

                                if (!string.IsNullOrEmpty(txtFileno.Text))
                                {
                                    licenseRecord.FileNO = Convert.ToString(txtFileno.Text);
                                }


                                if (!string.IsNullOrEmpty(txtphysicalLocation.Text))
                                    licenseRecord.PhysicalLocation = Convert.ToString(txtphysicalLocation.Text);

                                InternalLicenseMgmt.UpdateLicenseNew(licenseRecord);
                                #endregion
                            }

                            Lic_tbl_InternalLicenseInstance licenseRecord1 = new Lic_tbl_InternalLicenseInstance()
                            {
                                UpdatedBy = AuthenticationHelper.UserID
                            };
                            
                           


                            licenseRecord1.ID = LicenseID;
                            //LicenseMgmt.UpdateInternalLicenseCost(licenseRecord1);


                            Lic_tbl_InternalLicenseInstance_Log licenseRecordLog = new Lic_tbl_InternalLicenseInstance_Log()
                            {
                                UpdatedBy = AuthenticationHelper.UserID,
                                UpdatedOn = DateTime.Now,
                            };
                            
                            if (!string.IsNullOrEmpty(txtCost.Text))
                                licenseRecordLog.Cost = Convert.ToDecimal(txtCost.Text);

                            if (!string.IsNullOrEmpty(txtFileno.Text))
                                licenseRecordLog.FileNO = Convert.ToString(txtFileno.Text);

                            if (!string.IsNullOrEmpty(txtphysicalLocation.Text))
                                licenseRecordLog.PhysicalLocation = Convert.ToString(txtphysicalLocation.Text);

                            licenseRecordLog.LicenseID = LicenseID;
                            LicenseMgmt.UpdateInternalLicenseLogNew(licenseRecordLog);

                            Lic_tbl_InternalLicenseStatusTransaction license = new Lic_tbl_InternalLicenseStatusTransaction()
                            {
                                CustomerID = (int)AuthenticationHelper.CustomerID,
                                LicenseID = LicenseID,
                                StatusID = Convert.ToInt32(ddlStatus2.SelectedValue),
                                StatusChangeOn = DateTime.ParseExact(tbxDate2.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                IsActive = true,
                                CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                CreatedOn = DateTime.Now,
                                UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                                UpdatedOn = DateTime.Now,
                                Remark = "Change license status to " + ddlStatus2.SelectedItem.Text + ".",
                                InternalComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                            };
                            entities.Lic_tbl_InternalLicenseStatusTransaction.Add(license);
                            entities.SaveChanges();


                            Lic_tbl_InternalLicenseAudit_Log newLicenseInstance_Log = new Lic_tbl_InternalLicenseAudit_Log()
                            {
                                CustomerID = (int)AuthenticationHelper.CustomerID,
                                LicenseID = LicenseID,
                                StatusID = Convert.ToInt32(ddlStatus2.SelectedValue),
                                IsActive = true,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                Remark = "Change license status to " + ddlStatus2.SelectedItem.Text + "."
                            };
                            if (!string.IsNullOrEmpty(txtStartDate.Text))
                                newLicenseInstance_Log.StartDate = DateTimeExtensions.GetDate(txtStartDate.Text);

                            if (!string.IsNullOrEmpty(txtEndDate.Text))
                                newLicenseInstance_Log.EndDate = DateTimeExtensions.GetDate(txtEndDate.Text);
                            entities.Lic_tbl_InternalLicenseAudit_Log.Add(newLicenseInstance_Log);
                            entities.SaveChanges();

                        }
                        List<InternalFileData> files = new List<InternalFileData>();
                        List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                        var httpRequest = System.Web.HttpContext.Current.Request;
                        HttpFileCollection fileCollection1 = httpRequest.Files;
                        string directoryPath = null;
                        bool blankfileCount = true;
                        var TempDocData = Business.ComplianceManagement.GetTempInternalComplianceDocumentData(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                        if (TempDocData.Count > 0)
                        {
                            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var InstanceData = DocumentManagement.GetInternalComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));
                            var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (true)//fileCollection1.Count > 0
                                {
                                    string version = DocumentManagement.GetDocumnetVersionInternal(Convert.ToInt32(hdnComplianceScheduledOnId.Value));
                                    directoryPath = "InternalAvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                                    
                                    foreach (var item in TempDocData)
                                    {
                                        if (item.ISLink)
                                        {
                                            String fileName = "";
                                            if (item.DocType == "IS")
                                            {
                                                fileName = "InternalComplianceDoc_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                            }
                                            else
                                            {
                                                fileName = "InternalWorkingFiles_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 2));
                                            }

                                            InternalFileData file = new InternalFileData()
                                            {
                                                Name = fileName,
                                                Version = version,
                                                FilePath = item.DocPath,
                                                VersionDate = DateTime.UtcNow,
                                                ISLink = true
                                            };

                                            files.Add(file);
                                        }
                                        else
                                        {
                                            #region fileupload                              
                                            String fileName = "";
                                            if (item.DocType == "IS")
                                            {
                                                fileName = "InternalComplianceDoc_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                            }
                                            else
                                            {
                                                fileName = "InternalWorkingFiles_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 2));
                                            }

                                            Guid fileKey = Guid.NewGuid();
                                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));
                                            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));

                                            if (item.DocData.Length > 0)
                                            {
                                                string filepathvalue = string.Empty;
                                                string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                filepathvalue = vale.Replace(@"\", "/");

                                                InternalFileData file = new InternalFileData()
                                                {
                                                    Name = fileName,
                                                    FilePath = filepathvalue, // directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                                    FileKey = fileKey.ToString(),
                                                    Version = version,
                                                    VersionDate = DateTime.UtcNow,
                                                    FileSize = item.FileSize,
                                                };

                                                files.Add(file);

                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty(item.DocName))
                                                    blankfileCount = false;
                                            }
                                            #endregion
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (true)//fileCollection1.Count > 0
                                {
                                    string version = DocumentManagement.GetDocumnetVersionInternal(Convert.ToInt32(hdnComplianceScheduledOnId.Value));
                                    //Change by SACHIN on 21 JAN 2017 for Azure Drive or Local Drive
                                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                    {
                                        directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "/InternalAvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/" + version;
                                    }
                                    else
                                    {
                                        directoryPath = Server.MapPath("~/InternalAvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/" + version);
                                    }

                                    DocumentManagement.CreateDirectory(directoryPath);

                                    foreach (var item in TempDocData)
                                    {
                                        if (item.ISLink)
                                        {
                                            String fileName = "";
                                            if (item.DocType == "IS")
                                            {
                                                fileName = "InternalComplianceDoc_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                            }
                                            else
                                            {
                                                fileName = "InternalWorkingFiles_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 2));
                                            }

                                            InternalFileData file = new InternalFileData()
                                            {
                                                Name = fileName,
                                                Version = version,
                                                FilePath = item.DocPath,
                                                VersionDate = DateTime.UtcNow,
                                                ISLink = true
                                            };

                                            files.Add(file);
                                        }
                                        else
                                        {
                                            #region fileupload                              
                                            String fileName = "";
                                            if (item.DocType == "IS")
                                            {
                                                fileName = "InternalComplianceDoc_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 1));
                                            }
                                            else
                                            {
                                                fileName = "InternalWorkingFiles_" + item.DocName;
                                                list.Add(new KeyValuePair<string, int>(fileName, 2));
                                            }

                                            Guid fileKey = Guid.NewGuid();
                                            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));
                                            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));

                                            if (item.DocData.Length > 0)
                                            {
                                                string filepathvalue = string.Empty;
                                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                {
                                                    string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                                    filepathvalue = vale;
                                                }
                                                else
                                                {
                                                    filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                                }
                                                InternalFileData file = new InternalFileData()
                                                {
                                                    Name = fileName,
                                                    FilePath = filepathvalue, // directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                                    FileKey = fileKey.ToString(),
                                                    Version = version,
                                                    VersionDate = DateTime.UtcNow,
                                                    FileSize = item.FileSize,
                                                };

                                                files.Add(file);

                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty(item.DocName))
                                                    blankfileCount = false;
                                            }
                                            #endregion
                                        }
                                    }
                                }
                                #endregion
                            }

                        }
                        if (files.Count == 0)
                        {
                            if (Directory.Exists(directoryPath))
                                Directory.Delete(directoryPath);
                        }

                        bool flag = false;
                        bool flag1 = false;

                        if (blankfileCount)
                        {
                            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            flag = Business.InternalComplianceManagement.CreateTransaction(transaction, files, list, Filelist, directoryPath, Convert.ToInt64(hdnComplianceScheduledOnId.Value), customerID);
                            //try
                            //{
                            //    foreach (var item in TempDocData)
                            //    {
                            //        if (item.ISLink == false)
                            //        {
                            //            string path = Server.MapPath(item.DocPath);
                            //            FileInfo file = new FileInfo(path);
                            //            if (file.Exists) //check file exsit or not
                            //            {
                            //                file.Delete();
                            //            }
                            //        }
                            //    }
                            //}
                            //catch (Exception)
                            //{
                            //}
                           
                            flag1 = Business.ComplianceManagement.DeleteTempInternalDocumentFileFromScheduleOnID(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                            try
                            {
                                Business.InternalComplianceManagement.BindInternalDatainRedis(Convert.ToInt64(hdnComplianceInstanceID.Value), Convert.ToInt32(AuthenticationHelper.CustomerID));
                            }
                            catch { }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                        }
                        if (flag != true)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                        }
                        upComplianceDetails.Update();
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ClosePop();", true);
                        #endregion
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please select documents for upload.";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalPer();", true);

                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
                #endregion
            }
        }
        protected void grdTransactionHistory2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DOWNLOAD_FILE"))
                {
                    int fileID = Convert.ToInt32(e.CommandArgument);
                    var file = Business.InternalComplianceManagement.GetFile(fileID);
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                    //Response.BinaryWrite(file.Data); // create the file
                    Response.Flush(); // send it to the client to download
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnDownload2_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.InternalComplianceManagement.GetFile(Convert.ToInt32(hdlSelectedDocumentID.Value));

                Response.Buffer = true;
                //Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.Data); // create the file
                Response.End();
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void lbDownloadSample2_Click(object sender, EventArgs e)
        {
            try
            {
                //var file = Business.InternalComplianceManagement.GetInternalComplianceFormByID(Convert.ToInt32(lbDownloadSample4.CommandArgument));

                //Response.Buffer = true;
                //Response.Clear();
                //Response.ContentType = "application/octet-stream";
                //Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.FileData); // create the file
                //Response.Flush(); // send it to the client to download

                var complianceform = Business.InternalComplianceManagement.GetMultipleInternalComplianceFormByID(Convert.ToInt32(lbDownloadSample4.CommandArgument));
                int complianceID1 = Convert.ToInt32(lbDownloadSample4.CommandArgument);
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    int i = 0;
                    foreach (var file in complianceform)
                    {
                        if (file.FileData != null)
                        {
                            string[] filename = file.Name.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            ComplianceZip.AddEntry(str, file.FileData);
                            i++;
                        }
                    }

                    string fileName = "ComplianceID_" + complianceID1 + "_InternalSampleForms.zip";
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = 0;
                    byte[] data = zipMs.ToArray();

                    Response.Buffer = true;

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename= " + fileName);
                    Response.BinaryWrite(data);
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails2_Load(object sender, EventArgs e)
        {
            try
            {

                //DateTime date = DateTime.MinValue;
                //if (DateTime.TryParseExact(tbxDate2.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformerInternal", string.Format("initializeDatePickerforPerformerInternal(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformerInternal", "initializeDatePickerforPerformerInternal(null);", true);
                //}

                //if (lblDueDate.Text != "")
                //{
                //    if (ddlStatus2.SelectedValue == "3")
                //    {
                //        DateTime Date = DateTime.ParseExact(hiddenDueDateInternal.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //        Date = Date.AddDays(1);
                //        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerOverDueInternal", string.Format("initializeDatePickerOverDueInternal(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                //    }
                //    if (ddlStatus2.SelectedValue == "2")
                //    {
                //        DateTime Date = DateTime.ParseExact(hiddenDueDateInternal.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerInTimeInternal", string.Format("initializeDatePickerInTimeInternal(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                //    }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory2_RowCreated(object sender, GridViewRowEventArgs e)
        {
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        public void OpenTransactionPage(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {

                tbxRemarks2.Text = tbxDate2.Text = string.Empty;
                InternalVD = null;
                //InternalVD.DataBind();
                BindTransactionDetails(ScheduledOnID, complianceInstanceID);
                upComplianceDetails.Update();

                ViewState["InternalScheduledOnID"] = ScheduledOnID;
                ViewState["complianceInstanceID"] = complianceInstanceID;

                //upUsers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public event EventHandler OnSaved;

        protected void btnUpload_Click(object sender, EventArgs e)
        {


            HttpFileCollection fileCollection = Request.Files;
            for (int i = 0; i < fileCollection.Count; i++)
            {
                HttpPostedFile uploadfile = fileCollection[i];
                string fileName = Path.GetFileName(uploadfile.FileName);
                if (uploadfile.ContentLength > 0)
                {
                    //uploadfile.SaveAs(Server.MapPath("~/UploadFiles/") + fileName);
                    //lblMessage.Text += fileName + "Saved Successfully<br>";
                }
            }
        }

        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    DeleteFile(Convert.ToInt32(e.CommandArgument));
                    BindDocument(Convert.ToInt32(ViewState["InternalScheduledOnID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("Download"))
            {
                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            else if (e.CommandName.Equals("Delete"))
            {
                DeleteFile(Convert.ToInt32(e.CommandArgument));
                BindDocument(Convert.ToInt32(ViewState["InternalScheduledOnID"]));
            }
        }

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.InternalComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = string.Empty;
                    //Change by SACHIN on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                        filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);

                        // filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }

                    //string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteFile(int fileId)
        {
            try
            {
                var file = Business.InternalComplianceManagement.GetFile(fileId);
                if (file != null)
                {
                    string path = string.Empty;
                    //Change by SACHIN on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        //path = file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name);

                        string pathvalue = file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name);
                        path = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);

                    }
                    else
                    {
                        path = Server.MapPath(file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name));
                    }
                    DocumentManagement.DeleteFile(path, fileId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lbtLinkDocbutton = (LinkButton)e.Item.FindControl("lbtLinkDocbutton");
                scriptManager.RegisterAsyncPostBackControl(lbtLinkDocbutton);

            }
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lbtLinkbutton = (LinkButton)e.Item.FindControl("lbtLinkbutton");
                scriptManager.RegisterAsyncPostBackControl(lbtLinkbutton);
            }
        }

        protected void gridSubTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (e.CommandName.Equals("Download"))
                    {
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                if (taskDocument.Count > 0)
                                {
                                    foreach (var item in taskDocument)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), eachFile.FileName);
                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = eachFile.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                             
                                            
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                            
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                if (taskDocument.Count > 0)
                                {
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        //comment by rahul on 20 JAN 2017
                                        string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                        //string filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));

                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = eachFile.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            if (eachFile.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }

                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptComplianceVersionViewInternal.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptComplianceVersionViewInternal.DataBind();

                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageTaskInternal.Text = "";
                                                lblMessageTaskInternal.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                subTaskDocViewPath = filePath1;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);
                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                                lblMessageTaskInternal.Text = "";
                                                UpdatePanel2.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessageTaskInternal.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptComplianceVersionViewInternal.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptComplianceVersionViewInternal.DataBind();

                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageTaskInternal.Text = "";
                                                lblMessageTaskInternal.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();

                                                subTaskDocViewPath = FileName;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + subTaskDocViewPath + "');", true);
                                                lblMessageTaskInternal.Text = "";
                                                UpdatePanel2.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessageTaskInternal.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                        #region

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private bool BindSubTasks(long ComplianceScheduleOnID, int roleID, string period, int CustomerBranchid)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_TaskInstanceTransactionInternalView_Result> masterdocumentData = new List<SP_TaskInstanceTransactionInternalView_Result>();

                    masterdocumentData = (from row in entities.SP_TaskInstanceTransactionInternalView(customerID)
                                          where row.ForMonth == period
                                          && row.RoleID == roleID
                                          && row.CustomerBranchID == CustomerBranchid
                                          && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                          select row).ToList();

                    var documentData = (from row in masterdocumentData
                                        where row.ParentID == null
                                        && row.ForMonth == period
                                        && row.RoleID == roleID
                                        && row.CustomerBranchID == CustomerBranchid
                                        && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                        select row).ToList();

                  

                    var TaskdocumentData = (from row in entities.TaskDocumentsViews
                                            where row.CustomerID == customerID && row.TaskType == 2
                                            select row).ToList();
                    if (TaskdocumentData.Count > 0)
                    {
                        TaskdocumentData = TaskdocumentData.Where(entry => entry.TaskStatusID == 4 || entry.TaskStatusID == 5).ToList();
                        MastersTaskDocumentslistinternal = TaskdocumentData;
                    }


                    if (documentData.Count != 0)
                    {
                        MastersTasklistinternal = masterdocumentData;
                        divTask.Visible = true;
                        gridSubTaskInternal.DataSource = documentData;
                        gridSubTaskInternal.DataBind();

                        var closedSubTaskCount = documentData.Where(entry => (entry.TaskStatusID == 4 || entry.TaskStatusID == 5)).Count();

                        if (documentData.Count == closedSubTaskCount)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        divTask.Visible = false;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }
        protected string GetUserName(long taskInstanceID, long taskScheduleOnID, int roleID, byte taskType)
        {
            try
            {
                string result = "";

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                result = TaskManagment.GetTaskAssignedUser(customerID, taskType, taskInstanceID, taskScheduleOnID, roleID);

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cv.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }

        }

        protected void gridSubTaskInternal_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (e.CommandName.Equals("Download"))
                    {
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                if (taskDocument.Count > 0)
                                {
                                    foreach (var item in taskDocument)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }

                                    }
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), eachFile.FileName);
                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = eachFile.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                          
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                if (taskDocument.Count > 0)
                                {
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = eachFile.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            if (eachFile.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptComplianceVersionViewInternal.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptComplianceVersionViewInternal.DataBind();


                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageTaskInternal.Text = "";
                                                lblMessageTaskInternal.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewInternalPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                subTaskDocViewPath = filePath1;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);
                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReviewInternal('" + subTaskDocViewPath + "');", true);
                                                lblMessageTaskInternal.Text = "";
                                                UpdatePanel2.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessageTaskInternal.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewInternalPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptComplianceVersionViewInternal.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptComplianceVersionViewInternal.DataBind();

                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageTaskInternal.Text = "";
                                                lblMessageTaskInternal.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewInternalPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();

                                                subTaskDocViewPath = FileName;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReviewInternal('" + subTaskDocViewPath + "');", true);
                                                lblMessageTaskInternal.Text = "";
                                                UpdatePanel2.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessageTaskInternal.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewInternalPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                        #region

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceVersionViewInternal_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);
                int taskFileidID = Convert.ToInt32(commandArgs[2]);
                if (taskScheduleOnID != 0)
                {
                    if (e.CommandName.Equals("View"))
                    {
                        #region
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();
                        List<GetTaskDocumentView> taskFileData = new List<GetTaskDocumentView>();
                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
                        taskFileData = taskDocumenttoView;
                        taskDocumenttoView = taskDocumenttoView.Where(entry => entry.FileID == taskFileidID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    rptComplianceVersionViewInternal.DataSource = taskFileData.OrderBy(entry => entry.Version);
                                    rptComplianceVersionViewInternal.DataBind();
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageTaskInternal.Text = "";
                                                lblMessageTaskInternal.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                subTaskDocViewPath = filePath1;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);
                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReviewInternal('" + subTaskDocViewPath + "');", true);
                                                lblMessageTaskInternal.Text = "";
                                                //UpdatePanel4.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessageTaskInternal.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewInternal();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    rptComplianceVersionViewInternal.DataSource = taskFileData.OrderBy(entry => entry.Version);
                                    rptComplianceVersionViewInternal.DataBind();

                                    foreach (var file in taskDocumenttoView)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageTaskInternal.Text = "";
                                                lblMessageTaskInternal.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();

                                                subTaskDocViewPath = FileName;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReviewInternal('" + subTaskDocViewPath + "');", true);
                                                lblMessageTaskInternal.Text = "";
                                                //UpdatePanel4.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessageTaskInternal.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewInternal();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                        #endregion                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gridSubTaskInternal_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    Label lblSlashReview = (Label)e.Row.FindControl("lblSlashReview");
                    LinkButton btnSubTaskDocView = (LinkButton)e.Row.FindControl("btnSubTaskDocView");
                    LinkButton btnSubTaskDocDownload = (LinkButton)e.Row.FindControl("btnSubTaskDocDownload");
                    CheckBox chkTask = (CheckBox)e.Row.FindControl("chkTask");
                    Label lblIsTaskClose = (Label)e.Row.FindControl("lblIsTaskClose");
                    Label lblsubtasks = (Label)e.Row.FindControl("lblsubtasks");
                    Label lblTaskID = (Label)e.Row.FindControl("lblTaskID");
                    Label lblCbranchId = (Label)e.Row.FindControl("lblCbranchId");
                    Label lblForMonth = (Label)e.Row.FindControl("lblForMonth");
                    Label lblsubtaskDocuments = (Label)e.Row.FindControl("lblsubtaskDocuments");
                    Image chkDocument = (Image)e.Row.FindControl("chkDocument");

                    int taskId = Convert.ToInt32(lblTaskID.Text);
                    int CbranchId = Convert.ToInt32(lblCbranchId.Text);

                    if (MastersTasklistinternal.Count > 0)
                    {
                        var documentData = (from row in MastersTasklistinternal
                                            where row.ParentID == (long?)taskId &&
                                             row.ForMonth == lblForMonth.Text
                                             && row.CustomerBranchID == (long?)CbranchId
                                             && row.RoleID == 3
                                            select row).ToList();

                        string strDocumentSubtasks = "<div style='float:left;border-bottom: 2px solid #dddddd;' class='topdivlive subhead'> <div style='float:left;' class='topdivlivetext subhead'>Task/Compliance</div> <div style='float:left;' class='topdivliveperformer subhead'>Performer</div> <div style='float:left;' class='topdivliveimage subhead'>Action</div></div><div style='clear:both;height:5px;'></div>";
                        string strSubtasks = "<div style='float:left;border-bottom: 2px solid #dddddd;' class='topdivlive subhead'> <div style='float:left;' class='topdivlivetext subhead'>Task/Compliance</div> <div style='float:left;' class='topdivliveperformer subhead'>Performer</div> <div style='float:left;' class='topdivliveimage subhead'>Action</div></div><div style='clear:both;height:5px;'></div>";
                        int DocCounter = 0;
                        int counter = 0;
                        foreach (var item in documentData)
                        {
                            counter += 1;
                            string disp = "none";
                            if (item.UserID == AuthenticationHelper.UserID && (item.TaskStatusID == 1 || item.TaskStatusID == 8))
                            {
                                disp = "block";
                            }
                            #region Document
                            if (MastersTaskDocumentslistinternal.Count > 0)
                            {
                                var taskdocumentData = (from row in MastersTaskDocumentslistinternal
                                                        where row.ForMonth == item.ForMonth &&
                                                         row.CustomerBranchID == item.CustomerBranchID
                                                         && row.TaskScheduleOnID == item.TaskScheduledOnID
                                                        select row).ToList();
                                if (taskdocumentData.Count > 0)
                                {
                                    DocCounter = 1;
                                    strDocumentSubtasks += "<div style='float:left;' class='topdivlive'> <div style='float:left;' class='topdivlivetext'>" + item.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' style='display:" + disp + "'  onclick='downloadTaskSummary(this)' data-statusId='" + item.TaskStatusID + "' data-Formonth='" + item.ForMonth + "' data-InId='" + item.TaskInstanceID + "' data-scId='" + item.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                    strDocumentSubtasks += "</div>";
                                }
                            }
                            #endregion

                            if (1 == 1)
                            {
                                string topline = "";
                                if (counter > 1)
                                {
                                    topline = "topline";
                                }
                                strSubtasks += "<div style='float:left;' class='topdivlive " + topline + "'> <div style='float:left;' class='topdivlivetext'>" + item.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' style='display:" + disp + "'  onclick='openInternalTaskSummary(this)' data-statusId='" + item.TaskStatusID + "' data-Formonth='" + item.ForMonth + "' data-InId='" + item.TaskInstanceID + "' data-scId='" + item.TaskScheduledOnID + "' class='btnss'  ></div></div><div style='clear:both;height:5px;'>";

                                var documentData1 = (from row in MastersTasklistinternal
                                                     where
                                                      row.ForMonth == lblForMonth.Text
                                                      && row.CustomerBranchID == (long?)CbranchId
                                                      && row.RoleID == 3
                                                       && row.ParentID == item.TaskID
                                                     select row).ToList();
                                foreach (var item1 in documentData1)
                                {
                                    disp = "none";
                                    if (item1.UserID == AuthenticationHelper.UserID && (item1.TaskStatusID == 1 || item1.TaskStatusID == 8))
                                    {
                                        disp = "block";
                                    }
                                    //style='display:"+ disp + "'
                                    #region Document1
                                    if (MastersTaskDocumentslistinternal.Count > 0)
                                    {
                                        var taskdocumentData1 = (from row in MastersTaskDocumentslistinternal
                                                                 where row.ForMonth == item1.ForMonth &&
                                                                  row.CustomerBranchID == item1.CustomerBranchID
                                                                  && row.TaskScheduleOnID == item1.TaskScheduledOnID
                                                                 select row).ToList();
                                        if (taskdocumentData1.Count > 0)
                                        {
                                            DocCounter = 1;
                                            strDocumentSubtasks += "<div style='float:left;' class='topdivlive'> <div style='float:left;' class='topdivlivetext'>" + item1.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item1.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' style='display:" + disp + "'  onclick='downloadTaskSummary(this)' data-statusId='" + item1.TaskStatusID + "' data-Formonth='" + item1.ForMonth + "' data-InId='" + item1.TaskInstanceID + "' data-scId='" + item1.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                            strDocumentSubtasks += "</div>";
                                        }
                                    }
                                    #endregion

                                    strSubtasks += "<div style='clear:both;height:5px;'></div><div style='float:left;' class='topdivlive'><div style='float:left;margin-left: 5px; ' class='topdivlivetext'>-" + item1.TaskTitle + "</div><div style='float:left;margin-left: -5px;' class='topdivliveperformer'>" + item1.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button' style='display:" + disp + "' onclick='openInternalTaskSummary(this)' data-statusId='" + item1.TaskStatusID + "' data-Formonth='" + item1.ForMonth + "' data-InId='" + item1.TaskInstanceID + "' data-scId='" + item1.TaskScheduledOnID + "' class='btnss'  ></div></div><div style='clear:both;height:5px;'>";
                                    var documentData2 = (from row in MastersTasklistinternal
                                                         where
                                                           row.ForMonth == lblForMonth.Text
                                                           && row.CustomerBranchID == (long?)CbranchId
                                                           && row.RoleID == 3
                                                           && row.ParentID == item1.TaskID
                                                         select row).ToList();
                                    foreach (var item2 in documentData2)
                                    {
                                        disp = "none";
                                        if (item2.UserID == AuthenticationHelper.UserID && (item2.TaskStatusID == 1 || item2.TaskStatusID == 8))
                                        {
                                            disp = "block";
                                        }
                                        //style='display:"+ disp + "'
                                        #region Document2
                                        if (MastersTaskDocumentslistinternal.Count > 0)
                                        {
                                            var taskdocumentData2 = (from row in MastersTaskDocumentslistinternal
                                                                     where row.ForMonth == item2.ForMonth &&
                                                                      row.CustomerBranchID == item2.CustomerBranchID
                                                                      && row.TaskScheduleOnID == item2.TaskScheduledOnID
                                                                     select row).ToList();
                                            if (taskdocumentData2.Count > 0)
                                            {
                                                strDocumentSubtasks += "<div style='float:left;' class='topdivlive'><div style='float:left;margin-left: 5px; ' class='topdivlivetext'>-" + item2.TaskTitle + "</div><div style='float:left;margin-left: -10px;' class='topdivliveperformer'>" + item2.User + "</div> <div style='float:left;' class='topdivliveimage'><input style='display:" + disp + "' type='button' onclick='openInternalTaskSummary(this)' data-statusId='" + item2.TaskStatusID + "' data-Formonth='" + item2.ForMonth + "' data-InId='" + item2.TaskInstanceID + "' data-scId='" + item2.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                                strDocumentSubtasks += "</div>";
                                            }
                                        }
                                        #endregion


                                        strSubtasks += "<div style='clear:both;height:5px;'></div><div style='float:left;' class='topdivlive'><div style='float:left;margin-left: 5px; ' class='topdivlivetext'>-" + item2.TaskTitle + "</div><div style='float:left;margin-left: -10px;' class='topdivliveperformer'>" + item2.User + "</div> <div style='float:left;' class='topdivliveimage'><input style='display:" + disp + "' type='button' onclick='openInternalTaskSummary(this)' data-statusId='" + item2.TaskStatusID + "' data-Formonth='" + item2.ForMonth + "' data-InId='" + item2.TaskInstanceID + "' data-scId='" + item2.TaskScheduledOnID + "' class='btnss'  ></div></div><div style='clear:both;height:5px;'>";
                                        var documentData3 = (from row in MastersTasklistinternal
                                                             where
                                                               row.ForMonth == lblForMonth.Text
                                                               && row.CustomerBranchID == (long?)CbranchId
                                                               && row.RoleID == 3
                                                               && row.ParentID == item2.TaskID
                                                             select row).ToList();
                                        foreach (var item3 in documentData3)
                                        {
                                            disp = "none";
                                            if (item3.UserID == AuthenticationHelper.UserID && (item3.TaskStatusID == 1 || item3.TaskStatusID == 8))
                                            {
                                                disp = "block";
                                            }
                                            //style='display:"+ disp + "'
                                            #region Document3
                                            if (MastersTaskDocumentslistinternal.Count > 0)
                                            {
                                                var taskdocumentData3 = (from row in MastersTaskDocumentslistinternal
                                                                         where row.ForMonth == item3.ForMonth &&
                                                                          row.CustomerBranchID == item3.CustomerBranchID
                                                                          && row.TaskScheduleOnID == item3.TaskScheduledOnID
                                                                         select row).ToList();
                                                if (taskdocumentData3.Count > 0)
                                                {
                                                    DocCounter = 1;
                                                    strDocumentSubtasks += "<div style='float:left;' class='topdivlive'> <div style='float:left;' class='topdivlivetext'>" + item3.TaskTitle + "</div><div style='float:left;' class='topdivliveperformer'>" + item3.User + "</div> <div style='float:left;' class='topdivliveimage'><input type='button'  onclick ='downloadTaskSummary(this)' data-statusId='" + item3.TaskStatusID + "' data-Formonth='" + item3.ForMonth + "' data-InId='" + item3.TaskInstanceID + "' data-scId='" + item3.TaskScheduledOnID + "' class='btnss doctaks'  ></div></div><div style='clear:both;height:5px;'>";
                                                    strDocumentSubtasks += "</div>";
                                                }


                                            }
                                            #endregion

                                            strSubtasks += "<div style='clear:both;height:5px;'></div><div style='float:left;' class='topdivlive'><div style='float:left;margin-left: 5px; ' class='topdivlivetext'>-" + item3.TaskTitle + "</div><div style='float:left;margin-left: -15px;' class='topdivliveperformer'>" + item3.User + "</div> <div style='float:left;' style='display:" + disp + "' class='topdivliveimage'><input type='button'  onclick='openInternalTaskSummary(this)' data-statusId='" + item3.TaskStatusID + "' data-Formonth='" + item3.ForMonth + "' data-InId='" + item3.TaskInstanceID + "' data-scId='" + item3.TaskScheduledOnID + "' class='btnss'  ></div></div><div style='clear:both;height:5px;'>";
                                            strSubtasks += "</div>";
                                        }
                                        strSubtasks += "</div>";


                                    }
                                    strSubtasks += "</div>";

                                }
                                strSubtasks += "</div>";

                            }
                        }
                        lblsubtasks.Text = strSubtasks;
                        if (DocCounter == 0)
                        {
                            chkDocument.Visible = false;
                        }
                        lblsubtaskDocuments.Text = strDocumentSubtasks;
                    }
                    if (lblStatus != null && btnSubTaskDocDownload != null && lblSlashReview != null && btnSubTaskDocView != null)
                    {
                        if (lblStatus.Text != "")
                        {
                            if (lblStatus.Text == "Open")
                            {
                                btnSubTaskDocDownload.Visible = false;
                                lblSlashReview.Visible = false;
                                btnSubTaskDocView.Visible = false;
                            }
                            else
                            {
                                if (lblIsTaskClose.Text == "True")
                                {
                                    chkTask.Enabled = false;
                                    chkTask.Checked = true;
                                    btnSubTaskDocDownload.Visible = false;
                                    lblSlashReview.Visible = false;
                                    btnSubTaskDocView.Visible = false;
                                }
                                else
                                {
                                    btnSubTaskDocDownload.Visible = true;
                                    lblSlashReview.Visible = true;
                                    btnSubTaskDocView.Visible = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void LoadSubEntitiesTask(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            var query = (from row in entities.Tasks
                         where row.ParentID != null
                         && row.ParentID == nvp.ID
                         select row);

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Title }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                Tasklist.Add(item.ID);
                LoadSubEntitiesTask(item, false, entities);
            }
        }

        public static List<NameValueHierarchy> GetAllHierarchyTask(int TaskID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Tasks
                             where row.ID == TaskID
                             select row);
                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Title }).OrderBy(entry => entry.Name).ToList();
                foreach (var item in hierarchy)
                {
                    Tasklist.Add(item.ID);
                    LoadSubEntitiesTask(item, true, entities);
                }
            }
            return hierarchy;
        }

        protected void lblNo_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow gvrow in gridSubTaskInternal.Rows)
                {
                    CheckBox chk = (CheckBox)gvrow.FindControl("chkTask");
                    if (chk != null & chk.Checked)
                    {
                        int TaskID = Convert.ToInt32(gridSubTaskInternal.DataKeys[gvrow.RowIndex].Value.ToString());
                        string str = gridSubTaskInternal.DataKeys[gvrow.RowIndex].Value.ToString();
                        Label lblTaskScheduledOnID = (Label)gvrow.FindControl("lblTaskScheduledOnID");
                        int TaskScheduledOnID = Convert.ToInt32(lblTaskScheduledOnID.Text);
                        Label lblComplianceScheduleOnID = (Label)gvrow.FindControl("lblComplianceScheduleOnID");
                        int ComplianceScheduleOnID = Convert.ToInt32(lblComplianceScheduleOnID.Text);
                        Label lbllblTaskInstanceID = (Label)gvrow.FindControl("lblTaskInstanceID");
                        int TaskInstanceID = Convert.ToInt32(lbllblTaskInstanceID.Text);
                        Label lblMainTaskID = (Label)gvrow.FindControl("lblMainTaskID");
                        int MainTaskID = Convert.ToInt32(lblMainTaskID.Text);
                        Label lblForMonth = (Label)gvrow.FindControl("lblForMonth");
                        string ForMonth = Convert.ToString(lblForMonth.Text);


                        if (TaskID != -1)
                        {
                            long? parentID = TaskID;
                            List<NameValueHierarchy> hierarchy = new List<NameValueHierarchy>();
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                Tasklist.Clear();
                                GetAllHierarchyTask(TaskID);
                                int customerID = -1;
                                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                                var documentData = (from row in entities.TaskInstanceTransactionViews
                                                    where Tasklist.Contains(row.TaskID)
                                                    && row.ForMonth == ForMonth
                                                    && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                                    && row.RoleID == 4
                                                    && row.CustomerID == customerID
                                                    select row).ToList();

                                for (int i = 0; i < documentData.Count; i++)
                                {
                                    var IsTaskTransactionPresent = TaskManagment.CheckTaskTransacion(Convert.ToInt64(documentData[i].TaskInstanceID), Convert.ToInt64(documentData[i].TaskScheduledOnID), Convert.ToInt64(documentData[i].ComplianceScheduleOnID));
                                    if (IsTaskTransactionPresent == false)
                                    {
                                        int StatusID = 4;
                                        TaskTransaction transaction = new TaskTransaction()
                                        {
                                            ComplianceScheduleOnID = Convert.ToInt64(documentData[i].ComplianceScheduleOnID),
                                            TaskScheduleOnID = Convert.ToInt64(documentData[i].TaskScheduledOnID),
                                            TaskInstanceId = Convert.ToInt64(documentData[i].TaskInstanceID),
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedByText = AuthenticationHelper.User,
                                            StatusId = StatusID,
                                            StatusChangedOn = DateTime.ParseExact(DateTime.Now.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                            Remarks = "Not applicable",
                                            IsTaskClose = true,
                                        };
                                        bool sucess = TaskManagment.CreateTaskTransaction(transaction);
                                    }
                                }
                            }
                        }
                    }
                }
                var IsAfter = "";
                var Period = "";
                int CustomerBrachID = -1;
                int ScheduledOnID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["IsAfter"])))
                {
                    IsAfter = Convert.ToString(ViewState["IsAfter"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Period"])))
                {
                    Period = Convert.ToString(ViewState["Period"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomerBrachID"])))
                {
                    CustomerBrachID = Convert.ToInt32(ViewState["CustomerBrachID"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["InternalScheduledOnID"])))
                {
                    ScheduledOnID = Convert.ToInt32(ViewState["InternalScheduledOnID"]);
                }

                var showHideButton = false;
                if (IsAfter == "false")
                {
                    showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, CustomerBrachID);
                    btnSave2.Enabled = showHideButton;
                }
                else
                {
                    showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, CustomerBrachID);
                    btnSave2.Enabled = true;
                }

                upComplianceDetails.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceSampleView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    InternalComplianceForm complianceForm = Business.InternalComplianceManagement.GetSelectedInternalComplianceFileName(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[1]));

                    string Filename = complianceForm.Name;
                    string filePath = Filename;
                    if (filePath != null)
                    {
                        try
                        {
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + File;
                            string extension = System.IO.Path.GetExtension(filePath);
                            
                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                            if (!Directory.Exists(DateFolder))
                            {
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                            }                            
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            string User = AuthenticationHelper.UserID + "" + FileDate;
                            string FileName = DateFolder + "/" + User + "" + extension;
                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            bw.Write(complianceForm.FileData);
                            bw.Close();
                            lblpathsample4.Text = FileName;

                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFile();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfile4('" + lblpathsample4.Text + "');", true);
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                        
                    }

                   
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceSampleView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblSampleView = (LinkButton)e.Item.FindControl("lblSampleView");
                scriptManager.RegisterAsyncPostBackControl(lblSampleView);
            }
        }

        protected void ddlStatus2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var complianceInfo = Business.InternalComplianceManagement.GetInternalCompliance(Convert.ToInt64(lblComplianceID2.Text));
                if (complianceInfo.IsDocumentRequired != true)
                {
                    HDNIsDocumentCompulsary.Value = "F";
                    IsDocumentCompulsary = false;
                }
                else
                {
                    HDNIsDocumentCompulsary.Value = "T";
                    IsDocumentCompulsary = true;
                }                             
                if (ddlStatus2.SelectedValue == "7")
                {
                    fieldsetRenewal.Visible = true;
                }
                else
                {
                    fieldsetRenewal.Visible = false;
                }
                if (ddlStatus2.SelectedValue == "12")
                {
                    IsDocumentCompulsary =  false;
                    lblDate.Visible = false;
                    tbxDate2.Text= Convert.ToDateTime(DateTime.Now).ToString("dd-MM-yyyy");
                }
                else
                {
                    IsDocumentCompulsary =  true;
                    lblDate.Visible = true;
                    tbxDate2.Text = "";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindTempDocumentData(long ScheduledOnID)
        {
            try
            {
                grdInternalDocument.DataSource = null;
                grdInternalDocument.DataBind();
                var DocData = DocumentManagement.GetTempInternalComplianceDocumentData(ScheduledOnID);
                if (DocData.Count > 0)
                {
                    grdInternalDocument.Visible = true;
                    grdInternalDocument.DataSource = DocData;
                    grdInternalDocument.DataBind();
                }
                else
                {
                    grdInternalDocument.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdInternalDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Delete Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        if (Business.ComplianceManagement.DeleteTempDocumentFile(FileID))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);

                            BindTempDocumentData(Convert.ToInt64(ViewState["InternalScheduledOnID"]));
                        }
                    }
                }
                else if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
                            if (file != null)
                            {
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename= " + file.DocName);
                                Response.TransmitFile(Server.MapPath(file.DocPath));
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                        }
                        BindTempDocumentData(Convert.ToInt64(ViewState["InternalScheduledOnID"]));
                    }
                }
                else if (e.CommandName.Equals("View Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
                        if (file != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.DocPath));
                            if (file.DocName != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Zip file can't view please download it.')", true);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Zip file can't view please download it.";
                                }
                                else
                                {
                                    string CompDocReviewPath = file.DocPath;

                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                    ;
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenInternalDocumentPriview('" + CompDocReviewPath + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('There is no file to preview.')", true);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "There is no file to preview.";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again."; ;
            }
        }
        protected void grdInternalDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblDocumentName = (Label)e.Row.FindControl("lblDocumentName");
                    Label lblIsLinkTrue = (Label)e.Row.FindControl("lblIsLinkTrue");
                    Label lblInternalDocType = (Label)e.Row.FindControl("lblInternalDocType");

                    if (lblInternalDocType.Text.Trim() == "IS")
                    {
                        lblInternalDocType.Text = "Compliance Document";
                    }
                    else
                    {
                        lblInternalDocType.Text = "Working Files";
                    }


                    LinkButton lnkRedirectDocument = (LinkButton)e.Row.FindControl("lnkRedirectDocument");
                    LinkButton lnkDownloadDocument = (LinkButton)e.Row.FindControl("lnkDownloadInternalDocument");
                    LinkButton lnkViewDocument = (LinkButton)e.Row.FindControl("lnkViewInternalDocument");

                    if (lblIsLinkTrue.Text == "True")
                    {
                        lnkDownloadDocument.Visible = false;
                        lnkViewDocument.Visible = false;
                        lnkRedirectDocument.Visible = true;
                        lblDocumentName.Visible = false;
                        //lnkDeleteDocument.Visible = false;
                    }
                    else
                    {
                        lnkDownloadDocument.Visible = true;
                        lnkViewDocument.Visible = true;
                        lnkRedirectDocument.Visible = false;
                        lblDocumentName.Visible = true;
                        //lnkDeleteDocument.Visible = true;
                    }
                }
                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    Label lblInternalDocType = (Label)e.Row.FindControl("lblInternalDocType");

                //    if (lblInternalDocType.Text.Trim() == "IS")
                //    {
                //        lblInternalDocType.Text = "Compliance Document";
                //    }
                //    else
                //    {
                //        lblInternalDocType.Text = "Working Files";
                //    }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void UploadInternalDocument_Click(object sender, EventArgs e)
        {
            try
            {
                grdInternalDocument.DataSource = null;
                grdInternalDocument.DataBind();
                long ScheduledOnID = Convert.ToInt64(ViewState["InternalScheduledOnID"]);
                long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

                TempComplianceDocument tempComplianceDocument = null;
                HttpFileCollection fileCollection = Request.Files;
                if (fileCollection.Count > 0)
                {
                    #region file upload
                    bool isBlankFile = false;
                    string[] validFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                    for (int i = 0; i < fileCollection.Count; i++)
                    {
                        HttpPostedFile uploadfile = null;
                        uploadfile = fileCollection[i];
                        int filelength = uploadfile.ContentLength;
                        string fileName = Path.GetFileName(uploadfile.FileName);
                        string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                        if (!string.IsNullOrEmpty(fileName))
                        {
                            if (filelength == 0)
                            {
                                isBlankFile = true;
                                break;
                            }
                            else if (ext == "")
                            {
                                isBlankFile = true;
                                break;
                            }
                            else
                            {
                                if (ext != "")
                                {
                                    for (int j = 0; j < validFileTypes.Length; j++)
                                    {
                                        if (ext == "." + validFileTypes[j])
                                        {
                                            isBlankFile = true;
                                            break;
                                        }
                                    }
                                }

                            }
                        }
                    }
                    if (isBlankFile == false)
                    {

                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            string directoryPath = null;

                            if (!string.IsNullOrEmpty(fileName))
                            {
                                string[] keys = fileCollection.Keys[i].Split('$');
                                if (keys[keys.Count() - 1].Equals("fuSampleFile2"))
                                {
                                    directoryPath = Server.MapPath("~/TempDocuments/Internal/");
                                }
                                else
                                {
                                    directoryPath = Server.MapPath("~/TempDocuments/InternalWorking/");
                                }

                                DocumentManagement.CreateDirectory(directoryPath);
                                string finalPath = Path.Combine(directoryPath, fileName);
                                finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                                fileCollection[i].SaveAs(Server.MapPath(finalPath));

                                Stream fs = uploadfile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                if (uploadfile.ContentLength > 0)
                                {
                                    tempComplianceDocument = new TempComplianceDocument()
                                    {
                                        ScheduleOnID = ScheduledOnID,
                                        ComplianceInstanceID = complianceInstanceID,
                                        DocPath = finalPath,
                                        DocData = bytes,
                                        DocName = fileCollection[i].FileName,
                                        FileSize = uploadfile.ContentLength,
                                    };
                                    if (keys[keys.Count() - 1].Equals("fuSampleFile2"))
                                    {
                                        tempComplianceDocument.DocType = "IS";
                                    }
                                    else
                                    {
                                        tempComplianceDocument.DocType = "IW";
                                    }
                                    long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);
                                    if (_objTempDocumentID > 0)
                                    {
                                        BindTempDocumentData(ScheduledOnID);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                    }
                    #endregion
                }
                BindTempDocumentData(ScheduledOnID);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalPer();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #region License Documents
        protected void rptLicenseVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //Label lblSlashReview = (Label)e.Item.FindControl("lblSlashReviewnew");
                Label lblpathIlink = (Label)e.Item.FindControl("lblpathIlink");
                LinkButton lblViewfile = (LinkButton)e.Item.FindControl("lnkViewDoc");
                LinkButton lblfilePath = (LinkButton)e.Item.FindControl("lblpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblLicenseDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
                if (lblpathIlink.Text == "True")
                {
                    lblViewfile.Visible = false;
                    lblfilePath.Visible = true;
                    lblDownLoadfile.Visible = false;
                    //lblSlashReview.Visible = false;
                }
                else
                {
                    lblViewfile.Visible = true;
                    lblfilePath.Visible = false;
                    lblDownLoadfile.Visible = true;
                    //lblSlashReview.Visible = true;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseVersionDoc");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);

            //    LinkButton lblLicenseDocumentVersion = (LinkButton)e.Item.FindControl("lblLicenseDocumentVersion");
            //    scriptManager.RegisterAsyncPostBackControl(lblLicenseDocumentVersion);
            //}
        }
        protected void rptLicenseVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                }
                else
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                List<SP_GetInternalLicenseDocument_Result> ComplianceFileData = new List<SP_GetInternalLicenseDocument_Result>();
                List<SP_GetInternalLicenseDocument_Result> ComplianceDocument = new List<SP_GetInternalLicenseDocument_Result>();
                ComplianceDocument = InternalLicenseMgmt.GetInternalFileData(Convert.ToInt32(commandArgs[0]),-1).ToList();

                if (commandArgs[2].Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[2]).ToList();
                }

                    if (e.CommandName.Equals("version"))
                    {
                        if (e.CommandName.Equals("version"))
                        {
                            rptLicenseDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                            rptLicenseDocumnets.DataBind();

                            rptLicenseWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                            rptLicenseWorkingFiles.DataBind();

                            var documentVersionData = ComplianceDocument.Select(x => new
                            {
                                ID = x.ID,
                                ScheduledOnID = x.ScheduledOnID,
                                LicenseID = x.LicenseID,
                                Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                                VersionDate = x.VersionDate,
                                VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment,
                                FileID = x.FileID,
                                ISLink = x.ISLink,
                                FilePath = x.FilePath,
                                FileName = x.FileName
                            }).GroupBy(entry => new { entry.Version, entry.ISLink }).Select(entry => entry.FirstOrDefault()).ToList();

                            rptLicenseVersion.DataSource = documentVersionData;
                            rptLicenseVersion.DataBind();
                        }
                    }
                    else if (e.CommandName.Equals("Download"))
                    {
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWs Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                var LicenseData = InternalLicenseMgmt.GetLicense(Convert.ToInt32(commandArgs[0]));

                                ComplianceZip.AddDirectoryByName(LicenseData.LicenseNo);
                                ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                                if (ComplianceFileData.Count > 0)
                                {
                                    foreach (var item in ComplianceFileData)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = file.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                             
                                                ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                             
                                            i++;
                                        }
                                    }
                                }
                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=LicenseDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                var LicenseData = InternalLicenseMgmt.GetLicense(Convert.ToInt32(commandArgs[0]));

                                ComplianceZip.AddDirectoryByName(LicenseData.LicenseNo);
                                ComplianceFileData = ComplianceFileData.Where(x => x.ISLink == false).ToList();
                                if (ComplianceFileData.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in ComplianceFileData)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = file.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(LicenseData.LicenseNo + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }
                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=LicenseDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {

                        string[] commandArg = e.CommandArgument.ToString().Split(',');
                        List<SP_GetInternalLicenseDocument_Result> CMPDocuments = InternalLicenseMgmt.GetInternalFileData(Convert.ToInt32(commandArgs[0]), -1).ToList();
                        Session["LicenseID"] = commandArg[0];
                        if (CMPDocuments != null)
                        {
                            List<SP_GetInternalLicenseDocument_Result> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                SP_GetInternalLicenseDocument_Result entityData = new SP_GetInternalLicenseDocument_Result();
                                entityData.Version = "1.0";
                                entityData.LicenseID = Convert.ToInt64(commandArg[0]);
                                entitiesData.Add(entityData);
                            }
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    CMPDocuments = CMPDocuments.Where(x => x.ISLink == false).ToList();
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }

                                    foreach (var file in CMPDocuments)
                                    {
                                        rptLicenseVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptLicenseVersionView.DataBind();

                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                           
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageReviewer1.Text = "";
                                                lblMessageReviewer1.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileInternalLicenseAllShowPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                LicenseDocPath = filePath1;
                                                LicenseDocPath = LicenseDocPath.Substring(2, LicenseDocPath.Length - 2);

                                                lblMessageReviewer1.Text = "";

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileInternallicense('" + LicenseDocPath + "');", true);
                                            }
                                        }
                                        else
                                        {
                                            lblMessageReviewer1.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileInternalLicenseAllShowPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    CMPDocuments = CMPDocuments.Where(x => x.ISLink == false).ToList();
                                    foreach (var file in CMPDocuments)
                                    {
                                        rptLicenseVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptLicenseVersionView.DataBind();
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessageReviewer1.Text = "";
                                                lblMessageReviewer1.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileInternalLicenseAllShowPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();
                                                LicenseDocPath = FileName;

                                                LicenseDocPath = LicenseDocPath.Substring(2, LicenseDocPath.Length - 2);

                                                lblMessageReviewer1.Text = "";

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileInternallicense('" + LicenseDocPath + "');", true);
                                            }
                                        }
                                        else
                                        {
                                            lblMessageReviewer1.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileInternalLicenseAllShowPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptLicenseDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblpathCompIlink = (Label)e.Item.FindControl("lblCompDocpathIlink");
                LinkButton lblRedirectfile = (LinkButton)e.Item.FindControl("lblCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
                if (lblpathCompIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblRedirectfile.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblRedirectfile.Visible = false;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseDocumnets");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }
        protected void rptLicenseDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                else
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptLicenseWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblWorkCompDocpathIlink = (Label)e.Item.FindControl("lblWorkCompDocpathIlink");
                LinkButton lblWorkCompDocpathDownload = (LinkButton)e.Item.FindControl("lblWorkCompDocpathDownload");
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                if (lblWorkCompDocpathIlink.Text == "True")
                {
                    lblDownLoadfile.Visible = false;
                    lblWorkCompDocpathDownload.Visible = true;
                }
                else
                {
                    lblDownLoadfile.Visible = true;
                    lblWorkCompDocpathDownload.Visible = false;
                }
            }
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnLicenseWorkingFiles");
            //    var scriptManager = ScriptManager.GetCurrent(this.Page);
            //    scriptManager.RegisterPostBackControl(lblDownLoadfile);
            //}
        }
        protected void rptLicenseWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("RedirectURL"))
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "enableControls1()", true);
                else
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void rptLicenseVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblLicenseVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }
        protected void rptLicenseVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {

                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<SP_GetInternalLicenseDocument_Result> CMPDocuments = InternalLicenseMgmt.GetInternalFileData(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[2])).ToList();
                    Session["LicenseID"] = commandArg[0];

                    if (CMPDocuments != null)
                    {
                        List<SP_GetInternalLicenseDocument_Result> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            SP_GetInternalLicenseDocument_Result entityData = new SP_GetInternalLicenseDocument_Result();
                            entityData.Version = "1.0";
                            entityData.LicenseID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            if (entitiesData.Count > 0)
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                foreach (var file in CMPDocuments)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }

                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);
                                    if (file.FilePath != null && File.Exists(filePath))
                                    { 
                                        string extension = System.IO.Path.GetExtension(filePath);
                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessageReviewer1.Text = "";
                                            lblMessageReviewer1.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileInternalLicenseAllShowPopUp();", true);
                                        }
                                        else
                                        {
                                            string filePath1 = directoryPath + "/" + file.FileName;
                                            LicenseDocPath = filePath1;
                                            LicenseDocPath = LicenseDocPath.Substring(2, LicenseDocPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileInternallicense('" + LicenseDocPath + "');", true);
                                            lblMessageReviewer1.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessageReviewer1.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileInternalLicenseAllShowPopUp();", true);
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;
                                        string extension = System.IO.Path.GetExtension(filePath);

                                        if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                        {
                                            lblMessageReviewer1.Text = "";
                                            lblMessageReviewer1.Text = "Zip file can't view please download it";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileInternalLicenseAllShowPopUp();", true);
                                        }
                                        else
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(AuthenticationHelper.CustomerID);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            LicenseDocPath = FileName;
                                            LicenseDocPath = LicenseDocPath.Substring(2, LicenseDocPath.Length - 2);
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileInternallicense('" + LicenseDocPath + "');", true);
                                            lblMessageReviewer1.Text = "";

                                        }
                                    }
                                    else
                                    {
                                        lblMessageReviewer1.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileInternalLicenseAllShowPopUp();", true);
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        #endregion


        protected void UploadlinkCompliancefile_Click(object sender, EventArgs e)
        {
            grdInternalDocument.DataSource = null;
            grdInternalDocument.DataBind();
            long ScheduledOnID = Convert.ToInt64(ViewState["InternalScheduledOnID"]);
            long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

            string url = TxtCompliancedocumentlnk.Text;
            string fileName = DocumentManagement.getFileName(url);
            if (!string.IsNullOrEmpty(TxtCompliancedocumentlnk.Text))
            {
                TempComplianceDocument tempComplianceDocument = null;
                var bytes = new byte[] { };// { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };  
                tempComplianceDocument = new TempComplianceDocument()
                {
                    ScheduleOnID = ScheduledOnID,
                    ComplianceInstanceID = complianceInstanceID,
                    DocPath = url,//userpath,
                    DocData = bytes,
                    DocName = fileName,
                    DocType = "IS",
                    ISLink = true
                };

                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                if (_objTempDocumentID > 0)
                {
                    BindTempDocumentData(ScheduledOnID);
                }
            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalPer();", true);
            }
            TxtCompliancedocumentlnk.Text = "";
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalPer();", true);
        }
        protected void UploadlinkWorkingfile_Click(object sender, EventArgs e)
        {
            grdInternalDocument.DataSource = null;
            grdInternalDocument.DataBind();
            long ScheduledOnID = Convert.ToInt64(ViewState["InternalScheduledOnID"]);
            long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

            string url = Txtworkingdocumentlnk.Text;
            string fileName = DocumentManagement.getFileName(url);

            if (!string.IsNullOrEmpty(Txtworkingdocumentlnk.Text))
            {
                TempComplianceDocument tempComplianceDocument = null;
                var bytes = new byte[] { };// { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };  
                tempComplianceDocument = new TempComplianceDocument()
                {
                    ScheduleOnID = ScheduledOnID,
                    ComplianceInstanceID = complianceInstanceID,
                    DocPath = url,//userpath,
                    DocData = bytes,
                    DocName = fileName,
                    DocType = "IW",
                    ISLink = true
                };

                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                if (_objTempDocumentID > 0)
                {
                    BindTempDocumentData(ScheduledOnID);
                }
            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalPer();", true);
            }
            Txtworkingdocumentlnk.Text = "";
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModalInternalPer();", true);
        }
    }
}