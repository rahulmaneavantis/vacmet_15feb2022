﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ActDeactivation : System.Web.UI.Page
    {
        private int userId = AuthenticationHelper.UserID;
        public static string ActDocReviewPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                vsActdeactivation.CssClass = "alert alert-danger";
                vsActValidationGroup.CssClass = "alert alert-danger";
                BindAct();
                BindTypes();
                BindCatagory();
                BindFilterCatagory();
                BindFilterStates();
                BindStates();
                BindCities();
                BindActDepartment();
                BindMinistry();
                BindRegulator();
                BindIndustry();
                BindComplianceFilter();
            }
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private void BindComplianceFilter()
        {
            try
            {
                int catagoryid = -1;
                int Typeid = -1;
                long ActID = 0;
                // long stateid = 0;


                if (!string.IsNullOrEmpty(ddlFilterCategory.SelectedValue) && Convert.ToString(ddlFilterCategory.SelectedValue) != "-1")
                {
                    catagoryid = Convert.ToInt32(ddlFilterCategory.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlFilterType.SelectedValue) && Convert.ToString(ddlFilterType.SelectedValue) != "-1")
                {
                    Typeid = Convert.ToInt32(ddlFilterType.SelectedValue);
                }
                    //if (!string.IsNullOrEmpty(ddlState.SelectedValue) && Convert.ToString(ddlState.SelectedValue) != "-1")
                    //{
                    //    stateid = Convert.ToInt32(ddlState.SelectedValue);
                    //}
                if (!string.IsNullOrEmpty(ddlDeactiAct.SelectedValue) && Convert.ToString(ddlDeactiAct.SelectedValue) != "-1")
                 {
                    ActID = Convert.ToInt32(ddlDeactiAct.SelectedValue);
                 }


                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        grdAct.DataSource = null;
                        grdAct.DataBind();

                        var ClientActDetails = (from row in entities.SP_ComplianceActDeactivation(ActID)
                                                select row).ToList();


                        if (ClientActDetails.Count > 0)
                        {
                            //if (stateid != -1)
                            //{
                            //    ClientActDetails = ClientActDetails.Where(entry => entry.StateID == stateid).ToList();
                            //}

                            if (catagoryid != -1)
                            {
                                ClientActDetails = ClientActDetails.Where(entry => entry.ComplianceCategoryId == catagoryid).ToList();
                            }
                            if (Typeid != -1)
                            {
                                ClientActDetails = ClientActDetails.Where(entry => entry.ComplianceTypeId == Typeid).ToList();
                            }

                            if (!string.IsNullOrEmpty(tbxFilter.Text))
                            {
                                if (CheckInt(tbxFilter.Text))
                                {
                                    int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                                    ClientActDetails = ClientActDetails.Where(entry => entry.ComplianceID == a).ToList();
                                }
                                else
                                {
                                    ClientActDetails = ClientActDetails.Where(entry => entry.CustomerName.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.CustomerBranchName.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                                }
                            }

                            grdAct.DataSource = ClientActDetails;
                            grdAct.DataBind();
                        }
                    }
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSaveDeactivate_Click(object sender, EventArgs e)
        {
            try
            {
                long oldActID = Convert.ToInt32(ddlDeactiAct.SelectedValue);

                if (DeactivationAct.CheckActDeactivationRequest(Convert.ToInt64(oldActID)))
                {
                    if (!DeactivationAct.ExistsMappingRLCSorHRProduct("A",oldActID)) //Check Any Compliance Under this Act tagged to HRProduct/RLCS
                    {
                        long Deactivateactid = -1;
                        long Altenateactid = -1;
                        long complianceID = 0;
                        long customerID = 0;


                        //List<long> lstShortCompliance = new List<long>();

                        //for (int i = 0; i < grdAct.Columns.Count-1; i++)
                        //{
                        //    int ComplinaceDistinctID = (Convert.ToInt32(grdAct.Rows[i].Cells[0].Text));
                        //    lstShortCompliance.Add(ComplinaceDistinctID);
                        //}



                        //for (int i = 0; i < lstShortCompliance.Distinct().Count(); i++)
                        //{
                        //    DateTime Date = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        //    Business.DeactivationAct.ChangeStatus(Convert.ToInt32(lstShortCompliance[i]), Date, "A", txtDedescription.Text);
                        //    Business.DeactivationAct.UpdateComplianceDate(Convert.ToInt32(lstShortCompliance[i]));
                        //    Business.DeactivationAct.RemoveUpcomingSchudule(Convert.ToInt32(lstShortCompliance[i]), Date);
                        //}



                        if (!string.IsNullOrEmpty(ddlDeactiAct.SelectedValue) && Convert.ToString(ddlDeactiAct.SelectedValue) != "-1")
                        {
                            Deactivateactid = Convert.ToInt32(ddlDeactiAct.SelectedValue);
                        }
                        if (!string.IsNullOrEmpty(ddlAlterAct.SelectedValue) && Convert.ToString(ddlAlterAct.SelectedValue) != "-1")
                        {
                            Altenateactid = Convert.ToInt32(ddlAlterAct.SelectedValue);
                        }

                        if (Deactivateactid != -1)
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                long OldActComplianceCount = DeactivationAct.GetActComplianceCount(Convert.ToInt32(Deactivateactid));

                                long OldActCustomerCount = DeactivationAct.GetCustomerCount(Convert.ToInt32(Deactivateactid));

                                if (!string.IsNullOrEmpty(ddlAlterAct.SelectedValue) && Convert.ToString(ddlAlterAct.SelectedValue) != "-1")
                                {
                                    Altenateactid = Convert.ToInt32(ddlAlterAct.SelectedValue);
                                }

                                //Get Customer count for Compliance assignment
                                //var OldComplianceDetails = (from row in entities.sp_ComplianceCount(customerID, complianceID)
                                //select (long)row).ToList().Count();

                                //long ComplianceCountdetails = DeactivationAct.GetByActID(Convert.ToInt64(Deactivateactid));
                                //long TotalOldAssignedClient = DeactivationAct.GetByOLDAssignedClient(Convert.ToInt64(Deactivateactid));

                                long? NewActComplianceCount;
                                long? NewActCustomerCount;

                                if (Altenateactid != -1)
                                {
                                    NewActComplianceCount = DeactivationAct.GetActComplianceCount(Convert.ToInt32(Altenateactid));

                                    NewActCustomerCount = DeactivationAct.GetCustomerCount(Convert.ToInt32(Altenateactid));

                                    //TotalNewAssignedClient = DeactivationAct.GetByNewAssignedClient(Convert.ToInt64(Altenateactid));
                                }
                                else
                                {
                                    NewActComplianceCount = null;
                                    NewActCustomerCount = null;
                                }

                                Act_Deactivation act = new Act_Deactivation()
                                {
                                    ActID = Altenateactid,
                                    Old_Act_ID = Deactivateactid,
                                    Compliance_count = NewActComplianceCount,
                                    Old_Compliance = OldActComplianceCount,
                                    TotalOldAssigned_Client = OldActCustomerCount,
                                    TotalNewAssigned_Client = NewActCustomerCount,
                                    DeactivationDescription = txtDedescription.Text,
                                    DeactivationDate = DateTime.ParseExact(Convert.ToString(tbxStartDate.Text).Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                    Status = "D",
                                };

                                entities.Act_Deactivation.Add(act);
                                entities.SaveChanges();
                                CreateLogForDeactivation(Convert.ToInt32(Altenateactid));
                                cvDuplicateEntry1.IsValid = false;
                                cvDuplicateEntry1.ErrorMessage = "Record saved successfully";
                                vsActValidationGroup.CssClass = "alert alert-success";
                                BindComplianceFilter();
                                upComplianceDetails.Update();
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "One or more Compliance under Selected Act tagged to HR Compliance Product, Please contact IT before Act De-Activation";                        
                    }
                }
                else
                {
                    cvDuplicateEntry1.IsValid = false;
                    cvDuplicateEntry1.ErrorMessage = "Deativation request for this act already send";
                    vsActValidationGroup.CssClass = "alert alert-success";
                }
            }
            catch (Exception ex)
            {
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Something went wrong, Please try again";
                vsActValidationGroup.CssClass = "alert alert-danger";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void CreateLogForDeactivation(long actid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Log_Deactivation log = new Log_Deactivation()
                {
                   ComplianceID=actid,
                    Flag = "D",
                    IsCompliance = "Act",
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedOn = DateTime.Now,

                };
                entities.Log_Deactivation.Add(log);
                entities.SaveChanges();
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindComplianceFilter();
        }
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindIndustry()
        {
            try
            {
                rptIndustry.DataSource = DeactivationAct.GetAllIndustry();
                rptIndustry.DataBind();

                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");

                    if (!chkIndustry.Checked)
                    {
                        chkIndustry.Checked = true;
                    }
                }
                CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                IndustrySelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindActDepartment()
        {
            try
            {
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "ID";
                ddlDepartment.DataSource = DeactivationAct.GetAllActDepartment();
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("< Select Department>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindMinistry()
        {
            try
            {
                ddlMinistry.DataTextField = "Name";
                ddlMinistry.DataValueField = "ID";
                ddlMinistry.DataSource = DeactivationAct.GetAllMinistry();
                ddlMinistry.DataBind();
                ddlMinistry.Items.Insert(0, new ListItem("< Select Ministry>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindRegulator()
        {
            try
            {
                ddlRegulator.DataTextField = "Name";
                ddlRegulator.DataValueField = "ID";
                ddlRegulator.DataSource = DeactivationAct.GetAllRegulator();
                ddlRegulator.DataBind();
                ddlRegulator.Items.Insert(0, new ListItem("< Select Regulator>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindStates()
        {
            try
            {
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";

                ddlState.DataSource = DeactivationAct.GetAllStates();
                ddlState.DataBind();

                ddlState.Items.Insert(0, new ListItem("< All >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindFilterStates()
        {
            try
            {
                ddlfilterState.DataTextField = "Name";
                ddlfilterState.DataValueField = "ID";

                ddlfilterState.DataSource = DeactivationAct.GetAllStates();
                ddlfilterState.DataBind();

                ddlfilterState.Items.Insert(0, new ListItem("< Select States>", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindFilterCatagory()
        {
            try
            {
                var Catagorydetails= DeactivationAct.GetFilterCatagory();
                ddlFilterCategory.DataTextField = "Name";
                ddlFilterCategory.DataValueField = "ID";
                ddlFilterCategory.DataSource = Catagorydetails;
                ddlFilterCategory.DataBind();
                ddlFilterCategory.Items.Insert(0, new ListItem("< Select Category>", "-1"));

               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCatagory()
        {
            try
            {
                var Catagorydetails = DeactivationAct.GetAllCatagory();
               
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";
                ddlCategory.DataSource = Catagorydetails;
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, new ListItem("< Select Category>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindTypes()
        {
            try
            {
                var Typedetails= DeactivationAct.GetAllComplianceType();
                ddlFilterType.DataTextField = "Name";
                ddlFilterType.DataValueField = "ID";
                ddlFilterType.DataSource = Typedetails;
                ddlFilterType.DataBind();
                ddlFilterType.Items.Insert(0, new ListItem("< Select Type>", "-1"));

                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";
                ddlType.DataSource = Typedetails;
                ddlType.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindAct()
        {
            try
            {
                var ActDetails= DeactivationAct.GetAllAct();
                ddlDeactiAct.DataTextField = "Name";
                ddlDeactiAct.DataValueField = "ID";
                ddlDeactiAct.DataSource = ActDetails;
                ddlDeactiAct.DataBind();
                ddlDeactiAct.Items.Insert(0, new ListItem("< Select Act >", "-1"));
                ddlDeactiAct.SelectedIndex = -1;

                ddlAlterAct.DataTextField = "Name";
                ddlAlterAct.DataValueField = "ID";
                ddlAlterAct.DataSource = ActDetails;
                ddlAlterAct.DataBind();
                ddlAlterAct.Items.Insert(0, new ListItem("< Select Act >", "-1"));
                ddlAlterAct.SelectedIndex = -1;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCities()
        {
            try
            {
                ddlCity.DataSource = null;
                ddlCity.DataBind();
                ddlCity.ClearSelection();

                ddlCity.DataTextField = "Name";
                ddlCity.DataValueField = "ID";

                ddlCity.DataSource = DeactivationAct.GetAllCitiesByState(Convert.ToInt32(ddlState.SelectedValue));
                ddlCity.DataBind();

                ddlCity.Items.Insert(0, new ListItem("< All >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlfilterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                if (Convert.ToString(ddlFilterType.SelectedItem.Text).Trim().ToUpper() == "CENTRAL")
                {
                    ddlfilterState.SelectedValue = "-1";
                    
                    ddlfilterState.Visible = false;
                }
                else
                {
                    ddlfilterState.Visible = true;
                }

                BindComplianceFilter();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string type =DeactivationAct.GetByID(Convert.ToInt32(ddlType.SelectedValue)).Name;
                //if (type != "State" && type != "Central" && type != "National")
                if (type == "State")
                {
                    //divCity.Visible = ddlState.SelectedValue != "-1";
                    if (ddlState.SelectedValue != "-1")
                    {
                        BindCities();
                        ddlCity.SelectedValue = "-1";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddldeactiveAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //BindAct();
                BindComplianceFilter();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlAlternativeAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var compliancecount = DeactivationAct.GetByActID(Convert.ToInt64(ddlAlterAct.SelectedValue));
                Label1.Text =Convert.ToString(compliancecount);
             
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlfilterCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindComplianceFilter();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlfilterState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!(ddlType.SelectedItem.Text.Equals("National") || ddlType.SelectedItem.Text.Equals("Central") || ddlType.SelectedItem.Text.Equals("< Select Type >")))
                {
                    cmpvState.Enabled = true;
                    divState.Visible = true;
                    divCity.Visible = true;
                }
                else
                {
                    cmpvState.Enabled = false;
                    divState.Visible = false;
                    divCity.Visible = false;
                }

                if (ddlType.SelectedItem.Text == "State")
                {
                    cmpvState.Enabled = true;
                    divState.Visible = true;
                    divCity.Visible = true;
                }

                ddlState.SelectedValue = "-1";
                ddlCity.SelectedValue = "-1";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeIndustryList", string.Format("initializeJQueryUI('{0}', 'dvIndustry');", txtIndustry.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideIndustryList", "$(\"#dvIndustry\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                upComplianceDetails.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divActDeactivationDetailsDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            saveopo.Value = "true";
        }
        public static Act_Deactivation GetByID(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var act = (from row in entities.Act_Deactivation
                           where row.ActID == ID 
                           select row).FirstOrDefault();
                return act;
            }
        }
        protected void btnActSave_Click(object sender, EventArgs e)
        {
            try
            {
                int DepartmentID = -1;
                int Ministry = -1;
                int Regulator = -1;

                if (!string.IsNullOrEmpty(ddlDepartment.SelectedValue))
                {
                    if (ddlDepartment.SelectedValue != "-1")
                    {
                        DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlMinistry.SelectedValue))
                {
                    if (ddlMinistry.SelectedValue != "-1")
                    {
                        Ministry = Convert.ToInt32(ddlMinistry.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlRegulator.SelectedValue))
                {
                    if (ddlRegulator.SelectedValue != "-1")
                    {
                        Regulator = Convert.ToInt32(ddlRegulator.SelectedValue);
                    }
                }
                Act act = new Act()
                {
                    Name = tbxName.Text,
                    ComplianceTypeId = Convert.ToInt32(ddlType.SelectedValue),
                    ComplianceCategoryId = Convert.ToInt32(ddlCategory.SelectedValue),
                    CreatedBy = AuthenticationHelper.UserID,
                    ProductCode = "C",
                    Act_DeptID = DepartmentID,
                    RegulatorID = Regulator,
                    MinistryID = Ministry
                };

                string ComplianceType = ComplianceTypeManagement.GetByID((int)act.ComplianceTypeId).Name;
                if (ComplianceType != "National" && ComplianceType != "Central")
                {
                    act.StateID = ddlState.SelectedValue != "-1" ? Convert.ToInt32(ddlState.SelectedValue) : (int?)null;
                    act.State = ddlState.SelectedValue != "-1" ? ddlState.SelectedItem.Text : null;
                    if (ComplianceType == "State")
                    {
                        act.CityID = ddlCity.SelectedValue != "-1" ? Convert.ToInt32(ddlCity.SelectedValue) : (int?)null;
                        act.City = ddlCity.SelectedValue != "-1" ? ddlCity.SelectedItem.Text : null;
                    }
                }

                if (!string.IsNullOrEmpty(tbxStartDate.Text))
                    act.StartDate = DateTime.ParseExact(Convert.ToString(tbxStartDate.Text).Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture);

                #region check industry
                Boolean chkIndustryFlag = false;
                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                    if (chkIndustry.Checked)
                    {
                        chkIndustryFlag = true;
                        break;
                    }
                }
                #endregion

                if (chkIndustryFlag == false)
                {
                    saveopo.Value = "true";
                    cvDuplicateEntry1.ErrorMessage = "Please select at least one industry.";
                    cvDuplicateEntry1.IsValid = false;
                    //return;
                }
                else
                {
                    if ((int)ViewState["Mode"] == 1)
                    {
                        act.ID = Convert.ToInt32(ViewState["ActID"]);
                    }

                    bool isExists = ActManagement.Exists(act);

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (isExists)
                        {
                            saveopo.Value = "true";
                            cvDuplicateEntry1.ErrorMessage = "Act name already exists.";
                            cvDuplicateEntry1.IsValid = false;
                        }
                        else
                        {
                            #region Add                 
                            int _objActID = ActManagement.Create(act);

                            if (_objActID > 0)
                            {
                                Act_Document act_Document = null;
                                HttpFileCollection fileCollection = Request.Files;
                                if (fileCollection.Count > 0)
                                {
                                    #region file upload
                                    int count = 1;
                                    for (int i = 0; i < fileCollection.Count; i++)
                                    {
                                        HttpPostedFile uploadfile = null;
                                        uploadfile = fileCollection[i];
                                        string fileName = uploadfile.FileName;
                                        string directoryPath = null;

                                        if (!string.IsNullOrEmpty(fileName))
                                        {
                                            string version = string.Empty;

                                            version = Convert.ToString(count) + ".0";
                                            directoryPath = Server.MapPath("~/ActFiles/" + Convert.ToString(_objActID) + "/" + version);
                                            //directoryPath = Server.MapPath("~/ActFiles/" + Convert.ToString(_objActID));

                                            DocumentManagement.CreateDirectory(directoryPath);
                                            string finalPath = Path.Combine(directoryPath, uploadfile.FileName);
                                            finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                                            fileCollection[i].SaveAs(Server.MapPath(finalPath));

                                            Stream fs = uploadfile.InputStream;
                                            BinaryReader br = new BinaryReader(fs);
                                            Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                            act_Document = new Act_Document()
                                            {
                                                Act_ID = _objActID,
                                                FileName = fileCollection[i].FileName,
                                                //FileData = bytes,
                                                FilePath = finalPath,
                                                IsActive = true,
                                                CreatedOn = DateTime.Now,
                                                Createdby = userId,
                                                UpdatedOn = DateTime.Now,
                                                Updatedby = userId,
                                                Version = version,
                                                VersionDate = DateTime.Now
                                            };
                                            var UploadActdata = ActManagement.SaveActFiledata(act_Document);
                                            count++;
                                        }
                                    }
                                    #endregion
                                }

                                #region add Industry                            
                                List<Act_IndustryMapping> mappingInustries = new List<Act_IndustryMapping>();

                                foreach (RepeaterItem aItem in rptIndustry.Items)
                                {
                                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");

                                    if (chkIndustry.Checked)
                                    {
                                        Act_IndustryMapping ActIndustryMapping = new Act_IndustryMapping()
                                        {
                                            ActID = act.ID,
                                            IndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()),
                                            IsActive = true,
                                            EditedDate = DateTime.Now,
                                            EditedBy = Convert.ToInt32(Session["userID"]),
                                        };
                                        mappingInustries.Add(ActIndustryMapping);
                                    }
                                }

                                ActManagement.CreateOrUpdateActIndustryMapping(mappingInustries, act.ID, false);
                                #endregion
                            }

                            if (isExists == false && _objActID > 0)
                            {
                                saveopo.Value = "true";
                                cvDuplicateEntry.ErrorMessage = "Record Saved Sucessfully.";
                                cvDuplicateEntry.IsValid = false;
                                //btnSave.Enabled = false;
                               // BindGridForFile(_objActID);
                            }
                            #endregion
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                       

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                vsActdeactivation.CssClass = "alert alert-danger";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divActDeactivationDetailsDialog\").dialog('close')", true);
            BindAct();
        }

        protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DELETE_COMPLIANCE"))
                {

                    long CustomerID = 0;
                    int complianceID = Convert.ToInt32(e.CommandArgument);
                    var compliance = Business.DeactivationAct.GetByID(complianceID);
                    ViewState["Mode"] = 1;
                    ViewState["ComplianceID"] = complianceID;
                    bool chkexistsCompliance = Business.DeactivationAct.AssignmentExists(complianceID,CustomerID);
                    if (chkexistsCompliance == false)
                    {
                        
                       // Business.DeactivationAct.Delete(complianceID);
                        BindComplianceFilter();
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "Record deleted successfully";
                        vsActValidationGroup.CssClass = "alert alert-success";
                    }
                }

               
                else
                {
                    cvDuplicateEntry1.IsValid = false;
                    cvDuplicateEntry1.ErrorMessage = "Compliance can not be deleted ,because it is tagged to compliances which are already performed.";
                    vsActValidationGroup.CssClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry1.IsValid = false;
                cvDuplicateEntry1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }
        protected void grdCompliances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }
        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdAct.PageIndex = e.NewPageIndex;
                BindComplianceFilter();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

       


    }
}