﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using System.IO;
using System.Collections;
using System.Globalization;
using System.Configuration;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class AssignComplianceToUser : System.Web.UI.Page
    {
        //static bool ReviewerFlag;
        protected static List<Int32> roles;
        protected string Reviewername;
        protected string Performername;
        //static int UserRoleID;
        public static string CompDocReviewPath = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    //ReviewerFlag = false;                                                                           
                    BindLocationFilter();
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                    //if (roles.Contains(6))
                    //{
                    //    ReviewerFlag = true;
                    //    ShowReviewer(sender, e);
                    //}
                    btnSearch_Click(sender, e);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        //protected void upDownloadList_Load(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideActList", "$(\"#dvActList\").hide(\"blind\", null, 5, function () { });", true);
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}


        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);
                string isstatutoryinternal = string.Empty;
                
                if (ddlType.SelectedValue.Equals("S") || ddlType.SelectedValue.Equals("C") || ddlType.SelectedValue.Equals("E"))
                {
                    isstatutoryinternal = "S";
                }
                if (ddlType.SelectedValue.Equals("I") || ddlType.SelectedValue.Equals("IC"))
                {
                    isstatutoryinternal = "I";
                }

                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdReviewerComplianceDocument_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //FillComplianceDocuments();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                //if (ddlType.SelectedValue == "-1" || ddlType.SelectedValue == "1")
                //{

                    grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdReviewerComplianceDocument.PageIndex = 0;
                //}
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        public void FillComplianceDocuments()
        {
            try
            {

                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);                
                String location = tvFilterLocation.SelectedNode.Text;
                Session["TotalRows"] = 0;

                int branchID = -1;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                string type = ddlType.SelectedValue.ToString();                
                var dataSource = EventManagement.GetComplianceAssignedNew(Convert.ToInt32(AuthenticationHelper.UserID),Convert.ToInt32(AuthenticationHelper.CustomerID), branchID, type,Convert.ToString(AuthenticationHelper.Role)).ToList();                
                grdReviewerComplianceDocument.DataSource = dataSource;
                grdReviewerComplianceDocument.DataBind();
                Session["TotalRows"] = dataSource.Count;
                grdReviewerComplianceDocument.Visible = true;
                GetPageDisplaySummary();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdReviewerComplianceDocument.PageSize = int.Parse(((DropDownList)sender).SelectedValue);
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }
                
                grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                FillComplianceDocuments();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                //if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1")
                //{

                    grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //}
                //Reload the Grid
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                //if (ddlDocType.SelectedValue == "-1" || ddlDocType.SelectedValue == "1")
                //{

                    grdReviewerComplianceDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdReviewerComplianceDocument.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //}
                //Reload the Grid
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                String FileName = String.Empty;
                String ReportName = String.Empty;
                if (ddlType.SelectedValue.Equals("S"))
                {
                    FileName = "StatutoryComplianceReport";
                    ReportName = "Statutory Compliance Assignment Report";
                }
                else if (ddlType.SelectedValue.Equals("C"))
                {
                    FileName = "CheckListComplianceReport";
                    ReportName = "CheckList Report";
                }
                else if (ddlType.SelectedValue.Equals("E"))
                {
                    FileName = "EventBasedComplianceReport";
                    ReportName = "Event Based Report";
                }
                else if (ddlType.SelectedValue.Equals("I"))
                {
                    FileName = "InternalComplianceReport";
                    ReportName = "Internal Compliance Assignment Report";
                }
                else if (ddlType.SelectedValue.Equals("IC"))
                {
                    FileName = "InternalChecklistComplianceReport";
                    ReportName = "Internal CheckList Compliance Report";
                }
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                String location = tvFilterLocation.SelectedNode.Text;

                int branchID = -1;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }
                string type = ddlType.SelectedValue.ToString();
                var dataSource = EventManagement.GetComplianceAssignedNew(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(AuthenticationHelper.CustomerID), branchID, type, Convert.ToString(AuthenticationHelper.Role)).ToList();
                //if (dataToExport == null)
                //if (dataSource.Count < 1)
                //{
                //    ScriptManager.RegisterStartupScript(this, Page.GetType(), "EmptyDataForExport", " $(function () { alert('No data available for export...'); });", true);
                //    return;
                //}
                DataTable dataToExport = (dataSource as List<Sp_GetStatutoryInternalAssignment_Result>).ToDataTable();
                               
                if (ddlType.SelectedValue.Equals("S") || ddlType.SelectedValue.Equals("C") || ddlType.SelectedValue.Equals("E"))
                {
                    dataToExport.Columns.Remove("BranchID");
                    dataToExport.Columns.Remove("ComplianceCategoryId");
                    dataToExport.Columns.Remove("DetailedDescription");

                    using (ExcelPackage pck = new ExcelPackage())
                    {
                        var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));

                        ExcelWorksheet ws = pck.Workbook.Worksheets.Add(ReportName);
                        //ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Compliance");
                        ws.Cells["A1"].Style.Font.Bold = true;
                        ws.Cells["A1"].Value = "Customer Name:";

                        ws.Cells["B1:C1"].Merge = true;
                        ws.Cells["B1"].Value = cname;

                        ws.Cells["A2"].Style.Font.Bold = true;
                        ws.Cells["A2"].Value = "Report Name:";

                        ws.Cells["B2:C2"].Merge = true;
                        ws.Cells["B2"].Value = ReportName;

                        ws.Cells["A3"].Style.Font.Bold = true;
                        ws.Cells["A3"].Value = "Report Generated On:";

                        ws.Cells["B3:C3"].Merge = true;
                        ws.Cells["B3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                        ws.Cells["A5"].LoadFromDataTable(dataToExport, true);
                        ws.Cells["A5"].Style.Font.Bold = true;
                        ws.Cells["B5"].Style.Font.Bold = true;
                        ws.Cells["C5"].Style.Font.Bold = true;
                        ws.Cells["D5"].Style.Font.Bold = true;
                        ws.Cells["E5"].Style.Font.Bold = true;
                        ws.Cells["F5"].Style.Font.Bold = true;
                        ws.Cells["G5"].Style.Font.Bold = true;
                        ws.Cells["H5"].Style.Font.Bold = true;
                        ws.Cells["I5"].Style.Font.Bold = true;

                        ws.Cells["J5"].Style.Font.Bold = true;
                        ws.Cells["K5"].Style.Font.Bold = true;
                        ws.Cells["L5"].Style.Font.Bold = true;
                        ws.Cells["M5"].Style.Font.Bold = true;
                        ws.Cells["N5"].Style.Font.Bold = true;
                        ws.Cells["O5"].Style.Font.Bold = true;
                        ws.Cells["P5"].Style.Font.Bold = true;
                        ws.Cells["Q5"].Style.Font.Bold = true;
                        ws.Cells["Q5"].Value = "Label";
                        ws.Cells["R5"].Style.Font.Bold = true;

                        ws.SelectedRange["A5:R5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ws.SelectedRange["A5:R5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);

                        using (ExcelRange col = ws.Cells[5, 1, 5 + dataToExport.Rows.Count, 18])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            //col.AutoFitColumns();

                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }
                        using (ExcelRange col = ws.Cells[5, 1, 5 + dataToExport.Rows.Count, 1 + dataToExport.Columns.Count])
                        {
                            col.AutoFitColumns(30);
                        }
                        using (ExcelRange col = ws.Cells[5, 16, 5 + dataToExport.Rows.Count, 16])
                        {
                            col[5, 15, 5 + dataToExport.Rows.Count, 16].Style.Numberformat.Format = "dd/MMM/yyyy";
                        }

                        //ws.Cells["A5"].LoadFromDataTable(dataToExport, true);
                        Byte[] fileBytes = pck.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=" + FileName + ".xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                }
                if (ddlType.SelectedValue.Equals("I") || ddlType.SelectedValue.Equals("IC"))
                {
                    dataToExport.Columns.Remove("BranchID");
                    dataToExport.Columns.Remove("ComplianceCategoryId");
                    dataToExport.Columns.Remove("State");
                    dataToExport.Columns.Remove("ActName");
                    dataToExport.Columns.Remove("Description");
                    dataToExport.Columns.Remove("Section");
                    dataToExport.Columns.Remove("ShortForm");

                    using (ExcelPackage pck = new ExcelPackage())
                    {
                        var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));

                        ExcelWorksheet ws = pck.Workbook.Worksheets.Add(ReportName);
                        ws.Cells["A1"].Style.Font.Bold = true;
                        ws.Cells["A1"].Value = "Customer Name:";

                        ws.Cells["B1:C1"].Merge = true;
                        ws.Cells["B1"].Value = cname;

                        ws.Cells["A2"].Style.Font.Bold = true;
                        ws.Cells["A2"].Value = "Report Name:";

                        ws.Cells["B2:C2"].Merge = true;
                        ws.Cells["B2"].Value = ReportName;

                        ws.Cells["A3"].Style.Font.Bold = true;
                        ws.Cells["A3"].Value = "Report Generated On:";

                        ws.Cells["B3:C3"].Merge = true;
                        ws.Cells["B3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                        ws.Cells["A5"].LoadFromDataTable(dataToExport, true);
                        ws.Cells["A5"].Style.Font.Bold = true;
                        ws.Cells["B5"].Style.Font.Bold = true;
                        ws.Cells["C5"].Style.Font.Bold = true;
                        ws.Cells["D5"].Style.Font.Bold = true;
                        ws.Cells["E5"].Style.Font.Bold = true;
                        ws.Cells["F5"].Style.Font.Bold = true;
                        ws.Cells["G5"].Style.Font.Bold = true;
                        ws.Cells["H5"].Style.Font.Bold = true;
                        ws.Cells["I5"].Style.Font.Bold = true;

                        ws.Cells["J5"].Style.Font.Bold = true;
                        ws.Cells["K5"].Style.Font.Bold = true;
                        ws.Cells["L5"].Style.Font.Bold = true;
                        ws.Cells["M5"].Style.Font.Bold = true;
                        ws.Cells["M5"].Value = "Label";
                        ws.Cells["N5"].Style.Font.Bold = true;
                        //ws.Cells["O5"].Style.Font.Bold = true;

                        ws.SelectedRange["A5:N5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ws.SelectedRange["A5:N5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);

                        using (ExcelRange col = ws.Cells[5, 1, 5 + dataToExport.Rows.Count, 14])
                        {
                            col.Style.WrapText = true;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            //col.AutoFitColumns();

                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }
                        using (ExcelRange col = ws.Cells[5, 1, 5 + dataToExport.Rows.Count, 1 + dataToExport.Columns.Count])
                        {
                            col.AutoFitColumns(30);
                        }
                        using (ExcelRange col = ws.Cells[5, 11, 5 + dataToExport.Rows.Count, 12])
                        {
                            col[5, 12, 5 + dataToExport.Rows.Count, 12].Style.Numberformat.Format = "dd/MMM/yyyy";
                        }

                       
                        Byte[] fileBytes = pck.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=" + FileName + ".xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                }

            
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindLocationFilter();
        }
    }
}