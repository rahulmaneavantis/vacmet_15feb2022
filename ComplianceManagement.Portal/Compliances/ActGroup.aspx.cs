﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ActGroup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindData();
            }
        }

        protected void btnAddClick(object sender,EventArgs e)
        {
            txtName.Text = string.Empty;
            ViewState["Mode"] = 0;
            upActGroup.Update();
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divActGroupDialog\").dialog('open')", true);

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                com.VirtuosoITech.ComplianceManagement.Business.Data.ActGroupMaster actmaster = new com.VirtuosoITech.ComplianceManagement.Business.Data.ActGroupMaster()
                {
                      Name =txtName.Text,
                      CreatedBy=AuthenticationHelper.UserID,
                      CreatedOn=DateTime.Now,
                      IsActive=true
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    actmaster.ID = Convert.ToInt32(ViewState["actid"]);
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    if (Exists(actmaster.Name))
                    {
                        cvDuplicateEntry.ErrorMessage = "Act Group name already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                    else
                    {
                        CreateActGroup(actmaster);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record save successfully.";
                    }
                }
                else if((int)ViewState["Mode"]==1)
                {
                    if (Exists(actmaster.Name))
                    {
                        cvDuplicateEntry.ErrorMessage = "Act Group name already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                    else
                    {
                        Update(actmaster);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Record Update successfully.";
                    }
                 
                }
                
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divActGroupDialog\").dialog('close')", true);
                BindData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static void Update(ActGroupMaster actmaster)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var actgroupdata = (from row in entities.ActGroupMasters
                                    where row.ID == actmaster.ID
                                    select row).FirstOrDefault();
                actgroupdata.Name = actmaster.Name;
                entities.SaveChanges();
                
            }
        }
        public static bool Exists(string name)
        {
            Boolean flag;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ActGroupMasters
                            where row.Name == name
                            && row.IsActive == true
                            select row).SingleOrDefault();

                if (data != null)
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
                return flag;

            }
        }
        public static bool ExistsActGroupid(int actgroupID)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ActGroupMasters
                             where row.ID == actgroupID && row.IsActive==true
                             select row.ID).SingleOrDefault();

                if (query != 0)
                    return false;
                else
                    return true;
            }
        }

        public static void CreateActGroup(ActGroupMaster actgroupdata)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.ActGroupMasters.Add(actgroupdata);
                entities.SaveChanges();
            }
        }
        private void BindData()
        {
            var Actgroupdata = getdata(tbxFilter.Text);
            grdActGroup.DataSource=Actgroupdata;
            grdActGroup.DataBind();
            upActGroupList.Update();

        }
        public static object getdata(string filter=null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var details = (from row in entities.ActGroupMasters
                               where row.IsActive==true
                               select row);
                if (!string.IsNullOrEmpty(filter))
                {
                    details = details.Where(entry => entry.Name.Contains(filter));
                }
                return details.ToList();
            }
        }
        protected  void grdActGroup_RowCommand(object sender,GridViewCommandEventArgs e)
        {
            if(e.CommandName== "Edit_ActGroup")
            {
                int actgroupid = Convert.ToInt32(e.CommandArgument);
                ViewState["Mode"] = 1;
                ViewState["actid"] = actgroupid;
                var Actgroupdetails = Getactgroupdata(actgroupid);
                txtName.Text = Actgroupdetails.Name;

                upActGroup.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divActGroupDialog\").dialog('open')", true);

            }
            else if(e.CommandName== "Delete_ActGroup")
            {
                int actgroupid = Convert.ToInt32(e.CommandArgument);
                Boolean checkexists = ExistsActGroupid(actgroupid);
                if(!checkexists)
                {
                    Delete(actgroupid);
                }
                BindData();
            }
        }
        public static void Delete(int groupid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ActGroupMasters
                            where row.ID == groupid
                            select row).SingleOrDefault();

                data.IsActive = false;
                entities.SaveChanges();
            }
        }
        public static ActGroupMaster Getactgroupdata(int actgroupid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ActGroupMasters
                            where row.ID== actgroupid
                            select row).SingleOrDefault();
                return data;
            }
        }

        protected void grdActGroup_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //grdActGroup.PageIndex=0;
                grdActGroup.PageIndex = e.NewPageIndex;
                BindData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}