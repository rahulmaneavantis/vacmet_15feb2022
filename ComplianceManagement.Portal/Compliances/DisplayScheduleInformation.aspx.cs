﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class DisplayScheduleInformation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindActs();
            }
        }
        private void BindActs()
        {
            try
            {
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActManagement.GetAllAssignedActs();
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCompliances(long actID)
        {
            try
            {
                ddlCompliance.DataTextField = "Name";
                ddlCompliance.DataValueField = "ID";

                ddlCompliance.DataSource = ActManagement.GetAllAssignedCompliances(actID);
                ddlCompliance.DataBind();

                ddlCompliance.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlAct.SelectedValue) != -1)
            {
                BindCompliances(Convert.ToInt32(ddlAct.SelectedValue));
            }
        }
        protected void ddlCompliance_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdCompliances.DataSource = null;
            grdCompliances.DataBind();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (Convert.ToInt32(ddlAct.SelectedValue) != -1 && Convert.ToInt32(ddlCompliance.SelectedValue) != -1)
                {

                    long actid = Convert.ToInt32(ddlAct.SelectedValue);
                    long complianced = Convert.ToInt32(ddlCompliance.SelectedValue);
                

                    var ScheduleList = (from row in entities.Acts
                                        join row1 in entities.Compliances
                                        on row.ID equals row1.ActID
                                        join row2 in entities.ComplianceInstances
                                        on row1.ID equals row2.ComplianceId
                                        join row3 in entities.ComplianceAssignments
                                        on row2.ID equals row3.ComplianceInstanceID
                                        join row4 in entities.ComplianceScheduleOns
                                        on row3.ComplianceInstanceID equals row4.ComplianceInstanceID
                                        join row5 in entities.CustomerBranches
                                        on row2.CustomerBranchID equals row5.ID
                                        join row6 in entities.Customers
                                        on row5.CustomerID equals row6.ID
                                        where row1.IsDeleted == false && row1.ActID == actid
                                        && row1.ID == complianced
                                        && row4.IsUpcomingNotDeleted == true && row4.IsActive == true
                                        select new
                                        {
                                            ComplianceId=row1.ID,
                                            CustomerName = row6.Name,
                                            CustomerBranchName = row5.Name,
                                            ActName = row.Name,
                                            row1.ShortDescription,
                                            row4.ForMonth,
                                            row4.ScheduleOn,
                                            row4.ComplianceInstanceID,
                                        }).Distinct();

                    var compliances = ScheduleList.ToList();
                    compliances = compliances.OrderBy(entry => entry.CustomerName).ToList();
                    grdCompliances.DataSource = compliances;
                    grdCompliances.DataBind();
                }
               
            }

        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {

                 //this.Validate(btnExport.ValidationGroup);
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("ScheduleInformation");
                        DataTable ExcelData = null;
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {

                        entities.Database.CommandTimeout = 360;
                        long actid = Convert.ToInt32(ddlAct.SelectedValue);
                        long complianced = Convert.ToInt32(ddlCompliance.SelectedValue);

                        var ScheduleList = (from row in entities.Acts
                                            join row1 in entities.Compliances
                                            on row.ID equals row1.ActID
                                            join row2 in entities.ComplianceInstances
                                            on row1.ID equals row2.ComplianceId
                                            join row3 in entities.ComplianceAssignments
                                            on row2.ID equals row3.ComplianceInstanceID
                                            join row4 in entities.ComplianceScheduleOns
                                            on row3.ComplianceInstanceID equals row4.ComplianceInstanceID
                                            join row5 in entities.CustomerBranches
                                            on row2.CustomerBranchID equals row5.ID
                                            join row6 in entities.Customers
                                            on row5.CustomerID equals row6.ID
                                            where row1.IsDeleted == false && row1.ActID == actid
                                            && row1.ID == complianced
                                            && row4.IsUpcomingNotDeleted == true && row4.IsActive == true
                                            select new
                                            {
                                                ComplianceId=row1.ID,
                                                CustomerName = row6.Name,
                                                CustomerBranchName = row5.Name,
                                                ActName = row.Name,
                                                row1.ShortDescription,
                                                row4.ForMonth,
                                                row4.ScheduleOn,
                                                row4.ComplianceInstanceID,
                                            }).Distinct();

                        var Data = ScheduleList.ToList();
                        Data = Data.OrderBy(entry => entry.CustomerName).ToList();

                        DataTable table = Data.ToDataTable();

                            DataView view = new System.Data.DataView(table);
                   
                      
                        ExcelData = view.ToTable("Selected", false, "ComplianceId", "CustomerName", "CustomerBranchName", "ShortDescription", "ForMonth", "ScheduleOn");

                        ExcelData.Columns.Add("SNo", typeof(int)).SetOrdinal(0);
                        int rowCount = 0;
                        foreach (DataRow item in ExcelData.Rows)
                        {
                            item["SNo"] = ++rowCount;
                        }

                            exWorkSheet.Cells["A3:G3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["A3:G3"].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);


                       
                            exWorkSheet.Cells["A3"].LoadFromDataTable(ExcelData, true);
                            exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["A3"].Value = " Sr.No.";
                            exWorkSheet.Cells["A3"].AutoFitColumns(10);

                            exWorkSheet.Cells["B3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["B3"].Value = "Compliance Id";
                            exWorkSheet.Cells["B3"].AutoFitColumns(20);

                            exWorkSheet.Cells["C3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["C3"].Value = "Customer Name";
                            exWorkSheet.Cells["C3"].AutoFitColumns(40);

                            exWorkSheet.Cells["D3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["D3"].Value = "Customer Branch Name";
                            exWorkSheet.Cells["D3"].AutoFitColumns(40);


                            exWorkSheet.Cells["E3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["E3"].Value = "Short Description";
                            exWorkSheet.Cells["E3"].AutoFitColumns(50);


                            exWorkSheet.Cells["F3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["F3"].Value = "Period";
                            exWorkSheet.Cells["F3"].AutoFitColumns(25);

                            exWorkSheet.Cells["G3"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G3"].Style.Font.Size = 12;
                            exWorkSheet.Cells["G3"].Value = "Due Date";
                            exWorkSheet.Cells["G3"].AutoFitColumns(25);


                    }
                        using (ExcelRange col = exWorkSheet.Cells[1, 1, 3 + ExcelData.Rows.Count, 7])
                        {

                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                        }
                    
                         using (ExcelRange col = exWorkSheet.Cells[3, 6, 3 + ExcelData.Rows.Count, 7])
                          {
                               col[3, 6, 3 + ExcelData.Rows.Count, 7].Style.Numberformat.Format = "dd/MMM/yyyy";
                          }
                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=ScheduleInformationReport.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }

               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }
    }
}