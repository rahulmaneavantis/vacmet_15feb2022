﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class UsageReportIMP : System.Web.UI.Page
    {
        public static List<int> Branchlist = new List<int>();
        public static List<long> UserList = new List<long>();
        public static List<long> ICUserList = new List<long>();
        protected int CustomerID = 0;
        public string user_Roles = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    user_Roles = AuthenticationHelper.Role;
                    if(AuthenticationHelper.Role!="IMPT")
                    {
                        BindLocationFilter();
                    }
                    else
                    {
                        DivCustomer.Visible = true;
                        BindCustomer();
                    }
                   
                    //ExportAllData();
                    
                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "InitializeJQueryUI", "initializeJQueryUI();", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int CustomerID = -1;
                tvBranches.Nodes.Clear();
                user_Roles = AuthenticationHelper.Role;
                if (user_Roles == "IMPT")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        if (ddlCustomer.SelectedValue != "-1")
                        {
                            CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                        }
                        else
                        {
                            CustomerID = -1;
                        }
                    }
                }
                else
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                if (CustomerID != -1)
                {
                    tvBranches.Nodes.Clear();
                    NameValueHierarchy branch = null;
                    var branchs = CustomerBranchManagement.GetAllHierarchy(CustomerID);
                    if (branchs.Count > 0)
                    {
                        branch = branchs[0];
                    }
                    tbxBranch.Text = "< Select Location >";
                    List<TreeNode> nodes = new List<TreeNode>();
                    BindBranchesHierarchy(null, branch, nodes);
                    foreach (TreeNode item in nodes)
                    {
                        tvBranches.Nodes.Add(item);
                    }
                    tvBranches.CollapseAll();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<int> LocationList)
        {
            foreach (var item in nvp.Children)
            {
                TreeNode node = new TreeNode(item.Name, item.ID.ToString());

                if (FindNodeExists(item, LocationList))
                {
                    BindBranchesHierarchy(node, item, LocationList);
                    parent.ChildNodes.Add(node);
                }
            }
        }

        public bool FindNodeExists(NameValueHierarchy item, List<int> ListIDs)
        {
            bool result = false;
            try
            {
                if (ListIDs.Contains(item.ID))
                {
                    result = true;
                    return result;
                }
                else
                {
                    foreach (var childNode in item.Children)
                    {
                        if (ListIDs.Contains(childNode.ID))
                        {
                            result = true;
                        }
                        else
                        {
                            result = FindNodeExists(childNode, ListIDs);
                        }

                        if (result)
                        {
                            break;
                        }
                    }


                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "< Select Branch >";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "InitializeJQueryUI", "initializeJQueryUI();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void UpdatePanel1_Load(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "InitializeJQueryUI", "initializeJQueryUI();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static int MonthDiff(DateTime d1, DateTime d2)
        {
            int retVal = 0;

            if (d1.Month < d2.Month)
            {
                retVal = (d1.Month + 12) - d2.Month;
                retVal += ((d1.Year - 1) - d2.Year) * 12;
            }
            else
            {
                retVal = (d1.Month - d2.Month) + 1;
                retVal += (d1.Year - d2.Year) * 12;
            }
            return retVal;
        }
        public void ExportAllData()
        {
            try
            {
                DateTime fromDatePeroid = new DateTime();
                DateTime toDatePeroid = new DateTime();
                user_Roles = AuthenticationHelper.Role;
                CustomerID = -1;
                if (user_Roles == "IMPT")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        if (ddlCustomer.SelectedValue != "-1")
                        {
                            CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                        }
                        else
                        {
                            DateTime date = DateTime.Now;
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                            cvDuplicateEntry.ErrorMessage = "Customer should not be blank.";
                            cvDuplicateEntry.IsValid = false;
                            return;
                        }
                    }
                }
                else
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }


                if (!string.IsNullOrEmpty(txtStartDate.Text))
                {
                    if (!string.IsNullOrEmpty(txtEndDate.Text))
                    {
                        fromDatePeroid = DateTimeExtensions.GetDate(txtStartDate.Text);
                        toDatePeroid = DateTimeExtensions.GetDate(txtEndDate.Text);

                        if (fromDatePeroid > toDatePeroid)
                        {
                            cvDuplicateEntry.ErrorMessage = "From Date should not be greater than Todate.";
                            cvDuplicateEntry.IsValid = false;
                            DateTime date = DateTime.Now;
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "InitializeJQueryUI", "initializeJQueryUI();", true);
                            return;
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.ErrorMessage = "To Date should not be blank.";
                        cvDuplicateEntry.IsValid = false;
                        DateTime date = DateTime.Now;
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "InitializeJQueryUI", "initializeJQueryUI();", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                        return;
                    }
                }
                else
                {
                    cvDuplicateEntry.ErrorMessage = "From Date should not be blank.";
                    cvDuplicateEntry.IsValid = false;
                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "InitializeJQueryUI", "initializeJQueryUI();", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                    return;
                }

                var NoOfMonth = MonthDiff(toDatePeroid, fromDatePeroid);
                Branchlist.Clear();
                if (!string.IsNullOrEmpty(tvBranches.SelectedValue))
                {
                    GetAllHierarchy(CustomerID, Convert.ToInt32(tvBranches.SelectedValue));
                }
                List<SP_UsageReports_Result> Master_SP_UsageReports = new List<SP_UsageReports_Result>();
                List<SP_InternalUsageReports_Result> Master_SP_InternalUsageReports = new List<SP_InternalUsageReports_Result>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout =360 ;
                    Master_SP_UsageReports = (from row in entities.SP_UsageReports(AuthenticationHelper.UserID, CustomerID, "IMPT", fromDatePeroid, toDatePeroid)
                                              select row).ToList();

                    Master_SP_InternalUsageReports = (from row in entities.SP_InternalUsageReports(AuthenticationHelper.UserID, CustomerID, "IMPT", fromDatePeroid, toDatePeroid)
                                              select row).ToList();
                }

                #region Define DataTable
                DataTable dtLogingDatail = new DataTable();
                DataTable dtLastLogingDatails = new DataTable();
                DataTable dtLogingDatailOpen = new DataTable();
                DataTable dtCompleteCompliance = new DataTable();
                DataTable dtAfterDueDate = new DataTable();
                DataTable dtReviewerData = new DataTable();

               
                dtLogingDatail.Columns.Add("SLNO", typeof(string));
                dtLogingDatail.Columns.Add("UserID", typeof(string));
                dtLogingDatail.Columns.Add("UserName", typeof(string));
                dtLogingDatail.Columns.Add("Location", typeof(string));
                dtLogingDatail.Columns.Add("Role", typeof(string));
                for (int i = 0; i < NoOfMonth; i++)
                {
                    dtLogingDatail.Columns.Add("Month" + i, typeof(string));
                }

                //Last Loging
                dtLastLogingDatails.Columns.Add("SLNO", typeof(string));
                dtLastLogingDatails.Columns.Add("UserID", typeof(string));
                dtLastLogingDatails.Columns.Add("UserName", typeof(string));
                dtLastLogingDatails.Columns.Add("Location", typeof(string));
                dtLastLogingDatails.Columns.Add("Role", typeof(string));
                dtLastLogingDatails.Columns.Add("LastLogingTime", typeof(DateTime));

                //Open
                dtLogingDatailOpen.Columns.Add("SLNO", typeof(string));
                dtLogingDatailOpen.Columns.Add("UserID", typeof(string));
                dtLogingDatailOpen.Columns.Add("UserName", typeof(string));
                dtLogingDatailOpen.Columns.Add("Location", typeof(string));
                dtLogingDatailOpen.Columns.Add("Role", typeof(string));
                for (int i = 0; i < NoOfMonth; i++)
                {
                    dtLogingDatailOpen.Columns.Add("Month" + i, typeof(string));
                }

                //After
                dtCompleteCompliance.Columns.Add("SLNO", typeof(string));
                dtCompleteCompliance.Columns.Add("UserID", typeof(string));
                dtCompleteCompliance.Columns.Add("UserName", typeof(string));
                dtCompleteCompliance.Columns.Add("Location", typeof(string));
                dtCompleteCompliance.Columns.Add("Role", typeof(string));
                for (int i = 0; i < NoOfMonth; i++)
                {
                    dtCompleteCompliance.Columns.Add("Month" + i, typeof(string));
                }

                //Download
                dtAfterDueDate.Columns.Add("SLNO", typeof(string));
                dtAfterDueDate.Columns.Add("UserID", typeof(string));
                dtAfterDueDate.Columns.Add("UserName", typeof(string));
                dtAfterDueDate.Columns.Add("Location", typeof(string));
                dtAfterDueDate.Columns.Add("Role", typeof(string));
                dtAfterDueDate.Columns.Add("BeforeDays", typeof(string));
                dtAfterDueDate.Columns.Add("ThreeDays", typeof(string));
                dtAfterDueDate.Columns.Add("SevenDays", typeof(string));
                dtAfterDueDate.Columns.Add("FifteenDays", typeof(string));
                dtAfterDueDate.Columns.Add("ThirtyDays", typeof(string));
                dtAfterDueDate.Columns.Add("MoreThanThirtyDays", typeof(string));

                dtReviewerData.Columns.Add("SLNO", typeof(string));
                dtReviewerData.Columns.Add("UserID", typeof(string));
                dtReviewerData.Columns.Add("UserName", typeof(string));
                dtReviewerData.Columns.Add("Location", typeof(string));
                dtReviewerData.Columns.Add("Role", typeof(string));
                for (int i = 0; i < NoOfMonth; i++)
                {
                    dtReviewerData.Columns.Add("Month" + i, typeof(string));
                }

                #endregion

                #region Defining DataTable for Internal Compliance
                DataTable dtCompleteICompliance = new DataTable();
                DataTable dtAfterDueDateIC = new DataTable();
                DataTable dtLogingDatailOpenIC = new DataTable();
                DataTable dtReviewerDataIC = new DataTable();

                dtLogingDatailOpenIC.Columns.Add("SLNO", typeof(string));
                dtLogingDatailOpenIC.Columns.Add("UserID", typeof(string));
                dtLogingDatailOpenIC.Columns.Add("UserName", typeof(string));
                dtLogingDatailOpenIC.Columns.Add("Location", typeof(string));
                dtLogingDatailOpenIC.Columns.Add("Role", typeof(string));
                for (int i = 0; i < NoOfMonth; i++)
                {
                    dtLogingDatailOpenIC.Columns.Add("Month" + i, typeof(string));
                }

                dtCompleteICompliance.Columns.Add("SLNO", typeof(string));
                dtCompleteICompliance.Columns.Add("UserID", typeof(string));
                dtCompleteICompliance.Columns.Add("UserName", typeof(string));
                dtCompleteICompliance.Columns.Add("Location", typeof(string));
                dtCompleteICompliance.Columns.Add("Role", typeof(string));
                for (int i = 0; i < NoOfMonth; i++)
                {
                    dtCompleteICompliance.Columns.Add("Month" + i, typeof(string));
                }

                dtAfterDueDateIC.Columns.Add("SLNO", typeof(string));
                dtAfterDueDateIC.Columns.Add("UserID", typeof(string));
                dtAfterDueDateIC.Columns.Add("UserName", typeof(string));
                dtAfterDueDateIC.Columns.Add("Location", typeof(string));
                dtAfterDueDateIC.Columns.Add("Role", typeof(string));
                dtAfterDueDateIC.Columns.Add("BeforeDays", typeof(string));
                dtAfterDueDateIC.Columns.Add("ThreeDays", typeof(string));
                dtAfterDueDateIC.Columns.Add("SevenDays", typeof(string));
                dtAfterDueDateIC.Columns.Add("FifteenDays", typeof(string));
                dtAfterDueDateIC.Columns.Add("ThirtyDays", typeof(string));
                dtAfterDueDateIC.Columns.Add("MoreThanThirtyDays", typeof(string));

                dtReviewerDataIC.Columns.Add("SLNO", typeof(string));
                dtReviewerDataIC.Columns.Add("UserID", typeof(string));
                dtReviewerDataIC.Columns.Add("UserName", typeof(string));
                dtReviewerDataIC.Columns.Add("Location", typeof(string));
                dtReviewerDataIC.Columns.Add("Role", typeof(string));
                for (int i = 0; i < NoOfMonth; i++)
                {
                    dtReviewerDataIC.Columns.Add("Month" + i, typeof(string));
                }
                #endregion

                user_Roles = "IMPT";
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    #region Compliance Complete within Due Days
                    if (user_Roles.Contains("IMPT"))
                    {
                        List<int> Performerstatus = new List<int>();
                        Performerstatus.Add(2);
                        Performerstatus.Add(4);
                        Performerstatus.Add(7);
                        entities.Database.CommandTimeout = 180;
                        var FilteredData = Master_SP_UsageReports;
                        //FOR INTERNAL COMPLIANCE 
                        var FilteredIData = Master_SP_InternalUsageReports;
                        if (Branchlist.Count > 0)
                        {
                            FilteredData = FilteredData.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                            FilteredIData = FilteredIData.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                        }
                        FilteredData = FilteredData.Where(entry => entry.RoleID == 3).ToList();
                        FilteredIData = FilteredIData.Where(entry => entry.RoleID == 3).ToList();

                        UserList = FilteredData.Select(e => e.UserID).Distinct().ToList();
                        ICUserList = FilteredIData.Select(e => e.UserID).Distinct().ToList();
                        int Count = 0;
                        int RowCount1 = 0;
                        int RowCount2 = 0;
                        if (FilteredData.Count > 0)
                        {
                            foreach (var item in UserList)
                            {
                                string Location = string.Empty;
                                string UserName = string.Empty;
                                string Role = string.Empty;
                                List<string> PerformerMonthList = new List<string>();
                                Count++;
                                entities.Database.CommandTimeout = 180;
                                var objData = (from row in FilteredData
                                               join row1 in entities.Users
                                               on row.UserID equals row1.ID
                                               join role in entities.Roles
                                               on row.RoleID equals role.ID
                                               join cust in entities.Customers
                                               on row.CustomerID equals cust.ID
                                               where row.UserID == item
                                               select new UsageReport_Data()
                                               {
                                                   CustomerName = cust.Name,
                                                   UserName = row1.FirstName + " " + row1.LastName,
                                                   Role = role.Name
                                               }).FirstOrDefault();

                                if (objData != null)
                                {
                                    UserName = objData.UserName;
                                }


                                #region 12 month Data Assing
                                for (int i = 0; i < NoOfMonth; i++)
                                {
                                    DateTime now = fromDatePeroid.AddMonths(i);
                                    DateTime startDate = new DateTime(now.Year, now.Month, 1);
                                    DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                                    var DataMonth1 = (from data in FilteredData
                                                      where data.UserID == item
                                                      && data.ScheduledOn >= startDate
                                                      && data.ScheduledOn <= endDate
                                                      select data).ToList();
                                    var Mont1hToltal = DataMonth1.Select(e => e.ScheduledOn).Count();
                                    var Month1ActualData = DataMonth1.Where(e => e.submittedcnt != null).Count();
                                    // var Month1ActualData = DataMonth1.Where(e => Performerstatus.Contains(e.ComplianceStatusID)).Count();
                                    PerformerMonthList.Add(Month1ActualData + "/" + Mont1hToltal);
                                }
                                #endregion

                                Role = GetRole(item);
                                var UserLocation = (from data in entities.Users
                                                    join row in entities.CustomerBranches
                                                    on data.CustomerBranchID equals row.ID
                                                    where data.ID == item
                                                    select row.Name).FirstOrDefault();
                                dtCompleteCompliance.Rows.Add(Count, item, UserName, UserLocation, Role);

                                int p = 0;
                                for (int j = 5; j < dtCompleteCompliance.Columns.Count; j++)
                                {
                                    dtCompleteCompliance.Rows[RowCount1][j] = PerformerMonthList[p];
                                    p++;
                                }
                                RowCount1++;
                            }
                        }

                        if (FilteredIData.Count > 0)
                        {
                            foreach (var item in ICUserList)
                            {
                                string Location = string.Empty;
                                string UserName = string.Empty;
                                string Role = string.Empty;
                                List<string> PerformerMonthList = new List<string>();
                                Count++;
                                entities.Database.CommandTimeout = 180;
                                var objData = (from row in FilteredIData
                                               join row1 in entities.Users
                                               on row.UserID equals row1.ID
                                               join role in entities.Roles
                                               on row.RoleID equals role.ID
                                               join cust in entities.Customers
                                               on row.CustomerID equals cust.ID
                                               where row.UserID == item
                                               select new UsageReport_Data()
                                               {
                                                   CustomerName = cust.Name,
                                                   UserName = row1.FirstName + " " + row1.LastName,
                                                   Role = role.Name
                                               }).FirstOrDefault();

                                if (objData != null)
                                {
                                    UserName = objData.UserName;
                                }


                                #region 12 month Data Assing
                                for (int i = 0; i < NoOfMonth; i++)
                                {
                                    DateTime now = fromDatePeroid.AddMonths(i);
                                    DateTime startDate = new DateTime(now.Year, now.Month, 1);
                                    DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                                    var DataMonth1 = (from data in FilteredIData
                                                      where data.UserID == item
                                                      && data.ScheduledOn >= startDate
                                                      && data.ScheduledOn <= endDate
                                                      select data).ToList();
                                    var Mont1hToltal = DataMonth1.Select(e => e.ScheduledOn).Count();
                                    var Month1ActualData = DataMonth1.Where(e => e.submittedcnt != null).Count();
                                    // var Month1ActualData = DataMonth1.Where(e => Performerstatus.Contains(e.ComplianceStatusID)).Count();
                                    PerformerMonthList.Add(Month1ActualData + "/" + Mont1hToltal);
                                }
                                #endregion

                                Role = GetRole(item);
                                var UserLocation = (from data in entities.Users
                                                    join row in entities.CustomerBranches
                                                    on data.CustomerBranchID equals row.ID
                                                    where data.ID == item
                                                    select row.Name).FirstOrDefault();
                                dtCompleteICompliance.Rows.Add(Count, item, UserName, UserLocation, Role);

                                int p = 0;
                                for (int j = 5; j < dtCompleteICompliance.Columns.Count; j++)
                                {
                                    dtCompleteICompliance.Rows[RowCount2][j] = PerformerMonthList[p];
                                    p++;
                                }
                                RowCount2++;
                            }
                        }
                    }
                    #endregion

                    #region After Due date Completed

                    List<int> StatusList = new List<int>();
                    StatusList.Add(3);
                    StatusList.Add(5);
                    List<int> Performerstatus1 = new List<int>();
                    Performerstatus1.Add(2);
                    Performerstatus1.Add(4);
                    Performerstatus1.Add(7);

                    List<int> notinstatusid = new List<int>();
                    notinstatusid.Add(10);
                    notinstatusid.Add(11);
                    notinstatusid.Add(12);
                    notinstatusid.Add(13);
                    notinstatusid.Add(14);
                    if (user_Roles.Contains("IMPT"))
                    {
                        entities.Database.CommandTimeout = 180;
                        var FilteredData = Master_SP_UsageReports;
                        if (Branchlist.Count > 0)
                        {
                            FilteredData = FilteredData.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                        }


                        FilteredData = FilteredData.Where(entry => entry.RoleID == 3).ToList();
                        var DistinctUserList = FilteredData.Select(e => e.UserID).Distinct().ToList();
                        int Count = 0;
                        if (FilteredData.Count > 0)
                        {
                            foreach (var item in DistinctUserList)
                            {
                                string Location = string.Empty;
                                string UserName = string.Empty;
                                string Role = string.Empty;
                                string BeforeDays = string.Empty;
                                string ThreeDays = string.Empty;
                                string SevenDays = string.Empty;
                                string FifteenDays = string.Empty;
                                string ThirtyDays = string.Empty;
                                string MoreThanThirtyDays = string.Empty;
                                Count++;
                                var objData = (from row in FilteredData
                                               join row1 in entities.Users
                                               on row.UserID equals row1.ID
                                               join role in entities.Roles
                                               on row.RoleID equals role.ID
                                               join cust in entities.Customers
                                               on row.CustomerID equals cust.ID
                                               where row.UserID == item
                                               select new UsageReport_Data()
                                               {
                                                   CustomerName = cust.Name,
                                                   UserName = row1.FirstName + " " + row1.LastName,
                                                   Role = role.Name
                                               }).FirstOrDefault();

                                if (objData != null)
                                {
                                    UserName = objData.UserName;
                                }

                                #region After Due Date Data Assign
                                // On Or Before Due Date
                                var DataMonthBeforeDue = (from data in FilteredData
                                                          where data.UserID == item
                                                          //&& data.closeDate <= data.ScheduledOn 
                                                          //&& Performerstatus1.Contains(data.ComplianceStatusID)
                                                          select data).ToList();
                                var Month1ActualData = DataMonthBeforeDue.Where(e => e.submittedcnt != null).Count();

                                // BeforeDays = Convert.ToString(DataMonthBeforeDue.Select(e => e.ScheduledOn).Count());
                                BeforeDays = Convert.ToString(Month1ActualData);
                                // In 3 Days
                                var DataMonth1 = (from data in FilteredData
                                                  where data.UserID == item
                                                  && data.closeDate > data.ScheduledOn
                                                  && data.closeDate <= data.ScheduledOn.AddDays(3)
                                                  && !notinstatusid.Contains(data.ComplianceStatusID)
                                                  select data).ToList();
                                ThreeDays = Convert.ToString(DataMonth1.Select(e => e.ScheduledOn).Count());

                                // In 7 Days
                                var DataMonth2 = (from data in FilteredData
                                                  where data.UserID == item
                                                  && data.closeDate >= data.ScheduledOn.AddDays(3)
                                                  && data.closeDate <= data.ScheduledOn.AddDays(7)
                                                  && !notinstatusid.Contains(data.ComplianceStatusID)
                                                  select data).ToList();
                                SevenDays = Convert.ToString(DataMonth2.Select(e => e.ScheduledOn).Count());

                                // In 15 Days
                                var DataMonth3 = (from data in FilteredData
                                                  where data.UserID == item
                                                  && data.closeDate >= data.ScheduledOn.AddDays(7)
                                                  && data.closeDate <= data.ScheduledOn.AddDays(15)
                                                  && !notinstatusid.Contains(data.ComplianceStatusID)
                                                  select data).ToList();
                                FifteenDays = Convert.ToString(DataMonth3.Select(e => e.ScheduledOn).Count());

                                // In 30 Days
                                var DataMonth4 = (from data in FilteredData
                                                  where data.UserID == item
                                                  && data.closeDate >= data.ScheduledOn.AddDays(15)
                                                  && data.closeDate <= data.ScheduledOn.AddDays(30)
                                                  && !notinstatusid.Contains(data.ComplianceStatusID)
                                                  select data).ToList();
                                ThirtyDays = Convert.ToString(DataMonth4.Select(e => e.ScheduledOn).Count());

                                // More Than 30 Days
                                var DataMonth5 = (from data in FilteredData
                                                  where data.UserID == item
                                                  && data.closeDate >= data.ScheduledOn.AddDays(30)
                                                  && data.closeDate <= data.ScheduledOn.AddDays(365)
                                                  && !notinstatusid.Contains(data.ComplianceStatusID)
                                                  select data).ToList();
                                MoreThanThirtyDays = Convert.ToString(DataMonth5.Select(e => e.ScheduledOn).Count());

                                //// In 3 Days
                                //var DataMonth1 = (from data in FilteredData
                                //                  where data.UserID == item
                                //                  && data.Dated > data.ScheduledOn
                                //                  && data.Dated <= data.ScheduledOn.AddDays(3)
                                //                 // && StatusList.Contains(data.ComplianceStatusID)
                                //                  select data).ToList();
                                //ThreeDays = Convert.ToString(DataMonth1.Select(e => e.ScheduledOn).Count());

                                //// In 7 Days
                                //var DataMonth2 = (from data in FilteredData
                                //                  where data.UserID == item
                                //                  && data.Dated > data.ScheduledOn.AddDays(3)
                                //                  && data.Dated <= data.ScheduledOn.AddDays(7)
                                //                 // && StatusList.Contains(data.ComplianceStatusID)
                                //                  select data).ToList();
                                //SevenDays = Convert.ToString(DataMonth2.Select(e => e.ScheduledOn).Count());

                                //// In 15 Days
                                //var DataMonth3 = (from data in FilteredData
                                //                  where data.UserID == item
                                //                  && data.Dated > data.ScheduledOn.AddDays(7)
                                //                  && data.Dated <= data.ScheduledOn.AddDays(15)
                                //                 // && StatusList.Contains(data.ComplianceStatusID)
                                //                  select data).ToList();
                                //FifteenDays = Convert.ToString(DataMonth3.Select(e => e.ScheduledOn).Count());

                                //// In 30 Days
                                //var DataMonth4 = (from data in FilteredData
                                //                  where data.UserID == item
                                //                  && data.Dated > data.ScheduledOn.AddDays(15)
                                //                  && data.Dated <= data.ScheduledOn.AddDays(30)
                                //                //  && StatusList.Contains(data.ComplianceStatusID)
                                //                  select data).ToList();
                                //ThirtyDays = Convert.ToString(DataMonth4.Select(e => e.ScheduledOn).Count());

                                //// More Than 30 Days
                                //var DataMonth5 = (from data in FilteredData
                                //                  where data.UserID == item
                                //                  && data.Dated > data.ScheduledOn.AddDays(30)
                                //                  && data.Dated <= data.ScheduledOn.AddDays(365)
                                //                 // && StatusList.Contains(data.ComplianceStatusID)
                                //                  select data).ToList();
                                //MoreThanThirtyDays = Convert.ToString(DataMonth5.Select(e => e.ScheduledOn).Count());

                                #endregion

                                Role = GetRole(item);
                                var UserLocation = (from data in entities.Users
                                                    join row in entities.CustomerBranches
                                                    on data.CustomerBranchID equals row.ID
                                                    where data.ID == item
                                                    select row.Name).FirstOrDefault();
                                dtAfterDueDate.Rows.Add(Count, item, UserName, UserLocation, Role, BeforeDays, ThreeDays, SevenDays, FifteenDays, ThirtyDays, MoreThanThirtyDays);
                            }
                        }
                    }

                    #endregion

                    #region Internal Compliance After Due date Completed

                    List<int> ICnotinstatusid = new List<int>();
                    ICnotinstatusid.Add(10);
                    ICnotinstatusid.Add(11);
                    ICnotinstatusid.Add(12);
                    ICnotinstatusid.Add(13);
                    ICnotinstatusid.Add(14);
                    if (user_Roles.Contains("IMPT"))
                    {
                        entities.Database.CommandTimeout = 180;
                        var FilteredIData = Master_SP_InternalUsageReports;
                        if (Branchlist.Count > 0)
                        {
                            FilteredIData = FilteredIData.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                        }


                        FilteredIData = FilteredIData.Where(entry => entry.RoleID == 3).ToList();
                        var DistinctUserList = FilteredIData.Select(e => e.UserID).Distinct().ToList();
                        int Count = 0;
                        if (FilteredIData.Count > 0)
                        {
                            foreach (var item in DistinctUserList)
                            {
                                string Location = string.Empty;
                                string UserName = string.Empty;
                                string Role = string.Empty;
                                string BeforeDays = string.Empty;
                                string ThreeDays = string.Empty;
                                string SevenDays = string.Empty;
                                string FifteenDays = string.Empty;
                                string ThirtyDays = string.Empty;
                                string MoreThanThirtyDays = string.Empty;
                                Count++;
                                var objData = (from row in FilteredIData
                                               join row1 in entities.Users
                                               on row.UserID equals row1.ID
                                               join role in entities.Roles
                                               on row.RoleID equals role.ID
                                               join cust in entities.Customers
                                               on row.CustomerID equals cust.ID
                                               where row.UserID == item
                                               select new UsageReport_Data()
                                               {
                                                   CustomerName = cust.Name,
                                                   UserName = row1.FirstName + " " + row1.LastName,
                                                   Role = role.Name
                                               }).FirstOrDefault();

                                if (objData != null)
                                {
                                    UserName = objData.UserName;
                                }

                                #region After Due Date Data Assign
                                // On Or Before Due Date
                                var DataMonthBeforeDue = (from data in FilteredIData
                                                          where data.UserID == item
                                                          select data).ToList();
                                var Month1ActualData = DataMonthBeforeDue.Where(e => e.submittedcnt != null).Count();

                                BeforeDays = Convert.ToString(Month1ActualData);
                                // In 3 Days
                                var DataMonth1 = (from data in FilteredIData
                                                  where data.UserID == item
                                                  && data.closeDate > data.ScheduledOn
                                                  && data.closeDate <= data.ScheduledOn.AddDays(3)
                                                  && !ICnotinstatusid.Contains(data.ComplianceStatusID)
                                                  select data).ToList();
                                ThreeDays = Convert.ToString(DataMonth1.Select(e => e.ScheduledOn).Count());

                                // In 7 Days
                                var DataMonth2 = (from data in FilteredIData
                                                  where data.UserID == item
                                                  && data.closeDate >= data.ScheduledOn.AddDays(3)
                                                  && data.closeDate <= data.ScheduledOn.AddDays(7)
                                                  && !ICnotinstatusid.Contains(data.ComplianceStatusID)
                                                  select data).ToList();
                                SevenDays = Convert.ToString(DataMonth2.Select(e => e.ScheduledOn).Count());

                                // In 15 Days
                                var DataMonth3 = (from data in FilteredIData
                                                  where data.UserID == item
                                                  && data.closeDate >= data.ScheduledOn.AddDays(7)
                                                  && data.closeDate <= data.ScheduledOn.AddDays(15)
                                                  && !ICnotinstatusid.Contains(data.ComplianceStatusID)
                                                  select data).ToList();
                                FifteenDays = Convert.ToString(DataMonth3.Select(e => e.ScheduledOn).Count());

                                // In 30 Days
                                var DataMonth4 = (from data in FilteredIData
                                                  where data.UserID == item
                                                  && data.closeDate >= data.ScheduledOn.AddDays(15)
                                                  && data.closeDate <= data.ScheduledOn.AddDays(30)
                                                  && !ICnotinstatusid.Contains(data.ComplianceStatusID)
                                                  select data).ToList();
                                ThirtyDays = Convert.ToString(DataMonth4.Select(e => e.ScheduledOn).Count());

                                // More Than 30 Days
                                var DataMonth5 = (from data in FilteredIData
                                                  where data.UserID == item
                                                  && data.closeDate >= data.ScheduledOn.AddDays(30)
                                                  && data.closeDate <= data.ScheduledOn.AddDays(365)
                                                  && !ICnotinstatusid.Contains(data.ComplianceStatusID)
                                                  select data).ToList();
                                MoreThanThirtyDays = Convert.ToString(DataMonth5.Select(e => e.ScheduledOn).Count());

                               
                                #endregion

                                Role = GetRole(item);
                                var UserLocation = (from data in entities.Users
                                                    join row in entities.CustomerBranches
                                                    on data.CustomerBranchID equals row.ID
                                                    where data.ID == item
                                                    select row.Name).FirstOrDefault();
                                dtAfterDueDateIC.Rows.Add(Count, item, UserName, UserLocation, Role, BeforeDays, ThreeDays, SevenDays, FifteenDays, ThirtyDays, MoreThanThirtyDays);
                            }
                        }
                    }

                    #endregion

                    #region Open Compliance
                    if (user_Roles.Contains("IMPT"))
                    {
                        List<int> Openstatus = new List<int>();
                        Openstatus.Add(1);
                        Openstatus.Add(6);
                        Openstatus.Add(8);
                        Openstatus.Add(10);                        
                        Openstatus.Add(12);
                        Openstatus.Add(13);
                        Openstatus.Add(14);                        
                        entities.Database.CommandTimeout = 180;
                        var FilteredData = Master_SP_UsageReports;
                        if (Branchlist.Count > 0)
                        {
                            FilteredData = FilteredData.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                        }
                        FilteredData = FilteredData.Where(entry => entry.RoleID == 3).ToList();
                        UserList = FilteredData.Select(e => e.UserID).Distinct().ToList();
                        int Count = 0;
                        int RowCount1 = 0;
                        if (FilteredData.Count > 0)
                        {
                            foreach (var item in UserList)
                            {
                                string Location = string.Empty;
                                string UserName = string.Empty;
                                string Role = string.Empty;
                                List<string> PerformerMonthList = new List<string>();
                                Count++;
                                entities.Database.CommandTimeout = 180;
                                var objData = (from row in FilteredData
                                               join row1 in entities.Users
                                               on row.UserID equals row1.ID
                                               join role in entities.Roles
                                               on row.RoleID equals role.ID
                                               join cust in entities.Customers
                                               on row.CustomerID equals cust.ID
                                               where row.UserID == item
                                               select new UsageReport_Data()
                                               {
                                                   CustomerName = cust.Name,
                                                   UserName = row1.FirstName + " " + row1.LastName,
                                                   Role = role.Name
                                               }).FirstOrDefault();

                                if (objData != null)
                                {
                                    UserName = objData.UserName;
                                }


                                #region 12 month Data Assing
                                for (int i = 0; i < NoOfMonth; i++)
                                {
                                    DateTime now = fromDatePeroid.AddMonths(i);
                                    DateTime startDate = new DateTime(now.Year, now.Month, 1);
                                    DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                                    var DataMonth1 = (from data in FilteredData
                                                      where data.UserID == item
                                                      && data.ScheduledOn >= startDate
                                                      && data.ScheduledOn <= endDate
                                                      select data).ToList();
                                    var MonthOpenCount = DataMonth1.Where(e => Openstatus.Contains(e.ComplianceStatusID)).Count();
                                    PerformerMonthList.Add(Convert.ToString(MonthOpenCount));
                                }
                                #endregion

                                Role = GetRole(item);
                                var UserLocation = (from data in entities.Users
                                                    join row in entities.CustomerBranches
                                                    on data.CustomerBranchID equals row.ID
                                                    where data.ID == item
                                                    select row.Name).FirstOrDefault();
                                dtLogingDatailOpen.Rows.Add(Count, item, UserName, UserLocation, Role);

                                int p = 0;
                                for (int j = 5; j < dtCompleteCompliance.Columns.Count; j++)
                                {
                                    dtLogingDatailOpen.Rows[RowCount1][j] = PerformerMonthList[p];
                                    p++;
                                }
                                RowCount1++;
                            }
                        }
                    }
                    #endregion

                    #region Open Internal Compliance
                    if (user_Roles.Contains("IMPT"))
                    {
                        List<int> Openstatus = new List<int>();
                        Openstatus.Add(1);
                        Openstatus.Add(6);
                        Openstatus.Add(8);
                        Openstatus.Add(10);
                        Openstatus.Add(12);
                        Openstatus.Add(13);
                        Openstatus.Add(14);
                        entities.Database.CommandTimeout = 180;
                        var FilteredData = Master_SP_InternalUsageReports;
                        if (Branchlist.Count > 0)
                        {
                            FilteredData = FilteredData.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                        }
                        FilteredData = FilteredData.Where(entry => entry.RoleID == 3).ToList();
                        ICUserList = FilteredData.Select(e => e.UserID).Distinct().ToList();
                        int Count = 0;
                        int RowCount1 = 0;
                        if (FilteredData.Count > 0)
                        {
                            foreach (var item in ICUserList)
                            {
                                string Location = string.Empty;
                                string UserName = string.Empty;
                                string Role = string.Empty;
                                List<string> PerformerMonthList = new List<string>();
                                Count++;
                                entities.Database.CommandTimeout = 180;
                                var objData = (from row in FilteredData
                                               join row1 in entities.Users
                                               on row.UserID equals row1.ID
                                               join role in entities.Roles
                                               on row.RoleID equals role.ID
                                               join cust in entities.Customers
                                               on row.CustomerID equals cust.ID
                                               where row.UserID == item
                                               select new UsageReport_Data()
                                               {
                                                   CustomerName = cust.Name,
                                                   UserName = row1.FirstName + " " + row1.LastName,
                                                   Role = role.Name
                                               }).FirstOrDefault();

                                if (objData != null)
                                {
                                    UserName = objData.UserName;
                                }


                                #region 12 month Data Assing
                                for (int i = 0; i < NoOfMonth; i++)
                                {
                                    DateTime now = fromDatePeroid.AddMonths(i);
                                    DateTime startDate = new DateTime(now.Year, now.Month, 1);
                                    DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                                    var DataMonth1 = (from data in FilteredData
                                                      where data.UserID == item
                                                      && data.ScheduledOn >= startDate
                                                      && data.ScheduledOn <= endDate
                                                      select data).ToList();
                                    var MonthOpenCount = DataMonth1.Where(e => Openstatus.Contains(e.ComplianceStatusID)).Count();
                                    PerformerMonthList.Add(Convert.ToString(MonthOpenCount));
                                }
                                #endregion

                                Role = GetRole(item);
                                var UserLocation = (from data in entities.Users
                                                    join row in entities.CustomerBranches
                                                    on data.CustomerBranchID equals row.ID
                                                    where data.ID == item
                                                    select row.Name).FirstOrDefault();
                                dtLogingDatailOpenIC.Rows.Add(Count, item, UserName, UserLocation, Role);

                                int p = 0;
                                for (int j = 5; j < dtCompleteICompliance.Columns.Count; j++)
                                {
                                    dtLogingDatailOpenIC.Rows[RowCount1][j] = PerformerMonthList[p];
                                    p++;
                                }
                                RowCount1++;
                            }
                        }
                    }
                    #endregion

                    #region Reviewer Compliance Complete
                    if (user_Roles.Contains("IMPT"))
                    {
                        List<int> StatusList1 = new List<int>();
                        StatusList1.Add(2);
                        StatusList1.Add(3);
                        StatusList1.Add(4);
                        StatusList1.Add(5);
                        StatusList1.Add(11);
                        List<int> StatusListClose = new List<int>();
                        StatusListClose.Add(4);
                        StatusListClose.Add(5);
                        StatusListClose.Add(11);
                        entities.Database.CommandTimeout = 180;
                        var FilteredData = Master_SP_UsageReports;
                        if (Branchlist.Count > 0)
                        {
                            FilteredData = FilteredData.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                        }
                        FilteredData = FilteredData.Where(entry => entry.RoleID == 4).ToList();
                        //var ReviewerData = FilteredData.Where(entry => entry.RoleID == 4).ToList();
                        UserList = FilteredData.Select(e => e.UserID).Distinct().ToList();
                        int Count = 0;
                        int RowCount1 = 0;
                        if (FilteredData.Count > 0)
                        {
                            foreach (var item in UserList)
                            {
                                string Location = string.Empty;
                                string UserName = string.Empty;
                                string Role = string.Empty;
                                List<string> ReviewerMonthList = new List<string>();
                                Count++;
                                entities.Database.CommandTimeout = 180;
                                var objData = (from row in FilteredData
                                               join row1 in entities.Users
                                               on row.UserID equals row1.ID
                                               join role in entities.Roles
                                               on row.RoleID equals role.ID
                                               join cust in entities.Customers
                                               on row.CustomerID equals cust.ID
                                               where row.UserID == item
                                               select new UsageReport_Data()
                                               {
                                                   CustomerName = cust.Name,
                                                   UserName = row1.FirstName + " " + row1.LastName,
                                                   Role = role.Name
                                               }).FirstOrDefault();

                                if (objData != null)
                                {
                                    UserName = objData.UserName;
                                }

                                #region 12 month Data Assing
                                for (int i = 0; i < NoOfMonth; i++)
                                {
                                    DateTime now = fromDatePeroid.AddMonths(i);
                                    DateTime startDate = new DateTime(now.Year, now.Month, 1);
                                    DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                                    var DataMonth1 = (from data in FilteredData
                                                      where
                                                      data.UserID == item
                                                      && data.ScheduledOn >= startDate
                                                      && data.ScheduledOn <= endDate
                                                      //&& data.closeDate >= startDate
                                                      //&& data.closeDate <= endDate
                                                      select data).ToList();

                                    //var ScheduleOnIDList = DataMonth1.Select(e => e.ScheduledOnID).Distinct().ToList();

                                    //var PerformerData = (from data in FilteredData
                                    //                     where
                                    //                     data.RoleID == 3
                                    //                     && ScheduleOnIDList.Contains(data.ScheduledOnID)
                                    //                     && data.closeDate >= startDate
                                    //                     && data.closeDate <= endDate
                                    //                     select data).ToList();

                                    var Mont1hToltal = DataMonth1.Where(e => StatusList1.Contains(e.ComplianceStatusID)).Count();
                                    var Month1ActualData = DataMonth1.Where(e => StatusListClose.Contains(e.ComplianceStatusID)).Count();
                                    ReviewerMonthList.Add(Month1ActualData + "/" + Mont1hToltal);
                                }
                                #endregion

                                Role = GetRole(item);
                                var UserLocation = (from data in entities.Users
                                                    join row in entities.CustomerBranches
                                                    on data.CustomerBranchID equals row.ID
                                                    where data.ID == item
                                                    select row.Name).FirstOrDefault();
                                dtReviewerData.Rows.Add(Count, item, UserName, UserLocation, Role);
                                int p = 0;
                                for (int j = 5; j < dtReviewerData.Columns.Count; j++)
                                {
                                    dtReviewerData.Rows[RowCount1][j] = ReviewerMonthList[p];
                                    p++;
                                }
                                RowCount1++;
                            }
                        }
                    }
                    #endregion

                    #region Internal Compliance Reviewer Compliance Complete
                    if (user_Roles.Contains("IMPT"))
                    {
                        List<int> StatusList1 = new List<int>();
                        StatusList1.Add(2);
                        StatusList1.Add(3);
                        StatusList1.Add(4);
                        StatusList1.Add(5);
                        StatusList1.Add(11);
                        List<int> StatusListClose = new List<int>();
                        StatusListClose.Add(4);
                        StatusListClose.Add(5);
                        StatusListClose.Add(11);
                        entities.Database.CommandTimeout = 180;
                        var FilteredData = Master_SP_InternalUsageReports;
                        if (Branchlist.Count > 0)
                        {
                            FilteredData = FilteredData.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                        }
                        FilteredData = FilteredData.Where(entry => entry.RoleID == 4).ToList();
                        //var ReviewerData = FilteredData.Where(entry => entry.RoleID == 4).ToList();
                        ICUserList = FilteredData.Select(e => e.UserID).Distinct().ToList();
                        int Count = 0;
                        int RowCount1 = 0;
                        if (FilteredData.Count > 0)
                        {
                            foreach (var item in ICUserList)
                            {
                                string Location = string.Empty;
                                string UserName = string.Empty;
                                string Role = string.Empty;
                                List<string> ReviewerMonthList = new List<string>();
                                Count++;
                                entities.Database.CommandTimeout = 180;
                                var objData = (from row in FilteredData
                                               join row1 in entities.Users
                                               on row.UserID equals row1.ID
                                               join role in entities.Roles
                                               on row.RoleID equals role.ID
                                               join cust in entities.Customers
                                               on row.CustomerID equals cust.ID
                                               where row.UserID == item
                                               select new UsageReport_Data()
                                               {
                                                   CustomerName = cust.Name,
                                                   UserName = row1.FirstName + " " + row1.LastName,
                                                   Role = role.Name
                                               }).FirstOrDefault();

                                if (objData != null)
                                {
                                    UserName = objData.UserName;
                                }

                                #region 12 month Data Assing
                                for (int i = 0; i < NoOfMonth; i++)
                                {
                                    DateTime now = fromDatePeroid.AddMonths(i);
                                    DateTime startDate = new DateTime(now.Year, now.Month, 1);
                                    DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                                    var DataMonth1 = (from data in FilteredData
                                                      where
                                                      data.UserID == item
                                                      && data.ScheduledOn >= startDate
                                                      && data.ScheduledOn <= endDate
                                                      //&& data.closeDate >= startDate
                                                      //&& data.closeDate <= endDate
                                                      select data).ToList();

                                    var Mont1hToltal = DataMonth1.Where(e => StatusList1.Contains(e.ComplianceStatusID)).Count();
                                    var Month1ActualData = DataMonth1.Where(e => StatusListClose.Contains(e.ComplianceStatusID)).Count();
                                    ReviewerMonthList.Add(Month1ActualData + "/" + Mont1hToltal);
                                }
                                #endregion

                                Role = GetRole(item);
                                var UserLocation = (from data in entities.Users
                                                    join row in entities.CustomerBranches
                                                    on data.CustomerBranchID equals row.ID
                                                    where data.ID == item
                                                    select row.Name).FirstOrDefault();
                                dtReviewerDataIC.Rows.Add(Count, item, UserName, UserLocation, Role);
                                int p = 0;
                                for (int j = 5; j < dtReviewerDataIC.Columns.Count; j++)
                                {
                                    dtReviewerDataIC.Rows[RowCount1][j] = ReviewerMonthList[p];
                                    p++;
                                }
                                RowCount1++;
                            }
                        }
                    }
                    #endregion
                }


                #region Loging Details
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (user_Roles.Contains("IMPT"))
                    {
                        int Count = 0;
                        entities.Database.CommandTimeout = 180;
                        var FilteredData = (from row in entities.Sp_AVACOM_LoginDetailHistory(CustomerID, fromDatePeroid, toDatePeroid)
                                            select row).ToList();

                        var UserList1 = FilteredData.Select(e => e.UserID).Distinct().ToList();
                        int RowCount1 = 0;
                        foreach (var item in UserList1)//UserList
                        {
                            string Location = string.Empty;
                            string UserName = string.Empty;
                            string Role = string.Empty;
                            List<string> LogingMonthList = new List<string>();
                            Count++;
                            #region 12 month Data Assing
                            for (int i = 0; i < NoOfMonth; i++)
                            {
                                DateTime now = fromDatePeroid.AddMonths(i);
                                DateTime startDate = new DateTime(now.Year, now.Month, 1);
                                DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                                var DataMonth1 = (from data in FilteredData
                                                  where data.UserID == item
                                                  && data.LastLoginDate >= startDate
                                                  && data.LastLoginDate <= endDate
                                                  select data).ToList();
                                var Mont1hToltal = DataMonth1.Select(e => e.LastLoginDate).Count();
                                LogingMonthList.Add(Convert.ToString(Mont1hToltal));
                            }
                            #endregion

                            Role = GetRole(item);
                            var UserLocation = (from data in entities.Users
                                                join row in entities.CustomerBranches
                                                on data.CustomerBranchID equals row.ID
                                                where data.ID == item
                                                select row.Name).FirstOrDefault();

                            var Userlist = FilteredData.Where(e => e.UserID == item).FirstOrDefault();
                            if (Userlist != null)
                            {
                                UserName = Userlist.UserName;
                            }
                            if (string.IsNullOrEmpty(Role))
                            {
                                using (ComplianceDBEntities entities1 = new ComplianceDBEntities())
                                {
                                    Role = (from row in entities1.Users
                                            join row1 in entities1.Roles
                                            on row.RoleID equals row1.ID
                                            where row.ID == item
                                            select row1.Name).FirstOrDefault();
                                }
                                if (Role == "Company Admin" || Role == "Management")
                                {
                                }
                                else
                                {
                                    Role = string.Empty;
                                }
                            }

                            dtLogingDatail.Rows.Add(Count, item, UserName, UserLocation, Role);

                            int p = 0;
                            for (int j = 5; j < dtLogingDatail.Columns.Count; j++)
                            {
                                dtLogingDatail.Rows[RowCount1][j] = Convert.ToString(LogingMonthList[p]);
                                p++;
                            }
                            RowCount1++;
                        }
                    }
                }
                #endregion

                #region Loging Details
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (user_Roles.Contains("IMPT"))
                    {
                        entities.Database.CommandTimeout = 180;
                        var FilteredData = (from row in entities.Sp_AVACOM_LoginDetailHistory_LastLogin(CustomerID)
                                            select row).ToList();                        

                        int Count = 1;
                        foreach (var item in FilteredData)
                        {
                            var Role = GetRole(item.UserID);
                            if (string.IsNullOrEmpty(Role))
                            {
                                using (ComplianceDBEntities entities1 = new ComplianceDBEntities())
                                {
                                    Role = (from row in entities1.Users
                                            join row1 in entities1.Roles
                                            on row.RoleID equals row1.ID
                                            where row.ID == item.UserID
                                            select row1.Name).FirstOrDefault();
                                }
                                if (Role == "Company Admin" || Role == "Management")
                                {
                                }
                                else
                                {
                                    Role = string.Empty;
                                }
                            }
                            dtLastLogingDatails.Rows.Add(Count++, item.UserID, item.UserName, item.Location, Role, item.LastLoginTime);
                        }
                    }
                }
                #endregion

                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    var cname = CustomerManagement.CustomerGetByIDName(CustomerID);

                    if (user_Roles.Contains("IMPT"))
                    {
                        ExcelWorksheet exWorkSheet3 = exportPackge.Workbook.Worksheets.Add("Number of User logins in Month");
                        ExcelWorksheet exWorkSheet4 = exportPackge.Workbook.Worksheets.Add("Last Login Date Report");

                        #region Each Month Loging Details
                        DataView view3 = new System.Data.DataView(dtLogingDatail);
                        DataTable ExcelData3 = null;
                        //ExcelData3 = view3.ToTable("Selected", false, "SLNO", "UserName", "Location", "Role", "Month1", "Month2", "Month3", "Month4", "Month5", "Month6", "Month7", "Month8", "Month9", "Month10", "Month11", "Month12");

                        ExcelData3 = view3.ToTable();

                        exWorkSheet3.Cells["A1:B1"].Merge = true;
                        exWorkSheet3.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet3.Cells["A1"].Value = "Customer Name:";

                        exWorkSheet3.Cells["C1:D1"].Merge = true;
                        exWorkSheet3.Cells["C1"].Value = cname;

                        exWorkSheet3.Cells["A2:B2"].Merge = true;
                        exWorkSheet3.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet3.Cells["A2"].Value = "Report Name:";

                        exWorkSheet3.Cells["C2:D2"].Merge = true;
                        exWorkSheet3.Cells["C2"].Value = "Number of User logins in Month";

                        exWorkSheet3.Cells["A3:B3"].Merge = true;
                        exWorkSheet3.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet3.Cells["A3"].Value = "Report Generated On:";

                        exWorkSheet3.Cells["C3:D3"].Merge = true;
                        exWorkSheet3.Cells["C3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                        if (dtLogingDatail.Rows.Count > 0)
                        {
                            exWorkSheet3.Cells["A6"].LoadFromDataTable(ExcelData3, false);
                        }
                        exWorkSheet3.Cells["A5"].Value = "SrNo";
                        exWorkSheet3.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet3.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet3.Cells["A5"].AutoFitColumns(10);
                        exWorkSheet3.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet3.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet3.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet3.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet3.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet3.Cells["B5"].Value = "UserID";
                        exWorkSheet3.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet3.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet3.Cells["B5"].AutoFitColumns(15);
                        exWorkSheet3.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet3.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet3.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet3.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet3.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet3.Cells["C5"].Value = "User Name";
                        exWorkSheet3.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet3.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet3.Cells["C5"].AutoFitColumns(25);
                        exWorkSheet3.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet3.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet3.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet3.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet3.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet3.Cells["D5"].Value = "Location";
                        exWorkSheet3.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet3.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet3.Cells["D5"].AutoFitColumns(25);
                        exWorkSheet3.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet3.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet3.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet3.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet3.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet3.Cells["E5"].Value = "Role";
                        exWorkSheet3.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet3.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet3.Cells["E5"].AutoFitColumns(25);
                        exWorkSheet3.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet3.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet3.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet3.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet3.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        int AsciiValue = 70;
                        bool CheckAsciiVal = false;
                        for (int i = 0; i < NoOfMonth; i++)
                        {
                            DateTime CurrentMonth = fromDatePeroid.AddMonths(i);
                            var ColVal = string.Empty;
                            if (NoOfMonth > 21)
                            {
                                if (AsciiValue == 91)
                                {
                                    CheckAsciiVal = true;
                                    AsciiValue = 65;
                                }
                                if (CheckAsciiVal)
                                {
                                    ColVal = "A" + Convert.ToChar(AsciiValue);
                                }
                                else
                                {
                                    ColVal = Convert.ToChar(AsciiValue).ToString();
                                }

                                exWorkSheet3.Cells[ColVal + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                exWorkSheet3.Cells[ColVal + "5"].Style.Font.Bold = true;
                                exWorkSheet3.Cells[ColVal + "5"].Style.Font.Size = 12;
                                exWorkSheet3.Cells[ColVal + "5"].AutoFitColumns(15);
                                exWorkSheet3.Cells[ColVal + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet3.Cells[ColVal + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet3.Cells[ColVal + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet3.Cells[ColVal + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                AsciiValue++;
                            }
                            else
                            {
                                exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                                exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.Font.Bold = true;
                                exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.Font.Size = 12;
                                exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].AutoFitColumns(15);
                                exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet3.Cells[Convert.ToChar(AsciiValue) + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                                AsciiValue++;
                            }
                        }

                        int count3 = Convert.ToInt32(ExcelData3.Rows.Count) + 5;
                        using (ExcelRange col = exWorkSheet3.Cells[5, 1, count3, NoOfMonth + 5])
                        {
                            col.Style.WrapText = true;
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            //col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }
                        using (ExcelRange col = exWorkSheet3.Cells[5, 6, count3, NoOfMonth + 5])
                        {
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }
                        #endregion

                        #region Last Login

                        DataView view4 = new System.Data.DataView(dtLastLogingDatails);
                        DataTable ExcelData4 = null;
                        ExcelData4 = view4.ToTable("Selected", false, "SLNO", "UserID", "UserName", "Location", "Role", "LastLogingTime");

                        foreach (DataRow item in ExcelData4.Rows)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(item["LastLogingTime"])))
                            {
                                item["LastLogingTime"] = Convert.ToDateTime(item["LastLogingTime"]).ToString("dd-MMM-yyyy");
                            }
                        }

                        exWorkSheet4.Cells["A1:B1"].Merge = true;
                        exWorkSheet4.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["A1"].Value = "Customer Name:";

                        exWorkSheet4.Cells["C1:D1"].Merge = true;
                        exWorkSheet4.Cells["C1"].Value = cname;

                        exWorkSheet4.Cells["A2:B2"].Merge = true;
                        exWorkSheet4.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["A2"].Value = "Report Name:";

                        exWorkSheet4.Cells["C2:D2"].Merge = true;
                        exWorkSheet4.Cells["C2"].Value = "Last Login Date Report";

                        exWorkSheet4.Cells["A3:B3"].Merge = true;
                        exWorkSheet4.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["A3"].Value = "Report Generated On:";

                        exWorkSheet4.Cells["C3:D3"].Merge = true;
                        exWorkSheet4.Cells["C3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");


                        if (dtLastLogingDatails.Rows.Count > 0)
                        {
                            exWorkSheet4.Cells["A6"].LoadFromDataTable(ExcelData4, false);
                        }
                        exWorkSheet4.Cells["A5"].Value = "SrNo";
                        exWorkSheet4.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet4.Cells["A5"].AutoFitColumns(10);
                        exWorkSheet4.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet4.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet4.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet4.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet4.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet4.Cells["B5"].Value = "UserID";
                        exWorkSheet4.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet4.Cells["B5"].AutoFitColumns(15);
                        exWorkSheet4.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet4.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet4.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet4.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet4.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet4.Cells["C5"].Value = "User Name";
                        exWorkSheet4.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet4.Cells["C5"].AutoFitColumns(25);
                        exWorkSheet4.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet4.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet4.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet4.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet4.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet4.Cells["D5"].Value = "Location";
                        exWorkSheet4.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet4.Cells["D5"].AutoFitColumns(25);
                        exWorkSheet4.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet4.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet4.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet4.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet4.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        exWorkSheet4.Cells["E5"].Value = "Role";
                        exWorkSheet4.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet4.Cells["E5"].AutoFitColumns(25);
                        exWorkSheet4.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet4.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet4.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet4.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet4.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        exWorkSheet4.Cells["F5"].Value = "Last Login";
                        exWorkSheet4.Cells["F5"].Style.Font.Bold = true;
                        exWorkSheet4.Cells["F5"].Style.Font.Size = 12;
                        exWorkSheet4.Cells["F5"].AutoFitColumns(25);
                        exWorkSheet4.Cells["F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        exWorkSheet4.Cells["F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        exWorkSheet4.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        exWorkSheet4.Cells["F5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                        exWorkSheet4.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        int count4 = Convert.ToInt32(ExcelData4.Rows.Count) + 5;
                        using (ExcelRange col = exWorkSheet4.Cells[5, 1, count4, 6])
                        {
                            col.Style.WrapText = true;

                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            //col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }
                        using (ExcelRange col = exWorkSheet4.Cells[5, 5, count4, 6])
                        {
                            col.Style.Numberformat.Format = "dd-MMM-yyyy";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        }
                        #endregion
                    }
                    ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("SC-Performance Report - In Time (P)");
                    ExcelWorksheet exWorkSheet2 = exportPackge.Workbook.Worksheets.Add("SC-Ageing of System Updation");
                    ExcelWorksheet exWorkSheet6 = exportPackge.Workbook.Worksheets.Add("SC-Performance Report- OverDue (P)");
                    ExcelWorksheet exWorkSheet5 = exportPackge.Workbook.Worksheets.Add("SC-Performance Report (Reviewer)");

                    #region Compliance Due Date Detail of Year
                    DataView view = new System.Data.DataView(dtCompleteCompliance);
                    DataTable ExcelData = null;
                    ExcelData = view.ToTable();
                    exWorkSheet1.Cells["A1:B1"].Merge = true;
                    exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["A1"].Value = "Customer Name:";

                    exWorkSheet1.Cells["C1:D1"].Merge = true;
                    exWorkSheet1.Cells["C1"].Value = cname;

                    exWorkSheet1.Cells["A2:B2"].Merge = true;
                    exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["A2"].Value = "Report Name:";

                    exWorkSheet1.Cells["C2:D2"].Merge = true;
                    exWorkSheet1.Cells["C2"].Value = "User Wise Performance Report (Performers)";

                    exWorkSheet1.Cells["A3:B3"].Merge = true;
                    exWorkSheet1.Cells["A3"].Value = "Note:";
                    exWorkSheet1.Cells["A3"].Style.Font.Bold = true;

                    exWorkSheet1.Cells["C3:G3"].Merge = true;
                    exWorkSheet1.Cells["C3"].Value = "Month Wise Summary of Compliance completed " + " 'In Time'" + " as against compliances due in that Period";

                    exWorkSheet1.Cells["A4:B4"].Merge = true;
                    exWorkSheet1.Cells["A4"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["A4"].Value = "Report Generated On:";

                    exWorkSheet1.Cells["C4:D4"].Merge = true;
                    exWorkSheet1.Cells["C4"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                    if (dtCompleteCompliance.Rows.Count > 0)
                    {
                        exWorkSheet1.Cells["A6"].LoadFromDataTable(ExcelData, false);
                    }
                    exWorkSheet1.Cells["A5"].Value = "SrNo";
                    exWorkSheet1.Cells["A5"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["A5"].Style.Font.Size = 12;
                    exWorkSheet1.Cells["A5"].AutoFitColumns(10);
                    exWorkSheet1.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet1.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    exWorkSheet1.Cells["B5"].Value = "UserID";
                    exWorkSheet1.Cells["B5"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["B5"].Style.Font.Size = 12;
                    exWorkSheet1.Cells["B5"].AutoFitColumns(15);
                    exWorkSheet1.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet1.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    exWorkSheet1.Cells["C5"].Value = "User Name";
                    exWorkSheet1.Cells["C5"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["C5"].Style.Font.Size = 12;
                    exWorkSheet1.Cells["C5"].AutoFitColumns(25);
                    exWorkSheet1.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet1.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                    exWorkSheet1.Cells["D5"].Value = "Location";
                    exWorkSheet1.Cells["D5"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["D5"].Style.Font.Size = 12;
                    exWorkSheet1.Cells["D5"].AutoFitColumns(25);
                    exWorkSheet1.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet1.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                    exWorkSheet1.Cells["E5"].Value = "Role";
                    exWorkSheet1.Cells["E5"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["E5"].Style.Font.Size = 12;
                    exWorkSheet1.Cells["E5"].AutoFitColumns(25);
                    exWorkSheet1.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet1.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet1.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet1.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    int AsciiValue1 = 70;
                    bool CheckAsciiVal1 = false;
                    for (int i = 0; i < NoOfMonth; i++)
                    {
                        DateTime CurrentMonth = fromDatePeroid.AddMonths(i);
                        var ColVal = string.Empty; ;
                        if (NoOfMonth > 23)
                        {
                            if (AsciiValue1 == 91)
                            {
                                CheckAsciiVal1 = true;
                                AsciiValue1 = 65;
                            }
                            if (CheckAsciiVal1)
                            {
                                ColVal = "A" + Convert.ToChar(AsciiValue1);
                            }
                            else
                            {
                                ColVal = Convert.ToChar(AsciiValue1).ToString();
                            }

                            exWorkSheet1.Cells[ColVal + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                            exWorkSheet1.Cells[ColVal + "5"].Style.Font.Bold = true;
                            exWorkSheet1.Cells[ColVal + "5"].Style.Font.Size = 12;
                            exWorkSheet1.Cells[ColVal + "5"].AutoFitColumns(15);
                            exWorkSheet1.Cells[ColVal + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells[ColVal + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells[ColVal + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells[ColVal + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            AsciiValue1++;
                        }
                        else
                        {
                            exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                            exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.Font.Bold = true;
                            exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.Font.Size = 12;
                            exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].AutoFitColumns(15);
                            exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells[Convert.ToChar(AsciiValue1) + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            AsciiValue1++;
                        }
                    }

                    int count1 = Convert.ToInt32(ExcelData.Rows.Count) + 5;
                    using (ExcelRange col = exWorkSheet1.Cells[5, 1, count1, NoOfMonth + 5])
                    {
                        col.Style.WrapText = true;
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        //col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }
                    using (ExcelRange col = exWorkSheet1.Cells[5, 6, count1, NoOfMonth + 5])
                    {
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }
                    #endregion

                    #region After Due Date Complate

                    DataView view1 = new System.Data.DataView(dtAfterDueDate);
                    DataTable ExcelData1 = null;
                    ExcelData1 = view1.ToTable("Selected", false, "SLNO", "UserID", "UserName", "Location", "Role", "BeforeDays", "ThreeDays", "SevenDays", "FifteenDays", "ThirtyDays", "MoreThanThirtyDays");

                    exWorkSheet2.Cells["A1:B1"].Merge = true;
                    exWorkSheet2.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["A1"].Value = "Customer Name:";

                    exWorkSheet2.Cells["C1:D1"].Merge = true;
                    exWorkSheet2.Cells["C1"].Value = cname;

                    exWorkSheet2.Cells["A2:B2"].Merge = true;
                    exWorkSheet2.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["A2"].Value = "Report Name:";

                    exWorkSheet2.Cells["C2:D2"].Merge = true;
                    exWorkSheet2.Cells["C2"].Value = "Ageing of System Updation (From Due Date)";

                    exWorkSheet2.Cells["A3:B3"].Merge = true;
                    exWorkSheet2.Cells["A3"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["A3"].Value = "Report Generated On:";

                    exWorkSheet2.Cells["C3:D3"].Merge = true;
                    exWorkSheet2.Cells["C3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                    if (dtAfterDueDate.Rows.Count > 0)
                    {
                        exWorkSheet2.Cells["A6"].LoadFromDataTable(ExcelData1, false);
                    }
                    exWorkSheet2.Cells["A5"].Value = "SrNo";
                    exWorkSheet2.Cells["A5"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["A5"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["A5"].AutoFitColumns(10);
                    exWorkSheet2.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet2.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    exWorkSheet2.Cells["B5"].Value = "UserID";
                    exWorkSheet2.Cells["B5"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["B5"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["B5"].AutoFitColumns(15);
                    exWorkSheet2.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet2.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    exWorkSheet2.Cells["C5"].Value = "User Name";
                    exWorkSheet2.Cells["C5"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["C5"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["C5"].AutoFitColumns(25);
                    exWorkSheet2.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet2.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                    exWorkSheet2.Cells["D5"].Value = "Location";
                    exWorkSheet2.Cells["D5"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["D5"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["D5"].AutoFitColumns(25);
                    exWorkSheet2.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet2.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                    exWorkSheet2.Cells["E5"].Value = "Role";
                    exWorkSheet2.Cells["E5"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["E5"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["E5"].AutoFitColumns(25);
                    exWorkSheet2.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet2.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    exWorkSheet2.Cells["F5"].Value = "On Or Before Due Date";
                    exWorkSheet2.Cells["F5"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["F5"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["F5"].AutoFitColumns(25);
                    exWorkSheet2.Cells["F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["F5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet2.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    exWorkSheet2.Cells["G5"].Value = "1 - 3 Days";
                    exWorkSheet2.Cells["G5"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["G5"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["G5"].AutoFitColumns(25);
                    exWorkSheet2.Cells["G5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["G5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["G5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet2.Column(7).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    exWorkSheet2.Cells["H5"].Value = "4 - 7 Days";
                    exWorkSheet2.Cells["H5"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["H5"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["H5"].AutoFitColumns(25);
                    exWorkSheet2.Cells["H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["H5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet2.Column(8).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    exWorkSheet2.Cells["I5"].Value = "8 - 15 Days";
                    exWorkSheet2.Cells["I5"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["I5"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["I5"].AutoFitColumns(25);
                    exWorkSheet2.Cells["I5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["I5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["I5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet2.Column(9).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    exWorkSheet2.Cells["J5"].Value = "16 - 30 Days";
                    exWorkSheet2.Cells["J5"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["J5"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["J5"].AutoFitColumns(25);
                    exWorkSheet2.Cells["J5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["J5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["J5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet2.Column(10).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    exWorkSheet2.Cells["K5"].Value = "> 30 days";
                    exWorkSheet2.Cells["K5"].Style.Font.Bold = true;
                    exWorkSheet2.Cells["K5"].Style.Font.Size = 12;
                    exWorkSheet2.Cells["K5"].AutoFitColumns(25);
                    exWorkSheet2.Cells["K5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet2.Cells["K5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet2.Cells["K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet2.Cells["K5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet2.Column(11).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    int count2 = Convert.ToInt32(ExcelData1.Rows.Count) + 5;
                    using (ExcelRange col = exWorkSheet2.Cells[5, 1, count2, 11])
                    {
                        col.Style.WrapText = true;

                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    }
                    #endregion

                    #region Open/Overdue Compliance
                    DataView view6 = new System.Data.DataView(dtLogingDatailOpen);
                    DataTable ExcelData6 = null;
                    ExcelData6 = view6.ToTable();
                    exWorkSheet6.Cells["A1:B1"].Merge = true;
                    exWorkSheet6.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet6.Cells["A1"].Value = "Customer Name:";

                    exWorkSheet6.Cells["C1:D1"].Merge = true;
                    exWorkSheet6.Cells["C1"].Value = cname;

                    exWorkSheet6.Cells["A2:B2"].Merge = true;
                    exWorkSheet6.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet6.Cells["A2"].Value = "Report Name:";

                    exWorkSheet6.Cells["C2:D2"].Merge = true;
                    exWorkSheet6.Cells["C2"].Value = "User Wise Performance Report (Performers)";

                    exWorkSheet6.Cells["A3:B3"].Merge = true;
                    exWorkSheet6.Cells["A3"].Value = "Note:";
                    exWorkSheet6.Cells["A3"].Style.Font.Bold = true;

                    exWorkSheet6.Cells["C3:G3"].Merge = true;
                    exWorkSheet6.Cells["C3"].Value = "Month Wise  Summary of " + " 'Over due Compliance'" + " as against compliances due in that Period";

                    exWorkSheet6.Cells["A4:B4"].Merge = true;
                    exWorkSheet6.Cells["A4"].Style.Font.Bold = true;
                    exWorkSheet6.Cells["A4"].Value = "Report Generated On:";

                    exWorkSheet6.Cells["C4:D4"].Merge = true;
                    exWorkSheet6.Cells["C4"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                    if (dtLogingDatailOpen.Rows.Count > 0)
                    {
                        exWorkSheet6.Cells["A6"].LoadFromDataTable(ExcelData6, false);
                    }
                    exWorkSheet6.Cells["A5"].Value = "SrNo";
                    exWorkSheet6.Cells["A5"].Style.Font.Bold = true;
                    exWorkSheet6.Cells["A5"].Style.Font.Size = 12;
                    exWorkSheet6.Cells["A5"].AutoFitColumns(10);
                    exWorkSheet6.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet6.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet6.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet6.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet6.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    exWorkSheet6.Cells["B5"].Value = "UserID";
                    exWorkSheet6.Cells["B5"].Style.Font.Bold = true;
                    exWorkSheet6.Cells["B5"].Style.Font.Size = 12;
                    exWorkSheet6.Cells["B5"].AutoFitColumns(15);
                    exWorkSheet6.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet6.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet6.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet6.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet6.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    exWorkSheet6.Cells["C5"].Value = "User Name";
                    exWorkSheet6.Cells["C5"].Style.Font.Bold = true;
                    exWorkSheet6.Cells["C5"].Style.Font.Size = 12;
                    exWorkSheet6.Cells["C5"].AutoFitColumns(25);
                    exWorkSheet6.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet6.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet6.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet6.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet6.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                    exWorkSheet6.Cells["D5"].Value = "Location";
                    exWorkSheet6.Cells["D5"].Style.Font.Bold = true;
                    exWorkSheet6.Cells["D5"].Style.Font.Size = 12;
                    exWorkSheet6.Cells["D5"].AutoFitColumns(25);
                    exWorkSheet6.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet6.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet6.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet6.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet6.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                    exWorkSheet6.Cells["E5"].Value = "Role";
                    exWorkSheet6.Cells["E5"].Style.Font.Bold = true;
                    exWorkSheet6.Cells["E5"].Style.Font.Size = 12;
                    exWorkSheet6.Cells["E5"].AutoFitColumns(25);
                    exWorkSheet6.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet6.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet6.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet6.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet6.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    int AsciiValue6 = 70;
                    bool CheckAsciiVal6 = false;
                    for (int i = 0; i < NoOfMonth; i++)
                    {
                        DateTime CurrentMonth = fromDatePeroid.AddMonths(i);
                        var ColVal = string.Empty; ;
                        if (NoOfMonth > 23)
                        {
                            if (AsciiValue6 == 91)
                            {
                                CheckAsciiVal6 = true;
                                AsciiValue6 = 65;
                            }
                            if (CheckAsciiVal6)
                            {
                                ColVal = "A" + Convert.ToChar(AsciiValue6);
                            }
                            else
                            {
                                ColVal = Convert.ToChar(AsciiValue6).ToString();
                            }

                            exWorkSheet6.Cells[ColVal + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                            exWorkSheet6.Cells[ColVal + "5"].Style.Font.Bold = true;
                            exWorkSheet6.Cells[ColVal + "5"].Style.Font.Size = 12;
                            exWorkSheet6.Cells[ColVal + "5"].AutoFitColumns(15);
                            exWorkSheet6.Cells[ColVal + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet6.Cells[ColVal + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet6.Cells[ColVal + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet6.Cells[ColVal + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            AsciiValue6++;
                        }
                        else
                        {
                            exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                            exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.Font.Bold = true;
                            exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.Font.Size = 12;
                            exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].AutoFitColumns(15);
                            exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet6.Cells[Convert.ToChar(AsciiValue6) + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            AsciiValue6++;
                        }
                    }

                    int count6 = Convert.ToInt32(ExcelData6.Rows.Count) + 5;
                    using (ExcelRange col = exWorkSheet6.Cells[5, 1, count6, NoOfMonth + 5])
                    {
                        col.Style.WrapText = true;
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        //col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }

                    using (ExcelRange col = exWorkSheet6.Cells[5, 6, count6, NoOfMonth + 5])
                    {
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }
                    #endregion

                    #region Compliance Due Date Detail of Year Reviewer
                    DataView view5 = new System.Data.DataView(dtReviewerData);
                    DataTable ExcelData5 = null;
                    ExcelData5 = view5.ToTable();

                    exWorkSheet5.Cells["A1:B1"].Merge = true;
                    exWorkSheet5.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet5.Cells["A1"].Value = "Customer Name:";

                    exWorkSheet5.Cells["C1:D1"].Merge = true;
                    exWorkSheet5.Cells["C1"].Value = cname;

                    exWorkSheet5.Cells["A2:B2"].Merge = true;
                    exWorkSheet5.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet5.Cells["A2"].Value = "Report Name:";

                    exWorkSheet5.Cells["C2:D2"].Merge = true;
                    exWorkSheet5.Cells["C2"].Value = "User Wise Performance Report (Reviewer)";

                    exWorkSheet5.Cells["A3:B3"].Merge = true;
                    exWorkSheet5.Cells["A3"].Value = "Note:";
                    exWorkSheet5.Cells["A3"].Style.Font.Bold = true;

                    exWorkSheet5.Cells["C3:G3"].Merge = true;
                    exWorkSheet5.Cells["C3"].Value = "Month Wise Summary of Compliance reviewed as against compliances Submitted in that Period";

                    exWorkSheet5.Cells["A4:B4"].Merge = true;
                    exWorkSheet5.Cells["A4"].Style.Font.Bold = true;
                    exWorkSheet5.Cells["A4"].Value = "Report Generated On:";

                    exWorkSheet5.Cells["C4:D4"].Merge = true;
                    exWorkSheet5.Cells["C4"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                    if (dtReviewerData.Rows.Count > 0)
                    {
                        exWorkSheet5.Cells["A6"].LoadFromDataTable(ExcelData5, false);
                    }
                    exWorkSheet5.Cells["A5"].Value = "SrNo";
                    exWorkSheet5.Cells["A5"].Style.Font.Bold = true;
                    exWorkSheet5.Cells["A5"].Style.Font.Size = 12;
                    exWorkSheet5.Cells["A5"].AutoFitColumns(10);
                    exWorkSheet5.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet5.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet5.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet5.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet5.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    exWorkSheet5.Cells["B5"].Value = "UserID";
                    exWorkSheet5.Cells["B5"].Style.Font.Bold = true;
                    exWorkSheet5.Cells["B5"].Style.Font.Size = 12;
                    exWorkSheet5.Cells["B5"].AutoFitColumns(15);
                    exWorkSheet5.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet5.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet5.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet5.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet5.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    exWorkSheet5.Cells["C5"].Value = "User Name";
                    exWorkSheet5.Cells["C5"].Style.Font.Bold = true;
                    exWorkSheet5.Cells["C5"].Style.Font.Size = 12;
                    exWorkSheet5.Cells["C5"].AutoFitColumns(25);
                    exWorkSheet5.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet5.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet5.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet5.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet5.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                    exWorkSheet5.Cells["D5"].Value = "Location";
                    exWorkSheet5.Cells["D5"].Style.Font.Bold = true;
                    exWorkSheet5.Cells["D5"].Style.Font.Size = 12;
                    exWorkSheet5.Cells["D5"].AutoFitColumns(25);
                    exWorkSheet5.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet5.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet5.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet5.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet5.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                    exWorkSheet5.Cells["E5"].Value = "Role";
                    exWorkSheet5.Cells["E5"].Style.Font.Bold = true;
                    exWorkSheet5.Cells["E5"].Style.Font.Size = 12;
                    exWorkSheet5.Cells["E5"].AutoFitColumns(25);
                    exWorkSheet5.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet5.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet5.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet5.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet5.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    int AsciiValue2 = 70;
                    bool CheckAsciiVal2 = false;
                    for (int i = 0; i < NoOfMonth; i++)
                    {
                        DateTime CurrentMonth = fromDatePeroid.AddMonths(i);
                        var ColVal = string.Empty; ;
                        if (NoOfMonth > 23)
                        {
                            if (AsciiValue2 == 91)
                            {
                                CheckAsciiVal2 = true;
                                AsciiValue2 = 65;
                            }
                            if (CheckAsciiVal2)
                            {
                                ColVal = "A" + Convert.ToChar(AsciiValue2);
                            }
                            else
                            {
                                ColVal = Convert.ToChar(AsciiValue2).ToString();
                            }

                            exWorkSheet5.Cells[ColVal + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                            exWorkSheet5.Cells[ColVal + "5"].Style.Font.Bold = true;
                            exWorkSheet5.Cells[ColVal + "5"].Style.Font.Size = 12;
                            exWorkSheet5.Cells[ColVal + "5"].AutoFitColumns(15);
                            exWorkSheet5.Cells[ColVal + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet5.Cells[ColVal + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet5.Cells[ColVal + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet5.Cells[ColVal + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            AsciiValue2++;
                        }
                        else
                        {
                            exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                            exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.Font.Bold = true;
                            exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.Font.Size = 12;
                            exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].AutoFitColumns(15);
                            exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet5.Cells[Convert.ToChar(AsciiValue2) + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            AsciiValue2++;
                        }
                    }

                    int count5 = Convert.ToInt32(ExcelData5.Rows.Count) + 5;
                    using (ExcelRange col = exWorkSheet5.Cells[5, 1, count5, NoOfMonth + 5])
                    {
                        col.Style.WrapText = true;
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        //col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }

                    using (ExcelRange col = exWorkSheet5.Cells[5, 6, count5, NoOfMonth + 5])
                    {
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }
                    #endregion

                    

                    ExcelWorksheet exWorkSheet7 = exportPackge.Workbook.Worksheets.Add("IC-Performance Report - In Time (P)");
                    ExcelWorksheet exWorkSheet8 = exportPackge.Workbook.Worksheets.Add("IC-Ageing of System Updation");
                    ExcelWorksheet exWorkSheet10 = exportPackge.Workbook.Worksheets.Add("IC-Performance Report- OverDue (P)");
                    ExcelWorksheet exWorkSheet9 = exportPackge.Workbook.Worksheets.Add("IC-Performance Report (Reviewer)");

                    #region Internal Compliance

                    #region Internal Compliance Due Date Detail of Year
                    DataView view7 = new System.Data.DataView(dtCompleteICompliance);
                    DataTable ExcelData7 = null;
                    ExcelData7 = view7.ToTable();
                    exWorkSheet7.Cells["A1:B1"].Merge = true;
                    exWorkSheet7.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet7.Cells["A1"].Value = "Customer Name:";
                               
                    exWorkSheet7.Cells["C1:D1"].Merge = true;
                    exWorkSheet7.Cells["C1"].Value = cname;
                               
                    exWorkSheet7.Cells["A2:B2"].Merge = true;
                    exWorkSheet7.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet7.Cells["A2"].Value = "Report Name:";
                               
                    exWorkSheet7.Cells["C2:D2"].Merge = true;
                    exWorkSheet7.Cells["C2"].Value = "Internal Compliance - User Wise Performance Report (Performers)";
                               
                    exWorkSheet7.Cells["A3:B3"].Merge = true;
                    exWorkSheet7.Cells["A3"].Value = "Note:";
                    exWorkSheet7.Cells["A3"].Style.Font.Bold = true;
                               
                    exWorkSheet7.Cells["C3:G3"].Merge = true;
                    exWorkSheet7.Cells["C3"].Value = "Month Wise Summary of Compliance completed " + " 'In Time'" + " as against compliances due in that Period";
                               
                    exWorkSheet7.Cells["A4:B4"].Merge = true;
                    exWorkSheet7.Cells["A4"].Style.Font.Bold = true;
                    exWorkSheet7.Cells["A4"].Value = "Report Generated On:";
                               
                    exWorkSheet7.Cells["C4:D4"].Merge = true;
                    exWorkSheet7.Cells["C4"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                    if (dtCompleteICompliance.Rows.Count > 0)
                    {
                        exWorkSheet7.Cells["A6"].LoadFromDataTable(ExcelData7, false);
                    }
                    exWorkSheet7.Cells["A5"].Value = "SrNo";
                    exWorkSheet7.Cells["A5"].Style.Font.Bold = true;
                    exWorkSheet7.Cells["A5"].Style.Font.Size = 12;
                    exWorkSheet7.Cells["A5"].AutoFitColumns(10);
                    exWorkSheet7.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet7.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet7.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet7.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet7.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                               
                    exWorkSheet7.Cells["B5"].Value = "UserID";
                    exWorkSheet7.Cells["B5"].Style.Font.Bold = true;
                    exWorkSheet7.Cells["B5"].Style.Font.Size = 12;
                    exWorkSheet7.Cells["B5"].AutoFitColumns(15);
                    exWorkSheet7.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet7.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet7.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet7.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet7.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                               
                    exWorkSheet7.Cells["C5"].Value = "User Name";
                    exWorkSheet7.Cells["C5"].Style.Font.Bold = true;
                    exWorkSheet7.Cells["C5"].Style.Font.Size = 12;
                    exWorkSheet7.Cells["C5"].AutoFitColumns(25);
                    exWorkSheet7.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet7.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet7.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet7.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet7.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                               
                    exWorkSheet7.Cells["D5"].Value = "Location";
                    exWorkSheet7.Cells["D5"].Style.Font.Bold = true;
                    exWorkSheet7.Cells["D5"].Style.Font.Size = 12;
                    exWorkSheet7.Cells["D5"].AutoFitColumns(25);
                    exWorkSheet7.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet7.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet7.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet7.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet7.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                               
                    exWorkSheet7.Cells["E5"].Value = "Role";
                    exWorkSheet7.Cells["E5"].Style.Font.Bold = true;
                    exWorkSheet7.Cells["E5"].Style.Font.Size = 12;
                    exWorkSheet7.Cells["E5"].AutoFitColumns(25);
                    exWorkSheet7.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet7.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet7.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet7.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet7.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    int AsciiValue7 = 70;
                    bool CheckAsciiVal7 = false;
                    for (int i = 0; i < NoOfMonth; i++)
                    {
                        DateTime CurrentMonth = fromDatePeroid.AddMonths(i);
                        var ColVal = string.Empty; ;
                        if (NoOfMonth > 23)
                        {
                            if (AsciiValue7 == 91)
                            {
                                CheckAsciiVal7 = true;
                                AsciiValue7 = 65;
                            }
                            if (CheckAsciiVal7)
                            {
                                ColVal = "A" + Convert.ToChar(AsciiValue7);
                            }
                            else
                            {
                                ColVal = Convert.ToChar(AsciiValue7).ToString();
                            }

                            exWorkSheet7.Cells[ColVal + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                            exWorkSheet7.Cells[ColVal + "5"].Style.Font.Bold = true;
                            exWorkSheet7.Cells[ColVal + "5"].Style.Font.Size = 12;
                            exWorkSheet7.Cells[ColVal + "5"].AutoFitColumns(15);
                            exWorkSheet7.Cells[ColVal + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet7.Cells[ColVal + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet7.Cells[ColVal + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet7.Cells[ColVal + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            AsciiValue7++;
                        }
                        else
                        {
                            exWorkSheet7.Cells[Convert.ToChar(AsciiValue7) + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                            exWorkSheet7.Cells[Convert.ToChar(AsciiValue7) + "5"].Style.Font.Bold = true;
                            exWorkSheet7.Cells[Convert.ToChar(AsciiValue7) + "5"].Style.Font.Size = 12;
                            exWorkSheet7.Cells[Convert.ToChar(AsciiValue7) + "5"].AutoFitColumns(15);
                            exWorkSheet7.Cells[Convert.ToChar(AsciiValue7) + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet7.Cells[Convert.ToChar(AsciiValue7) + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet7.Cells[Convert.ToChar(AsciiValue7) + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet7.Cells[Convert.ToChar(AsciiValue7) + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            AsciiValue7++;
                        }
                    }

                    int count7 = Convert.ToInt32(ExcelData7.Rows.Count) + 5;
                    using (ExcelRange col = exWorkSheet7.Cells[5, 1, count7, NoOfMonth + 5])
                    {
                        col.Style.WrapText = true;
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        //col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }
                    using (ExcelRange col = exWorkSheet7.Cells[5, 6, count7, NoOfMonth + 5])
                    {
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }
                    #endregion

                    #region Internal Compliance After Due Date Complete

                    DataView view8 = new System.Data.DataView(dtAfterDueDateIC);
                    DataTable ExcelData8 = null;
                    ExcelData8 = view8.ToTable("Selected", false, "SLNO", "UserID", "UserName", "Location", "Role", "BeforeDays", "ThreeDays", "SevenDays", "FifteenDays", "ThirtyDays", "MoreThanThirtyDays");

                    exWorkSheet8.Cells["A1:B1"].Merge = true;
                    exWorkSheet8.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet8.Cells["A1"].Value = "Customer Name:";

                    exWorkSheet8.Cells["C1:D1"].Merge = true;
                    exWorkSheet8.Cells["C1"].Value = cname;

                    exWorkSheet8.Cells["A2:B2"].Merge = true;
                    exWorkSheet8.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet8.Cells["A2"].Value = "Report Name:";

                    exWorkSheet8.Cells["C2:D2"].Merge = true;
                    exWorkSheet8.Cells["C2"].Value = "Ageing of System Updation (From Due Date)";

                    exWorkSheet8.Cells["A3:B3"].Merge = true;
                    exWorkSheet8.Cells["A3"].Style.Font.Bold = true;
                    exWorkSheet8.Cells["A3"].Value = "Report Generated On:";

                    exWorkSheet8.Cells["C3:D3"].Merge = true;
                    exWorkSheet8.Cells["C3"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                    if (dtAfterDueDateIC.Rows.Count > 0)
                    {
                        exWorkSheet8.Cells["A6"].LoadFromDataTable(ExcelData8, false);
                    }
                    exWorkSheet8.Cells["A5"].Value = "SrNo";
                    exWorkSheet8.Cells["A5"].Style.Font.Bold = true;
                    exWorkSheet8.Cells["A5"].Style.Font.Size = 12;
                    exWorkSheet8.Cells["A5"].AutoFitColumns(10);
                    exWorkSheet8.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet8.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet8.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet8.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet8.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    exWorkSheet8.Cells["B5"].Value = "UserID";
                    exWorkSheet8.Cells["B5"].Style.Font.Bold = true;
                    exWorkSheet8.Cells["B5"].Style.Font.Size = 12;
                    exWorkSheet8.Cells["B5"].AutoFitColumns(15);
                    exWorkSheet8.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet8.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet8.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet8.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet8.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    exWorkSheet8.Cells["C5"].Value = "User Name";
                    exWorkSheet8.Cells["C5"].Style.Font.Bold = true;
                    exWorkSheet8.Cells["C5"].Style.Font.Size = 12;
                    exWorkSheet8.Cells["C5"].AutoFitColumns(25);
                    exWorkSheet8.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet8.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet8.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet8.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet8.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                    exWorkSheet8.Cells["D5"].Value = "Location";
                    exWorkSheet8.Cells["D5"].Style.Font.Bold = true;
                    exWorkSheet8.Cells["D5"].Style.Font.Size = 12;
                    exWorkSheet8.Cells["D5"].AutoFitColumns(25);
                    exWorkSheet8.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet8.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet8.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet8.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet8.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                               
                    exWorkSheet8.Cells["E5"].Value = "Role";
                    exWorkSheet8.Cells["E5"].Style.Font.Bold = true;
                    exWorkSheet8.Cells["E5"].Style.Font.Size = 12;
                    exWorkSheet8.Cells["E5"].AutoFitColumns(25);
                    exWorkSheet8.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet8.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet8.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet8.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet8.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                               
                    exWorkSheet8.Cells["F5"].Value = "On Or Before Due Date";
                    exWorkSheet8.Cells["F5"].Style.Font.Bold = true;
                    exWorkSheet8.Cells["F5"].Style.Font.Size = 12;
                    exWorkSheet8.Cells["F5"].AutoFitColumns(25);
                    exWorkSheet8.Cells["F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet8.Cells["F5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet8.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet8.Cells["F5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet8.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                               
                    exWorkSheet8.Cells["G5"].Value = "1 - 3 Days";
                    exWorkSheet8.Cells["G5"].Style.Font.Bold = true;
                    exWorkSheet8.Cells["G5"].Style.Font.Size = 12;
                    exWorkSheet8.Cells["G5"].AutoFitColumns(25);
                    exWorkSheet8.Cells["G5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet8.Cells["G5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet8.Cells["G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet8.Cells["G5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet8.Column(7).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                               
                    exWorkSheet8.Cells["H5"].Value = "4 - 7 Days";
                    exWorkSheet8.Cells["H5"].Style.Font.Bold = true;
                    exWorkSheet8.Cells["H5"].Style.Font.Size = 12;
                    exWorkSheet8.Cells["H5"].AutoFitColumns(25);
                    exWorkSheet8.Cells["H5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet8.Cells["H5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet8.Cells["H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet8.Cells["H5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet8.Column(8).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                               
                    exWorkSheet8.Cells["I5"].Value = "8 - 15 Days";
                    exWorkSheet8.Cells["I5"].Style.Font.Bold = true;
                    exWorkSheet8.Cells["I5"].Style.Font.Size = 12;
                    exWorkSheet8.Cells["I5"].AutoFitColumns(25);
                    exWorkSheet8.Cells["I5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet8.Cells["I5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet8.Cells["I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet8.Cells["I5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet8.Column(9).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                               
                    exWorkSheet8.Cells["J5"].Value = "16 - 30 Days";
                    exWorkSheet8.Cells["J5"].Style.Font.Bold = true;
                    exWorkSheet8.Cells["J5"].Style.Font.Size = 12;
                    exWorkSheet8.Cells["J5"].AutoFitColumns(25);
                    exWorkSheet8.Cells["J5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet8.Cells["J5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet8.Cells["J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet8.Cells["J5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet8.Column(10).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                               
                    exWorkSheet8.Cells["K5"].Value = "> 30 days";
                    exWorkSheet8.Cells["K5"].Style.Font.Bold = true;
                    exWorkSheet8.Cells["K5"].Style.Font.Size = 12;
                    exWorkSheet8.Cells["K5"].AutoFitColumns(25);
                    exWorkSheet8.Cells["K5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet8.Cells["K5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet8.Cells["K5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet8.Cells["K5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet8.Column(11).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    int count8 = Convert.ToInt32(ExcelData8.Rows.Count) + 5;
                    using (ExcelRange col = exWorkSheet8.Cells[5, 1, count8, 11])
                    {
                        col.Style.WrapText = true;

                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    }
                    #endregion

                    #region Open/Overdue Internal Compliance
                    DataView view10 = new System.Data.DataView(dtLogingDatailOpenIC);
                    DataTable ExcelData10 = null;
                    ExcelData10 = view10.ToTable();
                    exWorkSheet10.Cells["A1:B1"].Merge = true;
                    exWorkSheet10.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet10.Cells["A1"].Value = "Customer Name:";
                               
                    exWorkSheet10.Cells["C1:D1"].Merge = true;
                    exWorkSheet10.Cells["C1"].Value = cname;
                               
                    exWorkSheet10.Cells["A2:B2"].Merge = true;
                    exWorkSheet10.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet10.Cells["A2"].Value = "Report Name:";
                               
                    exWorkSheet10.Cells["C2:D2"].Merge = true;
                    exWorkSheet10.Cells["C2"].Value = "Internal Compliance - User Wise Performance Report (Performers)";
                               
                    exWorkSheet10.Cells["A3:B3"].Merge = true;
                    exWorkSheet10.Cells["A3"].Value = "Note:";
                    exWorkSheet10.Cells["A3"].Style.Font.Bold = true;
                               
                    exWorkSheet10.Cells["C3:G3"].Merge = true;
                    exWorkSheet10.Cells["C3"].Value = "Month Wise  Summary of " + " 'Over due Compliance'" + " as against compliances due in that Period";
                               
                    exWorkSheet10.Cells["A4:B4"].Merge = true;
                    exWorkSheet10.Cells["A4"].Style.Font.Bold = true;
                    exWorkSheet10.Cells["A4"].Value = "Report Generated On:";
                               
                    exWorkSheet10.Cells["C4:D4"].Merge = true;
                    exWorkSheet10.Cells["C4"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                    if (dtLogingDatailOpenIC.Rows.Count > 0)
                    {
                        exWorkSheet10.Cells["A6"].LoadFromDataTable(ExcelData10, false);
                    }
                    exWorkSheet10.Cells["A5"].Value = "SrNo";
                    exWorkSheet10.Cells["A5"].Style.Font.Bold = true;
                    exWorkSheet10.Cells["A5"].Style.Font.Size = 12;
                    exWorkSheet10.Cells["A5"].AutoFitColumns(10);
                    exWorkSheet10.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet10.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet10.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet10.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet10.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                               
                    exWorkSheet10.Cells["B5"].Value = "UserID";
                    exWorkSheet10.Cells["B5"].Style.Font.Bold = true;
                    exWorkSheet10.Cells["B5"].Style.Font.Size = 12;
                    exWorkSheet10.Cells["B5"].AutoFitColumns(15);
                    exWorkSheet10.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet10.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet10.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet10.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet10.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                               
                    exWorkSheet10.Cells["C5"].Value = "User Name";
                    exWorkSheet10.Cells["C5"].Style.Font.Bold = true;
                    exWorkSheet10.Cells["C5"].Style.Font.Size = 12;
                    exWorkSheet10.Cells["C5"].AutoFitColumns(25);
                    exWorkSheet10.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet10.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet10.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet10.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet10.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                               
                    exWorkSheet10.Cells["D5"].Value = "Location";
                    exWorkSheet10.Cells["D5"].Style.Font.Bold = true;
                    exWorkSheet10.Cells["D5"].Style.Font.Size = 12;
                    exWorkSheet10.Cells["D5"].AutoFitColumns(25);
                    exWorkSheet10.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet10.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet10.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet10.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet10.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                               
                    exWorkSheet10.Cells["E5"].Value = "Role";
                    exWorkSheet10.Cells["E5"].Style.Font.Bold = true;
                    exWorkSheet10.Cells["E5"].Style.Font.Size = 12;
                    exWorkSheet10.Cells["E5"].AutoFitColumns(25);
                    exWorkSheet10.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet10.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet10.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet10.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet10.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    int AsciiValue10 = 70;
                    bool CheckAsciiVal10 = false;
                    for (int i = 0; i < NoOfMonth; i++)
                    {
                        DateTime CurrentMonth = fromDatePeroid.AddMonths(i);
                        var ColVal = string.Empty; ;
                        if (NoOfMonth > 23)
                        {
                            if (AsciiValue10 == 91)
                            {
                                CheckAsciiVal10 = true;
                                AsciiValue10 = 65;
                            }
                            if (CheckAsciiVal10)
                            {
                                ColVal = "A" + Convert.ToChar(AsciiValue10);
                            }
                            else
                            {
                                ColVal = Convert.ToChar(AsciiValue10).ToString();
                            }

                            exWorkSheet10.Cells[ColVal + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                            exWorkSheet10.Cells[ColVal + "5"].Style.Font.Bold = true;
                            exWorkSheet10.Cells[ColVal + "5"].Style.Font.Size = 12;
                            exWorkSheet10.Cells[ColVal + "5"].AutoFitColumns(15);
                            exWorkSheet10.Cells[ColVal + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet10.Cells[ColVal + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet10.Cells[ColVal + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet10.Cells[ColVal + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            AsciiValue10++;
                        }
                        else
                        {
                            exWorkSheet10.Cells[Convert.ToChar(AsciiValue10) + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                            exWorkSheet10.Cells[Convert.ToChar(AsciiValue10) + "5"].Style.Font.Bold = true;
                            exWorkSheet10.Cells[Convert.ToChar(AsciiValue10) + "5"].Style.Font.Size = 12;
                            exWorkSheet10.Cells[Convert.ToChar(AsciiValue10) + "5"].AutoFitColumns(15);
                            exWorkSheet10.Cells[Convert.ToChar(AsciiValue10) + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet10.Cells[Convert.ToChar(AsciiValue10) + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet10.Cells[Convert.ToChar(AsciiValue10) + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet10.Cells[Convert.ToChar(AsciiValue10) + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            AsciiValue10++;
                        }
                    }

                    int count10 = Convert.ToInt32(ExcelData10.Rows.Count) + 5;
                    using (ExcelRange col = exWorkSheet10.Cells[5, 1, count10, NoOfMonth + 5])
                    {
                        col.Style.WrapText = true;
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        //col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }

                    using (ExcelRange col = exWorkSheet10.Cells[5, 6, count10, NoOfMonth + 5])
                    {
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }
                    #endregion

                    #region Internal Compliance Due Date Detail of Year Reviewer
                    DataView view9 = new System.Data.DataView(dtReviewerDataIC);
                    DataTable ExcelData9 = null;
                    ExcelData9 = view9.ToTable();

                    exWorkSheet9.Cells["A1:B1"].Merge = true;
                    exWorkSheet9.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet9.Cells["A1"].Value = "Customer Name:";
                               
                    exWorkSheet9.Cells["C1:D1"].Merge = true;
                    exWorkSheet9.Cells["C1"].Value = cname;
                               
                    exWorkSheet9.Cells["A2:B2"].Merge = true;
                    exWorkSheet9.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet9.Cells["A2"].Value = "Report Name:";
                               
                    exWorkSheet9.Cells["C2:D2"].Merge = true;
                    exWorkSheet9.Cells["C2"].Value = "Internal Compliance - User Wise Performance Report (Reviewer)";
                               
                    exWorkSheet9.Cells["A3:B3"].Merge = true;
                    exWorkSheet9.Cells["A3"].Value = "Note:";
                    exWorkSheet9.Cells["A3"].Style.Font.Bold = true;
                               
                    exWorkSheet9.Cells["C3:G3"].Merge = true;
                    exWorkSheet9.Cells["C3"].Value = "Month Wise Summary of Compliance reviewed as against compliances Submitted in that Period";
                               
                    exWorkSheet9.Cells["A4:B4"].Merge = true;
                    exWorkSheet9.Cells["A4"].Style.Font.Bold = true;
                    exWorkSheet9.Cells["A4"].Value = "Report Generated On:";
                               
                    exWorkSheet9.Cells["C4:D4"].Merge = true;
                    exWorkSheet9.Cells["C4"].Value = DateTime.Today.Date.ToString("dd-MMM-yyyy");

                    if (dtReviewerDataIC.Rows.Count > 0)
                    {
                        exWorkSheet9.Cells["A6"].LoadFromDataTable(ExcelData9, false);
                    }
                    exWorkSheet9.Cells["A5"].Value = "SrNo";
                    exWorkSheet9.Cells["A5"].Style.Font.Bold = true;
                    exWorkSheet9.Cells["A5"].Style.Font.Size = 12;
                    exWorkSheet9.Cells["A5"].AutoFitColumns(10);
                    exWorkSheet9.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet9.Cells["A5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet9.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet9.Cells["A5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet9.Column(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    exWorkSheet9.Cells["B5"].Value = "UserID";
                    exWorkSheet9.Cells["B5"].Style.Font.Bold = true;
                    exWorkSheet9.Cells["B5"].Style.Font.Size = 12;
                    exWorkSheet9.Cells["B5"].AutoFitColumns(15);
                    exWorkSheet9.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet9.Cells["B5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet9.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet9.Cells["B5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet9.Column(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                               
                    exWorkSheet9.Cells["C5"].Value = "User Name";
                    exWorkSheet9.Cells["C5"].Style.Font.Bold = true;
                    exWorkSheet9.Cells["C5"].Style.Font.Size = 12;
                    exWorkSheet9.Cells["C5"].AutoFitColumns(25);
                    exWorkSheet9.Cells["C5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet9.Cells["C5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet9.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet9.Cells["C5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet9.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                               
                    exWorkSheet9.Cells["D5"].Value = "Location";
                    exWorkSheet9.Cells["D5"].Style.Font.Bold = true;
                    exWorkSheet9.Cells["D5"].Style.Font.Size = 12;
                    exWorkSheet9.Cells["D5"].AutoFitColumns(25);
                    exWorkSheet9.Cells["D5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet9.Cells["D5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet9.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet9.Cells["D5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet9.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                               
                    exWorkSheet9.Cells["E5"].Value = "Role";
                    exWorkSheet9.Cells["E5"].Style.Font.Bold = true;
                    exWorkSheet9.Cells["E5"].Style.Font.Size = 12;
                    exWorkSheet9.Cells["E5"].AutoFitColumns(25);
                    exWorkSheet9.Cells["E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet9.Cells["E5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet9.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet9.Cells["E5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    exWorkSheet9.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    int AsciiValue9 = 70;
                    bool CheckAsciiVal9 = false;
                    for (int i = 0; i < NoOfMonth; i++)
                    {
                        DateTime CurrentMonth = fromDatePeroid.AddMonths(i);
                        var ColVal = string.Empty; ;
                        if (NoOfMonth > 23)
                        {
                            if (AsciiValue9 == 91)
                            {
                                CheckAsciiVal9 = true;
                                AsciiValue9 = 65;
                            }
                            if (CheckAsciiVal9)
                            {
                                ColVal = "A" + Convert.ToChar(AsciiValue9);
                            }
                            else
                            {
                                ColVal = Convert.ToChar(AsciiValue9).ToString();
                            }

                            exWorkSheet9.Cells[ColVal + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                            exWorkSheet9.Cells[ColVal + "5"].Style.Font.Bold = true;
                            exWorkSheet9.Cells[ColVal + "5"].Style.Font.Size = 12;
                            exWorkSheet9.Cells[ColVal + "5"].AutoFitColumns(15);
                            exWorkSheet9.Cells[ColVal + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet9.Cells[ColVal + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet9.Cells[ColVal + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet9.Cells[ColVal + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            AsciiValue9++;
                        }
                        else
                        {
                            exWorkSheet9.Cells[Convert.ToChar(AsciiValue9) + "5"].Value = CurrentMonth.ToString("MMM") + "-" + CurrentMonth.ToString("yyyy");
                            exWorkSheet9.Cells[Convert.ToChar(AsciiValue9) + "5"].Style.Font.Bold = true;
                            exWorkSheet9.Cells[Convert.ToChar(AsciiValue9) + "5"].Style.Font.Size = 12;
                            exWorkSheet9.Cells[Convert.ToChar(AsciiValue9) + "5"].AutoFitColumns(15);
                            exWorkSheet9.Cells[Convert.ToChar(AsciiValue9) + "5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet9.Cells[Convert.ToChar(AsciiValue9) + "5"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet9.Cells[Convert.ToChar(AsciiValue9) + "5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet9.Cells[Convert.ToChar(AsciiValue9) + "5"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                            AsciiValue9++;
                        }
                    }

                    int count9 = Convert.ToInt32(ExcelData9.Rows.Count) + 5;
                    using (ExcelRange col = exWorkSheet9.Cells[5, 1, count9, NoOfMonth + 5])
                    {
                        col.Style.WrapText = true;
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        //col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }

                    using (ExcelRange col = exWorkSheet9.Cells[5, 6, count9, NoOfMonth + 5])
                    {
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }
                    #endregion

                    #endregion

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    string locatioName = cname + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                    Response.AddHeader("content-disposition", "attachment;filename=UsageReport-" + locatioName + ".xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected String GetRole(long UserID)
        {
            try
            {
                String Role = String.Empty;

                int RoleID = -1;

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var DistinctRoles = (from row in entities.ComplianceAssignments
                                         where row.UserID == UserID
                                         select row).GroupBy(Entry => Entry.RoleID)
                                         .Select(a => a.FirstOrDefault())
                                         .Select(a => a.RoleID).ToList();

                    if (RoleID != -1)
                        DistinctRoles = DistinctRoles.Where(Entry => Entry == RoleID).ToList();

                    DistinctRoles.ForEach(EachRole =>
                    {
                        var RoleName = (from row in entities.Roles
                                        where row.ID == EachRole
                                        select row.Name).FirstOrDefault();

                        if (RoleName != null)
                        {
                            if (RoleName == "Reviewer1" || RoleName == "Reviewer2")
                                RoleName = "Reviewer";

                            if (!String.IsNullOrEmpty(Role))
                                Role += ", " + RoleName;
                            else
                                Role += RoleName;
                        }
                    });
                }

                return Role;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            ExportAllData();
           
            DateTime date = DateTime.Now;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxBranch.ClientID), true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        public class UsageReport_Data
        {
            public int CustomerID { get; set; }
            public long UserID { get; set; }
            public int RoleID { get; set; }
            public int CustomerBranchID { get; set; }
            public string CustomerName { get; set; }
            public string UserName { get; set; }
            public string CustomerBranchName { get; set; }
            public DateTime ScheduledOn { get; set; }
            public int ComplianceStatusID { get; set; }
            public string Role { get; set; }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            user_Roles = AuthenticationHelper.Role;
            BindLocationFilter();
            DateTime date = DateTime.Now;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "InitializeJQueryUI", "initializeJQueryUI();", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
        }

        private void BindCustomer()
        {
            try
            {
                int userid = -1;
                userid = Convert.ToInt32(AuthenticationHelper.UserID);
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                //ddlCustomer.DataSource = CustomerManagement.GetAll("");
                ddlCustomer.DataSource = Assigncustomer.GetAllCustomer(userid);
                ddlCustomer.DataBind();
                
                ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    ddlCustomer.SelectedValue = Convert.ToString(customerID);
                    ddlCustomer.Enabled = false;
                }
               

                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "InitializeJQueryUI", "initializeJQueryUI();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}