﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ClientEventMappingList : System.Web.UI.Page
    {
        
            DataSet ds = new DataSet();
        public static int UserID;
            protected void Page_Load(object sender, EventArgs e)
            {
                if (!IsPostBack)
                {
                UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = "EventID";
                    BindCustomers(UserID);
                    BindCustomersList(UserID);
                    BindMapping();
                    Session["CurrentRole"] = AuthenticationHelper.Role;
                    Session["CurrentUserId"] = AuthenticationHelper.UserID;                    
                }               
            }        
            private void BindCustomers(int UserID)
            {
                try
                {
                    ddlCustomer.DataTextField = "Name";
                    ddlCustomer.DataValueField = "ID";
                //   ddlCustomer.DataSource = Business.CustomerManagement.GetClient();
                ddlCustomerList.DataSource = GetAllCustomer(UserID);
                ddlCustomer.DataBind();                  
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        public static object GetAllCustomer(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var users = (from row in entities.Customers
                             join row1 in entities.KeyDetails
                            on row.ID equals row1.CustomerID
                             join row2 in entities.CustomerAssignmentDetails
                             on row.ID equals row2.CustomerID
                             where row2.UserID == userID
                             && row.IsDeleted == false
                             && row2.IsDeleted == false
                             select row);

                return users.Distinct().ToList();
            }
        }
        private void BindCustomersList(int UserID)
            {
                try
                {
                    ddlCustomerList.DataTextField = "Name";
                    ddlCustomerList.DataValueField = "ID";
                //   ddlCustomerList.DataSource = Business.CustomerManagement.GetClient();
                ddlCustomerList.DataSource = GetAllCustomer(UserID);
                ddlCustomerList.DataBind();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
          
            protected void btnAddUser_Click(object sender, EventArgs e)
            {
                Add();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open')", true);
            }
            public void Add()
            {
                try
                {
                    ViewState["Mode"] = 0;
                    ddlCustomer.Enabled = true;
                    upUsers.Update();
                    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open');", true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
           #region user Detail
          
            protected void grdMapping_RowCommand(object sender, GridViewCommandEventArgs e)
            {
                try
                {                    
                    //int customerid = Convert.ToInt32(Convert.ToString(e.CommandArgument).Split(';')[0]);
                    //int productid = Convert.ToInt32(Convert.ToString(e.CommandArgument).Split(';')[1]);
                    //if (e.CommandName.Equals("EDIT_USER"))
                    //{
                    //    EditUserInformation(customerid, productid);
                    //}
                    //else if (e.CommandName.Equals("DELETE_USER"))
                    //{
                    //    CommanClass.Delete(customerid, productid);
                    //    CommanClass.ProductMappingRiskDelete(customerid, productid);
                    //    BindMapping();
                    //}                                                     
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }

        public void BindMapping()
        {
            try
            {
                int customerID = -1;
                if (ddlCustomerList.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                var productmappinglist = Business.ComplianceManagement.GetAllCliantMappingEvent(customerID, tbxFilter.Text);
                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "EventID")
                    {
                        productmappinglist = productmappinglist.OrderBy(entry => entry.EventID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "EventName")
                    {
                        productmappinglist = productmappinglist.OrderBy(entry => entry.EventName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "CustomerName")
                    {
                        productmappinglist = productmappinglist.OrderBy(entry => entry.CustomerName).ToList();
                    }
                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "EventID")
                    {
                        productmappinglist = productmappinglist.OrderByDescending(entry => entry.EventID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "EventName")
                    {
                        productmappinglist = productmappinglist.OrderByDescending(entry => entry.EventName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "CustomerName")
                    {
                        productmappinglist = productmappinglist.OrderByDescending(entry => entry.CustomerName).ToList();
                    }

                    direction = SortDirection.Ascending;
                }
                grdMapping.DataSource = productmappinglist;
                grdMapping.DataBind();
                upUserList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdMapping_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                try
                {
                    grdMapping.PageIndex = e.NewPageIndex;
                    BindMapping();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
            protected void grdMapping_Sorting(object sender, GridViewSortEventArgs e)
            {
                try
                {
                int customerID = -1;
                if (ddlCustomerList.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                var productmappinglist = Business.ComplianceManagement.GetAllCliantMappingEvent(customerID, tbxFilter.Text);
                if (direction == SortDirection.Ascending)
                    {
                        productmappinglist = productmappinglist.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Descending;
                        ViewState["SortOrder"] = "Asc";
                        ViewState["SortExpression"] = e.SortExpression.ToString();
                    }
                    else
                    {
                        productmappinglist = productmappinglist.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Ascending;
                        ViewState["SortOrder"] = "Desc";
                        ViewState["SortExpression"] = e.SortExpression.ToString();
                    }
                    foreach (DataControlField field in grdMapping.Columns)
                    {
                        if (field.SortExpression == e.SortExpression)
                        {
                            ViewState["SortIndex"] = grdMapping.Columns.IndexOf(field);
                        }
                    }
                    grdMapping.DataSource = productmappinglist;
                    grdMapping.DataBind();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
            protected void grdMapping_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                try
                {                   
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
            protected void grdMapping_RowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row);
                    }
                }
            }

            protected void AddSortImage(int columnIndex, GridViewRow headerRow)
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../Images/SortAsc.gif";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../Images/SortDesc.gif";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }

            #endregion           
            protected void tbxFilter_TextChanged(object sender, EventArgs e)
            {
                try
                {
                    grdMapping.PageIndex = 0;
                    BindMapping();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
            protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
            {
                try
                {
                    BindMapping();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }                                 
            public SortDirection direction
            {
                get
                {
                    if (ViewState["dirState"] == null)
                    {
                        ViewState["dirState"] = SortDirection.Ascending;
                    }
                    return (SortDirection)ViewState["dirState"];
                }
                set
                {
                    ViewState["dirState"] = value;
                }
            }           
            protected void AddInstancesSortImage(int columnIndex, GridViewRow headerRow)
            {
                try
                {
                    System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                    sortImage.ImageAlign = ImageAlign.AbsMiddle;
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../Images/SortAsc.gif";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../Images/SortDesc.gif";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }          
            protected void upUsers_Load(object sender, EventArgs e)
            {
                try
                {
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox(1);", true);                    
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<ClientComplianceMapping> CCMList = new List<ClientComplianceMapping>();
                List<ClientEventMapping> CEMList = new List<ClientEventMapping>();
                List<string> EventIDList = new List<string>();
                List<long> eventList = new List<long>();
                var keydetail = ComplianceManagement.Business.ComplianceManagement.GetKeyDetail(Convert.ToInt32(ddlCustomer.SelectedValue));
                EventIDList = txtEventIDList.Text.Split(',').ToList();
                eventList = EventIDList.Select(long.Parse).ToList();
                foreach (long clist in eventList)
                {
                    ClientEventMapping CEM = new ClientEventMapping();
                    CEM.Customerid = Convert.ToInt32(ddlCustomer.SelectedValue);
                    CEM.KeyId = Convert.ToInt32(keydetail.ID);
                    CEM.EventID = Convert.ToInt32(clist);
                    CEMList.Add(CEM);
                }
                bool checkFlag = false;
                checkFlag = Business.ComplianceManagement.BlukClientMapplingEvent(CEMList);               
                var ComplianceList = Business.ComplianceManagement.GetEventBasedMappedCompliance(eventList);
                var List= ComplianceList.Distinct().ToList();               
                foreach (long clist in List)
                {
                    ClientComplianceMapping CCM = new ClientComplianceMapping();
                    CCM.Customerid = Convert.ToInt32(ddlCustomer.SelectedValue);
                    CCM.KeyId = Convert.ToInt32(keydetail.ID);
                    CCM.ComplianceID = Convert.ToInt32(clist);
                    CCMList.Add(CCM);
                }
                bool checkFlag1 = false;
                checkFlag1 = Business.ComplianceManagement.BlukClientMapplingCompliance(CCMList);

                if (checkFlag && checkFlag1)
                {
                    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "CloseDialog", "$(\"#divUsersDialog\").dialog('close')", true);
                }
                BindMapping();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }                              
    }
}