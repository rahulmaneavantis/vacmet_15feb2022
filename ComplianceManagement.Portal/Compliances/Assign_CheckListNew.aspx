﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="Assign_CheckListNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.Assign_CheckListNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function Showalert() {
            alert("Checklist(s) Assigned Successfully.");
        }
        $(function () {

            initializeCombobox();

        });
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function initializeCombobox() {
            $("#<%= ddlFilterPerformer.ClientID %>").combobox();
            $("#<%= ddlFilterReviewer.ClientID %>").combobox();
        <%--    $("#<%= ddlFilterApprover.ClientID %>").combobox();--%>
            $("#<%= ddlComplianceType.ClientID %>").combobox();
            $("#<%= ddlComplianceCatagory.ClientID %>").combobox();
            $("#<%= ddlFilterDepartment.ClientID %>").combobox();     
        }

      
        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }
        function UncheckHeaderCustomer() {
            var rowCheckBox = $("#RepeaterSubTable input[id*='chkApprover']");
            var rowCheckBoxSelected = $("#RepeaterSubTable input[id*='chkApprover']:checked");
            var rowCheckBoxHeader = $("#RepeaterSubTable input[id*='ApproverSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }
        function checkAllCustomer(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkApprover") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }
        function initializeJQueryUI1(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='actSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>

    <style type="text/css">
        .td1 {
            width: 15%;
        }

        .td2 {
            width: 25%;
        }

        .td3 {
            width: 15%;
        }

        .td4 {
            width: 25%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceTypeList_Load">
        <ContentTemplate>
            <center>
                <table runat="server" width="80%">
                    <tr>
                           <td class="td1">
                               <div style="">
                               <label style="width: 10px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px; color: red;margin-left: -12px;">*</label>
                                <label style="width: 130px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Customer:
                            </label>
                                   </div>
                        </td>
                        <td class="td2">
                            <div id="customerdiv" runat="server">
                            <asp:DropDownList runat="server" ID="ddlCustomer" Style="padding: 0px; margin: 0px; margin-left: 2px; height: 2px; width: 280px;height:24px;width:385px;"
                           OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"  CssClass="txtbox" AutoPostBack="true"/>
              
                                   <asp:CompareValidator ErrorMessage="Please select Customer." ControlToValidate="ddlCustomer"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />
                                </div>
                        </td>
                        <td class="td2">
                            <div style="">
                               <label style="width: 10px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px; color: red;margin-left: 35px;">*</label>
                                 <label style="width: 100px; display: block; font-size: 13px; color: #333; float: right; margin-top: 4px;margin-right:32px">
                                Select Location:</label>
                                </div>
                        </td>
                        <td class="td3">
                            <div style="">
                            <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                                </div>
                            <div style="margin-left: 0px; position: absolute; z-index: 10" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="200px" Width="300px"
                                    Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                </asp:TreeView>
                                <asp:CompareValidator ControlToValidate="tbxFilterLocation" ErrorMessage="Please select Location."
                                    runat="server" ValueToCompare="< Select >" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                    Display="None" />
                            </div>
                        </td>
                        </tr>
                    <tr>
                        <td class="td1">
                              <label style="width: 10px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px; color: red;margin-left: -12px;">*</label>
                             <label style="width: 105px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Select Performer:
                            </label>
                        </td>
                        <td class="td2">
                            <asp:DropDownList runat="server" ID="ddlFilterPerformer" Style="padding: 0px; margin: 0px; height: 22px; width: 50px;">
                            </asp:DropDownList>
                            <asp:CompareValidator ErrorMessage="Please select Performer." ControlToValidate="ddlFilterPerformer"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />
                        </td>
                                      
                        <td class="td3">
                            <label style="width: 10px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px; color: red;margin-left: 35px;">*</label>
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: right; margin-top: 4px;margin-right:32px">
                                Select Reviewer:
                            </label>
                        </td>
                        <td class="td4">
                            <asp:DropDownList runat="server" ID="ddlFilterReviewer" Style="padding: 0px; margin: 0px; min-height: 22px; min-width: 50px;">
                            </asp:DropDownList>
                            <asp:CompareValidator ErrorMessage="Please select Reviewer." ControlToValidate="ddlFilterReviewer"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />
                        </td>
                        </tr>
                    <tr>
                       <td class="td1">
                            <label style="width: 105px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Select Approver:
                            </label>
                        </td>
                        <td class="td2">
                         <%--   <asp:DropDownList runat="server" ID="ddlFilterApprover" Style="padding: 0px; margin: 0px; min-height: 22px; min-width: 50px;">
                            </asp:DropDownList>--%>
                              <asp:TextBox runat="server" ID="txtapprover" Style="padding: 0px; margin: 0px; height: 22px; width: 388px;"
                            CssClass="txtbox" />
                        <div style="margin-left: 7px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px; width: 300px;" id="dvapprover">
                            <asp:Repeater ID="rptApprover" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterSubTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="ApproverSelectAll" Text=" All"
                                                    runat="server" onclick="checkAllApprover(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRepeatersub" Text="Ok" Style="float: left"  /></td>
                                   </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkApprover" runat="server" onclick="UncheckHeaderCustomer();" /></td>
                                        <td style="width: 560px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 600px; padding-bottom: 5px;">
                                                <asp:Label ID="lblapproverid" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblapproverName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                        </td>
                   
                        <td class="td3">
                              <label style="width: 10px; display: block; font-size: 13px; color: #333; float: right; margin-top: 2px;margin-right:135px;margin-left:34px;margin-bottom:-35px;color:red">*</label>
                            <label style="width: 130px; display: block; font-size: 13px; color: #333; float: right; margin-top: 4px;margin-right:3px;margin-left:25px;">
                                Compliance Category:
                            </label>
                        </td>
                        <td class="td4">
                            <asp:DropDownList runat="server" ID="ddlComplianceCatagory" Style="padding: 0px; margin: 0px; height: 22px; width: 50px;"
                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplianceCatagory_SelectedIndexChanged">
                            </asp:DropDownList>
                                   <asp:CompareValidator ErrorMessage="Please select Compliance Catagory." ControlToValidate="ddlComplianceCatagory"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />
                        </td>
                         </tr>
                    <tr>
                        <td class="td1">
                            <label style="width: 105px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Compliance Type:
                            </label>
                        </td>
                        <td class="td2">
                            <asp:DropDownList runat="server" ID="ddlComplianceType" Style="padding: 0px; margin: 0px; height: 22px; width: 50px;"
                                CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged">
                            </asp:DropDownList>
                           <%-- <asp:CompareValidator ErrorMessage="Please select Compliance Type." ControlToValidate="ddlComplianceType"
                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceInstanceValidationGroup"
                                Display="None" />--%>
                        </td>
                   
                        <td class="td3">
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: right; margin-top: 4px;margin-right:30px">
                                Act:
                            </label>
                        </td>
                        <td class="td4">
                            <asp:TextBox runat="server" ID="txtactList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                CssClass="txtbox" />
                            <div style="margin-left: 0px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvActList">
                                <asp:Repeater ID="rptActList" runat="server">
                                    <HeaderTemplate>
                                        <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                            <tr>
                                                <td style="width: 100px;">
                                                    <asp:CheckBox ID="actSelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                                <td style="width: 282px;">
                                                    <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" OnClick="btnRefresh_Click" /></td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width: 20px;">
                                                <asp:CheckBox ID="chkAct" runat="server" onclick="UncheckHeader();" /></td>
                                            <td style="width: 200px;">
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                    <asp:Label ID="lblActID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                    <asp:Label ID="lblActName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </td>
                        </tr>
                    <tr>
                       <td class="td1">
                              <label style="width: 115px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Select Department:
                            </label>
                        </td>
                        <td class="td2">

                             <asp:DropDownList runat="server" ID="ddlFilterDepartment" 
                                 Style="padding: 0px; margin: 0px; min-height: 22px; min-width: 50px;">
                            </asp:DropDownList>
                        </td>
                        <td class="td3">
                            <label style="width: 100px; display: block; font-size: 13px; color: #333; float: right; margin-top: 4px;margin-right:30px">
                                Filter:
                            </label>
                        </td>
                        <td class="td4">
                            <asp:TextBox runat="server" ID="tbxFilter" Style="height: 16px; width: 390px;" MaxLength="50" AutoPostBack="true"
                                OnTextChanged="tbxFilter_TextChanged" />
                        </td>
                        </tr>
                    <tr>
                        <td class="td1">

                         <%--   <label style="width: 100px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                Event Based:
                            </label>--%>
                        </td>
                        <td class="td2">
                            <asp:CheckBox runat="server" ID="chkEvent" Visible="false" OnCheckedChanged="chkEvent_CheckedChanged" AutoPostBack="true" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ComplianceInstanceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                        </td>
                    </tr>
                    <br />
                    <tr>
                        <td colspan="4" align="center">
                             <asp:Panel ID="Panel1" Width="100%" Height="330px" ScrollBars="Auto" runat="server">
                            <asp:GridView runat="server" ID="grdComplianceRoleMatrix" AutoGenerateColumns="false" GridLines="Vertical" OnRowCreated="grdComplianceRoleMatrix_RowCreated"
                                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true" OnSorting="grdComplianceRoleMatrix_Sorting"
                                CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="50" Width="100%" OnRowDataBound="grdComplianceRoleMatrix__RowDataBound"
                                Font-Size="12px" DataKeyNames="ID" OnPageIndexChanging="grdComplianceRoleMatrix_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText="Compliance ID" SortExpression="ComplianceID">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                <asp:Label ID="lblComplianceID" runat="server" Text='<%# Eval("ID")%>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Section" SortExpression="Sections">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                <asp:Label runat="server" Text='<%# Eval("Sections")%>' ToolTip='<%# Eval("Sections") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" SortExpression="Description">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 550px;">
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAssignSelectAll" Text="Assign" runat="server" AutoPostBack="true" OnCheckedChanged="chkAssignSelectAll_CheckedChanged" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkAssign" runat="server"  />
                                            <%--AutoPostBack="true" OnCheckedChanged="chkAssign_CheckedChanged"--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#CCCC99" />
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Small" />
                                <PagerSettings Position="Top" />
                                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                <AlternatingRowStyle BackColor="#E6EFF7" />
                                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    No Records Found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                             </asp:Panel> 
                            <asp:Button Text="Save" runat="server" ID="Button1" OnClick="btnSave_Click" CssClass="button"
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                        </td>
                    </tr>
                </table>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
