﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Text;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Text.RegularExpressions;
using System.Data;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ComplianceDeactivate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (AuthenticationHelper.UserID == 8877 || AuthenticationHelper.UserID == 9339)
                {
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = "ID";

                    BindCategories();
                    BindTypes();
                    BindFilterFrequencies();
                    BindCompliancesNew();                   
                }
                else
                {
                    Response.Redirect("~/Common/CompanyStructure.aspx", false);
                }
            }
        }

        public string ComplianceActiveOrInActive(int ComplianceID)
        {
            try
            {
                string result = "Active";
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var data = (from row in entities.Compliances
                            where row.ID == ComplianceID
                            select row).FirstOrDefault();
                if (data.Status == "D" || data.Status == null)
                {
                    result = "Active";
                }
                else
                {
                    result = "DeActive";
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return "DeActive";
        }

       protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("STATUS"))
                {
                    Div1.Visible = false;
                    int complianceID = Convert.ToInt32(e.CommandArgument);
                    var compliance = Business.ComplianceManagement.GetByID(complianceID);
                    var compliancedeactive = Business.ComplianceManagement.GetComplianceDeactivate(complianceID);

                    ViewState["Mode"] = 1;
                    ViewState["ComplianceID"] = complianceID;
                    BindStatusActs();
                    ddlStatusAct.SelectedValue = compliance.ActID.ToString();
                    txtShortDescriptionStatus.Text = compliance.ShortDescription;
                    tbxDescriptionStatus.Text = compliance.Description;
                    tbxSectionsStatus.Text = compliance.Sections;

                    string deactivateOn = "";
                    if (compliance.DeactivateOn != null)
                    {
                        deactivateOn = Convert.ToDateTime(compliance.DeactivateOn).ToString("dd-MM-yyyy");
                    }

                    txtDeactivateDate.Text = deactivateOn;

                    txtDeactivateDesc.Text = compliance.DeactivateDesc;

                    if (compliancedeactive != null)
                    {
                        Div1.Visible = true;
                        var compliancedeactiveList = Business.ComplianceManagement.GetComplianceDeactivateList(complianceID);
                        //lblDeactivate.Text = compliancedeactive.Name;
                        grdFile.DataSource = compliancedeactiveList;
                        grdFile.DataBind();

                    }
                    upComplianceStatusDetails.Update();
                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog2", "$(\"#divComplianceStatusDialog\").dialog('open')", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCompliances.PageIndex = e.NewPageIndex;
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindCompliancesNew();
        }

        protected void upComplianceDetailsStatus_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);

                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker1", string.Format("initializeDatePicker1(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCompliances_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdCompliances_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int CmType = -1;
                if (rdFunctionBased.Checked)
                    CmType = 0;
                if (rdChecklist.Checked)
                    CmType = 1;

                var compliancesData = Business.ComplianceManagement.GetDeactivateCompliance(Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplinceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, tbxFilter.Text);
                List<object> dataSource = new List<object>();
                foreach (var complianceInfo in compliancesData)
                {
                    string risk = "";
                    if (complianceInfo.RiskType == 0)
                        risk = "High";
                    else if (complianceInfo.RiskType == 1)
                        risk = "Medium";
                    else if (complianceInfo.RiskType == 2)
                        risk = "Low";

                    dataSource.Add(new
                    {
                        complianceInfo.ID,
                        complianceInfo.ActName,
                        complianceInfo.Sections,
                        complianceInfo.Description,
                        complianceInfo.ComplianceType,
                        complianceInfo.EventID,
                        //NatureOfCompliance = ((NatureOfCompliance)complianceInfo.Key.NatureOfCompliance.Value).ToString(),
                        complianceInfo.UploadDocument,
                        complianceInfo.RequiredForms,
                        FrequencyName = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1)),
                        complianceInfo.NonComplianceEffects,
                        Risk = risk,
                        complianceInfo.ShortDescription,
                        complianceInfo.SubComplianceType,
                        complianceInfo.CheckListTypeID
                        //Parameters = GetParameters(complianceInfo.Value)
                    });
                }

                if (direction == SortDirection.Ascending)
                {
                    dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                foreach (DataControlField field in grdCompliances.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCompliances.Columns.IndexOf(field);
                    }
                }

                grdCompliances.DataSource = dataSource;
                grdCompliances.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlComplinceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlFilterComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void rdChecklist_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rdFunctionBased_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTypes()
        {
            try
            {
                ddlFilterComplianceType.DataTextField = "Name";
                ddlFilterComplianceType.DataValueField = "ID";

                ddlFilterComplianceType.DataSource = ComplianceTypeManagement.GetAll();
                ddlFilterComplianceType.DataBind();

                ddlFilterComplianceType.Items.Insert(0, new ListItem("< Select Compliance Type >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

       private void BindCategories()
        {
            try
            {
                ddlComplinceCatagory.DataTextField = "Name";
                ddlComplinceCatagory.DataValueField = "ID";

                ddlComplinceCatagory.DataSource = ComplianceCategoryManagement.GetAll();
                ddlComplinceCatagory.DataBind();

                ddlComplinceCatagory.Items.Insert(0, new ListItem("< Select Compliance Category>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        private void BindFilterFrequencies()
        {
            try
            {
                ddlFilterFrequencies.DataTextField = "Name";
                ddlFilterFrequencies.DataValueField = "ID";

                ddlFilterFrequencies.DataSource = Enumerations.GetAll<Frequency>();
                ddlFilterFrequencies.DataBind();

                ddlFilterFrequencies.Items.Insert(0, new ListItem("< Select Frequency>", "-1"));
                ddlFilterFrequencies.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCompliancesNew()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int CmType = -1;
                if (rdFunctionBased.Checked)
                    CmType = 0;
                if (rdChecklist.Checked)
                    CmType = 1;

                var compliancesQuery = (from row in entities.Compliances
                                        join row1 in entities.Acts on row.ActID equals row1.ID
                                        where row.IsDeleted == false && row.Status == "D"
                                        select new
                                        {
                                            row.ID
                                                ,
                                            row.ActID
                                                ,
                                            ActName = row1.Name
                                                ,
                                            row.Description
                                                ,
                                            row.Sections
                                                ,
                                            row.ComplianceType
                                                ,
                                            row.UploadDocument
                                                ,
                                            row.NatureOfCompliance
                                                ,
                                            row.RequiredForms
                                                ,
                                            row.Frequency
                                                ,
                                            row.DueDate
                                                ,
                                            row.RiskType
                                                ,
                                            row.NonComplianceType
                                                ,
                                            row.NonComplianceEffects
                                                ,
                                            row.ShortDescription
                                                ,
                                            row.CreatedOn
                                                ,
                                            row1.ComplianceCategoryId
                                                ,
                                            row1.ComplianceTypeId
                                                ,
                                            row.EventID
                                                ,
                                            row.SubComplianceType
                                                ,
                                            row.FixedGap
                                                ,
                                            row.CheckListTypeID
                                                ,
                                            RISK = row.RiskType == 0 ? "High" :
                                                row.RiskType == 1 ? "Medium" :
                                                row.RiskType == 2 ? "Low" : "",
                                            FrequencyName = row.Frequency == 0 ? "Monthly" :
                                            row.Frequency == 1 ? "Quarterly" :
                                            row.Frequency == 2 ? "HalfYearly" :
                                            row.Frequency == 3 ? "Annual" :
                                            row.Frequency == 4 ? "FourMonthly" :
                                            row.Frequency == 5 ? "TwoYearly" :
                                            row.Frequency == 6 ? "SevenYearly" : ""
                                        });

                int risk = -1;
                if (tbxFilter.Text.ToUpper().Equals("HIGH"))
                    risk = 0;
                else if (tbxFilter.Text.ToUpper().Equals("MEDIUM"))
                    risk = 1;
                else if (tbxFilter.Text.ToUpper().Equals("LOW"))
                    risk = 2;

                string frequency = tbxFilter.Text.ToUpper();
                if (frequency.Equals("MONTHLY"))
                    frequency = "Monthly";
                if (frequency.Equals("QUARTERLY"))
                    frequency = "Quarterly";
                if (frequency.Equals("HALFYEARLY"))
                    frequency = "HalfYearly";
                if (frequency.Equals("ANNUAL"))
                    frequency = "Annual";
                if (frequency.Equals("FOURMONTHALY"))
                    frequency = "FourMonthly";
                if (frequency.Equals("TWOYEARLY"))
                    frequency = "TwoYearly";
                if (frequency.Equals("SEVENYEARLY"))
                    frequency = "SevenYearly";

                int frequencyId = Enumerations.GetEnumByName<Frequency>(Convert.ToString(frequency));
                if (frequencyId == 0 && frequency != "Monthly")
                {
                    frequencyId = -1;
                }

                if (!string.IsNullOrEmpty(tbxFilter.Text))
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ActName.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.ShortDescription.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.Sections.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.RequiredForms.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.Frequency == frequencyId || entry.RiskType == risk);
                }

                if (Convert.ToInt32(ddlFilterComplianceType.SelectedValue) != -1)
                {
                    int a = Convert.ToInt32(ddlFilterComplianceType.SelectedValue);
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceTypeId == a);
                }
                if (Convert.ToInt32(ddlComplinceCatagory.SelectedValue) != -1)
                {
                    int b = Convert.ToInt32(ddlComplinceCatagory.SelectedValue);
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceCategoryId == b);
                }
                if (CmType != -1)
                {
                    compliancesQuery = compliancesQuery.Where(entry => entry.ComplianceType == CmType);
                }
                if (Convert.ToInt32(ddlFilterFrequencies.SelectedValue) != -1)
                {
                    int c = Convert.ToInt32(ddlFilterFrequencies.SelectedValue);
                    compliancesQuery = compliancesQuery.Where(entry => entry.Frequency == c);
                }
                entities.Database.CommandTimeout = 360;
                var compliances = compliancesQuery.ToList();

                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "ID")
                    {
                        compliances = compliances.OrderBy(entry => entry.ID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ActName")
                    {
                        compliances = compliances.OrderBy(entry => entry.ActName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ShortDescription")
                    {
                        compliances = compliances.OrderBy(entry => entry.ShortDescription).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Sections")
                    {
                        compliances = compliances.OrderBy(entry => entry.Sections).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "RISK")
                    {
                        compliances = compliances.OrderBy(entry => entry.RISK).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "FrequencyName")
                    {
                        compliances = compliances.OrderBy(entry => entry.FrequencyName).ToList();
                    }
                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "ID")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.ID).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ActName")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.ActName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ShortDescription")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.ShortDescription).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Sections")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.Sections).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "RISK")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.RISK).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "FrequencyName")
                    {
                        compliances = compliances.OrderByDescending(entry => entry.FrequencyName).ToList();
                    }
                    direction = SortDirection.Ascending;
                }
                grdCompliances.DataSource = compliances;
                grdCompliances.DataBind();
                upCompliancesList.Update();
            }
        }

        private void BindCompliances()
        {
            try
            {
                int CmType = -1;
                if (rdFunctionBased.Checked)
                    CmType = 0;
                if (rdChecklist.Checked)
                    CmType = 1;

                var compliancesData = Business.ComplianceManagement.GetAll1(Convert.ToInt32(ddlFilterComplianceType.SelectedValue), Convert.ToInt32(ddlComplinceCatagory.SelectedValue), Convert.ToInt32(ddlFilterFrequencies.SelectedValue), CmType, tbxFilter.Text);
                List<object> dataSource = new List<object>();
                foreach (var complianceInfo in compliancesData)
                {
                    string risk = "";
                    if (complianceInfo.RiskType == 0)
                        risk = "High";
                    else if (complianceInfo.RiskType == 1)
                        risk = "Medium";
                    else if (complianceInfo.RiskType == 2)
                        risk = "Low";

                    string ss = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency));
                    dataSource.Add(new
                    {
                        complianceInfo.ID,
                        complianceInfo.ActName,
                        complianceInfo.Sections,
                        complianceInfo.Description,
                        complianceInfo.ComplianceType,
                        complianceInfo.EventID,
                        complianceInfo.UploadDocument,
                        complianceInfo.RequiredForms,
                        Frequency = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1)),
                        complianceInfo.NonComplianceEffects,
                        Risk = risk,
                        complianceInfo.ShortDescription,
                        complianceInfo.SubComplianceType,
                        complianceInfo.CheckListTypeID
                    });
                }
                grdCompliances.DataSource = dataSource;
                grdCompliances.DataBind();
                upCompliancesList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
 
        private string GetParameters(List<ComplianceParameter> parameters)
        {
            try
            {
                StringBuilder paramString = new StringBuilder();
                foreach (var item in parameters)
                {
                    paramString.Append(paramString.Length == 0 ? item.Name : ", " + item.Name);
                }

                return paramString.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
            return null;
        }

        private void BindStatusActs()
        {
            try
            {
                ddlStatusAct.DataTextField = "Name";
                ddlStatusAct.DataValueField = "ID";

                ddlStatusAct.DataSource = ActManagement.GetAllNVP();
                ddlStatusAct.DataBind();

                ddlStatusAct.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Row.RowType == DataControlRowType.DataRow)
        //        {
        //            //e.Row.Cells[0].Attributes.Add("style", "word-break:break-all;word-wrap:break-word");
        //            LinkButton lbtEdit = (LinkButton)e.Row.FindControl("lbtEdit");
        //            LinkButton lbtDelete = (LinkButton)e.Row.FindControl("lbtDelete");
        //            LinkButton LinkButton3 = (LinkButton)e.Row.FindControl("LinkButton3");
        //            lbtEdit.Visible = false;
        //            lbtDelete.Visible = false;
        //            LinkButton3.Visible = false;
        //            //lblDeactivate.Visible = false;
        //            if (AuthenticationHelper.Role.Equals("SADMN"))
        //            {
        //                lbtEdit.Visible = true;
        //                lbtDelete.Visible = true;
        //                LinkButton3.Visible = true;
        //                //lblDeactivate.Visible = true;
        //            }
        //            if (AuthenticationHelper.Role.Equals("IMPT"))
        //            {
        //                lbtEdit.Visible = true;
        //                lbtDelete.Visible = true;
        //                LinkButton3.Visible = true;
        //               // lblDeactivate.Visible = true;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            try
            {
                string filePath = string.Empty;
                LinkButton lnkbtn = sender as LinkButton;
                GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
                string fileName = string.Empty;  
                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                {
                    fileName =  grdFile.DataKeys[gvrow.RowIndex].Values[2].ToString();
                    string pathvalue = grdFile.DataKeys[gvrow.RowIndex].Values[0].ToString();
                    filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
                    Response.ContentType = "application/ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
                    Response.TransmitFile(filePath);
                    Response.End();
                }
                else
                {
                    filePath = grdFile.DataKeys[gvrow.RowIndex].Values[0].ToString();
                    Response.ContentType = "application/ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
                    Response.TransmitFile(Server.MapPath(filePath));
                    Response.End();
                }                                              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }     
   
        private void BindLocation(int eventID)
        {
            try
            {
                var subEvents = EventManagement.GetAllHierarchy(eventID);
                if (subEvents[0].Children.Count > 0)
                {
                    foreach (var item in subEvents)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        BindBranchesHierarchy(node, item);
                    }
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void SelectNodeByValue(TreeNode Node, string ValueToSelect)
        {
            foreach (TreeNode n in Node.ChildNodes)
            {
                if (n.Value == ValueToSelect) { n.Select(); } else { SelectNodeByValue(n, ValueToSelect); }
            }
        }
   
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
        }

        protected void btnSaveDeactivate_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime Date = DateTime.ParseExact(txtDeactivateDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                Business.ComplianceManagement.ChangeStatus(Convert.ToInt32(ViewState["ComplianceID"]), Date, "A", txtDeactivateDesc.Text);
                Business.ComplianceManagement.UpdateComplianceDate(Convert.ToInt32(ViewState["ComplianceID"]));
                CreateLogForDeactivation(Convert.ToInt32(ViewState["ComplianceID"]));
                //if (FileUploadDeactivateDoc.FileBytes != null && FileUploadDeactivateDoc.FileBytes.LongLength > 0)
                //{
                //    FileUploadDeactivateDoc.SaveAs(Server.MapPath("~/DeactiveComplainceFiles/" + FileUploadDeactivateDoc.FileName));
                //    ComplianceDeactive Deact = new ComplianceDeactive()
                //    {
                //        ComplianceID = Convert.ToInt32(ViewState["ComplianceID"]),
                //        Name = FileUploadDeactivateDoc.FileName,
                //        FileData = FileUploadDeactivateDoc.FileBytes,
                //        FilePath = "~/DeactiveComplianceFiles/" + FileUploadDeactivateDoc.FileName
                //    };
                //    Business.ComplianceManagement.DeleteOldDeactiveDoc(Convert.ToInt32(ViewState["ComplianceID"]));
                //    Business.ComplianceManagement.CreateDeactivateFile(Deact, true);
                //}

                Business.ComplianceManagement.RemoveUpcomingSchudule(Convert.ToInt32(ViewState["ComplianceID"]), Date);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceStatusDialog\").dialog('close')", true);
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static void CreateLogForDeactivation(long comid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Log_Deactivation log = new Log_Deactivation()
                {
                    ComplianceID = comid,
                    Flag = "D",
                    IsCompliance = "Compliance",
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedOn = DateTime.Now,

                };
                entities.Log_Deactivation.Add(log);
                entities.SaveChanges();
            }
        }
        protected void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime Date = DateTime.ParseExact(txtDeactivateDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                Business.ComplianceManagement.ChangeStatus(Convert.ToInt32(ViewState["ComplianceID"]), null, null, null);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divComplianceStatusDialog\").dialog('close')", true);
                BindCompliancesNew();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}