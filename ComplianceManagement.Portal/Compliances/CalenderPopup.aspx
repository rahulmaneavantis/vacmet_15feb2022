﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/blank.Master" CodeBehind="CalenderPopup.aspx.cs" EnableEventValidation="false"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.CalenderPopup" %>

<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>

<%--<%@ Register Src="~/Controls/ComplianceStatusTransaction.ascx" TagName="ComplianceStatusTransaction"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/ComplianceReviewerStatusTransaction.ascx" TagName="ComplianceReviewerStatusTransaction"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/InternalComplianceStatusTransaction.ascx" TagName="InternalComplianceStatusTransaction"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/InternalComplianceReviewerStatusTransaction.ascx" TagName="InternalComplianceReviewerStatusTransaction"
    TagPrefix="vit" %>--%>
<%--<%@ Register Src="~/Controls/ComplianceStatusTransactionChkList.ascx" TagName="ComplianceStatusTransactionChkList"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/ComplianceStatusTransactionInternalChkList.ascx" TagName="ComplianceStatusTransactionInternalChkList"
    TagPrefix="vit" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <link href="../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <link href="../NewCSS/fullcalendar.css" rel="stylesheet" />
    <link href="../assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
    <link href="../Newjs/bxslider/jquery.bxslider.css" rel="stylesheet" />
    <link href="../NewCSS/owl.carousel.css" rel="stylesheet" />
    <link rel="stylesheet" href="../NewCSS/responsive-calendar.css" />
    <link rel="stylesheet" href="../NewCSS/font-awesome.min.css" />
    <script type="text/javascript">

        function initializeDatePicker() { }
        function initializeDatePickerforPerformer1() { }
        function initializeDatePickerforPerformer() { }
        function initializeDatePickerforPerformerInternal() { }

        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }

        function CloseCalenderPERPop() {
            fcloseandcallcal();
        }

        function fcloseandcallcal() {
            try {
                parent.fclosepopcal($('#ContentPlaceHolder1_dts').val())

            } catch (e) {

            }
        }
        $(document).ready(function () {
            $('#loaderdiv').hide();
        });

    </script>
    <style type="text/css">
        .clspenaltysave {
            font-weight: bold;
            margin-left: 15px;
        }

        .btnss {
            background-image: url(../Images/edit_icon_new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > thead > tr > th > a {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .Inforamative {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }

        tr.Inforamative > td {
            color: blue !important;
            border-top: 1px solid #dddddd !important;
        }

        .circle {
            /*background-color: green;*/
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }

        /*.modal-body {
    max-height: calc(100vh - 210px);
    overflow-y: auto;
}*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="dts" runat="server" />
    <div>
        <div>
            <div>
                <%--<vit:ComplianceStatusTransactionChkList runat="server" Visible="false" ID="udcStatusTranscatopnChkList" />--%>
                 <iframe id="iInternalChecklistPerformerFrame" runat="server" Visible="false" src="about:blank" width="100%" height="650px" frameborder="0"></iframe>
            </div>
            <div>
                <%--<vit:ComplianceStatusTransactionInternalChkList runat="server" Visible="false" ID="udcComplianceStatusTransactionInternalChkList" />--%>
                 <iframe id="iChecklistPerformerFrame" runat="server" Visible="false" src="about:blank" width="100%" height="650px" frameborder="0"></iframe>
            </div>
             <div>
                <%--<vit:ComplianceStatusTransactionInternalChkList runat="server" Visible="false" ID="udcComplianceStatusTransactionInternalChkList" />--%>
                 <iframe id="iChecklistReviewerFrame" runat="server" Visible="false" src="about:blank" width="100%" height="650px" frameborder="0"></iframe>
            </div>
            <div>
                <%--<vit:ComplianceReviewerStatusTransaction runat="server" Visible="false" ID="udcReviewerStatusTranscatopn" />--%>
                <iframe id="iReviewerFrame" runat="server" Visible="false" src="about:blank" width="100%" height="650px" frameborder="0"></iframe>
            </div>
             <div>
                <%--<vit:ComplianceReviewerStatusTransaction runat="server" Visible="false" ID="udcReviewerStatusTranscatopn" />--%>
                <iframe id="iReviewerFrameLic" runat="server" Visible="false" src="about:blank" width="100%" height="650px" frameborder="0"></iframe>
            </div>
            <div>
                <%--<vit:InternalComplianceReviewerStatusTransaction runat="server" Visible="false" ID="udcStatusTranscationInternal" />--%>
                <iframe id="iInternalReviewerFrame" runat="server" Visible="false" src="about:blank" width="100%" height="650px" frameborder="0"></iframe>
            </div>
             <div>
                <%--<vit:InternalComplianceReviewerStatusTransaction runat="server" Visible="false" ID="udcStatusTranscationInternal" />--%>
                <iframe id="iInternalReviewerFrameLic" runat="server" Visible="false" src="about:blank" width="100%" height="650px" frameborder="0"></iframe>
            </div>
            <div>
                <%-- <vit:ComplianceStatusTransaction runat="server" Visible="false" ID="udcStatusTranscatopn" />--%>
                <iframe id="iPerformerFrame" runat="server" Visible="false" src="about:blank" width="100%" height="650px" frameborder="0"></iframe>
            </div>
              <div>
                <%-- <vit:ComplianceStatusTransaction runat="server" Visible="false" ID="udcStatusTranscatopn" />--%>
                <iframe id="iPerformerFrameLic" runat="server" Visible="false" src="about:blank" width="100%" height="650px" frameborder="0"></iframe>
            </div>
            <div>
                <%--<vit:InternalComplianceStatusTransaction runat="server" Visible="false" ID="udcInternalPerformerStatusTranscation" />--%>
                <iframe id="iInternalPerformerFrame" runat="server" Visible="false" src="about:blank" width="100%" height="650px" frameborder="0"></iframe>
            </div>
             <div>
                <%--<vit:InternalComplianceStatusTransaction runat="server" Visible="false" ID="udcInternalPerformerStatusTranscation" />--%>
                <iframe id="iInternalPerformerFrameLic" runat="server" Visible="false" src="about:blank" width="100%" height="650px" frameborder="0"></iframe>
            </div>
        </div>
    </div>

    <%--    </form>
</body>--%>

    <script type="text/javascript">
        function openModalPerformer(scheduleonId, InstanceId) {
            $('#ContentPlaceHolder1_iPerformerFrame').attr('src', '/controls/compliancestatusperformer.aspx?sId=' + scheduleonId + '&InId=' + InstanceId);
            return true;
        }
        //License performer Popup Call Statutory License
        function openModalPerformerLic(scheduleonId, InstanceId, status) {
            $('#ContentPlaceHolder1_iPerformerFrameLic').attr('src', '/compliances/perCompliancestatusTransaction.aspx?SOID=' + scheduleonId + '&CID=' + InstanceId + "&status=" + status);
            //$('#showdetails').attr('src', "/compliances/perCompliancestatusTransaction.aspx?CID=" + CID + "&SOID=" + SOID + "&status=" + status);
            return true;
        }

        function openModalInternalPerformer(scheduleonId, InstanceId) {
            $('#ContentPlaceHolder1_iInternalPerformerFrame').attr('src', '/controls/InternalComplianceStatusperformer.aspx?sId=' + scheduleonId + '&InId=' + InstanceId);
            return true;
        }

        function openModalInternalPerformerLic(scheduleonId, InstanceId, status) {
            $('#ContentPlaceHolder1_iInternalPerformerFrameLic').attr('src', '/compliances/perInternalCompliancestatusTransaction.aspx?SOID=' + scheduleonId + '&CID=' + InstanceId + "&status=" + status);
            return true;
        }

        function openModalReviewer(scheduleonId, InstanceId) {
            $('#ContentPlaceHolder1_iReviewerFrame').attr('src', '/controls/compliancestatusreviewer.aspx?sId=' + scheduleonId + '&InId=' + InstanceId);
            return true;
        }

         //License Reviewer Popup Call Statutory License
        function openModalReviewerLic(scheduleonId, InstanceId, status) {
            $('#ContentPlaceHolder1_iReviewerFrameLic').attr('src', '/compliances/revCompliancestatusTransaction.aspx?SOID=' + scheduleonId + '&CID=' + InstanceId + "&status=" + status);
            //$('#showdetails').attr('src', "/compliances/revCompliancestatusTransaction.aspx?CID=" + CID + "&SOID=" + SOID + "&status=" + status);
            return true;
        }

        function openModalInternalReviewer(scheduleonId, InstanceId) {
            $('#ContentPlaceHolder1_iInternalReviewerFrame').attr('src', '/controls/InternalComplianceStatusReviewer.aspx?sId=' + scheduleonId + '&InId=' + InstanceId);
            return true;
        }

        function openModalInternalReviewerLic(scheduleonId, InstanceId, status) {
            $('#ContentPlaceHolder1_iInternalReviewerFrameLic').attr('src', '/compliances/RevInternalCompliancestatusTransaction.aspx?SOID=' + scheduleonId + '&CID=' + InstanceId + "&status=" + status);
            return true;
        }

        function openModalChecklistPerformer(scheduleonId, InstanceId) {
            $('#ContentPlaceHolder1_iChecklistPerformerFrame').attr('src', '/controls/checkliststatusperformer.aspx?sId=' + scheduleonId + '&InId=' + InstanceId);
            return true;
        }
        function openModalChecklistReviewer(scheduleonId, InstanceId) {
            $('#ContentPlaceHolder1_iChecklistReviewerFrame').attr('src', '/controls/Checkliststatusreviewer.aspx?sId=' + scheduleonId + '&InId=' + InstanceId);
            return true;
        }

        function openModalInternalChecklistPerformer(scheduleonId, InstanceId) {
            $('#ContentPlaceHolder1_iInternalChecklistPerformerFrame').attr('src', '/controls/internalCheckliststatusperformer.aspx?sId=' + scheduleonId + '&InId=' + InstanceId);
            return true;
        }
    </script>

    <script type="text/javascript" src="../Newjs/bxslider/jquery.bxslider.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

    <!-- charts scripts -->
    <script type="text/javascript" src="../assets/jquery-knob/js/jquery.knob.js"></script>

    <script type="text/javascript" src="../Newjs/owl.carousel.js"></script>
    <!-- jQuery full calendar -->
    <script type="text/javascript" src="../Newjs/fullcalendar.min.js"></script>

    <!--script for this page only-->
    <script type="text/javascript" src="../Newjs/calendar-custom.js"></script>
    <script type="text/javascript" src="../Newjs/jquery.rateit.min.js"></script>
    <!-- custom select -->
    <script type="text/javascript" src="../Newjs/jquery.customSelect.min.js"></script>

    <!--custome script for all page-->
    <%-- <script type="text/javascript" src="../Newjs/scripts.js"></script>--%>
    <!-- custom script for this page-->


    <%--</html>--%>
</asp:Content>
