﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Linq;
using System.Web.Security;
using System.Web;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Collections.Generic;
using OfficeOpenXml;
using System.Text.RegularExpressions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class widget_master : System.Web.UI.Page
    {
        public static List<long?> actList = new List<long?>();                
        private int userId = AuthenticationHelper.UserID;
        protected int UserID = -1;
        public bool MGM_KEy;
        protected static int customerid;
        protected void Page_Load(object sender, EventArgs e)
        {
            bool ISCADMN = false;
            if (AuthenticationHelper.Role == "CADMN")
            {
                ISCADMN = true;
            }
            bool ISIMPL = false;
            if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "RREV")
            {
                ISIMPL = true;
            }
            if (AuthenticationHelper.Role == "MGMT")
            {
                customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            MGM_KEy = CaseManagement.CheckForClient(customerid, "MGMT_CompanyAdmin");
            if (!IsPostBack)
            {
                bool ISMGMT = false;
                if (MGM_KEy && AuthenticationHelper.Role != "EXCT")
                {
                    ISMGMT = true;
                }
                else
                {
                    ISMGMT = false;
                }
                if (HttpContext.Current.Request.IsAuthenticated && (ISMGMT || ISCADMN || ISIMPL))
                {

                    UserID = AuthenticationHelper.UserID;
                    BindCustomers(UserID);
                    BindAct();
                    BindWidgetsDropdown(ddlfilterWidget);
                    BindGrids();
                    BindCustomersDrop(UserID);
                    BindCompliance(actList);
                    actList.Clear();
                }
                else
                {
                    //added by rahul on 12 June 2018 Url Sequrity
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }

                tbxBranch.Text = "< Select Location >";

                if (AuthenticationHelper.Role == "IMPT")
                {
                    UserID = AuthenticationHelper.UserID;
                    customerdiv.Visible = true;
                    lblcustomer.Visible = true;
                    customerdrop.Visible = true;
                    BindCustomers(UserID);
                }
                else
                {
                    customerdiv.Visible = false;
                    lblcustomer.Visible = false;
                    customerdrop.Visible = false;
                    BindGrids();

                    tbxBranch.Text = string.Empty;
                    BindCustomerBranches();

                    //BindCustomersDrop(UserID);
                    //tbxFilterLocation.Text = "< Select >";
                }
            }


        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "< Select Branch >";
                ScriptManager.RegisterStartupScript(this.upWidget, this.upWidget.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
          
            BindWidgetsDropdown(ddlfilterWidget);
            BindGrids();
            BindAct();
            BindCompliance(actList);
        }

        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomerNew.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                if (customerID != -1)
                {
                    tvBranches.Nodes.Clear();
                    NameValueHierarchy branch = null;
                    var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                    if (branchs.Count > 0)
                    {
                        branch = branchs[0];
                    }
                    tbxBranch.Text = "< Select Location >";
                    List<TreeNode> nodes = new List<TreeNode>();
                    BindBranchesHierarchy(null, branch, nodes);
                    foreach (TreeNode item in nodes)
                    {
                        tvBranches.Nodes.Add(item);
                    }
                    tvBranches.CollapseAll();
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }

        private void BindCustomers(int userid)
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                ddlCustomer.DataSource = Assigncustomer.GetAllCustomer(userid);
                ddlCustomer.DataBind();

                ddlCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
        private void BindWidgetsDropdown(DropDownList ddlList)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                ddlList.DataTextField = "WidgetName";
                ddlList.DataValueField = "ID";

                ddlList.DataSource = WidgetModalclass.GetAll(customerID);
                ddlList.DataBind();

                ddlList.Items.Insert(0, new ListItem("< Select Widget>", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry2.IsValid = false;
                cvDuplicateEntry2.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindCompliance(List<long?> ActList)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomerNew.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                
                rptCompliances.DataSource = WidgetModalclass.GetAllCompliance(customerID, ActList).ToList();
                rptCompliances.DataBind();
                actList.Clear();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindAct()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomerNew.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                rptActs.DataSource = WidgetModalclass.GetAllACTs(customerID);
                rptActs.DataBind();

                foreach (RepeaterItem aItem in rptActs.Items)
                {
                    CheckBox chkACT = (CheckBox)aItem.FindControl("chkACT");

                    if (!chkACT.Checked)
                    {
                        chkACT.Checked = true;
                    }
                }
                CheckBox ActSelectAll = (CheckBox)rptActs.Controls[0].Controls[0].FindControl("ActSelectAll");
                ActSelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindGrids()
        {
            try
            {
                int customerID = -1;
                //string widgetname = "";
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    //widgetname = Convert.ToString(Convert.ToUInt32(ddlfilterWidget.SelectedValue));
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                int widgetid = -1;
                if (ddlfilterWidget.SelectedValue != "-1")
                {
                    widgetid = Convert.ToInt32(ddlfilterWidget.SelectedValue);
                }
                //string widgetname = Convert.ToString(ViewState["Widgetname"]);
                var WidgetMasterList = WidgetModalclass.GetAll(customerID);
                WidgetMasterList = WidgetMasterList.Where(a => a.CustomerID == customerID).ToList();
                //WidgetMasterList = WidgetMasterList.Where(a => a.WidgetName == widgetname).ToList();

                if (Convert.ToInt32(ddlfilterWidget.SelectedValue) != -1)
                {
                    WidgetMasterList = WidgetMasterList.Where(entry => entry.ID == widgetid).ToList();
                }

                if (!string.IsNullOrEmpty(tbxFilter.Text))
                { 
                    if (CheckInt(tbxFilter.Text))
                    {
                        int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                        WidgetMasterList = WidgetMasterList.Where(entry => entry.ID == a).ToList();
                    }
                    else
                    {
                        WidgetMasterList = WidgetMasterList.Where(entry => entry.WidgetName.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                    }
                }
                grdWidget.DataSource = WidgetMasterList;
                grdWidget.DataBind();
                upWidget.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomDuplicateEntry.IsValid = false;
                CustomDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdWidget_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_Widget"))
                {
                    int WidgetID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["ID"] = WidgetID;
                    WidgetMaster widgetmaster = WidgetModalclass.GetByID(WidgetID);
                    //BindCustomersDrop(UserID);
                    //ddlCustomerNew.SelectedValue =Convert.ToString(widgetmaster.CustomerID);

                  

                    if (AuthenticationHelper.Role == "IMPT")
                    {
                        UserID = AuthenticationHelper.UserID;
                        customerdiv.Visible = true;
                        lblcustomer.Visible = true;
                        customerdrop.Visible = true;
                        BindCustomers(UserID);
                        ddlCustomerNew.SelectedValue = Convert.ToString(widgetmaster.CustomerID);
                    }
                    else
                    {
                        customerdiv.Visible = false;
                        lblcustomer.Visible = false;
                        customerdrop.Visible = false;
                        BindGrids();

                        tbxBranch.Text = string.Empty;
                        BindCustomerBranches(); 
                    }

                    var branchid = WidgetModalclass.GetWidgetDetail(WidgetID);

                    if (branchid !=0)
                    {
                        Queue<TreeNode> queue = new Queue<TreeNode>();
                        foreach (TreeNode node in tvBranches.Nodes)
                        {
                            queue.Enqueue(node);
                        }
                        while (queue.Count > 0)
                        {
                            TreeNode node = queue.Dequeue();
                            if (node.Value == branchid.ToString())
                            {
                                node.Selected = true;
                                break;
                            }
                            else
                            {
                                foreach (TreeNode child in node.ChildNodes)
                                {
                                    queue.Enqueue(child);
                                }
                            }
                        }
                        tvBranches_SelectedNodeChanged(null, null);
                    }

                    tbxName.Text = widgetmaster.WidgetName;
                    tbxName.ToolTip = widgetmaster.WidgetName;
                    #region Act
                    txtACTName.Text = "< Select Act >";
                    var vGetActMappedIDs = WidgetModalclass.GetWidgetActMappedID(WidgetID);
                    foreach (RepeaterItem aItem in rptActs.Items)
                    {
                        CheckBox chkACT = (CheckBox)aItem.FindControl("chkACT");
                        chkACT.Checked = false;
                        CheckBox ActSelectAll = (CheckBox)rptActs.Controls[0].Controls[0].FindControl("ActSelectAll");

                        for (int i = 0; i <= vGetActMappedIDs.Count - 1; i++)
                        {
                            if (((Label)aItem.FindControl("lblActID")).Text.Trim() == vGetActMappedIDs[i].ToString())
                                chkACT.Checked = true;
                        }
                        if ((rptActs.Items.Count) == (vGetActMappedIDs.Count))
                            ActSelectAll.Checked = true;
                        else
                            txtComplince.Text = "<Select Compliance>";
                            //ActSelectAll.Checked = false;
                    }
                    foreach (var item in vGetActMappedIDs)
                    {
                        int actID = Convert.ToInt32(item);
                        actList.Add(actID);
                    }
                    if (actList.Count >= 0)
                    {
                        BindCompliance(actList);
                    }
                    #endregion


                    #region Complince  

                    txtComplince.Text = "<Select Compliance>";
                    var vGetComplinceMappedIDs = WidgetModalclass.GetWidgetComplinceMappedID(WidgetID);
                    foreach (RepeaterItem aItem1 in rptCompliances.Items)
                    {
                        CheckBox chkcompliance = (CheckBox)aItem1.FindControl("chkcompliance");
                        chkcompliance.Checked = false;
                        CheckBox ComplianceSelectAll = (CheckBox)rptCompliances.Controls[0].Controls[0].FindControl("ComplianceSelectAll");

                        for (int i = 0; i <= vGetComplinceMappedIDs.Count - 1; i++)
                        {
                            if (((Label)aItem1.FindControl("lblcomplianceid")).Text.Trim() == vGetComplinceMappedIDs[i].ToString())
                                chkcompliance.Checked = true;
                        }
                        if ((rptCompliances.Items.Count) == (vGetComplinceMappedIDs.Count))
                            ComplianceSelectAll.Checked = true;
                        else
                            ComplianceSelectAll.Checked = false;
                    }

                    #endregion
                   
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divActDialog\").dialog('open')", true);
                    upWidget.Update();
                }
                else if (e.CommandName.Equals("DELETE_Widget"))
                {
                    int WidgetID = Convert.ToInt32(e.CommandArgument);
                    WidgetModalclass.DeleteWidgetData(WidgetID);
                    BindGrids();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomDuplicateEntry.IsValid = false;
                CustomDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

      

        protected void grdWidget_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdWidget.PageIndex = e.NewPageIndex;
                BindGrids();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomDuplicateEntry.IsValid = false;
                CustomDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddWidget_Click(object sender, EventArgs e)
        {
            try
            {
                //BindCustomersDrop(UserID);
                ViewState["Mode"] = 0;
                saveopo.Value = "false";
                tbxName.Text = string.Empty;
                txtACTName.Text = "< Select Act >";
                foreach (RepeaterItem aItem in rptActs.Items)
                {
                    CheckBox chkACT = (CheckBox)aItem.FindControl("chkACT");
                    chkACT.Checked = false;
                    CheckBox ActSelectAll = (CheckBox)rptActs.Controls[0].Controls[0].FindControl("ActSelectAll");
                    ActSelectAll.Checked = false;
                }

                txtComplince.Text = "< Select Compliance>";
                foreach (RepeaterItem aItem in rptCompliances.Items)
                {
                    CheckBox chkcompliance = (CheckBox)aItem.FindControl("chkcompliance");
                    chkcompliance.Checked = false;
                    CheckBox ComplianceSelectAll = (CheckBox)rptCompliances.Controls[0].Controls[0].FindControl("ComplianceSelectAll");
                    ComplianceSelectAll.Checked = false;
                }
                upWidget.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divActDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomDuplicateEntry.IsValid = false;
                CustomDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlfilterWidget_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //string type = WidgetModalclass.GetByID(Convert.ToInt32(ddlfilterWidget.SelectedValue)).WidgetName;
                //ViewState["Widgetname"] = type;
                //if (type!=null)
                //{    
                        BindGrids();  
                //}
                       
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomDuplicateEntry.IsValid = false;
                CustomDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }



        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            foreach (RepeaterItem aItem in rptActs.Items)
            {
                CheckBox chk = (CheckBox)aItem.FindControl("chkACT");

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (chk.Checked)
                    {
                        int actID = Convert.ToInt32(((Label)aItem.FindControl("lblActID")).Text.Trim());
                        actList.Add(actID);
                    }
                }
            }
            BindCompliance(actList);

 
        }

        protected void btnRefreshnew_Click(object sender, EventArgs e)
        {
 
        }


        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdWidget.PageIndex = 0;
                BindGrids();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomDuplicateEntry.IsValid = false;
                CustomDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void txtACTName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //txtComplince.Clear();
                grdWidget.PageIndex = 0;
                BindCompliance(actList);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomDuplicateEntry.IsValid = false;
                CustomDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
                if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = Convert.ToInt32(ddlCustomerNew.SelectedValue);
                }
                else
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                customerID = Convert.ToInt32(customerID);

              

                if (customerID != -1)
                {
                    WidgetMaster obj = new WidgetMaster()
                    {
                        WidgetName = tbxName.Text.Trim(),
                        CreatedBy = AuthenticationHelper.UserID,
                        IsDeleted = false,
                        CreatedDate = DateTime.Now,
                        CustomerID = (int)customerID,
                    };

                 

                    if ((int)ViewState["Mode"] == 1)
                    {
                        obj.ID = Convert.ToInt32(ViewState["ID"]);
                    }
                    if ((int)ViewState["Mode"] == 0)
                    {
                        if ((WidgetExist(obj)))
                        {
                            saveopo.Value = "true";
                            CustomDuplicateEntry.IsValid = false;
                            CustomDuplicateEntry.ErrorMessage = "Widget Name already exists";
                            // vsLicenseListPage.CssClass = "alert alert-success";
                        }
                        else
                        {
                            CreateWidget(obj);
                            foreach (RepeaterItem aItem in rptCompliances.Items)
                            {
                                CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkCompliance");
                                if (chkCompliance.Checked)
                                {
                                    int BranchID = -1;
                                    if (tvBranches.SelectedNode.Value != "-1" && tvBranches.SelectedNode.Value != null)
                                    {
                                       BranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                                    }

                                    var cid = Convert.ToInt32(((Label)aItem.FindControl("lblcomplianceid")).Text.Trim());
                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                    {
                                        // var actid = (from row in entities.Compliances where row.ID == cid select row.ActID).FirstOrDefault();
                                        var actid = (from row in entities.Compliances where row.ID == cid select row.ActID).FirstOrDefault();
                                        WidgetDetail objcategory = new WidgetDetail()
                                        {
                                            WidgetID = obj.ID,
                                            ActID = actid,
                                            ComplianceID = cid,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            IsDeleted = false,
                                            CreatedDate = DateTime.Now,
                                            BranchID = BranchID,
                                    };
                                        entities.WidgetDetails.Add(objcategory);
                                        entities.SaveChanges();
                                    }
                                }
                            }
                            saveopo.Value = "true";
                            CustomDuplicateEntry.IsValid = false;
                            CustomDuplicateEntry.ErrorMessage = "Widget Details Saved Successfully.";
                            //vsLicenseListPage.CssClass = "alert alert-success";

                        }
                    }
                    if ((int)ViewState["Mode"] == 1)
                    {
                        var Groupdetails = WidgetModalclass.GetByID(obj.ID);
                        List<WidgetDetail> objwidget = new List<WidgetDetail>();
                        if (Groupdetails != null)
                        {
                            if (Groupdetails.WidgetName == obj.WidgetName)
                            {
                                WidgetModalclass.UpdateWidgetMaster(obj);
                                foreach (RepeaterItem aItem in rptCompliances.Items)
                                {
                                    CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkCompliance");
                                    if (chkCompliance.Checked)
                                    {
                                        int BranchID = -1;
                                        if (tvBranches.SelectedNode.Value != "-1" && tvBranches.SelectedNode.Value != null)
                                        {
                                            BranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                                        }

                                        var cid = Convert.ToInt32(((Label)aItem.FindControl("lblcomplianceid")).Text.Trim());
                                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                        {
                                            var actid = (from row in entities.Compliances where row.ID == cid select row.ActID).FirstOrDefault();
                                            WidgetDetail objcategory = new WidgetDetail()
                                            {
                                                WidgetID = obj.ID,
                                                ActID = actid,
                                                ComplianceID = cid,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                IsDeleted = false,
                                                CreatedDate = DateTime.Now,
                                                BranchID =BranchID,
                                            };
                                            // entities.WidgetDetails.Add(objcategory);
                                            objwidget.Add(objcategory);
                                            WidgetModalclass.CreateOrUpdateWidgetMapping(objwidget, actid, true);
                                            entities.SaveChanges();
                                            saveopo.Value = "true";
                                            CustomDuplicateEntry.IsValid = false;
                                            CustomDuplicateEntry.ErrorMessage = "Widget details updated successfully";
                                            //vsLicenseListPage.CssClass = "alert alert-success";  
                                        }
                                    }
                                }
                            }
                            else
                            {
                                var GroupdetailsName = WidgetModalclass.WidgetDetailByName(obj.WidgetName, customerID);

                                if (GroupdetailsName == null)
                                {
                                    WidgetModalclass.UpdateWidgetMaster(obj);
                                    List<WidgetDetail> widgetlist = new List<WidgetDetail>();
                                    foreach (RepeaterItem aItem in rptCompliances.Items)
                                    {
                                        CheckBox chkCompliance = (CheckBox)aItem.FindControl("chkCompliance");
                                        if (chkCompliance.Checked)
                                        {
                                            int BranchID = -1;
                                            if (tvBranches.SelectedNode.Value != "-1" && tvBranches.SelectedNode.Value != null)
                                            {
                                                BranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                                            }


                                            var cid = Convert.ToInt32(((Label)aItem.FindControl("lblcomplianceid")).Text.Trim());
                                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                            {
                                                var actid = (from row in entities.Compliances where row.ID == cid select row.ActID).FirstOrDefault();
                                                WidgetDetail objcategory = new WidgetDetail()
                                                {
                                                    WidgetID = obj.ID,
                                                    ActID = actid,
                                                    ComplianceID = cid,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    IsDeleted = false,
                                                    CreatedDate = DateTime.Now,
                                                    BranchID = BranchID,
                                                };
                                                //  entities.WidgetDetails.Add(objcategory);
                                                widgetlist.Add(objcategory);
                                                entities.SaveChanges();

                                            }
                                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                            {
                                                int customerID1 = -1;
                                                if (AuthenticationHelper.Role == "IMPT")
                                                {
                                                    customerID1 = Convert.ToInt32(ddlCustomer.SelectedValue);
                                                }
                                                else
                                                {
                                                    customerID1= Convert.ToInt32(AuthenticationHelper.CustomerID);
                                                }

                                                var mgmtuserlist = entities.Users.Where(entry => entry.CustomerID == customerID1 && entry.RoleID == 8).ToList();
                                                foreach (var item in mgmtuserlist)
                                                {
                                                    var widget = entities.widgets.Where(en => en.UserId == item.ID).FirstOrDefault();
                                                    if (widget != null)
                                                    {
                                                        widget.CustomWidget = true;
                                                        entities.SaveChanges();
                                                    }
                                                }
                                            }
                                            saveopo.Value = "true";
                                            CustomDuplicateEntry.IsValid = false;
                                            CustomDuplicateEntry.ErrorMessage = "Widget details updated successfully";
                                            
                                        }
                                    }

                                }
                                else
                                {
                                    saveopo.Value = "true";
                                    CustomDuplicateEntry.IsValid = false;
                                    CustomDuplicateEntry.ErrorMessage = "Widget Name Already Exist";
                                }
                            }
                            clearAll();
                        }
                    }
                    BindGrids();
                    BindWidgetsDropdown(ddlfilterWidget);
                    upWidgetList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomDuplicateEntry.IsValid = false;
                CustomDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void clearAll()
        {
            tbxName.Text = string.Empty;
        }
        public static int CreateWidget(WidgetMaster dept)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                entities.WidgetMasters.Add(dept);
                entities.SaveChanges();
                return dept.ID;
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divActDialog\").dialog('close')", true);
            BindGrids();
        }
        public static bool WidgetExist(WidgetMaster deptmaster)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.WidgetMasters
                             where
                             row.WidgetName.ToUpper().Trim() == deptmaster.WidgetName.ToUpper().Trim()
                             && row.CustomerID == deptmaster.CustomerID
                             select row);
                return query.Select(entry => true).FirstOrDefault();
            }
        }
        public long WidgetNameExists(int customerid, string questionname)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.WidgetMasters
                             where row.IsDeleted == false
                             && row.CustomerID == customerid
                             && row.WidgetName.ToUpper().Trim().Equals(questionname.ToUpper().Trim())
                             select row.ID).FirstOrDefault();
                return query;
            }
        }

        protected void upWidget_Load(object sender, EventArgs e)
        {
            try
            {                                
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeACTList", string.Format("initializeJQueryUI('{0}', 'dvACT');", txtACTName.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeComplianceList", string.Format("initializeJQueryUI1('{0}', 'dvCompliance');", txtComplince.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideACTList", "$(\"#dvACT\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideComplianceList", "$(\"#dvCompliance\").hide(\"blind\", null, 5, function () { });", true);
                ScriptManager.RegisterStartupScript(this.upWidget, this.upWidget.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry2.IsValid = false;
                cvDuplicateEntry2.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private bool WidgetSampleSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("WidgetSample"))
                    {
                        if (sheet.Name.Trim().Equals("WidgetSample"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry2.IsValid = false;
                cvDuplicateEntry2.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;
            finalErrMsg += "<ol type='1'>";
            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry2.IsValid = false;
            cvDuplicateEntry2.ErrorMessage = finalErrMsg;
        }


        public bool CreateExcelWidgetDetailMapping(List<WidgetDetail> TempassignmentTable)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempassignmentTable.ForEach(entry =>
                    {

                        entities.WidgetDetails.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        private void ProcessWidgetData(ExcelPackage xlWorkbook)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                try
                {
                   
                    List<WidgetDetail> widgetdetailslist = new List<WidgetDetail>();

                    bool sucessmsg = false;
                    int customerID = -1;

                    
                    if (AuthenticationHelper.Role == "IMPT")
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                    else
                    {
                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }

                    ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["WidgetSample"];
                    if (xlWorksheet != null)
                    {
                        List<string> errorMessage = new List<string>();
                        List<string> uploded_data = new List<string>();
                        int count = 0;
                        int xlrow2 = xlWorksheet.Dimension.End.Row;

                        string valWidgetName = string.Empty;
                        int valACTID = -1;
                        int valComplianceID = -1;
                        int valBranchID = -1;
                        #region Validations
                        for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                        {
                            count = count + 1;
                            valWidgetName = string.Empty;
                            valACTID = -1;
                            valComplianceID = -1;

                            #region 1 Widget Name
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString().Trim()))
                            {
                                valWidgetName = Convert.ToString(xlWorksheet.Cells[rowNum, 1].Text).Trim();
                            }
                            if (String.IsNullOrEmpty(valWidgetName))
                            {
                                errorMessage.Add("Required Widget Name at row number - " + (count + 1) + "");
                            }
                            #endregion

                            #region 2 ACTID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString().Trim()))
                            {
                                valACTID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 2].Text.Trim());
                            }
                            if (valACTID == -1 || valACTID == 0)
                            {
                                errorMessage.Add("Required ACTID at row number-" + rowNum);
                            }
                            else
                            {
                                if (ActExists(valACTID) == false)
                                {
                                    errorMessage.Add("ACTID not defined in the System  at row number-" + rowNum);
                                }
                            }
                            #endregion

                            #region 3 ComplianceID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 3].Text.ToString().Trim()))
                            {
                                valComplianceID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 3].Text.Trim());
                            }
                            if (valComplianceID == -1 || valComplianceID == 0)
                            {
                                errorMessage.Add("Required ComplianceID at row number-" + rowNum);
                            }
                            else
                            {
                                if (ComplainceExists(valComplianceID, valACTID) == false)
                                {
                                    errorMessage.Add("ComplianceID not defined in the System or not mapped to this customer at row number-" + rowNum);
                                }
                            }
                            #endregion

                            #region 3 BranchID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 4].Text.ToString().Trim()))
                            {
                                valBranchID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 4].Text.Trim());
                            }
                            if (valBranchID == -1 || valBranchID == 0 || valBranchID == 0)
                            {
                                errorMessage.Add("Required BranchID at row number-" + rowNum);
                            }
                            else
                            {
                                if (BranchExists(valBranchID) == false)
                                {
                                    errorMessage.Add("BranchID not defined in the System at row number-" + rowNum);
                                }
                            }
                            #endregion
                        }
                        #endregion
                        if (errorMessage.Count > 0)
                        {
                            ErrorMessages(errorMessage);
                        }
                        else
                        {
                            #region Save

                            for (int rowNum1 = 2; rowNum1 <= xlrow2; rowNum1++)
                            {
                                count = count + 1;
                                string WidgetName = string.Empty;
                                int ACTID = -1;
                                int ComplianceID = -1;
                                int BranchID = -1;
                                #region 1 Widget Name
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 1].Text.ToString().Trim()))
                                {
                                    WidgetName = Convert.ToString(xlWorksheet.Cells[rowNum1, 1].Text).Trim();
                                }
                                #endregion

                                #region 2 ACTID
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 2].Text.ToString().Trim()))
                                {
                                    ACTID = Convert.ToInt32(xlWorksheet.Cells[rowNum1, 2].Text.Trim());
                                }

                                #endregion

                                #region 3 ComplianceID
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 3].Text.ToString().Trim()))
                                {
                                    ComplianceID = Convert.ToInt32(xlWorksheet.Cells[rowNum1, 3].Text.Trim());
                                }
                                #endregion

                                #region 3 BranchID
                                if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum1, 4].Text.ToString().Trim()))
                                {
                                    BranchID = Convert.ToInt32(xlWorksheet.Cells[rowNum1,4].Text.Trim());
                                }
                                #endregion

                                if (customerID != -1)
                                {

                                    var Cquid = WidgetNameExists(customerID, WidgetName.Trim());
                                    if (Cquid == 0)
                                    {
                                        WidgetMaster WM = new WidgetMaster();
                                        WM.WidgetName = WidgetName.Trim();
                                        WM.CustomerID = (int)customerID;
                                        WM.CreatedDate = DateTime.Now;
                                        WM.CreatedBy = AuthenticationHelper.UserID;
                                        WM.IsDeleted = false;

                                        entities.WidgetMasters.Add(WM);
                                        entities.SaveChanges();
                                        Cquid = WM.ID;
                                    }
                                    WidgetDetail objcategory = new WidgetDetail()
                                    {
                                        WidgetID = (int)Cquid,
                                        ActID = ACTID,
                                        ComplianceID = ComplianceID,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        IsDeleted = false,
                                        CreatedDate = DateTime.Now,
                                        BranchID = BranchID,
                                    };
                                    widgetdetailslist.Add(objcategory);
                                }
                            }
                            if (widgetdetailslist.Count>0)
                            {
                                sucessmsg=CreateExcelWidgetDetailMapping(widgetdetailslist);
                                var mgmtuserlist = entities.Users.Where(entry => entry.CustomerID == customerID && entry.RoleID == 8).ToList();
                                foreach (var item in mgmtuserlist)
                                {
                                    var widget = entities.widgets.Where(en => en.UserId == item.ID).FirstOrDefault();
                                    if (widget !=null)
                                    {
                                        widget.CustomWidget = true;
                                        entities.SaveChanges(); 
                                    }
                                }
                            }
                            #endregion

                            if (sucessmsg)
                            {
                                cvDuplicateEntry2.IsValid = false;
                                cvDuplicateEntry2.ErrorMessage = "Widget Details saved successfully";
                            }
                            else
                            {
                                cvDuplicateEntry2.IsValid = false;
                                cvDuplicateEntry2.ErrorMessage = "No Data Uploaded from Excel Document";
                            }
                        }                                               
                    }
                    else
                    {
                        cvDuplicateEntry2.IsValid = false;
                        cvDuplicateEntry2.ErrorMessage = "Error uploading file. Please try again";
                    }
                    BindGrids();
                    upWidgetList.Update();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry2.IsValid = false;
                    cvDuplicateEntry2.ErrorMessage = "Something went wrong, Please try again later";
                }
            }
        }


        public bool ComplainceExists(int ComplianceID,int vACTID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<SP_WidgetComplianceMaster_Result>)Cache["WidgetComplianceListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where  row.ActID== vACTID && row.ID == ComplianceID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public bool BranchExists(int BranchID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<NameValue>)Cache["WidgetBranchListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == BranchID  
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public bool ActExists(int ActID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<SP_WidgetActMaster_Result>)Cache["WidgetActListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == ActID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        public static void GetCompliance(int customerID)
        {
            List<SP_WidgetComplianceMaster_Result> Records = new List<SP_WidgetComplianceMaster_Result>();

            var ComplianceList = HttpContext.Current.Cache["WidgetComplianceListData"];
            
            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {                    
                    Records = (from row in entities.SP_WidgetComplianceMaster(customerID)                              
                                 select row).ToList();                
                    HttpContext.Current.Cache.Insert("WidgetComplianceListData", Records); // add it to cache
                }
            }
        }

        public static void GetBranchList(int customerID)
        {
            List<NameValue> Records = new List<NameValue>();
            if (customerID != -1)
            {
                Records = CustomerBranchManagement.GetAllNVP(customerID);
            }

            HttpContext.Current.Cache.Insert("WidgetBranchListData", Records); // add it to cache

        }
        public static void GetACT(int customerID)
        {

          
            List<SP_WidgetActMaster_Result> Records = new List<SP_WidgetActMaster_Result>();

            var ComplianceList = HttpContext.Current.Cache["WidgetActListData"];
            
            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = WidgetModalclass.GetAllACTs(customerID);

                    HttpContext.Current.Cache.Insert("WidgetActListData", Records); // add it to cache
                }
            }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {

            try
            {
                if (fnuploadwidget.HasFile)
                {
                    try
                    {

                        string filename = Path.GetFileName(fnuploadwidget.FileName);
                        fnuploadwidget.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                        FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());

                        if (excelfile != null)
                        {
                            using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                            {
                                bool flag = WidgetSampleSheetsExitsts(xlWorkbook, "WidgetSample");
                                if (flag == true)
                                {                                                
                                    if (HttpContext.Current.Cache.Get("WidgetComplianceListData") != null)
                                    {
                                        HttpContext.Current.Cache.Remove("WidgetComplianceListData");
                                    }
                                    if (HttpContext.Current.Cache.Get("WidgetActListData") != null)
                                    {
                                        HttpContext.Current.Cache.Remove("WidgetActListData");
                                    }
                                    if (HttpContext.Current.Cache.Get("WidgetBranchListData") != null)
                                    {
                                        HttpContext.Current.Cache.Remove("WidgetBranchListData");
                                    }

                                    int customerID = -1;

                                    if (AuthenticationHelper.Role == "IMPT")
                                    {
                                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                                    }
                                    else
                                    {
                                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                    }

                                    GetCompliance(customerID);
                                    GetACT(customerID);

                                    GetBranchList(customerID);
                                    ProcessWidgetData(xlWorkbook);
                                    if (HttpContext.Current.Cache.Get("WidgetComplianceListData") != null)
                                    {
                                        HttpContext.Current.Cache.Remove("WidgetComplianceListData");
                                    }
                                    if (HttpContext.Current.Cache.Get("WidgetActListData") != null)
                                    {
                                        HttpContext.Current.Cache.Remove("WidgetActListData");
                                    }
                                    if (HttpContext.Current.Cache.Get("WidgetBranchListData") != null)
                                    {
                                        HttpContext.Current.Cache.Remove("WidgetBranchListData");
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry2.IsValid = false;
                                    cvDuplicateEntry2.ErrorMessage = "Please correct the sheet name.";
                                }

                            }
                        }
                        else
                        {
                            cvDuplicateEntry2.IsValid = false;
                            cvDuplicateEntry2.ErrorMessage = "Error uploading file. Please try again.";
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        cvDuplicateEntry2.IsValid = false;
                        cvDuplicateEntry2.ErrorMessage = "Server Error Occurred. Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry2.IsValid = false;
                cvDuplicateEntry2.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
       
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected void ddlCustomerNew_SelectedIndexChanged(object sender, EventArgs e)
        {
            UserID = AuthenticationHelper.UserID;
            //BindCustomersDrop(UserID);
            tbxBranch.Text = string.Empty;
            BindCustomerBranches();
            BindAct();
            BindCompliance(actList);

            ScriptManager.RegisterStartupScript(this.upWidget, this.upWidget.GetType(), "HideTreeViewCustomerChange", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);

        }
        private void BindCustomersDrop(int userid)
        {
            try
            {
                ddlCustomerNew.DataTextField = "Name";
                ddlCustomerNew.DataValueField = "ID";

                ddlCustomerNew.DataSource = Assigncustomer.GetAllCustomer(userid);
                ddlCustomerNew.DataBind();

                ddlCustomerNew.Items.Insert(0, new ListItem("< select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }
    }
}