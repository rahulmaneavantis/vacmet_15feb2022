﻿<%@ Page Title="Check List" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="CompanyAdminChList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Compliances.CompanyAdminChList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div onselectstart="return false;" style="-moz-user-select: none;">
        <asp:UpdatePanel ID="upCompliancesList" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td>
                            <label style="font-weight: bold;">Checklist Compliances</label>
                        </td>

                    </tr>
                </table>
               <div>
                   <asp:Panel ID="Panel1" Width="100%" Height="420px" ScrollBars="Vertical" runat="server">
                       <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" AllowSorting="true"
                           GridLines="Both" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid"
                           BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="13" OnSorting="grdComplianceTransactions_Sorting"
                           Width="100%" Font-Size="12px" DataKeyNames="ScheduledOnID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging"
                           OnRowDataBound="grdComplianceTransactions_RowDataBound" OnRowCreated="grdComplianceTransactions_RowCreated">
                           <Columns>
                               <asp:TemplateField HeaderText="Location-wise Compliance" ItemStyle-Width="30%" SortExpression="Description">
                                   <ItemTemplate>
                                       <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 450px;">
                                           <asp:Label ID="Label2" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                       </div>
                                   </ItemTemplate>
                               </asp:TemplateField>
                               <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" HeaderText="Risk Category">
                                   <ItemTemplate>
                                       <asp:Image runat="server" ID="imtemplat" />
                                       <asp:Label ID="lblComplianceId" runat="server" Text='<%# Eval("ComplianceID") %>' Visible="false"></asp:Label>
                                       <asp:Label ID="lblRisk" runat="server" Text='<%# Eval("Risk") %>' Visible="false"></asp:Label>
                                       <asp:Label ID="lblInstanceID" runat="server" Text='<%# Eval("ComplianceInstanceID") %>' Visible="false"></asp:Label>
                                       <asp:Label ID="lblScheduleOnID" runat="server" Text='<%# Eval("ScheduledOnID") %>' Visible="false"></asp:Label>
                                       <asp:Label ID="lblCheckListTypeID" runat="server" Text='<%# Eval("CheckListTypeID") %>' Visible="false"></asp:Label>
                                       <%--<asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>' Visible="false"></asp:Label>--%>
                                      <%-- <asp:Label ID="lblFilePath" runat="server" Text='<%# Eval("FilePath") %>' Visible="false"></asp:Label>--%>
                                   </ItemTemplate>
                               </asp:TemplateField>
                               <%-- <asp:TemplateField HeaderText="Impact" ItemStyle-Width="20%" SortExpression="NonComplianceType">
                    <ItemTemplate>
                            <asp:Label ID="lblImpact" runat="server" Text='<%# Eval("NonComplianceType") %>'></asp:Label>
                            <asp:Label ID="lblRisk" runat="server" Text='<%# Eval("Risk") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblComplianceId" runat="server" Text='<%# Eval("ComplianceID") %>' Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                               <%--<asp:TemplateField HeaderText="Due Date" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblScheduledOn" runat="server" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                               <asp:TemplateField HeaderText="For Month" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="ForMonth">
                                   <ItemTemplate>
                                       <%-- <%# Eval("ForMonth") %>--%>
                                       <asp:Label ID="lblForMonth" runat="server" Text='<%# Eval("ForMonth") %>'></asp:Label>
                                   </ItemTemplate>
                               </asp:TemplateField>
                               <asp:TemplateField HeaderText="Scheduled On" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn">
                                   <ItemTemplate>
                                       <asp:Label ID="lblScheduledOn" runat="server" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>' Visible="false"></asp:Label>
                                   </ItemTemplate>
                               </asp:TemplateField>
                               <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Height="20px" SortExpression="Status" ItemStyle-Width="20%" />
                               <asp:TemplateField HeaderText="Event" ItemStyle-Width="5%" SortExpression="EventID">
                                   <ItemTemplate>
                                       <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                           <asp:Label ID="lblEventID" runat="server" Text='<%# ShowType((long?)Eval("EventID")) %>' ToolTip='<%# ShowType((long?)Eval("EventID")) %>'></asp:Label>
                                       </div>
                                   </ItemTemplate>
                               </asp:TemplateField>
                               <asp:TemplateField HeaderText="Event Name" ItemStyle-Width="25%" SortExpression="EventName">
                                   <ItemTemplate>
                                       <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                           <asp:Label ID="lblEventName" runat="server" Text='<%# Eval("EventName") %>' ToolTip='<%# Eval("EventName") %>'></asp:Label>
                                       </div>
                                   </ItemTemplate>
                               </asp:TemplateField>
                               <asp:TemplateField HeaderText="Event Nature" ItemStyle-Width="30%" SortExpression="EventNature">
                                   <ItemTemplate>
                                       <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                           <asp:Label ID="lblEventNature" runat="server" Text='<%# Eval("EventNature") %>' ToolTip='<%# Eval("EventNature") %>'></asp:Label>
                                       </div>
                                   </ItemTemplate>
                               </asp:TemplateField>
                               <asp:TemplateField HeaderText="Reference Material" ItemStyle-Width="20%" SortExpression="ReferenceMaterialText">
                                   <ItemTemplate>
                                       <asp:UpdatePanel ID="Updownload" runat="server">
                                           <ContentTemplate>
                                               <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                   <asp:LinkButton ID="lnkDownload" Visible="false" runat="server" Text="Download" OnClick="lnkDownload_Click"></asp:LinkButton>
                                                   <asp:Label ID="lblReferenceMaterialText" Visible="false" runat="server" Text="Text" ToolTip='<%# Eval("ReferenceMaterialText") %>'></asp:Label>
                                               </div>
                                           </ContentTemplate>
                                           <Triggers>
                                               <asp:PostBackTrigger ControlID="lnkDownload" />
                                           </Triggers>
                                       </asp:UpdatePanel>
                                   </ItemTemplate>
                               </asp:TemplateField>
                               <%-- <asp:TemplateField ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                         <ItemTemplate>
                        <asp:LinkButton ID="btnChangeStatus" runat="server" Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("ComplianceStatusID")) %>'
                            CommandName="CHANGE_STATUS" CommandArgument='<%# Eval("ScheduledOnID") + "," + Eval("ComplianceInstanceID") %>'><img src='<%# ResolveUrl("~/Images/change_status_icon.png")%>' alt="Change Status" title="Change Status" /></asp:LinkButton>
                         </ItemTemplate>
                         <HeaderStyle HorizontalAlign="Left" />
                         </asp:TemplateField>--%>
                               <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                   <HeaderTemplate>
                                       <asp:CheckBox ID="chkCompletedSelectAll" Text="Completed" runat="server" AutoPostBack="true" OnCheckedChanged="chkCompletedSelectAll_CheckedChanged" />
                                       <%--onclick="javascript:SelectheaderCheckboxes(this)"--%>
                                   </HeaderTemplate>
                                   <ItemTemplate>
                                       <asp:CheckBox ID="chkCompleted" runat="server" AutoPostBack="true" OnCheckedChanged="chkCompleted_CheckedChanged" />
                                   </ItemTemplate>
                               </asp:TemplateField>
                               <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                   <HeaderTemplate>
                                       <asp:CheckBox ID="chkNotCompletedSelectAll" Text="Not Completed" runat="server" AutoPostBack="true" OnCheckedChanged="chkNotCompletedSelectAll_CheckedChanged" />
                                       
                                   </HeaderTemplate>
                                   <ItemTemplate>
                                       <asp:CheckBox Visible="false" ID="chkNotCompleted" runat="server" AutoPostBack="true" OnCheckedChanged="chkNotCompleted_CheckedChanged" />
                                   </ItemTemplate>
                               </asp:TemplateField>
                           </Columns>
                           <FooterStyle BackColor="#CCCC99" />
                           <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                           <PagerSettings Position="Top" />
                           <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                           <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                           <AlternatingRowStyle BackColor="#E6EFF7" />
                           <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                           <EmptyDataTemplate>
                               No Records Found.
                           </EmptyDataTemplate>
                       </asp:GridView>
                   </asp:Panel>
               </div>

                <br />
                <div style="text-align: center;">

                    <asp:Button ID="btnSave" runat="server" Text="Submit" OnClick="btnSave_Click" />
                </div>

            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />

            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
