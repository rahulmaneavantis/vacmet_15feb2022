﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Compliances
{
    public partial class ClientBasedFrequency : System.Web.UI.Page
    {
        public static int userID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                userID = Convert.ToInt32(AuthenticationHelper.UserID);
                vsLicenseListPage.CssClass = "alert alert-danger";                
                BindCustomersList(userID);
                BindActs();                
                BindClientFrequencies();                                                         
            }
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindComplianceFilter();
        }
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private void BindComplianceFilter()
        {
            try
            {
                int customerid = -1;
                int frequencyid = -1;
                int actid = -1;
                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue) && Convert.ToString(ddlFilterCustomer.SelectedValue) != "-1")
                {
                    customerid = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlFilterFrequencies.SelectedValue) && Convert.ToString(ddlFilterFrequencies.SelectedValue) != "-1")
                {
                    frequencyid = Convert.ToInt32(ddlFilterFrequencies.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlFilterAct.SelectedValue) && Convert.ToString(ddlFilterAct.SelectedValue) != "-1")
                {
                    actid = Convert.ToInt32(ddlFilterAct.SelectedValue);
                }
               
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    grdClient.DataSource = null;
                    grdClient.DataBind();
                    var ClientcomplianceDetails = (from row in entities.SP_ClientBasedCheckListFrequency(customerid)
                                       select row).ToList();

                    if (ClientcomplianceDetails.Count>0)
                    {
                        if (frequencyid!=-1)
                        {
                            //ClientcomplianceDetails = ClientcomplianceDetails.Where(entry => entry.Frequency == frequencyid).ToList();
                            ClientcomplianceDetails = ClientcomplianceDetails.Where(entry => entry.CFrequency == frequencyid).ToList();
                        }
                        if (actid != -1)
                        {
                            ClientcomplianceDetails = ClientcomplianceDetails.Where(entry => entry.ActID == actid).ToList();
                        }
                        if (!string.IsNullOrEmpty(tbxFilter.Text))
                        {
                            if (CheckInt(tbxFilter.Text))
                            {
                                int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                                ClientcomplianceDetails = ClientcomplianceDetails.Where(entry => entry.ComplianceID == a).ToList();
                            }
                            else
                            {
                                ClientcomplianceDetails = ClientcomplianceDetails.Where(entry => entry.ActName.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.ShortDescription.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                            }
                        }
                        grdClient.DataSource = ClientcomplianceDetails;
                        grdClient.DataBind();
                    }
                }                                                      
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }       
        protected void ddlFilterCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindComplianceFilter();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }     
        }             
        protected void ddlFilterAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindComplianceFilter();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlFilterFrequencies_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindComplianceFilter();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }     
        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {          
        }
        private void BindCustomersList(int UserID)
        {
            try
            {
                //  var details= CustomerManagement.GetAll(-1);
                var details = Assigncustomer.GetAllCustomer(userID);
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";
                ddlCustomerList.DataSource = details;
                ddlCustomerList.DataBind();
                ddlCustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                ddlCustomerList.SelectedIndex = -1;

                ddlFilterCustomer.DataTextField = "Name";
                ddlFilterCustomer.DataValueField = "ID";
                ddlFilterCustomer.DataSource = details;
                ddlFilterCustomer.DataBind();
                ddlFilterCustomer.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                ddlFilterCustomer.SelectedIndex = -1;

                //ddlUploadCustomerList.DataTextField = "Name";
                //ddlUploadCustomerList.DataValueField = "ID";
                //ddlUploadCustomerList.DataSource = details;
                //ddlUploadCustomerList.DataBind();
                //ddlUploadCustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
                //ddlUploadCustomerList.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }
        private void BindClientFrequencies()
        {
            try
            {
                var FrequencyDetails= ChecklistFrequency.GetAllClientFrequency<Frequency>();

                FrequencyDetails = FrequencyDetails.Where(a => a.ID != 4 && a.ID != 7 && a.ID != 8).ToList();
                ddlClientFrequencies.DataTextField = "Name";
                ddlClientFrequencies.DataValueField = "ID";
                ddlClientFrequencies.DataSource = FrequencyDetails;
                ddlClientFrequencies.DataBind();
                ddlClientFrequencies.Items.Insert(0, new ListItem("< Select Frequency>", "-1"));
                ddlClientFrequencies.SelectedIndex = -1;

                ddlFilterFrequencies.DataTextField = "Name";
                ddlFilterFrequencies.DataValueField = "ID";
                ddlFilterFrequencies.DataSource = FrequencyDetails;
                ddlFilterFrequencies.DataBind();
                ddlFilterFrequencies.Items.Insert(0, new ListItem("< Select Frequency>", "-1"));
                ddlFilterFrequencies.SelectedIndex = -1;

                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }
        protected void ddlClientFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {            
        }
        private void BindActs()
        {
            try
            {
                var ActDetails= ChecklistFrequency.GetAllAct();
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActDetails;
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("< Select Act >", "-1"));
                ddlAct.SelectedIndex = -1;

                ddlFilterAct.DataTextField = "Name";
                ddlFilterAct.DataValueField = "ID";
                ddlFilterAct.DataSource = ActDetails;
                ddlFilterAct.DataBind();
                ddlFilterAct.Items.Insert(0, new ListItem("< Select Act >", "-1"));
                ddlFilterAct.SelectedIndex = -1;                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }
        protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                long actid = -1;
                if (!string.IsNullOrEmpty(ddlAct.SelectedValue) && Convert.ToString(ddlAct.SelectedValue) != "-1")
                {
                    actid = Convert.ToInt32(ddlAct.SelectedValue);
                }
                if (actid != -1)
                {
                    BindShortDescription(Convert.ToInt64(ddlAct.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }       
        private void BindShortDescription(long ActID)
        {
            try
            {
                ddlshortdescription.DataTextField = "ShortDescription";
                ddlshortdescription.DataValueField = "ID";

                ddlshortdescription.DataSource = ChecklistFrequency.GetShortdescription(ActID);
                ddlshortdescription.DataBind();

                ddlshortdescription.Items.Insert(0, new ListItem("< Select Short Description >", "-1"));
                ddlshortdescription.SelectedIndex = -1;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlshortdescriptionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {                
                var compliance = ChecklistFrequency.GetCompliance(Convert.ToInt64(ddlshortdescription.SelectedValue));
                if (compliance == 0)
                {
                    lblAvacomFrequency.Text = "Monthly";
                }
                else if (compliance == 1)
                {
                    lblAvacomFrequency.Text = "Quarterly";
                }
                else if (compliance == 2)
                {
                    lblAvacomFrequency.Text = "HalfYearly";
                }
                else if (compliance == 3)
                {
                    lblAvacomFrequency.Text = "Annual";
                }
                else if (compliance == 4)
                {
                    lblAvacomFrequency.Text = "FourMonthly";
                }
                else if (compliance == 5)
                {
                    lblAvacomFrequency.Text = "TwoYearly";
                }
                else if (compliance == 6)
                {
                    lblAvacomFrequency.Text = "SevenYearly";
                }
                else if (compliance == 7)
                {
                    lblAvacomFrequency.Text = "Daily";
                }
                else if (compliance == 8)
                {
                    lblAvacomFrequency.Text = "Weekly";
                }
                else
                {
                    lblAvacomFrequency.Text = "";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }                
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                long customerid = -1;
                int frequencyid = -1;
                long actid = -1;
                long complianceid = -1;
                if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue) && Convert.ToString(ddlCustomerList.SelectedValue) != "-1")
                {
                    customerid = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlClientFrequencies.SelectedValue) && Convert.ToString(ddlClientFrequencies.SelectedValue) != "-1")
                {
                    frequencyid = Convert.ToInt32(ddlClientFrequencies.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlAct.SelectedValue) && Convert.ToString(ddlAct.SelectedValue) != "-1")
                {
                    actid = Convert.ToInt32(ddlAct.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlshortdescription.SelectedValue) && Convert.ToString(ddlshortdescription.SelectedValue) != "-1")
                {
                    complianceid = Convert.ToInt32(ddlshortdescription.SelectedValue);
                }
                if (customerid != -1 && frequencyid != -1 && actid != -1 && complianceid != -1)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        ClientBasedCheckListFrequency freq = new ClientBasedCheckListFrequency()
                        {
                            ClientID = customerid,
                            ComplianceID = complianceid,
                            ActID = actid,
                            CFrequency = frequencyid,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,
                            IsDeleted = false
                        };
                        if ((int)ViewState["Mode"] == 1)
                        {
                            freq.ID = Convert.ToInt32(ViewState["CBCTID"]);
                        }
                       
                        if ((int)ViewState["Mode"] == 0)
                        {
                            if (Exists(freq))
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "compliance already exists for this client.";
                                vsLicenseListPage.CssClass = "alert alert-danger";
                                return;
                            }
                            entities.ClientBasedCheckListFrequencies.Add(freq);
                            entities.SaveChanges();

                            if (frequencyid != 7)
                            {
                                if (frequencyid != 8)
                                {
                                    var compliancedetails = (from row in entities.Compliances
                                                             where row.ID == complianceid
                                                             select row.SubComplianceType).FirstOrDefault();
                                    GenerateDefaultScheduleForComplianceID(complianceid, compliancedetails, customerid,(byte)frequencyid);
                                }
                            }

                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Record saved successfully";
                            vsLicenseListPage.CssClass = "alert alert-success";
                            //clearfilter();
                        }
                        else if ((int)ViewState["Mode"] == 1)
                        {
                            if (!AssignmentExists(customerid, complianceid))
                            {
                                freq.UpdatedBy = AuthenticationHelper.UserID;
                                Update(freq);

                                if (frequencyid != 7)
                                {
                                    if (frequencyid != 8)
                                    {
                                        var compliancedetails = (from row in entities.Compliances
                                                                 where row.ID == complianceid
                                                                 select row.SubComplianceType).FirstOrDefault();
                                        GenerateDefaultScheduleForComplianceID(complianceid, compliancedetails, customerid, (byte)frequencyid, true);
                                    }
                                }
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Record updated successfully";
                                vsLicenseListPage.CssClass = "alert alert-success";
                                //clearfilter();
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Compliance can not be updated ,because it is tagged to compliances which are already performed.";
                                vsLicenseListPage.CssClass = "alert alert-danger";
                            }
                        }
                        BindComplianceFilter();
                        upComplianceTypeList.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                vsLicenseListPage.CssClass = "alert alert-danger";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                              
            }
        }    
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);                                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }

        

        protected void btnAddFrequency_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                clearfilter();
                upComplianceDetails.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divClientFrequencyDetailsDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);              
            }
        }
        protected void clearfilter()
        {
            lblAvacomFrequency.Text = string.Empty;
            ddlshortdescription.ClearSelection();
            ddlAct.ClearSelection();
            ddlCustomerList.ClearSelection();
            ddlClientFrequencies.ClearSelection();
        }
        protected void grdCompliances_RowDataBound(object sender, GridViewRowEventArgs e)
        {           
        }
        protected void grdCompliances_RowCreated(object sender, GridViewRowEventArgs e)
        {           
        }
        protected void grdCompliances_Sorting(object sender, GridViewSortEventArgs e)
        {          
        }
        protected void grdCompliances_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_COMPLIANCE"))
                {
                    int ID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["CBCTID"] = ID;
                    clearfilter();                   
                    var ltlm = GetByID(ID);
                    if (!string.IsNullOrEmpty(Convert.ToString(ltlm.ClientID)))
                    {
                        ddlCustomerList.SelectedValue = Convert.ToString(ltlm.ClientID);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(ltlm.ActID)))
                    {
                        ddlAct.SelectedValue = Convert.ToString(ltlm.ActID);
                        BindShortDescription(Convert.ToInt64(ddlAct.SelectedValue));
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(ltlm.ComplianceID)))
                    {
                        ddlshortdescription.SelectedValue = Convert.ToString(ltlm.ComplianceID);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(ltlm.CFrequency)))
                    {
                        ddlClientFrequencies.SelectedValue = Convert.ToString(ltlm.CFrequency);
                    }
                    var compliance = ChecklistFrequency.GetCompliance(Convert.ToInt64(ddlshortdescription.SelectedValue));
                    if (compliance == 0)
                    {
                        lblAvacomFrequency.Text = "Monthly";
                    }
                    else if (compliance == 1)
                    {
                        lblAvacomFrequency.Text = "Quarterly";
                    }
                    else if (compliance == 2)
                    {
                        lblAvacomFrequency.Text = "HalfYearly";
                    }
                    else if (compliance == 3)
                    {
                        lblAvacomFrequency.Text = "Annual";
                    }
                    else if (compliance == 4)
                    {
                        lblAvacomFrequency.Text = "FourMonthly";
                    }
                    else if (compliance == 5)
                    {
                        lblAvacomFrequency.Text = "TwoYearly";
                    }
                    else if (compliance == 6)
                    {
                        lblAvacomFrequency.Text = "SevenYearly";
                    }
                    else if (compliance == 7)
                    {
                        lblAvacomFrequency.Text = "Daily";
                    }
                    else if (compliance == 8)
                    {
                        lblAvacomFrequency.Text = "Weekly";
                    }
                    else
                    {
                        lblAvacomFrequency.Text = "";
                    }

                    var scheduleList = GetScheduleByComplianceID(ltlm.ComplianceID, ltlm.ClientID);
                    if (scheduleList.Count == 0)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var compliancedetails = (from row in entities.Compliances
                                                     where row.ID == ltlm.ComplianceID
                                                     select row.SubComplianceType).FirstOrDefault();
                            if (ltlm.CFrequency != 7)
                            {
                                if (ltlm.CFrequency != 8)
                                {
                                    GenerateDefaultScheduleForComplianceID(ltlm.ComplianceID, compliancedetails, ltlm.ClientID, (byte)ltlm.CFrequency);
                                }
                            }
                        }
                    }

                    upComplianceDetails.Update();
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divClientFrequencyDetailsDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_COMPLIANCE"))
                {
                    string Args = e.CommandArgument.ToString();                    
                    string[] arg = Args.ToString().Split(';');
                    long ID = -1;
                    long Customerid = -1;
                    long Complianceid = -1;
                    if (arg.Length > 2)
                    {
                        ID = Convert.ToInt32(arg[0]);
                        Customerid = Convert.ToInt32(arg[1]);
                        Complianceid = Convert.ToInt32(arg[2]);
                        if (!AssignmentExists(Customerid, Complianceid))
                        {
                            Delete(ID, AuthenticationHelper.UserID);
                            cvDuplicateEntry1.IsValid = false;
                            cvDuplicateEntry1.ErrorMessage = "Record deleted successfully";                            
                            BindComplianceFilter();
                        }
                        else
                        {
                            cvDuplicateEntry1.IsValid = false;
                            cvDuplicateEntry1.ErrorMessage = "Compliance can not be deleted ,because it is tagged to compliances which are already performed.";                                                        
                        }
                    }
                }          
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {            
        }
        public static void Delete(long ID, long updatedbyID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ClientBasedCheckListFrequency ltlmToUpdate = (from row in entities.ClientBasedCheckListFrequencies
                                                              where row.ID == ID
                                                              select row).FirstOrDefault();


                ltlmToUpdate.UpdatedBy = updatedbyID;
                ltlmToUpdate.IsDeleted = true;
                ltlmToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static ClientBasedCheckListFrequency GetByID(int ID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var act = (from row in entities.ClientBasedCheckListFrequencies
                           where row.ID == ID && row.IsDeleted == false
                           select row).FirstOrDefault();
                return act;
            }
        }
        public static bool AssignmentExists(long CustomerID,long ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from CI in entities.ComplianceInstances
                             join CA in entities.ComplianceAssignments
                             on CI.ID equals CA.ComplianceInstanceID
                             join CB in entities.CustomerBranches
                             on CI.CustomerBranchID equals CB.ID
                             where CI.IsDeleted == false                             
                             && CI.ComplianceId == ComplianceID
                             && CB.CustomerID== CustomerID
                             select CI).ToList();

                if (query.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public static bool Exists(ClientBasedCheckListFrequency ltlm)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.ClientBasedCheckListFrequencies
                             where row.IsDeleted == false 
                             && row.ClientID==ltlm.ClientID
                             && row.ComplianceID== ltlm.ComplianceID
                             select row);

                if (ltlm.ID > 0)
                {
                    query = query.Where(entry => entry.ID != ltlm.ID);
                }

                return query.Select(entry => true).SingleOrDefault();
            }
        }

        public static void UploadUpdate(ClientBasedCheckListFrequency ltlm)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {    
               ClientBasedCheckListFrequency ltlmToUpdate = (from row in entities.ClientBasedCheckListFrequencies
                                                              where row.ClientID == ltlm.ClientID
                                                              && row.ComplianceID==ltlm.ComplianceID                                                                                                                          
                                                      select row).FirstOrDefault();

                ltlmToUpdate.CFrequency = ltlm.CFrequency;
                ltlmToUpdate.UpdatedBy = ltlm.UpdatedBy;
                ltlmToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static void Update(ClientBasedCheckListFrequency ltlm)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ClientBasedCheckListFrequency ltlmToUpdate = (from row in entities.ClientBasedCheckListFrequencies
                                                              where row.ID == ltlm.ID
                                                              select row).FirstOrDefault();

                ltlmToUpdate.CFrequency = ltlm.CFrequency;
                ltlmToUpdate.UpdatedBy = ltlm.UpdatedBy;
                ltlmToUpdate.UpdatedOn = DateTime.Now;
                entities.SaveChanges();
            }
        }
        public static List<ComplianceScheduleClientFrequency> GetScheduleByComplianceID(long complianceID,long clientID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var scheduleList = (from row in entities.ComplianceScheduleClientFrequencies
                                    where row.ComplianceID == complianceID
                                    && row.CustomerID== clientID
                                    orderby row.ForMonth
                                    select row).ToList();

                return scheduleList;
            }
        }
        public static void GenerateDefaultScheduleForComplianceID(long complianceID, byte? subcomplincetype,long customerid,byte frequency, bool deleteOldData = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (!(subcomplincetype == 0))
                {
                    if (deleteOldData)
                    {
                        var ids = (from row in entities.ComplianceScheduleClientFrequencies
                                   where row.CustomerID== customerid && row.ComplianceID == complianceID
                                   select row.ID).ToList();
                        ids.ForEach(entry =>
                        {
                            var schedule = (from row in entities.ComplianceScheduleClientFrequencies
                                                                          where row.ID == entry
                                                                          select row).FirstOrDefault();
                            entities.ComplianceScheduleClientFrequencies.Remove(schedule);
                        });
                        entities.SaveChanges();
                    }

                    var compliance = (from row in entities.Compliances
                                      where row.ID == complianceID
                                      select row).First();
                    int step = 1;
                    switch ((Frequency)frequency)
                    {
                        case Frequency.Quarterly:
                            step = 3;
                            break;
                        case Frequency.FourMonthly:
                            step = 4;
                            break;
                        case Frequency.HalfYearly:
                            step = 6;
                            break;
                        case Frequency.Annual:
                            step = 12;
                            break;
                        case Frequency.TwoYearly:
                            step = 12;
                            break;
                        case Frequency.SevenYearly:
                            step = 12;
                            break;
                    }
                    byte startMonth = 4;
                    if (frequency == 0)
                    {
                        startMonth = Convert.ToByte(1);
                    }
                    //else if (frequency == 4)
                    //{
                    //    startMonth = Convert.ToByte(1);
                    //}
                    else if (frequency == 1)
                    {
                        startMonth = Convert.ToByte(1);
                    }
                    int SpecialMonth;
                    for (int month = startMonth; month <= 12; month += step)
                    {
                        ComplianceScheduleClientFrequency complianceShedule = new ComplianceScheduleClientFrequency();
                        complianceShedule.ComplianceID = complianceID;
                        complianceShedule.ForMonth = month;
                        complianceShedule.CustomerID = customerid;
                        if (subcomplincetype == 1 || subcomplincetype == 2)
                        {
                            int monthCopy = month;
                            if (monthCopy == 1)
                            {
                                monthCopy = 12;
                                complianceShedule.ForMonth = monthCopy;
                            }
                            else
                            {
                                monthCopy = month - 1;
                                complianceShedule.ForMonth = monthCopy;
                            }
                            if (frequency == 3 || frequency == 5 || frequency == 6)
                            {
                                monthCopy = 3;
                                complianceShedule.ForMonth = monthCopy;
                            }
                            int lastdateOfPeriodic = DateTime.DaysInMonth(DateTime.Now.Year, monthCopy);
                            complianceShedule.SpecialDate = lastdateOfPeriodic.ToString() + monthCopy.ToString("D2");
                        }
                        else
                        {
                            SpecialMonth = month;
                            if (frequency == 0 && SpecialMonth == 12)
                            {
                                SpecialMonth = 1;
                            }
                            else if (frequency == 1 && SpecialMonth == 10)
                            {
                                SpecialMonth = 1;
                            }
                            else if (frequency == 2 && SpecialMonth == 7)
                            {
                                SpecialMonth = 1;
                            }
                            else if (frequency == 2 && SpecialMonth == 10)
                            {
                                SpecialMonth = 4;
                            }
                            else if (frequency == 4 && SpecialMonth == 9)
                            {
                                SpecialMonth = 1;
                            }
                            else if (frequency == 4 && SpecialMonth == 12)
                            {
                                SpecialMonth = 4;
                            }

                            //else if (frequency == 4 && SpecialMonth == 10)
                            //{
                            //    SpecialMonth = 9;
                            //}
                            else if ((frequency == 3 || frequency == 5 || frequency == 6) && SpecialMonth == 4)
                            {
                                SpecialMonth = 4;
                            }
                            else
                            {
                                SpecialMonth = SpecialMonth + step;
                            }

                            int lastdate = DateTime.DaysInMonth(DateTime.Now.Year, SpecialMonth);
                            if (Convert.ToInt32(compliance.DueDate.Value.ToString("D2")) > lastdate)
                            {
                                complianceShedule.SpecialDate = lastdate.ToString() + SpecialMonth.ToString("D2");
                            }
                            else
                            {
                                complianceShedule.SpecialDate = compliance.DueDate.Value.ToString("D2") + SpecialMonth.ToString("D2");
                            }
                        }
                        entities.ComplianceScheduleClientFrequencies.Add(complianceShedule);
                    }
                    entities.SaveChanges();
                    
                }
            }
        }

        #region Upload                
        private bool SheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("UploadClientBasedFrequency"))
                    {
                        if (sheet.Name.Trim().Equals("UploadClientBasedFrequency") || sheet.Name.Trim().Equals("UploadClientBasedFrequency") || sheet.Name.Trim().Equals("UploadClientBasedFrequency"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }


                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        private int GetDimensionRows(ExcelWorksheet sheet)
        {
            var startRow = sheet.Dimension.Start.Row;
            var endRow = sheet.Dimension.End.Row;
            return endRow - startRow;
        }

        public static void GetCompliance()
        {
            List<ComplianceManagement.Business.Data.Compliance> Records = new List<ComplianceManagement.Business.Data.Compliance>();
            var ComplianceList = HttpContext.Current.Cache["ChecklistComplianceListData"];
            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from C in entities.Compliances
                               //join CI in entities.ComplianceInstances on C.ID equals CI.ComplianceId
                               //join CB in entities.CustomerBranches on CI.CustomerBranchID equals CB.ID
                               where 
                               C.IsDeleted == false 
                               && C.EventFlag == null
                               && C.Status == null
                               && C.ComplinceVisible == true 
                               && C.ComplianceType==1                                  
                               select C).ToList();

                    HttpContext.Current.Cache.Insert("ChecklistComplianceListData", Records); // add it to cache
                }
            }
        }
        public bool ComplainceExists(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.Compliance>)Cache["ChecklistComplianceListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == ComplianceID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public int getActID(int ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int ActID = -1;
                var Records = (List<ComplianceManagement.Business.Data.Compliance>)Cache["ChecklistComplianceListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == ComplianceID
                                 select row.ActID).FirstOrDefault();
                    ActID = query;
                }
                return ActID;
            }
        }
        public partial class CBCTempTable
        {
            public long ComplianceId { get; set; }            
        }
        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvDuplicateEntry1.IsValid = false;
            cvDuplicateEntry1.ErrorMessage = finalErrMsg;
        }
        protected void btnUploadSave_Click(object sender, EventArgs e)
        {
            

            if (FU_Upload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(FU_Upload.FileName);
                    FU_Upload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());
                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                            bool flag = SheetsExitsts(xlWorkbook, "UploadClientBasedFrequency");
                            if (flag == true)
                            {                                
                                if (HttpContext.Current.Cache.Get("ChecklistComplianceListData") != null)
                                {
                                    HttpContext.Current.Cache.Remove("ChecklistComplianceListData");
                                }
                                GetCompliance();
                                ExcelWorksheet xlWorksheetvalidation = xlWorkbook.Workbook.Worksheets["UploadClientBasedFrequency"];
                                List<string> errorMessage = new List<string>();
                                List<string> saveerrorMessage = new List<string>();
                                List<CBCTempTable> lstTemptable = new List<CBCTempTable>();

                                List<ClientBasedCheckListFrequency> CBCheckListFrequency = new List<ClientBasedCheckListFrequency>();

                                if (xlWorksheetvalidation != null)
                                {
                                    if (GetDimensionRows(xlWorksheetvalidation) != 0)
                                    {
                                        int xlrow2 = xlWorksheetvalidation.Dimension.End.Row;
                                        #region Validations
                                        string valfrequency = string.Empty;
                                        int valcomplianceid = -1;
                                        for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                                        {
                                            valcomplianceid = -1;
                                            valfrequency = string.Empty;

                                            #region 1 ComplianceID
                                            if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 1].Text.ToString().Trim()))
                                            {
                                                valcomplianceid = Convert.ToInt32(xlWorksheetvalidation.Cells[rowNum, 1].Text.Trim());
                                            }
                                            if (valcomplianceid == 0 || valcomplianceid == -1)
                                            {
                                                errorMessage.Add("Required ComplianceID at row number-" + rowNum);
                                            }
                                            else
                                            {
                                                if (ComplainceExists(valcomplianceid) == false)
                                                {
                                                    errorMessage.Add("ComplianceID not Defined in the System or It is Event Based or Informative Compliance at row number-" + rowNum);
                                                }
                                                else
                                                {
                                                    if (!lstTemptable.Any(x => x.ComplianceId == valcomplianceid))
                                                    {
                                                        CBCTempTable tt = new CBCTempTable();
                                                        tt.ComplianceId = valcomplianceid;
                                                        lstTemptable.Add(tt);
                                                    }
                                                    else
                                                    {
                                                        errorMessage.Add("Compliance with this ComplianceID (" + valcomplianceid + ") , Exists Multiple Times in the Uploaded Excel Document at Row - " + rowNum + "");
                                                    }
                                                }
                                            }
                                            #endregion

                                            #region 2 Frequency
                                            if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[rowNum, 2].Text.ToString().Trim()))
                                            {
                                                valfrequency = xlWorksheetvalidation.Cells[rowNum, 2].Text.Trim();
                                            }
                                            if (string.IsNullOrEmpty(valfrequency))
                                            {
                                                errorMessage.Add("Required Frquency at row number-" + rowNum);
                                            }
                                            else
                                            {
                                                if (valfrequency.ToUpper().Trim() != "Daily".ToUpper().Trim() 
                                                    && valfrequency.ToUpper().Trim() != "FourMonthly".ToUpper().Trim()
                                                    && valfrequency.ToUpper().Trim() != "Weekly".ToUpper().Trim())
                                                {
                                                    int Frequencycheck = -1;
                                                    var FrequencyDetails = ChecklistFrequency.GetAllClientFrequency<Frequency>();
                                                    FrequencyDetails = FrequencyDetails.Where(a => a.ID != 4 && a.ID != 7 && a.ID != 8).ToList();
                                                    Frequencycheck = (from row in FrequencyDetails
                                                                      where row.Name.ToUpper() == valfrequency.ToUpper()
                                                                      select row.ID).FirstOrDefault();

                                                    if (Frequencycheck == -1)
                                                    {
                                                        errorMessage.Add("Frquency Name not defined in the system at row number-" + rowNum);
                                                    }
                                                }
                                                else
                                                {
                                                    errorMessage.Add("Frquency Name not defined in the system at row number-" + rowNum);
                                                }
                                                                                                   
                                            }
                                            #endregion                                           
                                        }
                                        #endregion

                                        if (errorMessage.Count > 0)
                                        {
                                            ErrorMessages(errorMessage);                                                                                    
                                        }
                                        else
                                        {
                                            #region Save
                                            string noupdatedid = string.Empty;
                                            for (int saverowNum = 2; saverowNum <= xlrow2; saverowNum++)
                                            {                                                
                                                long customerid = -1;
                                                int frequencyid = -1;
                                                string frequency = string.Empty;
                                                long actid = -1;
                                                int complianceid = -1;
                                                if (!string.IsNullOrEmpty(ddlFilterCustomer.SelectedValue) && Convert.ToString(ddlFilterCustomer.SelectedValue) != "-1")
                                                {
                                                    customerid = Convert.ToInt32(ddlFilterCustomer.SelectedValue);
                                                }
                                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[saverowNum, 1].Text.ToString().Trim()))
                                                {
                                                    complianceid = Convert.ToInt32(xlWorksheetvalidation.Cells[saverowNum, 1].Text.Trim());
                                                }
                                                if (!String.IsNullOrEmpty(xlWorksheetvalidation.Cells[saverowNum, 2].Text.ToString().Trim()))
                                                {
                                                    frequency = xlWorksheetvalidation.Cells[saverowNum, 2].Text.Trim();
                                                }
                                                if (!string.IsNullOrEmpty(frequency))
                                                {                                                   
                                                    var FrequencyDetails = ChecklistFrequency.GetAllClientFrequency<Frequency>();                                                
                                                    FrequencyDetails = FrequencyDetails.Where(a => a.ID != 4 && a.ID != 7 && a.ID != 8).ToList();
                                                    frequencyid = (from row in FrequencyDetails
                                                                      where row.Name.ToUpper() == frequency.ToUpper()
                                                                      select row.ID).FirstOrDefault();                                                    
                                                }
                                                if (complianceid != -1)
                                                {
                                                    actid = getActID(complianceid);
                                                }                                                
                                                if (customerid != -1 && frequencyid != -1 && actid != -1 && complianceid != -1)
                                                {
                                                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                                    {
                                                        ClientBasedCheckListFrequency CBCF = new ClientBasedCheckListFrequency();
                                                        CBCF.ClientID = customerid;
                                                        CBCF.ComplianceID = complianceid;
                                                        CBCF.ActID = actid;
                                                        CBCF.CFrequency = frequencyid;
                                                        CBCF.CreatedBy = AuthenticationHelper.UserID;
                                                        CBCF.CreatedOn = DateTime.Now;
                                                        CBCF.IsDeleted = false;
                                                        if (Exists(CBCF))
                                                        {
                                                            if (!AssignmentExists(customerid, complianceid))
                                                            {
                                                                CBCF.UpdatedBy = AuthenticationHelper.UserID;
                                                                UploadUpdate(CBCF);
                                                                if (frequencyid != 7)
                                                                {
                                                                    if (frequencyid != 8)
                                                                    {
                                                                        var compliancedetails = (from row in entities.Compliances
                                                                                                 where row.ID == complianceid
                                                                                                 select row.SubComplianceType).FirstOrDefault();
                                                                        GenerateDefaultScheduleForComplianceID(complianceid, compliancedetails, customerid, (byte)frequencyid, true);
                                                                    }
                                                                }
                                                                cvDuplicateEntry1.IsValid = false;
                                                                cvDuplicateEntry1.ErrorMessage = "Record updated successfully";                                                                
                                                            }
                                                            else
                                                            {
                                                                saveerrorMessage.Add("This Compliance" + complianceid + " can not be updated ,because it is tagged to compliances which are already performed.");
                                                                //cvDuplicateEntry1.IsValid = false;
                                                                //cvDuplicateEntry1.ErrorMessage = "Compliance can not be updated ,because it is tagged to compliances which are already performed.";                                                                
                                                            }
                                                        }
                                                        else
                                                        {
                                                            entities.ClientBasedCheckListFrequencies.Add(CBCF);
                                                            entities.SaveChanges();

                                                            if (frequencyid != 7)
                                                            {
                                                                if (frequencyid != 8)
                                                                {
                                                                    var compliancedetails = (from row in entities.Compliances
                                                                                             where row.ID == complianceid
                                                                                             select row.SubComplianceType).FirstOrDefault();
                                                                    GenerateDefaultScheduleForComplianceID(complianceid, compliancedetails, customerid, (byte)frequencyid);
                                                                }
                                                            }

                                                            cvDuplicateEntry1.IsValid = false;
                                                            cvDuplicateEntry1.ErrorMessage = "Record saved successfully";                                                                                                                        
                                                        }                                                                                                                                                            
                                                    }
                                                }
                                            }
                                            if (saveerrorMessage.Count > 0)
                                            {
                                                ErrorMessages(saveerrorMessage);
                                            }
                                            noupdatedid = string.Empty;
                                            #endregion


                                            if (HttpContext.Current.Cache.Get("ChecklistComplianceListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("ChecklistComplianceListData");
                                            }                                            
                                        }
                                    }
                                }
                            }
                            else
                            {
                                cvDuplicateEntry1.IsValid = false;
                                cvDuplicateEntry1.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'RiskControlMatrixCreation'.";
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry1.IsValid = false;
                        cvDuplicateEntry1.ErrorMessage = "Error uploading file. Please try again.";
                    }                    
                }
                catch (Exception ex)
                {
                    cvDuplicateEntry1.IsValid = false;
                    cvDuplicateEntry1.ErrorMessage = "Something went wrong, Please try again";                    
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }         
        }

        #endregion
    }
}