﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Common
{
    public partial class UpcomingHearingGridData : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Convert.ToString(Request.QueryString["date"]) != "null")
                {
                    DateTime date = Convert.ToDateTime(Request.QueryString["date"]);
                    string Dateval = Convert.ToDateTime(Request.QueryString["date"]).ToString("yyyy-dd-MM");
                    DateTime dt = Business.DateTimeExtensions.GetDateCalender(Dateval);
                }
                BindUpcompingHearing();
            }
        }

        public string BindUpcompingHearing()
        {
            string subtypeID = string.Empty;
            try
            {
                DateTime date = new DateTime();
                if (Convert.ToString(Request.QueryString["date"]) != "null")
                {
                    date = Convert.ToDateTime(Request.QueryString["date"]);
                }

                int branchID = -1;
                int partyID = -1;
                int deptID = -1;
                int catID = -1;
                int riskID = -1;
                int stageID = -1;
                int noticeCaseStatus = -1;

                string noticeCaseType = string.Empty;
                string filterNoticeOrCase = string.Empty;
                var TodayDatetime = DateTime.Now;
                var TodayDate = TodayDatetime.Date;

                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                List<View_LitigationCaseResponse> viewHearingDetails = new List<View_LitigationCaseResponse>();
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    if (Convert.ToString(Request.QueryString["date"]) != "null")
                    {
                        if (date < TodayDate)
                        {
                            viewHearingDetails = (from row in entities.View_LitigationCaseResponse
                                                  where row.CustomerID == customerID
                                                  && row.ReminderDate != null
                                                  && row.ResponseDate != date
                                                  && row.ReminderDate == date
                                                  && row.TxnStatusID != 3 //Only Open Cases
                                                  select row).ToList();
                            if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                                viewHearingDetails = viewHearingDetails.Where(entry => (entry.UserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID)).ToList();
                            else // In case of MGMT or CADMN 
                            {
                                viewHearingDetails = viewHearingDetails.Where(entry => entry.RoleID == 3).ToList();
                            }

                        }
                        else
                        {
                            viewHearingDetails = (from row in entities.View_LitigationCaseResponse
                                                  where row.CustomerID == customerID
                                                  && row.ReminderDate != null
                                                  && row.ReminderDate == date
                                                  && row.TxnStatusID != 3 //Only Open Cases
                                                  select row).ToList();
                            if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                                viewHearingDetails = viewHearingDetails.Where(entry => (entry.UserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID)).ToList();
                            else // In case of MGMT or CADMN 
                            {
                                viewHearingDetails = viewHearingDetails.Where(entry => entry.RoleID == 3).ToList();
                            }
                        }
                    }
                    else
                    {
                        viewHearingDetails = (from row in entities.View_LitigationCaseResponse
                                              where row.CustomerID == customerID
                                              && row.ReminderDate != null
                                            //  && row.ReminderDate >= TodayDate
                                              && row.TxnStatusID != 3 //Only Open Cases
                                              select row).ToList();
                        if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                            viewHearingDetails = viewHearingDetails.Where(entry => (entry.UserID == AuthenticationHelper.UserID && entry.RoleID == 3) || (entry.OwnerID == AuthenticationHelper.UserID)).ToList();
                        else // In case of MGMT or CADMN 
                        {
                            viewHearingDetails = viewHearingDetails.Where(entry => entry.RoleID == 3).ToList();
                        }

                        var ChekNextHearing= (from row in viewHearingDetails
                                              where row.CustomerID == customerID
                                              && row.ReminderDate != null
                                               && row.ReminderDate >= TodayDate
                                              && row.TxnStatusID != 3 //Only Open Cases
                                               select row).ToList();
                        if (ChekNextHearing.Count == 0)
                        {
                            

                            viewHearingDetails = (from row in viewHearingDetails
                                                  where row.CustomerID == customerID
                                                  && row.ReminderDate != null

                                                  && row.TxnStatusID != 3 //Only Open Cases
                                                  select row).ToList();
                        }
                        else
                        {
                            viewHearingDetails = (from row in viewHearingDetails
                                                  where row.CustomerID == customerID
                                                  && row.ReminderDate != null
                                                    && row.ReminderDate >= TodayDate
                                                  && row.TxnStatusID != 3 //Only Open Cases
                                                  select row).ToList();
                        }
                       
                    }

                    var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                

                    if (viewHearingDetails.Count > 0)
                    {
                        GetDashboardUpcomingHearing(viewHearingDetails, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, filterNoticeOrCase, noticeCaseType, branchList, partyID, deptID, catID, riskID, stageID, noticeCaseStatus);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Closeimg", "CloseRefresh();", true);
                        lnkDownloadHearing.Visible = false;
                    }
                }
                return subtypeID;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return subtypeID;
            }
        }

        public void GetDashboardUpcomingHearing(List<View_LitigationCaseResponse> viewRecords, int loggedInUserID, string loggedInUserRole, int loggedInUserRoleID, string noticeCase, string noticeCaseType, List<int> branchList, int partyID, int deptID, int catID, int riskID, int stageID, int noticeCaseStatus)
        {
            try
            {
                Session["TotalRowsval"] = 0;

                if (viewRecords.Count > 0)
                {
                    viewRecords = (from g in viewRecords
                                   group g by new
                                   {
                                       g.NoticeCaseInstanceID,
                                       g.Title,
                                       g.CaseRefNo,
                                       g.CourtID,
                                       g.CourtName,
                                       g.CaseType,
                                       g.CustomerID,
                                       g.CustomerBranchID,
                                       g.BranchName,
                                       g.DepartmentID,
                                       g.CaseRiskID,
                                       g.PartyID,
                                       g.PartyName,
                                       // g.Party,
                                       g.TxnStatusID,
                                       g.ReminderDate,
                                       g.lstAssignedTo,
                                       g.ResponseID
                                   } into GCS
                                   select new View_LitigationCaseResponse()
                                   {
                                       NoticeCaseInstanceID = GCS.Key.NoticeCaseInstanceID,
                                       Title = GCS.Key.Title,
                                       CaseRefNo = GCS.Key.CaseRefNo,
                                       CourtID = GCS.Key.CourtID,
                                       CourtName = GCS.Key.CourtName,
                                       CaseType = GCS.Key.CaseType,
                                       CustomerID = GCS.Key.CustomerID,
                                       CustomerBranchID = GCS.Key.CustomerBranchID,
                                       BranchName = GCS.Key.BranchName,
                                       DepartmentID = GCS.Key.DepartmentID,
                                       CaseRiskID = GCS.Key.CaseRiskID,
                                       PartyID = GCS.Key.PartyID,
                                       PartyName = GCS.Key.PartyName,
                                       // Party = GCS.Key.Party,
                                       TxnStatusID = GCS.Key.TxnStatusID,
                                       ReminderDate = GCS.Key.ReminderDate,
                                       lstAssignedTo = GCS.Key.lstAssignedTo,
                                       ResponseID = GCS.Key.ResponseID
                                   }).ToList();
                }

                if (branchList.Count > 0)
                    viewRecords = viewRecords.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();

                if (partyID != -1)
                    viewRecords = viewRecords.Where(entry => entry.PartyID.Contains(partyID.ToString())).ToList();

                if (deptID != -1)
                    viewRecords = viewRecords.Where(entry => entry.DepartmentID == deptID).ToList();

                if (riskID != -1 && riskID != 0)
                    viewRecords = viewRecords.Where(entry => entry.CaseRiskID == riskID).ToList();

                if (noticeCaseType != "" && noticeCaseType != "B") //B--Both --Inward(I) and Outward(O)
                    viewRecords = viewRecords.Where(entry => entry.CaseType == noticeCaseType).ToList();

                if (noticeCaseStatus != 0 && noticeCaseStatus != -1)
                {
                    if (noticeCaseStatus == 3) //3--Closed otherwise Open
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID == noticeCaseStatus).ToList();
                    else
                        viewRecords = viewRecords.Where(entry => entry.TxnStatusID < 3).ToList();
                }

                if (viewRecords.Count > 0)
                {
                    viewRecords = viewRecords.OrderBy(entry => entry.ReminderDate).ToList();
                    grdUpcomingHearing.DataSource = viewRecords;
                    Session["grdLitigationCaseResponseDatamgmt"] = null;
                    Session["TotalRowsval"] = viewRecords.Count();
                    Session["grdLitigationCaseResponseDatamgmt"] = (grdUpcomingHearing.DataSource as List<View_LitigationCaseResponse>).ToDataTable();
                    grdUpcomingHearing.DataBind();
                    lnkDownloadHearing.Visible = true;
                }
                else
                {
                    grdUpcomingHearing.DataSource = null;
                    Session["grdLitigationCaseResponseDatamgmt"] = null;
                    Session["TotalRowsval"] = 0;
                    grdUpcomingHearing.DataBind();
                    lnkDownloadHearing.Visible = false;
                }
                TotalRows.Value = Session["TotalRowsval"].ToString();
                GetPageDisplaySummary();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Closeimg", "CloseRefresh();", true);
                if (viewRecords.Count > 5)
                    lnkShowDetailCase.Visible = true;
                else
                    lnkShowDetailCase.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdUpcomingHearing_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdUpcomingHearing.PageIndex = e.NewPageIndex;

                //Re-Bind Data
                BindUpcompingHearing();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void imgBtnShowDetail_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }
                grdUpcomingHearing.PageSize = 5;
                grdUpcomingHearing.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                BindUpcompingHearing();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }
                grdUpcomingHearing.PageSize = 5;
                grdUpcomingHearing.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                BindUpcompingHearing();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();
                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRowsval"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / 5;
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % 5;
                if (pageItemRemain > 0)
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        protected string GetAssignedTo(long NoticeCaseInstanceID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    using (ComplianceDBEntities entity = new ComplianceDBEntities())
                    {
                        var getuserdetails = (from row in entities.tbl_LegalCaseAssignment
                                              join row1 in entity.Users
                                              on row.UserID equals row1.ID
                                              where row1.CustomerID == AuthenticationHelper.CustomerID
                                              && row.CaseInstanceID == NoticeCaseInstanceID
                                              && row.IsActive == true
                                              && row1.IsDeleted == false
                                              && row1.IsActive == true
                                              select row1.FirstName + " " + row1.LastName).ToList();
                        string result = string.Empty;
                        if (getuserdetails.Count > 0)
                        {
                            foreach (var item in getuserdetails)
                            {
                                result += item + ',';
                            }
                        }
                        result = result.Trim(',');
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }

        }


        protected void lnkshowCaseDetails_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton) (sender);
                if (btn != null)
                {
                    string[] commandArg = btn.CommandArgument.ToString().Split(',');

                    int noticeCaseInstanceID = Convert.ToInt32(commandArg[0]);
                    //string type = commandArg[1];
                    string type = "C";

                    if (noticeCaseInstanceID != 0 && !string.IsNullOrEmpty(type))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowNoticeCaseDialog(" + noticeCaseInstanceID + ",'" + type + "');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkDownloadHearing_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                    DataTable ExcelData = null;
                    DataView view = new System.Data.DataView((DataTable) Session["grdLitigationCaseResponseDatamgmt"]);
                    ExcelData = view.ToTable("Selected", false, "CaseRefNo", "ReminderDate", "Title", "CourtName", "PartyName", "lstAssignedTo", "BranchName", "NoticeCaseInstanceID");
                    foreach (DataRow item in ExcelData.Rows)
                    {
                        item["lstAssignedTo"] = GetAssignedTo(Convert.ToInt32(item["NoticeCaseInstanceID"].ToString()));
                    }
                    ExcelData.Columns.Remove("NoticeCaseInstanceID");
                    #region Lawyer Performance Report
                    exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);
                    exWorkSheet.Cells["A1"].Value = "Court Case No.";
                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A1"].AutoFitColumns(24);
                    exWorkSheet.Cells["A1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["A1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["A1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet.Cells["B1"].Value = "Hearing Date";
                    exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B1"].AutoFitColumns(28);
                    exWorkSheet.Cells["B1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["B1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet.Cells["C1"].Value = "Case Title";
                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C1"].AutoFitColumns(45);
                    exWorkSheet.Cells["C1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["C1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet.Cells["D1"].Value = "Court";
                    exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D1"].AutoFitColumns(22);
                    exWorkSheet.Cells["D1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["D1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["D1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                    exWorkSheet.Cells["E1"].Value = "Opponent";
                    exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E1"].AutoFitColumns(22);
                    exWorkSheet.Cells["E1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["E1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["E1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663

                    exWorkSheet.Cells["F1"].Value = "Assign To";
                    exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F1"].AutoFitColumns(22);
                    exWorkSheet.Cells["F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["F1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["F1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663


                    exWorkSheet.Cells["G1"].Value = "Location";
                    exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G1"].AutoFitColumns(22);
                    exWorkSheet.Cells["G1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    exWorkSheet.Cells["G1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    exWorkSheet.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet.Cells["G1"].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D8D8D8"));//#1F5663
                    #endregion

                    int count = Convert.ToInt32(ExcelData.Rows.Count) + 1;
                    using (ExcelRange col = exWorkSheet.Cells[1, 1, count, 7])
                    {
                        col.Style.WrapText = true;
                        col.Style.Numberformat.Format = "dd/MMM/yyyy";
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        col.Style.Font.Size = 12;
                    }
                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    string locatioName = "CaseHearing-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";
                    Response.AddHeader("content-disposition", "attachment;filename=LitigationReport-" + locatioName);//locatioName                
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                    

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}