﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Common
{
    public partial class ShowCaseDetail : System.Web.UI.Page
    {
        public static string DocumentPath = "";
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindAct();
                    BindParty();
                    BindCustomerBranches();
                    BindDepartment();
                    BindUsers();
                    BindLawyer();
                    BindCaseCategoryType();
                    Bindcourt();
                    BindCaseStage();

                    if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                    {
                        var caseInstanceID = Request.QueryString["AccessID"];
                        if (caseInstanceID != "")
                        {
                            ViewState["CaseInstanceID"] = caseInstanceID;

                            if (Convert.ToInt32(caseInstanceID) == 0)
                            {
                                liCaseHearing.Visible = false;
                                liCaseTask.Visible = false;
                                liCaseOrder.Visible = false;
                                liCaseStatus.Visible = false;

                                btnAddCase_Click(sender, e);  //Add Detail 
                            }
                            else
                            {
                                liCaseHearing.Visible = true;
                                liCaseTask.Visible = true;
                                liCaseOrder.Visible = true;
                                liCaseStatus.Visible = true;

                                btnEditCase_Click(sender, e); //Edit Detail
                            }

                            TabCase_Click(sender, e);
                        }
                    }

                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);

                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                }

                //Show Hide Grid Control - Enable/Disable Form Controls
                if (ViewState["caseStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["caseStatus"]) == 3)
                        enableDisableCasePopUpControls(false);
                    else
                        enableDisableCasePopUpControls(true);
                }
                else
                    enableDisableCasePopUpControls(true);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region Common        

        private void BindCaseStage()
        {
            try
            {
                var lstCaseStages = LitigationManagement.GetAllType("CS");

                ddlCaseStage.DataTextField = "TypeName";
                ddlCaseStage.DataValueField = "ID";

                ddlCaseStage.DataSource = lstCaseStages;
                ddlCaseStage.DataBind();

                ddlCaseStage.Items.Insert(0, new ListItem("Select Stage", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindAct()
        {
            var actList = LitigationLaw.GetAllAct();

            ddlAct.DataTextField = "Name";
            ddlAct.DataValueField = "ID";

            ddlAct.DataSource = actList;
            ddlAct.DataBind();

            ddlAct.Items.Add(new ListItem("Add New", "0"));
        }

        private void BindCaseCategoryType()
        {
            try
            {
                var lstCaseCaseType = LitigationCourtAndCaseType.GetAllLegalCaseTypeData(CustomerID);

                ddlCaseCategory.DataTextField = "CaseType";
                ddlCaseCategory.DataValueField = "ID";

                ddlCaseCategory.DataSource = lstCaseCaseType;
                ddlCaseCategory.DataBind();

                ddlCaseCategory.Items.Add(new ListItem("Add New", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                tvBranches.Nodes.Clear();
                NameValueHierarchy branch = null;

               // var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                List<NameValueHierarchy> branches;
                string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                if (CacheHelper.Exists(key))
                {
                    CacheHelper.Get<List<NameValueHierarchy>>(key, out branches);
                }
                else
                {
                    branches = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                    CacheHelper.Set<List<NameValueHierarchy>>(key, branches);
                }
                if (branches.Count > 0)
                {
                    branch = branches[0];
                }
                tbxBranch.Text = "Select Entity/Location";
                List<TreeNode> nodes = new List<TreeNode>();
                BindBranchesHierarchy(null, branch, nodes);
                foreach (TreeNode item in nodes)
                {
                    tvBranches.Nodes.Add(item);
                }

                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            ddlDepartment.DataTextField = "Name";
            ddlDepartment.DataValueField = "ID";

            ddlDepartment.DataSource = obj;
            ddlDepartment.DataBind();

            ddlDepartment.Items.Add(new ListItem("Add New", "0"));
        }

        public void Bindcourt()
        {
            var obj = LitigationCourtAndCaseType.BindAllCourtMasterData(CustomerID);

            ddlCourt.DataTextField = "CourtName";
            ddlCourt.DataValueField = "ID";

            ddlCourt.DataSource = obj;
            ddlCourt.DataBind();

            ddlCourt.Items.Add(new ListItem("Add New", "0"));
        }

        public void BindLawyer()
        {
            long customerID = -1;
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

            var obj = LawyerManagement.GetLawyerListForMapping(customerID);

            ddlLawFirm.DataTextField = "Name";
            ddlLawFirm.DataValueField = "ID";

            ddlLawFirm.DataSource = obj;
            ddlLawFirm.DataBind();


        }

        public void BindParty()
        {
            var obj = LitigationLaw.GetLCPartyDetails(CustomerID);

            //Drop-Down at Modal Pop-up
            ddlParty.DataTextField = "Name";
            ddlParty.DataValueField = "ID";

            ddlParty.DataSource = obj;
            ddlParty.DataBind();

            ddlParty.Items.Add(new ListItem("Add New", "0"));
        }

        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                //var lstAllUsers = LitigationUserManagement.GetLitigationUsers(customerID,);

                var lstAllUsers = LitigationUserManagement.GetLitigationAllUsers(customerID);

                var bothUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 0);

                ddlOwner.DataValueField = "ID";
                ddlOwner.DataTextField = "Name";
                ddlOwner.DataSource = bothUsers;
                ddlOwner.DataBind();

                var internalUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 1);

                lstBoxPerformer.DataTextField = "Name";
                lstBoxPerformer.DataValueField = "ID";

                lstBoxPerformer.DataSource = internalUsers;
                lstBoxPerformer.DataBind();

                //ddlPerformer.DataValueField = "ID";
                //ddlPerformer.DataTextField = "Name";
                //ddlPerformer.DataSource = internalUsers;
                //ddlPerformer.DataBind();

                ddlReviewer.DataValueField = "ID";
                ddlReviewer.DataTextField = "Name";
                ddlReviewer.DataSource = internalUsers;
                ddlReviewer.DataBind();

                //var externalUsers = LitigationUserManagement.GetRequiredUsers(lstAllUsers, 2);

                //ddlLawyerUser.DataValueField = "ID";
                //ddlLawyerUser.DataTextField = "Name";
                //ddlLawyerUser.DataSource = externalUsers;
                //ddlLawyerUser.DataBind();

                lstAllUsers.Clear();
                lstAllUsers = null;

                internalUsers.Clear();
                internalUsers = null;

                //externalUsers.Clear();
                //externalUsers = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindPaymentType(DropDownList ddl)
        {
            try
            {
                var PaymentMasterList = LitigationPaymentType.GetAllPaymentMasterList();

                ddl.DataValueField = "ID";
                ddl.DataTextField = "TypeName";

                ddl.DataSource = PaymentMasterList;
                ddl.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePayment.IsValid = false;
                cvCasePayment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void TabCase_Click(object sender, EventArgs e)
        {
            liCaseDetail.Attributes.Add("class", "active");
            liCaseHearing.Attributes.Add("class", "");
            liCaseTask.Attributes.Add("class", "");
            liCaseOrder.Attributes.Add("class", "");
            liCaseStatus.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 0;

            if (ViewState["Mode"] != null)
            {
                if (Convert.ToInt32(ViewState["Mode"]) == 0)
                {
                    enableDisableCaseControls(true);
                }
                else if (Convert.ToInt32(ViewState["Mode"]) == 1)
                {
                    enableDisableCaseControls(false);
                }
            }
        }

        protected void TabTask_Click(object sender, EventArgs e)
        {
            liCaseDetail.Attributes.Add("class", "");
            liCaseTask.Attributes.Add("class", "active");
            liCaseHearing.Attributes.Add("class", "");
            liCaseOrder.Attributes.Add("class", "");
            liCaseStatus.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 1;

            if (ViewState["CaseInstanceID"] != null)
            {
                BindCaseTasks(Convert.ToInt32(ViewState["CaseInstanceID"]), grdTaskActivity);
            }
        }

        protected void TabHearing_Click(object sender, EventArgs e)
        {
            liCaseDetail.Attributes.Add("class", "");
            liCaseTask.Attributes.Add("class", "");
            liCaseHearing.Attributes.Add("class", "active");
            liCaseOrder.Attributes.Add("class", "");
            liCaseStatus.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 2;

            if (ViewState["CaseInstanceID"] != null)
            {
                BindCaseResponses(Convert.ToInt32(ViewState["CaseInstanceID"]));
            }
        }

        protected void TabOrder_Click(object sender, EventArgs e)
        {
            liCaseDetail.Attributes.Add("class", "");
            liCaseHearing.Attributes.Add("class", "");
            liCaseTask.Attributes.Add("class", "");
            liCaseOrder.Attributes.Add("class", "active");
            liCaseStatus.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 3;

            if (ViewState["CaseInstanceID"] != null)
            {
                BindCaseOrders(Convert.ToInt32(ViewState["CaseInstanceID"]));
            }
        }

        protected void TabStatus_Click(object sender, EventArgs e)
        {
            liCaseDetail.Attributes.Add("class", "");
            liCaseHearing.Attributes.Add("class", "");
            liCaseTask.Attributes.Add("class", "");
            liCaseOrder.Attributes.Add("class", "");
            liCaseStatus.Attributes.Add("class", "active");

            MainView.ActiveViewIndex = 4;

            if (ViewState["CaseInstanceID"] != null)
            {
                BindCasePayments(Convert.ToInt32(ViewState["CaseInstanceID"]));
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "Select Entity/Location";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                //ScriptManager.RegisterStartupScript(this.upCasePopup, this.upCasePopup.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #endregion


        #region Master

        protected void btnfilldept_Click(object sender, EventArgs e)
        {
            BindDepartment();
        }

        protected void lblCategory_Click(object sender, EventArgs e)
        {
            BindCaseCategoryType();
        }

        protected void lblAct_Click(object sender, EventArgs e)
        {
            BindAct();
        }

        protected void lblParty_Click(object sender, EventArgs e)
        {
            BindParty();
        }

        protected void lnkBtnParty_Click(object sender, EventArgs e)
        {
            BindParty();
        }

        protected void lnkBtnAct_Click(object sender, EventArgs e)
        {
            BindAct();
        }

        protected void lnkBtnCategory_Click(object sender, EventArgs e)
        {
            BindCaseCategoryType();
        }

        protected void lnkBtnDept_Click(object sender, EventArgs e)
        {
            BindDepartment();
        }

        protected void lblCourt_Click(object sender, EventArgs e)
        {
            Bindcourt();
        }

        #endregion

        #region Case Detail

        public void enableDisableCaseControls(bool flag)
        {
            try
            {
                rbCaseInOutType.Enabled = flag;
                txtCaseDate.Enabled = flag;
                tbxRefNo.Enabled = flag;
                tbxTitle.Enabled = flag;
                ddlAct.Enabled = flag;
                tbxSection.Enabled = flag;
                ddlParty.Enabled = flag;
                ddlCaseCategory.Enabled = flag;
                ddlCourt.Enabled = flag;

                tbxJudge.Enabled = flag;

                tbxDescription.Enabled = flag;
                tbxBranch.Enabled = flag;
                ddlDepartment.Enabled = flag;
                ddlOwner.Enabled = flag;
                ddlCaseRisk.Enabled = flag;
                tbxClaimedAmt.Enabled = flag;
                tbxProbableAmt.Enabled = flag;

                rblPotentialImpact.Enabled = flag;
                tbxMonetory.Enabled = flag;
                tbxNonMonetory.Enabled = flag;
                tbxNonMonetoryYears.Enabled = flag;

                CaseFileUpload.Enabled = flag;

                ddlLawFirm.Enabled = flag;

                lstBoxPerformer.Enabled = flag;
                ddlReviewer.Enabled = flag;
                lstBoxLawyerUser.Enabled = flag;
                ddlParty.Enabled = flag;
                ddlAct.Enabled = flag;

                if (flag)
                {
                    ddlLawFirm.Attributes.Remove("disabled");

                    lstBoxPerformer.Attributes.Remove("disabled");
                    lstBoxLawyerUser.Attributes.Remove("disabled");
                    ddlReviewer.Attributes.Remove("disabled");
                    ddlParty.Attributes.Remove("disabled");
                    ddlAct.Attributes.Remove("disabled");
                }
                else
                {
                    ddlLawFirm.Attributes.Add("disabled", "disabled");

                    lstBoxPerformer.Attributes.Add("disabled", "disabled");
                    lstBoxLawyerUser.Attributes.Add("disabled", "disabled");
                    ddlReviewer.Attributes.Add("disabled", "disabled");
                    ddlParty.Attributes.Add("disabled", "disabled");
                    ddlAct.Attributes.Add("disabled", "disabled");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void enableDisableCasePopUpControls(bool flag)
        {
            try
            {
                pnlCase.Enabled = flag;
                pnlCaseAssignment.Enabled = flag;

                grdTaskActivity.Columns[7].Visible = flag;

                grdCasePayment.ShowFooter = flag;
                grdCasePayment.Columns[5].Visible = flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnEditCaseControls_Click(object sender, EventArgs e)
        {
            try
            {
                enableDisableCaseControls(true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindCaseRelatedDocuments(int caseInstanceID)
        {
            try
            {
                List<Sp_Litigation_CaseDocument_Result> lstCaseDocs = new List<Sp_Litigation_CaseDocument_Result>();

                lstCaseDocs = CaseManagement.GetCaseDocumentMapping(caseInstanceID, "C", AuthenticationHelper.CustomerID);
                if (lstCaseDocs.Count > 0)
                {
                    lstCaseDocs = (from g in lstCaseDocs
                                   group g by new
                                   {
                                       g.ID,
                                       g.DocType,
                                       g.FileName,
                                       g.Version,
                                       g.CreatedByText,
                                       g.CreatedOn
                                   } into GCS
                                   select new Sp_Litigation_CaseDocument_Result()
                                   {
                                       ID = GCS.Key.ID,
                                       DocType = GCS.Key.DocType,
                                       FileName = GCS.Key.FileName,
                                       Version = GCS.Key.Version,
                                       CreatedByText = GCS.Key.CreatedByText,
                                       CreatedOn = GCS.Key.CreatedOn,
                                   }).ToList();
                }
                if (lstCaseDocs != null && lstCaseDocs.Count > 0)
                {
                    grdCaseDocuments.DataSource = lstCaseDocs;
                    grdCaseDocuments.DataBind();
                }
                else
                {
                    grdCaseDocuments.DataSource = null;
                    grdCaseDocuments.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void grdCaseDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    grdCaseDocuments.PageIndex = e.NewPageIndex;
                    BindCaseRelatedDocuments(Convert.ToInt32(ViewState["CaseInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCaseDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadCaseDoc"))
                    {
                        DownloadCaseDocument(Convert.ToInt32(e.CommandArgument));
                    }
                    else if (e.CommandName.Equals("DeleteCaseDoc"))
                    {
                        DeleteCaseFile(Convert.ToInt32(e.CommandArgument));

                        //Bind Case Related Documents
                        if (ViewState["CaseInstanceID"] != null)
                            BindCaseRelatedDocuments(Convert.ToInt32(ViewState["CaseInstanceID"]));
                    }
                    else if (e.CommandName.Equals("ViewCaseOrder"))
                    {
                        var AllinOneDocumentList = CaseManagement.GetCaseDocumentByID(Convert.ToInt32(e.CommandArgument));
                        if (AllinOneDocumentList != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                            if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (AllinOneDocumentList.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                lblMessage.Text = "";
                            }
                            else
                            {
                                lblMessage.Text = "There is no file to preview";
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCaseDocuments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                LinkButton lnkBtnDownLoadCaseDoc = (LinkButton) e.Row.FindControl("lnkBtnDownLoadCaseDoc");

                if (lnkBtnDownLoadCaseDoc != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDownLoadCaseDoc);
                }

                LinkButton lnkBtnDeleteCaseDoc = (LinkButton) e.Row.FindControl("lnkBtnDeleteCaseDoc");
                if (lnkBtnDeleteCaseDoc != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeleteCaseDoc);

                    if (ViewState["caseStatus"] != null)
                    {
                        if (Convert.ToInt32(ViewState["caseStatus"]) == 3)
                            lnkBtnDeleteCaseDoc.Visible = false;
                        else
                            lnkBtnDeleteCaseDoc.Visible = true;
                    }
                }
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        public void DeleteCaseFile(int caseFileID)
        {
            try
            {
                if (caseFileID != 0)
                {
                    if (CaseManagement.DeleteCaseDocument(caseFileID, AuthenticationHelper.UserID))
                    {
                        cvCasePopUp.IsValid = false;
                        cvCasePopUp.ErrorMessage = "Document Deleted Successfully.";
                    }
                    else
                    {
                        cvCasePopUp.IsValid = false;
                        cvCasePopUp.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddCase_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;

                enableDisableCasePopUpControls(true);

                grdCaseDocuments.DataSource = null;
                grdCaseDocuments.DataBind();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnEditCase_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    int caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);

                    if (caseInstanceID != 0)
                    {
                        ViewState["Mode"] = 1;

                        var caseRecord = CaseManagement.GetCaseByID(caseInstanceID);

                        if (caseRecord != null)
                        {
                            enableDisableCaseControls(false);

                            if (caseRecord.CaseType != null)
                            {
                                if (caseRecord.CaseType.ToString() == "I")
                                    rbCaseInOutType.SelectedValue = "I";
                                else if (caseRecord.CaseType.ToString() == "O")
                                    rbCaseInOutType.SelectedValue = "O";
                            }

                            if (caseRecord.OpenDate != null)
                                txtCaseDate.Text = Convert.ToDateTime(caseRecord.OpenDate).ToString("dd-MM-yyyy");

                            tbxRefNo.Text = caseRecord.CaseRefNo;

                            tbxTitle.Text = caseRecord.CaseTitle;

                            var ListofAct = CaseManagement.GetListOfAct(caseInstanceID, 1);
                            ddlAct.ClearSelection();
                            if (ddlAct.Items.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(ListofAct.ToString()))
                                {
                                    foreach (var item in ListofAct)
                                    {
                                        ddlAct.Items.FindByValue(item.ActID.ToString()).Selected = true;
                                    }
                                }
                            }

                            tbxSection.Text = caseRecord.Section;

                            if (caseRecord.CaseCategoryID != null)
                            {
                                ddlCaseCategory.ClearSelection();

                                if (ddlCaseCategory.Items.FindByValue(caseRecord.CaseCategoryID.ToString()) != null)
                                    ddlCaseCategory.SelectedValue = caseRecord.CaseCategoryID.ToString();
                            }

                            var ListofParty = CaseManagement.GetListOfParty(caseInstanceID, 1);
                            ddlParty.ClearSelection();
                            if (ddlParty.Items.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(ListofParty.ToString()))
                                {
                                    foreach (var item in ListofParty)
                                    {
                                        ddlParty.Items.FindByValue(item.PartyID.ToString()).Selected = true;
                                    }
                                }
                            }

                            if (caseRecord.CourtID != null)
                            {
                                ddlCourt.ClearSelection();

                                if (ddlCourt.Items.FindByValue(caseRecord.CourtID.ToString()) != null)
                                    ddlCourt.SelectedValue = caseRecord.CourtID.ToString();
                            }

                            tbxJudge.Text = caseRecord.Judge;

                            tbxDescription.Text = caseRecord.CaseDetailDesc;

                            if (caseRecord.CustomerBranchID != 0)
                            {
                                foreach (TreeNode node in tvBranches.Nodes)
                                {
                                    if (node.Value == caseRecord.CustomerBranchID.ToString())
                                    {
                                        node.Selected = true;
                                    }
                                    foreach (TreeNode item1 in node.ChildNodes)
                                    {
                                        if (item1.Value == caseRecord.CustomerBranchID.ToString())
                                            item1.Selected = true;
                                    }
                                }
                            }

                            tvBranches_SelectedNodeChanged(null, null);

                            ddlDepartment.ClearSelection();

                            if (ddlDepartment.Items.FindByValue(caseRecord.DepartmentID.ToString()) != null)
                                ddlDepartment.SelectedValue = caseRecord.DepartmentID.ToString();

                            if (caseRecord.OwnerID != null)
                            {
                                ddlOwner.ClearSelection();

                                if (ddlOwner.Items.FindByValue(caseRecord.OwnerID.ToString()) != null)
                                    ddlOwner.SelectedValue = caseRecord.OwnerID.ToString();
                            }

                            if (caseRecord.CaseRiskID != null)
                            {
                                ddlCaseRisk.ClearSelection();

                                if (ddlCaseRisk.Items.FindByValue(caseRecord.CaseRiskID.ToString()) != null)
                                    ddlCaseRisk.SelectedValue = caseRecord.CaseRiskID.ToString();
                            }

                            if (caseRecord.ClaimAmt != null)
                                tbxClaimedAmt.Text = caseRecord.ClaimAmt.ToString();
                            else
                                tbxClaimedAmt.Text = "";

                            if (caseRecord.ProbableAmt != null)
                                tbxProbableAmt.Text = caseRecord.ProbableAmt.ToString();
                            else
                                tbxProbableAmt.Text = "";

                            //Potential Impact
                            if (caseRecord.ImpactType != null)
                            {
                                if (caseRecord.ImpactType.ToString() == "M")
                                    rblPotentialImpact.SelectedValue = "M";
                                else if (caseRecord.ImpactType.ToString() == "N")
                                    rblPotentialImpact.SelectedValue = "N";
                                else if (caseRecord.ImpactType.ToString() == "B")
                                    rblPotentialImpact.SelectedValue = "B";
                            }

                            if (caseRecord.Monetory != null)
                                tbxMonetory.Text = caseRecord.Monetory.ToString();
                            else
                                tbxMonetory.Text = "";

                            if (caseRecord.NonMonetory != null)
                                tbxNonMonetory.Text = caseRecord.NonMonetory.ToString();
                            else
                                tbxNonMonetory.Text = "";

                            if (caseRecord.Years != null)
                                tbxNonMonetoryYears.Text = caseRecord.Years.ToString();
                            else
                                tbxNonMonetoryYears.Text = "";

                            //Get Lawyer Mapping
                            var lstCaseLawyer = CaseManagement.GetCaseLawyerMapping(caseInstanceID);

                            if (lstCaseLawyer != null)
                            {
                                ddlLawFirm.ClearSelection();
                                ddlLawFirm.SelectedValue = Convert.ToString(lstCaseLawyer.LawyerID);
                            }

                            //Get Case User Assignment
                            var lstCaseAssignment = CaseManagement.GetCaseAssignment(caseInstanceID);

                            if (lstCaseAssignment.Count > 0)
                            {
                                lstBoxPerformer.ClearSelection();
                                ddlReviewer.ClearSelection();
                                lstBoxLawyerUser.ClearSelection();

                                foreach (var eachAssignmentRecord in lstCaseAssignment)
                                {
                                    if (eachAssignmentRecord.RoleID == 3)
                                    {
                                        if (lstBoxPerformer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxPerformer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;

                                        if (lstBoxLawyerUser.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxLawyerUser.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;

                                        //if (ddlPerformer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                        //    ddlPerformer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;

                                        //if (ddlLawyerUser.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                        //    ddlLawyerUser.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                    else if (eachAssignmentRecord.RoleID == 4)
                                    {
                                        if (ddlReviewer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            ddlReviewer.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                }
                            }

                            //Case Stage
                            if (caseRecord.CaseStageID != null)
                            {
                                ddlCaseStage.ClearSelection();

                                if (ddlCaseStage.Items.FindByValue(caseRecord.CaseStageID.ToString()) != null)
                                    ddlCaseStage.SelectedValue = caseRecord.CaseStageID.ToString();
                            }

                            //Case Status 
                            var StatusDetails = CaseManagement.GetCaseStatusDetail(caseInstanceID);

                            if (StatusDetails != null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(StatusDetails.TxnStatusID)))
                                {
                                    ddlCaseStatus.ClearSelection();

                                    if (ddlCaseStatus.Items.FindByValue(StatusDetails.TxnStatusID.ToString()) != null)
                                        ddlCaseStatus.SelectedValue = StatusDetails.TxnStatusID.ToString();

                                    ViewState["caseStatus"] = Convert.ToInt32(StatusDetails.TxnStatusID);
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(StatusDetails.CloseDate)))
                                {
                                    tbxCaseCloseDate.Text = Convert.ToString(StatusDetails.CloseDate);
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(StatusDetails.ClosureRemark)))
                                {
                                    tbxCloseRemark.Text = StatusDetails.ClosureRemark;
                                }
                            }

                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "changeCaseStatus", "ddlStatusChange();", true);
                            //Case Status Log--End

                            //Bind Case Related Documents
                            BindCaseRelatedDocuments(caseInstanceID);

                            //Bind Case Hearing Details
                            BindCaseResponses(caseInstanceID);

                            //Bind Case Task Details
                            BindCaseTasks(caseInstanceID, grdTaskActivity);

                            //Bind Case Order Details
                            BindCaseOrders(caseInstanceID);

                            //Bind Case Payment Details
                            BindCasePayments(caseInstanceID);
                        }

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void DownloadCaseDocument(int caseFileID)
        {
            try
            {
                var file = CaseManagement.GetCaseDocumentByID(caseFileID);

                if (file != null)
                {
                    if (file.FilePath != null)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                        if (filePath != null && File.Exists(filePath))
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(file.FileName));
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #endregion

        #region Case-Hearing

        public void BindCaseResponses(int caseInstanceID)
        {
            try
            {
                var lstCaseResponses = CaseManagement.GetCaseResponseDetails(caseInstanceID);

                if (lstCaseResponses != null && lstCaseResponses.Count > 0)
                    lstCaseResponses = lstCaseResponses.OrderByDescending(entry => entry.HearingDate).ToList();

                grdResponseLog.DataSource = lstCaseResponses;
                grdResponseLog.DataBind();

                lstCaseResponses.Clear();
                lstCaseResponses = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public bool DeleteCaseResponse(long caseInstanceID, int caseResponseID)
        {
            try
            {
                if (caseResponseID != 0)
                {
                    //Delete Hearing with Documents
                    if (CaseManagement.DeleteCaseResponseLog(caseInstanceID, caseResponseID, AuthenticationHelper.UserID))
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        protected void grdResponseLog_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    grdResponseLog.PageIndex = e.NewPageIndex;

                    BindCaseResponses(Convert.ToInt32(ViewState["CaseInstanceID"]));

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "gridPageIndexChanged", "gridPageIndexChanged();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int responseID = Convert.ToInt32(commandArgs[0]);//HearingID
                        int CaseInstanceID = Convert.ToInt32(commandArgs[1]);

                        if (responseID != 0 && CaseInstanceID != 0)
                        {
                            var lstResponseDocument = CaseManagement.GetCaseResponseDocuments(CaseInstanceID, responseID, "CH");

                            if (lstResponseDocument.Count > 0)
                            {
                                using (ZipFile responseDocZip = new ZipFile())
                                {
                                    int i = 0;
                                    foreach (var file in lstResponseDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                            if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    responseDocZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                    Response.BinaryWrite(Filedata);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                            }
                            else
                            {
                                cvCasePopUpResponse.IsValid = false;
                                cvCasePopUpResponse.ErrorMessage = "No Document Available for Download.";
                                return;
                            }

                            DownloadCaseDocument(Convert.ToInt32(e.CommandArgument)); //Parameter - ResponseID
                        }
                    }
                    else if (e.CommandName.Equals("DeleteResponse"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int responseID = Convert.ToInt32(commandArgs[0]); //HearingID
                        int CaseInstanceID = Convert.ToInt32(commandArgs[1]);

                        if (responseID != 0 && CaseInstanceID != 0)
                        {
                            var deleteSuccess = DeleteCaseResponse(CaseInstanceID, responseID); //Parameter - ResponseID and caseInstanceID

                            //Re-Bind Case Responses
                            if (ViewState["CaseInstanceID"] != null)
                            {
                                BindCaseResponses(Convert.ToInt32(ViewState["CaseInstanceID"]));
                            }

                            if (deleteSuccess)
                            {
                                cvCasePopUpResponse.IsValid = false;
                                cvCasePopUpResponse.ErrorMessage = "Hearing Detail Deleted Successfully.";
                            }
                            else
                            {
                                cvCasePopUpResponse.IsValid = false;
                                cvCasePopUpResponse.ErrorMessage = "Something went wrong, Please try again.";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdResponseLog_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownLoadResponseDoc = (LinkButton) e.Row.FindControl("lnkBtnDownLoadResponseDoc");

            if (lnkBtnDownLoadResponseDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownLoadResponseDoc);
            }

            LinkButton lnkBtnDeleteResponse = (LinkButton) e.Row.FindControl("lnkBtnDeleteResponse");
            if (lnkBtnDeleteResponse != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteResponse);

                if (ViewState["caseStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["caseStatus"]) == 3)
                        lnkBtnDeleteResponse.Visible = false;
                    else
                        lnkBtnDeleteResponse.Visible = true;
                }
            }

            //Bind Response Related Tasks

            if (ViewState["CaseInstanceID"] != null)
            {
                long caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                if (caseInstanceID != 0)
                {
                    GridView grdResTaskActivity = e.Row.FindControl("grdResTaskActivity") as GridView;

                    HtmlImage imgCollapseExpand = e.Row.FindControl("imgCollapseExpand") as HtmlImage;

                    if (grdResTaskActivity != null)
                    {
                        Label lblRefID = (Label) e.Row.FindControl("lblRefID");

                        if (lblRefID != null)
                        {
                            long refID = Convert.ToInt32(lblRefID.Text);

                            if (refID != 0)
                            {
                                var dataBindSuccess = BindCaseResponseTasks(caseInstanceID, refID, grdResTaskActivity);

                                if (imgCollapseExpand != null)
                                {
                                    if (dataBindSuccess)
                                        imgCollapseExpand.Visible = true;
                                    else
                                        imgCollapseExpand.Visible = false;
                                }
                            }
                        }
                    }

                }
            } //Bind Response Task - END
        }

        protected void grdResponseLog_RowCreated(object sender, GridViewRowEventArgs e)
        {
            string rowID = String.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                rowID = "row" + e.Row.RowIndex;

                e.Row.Attributes.Add("id", "row" + e.Row.RowIndex);
                //e.Row.Attributes.Add("onclick", "ChangeRowColor(" + "'" + rowID + "'" + ")");
            }
        }

        protected void upResponseDocUpload_Load(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "bindAddCollapse", "imgExpandCollapse();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public string ShowCaseResponseDocCount(long caseInstanceID, long noticeResponseID)
        {
            try
            {
                var docCount = CaseManagement.GetCaseResponseDocuments(caseInstanceID, noticeResponseID, "CH").Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        #endregion

        #region Case-Task-Activity       

        public void BindCaseTasks(int caseInstanceID, GridView grd)
        {
            try
            {
                var lstCaseTasks = LitigationTaskManagement.GetTaskDetails(caseInstanceID, "C");

                grd.DataSource = lstCaseTasks;
                grd.DataBind();

                lstCaseTasks.Clear();
                lstCaseTasks = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpTask.IsValid = false;
                cvCasePopUpTask.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public bool BindCaseResponseTasks(long caseInstanceID, long refID, GridView grd)
        {
            try
            {
                var lstResponseTasks = LitigationTaskManagement.GetResponseTaskDetails(caseInstanceID, refID, "C");

                if (lstResponseTasks != null && lstResponseTasks.Count > 0)
                {
                    grd.DataSource = lstResponseTasks;
                    grd.DataBind();

                    lstResponseTasks.Clear();
                    lstResponseTasks = null;

                    return true;
                }
                else
                {
                    grd.DataSource = null;
                    grd.DataBind();

                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpResponse.IsValid = false;
                cvCasePopUpResponse.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }

        public string ShowTaskResponseDocCount(long taskID, long taskResponseID)
        {
            try
            {
                var docCount = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID).Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected void grdTaskActivity_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkBtnTaskReminder = (LinkButton) e.Row.FindControl("lnkBtnTaskReminder");

                if (lnkBtnTaskReminder != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnTaskReminder);
                }

                LinkButton lnkBtnTaskResponse = (LinkButton) e.Row.FindControl("lnkBtnTaskResponse");

                if (lnkBtnTaskResponse != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnTaskResponse);
                }

                LinkButton lnkBtnDeleteTask = (LinkButton) e.Row.FindControl("lnkBtnDeleteTask");

                if (lnkBtnDeleteTask != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeleteTask);
                }

                Label lblTaskStatus = e.Row.FindControl("lblTaskStatus") as Label;

                HtmlImage imgCollapseExpand = e.Row.FindControl("imgCollapseExpand") as HtmlImage;

                if (lblTaskStatus != null)
                {
                    if (lblTaskStatus.Text != "" && lblTaskStatus.Text != "Open")
                    {
                        GridView gvTaskResponses = e.Row.FindControl("gvTaskResponses") as GridView;

                        if (gvTaskResponses != null)
                        {
                            if (grdTaskActivity.DataKeys[e.Row.RowIndex].Value != null)
                            {
                                int taskID = 0;
                                taskID = Convert.ToInt32(grdTaskActivity.DataKeys[e.Row.RowIndex].Value);

                                var dataBindSuccess = BindTaskResponses(taskID, gvTaskResponses);

                                if (imgCollapseExpand != null)
                                    if (dataBindSuccess)
                                        imgCollapseExpand.Visible = true;
                                    else
                                        imgCollapseExpand.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        if (imgCollapseExpand != null)
                            imgCollapseExpand.Visible = false;
                    }

                    //Hide Close Task Button
                    LinkButton lnkBtnCloseTask = e.Row.FindControl("lnkBtnCloseTask") as LinkButton;
                    if (lnkBtnCloseTask != null)
                    {
                        if (lblTaskStatus.Text != "" && lblTaskStatus.Text == "Closed")
                            lnkBtnCloseTask.Visible = false;
                        else
                            lnkBtnCloseTask.Visible = true;
                    }
                }
            }
        }

        protected void grdTaskActivity_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    grdTaskActivity.PageIndex = e.NewPageIndex;

                    //Re-Bind Case Related Tasks
                    BindCaseTasks(Convert.ToInt32(ViewState["CaseInstanceID"]), grdTaskActivity);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "gridPageIndexChanged", "gridPageIndexChanged();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdResTaskActivity_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    GridView grdResTaskActivity = (sender as GridView);

                    if (grdResTaskActivity != null)
                    {
                        grdResTaskActivity.PageIndex = e.NewPageIndex;

                        //Re-Bind Hearing Related Tasks
                        BindCaseTasks(Convert.ToInt32(ViewState["CaseInstanceID"]), grdResTaskActivity);

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "gridPageIndexChanged", "gridPageIndexChanged();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskActivity_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (ViewState["CaseInstanceID"] != null)
                    {
                        int CaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                        int taskID = Convert.ToInt32(e.CommandArgument);

                        if (e.CommandName.Equals("DeleteTask"))
                        {
                            DeleteTask(taskID,AuthenticationHelper.CustomerID);

                            //Re-Bind Case Task
                            BindCaseTasks(Convert.ToInt32(ViewState["CaseInstanceID"]), grdTaskActivity);
                        }
                        else if (e.CommandName.Equals("CloseTask"))
                        {
                            //Update Task Status to Closed and Expire URL
                            LitigationTaskManagement.UpdateTaskStatus(taskID, 3, AuthenticationHelper.UserID, AuthenticationHelper.CustomerID); //Status 3 - Closed

                            //Re-Bind Case Task
                            BindCaseTasks(Convert.ToInt32(ViewState["CaseInstanceID"]), grdTaskActivity);
                        }
                        else if (e.CommandName.Equals("TaskReminder")) //Send Reminder or Re-generate URL
                        {
                            if (taskID != 0)
                            {
                                string accessURL = string.Empty;
                                bool sendSuccess = false;
                                string portalURL = string.Empty;
                                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                if (Urloutput != null)
                                {
                                    portalURL = Urloutput.URL;
                                }
                                else
                                {
                                    portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                }
                                //Send Mail to User if Internal then Task Assigned Mail and External Task Assigned with Link Detail
                                accessURL = Convert.ToString(portalURL) + "/Litigation/aspxPages/myTask.aspx?TaskID=" +
                                 CryptographyManagement.Encrypt(taskID.ToString()) +
                                 "&NID=" + CryptographyManagement.Encrypt(CaseInstanceID.ToString());
                                //accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]) + "/Litigation/aspxPages/myTask.aspx?TaskID=" +
                                //    CryptographyManagement.Encrypt(taskID.ToString()) +
                                //    "&NID=" + CryptographyManagement.Encrypt(CaseInstanceID.ToString());

                                //Get Task Record
                                var taskRecord = LitigationTaskManagement.GetTaskDetailByTaskID(CaseInstanceID, taskID, "C", AuthenticationHelper.CustomerID);

                                if (taskRecord != null)
                                {
                                    sendSuccess = SendTaskAssignmentMail(taskRecord, accessURL, AuthenticationHelper.User);

                                    if (sendSuccess)
                                    {
                                        cvCasePopUpTask.ErrorMessage = "An Email containing task detail and access URL to provide response sent to assignee.";

                                        taskRecord.AccessURL = accessURL;
                                        taskRecord.UpdatedBy = AuthenticationHelper.UserID;
                                        sendSuccess = LitigationTaskManagement.UpdateTaskAccessURL(taskRecord.ID, taskRecord);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpTask.IsValid = false;
                cvCasePopUpTask.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTaskActivity_RowCreated(object sender, GridViewRowEventArgs e)
        {
            string rowID = String.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                rowID = "row" + e.Row.RowIndex;

                e.Row.Attributes.Add("id", "row" + e.Row.RowIndex);
                e.Row.Attributes.Add("onclick", "ChangeRowColor(" + "'" + rowID + "'" + ")");
            }
        }

        public void DeleteTask(int taskID,long CustomerID)
        {
            try
            {
                if (taskID != 0)
                {
                    if (LitigationTaskManagement.DeleteTask(taskID, AuthenticationHelper.UserID, CustomerID))
                    {
                        cvCasePopUpTask.IsValid = false;
                        cvCasePopUpTask.ErrorMessage = "Task Detail Deleted Successfully.";
                    }
                    else
                    {
                        cvCasePopUpTask.IsValid = false;
                        cvCasePopUpTask.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpTask.IsValid = false;
                cvCasePopUpTask.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public bool SendTaskAssignmentMail(tbl_TaskScheduleOn taskRecord, string accessURL, string assignedBy)
        {
            try
            {
                if (taskRecord != null)
                {
                    User User = UserManagement.GetByID(Convert.ToInt32(taskRecord.AssignTo));

                    if (User != null)
                    {
                        if (User.Email != null && User.Email != "")
                        {
                            string taskPriority = string.Empty;

                            if (taskRecord.PriorityID != null)
                            {
                                if (taskRecord.PriorityID == 1)
                                    taskPriority = "High";
                                else if (taskRecord.PriorityID == 2)
                                    taskPriority = "Medium";
                                else if (taskRecord.PriorityID == 3)
                                    taskPriority = "Low";
                            }

                            string username = string.Format("{0} {1}", User.FirstName, User.LastName);
                            string portalURL = string.Empty;
                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (Urloutput != null)
                            {
                                portalURL = Urloutput.URL;
                            }
                            else
                            {
                                portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                            }
                            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_TaskAssignment
                                                  .Replace("@User", username)
                                                  .Replace("@TaskTitle", taskRecord.TaskTitle)
                                                  .Replace("@TaskDesc", taskRecord.TaskDesc)
                                                  .Replace("@Priority", taskPriority)
                                                  .Replace("@DueDate", taskRecord.ScheduleOnDate.ToString("dd-MM-yyyy"))
                                                  .Replace("@AssignedBy", assignedBy)
                                                  .Replace("@Remark", taskRecord.Remark)
                                                  .Replace("@AccessURL", accessURL) //Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])
                                                  .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]))
                                                  .Replace("@PortalURL", Convert.ToString(portalURL));


                            //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_TaskAssignment
                            //                        .Replace("@User", username)
                            //                        .Replace("@TaskTitle", taskRecord.TaskTitle)
                            //                        .Replace("@TaskDesc", taskRecord.TaskDesc)
                            //                        .Replace("@Priority", taskPriority)
                            //                        .Replace("@DueDate", taskRecord.ScheduleOnDate.ToString("dd-MM-yyyy"))
                            //                        .Replace("@AssignedBy", assignedBy)
                            //                        .Replace("@Remark", taskRecord.Remark)
                            //                        .Replace("@AccessURL", accessURL) //Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])
                            //                        .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]))
                            //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { User.Email }), null, null, "Litigation Notification-Task Assigned", message);

                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public bool BindTaskResponses(int taskID, GridView grd)
        {
            try
            {
                List<tbl_TaskResponse> lstTaskResponses = new List<tbl_TaskResponse>();

                lstTaskResponses = LitigationTaskManagement.GetTaskResponseDetails(taskID);

                if (lstTaskResponses != null && lstTaskResponses.Count > 0)
                {
                    lstTaskResponses = lstTaskResponses.OrderByDescending(entry => entry.UpdatedOn).ThenByDescending(entry => entry.CreatedOn).ToList();
                    grd.DataSource = lstTaskResponses;
                    grd.DataBind();

                    return true;
                }
                else
                {
                    grd.DataSource = null;
                    grd.DataBind();

                    return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpTask.IsValid = false;
                cvCasePopUpTask.ErrorMessage = "Server Error Occured. Please try again.";

                return false;
            }
        }

        public void DeleteTaskResponse(int taskResponseID)
        {
            try
            {
                if (taskResponseID != 0)
                {
                    //Delete Response with Documents
                    if (LitigationTaskManagement.DeleteTaskResponseLog(taskResponseID, AuthenticationHelper.UserID))
                    {
                        cvCasePopUpTask.IsValid = false;
                        cvCasePopUpTask.ErrorMessage = "Response Deleted Successfully.";
                    }
                    else
                    {
                        cvCasePopUpTask.IsValid = false;
                        cvCasePopUpTask.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpTask.IsValid = false;
                cvCasePopUpTask.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTaskResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int taskResponseID = Convert.ToInt32(commandArgs[0]);
                        int taskID = Convert.ToInt32(commandArgs[1]);

                        if (taskResponseID != 0 && taskID != 0)
                        {
                            var lstTaskResponseDocument = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID);

                            if (lstTaskResponseDocument.Count > 0)
                            {
                                using (ZipFile responseDocZip = new ZipFile())
                                {
                                    int i = 0;
                                    foreach (var file in lstTaskResponseDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                            if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    responseDocZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                    Response.BinaryWrite(Filedata);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                            }
                            else
                            {
                                cvCasePopUpTask.IsValid = false;
                                cvCasePopUpTask.ErrorMessage = "No Document Available for Download.";
                                return;
                            }
                        }
                    }
                    else if (e.CommandName.Equals("DeleteTaskResponse"))
                    {
                        DeleteTaskResponse(Convert.ToInt32(e.CommandArgument)); //Parameter - TaskResponseID 

                        //Bind Task Responses
                        if (ViewState["TaskID"] != null)
                        {
                            BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]), grdTaskActivity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpTask.IsValid = false;
                cvCasePopUpTask.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTaskResponseLog_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownloadTaskResDoc = (LinkButton) e.Row.FindControl("lnkBtnDownloadTaskResDoc");

            if (lnkBtnDownloadTaskResDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownloadTaskResDoc);
            }

            LinkButton lnkBtnDeleteTaskResponse = (LinkButton) e.Row.FindControl("lnkBtnDeleteTaskResponse");
            if (lnkBtnDeleteTaskResponse != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteTaskResponse);

                if (ViewState["caseStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["caseStatus"]) == 3)
                        lnkBtnDeleteTaskResponse.Visible = false;
                    else
                        lnkBtnDeleteTaskResponse.Visible = true;
                }
            }
        }

        #endregion

        #region Case-Status
        protected void btnSaveStatus_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;

                if (ViewState["CaseInstanceID"] != null)
                {
                    if (!String.IsNullOrEmpty(ddlCaseStatus.SelectedValue) && ddlCaseStatus.SelectedValue != "0")
                    {
                        if (!String.IsNullOrEmpty(ddlCaseStage.SelectedValue) && ddlCaseStage.SelectedValue != "0")
                        {
                            long caseInstanceID = 0;
                            caseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);

                            if (caseInstanceID != 0)
                            {
                                int selectedStatusID = Convert.ToInt32(ddlCaseStatus.SelectedValue);
                                int selectedStageID = Convert.ToInt32(ddlCaseStage.SelectedValue);

                                //Update Case Stage in Case Instance Table
                                saveSuccess = CaseManagement.UpdateCaseStage(caseInstanceID, selectedStageID, AuthenticationHelper.UserID);

                                if (saveSuccess)
                                {
                                    //Status Transaction Record - Which will Create or Update on Each Status Move
                                    tbl_LegalCaseStatusTransaction newStatusTxnRecord = new tbl_LegalCaseStatusTransaction()
                                    {
                                        CaseInstanceID = caseInstanceID,
                                        StatusID = selectedStatusID,
                                        StatusChangeOn = DateTime.Now,
                                        IsActive = true,
                                        IsDeleted = false,
                                        UserID = AuthenticationHelper.UserID,
                                        RoleID = 3,
                                        CreatedBy = AuthenticationHelper.UserID,
                                        UpdatedBy = AuthenticationHelper.UserID,
                                    };

                                    //Status Record - i.e. Case Closure Record; Only Active on Case Close otherwise DeActive 
                                    tbl_LegalCaseStatus newStatusRecord = new tbl_LegalCaseStatus()
                                    {
                                        CaseInstanceID = caseInstanceID,
                                        StatusID = selectedStatusID,
                                        CloseDate = DateTime.Now,
                                        IsActive = true,
                                        IsDeleted = false,

                                        CreatedBy = AuthenticationHelper.UserID,
                                        UpdatedBy = AuthenticationHelper.UserID,
                                    };

                                    if (tbxCloseRemark.Text != "")
                                        newStatusRecord.ClosureRemark = tbxCloseRemark.Text;

                                    if (ddlCaseStatus.SelectedValue != "3") //Open or In Progress
                                    {
                                        if (!CaseManagement.ExistCaseStatusTransaction(newStatusTxnRecord))
                                        {
                                            saveSuccess = CaseManagement.DeActiveCaseStatusTransaction(newStatusTxnRecord);
                                            saveSuccess = CaseManagement.CreateCaseStatusTransaction(newStatusTxnRecord);
                                        }
                                        else
                                            saveSuccess = CaseManagement.UpdateCaseStatusTransaction(newStatusTxnRecord);

                                        //If Exists Case Closure Record then DeActive it
                                        if (CaseManagement.ExistCaseStatus(newStatusRecord))
                                            saveSuccess = CaseManagement.UpdateCaseStatus(newStatusRecord);
                                    }
                                    else if (ddlCaseStatus.SelectedValue == "3") //Close
                                    {
                                        if (tbxCaseCloseDate.Text != "")
                                        {
                                            //Create or Update Status Transaction Records
                                            if (!CaseManagement.ExistCaseStatusTransaction(newStatusTxnRecord))
                                            {
                                                saveSuccess = CaseManagement.DeActiveCaseStatusTransaction(newStatusTxnRecord);
                                                saveSuccess = CaseManagement.CreateCaseStatusTransaction(newStatusTxnRecord);
                                            }
                                            else
                                                saveSuccess = CaseManagement.UpdateCaseStatusTransaction(newStatusTxnRecord);

                                            //Create or Update Status Record
                                            if (!CaseManagement.ExistCaseStatus(newStatusRecord))
                                                saveSuccess = CaseManagement.CreateCaseStatus(newStatusRecord);
                                            else
                                                saveSuccess = CaseManagement.UpdateCaseStatus(newStatusRecord);
                                        }
                                        else
                                        {
                                            cvCaseStatus.IsValid = false;
                                            cvCaseStatus.ErrorMessage = "Please Provide Close Date.";
                                            return;
                                        }
                                    }
                                }

                                if (saveSuccess)
                                {
                                    ViewState["caseStatus"] = selectedStatusID;

                                    cvCaseStatus.IsValid = false;
                                    cvCaseStatus.ErrorMessage = "Details Saved Successfully.";
                                    return;
                                }
                            }
                        }
                        else
                        {
                            cvCaseStatus.IsValid = false;
                            cvCaseStatus.ErrorMessage = "Select Case Stage.";
                            return;
                        }
                    }
                    else
                    {
                        cvCaseStatus.IsValid = false;
                        cvCaseStatus.ErrorMessage = "Select Case Status.";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Case-Payment

        public void BindCasePayments(int caseInstanceID)
        {
            try
            {
                List<tbl_NoticeCasePayment> lstCasePayments = new List<tbl_NoticeCasePayment>();

                lstCasePayments = CaseManagement.GetNoticeCasePaymentDetails(caseInstanceID, "C");

                grdCasePayment.DataSource = lstCasePayments;
                grdCasePayment.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePayment.IsValid = false;
                cvCasePayment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCasePayment_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DeletePayment"))
                    {
                        DeletePaymentLog(Convert.ToInt32(e.CommandArgument));

                        //Re-Bind Case Payments
                        if (ViewState["CaseInstanceID"] != null)
                        {
                            BindCasePayments(Convert.ToInt32(ViewState["CaseInstanceID"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUp.IsValid = false;
                cvCasePopUp.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCasePayment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkBtnDeletePayment = (LinkButton) e.Row.FindControl("lnkBtnDeletePayment");
                if (lnkBtnDeletePayment != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeletePayment);
                }
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                DropDownList ddlPaymentType = (DropDownList) e.Row.FindControl("ddlPaymentType");

                if (ddlPaymentType != null)
                    BindPaymentType(ddlPaymentType);
            }
        }

        protected void grdCasePayment_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    grdCasePayment.PageIndex = e.NewPageIndex;

                    //Re-Bind Case Payments
                    BindCasePayments(Convert.ToInt32(ViewState["CaseInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnPaymentSave_Click(object sender, EventArgs e)
        {
            try
            {
                TextBox tbxPaymentDate = (TextBox) grdCasePayment.FooterRow.FindControl("tbxPaymentDate");
                DropDownList ddlPaymentType = (DropDownList) grdCasePayment.FooterRow.FindControl("ddlPaymentType");
                TextBox tbxAmount = (TextBox) grdCasePayment.FooterRow.FindControl("tbxAmount");
                TextBox tbxPaymentRemark = (TextBox) grdCasePayment.FooterRow.FindControl("tbxPaymentRemark");

                if (ViewState["CaseInstanceID"] != null)
                {
                    if (tbxPaymentDate != null && ddlPaymentType != null && tbxAmount != null && tbxPaymentRemark != null)
                    {
                        bool validateData = false;
                        bool saveSuccess = false;

                        if (tbxPaymentDate.Text != "")
                        {
                            if (!String.IsNullOrEmpty(ddlPaymentType.SelectedValue))
                            {
                                if (tbxAmount.Text != "")
                                {
                                    if (tbxPaymentRemark.Text != "")
                                    {
                                        try
                                        {
                                            Convert.ToDecimal(tbxAmount.Text);
                                            validateData = true;
                                        }
                                        catch (Exception ex)
                                        {
                                            validateData = false;
                                        }
                                    }
                                }
                            }
                        }

                        if (validateData)
                        {
                            tbl_NoticeCasePayment newRecord = new tbl_NoticeCasePayment()
                            {
                                NoticeOrCase = "C",
                                IsActive = true,
                                NoticeOrCaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]),
                                PaymentDate = DateTimeExtensions.GetDate(tbxPaymentDate.Text),
                                PaymentID =Convert.ToInt32(ddlPaymentType.SelectedValue),
                                Amount = Convert.ToDecimal(tbxAmount.Text),
                                CreatedBy = AuthenticationHelper.UserID,
                            };

                            if (tbxPaymentRemark.Text != "")
                                newRecord.Remark = tbxPaymentRemark.Text;

                            saveSuccess = CaseManagement.CreateCasePaymentLog(newRecord);
                        }

                        if (saveSuccess)
                        {
                            cvCasePayment.IsValid = false;
                            cvCasePayment.ErrorMessage = "Payment Detail Save Successfully.";

                            //Re-Bind Case Payment Log Details
                            BindCasePayments(Convert.ToInt32(ViewState["CaseInstanceID"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void DeletePaymentLog(int noticePaymentID)
        {
            try
            {
                if (noticePaymentID != 0)
                {
                    if (CaseManagement.DeleteCasePaymentLog(noticePaymentID, AuthenticationHelper.UserID))
                    {
                        cvCasePayment.IsValid = false;
                        cvCasePayment.ErrorMessage = "Action Detail Deleted Successfully.";
                    }
                    else
                    {
                        cvCasePayment.IsValid = false;
                        cvCasePayment.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePayment.IsValid = false;
                cvCasePayment.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #endregion

        #region Case-Order

        public void BindCaseOrders(int caseInstanceID)
        {
            try
            {
                var lstCaseOrders = CaseManagement.GetCaseOrderDetails(caseInstanceID);

                grdCaseOrder.DataSource = lstCaseOrders;
                grdCaseOrder.DataBind();

                lstCaseOrders.Clear();
                lstCaseOrders = null;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCaseOrderPopup.IsValid = false;
                cvCaseOrderPopup.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string ShowOrderType(int orderTypeID)
        {
            try
            {
                return CaseManagement.GetOrderTypeByID(orderTypeID, "O");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public string ShowOrderDocCount(long CaseInstanceID, long orderID)
        {
            try
            {
                var docCount = CaseManagement.GetCaseOrderDocuments(CaseInstanceID, orderID, "CO").Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected void grdCaseOrder_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblOrderType = (Label) e.Row.FindControl("lblOrderType");

                if (lblOrderType != null)
                {
                    lblOrderType.ToolTip = lblOrderType.Text;
                }

                LinkButton lnkBtnDownloadOrderDoc = (LinkButton) e.Row.FindControl("lnkBtnDownloadOrderDoc");

                if (lnkBtnDownloadOrderDoc != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDownloadOrderDoc);
                }

                LinkButton lnkBtnDeleteOrder = (LinkButton) e.Row.FindControl("lnkBtnDeleteOrder");

                if (lnkBtnDeleteOrder != null)
                {
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    scriptManager.RegisterPostBackControl(lnkBtnDeleteOrder);

                    if (ViewState["caseStatus"] != null)
                    {
                        if (Convert.ToInt32(ViewState["caseStatus"]) == 3)
                            lnkBtnDeleteOrder.Visible = false;
                        else
                            lnkBtnDeleteOrder.Visible = true;
                    }
                }
            }
        }

        protected void grdCaseOrder_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["CaseInstanceID"] != null)
                {
                    grdCaseOrder.PageIndex = e.NewPageIndex;

                    //Re-Bind Notice Related Documents
                    BindCaseOrders(Convert.ToInt32(ViewState["CaseInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCaseOrder_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (ViewState["CaseInstanceID"] != null)
                    {
                        int CaseInstanceID = Convert.ToInt32(ViewState["CaseInstanceID"]);
                        int OrderID = Convert.ToInt32(e.CommandArgument);

                        if (e.CommandName.Equals("DeleteOrder"))
                        {
                            if (CaseInstanceID != 0 && OrderID != 0)
                            {
                                DeleteCaseOrder(CaseInstanceID, OrderID);

                                //Re-Bind Case Orders
                                BindCaseOrders(Convert.ToInt32(ViewState["CaseInstanceID"]));
                            }
                        }
                        else if (e.CommandName.Equals("DownloadCaseOrder"))
                        {
                            if (CaseInstanceID != 0 && OrderID != 0)
                            {
                                var lstCaseOrderDocument = CaseManagement.GetCaseOrderDocuments(CaseInstanceID, OrderID, "CO");

                                if (lstCaseOrderDocument.Count > 0)
                                {
                                    using (ZipFile responseDocZip = new ZipFile())
                                    {
                                        foreach (var file in lstCaseOrderDocument)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                int idx = file.FileName.LastIndexOf('.');
                                                string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                                if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                                {
                                                    if (file.EnType == "M")
                                                    {
                                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    else {
                                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                }
                                            }
                                        }

                                        var zipMs = new MemoryStream();
                                        responseDocZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] Filedata = zipMs.ToArray();
                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=CaseOrderDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                        Response.BinaryWrite(Filedata);
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                    }
                                }
                                else
                                {
                                    cvCasePopUpTask.IsValid = false;
                                    cvCasePopUpTask.ErrorMessage = "No Document Available for Download.";
                                    return;
                                }
                            }
                        }
                        else if (e.CommandName.Equals("ViewCaseOrder"))
                        {
                            var lstCaseOrderDocument = CaseManagement.GetCaseOrderDocuments(CaseInstanceID, OrderID, "CO");

                            if (lstCaseOrderDocument != null)
                            {
                                List<tbl_LitigationFileData> entitiesData = lstCaseOrderDocument.Where(entry => entry.Version != null).ToList();
                                if (lstCaseOrderDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                                    entityData.Version = "1.0";
                                    entityData.DocTypeInstanceID = OrderID;
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in lstCaseOrderDocument)
                                    {
                                        rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptDocmentVersionView.DataBind();
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);

                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            DocumentPath = FileName;

                                            DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                            lblMessage.Text = "";

                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                        }
                                        break;
                                    }
                                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCasePopUpTask.IsValid = false;
                cvCasePopUpTask.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void DeleteCaseOrder(long caseInstanceID, int orderID)
        {
            try
            {
                if (orderID != 0)
                {
                    if (CaseManagement.DeleteCaseOrderLog(caseInstanceID, orderID, AuthenticationHelper.UserID, "CO"))
                    {
                        cvCaseOrderPopup.IsValid = false;
                        cvCaseOrderPopup.ErrorMessage = "Order Detail Deleted Successfully.";
                    }
                    else
                    {
                        cvCaseOrderPopup.IsValid = false;
                        cvCaseOrderPopup.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvCaseOrderPopup.IsValid = false;
                cvCaseOrderPopup.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #endregion

        protected void lnkAddNewUser_Click(object sender, EventArgs e)
        {
            BindUsers();
        }

        protected void ddlLawFirm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLawFirm.SelectedValue))
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                var lstAllUsers = LitigationUserManagement.GetLitigationAllUsers(customerID);
                var externalUsers = LitigationUserManagement.GetRequiredUsersByLawFirm(lstAllUsers, 2, Convert.ToInt32(ddlLawFirm.SelectedValue));

                lstBoxLawyerUser.DataTextField = "Name";
                lstBoxLawyerUser.DataValueField = "ID";
                lstBoxLawyerUser.DataSource = externalUsers;
                lstBoxLawyerUser.DataBind();
            }
        }

        protected void rptDocmentVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                // var AllinOneDocumentList=null;
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    var AllinOneDocumentList = CaseManagement.GetCaseDocumentByID(Convert.ToInt32(commandArgs[2]));

                    if (AllinOneDocumentList != null)
                    {
                        string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                        if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                        {
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                            string DateFolder = Folder + "/" + File;

                            string extension = System.IO.Path.GetExtension(filePath);

                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                            if (!Directory.Exists(DateFolder))
                            {
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                            }

                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                            string FileName = DateFolder + "/" + User + "" + extension;

                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            if (AllinOneDocumentList.EnType == "M")
                            {
                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            bw.Close();
                            DocumentPath = FileName;
                            DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            lblMessage.Text = "";
                        }
                        else
                        {
                            lblMessage.Text = "There is no file to preview";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptDocmentVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton) e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }
    }
}