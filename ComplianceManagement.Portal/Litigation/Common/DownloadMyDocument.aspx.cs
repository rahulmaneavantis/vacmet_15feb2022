﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Common
{
    public partial class DownloadMyDocument : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["InstanceID"])
                && !string.IsNullOrEmpty(Request.QueryString["NoticeCaseInstanceID"])
                && !string.IsNullOrEmpty(Request.QueryString["DocType"])
                && !string.IsNullOrEmpty(Request.QueryString["DocTypeDownload"]))
            {
                string TaskId1 = Request.QueryString["InstanceID"];
                string NoticeCaseInstanceID1 = Request.QueryString["NoticeCaseInstanceID"];
                string DocType = Request.QueryString["DocType"];
                string DocTypeDownload = Request.QueryString["DocTypeDownload"];

                if (DocType == "N" || DocType == "C")
                {
                    string TypeName = string.Empty;
                    List<string> Doctypes = new List<string>();

                    if (DocTypeDownload != "-1")
                    {
                        if (DocTypeDownload == "A")
                        {
                            if (DocType == "N")
                            {
                                Doctypes.Add("A");
                                Doctypes.Add("N");
                                Doctypes.Add("NR");
                                Doctypes.Add("NT");
                                TypeName = "Notice Document";
                            }
                            else
                            {
                                Doctypes.Add("A");
                                Doctypes.Add("C");
                                Doctypes.Add("CH");
                                Doctypes.Add("CT");
                                Doctypes.Add("CO");
                                Doctypes.Add("CA");
                                TypeName = "Case Document";
                            }
                        }
                        else
                        {
                            Doctypes.Add(DocTypeDownload);
                        }
                    }
                    var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                    if (AWSData != null)
                    {
                        #region AWS
                        using (ZipFile LitigationZip = new ZipFile())
                        {
                            List<tbl_LitigationFileData> CMPDocuments = new List<tbl_LitigationFileData>();
                            CMPDocuments = LitigationDocumentManagement.GetDocuments(Convert.ToInt64(NoticeCaseInstanceID1), Doctypes);

                            if (CMPDocuments.Count > 0)
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                foreach (var file in CMPDocuments)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }
                                }

                                int i = 0;
                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        int idx = file.FileName.LastIndexOf('.');
                                        string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);
                                        string Dates = DateTime.Now.ToString("dd/mm/yyyy");

                                        LitigationZip.AddEntry("Litigation Document" + "/" + TypeName + "_" + Dates + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                       
                                        i++;
                                    }
                                }
                                var zipMs = new MemoryStream();
                                LitigationZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=LitigationDocument.zip");
                                Response.BinaryWrite(Filedata);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal
                        using (ZipFile LitigationZip = new ZipFile())
                        {
                            List<tbl_LitigationFileData> CMPDocuments = new List<tbl_LitigationFileData>();
                            CMPDocuments = LitigationDocumentManagement.GetDocuments(Convert.ToInt64(NoticeCaseInstanceID1), Doctypes);

                            if (CMPDocuments.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        int idx = file.FileName.LastIndexOf('.');
                                        string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);
                                        string Dates = DateTime.Now.ToString("dd/mm/yyyy");
                                        if (file.EnType == "M")
                                        {
                                            LitigationZip.AddEntry("Litigation Document" + "/" + TypeName + "_" + Dates + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            LitigationZip.AddEntry("Litigation Document" + "/" + TypeName + "_" + Dates + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                                var zipMs = new MemoryStream();
                                LitigationZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=LitigationDocument.zip");
                                Response.BinaryWrite(Filedata);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                            }
                        }
                        #endregion
                    }
                }
                if (DocType == "T")
                {
                    long TaskId = Convert.ToInt64(TaskId1);
                    long NoticeCaseInstanceID= Convert.ToInt64(NoticeCaseInstanceID1);
                    var lstTaskDocument = LitigationTaskManagement.GetTaskDocuments(TaskId, NoticeCaseInstanceID);

                    if (lstTaskDocument.Count > 0)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS
                            using (ZipFile responseDocZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                foreach (var file in lstTaskDocument)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }
                                }
                                int i = 0;
                                foreach (var file in lstTaskDocument)
                                {
                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        int idx = file.FileName.LastIndexOf('.');
                                        string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                        if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, DocumentManagement.ReadDocFiles(filePath));                                           
                                        }
                                        i++;
                                    }
                                }

                                var zipMs = new MemoryStream();
                                responseDocZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal
                            using (ZipFile responseDocZip = new ZipFile())
                            {
                                int i = 0;
                                foreach (var file in lstTaskDocument)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        int idx = file.FileName.LastIndexOf('.');
                                        string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                        if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                        {
                                            if (file.EnType == "M")
                                            {
                                                responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                        }
                                        i++;
                                    }
                                }

                                var zipMs = new MemoryStream();
                                responseDocZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                    }
                }
            }
        }
    }
}