﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Data;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Common
{
    public partial class BulkDocumentUpload : System.Web.UI.Page
    {
        List<string> lstErrorMsg = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindGrid();
                    bindPageNumber();
                    ShowGridDetail();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
        }

        public static List<TempDocument> getUploadedDocList(int ProductID, string productype)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var AllDocument = (from row in entities.TempDocuments
                                   where row.IsActive == true
                                   && row.ProductID == ProductID
                                   && row.FileType == productype
                                   select row).ToList();


                return AllDocument;
            }
        }
        public void BindGrid()
        {
            grdBulkDocUpload.DataSource = null;
            grdBulkDocUpload.DataBind();
            if (rdoCaseNotice.SelectedItem.Text == "Case")
            {
                var AllDocument = getUploadedDocList(2, "C");
                grdBulkDocUpload.DataSource = AllDocument;
                grdBulkDocUpload.DataBind();
                Session["TotalRows"] = AllDocument.Count;
            }
            else if (rdoCaseNotice.SelectedItem.Text == "Notice")
            {
                var AllDocument = getUploadedDocList(2, "N");
                grdBulkDocUpload.DataSource = AllDocument;
                grdBulkDocUpload.DataBind();
                Session["TotalRows"] = AllDocument.Count;
            }
        }
        protected void grdBulkDocUpload_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataRowView drv = e.Row.DataItem as DataRowView;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var fileno = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "FileNo"));
                if (fileno == "")
                {
                    Label lbl = (Label)e.Row.FindControl("lblFileNo");
                    lbl.Visible = false;
                    DropDownList ddlFile = (DropDownList)e.Row.FindControl("ddlFileNo");
                    BindFileno(ddlFile);
                }
                else
                {
                    DropDownList ddlFile = (DropDownList)e.Row.FindControl("ddlFileNo");
                    ddlFile.Visible = false;
                }
            }

        }
        private void BindFileno(DropDownList ddlfiles)
        {
            try
            {
                ddlfiles.Items.Clear();
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    if (rdoCaseNotice.SelectedItem.Text == "Case")
                    {
                        var FileNoList = ((from tom in entities.tbl_LegalCaseInstance
                                           where tom.IsDeleted == false && tom.CustomerID == customerID &&
                                          tom.CaseRefNo != ""
                                           select new
                                           {
                                               ID = tom.ID,
                                               Fileno = tom.CaseRefNo,
                                           }).ToList());

                        ddlfiles.DataTextField = "Fileno";
                        ddlfiles.DataValueField = "ID";
                        ddlfiles.DataSource = FileNoList;
                        ddlfiles.DataBind();
                        ddlfiles.Items.Insert(0, new ListItem("Select Case No.", "-1"));

                    }
                    else
                   if (rdoCaseNotice.SelectedItem.Text == "Notice")
                    {

                        var FileNoList = ((from tom in entities.tbl_LegalNoticeInstance
                                           where tom.IsDeleted == false && tom.CustomerID == customerID
                                           select new
                                           {
                                               ID = tom.ID,
                                               Fileno = tom.RefNo,
                                           }).ToList());

                        ddlfiles.DataTextField = "Fileno";
                        ddlfiles.DataValueField = "ID";
                        ddlfiles.DataSource = FileNoList;
                        ddlfiles.DataBind();
                        ddlfiles.Items.Insert(0, new ListItem("Select Notice No.", "-1"));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvBulkDocUpload.IsValid = false;
                cvBulkDocUpload.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
        protected void lnkBtnBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void ShowGridDetail()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public static long CreateTempDocument(TempDocument tempComplianceDocument)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                entities.TempDocuments.Add(tempComplianceDocument);
                entities.SaveChanges();
                if (tempComplianceDocument.ID > 0)
                {
                    return tempComplianceDocument.ID;
                }
                else
                {
                    return 0;
                }
            }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            //Save in temp Table
            #region Upload Document            
            bool saveSuccess = false;        
            try
            {
                if (fuBulkDocUpload.HasFiles)
                {

                    TempDocument ObjTempDoc = null;
                    HttpFileCollection fileCollection = Request.Files;

                    if (fileCollection.Count > 0)
                    {
                        List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();
                        int customerID = -1;
                        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];


                            string fileName = Path.GetFileName(uploadfile.FileName);
                            string directoryPath = null;

                            if (!string.IsNullOrEmpty(fileName))
                            {
                                string[] keys = fileCollection.Keys[i].Split('$');
                                if (keys[keys.Count() - 1].Equals("fuBulkDocUpload"))
                                {
                                    directoryPath = Server.MapPath("~/CommonDocuments/" + customerID + "/" + 1 + ".0");
                                }
                                DocumentManagement.CreateDirectory(directoryPath);
                                string finalPath = Path.Combine(directoryPath, fileName);
                                finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                fileCollection[i].SaveAs(Server.MapPath(finalPath));
                                Stream fs = uploadfile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                ObjTempDoc = new TempDocument()
                                {
                                    CustomerId = customerID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    IsActive = true,
                                    ProductID = 2,
                                    FilePath = finalPath,
                                    FileKey = bytes,
                                    FileName = fileCollection[i].FileName,
                                };
                                int index = fileName.LastIndexOf('.');
                                var fileno = index == -1 ? fileName : fileName.Substring(0, index);

                                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                                {

                                    if (rdoCaseNotice.SelectedItem.Text == "Case")
                                    {
                                        var FileNoList = (from tom in entities.tbl_LegalCaseInstance
                                                          where tom.CaseRefNo == fileno
                                                          select new
                                                          {
                                                              ID = tom.ID,
                                                              Fileno = tom.CaseRefNo,
                                                          }).FirstOrDefault();

                                        if (FileNoList != null)
                                        {
                                            ObjTempDoc.FileNo = FileNoList.Fileno;
                                            ObjTempDoc.fileNoID = Convert.ToInt32(FileNoList.ID);
                                        }
                                        else
                                        {
                                            ObjTempDoc.FileNo = null;
                                            ObjTempDoc.fileNoID = null;
                                        }

                                    }
                                    else
                                    if (rdoCaseNotice.SelectedItem.Text == "Notice")
                                    {
                                        var FileNoList = (from tom in entities.tbl_LegalNoticeInstance
                                                          where tom.RefNo == fileno
                                                          select new
                                                          {
                                                              ID = tom.ID,
                                                              Fileno = tom.RefNo,
                                                          }).FirstOrDefault();

                                        if (FileNoList != null)
                                        {
                                            ObjTempDoc.FileNo = FileNoList.Fileno;
                                            ObjTempDoc.fileNoID = Convert.ToInt32(FileNoList.ID);
                                        }
                                        else
                                        {
                                            ObjTempDoc.FileNo = null;
                                            ObjTempDoc.fileNoID = null;
                                        }
                                    }
                                }
                                ObjTempDoc.Version = 1 + ".0";
                                ObjTempDoc.CreatedOn = DateTime.Now;
                                if (rdoCaseNotice.SelectedItem.Text == "Case")
                                {
                                    ObjTempDoc.FileType = "C";
                                }
                                else if (rdoCaseNotice.SelectedItem.Text == "Notice")
                                {
                                    ObjTempDoc.FileType = "N";
                                }
                                else
                                {
                                    ObjTempDoc.FileType = "C";
                                }
                                long _objTempDocumentID = CreateTempDocument(ObjTempDoc);
                            }//End For Loop 
                        }
                    }
                    else
                    {
                        cvBulkDocUpload.IsValid = false;
                        cvBulkDocUpload.ErrorMessage = "File not selected, Please select file. ";
                        VsBulkDocUpload.CssClass = "alert alert-danger";
                        if (lstErrorMsg.Count > 0)
                        {
                            showErrorMessages(lstErrorMsg, cvBulkDocUpload);
                        }
                    }
                }
                else
                {
                    cvBulkDocUpload.IsValid = false;
                    cvBulkDocUpload.ErrorMessage = "File not selected, Please select file. ";
                    VsBulkDocUpload.CssClass = "alert alert-danger";
                    if (lstErrorMsg.Count > 0)
                    {
                        showErrorMessages(lstErrorMsg, cvBulkDocUpload);
                    }
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            #endregion            
            BindGrid();
            bindPageNumber();
            ShowGridDetail();
        }
        public static int ExistsCaseNoticeDocumentType(String DocumentType, int CustomerID)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var objDocType = (from row in entities.tbl_Litigation_DocumentTypeMaster
                                  where row.TypeName.Equals(DocumentType)
                                  && row.CustomerID == CustomerID && row.IsDeleted == false
                                  select (int)row.ID).FirstOrDefault();

                return objDocType;
            }
        }
        protected void btnProcessDocUpload_Click(object sender, EventArgs e)
        {
            try
            {
                bool DocUploadSuccess = false;
                bool saveSuccess = false;
                List<string> lstErrorMsg = new List<string>();
                long cid = -1;
                int doctypeid = -1;
                cid = Convert.ToInt64(AuthenticationHelper.CustomerID);
                if (cid != -1)
                {
                    if (rdoCaseNotice.SelectedItem.Text == "Case")
                    {
                        doctypeid = ExistsCaseNoticeDocumentType("Case Document", (Int32)cid);
                        if (doctypeid == 0)
                        {
                            tbl_Litigation_DocumentTypeMaster _objContDoc = new tbl_Litigation_DocumentTypeMaster()
                            {
                                TypeName = "Case Document",
                                CustomerID = (int)cid,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                IsDeleted = false
                            };
                            CaseManagement.CreateDocumentType(_objContDoc);
                            doctypeid = (int)_objContDoc.ID;
                        }
                    }
                    else
                    {
                        doctypeid = ExistsCaseNoticeDocumentType("Notice Document", (Int32)cid);
                        if (doctypeid == 0)
                        {
                            tbl_Litigation_DocumentTypeMaster _objContDoc = new tbl_Litigation_DocumentTypeMaster()
                            {
                                TypeName = "Notice Document",
                                CustomerID = (int)cid,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                IsDeleted = false
                            };
                            CaseManagement.CreateDocumentType(_objContDoc);
                            doctypeid = (int)_objContDoc.ID;
                        }
                    }                                     
                    int chkflg = 0;
                    for (int i = 0; i < grdBulkDocUpload.Rows.Count; i++)
                    {
                        CheckBox chk = ((CheckBox)grdBulkDocUpload.Rows[i].Cells[0].FindControl("rechild"));
                        if (chk.Checked)
                        {
                            chkflg = 1;
                            #region Validation                            
                            Label fileno = (Label)grdBulkDocUpload.Rows[i].FindControl("lblFileNo");
                            string fileno1 = string.Empty;
                            if (fileno != null)
                            {
                                fileno1 = fileno.Text;
                            }
                            if (fileno1 == "" || fileno1 == null)
                            {
                                if (!string.IsNullOrEmpty(((DropDownList)grdBulkDocUpload.Rows[i].FindControl("ddlFileNo")).Text))
                                {
                                    try
                                    {
                                        DropDownList FileNo = (DropDownList)grdBulkDocUpload.Rows[i].FindControl("ddlFileNo");
                                        if (FileNo.SelectedValue == "-1")
                                        {
                                            lstErrorMsg.Add("Please Select Case/Notice No. At Row No  - " + (i + 1) + "  or Case/Notice No. should not be blank");
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        lstErrorMsg.Add("Please Select Case/Notice No. At Row No  - " + (i + 1) + "  or Case/Notice No. should not be blank");
                                    }
                                }
                                else
                                {
                                    lstErrorMsg.Add("Please Select Case/Notice No. At Row No  - " + (i + 1) + "  or Case/Notice No. should not be blank");
                                }
                            }


                            if (lstErrorMsg.Count > 0)
                            {
                                DocUploadSuccess = false;
                                showErrorMessages(lstErrorMsg, cvBulkDocUpload);
                            }
                            else
                                DocUploadSuccess = true;

                            #endregion
                        }
                    }
                    if (chkflg == 0)
                    {
                        lstErrorMsg.Add("Please select at least one CheckBox for Process");
                        if (lstErrorMsg.Count > 0)
                        {
                            DocUploadSuccess = false;
                            showErrorMessages(lstErrorMsg, cvBulkDocUpload);
                        }
                        else
                            DocUploadSuccess = true;
                    }

                    #region Save Multiple Documents upload

                    if (grdBulkDocUpload.Rows.Count > 0 && DocUploadSuccess == true)
                    {
                        foreach (GridViewRow row in grdBulkDocUpload.Rows)
                        {
                            CheckBox chk = ((CheckBox)row.FindControl("rechild"));
                            if (chk.Checked)
                            {
                                List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                                List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                                int RowID = Convert.ToInt32(((HiddenField)row.FindControl("HiddenID")).Value);
                                var AllDocumentData = LitigationManagement.getAllUploadedDocList();
                                #region Litigation

                                tbl_LitigationFileData objCaseNoticeDoc = new tbl_LitigationFileData()
                                {
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    IsDeleted = false,
                                    EnType = "A",
                                };

                                tbl_Litigation_FileDataTagsMapping objCaseNoticeTag = new tbl_Litigation_FileDataTagsMapping()
                                {
                                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                    UpdatedOn = DateTime.Now,
                                    IsActive = true
                                };

                                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                                {
                                    var FileNoList = (from tom in entities.TempDocuments
                                                      where tom.ID == RowID
                                                      select tom).FirstOrDefault();

                                    if (FileNoList != null)
                                    {
                                        objCaseNoticeDoc.DocType = FileNoList.FileType;
                                        objCaseNoticeDoc.FileName = FileNoList.FileName;
                                        objCaseNoticeDoc.VersionDate = DateTime.Now;
                                        objCaseNoticeDoc.CreatedOn = DateTime.Now;
                                        objCaseNoticeDoc.CreatedBy = AuthenticationHelper.UserID;
                                        objCaseNoticeDoc.VersionComment = "Bulk Document Uploaded";
                                    }
                                    // NoticeCaseInstanceID
                                    if (FileNoList.fileNoID != null)
                                    {
                                        objCaseNoticeDoc.NoticeCaseInstanceID = Convert.ToInt32(FileNoList.fileNoID);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty((((DropDownList)row.FindControl("ddlFileNo")).SelectedValue)))
                                        {
                                            objCaseNoticeDoc.NoticeCaseInstanceID = Convert.ToInt32(((DropDownList)row.FindControl("ddlFileNo")).SelectedValue);
                                        }
                                    }
                                    if (rdoCaseNotice.SelectedItem.Text == "Case")
                                    {
                                        objCaseNoticeDoc.DocTypeID = doctypeid;
                                    }
                                    else
                                    {
                                        objCaseNoticeDoc.DocTypeID = doctypeid;
                                    }

                                    //Get Document Version
                                    var caseDocVersion = CaseManagement.ExistsCaseDocumentReturnVersion(objCaseNoticeDoc);
                                    caseDocVersion++;
                                    objCaseNoticeDoc.Version = caseDocVersion + ".0";

                                    string directoryPath = "";
                                    int customerID = Convert.ToInt32(FileNoList.CustomerId);
                                    directoryPath = Server.MapPath("~/LitigationDocuments/" + customerID + "/Cases/" + Convert.ToInt32(FileNoList.fileNoID) + "/CaseDocument/" + objCaseNoticeDoc.Version);

                                    if (!Directory.Exists(directoryPath))
                                        Directory.CreateDirectory(directoryPath);

                                    Guid fileKey1 = Guid.NewGuid();
                                    string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(FileNoList.FileName));

                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath1, FileNoList.FileKey));
                                    long filesize = 0;
                                    try
                                    {
                                        filesize = Filelist.Select(A => A.Value).FirstOrDefault().Length;
                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }

                                    objCaseNoticeDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                    objCaseNoticeDoc.FileKey = fileKey1.ToString();
                                    objCaseNoticeDoc.FileSize = filesize;

                                    DocumentManagement.SaveDocFiles(Filelist);

                                    //Contract  tbl_LitigationFileData Save
                                    entities.tbl_LitigationFileData.Add(objCaseNoticeDoc);
                                    entities.SaveChanges();

                                    //Contract tag Save
                                    objCaseNoticeTag.FileID = objCaseNoticeDoc.ID;
                                    objCaseNoticeTag.FileTag = "Bulk";
                                    entities.tbl_Litigation_FileDataTagsMapping.Add(objCaseNoticeTag);
                                    entities.SaveChanges();

                                    //Disable Litigation Data from  Temp Table
                                    FileNoList.IsActive = false;
                                    entities.SaveChanges();

                                    Filelist.Clear();
                                    saveSuccess = true;
                                }

                                #endregion
                            }
                        }
                    }
                    #endregion

                    if (saveSuccess)
                    {
                        BindGrid();
                        bindPageNumber();
                        ShowGridDetail();
                        cvBulkDocUpload.IsValid = false;
                        cvBulkDocUpload.ErrorMessage = "Document Upload Successfully.";
                        VsBulkDocUpload.CssClass = "alert alert-success";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdBulkDocUpload.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
  
                BindGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdBulkDocUpload.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdBulkDocUpload.PageIndex = chkSelectedPage - 1;
            grdBulkDocUpload.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindGrid();
            ShowGridDetail();
        }
        public void showErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
            string finalErrMsg = string.Empty;
            finalErrMsg += "<ol type='1'>";
            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }
            cvtoShowErrorMsg.IsValid = false;
            cvtoShowErrorMsg.ErrorMessage = finalErrMsg;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);
        }

        public static long CreateContractDocumentMapping(Cont_tbl_FileData newContDoc)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Cont_tbl_FileData.Add(newContDoc);
                    entities.SaveChanges();
                    return newContDoc.ID;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
    }
}