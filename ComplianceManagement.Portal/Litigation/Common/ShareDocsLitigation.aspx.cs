﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataContract;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Common
{
    public partial class ShareDocsLitigation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                if (!IsPostBack)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                    {
                        ViewState["statusFileEditID"] = 0;
                        BindContractDocSharing();
                        divfrmdt.Visible = false;
                        divtodt.Visible = false;
                    }
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "showDocInfo", "BindControls();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void btnUpdateDocInfo_Click(object sender, EventArgs e)
        {
            try
            {
                bool validation = true;
                if (ddlPeriod.SelectedValue == "" || ddlPeriod.SelectedValue == "-1" || ddlPeriod.SelectedValue == "0")
                {
                    validation = false;
                    cvDocInfo.IsValid = false;
                    cvDocInfo.ErrorMessage = "Please Select Period.";
                    return;
                }
                if (string.IsNullOrEmpty(txtEmail.Text.Trim()))
                {
                    validation = false;
                    cvDocInfo.IsValid = false;
                    cvDocInfo.ErrorMessage = "Please Enter Email.";
                    return;
                }
                if (string.IsNullOrEmpty(txtcontactNum.Text))
                {
                    validation = false;
                    cvDocInfo.IsValid = false;
                    cvDocInfo.ErrorMessage = "Please Enter Contact Number";
                    return;
                }
                if (ddlPeriod.SelectedValue == "6")
                {
                    if (string.IsNullOrEmpty(txtfromdt.Text))
                    {
                        validation = false;
                        cvDocInfo.IsValid = false;
                        cvDocInfo.ErrorMessage = "Please Enter From Date";
                        return;
                    }
                    if (string.IsNullOrEmpty(txttodate.Text))
                    {
                        validation = false;
                        cvDocInfo.IsValid = false;
                        cvDocInfo.ErrorMessage = "Please Enter To Date";
                        return;
                    }
                    if (!string.IsNullOrEmpty(txtfromdt.Text) && !string.IsNullOrEmpty(txttodate.Text))
                    {
                        //string startdate = Convert.ToDateTime(txtfromdt.Text).ToString("MM-dd-yyyy");//dt.ToString("yyyy-MM-dd");
                        //string enddate = Convert.ToDateTime(txttodate.Text).ToString("MM-dd-yyyy");

                        DateTime d1 = Convert.ToDateTime(txtfromdt.Text);
                        DateTime d2 = Convert.ToDateTime(txttodate.Text);
                        if (d2 < d1)
                        {
                            validation = false;
                            cvDocInfo.IsValid = false;
                            cvDocInfo.ErrorMessage = "End Date should be greater than Start Date";
                            return;
                        }
                    }
                }

                if (validation)
                {
                    using (ContractMgmtEntities entities = new ContractMgmtEntities())
                    {
                        long VendorID = 0;

                        bool savesuccess = false;
                        long CustID = Convert.ToInt64(AuthenticationHelper.CustomerID);
                        var selectedFileids = Request.QueryString["AccessID"];
                        long CaseID = Convert.ToInt64(Request.QueryString["CaseInstanceID"]);
                        int Permission = 0;
                        List<int> fileIDs = selectedFileids.Split(',').Select(int.Parse).ToList();
                        DateTime StartDt = DateTime.Now;
                        DateTime EndDt = DateTime.Now;


                        /////Added by Vishal
                        bool saveSuccessNew = false;
                        Litigation_AssignedDocMaster objVendor = new Litigation_AssignedDocMaster()
                        {
                            CustomerID = Convert.ToInt32(CustID),
                            //Type = Convert.ToInt32(rbPartyType.SelectedValue),
                            VendorName = txtEmail.Text,
                            //ContactPerson = tbxContactPerson.Text,
                            Email = txtEmail.Text,
                            //ContactNumber = tbxContactNo.Text,
                            //Address = tbxAddress.Text,
                            //VAT = tbxVAT.Text,
                            //TIN = tbxTIN.Text,
                            //GSTIN = tbxGSTIN.Text,
                            //PAN = tbxPAN.Text,
                            IsDeleted = false,
                            CreatedOn = DateTime.Now,
                            CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                            UpdatedOn = DateTime.Now,
                            UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID),
                        };


                        Litigation_AssignedDocMaster newAssignedUserMaster = null;//CheckEmail(objVendor);
                        if (newAssignedUserMaster == null)
                        {
                            saveSuccessNew = CreateAssignedUserData(objVendor);
                            VendorID = objVendor.ID;
                        }
                        else
                            VendorID = newAssignedUserMaster.ID;
                        ////

                        foreach (var item in fileIDs)
                        {
                            long fileID = Convert.ToInt64(item);

                            //long VendorID = Convert.ToInt64(ddlVendor.SelectedValue);
                            long Period = Convert.ToInt64(ddlPeriod.SelectedValue);
                            if (ddlPermission1.SelectedValue != "" && ddlPermission1.SelectedValue != "-1" && ddlPermission1.SelectedValue != "0")
                            {
                                Permission = Convert.ToInt32(ddlPermission1.SelectedValue);
                            }

                            Litigation_Assigned_DocShare _objFileRecord = new Litigation_Assigned_DocShare()
                            {
                                FileID = fileID,
                                NoticeCaseInstanceID = CaseID,
                                AssignedDocUID = VendorID,
                                Period = Period,
                                Permission = Permission,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now,
                                IsDeleted = false,
                                CustomerID = CustID
                            };

                            DateTime SD = DateTime.Now;
                            _objFileRecord.StartDate = SD;
                            if (Period == 1)
                            {
                                DateTime ED = SD.AddDays(30);
                                _objFileRecord.EndDate = ED;
                                EndDt = ED;
                            }
                            if (Period == 2)
                            {
                                DateTime ED = SD.AddDays(90);
                                _objFileRecord.EndDate = ED;
                                EndDt = ED;
                            }
                            if (Period == 3)
                            {
                                DateTime ED = SD.AddDays(180);
                                _objFileRecord.EndDate = ED;
                                EndDt = ED;
                            }
                            if (Period == 4)
                            {
                                DateTime ED = SD.AddDays(365);
                                _objFileRecord.EndDate = ED;
                                EndDt = ED;
                            }
                            if (Period == 6)
                            {
                                _objFileRecord.StartDate = Convert.ToDateTime(txtfromdt.Text);
                                _objFileRecord.EndDate = Convert.ToDateTime(txttodate.Text);
                                EndDt = Convert.ToDateTime(txttodate.Text);
                            }

                            if (DateTime.Now.Date <= EndDt.Date)
                                _objFileRecord.docexpired = false;
                            else
                                _objFileRecord.docexpired = true;

                            bool saveSuccess = AssignedUserDocSharingMapping(_objFileRecord);
                            if (saveSuccess)
                            {
                                savesuccess = true;
                                ContractManagement.CreateAuditLog("C", VendorID, "Litigation_Assigned_DocShare", "Create", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract document sharing Updated", true, Convert.ToInt32(VendorID));

                                Litigation_AssignedDocMaster data = (from row in entities.Litigation_AssignedDocMaster
                                                                     where row.ID == VendorID
                                                                   && row.CustomerID == CustID
                                                                     select row).FirstOrDefault();
                                if (data != null)
                                {
                                    data.Email = txtEmail.Text;
                                    data.ContactNumber = txtcontactNum.Text;
                                    entities.SaveChanges();
                                }
                            }
                        }
                        #region Mail send to Vendor for share document                          
                        if (savesuccess)
                        {
                            string vendorname = txtEmail.Text;
                            //var obj = ContractManagement.getContractdetail(Convert.ToInt32(CustID), Convert.ToInt32(VendorID));
                            //if (Request.QueryString["CaseTitle"] != null)
                            //{
                            //if (string.IsNullOrEmpty(Request.QueryString["CaseTitle"]))
                            //    Request.QueryString["CaseTitle"] = "";
                            string Title = Request.QueryString["CaseTitle"] == null ? Request.QueryString["UniqueNumber"] : Request.QueryString["CaseTitle"];
                            string contnum = Request.QueryString["contnum"];
                            string portalURL = string.Empty;
                            URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                            if (Urloutput != null)
                            {
                                portalURL = Urloutput.URL;
                            }
                            else
                            {
                                portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                            }

                            string AccessURL = Convert.ToString(portalURL) +
                                                        "/ContractDocVerifyOtp.aspx?" +
                                                        "UID=" + VendorID.ToString() +
                                                          "&CustID=" + CustID +
                                                            "&CaseID=" + CaseID.ToString() + "&PageName=" + Request.QueryString["Page"];

                            string header = "Dear User" + "<br><br>";

                            string data = string.Empty;

                            if (ddlPeriod.SelectedValue == "5")
                            {
                                data = "Document has been shared with you for a specific period  " + StartDt.ToString("dd-MM-yyyy") + " to " + "Permanent" + "<br><br>";
                            }
                            else
                                data = "Document has been shared with you for a specific period  " + StartDt.ToString("dd-MM-yyyy") + " to " + EndDt.ToString("dd-MM-yyyy") + "<br><br>";

                            string ContractName = "Title Name - " + Title + "<br>" + "<br>";

                            string uniqueNo = " Number - " + Request.QueryString["UniqueNumber"] + "<br>" + "<br>";

                            //string ContractNo = "Contact no. - " + contnum + "<br>" + "<br>";

                            string FinalMail = string.Empty;

                            string UrlAccess = "Url to access document - " + AccessURL;

                            if (Request.QueryString["MailHeader"] == "For Task")
                            {
                                FinalMail = header + data + ContractName + UrlAccess;
                            }
                            else
                                FinalMail = header + data + ContractName + uniqueNo + UrlAccess;

                            string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();

                            string vendorEmail = txtEmail.Text;

                            string PageHeader = string.Empty;

                            if (!string.IsNullOrEmpty(Request.QueryString["MailHeader"]))
                            {
                                PageHeader = Request.QueryString["MailHeader"];
                            }
                            else
                            {
                                PageHeader = "";
                            }

                            try
                            {
                                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { vendorEmail }), null, null, "Shared " + Request.QueryString["Page"] + " " + PageHeader + " Document", FinalMail);
                                cvDocInfo.IsValid = false;
                                cvDocInfo.ErrorMessage = "Document shared successfully.";
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }

                        }
                        #endregion
                        txtEmail.Text = "";
                        txtcontactNum.Text = "";
                        BindContractDocSharing();

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDocInfo.IsValid = false;
                cvDocInfo.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        

        private void BindContractDocSharing()
        {
            try
            {
                using (ContractMgmtEntities entities = new ContractMgmtEntities())
                {
                    //List<Cont_SP_GetSharingDocDetail_Result> lstContDocs = new List<Cont_SP_GetSharingDocDetail_Result>();
                    List<Litigation_SP_GetSharingDocDetail_Result> lstContDocs = new List<Litigation_SP_GetSharingDocDetail_Result>();
                    //var selectedFileids = Request.QueryString["AccessID"];
                    List<int> fileIDs = Request.QueryString["AccessID"].Split(',').Select(int.Parse).ToList();
                    //long fileID = Convert.ToInt64(Request.QueryString["AccessID"]);
                    long CaseID = Convert.ToInt64(Request.QueryString["CaseInstanceID"]);

                    string PageName = Request.QueryString["Page"];

                    long CustID = Convert.ToInt64(AuthenticationHelper.CustomerID);

                    lstContDocs = LitigationTaskManagement.GetDocSharing_All(CustID, PageName);
                    lstContDocs = lstContDocs.Where(x => fileIDs.Contains((int)x.FileID)).ToList();
                    //lstContDocs = lstContDocs.Where(x => x.ContractID == ContID).ToList();

                    if (!string.IsNullOrEmpty(Request.QueryString["EditFileID"]))
                    {
                        int ID = Convert.ToInt32(Request.QueryString["EditFileID"]);
                        lstContDocs = lstContDocs.Where(x => x.ID == ID).ToList();
                    }

                    grdContractDocuments.DataSource = lstContDocs;
                    grdContractDocuments.DataBind();
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            divfrmdt.Visible = false;
            divtodt.Visible = false;

            if (Convert.ToInt32(ddlPeriod.SelectedValue) == 6)
            {
                divfrmdt.Visible = true;
                divtodt.Visible = true;
            }
        }
        protected void grdContractDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdContractDocuments.PageIndex = e.NewPageIndex;
                //BindContractDocSharing();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdContractDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    GridView senderGridView = sender as GridView;

                    if (senderGridView != null)
                    {
                        if (e.CommandName.Equals("DeleteContDoc"))
                        {
                            int RecordId = Convert.ToInt32(e.CommandArgument);
                            int FileID = -1;
                            //int FileID = Convert.ToInt32(Request.QueryString["AccessID"]);
                            int CaseID = Convert.ToInt32(Request.QueryString["CaseInstanceID"]);

                            int customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                            int userID = -1;
                            userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                            bool saveSuccess = DocUnSharingMapping(RecordId, customerID, CaseID, FileID, userID);
                            if (saveSuccess)
                            {
                                cvDocInfo.IsValid = false;
                                cvDocInfo.ErrorMessage = "Document unshared successfully.";
                                BindContractDocSharing();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static bool CreateAssignedUserData(Litigation_AssignedDocMaster objVendor)
        {
            try
            {
                using (ContractMgmtEntities entities = new ContractMgmtEntities())
                {
                    entities.Litigation_AssignedDocMaster.Add(objVendor);
                    entities.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        public static Litigation_AssignedDocMaster CheckEmail(Litigation_AssignedDocMaster objVendor)
        {
            Litigation_AssignedDocMaster data = new Litigation_AssignedDocMaster();
            try
            {
                using (ContractMgmtEntities entities = new ContractMgmtEntities())
                {
                    //entities.Cont_tbl_VendorMaster.Add(objVendor);
                    //entities.SaveChanges();
                    data = (from row in entities.Litigation_AssignedDocMaster
                            where row.Email == objVendor.Email
                            select row).FirstOrDefault();

                }

                return data;
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public static bool AssignedUserDocSharingMapping(Litigation_Assigned_DocShare record)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                bool saveSuccess = false;
                try
                {
                    Litigation_Assigned_DocShare vendorMappingExists = (from row in entities.Litigation_Assigned_DocShare
                                                                        where row.AssignedDocUID == record.AssignedDocUID
                                                               //&& row.VendorID == record.VendorID
                                                               && row.FileID == record.FileID
                                                               select row).FirstOrDefault();

                    if (vendorMappingExists != null)
                    {
                        vendorMappingExists.IsDeleted = record.IsDeleted;
                        vendorMappingExists.UpdatedBy = record.CreatedBy;
                        vendorMappingExists.UpdatedOn = DateTime.Now;
                        saveSuccess = true;
                    }
                    else
                    {
                        entities.Litigation_Assigned_DocShare.Add(record);
                        saveSuccess = true;
                    }

                    entities.SaveChanges();

                    return saveSuccess;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        public static bool DocUnSharingMapping(int RecordId, int CustID, int CaseID, int FileID, int UID)
        {
            using (ContractMgmtEntities entities = new ContractMgmtEntities())
            {
                bool saveSuccess = false;
                try
                {
                    Litigation_Assigned_DocShare DocMappingExists = (from row in entities.Litigation_Assigned_DocShare
                                                                        where row.NoticeCaseInstanceID == CaseID
                                                               && row.CustomerID == CustID
                                                               //&& row.FileID == FileID
                                                               && row.ID == RecordId
                                                               select row).FirstOrDefault();

                    if (DocMappingExists != null)
                    {
                        DocMappingExists.IsDeleted = true;
                        DocMappingExists.UpdatedBy = UID;
                        DocMappingExists.UpdatedOn = DateTime.Now;
                        saveSuccess = true;
                    }

                    entities.SaveChanges();

                    return saveSuccess;
                }
                catch (Exception ex)
                {
                    ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    return false;
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterOnSubmitStatement(typeof(Page), "closePage", "window.onunload = CloseWindow();");
        }
    }

    public class LitigationShareAssignedModel
    {
        public int ID { get; set; }

        public Nullable<long> NoticeCaseInstanceID { get; set; }
        public int AssignedDocUID { get; set; }
        public string AssignedName { get; set; }
        public string FileName { get; set; }

        public int FileID { get; set; }

        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<bool> docexpired { get; set; }
        public Nullable<int> Permission { get; set; }


    }
}