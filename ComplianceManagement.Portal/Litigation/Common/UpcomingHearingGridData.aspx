﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpcomingHearingGridData.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Common.UpcomingHearingGridData" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <!-- font icon -->
    <link href="../../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <script type="text/javascript">
        function ShowNoticeCaseDialog(noticeCaseInstanceID, type) {
            parent.ShowUpcomingHearingPop(noticeCaseInstanceID, type);
        };
        function CloseRefresh() {
            window.parent.hideloader();
        }

        $(document).ready(function () {
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        });
    </script>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>

            <div class="col-md-12">
                <asp:GridView runat="server" ID="grdUpcomingHearing" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    GridLines="None" PageSize="5" AutoPostBack="true" AllowPaging="true" CssClass="table" Width="100%" OnPageIndexChanging="grdUpcomingHearing_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <div>
                                    <asp:Label ID="lblCaseRefNo" runat="server" Text='<%# Eval("CaseRefNo")%>'></asp:Label>
                                    <asp:Label ID="lbllstAssignedTo" runat="server" Text='<%# Eval("lstAssignedTo")%>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Hearing Date" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label ID="lblReminderDate" runat="server" Text='<%# Eval("ReminderDate") != null ? Convert.ToDateTime(Eval("ReminderDate")).ToString("dd-MM-yyyy") : "" %>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ReminderDate") != null ? Convert.ToDateTime(Eval("ReminderDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Case" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                    <asp:Label ID="lblCaseTitle" Width="100%" runat="server" Text='<%# Eval("Title")%>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Title")%>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Court" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label ID="lblCourt" runat="server" Text='<%# Eval("CourtName")%>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CourtName")%>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Opponent" Visible="false" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                    <asp:Label ID="lblParty" runat="server" Text='<%# Eval("PartyName")%>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("PartyName")%>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Entity/Branch/Location" Visible="false" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                    <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("BranchName")%>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("BranchName")%>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkshowCaseDetails" runat="server" ToolTip="View" data-placement="left" CommandArgument='<%# Eval("NoticeCaseInstanceID")%>' data-toggle="tooltip" OnClick="lnkshowCaseDetails_Click">                                   
                                    <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>'/></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
            <div class="col-md-12 colpadding0">
                <div class="col-md-6 colpadding0">
                </div>
                <div class="col-md-6 colpadding0">                    
                     <asp:LinkButton ID="lnkDownloadHearing" runat="server" Style="margin-left: 76%; margin-bottom: 15px;" ToolTip="Download Upcoming Hearing" data-toggle="tooltip" data-placement="bottom" OnClick="lnkDownloadHearing_Click">                                   
                                    <img src='../../Images/download_icon_new.png' alt="Download Upcoming Hearing"/></asp:LinkButton>
                    <div class="table-paging" style="margin-bottom: 20px; width: 100px;">
                        <asp:ImageButton ID="lBPrevious" Style="width: 28px;" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />
                        <div class="table-paging-text" style="width: 40px; margin-top: -2px;">
                            <p>
                                <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                 <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                        <asp:ImageButton ID="lBNext" Style="width: 28px;" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="text-align: right; display: none;">
                <asp:LinkButton runat="server" ID="lnkShowDetailCase" Text="..Show More"
                    PostBackUrl="~/Litigation/aspxPages/CaseList.aspx"></asp:LinkButton>
            </div>
            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
        </div>
    </form>
    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <button type="button" class="close" onclick="javascript:reloadTaskList();" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="75%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
