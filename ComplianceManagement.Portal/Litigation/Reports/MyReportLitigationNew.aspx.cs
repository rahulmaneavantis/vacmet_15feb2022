﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Reports
{
    public partial class MyReportLitigationNew : System.Web.UI.Page
    {
        protected static string Path;
        protected static string FlagIsApp;
        protected static int CustId;
        protected static int UId;
        public static string LititigationType;
        protected static string Authorization;
        public string IsNoticeDate;
        public string customerID;
        protected static int GetReportCustId;
        public bool NewColumnsLitigation;
        protected bool FYYear;

        protected void Page_Load(object sender, EventArgs e)
        {
            NewColumnsLitigation = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "LitigationNewColumn");
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            IsNoticeDate = Convert.ToString(ConfigurationManager.AppSettings["IsNoticeDateCustID"]);
            customerID = Convert.ToString(AuthenticationHelper.CustomerID);
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UId = Convert.ToInt32(AuthenticationHelper.UserID);
            FlagIsApp = AuthenticationHelper.Role;
            LititigationType = "4";
            GetReportCustId = GetClientName(CustId);  
            Page.Header.DataBind();
            FYYear = CheckForClient(Convert.ToInt32(customerID), "CaseNoticeLabel");
            if (!IsPostBack)
            {
                if (Request.QueryString["type"] != "" && Request.QueryString["type"] != null)
                {
                    LititigationType = Request.QueryString["type"];
                }

            }   
    }
        public static int GetClientName(int CustomerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClientCustomizations
                            where row.CustomizationName == "ReportCustID"
                            && row.ClientID==CustomerID
                            select row.ClientID).FirstOrDefault();
                return data;
            }
        }
        public static bool CheckForClient(int CustomerID, string Param)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var data = (from row in entities.ClientCustomizations
                            where row.CustomizationName.ToLower() == Param.ToLower()
                            && row.ClientID == CustomerID
                            select row).Count();

                if (data != 0)
                {
                    return true;
                }
                else
                    return false;
            }
        }
    }
}