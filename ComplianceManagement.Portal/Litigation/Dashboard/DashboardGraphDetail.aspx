﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DashboardGraphDetail.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Dashboard.DashboardGraphDetail" %>

<!DOCTYPE html>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="DropDownListChosen" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>

    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="../../NewCSS/chosen.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/chosen.min.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        @media screen and (max-width: 750px) {
            iframe {
                max-width: 100% !important;
                width: auto !important;
                height: auto !important;
            }
        }

        .chosen-container .chosen-results {
            max-height: 150px !important;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

          .modal-header {
            padding: 15px;
            border-bottom: 0px solid #e5e5e5;
            min-height: 16.43px;
        }
    </style>

    <script type="text/javascript">
        $("html").mouseover(function () {
            $("html").getNiceScroll().resize();
        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }


        $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

        function resizeIframe() {
          
            var objElement = window.parent.document.getElementById("showChartDetails");
            if (objElement != null) {
                objElement.style.height = objElement.contentWindow.document.body.scrollHeight + 'px';
            }
        }

        $(document).ready(function () {
            //resizeIframe();
        });

        function resizeParentIframe() {
           

            window.parent.document.getElementById("showChartDetails").style.height = '600px';

            //if (x != undefined) {
            //    $(x).css('width', '90%');
            //    $(x).css('height', '1500px');               
            //}
            //$('#showChartDetails', window.parent.document).width('1500px');
            //$('#showChartDetails', window.parent.document).height('1500px');
        }

        function ShowDialog(noticeCaseInstanceID, type) {
            if (type != '') {
                $('#divShowDialog').modal('show');
                $('#showdetails').attr('width', '100%');
                $('#showdetails').attr('height', '650px');
                $('.modal-dialog').css('width', '95%');
                $('.modal-dialog').css('height', '500px');

                if (type == 'C') {
                    //    $('#showdetails').attr('src', "../Common/ShowCaseDetail.aspx?AccessID=" + noticeCaseInstanceID);
                    $('#showdetails').attr('src', "../aspxPages/CaseDetailPage.aspx?AccessID=" + noticeCaseInstanceID);
                }
                else if (type == 'N') {
                    //    $('#showdetails').attr('src', "../Common/ShowNoticeDetail.aspx?AccessID=" + noticeCaseInstanceID);
                    $('#showdetails').attr('src', "../aspxPages/NoticeDetailPage.aspx?AccessID=" + noticeCaseInstanceID);
                }

            }
        };

        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });
    </script>

</head>
<body onload="resizeIframe()">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <%--<asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional">--%>
            <%--<ContentTemplate>--%>

            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.2;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="col-md-12">
                <asp:ValidationSummary ID="vsGraphDetailPage" runat="server" class="alert alert-block alert-danger fade in"
                    ValidationGroup="GraphDetailPageValidationGroup" />
                <asp:CustomValidator ID="cvGraphDetail" runat="server" EnableClientScript="False"
                    ValidationGroup="GraphDetailPageValidationGroup" Display="None" />
            </div>

            <div class="col-md-12">
                <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                    <ContentTemplate>
                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                            <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">Entity/Branch/Location</label>
                            <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 32px; width: 95%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                CssClass="s" />
                            <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px;" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" Width="100%" NodeStyle-ForeColor="#8e8e93"
                                    Style="overflow: auto; border-left: 1px solid #c7c7cc; margin-top: -20px; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true" OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                </asp:TreeView>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                    <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">Notice/Case</label>
                    <asp:DropDownListChosen runat="server" ID="ddlTypePage" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select Type" class="form-control" Width="95%">
                        <asp:ListItem Text="Select Notice/Case" Value="-1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Both" Value="B"></asp:ListItem>
                        <asp:ListItem Text="Notice" Value="N"></asp:ListItem>
                        <asp:ListItem Text="Case" Value="C"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                    <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">Notice/Case Type</label>
                    <asp:DropDownListChosen runat="server" ID="ddlNoticeTypePage" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select Status" class="form-control" Width="95%">
                        <asp:ListItem Text="Select Type" Value="-1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Both" Value="B"></asp:ListItem>
                        <asp:ListItem Text="Inward/Defendant" Value="I"></asp:ListItem>
                        <asp:ListItem Text="Outward/Plaintiff" Value="O"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                    <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">Department</label>
                    <asp:DropDownListChosen runat="server" ID="ddlDeptPage" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select Department" class="form-control" Width="95%" />
                </div>
            </div>

            <div class="col-md-12">
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                    <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">Opponent</label>
                    <asp:DropDownListChosen runat="server" ID="ddlPartyPage" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select Opponent" class="form-control" Width="95%" />
                </div>
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                    <label style="display: block; float: left; font-size: 13px; font-weight: 400; color: #333;">Status</label>
                    <asp:DropDownListChosen runat="server" ID="ddlStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select Status" class="form-control" Width="95%">
                        <asp:ListItem Text="Select Status" Value="-1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="All" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Open" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Closed" Value="3"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>
                <div class="col-md-3" style="margin-top: 5px;">
                    <div class="col-md-2 colpadding0" style="width: 20%; display: none">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" Visible="false">
                        <asp:ListItem Text="8" Selected="True" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; text-align: right;">
                    <div style="float: left; margin-right: 15px; margin-top: 15px">
                        <asp:LinkButton ID="btnApplyTo" class="btn btn-primary" runat="server" Text="<i class='fa fa-check'></i> Apply" OnClick="btnApplyTo_Click"></asp:LinkButton>
                    </div>
                    <div style="float: left; margin-top: 15px">
                        <asp:LinkButton ID="btnExportExcel" class="btn btn-primary" runat="server" Text="<i class='fa fa-file-excel-o'></i> Export to Excel" OnClick="btnExportExcel_Click"></asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"></div>

            <div class="col-md-12">
                <asp:GridView runat="server" ID="grdNoticeCaseDetails" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    PageSize="8" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" OnSorting="grdNoticeCaseDetails_Sorting" OnRowCreated="grdNoticeCaseDetails_RowCreated">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Notice/Case" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("TypeName") %>' ToolTip='<%# Eval("TypeName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Type" ItemStyle-Width="8%" SortExpression="NoticeTypeName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("NoticeTypeName") %>' ToolTip='<%# Eval("NoticeTypeName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Notice/Case No." ItemStyle-Width="10%" SortExpression="RefNo">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("RefNo") %>' ToolTip='<%# Eval("RefNo") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Title" ItemStyle-Width="10%" SortExpression="NoticeTitle">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("NoticeTitle") %>' ToolTip='<%# Eval("NoticeTitle") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="10%" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("NoticeDetailDesc") %>' ToolTip='<%# Eval("NoticeDetailDesc") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Opponent" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("PartyName") %>' ToolTip='<%# Eval("PartyName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Entity" ItemStyle-Width="10%" SortExpression="BranchName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Department" ItemStyle-Width="10%" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DeptName") %>' ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Category" ItemStyle-Width="5%" SortExpression="NoticeCategory">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("NoticeCategory") %>' ToolTip='<%# Eval("NoticeCategory") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Expense" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Right" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" CssClass="text-right"
                                        Text='<%# Eval("TotalPayment") != DBNull.Value ? Convert.ToDecimal(Eval("TotalPayment")).ToString() : "" %>'
                                        ToolTipText='<%# Eval("TotalPayment") != DBNull.Value ? Convert.ToDecimal(Eval("TotalPayment")).ToString() : "" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Stage" ItemStyle-Width="5%" SortExpression="CaseStage">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CaseStage") %>' ToolTip='<%# Eval("CaseStage") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Status" ItemStyle-Width="5%" SortExpression="Status">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgBtnShowDetail" runat="server" ImageUrl="~/Images/View-icon-new.png" OnClick="lnkShowDetail_Click"
                                    CommandArgument='<%# Eval("NoticeInstanceID")+ ","+ Eval("Type") %>' data-toggle="tooltip" ToolTip="View" data-placement="left"></asp:ImageButton>

                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="col-md-12">
                <div class="col-md-10 colpadding0">
                    <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                        <p style="padding-right: 0px !Important;">
                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="col-md-2" style="text-align: right;">
                    <div style="float: left; width: 55%">
                        <p class="clsPageNo">Page</p>
                    </div>
                    <div style="float: left; width: 45%">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="70%" Height="30px">
                        </asp:DropDownListChosen>
                    </div>
                </div>
                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
            </div>
        </div>

        <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f7f7f7; height: 42px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeCaseModal();">&times;</button>
                    </div>

                    <div class="modal-body" style="background-color: #f7f7f7; height: 620px;">
                        <iframe id="showdetails" src="about:blank" style="width: 95%; height: 90% !important" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </form>

</body>
</html>
