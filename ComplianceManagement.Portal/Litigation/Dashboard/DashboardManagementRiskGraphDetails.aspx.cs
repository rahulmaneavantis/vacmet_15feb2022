﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Dashboard
{
    public partial class DashboardManagementRiskGraphDetails : System.Web.UI.Page
    {
        protected static string Path;
        protected static string Custid;
        protected static int UserID;
        protected static string Role;
        protected static string type;
        protected string Internalsatutory;
        protected int RiskTypeID;
        protected int NoticeCaseStatusID;
        protected int statVal;
        protected string StatusFlag;
        protected static string Authorization;
        protected static string NoticeCase;
        protected static string Enddate;
        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }
            Path = ConfigurationManager.AppSettings["KendoPathApp"];
            Custid = Convert.ToString(AuthenticationHelper.CustomerID);
            UserID = AuthenticationHelper.UserID;
            Role = AuthenticationHelper.Role;
            RiskTypeID = -1;
            StatusFlag = Convert.ToString(Request.QueryString["Status"]);
            RiskTypeID = Convert.ToInt32(Request.QueryString["CID"]);
            NoticeCaseStatusID = Convert.ToInt32(Request.QueryString["Status"]);
            NoticeCase = Convert.ToString(Request.QueryString["NOC"]);
            if(NoticeCase == "N")
            {
                NoticeCase = "Notice";
            }
            else if(NoticeCase == "C")
            {
                NoticeCase = "Case";
            }
            else if(NoticeCase == "B")
            {
                NoticeCase = "Both";
            }

        }
    }
}