﻿<%@ Page Title="MY DASHBOARD" Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="LitigationDashboard.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Dashboard.LitigationDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- Offline-->
    <script type="text/javascript" src="../../avantischarts/jQuery-v1.12.1/jQuery-v1.12.1.js"></script>
    <script type="text/javascript" src="../../avantischarts/highcharts/js/highcharts.js"></script>
    <script type="text/javascript" src="../../avantischarts/highcharts/js/modules/drilldown.js"></script>
    <%--<script type="text/javascript" src="../../avantischarts/highcharts/js/modules/exporting.js"></script>--%>
    <script type="text/javascript" src="../../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../newjs/spectrum.js"></script>

    <link href="../../avantischarts/jquery-ui-v1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../../newcss/spectrum.css" rel="stylesheet" />
    <link href='<%# ResolveUrl("../../NewCSS/responsive-calendar.css")%>' rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/responsive-calendar.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            setactivemenu('leftdashboardmenu');
            fhead('My Dashboard');
        });

    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            imgExpandCollapse();
        });

        function gridPageIndexChanged() {
            imgExpandCollapse();
        }

        function imgExpandCollapse() {
            $("[src*=collapse]").on('click', function () {

                $(this).attr("src", "/Images/add.png");
                $(this).closest("tr").next().remove();
            });

            $("[src*=add]").on('click', function () {

                if ($(this).attr('src').indexOf('add.png') > -1) {
                    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
                    $(this).attr("src", "/Images/collapse.png");
                } else if ($(this).attr('src').indexOf('collapse.png') > -1) {
                    $(this).attr("src", "/Images/add.png");
                    $(this).closest("tr").next().remove();
                }
            });
        }

        function ChangeRowColor(rowID) {

            var color = document.getElementById(rowID).style.backgroundColor;
            var oldColor = document.getElementById(rowID).style.backgroundColor;

            //alert(color);

            if (color != 'rgb(247, 247, 247)')
                document.getElementById("hiddenColor").style.backgroundColor = color;

            //alert(oldColor);

            if (color == 'rgb(247, 247, 247)')
                document.getElementById(rowID).style.backgroundColor = document.getElementById("hiddenColor").style.backgroundColor;
            else
                document.getElementById(rowID).style.backgroundColor = 'rgb(247, 247, 247)';
        }
        function ftotalliability() {
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '810px');
            $('.modal-dialog').css('width', '88%');
            $('#showdetails').attr('src', "../aspxPages/Valueashboard.aspx");
        }

        function ShowUpcomingHearingPop(noticeCaseInstanceID, type) {
            
            if (type != '') {
                $('#divShowDialog').modal('show');
                $('#showdetails').attr('width', '100%');
                $('#showdetails').attr('height', '500px');
                $('.modal-dialog').css('width', '95%');
                //$('.modal-dialog').css('height', '400px');

                if (type == 'C') {
                    $('#showdetails').attr('src', "../aspxPages/CaseDetailPage.aspx?AccessID=" + noticeCaseInstanceID + "&Flagtab=H");
                }
                else if (type == 'N') {
                    $('#showdetails').attr('src', "../aspxPages/NoticeDetailPage.aspx?AccessID=" + noticeCaseInstanceID);
                }
            }
        };
    </script>
    <%--//Calendar Start//--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $.get("/Litigation/Common/UpcomingHearingCalendar.aspx?m=3&type=1", function (data) {
                $(".responsive-calendar").html(data);
            });

            setInterval(setcolor, 1000);
        });

        function setcolor() {
            $('.Upcoming').closest('div').find('a').css('background-color', '#1d86c8');
            $('.Pending').closest('div').find('a').css('background-color', '#FFC200');
        }
        $(document).ready(function (ClassName) {
            $("a").addClass(ClassName);
            //fcal(new Date().toLocaleDateString());
            fcal(null);
        });

        function BindUpcomingGridAllData()
        {
            fcal(null);
        }

        function formatDate(date) {
            var monthNames = [
              "January", "February", "March",
              "April", "May", "June", "July",
              "August", "September", "October",
              "November", "December"
            ];

            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();

            return day + ' ' + monthNames[monthIndex] + ' ' + year;
        }
        
        function fcal(dt) {
            try {
                if (dt != null) {
                    var ddata = new Date(dt);
                    $('#clsdatel').html('Hearing for date ' + formatDate(ddata));
                }
                else {
                    $('#clsdatel').html('Hearing of all dates');
                }
            } catch (e) { }
            //$('#imgcaldate').show();
           // $('#calframe').attr('src', '/Litigation/Common/UpcomingHearingGridData.aspx?m=3&date=' + dt + '&type=' + 1)
            $('#calframe').attr('src', '/Litigation/Common/calendardataAPINew.aspx?m=3&date=' + dt + '&type=' + 1);

            return;
        }

        function hideloader() {
            $('#imgcaldate').hide();
        }
    </script>
    <style type="text/css">
        .clscircle {
            margin-right: 7px !important;
        }

        .fixwidth {
            width: 20% !important;
        }

        .badge {
            font-size: 10px !important;
            font-weight: 200 !important;
        }

        .responsive-calendar .day {
            width: 13.7% !important;
            height: 50px;
        }

            .responsive-calendar .day.cal-header {
                border-bottom: none !important;
                width: 13.9% !important;
                font-size: 17px;
                height: 25px;
            }

        #collapsePerformerLoc > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        #collapsePreviewerLoc > div > div > div > div > div > div > a.bx-prev {
            left: 0%;
        }

        .bx-viewport {
            height: 285px !important;
        }

        #dailyupdates .bx-viewport {
            height: 190px !important;
        }

        .graphcmp {
            margin-left: 36%;
            font-size: 16px;
            margin-top: -3%;
            color: #666666;
            font-family: 'Roboto';
        }

        .days > div.day {
            margin: 1px;
            background: #eee;
        }

        .overdue ~ div > a {
            background: red;
        }

        .info-box {
            min-height: 105px !important;
        }

        .dashboardProgressbar {
            display: none;
        }

        #reviewersummary {
            height: 150px;
        }

        #performersummary {
            height: 150px;
        }

        #eventownersummary {
            height: 150px;
        }

        #performersummarytask {
            height: 150px;
        }

        #reviewersummarytask {
            height: 150px;
        }

        .info-box {
            margin-bottom: 0px !important;
            border: 1px solid #ADD8E6;
        }

        div.panel {
            margin-bottom: 12px;
        }

        .panel .panel-heading .panel-actions {
            height: 25px !important;
        }

        hr {
            margin-bottom: 8px;
        }

        .panel .panel-heading h2 {
            font-size: 20px;
        }
    </style>
    <%--//Calendar End//--%>

    <style type="text/css">
        .panel .panel-heading {
            border: none;
            background: #fff;
            padding: 0;
        }

            .panel .panel-heading h2 {
                font-size: 20px;
            }

        .mang-dashboard-white-widget {
            background: #fff;
            padding: 5px 0px 0px 0px;
            margin-bottom: 10px;
            border-radius: 10px;
        }

        .panel {
            margin-bottom: 10px;
        }

            .panel .panel-heading h2 {
                font-size: 20px;
            }

            .panel .panel-heading-dashboard {
                border: none;
                background: #fff;
                padding: 0;
            }

        .d-none {
            display: none !important;
        }

        .info-box {
            min-height: 115px;
            margin-bottom: 10px;
            border: 1px solid #ADD8E6;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%-- <div class="clearfix" style="height: 20px"></div>--%>

    <div class="row1" style="background: none;">
        <div class="dashboard1">

            <div id="countSummary" class="col-lg-12 col-md-12 colpadding0">
                <div class="panel panel-default" style="background: none;">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivCountSummary" style="background: none;">
                        <h2>Summary</h2>
                        <div class="panel-actions">
                            <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseDivCountSummary"><i class="fa fa-chevron-up"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div id="collapseDivCountSummary" class="panel-collapse collapse in" style="margin-bottom: 10px">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                        <div class="info-box white-bg">
                            <div class="title">Notice</div>

                            <div class="col-md-6  borderright">
                                <a href="../aspxPages/CaseListNew.aspx?Status=Open&CT=N">
                                    <div class="count" runat="server" id="divOpenNoticeCount">0</div>
                                </a>
                                <div class="desc">Open</div>
                                <div class="progress thin dashboardProgressbar d-none">
                                    <% var openNoticePer = GetOpenNoticeGuagePercentage(); %>
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=openNoticePer%>%">
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-6">
                                <a href="../aspxPages/CaseListNew.aspx?Status=Closed&CT=N">
                                    <div class="count" runat="server" id="divClosedNoticeCount">0</div>
                                </a>
                                <div class="desc">Closed</div>
                                <div class="progress thin dashboardProgressbar d-none">
                                    <% var closedNoticePer = GetClosedNoticeGuagePercentage(); %>
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=closedNoticePer%>%">
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                        <div class="info-box white-bg">
                            <div class="title">Case</div>

                            <div class="col-md-6 borderright">
                                <a href="../aspxPages/CaseListNew.aspx?Status=Open&CT=C">
                                    <div class="count" runat="server" id="divOpenCaseCount">0</div>
                                </a>
                                <div class="desc">Open</div>
                                <div class="progress thin dashboardProgressbar d-none">
                                    <% var openCasePer = GetOpenCaseGuagePercentage(); %>
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=openCasePer%>%">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <a href="../aspxPages/CaseListNew.aspx?Status=Closed&CT=C">
                                    <div class="count" runat="server" id="divClosedCaseCount">0</div>
                                </a>
                                <div class="desc">Closed</div>
                                <div class="progress thin dashboardProgressbar d-none">
                                    <% var closedCasePer = GetClosedCaseGuagePercentage(); %>
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=closedCasePer%>%">
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                        <div class="info-box white-bg">
                            <div class="title">Task</div>

                            <div class="col-md-6 borderright">
                                <a href="../aspxPages/TaskListNew.aspx?Status=Open">
                                    <div class="count" runat="server" id="divOpenTaskCount">0</div>
                                </a>
                                <div class="desc">Open</div>
                                <div class="progress thin dashboardProgressbar d-none">
                                    <% var openTaskPer = GetOpenTaskGuagePercentage(); %>
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=openTaskPer%>%">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                 <a href="../aspxPages/TaskListNew.aspx?Status=Closed">
                                    <div class="count" runat="server" id="divClosedTaskCount">0</div>
                                </a>
                                <div class="desc">Closed</div>
                                <div class="progress thin dashboardProgressbar d-none">
                                    <% var closedTaskPer = GetClosedTaskGuagePercentage(); %>
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <%=closedTaskPer%>%">
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                     <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == 5)
                                {%>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                                <div class="info-box white-bg">
                                    <div class="title">Case Hearing</div>

                                    <div class="col-md-12 ">
                                        <a href="../aspxPages/PendingHearingList.aspx?Status=Pending">
                                            <div class="count" runat="server" id="divPendingHearing">0</div>
                                        </a>
                                        <div class="desc">Pending for Updation</div>
                                        <div class="progress thin dashboardProgressbar d-none">
                                            <% var PendingHearingPer = GetPendingHearingGuagePercentage(); %>
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: <%=PendingHearingPer%>%">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <%}%>


                     <%--  <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == 5)
                     {%>--%>
                     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth">
                        <div class="info-box white-bg">
                            <div class="title">Total Liability</div>                        
                            <div class="col-md-12">
                               <%-- <a href="../aspxPages/Valueashboard.aspx"></a>--%>
                                    <div class="count" runat="server" onclick="ftotalliability()" id="divTotalLiability">0</div>                             
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <%-- <%}%> --%>
                </div>
            </div>

            <!-- Upcoming Hearing-->
            <div class="row">
                <div id="divOuterUpcomingHearing" class="row mang-dashboard-white-widget" style="margin-top: 10px">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseUpcomingHearing" style="margin-left: 10px;">
                                    <h2>Hearing Calendar</h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseUpcomingHearing">
                                            <i class="fa fa-chevron-up"></i></a>
                                    </div>
                                </div>
                                <div id="collapseUpcomingHearing" class="panel-collapse collapse in">
                                    <div class="panel-body" style="max-height: 500px; overflow: auto; overflow-y: hidden">
                                        <div class="col-md-12">
                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                ValidationGroup="DashboardTaskValidationGroup1" />
                                            <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                                ValidationGroup="DashboardTaskValidationGroup1" Display="None" />
                                        </div>

                                        <div class="col-md-5 colpadding0" style="margin-bottom: 27px;">
                                            <!-- Responsive calendar - START -->
                                            <div class="responsive-calendar" style="width: 95%; margin-top: 15px;">
                                                <img src="../../images/processing.gif">
                                            </div>
                                            <!-- Responsive calendar - END -->
                                            <div>
                                                <asp:LinkButton runat="server" ID="btnCreateICS" OnClick="btnCreateICS_Click" ToolTip="Download Calendar (Outlook, Google)" data-toggle="tooltip" Style="float: right; margin-right: 76px">
                                                        <img src="../../Images/Calendar_ICS.png" style="position: absolute;"/>
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div id="divUpcomingHearing">
                                                <div ><span style="float:left;height: 20px;font-family: 'Roboto',sans-serif;color: #666;font-size:16px;" id="clsdatel"></span>
                                                    <asp:LinkButton style="float:right;"  runat="server" ID="lnkShowAll" Text="Show All" OnClientClick="BindUpcomingGridAllData();"></asp:LinkButton>
                                                    <br /> <p style="font-family: 'Roboto',sans-serif; color: #666; float:left; width:100%">Select a date from calendar to view details</p>
                                                    
                                                </div>
                                                 <div class="clearfix" style="height:0px;"></div>
                                                   <div id="datacal">
                                                             <img src="../../images/processing.gif" id="imgcaldate" style="position: absolute;">
                                                       <iframe  id="calframe" src="about:blank"  scrolling="no" frameborder="0" width="100%" height="400px"></iframe>
                                                   </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Upcoming Hearing END-->

            <div class="row">
                <%-- <div class="col-md-12">--%>
                <div id="divOuterTask" class="row mang-dashboard-white-widget" style="margin-top: 10px;">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">

                            <div class="panel panel-default">
                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTask" style="margin-left: 10px;">
                                    <h2>Task</h2>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" data-toggle="collapse" data-parent="#accordion" href="#collapseTask">
                                            <i class="fa fa-chevron-up"></i></a>
                                    </div>
                                </div>

                                <div id="collapseTask" class="panel-collapse collapse in">
                                    <div class="panel-body" style="max-height: 250px; overflow: auto;">
                                        <div class="col-md-12">
                                            <asp:ValidationSummary ID="vsTaskPanel" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                ValidationGroup="DashboardTaskValidationGroup" />
                                            <asp:CustomValidator ID="cvTaskPanel" runat="server" EnableClientScript="False"
                                                ValidationGroup="DashboardTaskValidationGroup" Display="None" />
                                        </div>

                                        <div id="divTask" class="col-md-12">
                                            <asp:GridView runat="server" ID="grdTaskActivity" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                GridLines="None" PageSize="5" AutoPostBack="true" CssClass="table" Width="100%"
                                                DataKeyNames="TaskID" OnRowDataBound="grdTaskActivity_RowDataBound" OnRowCreated="grdTaskActivity_RowCreated">
                                                <Columns>

                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <%--<img id="imgdiv<%# Eval("TaskID") %>" class="sample" src="/Images/add.png" alt="Show Responses" style="cursor: pointer" />--%>
                                                            <img id="imgCollapseExpand" class="sample" src="/Images/add.png" runat="server" alt="Show Responses" style="cursor: pointer" />
                                                            <asp:Panel ID="pnlTaskResponse" runat="server" Style="display: none">
                                                                <asp:GridView ID="gvTaskResponses" runat="server" AutoGenerateColumns="false" CssClass="table"
                                                                    Width="100%" ShowHeaderWhenEmpty="false" GridLines="None" OnRowCommand="grdTaskResponseLog_RowCommand">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <%#Container.DataItemIndex+1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Response" ItemStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                    <asp:Label ID="lblTask" runat="server" Text='<%# Eval("Description") %>'
                                                                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Remark" ItemStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblTaskDesc" runat="server" Text='<%# Eval("Remark") %>'
                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Remark") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Responded On" ItemStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                    <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ResponseDate") != null ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                        data-toggle="tooltip" data-placement="bottom"
                                                                                        ToolTip='<%# Eval("ResponseDate") != null ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>



                                                                        <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                    <asp:Label ID="lblTaskResDoc" runat="server" Text='<%# ShowTaskResponseDocCount((long)Eval("TaskID"),(long)Eval("ID")) %>'>  <%--ID=TaskResponseID--%>
                                                                                    </asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel runat="server" ID="upTaskResDocDelete" UpdateMode="Always">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton
                                                                                            CommandArgument='<%# Eval("ID")+","+ Eval("TaskID")%>' CommandName="DownloadTaskResponseDoc"
                                                                                            ID="lnkBtnDownloadTaskResDoc" runat="server">
                                                                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download Documents" />
                                                                                        </asp:LinkButton>

                                                                                        <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                            AutoPostBack="true" CommandName="DeleteTaskResponse"
                                                                                            OnClientClick="return confirm('Are you certain you want to delete this Response?');"
                                                                                            ID="lnkBtnDeleteTaskResponse" runat="server">
                                                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete Response" />
                                                                                        </asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="lnkBtnDownloadTaskResDoc" />
                                                                                        <asp:PostBackTrigger ControlID="lnkBtnDeleteTaskResponse" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <EmptyDataTemplate>
                                                                        No Response Submitted yet.
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </asp:Panel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <%#Container.DataItemIndex+1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Priority" ItemStyle-Width="10%" FooterStyle-Width="10%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTaskPriority" runat="server" Text='<%# Eval("Priority") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task" ItemStyle-Width="20%">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                <asp:Label ID="lblTask" runat="server" Text='<%# Eval("TaskTitle") %>'
                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task Description" ItemStyle-Width="20%">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                <asp:Label ID="lblTaskDesc" runat="server" Text='<%# Eval("TaskDesc") %>'
                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskDesc") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="10%">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="10%" FooterStyle-Width="10%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTaskStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Type" ItemStyle-Width="10%">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">

                                                                <asp:Label ID="lblC" runat="server" Text='<%# Eval("TaskType") != null ? Convert.ToString(Eval("TaskType")).ToLower() == "c" ? "Case":"Notice" : "Task" %>'
                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskType") != null ? Convert.ToString(Eval("TaskType")).ToLower() == "c" ? "Case":"Notice" : "Task" %>'></asp:Label>

                                                                <asp:Label ID="NoticeCaseInstanceID" runat="server" Style="display: none;" Text='<%# Eval("NoticeCaseInstanceID") %>'
                                                                    data-toggle="tooltip" data-placement="bottom"></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Type Title" ItemStyle-Width="10%">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                <asp:Label ID="lblCTitle" runat="server"
                                                                    data-toggle="tooltip" data-placement="bottom"></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Assigned To" ItemStyle-Width="20%">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                <asp:Label ID="lblAssignedTo" runat="server" Text='<%# Eval("AssignToName") %>'
                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignToName") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                                <RowStyle CssClass="clsROWgrid" />
                                                <HeaderStyle CssClass="clsheadergrid" />
                                                <EmptyDataTemplate>
                                                    No Records Found.
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="col-md-12" style="text-align: right">
                                        <asp:LinkButton runat="server" ID="lnkShowMoreTask" Text="..Show More"
                                            PostBackUrl="~/Litigation/aspxPages/TaskList.aspx"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%-- </div>--%>
            </div>

        </div>
    </div>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="75%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
