﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddCourt : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["CourtID"]))
                    {
                        int ID = Convert.ToInt32(Request.QueryString["CourtID"]);
                        tbl_CourtType objCourt = LitigationCourtAndCaseType.GetCourtTypeDetailByID(ID, CustomerID);
                        tbxCourtType.Text = objCourt.CourtType;
                        ViewState["Mode"] = 1;
                        ViewState["CourtID"] = ID;
                    }
                    else
                    {
                        ViewState["Mode"] = 0;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvAddCourtPopup1.IsValid = false;
                    cvAddCourtPopup1.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                tbl_CourtType objCourt = new tbl_CourtType()
                {
                    CourtType = tbxCourtType.Text.Trim(),
                    CustomerID =(int) CustomerID
                };

                if ((int) ViewState["Mode"] == 1)
                {
                    objCourt.CourtID = Convert.ToInt32(ViewState["CourtID"]);
                }

                if ((int) ViewState["Mode"] == 0)
                {
                    if (LitigationCourtAndCaseType.CheckCourtTypeExist(objCourt))
                    {
                        cvAddCourtPopup1.IsValid = false;
                        cvAddCourtPopup1.ErrorMessage = "Court Type already exists.";
                    }
                    else
                    {
                        objCourt.Createdby = AuthenticationHelper.UserID;
                        objCourt.CreatedOn = DateTime.Now;
                        objCourt.IsDeleted = false;
                        LitigationCourtAndCaseType.CreateCourtTypeDetails(objCourt);
                        cvAddCourtPopup1.IsValid = false;
                        cvAddCourtPopup1.ErrorMessage = "Court Type Saved Successfully.";
                        ValidationSummary2.CssClass = "alert alert-success";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindCourt", "restoreSelectedCourt();", true);
                        upPromotor.Update();
                        tbxCourtType.Text = string.Empty;
                        var key = "LitiCourt-" + Convert.ToString(CustomerID);
                        if (CacheHelper.Exists(key))
                        {
                            CacheHelper.Remove(key);
                        }
                    }
                }

                else if ((int) ViewState["Mode"] == 1)
                {
                    objCourt.Updatedby = AuthenticationHelper.UserID;
                    objCourt.UpdatedOn = DateTime.Now;
                    LitigationCourtAndCaseType.UpdateCourtTypeDetails(objCourt);
                    cvAddCourtPopup1.IsValid = false;
                    cvAddCourtPopup1.ErrorMessage = "Court Type Updated Successfully.";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindCourt", "restoreSelectedCourt();", true);
                    var key = "LitiCourt-" + Convert.ToString(CustomerID);
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Remove(key);
                    }
                    upPromotor.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddCourtPopup1.IsValid = false;
                cvAddCourtPopup1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseMe();", true);
        }
    }
}