﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="Liti_MailAuthorization.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.Liti_MailAuthorization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

    <style type="text/css">
        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
        }

        .k-textbox .k-icon {
            top: 50%;
            margin: -7px 5px 0px;
            position: absolute;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 200px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: none;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
            margin: 5px;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-multiselect-wrap .k-input {
            /*padding-top:6px;*/
            display: inherit !important;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftmastermenu');
            fhead('Masters / Mail Authorization');

            Bindgrid();

            $("#dropdownUserType").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                optionLabel: "Type of User",
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    BindUser();
                    FilterAll();
                },
                dataSource: [
                    { text: "Internal", value: "Internal User" },
                    { text: "Lawyer", value: "Lawyer User" },
                    { text: "External", value: "External user" },
                ]
            });

            $("#dropdownRole").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                optionLabel: "Role",
                dataTextField: "Name",
                dataValueField: "Name",
                change: function (e) {
                    BindUser();
                    FilterAll();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: "<% =Path%>Litigation/RoleTypes?CustId=<% =CustId%>",
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                }
            });

            $("#dropdownMailServices").kendoDropDownTree({
                placeholder: "Mail Services",
                filter: "contains",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "text",
                dataValueField: "text",
                change: function (e) {

                    fCreateStoryBoard('dropdownMailServices', 'filterMailService', 'Mail');
                },
                dataSource: [
                    { text: "Monthly Hearings", value: "1" },
                    { text: "Hearings After 2 Days", value: "4" },
                    { text: "Hearings After 7 Days", value: "5" },
                    { text: "Case Creation", value: "6" },
                    { text: "On Add Hearing", value: "7" },
                    { text: "Hearing Update", value: "8" },
                    { text: "Task Assigned", value: "9" },
                    { text: "Case Closed", value: "10" },
                ]
            });
            BindUser();

        });

        function BindUser() {
            debugger

            if ($('#dropdownUsers').data('kendoDropDownTree')) {
                $('#dropdownUsers').data('kendoDropDownTree').destroy();

                $('#divusers').empty();
                $('#divusers').append('<input id="dropdownUsers" />');
            }

            $("#dropdownUsers").kendoDropDownTree({
                placeholder: "Users",
                filter: "contains",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "UserName",
                dataValueField: "UserName",
                change: function (e) {
                    FilterAll();
                    fCreateStoryBoard('dropdownUsers', 'filterUserList', 'users');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: "<% =Path%>Litigation/MailAuthorizationUserList?CustId=<% =CustId%>&UserType=" + $("#dropdownUserType").val() + '&Role=' + $("#dropdownRole").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                }
            });
        }


        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();
            fCreateStoryBoard('dropdownMailServices', 'filterMailService', 'Mail');
            fCreateStoryBoard('dropdownUsers', 'filterUserList', 'users');
        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filterMailService') {
                $('#' + div).append('Mail Services&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            if (div == 'filterUserList') {
                $('#' + div).append('Users&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB;height: 20px;Color:Gray;margin-left:5px;margin-bottom: 4px;border-radius:10px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="Clear" aria-label="Clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" title="Clear" aria-label="Clear" style="font-size: 12px;"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
        }

        function ClearAllFilterMain(e) {
            $("#dropdownRole").data("kendoDropDownList").select(0);
            $("#dropdownUserType").data("kendoDropDownList").select(0);
            $("#dropdownUsers").data("kendoDropDownTree").value([]);
            $("#dropdownMailServices").data("kendoDropDownTree").value([]);
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function FilterAll() {

            var Userlist = $("#dropdownUsers").data("kendoDropDownTree")._values;

            if (($("#dropdownRole").val() != 0 && $("#dropdownRole").val() != -1 && $("#dropdownRole").val() != "")
                || ($("#dropdownUserType").val() != 0 && $("#dropdownUserType").val() != -1 && $("#dropdownUserType").val() != "")
                || Userlist.length) {

                var finalSelectedfilter = { logic: "and", filters: [] };

                if (Userlist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(Userlist, function (i, v) {
                        locFilter.filters.push({
                            field: "UserName", operator: "contains", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }

                if ($("#dropdownRole").val() != 0 && $("#dropdownRole").val() != -1 && $("#dropdownRole").val() != "") {
                    var RoleFilter = { logic: "or", filters: [] };

                    RoleFilter.filters.push({
                        field: "LitigationRole", operator: "eq", value: $("#dropdownRole").val()
                    });

                    finalSelectedfilter.filters.push(RoleFilter);
                }

                if ($("#dropdownUserType").val() != 0 && $("#dropdownUserType").val() != -1 && $("#dropdownUserType").val() != "") {
                    var typeFilter = { logic: "or", filters: [] };

                    typeFilter.filters.push({
                        field: "UserType", operator: "eq", value: $("#dropdownUserType").val()
                    });

                    finalSelectedfilter.filters.push(typeFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }

        }

        var DDFS = [
            { text: "Monthly", value: "1" },
            { text: "After 2 Days", value: "2" },
            { text: "After 7 Days", value: "3" },
            { text: "2 Days Before", value: "4" },
            { text: "7 Days Before", value: "5" },
            { text: "Hearing Update", value: "6" },
            { text: "On Add Hearing", value: "7" },
        ];

        function Bindgrid() {

            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: "<% =Path%>Litigation/MailAuthorizationUserListKendo?CustId=<% =CustId%>",
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    pageSize: 10,
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                toolbar: false,
                resizable: true,
                multi: true,
                selectable: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                    }
                },
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    {
                        field: "UserName", title: 'User Name',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        field: "Email", title: 'Email',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        field: "LitigationRole", title: 'Role',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%"
                    },
                    {
                        field: "UserType", title: 'Type of User',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%"
                    },
                    {
                        field: "MailServicesID", title: 'Mail Services',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "30%"
                    },
                ]
            });


            $("#grid").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "th",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

        }

        var multiSelectArrayToString = function (item) {
            return item.MailServicesID;
        }

        function btnDisabledMail_Click(e) {
            var grid = $("#grid").getKendoGrid();
            var trs = grid.dataSource;
            var filteredDataSource = new kendo.data.DataSource({
                data: trs.data(),
                filter: trs.filter()
            });

            filteredDataSource.read();
            var data = filteredDataSource.view();

            var MailServicesID = $("#dropdownMailServices").data("kendoDropDownTree").value();
            var SuccessResult = 0;
            for (var i = 0; i < data.length; i++) {
                $.ajax({
                    type: 'POST',
                    url: "<% =Path%>Litigation/EditMailAuthorizationUserList?ID=" + data[i].ID + '&MailServicesID=' + MailServicesID + '&UserName=disable&Email=&Role=&Code=&UserType=&LitigationRole=',
                    dataType: "json",
                    success: function (result) {
                        SuccessResult++;
                        if (data.length == SuccessResult) {
                            Bindgrid();
                            FilterAll();
                        }
                    },
                    error: function (result) {
                        // notify the data source that the request failed
                        console.log(result);
                    }
                });
            }

            e.preventDefault();
        }

        function btnEnabledMail_Click(e) {
            var grid = $("#grid").getKendoGrid();
            var trs = grid.dataSource;
            var filteredDataSource = new kendo.data.DataSource({
                data: trs.data(),
                filter: trs.filter()
            });

            filteredDataSource.read();
            var data = filteredDataSource.view();

            var MailServicesID = $("#dropdownMailServices").data("kendoDropDownTree").value();
            var SuccessResult = 0;
            for (var i = 0; i < data.length; i++) {
                $.ajax({
                    type: 'POST',
                    url: "<% =Path%>Litigation/EditMailAuthorizationUserList?ID=" + data[i].ID + '&MailServicesID=' + MailServicesID + '&UserName=enable&Email=&Role=&Code=&UserType=&LitigationRole=',
                    dataType: "json",
                    success: function (result) {
                        SuccessResult++;
                        if (data.length == SuccessResult) {
                            Bindgrid();
                            FilterAll();
                        }
                    },
                    error: function (result) {
                        // notify the data source that the request failed
                        console.log(result);
                    }
                });

            }
            e.preventDefault();
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div style="margin: 0.5% -0.4% 0; width: 99%;">
        <div class="col-md-1" style="width: 15.5%; margin-right: 10px;">
            <input id="dropdownUserType" />
        </div>
        <div class="col-md-1" style="width: 15.5%; margin-right: 10px;">
            <input id="dropdownRole" />
        </div>
        <div id="divusers" class="col-md-2" style="width: 15.5%; margin-right: 10px;">
            <input id="dropdownUsers" />
        </div>
        <div class="col-md-2" style="width: 15.5%; margin-right: 10px">
            <input id="dropdownMailServices" />
        </div>
        <div class="col-md-1" style="width: 7%;">
            <button id="btnEnabledMail" onclick="btnEnabledMail_Click(event)" class="k-button">Enabled</button>
        </div>
        <div class="col-md-1" style="width: 7.5%;">
            <button id="btnDisabledMail" onclick="btnDisabledMail_Click(event)" class="k-button">Disabled</button>
        </div>
        <div class="col-md-1">
            <button id="ClearfilterMain" class="k-button" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
        </div>
        <div class="col-md-3">
        </div>
    </div>
    <div>
        <div class="row" style="font-size: 12px; font-weight: bold; color: black; margin-left: 10px; margin-top: 10px;" id="filterMaiL">&nbsp;</div>
        <div class="row" style="font-size: 12px; font-weight: bold; color: black; margin-left: 10px; margin-top: 10px;display:none" id="filterUserList">&nbsp;</div>
        <div class="row" style="font-size: 12px; font-weight: bold; color: black; margin-left: 10px; margin-top: 10px;display:none" id="filterMailService">&nbsp;</div>
    </div>
    <div class="row">
        <div id="grid" style="width: 98.4%; margin: 10px;"></div>
    </div>

</asp:Content>

