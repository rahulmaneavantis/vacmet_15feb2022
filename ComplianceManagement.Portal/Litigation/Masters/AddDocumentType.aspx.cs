﻿using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddDocumentType : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        public static string docTypeID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["LitigationDocId"]))
                    {
                        long docTypeID = Convert.ToInt64(Request.QueryString["LitigationDocId"]);

                        tbl_Litigation_DocumentTypeMaster _objContDocRecord = CaseManagement.GetDocTypeDetailsByID(docTypeID, Convert.ToInt32(CustomerID));

                        if (_objContDocRecord != null)
                        {
                            btnSave.Text = "Update";
                            tbxDocumentType.Text = _objContDocRecord.TypeName;
                            ViewState["Mode"] = 1;
                            ViewState["ContDocTypeID"] = docTypeID;
                        }
                    }
                    else
                    {
                        btnSave.Text = "Save";
                        ViewState["Mode"] = 0;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Something went wrong, Please try again";
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                tbl_Litigation_DocumentTypeMaster _objContDoc = new tbl_Litigation_DocumentTypeMaster()
                {
                    TypeName = tbxDocumentType.Text.Trim(),
                    CustomerID = (int)CustomerID,
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedOn = DateTime.Now,
                    IsDeleted = false
                };

                if ((int)ViewState["Mode"] == 0)
                {
                    if (CaseManagement.ExistsContDocumentType(_objContDoc))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Litigation Document-Type already exists.";
                    }
                    else
                    {
                        long newDocTypeID = 0;

                        newDocTypeID = CaseManagement.CreateDocumentType(_objContDoc);
                        tbxDocumentType.Text = "";
                        if (newDocTypeID > 0)
                        {
                            docTypeID = newDocTypeID.ToString();

                            cvDuplicateLocation.IsValid = false;
                            cvDuplicateLocation.ErrorMessage = "Document Type Added Successfully";
                            ValidationSummary1.CssClass = "alert alert-success";
                        }
                    }
                }

                else if ((int)ViewState["Mode"] == 1)
                {
                    if (ViewState["ContDocTypeID"] != null)
                    {
                        _objContDoc.ID = Convert.ToInt64(ViewState["ContDocTypeID"]);
                        _objContDoc.UpdatedBy = AuthenticationHelper.UserID;
                        _objContDoc.UpdatedOn = DateTime.Now;

                        bool saveSuccess = CaseManagement.UpdateDocTypeDetails(_objContDoc);
                        tbxDocumentType.Text = "";
                        if (saveSuccess)
                        {
                            cvDuplicateLocation.IsValid = false;
                            cvDuplicateLocation.ErrorMessage = "Document Type Updated Successfully";
                            ValidationSummary1.CssClass = "alert alert-success";
                        }
                        else
                        {
                            cvDuplicateLocation.IsValid = false;
                            cvDuplicateLocation.ErrorMessage = "Something went wrong, Please try again";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateLocation.IsValid = false;
                cvDuplicateLocation.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        //[WebMethod]
        //public static string getDocTypeID()
        //{
        //    string doctypeID = string.Empty;
        //    try
        //    {
        //        if (docTypeID != null)
        //        {
        //            doctypeID = docTypeID;
        //        }

        //        return doctypeID;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        return "null";
        //    }
        //}

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseMe();", true);
        }
    }
}