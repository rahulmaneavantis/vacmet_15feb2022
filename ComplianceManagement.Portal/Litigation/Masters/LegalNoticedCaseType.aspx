﻿<%@ Page Title="Case/Notice Type Master" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="LegalNoticedCaseType.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.LegalNoticedCaseType" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <style type="text/css">
        .AddNewspan1 {
    padding: 5px;
    border: 2px solid #f4f0f0;
    border-radius: 51px;
    display: inline-block;
    height: 26px;
    width: 26px;
}
    </style>
    <script type="text/javascript">
        function fopenpopup() {
            $('#divCaseTypeDialog').modal('show');
        }

        function OpenCaseTypePopup(CaseTypeID) {
            $('#AddCaseTypePopUp').modal('show');
            $('#ContentPlaceHolder1_IframeCaseType').attr('b')
            $('#ContentPlaceHolder1_IframeCaseType').attr('src', "../../Litigation/Masters/AddCaseType.aspx?CaseTypeId=" + CaseTypeID);
        }

        function CloseCaseTypePopUp() {
            $('#AddCaseTypePopUp').modal('hide');
            //location.reload();
        }
        function RefreshParent() {
            window.location.href = window.location.href;
        }
    </script>


    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            setactivemenu('leftmastermenu');
            fhead('Masters / Case or Notice Type');
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12 ">
                <div style="margin-bottom: 4px" />
                
                <div class="col-md-3 colpadding0 entrycount">
                    <asp:TextBox runat="server" ID="tbxtypeTofilter" placeholder="Type to Search" CssClass="form-control" OnTextChanged="tbxtypeTofilter_TextChanged" AutoPostBack="true" />
                </div>

                <div style="text-align: right">
                    <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lnkBtnApplyFilter" Width="7%" OnClick="lnkBtnApplyFilter_Click" data-toggle="tooltip" ToolTip="Apply"/>
                    <asp:LinkButton  CssClass="btn btn-primary" runat="server" ID="btnAddPromotor" OnClick="btnAddPromotor_Click"  data-toggle="tooltip" ToolTip="Add New Case/Notice Type">
                     <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                </div>
                <div style="margin-bottom: 4px">

                    <asp:GridView runat="server" ID="grdCaseType" AutoGenerateColumns="false" AllowSorting="true" OnRowDataBound="grdCaseType_RowDataBound" ShowHeaderWhenEmpty="true"
                        PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="ID" 
                        OnRowCommand="grdCaseType_RowCommand" OnSorting="grdCaseType_Sorting" OnRowCreated="grdCaseType_RowCreated">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="CaseType" HeaderText="Case/Notice Type" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="90%" SortExpression="CaseType"/>
                            <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="8%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_CaseType" ToolTip="Edit Case/Notice Type" data-toggle="tooltip"
                                        CommandArgument='<%# Eval("ID") %>'><img src='<%# ResolveUrl("../../Images/edit_icon_new.png")%>' alt="Edit"/></asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_CaseType" ToolTip="Delete Case/Notice Type" data-toggle="tooltip"
                                        CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this Case/Notice Type?');">
                                                <img src='<%# ResolveUrl("../../Images/delete_icon_new.png")%>' alt="Delete"/></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />
                        <PagerSettings Visible="false" />
                        <EmptyDataTemplate>
                            No Record Found
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>

                <div class="col-md-12 colpadding0">
                    <div class="col-md-8 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                            <p style="padding-right: 0px !Important;">
                                <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>&nbsp;- 
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-2 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2 colpadding0" style="float: right;">
                        <div style="float: left; width: 50%">
                            <p class="clsPageNo" style="color: #666">Page</p>
                        </div>
                        <div style="float: left; width: 50%">
                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                            </asp:DropDownListChosen>
                        </div>
                    </div>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divCaseTypeDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 430px;">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 180px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Add/Edit Notice & Case Type</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="AddCaseType">
                        <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateLocation" runat="server" EnableClientScript="False"
                                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                        <label style="width: 110px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                            Case Type</label>
                                        <asp:TextBox runat="server" ID="tbxCaseType" CssClass="form-control" Style="width: 250px;" MaxLength="100" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Case type can not be empty." ControlToValidate="tbxCaseType"
                                            runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="PromotorValidationGroup"
                                            ErrorMessage="Please enter a valid Case Type" ControlToValidate="tbxCaseType"
                                            ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 140px; margin-top: 10px">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="PromotorValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                                    </div>
                                    <div class="clearfix" style="height: 50px">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="AddCaseTypePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 35%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 300px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Add/Edit Notice & Case Type</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="RefreshParent()">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframeCaseType" runat="server" frameborder="0" width="100%" height="261px"></iframe>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
