﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddRefNo : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["InsID"]))
                    {
                        int caseInstanceID = Convert.ToInt32(Request.QueryString["CourtID"]);
                        if (caseInstanceID != 0)
                        {
                            ViewState["Mode"] = 0;
                            ViewState["InsID"] = caseInstanceID;
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvAddRefNo.IsValid = false;
                    cvAddRefNo.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["InsID"] != null)
                {
                    int caseInstanceID = Convert.ToInt32(ViewState["InsID"]);

                    if (caseInstanceID != 0)
                    {
                        bool validateData = false;
                        if(tbxHearingDate.Text!="")
                        {
                            if(tbxHearingRefNo.Text!="")
                            {
                                validateData = true;
                            }
                            else
                            {
                                cvAddRefNo.IsValid = false;
                                cvAddRefNo.ErrorMessage = "";
                            }
                        }
                        else
                        {
                            cvAddRefNo.IsValid = false;
                            cvAddRefNo.ErrorMessage = "Please provide hearing Date.";
                        }

                        if (validateData)
                        {
                            tbl_CaseHearingRef objNewRefNo = new tbl_CaseHearingRef()
                            {
                                CaseNoticeInstanceID = caseInstanceID,
                                HearingDate = Convert.ToDateTime(tbxHearingDate.Text),
                                HearingRefNo = tbxHearingRefNo.Text.Trim(),
                                CustomerID = (int) CustomerID,
                                IsDeleted = false,

                                CreatedBy = AuthenticationHelper.UserID,
                            };

                            if ((int) ViewState["Mode"] == 0)
                            {
                                if (!CaseManagement.ExistsRefNo(objNewRefNo))
                                {
                                    var newID = CaseManagement.CreateNewRefNo(objNewRefNo);
                                    if (newID > 0)
                                    {
                                        cvAddRefNo.IsValid = false;
                                        cvAddRefNo.ErrorMessage = "Save Successfully.";
                                    }                                 
                                }
                                else 
                                {
                                    cvAddRefNo.IsValid = false;
                                    cvAddRefNo.ErrorMessage = "Hearing Date already exists, Please select Hearing and continue with the task creation.";                                  
                                }
                            }                           
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddRefNo.IsValid = false;
                cvAddRefNo.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseMe();", true);
        }

        protected void tbxHearingDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["InsID"] != null)
                {
                    int caseInstanceID = Convert.ToInt32(ViewState["InsID"]);

                    if (caseInstanceID != 0)
                    {
                        tbl_CaseHearingRef objNewRefNo = new tbl_CaseHearingRef()
                        {
                            CaseNoticeInstanceID = caseInstanceID,
                            HearingDate = Convert.ToDateTime(tbxHearingDate.Text),
                            HearingRefNo = tbxHearingRefNo.Text.Trim(),
                            CustomerID = (int) CustomerID,
                            IsDeleted = false,

                            CreatedBy = AuthenticationHelper.UserID,
                        };

                        var result = CaseManagement.GetExistsRefNo(objNewRefNo);
                        if (result != -1)
                        {
                            tbxHearingRefNo.Text = "Hearing-" + (++result) + "-" + tbxHearingDate.Text;                           
                        }
                        else
                        {
                            cvAddRefNo.IsValid = false;
                            cvAddRefNo.ErrorMessage = "Hearing Date already exists, Please select Hearing and continue with the task creation.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}