﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddLawyer.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.AddLawyer" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <script type="text/javascript">
        function CloseMe() {
            window.parent.CloseFirmLawyerPop();
        }

        $(document).ready(function () {
            $("#tbxBranch").unbind('click');
            $("#tbxBranch").click(function () {
                $("#divBranches").toggle("blind", null, 500, function () { });
            });
        });


        function txtclick() {
            $("#divBranches").toggle("blind", null, 500, function () { });
        }

        //function hideDivBranch() {
        //    $('#divBranches').hide("blind", null, 500, function () { });
        //}

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        function OpenDepartmentPopup(a) {
            $('#AddDepartmentPopUp').modal('show');
            $('#IframeDepartment').attr('src', "../../Litigation/Masters/AddDepartMent.aspx?DepartmentID=" + a);
        }

        function OpenLawFirmPopupModel() {
            $('#AddLawFirmModelPopup').modal('show');
            $('#IFLawFirm').attr('src', "/Litigation/Masters/AddLawFirm.aspx");
        }
    </script>
    <style type="text/css">
        .chosen-container {
            width: 250px;
        }

        .ul {
            height: 150px;
        }
    </style>

</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="LitigationAddUser" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upUsersPopup" runat="server" UpdateMode="Conditional" OnLoad="upUsersPopup_Load">
                <ContentTemplate>
                    <div>
                        <div style="margin-bottom: 7px">
                            <asp:ValidationSummary ID="vsUserPopup" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="UserPopupValidationGroup" />
                            <asp:CustomValidator ID="cvUserPopup" runat="server" EnableClientScript="False"
                                ErrorMessage="Email already exists." ValidationGroup="UserPopupValidationGroup" Display="None" />
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                    First Name</label>
                                <asp:TextBox runat="server" ID="tbxFirstNameUser" Style="width: 70%;" CssClass="form-control" MaxLength="100" />
                                <span id="lblError" style="color: red; margin-left: 113px;"></span>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="First Name can not be empty."
                                    ControlToValidate="tbxFirstNameUser" runat="server" ValidationGroup="UserPopupValidationGroup"
                                    Display="None" />
                            </div>
                            <div class="form-group col-md-6">
                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                    Last Name</label>
                                <asp:TextBox runat="server" ID="tbxLastNameUser" Style="width: 70%;" CssClass="form-control"
                                    MaxLength="100" />
                                <span id="lblError1" style="color: red; margin-left: 113px;"></span>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Last Name can not be empty."
                                    ControlToValidate="tbxLastNameUser" runat="server" ValidationGroup="UserPopupValidationGroup"
                                    Display="None" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                    Designation</label>
                                <asp:TextBox runat="server" ID="tbxDesignation" Style="width: 70%;" CssClass="form-control"
                                    MaxLength="50" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Designation can not be empty."
                                    ControlToValidate="tbxDesignation" runat="server" ValidationGroup="UserPopupValidationGroup"
                                    Display="None" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                                    ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter a valid designation."
                                    ControlToValidate="tbxDesignation" ValidationExpression="^[a-zA-Z_]+[a-zA-Z0-9_ .]*$"></asp:RegularExpressionValidator>
                            </div>
                            <div class="form-group col-md-6">
                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                    Email</label>
                                <asp:TextBox runat="server" ID="tbxEmailUser" Style="width: 70%;" MaxLength="200" CssClass="form-control" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Email can not be empty."
                                    ControlToValidate="tbxEmailUser" runat="server" ValidationGroup="UserPopupValidationGroup"
                                    Display="None" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" Display="None" runat="server"
                                    ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter a valid email."
                                    ControlToValidate="tbxEmailUser" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                    Contact No</label>
                                <asp:TextBox runat="server" ID="tbxContactNoUser" Style="width: 70%;" CssClass="form-control"
                                    MaxLength="32" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ErrorMessage="Contact Number can not be empty."
                                    ControlToValidate="tbxContactNoUser" runat="server" ValidationGroup="UserPopupValidationGroup"
                                    Display="None" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" Display="None" runat="server"
                                    ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter a valid contact number."
                                    ControlToValidate="tbxContactNoUser" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers" TargetControlID="tbxContactNoUser" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" Display="None" runat="server"
                                    ValidationGroup="UserPopupValidationGroup" ErrorMessage="Please enter only 10 digit contact number."
                                    ControlToValidate="tbxContactNoUser" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                            </div>
                            <div class="form-group col-md-6">
                                <div runat="server" id="divddlLayerList">
                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                        Law Firm
                                    </label>
                                    <asp:DropDownList runat="server" ID="ddlLayerList" Style="padding: 0px; margin: 0px; width: 70%;" CssClass="form-control m-bot15"
                                        onchange="ShowLawFirmAddbutton()" />
                                    <img id="lnkShowAddNewLawFirmModal" style="float: right; display: none; margin-top: -30px" src="../../Images/add_icon_new.png"
                                        onclick="OpenLawFirmPopupModel();" alt="Add New Lawyer" title="Add New Lawyer" />
                                </div>
                                <div style="float: right; text-align: center; width: 0%; margin-top: 1%;">
                                    <asp:LinkButton ID="lnkLawFirmBind" OnClick="lnkLawFirmBind_Click" Style="float: right; display: none;" runat="server"></asp:LinkButton>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                    Department</label>
                                <asp:DropDownList runat="server" ID="ddlDepartment" Style="padding: 0px; margin: 0px; width: 70%;" CssClass="form-control m-bot15" onchange="ddlDepartmentChange()" />
                                <img id="lnkAddNewDepartmentModal" style="float: right; display: none; margin-top: -30px" src="../../Images/add_icon_new.png" onclick="OpenDepartmentPopup('')" alt="Add New Department" title="Add New Department" /><br />
                                <asp:CheckBox runat="server" ID="chkHead" Style="display: none; padding: 0px; margin: 0px; height: 30px; width: 10px; color: #333; margin-left: 165px" Text="Is Department Head" />
                                <asp:CompareValidator ID="CVDept" ErrorMessage="Please select department." ControlToValidate="ddlDepartment"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserPopupValidationGroup" Display="None" />
                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                    <asp:LinkButton ID="lnkBtnDept" OnClick="lnkBtnDept_Click" Style="float: right; display: none;" runat="server"></asp:LinkButton>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                    Address</label>
                                <asp:TextBox runat="server" ID="tbxAddressUser" Style="width: 70%;" MaxLength="500"
                                    TextMode="MultiLine" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <div runat="server" id="divLitigationRole">
                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                        Role (Litigation)</label>
                                    <asp:DropDownList runat="server" ID="ddlLitigationRole" Style="padding: 0px; margin: 0px; width: 70%;"
                                        CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlLitigationRole_SelectedIndexChanged" />
                                    <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please Select User Role." ControlToValidate="ddlLitigationRole"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="UserPopupValidationGroup"
                                        Display="None" />
                                </div>
                            </div>
                            <div class="form-group col-md-6">

                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                    Specialization</label>
                                <asp:TextBox runat="server" ID="TextBox1" CssClass="form-control" Style="width: 70%;" MaxLength="500" />
                            </div>
                        </div>

                        <div id="div1" style="margin-bottom: 7px; display: none;">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                            <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                User Type</label>
                            <asp:RadioButtonList ID="rbCheckType" runat="server" RepeatDirection="Horizontal"
                                Style="padding: 0px; margin: 0px; width: 390px;">
                                <%--OnSelectedIndexChanged="rbCheckType_SelectedIndexChanged" AutoPostBack="true"--%>
                                <asp:ListItem class="radio-inline" Text="Internal" Value="Internal" Selected="True"></asp:ListItem>
                                <asp:ListItem class="radio-inline" Text="External" Value="External"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <div runat="server" id="divCustomer">
                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                        Customer</label>
                                    <asp:DropDownList runat="server" ID="ddlCustomerPopup" Style="padding: 0px; margin: 0px; width: 70%;"
                                        CssClass="form-control m-bot15" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerPopup_SelectedIndexChanged" />
                                    <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please Select Customer."
                                        ControlToValidate="ddlCustomerPopup" runat="server" ValueToCompare="-1" Operator="NotEqual"
                                        ValidationGroup="UserPopupValidationGroup" Display="None" />
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div runat="server" id="divReportingTo">
                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                    <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                        Reporting to</label>
                                    <asp:DropDownList runat="server" ID="ddlReportingTo" Style="padding: 0px; margin: 0px; width: 70%;"
                                        CssClass="form-control m-bot15" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <div runat="server" id="divCustomerBranch">
                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                    <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                        Location</label>
                                    <asp:TextBox runat="server" ID="tbxBranch" onclick="txtclick()" Style="padding: 5px; margin: 0px; width: 70%;" AutoComplete="off" CausesValidation="true"
                                        CssClass="form-control" />
                                    <div style="margin-left: 21%; position: absolute; z-index: 10; margin-top: -20px;" id="divBranches">
                                        <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                            BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="100px" Width="100%"
                                            Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged">
                                        </asp:TreeView>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ErrorMessage="Please select Location."
                                        ControlToValidate="tbxBranch" runat="server" ValidationGroup="UserPopupValidationGroup" InitialValue="Select Location"
                                        Display="None" />
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                            </div>
                        </div>


                        <div class="row">
                            <div class="form-group col-md-6">

                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                    Profile Picture</label>
                                <asp:FileUpload ID="UserImageUpload" runat="server" ForeColor="#333" />
                                <asp:Button ID="btnUpload" runat="server" Text="Upload" Visible="false" />
                            </div>
                            <div class="form-group col-md-6">
                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 19%; display: block; float: left; font-size: 13px; color: #333;">
                                    &nbsp;
                                </label>
                                <asp:Image ID="ImageShow" runat="server" Height="100" Width="70%" ImageUrl="~/UserPhotos/DefaultImage.png" Visible="false" />
                            </div>
                        </div>

                        <div style="margin-bottom: 7px">
                        </div>

                        <div style="margin-bottom: 7px">
                        </div>

                        <div style="margin-bottom: 7px">
                        </div>

                        <asp:Repeater runat="server" ID="repParameters">
                            <ItemTemplate>
                                <div style="margin-bottom: 7px">
                                    <asp:Label runat="server" ID="lblName" Style="width: 150px; display: block; float: left; font-size: 13px; color: #333;"
                                        Text='<%# Eval("Name")  + ":"%>' />
                                    <asp:TextBox runat="server" ID="tbxValue" Style="height: 20px; width: 390px;" Text='<%# Eval("Value") %>'
                                        MaxLength='<%# Eval ("Length") %>' />
                                    <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ValueID") %>' />
                                    <asp:HiddenField runat="server" ID="hdnEntityParameterID" Value='<%# Eval("ParameterID") %>' />
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>

                        <div class="clearfix"></div>

                        <div style="margin-bottom: 7px; margin-top: 10px; text-align: center;">
                            <%--float: right; margin-right: 257px; --%>;
                                    <asp:Button Text="Save" runat="server" ID="SaveLawyer" CssClass="btn btn-primary" OnClick="SaveLawyer_Click"
                                        ValidationGroup="UserPopupValidationGroup" />
                            <asp:Button Text="Close" runat="server" ID="CloseLaywerPopUp" CssClass="btn btn-primary" OnClientClick="CloseMe();" />

                        </div>

                        <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 25px;">
                            <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                        </div>
                        <div class="clearfix" style="height: 50px"></div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <%--<asp:PostBackTrigger ControlID="btnSave" />--%>
                    <%--<asp:AsyncPostBackTrigger ControlID="btnUpload" EventName="Click" />--%>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>

    <%--Department Popup--%>
    <div class="modal fade" id="AddDepartmentPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 40%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom">
                        Add New Department</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframeDepartment" frameborder="0" runat="server" width="100%" height="150px"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="AddLawFirmModelPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog p0" style="width: 60%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label class="modal-header-custom" id="Lawfirmmodel">
                        Add/Edit Law Firm</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IFLawFirm" frameborder="0" runat="server" width="100%" height="450px"></iframe>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function ClosePopDepartment() {
            $('#AddDepartmentPopUp').modal('hide');
            document.getElementById('<%= lnkBtnDept.ClientID %>').click();
        }

        function CloseLawFirmModal() {
            $('#AddLawFirmModelPopup').modal('hide');
            document.getElementById('<%= lnkLawFirmBind.ClientID %>').click();
        }

        function ddlDepartmentChange() {
            var selectedDeptID = $("#<%=ddlDepartment.ClientID %>").val();
            if (selectedDeptID != null) {
                if (selectedDeptID == 0) {
                    $("#lnkAddNewDepartmentModal").show();
                }
                else {
                    $("#lnkAddNewDepartmentModal").hide();
                }
            }
        }

        function ShowLawFirmAddbutton() {
            var selectedPartyID = $("#ddlLayerList").find("option:selected").text();
            if (selectedPartyID != null) {
                if (selectedPartyID == "Add New") {
                    $("#lnkShowAddNewLawFirmModal").show();
                }
                else {
                    $("#lnkShowAddNewLawFirmModal").hide();
                }
            }
        }
    </script>
</body>
</html>
