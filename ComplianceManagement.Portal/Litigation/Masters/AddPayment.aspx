﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddPayment.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.AddPayment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="../../NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="../../NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="../../NewCSS/style.css" rel="stylesheet" />
    <link href="../../NewCSS/style-responsive.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript">

        function CloseMe() {
            window.parent.ClosePopPayment();
        }
        function RefreshParent() {
            window.parent.location.href = window.parent.location.href;
        }
        $(function () {
            $("#txtFName").keypress(function (e) {
                var keyCode = e.keyCode || e.which;

                $("#lblError").html("");

                //Regex for Valid Characters i.e. Alphabets.
                var regex = /^[a-zA-Z\s-, ]+$/;

                //Validate TextBox value against the Regex.
                var isValid = regex.test(String.fromCharCode(keyCode));
                if (!isValid) {
                    $("#lblError").html("Only alphabets allowed.");
                }

                return isValid;
            });
        });
    </script>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="LitigationAddPayment" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upPayment" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <div style="margin-bottom: 7px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PaymentPopupValidationGroup" />
                            <asp:CustomValidator ID="cvDeptPopup" runat="server" EnableClientScript="False"
                                ValidationGroup="PaymentPopupValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>
                        <div style="margin-bottom: 7px; align-content: center">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                            <label style="width: 60px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Payment</label>
                            <asp:TextBox runat="server" ID="txtFName" CssClass="form-control" Style="width: 250px;" MaxLength="100" />
                            <span id="lblError" style="color: red;margin-left: 70px;"></span>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Payment Name can not be empty." ControlToValidate="txtFName"
                                runat="server" ValidationGroup="PaymentPopupValidationGroup" Display="None" />
                        </div>
                        <div style="margin-bottom: 7px; text-align: center">
                            <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary"
                                ValidationGroup="PaymentPopupValidationGroup" OnClick="btnSave_Click" />
                            <asp:Button Text="Close" runat="server" ID="btnCancelDeptPopUp" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe()" />

                        </div>
                        <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                            <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                        </div>
                        <div class="clearfix" style="height: 50px"></div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
