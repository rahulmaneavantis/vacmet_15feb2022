﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddCaseStage : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["CaseStageTypeId"]))
                    {
                        int ID = Convert.ToInt32(Request.QueryString["CaseStageTypeId"]);
                        tbl_TypeMaster _objCaseStage = CaseManagement.GetCaseStageDetailByID(ID, CustomerID);

                        if (_objCaseStage != null)
                        {
                            btnSave.Text = "Update";
                            tbxcasestageType.Text = _objCaseStage.TypeName;
                            //tbxcasestageTypecode.Text = _objCaseStage.TypeCode;
                            ViewState["Mode"] = 1;
                            ViewState["CaseStageID"] = ID;
                        }
                    }
                    else
                    {
                        btnSave.Text = "Save";
                        ViewState["Mode"] = 0;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvAddEditCaseStageType.IsValid = false;
                    cvAddEditCaseStageType.ErrorMessage = "Server Error Occurred. Please try again.";
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                tbl_TypeMaster _objCaseStage = new tbl_TypeMaster()
                {
                    TypeName = tbxcasestageType.Text.Trim(),
                    //  TypeCode = tbxcasestageTypecode.Text,
                    customerID = (int)CustomerID
                };
                if ((int)ViewState["Mode"] == 1)
                {
                    _objCaseStage.ID = Convert.ToInt32(ViewState["CaseStageID"]);
                }
                if ((int)ViewState["Mode"] == 0)
                {
                    if (CaseManagement.ExistsCaseStageType(_objCaseStage))
                    {
                        cvAddEditCaseStageType.IsValid = false;
                        cvAddEditCaseStageType.ErrorMessage = "Contract type already exists.";
                    }
                    else
                    {
                        _objCaseStage.CreatedBy = AuthenticationHelper.UserID;
                        _objCaseStage.CreatedOn = DateTime.Now;
                        _objCaseStage.TypeCode = "CS";
                        _objCaseStage.IsActive = true;
                        _objCaseStage.UpdatedBy = AuthenticationHelper.UserID;
                        _objCaseStage.UpdatedOn = DateTime.Now;
                        long casetypeID = CaseManagement.CreateCaseStageDetails(_objCaseStage);
                        if (casetypeID > 0)
                        {
                            cvAddEditCaseStageType.IsValid = false;
                            cvAddEditCaseStageType.ErrorMessage = "Case Stage Type Saved Successfully.";
                            vsAddEditCaseStageType.CssClass = "alert alert-success";
                            tbxcasestageType.Text = string.Empty;
                        }
                    }
                }

                else if ((int)ViewState["Mode"] == 1)
                {
                    _objCaseStage.TypeCode = "CS";
                    _objCaseStage.UpdatedBy = AuthenticationHelper.UserID;
                    _objCaseStage.UpdatedOn = DateTime.Now;
                    CaseManagement.UpdateCaseStageDetails(_objCaseStage);
                    cvAddEditCaseStageType.IsValid = false;
                    cvAddEditCaseStageType.ErrorMessage = "Case Stage Type Updated Successfully.";
                    vsAddEditCaseStageType.CssClass = "alert alert-success";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddEditCaseStageType.IsValid = false;
                cvAddEditCaseStageType.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }
    }
}