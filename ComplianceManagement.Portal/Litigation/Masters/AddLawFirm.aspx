﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddLawFirm.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.AddLawFirm" %>

<!DOCTYPE html>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml" style="overflow: hidden;">
<head runat="server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <link href="../../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <link href="../../NewCSS/litigation_custom_style.css" rel="stylesheet" />

    <script type="text/javascript">
        function CloseMe() {
            window.parent.CloseLawFirmModal();
        }

        $(function () {

            $('[id*=ddlLawyerTypes]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '390px'
            });
        });


        function RebindPopupddllayer() {
            $('[id*=ddlLawyerTypes]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '390px'
            });
        }

    </script>
    <style type="text/css">
        .chosen-container {
            width: 250px;
        }

        .ul {
            height: 150px;
        }
    </style>

</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="smLawFirm" runat="server"></asp:ScriptManager>
        <div>
            <asp:UpdatePanel ID="upLawyerPop" runat="server" UpdateMode="Conditional" style="margin-left: 13px;">
                <ContentTemplate>
                    <div style="margin: 5px">

                        <div style="margin-bottom: 7px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                ValidationGroup="NewAddLaywerValidate" />
                            <asp:CustomValidator ID="CustomModifyAsignment" runat="server" EnableClientScript="False"
                                ValidationGroup="NewAddLaywerValidate" Display="None" />
                        </div>
                        <div>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Law Firm</label>
                                <asp:TextBox runat="server" ID="tbxFirstName" Style="width: 390px;" CssClass="form-control" />
                                <span id="lblError" style="color: red;margin-left: 162px;"></span>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ErrorMessage="Name can not be empty."
                                    ControlToValidate="tbxFirstName" runat="server" ValidationGroup="NewAddLaywerValidate"
                                    Display="None" />
                            </div>
                            <div style="margin-bottom: 7px; display: none;">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Last Name</label>
                                <asp:TextBox runat="server" ID="tbxLastName" Style="width: 390px;" CssClass="form-control"
                                    MaxLength="100" />
                            </div>

                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Email</label>
                                <asp:TextBox runat="server" ID="tbxEmail" Style="width: 390px;" MaxLength="200" CssClass="form-control" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage="Email can not be empty."
                                    ControlToValidate="tbxEmail" runat="server" ValidationGroup="NewAddLaywerValidate"
                                    Display="None" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="None" runat="server"
                                    ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid email."
                                    ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                            </div>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Contact No</label>
                                <asp:TextBox runat="server" ID="tbxContactNo" Style="width: 390px;" CssClass="form-control"
                                    MaxLength="32" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ErrorMessage="Contact Number can not be empty."
                                    ControlToValidate="tbxContactNo" runat="server" ValidationGroup="NewAddLaywerValidate"
                                    Display="None" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                                    ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid contact number."
                                    ControlToValidate="tbxContactNo" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="tbxContactNo" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                                    ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter only 10 digit contact no."
                                    ControlToValidate="tbxContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                            </div>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Address</label>
                                <asp:TextBox runat="server" ID="tbxAddress" Style="width: 390px;" MaxLength="500" CssClass="form-control"
                                    TextMode="MultiLine" />
                            </div>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Court Type</label>
                                <asp:ListBox ID="ddlLawyerTypes" CssClass="form-control" runat="server" SelectionMode="Multiple"></asp:ListBox>

                            </div>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                    Specialization</label>
                                <asp:TextBox runat="server" ID="tbxSpecilisation" Style="width: 390px;" MaxLength="500" CssClass="form-control" />
                            </div>

                            <div style="margin-bottom: 7px; text-align: center; margin-top: 10px;">
                                <asp:Button Text="Save" runat="server" ID="btnSaveLawFirm" OnClick="btnSaveLawFirm_Click" CssClass="btn btn-primary"
                                    ValidationGroup="NewAddLaywerValidate" />
                                <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();" />
                                <%--<asp:LinkButton ID="lnkLawFirmBind" OnClick="lnkLawFirmBind_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                </asp:LinkButton>--%>
                            </div>
                            <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 15px;">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 100px"></div>

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
