﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class LegalNoticedCaseType : System.Web.UI.Page
    {
        protected bool flag;
        private long CutomerID = AuthenticationHelper.CustomerID;
        protected tbl_PageAuthorizationMaster authpage;
        protected int pageid = 6;
        protected void Page_Load(object sender, EventArgs e)
        {
            String key = "Authenticate" + AuthenticationHelper.UserID;
            if (Cache.Get(key) != null)
            {
                var Records = (List<tbl_PageAuthorizationMaster>) Cache.Get(key);
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.PageID == pageid
                                 select row).FirstOrDefault();
                    authpage = query;
                }
            }
            if (!IsPostBack)
            {
                flag = false;
                BindCaseType();
                bindPageNumber();
                hideAddControls();
            }
        }

        public void hideAddControls()
        {
            if (authpage != null)
            {
                if (authpage.Addval == false)
                {
                    btnAddPromotor.Visible = false;
                }
                else
                {
                    btnAddPromotor.Visible = true;
                }
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        private void BindCaseType()
        {
            try
            {
                var caseTypeList = LitigationCourtAndCaseType.GetAllLegalCaseTypeData(CutomerID);

                if (caseTypeList.Count > 0)
                    caseTypeList = caseTypeList.OrderBy(entry => entry.CaseType).ToList();

                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                {
                    caseTypeList = caseTypeList.Where(entry => entry.CaseType.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();
                }

                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            caseTypeList = caseTypeList.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            caseTypeList = caseTypeList.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                flag = true;
                Session["TotalRows"] = null;    
                
                if (caseTypeList.Count > 0)
                {
                    grdCaseType.DataSource = caseTypeList;
                    Session["TotalRows"] = caseTypeList.Count;
                    grdCaseType.DataBind();
                }
                else
                {
                    grdCaseType.DataSource = caseTypeList;
                    grdCaseType.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }


                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
           else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdCaseType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindCaseType();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdCaseType.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                string ID = null;
                tbxCaseType.Text = string.Empty;
                upPromotor.Update();
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenCaseTypePopup('" + ID + "');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdCaseType.PageIndex = chkSelectedPage - 1;
            grdCaseType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindCaseType(); ShowGridDetail();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                tbl_CaseType objCase = new tbl_CaseType()
                {
                    CaseType = tbxCaseType.Text,
                    CustomerID=(int) CutomerID
                };

                if ((int) ViewState["Mode"] == 1)
                {
                    objCase.ID = Convert.ToInt32(ViewState["CaseID"]);
                }

                if ((int) ViewState["Mode"] == 0)
                {
                    if (LitigationCourtAndCaseType.CheckLegalCaseTypeExist(objCase))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Case/Notice Category Already Exists";
                    }
                    else
                    {
                        objCase.CreatedBy = AuthenticationHelper.UserID;
                        objCase.CreatedOn = DateTime.Now;
                        objCase.IsDeleted = false;
                        LitigationCourtAndCaseType.CreateLegalCaseTypeDetails(objCase);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Case/Notice Category Saved Successfully";
                        tbxCaseType.Text = string.Empty;
                    }
                }

                else if ((int) ViewState["Mode"] == 1)
                {
                    objCase.UpdatedBy = AuthenticationHelper.UserID;
                    objCase.UpdatedOn = DateTime.Now;
                    LitigationCourtAndCaseType.UpdateLegalCaseTypeDetails(objCase);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Case/Notice Category Updated Successfully";
                }

                BindCaseType();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdCaseType.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCaseType_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                if (authpage != null)
                {
                    if (authpage.Deleteval == false)
                    {
                        LinkButton LinkButton2 = (LinkButton) e.Row.FindControl("LinkButton2");
                        LinkButton2.Visible = false;
                    }
                    else
                    {
                        LinkButton LinkButton2 = (LinkButton) e.Row.FindControl("LinkButton2");
                        LinkButton2.Visible = true;
                    }
                    if (authpage.Modify == false)
                    {
                        LinkButton LinkButton1 = (LinkButton) e.Row.FindControl("LinkButton1");
                        LinkButton1.Visible = false;
                    }
                    else
                    {
                        LinkButton LinkButton1 = (LinkButton) e.Row.FindControl("LinkButton1");
                        LinkButton1.Visible = true;
                    }
                    if (authpage.Modify == false && authpage.Deleteval == false)
                    {
                        grdCaseType.Columns[2].Visible = false;
                    }
                }
            }
        }

        protected void grdCaseType_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int ID = 0;
                long CustomerID = AuthenticationHelper.CustomerID;

                if (e.CommandName.Equals("EDIT_CaseType"))
                {
                    ID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["CaseID"] = ID;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenCaseTypePopup('" + ID + "');", true);
                    tbl_CaseType Objcases = LitigationCourtAndCaseType.GetLegalCaseTypeDetailByID(ID, CustomerID);
                    tbxCaseType.Text = Objcases.CaseType;
                    BindCaseType();
                    upPromotor.Update();
                }
                else if (e.CommandName.Equals("DELETE_CaseType"))
                {
                    ID = Convert.ToInt32(e.CommandArgument);

                    if (CaseManagement.CheckCaseNoticeTypeAssignmentExist(ID))
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "javascript:alert('Case/Notice Type can not be delete. One or more case/notice are assigned to this Case/Notice Type');", true);
                    }
                    else
                    {
                        LitigationCourtAndCaseType.DeleteLegalCaseTypeDetail(ID, CustomerID);
                        BindCaseType();
                        bindPageNumber();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            BindCaseType();
            bindPageNumber();
        }

        protected void grdCaseType_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var caseTypeList = LitigationCourtAndCaseType.GetAllLegalCaseTypeData(CutomerID);

                if (caseTypeList.Count > 0)
                    caseTypeList = caseTypeList.OrderBy(entry => entry.CaseType).ToList();

                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                {
                    caseTypeList = caseTypeList.Where(entry => entry.CaseType.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();
                }

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    caseTypeList = caseTypeList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    caseTypeList = caseTypeList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdCaseType.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCaseType.Columns.IndexOf(field);
                    }
                }
                Session["TotalRows"] = null;
                flag = true;
                if (caseTypeList.Count > 0)
                {
                    grdCaseType.DataSource = caseTypeList;
                    Session["TotalRows"] = caseTypeList.Count;
                    grdCaseType.DataBind();
                }
                else
                {
                    grdCaseType.DataSource = caseTypeList;
                    grdCaseType.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCaseType_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void tbxtypeTofilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCaseType.PageIndex = 0;
                BindCaseType();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}