﻿<%@ Page Title="Opponent Master" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="LegalCasePartyDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.LegalCasePartyDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
    <style type="text/css">
        div#ContentPlaceHolder1_ddlState_chosen {
            width: 250px !important;
        }

        div#ContentPlaceHolder1_ddlCity_chosen {
            width: 250px !important;
        }

         .AddNewspan1 {
    padding: 5px;
    border: 2px solid #f4f0f0;
    border-radius: 51px;
    display: inline-block;
    height: 26px;
    width: 26px;
}
    </style>
    <script type="text/javascript">
        function fopenpopup() {
            $('#DivAddLCPartyDetails').modal('show');
        }
    </script>

    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            setactivemenu('leftmastermenu');
            fhead('Masters / Opponent');
        });

        function OpenPartyDetailPopup(a) {
            $('#AddPartyDetailsPop').modal('show');
            $('#ContentPlaceHolder1_IframePartyDetial').attr('src', "../../Litigation/Masters/AddPartyDetails.aspx?CaseTypeId=" + a);
        }

        function ClosePopParty() {
            //location.reload();
            $('#AddPartyDetailsPop').modal('hide');
            RefreshParent();
        }

        function RefreshParent() {
            window.location.href = window.location.href;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="col-lg-12 col-md-12 ">
                <div style="margin-bottom: 4px; margin-top: 5px;" />
                <div class="col-md-3 colpadding0 entrycount">
                    <asp:TextBox runat="server" ID="tbxtypeTofilter" placeholder="Type to Search" CssClass="form-control" OnTextChanged="tbxtypeTofilter_TextChanged" AutoPostBack="true" />
                </div>
               
                <div style="text-align: right">
                    <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lnkBtnApplyFilter" Width="7%" OnClick="lnkBtnApplyFilter_Click" data-toggle="tooltip" ToolTip="Apply"/>
                    <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" ID="btnAddPromotor" OnClick="btnAddPromotor_Click" data-toggle="tooltip" ToolTip="Add New Opponent">
                     <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                </div>
                <%--<i class='fa fa-plus'></i>--%>
                <div style="margin-bottom: 4px">
                    <asp:GridView runat="server" ID="grdLCParty" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true" OnSorting="grdLCParty_Sorting"
                        PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="ID" 
                        OnRowCommand="grdLCParty_RowCommand" OnRowCreated="grdLCParty_RowCreated" OnRowDataBound="grdLCParty_RowDataBound">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Type" SortExpression="PartyType" ItemStyle-Width="20%">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("PartyType") %>' ToolTip='<%# Eval("PartyType") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Name" ItemStyle-Width="20%" SortExpression="Name">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="State" Visible="false">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("StateID") %>' ToolTip='<%# Eval("StateID") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City" Visible="false">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CityID") %>' ToolTip='<%# Eval("CityID") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email" ItemStyle-Width="20%" SortExpression="Email">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Email") %>' ToolTip='<%# Eval("Email") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Contact Number" ItemStyle-Width="20%" SortExpression="ContactNumber">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                        <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ContactNumber") %>' ToolTip='<%# Eval("ContactNumber") %>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="8%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_LawyerLC" ToolTip="Edit Opponent" data-toggle="tooltip"
                                        CommandArgument='<%# Eval("ID") %>'><img src='<%# ResolveUrl("../../Images/edit_icon_new.png")%>' alt="Edit Details" /></asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_LawyerLC" ToolTip="Delete Opponent" data-toggle="tooltip"
                                        CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this Opponent?');"><img src='<%# ResolveUrl("../../Images/delete_icon_new.png")%>' alt="Delete Details"/></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />
                        <PagerSettings Visible="false" />
                        <PagerTemplate>
                        </PagerTemplate>
                        <EmptyDataTemplate>
                            No Record Found
                        </EmptyDataTemplate>
                    </asp:GridView>

                </div>
                <div class="col-md-12 colpadding0">
                    <div class="col-md-8 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                            <p style="padding-right: 0px !Important;">
                                <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>&nbsp;- 
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-2 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                        <asp:ListItem Text="5"/>
                        <asp:ListItem Text="10" Selected="True" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                        </div>
                    <div class="col-md-2 colpadding0" style="float: right;">
                        <div style="float: left; width: 50%">
                            <p class="clsPageNo">Page</p>
                        </div>
                        <div style="float: left; width: 50%">
                            <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                            </asp:DropDownListChosen>
                        </div>
                    </div>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="AddPartyDetailsPop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 35%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 180px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Add/Edit Opponent</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="RefreshParent()">x</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="IframePartyDetial" frameborder="0" runat="server" width="100%" height="490px"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
