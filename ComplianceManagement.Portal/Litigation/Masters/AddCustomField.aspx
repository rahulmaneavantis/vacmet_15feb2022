﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCustomField.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.AddCustomField" %>

<!DOCTYPE html>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>--%>

    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>

    <link href="~/NewCSS/litigation_custom_style.css" rel="stylesheet" />
    <script type="text/javascript">
        function CloseMe() {
            window.parent.ClosePopCustomField();
        }
        function RefreshParent() {
            window.parent.location.href = window.parent.location.href;
        }

        function OpenCategoryTypePopup() {
            var a = '';
            $('#AddCategoryType').modal('show');
            $('#IframeCategoryType').attr('src', "../../Litigation/Masters/AddCaseType.aspx?CaseTypeId=" + a);
        }

        $(function () {
            $('[id*=ddlTypes]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '68%'
            });
        });
    </script>
    
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>
            <div id="AddCustFieldDiv">
                <asp:ScriptManager ID="LitigationAddCustField" runat="server"></asp:ScriptManager>

                <div>
                    <div style="margin-bottom: 7px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="custFieldValidationGroup" />
                        <asp:CustomValidator ID="CvCustValiation" runat="server" EnableClientScript="False"
                            ValidationGroup="custFieldValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                    </div>
                    <div style="margin-bottom: 7px; align-content: center">
                        <label style="width: 3%; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                        <label style="width: 27%; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                            Type</label>
                        <asp:ListBox ID="ddlTypes" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="62%"></asp:ListBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Type can not be empty." ControlToValidate="ddlTypes"
                            runat="server" ValidationGroup="custFieldValidationGroup" Display="None" />
                        <%--<div style="float: right; text-align: center; width: 8%; margin-top: 1%;" visible="false">
                            <img id="lnkAddNewCaseCategoryModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenCategoryTypePopup()" alt="Add New Opponent" title="Add New Opponent" />
                        </div>--%>
                    </div>

                    <div style="margin-bottom: 7px; margin-top: 20px; align-content: center">
                        <label style="width: 3%; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                        <label style="width: 27%; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                            Parameter Label</label>
                        <asp:TextBox runat="server" ID="tbxLableName" CssClass="form-control" Style="width: 68%;" MaxLength="100" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Parameter Label can not be empty." ControlToValidate="tbxLableName"
                            runat="server" ValidationGroup="custFieldValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 140px; margin-top: 10px">
                        <asp:Button runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                            ValidationGroup="custFieldValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();RefreshParent();" />
                    </div>
                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                        <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                    </div>
                    <div class="clearfix" style="height: 50px">
                    </div>
                </div>
                <%--<asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                    </ContentTemplate>--%>
                <%--<Triggers>
                        <asp:PostBackTrigger ControlID="lnkAddNewCaseCategoryModal" />
                    </Triggers>
                </asp:UpdatePanel>--%>
            </div>
        </div>

        <%--Add Legal Case category--%>
        <div class="modal fade" id="AddCategoryType" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: 80%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 260px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                            Add Case Type</label>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    </div>
                    <div class="modal-body" style="width: 100%;">
                        <iframe src="about:blank" id="IframeCategoryType" frameborder="0" runat="server" width="100%" height="200px"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
