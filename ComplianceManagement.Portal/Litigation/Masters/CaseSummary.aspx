﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="CaseSummary.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.CaseSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .AddNewspan1 {
            padding: 5px;
            border: 2px solid #f4f0f0;
            border-radius: 51px;
            display: inline-block;
            height: 26px;
            width: 26px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftreportsmenu');
            fhead('Masters /MIS Summary');
        });

        function CloseCaseSummary() {

            $('#AddcaseSpeciman').modal('hide');
            RefreshParent();
        }

        function RefreshParent() {
            window.location.href = window.location.href;
        }
        function OpenPerticularsHeaders(a) {
            $('#AddParticulars').modal('show');
            $('#IframeParticulars').attr('src', "../aspxPages/AddDepartMent.aspx?DepartmentID=" + a);
        }

        function OpenCaseSummary(a) {
          
            $('#AddcaseSpeciman').modal('show');
            $('#IframeCaseSpeciman').attr('src', "../../Litigation/Masters/AddCaseSummary.aspx?CaseSummaryId=" + a);
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row colpadding0">
        <div class="col-md-12">
            <div class="col-md-6 colpadding0">
                <asp:TextBox runat="server" ID="tbxtypeTofilter" placeholder="Type to Search Perticulars" CssClass="form-control" Width="70%"/>
            </div>
            <div class="col-md-6 colpadding0">
                <div class="col-md-8"></div>
                <div class="col-md-2 colpadding0">
                    <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lbtsearchSummary" OnClick="lbtsearchSummary_Click" data-toggle="tooltip" ToolTip="Apply" Style="float: right"/>
                </div>
                <div class="col-md-2 colpadding0">
                    <asp:LinkButton Visible="false" Text="Add New" CssClass="btn btn-primary" runat="server" ID="btnAddSpecimen" OnClick="btnAddSummary_Click" data-toggle="tooltip" ToolTip="Add New Summary" Style="float: right">
                     <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>


    <div style="margin-bottom: 7px" class="col-md-12 row">
        <asp:ValidationSummary ID="ValidationSummary4" runat="server" Display="none" class="alert alert-block alert-danger fade in"
            ValidationGroup="CaseSummaryPopUpValidationGroup" />
        <asp:CustomValidator ID="cvCasePayment" runat="server" EnableClientScript="true"
            ValidationGroup="CaseSummaryPopUpValidationGroup" Display="None" />
    </div>
    <div class="form-group col-md-12 colpadding0">
      <%--  <asp:UpdatePanel ID="upCaseSummary" runat="server" UpdateMode="Conditional">
            <ContentTemplate>--%>
                <asp:GridView runat="server" ID="grd_Summary" AutoGenerateColumns="false" AllowSorting="true" EmptyDataText="No Records Found" ShowHeaderWhenEmpty="true"
                    GridLines="None" PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="true" OnSorting="grd_Summary_Sorting"
                    OnRowCommand="grd_Summary_RowCommand" OnRowDataBound="grd_Summary_RowDataBound" OnRowCreated="grd_Summary_RowCreated" DataKeyNames="Id">

                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Perticulars" HeaderText="Perticulars" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="30%" SortExpression="Perticulars" />
                        <asp:BoundField DataField="StateName" HeaderText="State Name" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%" SortExpression="StateName" />
                        <asp:BoundField DataField="DisputedAmt" HeaderText="Disputed Amount" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" />
                        <asp:BoundField DataField="Interest" HeaderText="Interest" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" />
                        <asp:BoundField DataField="Penalty" HeaderText="Penalty" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" />
                        <asp:BoundField DataField="Total" HeaderText="Total" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" />
                        <asp:BoundField DataField="AmountPaid" HeaderText="Amount Paid" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" />
                        <asp:BoundField DataField="Favourable" HeaderText="Favourable" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" />
                        <asp:BoundField DataField="Unfavourable" HeaderText="Unfavourable" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" />
                        <asp:BoundField DataField="Expense" HeaderText="Expense" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%" />
                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="30%">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_CaseSummary" ToolTip="Edit MIS Summary" data-toggle="tooltip"
                                    CommandArgument='<%# Eval("Id") %>'><img src='<%# ResolveUrl("../../Images/edit_icon_new.png")%>' alt="Edit" style="    margin-left: -19%;"/></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_CaseSummary" ToolTip="Delete MIS Summary" data-toggle="tooltip"
                                    CommandArgument='<%# Eval("Id") %>' OnClientClick="return confirm('Are you certain you want to delete this MIS Summary?');">
                                                <img src='<%# ResolveUrl("../../Images/delete_icon_new.png")%>' alt="Delete"/></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                     <PagerSettings Visible="false" />
                    <EmptyDataTemplate>
                        No Records Found
                    </EmptyDataTemplate>
                </asp:GridView>
         <%--   </ContentTemplate>
        </asp:UpdatePanel>--%>

        <div class="row">
            <div class="col-md-12 colpadding0">
                <div class="col-md-8 colpadding0">
                    <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                        <p style="padding-right: 0px !Important;">
                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="col-md-3 colpadding0">
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control" Style="width: 30%; float: right; margin-right: 3%;"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                        <asp:ListItem Text="5" />
                        <asp:ListItem Text="10" Selected="True" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                <div class="col-md-1 colpadding0" style="float: right;">
                    <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                        OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control" Width="100%" Height="30px">
                    </asp:DropDownListChosen>
                </div>

            </div>
            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
        </div>

    </div>

    <%--Add Perticular Headers --%>
    <div class="modal fade" id="AddParticulars" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog pt10 pb10">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-header-custom">
                        Particulars</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -9px;" onclick="CloseParticulars();">&times;</button>
                </div>
                <div class="modal-body">
                    <iframe id="IframeParticulars" src="about:blank" width="95%" height="auto" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <%--Add Perticular Headers --%>

    <%--Add Case Summary --%>
    <div class="modal fade" id="AddcaseSpeciman" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 80%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label class="modal-header-custom">
                        Add MIS Summary</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -9px;">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe id="IframeCaseSpeciman" src="about:blank" width="100%" height="490px" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <%--Add Case Speciman --%>
</asp:Content>
