﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddAuthority : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AuthId"]))
                    {
                        int ID = Convert.ToInt32(Request.QueryString["AuthId"]);
                        Authorityofthestate Objcases = Authority.GetAuthority(ID);
                        tbxName.Text = Objcases.Name;
                        tbxDesignation.Text = Objcases.Designation;
                        ViewState["Mode"] = 1;
                        ViewState["AuthId"] = ID;
                    }
                    else
                    {
                        ViewState["Mode"] = 0;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Authorityofthestate objAuth = new Authorityofthestate()
                {
                    Name = tbxName.Text.Trim(),
                    Designation = tbxDesignation.Text.Trim(),
                    CustomerId = (int)CustomerID,
                    UpdatedBy = AuthenticationHelper.UserID,
                    UpdatedDate = DateTime.Now
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    objAuth.Id = Convert.ToInt32(ViewState["AuthId"]);
                }

                if ((int)ViewState["Mode"] == 0)
                {

                    objAuth.CreatedBy = AuthenticationHelper.UserID;
                    objAuth.CreatedDate = DateTime.Now;
                    Authority.CreateAuthority(objAuth);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Authority Saved Successfully.";


                }

                else if ((int)ViewState["Mode"] == 1)
                {

                    Authority.UpdateAuthoritys(objAuth);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Authority Updated Successfully.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateLocation.IsValid = false;
                cvDuplicateLocation.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseMe();", true);
        }
    }
}
