﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddPartyDetails : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCountry();
                if (!string.IsNullOrEmpty(Request.QueryString["CaseTypeId"]))
                {
                    int PratyID = Convert.ToInt32(Request.QueryString["CaseTypeId"]);
                    tbl_PartyDetail LCData = LitigationLaw.GetLCPartyDetailsByID(PratyID);
                    tbxName.Text = LCData.Name;
                    ddlCountry.SelectedValue = (LCData.Country).ToString();
                    ddlCountry_SelectedIndexChanged(sender, e);
                    ddlState.SelectedValue = (LCData.StateID).ToString();
                    ddlState_SelectedIndexChanged(sender, e);
                    ddlCity.SelectedValue = (LCData.CityID).ToString();
                    tbxEmail.Text = LCData.Email;
                    tbxContactNo.Text = LCData.ContactNumber;
                    tbxAddress.Text = LCData.Address;
                    if (LCData.PartyType != null)
                    {
                        rbPartyType.SelectedValue = null;
                        if (LCData.PartyType.ToString() == "Individual")
                            rbPartyType.SelectedValue = "Individual";
                        else if (LCData.PartyType.ToString() == "Corporate")
                            rbPartyType.SelectedValue = "Corporate";
                    }
                    rbPartyType.SelectedValue = LCData.PartyType;
                    ViewState["Mode"] = 1;
                    ViewState["DeptID"] = PratyID;
                }
                else
                {
                    ViewState["Mode"] = 0;
                }
            }
        }

        public void BindCountry()
        {
            var CountryList = CityStateCountryManagement.GetAllContryData();

            ddlCountry.DataTextField = "Name";
            ddlCountry.DataValueField = "ID";

            ddlCountry.DataSource = CountryList;
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("Select Country", "-1"));

        }
        public void BindCity(int a)
        {
            if (ddlState.SelectedValue != "-1" && ddlState.SelectedValue != null)
            {
                var CountryList = StateCityManagement.GetAllCitysByState(Convert.ToInt32(ddlState.SelectedValue));

                ddlCity.DataTextField = "Name";
                ddlCity.DataValueField = "ID";

                ddlCity.DataSource = CountryList;
                ddlCity.DataBind();
                ddlCity.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
                ddlCity.Items.Insert(0, new ListItem("Select City", "-1"));
            }
        }

        public void BindState(int CountryID)
        {
            var StateListcountrywise = CityStateCountryManagement.GetAllStateCountryWise(CountryID);

            ddlState.DataTextField = "Name";
            ddlState.DataValueField = "ID";

            ddlState.DataSource = StateListcountrywise;
            ddlState.DataBind();

            ddlState.Items.Insert(0, new ListItem("Select State", "-1"));

        }
        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlState.SelectedValue))
            {
                if (ddlState.SelectedValue != "-1")
                {
                    BindCity(Convert.ToInt32(ddlState.SelectedValue));
                }
            }
        }
        protected void lnkBtnCity_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbxCity.Text != "")
                {
                    bool success = true;
                    int stateid = -1;
                    if (!string.IsNullOrEmpty(ddlState.SelectedValue))
                    {
                        stateid = Convert.ToInt32(ddlState.SelectedValue);
                    }
                    int newCityID = 0;
                    City newCity = new City()
                    {
                        Name = tbxCity.Text.Trim(),
                        StateId = stateid,
                        IsDeleted = false,
                    };

                    if (!StateCityManagement.ExitsCity(newCity))
                    {
                        CityValidator.IsValid = false;

                        CityValidator.ErrorMessage = "City with same name already exists.";
                        success = false;
                        return;
                    }

                    if (success)
                    {
                        newCityID = StateCityManagement.CreateCity(newCity);

                        if (newCityID != 0)
                        {
                            CityValidator.IsValid = false;
                            CityValidator.ErrorMessage = "City Saved Successfully.";
                            ValidationSummary1.CssClass = "alert alert-success";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindCity", "restoreSelectedCity();", true);
                            BindCity(Convert.ToInt32(ddlState.SelectedValue));
                            upDepartment.Update();
                        }
                    }


                }
                tbxCity.Text = " ";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CityValidator.IsValid = false;
                CityValidator.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void lnkRebindCity_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(ddlState.SelectedValue))
            {
                if (ddlState.SelectedValue != "-1")
                {
                    
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindCity", "restoreSelectedCity();", true);
                    BindCity(Convert.ToInt32(ddlState.SelectedValue));
                    upDepartment.Update();
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                tbl_PartyDetail objParty = new tbl_PartyDetail()
                {
                    PartyType = rbPartyType.SelectedValue,
                    Name = tbxName.Text.Trim(), 
                    Email = tbxEmail.Text.Trim(),
                    ContactNumber = tbxContactNo.Text.Trim(),                    
                    Address = tbxAddress.Text.Trim(),
                    CustomerID = Convert.ToInt32(CustomerID)
                };

                if (ddlCountry.SelectedValue != "")
                    objParty.Country = Convert.ToInt32(ddlCountry.SelectedValue);

                if (ddlState.SelectedValue != "")
                    objParty.StateID = Convert.ToInt32(ddlState.SelectedValue);

                if (ddlCity.SelectedValue != "")
                    objParty.CityID = Convert.ToInt32(ddlCity.SelectedValue);

                if ((int) ViewState["Mode"] == 1)
                {
                    objParty.ID = Convert.ToInt32(ViewState["DeptID"]);
                }

                if (!cvCustomeRemark.IsValid)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);


                if ((int) ViewState["Mode"] == 0)
                {
                    if (!string.IsNullOrEmpty(tbxName.Text.Trim()))
                    {                        
                        if (LitigationLaw.ExistsPartyName(objParty,0)) /*LitigationLaw.CheckLCDataExist(objlawyer)*/
                        {
                            cvCustomeRemark.IsValid = false;
                            cvCustomeRemark.ErrorMessage = "Opponent with same name already exists.";
                        }
                        else
                        {
                            LitigationLaw.CreateLegalCasePartyData(objParty);

                            cvCustomeRemark.IsValid = false;
                            cvCustomeRemark.ErrorMessage = "Opponent Details Saved Successfully.";
                            ValidationSummary1.CssClass = "alert alert-success";
                            var key = "LitiParty-" + Convert.ToString(CustomerID);
                            if (CacheHelper.Exists(key))
                            {
                                CacheHelper.Remove(key);
                            }
                            tbxName.Text = string.Empty;
                            tbxEmail.Text = string.Empty;
                            tbxContactNo.Text = string.Empty;
                            ddlState.SelectedValue = "-1";
                            ddlCity.SelectedValue = "-1";
                            ddlCountry.SelectedValue = "-1";
                            tbxAddress.Text = string.Empty;
                            rbPartyType.SelectedValue = "I";
                        }
                    }
                }
                else if ((int) ViewState["Mode"] == 1)
                {
                    if (!LitigationLaw.ExistsPartyName(objParty, objParty.ID))
                    {
                        LitigationLaw.UpdateLegalCasePartyData(objParty);

                        cvCustomeRemark.IsValid = false;
                        cvCustomeRemark.ErrorMessage = "Opponent Details Updated Successfully.";
                        ValidationSummary1.CssClass = "alert alert-success";
                        var key = "LitiParty-" + Convert.ToString(CustomerID);
                        if (CacheHelper.Exists(key))
                        {
                            CacheHelper.Remove(key);
                        }
                    }
                    else
                    {
                        cvCustomeRemark.IsValid = false;
                        cvCustomeRemark.ErrorMessage = "Opponent with same name already exists.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
            {
                if (ddlCountry.SelectedValue != "-1")
                {
                    BindState(Convert.ToInt32(ddlCountry.SelectedValue));
                }
            }
        }
    }
}