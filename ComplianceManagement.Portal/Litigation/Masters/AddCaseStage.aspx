﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCaseStage.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.AddCaseStage" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="../../NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="../../NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="../../NewCSS/style.css" rel="stylesheet" />
    <link href="../../NewCSS/style-responsive.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <script type="text/javascript">
        function CloseMe() {
            window.parent.CloseCaseStageTypePopUp();
        }
        function RefreshParent() {
            window.parent.location.href = window.parent.location.href;
        }
        $(function () {
            $("#tbxcasestageType").keypress(function (e) {
                var keyCode = e.keyCode || e.which;

                $("#lblError").html("");

                //Regex for Valid Characters i.e. Alphabets.
                var regex = /^[a-zA-Z\s-, ]+$/;

                //Validate TextBox value against the Regex.
                var isValid = regex.test(String.fromCharCode(keyCode));
                if (!isValid) {
                    $("#lblError").html("Only alphabets allowed.");
                }

                return isValid;
            });
        });
    </script>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="smAddEditCaseStageType" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upAddEditCaseStageType" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <div class="row form-group">
                            <asp:ValidationSummary ID="vsAddEditCaseStageType" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="CaseStageTypeValidationGroup" />
                            <asp:CustomValidator ID="cvAddEditCaseStageType" runat="server" EnableClientScript="False"
                                ValidationGroup="CaseStageTypeValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>
                        <%-- <div class="row form-group">
                            <div class="form-group required col-md-4">
                                <label for="tbxcasestageType" class="control-label">CaseStage TypeCode</label>
                                <asp:TextBox runat="server" ID="tbxcasestageTypecode" CssClass="form-control" />
                                <asp:RequiredFieldValidator ID="rfvtbxcasestageTypeCode" ErrorMessage="Required CaseStage CodeName" ControlToValidate="tbxcasestageTypecode"
                                    runat="server" ValidationGroup="CaseStageTypeValidationGroup" Display="None" />
                            </div>
                        </div>--%>
                        <div class="row form-group">
                            <div class="form-group required col-md-4">
                                <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                            <label style="width: 100px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Case Stage</label>
                                <asp:TextBox runat="server" ID="tbxcasestageType" CssClass="form-control" Style="width: 250px;" MaxLength="100" />
                                <span id="lblError" style="color: red;margin-left: 2px;"></span>
                                <asp:RequiredFieldValidator ID="rfvtbxcasestageTypeName" ErrorMessage="Required Case Stage Name" ControlToValidate="tbxcasestageType"
                                    runat="server" ValidationGroup="CaseStageTypeValidationGroup" Display="None" />
                            </div>
                        </div>
                        <div class="row form-group text-center">
                            <div class="col-md-12" style="margin-top: -9px;">
                                <asp:Button runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                    ValidationGroup="CaseStageTypeValidationGroup"  />
                                <asp:Button Text="Close" runat="server" ID="btnCancel"
                                    CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();" style="margin-left:13px"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
