﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddEntitiesAssignment.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.AddEntitiesAssignment" %>

<!DOCTYPE html>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <!-- Bootstrap CSS -->
     <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="../../NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="../../NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="../../NewCSS/style.css" rel="stylesheet" />
    <link href="../../NewCSS/style-responsive.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
     <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <script>
        function CloseMe() {
            window.parent.CloseEntitiesPopup();
        }
       
    </script>

    <script type="text/javascript">
        function fopenpopup() {
            $('#divContDocTypeDialog').modal('show');
        }

        function OpenEntitiesPopup() {
            $('#AddentitiesPopUp').modal('show');
            <%-- $("#<%=IframeDocType.ClientID %>").attr('src', "/ContractProduct/Masters/AddNewEntitiesAssignment.aspx);--%>
            $('#ContentPlaceHolder1_IframeEntities').attr('src', "/Litigation/Masters/AddEntitiesAssignment.aspx");
        }

        function CloseEntitiesPopup() {
            $('#AddentitiesPopUp').modal('hide');
             <%--document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();--%>
        }

        function RefreshParent() {
            window.parent.location.href = window.parent.location.href;
        }
    </script>
    
    
      <script language="javascript" type="text/javascript">

     
        function fCheckTree(obj) {
            var id = $(obj).attr('data-attr');
            var elm = $("#" + id);
            $(elm).trigger('click');
        }
        function FnSearch() {

            var tree = document.getElementById('BodyContent_tvFilterLocation');
            var links = tree.getElementsByTagName('a');
            var keysrch = document.getElementById('BodyContent_tbxFilterLocation').value.toLowerCase();
            var keysrchlen = keysrch.length
            if (keysrchlen > 2) {
                $('#bindelement').html('');
                for (var i = 0; i < links.length; i++) {

                    var anch = $(links[i]);
                    var twoletter = $(anch).html().toLowerCase().indexOf(keysrch);
                    var getId = $(anch).attr('id');
                    var parendNode = '#' + getId + 'Nodes';
                    var childanchor = $(parendNode).find('a');
                    if (childanchor.length == 0) {
                        if (twoletter > -1) {

                            var idchild = $($(anch).siblings('input')).attr('name');                            
                            var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  data-attr="' + idchild + '" ><a  >' + anch.html() + '</a></br>';
                            $('#bindelement').append(createanchor);
                        }
                    }

                }
                $(tree).hide();
                $('#bindelement').show();
            } else {
                $('#bindelement').html('');
                $('#bindelement').hide();
                $(tree).show();
            }

        }

        function OnTreeClick(evt)
        {
            <%--if ('<% =IsForBranch%>' == 0) {alert(1);}else{alert(111)}--%>
            debugger;
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                if ('<% =IsForBranch%>' == 0)
                {
                    var parentTable = GetParentByTagName("table", src);
                    var nxtSibling = parentTable.nextSibling;
                    if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                    {
                        if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                        {
                            //check or uncheck children at all levels
                            CheckUncheckChildren(parentTable.nextSibling, src.checked);
                        }
                    }
                    //check or uncheck parents at all levels
                    CheckUncheckParents(src, src.checked);
                }
            }
        }

        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }

        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }
      

</script>


    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
      .chosen-container {
   
         margin-left: 32px;
       }
    </style>
    <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {

            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });


            $('#BodyContent_tbxFilterLocation').keyup(function () {
                FnSearch();
            });
        }
        
    </script>

      <script type="text/javascript">
    $(function () {
        $("[id*=tvBranches] input[type=checkbox]").bind("click", function () {
            var table = $(this).closest("table");
            if (table.next().length > 0 && table.next()[0].tagName == "DIV") {
                //Is Parent CheckBox
                var childDiv = table.next();
                var isChecked = $(this).is(":checked");
                $("input[type=checkbox]", childDiv).each(function () {
                    if (isChecked) {
                        $(this).attr("checked", "checked");
                    } else {
                        $(this).removeAttr("checked");
                    }
                });
            } else {
                //Is Child CheckBox
                var parentDIV = $(this).closest("DIV");
                if ($("input[type=checkbox]", parentDIV).length == $("input[type=checkbox]:checked", parentDIV).length) {
                    $("input[type=checkbox]", parentDIV.prev()).attr("checked", "checked");
                } else {
                    $("input[type=checkbox]", parentDIV.prev()).removeAttr("checked");
                }
            }
        });


        $('#BodyContent_tbxFilterLocation').keyup(function () {
            FnSearch();
        });
    })
</script>

      <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
        .excelgriddisplay{
            display:none;
        }
               .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }

        .dd_chk_select {
            height: 30px !important;  
            text-align: center;          
            border-radius: 5px;
        }

        div.dd_chk_select div#caption
        {
            margin-top: 5px;
        }
          .chosen-container-single .chosen-single div {
              position: absolute;
              top: 0;
              right: 0;
              display: block;
              width: 18px;
              height: 100%;
              margin-top: 4px;
          }
    </style>

      <script type="text/javascript">
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });


            $('#BodyContent_tbxFilterLocation').keyup(function () {
                FnSearch();
            });

        }
        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }
    
        
    </script>

    <%--<script type="text/javascript">
        $(document).ready(function () {
           fhead('Entities Assignment');
        });

      </script>--%>


     <%-- <script type="text/javascript">
 
           function showProgress() {              
            var updateProgress = $get("<%# updateProgress.ClientID %>");
            updateProgress.style.display = "block";
        }
    </script>--%>

      <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 12px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
          .form-control {
              font-size: 13px;
          }
    </style>
    
</head>
<body  style="background-color: white">
    <form id="form1" runat="server">
    <div>
            <asp:ScriptManager ID="smAddNewDept" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upComplianceTypeList" runat="server" UpdateMode="Conditional" OnLoad="upComplianceTypeList_Load">
                <ContentTemplate>
                    <div>
                        <%--<div class="row form-group">
                            <asp:ValidationSummary ID="vsentities" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="EntitiesPopupValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="EntitiesPopupValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>--%>
                         <div class="row form-group">
                             <div class="col-md-12 alert alert-block alert-success fade in" runat="server" visible="false" id="divsuccessmsgaCDoctype">
                            <asp:Label runat="server" ID="successmsgaCDoctype" ></asp:Label>
                        </div>
                            <asp:ValidationSummary ID="vsAddDocType" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="EntitiesPopupValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="EntitiesPopupValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>
                       
                          <div class="row form-group">
                            <div class="form-group required col-md-4">
                                <label for="ddlUser" class="control-label">Select User</label>
                                   <%-- <input type="color" id="favcolor" runat="server" style="margin-left: 36px;" name="favcolor" value="#ff0000"><br> 
                                    <asp:TextBox runat="server" Style="display:none;" ID="Txtboxcolor" CssClass="form-control" />--%>   
                                 <asp:DropDownListChosen runat="server" AutoPostBack="true" ID="ddlUser"  AllowSingleDeselect="false" DisableSearchThreshold="3"
                               DataPlaceHolder="Select User" class="form-control" Width="70%"/> 
                                <%-- <asp:CompareValidator ID="CVDept" ErrorMessage="Please select User." ControlToValidate="ddlUser"
                                    runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="EntitiesPopupValidationGroup" Display="None" />   --%>
                                <asp:RequiredFieldValidator ID="rfvUser" ErrorMessage="Please Select User"
                                        ControlToValidate="ddlUser" runat="server" ValidationGroup="EntitiesPopupValidationGroup" Display="None" />      
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="form-group required col-md-4">
                               
                            
                                <div id="FilterLocationdiv" runat="server" style="float: left; ">
                              
                                     <label for="tbxSubContType" class="control-label">Select Location</label>
                                    <asp:TextBox runat="server" ID="tbxFilterLocation" Style="padding: 0px; margin-left: 115px;margin-top: -37px; height: 36px; width: 270px;"
                                        CssClass="form-control" />
                                    <div style="margin-left: 115px;margin-top: -21px; position: absolute; z-index: 10;" id="divFilterLocation">
                                        <asp:TreeView runat="server" ID="tvFilterLocation" BackColor="White" BorderColor="Black"
                                            BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="136px" Width="268px"
                                            Style="overflow: auto" ShowLines="true" ShowCheckBoxes="All" onclick="OnTreeClick(event)">
                                            
                                        </asp:TreeView>

                                        <div id="bindelement" style="background: white; height: 292px; display: none; width: 200px; border: 1px solid; overflow: auto;"></div>

                                        <asp:Button ID="btnlocation" runat="server" OnClick="btnlocation_Click" Text="Select" />
                                        <asp:Button ID="btnClear1" Visible="true" runat="server" OnClick="btnClear1_Click" Text="Clear" />
                                    </div>
                                </div>

                            </div>
                        </div>

                        
                      

                        <div class="row form-group text-center">
                            <div class="col-md-12">
                                <asp:Button Text="Save" runat="server" ID="btnSave" onclick="btnSave_Click" CssClass="btn btn-primary"
                                    ValidationGroup="EntitiesPopupValidationGroup" />
                                <asp:Button Text="Close" runat="server" ID="btnCloseDeptPopUp" CssClass="btn btn-primary"
                                    data-dismiss="modal" OnClientClick="CloseMe(); RefreshParent();" />
                            </div>
                        </div>                      
                       
                       <div class="row">
                            <div class="col-md-12">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
