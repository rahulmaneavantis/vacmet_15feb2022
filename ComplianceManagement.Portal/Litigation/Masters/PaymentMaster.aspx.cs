﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using System.Web.UI;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class PaymentMaster : System.Web.UI.Page
    {
        protected bool flag;
        protected tbl_PageAuthorizationMaster authpage;
        protected int pageid = 7;
        protected void Page_Load(object sender, EventArgs e)
        {
            String key = "Authenticate" + AuthenticationHelper.UserID;
            if (Cache.Get(key) != null)
            {
                var Records = (List<tbl_PageAuthorizationMaster>) Cache.Get(key);
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.PageID == pageid
                                 select row).FirstOrDefault();
                    authpage = query;
                }
            }
            if (!IsPostBack)
            {
                flag = false;
                BindPaymentList();
                bindPageNumber();
                hideAddControls();
            }
        }

        public void hideAddControls()
        {
            try
            {
                if (authpage != null)
                {
                    if (authpage.Addval == false)
                    {
                        btnAddPayment.Visible = false;
                    }
                    else
                    {
                        btnAddPayment.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPage.IsValid = false;
                cvPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdPayment_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_Payment"))
                {
                    int PaymentID = Convert.ToInt32(e.CommandArgument);
                    ViewState["Mode"] = 1;
                    ViewState["DeptID"] = PaymentID;
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenPaymentPopup('" + PaymentID + "');", true);
                    tbl_PaymentType RPD = LitigationPaymentType.PaymentMasterGetByID(PaymentID);
                }
                else if (e.CommandName.Equals("DELETE_Payment"))
                {
                    int PaymentID = Convert.ToInt32(e.CommandArgument);

                    if (CaseManagement.CheckPaymentTypeMappingExist(PaymentID))
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "javascript:alert('Payment Type can not be delete. One or more case/notice are assigned to this Payment Type');", true);
                    }
                    else
                    {
                        LitigationPaymentType.DeletePaymentMaster(PaymentID);
                        BindPaymentList();
                        bindPageNumber();
                        //GetPageDisplaySummary();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPage.IsValid = false;
                cvPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddPayment_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                string ID = null;
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenPaymentPopup('" + ID + "');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPage.IsValid = false;
                cvPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdPayment.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindPaymentList();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdPayment.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private void BindPaymentList()
        {
            try
            {
                var PaymentMasterList = LitigationPaymentType.GetAllPaymentMasterList();

                if (PaymentMasterList.Count > 0)
                    PaymentMasterList = PaymentMasterList.OrderBy(entry => entry.TypeName).ToList();

                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                {
                    PaymentMasterList = PaymentMasterList.Where(entry => entry.TypeName.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();
                }
                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            PaymentMasterList = PaymentMasterList.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            PaymentMasterList = PaymentMasterList.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                flag = true;
                Session["TotalRows"] = null;
                if (PaymentMasterList.Count > 0)
                {
                    grdPayment.DataSource = PaymentMasterList;
                    Session["TotalRows"] = PaymentMasterList.Count;
                    grdPayment.DataBind();
                }
                else
                {
                    grdPayment.DataSource = PaymentMasterList;
                    grdPayment.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPage.IsValid = false;
                cvPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }
                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdPayment.PageIndex = chkSelectedPage - 1;

                //SelectedPageNo.Text = (chkSelectedPage).ToString();
                grdPayment.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindPaymentList(); ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            BindPaymentList(); bindPageNumber();
        }

        protected void grdPayment_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var PaymentMasterList = LitigationPaymentType.GetAllPaymentMasterList();

                if (PaymentMasterList.Count > 0)
                    PaymentMasterList = PaymentMasterList.OrderBy(entry => entry.TypeName).ToList();

                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                {
                    PaymentMasterList = PaymentMasterList.Where(entry => entry.TypeName.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();
                }

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    PaymentMasterList = PaymentMasterList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    PaymentMasterList = PaymentMasterList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;
                foreach (DataControlField field in grdPayment.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdPayment.Columns.IndexOf(field);
                    }
                }
                Session["TotalRows"] = null;
                flag = true;
                if (PaymentMasterList.Count > 0)
                {
                    grdPayment.DataSource = PaymentMasterList;
                    Session["TotalRows"] = PaymentMasterList.Count;
                    grdPayment.DataBind();
                }
                else
                {
                    grdPayment.DataSource = PaymentMasterList;
                    grdPayment.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdPayment_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdPayment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (authpage != null)
                    {
                        if (authpage.Deleteval == false)
                        {
                            LinkButton LinkButton2 = (LinkButton) e.Row.FindControl("LinkButton2");
                            LinkButton2.Visible = false;
                        }
                        else
                        {
                            LinkButton LinkButton2 = (LinkButton) e.Row.FindControl("LinkButton2");
                            LinkButton2.Visible = true;
                        }
                        if (authpage.Modify == false)
                        {
                            LinkButton LinkButton1 = (LinkButton) e.Row.FindControl("LinkButton1");
                            LinkButton1.Visible = false;
                        }
                        else
                        {
                            LinkButton LinkButton1 = (LinkButton) e.Row.FindControl("LinkButton1");
                            LinkButton1.Visible = true;
                        }
                        if (authpage.Deleteval == false && authpage.Modify == false)
                        {
                            grdPayment.Columns[2].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPage.IsValid = false;
                cvPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}