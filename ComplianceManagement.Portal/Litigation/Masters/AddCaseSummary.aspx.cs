﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddCaseSummary : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["CaseSummaryId"]))
                    {
                        int ID = Convert.ToInt32(Request.QueryString["CaseSummaryId"]);
                        ViewState["CaseSummaryId"] = ID;
                        ViewState["Mode"] = 1;
                        BindState();
                        BindPerticulars();
                        BindCaseSummary(ID);

                    }
                    else
                    {
                        ViewState["Mode"] = 0;
                        BindState();
                        BindPerticulars();
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        private void BindCaseSummary(int Id)
        {
            try
            {
                if (Id != 0)
                {
                    var getcaseSummarydtls = LitigationManagement.getalldtlsofCaseSummary(Id, Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (getcaseSummarydtls != null)
                    {
                        ddlParticulars.SelectedValue = Convert.ToString(getcaseSummarydtls.PerticularsId);
                        ddlState.SelectedValue = Convert.ToString(getcaseSummarydtls.StateId);
                        //txtTotal.Text = Convert.ToString(getcaseSummarydtls.Total);
                        txtDisputedAmt.Text = Convert.ToString(getcaseSummarydtls.DisputedAmt);
                        txtInterest.Text = Convert.ToString(getcaseSummarydtls.Interest);
                        txtPenalty.Text = Convert.ToString(getcaseSummarydtls.Penalty);
                        txtAmountPaid.Text = Convert.ToString(getcaseSummarydtls.AmountPaid);
                        txtFavourable.Text = Convert.ToString(getcaseSummarydtls.Favourable);
                        txtUnfavourable.Text = Convert.ToString(getcaseSummarydtls.Unfavourable);
                        txtExpense.Text = Convert.ToString(getcaseSummarydtls.Expense);
                        string StateID = Convert.ToString(getcaseSummarydtls.StateId);
                        string DisputedAmount = Convert.ToString(getcaseSummarydtls.DisputedAmt);
                        string Interest = Convert.ToString(getcaseSummarydtls.Interest);
                        string Penalty = Convert.ToString(getcaseSummarydtls.Penalty);
                        string AmountPaid = Convert.ToString(getcaseSummarydtls.AmountPaid);
                        string Favourable = Convert.ToString(getcaseSummarydtls.Favourable);
                        string Unfavourable = Convert.ToString(getcaseSummarydtls.Unfavourable);
                        string Expense = Convert.ToString(getcaseSummarydtls.Expense);
                        ViewState["Message"] = "State,Distput,Interest,Penalty,AmtPaid,Fvble,Unfvble,Expense," +
                            StateID + "," + DisputedAmount + "," + Interest + "," + Penalty + "," + AmountPaid + "," +
                            Favourable + "," + Unfavourable + "," + Expense + " New Values- ";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindPerticulars()
        {
            try
            {
                ddlParticulars.DataTextField = "CaseType";
                ddlParticulars.DataValueField = "ID";

                ddlParticulars.DataSource = LitigationManagement.GetAllPerticularsbyCustomerId(Convert.ToInt32(CustomerID));
                ddlParticulars.DataBind();

                ddlParticulars.Items.Insert(0, new ListItem("Select", "-1"));
                //  ddlParticulars.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindState()
        {
            try
            {
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";

                ddlState.DataSource = LitigationManagement.getAllState();
                ddlState.DataBind();

                ddlState.Items.Insert(0, new ListItem("Select", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlParticulars.SelectedValue != "" && ddlParticulars.SelectedValue != "-1")
                {
                    if (txtDisputedAmt.Text == "")
                    {
                        txtDisputedAmt.Text = "0";
                    }
                    if (txtInterest.Text == "")
                    {
                        txtInterest.Text = "0";
                    }
                    if (txtPenalty.Text == "")
                    {
                        txtPenalty.Text = "0";
                    }
                    if (txtAmountPaid.Text == "")
                    {
                        txtAmountPaid.Text = "0";
                    }
                    if (txtFavourable.Text == "")
                    {
                        txtFavourable.Text = "0";
                    }

                    if (txtUnfavourable.Text == "")
                    {
                        txtUnfavourable.Text = "0";
                    }
                    if (txtExpense.Text == "")
                    {
                        txtExpense.Text = "0";
                    }
                    tbl_CaseSummary _objcasesummary = new tbl_CaseSummary()
                    {
                        CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID),
                        PerticularsId = Convert.ToInt32(ddlParticulars.SelectedValue),
                        StateId = Convert.ToInt32(ddlState.SelectedValue),
                        isActive = true,
                        CreatedOn = DateTime.Now,
                        Createdby = Convert.ToInt32(AuthenticationHelper.UserID),
                    };
                    if ((int)ViewState["Mode"] == 1)
                    {
                        _objcasesummary.Id = Convert.ToInt32(ViewState["CaseSummaryId"]);
                    }
                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (LitigationManagement.CheckCaseSummaryExist(_objcasesummary))
                        {
                            cvAddEditCaseSummary.IsValid = false;
                            cvAddEditCaseSummary.ErrorMessage = "Perticulars with Same State are already exists.";
                            ClearPage();
                        }
                        else
                        {
                            _objcasesummary.DisputedAmt = Convert.ToDecimal(txtDisputedAmt.Text);
                            _objcasesummary.Interest = Convert.ToDecimal(txtInterest.Text);
                            _objcasesummary.Penalty = Convert.ToDecimal(txtPenalty.Text);
                            _objcasesummary.Total = Convert.ToDecimal(txtDisputedAmt.Text) + Convert.ToDecimal(txtInterest.Text) + Convert.ToDecimal(txtPenalty.Text);
                            _objcasesummary.AmountPaid = Convert.ToDecimal(txtAmountPaid.Text);
                            _objcasesummary.Favourable = Convert.ToDecimal(txtFavourable.Text);
                            _objcasesummary.Unfavourable = Convert.ToDecimal(txtUnfavourable.Text);
                            _objcasesummary.Expense = Convert.ToDecimal(txtExpense.Text);
                            bool savecasesummery = LitigationManagement.saveCaseSummary(_objcasesummary);
                            if (savecasesummery)
                            {
                                cvAddEditCaseSummary.IsValid = false;
                                cvAddEditCaseSummary.ErrorMessage = "Perticulars Save Successfully.";
                                ClearPage();
                            }
                            else
                            {
                                cvAddEditCaseSummary.IsValid = false;
                                cvAddEditCaseSummary.ErrorMessage = "Something wents wrong.";
                            }
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        _objcasesummary.Id = Convert.ToInt32(ViewState["CaseSummaryId"]);
                        _objcasesummary.PerticularsId = Convert.ToInt32(ddlParticulars.SelectedValue);
                        _objcasesummary.StateId = Convert.ToInt32(ddlState.SelectedValue);
                        _objcasesummary.DisputedAmt = Convert.ToDecimal(txtDisputedAmt.Text);
                        _objcasesummary.Interest = Convert.ToDecimal(txtInterest.Text);
                        _objcasesummary.Penalty = Convert.ToDecimal(txtPenalty.Text);
                        _objcasesummary.Total = Convert.ToDecimal(txtPenalty.Text) + Convert.ToDecimal(txtInterest.Text) + Convert.ToDecimal(txtDisputedAmt.Text);
                        _objcasesummary.AmountPaid = Convert.ToDecimal(txtAmountPaid.Text);
                        _objcasesummary.Favourable = Convert.ToDecimal(txtFavourable.Text);
                        _objcasesummary.Unfavourable = Convert.ToDecimal(txtUnfavourable.Text);
                        _objcasesummary.Expense = Convert.ToDecimal(txtExpense.Text);
                        _objcasesummary.Updatedby = AuthenticationHelper.UserID;
                        _objcasesummary.UpdatedOn = DateTime.Now;
                        bool updateCaseSummary = LitigationManagement.UpdateCaseSummuryDetails(_objcasesummary);

                        string message = Convert.ToString(ViewState["Message"]) + ddlState.SelectedValue + "," + txtDisputedAmt.Text + "," +
                                         txtInterest.Text + "," + txtPenalty.Text + "," + txtAmountPaid.Text + "," +
                                        txtFavourable.Text + "," + txtUnfavourable.Text + "," + txtExpense.Text;

                        LitigationManagement.CreateAuditLog("M", 0, "tbl_CaseSummary", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, message, true);
                        if (updateCaseSummary)
                        {
                            cvAddEditCaseSummary.IsValid = false;
                            cvAddEditCaseSummary.ErrorMessage = "Perticulars Updated Successfully.";
                            ClearPage();
                        }
                        else
                        {
                            cvAddEditCaseSummary.IsValid = false;
                            cvAddEditCaseSummary.ErrorMessage = "Somthing wents Wrong";
                        }
                    }
                }
                else
                {
                    cvAddEditCaseSummary.IsValid = false;
                    cvAddEditCaseSummary.ErrorMessage = "Please Select Perticulars";

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ClearPage()
        {

            ddlParticulars.SelectedValue = "0";
            ddlState.SelectedValue = "0";
            txtDisputedAmt.Text = "";
            txtInterest.Text = "";
            txtPenalty.Text = "";

            txtAmountPaid.Text = "";
            txtFavourable.Text = "";
            txtUnfavourable.Text = "";
            txtExpense.Text = "";

        }
    }
}