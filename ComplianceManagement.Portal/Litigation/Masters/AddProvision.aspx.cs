﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddProvision : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["PId"]))
                    {
                        int ID = Convert.ToInt32(Request.QueryString["PId"]);
                        Provision Objcases = Provisionclass.GetProvision(ID);
                        tbxtitle.Text = Objcases.Title;
                        txtDetails.Text = Objcases.Details;
                        ViewState["Mode"] = 1;
                        ViewState["PId"] = ID;
                    }
                    else
                    {
                        ViewState["Mode"] = 0;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Provision objCase = new Provision()
                {
                    Title = tbxtitle.Text.Trim(),
                    Details = txtDetails.Text.Trim(),
                    CustomerId = (int)CustomerID,
                    UpdatedBy = AuthenticationHelper.UserID,
                    UpdatedDate = DateTime.Now
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    objCase.Id = Convert.ToInt32(ViewState["PId"]);
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    
                        objCase.CreatedBy = AuthenticationHelper.UserID;
                        objCase.CreatedDate = DateTime.Now;
                        Provisionclass.CreateProvision(objCase);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Provision Saved Successfully.";
                        
                    
                }

                else if ((int)ViewState["Mode"] == 1)
                {
                    objCase.UpdatedBy = AuthenticationHelper.UserID;
                    objCase.UpdatedDate = DateTime.Now;
                    Provisionclass.UpdateProvision(objCase);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Provision Updated Successfully.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateLocation.IsValid = false;
                cvDuplicateLocation.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseMe();", true);
        }
    }
}
