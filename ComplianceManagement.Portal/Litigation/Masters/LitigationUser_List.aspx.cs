﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Configuration;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class LitigationUser_List : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected bool flag;
        protected tbl_PageAuthorizationMaster authpage;
        protected int pageid = 3;
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.ProductApplicableLogin == "L")
                this.MasterPageFile = "~/LitigationMaster.Master";
            else
                MasterPageFile = "~/LitigationMaster.Master";

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            String key = "Authenticate" + AuthenticationHelper.UserID;
            if (Cache.Get(key) != null)
            {
                var Records = (List<tbl_PageAuthorizationMaster>) Cache.Get(key);
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.PageID == pageid
                                 select row).FirstOrDefault();
                    authpage = query;
                }
            }
            if (!IsPostBack)
            {
                flag = false;
                //Page
                BindCustomers(ddlCustomerPage);
                hideAddControls();
                //Popup
                BindCustomers(ddlCustomerPopup);
                BindRoles();
                BindDepartment();
                BingLawyerList();

                if (!string.IsNullOrEmpty(Request.QueryString["FirmID"]))
                {
                    lblBack.Visible = true;
                    ddlUserType.SelectedValue = "1";
                    ddlLawyerListPage.SelectedValue = Convert.ToString(Request.QueryString["FirmID"]);
                }
                BindUsers();
                Session["CurrentRole"] = AuthenticationHelper.Role;
                Session["CurrentUserId"] = AuthenticationHelper.UserID;
                
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    btnAddUser.Visible = false;
                }

                divddlLayerList.Visible = true;

                bindPageNumber();
            }
        }

        public void hideAddControls()
        {
            try
            {
                if (authpage != null)
                {
                    if (authpage.Addval == false)
                    {
                        btnAddUser.Visible = false;
                    }
                    else
                    {
                        btnAddUser.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }

                DropDownListPageNo.DataBind();

                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }


                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }
        private void BindCustomers(DropDownList ddlitoFill)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                ddlitoFill.DataTextField = "Name";
                ddlitoFill.DataValueField = "ID";

                ddlitoFill.DataSource = CustomerManagement.GetAll(customerID);
                ddlitoFill.DataBind();

                //ddlitoFill.Items.Insert(0, new ListItem("Select Customer", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (ddlCustomerPage.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerPage.SelectedValue);
                }

                var userList = LitigationUserManagement.GetAllUser(customerID, tbxFilter.Text);

                if (userList.Count > 0)
                {
                    if (ddlUserType.SelectedValue != "" && ddlUserType.SelectedValue != "-1")
                    {
                        if (ddlUserType.SelectedValue == "0")//Internal Users 
                        {
                            userList = userList.Where(entry => entry.LawyerFirmID == null && entry.IsExternal == false).ToList();
                            ddlLawyerListPage.Visible = false;
                        }
                        else if (ddlUserType.SelectedValue == "1")//Lawyer Users 
                        {
                            userList = userList.Where(entry => entry.LawyerFirmID != null && entry.IsExternal == false).ToList();
                            ddlLawyerListPage.Visible = true;

                            if (ddlLawyerListPage.SelectedValue != "" && ddlLawyerListPage.SelectedValue != "-1")
                            {
                                userList = userList.Where(entry => entry.LawyerFirmID == Convert.ToInt32(ddlLawyerListPage.SelectedValue)).ToList();
                            }
                        }
                        else if (ddlUserType.SelectedValue == "2")//External Users 
                            userList = userList.Where(entry => entry.IsExternal == true).ToList();
                    }                        
                }
                
                List<object> dataSource = new List<object>();

                foreach (View_DisplayUserWithRating user in userList)
                {
                    dataSource.Add(new
                    {
                        user.ID,
                        user.FirstName,
                        user.LastName,
                        user.Email,
                        user.ContactNumber,
                        ComRole = RoleManagement.GetByID(Convert.ToInt32(user.RoleID)).Name,
                        Role = user.LitigationRoleID != null ? RoleManagement.GetByID(Convert.ToInt32(user.LitigationRoleID)).Name : "",
                        user.IsActive,
                        user.Rating
                    });
                }
                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                flag = true;
                Session["TotalRows"] = null;
               
                if (dataSource.Count > 0)
                {
                    grdUser.DataSource = dataSource;
                    grdUser.DataBind();
                    Session["TotalRows"] = dataSource.Count;
                }
                else
                {
                    grdUser.DataSource = dataSource;
                    grdUser.DataBind();
                }
                upUserList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        public int ShowLawyerRating(decimal savedRating)
        {
            int returnRating = 0;
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(savedRating)))
                {
                    returnRating = Convert.ToInt32(savedRating);
                }
                return returnRating;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return returnRating;
            }
        }

        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomerPage.SelectedValue))
            {
                if (ddlCustomerPage.SelectedValue != "-1")
                {
                    AddNewUser(Convert.ToInt32(ddlCustomerPage.SelectedValue));
                }
                else
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    AddNewUser(customerID);
                }
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                //Reload the Grid
                BindUsers();

                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdUser.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdUser.PageIndex = chkSelectedPage - 1;

                //SelectedPageNo.Text = (chkSelectedPage).ToString();
                grdUser.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindUsers(); ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void ddlCustomerPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    if (ddlCustomerPage.SelectedItem.Text == "Select Customer")
                    {
                        btnAddUser.Visible = false;
                    }
                    else
                    {
                        btnAddUser.Visible = true;
                    }
                }

                BindUsers(); bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdUser_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                if (ddlCustomerPage.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerPage.SelectedValue);
                }

                var userList = LitigationUserManagement.GetAllUser(customerID, tbxFilter.Text);
                
                if (userList.Count > 0)
                {
                    if (ddlUserType.SelectedValue != "" && ddlUserType.SelectedValue != "-1")
                    {
                        if (ddlUserType.SelectedValue == "0")//Internal Users 
                        {
                            userList = userList.Where(entry => entry.LawyerFirmID == null && entry.IsExternal == false).ToList();
                            ddlLawyerListPage.Visible = false;
                        }
                        else if (ddlUserType.SelectedValue == "1")//Lawyer Users 
                        {
                            userList = userList.Where(entry => entry.LawyerFirmID != null && entry.IsExternal == false).ToList();
                            ddlLawyerListPage.Visible = true;

                            if (ddlLawyerListPage.SelectedValue != "" && ddlLawyerListPage.SelectedValue != "-1")
                            {
                                userList = userList.Where(entry => entry.LawyerFirmID == Convert.ToInt32(ddlLawyerListPage.SelectedValue)).ToList();
                            }
                        }
                        else if (ddlUserType.SelectedValue == "2")//External Users 
                            userList = userList.Where(entry => entry.IsExternal == true).ToList();
                    }
                }
                
                List<object> dataSource = new List<object>();

                foreach (View_DisplayUserWithRating user in userList)
                {
                    dataSource.Add(new
                    {
                        user.ID,
                        user.FirstName,
                        user.LastName,
                        user.Email,
                        user.ContactNumber,
                        ComRole = RoleManagement.GetByID(Convert.ToInt32(user.RoleID)).Name,
                        Role = user.LitigationRoleID != null ? RoleManagement.GetByID(Convert.ToInt32(user.LitigationRoleID)).Name : "",
                        user.IsActive,
                        user.Rating
                    });
                }

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdUser.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdUser.Columns.IndexOf(field);
                    }
                }
                Session["TotalRows"] = null;
                flag = true;
                if (dataSource.Count > 0)
                {
                    grdUser.DataSource = dataSource;
                    grdUser.DataBind();
                    Session["TotalRows"] = dataSource.Count;
                }
                else
                {
                    grdUser.DataSource = dataSource;
                    grdUser.DataBind();
                }
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        protected void grdUser_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int userID = 0;
                if (e.CommandName.Equals("EDIT_USER"))
                {
                    userID = Convert.ToInt32(e.CommandArgument);
                    EditUserInformation(userID);
                }
                else if (e.CommandName.Equals("DELETE_USER"))
                {
                    userID = Convert.ToInt32(e.CommandArgument);

                    if (UserManagement.HasCompliancesAssigned(userID))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deleted. One or more Compliances are assigned to user, please re-assign to other user.');", true);
                    }
                    else if (EventManagement.GetAllAssignedInstancesByUser(userID).Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "EventInform", "alert('Account can not be deleted. One or more Event are assigned to user, please re-assign to other user.');", true);
                    }
                    if (CaseManagement.CheckUserExistInLitigation(userID))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deleted. One or more Notice/Cases are assigned to user, please re-assign to other user.');", true);
                    }
                    else
                    {
                        UserManagement.Delete(userID);
                        UserManagementRisk.Delete(userID);
                    }
                    BindUsers(); bindPageNumber();
                }
                else if (e.CommandName.Equals("CHANGE_STATUS"))
                {
                    userID = Convert.ToInt32(e.CommandArgument);
                    if (UserManagement.IsActive(userID) && UserManagement.HasCompliancesAssigned(userID))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be De-activated. One or more Compliances are assigned to user, please re-assign to other user.');", true);
                    }
                    else
                    {
                        UserManagement.ToggleStatus(userID);
                        UserManagementRisk.ToggleStatus(userID);

                    }
                    BindUsers(); bindPageNumber();
                }
                else if (e.CommandName.Equals("RESET_PASSWORD"))
                {
                    userID = Convert.ToInt32(e.CommandArgument);
                    User user = UserManagement.GetByID(userID);
                    mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(userID);
                    string passwordText = Util.CreateRandomPassword(10);
                    user.Password = Util.CalculateAESHash(passwordText);
                    mstuser.Password = Util.CalculateAESHash(passwordText);

                    // added by sudarshan for comman notification
                    int customerID = -1;
                    string ReplyEmailAddressName = "";
                    if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                    {
                        ReplyEmailAddressName = "Avantis";
                    }
                    else
                    {
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                        ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                    }
                    string portalURL = string.Empty;
                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (Urloutput != null)
                    {
                        portalURL = Urloutput.URL;
                    }
                    else
                    {
                        portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                    }
                    string message = EmailNotations.SendPasswordResetNotificationEmail(user, passwordText, Convert.ToString(portalURL), ReplyEmailAddressName);

                    //string message = EmailNotations.SendPasswordResetNotificationEmail(user, passwordText, Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]), ReplyEmailAddressName);

                    bool result = UserManagement.ChangePassword(user);
                    bool result1 = UserManagementRisk.ChangePassword(mstuser);
                    if (result && result1)
                    {
                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<string>(new string[] { user.Email }), null, null, "AVACOM account password changed", message);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Password Reset Successfully.');", true);
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                        ValidationSummary2.CssClass = "alert alert-danger";
                    }
                }
                else if (e.CommandName.Equals("UNLOCK_USER"))
                {
                    userID = Convert.ToInt32(e.CommandArgument);
                    UserManagement.WrongAttemptCountUpdate(userID);
                    UserManagementRisk.WrongAttemptCountUpdate(userID);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "Unloack", "alert('User Account Unlocked Successfully.');", true);
                    BindUsers(); bindPageNumber();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected bool IsLocked(string emailID)
        {
            try
            {
                if (UserManagement.WrongAttemptCount(emailID.Trim()) >= 3)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
                return false;
            }
        }
        
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageIndex = 0;
                BindUsers(); bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }
        
        #region User Detail--Popup

        protected void Upload(object sender, EventArgs e)
        {
            if (UserImageUpload.HasFile)
            {
                string[] validFileTypes = { "bmp", "gif", "png", "jpg", "jpeg" };

                string ext = System.IO.Path.GetExtension(UserImageUpload.PostedFile.FileName);
                bool isValidFile = false;
                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidFile = true;
                        break;
                    }
                }
                if (!isValidFile)
                {
                    cvUserPopup.IsValid = false;
                    cvUserPopup.ErrorMessage = "Invalid File. Please upload a File with extension " + string.Join(",", validFileTypes);
                }
            }
        }

        protected void upUsersPopup_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tvBranches.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        private void BindDepartment()
        {
            try
            {
                if (ddlCustomerPopup.SelectedValue != "" && ddlCustomerPopup.SelectedValue != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    ddlDepartment.DataTextField = "Name";
                    ddlDepartment.DataValueField = "ID";
                    ddlDepartment.DataSource = CompDeptManagement.FillDepartment(customerID);
                    ddlDepartment.DataBind();
                    ddlDepartment.Items.Insert(0, new ListItem("Select Department", "-1"));
                    ddlDepartment.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        private void BindRoles()
        {
            try
            {
                var CheckRole = string.Empty;
                ddlLitigationRole.DataTextField = "Name";
                ddlLitigationRole.DataValueField = "ID";

                var roles = RoleManagement.GetLitigationRoles(true);
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN") && !entry.Code.Equals("IMPT")).ToList();
                }
                else if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("IMPT")).ToList();
                }
                else
                {
                    roles = roles.Where(entry => entry.Code.Equals("EXCT")).ToList();
                    CheckRole = "EXCT";
                }
                
                ddlLitigationRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlLitigationRole.DataBind();
                ddlLitigationRole.Items.Insert(0, new ListItem("Select User Role", "-1"));
                ddlLitigationRole.Enabled = true;
                //if (CheckRole == "EXCT")
                //{
                //    ddlLitigationRole.SelectedItem.Text = "Non - Admin";
                //    ddlLitigationRole.Enabled = false;
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }
        
        protected void ddlLitigationRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlLitigationRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }

                divReportingTo.Visible = divCustomer.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                divCustomerBranch.Visible = roleCode.Equals("EXCT");

                if (divCustomer.Visible && AuthenticationHelper.Role != "CADMN")
                {
                    //ddlCustomerPopup.SelectedValue = "-1";
                    ddlCustomerPopup_SelectedIndexChanged(null, null);
                }
                if (divCustomerBranch.Visible)
                {
                    ddlCustomerPopup_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tvBranches.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                }

                if (divReportingTo.Visible)
                {
                    BindReportingTo();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        protected void ddlCustomerPopup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = string.Empty;
                BindCustomerBranches();
                BindReportingTo();
                if (divCustomerBranch.Visible)
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tvBranches.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        private void BindReportingTo()
        {
            try
            {
                ddlReportingTo.DataTextField = "Name";
                ddlReportingTo.DataValueField = "ID";

                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlLitigationRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }

                ddlReportingTo.DataSource = UserManagement.GetAllByCustomerID(Convert.ToInt32(ddlCustomerPopup.SelectedValue), roleCode);
                ddlReportingTo.DataBind();

                ddlReportingTo.Items.Insert(0, new ListItem("Select Reporting to person", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "Select Branch";
                ScriptManager.RegisterStartupScript(this.upUsersPopup, this.upUsersPopup.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                tvBranches.Nodes.Clear();
                NameValueHierarchy branch = null;
                var branchs = CustomerBranchManagement.GetAllHierarchy(Convert.ToInt32(ddlCustomerPopup.SelectedValue));
                if (branchs.Count > 0)
                {
                    branch = branchs[0];
                }
                tbxBranch.Text = "Select Location";
                List<TreeNode> nodes = new List<TreeNode>();
                BindBranchesHierarchy(null, branch, nodes);
                foreach (TreeNode item in nodes)
                {
                    tvBranches.Nodes.Add(item);
                }

                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        private void BindParameters(List<UserParameterValueInfo> userParameterValues = null)
        {
            try
            {
                if (userParameterValues == null)
                {
                    userParameterValues = new List<UserParameterValueInfo>();
                    var userParameters = UserManagement.GetAllUserParameters();
                    userParameters.ForEach(entry =>
                    {
                        userParameterValues.Add(new UserParameterValueInfo()
                        {
                            UserID = -1,
                            ParameterID = entry.ID,
                            ValueID = -1,
                            Name = entry.Name,
                            DataType = (DataType) entry.DataType,
                            Length = (int) entry.Length,
                            Value = string.Empty
                        });
                    });
                }

                repParameters.DataSource = userParameterValues;
                repParameters.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        public void AddNewUser(int CustomerID)
        {
            try
            {
                ViewState["Mode"] = 0;
                tbxFirstName.Text = tbxLastName.Text = tbxDesignation.Text = tbxEmail.Text = tbxContactNo.Text = tbxAddress.Text = string.Empty;
                //tbxUsername.Enabled = true;
                tbxEmail.Enabled = true;
                ddlCustomerPopup.Enabled = true;

                ddlDepartment.ClearSelection();
                ddlLitigationRole.ClearSelection();

                ddlLitigationRole.Enabled = true;
                ddlLitigationRole.Attributes.Remove("disabled");
                ddlLitigationRole_SelectedIndexChanged(null, null);

                BindParameters();

                #region Previous Code
                //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                //{
                //    bool ab = false;
                //    int customerID = -1;
                //    if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                //    {
                //        customerID = CustomerID;
                //    }
                //    else
                //    {
                //        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                //    }

                //    bool ac = UserManagementRisk.CheckProductmapping(customerID, 1);
                //    if (ac == true)
                //    {
                //        divComplianceRole.Visible = true;
                //        ddlRole.Enabled = true;
                //    }
                //    else
                //    {
                //        divComplianceRole.Visible = false;
                //        ddlRole.Enabled = false;
                //    }
                //    bool a = UserManagementRisk.CheckProductmapping(customerID, 3);
                //    if (a == true)
                //    {
                //        divRiskRole.Visible = true;
                //        ddlRiskRole.Enabled = true;
                //    }
                //    else
                //    {
                //        ab = UserManagementRisk.CheckProductmapping(customerID, 4);
                //        if (ab == true)
                //        {
                //            divRiskRole.Visible = true;
                //            ddlRiskRole.Enabled = true;
                //        }
                //        else
                //        {
                //            divRiskRole.Visible = false;
                //            ddlRiskRole.Enabled = false;
                //        }
                //    }
                //    if (ac == false && a == false && ab == false)
                //    {
                //        divComplianceRole.Visible = true;
                //        ddlRole.Enabled = true;
                //    }
                //}
                #endregion

                upUsersPopup.Update();
                //ScriptManager.RegisterStartupScript(this.upUsersPopup, this.upUsersPopup.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        public void AddNewUser()
        {
            try
            {
                ViewState["Mode"] = 0;

                tbxFirstName.Text = tbxLastName.Text = tbxDesignation.Text = tbxEmail.Text = tbxContactNo.Text = tbxAddress.Text = string.Empty;
                //tbxUsername.Enabled = true;
                tbxEmail.Enabled = true;
                ddlCustomerPopup.Enabled = true;
                ddlDepartment.ClearSelection();

                ddlLitigationRole.ClearSelection();
                ddlLitigationRole.SelectedValue = "-1";
                ddlLitigationRole_SelectedIndexChanged(null, null);

                BindParameters();

                #region Previous Code

                //using (ComplianceDBEntities entities = new ComplianceDBEntities())
                //{
                //    bool ab = false;
                //    int customerID = -1;
                //    //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                //    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                //    bool ac = UserManagementRisk.CheckProductmapping(customerID, 1);
                //    if (ac == true)
                //    {
                //        divComplianceRole.Visible = true;
                //    }
                //    else
                //    {
                //        divComplianceRole.Visible = false;
                //    }
                //    bool a = UserManagementRisk.CheckProductmapping(customerID, 3);
                //    if (a == true)
                //    {
                //        divRiskRole.Visible = true;
                //    }
                //    else
                //    {
                //        ab = UserManagementRisk.CheckProductmapping(customerID, 4);
                //        if (ab == true)
                //        {
                //            divRiskRole.Visible = true;
                //        }
                //        else
                //        {
                //            divRiskRole.Visible = false;
                //        }
                //    }
                //    if (ac == false && a == false && ab == false)
                //    {
                //        divComplianceRole.Visible = true;
                //    }
                //    //bool a = UserManagementRisk.CheckProductmapping(customerID);
                //    //if (a == true)
                //    //{
                //    //    divRiskRole.Visible = true;
                //    //}
                //    //else
                //    //{
                //    //    divRiskRole.Visible = false;
                //    //}
                //}

                #endregion

                upUsersPopup.Update();

                //ScriptManager.RegisterStartupScript(this.upUsersPopup, this.upUsersPopup.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open');", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        public void EditUserInformation(int userID)
        {
            try
            {

                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox(0);", true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "disableCombobox", "disableCombobox();", true);
                ViewState["Mode"] = 1;
                ViewState["UserID"] = userID;

                //User user = UserManagement.GetByID(userID);
                var user = CaseManagement.GetUserWithSpecilization(userID);
                List<UserParameterValueInfo> userParameterValues = UserManagement.GetParameterValuesByUserID(userID);

                tbxFirstName.Text = user.FirstName;
                tbxLastName.Text = user.LastName;
                tbxDesignation.Text = user.Designation;
                tbxEmail.Text = user.Email;
                tbxContactNo.Text = user.ContactNumber;
                //tbxUsername.Text = user.Username;
                tbxAddress.Text = user.Address;
                tbxSpecilisation.Text = user.Specilisation;
                //tbxUsername.Enabled = false;
                //tbxEmail.Enabled = false;

                if (user.DepartmentID != null)
                {
                    ddlDepartment.ClearSelection();

                    if (ddlDepartment.Items.FindByValue(user.DepartmentID.ToString()) != null)
                        ddlDepartment.Items.FindByValue(user.DepartmentID.ToString()).Selected = true;
                }
                
                //ddlDepartment.SelectedValue = user.DepartmentID != null ? user.DepartmentID.ToString() : "-1";
                ddlLitigationRole.SelectedValue = user.LitigationRoleID != null ? user.LitigationRoleID.ToString() : "-1";

                //ddlRole.SelectedValue = user.RoleID.ToString();
                ddlLitigationRole_SelectedIndexChanged(null, null);

                //ddlLitigationRole.Enabled = false;
                //ddlLitigationRole.Attributes.Add("disabled", "disabled");

                if (user.IsHead == true)
                    chkHead.Checked = true;
                else
                    chkHead.Checked = false;

                if (divCustomer.Visible && user.CustomerID.HasValue)
                {
                    ddlCustomerPopup.SelectedValue = user.CustomerID.Value.ToString();
                    ddlCustomerPopup_SelectedIndexChanged(null, null);
                }

                if (user.LawyerFirmID != null)
                {
                    ddlLayerList.ClearSelection();

                    if (ddlLayerList.Items.FindByValue(user.LawyerFirmID.ToString()) != null)
                        ddlLayerList.Items.FindByValue(user.LawyerFirmID.ToString()).Selected = true;

                    divddlLayerList.Visible = true;
                    //rbCheckType.Items.FindByValue("External").Selected = true;
                }

                ddlReportingTo.SelectedValue = user.ReportingToID != null ? user.ReportingToID.ToString() : "-1";

                if (divCustomerBranch.Visible && user.CustomerBranchID.HasValue)
                {
                    Queue<TreeNode> queue = new Queue<TreeNode>();
                    foreach (TreeNode node in tvBranches.Nodes)
                    {
                        queue.Enqueue(node);
                    }
                    while (queue.Count > 0)
                    {
                        TreeNode node = queue.Dequeue();
                        if (node.Value == user.CustomerBranchID.Value.ToString())
                        {
                            node.Selected = true;
                            break;
                        }
                        else
                        {
                            foreach (TreeNode child in node.ChildNodes)
                            {
                                queue.Enqueue(child);
                            }
                        }
                    }
                    tvBranches_SelectedNodeChanged(null, null);
                }

                BindParameters(userParameterValues);
                upUsersPopup.Update();

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tvBranches.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);

                int? LaywerID = null;

                if (!string.IsNullOrEmpty(ddlLayerList.SelectedValue))
                {
                    if (ddlLayerList.SelectedValue != "-1")
                    {
                        LaywerID = Convert.ToInt32(ddlLayerList.SelectedValue);
                    }
                }

                #region Compliance User
                User user = new User()
                {
                    FirstName = tbxFirstName.Text.Trim(),
                    LastName = tbxLastName.Text.Trim(),
                    Designation = tbxDesignation.Text.Trim(),
                    Email = tbxEmail.Text.Trim(),
                    ContactNumber = tbxContactNo.Text.Trim(),
                    Address = tbxAddress.Text.Trim(),
                    //RoleID = getproductcOMPLIANCE,
                    DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                    LawyerFirmID = LaywerID,
                    CreatedFrom=2
                };

                if (chkHead.Checked)
                    user.IsHead = true;
                else
                    user.IsHead = false;

                if (divCustomer.Visible)
                {
                    user.CustomerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                }

                if (divCustomerBranch.Visible)
                {
                    user.CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                }

                if (ddlReportingTo.SelectedValue != "-1" && ddlReportingTo.SelectedValue != "")
                {
                    user.ReportingToID = Convert.ToInt64(ddlReportingTo.SelectedValue);
                }

                if (ddlLitigationRole.SelectedValue != "" && ddlLitigationRole.SelectedValue != "-1")
                {
                    user.LitigationRoleID = Convert.ToInt32(ddlLitigationRole.SelectedValue);
                }

                List<UserParameterValue> parameters = new List<UserParameterValue>();
                foreach (var item in repParameters.Items)
                {
                    RepeaterItem entry = item as RepeaterItem;
                    TextBox tbxValue = ((TextBox) entry.FindControl("tbxValue"));
                    HiddenField hdnEntityParameterID = ((HiddenField) entry.FindControl("hdnEntityParameterID"));
                    HiddenField hdnID = ((HiddenField) entry.FindControl("hdnID"));

                    parameters.Add(new UserParameterValue()
                    {
                        ID = Convert.ToInt32(hdnID.Value),
                        UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                        Value = tbxValue.Text
                    });
                }

                #endregion

                #region Audit User

                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                {
                    FirstName = tbxFirstName.Text.Trim(),
                    LastName = tbxLastName.Text.Trim(),
                    Designation = tbxDesignation.Text.Trim(),
                    Email = tbxEmail.Text.Trim(),
                    ContactNumber = tbxContactNo.Text.Trim(),
                    Address = tbxAddress.Text.Trim(),
                    //RoleID = getproductrisk,
                    DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                    LawyerFirmID = LaywerID
                };

                if (chkHead.Checked)
                    mstUser.IsHead = true;
                else
                    mstUser.IsHead = false;

                if (divCustomer.Visible)
                {
                    mstUser.CustomerID = Convert.ToInt32(ddlCustomerPopup.SelectedValue);
                }

                if (divCustomerBranch.Visible)
                {
                    mstUser.CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                }

                if (ddlLitigationRole.SelectedValue != "" && ddlLitigationRole.SelectedValue != "-1")
                {
                    mstUser.LitigationRoleID = Convert.ToInt32(ddlLitigationRole.SelectedValue);
                }

                List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk> parametersRisk = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk>();
                foreach (var item in repParameters.Items)
                {
                    RepeaterItem entry = item as RepeaterItem;
                    TextBox tbxValue = ((TextBox) entry.FindControl("tbxValue"));
                    HiddenField hdnEntityParameterID = ((HiddenField) entry.FindControl("hdnEntityParameterID"));
                    HiddenField hdnID = ((HiddenField) entry.FindControl("hdnID"));

                    parametersRisk.Add(new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk()
                    {
                        ID = Convert.ToInt32(hdnID.Value),
                        UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                        Value = tbxValue.Text
                    });
                }
                #endregion

                if (rbCheckType.SelectedValue == "Internal")
                {
                    user.IsExternal = false;
                    mstUser.IsExternal = false;
                }
                else if (rbCheckType.SelectedValue == "External")
                {
                    if (ddlLayerList.SelectedValue != "" && ddlLayerList.SelectedValue != "-1")
                    {
                        user.IsExternal = true;
                        mstUser.IsExternal = true;
                    }
                    else
                    {
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "Select Law Firm, if User Type is External";
                        return;
                    }
                }

                bool result = false;
                int resultValue = 0;
                if ((int) ViewState["Mode"] == 0)
                {
                    //Check other User with same email
                    bool emailExists;
                    UserManagement.Exists(user, out emailExists);
                    if (emailExists)
                    {
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "User with Same Email already Exists.";
                        ValidationSummary2.CssClass = "alert alert-danger";
                        return;
                    }

                    UserManagementRisk.Exists(mstUser, out emailExists);
                    if (emailExists)
                    {
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "User with Same Email already Exists.";
                        ValidationSummary2.CssClass = "alert alert-danger";
                        return;
                    }

                    string passwordText = Util.CreateRandomPassword(10);

                    user.CreatedBy = AuthenticationHelper.UserID;
                    user.CreatedByText = AuthenticationHelper.User;
                    user.Password = Util.CalculateAESHash(passwordText);
                    user.RoleID = Convert.ToInt32(user.LitigationRoleID);

                    string message = getEmailMessage(user, passwordText);

                    mstUser.CreatedBy = AuthenticationHelper.UserID;
                    mstUser.CreatedByText = AuthenticationHelper.User;
                    mstUser.Password = Util.CalculateAESHash(passwordText);
                    //mstUser.RoleID = Convert.ToInt32(mstUser.LitigationRoleID);
                    mstUser.RoleID = -1;

                    resultValue = UserManagement.CreateNew(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                    if (resultValue > 0)
                    {
                        var key = "Litiusers-" + Convert.ToString(user.CustomerID);
                        if (CacheHelper.Exists(key))
                        {
                            CacheHelper.Remove(key);
                        }
                        result = UserManagementRisk.Create(mstUser, parametersRisk, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                        if (result == false)
                        {
                            UserManagement.deleteUser(resultValue);
                            if (CacheHelper.Exists(key))
                            {
                                CacheHelper.Remove(key);
                            }
                        }
                        if (!string.IsNullOrEmpty(tbxSpecilisation.Text))
                        {
                            tbl_LawyerFinalRating objrating = new tbl_LawyerFinalRating()
                            {
                                LawyerID = resultValue,
                                IsActive=true,
                                Specilisation = tbxSpecilisation.Text,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedOn = DateTime.Now
                            };
                            if (CaseManagement.CheckUserSpecilazationExist(resultValue))
                            {
                                CaseManagement.UpdateFinalratingSpecilasation(objrating);
                            }
                            else
                            {
                                CaseManagement.AddFinalratingSpecilasation(objrating);
                            }
                        }
                    }

                }
                else if ((int) ViewState["Mode"] == 1)
                {
                    int userID = -1;
                    userID = Convert.ToInt32(ViewState["UserID"]);

                    user.ID = Convert.ToInt32(ViewState["UserID"]);
                    mstUser.ID = Convert.ToInt32(ViewState["UserID"]);

                    //Check other User with same email
                    bool emailExists;
                    UserManagement.Exists(user, out emailExists);

                    if (!string.IsNullOrEmpty(tbxSpecilisation.Text))
                    {
                        tbl_LawyerFinalRating objrating = new tbl_LawyerFinalRating()
                        {
                            LawyerID = userID,
                            IsActive = true,
                            Specilisation = tbxSpecilisation.Text,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now
                        };
                        if (CaseManagement.CheckUserSpecilazationExist(userID))
                        {
                            CaseManagement.UpdateFinalratingSpecilasation(objrating);
                        }
                        else
                        {
                            CaseManagement.AddFinalratingSpecilasation(objrating);
                        }
                    }

                    if (emailExists)
                    {
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "User with Same Email already Exists.";
                        ValidationSummary2.CssClass = "alert alert-danger";
                        return;
                    }

                    UserManagementRisk.Exists(mstUser, out emailExists);
                    if (emailExists)
                    {
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "User with Same Email already Exists.";
                        ValidationSummary2.CssClass = "alert alert-danger";
                        return;
                    }

                    //Get Existing Users AuditorID and Compliance-Audit Product RoleID
                    User complianceUser = UserManagement.GetByID(userID);
                    mst_User auditUser = UserManagementRisk.GetByID(userID);

                    if (complianceUser != null)
                    {
                        user.AuditorID = complianceUser.AuditorID;
                        user.RoleID = complianceUser.RoleID;
                    }

                    if (auditUser != null)
                    {
                        mstUser.AuditorID = auditUser.AuditorID;
                        mstUser.RoleID = auditUser.RoleID;
                    }



                    //result = UserManagement.Update(user, parameters);
                    //result = UserManagementRisk.Update(mstUser, parametersRisk);

                    result = LitigationUserManagement.UpdateUserLitigation(user, parameters);
                    result = LitigationUserManagement.UpdateAuditDBUserLitigation(mstUser, parametersRisk);

                    if (tbxEmail.Text.Trim() != complianceUser.Email && result)
                    {
                        string message = SendNotificationEmailChanged(user);
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Email ID Changed.", message);
                    }
                }
                else
                {
                    cvUserPopup.IsValid = false;
                    cvUserPopup.ErrorMessage = "Server Error Occured. Please try again.";
                    ValidationSummary2.CssClass = "alert alert-danger";
                }

                if (result)
                {
                    if (UserImageUpload.HasFile)
                    {
                        string fileName = user.ID + "-" + user.FirstName + " " + user.LastName + Path.GetExtension(UserImageUpload.PostedFile.FileName);
                        UserImageUpload.PostedFile.SaveAs(Server.MapPath("~/UserPhotos/") + fileName);

                        string filepath = "~/UserPhotos/" + fileName;

                        UserManagement.UpdateUserPhoto(Convert.ToInt32(user.ID), filepath, fileName);
                        UserManagementRisk.UpdateUserPhoto(Convert.ToInt32(mstUser.ID), filepath, fileName);
                    }

                    if (result)
                    {
                        var key = "Litiusers-" + Convert.ToString(user.CustomerID);
                        if (CacheHelper.Exists(key))
                        {
                            CacheHelper.Remove(key);
                        }
                        cvUserPopup.IsValid = false;
                        cvUserPopup.ErrorMessage = "Details Updated Successfully.";
                        vsUserPopup.CssClass = "alert alert-success";
                        upUsersPopup.Update();
                    }
                }

                //ScriptManager.RegisterStartupScript(this.upUsersPopup, this.upUsersPopup.GetType(), "CloseDialog", "$(\"#divUsersDialog\").dialog('close')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        private string getEmailMessage(User user, string passwordText)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string portalURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    portalURL = Urloutput.URL;
                }
                else
                {
                    portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                   .Replace("@Username", user.Email)
                                   .Replace("@User", username)
                                   .Replace("@PortalURL", Convert.ToString(portalURL))
                                   .Replace("@Password", passwordText)
                                   .Replace("@From", ReplyEmailAddressName)
                                   .Replace("@URL", Convert.ToString(portalURL));
                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@Password", passwordText)
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
                return null;
            }
        }

        private string SendNotificationEmailChanged(User user)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string portalURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    portalURL = Urloutput.URL;
                }
                else
                {
                    portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                                    .Replace("@Username", user.Email)
                                    .Replace("@User", username)
                                    .Replace("@PortalURL", Convert.ToString(portalURL))
                                    .Replace("@From", ReplyEmailAddressName)
                                    .Replace("@URL", Convert.ToString(portalURL));
                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUserPopup.IsValid = false;
                cvUserPopup.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
            return null;
        }

        #endregion
        
        private void BingLawyerList()
        {
            try
            {
                var AllData = LawyerManagement.GetLawyerListAll(CustomerID);
                var users = (from row in AllData
                             select new { ID = row.ID, Name = row.FirstName + " " + row.LastName }).OrderBy(entry => entry.Name).ToList<object>();


                ddlLayerList.DataTextField = "Name";
                ddlLayerList.DataValueField = "ID";
                ddlLayerList.DataSource = users;
                ddlLayerList.DataBind();

                ddlLayerList.Items.Insert(0, new ListItem("Select Law Firm", "-1"));

                ddlLawyerListPage.DataTextField = "Name";
                ddlLawyerListPage.DataValueField = "ID";
                ddlLawyerListPage.DataSource = users;
                ddlLawyerListPage.DataBind();

                ddlLawyerListPage.Items.Insert(0, new ListItem("Select Law Firm", "-1"));
                ddlLayerList.Items.Add(new System.Web.UI.WebControls.ListItem("Add New", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }
        protected void rbCheckType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbCheckType.SelectedValue == "Internal")
            {
                divddlLayerList.Visible = false;
            }
            else if (rbCheckType.SelectedValue == "External")
            {
                divddlLayerList.Visible = true;
            }
        }

        protected void lblBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Litigation/Masters/Lawyer_List.aspx", false);
        }
        
        protected void lnkApply_Click(object sender, EventArgs e)
        {
            BindUsers(); bindPageNumber();
        }

        protected void grdUser_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        protected void grdUser_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (authpage != null)
                    {
                        if (authpage.Deleteval == false)
                        {
                            LinkButton LinkButton2 = (LinkButton) e.Row.FindControl("lbtnDelete");
                            LinkButton2.Visible = false;
                        }
                        else
                        {
                            LinkButton LinkButton2 = (LinkButton) e.Row.FindControl("lbtnDelete");
                            LinkButton2.Visible = true;
                        }
                        if (authpage.Modify == false)
                        {
                            LinkButton LinkButton1 = (LinkButton) e.Row.FindControl("lbtnEdit");
                            LinkButton1.Visible = false;
                        }
                        else
                        {
                            LinkButton LinkButton1 = (LinkButton) e.Row.FindControl("lbtnEdit");
                            LinkButton1.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                ValidationSummary2.CssClass = "alert alert-danger";
            }
        }

        protected void lnkLawFirmBind_Click(object sender, EventArgs e)
        {
            BingLawyerList();
        }

        protected void lnkBtnDept_Click(object sender, EventArgs e)
        {
            BindDepartment();
        }

        protected void tbxFilter_TextChanged1(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageIndex = 0;
                BindUsers();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}