﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddRefNo.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.AddRefNo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <script type="text/javascript">

        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        $(document).ready(function () {
            BindControls();
        });

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=tbxHearingDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true
                    });
            });
        }

        function CloseMe() {
            window.parent.CloseRefNoPopup();
        }
    </script>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>
            <div id="AddCourtType">
                <asp:ScriptManager ID="LitigationAddRefNo" runat="server"></asp:ScriptManager>
                <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div>
                            <div style="margin-bottom: 7px">
                                <asp:ValidationSummary ID="vsAddRefNo" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                    ValidationGroup="AddRefNoValidationGroup" />
                                <asp:CustomValidator ID="cvAddRefNo" runat="server" EnableClientScript="False"
                                    ValidationGroup="AddRefNoValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                            </div>
                            <div style="margin-bottom: 7px; margin-top: 50px; align-content: center">
                                <label style="width: 2%; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                <label style="width: 38%; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                    Hearing Date</label>
                                <asp:TextBox runat="server" ID="tbxHearingDate" OnTextChanged="tbxHearingDate_TextChanged" AutoPostBack="true" CssClass="form-control" MaxLength="100" Width="35%" />
                            </div>
                            <div style="margin-bottom: 7px; margin-top: 50px; align-content: center">
                                <label style="width: 2%; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                <label style="width: 38%; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                    Hearing Ref No.</label>
                                <asp:TextBox runat="server" ID="tbxHearingRefNo" CssClass="form-control" Enabled="false" Width="50%" />
                            </div>
                            <div style="margin-bottom: 7px; text-align: center; margin-top: 10px">
                                <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                    ValidationGroup="AddRefNoValidationGroup" />
                                <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe()" />
                            </div>
                            <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                            </div>
                            <div class="clearfix" style="height: 50px">
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </form>
</body>
</html>
