﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddPartyDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.AddPartyDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="overflow: hidden;">
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<head runat="server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <link href="../../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <link href="../../NewCSS/litigation_custom_style.css" rel="stylesheet" />


    <script type="text/javascript">
       
        function ddlCityChange() {

            var selectedCityID = $("#ddlCity").find("option:selected").val();
            if (selectedCityID != null) {
                if (selectedCityID == "0") {
                    $("#lnkShowAddNewCityModal").show();
                }
                else {
                    $("#lnkShowAddNewCityModal").hide();
                }
            }
        }

        function scrollUpPage() {

            $("#divMainView").animate({
                scrollTop: 0
            },
                'slow');
        }

        function scrollUpPage() {
            $("#divMainView").animate({ scrollTop: 0 }, 'slow');
        }

        function OpenAddCityPopup() {
            $('#AddCityPopUp').modal('show');
            // $('#IframeCity').attr('src', "../../ContractProduct/Masters/VendorMaster.aspx");
        }
        function CloseMe() {
            window.parent.ClosePopParty();
        }
        function RefreshParent() {
           
            var checkPath = window.parent.location.href.toLowerCase();
            if (checkPath.toLowerCase().indexOf("casedetailpage.aspx") >= 0) {
                if (window.parent.location.href.toLowerCase().indexOf('casedetailpage.aspx') == -1)
                    window.parent.location.href = window.parent.location.href;
            }
            if (checkPath.toLowerCase().indexOf("noticedetailpage.aspx") >= 0) {
                if (window.parent.location.href.toLowerCase().indexOf('noticedetailpage.aspx') == -1)
                    window.parent.location.href = window.parent.location.href;
            }
        }
        $(function () {
            $("#tbxName").keypress(function (e) {
                var keyCode = e.keyCode || e.which;

                $("#lblError").html("");

                //Regex for Valid Characters i.e. Alphabets.
                 var regex = /^[a-zA-Z\s-, ]+$/;
               
                //Validate TextBox value against the Regex.
                var isValid = regex.test(String.fromCharCode(keyCode));
                if (!isValid) {
                    $("#lblError").html("Only alphabets allowed.");
                }

                return isValid;
            });
        });
        function scrollUp() {
            $('html, body').animate({ scrollTop: '0px' }, 800);
        }

        function scrollUpPage() {

            $("#divCustomerBranchesDialog").animate({
                scrollTop: 0
            },
                'slow');
        }
        $(document).ready(function () {
         
            function scrollUpPage() {
                $("#divCustomerBranchesDialog").animate({ scrollTop: 0 }, 'slow');
            }
        });
    </script>
    <style type="text/css">
        .chosen-container {
            width: 250px;
        }

        .ul {
            height: 150px;
        }
    </style>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
       <div id="divCustomerBranchesDialog" style="overflow-y: auto;">

            <asp:HiddenField ID="hdnSelectedCity" runat="server" />
            <asp:ScriptManager ID="LitigationAddDept" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upDepartment" style="margin-left: 12px;" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <div style="margin-bottom: 7px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="NewAddLaywerValidate" />
                            <asp:CustomValidator ID="cvCustomeRemark" runat="server" EnableClientScript="False"
                                ValidationGroup="NewAddLaywerValidate" Display="none" class="alert alert-block alert-danger fade in" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="NewAddLaywerValidate" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>
                        <div>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                    Opponent Type</label>
                                <asp:RadioButtonList ID="rbPartyType" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem class="radio-inline" Text="Individual" Value="Individual" Selected="True"></asp:ListItem>
                                    <asp:ListItem class="radio-inline" Text="Corporate" Value="Corporate"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>

                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                    Name</label>
                                <asp:TextBox runat="server" ID="tbxName" Style="width: 250px;" CssClass="form-control" MaxLength="100" />
                                <span id="lblError" style="color: red; margin-left: 113px;"></span>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty."
                                    ControlToValidate="tbxName" runat="server" ValidationGroup="NewAddLaywerValidate"
                                    Display="None" />
                            </div>

                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                    Country</label>
                                <asp:DropDownListChosen runat="server" ID="ddlCountry" class="form-control m-bot15" Style="width: 250px;" PlaceHolder="Country"
                                    OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="5" />
                            </div>

                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                    State</label>
                                <asp:DropDownListChosen runat="server" ID="ddlState" class="form-control m-bot15" Style="width: 250px;" PlaceHolder="State"
                                    OnSelectedIndexChanged="ddlState_SelectedIndexChanged" AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="5" />
                            </div>

                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                    City</label>
                                <asp:DropDownListChosen runat="server" ID="ddlCity" class="form-control m-bot15" Style="width: 250px;" PlaceHolder="City" onchange="ddlCityChange()"
                                    AllowSingleDeselect="false" DisableSearchThreshold="5" />

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please Select City or Select 'Not Applicable'"
                                        ControlToValidate="ddlCity" runat="server" ValidationGroup="CasePopUpValidationGroup"
                                        Display="None" />
                                
                            <div style="float: right; text-align: center; width: 5%; margin-top: 1%;">
                                    <img id="lnkShowAddNewCityModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenAddCityPopup()" alt="Add New Opponent" title="Add New Opponent" />

                                </div>
                            </div>


                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                    Address</label>
                                <asp:TextBox runat="server" ID="tbxAddress" Style="width: 250px;" MaxLength="200" CssClass="form-control" TextMode="MultiLine" />
                            </div>

                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                    Email</label>
                                <asp:TextBox runat="server" ID="tbxEmail" Style="width: 250px;" MaxLength="200" CssClass="form-control" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" runat="server"
                                    ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid email."
                                    ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                            </div>

                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                                    Contact No</label>
                                <asp:TextBox runat="server" ID="tbxContactNo" Style="width: 250px;" CssClass="form-control"
                                    MaxLength="32" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                                    ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid contact number."
                                    ControlToValidate="tbxContactNo" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="tbxContactNo" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                                    ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter only 10 digit contact number."
                                    ControlToValidate="tbxContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                            </div>

                            <div style="margin-bottom: 7px; text-align: center; margin-top: 10px;">
                                <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSave_Click" OnClientClick="scrollUpPage()"
                                    ValidationGroup="NewAddLaywerValidate" />
                                <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();RefreshParent();" />
                            </div>

                            <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 15px;">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 100px">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        </div>
        
        <div class="modal fade" id="AddCityPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: 90%; height: 40%">
                <div class="modal-content">
                    <div class="modal-header">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="modal-header-custom">
                            Add New City</label>
                    </div>
                    <div class="modal-body" style="width: 100%;">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="margin-bottom: 7px">
                                    <asp:ValidationSummary ID="cityValidationSummary" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="CityAddPopupValidationGroup" />
                                    <asp:CustomValidator ID="CityValidator" runat="server" EnableClientScript="False"
                                        ValidationGroup="CityAddPopupValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                </div>
                                <div style="margin-bottom: 7px; margin-top: 20px; align-content: center">
                                    <label style="width: 3%; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                    <label style="width: 27%; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                        City</label>
                                    <asp:TextBox runat="server" ID="tbxCity" CssClass="form-control" Style="width: 68%;" MaxLength="100" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="City can not be empty." ControlToValidate="tbxCity"
                                        runat="server" ValidationGroup="CityAddPopupValidationGroup" Display="None" />
                                </div>

                                <div style="margin-bottom: 7px; margin-left: 140px; margin-top: 10px">
                                    <div style="margin-bottom: 7px; text-align: center">

                                        <asp:LinkButton runat="server" ID="lnkRebindCity" Visible="false" OnClick="lnkRebindCity_Click"></asp:LinkButton>
                                        <asp:Button Text="Save" runat="server" ID="Button1" OnClick="lnkBtnCity_Click" CssClass="btn btn-primary" ValidationGroup="CityAddPopupValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancelCityPopUp" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMeCity();RefreshParent1();" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="lnkRebindCity" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>

    </form>
    
 <script type="text/javascript">
     function restoreSelectedCity() {
            debugger;

            var selectedStr = $('#<% =hdnSelectedCity.ClientID %>').val();

            //alert(selectedStr);

            $('[id*=ddlCity]').multiselect('rebuild');

            var value = selectedStr.split(",");
            for (var i = 0; i < value.length; i++) {
                $("#ddlCity option[value=" + value[i] + "]").prop('selected', true);
            }
          
            $('[id*=ddlCity]').multiselect('refresh');
        }
        function CloseMeCity() {
            debugger;
	        $('#tbxCity').val('');
	        $('#AddCityPopUp').modal('hide');
	        document.getElementById('<%=lnkRebindCity.ClientID %>').click();
            restoreSelectedCity();
        }
        
    </script>
</body>
</html>
