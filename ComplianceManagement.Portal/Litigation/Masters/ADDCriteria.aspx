﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ADDCriteria.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters.ADDCriteria" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="../../NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="../../NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="../../NewCSS/style.css" rel="stylesheet" />
    <link href="../../NewCSS/style-responsive.css" rel="stylesheet" />

       <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>


    <script type="text/javascript">
        function CloseMe() {
            window.parent.CloseCriteriaPopUp();
        }
        function RefreshParent() {
            window.parent.location.href = window.parent.location.href;
        }
        $(function () {
            $("#tbxCriteria").keypress(function (e) {
                var keyCode = e.keyCode || e.which;

                $("#lblError").html("");

                //Regex for Valid Characters i.e. Alphabets.
                var regex = /^[a-zA-Z\s-, ]+$/;

                //Validate TextBox value against the Regex.
                var isValid = regex.test(String.fromCharCode(keyCode));
                if (!isValid) {
                    $("#lblError").html("Only alphabets allowed.");
                }

                return isValid;
            });
        });


    </script>
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="LitigationCriteriaRating" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upPromotor" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <div style="margin-bottom: 20px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="PromotorValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateLocation" runat="server" EnableClientScript="False"
                                ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="PromotorValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                        </div>
                        <div style="margin-bottom: 7px; align-content: center;margin-left:52px">
                            <label style="width: 10px; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                            <label style="width: 50px; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                Criteria</label>
                            <asp:TextBox runat="server" ID="tbxCriteria" CssClass="form-control" Style="width: 250px;" MaxLength="100" />                     
                            <span id="lblError" style="color: red;margin-left: 60px;"></span> 
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Criteria can not be empty." ControlToValidate="tbxCriteria"
                                runat="server" ValidationGroup="PromotorValidationGroup" Display="None" />
                        </div>
                        <div style="margin-bottom: 7px; text-align: center; margin-top: 10px;margin-left:25px">
                            <asp:Button runat="server" ID="btnSave"  OnClick="btnSave_Click" CssClass="btn btn-primary" style="margin-right:10px"
                                ValidationGroup="PromotorValidationGroup" />
                             <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" 
                                    OnClientClick="CloseMe();" />
                        </div>
                        <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                            <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                        </div>
                        <div class="clearfix" style="height: 50px">
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
