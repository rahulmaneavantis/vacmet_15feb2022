﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddEntitiesAssignment : System.Web.UI.Page
    {
        protected static int IsForBranch;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUser();
                BindLocationFilter();
                tbxFilterLocation.Text = "Select Entity/Location";
                string flag = Convert.ToString(Request.QueryString["Flag"]);
                if(flag=="Save")
                {
                    ViewState["Mode"] = 0;
                }
                if (AuthenticationHelper.Role == "CADMN")
                {
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    IsForBranch = UserManagement.ExistBranchAssignment(customerID);
                }
            }
        

        }

        public void BindUser()
        {
            int customerID = -1;
            int complianceProductType = 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            if (AuthenticationHelper.Role == "CADMN")
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                complianceProductType = AuthenticationHelper.ComplianceProductType;
                //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
            }
            var users = UserManagement.GetAllManagmentLitigationUser(customerID, complianceProductType);
            

            ddlUser.DataValueField = "ID";
            ddlUser.DataTextField = "Name";
            ddlUser.DataSource = users;
            ddlUser.DataBind();

            ddlUser.Items.Insert(0, new ListItem("Select User", "-1"));

        }
        private void BindLocationFilter()
        {
            try
            {

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                var bracnhes = CustomerBranchManagement.GetAllHierarchy(customerID);

                //TreeNode node = new TreeNode("< All >", "-1");
                //node.Selected = true;
                //tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    BindBranchesHierarchy(node, item);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();

                tvFilterLocation.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnlocation_Click(object sender, EventArgs e)
        {
            try
            {
                //BindComplianceEntityInstances();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnClear1_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < this.tvFilterLocation.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvFilterLocation.Nodes[i]);
            }
        }

        protected void ChkBoxClear(TreeNode node)
        {

            if (node.Checked) // && node.ChildNodes.Count == 0 if (node.Checked)
            {
                node.Checked = false;
            }
            foreach (TreeNode tn in node.ChildNodes)
            {

                if (tn.Checked)//&& tn.ChildNodes.Count == 0)//  && tn.ChildNodes.Count == 0if (tn.Checked)              
                {
                    tn.Checked = false;
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        ChkBoxClear(tn.ChildNodes[i]);
                    }
                }
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int userID = -1;
            if (!string.IsNullOrEmpty(ddlUser.SelectedValue))
            {
                if (ddlUser.SelectedValue != "-1")
                {
                    userID = Convert.ToInt32(ddlUser.SelectedValue);
                }
            }

            Boolean chkSubIndustryFlag = false;
            foreach (TreeNode node in tvFilterLocation.CheckedNodes)
            {
                chkSubIndustryFlag = true;
                break;

            }
            if (chkSubIndustryFlag == false)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Please select at least one Location.";
            }
            else
            {

           
            if ((int)ViewState["Mode"] == 0)
            {
                foreach (TreeNode node in tvFilterLocation.CheckedNodes)
                {
                    int branchId1 = Convert.ToInt32(node.Value);

                    var data = SelectEntity(branchId1, userID);
                    if (!(data != null))
                    {
                        LitigationEntitiesAssignment objEntitiesAssignment = new LitigationEntitiesAssignment();
                        objEntitiesAssignment.UserID = userID;
                        objEntitiesAssignment.BranchID = Convert.ToInt32(node.Value);
                        objEntitiesAssignment.CreatedOn = DateTime.UtcNow;

                        Create(objEntitiesAssignment);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Entity assigned to user successfully.";
                        vsAddDocType.CssClass = "alert alert-success";
                        }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Entity already assigned to User.";
                        vsAddDocType.CssClass = "alert alert-success";
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
                    }

                }
            }
           
          }

        }

        public static void Create(LitigationEntitiesAssignment objEntitiesAssignment)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                entities.LitigationEntitiesAssignments.Add(objEntitiesAssignment);
                entities.SaveChanges();
            }
        }
        public static LitigationEntitiesAssignment SelectEntity(int branchId = -1, int userID =-1)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var EntitiesAssignmentData = (from row in entities.LitigationEntitiesAssignments
                                              where row.BranchID == branchId && row.UserID == userID 
                                              select row).FirstOrDefault();
                return EntitiesAssignmentData;
            }
        }
    }
}