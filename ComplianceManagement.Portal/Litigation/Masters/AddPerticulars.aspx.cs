﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddPerticulars : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["CaseTypeId"]))
                    {
                        int ID = Convert.ToInt32(Request.QueryString["CaseTypeId"]);
                        ViewState["Mode"] = 1;
                        ViewState["CaseID"] = ID;
                    }
                    else
                    {
                        ViewState["Mode"] = 0;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                tbl_Perticulars objper = new tbl_Perticulars()
                {
                    Perticulars = txtperticulars.Text.Trim(),
                    CustomerId = (int)CustomerID
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    objper.Id = Convert.ToInt32(ViewState["CaseID"]);
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    if (LitigationManagement.CheckPerticularsExist(objper))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Particulars  already exists.";
                    }
                    else
                    {
                        objper.CreatedBy = AuthenticationHelper.UserID;
                        objper.CreatedOn = DateTime.Now;
                        objper.isDelete = false;
                        LitigationManagement.CreatePerticularsDetails(objper);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Particulars Save Successfully.";
                        txtperticulars.Text = string.Empty;
                    }
                }

                else if ((int)ViewState["Mode"] == 1)
                {
                    objper.Updatedby = AuthenticationHelper.UserID;
                    objper.UpdatedOn = DateTime.Now;
                    LitigationManagement.UpdatePerticularsDetails(objper);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Perticulars Updated Successfully.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateLocation.IsValid = false;
                cvDuplicateLocation.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseMe();", true);
        }
    }
}