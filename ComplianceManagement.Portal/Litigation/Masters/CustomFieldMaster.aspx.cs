﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class CustomFieldMaster : System.Web.UI.Page
    {
        protected bool flag;
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected tbl_PageAuthorizationMaster authpage;
        protected int pageid = 8;
        protected void Page_Load(object sender, EventArgs e)
        {
            String key = "Authenticate" + AuthenticationHelper.UserID;
            if (Cache.Get(key) != null)
            {
                var Records = (List<tbl_PageAuthorizationMaster>) Cache.Get(key);
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.PageID == pageid
                                 select row).FirstOrDefault();
                    authpage = query;
                }
            }
            if (!IsPostBack)
            {
                flag = false;
                BindCaseCategoryType();
                BindData(); bindPageNumber();
                ViewState["Mode"] = 0;
                hideAddControls();
            }
           
        }

        public void hideAddControls()
        {
            try
            {
                if (authpage != null)
                {
                    if (authpage.Addval == false)
                    {
                        btnAdd.Visible = false;
                    }
                    else
                    {
                        btnAdd.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomFieldList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindData(); bindPageNumber(); ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCaseCategoryType()
        {
            try
            {
                var lstCaseCaseType = LitigationCourtAndCaseType.GetAllLegalCaseTypeData(CustomerID);

                ddlType.DataTextField = "CaseType";
                ddlType.DataValueField = "ID";

                ddlType.DataSource = lstCaseCaseType;
                ddlType.DataBind();

                ddlType.Items.Insert(0, new ListItem("All", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindData()
        {
            try
            {
                List<CustFieldAndType> CustFieldList = CaseManagement.BindCustomeFieldData(CustomerID);

                if (CustFieldList.Count > 0)
                    CustFieldList = CustFieldList.OrderBy(entry => entry.Lable).ToList();

                if (!string.IsNullOrEmpty(ddlType.SelectedValue))
                {
                    if (ddlType.SelectedValue != "0" && ddlType.SelectedValue != "-1")
                    {
                        CustFieldList = CustFieldList.Where(entry => entry.TypeID == Convert.ToInt32(ddlType.SelectedValue)).ToList();
                    }
                }

                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                {
                    CustFieldList = CustFieldList.Where(entry => entry.Lable.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) || entry.CaseType.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();
                }
                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            CustFieldList = CustFieldList.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            CustFieldList = CustFieldList.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                flag = true;
                Session["TotalRows"] = null;

                if (CustFieldList.Count > 0)
                {
                    grdCustomFieldList.DataSource = CustFieldList;
                    Session["TotalRows"] = CustFieldList.Count;
                    grdCustomFieldList.DataBind();
                }
                else
                {
                    grdCustomFieldList.DataSource = CustFieldList;
                    grdCustomFieldList.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            string ID = null;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenCustomFieldPopup('" + ID + "');", true);
            BindData(); bindPageNumber();
        }

        protected void grdCustomFieldList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                long customerID = AuthenticationHelper.CustomerID;
                if (e.CommandName.Equals("EDIT_Field"))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                    string lblValue = commandArgs[0];
                    int TypeID = Convert.ToInt32(commandArgs[1]);
                    int ID = Convert.ToInt32(commandArgs[2]);
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenCustomFieldPopup('" + lblValue + "','" + TypeID + "','" + ID + "');", true);
                }
                else if (e.CommandName.Equals("DELETE_Field"))
                {
                    int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                    int ID = Convert.ToInt32(commandArgs[0]);
                    int TypeID = Convert.ToInt32(commandArgs[1]);

                    if (CaseManagement.CheckCustomParameterAssignmentExist(ID))
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "javascript:alert('Custom Parameter can not be delete. One or more case/notice are assigned to this Custom Parameter');", true);
                    }
                    else
                    {
                        bool _objCriteriadel = CaseManagement.DeleteCustomFieldDyID(ID, TypeID, CustomerID);
                        if (_objCriteriadel == true)
                        {
                            BindData();
                            bindPageNumber();                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdCustomFieldList.PageIndex = chkSelectedPage - 1;
            grdCustomFieldList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindData(); ShowGridDetail();
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }

                DropDownListPageNo.DataBind();

                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }
                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            BindData(); bindPageNumber();
        }

        protected void grdCustomFieldList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                List<CustFieldAndType> CustFieldList = CaseManagement.BindCustomeFieldData(CustomerID);

                if (CustFieldList.Count > 0)
                    CustFieldList = CustFieldList.OrderBy(entry => entry.Lable).ToList();

                if (!string.IsNullOrEmpty(ddlType.SelectedValue))
                {
                    if (ddlType.SelectedValue != "0" && ddlType.SelectedValue != "-1")
                    {
                        CustFieldList = CustFieldList.Where(entry => entry.TypeID == Convert.ToInt32(ddlType.SelectedValue)).ToList();
                    }
                }

                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                {
                    CustFieldList = CustFieldList.Where(entry => entry.Lable.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) || entry.TypeID.ToString().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();
                }
                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    CustFieldList = CustFieldList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    CustFieldList = CustFieldList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdCustomFieldList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdCustomFieldList.Columns.IndexOf(field);
                    }
                }
                flag = true;
                Session["TotalRows"] = null;
                if (CustFieldList.Count > 0)
                {
                    grdCustomFieldList.DataSource = CustFieldList;
                    Session["TotalRows"] = CustFieldList.Count;
                    grdCustomFieldList.DataBind();
                }
                else
                {
                    grdCustomFieldList.DataSource = CustFieldList;
                    grdCustomFieldList.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCustomFieldList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdCustomFieldList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (authpage != null)
                {
                    if (authpage.Deleteval == false)
                    {
                        LinkButton LinkButton2 = (LinkButton) e.Row.FindControl("lblDelete");
                        LinkButton2.Visible = false;
                    }
                    else
                    {
                        LinkButton LinkButton2 = (LinkButton) e.Row.FindControl("lblDelete");
                        LinkButton2.Visible = true;
                    }
                    if (authpage.Modify == false)
                    {
                        LinkButton LinkButton1 = (LinkButton) e.Row.FindControl("lblEdit");
                        LinkButton1.Visible = false;
                    }
                    else
                    {
                        LinkButton LinkButton1 = (LinkButton) e.Row.FindControl("lblEdit");
                        LinkButton1.Visible = true;
                    }
                    if (authpage.Deleteval == false && authpage.Modify == false)
                    {
                        grdCustomFieldList.Columns[3].Visible = false;
                    }
                }
            }
        }

        protected void tbxtypeTofilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomFieldList.PageIndex = 0;
                BindData();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
               
            }
        }
    }
}