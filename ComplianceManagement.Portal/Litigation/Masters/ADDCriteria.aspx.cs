﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class ADDCriteria : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["CriteriaID"]))
                    {
                        //string LawyerID = Request.QueryString["LawyerId"];
                        btnSave.Text = "Update";
                        int ID = Convert.ToInt32(Request.QueryString["CriteriaID"]);
                        tbl_CriteriaRatingMaster _Objcriteria = LawyerManagement.GetCriteriaVal(ID, CustomerID);
                        tbxCriteria.Text = _Objcriteria.Name;
                        ViewState["Mode"] = 1;
                        ViewState["CriteriaID"] = ID;
                    }
                    else
                    {
                        btnSave.Text = "Save";
                        ViewState["Mode"] = 0;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                tbl_CriteriaRatingMaster _objcriteriarating = new tbl_CriteriaRatingMaster()
                {
                    Name = tbxCriteria.Text.Trim(),
                    CustomerID = (int)CustomerID
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    _objcriteriarating.ID = Convert.ToInt32(ViewState["CriteriaID"]);
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    if (LawyerManagement.CheckCriteriaExist(_objcriteriarating))
                    {
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Criteria already exists.";
                    }
                    else
                    {

                        _objcriteriarating.CreatedBy = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        _objcriteriarating.CreatedOn = DateTime.Now;
                        _objcriteriarating.IsActive = true;
                        LawyerManagement.CreateCriteriaDetails(_objcriteriarating);
                        cvDuplicateLocation.IsValid = false;
                        cvDuplicateLocation.ErrorMessage = "Criteria Saved Successfully.";
                        ValidationSummary1.CssClass = "alert alert-success";
                        tbxCriteria.Text = string.Empty;
                    }
                }

                else if ((int)ViewState["Mode"] == 1)
                {
                    _objcriteriarating.UpdatedBy = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    _objcriteriarating.UpdatedOn = DateTime.Now;
                    LawyerManagement.UpdateCriteriaDetails(_objcriteriarating);
                    cvDuplicateLocation.IsValid = false;
                    cvDuplicateLocation.ErrorMessage = "Criteria Updated Successfully.";
                    ValidationSummary1.CssClass = "alert alert-success";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateLocation.IsValid = false;
                cvDuplicateLocation.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseMe();RefreshParent();", true);
        }
    }
}