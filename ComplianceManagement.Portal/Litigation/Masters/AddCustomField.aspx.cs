﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Masters
{
    public partial class AddCustomField : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        public static List<int> TypeList = new List<int>();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindCaseCategoryType();
                    if (!string.IsNullOrEmpty(Request.QueryString["lblValue"]) && !string.IsNullOrEmpty(Request.QueryString["TypeID"]) && !string.IsNullOrEmpty(Request.QueryString["ID"]))
                    {
                        btnSave.Text = "Update";
                        string lblValue = Request.QueryString["lblValue"];
                        int TypeID = Convert.ToInt32(Request.QueryString["TypeID"]);
                        ViewState["Mode"] = "Edit";
                        BindData(lblValue, TypeID);
                    }
                    else
                    {
                        btnSave.Text = "Save";
                        ViewState["Mode"] = "Add";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindData(string lblValue, int TypeID)
        {
            try
            {
                if (ddlTypes.Items.Count > 0)
                {
                    ddlTypes.Items.FindByValue(TypeID.ToString()).Selected = true;
                    tbxLableName.Text = lblValue;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCaseCategoryType()
        {
            try
            {
                var lstCaseCaseType = LitigationCourtAndCaseType.GetAllLegalCaseTypeData(CustomerID);

                ddlTypes.DataTextField = "CaseType";
                ddlTypes.DataValueField = "ID";

                ddlTypes.DataSource = lstCaseCaseType;
                ddlTypes.DataBind();

               // ddlTypes.Items.Add(new ListItem("Add New", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string CheckStatus = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Mode"])))
                {
                    CheckStatus = Convert.ToString(ViewState["Mode"]);
                }
                if (tbxLableName.Text != "")
                {
                    tbl_CustomField NewFieldAdd = new tbl_CustomField()
                    {
                        Label = tbxLableName.Text.Trim(),
                        CreatedBy = AuthenticationHelper.UserID,
                        IsActive = true,
                        CreatedOn = DateTime.Now,
                        CustomerID=AuthenticationHelper.CustomerID
                    };

                    TypeList.Clear();
                    for (int i = 0; i < ddlTypes.Items.Count; i++)
                    {
                        if (ddlTypes.Items[i].Selected)
                        {
                            TypeList.Add(Convert.ToInt32(ddlTypes.Items[i].Value));
                        }
                    }

                    if (TypeList.Count > 0)
                    {
                        foreach (var TypeID in TypeList)
                        {
                            NewFieldAdd.CaseNoticeCategory = TypeID;
                            if (CheckStatus == "Add")
                            {
                                if (CaseManagement.IsExistCustomeField(NewFieldAdd))
                                {
                                    CvCustValiation.IsValid = false;
                                    CvCustValiation.ErrorMessage = "Custome Field with same name already exists.";
                                    return;
                                }
                                else
                                {
                                    CaseManagement.SaveCustomeFieldData(NewFieldAdd);
                                    CvCustValiation.IsValid = false;
                                    CvCustValiation.ErrorMessage = "Custome Field Saved Successfully.";
                                    ValidationSummary1.CssClass = "alert alert-success";
                                    ViewState["Mode"] = "Add";
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(Request.QueryString["ID"]))
                                {
                                    NewFieldAdd.ID = Convert.ToInt32(Request.QueryString["ID"]);
                                    NewFieldAdd.UpdatedBy = AuthenticationHelper.UserID;
                                    CaseManagement.UpdateCustomeFieldData(NewFieldAdd);
                                    CvCustValiation.IsValid = false;
                                    CvCustValiation.ErrorMessage = "Custome Field Updated Successfully.";
                                    ValidationSummary1.CssClass = "alert alert-success";
                                    ViewState["Mode"] = "Add";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CvCustValiation.IsValid = false;
                CvCustValiation.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    }
}