﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class AdvocateBillApprover : System.Web.UI.Page
    {
        protected static string Path;
        protected static string Authorization;
        protected static int CustId;
        protected static int UserId;
        protected static string EmailID;
        protected static string CustomerName;
        protected static string ApproverType;
    
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
                string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
                Authorization = (string)HttpContext.Current.Cache[CacheName];
                if (Authorization == null)
                {
                    Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                    HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                }
                Path = ConfigurationManager.AppSettings["KendoPathApp"];
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                EmailID = UserManagement.GetEmailByUserID(Convert.ToInt32(AuthenticationHelper.UserID));
                CustomerName = GetCustomerName(CustId);

                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    ApproverType = (from row in entities.tbl_Advcatebillapprovers
                                    where row.IsDeleted == false
                                    && row.ApproverEmail.Trim().ToLower() == EmailID.Trim().ToLower()
                                    && row.CustomerID == CustId
                                    select row.ApproverLevel).FirstOrDefault();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

          
        }
        public static string GetCustomerName(int CID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                string CName = (from row in entities.CustomerViews
                                where row.ID == CID
                                select row.Name).Single();

                return CName;
            }
        }

    }
}