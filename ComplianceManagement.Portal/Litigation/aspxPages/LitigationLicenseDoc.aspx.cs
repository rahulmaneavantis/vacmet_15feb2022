﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class LitigationLicenseDoc : System.Web.UI.Page
    {
        
        protected bool flag;
        protected static bool AuthoriseViewDownloadDoc;
        protected static List<Litigation_SP_GetSharingDocDetail_Result> DocList;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AuthoriseViewDownloadDoc = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "AuthoriseViewDownloadDoc");
                //DocList = VendorDetails.GetDocSharing_All(Convert.ToInt64(Request.QueryString["CID"]), tbxFilter.Text.Trim().ToString());

                DocList = LitigationTaskManagement.GetDocSharing_All(Convert.ToInt64(Request.QueryString["CID"]), Request.QueryString["PageName"]);

                DocList = DocList.Where(x => x.docexpired == false).ToList();
                DocList = DocList.Where(x => x.AssignedDocUID == Convert.ToInt64(Request.QueryString["UID"])).ToList();
                if (!string.IsNullOrEmpty(Request.QueryString["CID"])
                    && !string.IsNullOrEmpty(Request.QueryString["UID"]))
                {
                    long CustID = Convert.ToInt64(Request.QueryString["CID"]);
                    long UID = Convert.ToInt64(Request.QueryString["UID"]);
                    flag = false;
                    BindsharedContractDocument(CustID, UID);
                    bindPageNumber();
                }
            }
        }

        protected void lnkApply_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["CID"])
                   && !string.IsNullOrEmpty(Request.QueryString["UID"]))
            {
                long CustID = Convert.ToInt64(Request.QueryString["CID"]);
                long UID = Convert.ToInt64(Request.QueryString["UID"]);
                BindsharedContractDocument(CustID, UID);
                bindPageNumber();
            }
        }

        protected bool CanViewAndDownloadDoc(long FileID, long ContractID, string Type)
        {
            try
            {
                bool result = true;
                if (AuthoriseViewDownloadDoc)
                {
                    if (Type == "Download")
                    {
                        var DocListResult = DocList.Where(x => x.AssignedDocUID == ContractID && x.FileID == FileID && x.Permission == 1).FirstOrDefault();
                        if (DocListResult != null)
                        {
                            if (DocListResult != null)
                            {
                                result = true;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                    else
                    {
                        result = true;
                    }
                }
                else
                {
                    result = true;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }

        private void BindsharedContractDocument(long CustID, long uid)
        {
            try
            {

                List<Litigation_SP_GetSharingDocDetail_Result> lstContDocs = new List<Litigation_SP_GetSharingDocDetail_Result>();
                lstContDocs = LitigationTaskManagement.GetDocSharing_All(Convert.ToInt64(Request.QueryString["CID"]), Request.QueryString["PageName"]);
                lstContDocs = lstContDocs.Where(x => x.docexpired == false).ToList();
                lstContDocs = lstContDocs.Where(x => x.AssignedDocUID == uid).ToList();
                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            lstContDocs = lstContDocs.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            lstContDocs = lstContDocs.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                flag = true;
                Session["TotalRows"] = null;
                if (lstContDocs.Count > 0)
                {
                    grdContDoctType.DataSource = lstContDocs;
                    Session["TotalRows"] = lstContDocs.Count;
                    grdContDoctType.DataBind();
                }
                else
                {
                    grdContDoctType.DataSource = lstContDocs;
                    grdContDoctType.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPageDocType.IsValid = false;
                cvPageDocType.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CID"])
                 && !string.IsNullOrEmpty(Request.QueryString["UID"]))
                {
                    long CustID = Convert.ToInt64(Request.QueryString["CID"]);
                    long UID = Convert.ToInt64(Request.QueryString["UID"]);
                    grdContDoctType.PageIndex = 0;
                    BindsharedContractDocument(CustID, UID);
                    bindPageNumber();
                    SetShowingRecords();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPageDocType.IsValid = false;
                cvPageDocType.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void lnkBtn_RebindGrid_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["CID"])
                && !string.IsNullOrEmpty(Request.QueryString["UID"]))
            {
                long CustID = Convert.ToInt64(Request.QueryString["CID"]);
                long UID = Convert.ToInt64(Request.QueryString["UID"]);
                BindsharedContractDocument(CustID, UID);
                bindPageNumber();
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CID"])
               && !string.IsNullOrEmpty(Request.QueryString["UID"]))
                {
                    long CustID = Convert.ToInt64(Request.QueryString["CID"]);
                    long UID = Convert.ToInt64(Request.QueryString["UID"]);
                    grdContDoctType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    BindsharedContractDocument(CustID, UID);
                    bindPageNumber();

                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdContDoctType.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }

                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private void SetShowingRecords()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                int totalPages = 0;
                if (Session["TotalRows"] != null)
                {
                    TotalRows.Value = Session["TotalRows"].ToString();

                    totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                    // total page item to be displayed
                    int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                    // remaing no of pages
                    if (pageItemRemain > 0)// set total No of pages
                    {
                        totalPages = totalPages + 1;
                    }
                    else
                    {
                        totalPages = totalPages + 0;
                    }
                    // return totalPages;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["CID"])
             && !string.IsNullOrEmpty(Request.QueryString["UID"]))
            {
                long CustID = Convert.ToInt64(Request.QueryString["CID"]);
                long UID = Convert.ToInt64(Request.QueryString["UID"]);
                int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                grdContDoctType.PageIndex = chkSelectedPage - 1;
                grdContDoctType.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindsharedContractDocument(CustID, UID); SetShowingRecords();
            }
        }


        protected void grdContDoctType_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CID"])
            && !string.IsNullOrEmpty(Request.QueryString["UID"]))
                {
                    long CustID = Convert.ToInt64(Request.QueryString["CID"]);
                    long UID = Convert.ToInt64(Request.QueryString["UID"]);

                    if (e.CommandName.Equals("DownloadContDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long fileID = Convert.ToInt64(commandArgs[0]);
                        long contractID = Convert.ToInt64(commandArgs[1]);

                        bool downloadSuccess = DownloadContractDocument(Convert.ToInt64(fileID));

                        if (!downloadSuccess)
                        {
                            //cvContractDocument.IsValid = false;
                            //cvContractDocument.ErrorMessage = "Sorry!No File To Download";
                        }
                    }
                    else if (e.CommandName.Equals("EDIT_ContDocShare"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long fileID = Convert.ToInt64(commandArgs[0]);
                        long contractID = Convert.ToInt64(commandArgs[1]);

                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "showDocInfo", "OpenDocSharePopup('" + fileID + "','" + contractID + "');", true);

                        var AllinOneDocumentList = ContractDocumentManagement.GetLitigationLicenseDocumentByID(Convert.ToInt64(fileID));

                        if (AllinOneDocumentList != null)
                        {
                            string DocumentPath = string.Empty;

                            string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                            if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                            {
                                string fileExtension = System.IO.Path.GetExtension(filePath);

                                if (fileExtension.ToUpper() == ".ZIP" || fileExtension.ToUpper() == ".7Z" || fileExtension.ToUpper() == ".RAR")
                                {
                                    //set alert
                                    //lblMessage.Text = "";
                                    //lblMessage.Text = "Compressed file(s) can not be preview, Please try to download file(s)";
                                }
                                else
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                    string DateFolder = Folder + "/" + File;
                                    string extension = System.IO.Path.GetExtension(filePath);
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }
                                    string customerID = Convert.ToString(CustID);
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                    string FileName = DateFolder + "/" + User + "" + extension;
                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (AllinOneDocumentList.EnType == "M")
                                    {
                                        bw.Write(BM_ManegmentServices.Services.Masters.CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(BM_ManegmentServices.Services.Masters.CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    DocumentPath = FileName;
                                    DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                }

                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocSharePopup('" + DocumentPath + "');", true);
                            }
                            else
                            {
                                //set alert
                                //lblMessage.Text = "There is no file to preview";
                                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvPageDocType.IsValid = false;
                cvPageDocType.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public bool DownloadContractDocument(long contFileID)
        {
            bool downloadSuccess = false;
            try
            {
                var file = ContractDocumentManagement.GetLitigationLicenseDocumentByID(contFileID);

                if (file != null)
                {
                    if (file.FilePath != null)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                        if (filePath != null && File.Exists(filePath))
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(file.FileName));
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(BM_ManegmentServices.Services.Masters.CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(BM_ManegmentServices.Services.Masters.CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            ContractManagement.CreateAuditLog("CT", Convert.ToInt32(ViewState["ContractInstanceID"]), "Cont_tbl_FileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Document Downloaded", true, Convert.ToInt32(ViewState["ContractInstanceID"]));

                            downloadSuccess = true;
                        }
                    }
                }

                return downloadSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return downloadSuccess;
            }
        }


        protected void grdContDoctType_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                long customerid = Convert.ToInt64(Request.QueryString["CID"]);
                long uid = Convert.ToInt64(Request.QueryString["UID"]);

                List<Litigation_SP_GetSharingDocDetail_Result> lstContDocs = new List<Litigation_SP_GetSharingDocDetail_Result>();

                lstContDocs = LitigationTaskManagement.GetDocSharing_All(Convert.ToInt64(Request.QueryString["CID"]), Request.QueryString["PageName"]);
                lstContDocs = lstContDocs.Where(x => x.docexpired == false).ToList();
                lstContDocs = lstContDocs.Where(x => x.AssignedDocUID == uid).ToList();
                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    lstContDocs = lstContDocs.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    lstContDocs = lstContDocs.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdContDoctType.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdContDoctType.Columns.IndexOf(field);
                    }
                }
                flag = true;
                grdContDoctType.DataSource = lstContDocs;
                grdContDoctType.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdContDoctType_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }


        protected void grdContDoctType_RowDataBound(object sendser, GridViewRowEventArgs e)
        {

        }
    }

}