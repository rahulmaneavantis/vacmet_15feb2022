﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" MaintainScrollPositionOnPostback="true" CodeBehind="CaseDetailPage.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.CaseDetailPage" %>

<!DOCTYPE html>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="DropDownListChosen" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Case Detail</title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <link href="~/NewCSS/litigation_custom_style.css" rel="stylesheet" />
    <script src="../../Newjs/tag-scrolling.js" type="text/javascript"></script>
    <link href="../../NewCSS/tag-scrolling.css" rel="stylesheet" />

    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <%--  <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>--%>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <%--    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>--%>

    <style type="text/css">
        .k-panelbar > .k-item > .k-link, .k-panelbar > .k-item > .k-link .k-icon, .k-panelbar > .k-item > .k-link:hover {
            background-color: #EEEEEE;
            color: black;
            /*border: 1px solid #EEEEEE;*/
            box-shadow: 0 0 #EEEEEE;
            margin-bottom: -1px;
        }

        .componentWraper {
            margin: 18px;
            position: relative;
            border: 1.2px solid #bdbdbd;
            border-radius: 12px;
            padding: 20px;
            margin-top: 8px;
        }

            .componentWraper .componentTitle {
                position: absolute;
                top: -10px;
                background: #fff;
                padding: 0 10px;
                font-weight: 600;
                margin-top: -2px;
                font-size: 17px;
                color: #78909c;
            }


        .k-panelbar-collapse.k-icon, .k-panelbar-expand.k-icon {
            position: absolute;
            top: 50%;
            right: 12px;
            margin-top: -8px;
            margin-right: 6px;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
        }

        .k-textbox .k-icon {
            top: 50%;
            margin: -7px 5px 0px;
            position: absolute;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }


        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            display: block;
            margin-right: 0px;
        }

        .k-grid-content {
            min-height: 30px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-grid td {
            line-height: 2em;
            border-bottom-width: 1px;
            /* background-color: white; */
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        /*.k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }*/

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
            height: auto;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        /*.k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }*/

        .k-filter-row th, .k-grid-header th.k-header {
            background: #E9EAEA;
            font-weight: bold;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            vertical-align: middle;
            white-space: pre-line;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: none;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        /*label {
            display: inline-block;
            margin-bottom: 0px;
        }*/

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
            margin: 5px;
        }

        .k-grid table {
            width: 100.5%;
        }

        .moving {
            -webkit-animation: spin 2s infinite linear;
            animation: spin 2s infinite linear;
        }

        .k-widget > span.k-invalid,
        input.k-invalid {
            border: 1px solid red !important;
        }

        .k-widget > span.k-invalid,
        textarea.k-invalid {
            border: 1px solid red !important;
        }

        .dropdown-validation-error {
            border: 1px solid red !important;
        }

        hr {
            margin-top: 0px;
            margin-bottom: 0px;
            border-top: 1px solid #f7f7f7;
        }

        b.caret {
            float: right;
            margin-top: 12px;
        }

        input#txtCaseDate {
            height: 32px;
        }
        .modal-header-custom {
            width: 229px;
        }
        em {
            color: red;
        }
    </style>
 <%--   <% if (NewColumnsLitigation == false)
        {%>
    <style type="text/css">
        em {
            color: white;
        }
    </style>
    <%} %>
    <%else if (NewColumnsLitigation == true)
        {%>
    <style type="text/css">
        em {
            color: red;
        }
    </style>
    <%} %>--%>

    <script type="text/javascript">



        var clcorder = true;
        function NumberOnly() {
            var AsciiValue = event.keyCode
            if ((AsciiValue >= 48 && AsciiValue <= 57))
                event.returnValue = true;
            else
                event.returnValue = false;
        }
        $(function () {
       
            $('[id*=lstBoxOppositionLawyer]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '60%',
                enableCaseInsensitiveFiltering: true
            });

            $('[id*=ddlLawyerTypes]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '64%',
                enableCaseInsensitiveFiltering: true
            });

            $('[id*=lstBoxPerformer]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '60%',
                enableCaseInsensitiveFiltering: true
            });

            $('[id*=ddlParty]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '100%',
                enableCaseInsensitiveFiltering: true
            });

            $('[id*=ddlAct]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '100%',
                enableCaseInsensitiveFiltering: true
            });
            $('[id*=DropDownListChosen1]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '100%',
                enableCaseInsensitiveFiltering: true
            });

            $('[id*=lstBoxLawyerUser]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '60%',
                enableCaseInsensitiveFiltering: true
            });
            $('[id*=lstAdvHearingRefNo]').multiselect({
                //includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '66%',
                enableCaseInsensitiveFiltering: true
            });
            $('[id*=ListBoxLawyerHearing]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '66%',
                enableCaseInsensitiveFiltering: true
            });

        });


        function OpenAddFirmLawyerPopup() {
            $('#AddOpposiLawyerPopUp').modal('show');
            $('#IFrameOppoLawyer').attr('src', "../../Litigation/Masters/AddLawyer.aspx");
        }


        function OpenDoumentPopup(NoticeInstanceID) {
            $('#AddDocumentList').modal('show');
            $('#IFrameManageDocument').attr('src', "../../Litigation/aspxPages/UploadLitigationDocuments.aspx?NoticeCaseID=" + NoticeInstanceID + "&Flag=" + "Cases");
        }

        function OpenAddOppostitionLawyerPopup() {
            $('#AddOpposiLawyerPopUp').modal('show');
            $('#IFrameOppoLawyer').attr('src', "../../Litigation/Masters/AddOppositionLawyer.aspx");
        }

        function OpenLawFirmPopupModel() {
            $('#AddLawFirmModelPopup').modal('show');
            $('#IFLawFirm').attr('src', "../../Litigation/Masters/AddLawFirm.aspx");
        }
        function fopendocfileReview(file) {

            $('#DocumentReviewPopUp1').modal('show');
            $('#CaseDocViewFrame').attr('src', "../../docviewer.aspx?docurl=" + file);
        }
        function fopenAdvBilldocfileReview(file) {

            $('#DocumentAdvBillReviewPopUp1').modal('show');
            $('#CaseAdvBillDocViewFrame').attr('src', "../../docviewer.aspx?docurl=" + file);
        }
        function OpenAdvBillDocviewer(file) {
            $('#DocumentAdvBillReviewPopUp1').modal('show');
            $('#CaseAdvBillDocViewFrame').attr('src', "../../docviewer.aspx?docurl=" + file);
        }
        function fopendocfileReviewNew(file) {

            $('#PaymentDocumentReviewPopUp1').modal('show');
            $('#PaymentCaseDocViewFrame').attr('src', "../../docviewer.aspx?docurl=" + file);
        }
        function OpenDocviewerNew(file) {
            $('#PaymentDocumentReviewPopUp1').modal('show');
            $('#PaymentCaseDocViewFrame').attr('src', "../../docviewer.aspx?docurl=" + file);
        }
        function OpposiLawerChangeAddButton() {
            var selectedPartyID = $("#lstBoxOppositionLawyer").find("option:selected").text();
            var strAddNew = "Add New";
            if (selectedPartyID != null) {
                if (selectedPartyID.indexOf(strAddNew) != -1) {
                    $("#lnkShowAddNewOppoLawyerModal").show();
                }
                else {
                    $("#lnkShowAddNewOppoLawyerModal").hide();
                }
            }
        }

        function FirmLawyerChangeAddButton() {
            var selectedPartyID = $("#lstBoxLawyerUser").find("option:selected").text();
            if (selectedPartyID != null) {
                if (selectedPartyID == "Add New") {
                    $("#lnkShowAddNewLawyerModal").show();
                }
                else {
                    $("#lnkShowAddNewLawyerModal").hide();
                }
            }
        }

        function rebindLawFirm() {
            $('#divComplianceDocumentShowDialog').modal('hide');
        }

        function ClosePopNoticeDetialPage() {
            $('#divComplianceDocumentShowDialog').modal('hide');
        }

        function OpenDocviewer(file) {
            $('#DocumentReviewPopUp1').modal('show');
            $('#CaseDocViewFrame').attr('src', "../../docviewer.aspx?docurl=" + file);
        }
        $(document).ready(function () {
            $("#panelbarSC").kendoPanelBar({
                expandMode: "single",
            });
            $("#panelbarHC").kendoPanelBar({
                expandMode: "single",
            });
            $("#panelbarDC").kendoPanelBar({
                expandMode: "single",
            });
            BindControls();
            ShowLawFirmAddbutton();

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);

            ddlTabHearingRefNoChange();
            ddlRefNoChange();
            ddlStatusChange();
            rblImpactChange();


            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });

            $('#DocumentReviewPopUp1').modal('hide');

            imgExpandCollapse();

            applyCSSToCaseDate();
            applyCSSToNoticeDate();
            applyCSSToInvoiceDateDate();
            if (clcorder == false) {
                HidShowOrderDivForEdit();
                HidShowAdvocateBillDivForEdit();
            }

            if (document.getElementById("ValidationSummary1") != null && document.getElementById("ValidationSummary1") != undefined) {
                if (document.getElementById("ValidationSummary1").style.display !== 'none') {
                    HidShowHearingDivForEdit();
                }
            }
            if (document.getElementById("ValidationSummary13") != null && document.getElementById("ValidationSummary13") != undefined) {
                if (document.getElementById("ValidationSummary13").style.display !== 'none') {
                    HidShowAdvocateBillDivForEdit();
                }
            }
        });

        function rebindLawyerUser() {
            $('[id*=lstBoxLawyerUser]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '60%'
            });
        }

        function rebindHearingLawyer() {
            $('[id*=ListBoxLawyerHearing]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '60%'
            });
        }


        function rebindAdvocateHearingRefNo() {


            $('[id*=lstAdvHearingRefNo]').multiselect({
                //includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '66%',
                enableCaseInsensitiveFiltering: true
            });
        }
        //Task
        function HidShowTaskDivForEdit() {
            $("#CollapsDivTaskFirstPanel").addClass('hide');
            $("#CollapsDivTaskSecondPanel").collapse('show');
        }

        function HidShowTaskDiv() {
            $("#CollapsDivTaskFirstPanel").addClass('hide');
            $("#CollapsDivTaskSecondPanel").collapse('show');

            $('#tbxTaskTitle').val('');
            $('#tbxTaskDueDate').val('');
            $('#tbxTaskHearingDate').val('');
            $('#tbxTaskDesc').val('');
            $('#tbxTaskRemark').val('');
            $('#tbxExpOutcome').val('');
            $('#ddlTaskLawyerListInternal').val(0);
            $('#ddlTaskPriority').val(0);
            $('#ddlTaskUserExternal').val(0);
            $('#ddlHearingRefNo').val(0);
        }

        function ShowTaskDiv() {
            $("#CollapsDivTaskFirstPanel").removeClass('hide');
            $("#CollapsDivTaskFirstPanel").collapse('show');
        }
        //Hearing
        function HidShowHearingDiv() {

            $("#CollapsDivHearingFirstPanel").addClass('hide');
            $("#collapseDivHearingLogsSecond").collapse('show');
        }
        //  tbxRemindMeOn

        function HidShowHearingDivForEdit() {
            $("#CollapsDivHearingFirstPanel").addClass('hide');
            $("#collapseDivHearingLogsSecond").collapse('show');
        }

        function ShowHearingDiv() {
            $("#CollapsDivHearingFirstPanel").removeClass('hide');
            $("#CollapsDivHearingFirstPanel").collapse('show');
        }
        //Order
        function HidShowOrderDiv() {
            $("#CollapsDivOrderFirstPanel").addClass('hide');
            $("#collapseDivOrderLogs").collapse('show');

            $('#tbxOrderDate').val('');
            $('#tbxOrderTitle').val('');
            $('#tbxOrderDesc').val('');
            $('#tbxOrderRemark').val('');
        }

        function HidShowOrderDivForEdit() {
            clcorder = false;
            $("#collapseDivOrderLogs").show()

        }

        function ShowOrderDiv() {
            $("#CollapsDivOrderFirstPanel").removeClass('hide');
            $("#CollapsDivOrderFirstPanel").collapse('show');
        }
        <%-------Added by renuka---%>
        //Advcate Bill
        function HidShowAdvocateBillDiv() {


            $('[id*=lstAdvHearingRefNo]').multiselect({
                //includeSelectAllOption: true,
                numberDisplayed: 1,
                buttonWidth: '66%',
                enableCaseInsensitiveFiltering: true
            });


            $("#CollapsDivAdvocateBillFirstPanel").addClass('hide');
            $("#collapseDivAdvocateBillLogs").collapse('show');

            $('#tbxAdvInvoiceno').val('');
            $('#tbxAdvInvoiceAmount').val('');
            $('#tbxAdvRemark').val('');
            //$('#tbxAdvocateBillHearingDate').val('');
        }
        function HidShowAdvocateBillDivForEdit() {
            clcorder = false;
            $("#collapseDivAdvocateBillLogs").collapse('show');
        }

        function ShowAdvocateBillDiv() {
            $("#CollapsDivAdvocateBillFirstPanel").removeClass('hide');
            $("#CollapsDivAdvocateBillFirstPanel").collapse('show');
        }

         <%-------End by renuka---%>


        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=txtCaseDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });

            $(function () {
                $('input[id*=txtNoticeDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });
            $(function () {
                $('input[id*=tbxinvoicedate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });

            $(function () {
                $('input[id*=tbxResponseDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });

            $(function () {
                $('input[id*=tbxDueOn]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });

            $(function () {
                $('input[id*=tbxCaseCloseDate]').datepicker(
                    {
                        dateFormat: 'mm-dd-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });

            $(function () {
                $('input[id*=tbxTabHearingDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        // minDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });

            $(function () {
                $('input[id*=tbxTaskHearingDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        minDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });

            $(function () {
                $('input[id*=tbxTaskDueDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        minDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });

            $(function () {
                $('input[id*=tbxPaymentDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });

            $(function () {
                $('input[id*=tbxReminderDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        minDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });

            $(function () {
                $('input[id*=tbxRemindMeOn]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        minDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });
            $(function () {
                $('input[id*=tbxOrderDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });


            $(function () {
                $('input[id*=tbxAdvocateBillHearingDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        minDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                        yearRange: '1950:2025',
                    });
            });
        }

        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);

        $("html").mouseover(function () {
            $("html").getNiceScroll().resize();
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        function hideDivBranch() {
            $('#divBranches').hide("blind", null, 500, function () { });
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $('.btn-minimize').click(function () {
            var s1 = $(this).find('i');
            if ($(this).hasClass('collapsed')) {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            } else {
                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            }
        });

        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }

        function OpenCaseLinkingPopup() {
            $('#divLinkCasePopup').modal('show');
            unCheckAll();
        }

        function scrollUpPage() {

            $("#divMainView").animate({
                scrollTop: 0
            },
                'slow');
        }

        function OpenSendMailPopup() {
            $('#divOpenSendMailPopup').modal('show');
        }

        function openCaseModal() {
            $('#divAddCaseModal').modal('show');
        }

        function openAddNewActModal() {
            $('#divAddNewActModal').modal('show');
        }

        function ShowDocumentDialog(CaseInstanceID, casetype) {
            $('#divComplianceDocumentShowDialog').modal('show');
            $('.modal-dialog').css('width', '100%');
            $('#IframeComplianceDocument').attr('width', '100%');
            $('#IframeComplianceDocument').attr('height', '475px');
            $('#IframeComplianceDocument').attr('background', '#fff;');
            $('#IframeComplianceDocument').attr('src', "../aspxPages/LitigationComplianceDocument.aspx?caseinstanceid=" + CaseInstanceID + "&Type=" + casetype);
        };

        function OpenDepartmentPopup(a) {
            $('#AddDepartmentPopUp').modal('show');
            $('#IframeDepartment').attr('src', "../../Litigation/Masters/AddDepartMent.aspx?DepartmentID=" + a);
        }

        function OpenAddActPopup() {
            $('#AddActPopUp').modal('show');
            $('#IframeAct').attr('src', "../../Litigation/Masters/AddAct.aspx");
        }

        function OpenPartyDetailsPopup() {
            $('#AddPartyPopUp').modal('show');
            $('#IframeParty').attr('src', "../../Litigation/Masters/AddPartyDetails.aspx");
        }

        function OpenCategoryTypePopup() {
            $('#AddCategoryType').modal('show');
            $('#IframeCategoryType').attr('src', "../../Litigation/Masters/AddCaseType.aspx?CaseTypeId=");
        }

        function OpenCourtPopup() {
            var a = '';
            $('#AddCourtsPopUp').modal('show');
            $('#IframeCourt').attr('src', "../../Litigation/Masters/AddCourtMaster.aspx?CourtID=" + a);
        }

        function OpenCaseNoticeHistoryPopup(NoticeCaseInstanceID, NoticeCaseType, HistoryFlag, Type) {
            if (Type == 'H')
                $('#historyPopUpHeader').text('Case History');
            else if (Type == 'L')
                $('#historyPopUpHeader').text('Linked Case Details');

            $('#divNoticeCaseHistoryPopup').modal('show');
            if (NoticeCaseType == '1') {
                $('#IFrameCaseHistory').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=" + NoticeCaseInstanceID + "&HistoryFlag=" + HistoryFlag);
            }
            else if (NoticeCaseType == '2') {
                $('#IFrameCaseHistory').attr('src', "../../Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=" + NoticeCaseInstanceID + "&HistoryFlag=" + HistoryFlag);
            }
        }

        function OpenRefNoPopup(InsID) {
            $('#AddCourtsPopUp').modal('show');
            $('#IframeCourt').attr('src', "../../Litigation/Masters/AddRefNo.aspx?InsID=" + InsID);
        }

        function CloseRefNoPopup() {
            $('#AddUserPopUp').modal('hide');
        }

        function CloseCaseDetailPage() {
            window.parent.ClosePopCaseDetialPage();
        }

        function OpenAddUserDetailPop() {
            $('#AddUserPopUp').modal('show');
            $('#IframeAddUser').attr('src', "../../Litigation/Masters/AddUser.aspx");
        }

        function gridPageIndexChanged() {
            imgExpandCollapse();
        }

        function imgExpandCollapse() {

            $("[src*=collapse]").on('click', function () {
                $(this).attr("src", "/Images/add.png");
                $(this).closest("tr").next().remove();
            });

            $("[src*=add]").on('click', function () {
                if ($(this).attr('src').indexOf('add.png') > -1) {
                    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
                    $(this).attr("src", "/Images/collapse.png");
                    imgExpandhide();
                } else if ($(this).attr('src').indexOf('collapse.png') > -1) {
                    $(this).attr("src", "/Images/add.png");
                    $(this).closest("tr").next().remove();
                    imgExpandhide();
                }
            });
        }

        function imgExpandhide() {
            $("[src*=hide]").on('click', function () {
                $(this).attr("src", "/Images/expand.png");
                $(this).closest("tr").next().remove();
            });

            $("[src*=expand]").on('click', function () {
                if ($(this).attr('src').indexOf('expand.png') > -1) {
                    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
                    $(this).attr("src", "/Images/hide.png");
                } else if ($(this).attr('src').indexOf('hide.png') > -1) {
                    $(this).attr("src", "/Images/expand.png");
                    $(this).closest("tr").next().remove();
                }
            });
        }

        function ChangeRowColor(rowID) {
            var color = document.getElementById(rowID).style.backgroundColor;
            var oldColor = document.getElementById(rowID).style.backgroundColor;

            if (color != 'rgb(247, 247, 247)')
                document.getElementById("hiddenColor").style.backgroundColor = color;

            if (color == 'rgb(247, 247, 247)')
                document.getElementById(rowID).style.backgroundColor = document.getElementById("hiddenColor").style.backgroundColor;
            else
                document.getElementById(rowID).style.backgroundColor = 'rgb(247, 247, 247)';
        }

        $("[id*=chkAllDocument]").click(function () {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function () {
                if (chkHeader.is(":checked")) {
                    $(this).attr("checked", "checked");
                } else {
                    $(this).removeAttr("checked");
                }
            });
        });
        //Add criteria Popup
        function OpenCriteriaRetingPopUp(LawyerID) {
            $('#AddLayerRatingCriteriaShowDialog').modal('show');
            $('#IframeLayerRatingCriteria').attr('src', "../../Litigation/Masters/ADDCriteria.aspx?LawyerId=" + LawyerID);
        }
    </script>
    <%--Added by Ruchi--%>
    <style>
        ul.multiselect-container.dropdown-menu {
            width: 100%;
            height: 150px;
            overflow-x: hidden;
        }

        .input-group-addon, .input-group-btn {
            width: 6%;
        }

        .scrolling-wrapper {
            overflow-x: auto;
            overflow-y: hidden;
            white-space: nowrap;
        }
    </style>
    <style type="text/css">
        .bootstrap-tagsinput .tag [data-role="remove"]:after {
            content: "";
            padding: 0px 2px;
        }

        .bootstrap-tagsinput {
        }

        .form-group {
            margin-bottom: 10px;
        }
    </style>
    <style>
        .tag .label {
            font-size: 100%;
        }

        span input[type=checkbox]:checked {
            color: #007aff;
            border: 1px solid;
            border-color: #007aff;
            background: none;
        }

        .label-info, .label-info-selected {
            font-size: 100%;
        }

            .label-info:active, .label-info:focus, .label-info:hover {
                color: #007aff;
                border: 1px solid;
                border-color: #007aff;
                background: 0 0;
            }

        .label-info-selected {
            color: #007aff;
            border: 1px solid;
            border-color: #007aff;
            background: 0 0;
        }

        textarea {
            resize: none;
            font-size: 13px;
            padding: 10px;
            height: 38px;
            min-height: 38px;
            max-height: 150px;
            width: 100%;
            box-sizing: border-box;
            overflow-y: auto;
        }

        span.label.label-info > label {
            color: #fff;
            font-weight: 100;
        }

        .chosen-container.chosen-with-drop .chosen-drop {
            left: 0;
        }

        .input-group-btn > .btn {
            position: relative;
            margin-left: -370px;
        }

        .form-control {
            width: 100%;
            margin-right: 84px;
        }
        /*.col-lg-12 {
    width: 135%;
}*/
        .table tbody > tr > td, .table tbody > tr > th, .table tfoot > tr > td, .table tfoot > tr > th, .table thead > tr > td, .table thead > tr > th {
            padding: 6px;
        }
    </style>

    <script>


        var view = $("#tslshow");
        var move = "100px";
        var sliderLimit = -750;


        $("#rightArrow").click(function () {
            // alert("right");
            var currentPosition = parseInt(view.css("left"));
            if (currentPosition >= sliderLimit) view.stop(false, true).animate({ left: "-=" + move }, { duration: 400 })

        });

        $("#leftArrow").click(function () {
            // alert("left");
            var currentPosition = parseInt(view.css("left"));
            if (currentPosition < 0) view.stop(false, true).animate({ left: "+=" + move }, { duration: 400 })

        });

    </script>
    <%--Added by Ruchi--%>


    <script type="text/javascript">

        function OpenDocSharePopup(FID, CID, CaseNo, Title) {
            $('#divDocumentsharePopup').modal('show');
            
            var codeTitle  = encodeURIComponent(Title);
            $('#Iframe_Docshare').attr('src', "/Litigation/Common/ShareDocsLitigation.aspx?AccessID=" + FID + "&CID=" + CID + "&UniqueNumber=" + CaseNo + "&Page=Litigation" + "&CaseTitle=" + codeTitle + "&MailHeader=For Case");
        }

        function CloseUploadShareDocumentPopup()
        {
            $('#divDocumentsharePopup').modal('hide');
        }
         
        function checkUncheckRowField() {

            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdCaseDocuments.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

        }

        function checkAllField(chkHeader) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdCaseDocuments.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }
            }
        }

        function showHideAuditLog(divID, iID) {
            if ($(iID).attr('class').indexOf('fa fa-plus') > -1) {
                $(iID).attr("class", "fa fa-minus");
                $(divID).collapse('toggle');
            } else if ($(iID).attr('class').indexOf('fa fa-minus') > -1) {
                $(iID).attr("class", "fa fa-plus");
                $(divID).collapse('toggle');
            }
        }
        jQuery(window).load(function () {
            $('#updateProgress').hide();
        });

        $(document).ready(function () {
            $("input").attr("autocomplete", "off");
            $('[data-toggle="popover"]').popover();

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);

            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
                $('[data-toggle="popover"]').popover();
            });

            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });

            $("html").getNiceScroll().resize();

            $('#updateProgress').hide();

            $('i.glyphicon.glyphicon-search').removeClass('glyphicon glyphicon-search').addClass('fa fa-search color-black');
            $('i.glyphicon glyphicon-remove-circle').removeClass('glyphicon glyphicon-remove-circle').addClass(' fa fa-remove');

            $('input[id*=lstBoxFileTags]').hide();

            $('textarea').on('input', function () {

                if (this.scrollHeight <= 150)
                    $(this).outerHeight(38).outerHeight(this.scrollHeight);

                if (this.scrollHeight >= 150)
                    $(this).outerHeight(150);
            });

        });

        function scrollUp() {
            $('html, body').animate({ scrollTop: '0px' }, 800);
        }

        function scrollDown() {
            $('html, body').animate({ scrollTop: $elem.height() }, 800);
        }

        function scrollUpPage() {
            $("#divMainView").animate({ scrollTop: 0 }, 'slow');
        }

        function hide(object) {
            if (object != null)
                object.style.display = "none";
        }

        function show(object) {
            if (object != null)
                object.style.display = "block";
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $('.btn-minimize').click(function () {
            var s1 = $(this).find('i');
            if ($(this).hasClass('collapsed')) {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            } else {
                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            }
        });

        function btnminimize(obj) {
            var s1 = $(obj).find('i');
            if ($(obj).hasClass('collapsed')) {

                $(s1).removeClass('fa-chevron-up');
                $(s1).addClass('fa-chevron-down');
            } else {
                $(s1).removeClass('fa-chevron-down');
                $(s1).addClass('fa-chevron-up');
            }
        }

        function bindMultiSelect() {

            $('[id*=lstBoxTaskUser]').multiselect({
                includeSelectAllOption: true,
                numberDisplayed: 2,
                buttonWidth: '100%',
                enableCaseInsensitiveFiltering: true,
                filterPlaceholder: 'Type to Search for User..',
                nSelectedText: ' - User(s) selected',
            });
        }

        var InvalidFilesTypes = ["exe", "bat", "dll", "css", "js", "jsp",
            "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp"];

        function CheckValidation() {
            var fuSampleFile = $("#<%=fuTaskDocUpload.ClientID%>").get(0).files;
            var isValidFile = true;
            $("#Labelmsg").css('display', 'none');
            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (InvalidFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
                else if (fuSampleFile[i].size == 0) {
                    isValidFile = false;
                    break;
                }
            }
            if (!isValidFile) {
                $("#Labelmsg").css('display', 'block');
                $('#Labelmsg').text("Invalid file error. System does not support uploaded file.Please upload another file..");
            }
            return isValidFile;
        }
        function CheckValidationForRes() {
            $("#lblValidResForDoc").css('display', 'none');
            var fuSampleFile = $("#<%=fuResponseDocUpload.ClientID%>").get(0).files;
            var isValidFile = true;
            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (InvalidFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
                else if (fuSampleFile[i].size == 0) {
                    isValidFile = false;
                    break;
                }
            }
            if (!isValidFile) {
                $("#lblValidResForDoc").css('display', 'block');
                $('#lblValidResForDoc').text("Invalid file error. System does not support uploaded file.Please upload another file..");
            }
            return isValidFile;
        }

        function CheckValidationForOrder() {
            var fuSampleFile = $("#<%=fuCaseOrderDocUpload.ClientID%>").get(0).files;
            var isValidFile = true;
            $("#lblOrderDoc").css('display', 'none');
            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (InvalidFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
                else if (fuSampleFile[i].size == 0) {
                    isValidFile = false;
                    break;
                }
            }
            if (!isValidFile) {
                $("#lblOrderDoc").css('display', 'block');
                $('#lblOrderDoc').text("Invalid file error. System does not support uploaded file.Please upload another file..");
            }
            return isValidFile;
        }

        function CheckValidationForAdvocateBill() {
            var fuSampleFile = $("#<%=fuAdvocateBillDocUpload.ClientID%>").get(0).files;
            var isValidFile = true;
            $("#Label2").css('display', 'none');
            for (var i = 0; i < fuSampleFile.length; i++) {
                var fileExtension = fuSampleFile[i].name.split('.').pop();
                if (InvalidFilesTypes.indexOf(fileExtension) != -1) {
                    isValidFile = false;
                    break;
                }
                else if (fuSampleFile[i].size == 0) {
                    isValidFile = false;
                    break;
                }
            }
            if (!isValidFile) {
                $("#Label2").css('display', 'block');
                $('#Label2').text("Invalid file error. System does not support uploaded file.Please upload another file..");
            }
            return isValidFile;
        }


        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $(function () {
            $("#tbxCaseBudget").keypress(function (e) {

                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
                document.getElementById("casebudget").style.display = ret ? "none" : "inline";
                return ret;

            });
        });

        //Created by Gaurav
        function BindCaseDetails(localDataSource1) {

            var grid = $('#gridCaseDetails').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridCaseDetails').empty();


            var grid = $("#gridCaseDetails").kendoGrid({
                dataSource: localDataSource1,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                columns: [
                    {
                        field: "['\ufeffColumn-0']", width: "20%"
                    },
                    {
                        field: "['Column-1']", width: "80%"
                    },
                ]

            });
            $("#gridCaseDetails").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#gridCaseDetails .k-grid-header").css('display', 'none');
        }

        function BindCaveats(data) {

            var grid = $('#gridCaveats').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridCaveats').empty();


            var grid = $("#gridCaveats").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                columns: [
                    { field: '["\ufeffS.No."]', title: "S.No.", width: "4%" },
                    { field: '["Caveat No./\\nReceiving Date /\\nStatus"]', title: "Caveat No./Receiving Date/Status", width: "10%" },
                    { field: '["Caveator\\nVs\\nCaveatee"]', title: "Caveator Vs Caveatee", width: "10%" },
                    { field: '["From Court / State / Bench"]', title: "From Court/State/Bench", width: "10%" },
                    { field: '["Case No. / Judgement Date"]', title: "Case No./Judgement Date", width: "10%" },
                    { field: "Advocate", title: 'Location', width: "10%" },
                    { field: '["Linked with Case and Date"]', title: "Linked with Case and Date", width: "10%" },
                ]
            });

            $("#gridCaveats").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }

        function BindCourtDetails(data) {
            var grid = $('#gridCourtDetails').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridCourtDetails').empty();


            var grid = $("#gridCourtDetails").kendoGrid({
                dataSource: data,
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                pageable: {
                    numeric: true,
                    pageSizes: [3, 5, 10],
                    pageSize: 5,
                    buttonCount: 3,
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { field: "['\ufeffS.No.']", title: "S.No.", width: "120px" },
                    { field: 'Court', title: "Court", width: "120px" },
                    { field: '["Agency State"]', title: "Agency State", width: "120px" },
                    { field: '["Agency Code"]', title: "Agency Code", width: "120px" },
                    { field: '["Case No."]', title: "Case No.", width: "120px" },
                    { field: '["Order Date"]', title: 'Order Date', width: "120px" },
                    { field: '["CNR No. / Designation"]', title: "CNR No. / Designation", width: "120px" },
                    { field: '["Judge1/ Judge2/ Judge3"]', title: "Judge1/ Judge2/ Judge3", width: "120px" },
                    { field: '["Police Station"]', title: "Police Station", width: "120px", },
                    { field: '["Crime No./ Year"]', title: "Crime No./ Year", width: "120px" },
                    { field: '["Authority / Organisation / Impugned Order No."]', title: "Authority / Organisation / Impugned Order No.", width: "120px" },
                    { field: '["Judgement Challanged"]', title: "Judgement Challanged", width: "120px" },
                    { field: '["Judgement Type"]', title: "Judgement Type", width: "120px" },
                    { field: '["Judgement Covered in"]', title: "Judgement Covered in", width: "120px" },
                    { field: '["Vehicle Number"]', title: "Vehicle Number", width: "120px" },
                    { field: '["Reference court / State / District / No."]', title: "Reference court / State / District / No.", width: "120px" },
                    { field: '["Relied Upon court / State / District / No."]', title: "Relied Upon court / State / District / No.", width: "120px" },
                    { field: '["Transfer To State / District / No."]', title: "Transfer To State / District / No.", width: "120px" },
                    { field: '["Government Notification State / No. / Date"]', title: "Government Notification State / No. / Date", width: "120px" },
                ]
            });
            $("#gridCourtDetails").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#gridCourtDetails .k-auto-scrollable").css('overflow-y', 'hidden');
            $("#gridCourtDetails .k-auto-scrollable").css('overflow-x', 'auto');
            $("#gridCourtDetails .k-grid-header-wrap.k-auto-scrollable").css('overflow', 'hidden');
        }

        function BindTaggedMatters(data) {
            var grid = $('#gridTaggedMatters').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridTaggedMatters').empty();


            var grid = $("#gridTaggedMatters").kendoGrid({
                dataSource: data,
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                pageable: {
                    numeric: true,
                    pageSizes: [3, 10, 20],
                    pageSize: 3,
                    buttonCount: 3,
                },
                sortable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { field: '[﻿"﻿Column-0"]', encoding: "UTF_8", title: " ", width: "2%" },
                    { field: '["Case No."]', title: "Case No.", width: "10%" },
                    { field: '["Petitioner vs. Respondant"]', title: "Petitioner vs. Respondant", width: "10%" },
                    { field: 'List', title: "List", width: "4%" },
                    { field: 'Status', title: "Status", width: "6%" },
                    { field: '["Stat. Info."]', title: 'Stat. Info.', width: "10%" },
                    { field: 'IA', title: "IA", width: "13%" },
                    { field: '["Entry Date"]', title: "Entry Date", width: "7%" },
                ]
            });
            $("#gridTaggedMatters").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }

        function Bindinteroculatoryapplications(data) {
            var grid = $('#gridinteroculatoryapplications').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridinteroculatoryapplications').empty();


            var grid = $("#gridinteroculatoryapplications").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                columns: [
                    { field: '["\ufeffINTERLOCUTARY APPLICATION(s) Reg. No./I.A. No."]', title: "Reg. No./I.A. No.", width: "10%" },
                    { field: '["INTERLOCUTARY APPLICATION(s) Particular"]', title: "Particular", width: "15%" },
                    { field: '["INTERLOCUTARY APPLICATION(s) Remark"]', title: "Remark", width: "10%" },
                    { field: '["INTERLOCUTARY APPLICATION(s) Filed By"]', title: "Filed By", width: "10%" },
                    { field: '["INTERLOCUTARY APPLICATION(s) Filing Date"]', title: "Filing Date", width: "10%" },
                    { field: '["INTERLOCUTARY APPLICATION(s) Sr. No."]', title: "Sr. No.", width: "5%" },
                    { field: '["INTERLOCUTARY APPLICATION(s) Status."]', title: "Status", width: "4%" },
                    { field: '["INTERLOCUTARY APPLICATION(s) Entered On"]', title: "Entered On", width: "10%" },

                ]
            });

            $("#gridinteroculatoryapplications").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }

        function BindOtherDocuments(data) {
            var grid = $('#gridotherdocuments').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridotherdocuments').empty();


            var grid = $("#gridotherdocuments").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                columns: [
                    { field: '["﻿OTHER DOCUMENT(s) Doc. No."]', title: "Doc. No.", width: "10%" },
                    { field: '["OTHER DOCUMENT(s) Document Type"]', title: "Document Type", width: "10%" },
                    { field: '["OTHER DOCUMENT(s) Filed By"]', title: "Filed By", width: "10%" },
                    { field: '["OTHER DOCUMENT(s) Filing Date"]', title: "Filing Date", width: "10%" },
                    { field: '["OTHER DOCUMENT(s) Enter By"]', title: "Enter By", width: "10%" },
                ]
            });

            $("#gridotherdocuments").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }

        function BindDefects(data) {
            var grid = $('#gridDefects').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridDefects').empty();


            var grid = $("#gridDefects").kendoGrid({
                dataSource: data,
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                pageable: {
                    numeric: true,
                    pageSizes: [5, 10, 20],
                    pageSize: 5,
                    buttonCount: 3,
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    { field: '["﻿S.No."]', title: "﻿S.No.", width: "8%" },
                    { field: 'Default', title: "Default" },
                    { field: 'Remarks', title: "Remarks" },
                    { field: '["Notification Date"]', title: "Notification Date" },
                    { field: '["Removed On Date"]', title: "Removed On Date" },
                ]
            });

            $("#gridDefects").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }

        function BindSimilarities(data) {
            var grid = $('#gridSimilarities').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridSimilarities').empty();


            var grid = $("#gridSimilarities").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                columns: [
                    { field: '["﻿S.No."]', title: "﻿S.No.", width: "8%" },
                    { field: '["Diary No."]', title: "Diary No.", width: "10%" },
                    { field: '["Registration No."]', title: "Registration No.", width: "10%" },
                    { field: '["From Court"]', title: "From Court", width: "10%" },
                    { field: 'State', title: "State", width: "7%" },
                    { field: 'Bench', title: "Bench", width: "10%" },
                    { field: '["Case No."]', title: "Case No.", width: "10%" },
                    { field: '["Judgement Date"]', title: "Judgement Date", width: "12%" },
                    { field: '["Judgement Challenged"]', title: "Judgement Challenged", width: "12%" },
                    { field: '["Judgement Type"]', title: "Judgement Type", width: "12%" },
                    { field: 'Status', title: "Status", width: "8%" },
                ]
            });

            $("#gridSimilarities").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }


        function BindJudgementOrders(data) {
            var grid = $('#gridSCJugdement').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridSCJugdement').empty();


            var grid = $("#gridSCJugdement").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                columns: [
                    {
                        field: "field", width: "20%", template: '#=field#' + '<span></span>',
                    },
                ]
                //columns: [
                //    //{ field: '["Column2"]', title: "Date of Judgment/Order", width: "8%", template: '#=Column2#' + '<span></span>', },
                //    { field: '["Column2"]', title: "Date of Judgment/Order", width: "8%", template: "#=Column2.replace('href=\\gi','href=https://main.sci.gov.in/')#" },
                //]
            });

            $("#gridSCJugdement .k-grid-header").css('display', 'none');
        }

        function BindListingDates(data) {
            var grid = $('#gridSCListingDates').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridSCListingDates').empty();


            var grid = $("#gridSCListingDates").kendoGrid({
                dataSource: data,
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                pageable: {
                    numeric: true,
                    pageSizes: [3, 5, 10],
                    pageSize: 5,
                    buttonCount: 3,
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    { field: '["\ufeffColumn-0"]', title: "", width: "8%" },
                    { field: '["Column-1"]', title: "" },
                    { field: '["Column-2"]', title: "" },
                    { field: '["Column-3"]', title: "" },
                    { field: '["Column-4"]', title: "" },
                    { field: '["Column-5"]', title: "", width: "15%" },
                    { field: '["Column-6"]', title: "" },
                    { field: '["Column-7"]', title: "" },
                    { field: '["Column-8"]', title: "" },
                ]
            });

            $("#gridSCListingDates").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
            $("#gridSCListingDates .k-grid-header").css('display', 'none');
        }

        function BindSCNotices(data) {
            var grid = $('#gridSCNotices').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridSCNotices').empty();


            var grid = $("#gridSCNotices").kendoGrid({
                dataSource: data,
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                pageable: {
                    numeric: true,
                    pageSizes: [3, 5, 10],
                    pageSize: 5,
                    buttonCount: 3,
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    { field: '["\ufeffS.No"]', title: "S.No" },
                    { field: '["Process Id"]', title: "Process Id", },
                    { field: '["Notice Type"]', title: "Notice Type" },
                    { field: "Name", title: "Name", width: "8%" },
                    { field: '["State / District"]', title: "State / District" },
                    { field: 'Station', title: "Station" },
                    { field: '["Issue Date"]', title: "Issue Date" },
                    { field: '["Returnable Date"]', title: "Returnable Date" },
                    { field: '["Dispatch Date"]', title: "Dispatch Date" },
                    { field: '["Serve Date"]', title: "Serve Date" },
                    { field: '["Ack. Date"]', title: "Ack. Date" },
                    { field: '["Served/Unserved"]', title: "Served/Unserved" },
                ]
            });

            $("#gridSCNotices").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }

        function BindSCOfficeReport(data) {
            var grid = $('#gridSCOfficeReport').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridSCOfficeReport').empty();


            var grid = $("#gridSCOfficeReport").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    {
                        field: "field", width: "20%", template: '#=field#' + '<span></span>',
                    },
                    //{ field: '["\ufeffColumn-0"]', title: "", width: "8%" },
                    //{ field: '["Column-1"]', title: "" },
                    //{ field: '["Column-2"]', title: "" },
                    //{ field: '["Column-3"]', title: "" },
                ]
            });
            $("#gridSCOfficeReport .k-grid-header").css('display', 'none');
        }

        function BindSCCourtFee(data) {
            var grid = $('#gridSCCourtFees').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridSCCourtFees').empty();


            var grid = $("#gridSCCourtFees").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                columns: [
                    {
                        field: "field", width: "20%"
                    },
                ]
            });

            $("#gridSCCourtFees").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#gridSCCourtFees .k-grid-header").css('display', 'none');
        }

        function BindIADetails(data) {
            var grid = $('#gridIADetails').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridIADetails').empty();


            var grid = $("#gridIADetails").kendoGrid({
                dataSource: data,
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                pageable: {
                    numeric: true,
                    pageSizes: [5, 10, 20],
                    pageSize: 5,
                    buttonCount: 3,
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    { field: '["﻿IA Number"]', title: "﻿IA Number", width: "30%" },
                    { field: "﻿Party", title: "﻿Party", width: "19%" },
                    { field: '["Date of Filing"]', title: "Date of Filing", width: "8%" },
                    { field: '["Next Date"]', title: "Next Date", width: "8%" },
                    { field: '["IA Status"]', title: "IA Status", width: "8%" },
                ]
            });

            $("#gridIADetails").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }

        function BindHCActs(data) {
            var grid = $('#gridHCActs').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridHCActs').empty();


            var grid = $("#gridHCActs").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    { field: '["\ufeffUnder Act(s)"]', title: "Under Act(s)" },
                    { field: '["Under Section(s)"]', title: "Under Section(s)" },
                ]
            });

            $("#gridHCActs").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }

        function BindHCCategory(data) {
            var grid = $('#gridHCCategories').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridHCCategories').empty();


            var grid = $("#gridHCCategories").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    {
                        field: '["\ufeffColumn-0"]'
                    },
                    {
                        field: "['Column-1']"
                    },

                ]
            });

            $("#gridHCCategories").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
            $("#gridHCCategories .k-grid-header").css('display', 'none');
        }

        var record = 0;

        function BindHCCaseHistory(data) {
            var grid = $('#gridHCCaseHistory').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridHCCaseHistory').empty();


            var grid = $("#gridHCCaseHistory").kendoGrid({
                dataSource: data,
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                pageable: {
                    numeric: true,
                    pageSizes: [5, 10, 20],
                    pageSize: 5,
                    buttonCount: 3,
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                    var rowIdx = 0;
                    var grid = $("#gridHCCaseHistory").data("kendoGrid");
                    $(grid.tbody).on("click", "td", function (e) {

                        var row = $(this).closest("tr");
                        rowIdx = $("tr", grid.tbody).index(row);
                        OpenHCDocumentPopup(record);
                    });

                },
                columns: [
                    {
                        title: "Sr. No.",
                        template: "#= ++record #",
                        width: 70
                    },
                    { field: '["\ufeffCause List Type"]', title: "Cause List Type", width: "10%" },
                    { field: '["Judge"]', title: "﻿Judge", width: "20%" },
                    { field: '["Business On Date"]', title: "Business On Date", width: "8%" },
                    { field: '["Hearing Date"]', title: "Hearing Date", width: "10%" },
                    { field: '["Purpose of hearing"]', title: "Purpose of hearing", width: "10%" },
                    {
                        command: [
                            { name: "view", text: "", iconClass: "k-icon k-i-eye", className: "ob-view" },
                        ], title: "Action", lock: true, width: "80px;", headerAttributes: { style: "text-align: center;" }
                    }
                ]
            });

            $("#gridHCCaseHistory").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#gridHCCaseHistory").kendoTooltip({
                filter: ".k-grid-view",
                content: function (e) {
                    return "View";
                }
            });
        }

        function OpenHCDocumentPopup(search) {

            var dataHC = [];
            dataHC.push({
                field: (dataCCaseHearingDOC[search].field)
            });


            BindHCHistoryDocument(dataHC);

            $("#HCDocumentPopup").kendoWindow({
                modal: true,
                width: "70%",
                height: "50%",
                title: "Hearing Documents",
                visible: false,
                draggable: true,
                refresh: true,
                pinned: true,
                actions: [
                    "Close"
                ]

            }).data("kendoWindow").open().center();
            e.preventDefault();
            return false;
        }


        function BindHCHistoryDocument(data) {

            var grid = $('#gridHCHistoryDocument').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridHCHistoryDocument').empty();


            var grid = $("#gridHCHistoryDocument").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                columns: [
                    {
                        field: "field", width: "20%", template: '#=field#' + '<span></span>'
                    },

                ]
            });
            $("#gridHCHistoryDocument .k-grid-header").css('display', 'none');
        }

        function BindHCorders(data) {
            var grid = $('#gridHCOrders').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridHCOrders').empty();


            var grid = $("#gridHCOrders").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                dataBound: OngridOrderBoundevent,
                columns: [
                    //{ field: '["\ufeffColumn-0"]', title: "Order Number", width: "10%" },
                    //{ field: '["Column-1"]', title: "Order on", width: "10%" },
                    //{ field: '["Column-2"]', title: "Judge", width: "10%" },
                    //{ field: '["Column-3"]', title: "Order Date", width: "10%" },
                    //{
                    //    command: [
                    //        { name: "view", text: "View", className: "ob-view" },
                    //    ], title: "Action", lock: true, width: "80px;", headerAttributes: { style: "text-align: center;" }
                    //}
                    //{ field: '["Column2"]', title: "Order Date",width:"25%",template: '#=Column2#' + '<span></span>' },
                    {
                        field: "field", width: "20%", template: '#=field#' + '<span></span>'
                    },

                ]
            });

            //$(document).on("click", "#gridHCOrders tbody tr .ob-view", function (e) {

            //    $('#divViewDocumentNew').modal('show');
            //    $('.modal-dialog').css('width', '98%');
            //    var item = $("#gridHCOrders").data("kendoGrid").dataItem($(this).closest("tr"));
            //    $('#OverViewsNew').attr('src', '../../docviewer.aspx?FileID=' + item["﻿Column-0"]);
            //    e.preventDefault();
            //});

            $("#gridHCOrders").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
            $("#gridHCOrders .k-grid-header").css('display', 'none');
        }

        function OngridPetBoundevent(e) {
            for (var i = 0; i < this.columns.length; i++) {
                this.autoWidth;
            }
            var grid = $("#gridHCPetRes").data("kendoGrid");
            var gridData = grid.dataSource.view();

            var currentUid = gridData[0].uid;
            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
            currentRow.hide();
        }

        function OngridOrderBoundevent(e) {
            for (var i = 0; i < this.columns.length; i++) {
                this.autoWidth;
            }
            var grid = $("#gridHCOrders").data("kendoGrid");
            var gridData = grid.dataSource.view();

            var currentUid = gridData[0].uid;
            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
            currentRow.hide();
        }


        function OngridResBoundevent(e) {

            for (var i = 0; i < this.columns.length; i++) {
                this.autoWidth;
            }

            var grid = $("#gridHCResAdvo").data("kendoGrid");
            var gridData = grid.dataSource.view();

            var currentUid = gridData[0].uid;
            var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
            currentRow.hide();
        }

        function BindHCobjections(data) {
            var grid = $('#gridHCobjections').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridHCobjections').empty();


            var grid = $("#gridHCobjections").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    { field: '["\ufeffColumn-0"]', title: "Sr.No.", width: "10%" },
                    { field: '["Column-1"]', title: "Scrutiny Date", width: "10%" },
                    { field: '["Column-2"]', title: "OBJECTION", width: "10%" },
                    { field: '["Column-3"]', title: "OBJECTION Compliance Date", width: "10%" },
                    { field: '["Column-4"]', title: "Receipt Date", width: "10%" },

                ]
            });

            $("#gridHCobjections").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#gridHCobjections .k-grid-header").css('display', 'none');
        }

        function BindHCDocuments(data) {
            var grid = $('#gridHCDocuments').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridHCDocuments').empty();


            var grid = $("#gridHCDocuments").kendoGrid({
                dataSource: data,
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                pageable: {
                    numeric: true,
                    pageSizes: [5, 10, 20],
                    pageSize: 5,
                    buttonCount: 3,
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    { field: '["\ufeffSr. No."]', title: "Sr. No.", width: "10%" },
                    { field: '["Document No."]', title: "Document No.", width: "10%" },
                    { field: '["Date of Receiving"]', title: "Date of Receiving", width: "10%" },
                    { field: '["Filed by"]', title: "Filed by", width: "10%" },
                    { field: '["Name of Advocate"]', title: "Name of Advocate", width: "10%" },
                    { field: '["Document Filed"]', title: "Document Filed", width: "10%" },
                ]
            });

            $("#gridHCDocuments").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }

        function BindHCCaseDetails(localDataSource1) {

            var grid = $('#gridHCCaseDetails').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridHCCaseDetails').empty();


            var grid = $("#gridHCCaseDetails").kendoGrid({
                dataSource: localDataSource1,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                columns: [
                    {
                        field: "field", width: "20%"
                    },
                    {
                        field: "value", width: "80%"
                    },
                ]

            });
            $("#gridHCCaseDetails").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#gridHCCaseDetails .k-grid-header").css('display', 'none');
        }

        function BindHCCaseStatus(data) {

            var grid = $('#gridHCCaseStatus').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridHCCaseStatus').empty();


            var grid = $("#gridHCCaseStatus").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                columns: [
                    { field: '["\ufeffColumn-0"]' },
                    {
                        field: "['Column-1']",
                    },

                ]
            });

            $("#gridHCCaseStatus").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#gridHCCaseStatus .k-grid-header").css('display', 'none');
        }


        function CaseStatusColumnTrim(Column2) {
            var SectionIndex = Column2.search(":");
            return Column2.slice(SectionIndex + 1);
        }

        function BindHCPetAdv(data) {
            var grid = $('#gridHCPetRes').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridHCPetRes').empty();


            var grid = $("#gridHCPetRes").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                dataBound: OngridPetBoundevent,
                columns: [
                    { field: "['\ufeffitem']", title: "Petitioner-Advocate" },
                ]
            });

            $("#gridHCPetRes").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }

        function BindHCResAdv(data) {
            var grid = $('#gridHCResAdvo').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridHCResAdvo').empty();


            var grid = $("#gridHCResAdvo").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                dataBound: OngridResBoundevent,
                columns: [
                    { field: "['\ufeffitem']", title: "Respondent-Advocate" },
                ]
            });

            $("#gridHCResAdvo").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }

        function BindSCActs(data) {
            var grid = $('#gridSCActs').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridSCActs').empty();


            var grid = $("#gridSCActs").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    { field: '["\ufeffUnder Act(s)"]', title: "Under Act(s)" },
                    { field: '["Under Section(s)"]', title: "Under Section(s)" },
                ]
            });

            $("#gridSCActs").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }

        function BindDCCaseHearingHistory(data) {
            var grid = $('#gridDCCaseHearingHistory').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridDCCaseHearingHistory').empty();


            var grid = $("#gridDCCaseHearingHistory").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                dataBound: function () {
                    for (var i = 0; i < this.columns.length; i++) {
                        this.autoWidth;
                    }
                },
                columns: [
                    { field: '["\ufeffJudge"]', title: "﻿Judge", width: "20%" },
                    { field: '["Business on Date"]', title: "Business On Date", width: "8%" },
                    { field: '["Hearing Date"]', title: "Hearing Date", width: "10%" },
                    { field: '["Purpose of Hearing"]', title: "Purpose of hearing", width: "10%" },
                ]
            });

            $("#gridDCCaseHearingHistory").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
        }

        function BindSCCaseDetails(localDataSource1) {

            var grid = $('#gridSCCaseDetails').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridSCCaseDetails').empty();


            var grid = $("#gridSCCaseDetails").kendoGrid({
                dataSource: localDataSource1,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                columns: [
                    {
                        field: "field", width: "20%"
                    },
                    {
                        field: "value", width: "80%"
                    },
                ]

            });

            $("#gridSCCaseDetails").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#gridSCCaseDetails .k-grid-header").css('display', 'none');
        }

        function BindSCCaseStatus(data) {

            var grid = $('#gridSCCaseStatus').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridSCCaseStatus').empty();


            var grid = $("#gridSCCaseStatus").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                columns: [
                    {
                        field: "['\ufeffColumn-0']", width: "20%"
                    },
                    {
                        field: "['Column-1']", width: "80%",
                    },
                ]

            });

            $("#gridSCCaseStatus").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#gridSCCaseStatus .k-grid-header").css('display', 'none');
        }

        function BindSCPetitioner(data) {

            var grid = $('#gridSCPetRes').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridSCPetRes').empty();


            var grid = $("#gridSCPetRes").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                columns: [
                    {
                        field: "['\ufeffColumn-0']", width: "20%"
                    },
                ]

            });

            $("#gridSCPetRes").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#gridSCPetRes .k-grid-header").css('display', 'none');
        }

        function BindSCRespondent(data) {

            var grid = $('#gridSCResAdvo').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#gridSCResAdvo').empty();


            var grid = $("#gridSCResAdvo").kendoGrid({
                dataSource: data,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                selectable: true,
                columns: [
                    {
                        field: "['\ufeffColumn-0']", width: "20%"
                    },
                ]

            });

            $("#gridSCResAdvo").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#gridSCResAdvo .k-grid-header").css('display', 'none');
        }

        var datastring = "";
        var dataRefresh = "";

        function btnRPARefresh_click(e) {
            dataRefresh = "&refresh=true";
            $.ajax({
                type: 'GET',
                url: 'https://cases-api.cap.avantisregtec.in/api/cases/?' + datastring + dataRefresh,
                dataType: "json",
                beforeSend: function (request) {
                    request.setRequestHeader('Authorization', 'Token 1bf60b3ef56203c71f3cf432bdd1a2229080caaa');
                },
                success: function (result) {
                }
            });
            $("#btnRPARefresh").append("<span class='k-icon k-i-reload moving'>" + '</span>');
            setTimeout(function (e) { liOthers_click(1); }, 60000);
            e.preventDefault();
        }

        var dataCCaseHearingDOC = [];

        function liOthers_click(param) {
            debugger

            if ("<%=court_type%>" == "Supreme Court of India") {

                datastring = 'case_number=<%=case_number%>&case_type=<%=case_type%>&case_year=<%=case_year%>&court_type=Supreme Court';
            }

            if ("<%=court_type%>" == "High Court") {

                datastring = 'case_number=<%=case_number%>&case_type=<%=case_type%>&case_year=<%=case_year%>&court_type=High Court&case_state=<%=StateID%>&court_id=<%=BenchID%>';
            }
            if ("<%=court_type%>" == "District Court") {
                datastring = 'case_number=<%=case_number%>&&case_type=<%=case_type%>&case_year=<%=case_year%>&court_type=District%20Court&case_state=<%=StateID%>&court_id=<%=BenchID%>';
            }
            var dataCCase = [];

            var dataCCaseDOC = [];

            $.ajax({
                type: 'GET',
                url: 'https://cases-api.cap.avantisregtec.in/api/cases/?' + datastring + dataRefresh,
                dataType: "json",
                beforeSend: function (request) {
                    request.setRequestHeader('Authorization', 'Token 1bf60b3ef56203c71f3cf432bdd1a2229080caaa');
                },
                success: function (result) {
                    debugger
                    if (result == null || result == 'undefined' || result.message == "No matching case type found" || result.data.data == '') {
                        $("#lblCaseStatus").text("Case Not Found");
                    }
                    else {

                        $('span.moving').remove();

                        var SectionIndex = result.data.updated_at.search("T");

                        $("#lblCaseStatus").text("Last Updated On : " + result.data.updated_at.slice(0, SectionIndex));

                        if ("<%=court_type%>" == "Supreme Court of India") {

                            document.getElementById('divCaseDetailsRPA').style.display = "block";

                            for (var idx = 0; idx < result.data.data.length; idx++) {

                                if (result.data.data[idx].data_type == "case-details") {
                                    BindCaseDetails(result.data.data[idx].data);
                                }

                                if (result.data.data[idx].data_type == "caveats") {

                                    BindCaveats(result.data.data[idx].data);
                                }
                                if (result.data.data[idx].data_type == "court-details") {
                                    BindCourtDetails(result.data.data[idx].data)
                                }
                                if (result.data.data[idx].data_type == "tagged-matters") {
                                    BindTaggedMatters(result.data.data[idx].data)
                                }
                                if (result.data.data[idx].data_type == "interoculatory-applications") {
                                    Bindinteroculatoryapplications(result.data.data[idx].data)
                                }
                                if (result.data.data[idx].data_type == "other-documents") {
                                    BindOtherDocuments(result.data.data[idx].data)
                                }
                                if (result.data.data[idx].data_type == "defects") {
                                    BindDefects(result.data.data[idx].data)
                                }
                                if (result.data.data[idx].data_type == "similarities") {
                                    BindSimilarities(result.data.data[idx].data)
                                }
                                if (result.data.data[idx].data_type == "office-report-documents") {
                                    var dataCCaseOfficeDOC = [];
                                    for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {

                                        var results = (result.data.data[idx].data[idx1]["Column2"]).replace('../../', 'https://main.sci.gov.in/');
                                        results = results.replace('id=\"newc\"', 'id=\"newc\" style=\"display:none\"');
                                        dataCCaseOfficeDOC.push({
                                            field: results
                                        });
                                    }
                                    BindSCOfficeReport(dataCCaseOfficeDOC);
                                }
                                if (result.data.data[idx].data_type == "listing-dates") {
                                    BindListingDates(result.data.data[idx].data)
                                }
                                if (result.data.data[idx].data_type == "notices") {
                                    BindSCNotices(result.data.data[idx].data)
                                }
                                if (result.data.data[idx].data_type == "judgement-documents") {
                                    for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {
                                        dataCCaseDOC.push({

                                            field: ((result.data.data[idx].data[idx1]["Column2"]).replaceAll('<a href=\"/', '<a href=\"https://main.sci.gov.in/'))
                                        });
                                    }
                                    BindJudgementOrders(dataCCaseDOC);
                                }

                                if (result.data.data[idx].data_type == "court-fees") {
                                    for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {

                                        dataCCase.push({
                                            field: result.data.data[idx].data[idx1]["Column2"],
                                        });
                                    }
                                    BindSCCourtFee(dataCCase)
                                }
                            }
                        }
                        else if ("<%=court_type%>" == "High Court") {
                            document.getElementById('divCaseDetailsHC').style.display = "block";
                            for (var idx = 0; idx < result.data.data.length; idx++) {

                                if (result.data.data[idx].data_type == "IA-details") {
                                    BindIADetails(result.data.data[idx].data);
                                }
                                if (result.data.data[idx].data_type == "acts") {
                                    BindHCActs(result.data.data[idx].data)
                                }
                                if (result.data.data[idx].data_type == "category-details") {
                                    BindHCCategory(result.data.data[idx].data);
                                }
                                if (result.data.data[idx].data_type == "case-history") {
                                    BindHCCaseHistory(result.data.data[idx].data)
                                }
                                if (result.data.data[idx].data_type == "hearing-details") {

                                    for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {

                                        dataCCaseHearingDOC.push({
                                            field: (result.data.data[idx].data[idx1]["\ufeffColumn1"])
                                        });
                                    }

                                    dataCCaseHearingDOC;
                                }
                                if (result.data.data[idx].data_type == "ordersDocuments") {
                                    var dataCCaseOrderDOC = [];
                                    for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {
                                        if (result.data.data[idx].data[idx1]["\ufeffColumn1"] == "orders-documents") {
                                            dataCCaseOrderDOC.push({

                                                field: (result.data.data[idx].data[idx1]["Column2"]).replaceAll('href=\"', 'href=\"https://hcservices.ecourts.gov.in/hcservices/')
                                            });
                                        }
                                    }
                                    BindHCorders(dataCCaseOrderDOC)
                                }
                                if (result.data.data[idx].data_type == "objections") {
                                    BindHCobjections(result.data.data[idx].data)
                                }
                                if (result.data.data[idx].data_type == "document-details") {
                                    BindHCDocuments(result.data.data[idx].data)
                                }
                                if (result.data.data[idx].data_type == "case-details") {

                                    for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {

                                        dataCCase.push({
                                            field: result.data.data[idx].data[idx1]["\ufeffColumn-0"],
                                            value: result.data.data[idx].data[idx1]["Column-1"]
                                        });

                                        dataCCase.push({
                                            field: result.data.data[idx].data[idx1]["Column-2"],
                                            value: result.data.data[idx].data[idx1]["Column-3"]
                                        });
                                    }
                                    BindHCCaseDetails(dataCCase);
                                }
                                if (result.data.data[idx].data_type == "case-status") {
                                    BindHCCaseStatus(result.data.data[idx].data)
                                }
                                if (result.data.data[idx].data_type == "petitioner-advocate") {
                                    BindHCPetAdv(result.data.data[idx].data);
                                }
                                if (result.data.data[idx].data_type == "respondent-advocate") {
                                    BindHCResAdv(result.data.data[idx].data);
                                }
                            }
                        }
                        else if ("<%=court_type%>" == "District Court") {

                            document.getElementById("divCaseDetailsDC").style = "display:block;";
                            for (var idx = 0; idx < result.data.data.length; idx++) {

                                if (result.data.data[idx].data_type == "acts") {
                                    BindSCActs(result.data.data[idx].data)
                                }
                                if (result.data.data[idx].data_type == "case-history") {
                                    BindDCCaseHearingHistory(result.data.data[idx].data)
                                }
                                if (result.data.data[idx].data_type == "case-status") {
                                    BindSCCaseStatus(result.data.data[idx].data);
                                }

                                if (result.data.data[idx].data_type == "petitioner-advocate") {
                                    BindSCPetitioner(result.data.data[idx].data);
                                }

                                if (result.data.data[idx].data_type == "respondent-advocate") {
                                    BindSCRespondent(result.data.data[idx].data);
                                }

                                if (result.data.data[idx].data_type == "case-details") {

                                    for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {

                                        dataCCase.push({
                                            field: result.data.data[idx].data[idx1]["\ufeffColumn-0"],
                                            value: result.data.data[idx].data[idx1]["Column-1"]
                                        });

                                        dataCCase.push({
                                            field: result.data.data[idx].data[idx1]["Column-2"],
                                            value: result.data.data[idx].data[idx1]["Column-3"]
                                        });
                                    }
                                    BindSCCaseDetails(dataCCase);
                                }
                            }
                        }

                }

                },
                error: function (result) {
                    console.log(result);
                }
            });

    }

    function DownloadAdvocateBillInPdf(advcid) {
        $.ajax({
            type: "GET",
            url: "<% =KendoPath%>/LitigationExportReport/AdvocateBillReport",
                data: {
                    CustomerID: <%=CustomerID%>,
                    AdvcocateBillID: advcid

                },
                success: function (response) {
                    if (response != "Error" && response != "No Record Found" && response != "") {

                        $('#divViewDocument').modal('show');
                        $('.modal-dialog').css('width', '98%');
                        $('#OverViews').attr('src', '../../docviewer.aspx?docurlAdvocateBillPDF=' + response);
                        e.preventDefault();
                    }
                    else {
                        alert("No Record Found");
                    }
                }
            });
            e.preventDefault();
            return false;
        }

    </script>
</head>
<body style="background: none !important; overflow-y: hidden;">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <input type="hidden" id="hiddenColor" style="display: none;" />

        <div class="mainDiv" style="background-color: #f7f7f7;">

            <header class="panel-heading tab-bg-primary" style="background: none !important;">
                <ul class="nav nav-tabs">
                    <li class="active" id="liCaseDetail" runat="server">
                        <asp:LinkButton ID="lnkCaseDetail" OnClick="TabCase_Click" runat="server" Style="background-color: #f7f7f7;">Case Summary</asp:LinkButton>
                    </li>
                    <li class="" id="liOthers" runat="server">
                        <asp:LinkButton ID="lnkOthers" OnClick="lnkOthers_Click" runat="server" Style="background-color: #f7f7f7;">Others</asp:LinkButton>
                    </li>
                    <li class="" id="liDocument" runat="server">
                        <asp:LinkButton ID="lnkDocument" OnClick="TabDocument_Click" runat="server" Style="background-color: #f7f7f7;">Documents</asp:LinkButton>
                    </li>
                    <li class="" id="liCaseTask" runat="server">
                        <asp:LinkButton ID="lnkCaseTask" OnClick="TabTask_Click" runat="server" Style="background-color: #f7f7f7;">Task/Activity</asp:LinkButton>
                    </li>
                    <li class="" id="liCaseHearing" runat="server">
                        <asp:LinkButton ID="lnkCaseHearing" OnClick="TabHearing_Click" runat="server" Style="background-color: #f7f7f7;">Hearing</asp:LinkButton>
                    </li>
                    <li class="" id="liCaseOrder" runat="server">
                        <asp:LinkButton ID="lnkCaseOrder" OnClick="TabOrder_Click" runat="server" Style="background-color: #f7f7f7;">Order</asp:LinkButton>
                    </li>
                    <li class="" id="liCaseAdvocateBill" runat="server">
                        <asp:LinkButton ID="lnkCaseAdvocateBill" OnClick="TabAdvocateBill_Click" runat="server" Style="background-color: #f7f7f7;">Advocate Bill</asp:LinkButton>
                    </li>
                    <li class="" id="liCaseStatus" runat="server">
                        <asp:LinkButton ID="lnkCaseStatus" OnClick="TabStatus_Click" runat="server" Style="background-color: #f7f7f7;">Status/Payment</asp:LinkButton>
                    </li>
                    <li class="" id="liCaseRating" runat="server">
                        <asp:LinkButton ID="lnkCaseRating" OnClick="TabRating_Click" runat="server" Style="background-color: #f7f7f7;">Lawyer Rating</asp:LinkButton>
                    </li>
                    <li class="" id="liAuditLog" runat="server">
                        <asp:LinkButton ID="lnkAuditLog" OnClick="TabAuditLog_Click" runat="server" Style="background-color: #f7f7f7;">Audit Log</asp:LinkButton>
                    </li>
                </ul>
            </header>

            <div class="clearfix" style="height: 20px;"></div>

            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 40%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div id="divMainView" class="boxscroll do-nicescroll4" style="height: 500px; overflow-y: auto;">
                <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="CaseSummaryView" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">

                                <div class="col-md-12 colpadding0" style="min-height: 0px; max-height: 240px; overflow-y: auto;">
                                    <asp:Panel ID="vdpanel1" runat="server" ScrollBars="Auto">
                                        <asp:ValidationSummary ID="VSCasePopup" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="CasePopUpValidationGroup" />
                                        <asp:CustomValidator ID="cvCasePopUp" runat="server" EnableClientScript="False"
                                            ValidationGroup="CasePopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </asp:Panel>
                                </div>

                                <div id="divCaseDetails" class="row Dashboard-white-widget">
                                    <!--CaseDetail Panel Start-->
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Case Detail">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivCaseDetails">
                                                    <a>
                                                        <h2></h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivCaseDetails">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseDivCaseDetails" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="form-group col-md-12">
                                                        <div class="col-md-6 float-left">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                        </div>
                                                        <div class="col-md-6 float-left text-right" style="margin-bottom: 10px;">
                                                            <asp:UpdatePanel ID="upSendMailPop" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:LinkButton runat="server" ID="lnkLinkCase" OnClientClick="OpenCaseLinkingPopup();" data-toggle="tooltip" ToolTip="Link Case with Other Case">
                                                                     <img src="../../Images/link-icon.png" alt="Link" title="" />
                                                                    </asp:LinkButton>

                                                                    <asp:LinkButton ID="lnkActDetails" runat="server"
                                                                        OnClick="lnkActDetails_Click" data-toggle="tooltip" ToolTip="View Compliance Document(s)">
                                                                  <img src="../../Images/View-icon-new.png" alt="View" title="" />
                                                                    </asp:LinkButton>

                                                                    <asp:LinkButton runat="server" ID="lnkSendMailWithDoc" OnClientClick="OpenSendMailPopup();" data-toggle="tooltip" ToolTip="Send E-Mail with Document(s)">
                                                                     <img src="../../Images/send-mail-icon.png" alt="Send" title="" />
                                                                    </asp:LinkButton>

                                                                    <asp:LinkButton runat="server" ID="btnEditCaseDetail" OnClick="btnEditCaseControls_Click" data-toggle="tooltip" ToolTip="Edit Case Detail(s)">
                                                                        <img src="../../Images/edit_icon_new.png" alt="Edit" title="" />
                                                                    </asp:LinkButton>

                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnEditCaseDetail" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <asp:Panel ID="pnlCase" runat="server">
                                                        <div class="row">
                                                            <div class="form-group col-md-5">
                                                                <label style="width: 4%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 31%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Type</label>
                                                                <asp:RadioButtonList ID="rbCaseInOutType" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem class="radio-inline" Text="Defendant" Value="I" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem class="radio-inline" Text="Plaintiff " Value="O"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>
                                                            <div class="form-group col-md-7">
                                                                <div class="form-group col-md-3 input-group date" style="width: 45%">
                                                                    <div>
                                                                        <label style="width: 7%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                        <label style="display: block; float: left; font-size: 13px; color: #333; width: 25%;">Open Date</label>
                                                                    </div>
                                                                    <div class="col-md-6 input-group date">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar color-black"></span>
                                                                        </span>
                                                                        <asp:TextBox runat="server" placeholder="Open Date" class="form-control" ID="txtCaseDate" />
                                                                        <asp:TextBox runat="server" class="form-control" ID="txtCaseYear" Style="display: none;" />
                                                                        <asp:TextBox runat="server" class="form-control" ID="txtBenchID" Style="display: none;" />
                                                                        <asp:TextBox runat="server" class="form-control" ID="txtStateID" Style="display: none;" />
                                                                    </div>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Case Date can not be empty."
                                                                        ControlToValidate="txtCaseDate" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                                </div>

                                                                <div class="form-group col-md-3" style="width: 53%;">
                                                                    <div>
                                                                        <%if (CaseLabel == true) %>
                                                                        <%{%>
                                                                        <label style="width: 6%; display: block; font-size: 13px; color: red; float: left;">*</label>
                                                                        <label style="display: block; float: left; font-size: 13px; color: #333; margin-left: -4px;">
                                                                            Disputed Financial Year</label>
                                                                        <%} %>
                                                                        <% else if (CaseLabel == false) %>
                                                                        <%{%>
                                                                        <label style="width: 6%; display: block; font-size: 13px; color: red; float: left;">*</label>
                                                                        <label style="display: block; float: left; font-size: 13px; color: #333; margin-left: -4px;">
                                                                            Financial Year</label>
                                                                        <%} %>
                                                                    </div>
                                                                    <div class="col-md-6 " style="width: 49%;">

                                                                        <asp:ListBox ID="DropDownListChosen1" CssClass="form-control" runat="server" SelectionMode="Multiple"
                                                                            EnableFilterSearch="true" FilterType="StartsWith" Width="100%"></asp:ListBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ErrorMessage=" Financial Year can not be empty."
                                                                            ControlToValidate="DropDownListChosen1" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Court Case No.</label>
                                                                <asp:TextBox runat="server" ID="tbxRefNo" Style="width: 60%" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:RequiredFieldValidator ID="rfvRefNo" ErrorMessage="Court Case No. can not be empty."
                                                                    ControlToValidate="tbxRefNo" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                                <asp:RegularExpressionValidator ValidationGroup="CasePopUpValidationGroup" Display="None" ControlToValidate="tbxRefNo" ID="revCaseRefNo"
                                                                    ValidationExpression="^[\s\S]{1,100}$" runat="server" ErrorMessage="Maximum 100 characters required in Court Case No">
                                                                </asp:RegularExpressionValidator>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Internal Case No.</label>
                                                                <asp:TextBox runat="server" ID="tbxInternalCaseNo" Style="width: 60%" CssClass="form-control" MaxLength="50" autocomplete="off" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Case Title</label>
                                                                <asp:TextBox runat="server" ID="tbxTitle" TextMode="MultiLine" Style="width: 80.6%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Title can not be empty."
                                                                    ControlToValidate="tbxTitle" runat="server" ValidationGroup="CasePopUpValidationGroup"
                                                                    Display="None" />
                                                                <asp:RegularExpressionValidator ValidationGroup="CasePopUpValidationGroup" Display="None" ControlToValidate="tbxRefNo" ID="revCaseTitle"
                                                                    ValidationExpression="^[\s\S]{1,100}$" runat="server" ErrorMessage="Maximum 100 characters required in Case Title">
                                                                </asp:RegularExpressionValidator>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Act</label>
                                                                <div style="float: left; width: 60%">
                                                                    <asp:ListBox ID="ddlAct" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="70%" onchange="ddlActChange()"></asp:ListBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please Select Act or Select 'Not Applicable'"
                                                                        ControlToValidate="ddlAct" runat="server" ValidationGroup="CasePopUpValidationGroup"
                                                                        Display="None" />
                                                                </div>
                                                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                    <img id="lnkShowAddNewActModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenAddActPopup()" alt="Add New Opponent" title="Add New Opponent" />
                                                                    <asp:LinkButton ID="lnkBtnAct" OnClick="lnkBtnAct_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Under Section</label>
                                                                <asp:TextBox runat="server" ID="tbxSection" Style="width: 60%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:RegularExpressionValidator ValidationGroup="CasePopUpValidationGroup" Display="None" ControlToValidate="tbxRefNo" ID="revSection"
                                                                    ValidationExpression="^[\s\S]{0,100}$" runat="server" ErrorMessage="Maximum 100 characters required in Section">
                                                                </asp:RegularExpressionValidator>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Case Type</label>
                                                                <div style="float: left; width: 60%">
                                                                    <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:DropDownListChosen runat="server" AutoPostBack="true" ID="ddlCaseCategory" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                class="form-control" Width="100%" DataPlaceHolder="Select Case Type" onchange="ddlCaseCategoryChange()" OnSelectedIndexChanged="ddlCaseCategory_SelectedIndexChanged">
                                                                            </asp:DropDownListChosen>
                                                                            <asp:RequiredFieldValidator ID="rfvCaseCategory" ErrorMessage="Please Select Case Type"
                                                                                ControlToValidate="ddlCaseCategory" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="ddlCaseCategory" EventName="SelectedIndexChanged" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>

                                                                </div>
                                                                <div style="float: right; text-align: center; width: 0%; margin-top: 1%;">
                                                                    <img id="lnkAddNewCaseCategoryModal" runat="server"
                                                                        style="float: right; display: none;" src="../../Images/add_icon_new.png"
                                                                        onclick="OpenCategoryTypePopup()" alt="Add"
                                                                        data-toggle="tooltip" data-placement="bottom" tooltip="Add New Case Type" />
                                                                    <asp:LinkButton ID="lnkBtnCategory" OnClick="lnkBtnCategory_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>

                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Case Budget</label>
                                                                <asp:TextBox runat="server" ID="tbxCaseBudget" Style="width: 60%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <span id="casebudget" style="color: Red; display: none; margin-left: 173px;">Input digits (0 - 9)</span>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server"
                                                                    ValidationGroup="CasePopUpValidationGroup" ErrorMessage="Please enter a valid Case Budget."
                                                                    ControlToValidate="tbxCaseBudget" ValidationExpression="[0-9]+(\.[0-9][0-9]?)?"></asp:RegularExpressionValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Opponent</label>
                                                                <div style="float: left; width: 60%">
                                                                    <asp:ListBox ID="ddlParty" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="70%" onchange="ddlPartyChange()"></asp:ListBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Select Opponent"
                                                                        ControlToValidate="ddlParty" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                                </div>
                                                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                    <img id="lnkShowAddNewPartyModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenPartyDetailsPopup()" alt="Add New Opponent" title="Add New Opponent" />
                                                                    <asp:LinkButton ID="lnkBtnParty" OnClick="lnkBtnParty_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Opposition Lawyer</label>
                                                                <asp:ListBox ID="lstBoxOppositionLawyer" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="70%" onchange="OpposiLawerChangeAddButton()"></asp:ListBox>

                                                                <div style="float: right; text-align: center; width: 0%; margin-top: 1%;">
                                                                    <img id="lnkShowAddNewOppoLawyerModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenAddOppostitionLawyerPopup()" alt="Add New Opposition Lawyer" title="Add New Opposition Lawyer" />
                                                                    <asp:LinkButton ID="lnkOpposiLawyer" OnClick="lnkOpposiLawyer_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Court</label>
                                                                <div style="float: left; width: 60%">
                                                                    <asp:DropDownListChosen runat="server" ID="ddlCourt" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        class="form-control" Width="100%" DataPlaceHolder="Select Court" onchange="ddlCourtChange()">
                                                                    </asp:DropDownListChosen>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Please Select Court"
                                                                        ControlToValidate="ddlCourt" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                                </div>

                                                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                    <img id="lnkAddNewCourtModal" style="float: right; margin-top: -7px; display: none;" src="../../Images/add_icon_new.png" onclick="OpenCourtPopup()" alt="Add New Court" title="Add New Court" />
                                                                    <asp:LinkButton ID="lblCourt" OnClick="lblCourt_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Judge</label>
                                                                <asp:TextBox runat="server" ID="tbxJudge" Style="width: 60%" CssClass="form-control" autocomplete="off" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Case Description</label>
                                                                <asp:TextBox runat="server" ID="tbxDescription" TextMode="MultiLine" Style="width: 80.6%; min-height: 115px;" CssClass="form-control" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Description can not be empty."
                                                                    ControlToValidate="tbxDescription" runat="server" ValidationGroup="CasePopUpValidationGroup"
                                                                    Display="None" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Entity/Location</label>

                                                                <asp:TextBox runat="server" ID="tbxBranch" Style="padding: 0px; padding-left: 10px; margin: 0px; width: 60%; background-color: #fff; cursor: pointer;"
                                                                    autocomplete="off" AutoCompleteType="None" CausesValidation="true" CssClass="form-control" ReadOnly="true" />
                                                                <%--onclick="txtclick()"--%>
                                                                <div style="margin-left: 28%; position: absolute; z-index: 10; width: 70%;" id="divBranches">
                                                                    <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                                                        BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="150px" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged"
                                                                        Style="overflow: auto; margin-top: -20px; border: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true">
                                                                    </asp:TreeView>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="rfvBranch" ErrorMessage="Please Select Entity/Location." InitialValue="Select Entity/Location"
                                                                    ControlToValidate="tbxBranch" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Jurisdiction</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlJurisdiction" DataPlaceHolder="Select Jurisdiction" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="60%" />
                                                            </div>

                                                        </div>
                                                        <div class="row">

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Department</label>
                                                                <div style="float: left; width: 60%">
                                                                    <asp:DropDownListChosen runat="server" ID="ddlDepartment" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                        DataPlaceHolder="Select Department" CssClass="form-control" Width="100%" onchange="ddlDepartmentChange()" />
                                                                    <asp:RequiredFieldValidator ID="rfvDept" ErrorMessage="Please Select Department"
                                                                        ControlToValidate="ddlDepartment" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                                </div>
                                                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                    <img id="lnkAddNewDepartmentModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenDepartmentPopup('')" alt="Add New Department" title="Add New Department" />
                                                                    <asp:LinkButton ID="lnkBtnDept" OnClick="lnkBtnDept_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                       <%if (Pernodkey == true) {%>
                                                                        Authorized <br />Representative
                                                                        <%} else {%>
                                                                        Contact Person Of Department
                                                                        <%} %>
                                                                </label>
                                                             
                                                                <asp:DropDownListChosen runat="server" ID="ddlCPDepartment" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="60%" />

                                                                <asp:RequiredFieldValidator ID="rfvCPDepartment" ErrorMessage="Please Select Contact Person Of Department" InitialValue="0"
                                                                    ControlToValidate="ddlCPDepartment" runat="server" ValidationGroup="ContractPopUpValidationGroup" Display="None" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Owner</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlOwner" DataPlaceHolder="Select Owner" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="60%" />
                                                                <asp:RequiredFieldValidator ID="rfvOwner" ErrorMessage="Please Select Owner"
                                                                    ControlToValidate="ddlOwner" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Winning Prospect</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlCaseRisk" DataPlaceHolder="Select Risk" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="60%">
                                                                    <asp:ListItem Text="High" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                                                                </asp:DropDownListChosen>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Claimed Amount</label>
                                                                <asp:TextBox runat="server" ID="tbxClaimedAmt" Style="width: 60%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:CompareValidator ID="cvClaimedAmt" runat="server" ControlToValidate="tbxClaimedAmt" ErrorMessage="Only Numbers in Claimed Amount."
                                                                    ValidationGroup="CasePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Probable Amount</label>
                                                                <asp:TextBox runat="server" ID="tbxProbableAmt" Style="width: 60%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                <asp:CompareValidator ID="cvProbableAmt" runat="server" ControlToValidate="tbxProbableAmt" ErrorMessage="Only Numbers in Probable Amount."
                                                                    ValidationGroup="CasePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Provisional Amount</label>
                                                                <asp:TextBox runat="server" ID="txtprovisionalamt" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                                <span id="error2" style="color: Red; display: none; margin-left: 173px;">Input digits (0 - 9)</span>
                                                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtprovisionalamt" ErrorMessage="Only Numbers in Provisional Amount."
                                                                    ValidationGroup="CasePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                                <%--<asp:CompareValidator runat="server" ValidationGroup="CasePopUpValidationGroup" ControlToValidate="txtprovisionalamt" Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" ErrorMessage="Provisional Amount cannot be less than zero" Display="Dynamic" Text="*" />--%>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Protest Money</label>
                                                                <asp:TextBox runat="server" ID="txtprotestmoney" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                                <span id="error3" style="color: Red; display: none; margin-left: 173px;">Input digits (0 - 9)</span>
                                                                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtprotestmoney" ErrorMessage="Only Numbers in protest money."
                                                                    ValidationGroup="CasePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                                <%-- <asp:CompareValidator ValidationGroup="CasePopUpValidationGroup" runat="server" ControlToValidate="txtprotestmoney" Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" ErrorMessage="Protest Money cannot be less than zero" Display="Dynamic" Text="*" />--%>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Recovery Amount</label>
                                                                <asp:TextBox runat="server" ID="txtRecovery" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                                <span id="error4" style="color: Red; display: none; margin-left: 173px;">Input digits (0 - 9)</span>
                                                                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtRecovery" ErrorMessage="Recovery money should be numric"
                                                                    ValidationGroup="CasePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                                <%-- <asp:CompareValidator ValidationGroup="CasePopUpValidationGroup" runat="server" ControlToValidate="txtprotestmoney" Operator="GreaterThanEqual" ValueToCompare="0" Type="Integer" ErrorMessage="Protest Money cannot be less than zero" Display="Dynamic" Text="*" />--%>
                                                            </div>
                                                            <div id="divNoticeDate" runat="server" class="form-group col-md-3 input-group date" style="width: 49%;">
                                                                <div>
                                                                    <label style="width: 26.5%; display: block; float: left; font-size: 13px; color: #333; margin-left: 16px; margin-right: -8px;">Notice Date</label>
                                                                </div>
                                                                <div class="col-md-6 input-group date" style="width: 67%;">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar color-black"></span>
                                                                    </span>
                                                                    <asp:TextBox runat="server" placeholder="Notice Date" class="form-control" ID="txtNoticeDate" />
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Bank Guarantee</label>
                                                                <asp:TextBox runat="server" ID="txtbankgurantee" Style="width: 80.6%; min-height: 115px" TextMode="MultiLine" CssClass="form-control" autocomplete="off" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Potential Impact</label>
                                                                <asp:RadioButtonList ID="rblPotentialImpact" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem class="radio-inline" Text="Monetary" Value="M" Selected="True" onclick="rblImpactChange()"></asp:ListItem>
                                                                    <asp:ListItem class="radio-inline" Text="Non-Monetary" Value="N" onclick="rblImpactChange()"></asp:ListItem>
                                                                    <asp:ListItem class="radio-inline" Text="Both" Value="B" onclick="rblImpactChange()"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>

                                                            <div class="form-group col-md-6" id="divMonetory">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Monetary</label>
                                                                <asp:TextBox runat="server" ID="tbxMonetory" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                            </div>
                                                        </div>

                                                        <div class="row" id="divNonMonetory">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Non-Monetary</label>
                                                                <asp:TextBox runat="server" ID="tbxNonMonetory" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Years</label>
                                                                <asp:TextBox runat="server" ID="tbxNonMonetoryYears" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                                <span id="Year" style="color: Red; display: none; margin-left: 173px;">Input digits (0 - 9)</span>
                                                            </div>
                                                        </div>



                                                        <div class="row" id="stste" runat="server">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    State</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlState" DataPlaceHolder="Select State" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="60%" />
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>

                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Law Firm</label>

                                                                        <asp:DropDownListChosen ID="ddlLawFirm" CssClass="form-control" AutoPostBack="true" runat="server"
                                                                            DataPlaceHolder="Select Law Firm" Width="60%"
                                                                            OnSelectedIndexChanged="ddlLawFirm_SelectedIndexChanged" AllowSingleDeselect="false"
                                                                            onchange="ShowLawFirmAddbutton()">
                                                                        </asp:DropDownListChosen>

                                                                        <div style="float: right; text-align: center; width: 0%; margin-top: 1%;">
                                                                            <img id="lnkShowAddNewLawFirmModal" style="float: right; display: none;" src="../../Images/add_icon_new.png"
                                                                                onclick="OpenLawFirmPopupModel();" alt="Add New Law Firm" title="Add New Law Firm" />
                                                                            <asp:LinkButton ID="lnkLawFirmBind" OnClick="lnkLawFirmBind_Click" Style="float: right; display: none;" Width="100%" runat="server"></asp:LinkButton>
                                                                        </div>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="ddlLawFirm" EventName="SelectedIndexChanged" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-6" id="divRemark">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Remark</label>
                                                                <asp:TextBox runat="server" ID="txtremark" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                            </div>
                                                            <div class="form-group col-md-6" id="divRisk" runat="server">
                                                                <label runat="server" style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Risk</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlRisk" DataPlaceHolder="Select Risk" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="60%">
                                                                    <%-- <asp:ListItem Text="Select Risk" Value="-1" Selected="True"></asp:ListItem>--%>
                                                                    <asp:ListItem Text="Select Risk" Value="-1"></asp:ListItem>
                                                                    <asp:ListItem Text="High" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                                                                </asp:DropDownListChosen>
                                                            </div>
                                                        </div>

                                                        <div id="emmamiusers" class="row colpadding0" runat="server" style="display: none;">
                                                            <div class="col-md-12 colpadding0">
                                                                <div class="row" id="divDeposits" runat="server">
                                                                    <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Pre-Deposit</label>
                                                                        <asp:TextBox runat="server" ID="txtPreDeposit" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                                        <asp:CompareValidator ID="cvPreDeposit" runat="server" ControlToValidate="txtPreDeposit" ErrorMessage="Only Numbers in Pre-Deposit"
                                                                            ValidationGroup="CasePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                                    </div>

                                                                    <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Post Deposit</label>
                                                                        <asp:TextBox runat="server" ID="txtPostDeposit" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                                        <asp:CompareValidator ID="cvPostDeposit" runat="server" ControlToValidate="txtPostDeposit" ErrorMessage="Only Numbers in Post-Deposit"
                                                                            ValidationGroup="CasePopUpValidationGroup" Display="None" Type="Double" Operator="DataTypeCheck"></asp:CompareValidator>
                                                                    </div>
                                                                </div>

                                                                <div class="row" id="divPeriod" runat="server">
                                                                    <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Period</label>
                                                                        <asp:DropDownListChosen runat="server" ID="ddlFY" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="false"
                                                                            DataPlaceHolder="Select FY" class="form-control" Width="60%">
                                                                            <asp:ListItem Text="2026-2027" Value="2026-2027"></asp:ListItem>
                                                                            <asp:ListItem Text="2025-2026" Value="2025-2026"></asp:ListItem>
                                                                            <asp:ListItem Text="2024-2025" Value="2024-2025"></asp:ListItem>
                                                                            <asp:ListItem Text="2023-2024" Value="2023-2024"></asp:ListItem>
                                                                            <asp:ListItem Text="2022-2023" Value="2022-2023"></asp:ListItem>
                                                                            <asp:ListItem Text="2021-2022" Value="2021-2022"></asp:ListItem>
                                                                            <asp:ListItem Text="2020-2021" Value="2020-2021"></asp:ListItem>
                                                                            <asp:ListItem Text="2019-2020" Value="2019-2020"></asp:ListItem>
                                                                            <asp:ListItem Text="2018-2019" Value="2018-2019"></asp:ListItem>
                                                                            <asp:ListItem Text="2017-2018" Value="2017-2018"></asp:ListItem>
                                                                            <asp:ListItem Text="2016-2017" Value="2016-2017"></asp:ListItem>
                                                                            <asp:ListItem Text="2015-2016" Value="2015-2016"></asp:ListItem>
                                                                            <asp:ListItem Text="2014-2015" Value="2014-2015"></asp:ListItem>
                                                                            <asp:ListItem Text="2013-2014" Value="2013-2014"></asp:ListItem>
                                                                            <asp:ListItem Text="2012-2013" Value="2012-2013"></asp:ListItem>
                                                                            <asp:ListItem Text="2011-2012" Value="2011-2012"></asp:ListItem>
                                                                            <asp:ListItem Text="2010-2011" Value="2010-2011"></asp:ListItem>
                                                                            <asp:ListItem Text="2009-2010" Value="2009-2010"></asp:ListItem>
                                                                            <asp:ListItem Text="2008-2009" Value="2008-2009"></asp:ListItem>
                                                                            <asp:ListItem Text="2007-2008" Value="2007-2008"></asp:ListItem>
                                                                            <asp:ListItem Text="2006-2007" Value="2006-2007"></asp:ListItem>
                                                                            <asp:ListItem Text="2005-2006" Value="2005-2006"></asp:ListItem>
                                                                            <asp:ListItem Text="2004-2005" Value="2004-2005"></asp:ListItem>
                                                                            <asp:ListItem Text="2003-2004" Value="2003-2004"></asp:ListItem>
                                                                            <asp:ListItem Text="2002-2003" Value="2002-2003"></asp:ListItem>
                                                                            <asp:ListItem Text="2001-2002" Value="2001-2002"></asp:ListItem>
                                                                            <asp:ListItem Text="2000-2001" Value="2000-2001"></asp:ListItem>
                                                                            <asp:ListItem Text="1999-2000" Value="1999-2000"></asp:ListItem>
                                                                            <asp:ListItem Text="1998-1999" Value="1998-1999"></asp:ListItem>
                                                                            <asp:ListItem Text="1997-1998" Value="1997-1998"></asp:ListItem>
                                                                            <asp:ListItem Text="1996-1997" Value="1996-1997"></asp:ListItem>
                                                                            <asp:ListItem Text="1995-1996" Value="1995-1996"></asp:ListItem>
                                                                            <asp:ListItem Text="1994-1995" Value="1994-1995"></asp:ListItem>
                                                                            <asp:ListItem Text="1993-1994" Value="1993-1994"></asp:ListItem>
                                                                            <asp:ListItem Text="1992-1993" Value="1992-1993"></asp:ListItem>
                                                                            <asp:ListItem Text="1991-1992" Value="1991-1992"></asp:ListItem>
                                                                            <asp:ListItem Text="1990-1991" Value="1990-1991"></asp:ListItem>
                                                                        </asp:DropDownListChosen>
                                                                    </div>

                                                                    <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Tax Demand</label>
                                                                        <asp:TextBox runat="server" ID="txttaxDemand" Style="width: 60%;" CssClass="form-control" autocomplete="off" onkeypress="return NumberOnly()" />
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorpenalty"
                                                                            ControlToValidate="txttaxDemand" runat="server"
                                                                            ErrorMessage="Please Insert Number Only" ValidationGroup="NoticePopUpValidationGroup"
                                                                            ValidationExpression="\d+" Display="None"> 
                                                                        </asp:RegularExpressionValidator>
                                                                    </div>
                                                                </div>

                                                                <div class="row" id="divpanalty" runat="server">
                                                                    <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Interest</label>
                                                                        <asp:TextBox runat="server" ID="txtIntrest" Style="width: 60%;" CssClass="form-control" autocomplete="off" onkeypress="return NumberOnly()" />
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                                                                            ControlToValidate="txtIntrest" runat="server"
                                                                            ErrorMessage="Please Insert Number Only" ValidationGroup="NoticePopUpValidationGroup"
                                                                            ValidationExpression="\d+" Display="None"> 
                                                                        </asp:RegularExpressionValidator>
                                                                    </div>

                                                                    <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Penalty</label>
                                                                        <asp:TextBox runat="server" ID="txtPenalty" Style="width: 60%;" CssClass="form-control" autocomplete="off" onkeypress="return NumberOnly()" />

                                                                    </div>
                                                                </div>

                                                                <div class="row" id="divProvisions" runat="server">
                                                                    <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Provision in Book</label>
                                                                        <asp:TextBox runat="server" ID="txtProvisonbook" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Amount paid</label>
                                                                        <asp:TextBox runat="server" ID="txtamountpaid" Style="width: 60%;" CssClass="form-control" autocomplete="off" onkeypress="return NumberOnly()" />

                                                                    </div>
                                                                    <div style="clear: both; height: 10px"></div>
                                                                    <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Favourable</label>
                                                                        <asp:TextBox runat="server" ID="txtfavourable" Style="width: 60%;" CssClass="form-control" autocomplete="off" />
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            UnFavourable</label>
                                                                        <asp:TextBox runat="server" ID="txtUnfavourable" Style="width: 60%;" CssClass="form-control" autocomplete="off" onkeypress="return NumberOnly()" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <asp:UpdatePanel ID="upCustomField" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:GridView runat="server" ID="grdCustomField" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" AllowPaging="true" AutoPostBack="true" CssClass="table" ShowFooter="true" ShowHeader="true"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdCustomField_Common_RowCommand"
                                                                            OnRowDataBound="grdCustomField_Common_RowDataBound">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("LableID") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LableID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="45%" FooterStyle-Width="45%" HeaderText="Parameter">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; margin-top: 5px;">
                                                                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Label") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Label") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:DropDownListChosen runat="server" ID="ddlFieldName_Footer" DataPlaceHolder="Select"
                                                                                            AllowSingleDeselect="false" DisableSearchThreshold="5" CssClass="form-control" Width="100%">
                                                                                        </asp:DropDownListChosen>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="8%" FooterStyle-Width="8%">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="txtDummyFooter" runat="server" CssClass="form-control" Width="90%" Visible="false"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox runat="server" ID="txtDummyFooter" CssClass="form-control" Visible="false" />
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="45%" FooterStyle-Width="45%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Value">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxLabelValue" runat="server" CssClass="form-control" PlaceHolder="Value" Text='<%# Eval("labelValue") %>'></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox runat="server" AutoPostBack="true" ID="txtFieldValue_Footer" PlaceHolder="Value" CssClass="form-control" />
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" FooterStyle-Width="15%"
                                                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="upCustomFieldDelete" UpdateMode="Conditional" class="mt5">
                                                                                            <ContentTemplate>
                                                                                                <asp:LinkButton CommandArgument='<%# Eval("LableID")%>' ID="lnkBtnDeleteCustomField"
                                                                                                    AutoPostBack="true" CommandName="DeleteCustomField" runat="server"
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="Delete this Custom Parameter"
                                                                                                    OnClientClick="return confirm('Are you sure!! You want to Delete this Custom Parameter?');">
                                                                                                    <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" />
                                                                                                </asp:LinkButton>
                                                                                            </ContentTemplate>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:LinkButton ID="lnkBtnAddCustomField" runat="server" AutoPostBack="true" OnClick="lnkBtnAddCustomField_Click"
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Add Custom Parameter">
                                                                                            <img src='<%# ResolveUrl("~/Images/add_icon_new.png")%>' alt="Add" />
                                                                                        </asp:LinkButton>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>

                                                                        <asp:GridView runat="server" ID="grdCustomField_TaxLitigation" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" AllowPaging="true" AutoPostBack="true" CssClass="table" ShowFooter="true" ShowHeader="true"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdCustomField_Common_RowCommand"
                                                                            OnRowDataBound="grdCustomField_Common_RowDataBound">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("LableID") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LableID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="25%" FooterStyle-Width="25%" HeaderText="Ground(s) of Appeal">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; margin-top: 5px;">
                                                                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Label") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Label") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:DropDownListChosen runat="server" ID="ddlFieldName_Footer" DataPlaceHolder="Select"
                                                                                            AllowSingleDeselect="false" DisableSearchThreshold="5" CssClass="form-control" Width="100%" AutoPostBack="true">
                                                                                        </asp:DropDownListChosen>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Tax Demand">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxLabelValue" runat="server" CssClass="form-control text-right" PlaceHolder="Value" Text='<%# Eval("labelValue") %>'
                                                                                            AutoPostBack="true" OnTextChanged="TextChangedInsideGridView_TextChanged"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox ID="txtFieldValue_Footer" runat="server" CssClass="form-control text-right" PlaceHolder="Value"
                                                                                            AutoPostBack="true" OnTextChanged="TextChangedInsideGridView_TextChanged" />
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Interest">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxInterestValue" runat="server" CssClass="form-control text-right" PlaceHolder="Value" Text='<%# Eval("Interest") %>'
                                                                                            AutoPostBack="true" OnTextChanged="TextChangedInsideGridView_TextChanged"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox ID="txtInterestValue_Footer" runat="server" CssClass="form-control text-right" PlaceHolder="Value"
                                                                                            AutoPostBack="true" OnTextChanged="TextChangedInsideGridView_TextChanged" />
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Penalty">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxPenaltyValue" runat="server" CssClass="form-control text-right" PlaceHolder="Value" Text='<%# Eval("Penalty") %>'
                                                                                            AutoPostBack="true" OnTextChanged="TextChangedInsideGridView_TextChanged"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox ID="txtPenaltyValue_Footer" runat="server" CssClass="form-control text-right" PlaceHolder="Value"
                                                                                            AutoPostBack="true" OnTextChanged="TextChangedInsideGridView_TextChanged" />
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Total">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxRowTotalValue" runat="server" CssClass="form-control text-right" PlaceHolder="Total" Enabled="false" Text='<%# Eval("Total") %>'></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox ID="tbxRowTotalValue_Footer" runat="server" CssClass="form-control text-right" PlaceHolder="Total" Enabled="false" />
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Settlement" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxSettlement" runat="server" CssClass="form-control text-right" PlaceHolder="Settlement" Text='<%# Eval("SettlementValue") %>'></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox ID="tbxSettlement_Footer" runat="server" CssClass="form-control text-right" PlaceHolder="Settlement"></asp:TextBox>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="15%" FooterStyle-Width="15%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Provision in Books">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxProvisionInbooks" runat="server" CssClass="form-control" PlaceHolder="Provision In Books" Text='<%# Eval("ProvisionInBook") %>'></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:TextBox ID="tbxProvisionInbooks_Footer" runat="server" CssClass="form-control" PlaceHolder="Provision In Books"></asp:TextBox>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="5%" FooterStyle-Width="5%"
                                                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="upCustomFieldDelete" UpdateMode="Conditional" class="mt5">
                                                                                            <ContentTemplate>
                                                                                                <asp:LinkButton CommandArgument='<%# Eval("LableID")%>' ID="lnkBtnDeleteCustomField_TaxLitigation"
                                                                                                    AutoPostBack="true" CommandName="DeleteCustomField" runat="server"
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="Delete this Custom Parameter"
                                                                                                    OnClientClick="return confirm('Are you sure!! You want to Delete this Custom Parameter?');">
                                                                                                    <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" />
                                                                                                </asp:LinkButton>
                                                                                            </ContentTemplate>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <asp:LinkButton ID="lnkBtnAddCustomField" runat="server" AutoPostBack="true" OnClick="lnkBtnAddCustomField_TaxLitigation_Click"
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Add Custom Parameter" CssClass="text-right mt5">
                                                                                            <img src='<%# ResolveUrl("~/Images/add_icon_new.png")%>' alt="Add" class="mt5" />
                                                                                        </asp:LinkButton>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                        <%--Notfor emaimi--%>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>

                                                            <div class="form-group col-md-12">
                                                                <asp:UpdatePanel ID="upCustomField_History" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:GridView runat="server" ID="grdCustomField_History" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" AllowPaging="true" AutoPostBack="true" CssClass="table" ShowFooter="false"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowDataBound="grdCustomField_History_RowDataBound">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("LableID") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LableID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="20%" HeaderText="Parameter">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Label") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Label") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="15%" HeaderText="Value"><%--HeaderText="Value"--%>
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                            <asp:Label ID="lblValue" runat="server" Text='<%# Eval("labelValue") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("labelValue") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"><%-- HeaderText="Result"--%>
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblIsAllowed" runat="server" Text='<%# Eval("IsAllowed") %>' Visible="false"></asp:Label>
                                                                                        <asp:DropDownList runat="server" ID="ddlGroundResult" DataPlaceHolder="Select" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                            CssClass="form-control" Width="100%" Enabled="false">
                                                                                            <asp:ListItem Text="Allowed" Value="1"></asp:ListItem>
                                                                                            <asp:ListItem Text="Disallowed" Value="0" Selected="True"></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>


                                                                            </Columns>
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>

                                                                        <asp:GridView runat="server" ID="grdCustomField_TaxLitigation_History" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" AllowPaging="true" AutoPostBack="true" CssClass="table" ShowFooter="false"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowDataBound="grdCustomField_History_RowDataBound">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("LableID") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LableID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="25%" HeaderText="Ground(s) of Appeal">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Label") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Label") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Result" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblIsAllowed" runat="server" Text='<%# Eval("IsAllowed") %>' Visible="false"></asp:Label>
                                                                                        <asp:DropDownList runat="server" ID="ddlGroundResult" DataPlaceHolder="Select" AllowSingleDeselect="false"
                                                                                            DisableSearchThreshold="5" CssClass="form-control" Width="100%" Enabled="false">
                                                                                            <asp:ListItem Text="Allowed" Value="1"></asp:ListItem>
                                                                                            <asp:ListItem Text="Disallowed" Value="0" Selected="True"></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" HeaderText="Tax Demand">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                            <asp:Label ID="lblValue" runat="server" Text='<%# Eval("labelValue") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("labelValue") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" HeaderText="Interest">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblInterest" runat="server" Text='<%# Eval("Interest") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Interest") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" HeaderText="Penalty">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblPenalty" runat="server" Text='<%# Eval("Penalty") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Penalty") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" HeaderText="Total">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblTotalValue" runat="server" Text='<%# Eval("Total") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Total") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="20%" HeaderText="Settlement Value">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblSettlementValue" runat="server" Text='<%# Eval("SettlementValue") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("SettlementValue") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--CaseDetail Panel End-->
                                </div>

                                <div id="divCaseAssignmentDetails" class="row Dashboard-white-widget">
                                    <!--CaseAssignment Panel Start-->
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="view case assignment details">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivCaseAssignmentDetails">
                                                    <a>
                                                        <h2>Case-User Assignment</h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion"
                                                            href="#collapseDivCaseAssignmentDetails">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseDivCaseAssignmentDetails" class="panel-collapse collapse in">
                                                <div class="col-md-12 plr0">
                                                    <asp:ValidationSummary ID="vsCaseUserAssign" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                        ValidationGroup="CaseUserAssignmentPopUpValidationGroup" />
                                                    <asp:CustomValidator ID="cvCaseUserAssignmemt" runat="server" EnableClientScript="False"
                                                        ValidationGroup="CaseUserAssignmentPopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                                </div>
                                                <div class="panel-body">

                                                    <div class="row" id="divGridUserAssignment" runat="server">
                                                        <div class="col-md-12 text-right">
                                                            <asp:LinkButton runat="server" ID="lnkBtnEditUserAssignment" OnClick="btnEditCaseControls_Click"
                                                                data-toggle="tooltip" ToolTip="Edit Case-User Assignment Detail(s)">
                                                                <img src="../../Images/edit_icon_new.png" alt="Edit" title="" />
                                                            </asp:LinkButton>
                                                        </div>
                                                        <asp:GridView runat="server" ID="grdUserAssignment" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false"
                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdUserAssignment_RowCommand"
                                                            OnRowDataBound="grdUserAssignment_RowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                    <ItemTemplate>
                                                                        <%#Container.DataItemIndex+1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="User Type" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblUserType" runat="server" Text='<%# Eval("UserType") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("UserType") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="User" ItemStyle-Width="25%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("UserName") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("UserName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Law Firm" ItemStyle-Width="25%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblLawFirm" runat="server" Text='<%# Eval("LawFirmName")%>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LawFirmName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Role" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="20%">
                                                                    <ItemTemplate>
                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                            <asp:Label ID="lblRole" runat="server" Text='<%# Eval("RoleName") %>'
                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("RoleName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                                    <ItemTemplate>
                                                                        <asp:UpdatePanel runat="server" ID="upDeleteUserAssignment">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")+","+Eval("NoticeCaseInstanceID") %>'
                                                                                    AutoPostBack="true" CommandName="Delete_UserAssignment"
                                                                                    ID="lnkDeleteUserAssignment" runat="server">
                                                                                         <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" />
                                                                                </asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <RowStyle CssClass="clsROWgrid" />
                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                            <EmptyDataTemplate>
                                                                No Records Found
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </div>

                                                    <asp:Panel ID="pnlCaseAssignment" runat="server">
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <%if (Pernodkey == true) %>
                                                                <%{%>
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Additional Owner</label>
                                                                <%} %>
                                                                <% else if (Pernodkey == false) %>
                                                                <%{%>
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Internal User</label>
                                                                <%} %>
                                                                <asp:ListBox ID="lstBoxPerformer" CssClass="form-control" runat="server" SelectionMode="Multiple" Width="70%"></asp:ListBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ErrorMessage=" Internal User can not be empty."
                                                                    ControlToValidate="lstBoxPerformer" runat="server" ValidationGroup="CasePopUpValidationGroup" Display="None" />
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>


                                                                    <div class="form-group col-md-6">
                                                                          <%if (Pernodkey == true) %>
                                                                        <%{%>
                                                                           <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            External Lawyer</label>
                                                                        <%} %>
                                                                        <% else if (Pernodkey == false) %>
                                                                        <%{%>
                                                                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                            Lawyer</label>
                                                                        <%} %>
                                                            
                                                                       
                                                                        <asp:ListBox ID="lstBoxLawyerUser" CssClass="form-control" runat="server" SelectionMode="Multiple" onchange="FirmLawyerChangeAddButton()" Width="60%"></asp:ListBox>

                                                                        <div style="float: right; text-align: center; width: 5%; margin-top: 1%;">
                                                                            <img id="lnkShowAddNewLawyerModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenAddFirmLawyerPopup()" alt="Add New Lawyer" title="Add New Lawyer/External Lawyer" />
                                                                            <asp:LinkButton ID="lnkLawyers" OnClick="lnkLawyers_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                            </asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>

                                                        <div class="row" style="display: none;">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                    Reviewer</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlReviewer" DataPlaceHolder="Select Reviewer" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="60%" />
                                                            </div>
                                                        </div>

                                                    </asp:Panel>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 text-center">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSave_Click" OnClientClick="scrollUpPage()"
                                            ValidationGroup="CasePopUpValidationGroup"></asp:Button>
                                        <asp:Button Text="Clear" runat="server" ID="btnClearCaseDetail" CssClass="btn btn-primary" OnClick="btnClearCaseControls_Click" />
                                    </div>
                                    <!--CaseAssignment Panel End-->
                                </div>

                                <div id="divCaseHistory" runat="server" class="row Dashboard-white-widget">
                                    <!--CaseHistory Panel Start-->
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Case History">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivCaseHistoryDetails">
                                                    <a>
                                                        <h2>Case History</h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion"
                                                            href="#collapseDivCaseHistoryDetails">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseDivCaseHistoryDetails" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <asp:Panel ID="Panel2" runat="server">
                                                        <div class="row">
                                                            <asp:UpdatePanel ID="upCaseHistory" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:GridView runat="server" ID="grdCaseHistory" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                        GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                        PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdCaseHistory_RowCommand"
                                                                        OnPageIndexChanging="grdCaseHistory_PageIndexChanging">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <%#Container.DataItemIndex+1 %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Notice No/Court Case No" ItemStyle-Width="20%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                                        <asp:Label ID="lblCaseNo" runat="server" Text='<%# Eval("NoticeCaseRefNo") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("NoticeCaseRefNo") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Notice/Case Title" ItemStyle-Width="25%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                                        <asp:Label ID="lblNoticeCaseTitle" runat="server" Text='<%# Eval("NoticeCaseTitle") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("NoticeCaseTitle") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Open Date" ItemStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                        <asp:Label ID="lblOpenDate" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                                            Text='<%# Eval("OpenDate") != null ? Convert.ToDateTime(Eval("OpenDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                            ToolTip='<%# Eval("OpenDate") != null ? Convert.ToDateTime(Eval("OpenDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Court" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                        <asp:Label ID="lblCourt" runat="server" Text='<%# Eval("CourtName") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CourtName") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Judge" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                        <asp:Label ID="lblJudge" runat="server" Text='<%# Eval("Judge") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Judge") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                                                <ItemTemplate>
                                                                                    <asp:UpdatePanel runat="server" ID="UpdateHist" UpdateMode="Always">
                                                                                        <ContentTemplate>
                                                                                            <asp:LinkButton CommandArgument='<%# Eval("NoticeCaseInstanceID")+","+ Eval("NoticeCaseType")%>'
                                                                                                AutoPostBack="true" CommandName="ViewNoticeCasePopup" ID="lnkViewCaseHistory" runat="server"
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="View Case Detail">
                                                                                         <img src='<%# ResolveUrl("~/Images/eye.png")%>' alt="View"/>
                                                                                            </asp:LinkButton>
                                                                                        </ContentTemplate>
                                                                                        <Triggers>
                                                                                        </Triggers>
                                                                                    </asp:UpdatePanel>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                        <EmptyDataTemplate>
                                                                            No Records Found
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!--Case History Panel End-->
                                </div>

                                <asp:UpdatePanel ID="upLinkedCases" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div id="divLinkedCases" runat="server" class="row Dashboard-white-widget">
                                            <!--Linked Cases Panel Start-->
                                            <div class="col-lg-12 col-md-12">
                                                <div class="panel panel-default">

                                                    <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Linked Case(s)">
                                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivLinkedCaseDetails">
                                                            <a>
                                                                <h2>Linked Case(s)</h2>
                                                            </a>
                                                            <div class="panel-actions">
                                                                <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivLinkedCaseDetails">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="collapseDivLinkedCaseDetails" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <div class="col-md-12 plr0">
                                                                <asp:ValidationSummary ID="vsLinkedCases" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                    ValidationGroup="LinkedCasesValidationGroup" />
                                                                <asp:CustomValidator ID="cvLinkedCases" runat="server" EnableClientScript="False"
                                                                    ValidationGroup="LinkedCasesValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                                            </div>
                                                            <asp:Panel ID="Panel4" runat="server">
                                                                <div class="row">

                                                                    <asp:GridView runat="server" ID="grdLinkedCases" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                        GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                        PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdLinkedCases_RowCommand"
                                                                        OnPageIndexChanging="grdLinkedCases_PageIndexChanging" OnRowDataBound="grdLinkedCases_RowDataBound">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <%#Container.DataItemIndex+1 %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Court Case No" ItemStyle-Width="20%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                                        <asp:Label ID="lblCaseNo" runat="server" Text='<%# Eval("NoticeCaseRefNo") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("NoticeCaseRefNo") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Case Title" ItemStyle-Width="25%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                                        <asp:Label ID="lblNoticeCaseTitle" runat="server" Text='<%# Eval("NoticeCaseTitle") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("NoticeCaseTitle") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Open Date" ItemStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                        <asp:Label ID="lblOpenDate" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                                            Text='<%# Eval("CaseNoticeDate") != null ? Convert.ToDateTime(Eval("CaseNoticeDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                            ToolTip='<%# Eval("CaseNoticeDate") != null ? Convert.ToDateTime(Eval("CaseNoticeDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Court" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                        <asp:Label ID="lblCourt" runat="server" Text='<%# Eval("CourtName") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CourtName") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Opponent" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                        <asp:Label ID="lblOpponent" runat="server" Text='<%# Eval("PartyName") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("PartyName") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                                                <ItemTemplate>
                                                                                    <asp:UpdatePanel runat="server" ID="UpdateHist" UpdateMode="Always">
                                                                                        <ContentTemplate>
                                                                                            <asp:LinkButton CommandArgument='<%# Eval("LinkedNoticeOrCaseInstanceID")+","+ Eval("NoticeCaseType")%>'
                                                                                                AutoPostBack="true" CommandName="ViewNoticeCasePopup" ID="lnkViewLinkedCase" runat="server"
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="View Case Detail">
                                                                                        <img src='<%# ResolveUrl("~/Images/eye.png")%>' alt="View"/>
                                                                                            </asp:LinkButton>

                                                                                            <asp:LinkButton CommandArgument='<%# Eval("NoticeCaseInstanceID")+","+Eval("LinkedNoticeOrCaseInstanceID")+","+ Eval("NoticeCaseType")%>'
                                                                                                ID="lnkBtnDeleteCaseLinking"
                                                                                                AutoPostBack="true" CommandName="DeleteCaseLinking" runat="server"
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip="Delete this Case Linking"
                                                                                                OnClientClick="return confirm('Are you sure!! You want to Delete this Linkg with Case?');">
                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" />
                                                                                            </asp:LinkButton>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                        <EmptyDataTemplate>
                                                                            No Case Linked yet
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>

                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <!--Linked Cases  Panel End-->
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <div class="form-group col-md-12" style="margin-left: 10px; float: left;">
                                    <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                </div>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="CaseOther" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px; background: white;">
                            <div class="container">
                                <label id="lblCaseStatus" style="color: red; font-size: 14px; font-weight: 400; margin: 1% 0 4px 1%;"></label>
                                <button id="btnRPARefresh" class="k-button" style="margin: 9px; float: right;" onclick="btnRPARefresh_click(event)">Refesh</button>
                                <div id="divCaseDetailsRPA" style="display: none;">
                                    <ul id="panelbarSC" style="background-color: white; border: none;">
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;" class="k-state-active">
                                            <span class="k-link k-state-selected">
                                                <label style="margin: 0px; font-size: 14px;">Case Details</label></span>
                                            <div id="gridCaseDetails" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Earlier Court Details</label></span>
                                            <div id="gridCourtDetails" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Tagged Matters</label></span>
                                            <div id="gridTaggedMatters" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Listing Dates</label></span>
                                            <div id="gridSCListingDates" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Interlocutory Application / Documents</label></span>
                                            <div>
                                                <label style="margin: 17px 0px -16px 20px; font-size: 13px; color: black;">INTERLOCUTARY APPLICATION(s)</label>
                                                <div id="gridinteroculatoryapplications" style="margin: 2%"></div>

                                                <label style="margin: 17px 0px -16px 20px; font-size: 13px; color: black;">OTHER DOCUMENT(s)</label>
                                                <div id="gridotherdocuments" style="margin: 2%"></div>
                                            </div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Court Fees</label></span>
                                            <div id="gridSCCourtFees" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Notices</label></span>
                                            <div id="gridSCNotices" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Defects</label></span>
                                            <div>
                                                <label style="margin: 6px 0px 0px 24px; font-size: 20px; color: black;">Default Details</label>
                                                <hr />
                                                <div id="gridDefects" style="margin: 2%"></div>
                                            </div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Jugdement/Orders</label></span>
                                            <div id="gridSCJugdement" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Office Report</label></span>
                                            <div id="gridSCOfficeReport" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Similarities</label></span>
                                            <div>
                                                <label style="margin: 17px 0px -16px 20px; font-size: 13px; color: black;">Similarities based on State, Bench, Case No. and Judgement Date</label>
                                                <div id="gridSimilarities" style="margin: 2%"></div>
                                            </div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Caveats</label></span>
                                            <div id="gridCaveats" style="margin: 2%"></div>
                                        </li>
                                    </ul>
                                </div>

                                <div id="divCaseDetailsHC" style="display: none">
                                    <ul id="panelbarHC" style="background-color: white; border: none;">
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;" class="k-state-active">
                                            <span class="k-link k-state-selected">
                                                <label style="margin: 0px; font-size: 14px;">Case Details</label></span>
                                            <div id="gridHCCaseDetails" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Case Status</label></span>
                                            <div id="gridHCCaseStatus" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Petitioner and Advocate</label></span>
                                            <div id="gridHCPetRes" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Respondent and Advocate</label></span>
                                            <div id="gridHCResAdvo" style="margin: 2%"></div>
                                        </li>

                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">IA Details</label></span>
                                            <div id="gridIADetails" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Acts</label></span>
                                            <div id="gridHCActs" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Category Details</label></span>
                                            <div id="gridHCCategories" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">History of Case Hearing</label></span>
                                            <div id="gridHCCaseHistory" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Orders</label></span>
                                            <div id="gridHCOrders" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Objections</label></span>
                                            <div id="gridHCobjections" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; margin-bottom: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Documents</label></span>
                                            <div id="gridHCDocuments" style="margin: 2%"></div>
                                        </li>
                                    </ul>
                                </div>

                                <div id="divCaseDetailsDC" style="display: none;">
                                    <ul id="panelbarDC" style="background-color: white; border: none;">

                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;" class="k-state-active">
                                            <span class="k-link k-state-selected">
                                                <label style="margin: 0px; font-size: 14px;">Case Details</label></span>
                                            <div id="gridSCCaseDetails" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Case Status</label></span>
                                            <div id="gridSCCaseStatus" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Petitioner-Advocate</label></span>
                                            <div id="gridSCPetRes" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Respondent-Advocate</label></span>
                                            <div id="gridSCResAdvo" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">History of Case Hearings</label></span>
                                            <div id="gridDCCaseHearingHistory" style="margin: 2%"></div>
                                        </li>
                                        <li style="margin: 8px; border: 1px solid #ceced2; border-radius: 2px;">
                                            <span class="k-link">
                                                <label style="margin: 0px; font-size: 14px;">Acts</label></span>
                                            <div id="gridSCActs" style="margin: 2%"></div>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="CaseDocumentView" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">

                                <div class="col-md-12 plr0">
                                    <asp:ValidationSummary ID="vsCaseDocument" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="CaseDocumentPopUpValidationGroup" />
                                    <asp:CustomValidator ID="cvCaseDocument" runat="server" EnableClientScript="False"
                                        ValidationGroup="CaseDocumentPopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                </div>

                                <div id="divCaseDocuments" class="row Dashboard-white-widget">
                                    <!--Case Document Panel Start-->
                                    <div class="col-lg-12 col-md-12">
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="panel panel-default">



                                                <div id="collapseDivCaseDocument" class="panel-collapse collapse in">
                                                    <div class="panel-body">

                                                        <div class="row col-md-12" id="divCaseDocumentControls" runat="server" style="display: none;">
                                                            <div class="col-md-3 plr0">
                                                                <label style="display: block; float: left; font-size: 13px; color: #333;">
                                                                    Upload Document(s)</label>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <asp:FileUpload ID="CaseFileUpload" runat="server" AllowMultiple="true" CssClass="fileUploadClass" />
                                                            </div>

                                                            <div class="col-md-3 text-right plr0">
                                                                <asp:LinkButton ID="lnkCaseDocumentUpload" runat="server" CssClass="btn btn-primary" OnClick="btnUploadCaseDoc_Click"
                                                                    Text="<i class='fa fa-upload' aria-hidden='true'></i> Upload Document(s)" ToolTip="Upload Selected Document(s)" data-toggle="tooltip"></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12 colpadding0">


                                                                <div id="outerDivFileTags" runat="server" class="col-md-11 colpadding0">
                                                                    <div id="leftArrow" class="scroller scroller-left mt5 mb5 col-md-1 colpadding0" style="width: 4%">
                                                                        <span class="arrow-button arrow-button-right">
                                                                            <i class="fa fa-chevron-left color-black"></i>
                                                                        </span>
                                                                    </div>
                                                                    <div id="rightArrow" class="scroller scroller-right mt5 mb5 col-md-1 colpadding0" style="width: 12%">
                                                                        <span runat="server" class="arrow-button arrow-button-left">
                                                                            <i class="fa fa-chevron-right color-black"></i>
                                                                        </span>
                                                                    </div>
                                                                    <div class="divFileTags col-md-10 colpadding0" style="overflow-x: hidden; overflow-y: hidden; width: 92%; margin-top: 25px">
                                                                        <asp:CheckBoxList ID="lstBoxFileTags" runat="server" CssClass="mt5" RepeatDirection="Horizontal" TextAlign="Left"
                                                                            OnSelectedIndexChanged="lstBoxFileTags_SelectedIndexChanged" AutoPostBack="true">
                                                                        </asp:CheckBoxList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1 colpadding0" style="float: right;">
                                                                    <asp:LinkButton CssClass="btn btn-primary" Style="float: right; display: none"
                                                                        runat="server" ID="lnkBindshowDocumentCase" OnClick="lnkBindshowDocumentCase_Click" />

                                                                    <asp:LinkButton CssClass="btn btn-primary" Style="float: left;margin-left:-71px"
                                                                        runat="server" ID="lnkAddNewDoctype" OnClick="lnkAddNewDoctype_Click" data-toggle="tooltip" ToolTip="Add New Document(s)" data-placement="bottom">
                                                                        <span class="AddNewspan"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>

                                                                    <asp:LinkButton ID="lnkBtnCaseDocument" runat="server" data-placement="bottom" CssClass="btn btn-primary" Style="float: right;"
                                                                ToolTip="Share Case Document(s)" data-toggle="tooltip" OnClick="lnkBtnShareDocument_Click">
                                                           <span class="AddNewspan"><i class='fa fa-share-alt'></i></span>&nbsp;Share</asp:LinkButton>	
                                                                </div>
                                                                <div style="clear: both; height: 5px">
                                                                </div>
                                                                <div id="docsearch" runat="server" class="col-md-12 colpadding0">


                                                                    <div class="divFileTags col-md-11 colpadding0" style="overflow-x: hidden; overflow-y: hidden; margin-top: 10px">
                                                                        <asp:TextBox ID="txtdocsearch" runat="server" CssClass="form-control" placeholder="Search Documents" Style="width: 25%; margin-left: 18px;"></asp:TextBox>
                                                                    </div>

                                                                </div>
                                                                <div class="divFileTags col-md-1 colpadding0" style="overflow-x: hidden; overflow-y: hidden; text-align: right; margin-top: 10px; margin-left: -793px;">
                                                                    <asp:Button ID="btnsearchDoc" runat="server" CssClass="btn btn-primary" OnClick="btnsearchDoc_Click" Text="Apply"></asp:Button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <asp:UpdatePanel ID="upCaseDocUploadPopup" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="form-group col-md-12">

                                                                    <asp:GridView runat="server" ID="grdCaseDocuments" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                        GridLines="None" PageSize="8" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false"
                                                                        PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCommand="grdCaseDocuments_RowCommand"
                                                                        OnRowDataBound="grdCaseDocuments_RowDataBound" OnPageIndexChanging="grdCaseDocuments_PageIndexChanging">
                                                                        <Columns>

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="chkHeader" runat="server" onclick="javascript:checkAllField(this)" />
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblSelectedFileID" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
                                                                                    <asp:CheckBox ID="chkRow" runat="server" onclick="javascript:checkUncheckRowField(this)" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <%#Container.DataItemIndex+1 %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Type" ItemStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblDocType" runat="server" Text='<%# ShowCaseDocType((string)Eval("DocType")) %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Document Type" ItemStyle-Width="20%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblDocType1" runat="server" Text='<%# Eval("TypeName") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Document" ItemStyle-Width="25%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px">
                                                                                        <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Version" ItemStyle-Width="5%" Visible="false">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblDocVersion" runat="server" Text='<%# Eval("Version") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Financial Year" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                                        <asp:Label ID="lblFinancialYear" runat="server" Text='<%# ShowFinancialYear((string)Eval("FinancialYear"))  %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# ShowFinancialYear((string)Eval("FinancialYear"))  %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Uploaded By" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                        <asp:Label ID="lblUploadedBy" runat="server" Text='<%# Eval("CreatedByText") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Uploaded On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                        <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") %>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="35%">
                                                                                <ItemTemplate>
                                                                                    <asp:UpdatePanel runat="server" ID="aa1naa" UpdateMode="Always">
                                                                                        <ContentTemplate>
                                                                                            <asp:LinkButton CommandArgument='<%# Eval("ID")%>' CommandName="DownloadCaseDoc" Visible='<%# ISDocumentVisible((int)Eval("ID")) %>'
                                                                                                ID="lnkBtnDownLoadCaseDoc" runat="server" data-toggle="tooltip" data-placement="bottom" title="Download Document">
                                                                                                <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" /> <%--width="15" height="15"--%>
                                                                                            </asp:LinkButton>

                                                                                            <asp:LinkButton CommandArgument='<%# Eval("ID") %>' AutoPostBack="true" Visible='<%# ISDocumentVisible((int)Eval("ID")) %>' CommandName="ViewCaseOrder"
                                                                                                ID="lnkBtnViewDocCase" runat="server" data-toggle="tooltip" data-placement="bottom" title="View Document">
                                                                                                <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" />
                                                                                            </asp:LinkButton>

                                                                                            <asp:LinkButton CommandArgument='<%# Eval("ID")%>' AutoPostBack="true" CommandName="DeleteCaseDoc"
                                                                                                OnClientClick="return confirm('Are you certain you want to delete this document?');"
                                                                                                ID="lnkBtnDeleteCaseDoc" runat="server" data-toggle="tooltip" data-placement="bottom" title="Delete Document">
                                                                                                <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" /> <%--width="15" height="15"--%>
                                                                                            </asp:LinkButton>
                                                                                            <asp:LinkButton CommandArgument='<%# Eval("ID")%>' AutoPostBack="true" CommandName="shareDoc"
                                                                                                OnClientClick="return confirm('Are you certain you want to share this document?');"
                                                                                                ID="LinkButton2" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip="share">
                                                                                                <img src='<%# ResolveUrl("~/Images/share-icons.png")%>' alt="Share" /> 
                                                                                            </asp:LinkButton>

                                                                                        </ContentTemplate>
                                                                                        <Triggers>
                                                                                            <asp:PostBackTrigger ControlID="lnkBtnDownLoadCaseDoc" />
                                                                                            <asp:PostBackTrigger ControlID="lnkBtnDeleteCaseDoc" />
                                                                                        </Triggers>
                                                                                    </asp:UpdatePanel>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                        <EmptyDataTemplate>
                                                                            No Records Found
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>

                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--Case Document Panel End-->
                            </div>
                        </div>

                    </asp:View>

                    <asp:View ID="TaskView" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">
                                <div id="DivTaskCollapsOne" class="row Dashboard-white-widget">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Task Detail">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#CollapsDivTaskFirstPanel">
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this);ShowTaskDiv();" data-toggle="collapse" data-parent="#accordion" href="#CollapsDivTaskFirstPanel">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="CollapsDivTaskFirstPanel" class="panel-collapse">
                                                <div class="row Dashboard-white-widget">
                                                    <div class="col-lg-12 col-md-12">
                                                        <div class="panel panel-default">
                                                            <div id="collapseDivTaskLogs" class="panel-collapse collapse in">
                                                                <asp:UpdatePanel ID="upCaseTaskActivity" runat="server">
                                                                    <ContentTemplate>
                                                                        <div class="row" runat="server" id="AddNewTaskDiv">
                                                                            <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="LinkButton1" Style="float: right" data-toggle="tooltip" data-target="#CollapsDivTaskFirstPanel" aria-expanded="false"
                                                                                ToolTip="Add New Task" aria-controls="CollapsDivTaskFirstPanel" OnClientClick="HidShowTaskDiv()">
                                                                                <span class="AddNewspan"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>

                                                                        </div>

                                                                        <div class="row">
                                                                            <asp:ValidationSummary ID="ValidationSummary8" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                                ValidationGroup="CvTaskSaveMsgGroup" />
                                                                            <asp:CustomValidator ID="CvTaskSaveMsg" runat="server" EnableClientScript="False"
                                                                                ValidationGroup="CvTaskSaveMsgGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                                                        </div>
                                                                        <asp:GridView runat="server" ID="grdTaskActivity" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                            OnRowCommand="grdTaskActivity_RowCommand" OnRowDataBound="grdTaskActivity_RowDataBound" DataKeyNames="TaskID"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnRowCreated="grdTaskActivity_RowCreated"
                                                                            OnPageIndexChanging="grdTaskActivity_OnPageIndexChanging">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>

                                                                                        <img id="imgCollapseExpand" class="sample" src="/Images/add.png" runat="server" alt="Show" style="cursor: pointer" />
                                                                                        <asp:Panel ID="pnlTaskResponse" runat="server" Style="display: none">
                                                                                            <asp:GridView ID="gvTaskResponses" runat="server" AutoGenerateColumns="false" CssClass="table" AllowPaging="false"
                                                                                                Width="100%" ShowHeaderWhenEmpty="false" GridLines="None" OnRowCommand="grdTaskResponseLog_RowCommand"
                                                                                                OnRowDataBound="grdTaskResponseLog_RowDataBound">
                                                                                                <Columns>
                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                                        <ItemTemplate>
                                                                                                            <%#Container.DataItemIndex+1 %>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Response" ItemStyle-Width="20%">
                                                                                                        <ItemTemplate>
                                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                                                <asp:Label ID="lblTask" runat="server" Text='<%# Eval("Description") %>'
                                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                                                            </div>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Remark" ItemStyle-Width="20%">
                                                                                                        <ItemTemplate>
                                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                                                                                <asp:Label ID="lblTaskDesc" runat="server" Text='<%# Eval("Remark") %>'
                                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Remark") %>'></asp:Label>
                                                                                                            </div>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Responded On" ItemStyle-Width="20%">
                                                                                                        <ItemTemplate>
                                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                                                <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ResponseDate") != null ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                                    data-toggle="tooltip" data-placement="bottom"
                                                                                                                    ToolTip='<%# Eval("ResponseDate") != null ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                                            </div>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                                        <ItemTemplate>
                                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                                                <asp:Label ID="lblTaskResDoc" runat="server" Text='<%# ShowTaskResponseDocCount((long)Eval("TaskID"),(long)Eval("ID")) %>'>  <%--ID=TaskResponseID--%>
                                                                                                                </asp:Label>
                                                                                                            </div>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:UpdatePanel runat="server" ID="upTaskResDocDelete" UpdateMode="Always">
                                                                                                                <ContentTemplate>
                                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID")+","+ Eval("TaskID")%>' CommandName="DownloadTaskResponseDoc"
                                                                                                                        ID="lnkBtnDownloadTaskResDoc" runat="server">
                                                                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download Documents" />
                                                                                                                    </asp:LinkButton>

                                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                                        AutoPostBack="true" CommandName="DeleteTaskResponse"
                                                                                                                        OnClientClick="return confirm('Are you certain you want to delete this Response?');"
                                                                                                                        ID="lnkBtnDeleteTaskResponse" runat="server">
                                                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete Response" />
                                                                                                                    </asp:LinkButton>
                                                                                                                </ContentTemplate>
                                                                                                                <Triggers>
                                                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDownloadTaskResDoc" />
                                                                                                                    <asp:PostBackTrigger ControlID="lnkBtnDeleteTaskResponse" />
                                                                                                                </Triggers>
                                                                                                            </asp:UpdatePanel>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                                <RowStyle CssClass="clsROWgrid" />
                                                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                                                <EmptyDataTemplate>
                                                                                                    No Response Submitted yet.
                                                                                                </EmptyDataTemplate>
                                                                                            </asp:GridView>
                                                                                        </asp:Panel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Hearing" ItemStyle-Wrap="true" ItemStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                                            <asp:Label ID="lblTaskHearingRef" runat="server" Text='<%# Eval("HearingRefNo") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("HearingRefNo") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task" ItemStyle-Wrap="true" ItemStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                                            <asp:Label ID="lblTask" runat="server" Text='<%# Eval("TaskTitle") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task Description" ItemStyle-Width="15%" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                            <asp:Label ID="lblTaskDesc" runat="server" Text='<%# Eval("TaskDesc") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskDesc") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Assigned To" ItemStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                            <asp:Label ID="lblAssignedTo" runat="server" Text='<%# Eval("AssignToName") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignToName") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Created By" ItemStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                            <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedByText") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                            <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="5%" FooterStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblTaskStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="aa1aa" UpdateMode="Conditional">
                                                                                            <ContentTemplate>
                                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 170px;">
                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>' CssClass="btn btn-primary"
                                                                                                        OnClientClick="return confirm('Are you sure!! You want to close this Task?');"
                                                                                                        AutoPostBack="true" CommandName="CloseTask" Text="Close Task"
                                                                                                        ID="lnkBtnResCloseTask" runat="server">
                                                                                                    </asp:LinkButton>

                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>'
                                                                                                        AutoPostBack="true" CommandName="EditTaskDoc"
                                                                                                        ID="lnkBtnEditTaskDoc" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit" ToolTip="Edit Task" data-toggle="tooltip" />
                                                                                                    </asp:LinkButton>

                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>'
                                                                                                        AutoPostBack="true" CommandName="TaskReminder"
                                                                                                        ID="lnkBtnResTaskReminder" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/send_icon.png")%>' alt="Send Reminder" ToolTip="Send Reminder" data-toggle="tooltip" /> <%--width="15" height="15" CssClass="btn btn-primary"--%>
                                                                                                    </asp:LinkButton>

                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>'
                                                                                                        AutoPostBack="true" CommandName="DeleteTask"
                                                                                                        OnClientClick="return confirm('Are you sure!! You want to Delete this Task Detail?');"
                                                                                                        ID="lnkBtnResDeleteTask" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" ToolTip="Delete Task" data-toggle="tooltip" />
                                                                                                    </asp:LinkButton>
                                                                                                </div>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnResDeleteTask" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="DivTaskCollapsTwo" class="row Dashboard-white-widget">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="view Task details">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#CollapsDivTaskSecondPanel">
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#CollapsDivTaskSecondPanel">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="CollapsDivTaskSecondPanel" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <asp:Panel ID="Panel1" runat="server">
                                                        <div class="container">
                                                            <asp:Panel ID="pnlTask" runat="server">
                                                                <div class="row">
                                                                    <asp:ValidationSummary ID="ValidationSummary5" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                        ValidationGroup="CasePopUpTaskValidationGroup" />
                                                                    <asp:CustomValidator ID="cvCasePopUpTask" runat="server" EnableClientScript="true"
                                                                        ValidationGroup="CasePopUpTaskValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                                                    <asp:Label ID="Labelmsg" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
                                                                </div>
                                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                    <ContentTemplate>
                                                                        <div class="row">
                                                                            <div class="form-group col-md-6">
                                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Hearing</label>
                                                                                <asp:DropDownListChosen ID="ddlHearingRefNo" CssClass="form-control" runat="server"
                                                                                    AllowSingleDeselect="false" DisableSearchThreshold="5" Width="66%" onchange="ddlRefNoChange()">
                                                                                </asp:DropDownListChosen>
                                                                            </div>

                                                                            <div class="form-group col-md-6 input-group date" id="divHearingDate">
                                                                                <div>
                                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                    <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                        Hearing Date</label>
                                                                                </div>

                                                                                <div class="col-md-6 input-group date" style="width: 65%">
                                                                                    <span class="input-group-addon">
                                                                                        <span class="fa fa-calendar color-black"></span>
                                                                                    </span>
                                                                                    <asp:TextBox runat="server" placeholder="Hearing Date" class="form-control" ID="tbxTaskHearingDate" />
                                                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ErrorMessage="Provide Hearing Date"
                                                                                    ControlToValidate="tbxTaskHearingDate" runat="server" ValidationGroup="CasePopUpTaskValidationGroup" Display="None" />--%>
                                                                                </div>

                                                                                <div style="float: left; text-align: center; width: 5%; margin-top: 1%;">
                                                                                    <asp:LinkButton ID="lnkSaveRefNo" runat="server" OnClick="btnSaveRefNo_Click">
                                                                                         <img src="../../Images/Save-icon.png" alt="Save" title="Save" /></asp:LinkButton>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="form-group col-md-12">
                                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Task Title</label>
                                                                                <asp:TextBox ID="tbxTaskTitle" runat="server" CssClass="form-control" Width="85.5%" MaxLength="100"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="rfvTaskTitle" ErrorMessage="Provide Task Title"
                                                                                    ControlToValidate="tbxTaskTitle" runat="server" ValidationGroup="CasePopUpTaskValidationGroup" Display="None" />
                                                                                <asp:RegularExpressionValidator ValidationGroup="CasePopUpTaskValidationGroup" Display="None" ControlToValidate="tbxTaskTitle" ID="revTaskTitle"
                                                                                    ValidationExpression="^[\s\S]{1,100}$" runat="server" ErrorMessage="Maximum 100 characters required in Task Title."></asp:RegularExpressionValidator>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="form-group col-md-12">
                                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Task Description</label>
                                                                                <asp:TextBox ID="tbxTaskDesc" runat="server" CssClass="form-control" Style="width: 85.5%; min-height: 115px;" TextMode="MultiLine"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="rfvTaskDesc" ErrorMessage="Provide Task Description"
                                                                                    ControlToValidate="tbxTaskDesc" runat="server" ValidationGroup="CasePopUpTaskValidationGroup" Display="None" />
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="form-group col-md-6 input-group date">
                                                                                <div>
                                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                    <label style="width: 24.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                        Due Date</label>
                                                                                </div>

                                                                                <div class="col-md-6 input-group date" style="width: 72%">
                                                                                    <span class="input-group-addon">
                                                                                        <span class="fa fa-calendar color-black"></span>
                                                                                    </span>
                                                                                    <asp:TextBox runat="server" placeholder="Due Date" class="form-control" ID="tbxTaskDueDate" />
                                                                                </div>
                                                                                <asp:RequiredFieldValidator ID="rfvTaskDueDate" ErrorMessage="Provide Due Date"
                                                                                    ControlToValidate="tbxTaskDueDate" runat="server" ValidationGroup="CasePopUpTaskValidationGroup" Display="None" />
                                                                            </div>

                                                                            <div class="form-group col-md-6">
                                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Priority</label>
                                                                                <asp:DropDownListChosen runat="server" ID="ddlTaskPriority" DataPlaceHolder="Select Task Priority" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                    CssClass="form-control" Width="70%">
                                                                                    <asp:ListItem Text="High" Value="1"></asp:ListItem>
                                                                                    <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                                                                                    <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                                                                                </asp:DropDownListChosen>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="form-group col-md-12">
                                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                                <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Expected Outcome</label>
                                                                                <asp:TextBox ID="tbxExpOutcome" runat="server" CssClass="form-control" Width="85.5%" TextMode="MultiLine"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="form-group col-md-6">
                                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                    Assign To</label>
                                                                                <label style="width: 15%; display: block; float: left; font-size: 13px; color: #333;">Internal User</label>
                                                                                <asp:DropDownListChosen ID="ddlTaskLawyerListInternal" CssClass="form-control" runat="server" Width="51%" AutoPostBack="true">
                                                                                </asp:DropDownListChosen>
                                                                                <asp:RequiredFieldValidator ID="rfvTaskUser" ErrorMessage="Please Select User to Assign Task"
                                                                                    ControlToValidate="ddlTaskLawyerListInternal" runat="server" ValidationGroup="CasePopUpTaskValidationGroup" Display="None" />
                                                                            </div>

                                                                            <div class="form-group col-md-6">
                                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">External User</label>
                                                                                <div style="float: left; width: 70%">
                                                                                    <asp:DropDownListChosen runat="server" ID="ddlTaskUserExternal" DataPlaceHolder="Select User" Onchange="ddlTaskUserChange()" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                        CssClass="form-control" Width="100%" />
                                                                                </div>
                                                                                <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                                    <img id="lnkShowAddNewOwnerModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenAddUserDetailPop()" alt="Add New User" title="Add New User" />
                                                                                    <asp:LinkButton ID="lnkBtnAssignUser" OnClick="lnkAddNewUser_Click" Style="float: right; display: none;" Width="100%" runat="server"> 
                                                                                    </asp:LinkButton>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div class="row">
                                                                            <div class="form-group col-md-12">
                                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                                <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Remark</label>
                                                                                <asp:TextBox ID="tbxTaskRemark" runat="server" CssClass="form-control" Width="85.5%"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="form-group col-md-12">
                                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                                <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Relevant Document(s)</label>
                                                                                <div style="width: 100%;">
                                                                                    <div style="width: 50%; float: left;">
                                                                                        <asp:FileUpload ID="fuTaskDocUpload" runat="server" AllowMultiple="true" CssClass="fileUploadClass" />
                                                                                    </div>
                                                                                </div>
                                                                                <asp:TextBox ID="tbxTaskID" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row" runat="server" id="DivTaskEdit" visible="false">
                                                                            <asp:GridView ID="grdTaskEditDoc" runat="server" AutoGenerateColumns="false" CssClass="table" AllowPaging="false"
                                                                                Width="100%" ShowHeaderWhenEmpty="true" GridLines="None" OnRowCommand="grdTaskEditDoc_RowCommand"
                                                                                PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                        <ItemTemplate>
                                                                                            <%#Container.DataItemIndex+1 %>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="FileName" ItemStyle-Width="50%">
                                                                                        <ItemTemplate>
                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                                                                <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                                                        <ItemTemplate>
                                                                                            <asp:UpdatePanel runat="server" ID="TaskEditDocUpdate" UpdateMode="Always">
                                                                                                <ContentTemplate>
                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID")%>' CommandName="DeleteTaskEditDocument"
                                                                                                        ID="lnkbtnTaskDelete" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="delete" title="Delete Documents" />
                                                                                                    </asp:LinkButton>
                                                                                                    <asp:LinkButton
                                                                                                        CommandArgument='<%# Eval("ID")%>' CommandName="DownloadTaskEditDocument"
                                                                                                        ID="lnkDownloadTaskEditDocument" runat="server">
                                                                                                <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="DownLoad" title="DownLoad Documents" />
                                                                                                    </asp:LinkButton>

                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                        AutoPostBack="true" CommandName="ViewTaskEditDocument"
                                                                                                        ID="lnkViewTaskEditDocument" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Document" />
                                                                                                    </asp:LinkButton>
                                                                                                </ContentTemplate>
                                                                                                <Triggers>
                                                                                                    <asp:PostBackTrigger ControlID="lnkbtnTaskDelete" />
                                                                                                    <asp:PostBackTrigger ControlID="lnkDownloadTaskEditDocument" />

                                                                                                </Triggers>
                                                                                            </asp:UpdatePanel>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <RowStyle CssClass="clsROWgrid" />
                                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                                <EmptyDataTemplate>
                                                                                    No Response Submitted yet.
                                                                                </EmptyDataTemplate>
                                                                            </asp:GridView>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="form-group col-md-12" style="text-align: center;">
                                                                                <asp:Button Text="Save" runat="server" ID="btnTaskSave" CssClass="btn btn-primary" OnClick="btnTaskSave_Click" OnClientClick="if (!CheckValidation()) return false;"
                                                                                    ValidationGroup="CasePopUpTaskValidationGroup"></asp:Button>
                                                                                <asp:Button Text="Clear" runat="server" ID="btnTaskClear" CssClass="btn btn-primary" OnClick="btnClearTask_Click" />
                                                                            </div>
                                                                        </div>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnTaskSave" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </asp:Panel>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!--CaseAssignment Panel End-->
                                </div>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="HearingView" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">
                                <div id="secondTabAccordion" class="row Dashboard-white-widget">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="view hearing detail">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#CollapsDivHearingFirstPanel">
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this);ShowHearingDiv();" data-toggle="collapse" data-parent="#accordion" href="#CollapsDivHearingFirstPanel">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="CollapsDivHearingFirstPanel" class="panel-collapse">
                                                <div class="row Dashboard-white-widget">
                                                    <div class="col-lg-12 col-md-12">
                                                        <div class="panel panel-default">
                                                            <div id="collapseDivHearingLogsFirstView" class="panel-collapse collapse in">
                                                                <asp:UpdatePanel ID="upResponseDocUpload" runat="server">
                                                                    <ContentTemplate>
                                                                        <div class="row" runat="server" id="AddNewHearingDiv">
                                                                            <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="lnkAddhearing" data-toggle="tooltip" data-target="#CollapsDivHearingFirstPanel" ToolTip="Add New Hearing" aria-expanded="false" aria-controls="CollapsDivHearingFirstPanel" OnClientClick="HidShowHearingDiv()" OnClick="lnkAddhearing_Click" Style="float: right">
                                                                                <span class="AddNewspan"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                                                                        </div>

                                                                        <div class="row">
                                                                            <asp:ValidationSummary ID="ValidationSummary9" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                                ValidationGroup="CasePopUpTaskValidationGroup" />
                                                                            <asp:CustomValidator ID="CvHearingSaveMsg" runat="server" EnableClientScript="False"
                                                                                ValidationGroup="CasePopUpTaskValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                                                        </div>
                                                                        <asp:GridView runat="server" ID="grdResponseLog" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" DataKeyNames="ResponseID"
                                                                            OnPageIndexChanging="grdResponseLog_OnPageIndexChanging" OnRowCommand="grdResponseLog_RowCommand"
                                                                            OnRowDataBound="grdResponseLog_RowDataBound" OnRowCreated="grdResponseLog_RowCreated">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <img id="imgCollapseExpand" class="sample" src="/Images/add.png" runat="server" alt="Show" style="cursor: pointer" />
                                                                                        <asp:Panel ID="pnlTask" runat="server" Style="display: none">
                                                                                            <asp:UpdatePanel ID="upResponseTask" runat="server">
                                                                                                <ContentTemplate>
                                                                                                    <asp:GridView runat="server" ID="grdResTaskActivity" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                                                        GridLines="None" PageSize="5" AllowPaging="false" AutoPostBack="true" CssClass="table" Width="100%"
                                                                                                        OnRowCommand="grdTaskActivity_RowCommand" OnRowDataBound="grdTaskActivity_RowDataBound" DataKeyNames="TaskID"
                                                                                                        PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="grdResTaskActivity_OnPageIndexChanging">
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                                                                                <ItemTemplate>
                                                                                                                    <img id="imgCollapseExpand" class="sample" src="/Images/expand.png" runat="server" alt="Show" style="cursor: pointer" />
                                                                                                                    <asp:Panel ID="pnlTaskResponse" runat="server" Style="display: none">
                                                                                                                        <asp:GridView ID="gvTaskResponses" runat="server" AutoGenerateColumns="false" CssClass="table" AllowPaging="false"
                                                                                                                            Width="100%" ShowHeaderWhenEmpty="true" GridLines="None" OnRowCommand="grdTaskResponseLog_RowCommand"
                                                                                                                            OnRowDataBound="grdTaskResponseLog_RowDataBound" PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
                                                                                                                            <Columns>
                                                                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateField>

                                                                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Response" ItemStyle-Width="20%">
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                                                                            <asp:Label ID="lblTask" runat="server" Text='<%# Eval("Description") %>'
                                                                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                                                                                        </div>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateField>

                                                                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Remark" ItemStyle-Width="20%">
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                                                                                                            <asp:Label ID="lblTaskDesc" runat="server" Text='<%# Eval("Remark") %>'
                                                                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Remark") %>'></asp:Label>
                                                                                                                                        </div>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateField>

                                                                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Responded On" ItemStyle-Width="20%">
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                                                                            <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ResponseDate") != null ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                                                                data-toggle="tooltip" data-placement="bottom"
                                                                                                                                                ToolTip='<%# Eval("ResponseDate") != null ? Convert.ToDateTime(Eval("ResponseDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                                                                        </div>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateField>

                                                                                                                                <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                                                                            <asp:Label ID="lblTaskResDoc" runat="server" Text='<%# ShowTaskResponseDocCount((long)Eval("TaskID"),(long)Eval("ID")) %>'>  <%--ID=TaskResponseID--%>
                                                                                                                                            </asp:Label>
                                                                                                                                        </div>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateField>

                                                                                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <asp:UpdatePanel runat="server" ID="upTaskResDocDelete" UpdateMode="Always">
                                                                                                                                            <ContentTemplate>
                                                                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")+","+ Eval("TaskID")%>' CommandName="DownloadTaskResponseDoc"
                                                                                                                                                    ID="lnkBtnDownloadResTaskResDoc" runat="server">
                                                                                                                                                    <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download Documents" />
                                                                                                                                                </asp:LinkButton>

                                                                                                                                            </ContentTemplate>
                                                                                                                                            <Triggers>
                                                                                                                                                <asp:PostBackTrigger ControlID="lnkBtnDownloadResTaskResDoc" />
                                                                                                                                            </Triggers>
                                                                                                                                        </asp:UpdatePanel>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateField>
                                                                                                                            </Columns>
                                                                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                                                                            <EmptyDataTemplate>
                                                                                                                                No Response Submitted yet.
                                                                                                                            </EmptyDataTemplate>
                                                                                                                        </asp:GridView>
                                                                                                                    </asp:Panel>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                                                <ItemTemplate>
                                                                                                                    <%#Container.DataItemIndex+1 %>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Hearing" ItemStyle-Wrap="true" ItemStyle-Width="20%" Visible="false">
                                                                                                                <ItemTemplate>
                                                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                                                        <asp:Label ID="lblTaskHearingRef" runat="server" Text='<%# Eval("HearingRefNo") %>'
                                                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("HearingRefNo") %>'></asp:Label>
                                                                                                                    </div>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task" ItemStyle-Wrap="true" ItemStyle-Width="20%">
                                                                                                                <ItemTemplate>
                                                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                                                        <asp:Label ID="lblTask" runat="server" Text='<%# Eval("TaskTitle") %>'
                                                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                                                                    </div>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Task Description" ItemStyle-Width="20%">
                                                                                                                <ItemTemplate>
                                                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                                                        <asp:Label ID="lblTaskDesc" runat="server" Text='<%# Eval("TaskDesc") %>'
                                                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("TaskDesc") %>'></asp:Label>
                                                                                                                    </div>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Assigned To" ItemStyle-Width="20%">
                                                                                                                <ItemTemplate>
                                                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                                                        <asp:Label ID="lblAssignedTo" runat="server" Text='<%# Eval("AssignToName") %>'
                                                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignToName") %>'></asp:Label>
                                                                                                                    </div>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Due Date" ItemStyle-Width="20%">
                                                                                                                <ItemTemplate>
                                                                                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                                                        <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("ScheduleOnDate") != null ? Convert.ToDateTime(Eval("ScheduleOnDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                                                    </div>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="10%" FooterStyle-Width="10%">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Label ID="lblTaskStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>

                                                                                                            <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%" Visible="false">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:UpdatePanel runat="server" ID="aa1aa" UpdateMode="Conditional">
                                                                                                                        <ContentTemplate>
                                                                                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                                                                <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>' CssClass="btn btn-primary"
                                                                                                                                    OnClientClick="return confirm('Are you sure!! You want to close this Task?');"
                                                                                                                                    AutoPostBack="true" CommandName="CloseTask" Text="Close Task"
                                                                                                                                    ID="lnkBtnCloseTask" runat="server">
                                                                                                                                </asp:LinkButton>

                                                                                                                                <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>'
                                                                                                                                    AutoPostBack="true" CommandName="TaskReminder"
                                                                                                                                    ID="lnkBtnTaskReminder" runat="server">
                                                                                                                                    <img src='<%# ResolveUrl("~/Images/send_icon.png")%>' alt="Send Reminder" title="Send Reminder"  /> <%--width="15" height="15" CssClass="btn btn-primary"--%>
                                                                                                                                </asp:LinkButton>

                                                                                                                                <asp:LinkButton CommandArgument='<%# Eval("TaskID")%>'
                                                                                                                                    AutoPostBack="true" CommandName="DeleteTask"
                                                                                                                                    OnClientClick="return confirm('Are you sure!! You want to Delete this Task Detail?');"
                                                                                                                                    ID="lnkBtnDeleteTask" runat="server">
                                                                                                                                    <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete" />
                                                                                                                                </asp:LinkButton>
                                                                                                                            </div>
                                                                                                                        </ContentTemplate>
                                                                                                                        <Triggers>
                                                                                                                            <asp:PostBackTrigger ControlID="lnkBtnDeleteTask" />
                                                                                                                        </Triggers>
                                                                                                                    </asp:UpdatePanel>
                                                                                                                </ItemTemplate>
                                                                                                            </asp:TemplateField>

                                                                                                        </Columns>
                                                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                                                        <EmptyDataTemplate>
                                                                                                            No Records Found
                                                                                                        </EmptyDataTemplate>
                                                                                                    </asp:GridView>
                                                                                                </ContentTemplate>
                                                                                            </asp:UpdatePanel>
                                                                                        </asp:Panel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Sr" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Hearing" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%" HeaderStyle-Width="10%" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                            <asp:Label ID="lblRefID" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                                                ToolTip='<%# Eval("RefID") %>' Text='<%# Eval("RefID") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Hearing" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%" HeaderStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                            <asp:Label ID="lblHearingRefNo" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                                                ToolTip='<%# Eval("HearingRefNo") %>' Text='<%# Eval("HearingRefNo") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Description" ItemStyle-Width="20%" HeaderStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                                            <asp:Label ID="lblResDesc" runat="server" Text='<%# Eval("Description") %>'
                                                                                                ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Next Hearing" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%" HeaderStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                            <asp:Label ID="lblReminderDate" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                                                ToolTip='<%# Eval("ReminderDate") != null ? Convert.ToDateTime(Eval("ReminderDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                Text='<%# Eval("ReminderDate") != null ? Convert.ToDateTime(Eval("ReminderDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Created By" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                            <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedByText") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Created On" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                            <asp:Label ID="lblUploadedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("CreatedOn") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                            <asp:Label ID="lblResDoc" runat="server" Text='<%# ShowCaseResponseDocCount((long)Eval("CaseInstanceID"),(long)Eval("ResponseID")) %>'>
                                                                                            </asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="upResDocDelete" UpdateMode="Always">
                                                                                            <ContentTemplate>
                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ResponseID")+","+ Eval("CaseInstanceID")%>'
                                                                                                    AutoPostBack="true" CommandName="EditCaseOrderHearing"
                                                                                                    ID="lnkBtnEditResponseDoc" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit" ToolTip="Edit Task" data-toggle="tooltip"  />
                                                                                                </asp:LinkButton>

                                                                                                <asp:LinkButton ID="lnkBtnDownLoadResponseDoc" runat="server" Visible='<%# canVisible((long)Eval("CaseInstanceID"),(long)Eval("ResponseID")) %>'
                                                                                                    CommandArgument='<%# Eval("ResponseID")+","+ Eval("CaseInstanceID")%>' CommandName="DownloadResponseDoc">
                                                                                                <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="DownLoad" title="DownLoad Documents" />
                                                                                                </asp:LinkButton>

                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ResponseID")+","+ Eval("CaseInstanceID")%>' Visible='<%# canVisible((long)Eval("CaseInstanceID"),(long)Eval("ResponseID")) %>'
                                                                                                    AutoPostBack="true" CommandName="ViewCaseOrderHearing" ID="lnkBtnHearingDocView" runat="server">
                                                                                                    <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Document" />
                                                                                                </asp:LinkButton>

                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ResponseID")+","+ Eval("CaseInstanceID")%>'
                                                                                                    AutoPostBack="true" CommandName="DeleteResponse" ID="lnkBtnDeleteResponse" runat="server"
                                                                                                    OnClientClick="return confirm('Are you certain you want to delete this Hearing Detail?');">
                                                                                                <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete Hearing Detail" />
                                                                                                </asp:LinkButton>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnDownLoadResponseDoc" />
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnDeleteResponse" />
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnEditResponseDoc" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />

                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="DivHearingCollapsTwo" class="row Dashboard-white-widget">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="view Task details">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivHearingLogsSecond">
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivHearingLogsSecond">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="collapseDivHearingLogsSecond" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="container">
                                                        <asp:UpdatePanel ID="upResponseTask" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Panel ID="pnlCaseHearing" runat="server">
                                                                    <div class="row">
                                                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                            ValidationGroup="CasePopUpHearingValidationGroup" />
                                                                        <asp:CustomValidator ID="cvCasePopUpResponse" runat="server" EnableClientScript="False"
                                                                            ValidationGroup="CasePopUpHearingValidationGroup" Display="None" />
                                                                        <asp:Label ID="lblValidResForDoc" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label style="width: 28%; display: block; float: left; font-size: 13px; color: #333;">Hearing</label>
                                                                            <asp:DropDownListChosen ID="ddlTabHearingRef" CssClass="form-control" runat="server"
                                                                                AllowSingleDeselect="false" DisableSearchThreshold="5" Width="56%" onchange="ddlTabHearingRefNoChange()">
                                                                            </asp:DropDownListChosen>
                                                                        </div>

                                                                        <div class="form-group col-md-6 input-group date" id="divTabHearingDate">
                                                                            <div>
                                                                                <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                <label style="width: 15%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                    Hearing Date</label>
                                                                            </div>

                                                                            <div class="col-md-6 input-group date" style="width: 58%">
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar color-black"></span>
                                                                                </span>
                                                                                <asp:TextBox runat="server" placeholder="Hearing Date" class="form-control" ID="tbxTabHearingDate" />
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Provide Hearing Date"
                                                                                    ControlToValidate="tbxTabHearingDate" runat="server" ValidationGroup="CasePopUpTaskValidationGroup" Display="None" />
                                                                            </div>

                                                                            <div style="float: left; text-align: center; width: 10%; margin-top: 1%;">
                                                                                <asp:LinkButton ID="lnkTabSaveRefNo" runat="server" OnClick="btnTabSaveRefNo_Click">
                                                                                <img src="../../Images/Save-icon.png" alt="Save" title="Save" /></asp:LinkButton>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <%--Added By Gaurav --%>
                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <%--                                                                            <asp:UpdatePanel ID="UpdatePanel17" runat="server" UpdateMode="Conditional">
                                                                                <ContentTemplate>--%>
                                                                            <label style="width: 2%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Law Firm</label>
                                                                            <asp:DropDownListChosen ID="DropDownListHearingLawFirm" CssClass="form-control" AutoPostBack="true" runat="server"
                                                                                DataPlaceHolder="Select Law Firm" Width="56%" OnSelectedIndexChanged="DropDownListHearingLawFirm_SelectedIndexChanged">
                                                                            </asp:DropDownListChosen>
                                                                            <%--         </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="DropDownListHearingLawFirm" EventName="SelectedIndexChanged" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>--%>
                                                                        </div>
                                                                        <asp:UpdatePanel ID="UpdatePanel16" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <div class="form-group col-md-6">
                                                                                    <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                                                    <label style="width: 17%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                        <%if (Pernodkey) {%>
                                                                                        External<br />Lawyer
                                                                                        <%} else {%>
                                                                                        Lawyer
                                                                                        <%} %></label>
                                                                                    <asp:ListBox ID="ListBoxLawyerHearing" DataPlaceHolder="Select Law Firm" CssClass="form-control" runat="server" AutoPostBack="false" SelectionMode="Multiple" Width="56%"></asp:ListBox>
                                                                                </div>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                        <div class="form-group col-md-6">
                                                                            <label style="width: 2%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                Budget</label>
                                                                            <asp:TextBox runat="server" ID="txtHearingBudget" Style="width: 56%;" CssClass="form-control" MaxLength="100" autocomplete="off" />
                                                                            <span id="budgetHearing" style="color: Red; display: none; margin-left: 173px;">Input digits (0 - 9)</span>
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                                                                                ValidationGroup="CasePopUpValidationGroup" ErrorMessage="Please enter a valid Case Budget."
                                                                                ControlToValidate="txtHearingBudget" ValidationExpression="[0-9]+(\.[0-9][0-9]?)?"></asp:RegularExpressionValidator>
                                                                        </div>
                                                                    </div>

                                                                    <%--  Added By Gaurav--%>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Hearing Description</label>
                                                                            <asp:TextBox ID="tbxResponseDesc" runat="server" CssClass="form-control" Style="width: 86%; min-height: 115px;" TextMode="MultiLine"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                            <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Remark</label>
                                                                            <asp:TextBox ID="tbxResponseRemark" runat="server" CssClass="form-control" Width="86%"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12 input-group date">
                                                                            <div>
                                                                                <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                                <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                    Next Hearing Date</label>
                                                                            </div>

                                                                            <div class="col-md-6 input-group date" style="width: 30%; margin-left: -15px;">
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar color-black"></span>
                                                                                </span>
                                                                                <asp:TextBox runat="server" placeholder="Hearing Date" CssClass="form-control" ID="tbxReminderDate" />
                                                                            </div>
                                                                            <asp:TextBox ID="tbxResponseID" autocomplete="off" runat="server" CssClass="form-control" Style="display: none;"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="form-group col-md-12 input-group date">
                                                                            <div>
                                                                                <label style="width: 1%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                                <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                    Remind Me On</label>
                                                                            </div>

                                                                            <div class="col-md-6 input-group date" style="width: 30%; margin-left: -15px;">
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar color-black"></span>
                                                                                </span>
                                                                                <asp:TextBox runat="server" placeholder="Remind Me On" CssClass="form-control" ID="tbxRemindMeOn" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label style="width: 14%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                Relevant Document(s)</label>
                                                                            <div style="width: 100%;">
                                                                                <div style="width: 50%; float: left;">
                                                                                    <asp:FileUpload ID="fuResponseDocUpload" runat="server" AllowMultiple="true" CssClass="fileUploadClass" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row" runat="server" id="divHearingEditdoc" visible="false">
                                                                        <asp:GridView ID="GrdHearingEditDocument" runat="server" AutoGenerateColumns="false" CssClass="table" AllowPaging="false"
                                                                            Width="100%" ShowHeaderWhenEmpty="true" GridLines="None" OnRowCommand="GrdHearingEditDocument_RowCommand"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="FileName" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                            <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="HearingDocDelete" UpdateMode="Always">
                                                                                            <ContentTemplate>
                                                                                                <asp:LinkButton
                                                                                                    CommandArgument='<%# Eval("ID")%>' CommandName="DownloadHearingEditDocument"
                                                                                                    ID="lnkDownloadHearingEditDocument" runat="server">
                                                                                                <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="DownLoad" title="DownLoad Documents" />
                                                                                                </asp:LinkButton>

                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                    AutoPostBack="true" CommandName="ViewHearingEditDocument"
                                                                                                    ID="lnkViewHearingEditDocument" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Document" />
                                                                                                </asp:LinkButton>
                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")%>' CommandName="DeleteHearingEditDocument"
                                                                                                    ID="lnkbtnHearingDelete" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="delete" title="Delete Documents" />
                                                                                                </asp:LinkButton>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lnkbtnHearingDelete" />
                                                                                                <asp:PostBackTrigger ControlID="lnkDownloadHearingEditDocument" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Response Submitted yet.
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12" style="text-align: center;">
                                                                            <asp:Button Text="Save" runat="server" ID="btnSaveHearing" CssClass="btn btn-primary" OnClick="btnSaveHearing_Click" OnClientClick="if (!CheckValidationForRes()) return false;"
                                                                                ValidationGroup="CasePopUpHearingValidationGroup"></asp:Button>
                                                                            <asp:Button Text="Clear" runat="server" ID="btnHearingClear" CssClass="btn btn-primary" OnClick="btnHearingClear_Click" />
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnSaveHearing" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>

                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="OrderView" runat="server">
                        <div style="width: 100%; float: left;">
                            <div class="container">
                                <div id="OrderTabAccordion" class="row Dashboard-white-widget">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Case Order Detail">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#OrderTabAccordion" href="#CollapsDivOrderFirstPanel">
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this);ShowOrderDiv();" data-toggle="collapse" data-parent="#OrderTabAccordion" href="#CollapsDivOrderFirstPanel">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="CollapsDivOrderFirstPanel" class="panel-collapse">
                                                <div class="row Dashboard-white-widget">
                                                    <div class="col-lg-12 col-md-12">
                                                        <div class="panel panel-default">
                                                            <div id="collapseDivOrderLogsFirstView" class="panel-collapse collapse in">
                                                                <asp:UpdatePanel ID="upCaseOrder" runat="server">
                                                                    <ContentTemplate>
                                                                        <div class="row" runat="server" id="AddNewOrderDiv">
                                                                            <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="LinkButton3" data-toggle="tooltip" data-target="#CollapsDivOrderFirstPanel" ToolTip="Add New Order" aria-expanded="false" aria-controls="CollapsDivOrderFirstPanel" OnClientClick="HidShowOrderDiv()" Style="float: right">
                                                                                <span class="AddNewspan"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                                                                        </div>
                                                                        <div class="row">
                                                                            <asp:ValidationSummary ID="ValidationSummary10" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                                ValidationGroup="CasePopUpTaskValidationGroup" />
                                                                            <asp:CustomValidator ID="CvOrderSaveMsg" runat="server" EnableClientScript="False"
                                                                                ValidationGroup="CasePopUpTaskValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                                                                        </div>
                                                                        <asp:GridView runat="server" ID="grdCaseOrder" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                            OnRowCommand="grdCaseOrder_RowCommand" OnRowDataBound="grdCaseOrder_RowDataBound" DataKeyNames="ID"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="grdCaseOrder_OnPageIndexChanging">
                                                                            <Columns>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Order Type" ItemStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                            <asp:Label ID="lblOrderType" runat="server" Text='<%# ShowOrderType((int)Eval("OrderTypeID")) %>'
                                                                                                data-toggle="tooltip" data-placement="bottom"></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Order" ItemStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                                            <asp:Label ID="lblOrderTitle" runat="server" Text='<%# Eval("OrderTitle") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("OrderTitle") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Description" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                            <asp:Label ID="lblOrderDesc" runat="server" Text='<%# Eval("OrderDesc") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("OrderDesc") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Order Date" ItemStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                            <asp:Label ID="lblDueOn" runat="server" Text='<%# Eval("OrderDate") != null ? Convert.ToDateTime(Eval("OrderDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("OrderDate") != null ? Convert.ToDateTime(Eval("OrderDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Uploaded By" ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                            <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                            <asp:Label ID="lblResDoc" runat="server" Text='<%# ShowOrderDocCount((long)Eval("CaseInstanceID"),(long)Eval("ID")) %>'>
                                                                                            </asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="aa1aa" UpdateMode="Conditional">
                                                                                            <ContentTemplate>
                                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID") %>'
                                                                                                        AutoPostBack="true" CommandName="EditCaseOrder"
                                                                                                        ID="lnkBtnEditOrderDoc" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit" title="Edit Document" />
                                                                                                    </asp:LinkButton>
                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID") %>'
                                                                                                        AutoPostBack="true" CommandName="DownloadCaseOrder"
                                                                                                        ID="lnkBtnDownloadOrderDoc" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download Document" />
                                                                                                    </asp:LinkButton>
                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID") %>'
                                                                                                        AutoPostBack="true" CommandName="ViewCaseOrder"
                                                                                                        ID="lnkBtnViewDoc" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Document" />
                                                                                                    </asp:LinkButton>

                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                        AutoPostBack="true" CommandName="DeleteOrder"
                                                                                                        OnClientClick="return confirm('Are you sure!! You want to Delete this Order Detail?');"
                                                                                                        ID="lnkBtnDeleteOrder" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete" />
                                                                                                    </asp:LinkButton>
                                                                                                </div>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnDeleteOrder" />
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnEditOrderDoc" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="DivOrderCollapsTwo" class="row Dashboard-white-widget">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="view Task details">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivOrderLogs">
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivOrderLogs">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="collapseDivOrderLogs" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="container">
                                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Panel ID="pnlOrder" runat="server">
                                                                    <div class="row">
                                                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                            ValidationGroup="CasePopUpOrderValidationGroup" />
                                                                        <asp:CustomValidator ID="cvCaseOrderPopup" runat="server" EnableClientScript="False"
                                                                            ValidationGroup="CasePopUpOrderValidationGroup" Display="None" />
                                                                        <asp:Label ID="lblOrderDoc" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-6 input-group date">
                                                                            <div>
                                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                    Order Date</label>
                                                                            </div>

                                                                            <div class="col-md-6 input-group date" style="width: 55%; margin-left: -13px;">
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar color-black"></span>
                                                                                </span>
                                                                                <asp:TextBox runat="server" placeholder="Order Date" CssClass="form-control" ID="tbxOrderDate" />
                                                                            </div>
                                                                            <asp:RequiredFieldValidator ID="rfvOrderDate" ErrorMessage="Provide Order Date"
                                                                                ControlToValidate="tbxOrderDate" runat="server" ValidationGroup="CasePopUpOrderValidationGroup" Display="None" />
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Order Type</label>
                                                                            <asp:DropDownListChosen ID="ddlOrderType" runat="server" CssClass="form-control"
                                                                                Width="50%" AllowSingleDeselect="false">
                                                                            </asp:DropDownListChosen>
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Order Title/ Number</label>
                                                                            <asp:TextBox ID="tbxOrderTitle" runat="server" CssClass="form-control" Width="70%"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvOrderTitle" ErrorMessage="Provide Order Title"
                                                                                ControlToValidate="tbxOrderTitle" runat="server" ValidationGroup="CasePopUpOrderValidationGroup" Display="None" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Description</label>
                                                                            <asp:TextBox ID="tbxOrderDesc" runat="server" CssClass="form-control" Style="width: 85.5%; min-height: 115px;" TextMode="MultiLine"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="rfvOrderDesc" ErrorMessage="Provide Order Description"
                                                                                ControlToValidate="tbxOrderDesc" runat="server" ValidationGroup="CasePopUpOrderValidationGroup" Display="None" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                            <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Remark</label>
                                                                            <asp:TextBox ID="tbxOrderRemark" runat="server" CssClass="form-control" Width="85.5%" TextMode="MultiLine"></asp:TextBox>
                                                                            <asp:TextBox ID="tbxOrderID" runat="server" CssClass="form-control" Style="display: none"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                            <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Upload Order Document</label>
                                                                            <div style="width: 100%;">
                                                                                <div style="width: 50%; float: left;">
                                                                                    <asp:FileUpload ID="fuCaseOrderDocUpload" runat="server" AllowMultiple="true" CssClass="fileUploadClass" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row" runat="server" id="divOrderEditdoc" visible="false">
                                                                        <asp:GridView ID="GrdOrderEditDocument" runat="server" AutoGenerateColumns="false" CssClass="table" AllowPaging="false"
                                                                            Width="100%" ShowHeaderWhenEmpty="true" GridLines="None" OnRowCommand="GrdOrderEditDocument_RowCommand"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="FileName" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                            <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="OrderDocDelete" UpdateMode="Always">
                                                                                            <ContentTemplate>
                                                                                                <asp:LinkButton
                                                                                                    CommandArgument='<%# Eval("ID")%>' CommandName="DownloadOrderEditDocument"
                                                                                                    ID="lnkDownloadOrderEditDocument" runat="server">
                                                                                                <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="DownLoad" title="DownLoad Documents" />
                                                                                                </asp:LinkButton>
                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                    AutoPostBack="true" CommandName="ViewOrderEditDocument"
                                                                                                    ID="lnkViewOrderEditDocument" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Document" />
                                                                                                </asp:LinkButton>
                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")%>' CommandName="DeleteOrderEditDocument"
                                                                                                    ID="lnkbtnOrderDelete" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="delete" title="delete Documents" />
                                                                                                </asp:LinkButton>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lnkbtnOrderDelete" />
                                                                                                <asp:PostBackTrigger ControlID="lnkDownloadOrderEditDocument" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Response Submitted yet.
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12" style="text-align: center;">
                                                                            <asp:Button Text="Save" runat="server" ID="btnOrderSave" CssClass="btn btn-primary" OnClick="btnOrderSave_Click" OnClientClick="if(!CheckValidationForOrder()) return false;"
                                                                                ValidationGroup="CasePopUpOrderValidationGroup"></asp:Button>
                                                                            <asp:Button Text="Clear" runat="server" ID="btnOrderClear" CssClass="btn btn-primary" OnClick="btnOrderClear_Click" />
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnOrderSave" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:View>

                    <%--Added by renuka 16 dec 2020 Advocate bill start--%>

                    <asp:View ID="AdvocateBillView" runat="server">
                        <div style="width: 100%; float: left;">
                            <div class="container">
                                <div id="AdvocateBillTabAccordion" class="row Dashboard-white-widget">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Case Advocate Bill Detail">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#AdvocateBillTabAccordion" href="#CollapsDivAdvocateBillFirstPanel">
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this);ShowAdvocateBillDiv();" data-toggle="collapse" data-parent="#AdvocateBillTabAccordion" href="#CollapsDivAdvocateBillFirstPanel">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="CollapsDivAdvocateBillFirstPanel" class="panel-collapse">
                                                <div class="row Dashboard-white-widget">
                                                    <div class="col-lg-12 col-md-12">
                                                        <div class="panel panel-default">
                                                            <div id="collapseDivAdvocateBillLogsFirstView" class="panel-collapse collapse in">
                                                                <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                                                    <ContentTemplate>
                                                                        <div class="row" style="margin-left: 79%;">

                                                                            <div class="col-md-2" runat="server" id="divAdvocateBill">
                                                                                <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="LnkAddAdvocateBill" data-placement="bottom" data-toggle="tooltip" data-target="#CollapsDivAdvocateBillFirstPanel" ToolTip="Add New Advocate Bill" aria-expanded="false" aria-controls="CollapsDivAdvocateBillFirstPanel" OnClientClick="HidShowAdvocateBillDiv();rebindAdvocateHearingRefNo();" OnClick="LnkAddAdvocateBill_Click">
                                                                               <span class="AddNewspan"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>

                                                                            </div>
                                                                            <div class="col-md-2" style="margin-left: 24%;">
                                                                                <asp:UpdatePanel ID="UpdatePanel14" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton Text="Apply" runat="server" ID="btnAdvbillExport" OnClick="btnAdvbillExport_Click" data-toggle="tooltip" data-placement="bottom" ToolTip="Export to Excel">
                                                                                      <img src="../../Images/Excel _icon.png"   /> 
                                                                                        </asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="btnAdvbillExport" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </div>


                                                                        </div>



                                                                        <div class="row">
                                                                            <asp:ValidationSummary ID="ValidationSummary12" runat="server" Display="none" class="alert alert-block alert-success fade in"
                                                                                ValidationGroup="CasePopUpAdvocateBillValidationGroupnew" />
                                                                            <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                                                                                ValidationGroup="CasePopUpAdvocateBillValidationGroupnew" Display="None" class="alert alert-block alert-success fade in" />
                                                                        </div>
                                                                        <asp:GridView runat="server" ID="grdAdvocateBill" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                            OnRowCommand="grdAdvocateBill_RowCommand" DataKeyNames="AdvocateBillId" OnRowDataBound="grdAdvocateBill_RowDataBound"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="grdAdvocateBill_PageIndexChanging">
                                                                            <Columns>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Hearing" ItemStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                            <asp:Label ID="lblHearingRefID" runat="server" Text='<%# Eval("HearingRef") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("HearingRef") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Invoice No" ItemStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                            <asp:Label ID="lblInvoiceNo" runat="server" Text='<%# Eval("InvoiceNo") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("InvoiceNo") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Invoice Date" ItemStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                            <asp:Label ID="lblInvoicedate" runat="server" Text='<%# Eval("InvoiceDate") != null ? Convert.ToDateTime(Eval("InvoiceDate")).ToString("dd-MM-yyyy") : "" %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("InvoiceDate") != null ? Convert.ToDateTime(Eval("InvoiceDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Invoice Amount" ItemStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                                            <asp:Label ID="lblInvoiceAmount" runat="server" Text='<%# Eval("InvoiceAmount") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("InvoiceAmount") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <%--<asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Assigned To" ItemStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                                            <asp:Label ID="lblAssignTo" runat="server" Text='<%# Eval("AssignTo") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("AssignTo") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>--%>



                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Law Firm" ItemStyle-Width="20%" FooterStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                            <asp:Label ID="lblLawyer" runat="server" Text='<%# Eval("Lawyer") %>' data-placement="bottom" data-toggle="tooltip" ToolTip='<%# Eval("Lawyer") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>



                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Remark" ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                            <asp:Label ID="lblRemark" runat="server" Text='<%# Eval("Remark") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Remark") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Status" ItemStyle-Width="5%" FooterStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                            <asp:Label ID="lblstatus" runat="server" Text='<%# Eval("Status") %>' data-placement="bottom" data-toggle="tooltip" ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>

                                                                                        <asp:HiddenField ID="HiddenField1" runat="server"
                                                                                            Value='<%# Eval("NoticeCaseInstanceID") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Documents" ItemStyle-Width="5%" HeaderStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px;">
                                                                                            <asp:Label ID="lbladvDoc" runat="server" Text='<%# ShowadvbillDocCount((long)Eval("NoticeCaseInstanceID"),(int)Eval("AdvocateBillId")) %>'>
                                                                                            </asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>

                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="UPAdvocateBill" UpdateMode="Conditional">
                                                                                            <ContentTemplate>
                                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">
                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("AdvocateBillId") %>'
                                                                                                        AutoPostBack="true" CommandName="EditCaseAdvocateBill"
                                                                                                        ID="lnkBtnEditCaseAdvocateBill" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit" title="Edit Advocate Bill" />
                                                                                                    </asp:LinkButton>
                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("AdvocateBillId") %>'
                                                                                                        AutoPostBack="true" CommandName="DownloadCaseAdvocatebill"
                                                                                                        ID="lnkBtnDownloadAdvocateBillDoc" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" title="Download Document" />
                                                                                                    </asp:LinkButton>
                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("AdvocateBillId") %>'
                                                                                                        AutoPostBack="true" CommandName="ViewCaseAdvocatebill"
                                                                                                        ID="lnkBtnViewAdvocateBillDoc" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Document" />
                                                                                                    </asp:LinkButton>

                                                                                                    <asp:LinkButton CommandArgument='<%# Eval("AdvocateBillId")%>'
                                                                                                        AutoPostBack="true" CommandName="DeleteAdvocateBill"
                                                                                                        OnClientClick="return confirm('Are you sure!! You want to Delete this Advocate Bill Detail?');"
                                                                                                        ID="lnkBtnDeleteAdvocateBill" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete" title="Delete" />
                                                                                                    </asp:LinkButton>
                                                                                                    <asp:LinkButton CommandName="ConverttopdfPayment" CommandArgument='<%# Eval("AdvocateBillId")%>' ID="LnkBtnconvetpdf" runat="server">
                                                                                                       <img src='<%# ResolveUrl("~/Images/pdf-files.png")%>' alt="Download" height="25";width="25" title="View and Download PDF" /></asp:LinkButton>
                                                                                                </div>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnEditCaseAdvocateBill" />
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnDeleteAdvocateBill" />
                                                                                                <asp:PostBackTrigger ControlID="lnkBtnDownloadAdvocateBillDoc" />
                                                                                                <%--   <asp:PostBackTrigger ControlID="lnkBtnViewAdvocateBillDoc" />--%>
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>


                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="DivAdvocateBillCollapsTwo" class="row Dashboard-white-widget">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Advocate Bill Details">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseDivAdvocateBillLogs">
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivAdvocateBillLogs">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="collapseDivAdvocateBillLogs" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="container">
                                                        <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Panel ID="Paneladvbill" runat="server">
                                                                    <div class="row">
                                                                        <asp:ValidationSummary ID="ValidationSummary13" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                            ValidationGroup="CasePopUpAdvocateBillValidationGroup" />
                                                                        <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                                                                            ValidationGroup="CasePopUpAdvocateBillValidationGroup" Display="None" />
                                                                        <asp:Label ID="Label2" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Hearing</label>

                                                                            <asp:ListBox ID="lstAdvHearingRefNo" CssClass="form-control" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ErrorMessage="Please Select Hearing."
                                                                                ControlToValidate="lstAdvHearingRefNo" runat="server" ValidationGroup="CasePopUpAdvocateBillValidationGroup" Display="None" />--%>
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Invoice Date</label>
                                                                            <div class="col-md-6 input-group date" style="width: 71%; margin-left: -14px;">
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar color-black"></span>
                                                                                </span>
                                                                                <asp:TextBox runat="server" placeholder="Invoice Date" class="form-control" ID="tbxinvoicedate" autocomplete="off" />
                                                                            </div>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ErrorMessage="Please Select Invoice Date."
                                                                                ControlToValidate="tbxinvoicedate" runat="server" ValidationGroup="CasePopUpAdvocateBillValidationGroup" Display="None" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Invoice No</label>
                                                                            <asp:TextBox ID="tbxAdvInvoiceno" runat="server" CssClass="form-control" Width="66%" autocomplete="off"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ErrorMessage="Please Enter Invoice No."
                                                                                ControlToValidate="tbxAdvInvoiceno" runat="server" ValidationGroup="CasePopUpAdvocateBillValidationGroup" Display="None" />
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Invoice Amount</label>
                                                                            <asp:TextBox ID="tbxAdvInvoiceAmount" runat="server" CssClass="form-control" Width="66%" OnTextChanged="tbxAdvInvoiceAmount_TextChanged" AutoPostBack="true" autocomplete="off"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ErrorMessage="Please Enter Invoice Amount."
                                                                                ControlToValidate="tbxAdvInvoiceAmount" runat="server" ValidationGroup="CasePopUpAdvocateBillValidationGroup" Display="None" />
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ErrorMessage="Please Enter Valid Invoice Amount." ControlToValidate="tbxAdvInvoiceAmount"
                                                                                ValidationExpression="^(\d*\.)?\d+$" ValidationGroup="CasePopUpAdvocateBillValidationGroup" Display="None"
                                                                                runat="server" />
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Law Firm</label>
                                                                            <asp:DropDownListChosen ID="ddlLawyerAdvocate" CssClass="form-control" runat="server"
                                                                                AllowSingleDeselect="false" DisableSearchThreshold="5" Width="66%" AutoPostBack="true">
                                                                            </asp:DropDownListChosen>
                                                                            <%--<asp:RequiredFieldValidator ID="rfvddlLawyerAdvocate"  ErrorMessage="Please Select Lawyer/Advocate."
                                                                                    ControlToValidate="ddlLawyerAdvocate" runat="server" ValidationGroup="CasePopUpAdvocateBillValidationGroup" Display="None" />
                                                                            --%>
                                                                            <%-- <asp:CompareValidator ID="CompareValidator4" ErrorMessage="Please Select Lawyer/Advocate." ControlToValidate="ddlLawyerAdvocate"
                                                                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="CasePopUpAdvocateBillValidationGroup"
                                                                                Display="None" />--%>
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <div id="divApprover1" runat="server">
                                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Approver 1</label>
                                                                                <asp:DropDownListChosen runat="server" ID="ddlApprover1" DataPlaceHolder="Select Approver" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                    CssClass="form-control" Width="66%">
                                                                                </asp:DropDownListChosen>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <div id="divApprover2" runat="server">
                                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Approver 2</label>
                                                                                <asp:DropDownListChosen runat="server" ID="ddlApprover2" DataPlaceHolder="Select Approver" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                    CssClass="form-control" Width="66%">
                                                                                </asp:DropDownListChosen>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <div id="divApprover3" runat="server" visible="false">
                                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Approver 3</label>
                                                                                <asp:DropDownListChosen runat="server" ID="ddlApprover3" DataPlaceHolder="Select Approver" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                    CssClass="form-control" Width="66%">
                                                                                </asp:DropDownListChosen>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                            <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Currency</label>
                                                                            <asp:TextBox ID="txtcurrency" runat="server" CssClass="form-control" Width="32.05%" autocomplete="off"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                            <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Remark</label>
                                                                            <asp:TextBox ID="tbxAdvRemark" runat="server" CssClass="form-control" Width="83.5%" autocomplete="off"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                            <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Upload Advocate Bill Document</label>
                                                                            <div style="width: 100%;">
                                                                                <div style="width: 50%; float: left;">
                                                                                    <asp:FileUpload ID="fuAdvocateBillDocUpload" runat="server" AllowMultiple="true" CssClass="fileUploadClass" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row" runat="server" id="divAdveditBilldocument" visible="false">
                                                                        <asp:GridView ID="grdAdvocateBillDocuments" runat="server" AutoGenerateColumns="false" CssClass="table" AllowPaging="false"
                                                                            Width="100%" ShowHeaderWhenEmpty="true" GridLines="None" OnRowCommand="grdAdvocateBillDocuments_RowCommand"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="File Name" ItemStyle-Width="20%">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                                                            <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" HeaderStyle-HorizontalAlign="Left">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel runat="server" ID="AdvBillDocDelete" UpdateMode="Always">
                                                                                            <ContentTemplate>
                                                                                                <asp:LinkButton
                                                                                                    CommandArgument='<%# Eval("ID")%>' CommandName="DownloadAdvBillEditDocument"
                                                                                                    ID="lnkDownloadAdvBillEditDocument" runat="server">
                                                                                                <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="DownLoad" title="DownLoad Documents" />
                                                                                                </asp:LinkButton>
                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                                    AutoPostBack="true" CommandName="ViewAdvBillEditDocument"
                                                                                                    ID="lnkViewAdvBillEditDocument" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" title="View Document" />
                                                                                                </asp:LinkButton>
                                                                                                <asp:LinkButton CommandArgument='<%# Eval("ID")%>' CommandName="DeleteAdvBillEditDocument"
                                                                                                    ID="lnkbtnAdvBillDelete" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="delete" title="delete Documents" />
                                                                                                </asp:LinkButton>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="lnkbtnAdvBillDelete" />
                                                                                                <asp:PostBackTrigger ControlID="lnkDownloadAdvBillEditDocument" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <RowStyle CssClass="clsROWgrid" />
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Response Submitted yet.
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="form-group col-md-12" style="text-align: center;">
                                                                            <asp:Button Text="Save" runat="server" ID="btnAdvocateBillSave" CssClass="btn btn-primary" OnClientClick="if(!CheckValidationForAdvocateBill()) return false;"
                                                                                OnClick="btnAdvocateBillSave_Click" ValidationGroup="CasePopUpAdvocateBillValidationGroup"></asp:Button>
                                                                            <asp:Button Text="Clear" runat="server" ID="btnAdvocateBillClear" CssClass="btn btn-primary" OnClick="btnAdvocateBillClear_Click" />
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnAdvocateBillSave" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:View>
                    <%--Advocate bill End--%>

                    <asp:View ID="StatusPaymentView" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">
                                <div id="FourthTabAccordion">

                                    <div class="row Dashboard-white-widget">
                                        <!--Status Log Panel Start-->
                                        <div class="col-lg-12 col-md-12">
                                            <div class="panel panel-default">
                                                <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View/Edit Case Status">
                                                    <div class="panel-heading" data-toggle="collapse" data-parent="#FourthTabAccordion" href="#collapseDivStatusLogs">
                                                        <a>
                                                            <h2>Status</h2>
                                                        </a>
                                                        <div class="panel-actions">
                                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#FourthTabAccordion" href="#collapseDivStatusLogs">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="collapseDivStatusLogs" class="panel-collapse collapse in">
                                                    <div class="panel-body">
                                                        <div style="margin-bottom: 7px">
                                                            <asp:ValidationSummary ID="ValidationSummary3" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                ValidationGroup="CasePopUpStatusValidationGroup" />
                                                            <asp:CustomValidator ID="cvCaseStatus" runat="server" EnableClientScript="False"
                                                                ValidationGroup="CasePopUpStatusValidationGroup" Display="None" />
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Case Stage</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlCaseStage" DataPlaceHolder="Select Status" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="45%">
                                                                </asp:DropDownListChosen>
                                                            </div>

                                                            <div class="form-group col-md-6">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Case Status</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlCaseStatus" DataPlaceHolder="Select Status" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="40%" onchange="ddlStatusChange()">
                                                                    <asp:ListItem Text="Select Status" Value="0"></asp:ListItem>
                                                                    <asp:ListItem Text="Open" Value="1"></asp:ListItem>
                                                                    <%--<asp:ListItem Text="In Progress" Value="2"></asp:ListItem>--%>
                                                                    <asp:ListItem Text="Close" Value="3"></asp:ListItem>
                                                                    <%-- <asp:ListItem Text="Settled" Value="4"></asp:ListItem>--%>
                                                                </asp:DropDownListChosen>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-6" id="divResult">
                                                                <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">Case Result</label>
                                                                <asp:DropDownListChosen runat="server" ID="ddlCaseResult" DataPlaceHolder="Select Status" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                    CssClass="form-control" Width="45%">
                                                                </asp:DropDownListChosen>
                                                            </div>

                                                            <div class="form-group col-md-6 input-group date" id="divClosureDetail">
                                                                <div>
                                                                    <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                    <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                        Close Date</label>
                                                                </div>

                                                                <div class="col-md-6 input-group date" style="width: 45%; margin-left: -13px;">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar color-black"></span>
                                                                    </span>
                                                                    <asp:TextBox runat="server" placeholder="Close Date" class="form-control" ID="tbxCaseCloseDate" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-group col-md-12" id="divClosureRemark">
                                                                <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <label style="width: 13%; display: block; float: left; font-size: 13px; color: #333;">Remark</label>
                                                                <asp:TextBox ID="tbxCloseRemark" runat="server" CssClass="form-control" Width="85%" TextMode="MultiLine"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-md-3">
                                                                    <asp:Button Text="Save" runat="server" ID="btnSaveStatus" CssClass="btn btn-primary pull-right" OnClick="btnSaveStatus_Click"
                                                                        ValidationGroup="CasePopUpStatusValidationGroup"></asp:Button>
                                                                </div>
                                                                <div id="divConvertToCase">
                                                                    <div class="col-md-3">
                                                                        <asp:Button Text="Appeal to Next Court" runat="server" ID="btnCaseTransfer" CssClass="btn btn-primary pull-left" OnClick="btnCaseTransfer_Click"></asp:Button>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div>
                                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                                Court Case No.</label>
                                                                        </div>
                                                                        <asp:TextBox runat="server" placeholder="Court Case No." Width="70%" class="form-control" ID="tbxAppealCaseNo" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row" id="divCustomField" runat="server" visible="false">
                                                            <div class="form-group col-md-12">
                                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:GridView runat="server" ID="grdCustomField_CaseTransfer" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                            GridLines="None" AllowPaging="true" AutoPostBack="true" CssClass="table"
                                                                            PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <%#Container.DataItemIndex+1 %>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("LableID") %>'
                                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("LableID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="20%" FooterStyle-Width="20%" HeaderText="Ground(s) of Appeal">
                                                                                    <ItemTemplate>
                                                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; margin-top: 5px;">
                                                                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Label") %>'
                                                                                                data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Label") %>'></asp:Label>
                                                                                        </div>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Tax Demand">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="tbxLabelValue" runat="server" Text='<%# Eval("labelValue") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Interest">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="tbxInterestValue" runat="server" Text='<%# Eval("Interest") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Penalty">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="tbxPenaltyValue" runat="server" Text='<%# Eval("Penalty") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Total">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="tbxRowTotalValue" runat="server" Text='<%# Eval("Total") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Provision in Books">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="tbxProvisionInbooks" runat="server" Text='<%# Eval("ProvisionInBook") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" HeaderText="Select">
                                                                                    <ItemTemplate>
                                                                                        <asp:DropDownList runat="server" ID="ddlGroundResult" DataPlaceHolder="Select" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                            CssClass="form-control" Width="100%" OnSelectedIndexChanged="ddlGroundResult_SelectedIndexChanged" AutoPostBack="true">
                                                                                            <asp:ListItem Text="Allowed" Value="1"></asp:ListItem>
                                                                                            <asp:ListItem Text="Disallowed" Value="0" Selected="True"></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField ItemStyle-Width="10%" FooterStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" HeaderText="Settlement Value">
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="tbxSettlementValue" runat="server" CssClass="form-control text-right" PlaceHolder="Settlement"
                                                                                            Text='<%# Eval("SettlementValue") %>' Visible="false"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="clsheadergrid" />
                                                                            <EmptyDataTemplate>
                                                                                No Records Found
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>

                                                            <div class="form-group col-md-12 text-center">
                                                                <asp:Button Text="Save" runat="server"
                                                                    ID="btnSaveCustomFieldCaseTransfer" CssClass="btn btn-primary" OnClick="btnSaveCustomFieldCaseTransfer_Click"></asp:Button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Status Log Panel End-->
                                </div>

                                <div class="row Dashboard-white-widget">
                                    <!--Payment Log Panel Start-->
                                    <div class="col-lg-12 col-md-12" style="width: 145%;">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;" data-toggle="tooltip" data-placement="bottom" title="View Payment Detail">
                                                <div class="panel-heading" data-toggle="collapse" data-parent="#FourthTabAccordion" href="#collapseDivPaymentLog">
                                                    <a>
                                                        <h2>Payment Log</h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#FourthTabAccordion" href="#collapseDivPaymentLog">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseDivPaymentLog" class="panel-collapse collapse in">
                                                <div class="panel-body colpadding0">

                                                    <div style="margin-bottom: 7px; margin-right: 247px;">
                                                        <asp:ValidationSummary ID="ValidationSummary4" runat="server" Display="none" class="alert alert-danger"
                                                            ValidationGroup="CasePopUpPaymentLogValidationGroup" />
                                                        <asp:CustomValidator ID="cvCasePayment" runat="server" EnableClientScript="false"
                                                            ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />


                                                    </div>
                                                    <div class="form-group col-md-12 colpadding0">
                                                        <asp:UpdatePanel ID="upCasePayment" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:GridView runat="server" ID="grdCasePayment" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                    GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="true"
                                                                    OnRowCommand="grdCasePayment_RowCommand" OnRowDataBound="grdCasePayment_RowDataBound"
                                                                    PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="grdCasePayment_PageIndexChanging">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="2%" FooterStyle-Width="2%">
                                                                            <ItemTemplate>
                                                                                <%#Container.DataItemIndex+1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Invoice No." ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblInvoiceNo" runat="server" Text='<%# Eval("InvoiceNo")%>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderTemplate>
                                                                                <em id="em1">* </em>Invoice No.
                                                                            </HeaderTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:TextBox ID="tbxInvoiceNo" runat="server" class="form-control" PlaceHolder="InvoiceNo"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="rfvInvoice" ErrorMessage="Provide Invoice No."
                                                                                    ControlToValidate="tbxInvoiceNo" runat="server" ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Lawyer" ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblLawyer" runat="server" Text='<%# Eval("Lawyer") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                          <%--  <HeaderTemplate>
                                                                                <em id="em1">* </em>Lawyer
                                                                            </HeaderTemplate>--%>
                                                                            <FooterTemplate>
                                                                                <asp:DropDownListChosen runat="server" ID="ddlLawyer" DataPlaceHolder="Lawyer" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                    CssClass="form-control" Width="100%">
                                                                                </asp:DropDownListChosen>
                                                                                <%--<asp:RequiredFieldValidator ID="rfvlawyer" ErrorMessage="Please select Lawyer." InitialValue="-1"
                                                                                    ControlToValidate="ddlLawyer" runat="server" ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />--%>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" HeaderText="Hearing Date" ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblHearingID" runat="server" Text='<%# Eval("HearingRefNo") %>' Style="text-align: left;"></asp:Label>
                                                                            </ItemTemplate>
                                                                           <%-- <HeaderTemplate>
                                                                                <em id="em1">* </em>HearingRefNo
                                                                            </HeaderTemplate>--%>
                                                                            <FooterTemplate>
                                                                                <asp:DropDownListChosen runat="server" ID="ddlHearingID" AutoPostBack="true" DataPlaceHolder="Hearing Date" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                    CssClass="form-control" Width="100%">
                                                                                </asp:DropDownListChosen>
                                                                                <%--<asp:RequiredFieldValidator ID="rfvhearingdate" ErrorMessage="Please select Hearing Date."
                                                                                    ControlToValidate="ddlHearingID" runat="server" ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />--%>

                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Invoice Date" ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblPaymentDate" runat="server" Text='<%# Eval("PaymentDate") != null ? Convert.ToDateTime(Eval("PaymentDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                         <%--   <HeaderTemplate>
                                                                                <em id="em1">* </em>Payment Date
                                                                            </HeaderTemplate>--%>
                                                                            <FooterTemplate>
                                                                                <div class="col-md-4 plr0 input-group date" style="width: 100%">
                                                                                    <span class="input-group-addon">
                                                                                        <span class="fa fa-calender" style="padding: 3px !important;"></span>
                                                                                    </span>
                                                                                    <asp:TextBox runat="server" placeholder="Payment Date" class="form-control" ID="tbxPaymentDate" />
                                                                                </div>
                                                                                <%--<asp:RequiredFieldValidator ID="rfvPaymentDate" ErrorMessage="Provide Payment Date." runat="server"
                                                                                    ControlToValidate="tbxPaymentDate" ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />--%>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Payment Type" ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblPaymentType" runat="server" Text='<%# Eval("PaymentType") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderTemplate>
                                                                                <em id="em1">* </em>Payment Type
                                                                            </HeaderTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:DropDownListChosen runat="server" ID="ddlPaymentType" DataPlaceHolder="Payment Type" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                    CssClass="form-control" Width="100%">
                                                                                </asp:DropDownListChosen>
                                                                                <asp:RequiredFieldValidator ID="rfvPaymentType" ErrorMessage="Select Payment Type."
                                                                                    ControlToValidate="ddlPaymentType" runat="server" ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Amount Excluding Tax" ItemStyle-Width="20%" FooterStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("Amount") %>' Style="text-align: right;"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderTemplate>
                                                                                <em id="em1">* </em>Amount Excluding Tax
                                                                            </HeaderTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:TextBox ID="tbxAmount" runat="server" class="form-control" PlaceHolder="Amount"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="rfvAmount" ErrorMessage="Provide Amount Excluding Tax"
                                                                                    ControlToValidate="tbxAmount" runat="server" ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Center" HeaderText="Tax Amount" ItemStyle-Width="15%" FooterStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="AmountTax" runat="server" Text='<%# Eval("AmountTax") %>' Style="text-align: right;"></asp:Label>
                                                                            </ItemTemplate>
                                                                          <%--  <HeaderTemplate>
                                                                                <em id="em1">* </em>AmountTax
                                                                            </HeaderTemplate>--%>
                                                                            <FooterTemplate>
                                                                                <asp:TextBox ID="tbxAmountTaxPaid" runat="server" class="form-control" PlaceHolder="TaxAmountPaid"></asp:TextBox>
                                                                                <%--<asp:RequiredFieldValidator ID="rfvTaxAmountPaid" ErrorMessage="Provide Payment Tax AmountPaid."
                                                                                    ControlToValidate="tbxAmountTaxPaid" runat="server" ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />--%>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" HeaderText="Amount Paid" ItemStyle-Width="13%" FooterStyle-Width="13%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblAmountPaid" runat="server" Text='<%# Eval("AmountPaid") %>' Style="text-align: left;"></asp:Label>
                                                                            </ItemTemplate>
                                                                          <%--  <HeaderTemplate>
                                                                                <em id="em1">* </em>AmountPaid
                                                                            </HeaderTemplate>--%>
                                                                            <FooterTemplate>
                                                                                <asp:TextBox ID="tbxAmountPaid" runat="server" class="form-control" PlaceHolder="AmountPaid"></asp:TextBox>
                                                                                <%--<asp:RequiredFieldValidator ID="rfvAmountPaid" ErrorMessage="Provide Payment AmountPaid."
                                                                                    ControlToValidate="tbxAmountPaid" runat="server" ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />--%>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Remark" ItemStyle-Width="18%" FooterStyle-Width="18%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblPaymentRemark" runat="server" Text='<%# Eval("Remark") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                         <%--   <HeaderTemplate>
                                                                                <em id="em1">* </em>Remark
                                                                            </HeaderTemplate>--%>
                                                                            <FooterTemplate>
                                                                                <asp:TextBox ID="tbxPaymentRemark" runat="server" class="form-control" PlaceHolder="Remark"></asp:TextBox>
                                                                                <asp:TextBox ID="tbxPaymentID" runat="server" CssClass="form-control" Style="display: none"></asp:TextBox>

                                                                                <%--<asp:RequiredFieldValidator ID="rfvPaymentRemark" ErrorMessage="Provide Payment Remark."
                                                                                    ControlToValidate="tbxPaymentRemark" runat="server" ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />--%>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Created By" ItemStyle-Width="20%" FooterStyle-Width="20%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedByText") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <%-- <HeaderTemplate>
                                                                              <em id="em1" style="margin-left:-10px;"> * </em> Created By
                                                                            </HeaderTemplate>--%>
                                                                            <FooterTemplate>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Upload Document" ItemStyle-Width="13%" FooterStyle-Width="13%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFileUpLoad" runat="server" Text='<%# Eval("FileName") %>'></asp:Label><%-- Text='<%# ShowFileName((long)Eval("ID")) %>'--%>
                                                                            </ItemTemplate>
                                                                            <%--  <HeaderTemplate>
                                                                              <em id="em1"> * </em> FileName
                                                                            </HeaderTemplate>--%>
                                                                            <FooterTemplate>
                                                                                <asp:FileUpload AllowMultiple="true" runat="server" ID="fuSampleFile" />
                                                                                <asp:HiddenField runat="server" ID="hdnFile" />
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="left" ItemStyle-Width="100%"
                                                                            FooterStyle-Width="100%" FooterStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%; margin-left: -29px;">
                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID") %>' AutoPostBack="true" CommandName="ViewCasePayment"
                                                                                        ID="lnkBtnViewPayment" runat="server" data-placement="bottom" title="View Payment Document">
                                                                                                <img src='<%# ResolveUrl("~/Images/Eye.png")%>' alt="View" />
                                                                                    </asp:LinkButton>

                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID") %>'
                                                                                        AutoPostBack="true" CommandName="EditPayment"
                                                                                        ID="lnkBtnEditPayment" runat="server">
                                                                                                        <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit" title="Edit Payment" />
                                                                                    </asp:LinkButton>

                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID")%>' CommandName="DownloadCasePaymentDoc"
                                                                                        ID="lnkBtnDownLoadCaseDoc" runat="server" data-toggle="tooltip" data-placement="bottom" title="Download Payment Document">
                                                                                                <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" /> <%--width="15" height="15"--%>
                                                                                    </asp:LinkButton>
                                                                                </div>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%; margin-left: 52px; margin-top: -24px;">

                                                                                    <asp:LinkButton CommandName="ConverttopdfPayment" CommandArgument='<%# Eval("ID")%>' ID="LinkBtnconvetpdf" runat="server" Visible='<%# Convert.ToString(Eval("HearingRefNo"))=="" ? false : true %>'>   <img src='<%# ResolveUrl("~/Images/pdf-files.png")%>' alt="Download" 
                                                                                          height="25";width="25" title="Download" /></asp:LinkButton>

                                                                                    <asp:LinkButton CommandArgument='<%# Eval("ID")%>'
                                                                                        AutoPostBack="true" CommandName="DeletePayment"
                                                                                        OnClientClick="return confirm('Are you sure!! You want to Delete this Payment Detail?');"
                                                                                        ID="lnkBtnDeletePayment" runat="server">
                                                                                            <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Payment" 
                                                                                                title="Delete" />
                                                                                    </asp:LinkButton>
                                                                                </div>
                                                                                <div style="margin-left: 78px; margin-top: -24px;">
                                                                                </div>

                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <asp:Button CssClass="btn btn-primary" ID="btnPaymentSave" runat="server" Text="Save" CausesValidation="true" ValidationGroup="CasePopUpPaymentLogValidationGroup" OnClick="btnPaymentSave_Click"></asp:Button>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <EmptyDataTemplate>
                                                                        No Records Found
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="grdCasePayment" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Payment Log Panel End-->
                                </div>
                            </div>
                        </div>
                    </asp:View>
                    <asp:View ID="RatingView" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">
                                <div id="FifthTabAccordion">
                                    <div class="row Dashboard-white-widget">
                                        <div class="col-lg-12 col-md-12">
                                            <div class="panel panel-default">
                                                <div class="row" style="margin-top: 20px;">
                                                    <div id="collapseDivLawRating" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div style="margin-bottom: 7px">
                                                                    <asp:ValidationSummary ID="ValidationSummary6" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                        ValidationGroup="ValidLayerRating" />
                                                                    <asp:CustomValidator ID="CvValidLaywRating" runat="server" EnableClientScript="False"
                                                                        ValidationGroup="ValidLayerRating" Display="None" />
                                                                </div>
                                                            </div>
                                                            <asp:Panel ID="pnlLawRating" runat="server">
                                                                <div class="form-group col-md-12">
                                                                    <div class="col-md-12">
                                                                        <div class="col-md-6">
                                                                            <asp:DropDownListChosen runat="server" ID="ddlLayerType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                                                CssClass="form-control" Width="50%" AutoPostBack="True" OnSelectedIndexChanged="ddlLayerType_SelectedIndexChanged">
                                                                            </asp:DropDownListChosen>
                                                                            <asp:RequiredFieldValidator ID="rfvLayerType" ErrorMessage="Select Lawyer"
                                                                                ControlToValidate="ddlLayerType" runat="server" ValidationGroup="CasePopUpPaymentLogValidationGroup" Display="None" />
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="btnAddPromotor" Style="float: right" OnClick="btnAddPromotor_Click" data-toggle="tooltip" ToolTip="Add New Criteria">
                                                                                <span class="AddNewspan"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                                                                        </div>
                                                                    </div>

                                                                    <asp:GridView runat="server" ID="grdLawyerRating" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                        GridLines="None" PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false"
                                                                        OnRowDataBound="grdLawyerRating_RowDataBound" OnPageIndexChanging="grdLawyerRating_PageIndexChanging"
                                                                        PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%" FooterStyle-Width="5%">
                                                                                <ItemTemplate>
                                                                                    <%#Container.DataItemIndex+1 %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Criteria" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblCriteria" runat="server" Text='<%# Eval("Name")%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Rating" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                                                                <ItemTemplate>
                                                                                    <ajaxToolkit:Rating ID="LawyerRating" runat="server" AutoPostBack="true" StarCssClass="blankstar"
                                                                                        WaitingStarCssClass="waitingstar" FilledStarCssClass="shiningstar" EmptyStarCssClass="blankstar"
                                                                                        CurrentRating='<%# ShowLawyerRating((decimal)Eval("Rating")) %>'>
                                                                                    </ajaxToolkit:Rating>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField Visible="false">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblCriteriaID" runat="server" Text='<%# Eval("criteriaID")%>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <RowStyle CssClass="clsROWgrid" />
                                                                        <HeaderStyle CssClass="clsheadergrid" />
                                                                        <EmptyDataTemplate>
                                                                            No Records Found
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="form-group col-md-12" style="text-align: center; width: 30%; margin-left: 358px;">
                                                                        <div style="text-align: center">
                                                                            <asp:Button Text="Save" runat="server" ID="btnSaveLawRating" CssClass="btn btn-primary" OnClick="btnSaveLawRating_Click"
                                                                                ValidationGroup="ValidLayerRating" CausesValidation="true"></asp:Button>
                                                                            <asp:LinkButton Style="display: none" runat="server" ID="lnkBtnRebindRating" OnClick="lnkBtnRebindRating_Click" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:View>

                    <asp:View ID="AuditLogView" runat="server">
                        <div style="width: 100%; float: left; margin-bottom: 15px">
                            <div class="container">
                                <div id="SixthTabAccordion">
                                    <div class="row Dashboard-white-widget">
                                        <div class="col-lg-12 col-md-12">
                                            <div class="panel panel-default">
                                                <div class="row" style="margin-top: 20px;">

                                                    <div id="collapseDivAuditLog" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <asp:UpdatePanel ID="upCaseAuditLog" runat="server">
                                                                <ContentTemplate>
                                                                    <div class="row">
                                                                        <div style="margin-bottom: 7px">
                                                                            <asp:ValidationSummary ID="ValidationSummary11" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                                                                ValidationGroup="validationCaseAuditLog" />
                                                                            <asp:CustomValidator ID="cvCaseAuditLog" runat="server" EnableClientScript="False"
                                                                                ValidationGroup="validationCaseAuditLog" Display="None" />
                                                                        </div>
                                                                    </div>
                                                                    <div style="margin-left: 94%;">

                                                                        <asp:UpdatePanel ID="UpdatePanel11" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton Text="Apply" runat="server" ID="btnExport" OnClick="btnExport_Click" Width="80%" Style="margin-top: 5px;" data-toggle="tooltip" ToolTip="Export to Excel">
                                                                                      <img src="../../Images/Excel _icon.png" alt="Export to Excel" title="Export to Excel" /> 
                                                                                </asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:PostBackTrigger ControlID="btnExport" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </div>
                                                                    <asp:Panel ID="Panel3" runat="server">
                                                                        <div class="form-group col-md-12">
                                                                            <asp:GridView runat="server" ID="gvCaseAuditLog" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                                GridLines="None" PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%" ShowFooter="false"
                                                                                PagerSettings-Position="Bottom" PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="gvCaseAuditLog_PageIndexChanging">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="10%" FooterStyle-Width="10%">
                                                                                        <ItemTemplate>
                                                                                            <%#Container.DataItemIndex+1 %>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Remark" ItemStyle-Width="40%" HeaderStyle-Width="40%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblRemark" runat="server" Text='<%# Eval("Remark")%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Created By" ItemStyle-Width="25%" HeaderStyle-Width="25%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblCreatedBy" runat="server" Text='<%# Eval("CreatedByUser")%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Created On" ItemStyle-Width="25%" HeaderStyle-Width="25%">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblCreatedOn" runat="server" Text='<%# Eval("CreatedOn") != DBNull.Value ? Convert.ToDateTime(Eval("CreatedOn")).ToString("dd-MM-yyyy hh:mm:ss:tt") : ""%>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <RowStyle CssClass="clsROWgrid" />
                                                                                <HeaderStyle CssClass="clsheadergrid" />
                                                                                <EmptyDataTemplate>
                                                                                    No Records Found
                                                                                </EmptyDataTemplate>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </div>

            <%--Compliance Document  Popup--%>
            <div class="modal fade" id="divComplianceDocumentShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog pt10 pb10">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label class="modal-header-custom">
                                Compliance Document</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -9px;" onclick="ClosePopNoticeDetialPage();">&times;</button>
                        </div>

                        <div class="modal-body">
                            <iframe id="IframeComplianceDocument" src="about:blank" width="95%" height="auto" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Department Popup--%>
            <div class="modal fade" id="AddDepartmentPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 40%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Department</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeDepartment" frameborder="0" runat="server" style="width: 100%; height: 209px;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Act Popup--%>
            <div class="modal fade" id="AddActPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 35%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Act</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeAct" overflow="hidden" frameborder="0" runat="server" style="width: 100%; height: 209px;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add Party Details--%>
            <div class="modal fade" id="AddPartyPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 35%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Opponent</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeParty" frameborder="0" runat="server" width="100%" height="500px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add Legal Case category--%>
            <div class="modal fade" id="AddCategoryType" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 40%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add/Edit Case Type</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" overflow="hidden" id="IframeCategoryType" frameborder="0" runat="server" style="width: 100%; height: 232px; margin-top: 20px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add Court--%>
            <div class="modal fade" id="AddCourtsPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 45%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New Court</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeCourt" overflow="hidden" frameborder="0" runat="server" width="100%" height="480px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add User--%>
            <div class="modal fade" id="AddUserPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 45%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add New User</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeAddUser" frameborder="0" runat="server" width="100%" height="400px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%-- Share Doc pop up--%>
             <div class="modal fade" id="divDocumentsharePopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p5" style="width: 82%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Add Sharing Details</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="Iframe_Docshare" frameborder="0" runat="server" width="100%" height="600px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%-- Adv Bill Document pop up--%>
            <div>
                <div class="modal fade" id="DocumentAdvBillReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                    <div class="modal-dialog" style="width: 100%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div style="width: 100%;">
                                    <div style="float: left; width: 10%">
                                        <table width="100%" style="text-align: left; margin-left: 5%;">
                                            <thead>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="UpdatePanel15" runat="server" UpdatleMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Repeater ID="rptAdvBilldocumentview" runat="server" OnItemCommand="rptAdvBilldocumentview_ItemCommand"
                                                                    OnItemDataBound="rptAdvBilldocumentview_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="tblComplianceDocumnets">
                                                                            <thead>
                                                                                <th>File Name</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("DocTypeInstanceID") + ","+ Eval("Version") + ","+ Eval("ID") %>' ID="lblAdvDocumentVersionView"
                                                                                            runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("FileName").ToString().Substring(0,10) %>'></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="lblAdvDocumentVersionView" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="rptAdvBilldocumentview" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div style="float: right; width: 90%">
                                        <asp:Label runat="server" ID="Label3" Style="color: red;"></asp:Label>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="CaseAdvBillDocViewFrame" runat="server" width="100%" height="535px"></iframe>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--End Adv Bill Document pop up--%>

            <div>
                <div class="modal fade" id="DocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                    <div class="modal-dialog" style="width: 100%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div style="width: 100%;">
                                    <div style="float: left; width: 10%">
                                        <table width="100%" style="text-align: left; margin-left: 5%;">
                                            <thead>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Repeater ID="rptDocmentVersionView" runat="server" OnItemCommand="rptDocmentVersionView_ItemCommand"
                                                                    OnItemDataBound="rptDocmentVersionView_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="tblComplianceDocumnets">
                                                                            <thead>
                                                                                <th>File Name</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("DocTypeInstanceID") + ","+ Eval("Version") + ","+ Eval("ID") %>' ID="lblDocumentVersionView"
                                                                                            runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("FileName").ToString().Substring(0,10) %>'></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="rptDocmentVersionView" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div style="float: right; width: 90%">
                                        <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="CaseDocViewFrame" runat="server" width="100%" height="535px"></iframe>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%-- Payment Document pop up--%>
            <div>
                <div class="modal fade" id="PaymentDocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                    <div class="modal-dialog" style="width: 100%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body" style="height: 570px;">
                                <div style="width: 100%;">
                                    <div style="float: left; width: 10%">
                                        <table width="100%" style="text-align: left; margin-left: 5%;">
                                            <thead>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:UpdatePanel ID="UpdatePanel10" runat="server" UpdatleMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Repeater ID="rptPaymentView" runat="server" OnItemCommand="rptPaymentDocmentView_ItemCommand"
                                                                    OnItemDataBound="rptPaymentDocmentView_ItemDataBound">
                                                                    <HeaderTemplate>
                                                                        <table id="tblComplianceDocumnets">
                                                                            <thead>
                                                                                <th>File Name</th>
                                                                            </thead>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("InvoiceNo") + ","+ Eval("NoticeCaseInstanceID") + ","+ Eval("ID") %>' ID="lblDocumentView"
                                                                                            runat="server" ToolTip='<%# Eval("Name")%>' Text='<%# Eval("Name")%>'></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="lblDocumentView" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        </table>
                                                                    </FooterTemplate>
                                                                </asp:Repeater>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="rptPaymentView" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div style="float: right; width: 90%">
                                        <asp:Label runat="server" ID="Label1" Style="color: red;"></asp:Label>
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="PaymentCaseDocViewFrame" runat="server" width="100%" height="535px"></iframe>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--Mail Document Popup--%>
            <div class="modal fade" id="divOpenSendMailPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 50%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 280px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                                Send Mail with Documents</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary7" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="VsCaseSendmailwithDoc" />
                                        <asp:CustomValidator ID="CvDocListWithMail" runat="server" EnableClientScript="False"
                                            ValidationGroup="VsCaseSendmailwithDoc" Display="None" />
                                    </div>
                                    <div class="row" style="margin: 10px">
                                        <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: #ff0000;">*</label>
                                        <label style="width: 13.5%; display: block; float: left; font-size: 13px; color: #333;">To</label>
                                        <asp:TextBox ID="tbxMailTo" runat="server" CssClass="form-control" Width="85%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ErrorMessage="From name can not be empty."
                                            ControlToValidate="tbxMailTo" runat="server" ValidationGroup="VsCaseSendmailwithDoc" Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" runat="server"
                                            ValidationGroup="VsCaseSendmailwithDoc" ErrorMessage="Please enter a valid email."
                                            ControlToValidate="tbxMailTo" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                                    </div>

                                    <div class="row" style="margin: 10px">
                                        <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: #ff0000;">*</label>
                                        <label style="width: 13.5%; display: block; float: left; font-size: 13px; color: #333;">Message</label>
                                        <asp:TextBox ID="tbxMailMsg" runat="server" CssClass="form-control" Width="85%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ErrorMessage="Message can not be empty."
                                            ControlToValidate="tbxMailMsg" runat="server" ValidationGroup="VsCaseSendmailwithDoc" Display="None" />
                                    </div>

                                    <asp:GridView runat="server" ID="grdShowDocumentList" AutoGenerateColumns="false" CssClass="table" GridLines="none" BorderWidth="0px"
                                        OnRowCommand="grdShowDocumentList_RowCommand" CellPadding="4" Width="100%" OnRowDataBound="grdShowDocumentList_RowDataBound"
                                        OnPageIndexChanging="grdShowDocumentList_PageIndexChanging" AllowPaging="True" PageSize="10" AutoPostBack="true">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkAllDocument" runat="server" OnCheckedChanged="chkAllDocument_CheckedChanged" AutoPostBack="true" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkDocument" runat="server" OnCheckedChanged="chkDocument_CheckedChanged" AutoPostBack="true" />
                                                    <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblFilePath" runat="server" Text='<%# Eval("FilePath") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                <ItemTemplate>
                                                    <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Type" ItemStyle-Width="5%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocType" runat="server" Text='<%# ShowCaseDocType((string)Eval("DocType")) %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Document Name">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px">
                                                        <asp:Label ID="lblDocument" runat="server" Text='<%# Eval("FileName") %>' ToolTip='<%# Eval("FileName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Right" />
                                        <RowStyle CssClass="clsROWgrid" />
                                        <HeaderStyle CssClass="clsheadergrid" />
                                        <EmptyDataTemplate>
                                            No Record Found
                                        </EmptyDataTemplate>

                                    </asp:GridView>

                                    <div class="row">
                                        <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; width: 15%; float: left;"></asp:Label>
                                        <asp:Button Text="Send" runat="server" ID="btnSendDocumentMail" CssClass="btn btn-primary" OnClick="btnSendDocumentMail_Click"
                                            Style="margin-left: 200px;" ValidationGroup="VsCaseSendmailwithDoc" Visible="false"></asp:Button>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div id="divDocCount_ImportRenew" class="row col-md-12 pl0" style="display: none;">
                                <div class="col-md-12 text-left">
                                    <asp:Label runat="server" ID="lblImportDocCount" Text="" CssClass="control-label"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--Notice/Case History Popup--%>
            <div class="modal fade" id="divNoticeCaseHistoryPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p0" style="width: 100%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom" id="historyPopUpHeader">
                                Case History</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IFrameCaseHistory" frameborder="0" runat="server" width="100%" height="450px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Link Case(s) Popup--%>
            <div class="modal fade" id="divLinkCasePopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p5" style="width: 80%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                Link Case(s)</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <asp:UpdatePanel ID="upLinkCases" runat="server">
                                <ContentTemplate>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="vsLinkCase" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="VGLinkCases" />
                                        <asp:CustomValidator ID="cvLinkCase" runat="server" EnableClientScript="False"
                                            ValidationGroup="VGLinkCases" Display="None" />
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 colpadding0">
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 9%; margin-right: 10px;">
                                                <asp:DropDownListChosen runat="server" ID="ddlLinkCaseStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                                    DataPlaceHolder="Select Status" class="form-control" Width="100%">
                                                    <asp:ListItem Text="All" Value="-1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Open" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Closed" Value="3"></asp:ListItem>
                                                </asp:DropDownListChosen>
                                            </div>
                                            <div class="col-md-6 colpadding0" style="margin-top: 5px; width: 49.8%; margin-right: 5px;">
                                                <asp:TextBox runat="server" ID="tbxtypeTofilter" Width="100%" AutoComplete="off" placeholder="Type to Search" CssClass="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 8%; float: right;">
                                            <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lnkLinkCaseFilter" Style="float: right; width: 85%" OnClick="lnkLinkCaseFilter_Click" data-toggle="tooltip" ToolTip="Apply" data-placement="bottom" />
                                        </div>
                                    </div>
                                    <div class="row" style="margin: 10px">
                                        <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: #ff0000;">*</label>
                                        <label style="display: block; float: left; font-size: 13px; color: #333;">Select One or More Case(s) and Click on Save to Link Case(s)</label>
                                    </div>

                                    <div class="row" style="margin: 10px; max-height: 350px; overflow-y: auto;">
                                        <asp:GridView runat="server" ID="grdCaseList_LinkCase" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            AllowPaging="false" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="CaseInstanceID">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCaseInstanceID" runat="server" Text='<%# Eval("CaseInstanceID") %>' Visible="false"></asp:Label>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkHeaderLinkCases" runat="server" onclick="javascript:checkAll(this)" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRowLinkCases" runat="server" onclick="javascript:checkUncheckRow(this)" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Type" ItemStyle-Width="10%" Visible="false">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CaseTypeName") %>' ToolTip='<%# Eval("CaseTypeName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Court Case No." ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CaseRefNo") %>' ToolTip='<%# Eval("CaseRefNo") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Case" ItemStyle-Width="30%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CaseTitle") %>' ToolTip='<%# Eval("CaseTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Opponent" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("PartyName") %>' ToolTip='<%# Eval("PartyName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Entity" ItemStyle-Width="17%" Visible="false">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Department" ItemStyle-Width="10%" Visible="false">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DeptName") %>' ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Stage" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                                            <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CaseStage") %>' ToolTip='<%# Eval("CaseStage") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <PagerSettings Visible="false" />

                                            <PagerTemplate>
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Record Found
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                    <div id="divLinkCaseSaveCount" class="row col-md-12 plr0" style="display: none;">
                                        <div class="col-md-5 text-left">
                                            <asp:Label runat="server" ID="lblTotalCaseSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                        </div>
                                        <div class="col-md-7 text-left">
                                            <asp:Button Text="Save" runat="server" ID="btnSaveLinkCase" CssClass="btn btn-primary" OnClick="btnSaveLinkCase_Click" ValidationGroup="VGLinkCases"></asp:Button>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <%--END--Link Case(s) Popup--%>

            <%--Notice/Case Opposition Lawyer and Lawyer--%>
            <div class="modal fade" id="AddOpposiLawyerPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p0" style="width: 90%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom" id="Oppostitionlawyermodel">
                                <%if (Pernodkey) {%>
                                Add New External Lawyer
                                <%} else {%>
                                 Add New Oppostion Lawyer
                                <%} %>
                            </label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IFrameOppoLawyer" frameborder="0" runat="server" width="100%" height="450px"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <%--END--Notice/Case Opposition Lawyer and Lawyer Popup--%>

            <%--Notice/Case Document List--%>
            <div class="modal fade" id="AddDocumentList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p0" style="width: 60%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom" id="Documentmodel">
                                Add Documents</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IFrameManageDocument" frameborder="0" runat="server" style="width: 100%; height: 380px;"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <%--END--Notice/Case Document ListPopup--%>
            <div class="modal fade" id="AddLawFirmModelPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p0" style="width: 50%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom" id="Lawfirmmodel">
                                Add New Law Firm</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IFLawFirm" frameborder="0" runat="server" width="100%" height="450px"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <%--Add Criteria --%>
            <div class="modal fade" id="AddLayerRatingCriteriaShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog pt10 pb10">
                    <div class="modal-content" style="width: 80%">
                        <div class="modal-header">
                            <label class="modal-header-custom">
                                Add New Criteria</label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -9px;" onclick="CloseLayerRatingCriteriaPage();">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="IframeLayerRatingCriteria" src="about:blank" width="95%" height="232" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <%--Add Criteria End --%>
        </div>
        <div id="HCDocumentPopup">
            <div id="gridHCHistoryDocument"></div>
        </div>
        <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 70%;">
                <div class="modal-content">
                    <div class="modal-header" style="border: none;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div class="col-md-12 colpadding0">
                                <asp:Label runat="server" ID="Label4" Style="color: red;"></asp:Label>
                                <fieldset style="height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="OverViews" width="100%" height="100%" frameborder="0"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script type="text/javascript">


        $(document).ready(function () {

            $("#<%=tbxBranch.ClientID %>").unbind('click');

            $("#<%=tbxBranch.ClientID %>").click(function () {
                $("#divBranches").toggle("blind", null, 500, function () { });
            });
            $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
            try {
                //    window.parent.setIframeHeight(document.body.scrollHeight + "px");
            } catch (e) { }

            if ($("#<%=tbxBranch.ClientID %>") != null) {
                $("#<%=tbxBranch.ClientID %>").unbind('click');

                $("#<%=tbxBranch.ClientID %>").click(function () {
                    $("#divBranches").toggle("blind", null, 500, function () { });
                });
            }

        });
        $(document).ready(function () {

            ddlCaseCategoryChange();
        });
        function CloseLawFirmModal() {

            $('#AddLawFirmModelPopup').modal('hide');
            document.getElementById('<%= lnkLawFirmBind.ClientID %>').click();
        }

        function ddlActChange() {

            var selectedActID = $("#ddlAct").find("option:selected").text();
            var strAddNew = "Add New";
            if (selectedActID != null) {
                if (selectedActID.indexOf(strAddNew) != -1) {
                    $("#lnkShowAddNewActModal").show();
                }
                else {
                    $("#lnkShowAddNewActModal").hide();
                }
            }
        }


        function ddlPartyChange() {

            var selectedPartyID = $("#ddlParty").find("option:selected").text();
            var strAddNew = "Add New";
            if (selectedPartyID != null) {
                if (selectedPartyID.indexOf(strAddNew) != -1) {
                    $("#lnkShowAddNewPartyModal").show();
                }
                else {
                    $("#lnkShowAddNewPartyModal").hide();
                }
            }
        }

        function ddlTaskUserChange() {
            var selectedTaskUserID = $("#<%=ddlTaskUserExternal.ClientID %>").val();
            if (selectedTaskUserID != null) {
                if (selectedTaskUserID == 0) {
                    $("#lnkShowAddNewOwnerModal").show();
                }
                else {
                    $("#lnkShowAddNewOwnerModal>").hide();
                }
            }
        }

        function ddlCaseCategoryChange() {
            var divemmami = document.getElementById('emmamiusers');
            var selectedCaseCatID = $('#ddlCaseCategory :selected').text();

            if (selectedCaseCatID != null) {
                if (("<%=CustomerID.Equals(76)%>") == "True") {
                    $('#emmamiusers').show();
                    $("#lnkAddNewCaseCategoryModal").hide();

                }
                else {
                    $('#emmamiusers').hide();

                }
                if (selectedCaseCatID == "Add New") {
                    $("#lnkAddNewCaseCategoryModal").show();
                }
                else {
                    $("#lnkAddNewCaseCategoryModal").hide();
                }

            }

        }




        function ShowLawFirmAddbutton() {
            var selectedPartyID = $("#ddlLawFirm").find("option:selected").text();
            if (selectedPartyID != null) {
                if (selectedPartyID == "Add New") {
                    $("#lnkShowAddNewLawFirmModal").show();
                }
                else {
                    $("#lnkShowAddNewLawFirmModal").hide();
                }
            }
        }


        function ddlCourtChange() {

            var selectedCaseCatID = $("#<%=ddlCourt.ClientID %>").val();
            if (selectedCaseCatID != null) {
                if (selectedCaseCatID == 0) {
                    $("#lnkAddNewCourtModal").show();
                }
                else {
                    $("#lnkAddNewCourtModal").hide();
                }
            }
        }

        function ddlDepartmentChange() {
            var selectedDeptID = $("#<%=ddlDepartment.ClientID %>").val();
            if (selectedDeptID != null) {
                if (selectedDeptID == 0) {
                    $("#lnkAddNewDepartmentModal").show();
                }
                else {
                    $("#lnkAddNewDepartmentModal").hide();
                }
            }
        }

        function ddlTabHearingRefNoChange() {
            var selectedRefID = $("#<%=ddlTabHearingRef.ClientID %>").val();
            if (selectedRefID != null) {
                if (selectedRefID == 0) {
                    $("#divTabHearingDate").show();
                }
                else {
                    $("#divTabHearingDate").hide();
                }
            }
        }

        function ddlRefNoChange() {
            var selectedRefID = $("#<%=ddlHearingRefNo.ClientID %>").val();
            if (selectedRefID != null) {
                if (selectedRefID == 0) {
                    $("#divHearingDate").show();
                }
                else {
                    $("#divHearingDate").hide();
                }
            }
        }

        function ddlStatusChange() {
            var selectedStatusID = $("#<%=ddlCaseStatus.ClientID %>").val();
            if (selectedStatusID != null) {
                if (selectedStatusID == 3) {
                    $("#divClosureDetail").show();
                    //$("#divClosureRemark").show();
                    $("#divConvertToCase").show();
                    $("#divResult").show();
                }
                else {
                    $("#divClosureDetail").hide();
                    //$("#divClosureRemark").hide();
                    $("#divConvertToCase").hide();
                    $("#divResult").hide();
                    $("#btnSaveStatus").removeAttr("disabled");
                }
            }
        }

        function rblImpactChange() {
            var radioButtonlist = document.getElementsByName("<%=rblPotentialImpact.ClientID%>");
            for (var x = 0; x < radioButtonlist.length; x++) {
                if (radioButtonlist[x].checked) {
                    var selectedImpactID = radioButtonlist[x].value;
                    if (selectedImpactID != null && selectedImpactID != '') {
                        if (selectedImpactID == 'M') {
                            $("#divMonetory").show();
                            $("#divNonMonetory").hide();
                        }
                        else if (selectedImpactID == 'N') {
                            $("#divMonetory").hide();
                            $("#divNonMonetory").show();
                        }
                        else if (selectedImpactID == 'B') {
                            $("#divMonetory").show();
                            $("#divNonMonetory").show();
                        }
                    }
                    x = radioButtonlist.length;
                }
            }
        }

        function CloseCourtMasterPopUp() {
            $('#AddCourtsPopUp').modal('hide');
            document.getElementById('<%= lblCourt.ClientID %>').click();
        }

        function ClosePopDepartment() {
            $('#AddDepartmentPopUp').modal('hide');
            document.getElementById('<%= lnkBtnDept.ClientID %>').click();
        }

        function ClosePopAct() {
            $('#AddActPopUp').modal('hide');
            document.getElementById('<%= lnkBtnAct.ClientID %>').click();
        }

        function ClosePopParty() {
            $('#AddPartyPopUp').modal('hide');
            document.getElementById('<%= lnkBtnParty.ClientID %>').click();
        }


        function CloseUploadDocumentPopup() {
            $('#AddDocumentList').modal('hide');
            document.getElementById('<%= lnkBindshowDocumentCase.ClientID %>').click();
        }

        function CloseCaseTypePopUp() {
            $('#AddCategoryType').modal('hide');
            document.getElementById('<%= lnkBtnCategory.ClientID %>').click();
        }

        function CloseFirmLawyerPop() {
            $('#AddOpposiLawyerPopUp').modal('hide');
            document.getElementById('<%= lnkLawyers.ClientID %>').click();
        }

        function CloseOppoLawyerPop() {
            $('#AddOpposiLawyerPopUp').modal('hide');
            document.getElementById('<%= lnkOpposiLawyer.ClientID %>').click();
        }

        function CloseCriteriaPopUp() {

            $('#AddLayerRatingCriteriaShowDialog').modal('hide');
            document.getElementById('<%# lnkBtnRebindRating.ClientID %>').click();
        }

        function ClosePopUser() {
            $('#AddUserPopUp').modal('hide');
            document.getElementById('<%= lnkBtnAssignUser.ClientID %>').click();
        }


        function applyCSSToCaseDate() {
            $('#<%= txtCaseDate.ClientID %>').removeClass();
            $('#<%= txtCaseDate.ClientID %>').addClass('form-control');
        }

        function applyCSSToNoticeDate() {
            $('#<%= txtNoticeDate.ClientID %>').removeClass();
            $('#<%= txtNoticeDate.ClientID %>').addClass('form-control');
        }
        function applyCSSToInvoiceDateDate() {
            $('#<%= tbxinvoicedate.ClientID %>').removeClass();
            $('#<%= tbxinvoicedate.ClientID %>').addClass('form-control');
        }

    </script>

    <script type="text/javascript">


        function checkAll(chkHeader) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdCaseList_LinkCase.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }

                var btnSaveLinkCase = document.getElementById("<%=btnSaveLinkCase.ClientID %>");
                var lblTotalCaseSelected = document.getElementById("<%=lblTotalCaseSelected.ClientID %>");
                if ((btnSaveLinkCase != null || btnSaveLinkCase != undefined) && (lblTotalCaseSelected != null || lblTotalCaseSelected != undefined)) {
                    if (selectedRowCount > 0) {
                        lblTotalCaseSelected.innerHTML = selectedRowCount + " Selected";
                        divLinkCaseSaveCount.style.display = "block";
                    }
                    else {
                        lblTotalCaseSelected.innerHTML = "";;
                        divLinkCaseSaveCount.style.display = "none";
                    }
                }
            }
        }


        function checkUncheckRow(clickedCheckBoxObj) {

            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdCaseList_LinkCase.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

            var btnSaveLinkCase = document.getElementById("<%=btnSaveLinkCase.ClientID %>");
            var lblTotalCaseSelected = document.getElementById("<%=lblTotalCaseSelected.ClientID %>");
            if ((btnSaveLinkCase != null || btnSaveLinkCase != undefined) && (lblTotalCaseSelected != null || lblTotalCaseSelected != undefined)) {
                if (selectedRowCount > 0) {
                    lblTotalCaseSelected.innerHTML = selectedRowCount + " Selected";
                    divLinkCaseSaveCount.style.display = "block";
                }
                else {
                    lblTotalCaseSelected.innerHTML = "";;
                    divLinkCaseSaveCount.style.display = "none";
                }
            }
        }

        function unCheckAll() {
            var grid = document.getElementById("<%=grdCaseList_LinkCase.ClientID %>");
            if (grid != null) {
                //Get all input elements in Gridview
                var inputList = grid.getElementsByTagName("input");

                for (var i = 1; i < inputList.length; i++) {
                    if (inputList[i].type == "checkbox") {
                        inputList[i].checked = false;
                    }
                }
            }
        }



    </script>

    <script type="text/javascript">
        function checkAll_Import(chkHeader) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdCaseDocuments.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }

                var lblTotalSelected = document.getElementById("<%=lblImportDocCount.ClientID %>");
                if ((lblTotalSelected != null || lblTotalSelected != undefined)) {
                    if (selectedRowCount > 0) {
                        lblTotalSelected.innerHTML = selectedRowCount + " Selected";
                        divDocCount_ImportRenew.style.display = "block";
                    }
                    else {
                        lblTotalSelected.innerHTML = "";;
                        divDocCount_ImportRenew.style.display = "none";
                    }
                }
            }
        }

        function checkUncheckRow_Import(clickedCheckBoxObj) {
            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdCaseDocuments.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

            var lblTotalSelected = document.getElementById("<%=lblImportDocCount.ClientID %>");
            if ((lblTotalSelected != null || lblTotalSelected != undefined)) {
                if (selectedRowCount > 0) {
                    lblTotalSelected.innerHTML = selectedRowCount + " Selected";
                    divDocCount_ImportRenew.style.display = "block";
                }
                else {
                    lblTotalSelected.innerHTML = "";;
                    divDocCount_ImportRenew.style.display = "none";
                }
            }
        }
        function fcheckcontract(obj) {
            var span = $(obj).parent('span.label.label - info');
            $(span).addClass('label-info-selected')
        }
    </script>

</body>
</html>
