﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class AddEditReminder : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                    {
                        var reminderID = Request.QueryString["AccessID"];
                        if (reminderID != "")
                        {
                            ViewState["ReminderID"] = reminderID;

                            if (Convert.ToInt32(reminderID) == 0)
                            {
                                btnAddCase_Click(sender, e);  //Add Detail 
                            }
                            else
                            {
                                btnEditCase_Click(sender, e); //Edit Detail
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlTypePopup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                int priorityID = -1;
                int partyID = -1;
                int deptID = -1;
                int Status = -1;
                string Type = string.Empty;

                if (!String.IsNullOrEmpty(ddlTypePopup.SelectedValue))
                {
                    var branchList = new List<int>();
                    ddlTitlePopup.Items.Clear();
                    if (ddlTypePopup.SelectedValue == "1") //1--Notice
                    {
                        var lstNoticeDetails = NoticeManagement.GetAssignedNoticeList(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, Status, Type);
                        if (lstNoticeDetails.Count > 0)
                        {
                            ddlTitlePopup.DataTextField = "NoticeTitle";
                            ddlTitlePopup.DataValueField = "NoticeInstanceID";
                            ddlTitlePopup.DataSource = lstNoticeDetails;
                            ddlTitlePopup.DataBind();
                        }
                        else
                        {
                            ddlTitlePopup.Items.Insert(0, new ListItem("No Notice Record Found", "-1"));
                        }
                    }
                    else if (ddlTypePopup.SelectedValue == "2") //2--Case
                    {
                        var lstCaseDetails = CaseManagement.GetAssignedCaseList(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, Status, Type);
                        if (lstCaseDetails.Count > 0)
                        {
                            ddlTitlePopup.DataTextField = "CaseTitle";
                            ddlTitlePopup.DataValueField = "CaseInstanceID";
                            ddlTitlePopup.DataSource = lstCaseDetails;
                            ddlTitlePopup.DataBind();
                        }
                        else
                        {
                            ddlTitlePopup.Items.Insert(0, new ListItem("No Case Record Found", "-1"));
                        }
                    }
                    else if (ddlTypePopup.SelectedValue == "3") //3--Task
                    {
                        var lstTaskDetails = LitigationTaskManagement.GetAssignedTaskList(AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, priorityID, partyID, deptID, Status, Type, AuthenticationHelper.CustomerID);

                        if (lstTaskDetails.Count > 0)
                        {
                            ddlTitlePopup.DataTextField = "TaskTitle";
                            ddlTitlePopup.DataValueField = "TaskID";
                            ddlTitlePopup.DataSource = lstTaskDetails;
                            ddlTitlePopup.DataBind();
                        }
                        else
                        {
                            ddlTitlePopup.Items.Insert(0, new ListItem("No Task Record Found", "-1"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //protected void ddlTypePage_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        long customerID = -1;
        //        //customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
        //        customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

        //        int priorityID = -1;
        //        int partyID = -1;
        //        int deptID = -1;
        //        int Status = -1;
        //        string Type = string.Empty;

        //        if (!String.IsNullOrEmpty(ddlTypePage.SelectedValue))
        //        {
        //            var branchList = new List<int>();

        //            ddlTitle.Items.Clear();

        //            if (ddlTypePage.SelectedValue == "1") //1--Notice
        //            {
        //                var lstNoticeDetails = NoticeManagement.GetAssignedNoticeList(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, Status, Type);
        //                if (lstNoticeDetails.Count > 0)
        //                {
        //                    ddlTitle.DataTextField = "NoticeTitle";
        //                    ddlTitle.DataValueField = "NoticeInstanceID";
        //                    ddlTitle.DataSource = lstNoticeDetails;
        //                    ddlTitle.DataBind();
        //                }
        //                else
        //                {
        //                    ddlTitle.Items.Insert(0, new ListItem("No Notice Assigned to you"));
        //                }
        //            }
        //            else if (ddlTypePage.SelectedValue == "2") //2--Case
        //            {
        //                var lstCaseDetails = CaseManagement.GetAssignedCaseList(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, Status, Type);
        //                if (lstCaseDetails.Count > 0)
        //                {
        //                    ddlTitle.DataTextField = "CaseTitle";
        //                    ddlTitle.DataValueField = "CaseInstanceID";
        //                    ddlTitle.DataSource = lstCaseDetails;
        //                    ddlTitle.DataBind();
        //                }
        //                else
        //                {
        //                    ddlTitle.Items.Insert(0, new ListItem("No Case Assigned to you"));
        //                }

        //            }
        //            else if (ddlTypePage.SelectedValue == "3") //3--Task
        //            {
        //                var lstTaskDetails = LitigationTaskManagement.GetAssignedTaskList(AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, priorityID, partyID, deptID, Status, Type);
        //                if (lstTaskDetails.Count > 0)
        //                {
        //                    ddlTitle.DataTextField = "TaskTitle";
        //                    ddlTitle.DataValueField = "TaskID";
        //                    ddlTitle.DataSource = lstTaskDetails;
        //                    ddlTitle.DataBind();
        //                }
        //                else
        //                {
        //                    ddlTitle.Items.Insert(0, new ListItem("No Task Assigned to you"));
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        protected void btnAddCase_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;

                ddlTypePopup_SelectedIndexChanged(sender, e);

                btnSave.Enabled = true;

                btnSave.Visible = true;
                btnSave.Text = "Save";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnEditCase_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 1;

                if (ViewState["ReminderID"] != null)
                {
                    long reminderID = Convert.ToInt32(ViewState["ReminderID"]);

                    if (reminderID != 0)
                    {
                        var reminderDetail = LitigationManagement.GetLitigationReminderDetailByID(reminderID);

                        if (reminderDetail != null)
                        {
                            int typeID = -1;

                            if (reminderDetail.Type == "N")
                                typeID = 1;
                            else if (reminderDetail.Type == "C")
                                typeID = 2;
                            else if (reminderDetail.Type == "T")
                                typeID = 3;

                            ddlTypePopup.ClearSelection();
                            if (ddlTypePopup.Items.FindByValue(typeID.ToString()) != null)
                                ddlTypePopup.Items.FindByValue(typeID.ToString()).Selected = true;

                            ddlTypePopup_SelectedIndexChanged(sender, e);

                            ddlTitlePopup.ClearSelection();
                            if (ddlTitlePopup.Items.FindByValue(reminderDetail.InstanceID.ToString()) != null)
                                ddlTitlePopup.Items.FindByValue(reminderDetail.InstanceID.ToString()).Selected = true;

                            txtReminderTitle.Text = reminderDetail.ReminderTitle;
                            txtReminderDesc.Text = reminderDetail.Description;
                            txtRemindOn.Text = reminderDetail.RemindOn.ToString("dd-MM-yyyy");
                            txtRemark.Text = reminderDetail.Remark;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool formValidateSuccess = false;
                bool saveSuccess = false;

                #region Data Validation

                if (!String.IsNullOrEmpty(ddlTypePopup.SelectedValue) && ddlTypePopup.SelectedValue != "-1")
                {
                    if (!String.IsNullOrEmpty(ddlTitlePopup.SelectedValue) && ddlTitlePopup.SelectedValue != "-1" && ddlTitlePopup.SelectedValue != "0")
                    {
                        if (txtReminderTitle.Text != "")
                        {
                            if (txtReminderDesc.Text != "")
                            {
                                if (txtRemindOn.Text != "")
                                {
                                    if (DateTimeExtensions.GetDate(txtRemindOn.Text).Date > DateTime.Now.Date)
                                        formValidateSuccess = true;
                                    else
                                    {
                                        cvReminderError.IsValid = false;
                                        cvReminderError.ErrorMessage = "Reminder Date Should be greater than Current Date";
                                    }
                                }
                                else
                                {
                                    cvReminderError.IsValid = false;
                                    cvReminderError.ErrorMessage = "Provide Reminder Date";
                                }
                            }
                            else
                            {
                                cvReminderError.IsValid = false;
                                cvReminderError.ErrorMessage = "Provide Reminder Description";
                            }
                        }
                        else
                        {
                            cvReminderError.IsValid = false;
                            cvReminderError.ErrorMessage = "Provide Reminder Title";
                        }
                    }
                    else
                    {
                        cvReminderError.IsValid = false;
                        cvReminderError.ErrorMessage = "Select Title (i.e. Case/ Notice/ Task Title)";
                    }
                }
                else
                {
                    cvReminderError.IsValid = false;
                    cvReminderError.ErrorMessage = "Select Type (i.e. Case/ Notice/ Task)";
                }
                #endregion

                #region Save/Edit Code

                if (formValidateSuccess)
                {
                    string selectedType = string.Empty;

                    if (ddlTypePopup.SelectedValue == "1")
                        selectedType = "N";
                    else if (ddlTypePopup.SelectedValue == "2")
                        selectedType = "C";
                    else if (ddlTypePopup.SelectedValue == "3")
                    {
                        if (!string.IsNullOrEmpty(ddlTitlePopup.SelectedValue))
                        {
                            if (ddlTitlePopup.SelectedValue != "-1")
                            {
                                selectedType = LitigationTaskManagement.GetTaskTypebyID(Convert.ToInt32(ddlTitlePopup.SelectedValue));
                            }
                        }
                        else
                        {
                            selectedType = "T";
                        }
                    }

                    tbl_LitigationCustomReminder newReminder = new tbl_LitigationCustomReminder()
                    {
                        Type = selectedType,
                        InstanceID = Convert.ToInt32(ddlTitlePopup.SelectedValue),
                        UserID = AuthenticationHelper.UserID,
                        ReminderTitle = txtReminderTitle.Text,
                        Description = txtReminderDesc.Text,
                        Remark = txtRemark.Text,
                        RemindOn = DateTimeExtensions.GetDate(txtRemindOn.Text),
                        Status = 0,
                        IsDeleted = false,
                        CreatedBy = AuthenticationHelper.UserID,
                    };

                    if ((int) ViewState["Mode"] == 0)
                    {
                        if (!LitigationManagement.ExistsLitigationReminder(newReminder, 0))
                            saveSuccess = LitigationManagement.CreateLitigationReminder(newReminder);
                        else
                        {
                            saveSuccess = false;

                            cvReminderError.IsValid = false;
                            cvReminderError.ErrorMessage = "Reminder with same details already exists";
                        }

                        if (saveSuccess)
                        {
                            cvReminderError.IsValid = false;
                            cvReminderError.ErrorMessage = "Reminder Saved Successfully.";
                            vsReminder.CssClass = "alert alert-success";
                            txtReminderTitle.Text = "";
                            txtReminderDesc.Text = "";
                            txtRemark.Text = "";
                            txtRemindOn.Text = "";
                            ddlTypePopup.SelectedValue = "1";
                            ddlTypePopup_SelectedIndexChanged(null, null);
                        }
                    }
                    else if ((int) ViewState["Mode"] == 1)
                    {
                        if (ViewState["ReminderID"] != null)
                        {
                            long reminderID = Convert.ToInt32(ViewState["ReminderID"]);

                            if (reminderID != 0)
                            {
                                newReminder.ID = reminderID;
                                newReminder.UpdatedBy = AuthenticationHelper.UserID;

                                if (!LitigationManagement.ExistsLitigationReminder(newReminder, newReminder.ID))
                                    saveSuccess = LitigationManagement.UpdateLitigationReminder(newReminder);
                                else
                                {
                                    saveSuccess = false;

                                    cvReminderError.IsValid = false;
                                    cvReminderError.ErrorMessage = "Reminder with same details already exists";

                                    txtReminderTitle.Focus();
                                }

                                if (saveSuccess)
                                {
                                    cvReminderError.IsValid = false;
                                    cvReminderError.ErrorMessage = "Reminder Updated Successfully";

                                    txtReminderTitle.Text = "";
                                    txtReminderDesc.Text = "";
                                    txtRemark.Text = "";
                                    txtRemindOn.Text = "";
                                    ddlTypePopup.SelectedValue = "1";
                                    ddlTypePopup_SelectedIndexChanged(null, null);
                                }
                            }
                        }
                    }
                }

                #endregion

                upReminder.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}