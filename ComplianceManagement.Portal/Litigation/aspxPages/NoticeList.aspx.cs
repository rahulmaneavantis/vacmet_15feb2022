﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class NoticeList : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected bool flag;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    flag = false;
                    BindParty();
                    BindCaseCategoryType();
                    //Bind Tree Views
                   // var branchList = CustomerBranchManagement.GetAllHierarchy(customerID);
                    List<NameValueHierarchy> branches;
                    string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Get<List<NameValueHierarchy>>(key, out branches);
                    }
                    else
                    {
                        branches = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                        CacheHelper.Set<List<NameValueHierarchy>>(key, branches);
                    }
                    bindOwner();
                    BindCustomerBranches(tvFilterLocation, tbxFilterLocation, branches);
                    BindFinancialYear();
                    BindDepartment();

                    if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    {
                        if (Request.QueryString["Status"] == "Open")
                        {
                            ViewState["Status"] = 1;

                            if (ddlStatus.Items.FindByValue("1") != null)
                            {
                                ddlStatus.ClearSelection();
                                ddlStatus.Items.FindByValue("1").Selected = true;
                            }
                        }

                        else if (Request.QueryString["Status"] == "Closed")
                        {
                            ViewState["Status"] = 3;

                            if (ddlStatus.Items.FindByValue("3") != null)
                            {
                                ddlStatus.ClearSelection();
                                ddlStatus.Items.FindByValue("3").Selected = true;
                            }
                        }
                    }

                    BindGrid(); bindPageNumber();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static object FillFnancialYear()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.FinancialYearDetails
                             where row.IsDeleted==false
                             select row).ToList();
                return query;
            }
        }

        public void BindFinancialYear()
        {

            ddlFinancialYear.DataValueField = "Id";
            ddlFinancialYear.DataTextField = "FinancialYear";
            ddlFinancialYear.DataSource = FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }


        public void bindOwner()
        {
            if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
            {
                int noticeStatus = Convert.ToInt32(ddlStatus.SelectedValue);

                var Ownerlist = LitigationUserManagement.GetOwnerListforNotice(CustomerID, noticeStatus);
                ddlOwnerlist.DataValueField = "ID";
                ddlOwnerlist.DataTextField = "Name";
                ddlOwnerlist.DataSource = Ownerlist;
                ddlOwnerlist.DataBind();
                ddlOwnerlist.Items.Insert(0, new ListItem("All", "-1"));
            }
        }

        private void BindCaseCategoryType()
        {
            try
            {
                var lstCaseCaseType = LitigationCourtAndCaseType.GetAllLegalCaseTypeData(CustomerID);

                ddlType.DataTextField = "CaseType";
                ddlType.DataValueField = "ID";

                ddlType.DataSource = lstCaseCaseType;
                ddlType.DataBind();
                ddlType.Items.Insert(0, new ListItem("All", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        {
            try
            {
                treetoBind.Nodes.Clear();

                NameValueHierarchy branch = null;

                if (branchList.Count > 0)
                {
                    branch = branchList[0];
                }

                treeTxtBox.Text = "Select Entity/Branch/Location";

                List<TreeNode> nodes = new List<TreeNode>();

                BindBranchesHierarchy(null, branch, nodes);

                foreach (TreeNode item in nodes)
                {
                    treetoBind.Nodes.Add(item);
                }

                treetoBind.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorNoticePage.IsValid = false;
                cvErrorNoticePage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorNoticePage.IsValid = false;
                cvErrorNoticePage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            //Page DropDown

            ddlDeptPage.DataTextField = "Name";
            ddlDeptPage.DataValueField = "ID";

            ddlDeptPage.DataSource = obj;
            ddlDeptPage.DataBind();

            ddlDeptPage.Items.Insert(0, new ListItem("All", "-1"));
        }

        public void BindParty()
        {
            var obj = LitigationLaw.GetLCPartyDetails(CustomerID);

            //Drop-Down at Page
            ddlPartyPage.DataTextField = "Name";
            ddlPartyPage.DataValueField = "ID";

            ddlPartyPage.DataSource = obj;
            ddlPartyPage.DataBind();

            ddlPartyPage.Items.Insert(0, new ListItem("All", "-1"));
        }

        public void BindGrid()
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                int branchID = -1;
                int partyID = -1;
                int deptID = -1;
                int noticeStatus = -1;
                string noticeType = string.Empty;
                int CategoryType = -1;
                int OwnerID = -1;
                string dlFY = string.Empty;
                if (!string.IsNullOrEmpty(ddlType.SelectedValue))
                {
                    CategoryType = Convert.ToInt32(ddlType.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                {
                    noticeStatus = Convert.ToInt32(ddlStatus.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlOwnerlist.SelectedValue))
                {
                    OwnerID = Convert.ToInt32(ddlOwnerlist.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlNoticeTypePage.SelectedValue))
                {
                    noticeType = ddlNoticeTypePage.SelectedValue;
                }

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlPartyPage.SelectedValue))
                {
                    partyID = Convert.ToInt32(ddlPartyPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }
                //if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue) && ddlFinancialYear.SelectedValue != "-1")
                //{
                //    dlFY = Convert.ToString(ddlFinancialYear.SelectedValue);
                //}
                if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        dlFY = Convert.ToString(ddlFinancialYear.SelectedItem.Text);
                    }
                }
                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                var lstNoticeDetails = NoticeManagement.GetAssignedNoticeList(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, noticeStatus, noticeType);

                if (CategoryType != -1)
                {
                    lstNoticeDetails = lstNoticeDetails.Where(entry => entry.NoticeCategoryID == CategoryType).ToList();
                }
                if (OwnerID != -1)
                {
                    lstNoticeDetails = lstNoticeDetails.Where(entry => entry.OwnerID == OwnerID).ToList();
                }
                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                {
                    lstNoticeDetails = lstNoticeDetails.Where(entry => entry.NoticeTitle.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) || entry.RefNo.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();
                }

                if (dlFY != "" && dlFY != "-1")
                {
                    lstNoticeDetails = lstNoticeDetails.Where(entry => entry.FYName != "" && entry.FYName != null).ToList();
                    string a = dlFY + "" + ",";
                    lstNoticeDetails = lstNoticeDetails.Where(aa => aa.FYName.Contains(a)).ToList();
                 
                }
                //if (dlFY != "")
                //{
                //    lstNoticeDetails = lstNoticeDetails.Where(entry => entry.FYName.Contains(ddlFinancialYear.SelectedValue)).ToList();
                //}

                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            lstNoticeDetails = lstNoticeDetails.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            lstNoticeDetails = lstNoticeDetails.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                flag = true;
                Session["TotalRows"] = null;
                if (lstNoticeDetails.Count > 0)
                {
                    grdNoticeDetails.DataSource = lstNoticeDetails;
                    grdNoticeDetails.DataBind();
                    Session["TotalRows"] = lstNoticeDetails.Count;
                }
                else
                {
                    grdNoticeDetails.DataSource = lstNoticeDetails;
                    grdNoticeDetails.DataBind();
                }

                lstNoticeDetails.Clear();
                lstNoticeDetails = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorNoticePage.IsValid = false;
                cvErrorNoticePage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton) (sender);
                if (btn != null)
                {
                    int noticeInstanceID = Convert.ToInt32(btn.CommandArgument);

                    if (noticeInstanceID != 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + noticeInstanceID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorNoticePage.IsValid = false;
                cvErrorNoticePage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdNoticeDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindGrid();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdNoticeDetails.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlTypePage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(ddlTypePage.SelectedValue))
                {
                    if (ddlTypePage.SelectedValue == "1")
                        Response.Redirect("~/Litigation/aspxPages/NoticeList.aspx", false);

                    else if (ddlTypePage.SelectedValue == "2")
                        Response.Redirect("~/Litigation/aspxPages/CaseList.aspx", false);

                    else if (ddlTypePage.SelectedValue == "3")
                        Response.Redirect("~/Litigation/aspxPages/TaskList.aspx", false);

                    Context.ApplicationInstance.CompleteRequest();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAddNotice_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + 0 + ");", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdNoticeDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DELETE_Notice"))
                {
                    int noticeInstanceID = Convert.ToInt32(e.CommandArgument);
                    int userID = AuthenticationHelper.UserID;
                    //NoticeManagement.DeleteNoticeByID(noticeInstanceID);
                    var NewCase = NoticeManagement.GetNoticeByID(noticeInstanceID);
                    bool saveSuccess = NoticeManagement.DeleteNoticeByID(noticeInstanceID, userID);

                    List<long> OwnerMailID = new List<long>();
                    List<string> OwnerMailList = new List<string>();
                    List<long> UpdatedAssingedUser = new List<long>();
                    List<int> CheckAssingedUserOld = new List<int>();
                    
                    CheckAssingedUserOld = NoticeManagement.GetAllAssingedUserList(noticeInstanceID, AuthenticationHelper.UserID);
                    if (CheckAssingedUserOld.Count > 0)
                    {
                        foreach (var item in CheckAssingedUserOld)
                        {
                            if (CheckAssingedUserOld.Contains(item))
                            {
                                UpdatedAssingedUser.Add(Convert.ToInt64(item));
                            }
                        }
                    }
                    
                    List<string> AssignedUseerMailID = CaseManagement.GetAssignedUserAndOwnerMail(UpdatedAssingedUser);

                    var caseRecord = NewCase;
                    OwnerMailID.Add((long)caseRecord.OwnerID);
                    OwnerMailList = CaseManagement.GetAssignedUserAndOwnerMail(OwnerMailID);

                    var Locations = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(caseRecord.CustomerBranchID)))
                    {
                        Locations = CaseManagement.GetLocationByCaseInstanceID(caseRecord.CustomerBranchID);
                    }

                    List<string> MgmUser = CaseManagement.GetmanagementUser(AuthenticationHelper.CustomerID);
                    List<string> UniqueMail = new List<string>();
                    if (OwnerMailList.Count > 0)
                    {
                        foreach (var item in OwnerMailList)
                        {
                            if (!UniqueMail.Contains(item))
                            {
                                UniqueMail.Add(item);
                            }
                        }
                    }

                    if (MgmUser.Count > 0)
                    {
                        foreach (var item in MgmUser)
                        {
                            if (!UniqueMail.Contains(item))
                            {
                                UniqueMail.Add(item);
                            }
                        }
                    }
                    // Remove assigned user if exist in mgm
                    if (AssignedUseerMailID.Count > 0)
                    {
                        foreach (var item in UniqueMail)
                        {
                            if (!AssignedUseerMailID.Contains(item))
                            {
                                AssignedUseerMailID.Add(item);
                                continue;
                            }
                        }
                        string CaseTitleMerge = caseRecord.NoticeTitle;
                        string FinalCaseTitle = string.Empty;
                        if (CaseTitleMerge.Length > 50)
                        {
                            FinalCaseTitle = CaseTitleMerge.Substring(0, 50);
                            FinalCaseTitle = FinalCaseTitle + "...";
                        }
                        else
                        {
                            FinalCaseTitle = CaseTitleMerge;
                        }

                        var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));
                        string CasePriority = string.Empty;
                        if (NewCase.NoticeRiskID != null)
                        {
                            if (NewCase.NoticeRiskID == 1)
                                CasePriority = "High";
                            else if (NewCase.NoticeRiskID == 2)
                                CasePriority = "Medium";
                            else if (NewCase.NoticeRiskID == 3)
                                CasePriority = "Low";
                        }

                        string portalURL = string.Empty;
                        URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                        if (Urloutput != null)
                        {
                            portalURL = Urloutput.URL;
                        }
                        else
                        {
                            portalURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                        }
                        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_LitigationNoticedeleted
                                   .Replace("@UpdateBy", AuthenticationHelper.User)
                                   .Replace("@CaseRefNo", NewCase.RefNo)
                                   .Replace("@CaseTitle", NewCase.NoticeTitle)
                                   .Replace("@CaseDetailDesc", NewCase.NoticeDetailDesc)
                                   .Replace("@Location", Locations)
                                   .Replace("@From", cname.Trim())
                                   .Replace("@PortalURL", Convert.ToString(portalURL));

                        //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_LitigationNoticedeleted
                        //            .Replace("@UpdateBy", AuthenticationHelper.User)
                        //            .Replace("@CaseRefNo", NewCase.RefNo)
                        //            .Replace("@CaseTitle", NewCase.NoticeTitle)
                        //            .Replace("@CaseDetailDesc", NewCase.NoticeDetailDesc)
                        //            .Replace("@Location", Locations)

                        //            .Replace("@From", cname.Trim())
                        //            .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(AssignedUseerMailID), UniqueMail, null, "Litigation- Notice Removal Notification - " + FinalCaseTitle, message);
                        if (saveSuccess)
                        {
                            BindGrid();
                            bindPageNumber();
                            LitigationManagement.CreateAuditLog("N", noticeInstanceID, "tbl_LegalNoticeInstance", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Deleted", true);

                            cvErrorNoticePage.IsValid = false;
                            cvErrorNoticePage.ErrorMessage = "Notice Deleted Successfully";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorNoticePage.IsValid = false;
                cvErrorNoticePage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        #region Page Number-Bottom

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdNoticeDetails.PageIndex = chkSelectedPage - 1;

            grdNoticeDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindGrid(); ShowGridDetail();
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                    DropDownListPageNo.SelectedValue = null;
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }
                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        #endregion

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdNoticeDetails_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                int branchID = -1;
                int partyID = -1;
                int deptID = -1;
                int noticeStatus = -1;
                string noticeType = string.Empty;
                int CategoryType = -1;
                int OwnerID = -1;

                if (!string.IsNullOrEmpty(ddlType.SelectedValue))
                {
                    CategoryType = Convert.ToInt32(ddlType.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                {
                    noticeStatus = Convert.ToInt32(ddlStatus.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlOwnerlist.SelectedValue))
                {
                    OwnerID = Convert.ToInt32(ddlOwnerlist.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlNoticeTypePage.SelectedValue))
                {
                    noticeType = ddlNoticeTypePage.SelectedValue;
                }

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlPartyPage.SelectedValue))
                {
                    partyID = Convert.ToInt32(ddlPartyPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                var lstNoticeDetails = NoticeManagement.GetAssignedNoticeList(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, noticeStatus, noticeType);
                if (CategoryType != -1)
                {
                    lstNoticeDetails = lstNoticeDetails.Where(entry => entry.NoticeCategoryID == CategoryType).ToList();
                }
                if (OwnerID != -1)
                {
                    lstNoticeDetails = lstNoticeDetails.Where(entry => entry.OwnerID == OwnerID).ToList();
                }
                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                {
                    lstNoticeDetails = lstNoticeDetails.Where(entry => entry.NoticeTitle.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) || entry.RefNo.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();
                }

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    lstNoticeDetails = lstNoticeDetails.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    //direction = SortDirection.Descending;
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    lstNoticeDetails = lstNoticeDetails.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    //direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdNoticeDetails.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdNoticeDetails.Columns.IndexOf(field);
                    }
                }
                ViewState["SortExpression"] = e.SortExpression;
                Session["TotalRows"] = null;
                flag = true;
                if (lstNoticeDetails.Count > 0)
                {
                    grdNoticeDetails.DataSource = lstNoticeDetails;
                    grdNoticeDetails.DataBind();
                    Session["TotalRows"] = lstNoticeDetails.Count;
                }
                else
                {
                    grdNoticeDetails.DataSource = lstNoticeDetails;
                    grdNoticeDetails.DataBind();
                }

                lstNoticeDetails.Clear();
                lstNoticeDetails = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        protected void grdNoticeDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindOwner();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            #region Sheet 1   
            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                string PageType = string.Empty;
                string PageName = string.Empty;
                string check = string.Empty;
                string DeskTitile = string.Empty;
                string RespDate = string.Empty;
                string caseStatus = string.Empty;
                DataTable tableCaseCustomField = new DataTable();
                DataTable tableNoticeCustomField = new DataTable();
                List<string> CaseCustomValueList = new List<string>();
                List<string> NoticeCustomValueList = new List<string>();
                string NameNoticeCase = string.Empty;
                try
                {
                    if (!string.IsNullOrEmpty(ddlTypePage.SelectedValue))
                    {
                        if (ddlTypePage.SelectedValue != "-1")
                        {
                            PageType = ddlTypePage.SelectedValue;
                        }
                    }
                    List<int> CaseInstanceIDDynamic = new List<int>();
                    List<int> NoticeInstanceIDDynamic = new List<int>();
                    List<SP_LitigationCaseReportWithCustomParameter_Result> Casetable = new List<SP_LitigationCaseReportWithCustomParameter_Result>();
                    List<sp_LitigationNoticeReportWithCustomParameter_Result> Noticetable = new List<sp_LitigationNoticeReportWithCustomParameter_Result>();

                    DataView view1 = new DataView();
                    DataView view2 = new DataView();
                    DataTable table = new DataTable();
                    DataTable table1 = new DataTable();
                    int P = 0;
                    tableCaseCustomField.Columns.Add("NoticecaseInstanceID", typeof(string));
                    tableCaseCustomField.Columns.Add("ResponseDate", typeof(string));
                    tableCaseCustomField.Columns.Add("Description", typeof(string));
                    tableCaseCustomField.Columns.Add("Remark", typeof(string));
                    tableCaseCustomField.Columns.Add("CreatedByText", typeof(string));
                    #region Without Litigation Tax
                    {
                        List<HearingDetailReport> HearingDataobj = new List<HearingDetailReport>();
                        List<ResponseDetailReport> ResponseDetailsObj = new List<ResponseDetailReport>();
                        table.Columns.Add("SrNo", typeof(string));
                        table.Columns.Add("NoticeCaseInstanceID", typeof(string));
                        table.Columns.Add("Notice/Case No", typeof(string));
                        table.Columns.Add("Title", typeof(string));
                        table.Columns.Add("NoticeTerm", typeof(string));
                        table.Columns.Add("Location", typeof(string));
                        table.Columns.Add("Jurisdiction", typeof(string));
                        table.Columns.Add("Notice/Case Description", typeof(string));
                        table.Columns.Add("OpenDate", typeof(string));
                        table.Columns.Add("CloseDate", typeof(string));
                        table.Columns.Add("Status", typeof(string));
                        table.Columns.Add("Label", typeof(string));
                        table.Columns.Add("LabelValue", typeof(string));
                        table.Columns.Add("ResponseDate", typeof(string));
                        table.Columns.Add("Description", typeof(string));
                        table.Columns.Add("Remark", typeof(string));
                        table.Columns.Add("CreatedByText", typeof(string));
                        table.Columns.Add("FYName", typeof(string));
                        table.Columns.Add("Provisionalamt", typeof(string));
                        table.Columns.Add("BankGurantee", typeof(string));
                        table.Columns.Add("ProtestMoney", typeof(string));
                        #region Notice


                        using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                        {
                            Noticetable = (from row in entities.sp_LitigationNoticeReportWithCustomParameter(Convert.ToInt32(AuthenticationHelper.CustomerID))
                                           select row).ToList();

                            if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                                Noticetable = Noticetable.Where(entry => (entry.AssingedUserID == AuthenticationHelper.UserID) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                            else // In case of MGMT or CADMN 
                            {
                                Noticetable = Noticetable.Where(entry => entry.RoleID == 3).ToList();
                            }

                            Noticetable = Noticetable.Where(entry => entry.NoticeCategoryID != 47).ToList();

                            if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                            {
                                caseStatus = ddlStatus.SelectedValue;
                                if (caseStatus == "1")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.Status == "Open" || entry.Status == "In Progress").ToList();
                                }
                                if (caseStatus == "3")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.Status == "Closed").ToList();
                                }
                            }

                            if (!string.IsNullOrEmpty(ddlNoticeTypePage.SelectedValue))
                            {
                                if (ddlNoticeTypePage.SelectedValue == "I")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.NoticeType == "I").ToList();
                                }
                                if (ddlNoticeTypePage.SelectedValue == "O")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.NoticeType == "O").ToList();
                                }
                            }

                            if (!string.IsNullOrEmpty(ddlPartyPage.SelectedValue))
                            {
                                if (ddlPartyPage.SelectedValue != "-1")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.PartyID.Contains(ddlPartyPage.SelectedValue)).ToList();
                                }
                            }

                            if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                            {
                                if (ddlDeptPage.SelectedValue != "-1")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.DepartmentID == Convert.ToInt32(ddlDeptPage.SelectedValue)).ToList();
                                }
                            }

                            if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                            {
                                //Noticetable = Noticetable.Where(entry => entry.CustomerBranchID == Convert.ToInt32(tvFilterLocation.SelectedValue)).ToList();
                                int branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(AuthenticationHelper.CustomerID), branchID);
                                if (branchList.Count > 0)
                                    Noticetable = Noticetable.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();
                            }
                            if (!string.IsNullOrEmpty(ddlType.SelectedValue))
                            {
                                if (ddlType.SelectedValue != "-1")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.NoticeCategoryID == Convert.ToInt32(ddlType.SelectedValue)).ToList();
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlOwnerlist.SelectedValue))
                            {
                                if (ddlOwnerlist.SelectedValue != "-1")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.OwnerID == Convert.ToInt32(ddlOwnerlist.SelectedValue)).ToList();
                                }
                            }

                            if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                            {
                                if (ddlFinancialYear.SelectedValue != "-1")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.FYName.Contains(ddlFinancialYear.SelectedItem.Text)).ToList();
                                    // Convert.ToString(ddlFinancialYear.SelectedItem.Text);

                                }
                            }

                            if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                            {
                                Noticetable = Noticetable.Where(entry => entry.Title.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) || entry.RefNo.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();
                            }

                            //new
                            long NoticeIDOld = 0;
                            foreach (var item in Noticetable)
                            {
                                long NoticeID = item.NoticeCaseInstanceID;
                                if (NoticeID != NoticeIDOld)
                                {
                                    NoticeIDOld = item.NoticeCaseInstanceID;
                                    List<ResponseDetailReport> HearingData = CaseManagement.GetResponseDetailNoticeWise(item.NoticeCaseInstanceID);

                                    foreach (var itemValues in HearingData)
                                    {
                                        ResponseDetailsObj.Add(itemValues);
                                        tableCaseCustomField.Rows.Add(itemValues.CaseInstanceID, itemValues.ResponseDate, itemValues.Description, itemValues.Remark, itemValues.CreatedByText);
                                    }
                                }
                            }

                            int beforecondition = 0;
                            for (int i = 0; i < Noticetable.Count; i++)
                            {
                                if (ResponseDetailsObj.Count > 0)
                                {
                                    beforecondition = Convert.ToInt32(Noticetable[i].NoticeCaseInstanceID);//1
                                    for (int j = 0; j <= i; j++)
                                    {
                                        if (ResponseDetailsObj.Count > j)
                                        {
                                            if (ResponseDetailsObj.Count == 1)
                                            {
                                                if (ResponseDetailsObj[0].CaseInstanceID == Noticetable[i].NoticeCaseInstanceID)
                                                {
                                                    Noticetable[i].Remark = ResponseDetailsObj[j].Remark;
                                                    if (!string.IsNullOrEmpty(Convert.ToString(ResponseDetailsObj[j].ResponseDate)))
                                                    {
                                                        Noticetable[i].ResponseDate = Convert.ToDateTime(ResponseDetailsObj[j].ResponseDate).ToString("dd-MMM-yyyy");
                                                    }
                                                    Noticetable[i].Description = ResponseDetailsObj[j].Description;
                                                    Noticetable[i].CreatedBy = ResponseDetailsObj[j].CreatedByText;
                                                    ResponseDetailsObj.RemoveAt(j);
                                                }
                                                break;
                                            }
                                            else
                                            {
                                                if (ResponseDetailsObj[j].CaseInstanceID == Noticetable[i].NoticeCaseInstanceID)
                                                {
                                                    Noticetable[i].Remark = ResponseDetailsObj[j].Remark;
                                                    if (!string.IsNullOrEmpty(Convert.ToString(ResponseDetailsObj[j].ResponseDate)))
                                                    {
                                                        Noticetable[i].ResponseDate = Convert.ToDateTime(ResponseDetailsObj[j].ResponseDate).ToString("dd-MMM-yyyy");
                                                    }
                                                    Noticetable[i].Description = ResponseDetailsObj[j].Description;
                                                    Noticetable[i].CreatedBy = ResponseDetailsObj[j].CreatedByText;
                                                    ResponseDetailsObj.RemoveAt(j);
                                                    j = i;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (ResponseDetailsObj.Count > 0)
                            {
                                for (int k = 0; k < ResponseDetailsObj.Count; k++)
                                {
                                    long IDRemCheck = Convert.ToInt32(ResponseDetailsObj[k].CaseInstanceID);
                                    string RespDates = string.Empty;
                                    if (!string.IsNullOrEmpty(Convert.ToString(ResponseDetailsObj[k].ResponseDate)))
                                    {
                                        RespDates = Convert.ToDateTime(ResponseDetailsObj[k].ResponseDate).ToString("dd-MMM-yyyy");
                                    }
                                    Noticetable.Add(new sp_LitigationNoticeReportWithCustomParameter_Result { NoticeCaseInstanceID = IDRemCheck, Remark = ResponseDetailsObj[k].Remark, ResponseDate = RespDates, Description = ResponseDetailsObj[k].Description, CreatedBy = ResponseDetailsObj[k].CreatedByText });
                                }
                            }


                            Noticetable = Noticetable.OrderBy(entry => entry.NoticeCaseInstanceID).ToList();
                            long NoticeIDOld3 = 0;
                            foreach (var item in Noticetable)
                            {
                                long NoticeID = item.NoticeCaseInstanceID;
                                if (NoticeID != NoticeIDOld3)
                                {
                                    ++P;
                                    CaseInstanceIDDynamic.Add(Convert.ToInt32(item.NoticeCaseInstanceID));
                                    NoticeIDOld3 = item.NoticeCaseInstanceID;
                                    table.Rows.Add(P, item.NoticeCaseInstanceID, item.RefNo, item.Title,item.NoticeTerm, item.BranchName,item.Jurisdiction, item.NoticeCaseDesc, item.OpenDate, item.CloseDate, item.Status, item.Label, item.LabelValue, item.ResponseDate, item.Description, item.Remark, item.CreatedBy,item.FYName,item.Provisionalamt,item.BankGurantee,item.ProtestMoney);
                                }
                                else
                                {
                                    CaseInstanceIDDynamic.Add(0);
                                    table.Rows.Add("", "", "", "", "", "", "", "", "","","", item.Label, item.LabelValue, item.ResponseDate, item.Description, item.Remark, item.CreatedBy);
                                }
                            }

                            PageName = "Notice";
                            DeskTitile = "Response";
                            RespDate = "Response Date";
                            NameNoticeCase = "Response Details";
                        }
                        #endregion

                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add(PageName + " Report");

                        DataTable ExcelData = null;
                        #region First Sheet
                        {
                            view1 = new System.Data.DataView(table);
                            ExcelData = view1.ToTable("Selected", false, "SrNo", "Notice/Case No", "Title", "NoticeTerm","Location", "Jurisdiction", "Notice/Case Description", "OpenDate", "CloseDate", "Status", "Label", "LabelValue", "ResponseDate", "Description", "Remark", "CreatedByText", "FYName", "Provisionalamt", "BankGurantee", "ProtestMoney");

                            foreach (DataRow item in ExcelData.Rows)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(item["OpenDate"])))
                                {
                                    item["OpenDate"] = Convert.ToDateTime(item["OpenDate"]).ToString("dd-MMM-yyyy");
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(item["CloseDate"])))
                                {
                                    item["CloseDate"] = Convert.ToDateTime(item["CloseDate"]).ToString("dd-MMM-yyyy");
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(item["ResponseDate"])))
                                {
                                    item["ResponseDate"] = Convert.ToDateTime(item["ResponseDate"]).ToString("dd-MMM-yyyy");
                                }
                            }

                            exWorkSheet.Cells["A1"].Value = "SrNo";
                            exWorkSheet.Cells["A1"].AutoFitColumns(8);
                            exWorkSheet.Cells["A1:A2"].Merge = true;
                            exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["A1"].Style.WrapText = true;

                            exWorkSheet.Cells["B1"].Value = PageName + " No.";
                            exWorkSheet.Cells["B1"].AutoFitColumns(20);
                            exWorkSheet.Cells["B1:B2"].Merge = true;
                            exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["B1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["B1"].Style.WrapText = true;

                            exWorkSheet.Cells["C1"].Value = PageName + " Title";
                            exWorkSheet.Cells["C1"].AutoFitColumns(20);
                            exWorkSheet.Cells["C1:C2"].Merge = true;
                            exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["C1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["C1"].Style.WrapText = true;

                            exWorkSheet.Cells["D1"].Value = "Notice Term";
                            exWorkSheet.Cells["D1"].AutoFitColumns(20);
                            exWorkSheet.Cells["D1:D2"].Merge = true;
                            exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["D1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["D1"].Style.WrapText = true;

                            exWorkSheet.Cells["E1"].Value = " Location";
                            exWorkSheet.Cells["E1"].AutoFitColumns(20);
                            exWorkSheet.Cells["E1:E2"].Merge = true;
                            exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["E1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["E1"].Style.WrapText = true;

                            exWorkSheet.Cells["F1"].Value = "Jurisdiction";
                            exWorkSheet.Cells["F1"].AutoFitColumns(15);
                            exWorkSheet.Cells["F1:F2"].Merge = true;
                            exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["F1:F2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["F1:F2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["F1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["F1"].Style.WrapText = true;

                            exWorkSheet.Cells["G1"].Value = PageName + " Description";
                            exWorkSheet.Cells["G1"].AutoFitColumns(15);
                            exWorkSheet.Cells["G1:G2"].Merge = true;
                            exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["G1:G2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["G1:G2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["G1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["G1"].Style.WrapText = true;

                            exWorkSheet.Cells["H1"].Value = "Open Date";
                            exWorkSheet.Cells["H1"].AutoFitColumns(15);
                            exWorkSheet.Cells["H1:H2"].Merge = true;
                            exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["H1:H2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["H1:H2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["H1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["H1"].Style.WrapText = true;

                            exWorkSheet.Cells["I1"].Value = "Close Date";
                            exWorkSheet.Cells["I1"].AutoFitColumns(15);
                            exWorkSheet.Cells["I1:I2"].Merge = true;
                            exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["I1:I2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["I1:I2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["I1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["I1"].Style.WrapText = true;

                            exWorkSheet.Cells["J1"].Value = "Status";
                            exWorkSheet.Cells["J1"].AutoFitColumns(15);
                            exWorkSheet.Cells["J1:J2"].Merge = true;
                            exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["J1:J2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["J1:J2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["J1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["J1"].Style.WrapText = true;

                            exWorkSheet.Cells["K1"].Value = "Additional Tracking Parameter(s)";
                            exWorkSheet.Cells["K1:L1"].Merge = true;
                            exWorkSheet.Cells["K1"].AutoFitColumns(100);
                            exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K1:L1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["K1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["K1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["K1"].Style.WrapText = true;

                            exWorkSheet.Cells["K2"].Value = "Name";
                            exWorkSheet.Cells["K2"].AutoFitColumns(15);
                            exWorkSheet.Cells["K2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["K2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["K2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["K2"].Style.WrapText = true;

                            exWorkSheet.Cells["L2"].Value = "Value";
                            exWorkSheet.Cells["L2"].AutoFitColumns(35);
                            exWorkSheet.Cells["L2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["L2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["L2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["L2"].Style.WrapText = true;

                            exWorkSheet.Cells["M1"].Value = NameNoticeCase;
                            exWorkSheet.Cells["M1:P1"].Merge = true;
                            exWorkSheet.Cells["M1"].AutoFitColumns(100);
                            exWorkSheet.Cells["M1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["M1:P1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["M1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["M1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["M1"].Style.WrapText = true;

                            exWorkSheet.Cells["M2"].Value = "Response Date";
                            exWorkSheet.Cells["M2"].AutoFitColumns(25);
                            exWorkSheet.Cells["M2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["M2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["M2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["M2"].Style.WrapText = true;

                            exWorkSheet.Cells["N2"].Value = "Description";
                            exWorkSheet.Cells["N2"].AutoFitColumns(25);
                            exWorkSheet.Cells["N2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["N2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["N2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["N2"].Style.WrapText = true;

                            exWorkSheet.Cells["O2"].Value = "Remark";
                            exWorkSheet.Cells["O2"].AutoFitColumns(25);
                            exWorkSheet.Cells["O2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["O2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["O2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["O2"].Style.WrapText = true;

                            exWorkSheet.Cells["P2"].Value = "Created By";
                            exWorkSheet.Cells["P2"].AutoFitColumns(25);
                            exWorkSheet.Cells["P2"].Style.Font.Bold = true;
                            exWorkSheet.Cells["P2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["P2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["P2"].Style.WrapText = true;

                           
                            exWorkSheet.Cells["Q1"].Value = " Financial Year";
                            exWorkSheet.Cells["Q1"].AutoFitColumns(15);
                            exWorkSheet.Cells["Q1:Q2"].Merge = true;
                            exWorkSheet.Cells["Q1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["Q1:Q2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["Q1:Q2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet.Cells["Q1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["Q1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["Q1"].Style.WrapText = true;

                            exWorkSheet.Cells["R1"].Value = "Provisional amount";
                            exWorkSheet.Cells["R1"].AutoFitColumns(15);
                            exWorkSheet.Cells["R1:R2"].Merge = true;
                            exWorkSheet.Cells["R1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["R1:R2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["R1:R2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet.Cells["R1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["R1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["R1"].Style.WrapText = true;

                            exWorkSheet.Cells["S1"].Value = "Bank Gurantee";
                            exWorkSheet.Cells["S1"].AutoFitColumns(15);
                            exWorkSheet.Cells["S1:S2"].Merge = true;
                            exWorkSheet.Cells["S1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["S1:S2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["S1:S2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet.Cells["S1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["S1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["S1"].Style.WrapText = true;

                            exWorkSheet.Cells["T1"].Value = "Protest Money";
                            exWorkSheet.Cells["T1"].AutoFitColumns(15);
                            exWorkSheet.Cells["T1:T2"].Merge = true;
                            exWorkSheet.Cells["T1"].Style.Font.Bold = true;
                            exWorkSheet.Cells["T1:T2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet.Cells["T1:T2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet.Cells["T1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet.Cells["T1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet.Cells["T1"].Style.WrapText = true;

                            #region Start Dynamic Column of Notice


                            if (ExcelData.Rows.Count > 0)
                            {
                                exWorkSheet.Cells["A3"].LoadFromDataTable(ExcelData, false);
                            }

                            #endregion

                            string ActulValue = string.Empty;
                            string NewValue = string.Empty;
                            int j = 3;
                            int k = 3; ;
                            for (int i = 3; i <= 2 + ExcelData.Rows.Count; i++)
                            {

                                string cOpenDate = "c" + i;
                                exWorkSheet.Cells[cOpenDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                string CloseDate = "D" + i;
                                exWorkSheet.Cells[CloseDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                string CStatus = "E" + i;
                                exWorkSheet.Cells[CStatus].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                string cResponceDate = "F" + i;
                                exWorkSheet.Cells[cResponceDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                                string chke = "B" + i;
                                string Checkcell = exWorkSheet.Cells[chke].Value.ToString();
                                if (i > 3)
                                {
                                    string cOpenDate1 = "c" + i;
                                    exWorkSheet.Cells[cOpenDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                    string CloseDate1 = "D" + i;
                                    exWorkSheet.Cells[CloseDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                    string CStatus1 = "E" + i;
                                    exWorkSheet.Cells[CStatus1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                    string cResponceDate1 = "F" + i;
                                    exWorkSheet.Cells[cResponceDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                                    if (string.IsNullOrEmpty(Checkcell))
                                    {
                                        j++;
                                    }
                                    else
                                    {
                                        string checknow4 = "A" + k + ":A" + j;
                                        string chekcnow = "B" + k + ":B" + j;
                                        string chekcnow1 = "C" + k + ":C" + j;
                                        string chekcnow2 = "D" + k + ":D" + j;
                                        string chekcnow3 = "E" + k + ":E" + j;
                                        string chekcnow4 = "F" + k + ":F" + j;
                                        string chekcnow5 = "G" + k + ":G" + j;
                                        string chekcnow6 = "H" + k + ":H" + j;
                                        exWorkSheet.Cells[checknow4].Merge = true;
                                        exWorkSheet.Cells[chekcnow].Merge = true;
                                        exWorkSheet.Cells[chekcnow1].Merge = true;
                                        exWorkSheet.Cells[chekcnow2].Merge = true;
                                        exWorkSheet.Cells[chekcnow3].Merge = true;
                                        exWorkSheet.Cells[chekcnow4].Merge = true;
                                        exWorkSheet.Cells[chekcnow5].Merge = true;
                                        exWorkSheet.Cells[chekcnow6].Merge = true;
                                        k = i;
                                        j = i;
                                    }
                                    if (j == (2 + ExcelData.Rows.Count))
                                    {
                                        string checknow4 = "A" + k + ":A" + j;
                                        string chekcnow = "B" + k + ":B" + j;
                                        string chekcnow1 = "C" + k + ":C" + j;
                                        string chekcnow2 = "D" + k + ":D" + j;
                                        string chekcnow3 = "E" + k + ":E" + j;
                                        string chekcnow4 = "F" + k + ":F" + j;
                                        string chekcnow5 = "G" + k + ":G" + j;
                                        string chekcnow6 = "H" + k + ":H" + j;
                                        exWorkSheet.Cells[checknow4].Merge = true;
                                        exWorkSheet.Cells[chekcnow].Merge = true;
                                        exWorkSheet.Cells[chekcnow1].Merge = true;
                                        exWorkSheet.Cells[chekcnow2].Merge = true;
                                        exWorkSheet.Cells[chekcnow3].Merge = true;
                                        exWorkSheet.Cells[chekcnow4].Merge = true;
                                        exWorkSheet.Cells[chekcnow5].Merge = true;
                                        exWorkSheet.Cells[chekcnow6].Merge = true;
                                    }
                                }
                            }

                            using (ExcelRange col = exWorkSheet.Cells[1, 1, 2 + ExcelData.Rows.Count, 20])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }
                        }
                        #endregion
                    }
                    #endregion

                    #region With Litigation Tax
                    {
                        List<HearingDetailReport> HearingDataobj = new List<HearingDetailReport>();
                        List<ResponseDetailReport> ResponseDetailsObj = new List<ResponseDetailReport>();
                        table1.Columns.Add("SrNo", typeof(string));
                        table1.Columns.Add("NoticeCaseInstanceID", typeof(string));
                        table1.Columns.Add("Notice/Case No", typeof(string));
                        table1.Columns.Add("Title", typeof(string));
                        table1.Columns.Add("NoticeTerm", typeof(string));
                        table1.Columns.Add("Location", typeof(string));
                        table1.Columns.Add("Jurisdiction", typeof(string));
                        table1.Columns.Add("Notice/Case Description", typeof(string));
                        table1.Columns.Add("OpenDate", typeof(string));
                        table1.Columns.Add("CloseDate", typeof(string));
                        table1.Columns.Add("Status", typeof(string));
                        table1.Columns.Add("Label", typeof(string));
                        table1.Columns.Add("LabelValue", typeof(string));
                        table1.Columns.Add("Interest", typeof(string));
                        table1.Columns.Add("Penalty", typeof(string));
                        table1.Columns.Add("ProvisionInBook", typeof(string));
                        table1.Columns.Add("Total", typeof(string));
                        table1.Columns.Add("SettlementValue", typeof(string));
                        table1.Columns.Add("IsAllowed", typeof(string));
                        table1.Columns.Add("ResponseDate", typeof(string));
                        table1.Columns.Add("Description", typeof(string));
                        table1.Columns.Add("Remark", typeof(string));
                        table1.Columns.Add("CreatedByText", typeof(string));
                        table1.Columns.Add("FYName", typeof(string));
                        table1.Columns.Add("Provisionalamt", typeof(string));
                        table1.Columns.Add("BankGurantee", typeof(string));
                        table1.Columns.Add("ProtestMoney", typeof(string));

                        #region Notice
                        using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                        {
                            Noticetable = (from row in entities.sp_LitigationNoticeReportWithCustomParameter(Convert.ToInt32(AuthenticationHelper.CustomerID))
                                           select row).ToList();

                            if (AuthenticationHelper.Role != "MGMT" && AuthenticationHelper.Role != "CADMN")
                                Noticetable = Noticetable.Where(entry => (entry.AssingedUserID == AuthenticationHelper.UserID) || (entry.OwnerID == AuthenticationHelper.UserID) || (entry.NoticeCreatedBy == AuthenticationHelper.UserID)).ToList();
                            else // In case of MGMT or CADMN 
                            {
                                Noticetable = Noticetable.Where(entry => entry.RoleID == 3).ToList();
                            }

                            Noticetable = Noticetable.Where(entry => entry.NoticeCategoryID == 47).ToList();

                            if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                            {
                                caseStatus = ddlStatus.SelectedValue;
                                if (caseStatus == "1")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.Status == "Open" || entry.Status == "In Progress").ToList();
                                }
                                if (caseStatus == "3")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.Status == "Closed").ToList();
                                }
                                if (caseStatus == "4")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.Status == "Settled").ToList();
                                }
                            }

                            if (!string.IsNullOrEmpty(ddlNoticeTypePage.SelectedValue))
                            {
                                if (ddlNoticeTypePage.SelectedValue == "I")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.NoticeType == "I").ToList();
                                }
                                if (ddlNoticeTypePage.SelectedValue == "O")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.NoticeType == "O").ToList();
                                }
                            }

                            if (!string.IsNullOrEmpty(ddlPartyPage.SelectedValue))
                            {
                                if (ddlPartyPage.SelectedValue != "-1")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.PartyID.Contains(ddlPartyPage.SelectedValue)).ToList();
                                }
                            }

                            if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                            {
                                if (ddlDeptPage.SelectedValue != "-1")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.DepartmentID == Convert.ToInt32(ddlDeptPage.SelectedValue)).ToList();
                                }
                            }

                            if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                            {
                                //Noticetable = Noticetable.Where(entry => entry.CustomerBranchID == Convert.ToInt32(tvFilterLocation.SelectedValue)).ToList();
                                int branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(AuthenticationHelper.CustomerID), branchID);
                                if (branchList.Count > 0)
                                    Noticetable = Noticetable.Where(Entry => branchList.Contains(Entry.CustomerBranchID)).ToList();
                            }
                            if (!string.IsNullOrEmpty(ddlType.SelectedValue))
                            {
                                if (ddlType.SelectedValue != "-1")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.NoticeCategoryID == Convert.ToInt32(ddlType.SelectedValue)).ToList();
                                }
                            }
                            if (!string.IsNullOrEmpty(ddlOwnerlist.SelectedValue))
                            {
                                if (ddlOwnerlist.SelectedValue != "-1")
                                {
                                    Noticetable = Noticetable.Where(entry => entry.OwnerID == Convert.ToInt32(ddlOwnerlist.SelectedValue)).ToList();
                                }
                            }
                            if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                            {
                                Noticetable = Noticetable.Where(entry => entry.Title.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) || entry.RefNo.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();
                            }
                            long NoticeIDOld = 0;
                            foreach (var item in Noticetable)
                            {
                                long NoticeID = item.NoticeCaseInstanceID;
                                if (NoticeID != NoticeIDOld)
                                {
                                    NoticeIDOld = item.NoticeCaseInstanceID;
                                    List<ResponseDetailReport> HearingData = CaseManagement.GetResponseDetailNoticeWise(item.NoticeCaseInstanceID);

                                    foreach (var itemValues in HearingData)
                                    {
                                        ResponseDetailsObj.Add(itemValues);
                                        tableCaseCustomField.Rows.Add(itemValues.CaseInstanceID, itemValues.ResponseDate, itemValues.Description, itemValues.Remark, itemValues.CreatedByText);
                                    }
                                }
                            }

                            int beforecondition = 0;
                            for (int i = 0; i < Noticetable.Count; i++)
                            {
                                if (ResponseDetailsObj.Count > 0)
                                {
                                    beforecondition = Convert.ToInt32(Noticetable[i].NoticeCaseInstanceID);//1
                                    for (int j = 0; j <= i; j++)
                                    {
                                        if (ResponseDetailsObj.Count > j)
                                        {
                                            if (ResponseDetailsObj.Count == 1)
                                            {
                                                if (ResponseDetailsObj[0].CaseInstanceID == Noticetable[i].NoticeCaseInstanceID)
                                                {
                                                    Noticetable[i].Remark = ResponseDetailsObj[j].Remark;
                                                    if (!string.IsNullOrEmpty(Convert.ToString(ResponseDetailsObj[j].ResponseDate)))
                                                    {
                                                        Noticetable[i].ResponseDate = Convert.ToDateTime(ResponseDetailsObj[j].ResponseDate).ToString("dd-MMM-yyyy");
                                                    }
                                                    Noticetable[i].Description = ResponseDetailsObj[j].Description;
                                                    Noticetable[i].CreatedBy = ResponseDetailsObj[j].CreatedByText;
                                                    ResponseDetailsObj.RemoveAt(j);
                                                }
                                                break;
                                            }
                                            else
                                            {
                                                if (ResponseDetailsObj[j].CaseInstanceID == Noticetable[i].NoticeCaseInstanceID)
                                                {
                                                    Noticetable[i].Remark = ResponseDetailsObj[j].Remark;
                                                    if (!string.IsNullOrEmpty(Convert.ToString(ResponseDetailsObj[j].ResponseDate)))
                                                    {
                                                        Noticetable[i].ResponseDate = Convert.ToDateTime(ResponseDetailsObj[j].ResponseDate).ToString("dd-MMM-yyyy");
                                                    }
                                                    Noticetable[i].Description = ResponseDetailsObj[j].Description;
                                                    Noticetable[i].CreatedBy = ResponseDetailsObj[j].CreatedByText;
                                                    ResponseDetailsObj.RemoveAt(j);
                                                    j = i;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (ResponseDetailsObj.Count > 0)
                            {
                                for (int k = 0; k < ResponseDetailsObj.Count; k++)
                                {
                                    long IDRemCheck = Convert.ToInt32(ResponseDetailsObj[k].CaseInstanceID);
                                    string RespDates = string.Empty;
                                    if (!string.IsNullOrEmpty(Convert.ToString(ResponseDetailsObj[k].ResponseDate)))
                                    {
                                        RespDates = Convert.ToDateTime(ResponseDetailsObj[k].ResponseDate).ToString("dd-MMM-yyyy");
                                    }
                                    Noticetable.Add(new sp_LitigationNoticeReportWithCustomParameter_Result { NoticeCaseInstanceID = IDRemCheck, Remark = ResponseDetailsObj[k].Remark, ResponseDate = RespDates, Description = ResponseDetailsObj[k].Description, CreatedBy = ResponseDetailsObj[k].CreatedByText });
                                }
                            }


                            Noticetable = Noticetable.OrderBy(entry => entry.NoticeCaseInstanceID).ToList();
                            long NoticeIDOld2 = 0;
                            foreach (var item in Noticetable)
                            {
                                long NoticeID = item.NoticeCaseInstanceID;
                                if (NoticeID != NoticeIDOld2)
                                {
                                    ++P;
                                    CaseInstanceIDDynamic.Add(Convert.ToInt32(item.NoticeCaseInstanceID));
                                    NoticeIDOld2 = item.NoticeCaseInstanceID;
                                    table1.Rows.Add(P, item.NoticeCaseInstanceID, item.RefNo, item.Title, item.NoticeTerm,item.BranchName,item.Jurisdiction, item.NoticeCaseDesc, item.OpenDate, item.CloseDate, item.Status, item.Label, item.LabelValue, item.Interest, item.Penalty, item.ProvisionInBook, item.Total, item.SettlementValue, item.IsAllowed, item.ResponseDate, item.Description, item.Remark, item.CreatedBy,item.FYName, item.Provisionalamt, item.BankGurantee, item.ProtestMoney);
                                }
                                else
                                {
                                    CaseInstanceIDDynamic.Add(0);
                                    table1.Rows.Add("", "", "", "", "", "", "", "", "","","", item.Label, item.LabelValue, item.Interest, item.Penalty, item.ProvisionInBook, item.Total, item.SettlementValue, item.IsAllowed, item.ResponseDate, item.Description, item.Remark, item.CreatedBy);
                                }
                            }

                            PageName = "Notice Tax Litigation";
                            DeskTitile = "Response";
                            RespDate = "Response Date";
                            NameNoticeCase = "Response Details";
                        }
                        #endregion


                        ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add(PageName + " Report");


                        #region Second Sheet
                        if (PageName == "Notice Tax Litigation" || PageName == "Case Tax Litigation")
                        {
                            view1 = new System.Data.DataView(table1);

                            DataTable ExcelData = null;

                            ExcelData = view1.ToTable("Selected", false, "SrNo", "Notice/Case No", "Title", "NoticeTerm", "Location", "Jurisdiction", "Notice/Case Description", "OpenDate", "CloseDate", "Status", "Label", "LabelValue", "Interest", "Penalty", "ProvisionInBook", "Total", "SettlementValue", "IsAllowed", "ResponseDate", "Description", "Remark", "CreatedByText", "FYName", "Provisionalamt", "BankGurantee", "ProtestMoney");

                            foreach (DataRow item in ExcelData.Rows)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(item["OpenDate"])))
                                {
                                    item["OpenDate"] = Convert.ToDateTime(item["OpenDate"]).ToString("dd-MMM-yyyy");
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(item["CloseDate"])))
                                {
                                    item["CloseDate"] = Convert.ToDateTime(item["CloseDate"]).ToString("dd-MMM-yyyy");
                                }
                                if (!string.IsNullOrEmpty(Convert.ToString(item["ResponseDate"])))
                                {
                                    item["ResponseDate"] = Convert.ToDateTime(item["ResponseDate"]).ToString("dd-MMM-yyyy");
                                }
                            }

                            exWorkSheet1.Cells["A1"].Value = "SrNo";
                            exWorkSheet1.Cells["A1"].AutoFitColumns(8);
                            exWorkSheet1.Cells["A1:A2"].Merge = true;
                            exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["A1"].Style.WrapText = true;

                            exWorkSheet1.Cells["B1"].Value = "Notice/Case No";
                            exWorkSheet1.Cells["B1"].AutoFitColumns(20);
                            exWorkSheet1.Cells["B1:B2"].Merge = true;
                            exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["B1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["B1"].Style.WrapText = true;

                            exWorkSheet1.Cells["C1"].Value = PageName + " Title";
                            exWorkSheet1.Cells["C1"].AutoFitColumns(20);
                            exWorkSheet1.Cells["C1:C2"].Merge = true;
                            exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["C1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["C1"].Style.WrapText = true;

                            exWorkSheet1.Cells["D1"].Value = "NoticeTerm" ;
                            exWorkSheet1.Cells["D1"].AutoFitColumns(20);
                            exWorkSheet1.Cells["D1:D2"].Merge = true;
                            exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["D1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["D1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["D1"].Style.WrapText = true;

                            exWorkSheet1.Cells["E1"].Value = "Location";
                            exWorkSheet1.Cells["E1"].AutoFitColumns(20);
                            exWorkSheet1.Cells["E1:E2"].Merge = true;
                            exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["E1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["E1"].Style.WrapText = true;

                            exWorkSheet1.Cells["F1"].Value = "Jurisdiction";
                            exWorkSheet1.Cells["F1"].AutoFitColumns(15);
                            exWorkSheet1.Cells["F1:F2"].Merge = true;
                            exWorkSheet1.Cells["F1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["F1:F2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["F1:F2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["F1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["F1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["F1"].Style.WrapText = true;

                            exWorkSheet1.Cells["G1"].Value = "Notice/Case Description";
                            exWorkSheet1.Cells["G1"].AutoFitColumns(15);
                            exWorkSheet1.Cells["G1:G2"].Merge = true;
                            exWorkSheet1.Cells["G1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["G1:G2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["G1:G2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["G1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["G1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["G1"].Style.WrapText = true;

                            exWorkSheet1.Cells["H1"].Value = "Open Date";
                            exWorkSheet1.Cells["H1"].AutoFitColumns(15);
                            exWorkSheet1.Cells["H1:H2"].Merge = true;
                            exWorkSheet1.Cells["H1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["H1:H2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["H1:H2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["H1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["H1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["H1"].Style.WrapText = true;

                            exWorkSheet1.Cells["I1"].Value = "Close Date";
                            exWorkSheet1.Cells["I1"].AutoFitColumns(15);
                            exWorkSheet1.Cells["I1:I2"].Merge = true;
                            exWorkSheet1.Cells["I1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["I1:I2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["I1:I2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["I1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["I1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["I1"].Style.WrapText = true;

                            exWorkSheet1.Cells["J1"].Value = "Status";
                            exWorkSheet1.Cells["J1"].AutoFitColumns(15);
                            exWorkSheet1.Cells["J1:J2"].Merge = true;
                            exWorkSheet1.Cells["J1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["J1:J2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["J1:J2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["J1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["J1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["J1"].Style.WrapText = true;

                            exWorkSheet1.Cells["K1"].Value = "Additional Tracking Parameter(s)";
                            exWorkSheet1.Cells["K1:R1"].Merge = true;
                            exWorkSheet1.Cells["K1"].AutoFitColumns(100);
                            exWorkSheet1.Cells["K1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["K1:R1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["K1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["K1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["K1"].Style.WrapText = true;

                            exWorkSheet1.Cells["K2"].Value = "Ground Of Appeal";
                            exWorkSheet1.Cells["K2"].AutoFitColumns(15);
                            exWorkSheet1.Cells["K2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["K2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["K2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["K2"].Style.WrapText = true;

                            exWorkSheet1.Cells["L2"].Value = "Tax demand";
                            exWorkSheet1.Cells["L2"].AutoFitColumns(35);
                            exWorkSheet1.Cells["L2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["L2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["L2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["L2"].Style.WrapText = true;

                            exWorkSheet1.Cells["M2"].Value = "Interest";
                            exWorkSheet1.Cells["M2"].AutoFitColumns(25);
                            exWorkSheet1.Cells["M2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["M2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["M2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["M2"].Style.WrapText = true;

                            exWorkSheet1.Cells["N2"].Value = "Penalty";
                            exWorkSheet1.Cells["N2"].AutoFitColumns(25);
                            exWorkSheet1.Cells["N2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["N2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["N2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["N2"].Style.WrapText = true;

                            exWorkSheet1.Cells["O2"].Value = "Provision In Book";
                            exWorkSheet1.Cells["O2"].AutoFitColumns(25);
                            exWorkSheet1.Cells["O2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["O2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["O2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["O2"].Style.WrapText = true;

                            exWorkSheet1.Cells["P2"].Value = "Total";
                            exWorkSheet1.Cells["P2"].AutoFitColumns(25);
                            exWorkSheet1.Cells["P2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["P2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["P2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["P2"].Style.WrapText = true;

                            exWorkSheet1.Cells["Q2"].Value = "Settlement Value";
                            exWorkSheet1.Cells["Q2"].AutoFitColumns(25);
                            exWorkSheet1.Cells["Q2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Q2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["Q2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["Q2"].Style.WrapText = true;

                            exWorkSheet1.Cells["R2"].Value = "Result";
                            exWorkSheet1.Cells["R2"].AutoFitColumns(25);
                            exWorkSheet1.Cells["R2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["R2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["R2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["R2"].Style.WrapText = true;

                            exWorkSheet1.Cells["S1"].Value = NameNoticeCase;
                            exWorkSheet1.Cells["S1:V1"].Merge = true;
                            exWorkSheet1.Cells["S1"].AutoFitColumns(100);
                            exWorkSheet1.Cells["S1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["S1:V1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["S1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["S1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["S1"].Style.WrapText = true;

                            exWorkSheet1.Cells["S2"].Value = "Response Date";
                            exWorkSheet1.Cells["S2"].AutoFitColumns(25);
                            exWorkSheet1.Cells["S2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["S2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["S2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["S2"].Style.WrapText = true;
                        
                            exWorkSheet1.Cells["T2"].Value = "Description";
                            exWorkSheet1.Cells["T2"].AutoFitColumns(25);
                            exWorkSheet1.Cells["T2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["T2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["T2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["T2"].Style.WrapText = true;

                            exWorkSheet1.Cells["U2"].Value = "Remark";
                            exWorkSheet1.Cells["U2"].AutoFitColumns(25);
                            exWorkSheet1.Cells["U2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["U2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["U2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["U2"].Style.WrapText = true;
                        
                            exWorkSheet1.Cells["V2"].Value = "Created By";
                            exWorkSheet1.Cells["V2"].AutoFitColumns(25);
                            exWorkSheet1.Cells["V2"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["V2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["V2"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["V2"].Style.WrapText = true;

                            
                            exWorkSheet1.Cells["W1"].Value = "Financial Year";
                            exWorkSheet1.Cells["W1"].AutoFitColumns(15);
                            exWorkSheet1.Cells["W1:W2"].Merge = true;
                            exWorkSheet1.Cells["W1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["W1:W2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            exWorkSheet1.Cells["W1:W2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            exWorkSheet1.Cells["W1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["W1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["W1"].Style.WrapText = true;

                            exWorkSheet1.Cells["X1"].Value = "Provisional amount";
                            exWorkSheet1.Cells["X1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["X1:X2"].Merge = true;
                            exWorkSheet1.Cells["X1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["X1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["X1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["X1"].Style.WrapText = true;

                            exWorkSheet1.Cells["Y1"].Value = "Bank Gurantee";
                            exWorkSheet1.Cells["Y1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["Y1:Y2"].Merge = true;
                            exWorkSheet1.Cells["Y1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Y1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["Y1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["Y1"].Style.WrapText = true;

                            exWorkSheet1.Cells["Z1"].Value = "Protest Money";
                            exWorkSheet1.Cells["Z1"].AutoFitColumns(25);
                            exWorkSheet1.Cells["Z1:Z2"].Merge = true;
                            exWorkSheet1.Cells["Z1"].Style.Font.Bold = true;
                            exWorkSheet1.Cells["Z1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            exWorkSheet1.Cells["Z1"].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            exWorkSheet1.Cells["Z1"].Style.WrapText = true;

                            if (ExcelData.Rows.Count > 0)
                            {
                                exWorkSheet1.Cells["A3"].LoadFromDataTable(ExcelData, false);
                            }


                            string ActulValue = string.Empty;
                            string NewValue = string.Empty;
                            int j = 3;
                            int k = 3; ;
                            for (int i = 3; i <= 2 + ExcelData.Rows.Count; i++)
                            {

                                string cOpenDate = "c" + i;
                                exWorkSheet1.Cells[cOpenDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                string CloseDate = "D" + i;
                                exWorkSheet1.Cells[CloseDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                string CStatus = "E" + i;
                                exWorkSheet1.Cells[CStatus].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                string cResponceDate = "F" + i;
                                exWorkSheet1.Cells[cResponceDate].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                                string chke = "B" + i;
                                string Checkcell = exWorkSheet1.Cells[chke].Value.ToString();
                                if (i > 3)
                                {
                                    string cOpenDate1 = "c" + i;
                                    exWorkSheet1.Cells[cOpenDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                    string CloseDate1 = "D" + i;
                                    exWorkSheet1.Cells[CloseDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                    string CStatus1 = "E" + i;
                                    exWorkSheet1.Cells[CStatus1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                    string cResponceDate1 = "F" + i;
                                    exWorkSheet1.Cells[cResponceDate1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                                    if (string.IsNullOrEmpty(Checkcell))
                                    {
                                        j++;
                                    }
                                    else
                                    {
                                        string checknow4 = "A" + k + ":A" + j;
                                        string chekcnow = "B" + k + ":B" + j;
                                        string chekcnow1 = "C" + k + ":C" + j;
                                        string chekcnow2 = "D" + k + ":D" + j;
                                        string chekcnow3 = "E" + k + ":E" + j;
                                        string chekcnow4 = "F" + k + ":F" + j;
                                        string chekcnow5 = "G" + k + ":G" + j;
                                        string chekcnow6 = "H" + k + ":H" + j;
                                        exWorkSheet1.Cells[checknow4].Merge = true;
                                        exWorkSheet1.Cells[chekcnow].Merge = true;
                                        exWorkSheet1.Cells[chekcnow1].Merge = true;
                                        exWorkSheet1.Cells[chekcnow2].Merge = true;
                                        exWorkSheet1.Cells[chekcnow3].Merge = true;
                                        exWorkSheet1.Cells[chekcnow4].Merge = true;
                                        exWorkSheet1.Cells[chekcnow5].Merge = true;
                                        exWorkSheet1.Cells[chekcnow6].Merge = true;
                                        k = i;
                                        j = i;
                                    }
                                    if (j == (2 + ExcelData.Rows.Count))
                                    {
                                        string checknow4 = "A" + k + ":A" + j;
                                        string chekcnow = "B" + k + ":B" + j;
                                        string chekcnow1 = "C" + k + ":C" + j;
                                        string chekcnow2 = "D" + k + ":D" + j;
                                        string chekcnow3 = "E" + k + ":E" + j;
                                        string chekcnow4 = "F" + k + ":F" + j;
                                        string chekcnow5 = "G" + k + ":G" + j;
                                        string chekcnow6 = "H" + k + ":H" + j;
                                        exWorkSheet1.Cells[checknow4].Merge = true;
                                        exWorkSheet1.Cells[chekcnow].Merge = true;
                                        exWorkSheet1.Cells[chekcnow1].Merge = true;
                                        exWorkSheet1.Cells[chekcnow2].Merge = true;
                                        exWorkSheet1.Cells[chekcnow3].Merge = true;
                                        exWorkSheet1.Cells[chekcnow4].Merge = true;
                                        exWorkSheet1.Cells[chekcnow5].Merge = true;
                                        exWorkSheet1.Cells[chekcnow6].Merge = true;
                                    }
                                }
                            }

                            using (ExcelRange col = exWorkSheet1.Cells[1, 1, 2 + ExcelData.Rows.Count, 26])
                            {
                                col.Style.WrapText = true;
                                col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            }
                        }
                        #endregion
                    }
                    #endregion

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=LitigationReport.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
            #endregion
        }

        protected void grdNoticeDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lnkDeleteNotice = (LinkButton)e.Row.FindControl("lnkDeleteNotice");

                    if (AuthenticationHelper.Role.Equals("CADMN"))
                    {
                        lnkDeleteNotice.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}