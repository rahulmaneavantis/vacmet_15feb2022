﻿<%@ Page Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="RPACaseStatus.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.RPACaseStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

    <style type="text/css">
        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 1px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            /*background: white;
            border: none;*/
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            background: #E9EAEA;
            font-weight: bold;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            vertical-align: middle;
            white-space: pre-line;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
            color: #666;
        }


        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .moving {
            -webkit-animation: spin 2s infinite linear;
            animation: spin 2s infinite linear;
        }

        .k-widget > span.k-invalid,
        input.k-invalid {
            border: 1px solid red !important;
        }

        .k-window-titlebar.k-header {
            background: #00b5ef;
            color: white;
        }

        .k-calendar td.k-state-selected .k-link, .k-calendar td.k-today.k-state-selected.k-state-hover .k-link {
            color: black;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftreportsmenu');
            fhead('My Workspace/Draft');
            BindType();
            Bindgrid();

            $("#txtSearch").on('input', function (e) {

                var grid = $('#grid').data('kendoGrid');
                var columns = grid.columns;

                var filter = { logic: 'or', filters: [] };

                columns.forEach(function (x) {
                    if (x.field == "CaseNo" || x.field == "CaseYear") {
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: e.target.value
                        });
                    }
                });
                grid.dataSource.filter(filter);
                grid.dataSource.filter(filter);
                if ($("#txtSearch").val() == "") {
                    $('.k-grid-add').show();
                }
                else {
                    $('.k-grid-add').hide();
                }
            });

        });


        function BindType() {
            $("#dropdownType").kendoDropDownList({
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    if ($("#dropdownType").val() == "1" || $("#dropdownType").val() == "2") {
                        window.location.href = "../aspxPages/CaseListNew.aspx";
                    }
                    else if ($("#dropdownType").val() == "3") {
                        window.location.href = "../aspxPages/TaskListNew.aspx";
                    }
                    else {
                        Bindgrid();
                    }
                },
                dataSource: [
                    { text: "Draft", value: "4" },
                    { text: "Notice", value: "1" },
                    { text: "Case", value: "2" },
                    { text: "Task", value: "3" }
                ]
            });

        }

        var dataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    url: '<%=Path%>Litigation/KendoRPAStatusList?CustId=<% =CustId%>',
                },
            },
            pageSize: 10,
            schema: {
                model: {
                    id: "ID",
                    fields: {
                        CreatedOn: { type: "date" }
                    }
                },
            }
        });

        function Bindgrid() {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();


            var grid = $("#grid").kendoGrid({
                dataSource: dataSource,
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                },
                dataBound: OnGridDataBound,
                columns: [
                    {
                        field: "CourtType", title: 'Court Type',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CaseType", title: 'Case Type',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CaseNo", title: 'Case No.',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CaseYear", title: 'Case Year',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "RPAStatus", title: 'Status',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "StateName", title: 'State',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "District", title: 'District',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CaseStage", title: 'Email',
                        hiddem:true,
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CreatedBy", title: 'Created By',
                        hidden: false,
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CreatedOn", title: 'Created On',
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            { name: "add", text: "", className: "ob-add", iconClass: "k-icon k-i-plus" },], title: "Action", lock: true, width: "8%;", headerAttributes: {
                                style: "border-right: solid 1px #ceced2;text-align: center;"
                            }
                    }
                ]

            });

            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: ".k-grid-add",
                content: function (e) {
                    return "Add Case";
                }
            });

            $(document).on("click", "#grid tbody tr .ob-add", function (e) {
                debugger
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                
                var dataString = "";
                if (item.CourtType == "Supreme Court") {
                    dataString = 'case_number=' + item.CaseNo + '&case_type=' + item.CaseTypeID + '&case_year=' + item.CaseYear + '&court_type=Supreme Court';
                }
                if (item.CourtType == "High Court") {
                    dataString = 'case_number=' + item.CaseNo + '&case_type=' + item.CaseTypeID + '&case_year=' + item.CaseYear + '&court_type=High Court&case_state=' + item.State + '&court_id=' + item.BenchID;
                }
                if (item.CourtType == "District Court") {
                    dataString = 'case_number=' + item.CaseNo + '&case_type=' + item.CaseTypeID + '&case_year=' + item.CaseYear + '&court_type=District Court&case_state=' + item.State + '&court_id=' + item.BenchID;
                }

                $.ajax({
                    type: 'GET',
                    url: 'https://cases-api.cap.avantisregtec.in/api/cases/?' + dataString,
                    dataType: "json",
                    beforeSend: function (request) {
                        request.setRequestHeader('Authorization', 'Token 1bf60b3ef56203c71f3cf432bdd1a2229080caaa');
                    },
                    success: function (result) {
                       
                        if (result == null || result == 'undefined' || result.message == "No matching case type found" || result.data.data == '' || result.data.rpa_status == "pending") {

                            var resultNew = "<p1>We are fetching data from the backend it may take few minutes.<br/> We will notify you once we get the update</p1>"
                            $('#confirmbox').kendoDialog({
                                width: "430px",
                                closable: false,
                                modal: true,
                                content: resultNew,
                                actions: [
                                    { text: 'OK' }
                                ]
                            }).data("kendoDialog").open();

                            $("#lblCaseStatusRPA").text("Case Not Found");
                        }
                        else if (result.data.rpa_status == "complete") {
                            var CaseRefNo = "";
                            var OpenDate = "";
                            var Section = "";
                            var CourtID = "";
                            var Act = "";
                            var State = "";
                            var Judge = "";
                            var casetitle = "";
                            var Description = "";
                            var InternalCaseNo = "";
                            var case_typeID = 0;
                            var CaseStage = "";
                            var CaseCloseDate = "";
                            var RPAData = [];

                            var RPAHCData = [];
                            var RPAHCResultData = [];

                            if (item.CourtType == "Supreme Court") {
                                for (var idx = 0; idx < result.data.data.length; idx++) {
                                    if (result.data.data[idx].data_type == "case-details") {

                                        for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {

                                            if (result.data.data[idx].data[idx1]["\ufeffColumn-0"].search("Status/Stage") != -1) {
                                                var CaseStatus = result.data.data[idx].data[idx1]["Column-1"];

                                                if (CaseStatus.search("DISPOSED") != -1) {
                                                    CaseStatus = "3";
                                                    CaseStage = "Disposed";
                                                    var CalcalateDate = result.data.data[idx].data[idx1]["Column-1"].search("Date:");
                                                    if (CalcalateDate != -1) {
                                                        CaseCloseDate = result.data.data[idx].data[idx1]["Column-1"].slice(CalcalateDate + 6);
                                                        CaseCloseDate = CaseCloseDate.slice(0, 10);
                                                    }
                                                }
                                                else {
                                                    CaseStatus = "1";
                                                    CaseStage = "Hearing";
                                                }

                                                RPAHCResultData.push({
                                                    CaseStatus: encodeURIComponent(CaseStatus),
                                                    CaseStage: CaseStage,
                                                    CaseResult: "",
                                                    CaseCloseDate: CaseCloseDate,
                                                });
                                            }

                                            if (result.data.data[idx].data[idx1]["\ufeffColumn-0"] == "Category") {

                                                Description = result.data.data[idx].data[idx1]["Column-1"];
                                            }
                                            if (result.data.data[idx].data[idx1]["\ufeffColumn-0"] == "Act") {
                                                Act = result.data.data[idx].data[idx1]["Column-1"];
                                            }
                                            if (result.data.data[idx].data[idx1]["\ufeffColumn-0"] == "Diary No.") {

                                                var SectionIndex = result.data.data[idx].data[idx1]["Column-1"].search("SECTION");
                                                Section = result.data.data[idx].data[idx1]["Column-1"].slice(SectionIndex - 1);
                                                var CalcalateDate = result.data.data[idx].data[idx1]["Column-1"].search("Filed on");
                                                var CalcalateDate1 = result.data.data[idx].data[idx1]["Column-1"].slice(CalcalateDate + 9);
                                                var SearchSection = CalcalateDate1.search("PENDING");
                                                var SearchSection1 = CalcalateDate1.search("DISPOSED");
                                                if (SearchSection != -1) {
                                                    OpenDate = CalcalateDate1.slice(0, SearchSection - 9);
                                                }
                                                else {
                                                    OpenDate = CalcalateDate1.slice(0, SearchSection1 - 9);
                                                }
                                            }

                                        }
                                    }
                                    if (result.data.data[idx].data_type == "case-name") {

                                        casetitle = result.data.data[idx].data[0]["Column2"];
                                    }
                                }
                                RPAData.push({
                                    'CaseYear': item.CaseYear,
                                    'CaseRefNo': item.CaseNo,
                                    'Section': encodeURIComponent(Section),
                                    'CourtID': '503',
                                    'OpenDate': encodeURIComponent(OpenDate),
                                    'case_type': encodeURIComponent(item.CaseType),
                                    'State': '',
                                    'Judge': '',
                                    'casetitle': encodeURIComponent(casetitle),
                                    'Act': Act,
                                    'Description': encodeURIComponent(Description),
                                    'InternalCaseNo': encodeURIComponent(InternalCaseNo),
                                    'case_typeID': item.CaseTypeID,
                                    'BenchID': item.BenchID
                                });
                            }

                            if (item.CourtType == "High Court") {
                                for (var idx = 0; idx < result.data.data.length; idx++) {

                                    if (result.data.data[idx].data_type == "case-details") {

                                        for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {


                                            if (result.data.data[idx].data[idx1]["Column-2"] == "Filing Date") {
                                                OpenDate = result.data.data[idx].data[idx1]["Column-3"];
                                            }

                                            if (result.data.data[idx].data[idx1]["\ufeffColumn-0"] == "Filing Number") {
                                                InternalCaseNo = result.data.data[idx].data[idx1]["Column-1"];
                                            }
                                        }

                                    }
                                    if (result.data.data[idx].data_type == "acts") {
                                        Section = result.data.data[idx].data[0]["Under Section(s)"];
                                        Act = result.data.data[idx].data[0]["\ufeffUnder Act(s)"];
                                    }

                                    if (result.data.data[idx].data_type == "case-status") {

                                        for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {

                                            if (result.data.data[idx].data[idx1]["\ufeffColumn-0"] == "Coram") {

                                                Judge = result.data.data[idx].data[idx1]["Column-1"];
                                            }

                                            if (result.data.data[idx].data[idx1]["\ufeffColumn-0"].search("Case Status") != -1) {

                                                var CaseStatus = result.data.data[idx].data[idx1]["Column-1"];

                                                if (CaseStatus.search("DISPOSED") != -1) {
                                                    CaseStatus = "3";
                                                    CaseStage = "Disposed";
                                                }
                                                else {
                                                    CaseStatus = "1";
                                                    CaseStage = "Hearing";
                                                }

                                                RPAHCResultData.push({
                                                    CaseStatus: encodeURIComponent(CaseStatus),
                                                    CaseStage: encodeURIComponent(CaseStage),
                                                    CaseResult: "",
                                                    CaseCloseDate: CaseCloseDate,
                                                });
                                            }
                                        }
                                    }

                                    if (result.data.data[idx].data_type == "case-history") {
                                   
                                        for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {
                                            RPAHCData.push({
                                                remarks: encodeURIComponent(result.data.data[idx].data[idx1]["Judge"]),
                                                hearingDate: encodeURIComponent(result.data.data[idx].data[idx1]["Business On Date"]),
                                                nextDateHearing: encodeURIComponent(result.data.data[idx].data[idx1]["Hearing Date"]),
                                                description: encodeURIComponent(result.data.data[idx].data[idx1]["Purpose of hearing"]),
                                            });
                                        }
                                    }
                                }
                                RPAData.push({
                                    'CaseYear': item.CaseYear,
                                    'CaseRefNo': encodeURIComponent(item.CaseNo),
                                    'Section': encodeURIComponent(Section),
                                    'CourtID': '504',
                                    'OpenDate': encodeURIComponent(OpenDate),
                                    'case_type': encodeURIComponent(item.CaseType),
                                    'State': encodeURIComponent(item.StateName),
                                    'Judge': encodeURIComponent(Judge),
                                    'casetitle': '',
                                    'Act': Act,
                                    'Description': encodeURIComponent(Description),
                                    'InternalCaseNo': encodeURIComponent(InternalCaseNo),
                                    'case_typeID': item.CaseTypeID,
                                    'BenchID': item.BenchID
                                });
                            }

                            if (item.CourtType == "Districy Court") {
                                for (var idx = 0; idx < result.data.data.length; idx++) {

                                    if (result.data.data[idx].data_type == "case-details") {

                                        for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {

                                            if (result.data.data[idx].data[idx1]["Column-2"] == "Filing Date") {
                                                OpenDate = result.data.data[idx].data[idx1]["Column-3"];
                                            }

                                            if (result.data.data[idx].data[idx1]["\ufeffColumn-0"] == "Filing Number") {
                                                InternalCaseNo = result.data.data[idx].data[idx1]["Column-1"];
                                            }
                                        }
                                    }
                                    if (result.data.data[idx].data_type == "acts") {
                                        Section = result.data.data[idx].data[0]["Under Section(s)"];
                                        Act = result.data.data[idx].data[0]["\ufeffUnder Act(s)"];
                                    }

                                    if (result.data.data[idx].data_type == "case-status") {
                                        var CaseStatus = 1;
                                        for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {

                                            if (result.data.data[idx].data[idx1]["\ufeffColumn-0"].search("Judge") != -1) {

                                                Judge = result.data.data[idx].data[idx1]["Column-1"];
                                            }

                                            if (result.data.data[idx].data[idx1]["\ufeffColumn-0"].search("Case Stage") != -1) {

                                                CaseStage = result.data.data[idx].data[idx1]["Column-1"];
                                            }

                                            if (result.data.data[idx].data[idx1]["\ufeffColumn-0"].search("Case Status") != -1) {

                                                CaseStatus = result.data.data[idx].data[idx1]["Column-1"];

                                                if (CaseStatus.search("DISPOSED") != -1) {
                                                    CaseStatus = "3";
                                                }
                                                else {
                                                    CaseStatus = "1";
                                                }
                                            }
                                        }
                                        RPAHCResultData.push({
                                            CaseStatus: encodeURIComponent(CaseStatus),
                                            CaseStage: CaseStage,
                                            CaseResult: "",
                                            CaseCloseDate: CaseCloseDate,
                                        });
                                    }

                                    if (result.data.data[idx].data_type == "case-history") {

                                        for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {
                                            RPAHCData.push({
                                                remarks: encodeURIComponent(result.data.data[idx].data[idx1]["﻿Judge"]),
                                                hearingDate: encodeURIComponent(result.data.data[idx].data[idx1]["Business on Date"]),
                                                nextDateHearing: encodeURIComponent(result.data.data[idx].data[idx1]["Hearing Date"]),
                                                description: encodeURIComponent(result.data.data[idx].data[idx1]["Purpose of Hearing"]),
                                            });
                                        }
                                    }
                                }
                                RPAData.push({
                                    'CaseYear': item.CaseYear,
                                    'CaseRefNo': encodeURIComponent(item.CaseNo),
                                    'Section': encodeURIComponent(Section),
                                    'CourtID': '505',
                                    'OpenDate': encodeURIComponent(OpenDate),
                                    'case_type': encodeURIComponent(item.CaseType),
                                    'State': encodeURIComponent(item.StateName),
                                    'Judge': encodeURIComponent(Judge),
                                    'casetitle': '',
                                    'Act': encodeURIComponent(Act),
                                    'Description': encodeURIComponent(Description),
                                    'InternalCaseNo': encodeURIComponent(InternalCaseNo),
                                    'case_typeID': item.CaseTypeID,
                                    'BenchID': item.BenchID
                                });
                            }

                            $('#divShowDialog').modal('show');
                            $('#showdetails').attr('width', '100%');
                            $('#showdetails').attr('height', '550px');
                            $('.modal-dialog').css('width', '100%');
                            if (RPAData != []) {
                                $('#showdetails').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=0&RPAResult=" + JSON.stringify(RPAData) + '&RPAHCData=' + JSON.stringify(RPAHCData) + '&RPAHCResultData=' + JSON.stringify(RPAHCResultData));
                            }
                        }
                    },
                    error: function (result) {
                        console.log(result);
                    }
                });
                return true;
            });
        }

        function OnGridDataBound(e) {
            for (var i = 0; i < this.columns.length; i++) {
                this.autoWidth;
            }
            var grid = $("#grid").data("kendoGrid");
            var gridData = grid.dataSource.view();
            for (var i = 0; i < gridData.length; i++) {

                var currentUid = gridData[i].uid;
                var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");

                if (gridData[i].RPAStatus != "complete") {
                    var addButton = $(currentRow).find(".ob-add");
                    addButton.hide();
                }
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin: 0.5% 0.8% 0.7%; width: 99%;">
        <input id="dropdownType" style="width: 15.5%; margin-right: 10px;" />
        <input id="txtSearch" placeholder="Search..." class="k-textbox" />
    </div>
    <div>
        <label id="lblCaseStatusRPA" style="color: red; font-size: 14px; margin-left: 40%; font-weight: 400;"></label>
    </div>
    <div class="row">
        <div id="grid" style="margin: 4px;"></div>
    </div>
    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 210px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Add/Edit Details</label>
                    <button id="btnAddEditcase1" type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="ClosePopNoticeDetialPage();">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div id="confirmbox"></div>
</asp:Content>