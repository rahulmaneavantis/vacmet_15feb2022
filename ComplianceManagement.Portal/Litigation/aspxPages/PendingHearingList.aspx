﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="PendingHearingList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.PendingHearingList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">


        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            fhead('My Workspace/Pending Hearing');
        });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">

            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                            AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 40%; left: 40%;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="clearfix"></div>
            <asp:UpdatePanel ID="upPendingHearingDetails" runat="server">
                <ContentTemplate>

                    <div class="row Dashboard-white-widget">
                        <div class="col-lg-12 col-md-12" style="margin-bottom: 4px">
                            <div class="row">
                                <asp:ValidationSummary ID="VSPendingHearing" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                    ValidationGroup="PendingHearingValidationGroup" />
                                <asp:CustomValidator ID="CvHearingSaveMsg" runat="server" EnableClientScript="False"
                                    ValidationGroup="PendingHearingValidationGroup" Display="None" class="alert alert-block alert-danger fade in" />
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-md-12 colpadding0">
                                 
                                <div class="col-md-3 colpadding0" style="margin-top: 5px; width: 49%; margin-right: 5px;">
                                    <asp:TextBox runat="server" ID="tbxtypeTofilter" Width="100%" AutoComplete="off" placeholder="Type to Search" CssClass="form-control" />
                                </div>

                                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 18%; float: right">
                                     <asp:LinkButton runat="server" ID="lnkBtnExcelExport" Style="margin-top: 5px; float: left; margin-left: -12px;" OnClick="btnExport_Click" data-toggle="tooltip" ToolTip="Export to Excel" data-placement="bottom">
                                    <img src="../../Images/Excel _icon.png" alt="Export to Excel"/> 
                                    </asp:LinkButton>
                                    <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lnkBtnApplyFilter" Style="float: right; width: 75px" OnClick="lnkBtnApplyFilter_Click" data-toggle="tooltip" ToolTip="Apply" data-placement="bottom" />
                                    <asp:LinkButton Text="Clear" CssClass="btn btn-primary" runat="server" ID="lnkBtnClearFilter"
                                        OnClick="lnkBtnClearFilter_Click" />
                                </div>

                            </div>
                            <div class="clearfix"></div>
                            <div class="row" style="float: left; padding-top: 10px">
                                <div class="col-md-12 colpadding0">

                                    <div class="form-group col-md-2">
                                        <asp:LinkButton ID="btnDownload" OnClick="btnDownload_Click" class="btn btn-primary" runat="server">Download</asp:LinkButton>
                                    </div>
                                    <div class="form-group required col-md-8">
                                        <asp:FileUpload ID="MasterFileUpload" Multiple="Multiple" runat="server" Style="color: black" />
                                    </div>
                                    <div class="form-group required col-md-2">
                                        <asp:LinkButton ID="btnUpload" OnClick="btnUpload_Click" class="btn btn-primary" runat="server">Upload</asp:LinkButton>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <%-- add gridview--%>
                            <div style="margin-bottom: 4px; width: 100%;overflow-x: scroll;">
                                <asp:GridView runat="server" ID="grdResponseLog" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                    PageSize="10" AllowPaging="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="COURTCASENO">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="3%">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%--  

                                    <asp:TemplateField HeaderText="Case" ItemStyle-Width="30%" SortExpression="CaseTitle">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 10%">
                                                <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CaseTitle") %>' ToolTip='<%# Eval("CaseTitle") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>

                                         <asp:TemplateField HeaderText="Location" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 120px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CBName") %>'
                                                        ToolTip='<%# Eval("CBName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%-- <asp:TemplateField HeaderText="State" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 120px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("State") %>'
                                                        ToolTip='<%# Eval("State") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderText="Region" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 120px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Region") %>'
                                                        ToolTip='<%# Eval("Region") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderText="Zone" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 120px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Zone") %>'
                                                        ToolTip='<%# Eval("Zone") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>

                                        <asp:TemplateField HeaderText="Court Case No." ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="12%" HeaderStyle-Width="12%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label ID="lblHearingRefNo" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                        ToolTip='<%# Eval("COURTCASENO") %>' Text='<%# Eval("COURTCASENO") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Case Title" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CaseTitle") %>'
                                                        ToolTip='<%# Eval("CaseTitle") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Case Description" ItemStyle-Width="20%" HeaderStyle-Width="20%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CaseDetailDesc") %>'
                                                        ToolTip='<%# Eval("CaseDetailDesc") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                       

                                        <asp:TemplateField HeaderText="Next Hearing Date" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label ID="lblReminderDate" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                        ToolTip='<%# Eval("HEARINGDATE") != null ? Convert.ToDateTime(Eval("HEARINGDATE")).ToString("dd-MM-yyyy") : "" %>'
                                                        Text='<%# Eval("HEARINGDATE") != null ? Convert.ToDateTime(Eval("HEARINGDATE")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Hearing Description" ItemStyle-Width="15%" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                    <asp:Label ID="lblResDesc" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("HearingDescription") %>'
                                                        ToolTip='<%# Eval("HearingDescription") %>'></asp:Label>
                                                    <%--data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Description") %>'--%>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerSettings Visible="false" />
                                    <PagerTemplate>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Record Found
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>

                        </div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-8 colpadding0">
                                <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                                    <p style="padding-right: 0px !Important;">
                                        <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                        <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-2 colpadding0">
                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right; height: 32px !important"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                    <asp:ListItem Text="5" />
                                    <asp:ListItem Text="10" Selected="True" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-2 colpadding0" style="float: right;">
                                <div style="float: left; width: 60%">
                                    <p class="clsPageNo">Page</p>
                                </div>
                                <div style="float: left; width: 40%">
                                    <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                        OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                                    </asp:DropDownListChosen>
                                </div>
                            </div>
                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                        </div>
                        <%-- <div class="row" style="float: left;">
                            <div class="col-md-12 colpadding0">

                                <div class="form-group col-md-2">
                                    <asp:LinkButton ID="btnDownload" OnClick="btnDownload_Click" class="btn btn-primary" runat="server">Downlaod</asp:LinkButton>
                                </div>
                                <div class="form-group required col-md-8">
                                    <asp:FileUpload ID="MasterFileUpload" Multiple="Multiple" runat="server" Style="color: black" />
                                </div>
                                <div class="form-group required col-md-2">
                                    <asp:LinkButton ID="btnUpload" OnClick="btnUpload_Click" class="btn btn-primary" runat="server">Upload</asp:LinkButton>
                                </div>
                            </div>
                        </div>--%>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnDownload" />
                    <asp:PostBackTrigger ControlID="btnUpload" />
                      <asp:PostBackTrigger ControlID="lnkBtnExcelExport" />
                    
                </Triggers>
            </asp:UpdatePanel>
        </div>

    </div>

</asp:Content>
