﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="myRemindersNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.myRemindersNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />

    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

    <style type="text/css">
         .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -1.7px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 180px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        /*label {
    display: inline-block;
    margin-bottom: 0px;
}*/

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
            box-shadow: inset 0 0 3px 1px rgba(0, 0, 0, 0.5);
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            background-color: white;
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
    </style>

    <script type="text/javascript">

        setTimeout(printSomething, 1000);

        function printSomething() {
            window.scrollTo(0, document.body.scrollHeight);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftremindersmenu');
            fhead('My Reminders');
            $('#dd2').hide();

            BindTypeofUser();
            Bindgrid();

            $(document).on("click", "#grid tbody tr .ob-deleteuser", function (e) {
                var retVal = confirm("Do you want to Delete Record..?");
                if (retVal == true) {
                    var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                    var $tr = $(this).closest("tr");
                    debugger;
                    $.ajax({
                        type: 'POST',
                        url: "<% =Path%>Litigation/Delete_Litigation_ReminderByID?reminderID=" + item.ReminderID,
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                        },
                        success: function (result) {
                            // notify the data source that the request succeeded
                            grid = $("#grid").data("kendoGrid");
                            grid.removeRow($tr);
                        },
                        error: function (result) {
                            // notify the data source that the request failed
                            console.log(result);
                        }
                    });
                }
                return true;
            });
        });

        var record = 0;
        function Bindgrid() {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();


            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/kendomyReminder?customerID=<% =Custid%>&loggedInUserID=<% =UserID %>&loggedInUserRole=<% =Role%>&reminderType=' + $("#dropdownlist1").val() + '&instanceID=-1',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Litigation/kendomyReminder?customerID=<% =Custid%>&loggedInUserID=<% =UserID %>&loggedInUserRole=<% =Role%>&reminderType=' + $("#dropdownlist1").val() +'&instanceID=-1'
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                columns: [{
                    title: "Sr. No.",
                    template: "#= ++record #",
                    width: 50
                },
                {
                    field: "Type", title: 'Type', template: "#if(Type == 'C') {#<div>Case</div>#}if(Type == 'T') {#<div>Task</div>#} if(Type == 'N') {#<div>Notice</div>#}#",
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains",
                            }
                        }
                    }, width: "10%",
                },
                {
                    field: "ReminderTitle", title: 'Reminder',
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "10%",
                },
                {
                    field: "Description", title: 'Description',
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "15%",
                },
                {
                    field: "Remark", title: 'Remark',
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "10%",
                },
                {
                    field: "RemindOn", title: 'Remind On',
                    type: "date",
                    format: "{0:dd-MMM-yyyy}",
                    template: "#= kendo.toString(kendo.parseDate(RemindOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                    attributes: {
                        style: 'white-space: nowrap;'
                    },
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                type: "date",
                                format: "{0:dd-MMM-yyyy}",
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        },
                    }, width: "10%",
                },
                {
                    field: "ReminderStatus", title: 'Status', template: "#if(ReminderStatus==0){#<div>pending</div>#}#",
                    attributes: {
                        style: 'white-space: nowrap;'

                    }, filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    }, width: "10%;",
                },

                {
                    command: [
                        { name: "edit", text: "", iconClass: "k-icon k-i-edit", className: "ob-edit" },
                        { name: "edit1", text: "", iconClass: "k-icon k-i-delete k-i-trash", className: "ob-deleteuser" }], title: "Action", lock: true, width: 100// width: 150,
                }
                ]
            });
            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Edit";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Delete";
                }
            });
            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                $('#divShowReminderDialog').modal('show');
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                $('#ContentPlaceHolder1_showReminderDetail').attr('src', "../aspxPages/AddEditReminder.aspx?AccessID=" + item.ReminderID)

            });
        }

        function BindTypeofUser() {

            $("#dropdownlist1").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                index: 0,
                change: function (e) {
                    Bindgrid();
                    showdropdown();
                },
                dataSource: [
                    { text: "All", value: "0" },
                    { text: "Notice", value: "1" },
                    { text: "case", value: "2" },
                    { text: "Task", value: "3" },

                ]
            });
        }

        function showdropdown() {
            if ($("#dropdownlist1").val() == 1 || $("#dropdownlist1").val() == 2 || $("#dropdownlist1").val() == 3) {
                $('#dd2').show();
                $("#dropdownlist2").kendoDropDownList({
                    filter: "startswith",
                    autoClose: false,
                    autoWidth: false,
                    dataTextField: "Name",
                    dataValueField: "ID",
                    optionLabel: "Select",
                    change: function (e) {
                        var values = this.value();
                        if (values != "" && values != null) {
                            var filter = { logic: "or", filters: [] };
                            filter.filters.push({
                                field: "InstanceID", operator: "eq", value: parseInt(values)
                            });
                            var dataSource = $("#grid").data("kendoGrid").dataSource;
                            dataSource.filter(filter);
                        }
                        else {
                            $("#grid").data("kendoGrid").dataSource.filter({});
                        }
                    },
                    dataSource: {
                        severFiltering: true,
                        transport: {
                            read: {
                                url: '<% =Path%>Litigation/kendomyReminderFilterList?customerID=<% =Custid%>&loggedInUserID=<% =UserID %>&loggedInUserRole=<% =Role%>&roleID=3&ddltype=' + $("#dropdownlist1").val(),
                                dataType: "json",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '<% =Authorization%>');
                                },
                            }
                            //read: '<% =Path%>Litigation/kendomyReminderFilterList?customerID=<% =Custid%>&loggedInUserID=<% =UserID %>&loggedInUserRole=<% =Role%>&roleID=3&ddltype=' + $("#dropdownlist1").val()

                        }
                    }
                });
            }
            if ($("#dropdownlist1").val() == 0) {
                $('#dd2').hide();
            }
        }

        function CloseMyReminderPopup() {
            $('#divShowReminderDialog').modal('hide');
            Bindgrid();
        }

        function AddnewTest(e) {
            $('#divShowReminderDialog').modal('show');
            $('#ContentPlaceHolder1_showReminderDetail').attr('src', "../aspxPages/AddEditReminder.aspx?AccessID=0")
            e.preventDefault();
            return true;
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-lg-12 col-md-12" style="margin-top: 13px;">
            <div class="col-lg-3 col-md-3" style="margin-left: -19px;">
                <input id="dropdownlist1" style="width: 100%;" />
            </div>
            <div id="dd2" class="col-lg-3 col-md-3">
                <input id="dropdownlist2" style="width: 100%;" />
            </div>

            <div class="col-lg-3 col-md-3">
            </div>
            <div class="col-lg-3 col-md-3" style="float: right;">
                <button id="Addnew" style="height: 35px;width: 100px;float: right;margin-right: -9%;" onclick="AddnewTest(event)" class="k-button">Add New</button>
            </div>

        </div>
    </div>
  
    <div class="row" style="padding-top: 12px;">
        <div id="grid" style="margin-left: 10px; margin-right: 10px;"></div>
    </div>

    <div class="modal fade" id="divShowReminderDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="height: 30px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseMyReminderPopup();">&times;</button>
                </div>

                <div class="modal-body">
                    <iframe src="about:blank" id="showReminderDetail" frameborder="0" runat="server" width="100%" height="300px"></iframe>
                </div>
            </div>
        </div>
    </div>

</asp:Content>