﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class LitigationComplianceDocument : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindAct();
                BindCompliance();
                FillComplianceDocuments();
                
            }
        }
        public static object GetAllCompliance(int CaseInstanceID, string type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int typeid = -1;
                if (type == "C")
                {
                    typeid = 1;
                }
                else if (type == "N")
                {
                    typeid = 2;
                }
                var query = (from row in entities.tbl_ActMapping
                             join row1 in entities.Compliances on
                             row.ActID equals row1.ActID
                             where row.IsActive == true && row.CaseNoticeInstanceID == CaseInstanceID
                             && row.Type == typeid && row1.IsDeleted == false && row1.Status == null
                             select row1).ToList();

                return query.OrderBy(entry => entry.ShortDescription).ToList();
            }
        }
        public static object GetAllAct(int CaseInstanceID, string type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int typeid = -1;
                if (type == "C")
                {
                    typeid = 1;
                }
                else if (type == "N")
                {
                    typeid = 2;
                }
                var query = (from row in entities.tbl_ActMapping
                             join row1 in entities.Acts on
                             row.ActID equals row1.ID
                             where row.IsActive == true && row.CaseNoticeInstanceID == CaseInstanceID
                             && row.Type == typeid
                             select row1).ToList();

                return query.OrderBy(entry => entry.Name).ToList();
            }
        }
        public void BindAct()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["caseinstanceid"]))
            {
                var caseInstanceID = Request.QueryString["caseinstanceid"];
                if (caseInstanceID != "")
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                    {
                        var Type = Request.QueryString["Type"];

                        var actList = GetAllAct(Convert.ToInt32(caseInstanceID), Type);

                        ddlAct.DataTextField = "Name";
                        ddlAct.DataValueField = "ID";

                        ddlAct.DataSource = actList;
                        ddlAct.DataBind();
                    }
                }
            }
        }

        public void BindCompliance()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["caseinstanceid"]))
            {
                var caseInstanceID = Request.QueryString["caseinstanceid"];
                if (caseInstanceID != "")
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                    {
                        var Type = Request.QueryString["Type"];

                        var actList = GetAllCompliance(Convert.ToInt32(caseInstanceID), Type);

                        ddlCompliance.DataTextField = "ShortDescription";
                        ddlCompliance.DataValueField = "ID";

                        ddlCompliance.DataSource = actList;
                        ddlCompliance.DataBind();

                    }
                }
            }
        }
        public List<SP_LitigationComplianceDocumentDisplay_Result> GetFilteredComplianceDocuments(int CaseInstanceID, DateTime DateFrom, DateTime DateTo, string isnoticeOrcase)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                int ActID = -1;
                int ComplianceID = -1;
                if (!String.IsNullOrEmpty(ddlAct.SelectedValue))
                {
                    if (ddlAct.SelectedValue != "-1")
                    {
                        ActID = Convert.ToInt32(ddlAct.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlCompliance.SelectedValue))
                {
                    if (ddlCompliance.SelectedValue != "-1")
                    {
                        ComplianceID = Convert.ToInt32(ddlCompliance.SelectedValue);
                    }
                }
                var documentData = (from row in entities.SP_LitigationComplianceDocumentDisplay(CaseInstanceID, isnoticeOrcase)
                                    select row).ToList();

                if (DateFrom.Year != 0001 && DateTo.Year != 0001)
                {
                    if (DateFrom.Year != 1900 && DateTo.Year != 1900)
                    {
                        documentData = documentData.Where(entry => (entry.ScheduleOn >= DateFrom && entry.ScheduleOn <= DateTo)).ToList();
                    }
                }
                if (ActID != -1)
                {
                    documentData = documentData.Where(entry => entry.ActID == ActID).ToList();
                }
                if (ComplianceID != -1)
                {
                    documentData = documentData.Where(entry => entry.ComplianceID == ComplianceID).ToList();
                }
                return documentData.GroupBy(entry => entry.ScheduledOnID).Select(en => en.FirstOrDefault()).ToList();
            }
        }
        public List<SP_LitigationComplianceDocumentDetails_Result> GetDocumnets(int CaseInstanceID, int ScheduledOnID, string isnoticeOrcase)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                int ActID = -1;
                int ComplianceID = -1;
                if (!String.IsNullOrEmpty(ddlAct.SelectedValue))
                {
                    if (ddlAct.SelectedValue != "-1")
                    {
                        ActID = Convert.ToInt32(ddlAct.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlCompliance.SelectedValue))
                {
                    if (ddlCompliance.SelectedValue != "-1")
                    {
                        ComplianceID = Convert.ToInt32(ddlCompliance.SelectedValue);
                    }
                }
                var DocumentList = (from row in entities.SP_LitigationComplianceDocumentDetails(CaseInstanceID, isnoticeOrcase, ScheduledOnID)
                                    select row).GroupBy(g => g.FileID).Select(g => g.FirstOrDefault()).ToList();

                if (ActID != -1)
                {
                    DocumentList = DocumentList.Where(entry => entry.ActID == ActID).ToList();
                }
                if (ComplianceID != -1)
                {
                    DocumentList = DocumentList.Where(entry => entry.ComplianceID == ComplianceID).ToList();
                }
                return DocumentList;
            }
        }
        public void FillComplianceDocuments()
        {
            try
            {

                if (!string.IsNullOrEmpty(Request.QueryString["caseinstanceid"]))
                {
                    var caseInstanceID = Request.QueryString["caseinstanceid"];
                    if (caseInstanceID != "")
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                        {
                            var Type = Request.QueryString["Type"];

                            DateTime DateFrom = new DateTime();
                            DateTime DateTo = new DateTime();

                            if (txtFromDate.Text != "")
                            {
                                DateFrom = DateTimeExtensions.GetDate(txtFromDate.Text);
                            }
                            if (txtEndDate.Text != "")
                            {
                                DateTo = DateTimeExtensions.GetDate(txtEndDate.Text);
                            }

                            var ComplianceDocs = GetFilteredComplianceDocuments(Convert.ToInt32(caseInstanceID), DateFrom, DateTo, Type);
                            grdComplianceDocument.DataSource = ComplianceDocs;
                            grdComplianceDocument.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceDocument_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            FillComplianceDocuments();
        }
        protected void grdComplianceDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }
        protected void grdComplianceDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Download")
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["caseinstanceid"]))
                    {
                        var caseInstanceID = Request.QueryString["caseinstanceid"];
                        if (caseInstanceID != "")
                        {
                            if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                            {
                                var Type = Request.QueryString["Type"];
                                string[] commandArg = e.CommandArgument.ToString().Split(',');
                                List<SP_LitigationComplianceDocumentDetails_Result> CMPDocuments = GetDocumnets(Convert.ToInt32(caseInstanceID), Convert.ToInt32(commandArg[0]), Type);
                                Session["ScheduleOnID"] = commandArg[0];

                                if (CMPDocuments != null)
                                {
                                    using (ZipFile ComplianceZip = new ZipFile())
                                    {
                                        List<SP_LitigationComplianceDocumentDetails_Result> ComplianceFileData = new List<SP_LitigationComplianceDocumentDetails_Result>();
                                        ComplianceFileData = CMPDocuments.ToList(); ;
                                        var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArg[0]));
                                        ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth);
                                        if (ComplianceFileData.Count > 0)
                                        {
                                            int i = 0;
                                            foreach (var file in ComplianceFileData)
                                            {
                                                string filePath = string.Empty;
                                                //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                {
                                                    filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                                }
                                                else
                                                {
                                                    filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                                }
                                                if (file.FilePath != null && File.Exists(filePath))
                                                {
                                                    string[] filename = file.FileName.Split('.');
                                                    string str = filename[0] + i + "." + filename[1];
                                                    if (file.EnType == "M")
                                                    {
                                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    else
                                                    {
                                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    i++;
                                                }
                                            }
                                        }

                                        var zipMs = new MemoryStream();
                                        ComplianceZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] data = zipMs.ToArray();
                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument.zip");
                                        Response.BinaryWrite(data);
                                        Response.Flush();
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Document available to Download.";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnkSaveDocument_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlDownloadLink.SelectedValue == "1")
                {
                    List<int> ScheduledOnIDList = new List<int>();
                    int chkDocumentCount = 0;
                    var caseInstanceID = 0;
                    var Type = string.Empty;
                    bool saveSuccess = false;

                    foreach (GridViewRow gvrow in grdComplianceDocument.Rows)
                    {
                        CheckBox chkDocument = (CheckBox) gvrow.FindControl("chkDocument");
                        Label lblScheduledOnID = (Label) gvrow.FindControl("lblScheduledOnID");

                        if (chkDocument.Checked)
                        {
                            ScheduledOnIDList.Add(Convert.ToInt32(lblScheduledOnID.Text));
                            chkDocumentCount++;
                        }
                    }

                    if (chkDocumentCount > 0)
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["caseinstanceid"]))
                        {
                            caseInstanceID = Convert.ToInt32(Request.QueryString["caseinstanceid"]);
                        }

                        List<FileData> ListofFiles = CaseManagement.GetAllComplianceDocuments(ScheduledOnIDList);

                        if (ListofFiles.Count > 0)
                        {
                            if (caseInstanceID > 0)
                            {
                                #region Upload Document

                                tbl_LitigationFileData objCaseDoc = new tbl_LitigationFileData()
                                {
                                    NoticeCaseInstanceID = caseInstanceID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    IsDeleted = false,
                                    DocType = "CD",
                                };

                                int customerID = -1;
                                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                                //string directoryPath = "";
                                String fileName = "";

                                if (caseInstanceID > 0)
                                {
                                    for (int i = 0; i < ListofFiles.Count; i++)
                                    {
                                        fileName = ListofFiles[i].Name;
                                        objCaseDoc.FileName = fileName;                                       
                                        objCaseDoc.FilePath = ListofFiles[i].FilePath;
                                        objCaseDoc.FileKey = ListofFiles[i].FileKey;
                                        objCaseDoc.VersionDate = DateTime.Now;
                                        objCaseDoc.CreatedOn = DateTime.Now;
                                        objCaseDoc.FileSize = ListofFiles[i].FileSize;
                                        saveSuccess = CaseManagement.CreateCaseDocumentMapping(objCaseDoc);                                        
                                    }
                                    if (saveSuccess)
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Compliance document saved successfully.";
                                    }

                                }//End For Each  

                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LitigationFileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Document(s) Uploaded", true);
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No Document available.";
                        }
                    }
                }
                else
                {
                    List<int> ScheduledOnIDList = new List<int>();
                    int chkDocumentCount = 0;
                    var Type = string.Empty;

                    foreach (GridViewRow gvrow in grdComplianceDocument.Rows)
                    {
                        CheckBox chkDocument = (CheckBox) gvrow.FindControl("chkDocument");
                        Label lblScheduledOnID = (Label) gvrow.FindControl("lblScheduledOnID");

                        if (chkDocument.Checked)
                        {
                            ScheduledOnIDList.Add(Convert.ToInt32(lblScheduledOnID.Text));
                            chkDocumentCount++;
                        }
                    }
                    if (chkDocumentCount > 0)
                    {
                        List<FileData> ListofFiles = CaseManagement.GetAllComplianceDocuments(ScheduledOnIDList);
                        if (ListofFiles.Count > 0)
                        {
                            using (ZipFile CompliancelitiZip = new ZipFile())
                            {
                                int i = 0;
                                foreach (var file in ListofFiles)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        int idx = file.Name.LastIndexOf('.');
                                        string str = file.Name.Substring(0, idx) + "_" + file.Version + "." + file.Name.Substring(idx + 1);

                                        if (!CompliancelitiZip.ContainsEntry(str))
                                        {
                                            if (file.EnType == "M")
                                            {
                                                CompliancelitiZip.AddEntry(str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                CompliancelitiZip.AddEntry(str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                        }
                                        i++;
                                    }
                                }

                                var zipMs = new MemoryStream();
                                CompliancelitiZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Downloaded", false);

                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No Document available to Download.";                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> ScheduledOnIDList = new List<int>();
                int chkDocumentCount = 0;
                var Type = string.Empty;

                foreach (GridViewRow gvrow in grdComplianceDocument.Rows)
                {
                    CheckBox chkDocument = (CheckBox) gvrow.FindControl("chkDocument");
                    Label lblScheduledOnID = (Label) gvrow.FindControl("lblScheduledOnID");

                    if (chkDocument.Checked)
                    {
                        ScheduledOnIDList.Add(Convert.ToInt32(lblScheduledOnID.Text));
                        chkDocumentCount++;
                    }
                }
                if (chkDocumentCount > 0)
                {
                    List<FileData> ListofFiles = CaseManagement.GetAllComplianceDocuments(ScheduledOnIDList);
                    if (ListofFiles.Count > 0)
                    {
                        using (ZipFile CompliancelitiZip = new ZipFile())
                        {
                            int i = 0;
                            foreach (var file in ListofFiles)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    int idx = file.Name.LastIndexOf('.');
                                    string str = file.Name.Substring(0, idx) + "_" + file.Version + "." + file.Name.Substring(idx + 1);

                                    if (!CompliancelitiZip.ContainsEntry(str))
                                    {
                                        if (file.EnType == "M")
                                        {
                                            CompliancelitiZip.AddEntry(str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            CompliancelitiZip.AddEntry(str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                    }
                                    i++;
                                }
                            }

                            var zipMs = new MemoryStream();
                            CompliancelitiZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] Filedata = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                            Response.BinaryWrite(Filedata);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Downloaded", false);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}
