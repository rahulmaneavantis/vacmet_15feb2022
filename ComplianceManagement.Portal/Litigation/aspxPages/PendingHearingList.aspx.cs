﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class PendingHearingList : System.Web.UI.Page
    {
        protected bool flag;
        private long CustomerID = AuthenticationHelper.CustomerID;
        public static bool isSort = false;
        public static bool isAscend = false;
        private const string ASCENDING = " ASC";
        private const string DESCENDING = " DESC";
        public static bool showImage = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["Status"] == "Pending")
                    {
                        ViewState["Status"] = 1;
                    }

                    BindGridResponseLog();
                    bindPageNumber();
                    flag = false;
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void BindGridResponseLog()
        {
            try
            {

                var lstCaseResponses = CaseManagement.GetPendingHearingList(AuthenticationHelper.CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3);

                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                {
                    lstCaseResponses = lstCaseResponses.Where(entry => entry.COURTCASENO != null).ToList();
                    lstCaseResponses = lstCaseResponses.Where(entry => entry.CaseTitle.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())
                     || entry.COURTCASENO.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())
                     || entry.CaseDetailDesc.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())
                     || entry.HearingDescription.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())
                     || entry.CBName.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();

                }

                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            lstCaseResponses = lstCaseResponses.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            lstCaseResponses = lstCaseResponses.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                flag = true;
                Session["TotalRows"] = null;
                if (lstCaseResponses.Count > 0)
                {
                    grdResponseLog.DataSource = lstCaseResponses;
                    Session["TotalRows"] = lstCaseResponses.Count;
                    grdResponseLog.DataBind();
                }
                else
                {
                    grdResponseLog.DataSource = lstCaseResponses;
                    grdResponseLog.DataBind();
                }
                lstCaseResponses.Clear();
                lstCaseResponses = null;


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CvHearingSaveMsg.IsValid = false;
                CvHearingSaveMsg.ErrorMessage = "Server Error Occurred. Please try again.";
                VSPendingHearing.CssClass = "alert alert-danger";
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }


                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdResponseLog.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindGridResponseLog();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdResponseLog.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdResponseLog.PageIndex = chkSelectedPage - 1;
            grdResponseLog.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindGridResponseLog();
            ShowGridDetail();
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }

                ShowGridDetail();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = 0;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                if (customerID != 0)
                {
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {

                        if (customerID != 0)
                        {
                            //var lstCaseResponses = CaseManagement.GetPendingHearingList();
                            List<USP_GetPendingHearingList_Result> MasterTransction = new List<USP_GetPendingHearingList_Result>();
                            {
                                MasterTransction = CaseManagement.GetPendingHearingList(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3);


                                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                                {
                                    MasterTransction = MasterTransction.Where(entry => entry.COURTCASENO != null).ToList();
                                    MasterTransction = MasterTransction.Where(entry => entry.CaseTitle.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())
                                     || entry.COURTCASENO.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())
                                     || entry.CaseDetailDesc.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())
                                     || entry.HearingDescription.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())
                                     || entry.CBName.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();

                                }
                                String FileName = String.Empty;
                                FileName = "HearingDatasheet";
                                ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Pending Hearing For Update");
                                DataTable ExcelData = null;
                                DataView view = new System.Data.DataView((DataTable)(MasterTransction).ToDataTable());
                                ExcelData = view.ToTable("Selected", false, "COURTCASENO", "HEARINGDATE");

                                ExcelData.Columns.Add("HearingDescription");
                                ExcelData.Columns.Add("NextHearingDate");
                                ExcelData.Columns.Add("Remark");
                                //ExcelData.Columns.Add("AdvocateAppearing");
                                //ExcelData.Columns.Add("StaffAppearing");
                                //ExcelData.Columns.Add("JudgeorAuthorityAppearing");

                                if (ExcelData.Rows.Count > 0)
                                {

                                    exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);
                                    exWorkSheet.Cells["A1"].Merge = true;
                                    exWorkSheet.Cells["A1"].Value = "Court Case No.";
                                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["A1"].AutoFitColumns(20);

                                    exWorkSheet.Cells["B1"].Merge = true;
                                    exWorkSheet.Cells["B1"].Value = "Hearing Date (DD-MMM-YYYY)";
                                    exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["B1"].AutoFitColumns(20);

                                    exWorkSheet.Cells["C1"].Merge = true;
                                    exWorkSheet.Cells["C1"].Value = "Hearing Description";
                                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["C1"].AutoFitColumns(50);

                                    exWorkSheet.Cells["D1"].Merge = true;
                                    exWorkSheet.Cells["D1"].Value = "NextHearing Date  (DD-MMM-YYYY)";
                                    exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["D1"].AutoFitColumns(20);

                                    exWorkSheet.Cells["E1"].Merge = true;
                                    exWorkSheet.Cells["E1"].Value = "Remark";
                                    exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["E1"].AutoFitColumns(20);

                                    //exWorkSheet.Cells["F1"].Merge = true;
                                    //exWorkSheet.Cells["F1"].Value = "AdvocateAppearing";
                                    //exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                                    //exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                                    //exWorkSheet.Cells["F1"].AutoFitColumns(20);

                                    //exWorkSheet.Cells["G1"].Merge = true;
                                    //exWorkSheet.Cells["G1"].Value = "StaffAppearing";
                                    //exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                                    //exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                                    //exWorkSheet.Cells["G1"].AutoFitColumns(20);

                                    //exWorkSheet.Cells["H1"].Merge = true;
                                    //exWorkSheet.Cells["H1"].Value = "JudgeorAuthorityAppearing";
                                    //exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                                    //exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                                    //exWorkSheet.Cells["H1"].AutoFitColumns(50);


                                    //Assign borders
                                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 5])
                                    {
                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                        col.Style.WrapText = true;

                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    }

                                    using (ExcelRange col = exWorkSheet.Cells[2, 2, 1 + ExcelData.Rows.Count, 5])
                                    {
                                        col[2, 2, 1 + ExcelData.Rows.Count, 8].Style.Numberformat.Format = "dd/MMM/yyyy";
                                    }
                                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                                    Response.ClearContent();
                                    Response.Buffer = true;
                                    Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");
                                    Response.Charset = "";
                                    Response.ContentType = "application/vnd.ms-excel";
                                    StringWriter sw = new StringWriter();
                                    Response.BinaryWrite(fileBytes);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                                else
                                {
                                    CvHearingSaveMsg.IsValid = false;
                                    CvHearingSaveMsg.ErrorMessage = "No data available to export";
                                    VSPendingHearing.CssClass = "alert alert-danger";
                                }
                            }
                        }
                    }
                }
                else
                {
                    CvHearingSaveMsg.IsValid = false;
                    CvHearingSaveMsg.ErrorMessage = "Please select Customer";
                    VSPendingHearing.CssClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(MasterFileUpload.FileName);
                    MasterFileUpload.SaveAs(Server.MapPath("~/Uploaded/") + filename.Trim());
                    FileInfo excelfile = new FileInfo(Server.MapPath("~/Uploaded/") + filename.Trim());

                    if (excelfile != null)
                    {
                        using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                        {
                           
                            
                            bool flag = TransactionSheetsExitsts(xlWorkbook, "Pending Hearing For Update");
                            if (flag == true)
                            {
                               
                                int CustomerID = 0;
                                bool saveSuccess = false;
                                int uploadedHearingCount = 0;
                                if (CustomerID == 0)
                                {
                                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                                }
                                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Pending Hearing For Update"];
                                if (xlWorksheet != null)
                                {
                                    List<string> errorMessage = new List<string>();
                                    int count = 0;
                                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                                    string Court_Case_No = string.Empty;
                                    string stringHearingDate = string.Empty;
                                    string stringNextHearingDate = string.Empty;
                                    string HearingDescription = string.Empty;
                                    //string AdvocateAppearing = string.Empty;
                                    //string StaffAppearing = string.Empty;
                                    //string judgeAuthorityAppearing = string.Empty;
                                    string Remarks = string.Empty;
                                    long legalcaseinstanceid = -1;

                                    #region Validations
                                    for (int i = 2; i <= xlrow2; i++)
                                    {
                                        count = count + 1;
                                        Court_Case_No = string.Empty;
                                        stringHearingDate = string.Empty;
                                        HearingDescription = string.Empty;
                                        //AdvocateAppearing = string.Empty;
                                        //StaffAppearing = string.Empty;
                                        //judgeAuthorityAppearing = string.Empty;
                                        Remarks = string.Empty;
                                        stringNextHearingDate = string.Empty;

                                        #region 1 Court Case No.
                                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                                        {
                                            Court_Case_No = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                                        }
                                        if (String.IsNullOrEmpty(Court_Case_No))
                                        {
                                            errorMessage.Add("Required Court Case No. at row number - " + (count + 1) + ".");
                                        }
                                        else
                                        {
                                            bool chkInternalCaseNo = CaseManagement.ExistsCaseRefNo(Court_Case_No, 0);
                                            if (!chkInternalCaseNo)
                                            {
                                                errorMessage.Add("Case with " + Court_Case_No + " not exists at row number - " + (count + 1) + ".");
                                            }
                                        }
                                        #endregion

                                        #region 2 Hearing Date
                                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text))
                                            stringHearingDate = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();

                                        if (stringHearingDate == "")
                                        {
                                            errorMessage.Add("Please Check the Hearing Date or Hearing Date can not be empty at row - " + i + "");
                                        }
                                        else
                                        {
                                            try
                                            {
                                                bool check = CheckDate(stringHearingDate);
                                                if (!check)
                                                {
                                                    errorMessage.Add("Please Check the Hearing Date or Hearing Date can not be empty at row - " + i + "");
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                errorMessage.Add("Please Check the Hearing Date or Hearing Date can not be empty at row - " + i + "");
                                            }
                                        }
                                        #endregion

                                        #region 3 Hearing Description
                                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                                        {
                                            HearingDescription = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                                        }
                                        if (String.IsNullOrEmpty(HearingDescription))
                                        {
                                            errorMessage.Add("Required Hearing Description at row number - " + (count + 1) + ".");
                                        }

                                        #endregion

                                        #region 4 Next Hearing Date
                                        if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text))
                                            stringNextHearingDate = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();

                                        if (!string.IsNullOrEmpty(stringNextHearingDate))
                                        {
                                            try
                                            {
                                                bool check = CheckDate(stringNextHearingDate);
                                                if (!check)
                                                {
                                                    errorMessage.Add("Please Check the Next Hearing Date or Next Hearing Date can not be empty at row - " + i + "");
                                                }
                                                else
                                                {
                                                    DateTime HD = Convert.ToDateTime(xlWorksheet.Cells[i, 2].Text);
                                                    DateTime NHD = Convert.ToDateTime(xlWorksheet.Cells[i, 4].Text);

                                                    if (NHD < HD)
                                                    {
                                                        errorMessage.Add("Please Check the Next Hearing Date should be greater than Hearing Date at row - " + i + "");
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                errorMessage.Add("Please Check the Next Hearing Date or Next Hearing Date can not be empty at row - " + i + "");
                                            }
                                        }
                                        #endregion

                                        #region 5 Remarks
                                        if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                                        {
                                            Remarks = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                                        }
                                        #endregion

                                        //#region 6 AdvocateAppearing
                                        //if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                                        //{
                                        //    AdvocateAppearing = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                                        //}
                                        //#endregion

                                        //#region 7 StaffAppearing
                                        //if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                                        //{
                                        //    StaffAppearing = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                                        //}
                                        //#endregion

                                        //#region 8 judgeAuthorityAppearing
                                        //if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                                        //{
                                        //    judgeAuthorityAppearing = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                                        //}
                                        //#endregion
                                    }
                                    #endregion

                                    if (errorMessage.Count <= 0)
                                    {
                                        #region Save

                                        for (int i = 2; i <= xlrow2; i++)
                                        {
                                            List<string> Both_Users = new List<string>();
                                            count = count + 1;
                                            Court_Case_No = string.Empty;
                                            stringHearingDate = string.Empty;
                                            HearingDescription = string.Empty;
                                            //AdvocateAppearing = string.Empty;
                                            //StaffAppearing = string.Empty;
                                            //judgeAuthorityAppearing = string.Empty;
                                            Remarks = string.Empty;
                                            stringNextHearingDate = string.Empty;

                                            #region 1 Court Case No.
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
                                            {
                                                Court_Case_No = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
                                            }
                                            #endregion

                                            #region 2 Hearing Date
                                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text))
                                                stringHearingDate = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
                                            #endregion

                                            #region 3 Hearing Description
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
                                            {
                                                HearingDescription = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
                                            }
                                            #endregion

                                            #region 4 Next Hearing Date
                                            if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text))
                                                stringNextHearingDate = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
                                            #endregion

                                            #region 5 Remarks
                                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
                                            {
                                                Remarks = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
                                            }
                                            #endregion

                                            //#region 6 AdvocateAppearing
                                            //if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
                                            //{
                                            //    AdvocateAppearing = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
                                            //}
                                            //#endregion

                                            //#region 7 StaffAppearing
                                            //if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
                                            //{
                                            //    StaffAppearing = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
                                            //}
                                            //#endregion

                                            //#region 8 judgeAuthorityAppearing
                                            //if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
                                            //{
                                            //    judgeAuthorityAppearing = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
                                            //}
                                            //#endregion

                                            legalcaseinstanceid = CaseManagement.GetCaseInstanceIDByCourtCaseNo(Court_Case_No);

                                            if (legalcaseinstanceid != 0 || legalcaseinstanceid != -1)
                                            {
                                                tbl_CaseHearingRef TBL_CHR = new tbl_CaseHearingRef();
                                                TBL_CHR.CustomerID = CustomerID;
                                                TBL_CHR.IsDeleted = false;
                                                TBL_CHR.CaseNoticeInstanceID = legalcaseinstanceid;
                                                if (!string.IsNullOrEmpty(stringHearingDate))
                                                {
                                                    TBL_CHR.HearingRefNo = "Hearing-" + Convert.ToDateTime(stringHearingDate).ToString("dd-MM-yyyy");
                                                }
                                                TBL_CHR.HearingDate = DateTimeExtensions.GetDate(Convert.ToDateTime(stringHearingDate).ToString("dd/MM/yyyy"));
                                                TBL_CHR.CreatedOn = DateTime.Now;
                                                TBL_CHR.CreatedBy = Portal.Common.AuthenticationHelper.UserID;

                                                var result = CaseManagement.GetExistsRefNo(TBL_CHR);

                                                if (result != -1)
                                                {
                                                    var newHearingRefID = CaseManagement.CreateNewRefNo(TBL_CHR);

                                                    if (newHearingRefID > 0)
                                                    //saveSuccess = true;
                                                    {
                                                        tbl_LegalCaseResponse TBL_LCR = new tbl_LegalCaseResponse();
                                                        TBL_LCR.IsActive = true;
                                                        TBL_LCR.CaseInstanceID = legalcaseinstanceid;
                                                        TBL_LCR.RefID = newHearingRefID;
                                                        TBL_LCR.ResponseDate = DateTimeExtensions.GetDate(Convert.ToDateTime(stringHearingDate).ToString("dd/MM/yyyy"));
                                                        TBL_LCR.Description = HearingDescription;
                                                        //TBL_LCR.AdvocateAppearing = AdvocateAppearing;
                                                        //TBL_LCR.StaffAppearing = StaffAppearing;
                                                        //TBL_LCR.JudgeorAuthorityAppearing = judgeAuthorityAppearing;
                                                        TBL_LCR.UserID = Portal.Common.AuthenticationHelper.UserID;
                                                        TBL_LCR.RoleID = 3;
                                                        TBL_LCR.Remark = Remarks;

                                                        if (!String.IsNullOrEmpty(stringNextHearingDate))
                                                            TBL_LCR.ReminderDate = DateTimeExtensions.GetDate(Convert.ToDateTime(stringNextHearingDate).ToString("dd/MM/yyyy"));

                                                        TBL_LCR.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
                                                        TBL_LCR.CreatedOn = DateTime.Now;
                                                        TBL_LCR.CreatedByText = Portal.Common.AuthenticationHelper.User;

                                                        var newResponseID = CaseManagement.CreateCaseResponseLog(TBL_LCR);
                                                        if (newResponseID > 0)
                                                        {
                                                            uploadedHearingCount++;
                                                            saveSuccess = true;

                                                            LitigationManagement.CreateAuditLog("C", legalcaseinstanceid, "tbl_LegalCaseResponse", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Created By Excel Upload", true);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        if (errorMessage.Count > 0)
                                        {
                                            ErrorMessages(errorMessage);
                                        }
                                    }

                                    if (saveSuccess)
                                    {
                                        if (uploadedHearingCount > 0)
                                        {
                                            tbxtypeTofilter.Text = string.Empty;
                                            BindGridResponseLog();
                                            bindPageNumber();
                                            CvHearingSaveMsg.IsValid = false;
                                            CvHearingSaveMsg.ErrorMessage = uploadedHearingCount + " Case Hearing(s) Details Uploaded Successfully";
                                            VSPendingHearing.CssClass = "alert alert-success";
                                        }
                                        else
                                        {
                                            CvHearingSaveMsg.IsValid = false;
                                            CvHearingSaveMsg.ErrorMessage = "No Data Uploaded from Excel Document";
                                        }
                                    }
                                }


                            }
                            else
                            {
                                CvHearingSaveMsg.IsValid = false;
                                CvHearingSaveMsg.ErrorMessage = "No Data Found in Excel Document or Sheet Name must be 'Pending Hearing For Update'.";
                            }
                        }
                    }
                    else
                    {
                        CvHearingSaveMsg.IsValid = false;
                        CvHearingSaveMsg.ErrorMessage = "Error uploading file. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    CvHearingSaveMsg.IsValid = false;
                    CvHearingSaveMsg.ErrorMessage = "Server Error Occurred. Please try again.";
                }
            }
            else
            {
                CvHearingSaveMsg.IsValid = false;
                CvHearingSaveMsg.ErrorMessage = "Please Select File For Upload Data";
            }
        }

        //protected void btnUpload_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        bool saveSuccess = false;
        //        int uploadedHearingCount = 0;
        //        if (CustomerID == 0)
        //        {
        //            CustomerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
        //        }
        //        ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["Case Hearing"];
        //        if (xlWorksheet != null)
        //        {
        //            List<string> errorMessage = new List<string>();
        //            int count = 0;
        //            int xlrow2 = xlWorksheet.Dimension.End.Row;
        //            string Court_Case_No = string.Empty;
        //            string stringHearingDate = string.Empty;
        //            string stringNextHearingDate = string.Empty;
        //            string HearingDescription = string.Empty;
        //            string AdvocateAppearing = string.Empty;
        //            string StaffAppearing = string.Empty;
        //            string judgeAuthorityAppearing = string.Empty;
        //            string Remarks = string.Empty;
        //            long legalcaseinstanceid = -1;

        //            #region Validations
        //            for (int i = 2; i <= xlrow2; i++)
        //            {
        //                count = count + 1;
        //                Court_Case_No = string.Empty;
        //                stringHearingDate = string.Empty;
        //                HearingDescription = string.Empty;
        //                AdvocateAppearing = string.Empty;
        //                StaffAppearing = string.Empty;
        //                judgeAuthorityAppearing = string.Empty;
        //                Remarks = string.Empty;
        //                stringNextHearingDate = string.Empty;

        //                #region 1 Court Case No.
        //                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
        //                {
        //                    Court_Case_No = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
        //                }
        //                if (String.IsNullOrEmpty(Court_Case_No))
        //                {
        //                    errorMessage.Add("Required Court Case No. at row number - " + (count + 1) + ".");
        //                }
        //                else
        //                {
        //                    bool chkInternalCaseNo = CaseManagement.ExistsCaseRefNo(Court_Case_No, 0);
        //                    if (!chkInternalCaseNo)
        //                    {
        //                        errorMessage.Add("Case with " + Court_Case_No + " not exists at row number - " + (count + 1) + ".");
        //                    }
        //                }
        //                #endregion

        //                #region 2 Hearing Date
        //                if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text))
        //                    stringHearingDate = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();

        //                if (stringHearingDate == "")
        //                {
        //                    errorMessage.Add("Please Check the Hearing Date or Hearing Date can not be empty at row - " + i + "");
        //                }
        //                else
        //                {
        //                    try
        //                    {
        //                        bool check = CheckDate(stringHearingDate);
        //                        if (!check)
        //                        {
        //                            errorMessage.Add("Please Check the Hearing Date or Hearing Date can not be empty at row - " + i + "");
        //                        }
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        errorMessage.Add("Please Check the Hearing Date or Hearing Date can not be empty at row - " + i + "");
        //                    }
        //                }
        //                #endregion

        //                #region 3 Hearing Description
        //                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
        //                {
        //                    HearingDescription = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
        //                }
        //                if (String.IsNullOrEmpty(HearingDescription))
        //                {
        //                    errorMessage.Add("Required Hearing Description at row number - " + (count + 1) + ".");
        //                }

        //                #endregion

        //                #region 4 Next Hearing Date
        //                if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text))
        //                    stringNextHearingDate = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();

        //                if (!string.IsNullOrEmpty(stringNextHearingDate))
        //                {
        //                    try
        //                    {
        //                        bool check = CheckDate(stringNextHearingDate);
        //                        if (!check)
        //                        {
        //                            errorMessage.Add("Please Check the Next Hearing Date or Next Hearing Date can not be empty at row - " + i + "");
        //                        }
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        errorMessage.Add("Please Check the Next Hearing Date or Next Hearing Date can not be empty at row - " + i + "");
        //                    }
        //                }
        //                #endregion

        //                #region 5 Remarks
        //                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
        //                {
        //                    Remarks = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
        //                }
        //                #endregion

        //                #region 6 AdvocateAppearing
        //                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
        //                {
        //                    AdvocateAppearing = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
        //                }
        //                #endregion

        //                #region 7 StaffAppearing
        //                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
        //                {
        //                    StaffAppearing = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
        //                }
        //                #endregion

        //                #region 8 judgeAuthorityAppearing
        //                if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
        //                {
        //                    judgeAuthorityAppearing = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
        //                }
        //                #endregion
        //            }
        //            #endregion

        //            if (errorMessage.Count <= 0)
        //            {
        //                #region Save

        //                for (int i = 2; i <= xlrow2; i++)
        //                {
        //                    List<string> Both_Users = new List<string>();
        //                    count = count + 1;
        //                    Court_Case_No = string.Empty;
        //                    stringHearingDate = string.Empty;
        //                    HearingDescription = string.Empty;
        //                    AdvocateAppearing = string.Empty;
        //                    StaffAppearing = string.Empty;
        //                    judgeAuthorityAppearing = string.Empty;
        //                    Remarks = string.Empty;
        //                    stringNextHearingDate = string.Empty;

        //                    #region 1 Court Case No.
        //                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString().Trim()))
        //                    {
        //                        Court_Case_No = Convert.ToString(xlWorksheet.Cells[i, 1].Text).Trim();
        //                    }
        //                    #endregion

        //                    #region 2 Hearing Date
        //                    if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text))
        //                        stringHearingDate = Convert.ToString(xlWorksheet.Cells[i, 2].Text).Trim();
        //                    #endregion

        //                    #region 3 Hearing Description
        //                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString().Trim()))
        //                    {
        //                        HearingDescription = Convert.ToString(xlWorksheet.Cells[i, 3].Text).Trim();
        //                    }
        //                    #endregion

        //                    #region 4 Next Hearing Date
        //                    if (!string.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text))
        //                        stringNextHearingDate = Convert.ToString(xlWorksheet.Cells[i, 4].Text).Trim();
        //                    #endregion

        //                    #region 5 Remarks
        //                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString().Trim()))
        //                    {
        //                        Remarks = Convert.ToString(xlWorksheet.Cells[i, 5].Text).Trim();
        //                    }
        //                    #endregion

        //                    #region 6 AdvocateAppearing
        //                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString().Trim()))
        //                    {
        //                        AdvocateAppearing = Convert.ToString(xlWorksheet.Cells[i, 6].Text).Trim();
        //                    }
        //                    #endregion

        //                    #region 7 StaffAppearing
        //                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 7].Text.ToString().Trim()))
        //                    {
        //                        StaffAppearing = Convert.ToString(xlWorksheet.Cells[i, 7].Text).Trim();
        //                    }
        //                    #endregion

        //                    #region 8 judgeAuthorityAppearing
        //                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 8].Text.ToString().Trim()))
        //                    {
        //                        judgeAuthorityAppearing = Convert.ToString(xlWorksheet.Cells[i, 8].Text).Trim();
        //                    }
        //                    #endregion

        //                    legalcaseinstanceid = CaseManagement.GetCaseInstanceIDByCourtCaseNo(Court_Case_No);

        //                    if (legalcaseinstanceid != 0 || legalcaseinstanceid != -1)
        //                    {
        //                        tbl_CaseHearingRef TBL_CHR = new tbl_CaseHearingRef();
        //                        TBL_CHR.CustomerID = CustomerID;
        //                        TBL_CHR.IsDeleted = false;
        //                        TBL_CHR.CaseNoticeInstanceID = legalcaseinstanceid;
        //                        if (!string.IsNullOrEmpty(stringHearingDate))
        //                        {
        //                            TBL_CHR.HearingRefNo = "Hearing-" + Convert.ToDateTime(stringHearingDate).ToString("dd-MM-yyyy");
        //                        }
        //                        TBL_CHR.HearingDate = DateTimeExtensions.GetDate(Convert.ToDateTime(stringHearingDate).ToString("dd/MM/yyyy"));
        //                        TBL_CHR.CreatedOn = DateTime.Now;
        //                        TBL_CHR.CreatedBy = Portal.Common.AuthenticationHelper.UserID;

        //                        var result = CaseManagement.GetExistsRefNo(TBL_CHR);

        //                        if (result != -1)
        //                        {
        //                            var newHearingRefID = CaseManagement.CreateNewRefNo(TBL_CHR);

        //                            if (newHearingRefID > 0)
        //                            //saveSuccess = true;
        //                            {
        //                                tbl_LegalCaseResponse TBL_LCR = new tbl_LegalCaseResponse();
        //                                TBL_LCR.IsActive = true;
        //                                TBL_LCR.CaseInstanceID = legalcaseinstanceid;
        //                                TBL_LCR.RefID = newHearingRefID;
        //                                TBL_LCR.ResponseDate = DateTimeExtensions.GetDate(Convert.ToDateTime(stringHearingDate).ToString("dd/MM/yyyy"));
        //                                TBL_LCR.Description = HearingDescription;
        //                                TBL_LCR.AdvocateAppearing = AdvocateAppearing;
        //                                TBL_LCR.StaffAppearing = StaffAppearing;
        //                                TBL_LCR.JudgeorAuthorityAppearing = judgeAuthorityAppearing;
        //                                TBL_LCR.UserID = Portal.Common.AuthenticationHelper.UserID;
        //                                TBL_LCR.RoleID = 3;
        //                                TBL_LCR.Remark = Remarks;

        //                                if (!String.IsNullOrEmpty(stringNextHearingDate))
        //                                    TBL_LCR.ReminderDate = DateTimeExtensions.GetDate(Convert.ToDateTime(stringNextHearingDate).ToString("dd/MM/yyyy"));

        //                                TBL_LCR.CreatedBy = Portal.Common.AuthenticationHelper.UserID;
        //                                TBL_LCR.CreatedOn = DateTime.Now;
        //                                TBL_LCR.CreatedByText = Portal.Common.AuthenticationHelper.User;

        //                                var newResponseID = CaseManagement.CreateCaseResponseLog(TBL_LCR);
        //                                if (newResponseID > 0)
        //                                {
        //                                    uploadedHearingCount++;
        //                                    saveSuccess = true;

        //                                    LitigationManagement.CreateAuditLog("C", legalcaseinstanceid, "tbl_LegalCaseResponse", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Created By Excel Upload", true);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //                #endregion
        //            }
        //            else
        //            {
        //                if (errorMessage.Count > 0)
        //                {
        //                    ErrorMessages(errorMessage);
        //                }
        //            }

        //            if (saveSuccess)
        //            {
        //                if (uploadedHearingCount > 0)
        //                {
        //                    CvHearingSaveMsg.IsValid = false;
        //                    CvHearingSaveMsg.ErrorMessage = uploadedHearingCount + " Case Hearing(s) Details Uploaded Successfully";
        //                    VSPendingHearing.CssClass = "alert alert-success";
        //                }
        //                else
        //                {
        //                    CvHearingSaveMsg.IsValid = false;
        //                    CvHearingSaveMsg.ErrorMessage = "No Data Uploaded from Excel Document";
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        CvHearingSaveMsg.IsValid = false;
        //        CvHearingSaveMsg.ErrorMessage = "Server Error Occurred. Please try again.";
        //    }


        //}

        public void ErrorMessages(List<string> emsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            CvHearingSaveMsg.IsValid = false;
            CvHearingSaveMsg.ErrorMessage = finalErrMsg;
        }

        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected void lnkBtnClearFilter_Click(object sender, EventArgs e)
        {
            try
            {
                tbxtypeTofilter.Text = string.Empty;
                lnkBtnApplyFilter_Click(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGridResponseLog(); bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = 0;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                if (customerID != 0)
                {
                    using (ExcelPackage exportPackge = new ExcelPackage())
                    {
                        if (customerID != 0)
                        {
                            List<USP_GetPendingHearingList_Result> MasterTransction = new List<USP_GetPendingHearingList_Result>();
                            {
                                MasterTransction = CaseManagement.GetPendingHearingList(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3);


                                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                                {
                                    MasterTransction = MasterTransction.Where(entry => entry.COURTCASENO != null).ToList();
                                    MasterTransction = MasterTransction.Where(entry => entry.CaseTitle.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())
                                     || entry.COURTCASENO.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())
                                     || entry.CaseDetailDesc.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())
                                     || entry.HearingDescription.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())
                                     || entry.CBName.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();

                                }
                                String FileName = String.Empty;
                                FileName = "PendingHearingList";
                                ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Pending Hearing List");
                                DataTable ExcelData = null;
                                DataView view = new System.Data.DataView((DataTable)(MasterTransction).ToDataTable());
                                ExcelData = view.ToTable("Selected", false, "COURTCASENO", "CaseTitle", "CaseDetailDesc", "CBName",  "HEARINGDATE", "HearingDescription");

                                if (ExcelData.Rows.Count > 0)
                                {

                                    exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);
                                    exWorkSheet.Cells["A1"].Merge = true;
                                    exWorkSheet.Cells["A1"].Value = "Court Case No.";
                                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["A1"].AutoFitColumns(20);

                                    exWorkSheet.Cells["B1"].Merge = true;
                                    exWorkSheet.Cells["B1"].Value = "CaseTitle";
                                    exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["B1"].AutoFitColumns(30);

                                    exWorkSheet.Cells["C1"].Merge = true;
                                    exWorkSheet.Cells["C1"].Value = "Case Detail Description";
                                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["C1"].AutoFitColumns(60);

                                    exWorkSheet.Cells["D1"].Merge = true;
                                    exWorkSheet.Cells["D1"].Value = "Location Name";
                                    exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["D1"].AutoFitColumns(30);

                                    //exWorkSheet.Cells["E1"].Merge = true;
                                    //exWorkSheet.Cells["E1"].Value = "State";
                                    //exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                                    //exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                                    //exWorkSheet.Cells["E1"].AutoFitColumns(30);


                                    //exWorkSheet.Cells["F1"].Merge = true;
                                    //exWorkSheet.Cells["F1"].Value = "Region";
                                    //exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                                    //exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                                    //exWorkSheet.Cells["F1"].AutoFitColumns(30);


                                    //exWorkSheet.Cells["G1"].Merge = true;
                                    //exWorkSheet.Cells["G1"].Value = "Zone";
                                    //exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                                    //exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                                    //exWorkSheet.Cells["G1"].AutoFitColumns(30);


                                    exWorkSheet.Cells["E1"].Merge = true;
                                    exWorkSheet.Cells["E1"].Value = "NextHearingDate (DD-MMM-YYYY)";
                                    exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["E1"].AutoFitColumns(20);

                                    exWorkSheet.Cells["F1"].Merge = true;
                                    exWorkSheet.Cells["F1"].Value = "HearingDescription";
                                    exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                                    exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                                    exWorkSheet.Cells["F1"].AutoFitColumns(30);
                                     
                                    //Assign borders
                                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 6])
                                    {
                                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                                        col.Style.WrapText = true;

                                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    }

                                    using (ExcelRange col = exWorkSheet.Cells[2, 2, 1 + ExcelData.Rows.Count, 6])
                                    {
                                        col[2, 2, 1 + ExcelData.Rows.Count, 8].Style.Numberformat.Format = "dd/MMM/yyyy";
                                    }
                                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                                    Response.ClearContent();
                                    Response.Buffer = true;
                                    Response.AddHeader("content-disposition", "attachment;filename=" + FileName + "-" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx");
                                    Response.Charset = "";
                                    Response.ContentType = "application/vnd.ms-excel";
                                    StringWriter sw = new StringWriter();
                                    Response.BinaryWrite(fileBytes);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                                else
                                {
                                    CvHearingSaveMsg.IsValid = false;
                                    CvHearingSaveMsg.ErrorMessage = "No data available to export";
                                    VSPendingHearing.CssClass = "alert alert-danger";
                                }
                            }
                        }
                    }
                }
                else
                {
                    CvHearingSaveMsg.IsValid = false;
                    CvHearingSaveMsg.ErrorMessage = "Loged in user has not assigned Customer";
                    VSPendingHearing.CssClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CvHearingSaveMsg.IsValid = false;
                CvHearingSaveMsg.ErrorMessage = "Server Error Occured. Please try again.";
                VSPendingHearing.CssClass = "alert alert-danger";
            }
        }

        private bool TransactionSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("Pending Hearing For Update"))
                    {
                        if (sheet.Name.Trim().Equals("Pending Hearing For Update"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                    
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CvHearingSaveMsg.IsValid = false;
                CvHearingSaveMsg.ErrorMessage = "Server Error Occurred. Please try again.";
            }
            return false;
        }
    }

}