﻿<%@ Page Title="Case :: My Workspace" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="CaseList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.CaseList" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function CloseCaseTypePopUp() {
            $('#AddCategoryType').modal('hide');
        }

        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { };

        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            fhead('My Workspace/Case');
        });
        function setIframeHeight(height) {
            alert(height)
            $('#showdetails').attr('height', height);
        }
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }<%-- event.target.id != '<%= tbxFilterLocation.ClientID %>'--%>
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });

        function ddlActChange() {

            var selectedActID = $("#<%=ddlAct.ClientID %>").val();
    if (selectedActID != null) {
        if (selectedActID == 0) {
            $("#<%=lnkShowAddNewActModal.ClientID %>").show();
        }
        else {
            $("#<%=lnkShowAddNewActModal.ClientID %>").hide();
                }
            }
        }

        function ddlPartyChange() {

            var selectedPartyID = $("#<%=ddlParty.ClientID %>").val();
    if (selectedPartyID != null) {
        if (selectedPartyID == 0) {
            $("#<%=lnkShowAddNewPartyModal.ClientID %>").show();
        }
        else {
            $("#<%=lnkShowAddNewPartyModal.ClientID %>").hide();
                }
            }
        }

        function ddlNoticeCategoryChange() {

            var selectedNoticeCatID = $("#<%=ddlNoticeCategory.ClientID %>").val();
    if (selectedNoticeCatID != null) {
        if (selectedNoticeCatID == 0) {
            $("#<%=lnkAddNewNoticeCategoryModal.ClientID %>").show();
        }
        else {
            $("#<%=lnkAddNewNoticeCategoryModal.ClientID %>").hide();
                }
            }
        }

        function ddlDepartmentChange() {

            var selectedDeptID = $("#<%=ddlDepartment.ClientID %>").val();
            if (selectedDeptID != null) {
                if (selectedDeptID == 0) {
                    $("#<%=lnkAddNewDepartmentModal.ClientID %>").show();
                }
                else {
                    $("#<%=lnkAddNewDepartmentModal.ClientID %>").hide();
                }
            }
        }

        function hideDivBranch() {
            $('#divBranches').hide("blind", null, 500, function () { });
        }

        function openNoticeModal() {
            $('#divAddNoticeModal').modal('show');
        }

        function closeCaseModal() {
            document.getElementById('<%= lnkBtnBindGrid.ClientID %>').click();
        }

        function openAddNewActModal() {
            $('#divAddNewActModal').modal('show');
        }

        function ShowDialog(CaseInstanceID) {
            $('#divShowDialog').modal('show');
            $('.modal-dialog').css('width', '95%');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '550px');
            $('#showdetails').attr('src', "../aspxPages/CaseDetailPage.aspx?AccessID=" + CaseInstanceID);
            // $('#divShowDialog').modal('handleUpdate');
        };

        function ClosePopNoticeDetialPage() {
            $('#divShowDialog').modal('hide');
        }

        function validateForm() {

            if ($('#<%=tbxRefNo.ClientID %>').val() == '') {
                $('#<%=tbxRefNo.ClientID %>').css('border-color', 'red');
            }
            else {
                $('#tbxRefNo').css('border-color', '');
            }
        }

    </script>

    <style type="text/css">
        .AddNewspan1 {
    padding: 5px;
    border: 2px solid #f4f0f0;
    border-radius: 51px;
    display: inline-block;
    height: 26px;
    width: 26px;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row Dashboard-white-widget">
        <div class="dashboard">

            <div class="col-md-12 colpadding0">
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                    ValidationGroup="CasePageValidationGroup" />
                <asp:CustomValidator ID="cvErrorNoticePage" runat="server" EnableClientScript="False"
                    ValidationGroup="CasePageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div class="col-md-12 colpadding0">

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 9%; margin-right:10px;">
                    <asp:DropDownListChosen runat="server" ID="ddlTypePage" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                        DataPlaceHolder="Select Type" class="form-control" Width="100%" OnSelectedIndexChanged="ddlTypePage_SelectedIndexChanged">
                        <asp:ListItem Text="Case" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Notice" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Task" Value="3"></asp:ListItem>
                         <asp:ListItem Text="Advocate Bill" Value="4"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 9%; margin-right:10px;">
                    <asp:DropDownListChosen runat="server" ID="ddlStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select Status" class="form-control" Width="100%" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                        <asp:ListItem Text="All" Value="-1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Open" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Closed" Value="3"></asp:ListItem>
                       <%-- <asp:ListItem Text="Settled" Value="4"></asp:ListItem>--%>
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 15%; margin-right:10px;">
                    <asp:DropDownListChosen runat="server" ID="ddlNoticeTypePage" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select Status" class="form-control" Width="100%">
                        <asp:ListItem Text="All" Value="B"></asp:ListItem>
                        <asp:ListItem Text="Inward/Defendant" Value="I"></asp:ListItem>
                        <asp:ListItem Text="Outward/Plaintiff" Value="O"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 13%; margin-right:10px;">
                    <asp:DropDownListChosen runat="server" ID="ddlPartyPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                        DataPlaceHolder="Select Opponent" class="form-control" Width="100%" />
                </div>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 13%; margin-right:10px;">
                    <asp:DropDownListChosen runat="server" ID="ddlDeptPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                        DataPlaceHolder="Select Department" class="form-control" Width="100%" />
                </div>

                <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                    <ContentTemplate>
                        <div class="col-md-4 colpadding0 entrycount" style="margin-top: 5px; width: 23%;">
                            <asp:TextBox runat="server" ID="tbxFilterLocation" PlaceHolder="Select Entity/Branch/Location" autocomplete="off" CssClass="clsDropDownTextBox" />
                            <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px; width: 95%" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                                    Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                    OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                </asp:TreeView>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 8%; float:right">
                    <asp:LinkButton Style="float: right;width: 75px;" CssClass="btn btn-primary" runat="server" ID="btnAddCase" data-toggle="tooltip" ToolTip="Add New Case"
                        OnClick="btnAddCase_Click"><span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                </div>
                

            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 colpadding0">
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 18.8%;  margin-right:10px;">
                    <asp:DropDownListChosen runat="server" ID="ddlType" DataPlaceHolder="Select Type" AllowSingleDeselect="false"
                        class="form-control m-bot15" Width="100%" Height="30px">
                    </asp:DropDownListChosen>
                </div>
                <div class="col-md-3 colpadding0" style="margin-top: 5px; width: 15%; margin-right:10px;">
                    <asp:DropDownListChosen runat="server" ID="ddlOwnerlist" DataPlaceHolder="Select Owner" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        CssClass="form-control" Width="100%" />
                </div>
                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 13%; margin-right:10px;">
                    <asp:DropDownListChosen runat="server" ID="ddlFinancialYear" AllowSingleDeselect="false" DisableSearchThreshold="3"
                        DataPlaceHolder="Select Financial Year" class="form-control" Width="100%" >                   
                        </asp:DropDownListChosen>
                </div>
                <div class="col-md-3 colpadding0" style="margin-top: 5px; width: 29.8%;  margin-right:5px;">
                    <asp:TextBox runat="server" ID="tbxtypeTofilter" Width="100%" AutoComplete="off" placeholder="Type to Search" CssClass="form-control" />
                </div>
                
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 13.9%;float:right">
                    <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lnkBtnApplyFilter" style=" float:right; width:75px" OnClick="lnkBtnApplyFilter_Click" data-toggle="tooltip" ToolTip="Apply" data-placement="bottom"/>
                    <asp:LinkButton runat="server" ID="btnExport" Style="margin-top: 5px;float:left" OnClick="btnExport_Click" data-toggle="tooltip" ToolTip="Export to Excel" data-placement="bottom">
                            <img src="../../Images/Excel _icon.png" alt="Export to Excel"/> 
                            </asp:LinkButton>
                </div>
                
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 colpadding0">
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 10%;">
                    <asp:LinkButton ID="lnkBtnBindGrid" OnClick="lnkBtnBindGrid_Click" Style="float: right; display: none;" Width="100%" runat="server">
                    </asp:LinkButton>
                </div>
            </div>

            <div class="clearfix"></div>
            <div style="margin-bottom: 4px">
                <asp:GridView runat="server" ID="grdCaseDetails" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    PageSize="10" AllowPaging="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="CaseInstanceID"
                    OnRowCommand="grdCaseDetails_RowCommand" OnRowDataBound="grdCaseDetails_RowDataBound" OnSorting="grdCaseDetails_Sorting" OnRowCreated="grdCaseDetails_RowCreated">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="3%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Type" ItemStyle-Width="10%" SortExpression="CaseTypeName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CaseTypeName") %>' ToolTip='<%# Eval("CaseTypeName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField SortExpression="CaseRefNo" HeaderText="Court Case No." ItemStyle-Width="20%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CaseRefNo") %>' ToolTip='<%# Eval("CaseRefNo") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Case" ItemStyle-Width="30%" SortExpression="CaseTitle">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CaseTitle") %>' ToolTip='<%# Eval("CaseTitle") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Opponent" ItemStyle-Width="15%" SortExpression="PartyName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("PartyName") %>' ToolTip='<%# Eval("PartyName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Entity" ItemStyle-Width="17%" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Department" ItemStyle-Width="10%" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DeptName") %>' ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Stage" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CaseStage") %>' ToolTip='<%# Eval("CaseStage") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="7%">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEditCase" runat="server" ToolTip="Edit Case Details" data-toggle="tooltip" OnClick="lnkEditCase_Click"
                                    CommandArgument='<%# Eval("CaseInstanceID") %>'>                                   
                                    <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="Edit Case Details"/></asp:LinkButton>
                                <asp:LinkButton ID="lnkDeleteCase" runat="server" CommandName="DELETE_Case" ToolTip="Delete Case" data-toggle="tooltip" Visible="false"
                                    CommandArgument='<%# Eval("CaseInstanceID") %>' OnClientClick="return confirm('Are you certain you want to Delete this Case?');">
                                     <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Case"/></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="col-md-12 colpadding0">
                <div class="col-md-8 colpadding0">
                    <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                        <p style="padding-right: 0px !Important;">
                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="col-md-2 colpadding0">
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right; height: 32px !important"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                        <asp:ListItem Text="5" />
                        <asp:ListItem Text="10" Selected="True" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                <div class="col-md-2 colpadding0" style="float: right;">
                    <div style="float: left; width: 60%">
                        <p class="clsPageNo">Page</p>
                    </div>
                    <div style="float: left; width: 40%">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                        </asp:DropDownListChosen>
                    </div>
                </div>
                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
            </div>
        </div>
        <%--</section>--%>
    </div>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 200px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Add/Edit Case Details</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeCaseModal();">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divAddNoticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 900px;">
            <div class="modal-content" style="background-color: #f1f1f1;">
                <div class="modal-header" style="background-color: #f1f1f1;">
                    <%--<h4 class="modal-title" id="myModalLabel">Add/Edit Notice</h4>--%>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <%-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>--%>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="upNoticePopup" runat="server" UpdateMode="Conditional" OnLoad="upNoticePopup_Load">
                        <ContentTemplate>
                            <div class="container">

                                <div style="margin-bottom: 7px">
                                    <asp:ValidationSummary ID="VSNoticePopup" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                        ValidationGroup="NoticePopUpValidationGroup" />
                                    <asp:CustomValidator ID="cvNoticePopUp" runat="server" EnableClientScript="False"
                                        ValidationGroup="NoticePopUpValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                </div>

                                <div id="divNoticeDetails" class="row Dashboard-white-widget">
                                    <!--NoticeDetail Panel Start-->
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseDivNoticeDetails">
                                                        <h2>Notice Details</h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivNoticeDetails">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseDivNoticeDetails" class="panel-collapse collapse">
                                                <div class="panel-body">

                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 13.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Type</label>
                                                            <asp:RadioButtonList ID="rbNoticeInOutType" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="Inward" Value="I" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="Outward" Value="O"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Reference No.</label>
                                                            <asp:TextBox runat="server" ID="tbxRefNo" Style="width: 70%" CssClass="form-control" MaxLength="100" />
                                                            <asp:RequiredFieldValidator ID="rfvRefNo" ErrorMessage="Ref Number can not be empty."
                                                                ControlToValidate="tbxRefNo" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Opponent</label>
                                                            <div style="float: left; width: 60%">
                                                                <asp:DropDownListChosen runat="server" ID="ddlParty" AllowSingleDeselect="false" DataPlaceHolder="Select Opponent" CssClass="form-control"
                                                                    Width="100%" onchange="ddlPartyChange()" />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Select Opponent"
                                                                    ControlToValidate="ddlParty" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                            </div>
                                                            <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                <asp:LinkButton ID="lnkShowAddNewPartyModal" Style="float: right; display: none;" Width="100%" runat="server"
                                                                    OnClientClick="openAddNewActModal()" CausesValidation="false">                                                               
                                                                      <img src='<%# ResolveUrl("~/Images/add_icon_new.png")%>' alt="Add New Opponent" title="Add New Opponent" />
                                                                </asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Act</label>
                                                            <div style="float: left; width: 60%">
                                                                <asp:DropDownListChosen runat="server" ID="ddlAct" AllowSingleDeselect="false" class="form-control" DataPlaceHolder="Select Act"
                                                                    Width="100%" onchange="ddlActChange()" />
                                                                <%--AutoPostBack="true" OnSelectedIndexChanged="ddlAct_SelectedIndexChanged"--%>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please Select Act"
                                                                    ControlToValidate="ddlAct" runat="server" ValidationGroup="NoticePopUpValidationGroup"
                                                                    Display="None" />
                                                            </div>
                                                            <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                <asp:LinkButton ID="lnkShowAddNewActModal" Style="float: right; display: none;" Width="100%" runat="server"
                                                                    OnClientClick="openAddNewActModal()" CausesValidation="false">                                                                     
                                                               <img src='<%# ResolveUrl("~/Images/add_icon_new.png")%>' alt="Add New Act" title="Add New Act" />
                                                                </asp:LinkButton>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Section</label>
                                                            <asp:TextBox runat="server" ID="tbxSection" Style="width: 70%;" CssClass="form-control" MaxLength="100" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Section can not be empty."
                                                                ControlToValidate="tbxSection" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Notice Category</label>
                                                            <div style="float: left; width: 60%">
                                                                <asp:DropDownListChosen runat="server" ID="ddlNoticeCategory" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                                    class="form-control" Width="100%" DataPlaceHolder="Select Notice Category" onchange="ddlNoticeCategoryChange()">
                                                                    <asp:ListItem Text="High" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                                                                </asp:DropDownListChosen>
                                                            </div>

                                                            <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                <asp:LinkButton ID="lnkAddNewNoticeCategoryModal" Style="float: right; display: none;" Width="100%" runat="server" OnClientClick="openAddNewActModal()" CausesValidation="false">
                                                                 <img src='<%# ResolveUrl("~/Images/add_icon_new.png")%>' alt="Add New Category" title="Add New Category" />                                                                  
                                                                </asp:LinkButton>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Notice Title</label>
                                                            <asp:TextBox runat="server" ID="tbxTitle" Style="width: 70%;" CssClass="form-control" MaxLength="100" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Title can not be empty."
                                                                ControlToValidate="tbxTitle" runat="server" ValidationGroup="NoticePopUpValidationGroup"
                                                                Display="None" />
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 13.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Notice Description</label>
                                                            <asp:TextBox runat="server" ID="tbxDescription" TextMode="MultiLine" Style="width: 85%;" CssClass="form-control" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Description can not be empty."
                                                                ControlToValidate="tbxDescription" runat="server" ValidationGroup="NoticePopUpValidationGroup"
                                                                Display="None" />
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Entity/Location</label>
                                                            <asp:TextBox runat="server" ID="tbxBranch" Style="padding: 5px; margin: 0px; width: 70%;" autocomplete="off" AutoCompleteType="None"
                                                                CausesValidation="true" CssClass="form-control" />
                                                            <%--onclick="txtclick()"--%>
                                                            <div style="margin-left: 28%; position: absolute; z-index: 10" id="divBranches">
                                                                <asp:TreeView runat="server" ID="tvBranches" BackColor="White" BorderColor="Black"
                                                                    BorderWidth="1" SelectedNodeStyle-Font-Bold="true" Height="100px" Width="85%"
                                                                    Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvBranches_SelectedNodeChanged">
                                                                </asp:TreeView>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="rfvBranch" ErrorMessage="Please Select Entity/Location."
                                                                ControlToValidate="tbxBranch" runat="server" ValidationGroup="NoticePopUpValidationGroup" InitialValue="Select Entity/Location"
                                                                Display="None" />
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Department</label>
                                                            <div style="float: left; width: 60%">
                                                                <asp:DropDownListChosen runat="server" ID="ddlDepartment" AllowSingleDeselect="false" DisableSearchThreshold="3"
                                                                    DataPlaceHolder="Select Department" class="form-control" Width="100%" onchange="ddlDepartmentChange()" />
                                                                <asp:RequiredFieldValidator ID="rfvDept" ErrorMessage="Please Select Department"
                                                                    ControlToValidate="ddlDepartment" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                            </div>
                                                            <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                                                <asp:LinkButton ID="lnkAddNewDepartmentModal" Style="float: right; display: none;" Width="100%" runat="server" OnClientClick="openAddNewActModal()" CausesValidation="false">
                                                                 <img src='<%# ResolveUrl("~/Images/add_icon_new.png")%>' alt="Add New Department" title="Add New Department" />

                                                                </asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Owner</label>
                                                            <asp:DropDownListChosen runat="server" ID="ddlOwner" DataPlaceHolder="Select Owner" AllowSingleDeselect="false" CssClass="form-control" Width="70%" />
                                                            <asp:RequiredFieldValidator ID="rfvOwner" ErrorMessage="Please Select Owner"
                                                                ControlToValidate="ddlOwner" runat="server" ValidationGroup="NoticePopUpValidationGroup" Display="None" />
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Risk</label>
                                                            <asp:DropDownListChosen runat="server" ID="ddlNoticeRisk" class="form-control" Width="70%">
                                                                <asp:ListItem Text="Select Risk" Value="-1" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="High" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                                                            </asp:DropDownListChosen>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Claimed Amount</label>
                                                            <asp:TextBox runat="server" ID="tbxClaimedAmt" Style="width: 70%;" CssClass="form-control" MaxLength="100" />
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Probable Amount</label>
                                                            <asp:TextBox runat="server" ID="tbxProbableAmt" Style="width: 70%;" CssClass="form-control" MaxLength="100" />
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div id="divNoticeAssignmentDetails" class="row Dashboard-white-widget">
                                    <!--NoticeDetail Panel Start-->
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseDivNoticeAssignmentDetails">
                                                        <h2>Assignment Details</h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion"
                                                            href="#collapseDivNoticeAssignmentDetails">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseDivNoticeAssignmentDetails" class="panel-collapse collapse">
                                                <div class="panel-body">

                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Lawyer</label>
                                                            <asp:DropDownListChosen ID="ddlLawFirm" CssClass="form-control" AutoPostBack="true" runat="server" DataPlaceHolder="Select User" Width="70%"
                                                                AllowSingleDeselect="false">
                                                            </asp:DropDownListChosen>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Performer</label>
                                                            <asp:DropDownListChosen runat="server" ID="ddlPerformer" DataPlaceHolder="Select Performer" AllowSingleDeselect="false" CssClass="form-control"
                                                                Width="70%" />
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                            <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Reviewer</label>
                                                            <asp:DropDownListChosen runat="server" ID="ddlReviewer" DataPlaceHolder="Select Reviewer" AllowSingleDeselect="false" CssClass="form-control"
                                                                Width="70%" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div id="divNoticeDocuments" class="row Dashboard-white-widget">
                                    <!--NoticeDetail Panel Start-->
                                    <div class="col-lg-12 col-md-12">
                                        <div class="panel panel-default">

                                            <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseDivNoticeDocuments">
                                                        <h2>Upload Documents</h2>
                                                    </a>
                                                    <div class="panel-actions">
                                                        <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseDivNoticeDocuments">
                                                            <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapseDivNoticeDocuments" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label style="width: 1.5%; display: block; float: left; font-size: 13px; color: red;"></label>
                                                            <label style="width: 13.5%; display: block; float: left; font-size: 13px; color: #333;">
                                                                Upload Documents</label>
                                                            <div style="width: 100%;">
                                                                <div style="width: 50%; float: left;">
                                                                    <asp:FileUpload ID="NoticeFileUpload" runat="server" />
                                                                </div>
                                                                <%-- <div style="width: 50%; float: right;">
                                                                    <asp:Button ID="btnNoticeUpload" runat="server" Text="Upload" CssClass="btn btn-search" OnClick="btnNoticeUpload_Click" />
                                                                </div>--%>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-12">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;"></label>
                                                        <label style="width: 115px; display: block; float: left; font-size: 13px; color: #333;">&nbsp;</label>

                                                        <asp:UpdatePanel ID="upNoticeUploadPopup" runat="server">
                                                            <ContentTemplate>
                                                                <asp:GridView runat="server" ID="grdCaseDocument" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                    GridLines="None" PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" Width="100%"
                                                                    OnRowCommand="grdCaseDocument_RowCommand" OnRowDataBound="grdCaseDocument_RowDataBound">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Document">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel runat="server" ID="aa1aa" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton
                                                                                            CommandArgument='<%# Eval("Id")%>' CommandName="DownloadNoticeDoc"
                                                                                            ID="lnkBtnDownLoadNoticeDoc" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                        </asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="lnkBtnDownLoadNoticeDoc" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Action">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel runat="server" ID="aa1naa" UpdateMode="Always">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton CommandArgument='<%# Eval("Id")%>'
                                                                                            AutoPostBack="true" CommandName="DeleteNoticeDoc"
                                                                                            OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                                            ID="lnkBtnDeleteNoticeDoc" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                                                        </asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="lnkBtnDeleteNoticeDoc" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <PagerTemplate>
                                                                        <table style="display: none">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </PagerTemplate>
                                                                    <EmptyDataTemplate>
                                                                        No Records Found.
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-12" style="text-align: center;">
                                    <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSave_Click"
                                        ValidationGroup="NoticePopUpValidationGroup" OnClientClick="validateForm();" />
                                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                </div>

                                <div class="form-group col-md-12" style="margin-left: 10px; float: left;">
                                    <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                </div>

                            </div>
                            <div class="clearfix"></div>

                        </ContentTemplate>
                        <%--<Triggers>
                            <asp:PostBackTrigger ControlID="btnSave" />
                        </Triggers>--%>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divAddNewActModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 40%;">
            <div class="modal-content">
                <div class="modal-header">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 180px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Add/Edit Act</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" aria-label="Close">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                            ValidationGroup="AddNewActModalValidationGroup" />
                        <asp:CustomValidator ID="cvNewActModal" runat="server" EnableClientScript="False"
                            ValidationGroup="AddNewActModalValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                    </div>

                    <div class="form-group col-md-12">
                        <label style="width: 3%; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 27%; display: block; float: left; font-size: 13px; color: #333;">
                            Act Name</label>
                        <asp:TextBox runat="server" ID="tbxNewActName" Style="width: 50%; float: left;" CssClass="form-control" />
                        <asp:RequiredFieldValidator ID="rfvNewActName" ErrorMessage="Required Act Name"
                            ControlToValidate="tbxNewActName" runat="server" ValidationGroup="AddNewActModalValidationGroup" Display="None" />
                    </div>
                    <div class="clearfix"></div>

                    <div class="form-group col-md-12" style="text-align: center; margin-top: 5%;">
                        <asp:Button Text="Save" runat="server" ID="btnSaveNewAct" CssClass="btn btn-primary"
                            ValidationGroup="AddNewActModalValidationGroup" OnClick="btnSaveNewAct_Click" />
                        <asp:Button Text="Close" runat="server" ID="btnCloseNewAct" CssClass="btn btn-primary" data-dismiss="modal" />
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
