﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="CaseListNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.CaseListNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

    <style type="text/css">
   
        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 1px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 180px !important;
              overflow-y: auto !important;
        }

        #grid1 .k-grid-content {
            min-height: 380px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        /*.k-auto-scrollable {
            overflow: hidden;
        }*/

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
            color: #666;
        }


        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
        .k-widget > span.k-invalid,
        input.k-invalid {
            border: 1px solid red !important;
        }

        .k-widget > span.k-invalid,
        textarea.k-invalid {
            border: 1px solid red !important;
        }

        .dropdown-validation-error {
            border: 1px solid red !important;
        }

        .k-window-titlebar.k-header {
            background: #00b5ef;
            color: white;
        }

        .datepickerRPA {
            width: 20%;
            margin-right: 18%;
            float: right;
        }

        .moving {
            -webkit-animation: spin 2s infinite linear;
            animation: spin 2s infinite linear;
        }

        .k-calendar td.k-state-selected .k-link, .k-calendar td.k-today.k-state-selected.k-state-hover .k-link {
            color: black;
        }

        hr {
            margin-top: 5px; 
            margin-bottom: 5px; 
            border-top: 2px solid #f7f7f7;
        }
               .k-window.k-dialog {
            position: fixed;
            padding-top: 0;
            width: 29%;
            overflow-y: auto;
        }

        .k-dialog .k-dialog-titlebar {
            position: relative;
            margin: 0;
            padding: .4em;
            width: auto;
            font-weight: 400;
            display: none;
        }

        .k-dialog .k-dialog-buttongroup.k-dialog-button-layout-stretched .k-button {
            padding: .4em;
        }

        .k-dialog.k-alert .k-content, .k-dialog.k-confirm .k-content {
            padding: 2.4em;
            font-weight: 600;
            font-size: 15px;
        }
        .k-dialog .k-dialog-titlebar .k-dialog-actions {
            color: black;
        }
        p1 {
            margin: 0 0 10px;
            font-size: 15px;
        }
        .k-widget.k-tooltip-validation {
            display: none !important;
        }
        .k-auto-scrollable k-grid-content {
            overflow-y: auto;
            overflow-x: scroll;
        }
        .k-multiselect-wrap .k-input {
            /*padding-top:6px;*/
            display: inherit !important;
        }
    </style>
    <%if(NewColumnsLitigation==false){%>
    <style type="text/css">

        .k-grid-content {
            min-height: 180px !important;
            overflow: hidden;
        }
                       
        </style>
     <%}%>

    <title></title>

    <script type="text/javascript">

        setTimeout(printSomething, 1000);

        function printSomething() {
            window.scrollTo(0, document.body.scrollHeight);
        }
    </script>

    <script type="text/javascript">

        function ReadQuerySt(e) {
            try {
                for (hu = window.location.search.substring(1), gy = hu.split('&'), i = 0; i < gy.length; i++)
                    if (ft = gy[i].split('='), ft[0].toLowerCase() == e.toLowerCase()) return ft[1];
                return ''
            } catch (t) {
                return ''
            }
        }

        $(document).ready(function () {
            $("input").attr("autocomplete", "off");
            bindFY();
            bindCalendarYear();
            BindStatus();
            BindType();
            //if (document.getElementById('CustomerId').value == "5")//89
            if (document.getElementById('CustomerId').value == "89" && ReadQuerySt('CT') == '') {
                $("#dropdownType").data("kendoDropDownList").value('2');
                $("#dropdownType1").data("kendoDropDownList").value('2');
            }
            BindLawyers();
            BindPeriod();
            BindCategory();
            BindOpponent();
            BindBranch();
            BindCourt();
            BindWinLoseType();
            BindDepartment();
            bindNoticeType();
            Bindgrid();
            BindgridPopup();

            var CourtDropDown = $("#dropdownCourtWise").data("kendoDropDownTree");
            if ($("#dropdownType1").val() == "1") {
                CourtDropDown.wrapper.hide();
            }
            else {
                CourtDropDown.wrapper.show();
            }

            $("#Startdatepicker").kendoDatePicker({
                change: onChange,
                format: "dd/MM/yyyy",
            });
            $("#Lastdatepicker").kendoDatePicker({
                change: onChange,
                format: "dd/MM/yyyy",
            });


            var ValidatordivCourtType = $("#divCourtType").kendoValidator({
                errorTemplate: "",
                validate: function (e) {
                    var dropDowns = $(".k-dropdown");
                    $.each(dropDowns, function (key, value) {
                        var input = $(value).find("input.k-invalid");
                        var span = $(this).find("span.k-dropdown-wrap");
                        if (input.size() > 0) {
                            $(span).addClass("dropdown-validation-error");
                        } else {
                            $(span).removeClass("dropdown-validation-error");
                        }
                    });
                },
            }).data("kendoValidator");

            var ValidatordivCommonFilters = $("#divCommonFilters").kendoValidator({
                errorTemplate: "",
                validate: function (e) {
                    var dropDowns = $(".k-dropdown");
                    $.each(dropDowns, function (key, value) {
                        var input = $(value).find("input.k-invalid");
                        var span = $(this).find("span.k-dropdown-wrap");
                        if (input.size() > 0) {
                            $(span).addClass("dropdown-validation-error");
                        } else {
                            $(span).removeClass("dropdown-validation-error");
                        }
                    });
                },
            }).data("kendoValidator");

            var ValidatordivDistrictCourt = $("#divDistrictCourt").kendoValidator({
                errorTemplate: "",
                validate: function (e) {
                    var dropDowns = $(".k-dropdown");
                    $.each(dropDowns, function (key, value) {
                        var input = $(value).find("input.k-invalid");
                        var span = $(this).find("span.k-dropdown-wrap");
                        if (input.size() > 0) {
                            $(span).addClass("dropdown-validation-error");
                        } else {
                            $(span).removeClass("dropdown-validation-error");
                        }
                    });
                },
            }).data("kendoValidator");

            var ValidatordivHghCourt = $("#divHghCourt").kendoValidator({
                errorTemplate: "",
                validate: function (e) {
                    var dropDowns = $(".k-dropdown");
                    $.each(dropDowns, function (key, value) {
                        var input = $(value).find("input.k-invalid");
                        var span = $(this).find("span.k-dropdown-wrap");
                        if (input.size() > 0) {
                            $(span).addClass("dropdown-validation-error");
                        } else {
                            $(span).removeClass("dropdown-validation-error");
                        }
                    });
                },
            }).data("kendoValidator");

            $('#btnSearch').click(function () {
                loopAgain = true;
                i = 0;

                $("#drpCaseState").attr("required", false);
                $("#drpDistrict").attr("required", false);
                $("#drpCourtNameList").attr("required", false);
                $("#drpCaseState").attr("required", false);
                $("#drpDistrict").attr("required", false);

                if ($("#drpCourtType").val() == "461") {
                    if (ValidatordivCommonFilters.validate()) {
                        $("#btnSearch").append("<span class='k-icon k-i-reload moving'>" + '</span>');
                        btnSearch_click(event);
                    }
                }
                else if ($("#drpCourtType").val() == "992") {
                    $("#drpHCCaseState").attr("required", true);
                    $("#drpHCCourtNameList").attr("required", true);
                    if (ValidatordivHghCourt.validate() && ValidatordivCommonFilters.validate()) {
                        $("#btnSearch").append("<span class='k-icon k-i-reload moving'>" + '</span>');
                        btnSearch_click(event);
                    }
                }
                else if ($("#drpCourtType").val() == "57") {

                    $("#drpCaseState").attr("required", true);
                    $("#drpDistrict").attr("required", true);
                    $("#drpCourtNameList").attr("required", true);

                    if (ValidatordivDistrictCourt.validate() && ValidatordivCommonFilters.validate()) {
                        $("#btnSearch").append("<span class='k-icon k-i-reload moving'>" + '</span>');
                        btnSearch_click(event);
                    }
                }
                else {
                    if (ValidatordivCourtType.validate()) {
                    }
                }

            });

        });

        function btnSkip_click(e) {

            $("#divRPAFilters").data("kendoWindow").close();
            ClearfilterRPA();

            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '550px');
            $('.modal-dialog').css('width', '100%');
            $('#showdetails').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=0");
            e.preventDefault();
        }

        function OpenAddNewCaseNotice(e) {

            if ($("#dropdownType").val() == "2") {

                if ("<%=RPACustomerEnable%>" == "True") {
                    BindCommonFilter();

                    $("#divRPAFilters").kendoWindow({
                        modal: true,
                        width: "65%",
                        height: "auto",
                        title: "Add New Case",
                        visible: false,
                        draggable: false,
                        refresh: true,
                        pinned: true,
                        actions: [
                            "Close"
                        ]

                    }).data("kendoWindow").open().center();
                    document.getElementById('divRPAFilters').style = "overflow:hidden;";
                }
                else {
                    $('#divShowDialog').modal('show');
                    $('#showdetails').attr('width', '100%');
                    $('#showdetails').attr('height', '550px');
                    $('.modal-dialog').css('width', '100%');
                    $('#showdetails').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=0");
                }
                e.preventDefault();
            }

            else if ($("#dropdownType").val() == '1') {

                $('#divShowDialog').modal('show');
                $('#showdetails').attr('width', '100%');
                $('#showdetails').attr('height', '550px');
                $('.modal-dialog').css('width', '100%');

                $('#showdetails').attr('src', "../../Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=0");
            }
            e.preventDefault();
            return false;
        }

        var dataString = "";

        function CloseRPADialog() {
            loopAgain = false;

            $.ajax({
                type: "POST",
                url: '<% =Path%>Litigation/Add_RPACaseStatus?' + dataString,
                data: dataString,
                cache: false,
                success: function (result) {
                },
                error: function (result) {
                    console.log(result);
                }
            });


            $('span.moving').remove();
            var dialog = $("#confirmbox").data("kendoDialog");
            setTimeout(function () {
                dialog.close();
            }, 1000);
        }

        var loopAgain = true;
        var i = 0;

        function btnSearch_click() {
            var dataString = "";
            if ($("#drpCourtType").val() == "461") {
                dataString = 'case_number=' + $("#txtCaseNo").val() + '&case_type=' + $("#drpCaseType").val() + '&case_year=' + $("#CaseYearCalender").val() + '&court_type=' + encodeURIComponent($("#drpCourtType").data("kendoDropDownList").text());
            }
            if ($("#drpCourtType").val() == "992") {
                dataString = 'case_number=' + $("#txtCaseNo").val() + '&case_type=' + $("#drpCaseType").val() + '&case_year=' + $("#CaseYearCalender").val() + '&court_type=' + encodeURIComponent($("#drpCourtType").data("kendoDropDownList").text()) + '&case_state=' + $("#drpHCCaseState").val() + '&court_id=' + $("#drpHCCourtNameList").val();
            }
            if ($("#drpCourtType").val() == "57") {
                dataString = 'case_number=' + $("#txtCaseNo").val() + '&case_type=' + $("#drpCaseType").val() + '&case_year=' + $("#CaseYearCalender").val() + '&court_type=' + encodeURIComponent($("#drpCourtType").data("kendoDropDownList").text()) + '&case_state=' + $("#drpCaseState").val() + '&court_id=' + $("#drpCourtNameList").val();
            }

            $.ajax({
                type: 'GET',
                url: 'https://cases-api.cap.avantisregtec.in/api/cases/?' + dataString,
                dataType: "json",
                beforeSend: function (request) {
                    request.setRequestHeader('Authorization', 'Token 1bf60b3ef56203c71f3cf432bdd1a2229080caaa');
                },
                success: function (result) {
                    var CourtType = encodeURIComponent($("#drpCourtType").data("kendoDropDownList").text());
                    var State = "";
                    var StateName = "";
                    if ($("#drpCourtType").val() == "992") {
                        State = $("#drpHCCaseState").val();
                        StateName = encodeURIComponent($("#drpHCCaseState").data("kendoDropDownList").text());
                    }
                    else if ($("#drpCourtType").val() == "57") {
                        State = $("#drpCaseState").val();
                        StateName = encodeURIComponent($("#drpCaseState").data("kendoDropDownList").text());
                    }
                    var CaseTypeID = $("#drpCaseType").val();
                    var CaseType = encodeURIComponent($("#drpCaseType").data("kendoDropDownList").text());
                    var CaseNo = $("#txtCaseNo").val();
                    var CaseYear = $("#CaseYearCalender").val();
                    var CourtName = "";
                    var Bench = "";
                    var BenchID = "";
                    var District = "";
                    if ($("#drpCourtType").val() == "992") {
                        CourtName = encodeURIComponent($("#drpHCCaseState").data("kendoDropDownList").text());
                        Bench = encodeURIComponent($("#drpHCCourtNameList").data("kendoDropDownList").text());
                        BenchID = encodeURIComponent($("#drpHCCourtNameList").val());
                    }
                    else if ($("#drpCourtType").val() == "57") {
                        CourtName = encodeURIComponent($("#drpCourtNameList").data("kendoDropDownList").text());
                        District = encodeURIComponent($("#drpDistrict").data("kendoDropDownList").text());
                        BenchID = encodeURIComponent($("#drpCourtNameList").val());
                    }
                    var CreatedBy = encodeURIComponent("<%=UserEmail%>");
                    var RPAStatus = result.data.data;


                    if (result == null || result == 'undefined' || result.message == "No matching case type found" || result.data.data == '') {
                        dataString = 'CourtType=' + CourtType + '&State=' + State + '&StateName=' + StateName + '&CaseTypeID=' + CaseTypeID +
                            '&CaseType=' + CaseType + '&CaseNo=' + CaseNo + '&CaseYear=' + CaseYear + '&CourtName=' + CourtName +
                            '&Bench=' + Bench + '&BenchID=' + BenchID + '&District=' + District + '&CreatedBy=' + CreatedBy + '&RPAStatus=' + RPAStatus + '&UserID=<%=UId%>&CustomerID=<%=custid%>';

                        var resultNew = "<p1>Please wait data is loading......  </p1><span class='k-icon k-i-reload moving'></span>";

                        if (loopAgain) {
                            setTimeout(function (e) { btnSearch_click(event); }, 3000);
                            if (i == 0) {
                                $('#confirmbox').kendoDialog({
                                    width: "430px",
                                    closable: false,
                                    title: "Notification",
                                    modal: true,
                                    content: resultNew,
                                    actions: [
                                        { text: 'Close and Inform on Email once fetched ', primary: true, action: CloseRPADialog }
                                    ]
                                }).data("kendoDialog").open();
                                $('#confirmbox').parent().find('.k-header').css('display', 'block');
                                $('#confirmbox').parent().find('.k-window-title').css('color', '#666');
                                $('#confirmbox').parent().find('.k-header').css('background-color', '#f3f3f4');
                                i++;
                            }
                        }
                    }
                    else {
                        $('span.moving').remove();
                        if (i > 0) {
                            CloseRPADialog();
                        }
                        var CaseRefNo = "";
                        var OpenDate = "";
                        var Section = "";
                        var CourtID = "";
                        var Act = "";
                        var State = "";
                        var Judge = "";
                        var casetitle = "";
                        var Description = "";
                        var InternalCaseNo = "";
                        var case_typeID = 0;
                        var CaseStage = "";
                        var CaseCloseDate = "";
                        var RPAData = [];

                        var RPAHCData = [];
                        var RPAHCResultData = [];

                        if ($("#drpCourtType").val() == "461") {
                            // $('span.moving').remove();

                            for (var idx = 0; idx < result.data.data.length; idx++) {
                                if (result.data.data[idx].data_type == "case-details") {

                                    for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {

                                        if (result.data.data[idx].data[idx1]["\ufeffColumn-0"].search("Status/Stage") != -1) {
                                            var CaseStatus = result.data.data[idx].data[idx1]["Column-1"];

                                            if (CaseStatus.search("DISPOSED") != -1) {
                                                CaseStatus = "3";
                                                CaseStage = "Disposed";
                                                var CalcalateDate = result.data.data[idx].data[idx1]["Column-1"].search("Date:");
                                                if (CalcalateDate != -1) {
                                                    CaseCloseDate = result.data.data[idx].data[idx1]["Column-1"].slice(CalcalateDate + 6);
                                                    CaseCloseDate = CaseCloseDate.slice(0, 10);
                                                }
                                            }
                                            else {
                                                CaseStatus = "1";
                                                CaseStage = "Hearing";
                                            }

                                            RPAHCResultData.push({
                                                CaseStatus: encodeURIComponent(CaseStatus),
                                                CaseStage: CaseStage,
                                                CaseResult: "",
                                                CaseCloseDate: CaseCloseDate,
                                            });
                                        }

                                        if (result.data.data[idx].data[idx1]["\ufeffColumn-0"] == "Category") {

                                            Description = result.data.data[idx].data[idx1]["Column-1"];
                                        }
                                        if (result.data.data[idx].data[idx1]["\ufeffColumn-0"] == "Act") {
                                            Act = result.data.data[idx].data[idx1]["Column-1"];
                                        }
                                        if (result.data.data[idx].data[idx1]["\ufeffColumn-0"] == "Diary No.") {

                                            var SectionIndex = result.data.data[idx].data[idx1]["Column-1"].search("SECTION");
                                            Section = result.data.data[idx].data[idx1]["Column-1"].slice(SectionIndex - 1);
                                            var CalcalateDate = result.data.data[idx].data[idx1]["Column-1"].search("Filed on");
                                            var CalcalateDate1 = result.data.data[idx].data[idx1]["Column-1"].slice(CalcalateDate + 9);
                                            var SearchSection = CalcalateDate1.search("PENDING");
                                            var SearchSection1 = CalcalateDate1.search("DISPOSED");
                                            if (SearchSection != -1) {
                                                OpenDate = CalcalateDate1.slice(0, SearchSection - 9);
                                            }
                                            else {
                                                OpenDate = CalcalateDate1.slice(0, SearchSection1 - 9);
                                            }
                                        }

                                    }
                                }
                                if (result.data.data[idx].data_type == "case-name") {

                                    casetitle = result.data.data[idx].data[0]["Column2"];
                                }
                            }
                            RPAData.push({
                                'CaseYear': CaseYear,
                                'CaseRefNo': $("#txtCaseNo").val(),
                                'Section': encodeURIComponent(Section),
                                'CourtID': $("#drpCourtType").val(),
                                'OpenDate': encodeURIComponent(OpenDate),
                                'case_type': encodeURIComponent($("#drpCaseType").data("kendoDropDownList").text()),
                                'State': '',
                                'Judge': '',
                                'casetitle': encodeURIComponent(casetitle),
                                'Act': encodeURIComponent(Act),
                                'Description': encodeURIComponent(Description),
                                'InternalCaseNo': encodeURIComponent(InternalCaseNo),
                                'case_typeID': $("#drpCaseType").val(),
                                'BenchID': BenchID
                            });

                            var dialog = $("#divRPAFilters").data("kendoWindow");
                            setTimeout(function () {
                                dialog.close();
                                ClearfilterRPA();
                            }, 1000);
                        }

                        if ($("#drpCourtType").val() == "992") {
                            $('span.moving').remove();
                            for (var idx = 0; idx < result.data.data.length; idx++) {

                                if (result.data.data[idx].data_type == "case-details") {

                                    for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {


                                        if (result.data.data[idx].data[idx1]["Column-2"] == "Filing Date") {
                                            OpenDate = result.data.data[idx].data[idx1]["Column-3"];
                                        }

                                        if (result.data.data[idx].data[idx1]["\ufeffColumn-0"] == "Filing Number") {
                                            InternalCaseNo = result.data.data[idx].data[idx1]["Column-1"];
                                        }
                                    }

                                }
                                if (result.data.data[idx].data_type == "acts") {
                                    Section = result.data.data[idx].data[0]["Under Section(s)"];
                                    Act = result.data.data[idx].data[0]["\ufeffUnder Act(s)"];
                                }

                                if (result.data.data[idx].data_type == "case-status") {

                                    for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {

                                        if (result.data.data[idx].data[idx1]["\ufeffColumn-0"] == "Coram") {

                                            Judge = result.data.data[idx].data[idx1]["Column-1"];
                                        }

                                        if (result.data.data[idx].data[idx1]["\ufeffColumn-0"].search("Case Status") != -1) {

                                            var CaseStatus = result.data.data[idx].data[idx1]["Column-1"];

                                            if (CaseStatus.search("DISPOSED") != -1) {
                                                CaseStatus = "3";
                                                CaseStage = "Disposed";
                                            }
                                            else {
                                                CaseStatus = "1";
                                                CaseStage = "Hearing";
                                            }

                                            RPAHCResultData.push({
                                                CaseStatus: encodeURIComponent(CaseStatus),
                                                CaseStage: encodeURIComponent(CaseStage),
                                                CaseResult: "",
                                                CaseCloseDate: CaseCloseDate,
                                            });
                                        }
                                    }
                                }

                                if (result.data.data[idx].data_type == "case-history") {
                                    for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {
                                        RPAHCData.push({
                                            remarks: encodeURIComponent(result.data.data[idx].data[idx1]["Judge"]),
                                            hearingDate: encodeURIComponent(result.data.data[idx].data[idx1]["Business On Date"]),
                                            nextDateHearing: encodeURIComponent(result.data.data[idx].data[idx1]["Hearing Date"]),
                                            description: encodeURIComponent(result.data.data[idx].data[idx1]["Purpose of hearing"]),
                                        });
                                    }
                                }
                            }
                            RPAData.push({
                                'CaseYear': CaseYear,
                                'CaseRefNo': $("#txtCaseNo").val(),
                                'Section': encodeURIComponent(Section),
                                'CourtID': $("#drpCourtType").val(),
                                'OpenDate': encodeURIComponent(OpenDate),
                                'case_type': encodeURIComponent($("#drpCaseType").data("kendoDropDownList").text()),
                                'State': $("#drpHCCaseState").val(),
                                'Judge': encodeURIComponent(Judge),
                                'casetitle': '',
                                'Act': Act,
                                'Description': encodeURIComponent(Description),
                                'InternalCaseNo': encodeURIComponent(InternalCaseNo),
                                'case_typeID': $("#drpCaseType").val(),
                                'BenchID': BenchID
                            });

                            var dialog = $("#divRPAFilters").data("kendoWindow");
                            setTimeout(function () {
                                dialog.close();
                                ClearfilterRPA();
                            }, 1000);
                        }

                        if ($("#drpCourtType").val() == "57") {
                            $('span.moving').remove();
                            for (var idx = 0; idx < result.data.data.length; idx++) {

                                if (result.data.data[idx].data_type == "case-details") {

                                    for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {

                                        if (result.data.data[idx].data[idx1]["Column-2"] == "Filing Date") {
                                            OpenDate = result.data.data[idx].data[idx1]["Column-3"];
                                        }

                                        if (result.data.data[idx].data[idx1]["\ufeffColumn-0"] == "Filing Number") {
                                            InternalCaseNo = result.data.data[idx].data[idx1]["Column-1"];
                                        }
                                    }
                                }
                                if (result.data.data[idx].data_type == "acts") {
                                    Section = result.data.data[idx].data[0]["Under Section(s)"];
                                    Act = result.data.data[idx].data[0]["\ufeffUnder Act(s)"];
                                }

                                if (result.data.data[idx].data_type == "case-status") {
                                    var CaseStatus = 1;
                                    for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {

                                        if (result.data.data[idx].data[idx1]["\ufeffColumn-0"].search("Judge") != -1) {

                                            Judge = result.data.data[idx].data[idx1]["Column-1"];
                                        }

                                        if (result.data.data[idx].data[idx1]["\ufeffColumn-0"].search("Case Stage") != -1) {

                                            CaseStage = result.data.data[idx].data[idx1]["Column-1"];
                                        }

                                        if (result.data.data[idx].data[idx1]["\ufeffColumn-0"].search("Case Status") != -1) {

                                            CaseStatus = result.data.data[idx].data[idx1]["Column-1"];

                                            if (CaseStatus.search("DISPOSED") != -1) {
                                                CaseStatus = "3";
                                            }
                                            else {
                                                CaseStatus = "1";
                                            }
                                        }
                                    }
                                    RPAHCResultData.push({
                                        CaseStatus: encodeURIComponent(CaseStatus),
                                        CaseStage: CaseStage,
                                        CaseResult: "",
                                        CaseCloseDate: CaseCloseDate,
                                    });
                                }

                                if (result.data.data[idx].data_type == "case-history") {

                                    for (var idx1 = 0; idx1 < result.data.data[idx].data.length; idx1++) {
                                        RPAHCData.push({
                                            remarks: encodeURIComponent(result.data.data[idx].data[idx1]["﻿Judge"]),
                                            hearingDate: encodeURIComponent(result.data.data[idx].data[idx1]["Business on Date"]),
                                            nextDateHearing: encodeURIComponent(result.data.data[idx].data[idx1]["Hearing Date"]),
                                            description: encodeURIComponent(result.data.data[idx].data[idx1]["Purpose of Hearing"]),
                                        });
                                    }
                                }
                            }
                            RPAData.push({
                                'CaseYear': CaseYear,
                                'CaseRefNo': $("#txtCaseNo").val(),
                                'Section': encodeURIComponent(Section),
                                'CourtID': $("#drpCourtType").val(),
                                'OpenDate': encodeURIComponent(OpenDate),
                                'case_type': encodeURIComponent($("#drpCaseType").data("kendoDropDownList").text()),
                                'State': $("#drpCaseState").val(),
                                'Judge': encodeURIComponent(Judge),
                                'casetitle': '',
                                'Act': Act,
                                'Description': encodeURIComponent(Description),
                                'InternalCaseNo': encodeURIComponent(InternalCaseNo),
                                'case_typeID': $("#drpCaseType").val(),
                                'BenchID': BenchID
                            });

                            $("#divRPAFilters").data("kendoWindow").close();
                            ClearfilterRPA();
                            //setTimeout(function () {
                            //setTimeout(function () {

                            //}, 1000);
                        }

                        $('#divShowDialog').modal('show');
                        $('#showdetails').attr('width', '100%');
                        $('#showdetails').attr('height', '550px');
                        $('.modal-dialog').css('width', '100%');
                        if (RPAData != []) {
                            $('#showdetails').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=0&RPAResult=" + JSON.stringify(RPAData) + '&RPAHCData=' + JSON.stringify(RPAHCData) + '&RPAHCResultData=' + JSON.stringify(RPAHCResultData));
                        }
                    }

                },
                error: function (result) {
                    console.log(result);
                }
            });
        }

        function BindCommonFilter() {

            $("#drpCaseType").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                filter: "contains",
                dataTextField: "name",
                dataValueField: "id",
                optionLabel: "Select Case Type",
                change: function (e) {
                    var drpCaseType = $('#drpCaseType').data("kendoDropDownList");
                    if (drpCaseType.value() != "") {
                        drpCaseType.wrapper.find("span.k-dropdown-wrap").removeClass("dropdown-validation-error");
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: 'https://cases-api.cap.avantisregtec.in/api/cases/types/',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', 'Token 1bf60b3ef56203c71f3cf432bdd1a2229080caaa');
                            },
                        }
                    },
                    schema: {
                        data: "data"
                    },
                }
            });

            $("#CaseYearCalender").kendoDatePicker({
                start: "decade",
                depth: "decade",
                format: "yyyy",
            });

            $("#drpCourtType").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: "Select",
                change: function (e) {
                    var drpCourtType = $('#drpCourtType').data("kendoDropDownList");
                    if (drpCourtType.value() != "") {
                        drpCourtType.wrapper.find("span.k-dropdown-wrap").removeClass("dropdown-validation-error");
                    }

                    if ($("#drpCourtType").val() == "57") {
                        document.getElementById('divDistrictCourt').style = "display:block;"
                    }
                    else {
                        $("#drpCaseState").data("kendoDropDownList").select(0);
                        $("#drpDistrict").data("kendoDropDownList").select(0);
                        $("#drpCourtNameList").data("kendoDropDownList").select(0);
                        document.getElementById('divDistrictCourt').style = "display:none;"
                    }
                    if ($("#drpCourtType").val() == "992") {
                        document.getElementById('divHghCourt').style = "display:block;"
                    }
                    else {
                        //$("#drpCaseState1").data("kendoDropDownList").select(0);
                        document.getElementById('divHghCourt').style = "display:none;"
                    }
                },
                dataSource: [
                    { text: "Supreme Court", value: "461" },
                    { text: "High Court", value: "992" },
                    { text: "District Court", value: "57" },
                ]
            });

            BindDistrict();

            $("#drpCaseState").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "name",
                dataValueField: "id",
                optionLabel: "Select State",
                filter: "contains",
                change: function (e) {
                    var drpCaseState = $('#drpCaseState').data("kendoDropDownList");
                    if (drpCaseState.value() != "") {
                        drpCaseState.wrapper.find("span.k-dropdown-wrap").removeClass("dropdown-validation-error");
                    }
                    BindDistrict();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: 'https://cases-api.cap.avantisregtec.in/api/states/',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', 'Token 1bf60b3ef56203c71f3cf432bdd1a2229080caaa');
                            },
                        }
                    },
                    schema: {
                        data: "data"
                    },
                }
            });

            BindCourtList();

            $("#drpHCCaseState").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "name",
                dataValueField: "state",
                //template: '<span style="width:10px" class="k-state-default">High Court of </span>' + " " +
                //    '<span class="k-state-default">#: data.name#</span>',
                //valueTemplate: '<span class="selected-value">High court of </span>' + " " + '<span class="selected-value">#: data.name#</span>',
                optionLabel: "Select High Court",
                filter: "contains",
                change: function (e) {
                    var drpHCCaseState = $('#drpHCCaseState').data("kendoDropDownList");
                    if (drpHCCaseState.value() != "") {
                        drpHCCaseState.wrapper.find("span.k-dropdown-wrap").removeClass("dropdown-validation-error");
                    }
                    BindHCList();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: 'https://cases-api.cap.avantisregtec.in/api/courts/?type=High%20Court',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', 'Token 1bf60b3ef56203c71f3cf432bdd1a2229080caaa');
                            },
                        }
                    },
                    schema: {
                        data: "data"
                    },
                }
            });
            BindHCList();

        }

        function BindHCList() {
            $("#drpHCCourtNameList").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "bench",
                dataValueField: "id",
                optionLabel: "Select Bench",
                change: function (e) {
                    var drpHCCourtNameList = $('#drpHCCourtNameList').data("kendoDropDownList");
                    if (drpHCCourtNameList.value() != "") {
                        drpHCCourtNameList.wrapper.find("span.k-dropdown-wrap").removeClass("dropdown-validation-error");
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: 'https://cases-api.cap.avantisregtec.in/api/courts/?type=high%20court&name=' + $("#drpHCCaseState").data("kendoDropDownList").text() + '&state=' + $("#drpHCCaseState").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', 'Token 1bf60b3ef56203c71f3cf432bdd1a2229080caaa');
                            },
                        }
                    },
                    schema: {
                        data: "data"
                    },
                }
            });
        }

        function BindCourtList() {
            $("#drpCourtNameList").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "name",
                dataValueField: "id",
                optionLabel: "Select Court Complex",
                filter: "contains",
                change: function (e) {
                    var drpCourtNameList = $('#drpCourtNameList').data("kendoDropDownList");
                    if (drpCourtNameList.value() != "") {
                        drpCourtNameList.wrapper.find("span.k-dropdown-wrap").removeClass("dropdown-validation-error");
                    }
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: 'https://cases-api.cap.avantisregtec.in/api/courts/?type=district%20court&state=' + $("#drpCaseState").val() + '&district=' + $("#drpDistrict").val(),
                            //url: 'https://cases-api.cap.avantisregtec.in/api/courts/?state=' + $("#drpCaseState").val() + '&district=' + $("#drpDistrict").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', 'Token 1bf60b3ef56203c71f3cf432bdd1a2229080caaa');
                            },
                        }
                    },
                    schema: {
                        data: "data"
                    },
                }
            });
        }

        function BindDistrict() {
            $("#drpDistrict").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "name",
                dataValueField: "id",
                optionLabel: "Select District",
                filter: "contains",
                change: function (e) {
                    var drpDistrict = $('#drpDistrict').data("kendoDropDownList");
                    if (drpDistrict.value() != "") {
                        drpDistrict.wrapper.find("span.k-dropdown-wrap").removeClass("dropdown-validation-error");
                    }
                    BindCourtList();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: 'https://cases-api.cap.avantisregtec.in/api/districts/?state=' + $("#drpCaseState").val(),
                            // url: 'https://cases-api.cap.avantisregtec.in/api/courts/?type=district%20court&state=' + $("#drpCaseState").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', 'Token 1bf60b3ef56203c71f3cf432bdd1a2229080caaa');
                            },
                        }
                    },
                    schema: {
                        data: "data"
                    },
                }
            });
        }

        function ClearfilterRPA() {
            $("#drpCaseType").data("kendoDropDownList").select(0);
            $("#drpCourtType").data("kendoDropDownList").select(0);
            $("#drpCourtType").data("kendoDropDownList").trigger("change");
            $("#drpCaseState").data("kendoDropDownList").select(0);
            $("#CaseYearCalender").data("kendoDatePicker").value(null);
            $("#drpHCCourtNameList").data("kendoDropDownList").select(0);
            $("#drpHCCaseState").data("kendoDropDownList").select(0);
            document.getElementById('txtCaseNo').value = '';
            document.getElementById('lblCaseStatusRPA').style = "display:none;";
        }

        function BindLawyers() {
            $("#dropdownLawyers").kendoDropDownTree({
                placeholder: "Opposition Lawyer",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "LawyerName",
                dataValueField: "LawyertID",
                change: function () {
                    FilterAllAdvanced();
                    fCreateStoryBoard1('dropdownLawyers', 'filtersstoryboardLawyer', 'Lawyer');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoLawyerList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/KendoLawyerList?CustId=<% =CustId%>"
                    },
                }
            });
        }

        function onChange() {
            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != undefined && $("#Startdatepicker").val() != "") {
                $("#dropdownFY1").data("kendoDropDownList").select(0);
                $("#dropdownPastData1").data("kendoDropDownList").select(4);
            }
            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != undefined && $("#Lastdatepicker").val() != "") {
                $("#dropdownFY1").data("kendoDropDownList").select(0);
                $("#dropdownPastData1").data("kendoDropDownList").select(4);

            }

        }

        function BindWinLoseType() {
            $("#dropdownWinLose").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "TypeName",
                dataValueField: "ID",
                optionLabel: "Result",
                change: function (e) {
                    //Bindgrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/TypeList?CutomerID=<% =CustId%>&type=' + $("#dropdownType1").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/TypeList?CutomerID=<% =CustId%>"
                    },
                }
            });

        }

        function bindNoticeType() {
            $("#NoticeType").kendoDropDownTree({
                placeholder: "Type",
                autoClose: true,
                autoWidth: true,
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    FilterAll();
                    fCreateStoryBoard('NoticeType', 'filtersstoryboardNoticeType', 'Type');
                },
                dataSource: [
                    { text: "Inward/Defendant", value: "I" },//Inward/Defendant
                    { text: "Outward/Plaintiff", value: "O" }//Outward/Plaintiff
                ]
            });
            $("#NoticeType1").kendoDropDownTree({
                placeholder: "Type",
                autoClose: true,
                autoWidth: true,
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    FilterAllAdvanced();
                    fCreateStoryBoard1('NoticeType1', 'filtersstoryboardNoticeType1', 'Type1');
                },
                dataSource: [
                    { text: "Inward/Defendant", value: "I" },//Inward/Defendant
                    { text: "Outward/Plaintiff", value: "O" }//Outward/Plaintiff
                ]
            });
        }

        function BindDepartment() {
            $("#dropdownDept").kendoDropDownTree({
                placeholder: "Department",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "DepartmentName",
                dataValueField: "DepartmentID",
                change: function () {
                    FilterAll();
                    fCreateStoryBoard('dropdownDept', 'filtersstoryboardDept', 'Dept');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoDeptList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/KendoDeptList?CustId=<% =CustId%>"
                    },
                }
            });

            $("#dropdownDept1").kendoDropDownTree({
                placeholder: "Department",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "DepartmentName",
                dataValueField: "DepartmentID",
                change: function () {
                    FilterAllAdvanced();
                    fCreateStoryBoard1('dropdownDept1', 'filtersstoryboardDept1', 'Dept1');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoDeptList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/KendoDeptList?CustId=<% =CustId%>"
                    },
                }
            });
        }

        function BindBranch() {
            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //checkAll: true,                
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterAll();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            <%--url: '<% =Path%>Litigation/GetAssignedEntitiesLocationList?customerId=<% =CustId%>&UserID=<% =UId%>&FlagIsApp=<% =FlagIsApp%>',--%>
                            url: '<% =Path%>Litigation/GetAssignedLocationList?customerId=<% =CustId%>&UserID=<% =UId%>&FlagIsApp=<% =FlagIsApp%>',
                            //url: '<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

            $("#dropdowntree1").kendoDropDownTree({
                placeholder: "Entity/Sub Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //checkAll: true,                
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterAllAdvanced();
                    fCreateStoryBoard1('dropdowntree1', 'filtersstoryboard1', 'loc1');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                           <%-- url: '<% =Path%>Litigation/GetAssignedEntitiesLocationList?customerId=<% =CustId%>&UserID=<% =UId%>&FlagIsApp=<% =FlagIsApp%>',--%>
                            url: '<% =Path%>Litigation/GetAssignedLocationList?customerId=<% =CustId%>&UserID=<% =UId%>&FlagIsApp=<% =FlagIsApp%>',
                            //url: '<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
        }

        function BindStatus() {
            $("#dropdownStatus").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {

                },
                dataSource: [
                    { text: "Status", value: "-1" },
                    { text: "Pending/Open", value: "1" },
                    { text: "Disposed/Closed", value: "3" },
                    <%if (RPACustomerEnable){%>
                    { text: "Draft RPA", value: "4" },
                    <%}%>
                ]
            });

            $("#dropdownStatus1").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    //BindgridPopup();
                },
                dataSource: [
                    { text: "Status", value: "-1" },
                    { text: "Pending/Open", value: "1" },
                    { text: "Disposed/Closed", value: "3" },
                    <%if (RPACustomerEnable){%>
                    { text: "Draft RPA", value: "4" },
                    <%}%>
                ]
            });
            $("#dropdownStatus").data("kendoDropDownList").value(<% =StatusFlagID%>);

            $("#dropdownStatus1").data("kendoDropDownList").value(<% =StatusFlagID%>);
        }

        function BindType() {
            $("#dropdownType").kendoDropDownList({
                autoClose: true,
               // placeholder: "Litigation Type",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    if ($("#dropdownType").val() == "3") {
                        window.location.href = "../aspxPages/TaskListNew.aspx";
                    }
                    if ($("#dropdownType").val() == "4") {
                        window.location.href = "../aspxPages/RPACaseStatus.aspx";
                    }
                    else {
                        Bindgrid();
                    }
                },
                dataSource: [
                    { text: "Notice", value: "1" },
                    { text: "Case", value: "2" },
                    { text: "Task", value: "3" },
                ]
            });

            $("#dropdownType1").kendoDropDownList({
                autoClose: true,
                //placeholder: "Litigation Type",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    var CourtDropDown = $("#dropdownCourtWise").data("kendoDropDownTree");
                    if ($("#dropdownType1").val() == "1") {
                        CourtDropDown.wrapper.hide();
                    }
                    else {
                        CourtDropDown.wrapper.show();
                    }
                    if ($("#dropdownType1").val() == "3") {
                        window.location.href = "../aspxPages/TaskListNew.aspx";
                    }
                    else {
                        BindgridPopup();
                        BindWinLoseType();
                    }
                },
                dataSource: [
                    { text: "Notice", value: "1" },
                    { text: "Case", value: "2" },
                    { text: "Task", value: "3" },
                ]
            });
            $("#dropdownType").data("kendoDropDownList").value('<% =CNTtype%>');

            $("#dropdownType1").data("kendoDropDownList").value('<% =CNTtype%>');
        }

        function BindOpponent() {
            $("#dropdownOpponent").kendoDropDownTree({
                placeholder: "Opponent",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                change: function () {
                    FilterAll();
                    fCreateStoryBoard1('dropdownOpponent', 'filtersstoryboardOpponent', 'Opponent');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/LegalCaseParty?CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                }
            });
        }

        function BindCourt() {
            $("#dropdownCourtWise").kendoDropDownTree({
                placeholder: "Court",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoWidth: false,
                autoClose: false,
                checkAllTemplate: "Select All",
                dataTextField: "CourtName",
                dataValueField: "ID",
                change: function (e) {
                    FilterAllAdvanced();
                    fCreateStoryBoard1('dropdownCourtWise', 'filtersstoryCourtboard', 'Court');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoCourtList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/KendoCourtList?CustId=<% =CustId%>"
                    }
                }
            });
        }

        function BindCategory() {
            $("#dropdownCategory").kendoDropDownTree({
                placeholder: "Category of Cases",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoWidth: false,
                checkAllTemplate: "Select All",
                dataTextField: "CaseType",
                //dataValueField: "ID",
                dataValueField: "CaseType",
                autoClose: false,
                change: function (e) {
                    FilterAll();
                    fCreateStoryBoard('dropdownCategory', 'filtersstoryCategoryboard', 'Category');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoCategoryList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/KendoCategoryList?CustId=<% =CustId%>"
                    }
                }
            });

            $("#dropdownCategory1").kendoDropDownTree({
                placeholder: "Category of Cases",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoWidth: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                dataTextField: "CaseType",
                //dataValueField: "ID",
                dataValueField: "CaseType",
                change: function (e) {
                    FilterAllAdvanced();
                    fCreateStoryBoard1('dropdownCategory1', 'filtersstoryCategoryboard1', 'Category1');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/KendoCategoryList?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/KendoCategoryList?CustId=<% =CustId%>"
                    }
                }
            });

        }

        function BindPeriod() {

            $("#dropdownPastData1").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    // BindgridPopup();
                    $("#dropdownFY1").data("kendoDropDownList").select(0);
                    $('#Startdatepicker').val('');
                    $('#Lastdatepicker').val('');
                },
                index: 4,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });
        }

        function bindCalendarYear() {
            $("#dropdownCalYear").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    if ($("#dropdownCalYear").val() != 0) {
                        $("#dropdownFY").data("kendoDropDownList").select(0);
                    }
                },
                dataSource: [
                    { text: "Calendar Year", value: "0" },
                    { text: "2021", value: "2021" },
                    { text: "2020", value: "2020" },
                    { text: "2019", value: "2019" },
                    { text: "2018", value: "2018" },
                    { text: "2017", value: "2017" },
                    { text: "2016", value: "2016" },
                    { text: "2015", value: "2015" },
                    { text: "2014", value: "2014" },
                    { text: "2013", value: "2013" },
                    { text: "2012", value: "2012" }

                ]
            });

            $("#dropdownCalYear1").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    if ($("#dropdownCalYear1").val() != 0) {
                        $("#dropdownFY1").data("kendoDropDownList").select(0);
                    }
                },
                dataSource: [
                    { text: "Calendar Year", value: "0" },
                    { text: "2021", value: "2021" },
                    { text: "2020", value: "2020" },
                    { text: "2019", value: "2019" },
                    { text: "2018", value: "2018" },
                    { text: "2017", value: "2017" },
                    { text: "2016", value: "2016" },
                    { text: "2015", value: "2015" },
                    { text: "2014", value: "2014" },
                    { text: "2013", value: "2013" },
                    { text: "2012", value: "2012" }

                ]
            });
        }

        function bindFY() {
            $("#dropdownFY").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    $("#dropdownCalYear").data("kendoDropDownList").select(0);
                },
                dataSource: [
                    
                    <%if (FYCustID == true)
                    {%>
                    { text: "Disputed Financial Year", value: "0" },
                    <%}%>
                    <%else
                     {%>
                     { text: "Financial Year", value: "0" },
                    <%}%>
                 
                    { text: "2020-2021", value: "2020-2021" },
                    { text: "2019-2020", value: "2019-2020" },
                    { text: "2018-2019", value: "2018-2019" },
                    { text: "2017-2018", value: "2017-2018" },
                    { text: "2016-2017", value: "2016-2017" },
                    { text: "2015-2016", value: "2015-2016" },
                    { text: "2014-2015", value: "2014-2015" },
                    { text: "2013-2014", value: "2013-2014" },
                    { text: "2012-2013", value: "2012-2013" }

                ]
            });

            $("#dropdownFY1").kendoDropDownList({
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    if ($("#dropdownFY1").val() != 0) {
                        $("#dropdownPastData1").data("kendoDropDownList").select(4);
                        $('#Startdatepicker').val('');
                        $('#Lastdatepicker').val('');
                        $("#dropdownCalYear1").data("kendoDropDownList").select(0);
                    }
                },
                dataSource: [
                    <%if (FYCustID == true)
                    {%>
                    { text: "Disputed Financial Year", value: "0" },
                    <%}%>
                    <%else
                     {%>
                     { text: "Financial Year", value: "0" },
                    <%}%>
                  //  { text: "Financial Year", value: "0" },
                    { text: "2020-2021", value: "2020-2021" },
                    { text: "2019-2020", value: "2019-2020" },
                    { text: "2018-2019", value: "2018-2019" },
                    { text: "2017-2018", value: "2017-2018" },
                    { text: "2016-2017", value: "2016-2017" },
                    { text: "2015-2016", value: "2015-2016" },
                    { text: "2014-2015", value: "2014-2015" },
                    { text: "2013-2014", value: "2013-2014" },
                    { text: "2012-2013", value: "2012-2013" }

                ]
            });
        }

        function fCreateStoryBoard(Id, div, filtername) {
            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Location
            }
            if (div == 'filtersstoryboardDept') {
                $('#' + div).append('Department&nbsp;&nbsp;&nbsp;:&nbsp;');//Department
            }
            if (div == 'filtersstoryboardNoticeType') {
                $('#' + div).append('Party Type &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Notice Type
            }

            if (div == 'filtersstoryCategoryboard') {
                $('#' + div).append('Category&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Category Type
            }
            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                //if (buttontest.length > 10) {
                //    buttontest = buttontest.substring(0, 10).concat("...");
                //}
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB;height: 20px;Color:Gray;margin-left:5px;margin-bottom: 4px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="Clear" aria-label="Clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" title="Clear" aria-label="Clear" style="font-size: 12px;"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
        }

        function fcloseStory(obj) {
            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoard('dropdownDept', 'filtersstoryboardDept', 'Dept');
            fCreateStoryBoard('NoticeType', 'filtersstoryboardNoticeType', 'Type');
            fCreateStoryBoard('dropdownCategory', 'filtersstoryCategoryboard', 'Category');
        };

        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownDept").data("kendoDropDownTree").value([]);
            $("#NoticeType").data("kendoDropDownTree").value([]);
            $("#dropdownCategory").data("kendoDropDownTree").value([]);
            $("#dropdownFY").data("kendoDropDownList").select(0);
            $("#dropdownCalYear").data("kendoDropDownList").select(0);
            $("#dropdownStatus").data("kendoDropDownList").select(0);
            $("#grid").data("kendoGrid").dataSource.filter({});
            Bindgrid();
            e.preventDefault();
        }

        function fCreateStoryBoard1(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard1') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Location
            }
            if (div == 'filtersstoryboardDept1') {
                $('#' + div).append('Department&nbsp;&nbsp;&nbsp;:&nbsp');//Department
            }
            if (div == 'filtersstoryboardNoticeType1') {
                $('#' + div).append('Party Type &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Notice Type
            }
            if (div == 'filtersstoryCategoryboard1') {
                $('#' + div).append('Category&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Category Type
            }
            if (div == 'filtersstoryCourtboard') {
                $('#' + div).append('Court &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Court
            }
            if (div == 'filtersstoryboardOpponent') {
                $('#' + div).append('Opponent &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//opponent
            }
            if (div == 'filtersstoryboardLawyer') {
                $('#' + div).append('Lawyer &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;');//Lawyer
            }
            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB;height: 20px;Color:Gray;margin-left:5px;margin-bottom: 4px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory1(this)" title="Clear" aria-label="Clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory1(this)"  class="k-icon k-i-close" title="Clear" aria-label="Clear" style="font-size: 12px;"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
        }

        function fcloseStory1(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard1('dropdowntree1', 'filtersstoryboard1', 'loc1');
            fCreateStoryBoard1('dropdownDept1', 'filtersstoryboardDept1', 'Dept1');
            fCreateStoryBoard1('NoticeType1', 'filtersstoryboardNoticeType1', 'Type1');
            fCreateStoryBoard1('dropdownCategory1', 'filtersstoryCategoryboard1', 'Category1');
            fCreateStoryBoard1('dropdownCourtWise', 'filtersstoryCourtboard', 'Court');
            fCreateStoryBoard1('dropdownOpponent', 'filtersstoryboardOpponent', 'Opponent');
            fCreateStoryBoard1('dropdownLawyers', 'filtersstoryboardLawyer', 'Lawyer');
        };

        function ClearAllFilter() {
            $("#dropdowntree1").data("kendoDropDownTree").value([]);
            $("#dropdownDept1").data("kendoDropDownTree").value([]);
            $("#NoticeType1").data("kendoDropDownTree").value([]);
            $("#dropdownCategory1").data("kendoDropDownTree").value([]);
            $("#dropdownCourtWise").data("kendoDropDownTree").value([]);
            $("#dropdownOpponent").data("kendoDropDownTree").value([]);
            $("#dropdownLawyers").data("kendoDropDownTree").value([]);
            $("#dropdownWinLose").data("kendoDropDownList").select(0);
            $("#dropdownPastData1").data("kendoDropDownList").select(4);
            $("#dropdownFY1").data("kendoDropDownList").select(0);
            $("#dropdownCalYear1").data("kendoDropDownList").select(0);
            document.getElementById('txtSearch').value = '';
            $('#Startdatepicker').val('');
            $('#Lastdatepicker').val('');
            BindgridPopup();
        }

        function FilterAllAdvanced() {
            var locationlist = $("#dropdowntree1").data("kendoDropDownTree")._values;

            //Department details
            var Departmentlist = $("#dropdownDept1").data("kendoDropDownTree")._values;

            var NoticeTypedetails = $("#NoticeType1").data("kendoDropDownTree")._values;

            //Category details
            var Categorylist8 = $("#dropdownCategory1").data("kendoDropDownTree")._values;

            //Court details
            var Courtlist7 = $("#dropdownCourtWise").data("kendoDropDownTree")._values;

            //Oppnent details
            var Oppnentlist4 = $("#dropdownOpponent").data("kendoDropDownTree")._values;

            //Lawyers details
            var Lawyerlist = $("#dropdownLawyers").data("kendoDropDownTree")._values;

            var FilterForAllSearch = document.getElementById('txtSearch').value;

            if (locationlist.length > 0
                || Departmentlist.length > 0
                || NoticeTypedetails.length > 0
                || Categorylist8.length > 0
                || Courtlist7.length > 0
                || Oppnentlist4.length > 0
                || Lawyerlist.length > 0
                || FilterForAllSearch != ""
                || ($("#dropdownFY1").val() != 0 && $("#dropdownFY1").val() != -1 && $("#dropdownFY1").val() != "")
                || ($("#dropdownWinLose").val() != 0 && $("#dropdownWinLose").val() != -1 && $("#dropdownWinLose").val() != "")) {
                var finalSelectedfilter = { logic: "and", filters: [] };

                if ($("#dropdownFY1").val() != 0 && $("#dropdownFY1").val() != -1 && $("#dropdownFY1").val() != "") {
                    var FYFilter = { logic: "or", filters: [] };

                    FYFilter.filters.push({
                        field: "FYName", operator: "contains", value: ($("#dropdownFY1").val() + ',')
                    });

                    finalSelectedfilter.filters.push(FYFilter);
                }
                if ($("#dropdownWinLose").val() != 0 && $("#dropdownWinLose").val() != -1 && $("#dropdownWinLose").val() != "") {
                    var WLFilter = { logic: "or", filters: [] };

                    WLFilter.filters.push({
                        field: "WLResult", operator: "eq", value: $("#dropdownWinLose").val()
                    });

                    finalSelectedfilter.filters.push(WLFilter);
                }

                if (locationlist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(locationlist, function (i, v) {
                        locFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }

                if (Departmentlist.length > 0) {
                    var DeptFilter = { logic: "or", filters: [] };

                    $.each(Departmentlist, function (i, v) {
                        DeptFilter.filters.push({
                            field: "DepartmentID", operator: "eq", value: parseInt(v)
                            //field: "DepartmentID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(DeptFilter);
                }

                if (NoticeTypedetails.length > 0) {

                    var TypeFilter = { logic: "or", filters: [] };
                    if ($("#dropdownType1").val() == "2") {
                        $.each(NoticeTypedetails, function (i, v) {
                            if (v == "I") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Defendant" });
                            }

                            if (v == "O") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Plaintiff" });
                            }

                        });
                        finalSelectedfilter.filters.push(TypeFilter);

                    }
                    if ($("#dropdownType1").val() == "1") {
                        $.each(NoticeTypedetails, function (i, v) {
                            if (v == "I") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Inward" });
                            }

                            if (v == "O") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Outward" });
                            }

                        });
                        finalSelectedfilter.filters.push(TypeFilter);

                    }

                }

                if (Categorylist8.length > 0) {
                    var CategoryFilter = { logic: "or", filters: [] };

                    $.each(Categorylist8, function (i, v) {
                        CategoryFilter.filters.push({
                            field: "Category", operator: "contains", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(CategoryFilter);
                }

                if (Courtlist7.length > 0) {
                    if ($("#dropdownType1").val() == "2") {
                        var CourtFilter = { logic: "or", filters: [] };

                        $.each(Courtlist7, function (i, v) {
                            CourtFilter.filters.push({
                                field: "CourtID", operator: "eq", value: parseInt(v)
                            });
                        });

                        finalSelectedfilter.filters.push(CourtFilter);
                    }
                }


                if (Oppnentlist4.length > 0) {
                    var OppnentFilter = { logic: "or", filters: [] };

                    $.each(Oppnentlist4, function (i, v) {
                        OppnentFilter.filters.push({
                            field: "PartyID", operator: "Contains", value: v + ','
                        });
                    });
                    finalSelectedfilter.filters.push(OppnentFilter);
                }

                if (Lawyerlist.length > 0) {
                    var lawyerFilter = { logic: "or", filters: [] };

                    $.each(Lawyerlist, function (i, v) {
                        lawyerFilter.filters.push({
                            field: " Opposition", operator: "Contains", value: v + ','
                        });
                    });
                    finalSelectedfilter.filters.push(lawyerFilter);
                }
                if (FilterForAllSearch != "" && FilterForAllSearch != undefined) {
                    var AllFilter = { logic: "or", filters: [] };

                    AllFilter.filters.push({
                        field: "Department", operator: "contains", value: FilterForAllSearch
                    });

                    AllFilter.filters.push({
                        field: "BranchName", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "Category", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "NoticeCaseDesc", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "TypeName", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "CaseRefNo", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "PartyName", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "RefNo", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "CaseStage", operator: "contains", value: FilterForAllSearch
                    });
                    AllFilter.filters.push({
                        field: "Title", operator: "contains", value: FilterForAllSearch
                    });


                    finalSelectedfilter.filters.push(AllFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid1").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid1").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid1").data("kendoGrid").dataSource.filter({});
            }

        }

        function FilterAll() {
            var locationlist = $("#dropdowntree").data("kendoDropDownTree")._values;
            var Departmentlist = $("#dropdownDept").data("kendoDropDownTree")._values;
            var NoticeTypedetails = $("#NoticeType").data("kendoDropDownTree")._values;
            var Categorylist8 = $("#dropdownCategory").data("kendoDropDownTree")._values;

            if (locationlist.length > 0
                || Departmentlist.length > 0
                || NoticeTypedetails.length > 0
                || Categorylist8.length > 0
                || ($("#dropdownFY").val() != 0 && $("#dropdownFY").val() != -1 && $("#dropdownFY").val() != "")) {
                var finalSelectedfilter = { logic: "and", filters: [] };

                if ($("#dropdownFY").val() != 0 && $("#dropdownFY").val() != -1 && $("#dropdownFY").val() != "") {
                    var FYFilter = { logic: "or", filters: [] };

                    FYFilter.filters.push({
                        field: "FYName", operator: "contains", value: ($("#dropdownFY").val() + ',')
                    });

                    finalSelectedfilter.filters.push(FYFilter);
                }
                if (locationlist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(locationlist, function (i, v) {
                        locFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }
                if (Departmentlist.length > 0) {
                    var DeptFilter = { logic: "or", filters: [] };

                    $.each(Departmentlist, function (i, v) {
                        DeptFilter.filters.push({
                            field: "DepartmentID", operator: "eq", value: parseInt(v)
                            //field: "DepartmentID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(DeptFilter);
                }
                if (NoticeTypedetails.length > 0) {

                    var TypeFilter = { logic: "or", filters: [] };
                    if ($("#dropdownType").val() == "2") {
                        $.each(NoticeTypedetails, function (i, v) {
                            if (v == "I") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Defendant" });
                            }

                            if (v == "O") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Plaintiff" });
                            }

                        });
                        finalSelectedfilter.filters.push(TypeFilter);

                    }
                    if ($("#dropdownType").val() == "1") {
                        $.each(NoticeTypedetails, function (i, v) {
                            if (v == "I") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Inward" });
                            }

                            if (v == "O") {
                                TypeFilter.filters.push({ field: "TypeName", operator: "eq", value: "Outward" });
                            }

                        });
                        finalSelectedfilter.filters.push(TypeFilter);

                    }

                }
                if (Categorylist8.length > 0) {
                    var CategoryFilter = { logic: "or", filters: [] };

                    $.each(Categorylist8, function (i, v) {
                        CategoryFilter.filters.push({
                            field: "Category", operator: "contains", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(CategoryFilter);
                }

                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }

        }

        var record = 0;
        var caserefno = 0;
        var LitigationType = 0;

        function Bindgrid() {
            if ($("#dropdownType").val() == 1) {
                caserefno = "RefNo";
                trueorfalse = true;
                LitigationType = "Notice"
            }
            else {
                caserefno = "CaseRefNo";
                trueorfalse = false;
                LitigationType = "Case"
            }
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/MyCaseNoticeWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =FlagIsApp%>&MonthId=All&StatusFlag=' + $("#dropdownType").val() + '&FY=0&StatusType=' + $("#dropdownStatus").val() + '&CourtId=&CategoryID=&StartDateDetail=&EndDateDetail=&CY=' + $("#dropdownCalYear").val() + '&WLResult=-1&AssingnedLawyer=-1',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                CloseDate: { type: "date", format: "dd/MM/yyyy" },
                                OpenDate: { type: "date", format: "dd/MM/yyyy" },
                            }
                        },
                        data: function (response) {
                            if ($("#dropdownType").val() == 1) {
                                return response[0].Notice;
                            }
                            else if ($("#dropdownType").val() == 2) {
                                return response[0].Case;
                            }
                        },
                        total: function (response) {
                            if ($("#dropdownType").val() == 1) {
                                return response[0].Notice.length;
                            }
                            else if ($("#dropdownType").val() == 2) {
                                return response[0].Case.length;
                            }
                        }
                    },

                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                    record = 0;
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBound: OnGridDataBoundAdvanced,
                <% if (NewColumnsLitigation == true){%>
                columns: [
                    {
                        title: "Sr. No.",
                        template: "#= ++record #",
                        width: "70px",
                        locked: true,
                        lockable: false,

                    },
                    {
                        field: "CBU", title: 'CBU',
                        width: "120px;",
                        locked: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Zone", title: 'Zone',
                        width: "120px;",
                        locked: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Region", title: 'Region',
                        width: "120px;",
                        locked: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Territory", title: 'Territory',
                        width: "120px;",
                        locked: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "TypeName", title: LitigationType + ' Type',
                        width: "140px",
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: caserefno, title: LitigationType + ' No.',
                        width: "150px",
                        lockable: false,
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Title", title: 'Title',
                        width: "280px",
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "PartyName", title: 'Opponents',
                        width: "200px",
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "NextHearingDate", title: 'Next Hearing Date',
                        template: "#if (NextHearingDate != '') {# #= ReviewerStatusName(NextHearingDate) # #} #",
                        width: "120px",
                        lockable: false,
                        hidden: trueorfalse,
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                    },
                    {
                        field: "NoticeCaseDesc", title: 'Description',
                        hidden: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "200px",
                    },
                    {
                        field: "CaseStage", title: 'Case Stage',
                        hidden: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "140px",
                    },
                    {
                        field: "NoticeStageName", title: 'Notice Stage',
                        hidden: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "140px",
                    },
                    {
                        field: "Department", title: 'Department',
                        hidden: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "140px",
                    },
                    {
                        field: "OpenDate",
                        title: "Open Date",
                        hidden: true,
                        lockable: false,
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }, width: "150px"
                    },
                    <% if (IsNoticeDate == customerID){%>
                    {
                        field: "NoticeDate",
                        title: "Notice Date",
                        hidden: true,
                        lockable: false,
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }, width: "120px"
                    },
                    <%}%>
                    {
                        field: "CloseDate",
                        title: "Close Date",
                        // hidden: true,
                        lockable: false,
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }, width: "120px"
                    },
                    {
                        field: "CaseCreator", title: 'Case/Notice Creator',
                        hidden: true,
                        type: "string",
                        lockable: false,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "160px",
                    },
                    {
                        field: "EmailId", title: 'Creator Email Id',
                        hidden: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "140px",
                    },
                    {
                        field: "ContactNumber", title: 'Creator Contact Number',
                        hidden: true,
                        lockable: false,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "120px",
                    },
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-edit", className: "ob-edit" },
                            { name: "delete", text: "", iconClass: "k-icon k-i-delete k-i-trash", className: "ob-delete" }
                        ], title: "Action", lock: true, width: 100, headerAttributes: {
                            style: "text-align: center;"
                        }
                    }
                ],
                <%}%>
                <% if (NewColumnsLitigation == false) {%>
                columns: [
                    {
                        title: "Sr. No.",
                        template: "#= ++record #",
                        width: "70px",
                    },
                    {
                        field: "BranchName", title: 'Location',
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "TypeName", title: LitigationType + ' Type',
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: caserefno, title: LitigationType + ' No.',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Title", title: 'Title',
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "PartyName", title: 'Opponents',
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    <% if (OwnerData == true)
                       {%>
                    {
                        field: "Owner", title: 'Internal User',
                        type: "string",
                        lockable: false,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "160px",
                    },
                    {
                        field: "OwnerName", title: 'Owner',
                        type: "string",
                        lockable: false,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "160px",
                    },
                         <%}%>
                    {
                        field: "NoticeCaseDesc", title: 'Description',
                        hidden: true,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CaseStage", title: 'Stage',
                        hidden: trueorfalse,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "NoticeStageName", title: 'Notice Stage',
                        hidden: true,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Department", title: 'Department',
                        hidden: true,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "OpenDate",
                        title: "Open Date",
                        hidden: true,
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }
                    },
                    <% if (IsNoticeDate  == customerID){%>
                    {
                        field: "NoticeDate",
                        title: "Notice Date",
                        hidden: true,
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }, width: "120px"
                    },
                       <%}%>
                    {
                        field: "CloseDate",
                        title: "Close Date",
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }
                    },
                    {
                        field: "NextHearingDate", title: 'Next Hearing Date',
                        hidden: trueorfalse,
                        template: "#if (NextHearingDate != '') {# #= ReviewerStatusName(NextHearingDate) # #} #",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                    },
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-edit", className: "ob-edit" },
                            { name: "delete", text: "", iconClass: "k-icon k-i-delete k-i-trash", className: "ob-delete" }
                        ], title: "Action", lock: true, width: 100, headerAttributes: {
                            style: "text-align: center;"
                        }
                    }
                ]
                <%}%>
            });

            function OnGridDataBoundAdvanced(e) {
                for (var i = 0; i < this.columns.length; i++) {
                    this.autoWidth;
                }
                var grid = $("#grid").data("kendoGrid");
                var gridData = grid.dataSource.view();
                for (var i = 0; i < gridData.length; i++) {

                    var currentUid = gridData[i].uid;
                    var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    if (<% =flagDelVal%> == 0) {
                        var deleteButton = $(currentRow).find(".ob-delete");
                        deleteButton.hide();
                    }
                }
                if ($("#dropdownStatus").val() == "1") {
                    grid.hideColumn("CloseDate");
                    if ($("#dropdownType").val() == 2) {
                        grid.showColumn("NextHearingDate");
                    }
                }
            }

            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "th", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                width: "150px",
                content: function (e) {
                    return "Edit " + LitigationType + " Details";
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-delete",
                width: "150px",
                content: function (e) {
                    return "Delete " + LitigationType + " Details";
                }
            });

            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                if (item.TypeCaseNotice != '') {
                    $('#divShowDialog').modal('show');
                    $('#showdetails').attr('width', '100%');
                    $('#showdetails').attr('height', '550px');
                    $('.modal-dialog').css('width', '100%');
                    
                    var codeTitle  = encodeURIComponent(item.Title);

                    if (item.TypeCaseNotice == 'C') {
                        $('#showdetails').attr('src', "../aspxPages/CaseDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID + "&CaseNumber=" + item.CaseRefNo +  "&Title=" + codeTitle);
                    }
                    else if (item.TypeCaseNotice == 'N') {
                        $('#showdetails').attr('src', "../../Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID  +  "&CaseNumber=" + item.RefNo +  "&Title=" + codeTitle);
                    }
                }
                return true;
            });
        }

        function ReviewerStatusName(Status) {
            if (Status != "" && Status != null) {
                var from = Status.split(".")
                return (from[2]) + "-" + (from[1]) + "-" + (from[0]);
            }
            else
                return "";
        }

        var trueorfalse = false;

        function BindgridPopup() {

            if ($("#dropdownType1").val() == 1) {
                caserefno = "RefNo";
                trueorfalse = true;
                LitigationType = "Notice";
            }
            else {
                caserefno = "CaseRefNo";
                trueorfalse = false;
                LitigationType = "Case";
            }

            var setStartDate = '';
            var setEndDate = '';

            if ($("#Startdatepicker").val() != undefined) {
                //setStartDate = kendo.toString($("#Startdatepicker").val(), "dd/MM/yyyy");
                setStartDate = $("#Startdatepicker").val();
            }
            if ($("#Lastdatepicker").val() != undefined) {
                //setEndDate = kendo.toString($("#Lastdatepicker").val(), "dd/MM/yyyy");
                setEndDate = $("#Lastdatepicker").val();
            }

            var grid = $('#grid1').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid1').empty();

            var grid = $("#grid1").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/MyCaseNoticeWorkSpace?UserId=<% =UId%>&CustomerID=<% =CustId%>&FlagIsApp=<% =FlagIsApp%>&MonthId=' + $("#dropdownPastData1").val() + '&StatusFlag=' + $("#dropdownType1").val() + '&FY=0&StatusType=' + $("#dropdownStatus1").val() + '&StartDateDetail=' + setStartDate + '&EndDateDetail=' + setEndDate + '&CY=' + $("#dropdownCalYear1").val() + '&WLResult=-1&AssingnedLawyer=-1',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                CloseDate: { type: "date", format: "dd/MM/yyyy" },
                                OpenDate: { type: "date", format: "dd/MM/yyyy" },
                            }
                        },
                        data: function (response) {
                            if ($("#dropdownType1").val() == 1) {
                                return response[0].Notice;
                            }
                            else if ($("#dropdownType1").val() == 2) {
                                return response[0].Case;
                            }
                        },
                        total: function (response) {
                            if ($("#dropdownType1").val() == 1) {
                                return response[0].Notice.length;
                            }
                            else if ($("#dropdownType1").val() == 2) {
                                return response[0].Case.length;
                            }
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBound: OnGridDataBoundAdvanced,
                <% if (NewColumnsLitigation == true){%>
                columns: [
                    {
                        title: "Sr. No.",
                        template: "#= ++record #",
                        width: "70px",
                        locked: true,
                        lockable: false,

                    },
                    {
                        field: "CBU", title: 'CBU',
                        width: "120px;",
                        locked: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Zone", title: 'Zone',
                        width: "120px;",
                        locked: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Region", title: 'Region',
                        width: "120px;",
                        locked: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Territory", title: 'Territory',
                        width: "120px;",
                        locked: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "TypeName", title: LitigationType + ' Type',
                        width: "140px",
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: caserefno, title: LitigationType + ' No.',
                        width: "150px",
                        lockable: false,
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Title", title: 'Title',
                        width: "280px",
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "PartyName", title: 'Opponents',
                        width: "200px",
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "NextHearingDate", title: 'Next Hearing Date',
                        template: "#if (NextHearingDate != '') {# #= ReviewerStatusName(NextHearingDate) # #} #",
                        width: "120px",
                        lockable: false,
                        hidden: trueorfalse,
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    //type: "date",
                                    //format: "{0:dd/MM/yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                    },
                    {
                        field: "NoticeCaseDesc", title: 'Description',
                        hidden: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "200px",
                    },
                    {
                        field: "CaseStage", title: 'Stage',
                        hidden: trueorfalse,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "140px",
                    },
                    {
                        field: "NoticeStageName", title: 'Notice Stage',
                        hidden: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "140px",
                    },
                    {
                        field: "Department", title: 'Department',
                        hidden: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "140px",
                    },
                    {
                        field: "OpenDate",
                        title: "Open Date",
                        hidden: true,
                        lockable: false,
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }, width: "150px"
                    },
                    <% if (IsNoticeDate == customerID){%>
                    {
                        field: "NoticeDate",
                        title: "Notice Date",
                        hidden: true,
                        lockable: false,
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }, width: "120px"
                    },
                    <%}%>
                    {
                        field: "CloseDate",
                        title: "Close Date",
                        // hidden: true,
                        lockable: false,
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }, width: "120px"
                    },
                    {
                        field: "CaseCreator", title: 'Case/Notice Creator',
                        hidden: true,
                        type: "string",
                        lockable: false,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "160px",
                    },
                    {
                        field: "EmailId", title: 'Creator Email Id',
                        hidden: true,
                        lockable: false,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "140px",
                    },
                    {
                        field: "ContactNumber", title: 'Creator Contact Number',
                        hidden: true,
                        lockable: false,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "120px",
                    },
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-edit", className: "ob-edit1" },
                            { name: "delete", text: "", iconClass: "k-icon k-i-delete k-i-trash", className: "ob-delete1" }
                        ], title: "Action", lock: true, width: 100, headerAttributes: {
                            style: "text-align: center;"
                        }
                    }
                ],
                <%}%>
                <% if (NewColumnsLitigation == false) {%>
                columns: [
                    {
                        title: "Sr. No.",
                        template: "#= ++record #",
                        width: "70px",
                    },
                    {
                        field: "BranchName", title: 'Location',
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "TypeName", title: LitigationType + ' Type',
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: caserefno, title: LitigationType + ' No.',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Title", title: 'Title',
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "PartyName", title: 'Opponents',
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                       <% if (OwnerData == true)
        {%>
                      {
                          field: "Owner", title: 'Internal User',
                          type: "string",
                          lockable: false,
                          attributes: {
                              style: 'white-space: nowrap;'
                          },
                          filterable: {
                              multi: true,
                              extra: false,
                              search: true,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }, width: "160px",
                      },
                         {
                             field: "OwnerName", title: 'Owner',
                             type: "string",
                             lockable: false,
                             attributes: {
                                 style: 'white-space: nowrap;'
                             },
                             filterable: {
                                 multi: true,
                                 extra: false,
                                 search: true,
                                 operators: {
                                     string: {
                                         eq: "Is equal to",
                                         neq: "Is not equal to",
                                         contains: "Contains"
                                     }
                                 }
                             }, width: "160px",
                         },
                         <%}%>
                    {
                        field: "NextHearingDate", title: 'Next Hearing Date',
                        template: "#if (NextHearingDate != '') {# #= ReviewerStatusName(NextHearingDate) # #} #",
                        hidden: trueorfalse,
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },
                    },
                    {
                        field: "NoticeCaseDesc", title: 'Description',
                        hidden: true,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CaseStage", title: 'Stage',
                        hidden: trueorfalse,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "NoticeStageName", title: 'Notice Stage',
                        hidden: true,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Department", title: 'Department',
                        hidden: true,
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "OpenDate",
                        title: "Open Date",
                        hidden: true,
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }
                    },
                    <% if (IsNoticeDate  == customerID){%>
                    {
                        field: "NoticeDate",
                        title: "Notice Date",
                        hidden: true,
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }, width: "120px"
                    },
                       <%}%>
                    {
                        field: "CloseDate",
                        title: "Close Date",
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }
                    },
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-edit", className: "ob-edit1" },
                            { name: "delete", text: "", iconClass: "k-icon k-i-delete k-i-trash", className: "ob-delete1" }
                        ], title: "Action", lock: true, width: 100, headerAttributes: {
                            style: "text-align: center;"
                        }
                    }
                ]
                <%}%>
            });

            function OnGridDataBoundAdvanced(e) {
                for (var i = 0; i < this.columns.length; i++) {
                    this.autoWidth;
                }
                var grid = $("#grid1").data("kendoGrid");
                var gridData = grid.dataSource.view();
                for (var i = 0; i < gridData.length; i++) {

                    var currentUid = gridData[i].uid;
                    var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    if (<% =flagDelVal%> == 0) {
                        var deleteButton = $(currentRow).find(".ob-delete1");
                        deleteButton.hide();
                    }
                }
                if ($("#dropdownStatus1").val() == "1") {
                    grid.hideColumn("CloseDate");
                    grid.showColumn("NextHearingDate");
                }
            }
            $("#grid1").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid1").kendoTooltip({
                filter: "th", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid1").kendoTooltip({
                filter: ".k-grid-edit",
                width: "150px",
                content: function (e) {
                    return "Edit " + LitigationType + " Details";
                }
            });
            $("#grid1").kendoTooltip({
                filter: ".k-grid-delete",
                width: "150px",
                content: function (e) {
                    return "Delete " + LitigationType + " Details";
                }
            });


            $(document).on("click", "#grid1 tbody tr .ob-edit1", function (e) {
                var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                if (item.TypeCaseNotice != '') {
                    $('#divShowDialog1').modal('show');
                    $('#showdetails1').attr('width', '100%');
                    $('#showdetails1').attr('height', '550px');
                    $('.modal-dialog').css('width', '98%');

                    var codeTitle  = encodeURIComponent(item.Title);

                    if (item.TypeCaseNotice == 'C') {
                        $('#showdetails1').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID +  "&Title=" + codeTitle);

                    }
                    else if (item.TypeCaseNotice == 'N') {
                        $('#showdetails1').attr('src', "../../Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=" + item.NoticeCaseInstanceID + "&Title=" + codeTitle);
                    }
                }
                return true;
            });
        }

        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            fhead('My Workspace');

            $(document).on("click", "#grid tbody tr .ob-delete", function (e) {
                var retVal = confirm("Do you want to Delete Record..?");
                if (retVal == true) {
                    var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                    var $tr = $(this).closest("tr");
                    $.ajax({
                        type: 'POST',
                        url: '<% =Path%>Litigation/Delete_CaseNotice_WorkSpace?UserID=<% =UId%>&CaseInstanceID=' + item.NoticeCaseInstanceID + '&Type=' + item.TypeCaseNotice,
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                        },
                        success: function (result) {
                            // notify the data source that the request succeeded
                            grid = $("#grid").data("kendoGrid");
                            grid.removeRow($tr);
                        },
                        error: function (result) {
                            // notify the data source that the request failed
                            console.log(result);
                        }
                    });
                }
                return true;
            });

            $(document).on("click", "#grid1 tbody tr .ob-delete1", function (e) {
                var retVal = confirm("Do you want to Delete Record..?");
                if (retVal == true) {
                    var item = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                    var $tr = $(this).closest("tr");
                    $.ajax({
                        type: 'POST',
                        url: '<% =Path%>Litigation/Delete_CaseNotice_WorkSpace?UserID=<% =UId%>&CaseInstanceID=' + item.CaseInstanceID + '&Type=' + item.TypeCaseNotice,
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                        },
                        success: function (result) {
                            grid = $("#grid1").data("kendoGrid");
                            grid.removeRow($tr);
                        },
                        error: function (result) {
                            // notify the data source that the request failed
                            console.log(result);
                        }
                    });
                }
                return true;
            });

        });

        function OpenAdvanceSearch(e) {
            $("#divAdvanceSearchModel").kendoWindow({
                modal: true,
                width: "97%",
                height: "92%",
                title: "Advanced Search",
                visible: false,
                draggable: true,
                refresh: true,
                pinned: true,
                actions: [
                    "Close"
                ], close: ClearAllFilter

            }).data("kendoWindow").open().center();
            e.preventDefault();
            return false;

        }

        function exportReportMain(e) {
            //location details
            var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
            var locationsdetails = [];
            $.each(list1, function (i, v) {
                locationsdetails.push(v);
            });

            //Department details
            var list2 = $("#dropdownDept").data("kendoDropDownTree")._values;
            var Deptdetails = [];
            $.each(list2, function (i, v) {
                Deptdetails.push(v);
            });

            //NoticeType details
            var list3 = $("#NoticeType").data("kendoDropDownTree")._values;
            var NoticeTypedetails = [];
            $.each(list3, function (i, v) {
                NoticeTypedetails.push(v);
            });

            //Category details
            var Categorylist = $("#dropdownCategory").data("kendoDropDownTree")._values;
            var CategoryID = [];
            $.each(Categorylist, function (i, v) {
                CategoryID.push(v);
            });

            var UId = document.getElementById('UId').value;
            var customerId = document.getElementById('CustomerId').value;
            var PathName = document.getElementById('Path').value;
            var FlagIsApp = document.getElementById('FlagDetail').value;

            $.ajax({
                type: "GET",
                url: '' + PathName + '//LitigationExportReport/NoticeCaseWorkSpaceReport',
                data: {
                    UserId: UId,
                    CustomerID: customerId,
                    FlagIsApp: FlagIsApp,
                    MonthId: 'All',
                    StatusFlag: $("#dropdownType").val(),
                    FY: $("#dropdownFY").val(),
                    StatusType: $("#dropdownStatus").val(),
                    StartDateDetail: '',
                    EndDateDetail: '',
                    Dept: JSON.stringify(Deptdetails),
                    NoticeType: JSON.stringify(NoticeTypedetails),
                    location: JSON.stringify(locationsdetails),
                    Category: JSON.stringify(CategoryID),
                    CY: $("#dropdownCalYear").val(),
                    CourtID: "-1",
                    Oppnent: "-1",
                    Lawyers: "-1",
                    Result: "-1",
                    AllSearch: ""
                },
                success: function (response) {
                    if (response != "Error" && response != "No Record Found" && response != "") {
                        window.location.href = '' + PathName + '/LitigationExportReport/GetFile?userpath=' + response + '';
                    }
                    if (response == "No Record Found") {
                        alert("No Record Found");
                    }
                }
            });
            e.preventDefault();
            return false;
        }

        function exportReportAdvanced(e) {

            //location details
            var list1 = $("#dropdowntree1").data("kendoDropDownTree")._values;
            var locationsdetails = [];
            $.each(list1, function (i, v) {
                locationsdetails.push(v);
            });

            //Department details
            var list2 = $("#dropdownDept1").data("kendoDropDownTree")._values;
            var Deptdetails = [];
            $.each(list2, function (i, v) {
                Deptdetails.push(v);
            });

            //NoticeType details
            var list3 = $("#NoticeType1").data("kendoDropDownTree")._values;
            var NoticeTypedetails = [];
            $.each(list3, function (i, v) {
                NoticeTypedetails.push(v);
            });

            //Category details
            var Categorylist = $("#dropdownCategory1").data("kendoDropDownTree")._values;
            var CategoryID = [];
            $.each(Categorylist, function (i, v) {
                CategoryID.push(v);
            });

            //CourtID details
            var Courtlist = $("#dropdownCourtWise").data("kendoDropDownTree")._values;
            var CourtDetailID = [];
            $.each(Courtlist, function (i, v) {
                CourtDetailID.push(v);
            });

            //Oppnent details
            var list4 = $("#dropdownOpponent").data("kendoDropDownTree")._values;
            var Oppnentdetails = [];
            $.each(list4, function (i, v) {
                Oppnentdetails.push(v);
            });

            //Lawyer details
            var list4 = $("#dropdownLawyers").data("kendoDropDownTree")._values;
            var Lawyerdetails = [];
            $.each(list4, function (i, v) {
                Lawyerdetails.push(v);
            });

            var WlResult = $("#dropdownWinLose").val();
            if (WlResult == "") {
                WlResult = -1;
            }
            else {
                WlResult = $("#dropdownWinLose").val();
            }


            var UId = document.getElementById('UId').value;
            var customerId = document.getElementById('CustomerId').value;
            var PathName = document.getElementById('Path').value;
            var FlagIsApp = document.getElementById('FlagDetail').value;
            var FilterForAll = document.getElementById('txtSearch').value;


            $.ajax({
                type: "GET",
                url: '' + PathName + '//LitigationExportReport/NoticeCaseWorkSpaceReport',
                data: {
                    UserId: UId,
                    CustomerID: customerId,
                    FlagIsApp: FlagIsApp,
                    MonthId: $("#dropdownPastData1").val(),
                    StatusFlag: $("#dropdownType1").val(),
                    FY: $("#dropdownFY1").val(),
                    StatusType: $("#dropdownStatus1").val(),
                    StartDateDetail: $("#Startdatepicker").val(),
                    EndDateDetail: $("#Lastdatepicker").val(),
                    Dept: JSON.stringify(Deptdetails),
                    NoticeType: JSON.stringify(NoticeTypedetails),
                    location: JSON.stringify(locationsdetails),
                    Category: JSON.stringify(CategoryID),
                    CY: $("#dropdownCalYear1").val(),
                    CourtID: JSON.stringify(CourtDetailID),
                    Oppnent: JSON.stringify(Oppnentdetails),
                    Lawyers: JSON.stringify(Lawyerdetails),
                    Result: WlResult,
                    AllSearch: FilterForAll,
                },
                success: function (response) {
                    if (response != "Error" && response != "No Record Found" && response != "") {
                        window.location.href = '' + PathName + '/LitigationExportReport/GetFile?userpath=' + response + '';
                    }
                    if (response == "No Record Found") {
                        alert("No Record Found");
                    }
                }
            });
            e.preventDefault();
            return false;
        }

        function OpenAddNewNotice(e) {
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('width', '100%');
            $('#showdetails').attr('height', '550px');
            $('.modal-dialog').css('width', '100%');

            if ($("#dropdownType").val() == "2") {
                $('#showdetails').attr('src', "../../Litigation/aspxPages/CaseDetailPage.aspx?AccessID=0");

            }
            else if ($("#dropdownType").val() == '1') {
                $('#showdetails').attr('src', "../../Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=0");
            }
            e.preventDefault();
            return false;
        }

        function ClosePopNoticeDetialPage() {
            $('#divShowDialog').modal('hide');
            $('#showdetails').attr('src', "../../Common/blank.html");
            //Bindgrid();
            //FilterAll();

            $("#grid").data("kendoGrid").dataSource.read();
            $("#grid").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoard('dropdownDept', 'filtersstoryboardDept', 'Dept');
            fCreateStoryBoard('NoticeType', 'filtersstoryboardNoticeType', 'Type');
            fCreateStoryBoard('dropdownCourtWise', 'filtersstoryCourtboard', 'Court');
            fCreateStoryBoard('dropdownCategory', 'filtersstoryCategoryboard', 'Category');

            //e.preventDefault();
        }

        function ApplyBtnAdvancedFilter(e) {
            BindgridPopup();
            FilterAllAdvanced();
        }

        function ApplyBtnMainFilter(e) {
            Bindgrid();
            FilterAll();
            e.preventDefault();
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="col-lg-8 col-md-8">
                    <input id="Path" type="hidden" value="<% =Path%>" />
                    <input id="CustomerId" type="hidden" value="<% =CustId%>" />
                    <input id="UId" type="hidden" value="<% =UId%>" />
                    <input id="FlagDetail" type="hidden" value="<% =FlagIsApp%>" />
                </div>

            </div>
        </div>

        <div style="margin: 0.4% 0.9% 1%; width: 99%;">
            <input id="dropdownType" style="width: 15.5%; margin-right: 10px;" />
            <input id="dropdowntree" style="width: 23%; margin-right: 10px;" />
            <input id="dropdownDept" style="width: 18%; margin-right: 10px;" />
            <input id="dropdownFY" style="width: 15%; margin-right: 10px;" />
            <input id="dropdownCalYear" style="width: 15%; margin-right: 10px;" />
            <button id="AddNewCaseNotice" runat="server" style="width: 6.1%" onclick="OpenAddNewCaseNotice(event)">
                <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</button>
        </div>

        <div style="margin: 0.4% 0.9% 0.5%; width: 99%;">
            <input id="dropdownStatus" style="width: 15.5%; margin-right: 10px;" />
            <input id="dropdownCategory" style="width: 23%; margin-right: 10px;" />
            <input id="NoticeType" style="width: 18%; margin-right: 10px;" />
            <button id="exportReport" onclick="exportReportMain(event)" style="height: 32px;" data-placement="bottom"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
            <button id="ApplyBtnMain" style="height: 32px; width: 77px; margin-left: 10px;" onclick="ApplyBtnMainFilter(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
            <button id="AdavanceSearch" style="height: 32px; width: 163px; margin-left: 1%;" onclick="OpenAdvanceSearch(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Advanced Search</button>
            <button id="ClearfilterMain" style="height: 32px; width: 72px; margin-left: 1%;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
        </div>

        <div class="row" style="display: none; margin-left: 10px; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboard">&nbsp;&nbsp;</div>
        <div class="row" style="display: none; margin-left: 10px; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboardDept">&nbsp;&nbsp;</div>
        <div class="row" style="display: none; margin-left: 10px; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboardNoticeType">&nbsp;&nbsp;</div>
        <div class="row" style="display: none; margin-left: 10px; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryCategoryboard">&nbsp;&nbsp;</div>


        <div class="row" style="padding-top: 2px;">
            <div id="grid" style="margin-left: 10px; margin-right: 10px; margin-top: 5px;"></div>
        </div>

        <div id="divAdvanceSearchModel" style="padding-top: 5px; display: none;">
            <div style="margin: 0.4% 0.5% 0.5%;">
                <input id="dropdownType1" data-placeholder="Type" style="width: 14.5%; margin-right: 0.8%;" />
                <input id="dropdownStatus1" data-placeholder="Status" style="width: 14.5%; margin-right: 0.8%;" />
                <input id="dropdownWinLose" style="width: 15%; margin-right: 0.8%;" />
                <input id="NoticeType1" style="width: 15%; margin-right: 0.8%;" />
                <input id="dropdownFY1" data-placeholder="Finance Year" style="width: 17.5%; margin-right: 0.8%;" />
                <input id="dropdownPastData1" style="width: 17.5%;" />
            </div>

            <div style="margin: 0.4% 0.5% 0.5%;">
                <input id="dropdowntree1" data-placeholder="Entity/Sub-Entity/Location" style="width: 30%; margin-right: 0.8%;" />
                <input id="dropdownDept1" data-placeholder="Department" style="width: 15%; margin-right: 0.8%;" />
                <input id="dropdownOpponent" style="width: 15%; margin-right: 0.8%;" />
                <input id="dropdownCalYear1" style="width: 14%; margin-right: 0.8%;" />
                <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" title="startdatepicker" style="width: 10%; margin-right: 0.8%;" />
                <input id="Lastdatepicker" placeholder="End Date" title="enddatepicker" style="width: 10%;" />
            </div>

            <div style="margin: 0.4% 0.5% 5px;">
                <input id="dropdownCourtWise" style="width: 30%; margin-right: 0.8%;" />
                <input id="dropdownLawyers" style="width: 15%; margin-right: 0.8%;" />
                <input id="txtSearch" type="text" style="width: 15%; margin-right: 0.8%;" onkeydown="return (event.keyCode!=13);" class="k-textbox" placeholder="Type to Search " />
                <input id="dropdownCategory1" data-placeholder="Category Of Case" style="width: 14%; margin-right: 0.8%;" />
                <button id="exportAdvanced" onclick="exportReportAdvanced(event)" style="height: 25px;" data-placement="bottom"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
                <button id="ApplyBtnAdvanced" style="height: 25px; width: 70px; margin-left: 0.8%;" onclick="ApplyBtnAdvancedFilter(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
                <button id="Clearfilter" style="height: 25px; width: 66px; margin-left: 0.7%;" onclick="ClearAllFilter()"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
            </div>

            <div class="row" style="display: none; margin-left: 8px; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboard1">&nbsp;&nbsp;</div>
            <div class="row" style="display: none; margin-left: 8px; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboardNoticeType1">&nbsp;&nbsp;</div>
            <div class="row" style="display: none; margin-left: 8px; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboardDept1">&nbsp;&nbsp;</div>
            <div class="row" style="display: none; margin-left: 8px; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryCategoryboard1">&nbsp;&nbsp;</div>
            <div class="row" style="display: none; margin-left: 8px; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryCourtboard">&nbsp;&nbsp;</div>
            <div class="row" style="display: none; margin-left: 8px; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboardOpponent">&nbsp;&nbsp;</div>
            <div class="row" style="display: none; margin-left: 8px; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboardLawyer">&nbsp;&nbsp;</div>

            <div class="row" style="padding-top: 3px;">
                <div id="grid1" style="margin-left: 7px; margin-right: 7px;"></div>
            </div>
            <div class="modal fade" id="divShowDialog1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label style="width: 210px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                                Add/Edit Details</label>
                            <button id="btnAddEditcase1" type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="ClosePopNoticeDetialPage();">&times;</button>
                        </div>

                        <div class="modal-body" style="background-color: #f7f7f7;">
                            <iframe id="showdetails1" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 210px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                            Add/Edit Details</label>
                        <button id="btnAddEditcase" type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="ClosePopNoticeDetialPage();">&times;</button>
                    </div>

                    <div class="modal-body" style="background-color: #f7f7f7;">
                        <iframe id="showdetails" src="about:blank" width="95%" height="95%" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div id="divRPAFilters" style="display: none;">

            <div style="margin: 1.4% 6% 0%;">
                <label style="width: 25%; margin-right: 0.5%;">Court Type<span style="color: red;">*</span></label>
            </div>
            <div id="divCourtType" style="margin: 0.4% 6% 1%;">
                <input id="drpCourtType" required="required" style="width: 30%;" />
            </div>
            <div id="divDistrictCourt" style="display: none;">
                <div style="margin: 1.4% 6% 0%;">
                    <label style="width: 30%; margin-right: 0.5%;">State</label>
                    <label style="width: 30%; margin-right: 0.5%;">District</label>
                    <label style="width: 30%; margin-right: 0.5%;">Court Name</label>
                </div>
                <div style="margin: 0.4% 6% 1%;">
                    <input id="drpCaseState" style="width: 30%; margin-right: 0.5%" />
                    <input id="drpDistrict"  style="width: 30%; margin-right: 0.5%;" />
                    <input id="drpCourtNameList" style="width: 30%;" />
                </div>
            </div>

            <div id="divHghCourt" style="display: none;">
                <div style="margin: 1.4% 6% 0%;">
                    <label style="width: 30%; margin-right: 0.5%;">Court Name<span style="color: red;">*</span></label>
                    <label style="width: 30%;">Bench<span style="color: red;">*</span></label>
                </div>
                <div style="margin: 0.4% 6% 1%;">
                    <input id="drpHCCaseState" style="width: 30%; margin-right: 0.5%" />
                    <input id="drpHCCourtNameList" style="width: 30%;" />
                </div>
            </div>

            <div id="divCommonFilters">
                <div style="margin: 1.4% 6% 0%;">
                    <label style="width: 30%; margin-right: 0.5%;">Case Type<span style="color: red;">*</span></label>
                    <label style="width: 30%; margin-right: 0.5%;">Case Number<span style="color: red;">*</span></label>
                    <label style="width: 20%; margin-right: 0.5%;">Case Year<span style="color: red;">*</span></label>
                </div>

                <div style="margin: 0.4% 6% 1%;">
                    <input id="drpCaseType" required="required" style="width: 30%; margin-right: 0.5%;" />
                    <input id="txtCaseNo" class="k-textbox" placeholder="Case Number" onkeypress="return /[0-9\s.]/i.test(event.key)" required="required" style="width: 30%; margin-right: 0.5%;" />
                    <div id="col-md-2" class="datepickerRPA">
                        <input id="CaseYearCalender" required placeholder="Case Year" />
                    </div>
                </div>
            </div>

            <div style="margin: 2% 6% 1.5%;">
                <button id="btnSearch" class="k-button" style="margin-right: 0.5%" type="button">Search</button>
                <button id="ClearfilterRPA" class="k-button" style="margin-right: 0.5%" onclick="ClearfilterRPA()"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
                <button id="btnSkip" class="k-button" style="margin-right: 0.5%" type="button" onclick="btnSkip_click(event);">Skip</button>
            </div>
            <hr />
            <div>
                <label id="lblCaseStatusRPA" style="color: red; font-size: 14px; margin-left: 40%; font-weight: 400;"></label>
            </div>
            <div style="margin-left: 6%; float: left;">
                <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
            </div>
        </div>
        <div id="confirmbox"></div>
    </div>
</asp:Content>