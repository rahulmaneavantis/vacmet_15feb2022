﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class litigation_Assignment : System.Web.UI.Page
    {
        public static List<int> LawyerTypeList = new List<int>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["Mode"] = 0;
                BindLegalEntityData();
                BindDepartment();
                BindCity(); BindGrid(); BindLawyer();
                Act();
            }
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var legalentitydata = LitigationLaw.FillLegalEntityData(customerID);
            ddlEntity.DataTextField = "Name";
            ddlEntity.DataValueField = "ID";
            ddlEntity.Items.Clear();
            ddlEntity.DataSource = legalentitydata;
            ddlEntity.DataBind();
        }
        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void BindCity()
        {
            ddlCity.DataTextField = "Name";
            ddlCity.DataValueField = "ID";
            var obj = StateCityManagement.getallCity();
            ddlCity.DataSource = obj;
            ddlCity.DataBind();
            //ddlCity.Items.Insert(0, new ListItem("Select City", "-1"));
        }

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            ddlDepartment.DataTextField = "Name";
            ddlDepartment.DataValueField = "ID";
           
            ddlDepartment.DataSource = obj;
            ddlDepartment.DataBind();
        }

        public void BindLawyer()
        {
            long customerID = -1;
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

            var obj = LawyerManagement.GetLawyerListForMapping(customerID);

            ddlMappingLawyer.DataTextField = "Name";
            ddlMappingLawyer.DataValueField = "ID";
            
            ddlMappingLawyer.DataSource = obj;
            ddlMappingLawyer.DataBind();
        }

        public void Act()
        {
            ddlAct.DataTextField = "Name";
            ddlAct.DataValueField = "ID";
            var obj = LitigationLaw.GetAllAct();
            ddlAct.DataSource = obj;
            ddlAct.DataBind();
        }

        public void BindGrid()
        {
            try
            {
                var AllData = LitigationManagement.GetLitigationAssignmentData();
                grdLitigationDetails.DataSource = AllData;
                Session["TotalRows"] = AllData.Count;
                grdLitigationDetails.DataBind();
                upLCPanel.Update();
                upPromotor.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                LitigationAssignment objlawyer = new LitigationAssignment()
                {
                    LegalEntity = Convert.ToInt32(ddlEntity.SelectedValue),
                    CityID = Convert.ToInt32(ddlCity.SelectedValue),
                    DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                    ActID = Convert.ToInt32(ddlAct.SelectedValue),
                    Title = tbxTitle.Text,
                    Description = tbxDescription.Text,
                    LiabilityAmount = Convert.ToDecimal(tbxAmtLiability.Text),
                    IsDeleted=false
                };

                if ((int) ViewState["Mode"] == 1)
                {
                    objlawyer.ID = Convert.ToInt32(ViewState["LawyerID"]);//Need to change DeptID
                }

                if ((int) ViewState["Mode"] == 0)
                {
                    LitigationManagement.CreateLitigationAssignment(objlawyer);
                    LawyerTypeList.Clear();
                    for (int i = 0; i < ddlMappingLawyer.Items.Count; i++)
                    {
                        if (ddlMappingLawyer.Items[i].Selected)
                        {
                            LawyerTypeList.Add(Convert.ToInt32(ddlMappingLawyer.Items[i].Value));
                        }
                    }
                    LitiAssignment_Mapping objAssignment = new LitiAssignment_Mapping();
                    var LawyerID = LitigationManagement.GetLitiAssignmentID(objlawyer);
                    foreach (var aItem in LawyerTypeList)
                    {
                        objAssignment.LawyerID=aItem;
                        objAssignment.Liti_AssignmentID=LawyerID.ID;
                        objAssignment.IsActive = true;
                        LitigationManagement.CreateLitiAssignmentMapping(objAssignment);
                    }
                    cvCustomeRemark.IsValid = false;
                    cvCustomeRemark.ErrorMessage = "Litigation Assingment Saved Successfully";
                    tbxTitle.Text = string.Empty;
                    tbxDescription.Text = string.Empty;
                    tbxAmtLiability.Text = string.Empty;
                    ddlMappingLawyer.Items.Clear();
                    ddlEntity.SelectedIndex = -1;
                    ddlCity.SelectedIndex = -1;
                    ddlDepartment.SelectedIndex = -1;
                    ddlAct.SelectedIndex = -1;
                }

                else if ((int) ViewState["Mode"] == 1)
                {
                    LitigationManagement.UpdateLitigationAssignment(objlawyer);
                    LawyerTypeList.Clear();
                    for (int i = 0; i < ddlMappingLawyer.Items.Count; i++)
                    {
                        if (ddlMappingLawyer.Items[i].Selected)
                        {
                            LawyerTypeList.Add(Convert.ToInt32(ddlMappingLawyer.Items[i].Value));
                        }
                    }
                    LitiAssignment_Mapping objMap = new LitiAssignment_Mapping();
                    objMap.Liti_AssignmentID = objlawyer.ID;
                    LitigationManagement.UpdateLitAssignmentMappingAllFalse(objMap);
                    foreach (var aItem in LawyerTypeList)
                    {
                        objMap.Liti_AssignmentID = aItem;
                        objMap.LawyerID = objlawyer.ID;
                        objMap.IsActive = true;
                        if (LitigationManagement.CheckLitiAssignmentExist(objMap))
                        {
                            LitigationManagement.UpdateLitigationMappingData(objMap);
                        }
                        else
                        {
                            LitigationManagement.CreateLitiAssignmentMapping(objMap);
                        }
                    }
                    cvCustomeRemark.IsValid = false;
                    cvCustomeRemark.ErrorMessage = "Litigation Assingment Updated Successfully";
                }
                upPromotor.Update();
                BindGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void upPromotor_Load(object sender, EventArgs e)
        {

        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdLitigationDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                BindGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdLitigationDetails.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            tbxTitle.Text = string.Empty;
            tbxDescription.Text = string.Empty;
            tbxAmtLiability.Text = string.Empty;
            //ddlMappingLawyer.Items.Clear();
            ddlEntity.SelectedIndex = -1;
            ddlCity.SelectedIndex = -1;
            ddlDepartment.SelectedIndex = -1;
            ddlAct.SelectedIndex = -1;
            LawyerTypeList.Clear();
            upPromotor.Update();
        }
                

        protected void upLCPanel_Load(object sender, EventArgs e)
        {

        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdLitigationDetails.PageIndex = chkSelectedPage - 1;

            grdLitigationDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindGrid();
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

    }
}