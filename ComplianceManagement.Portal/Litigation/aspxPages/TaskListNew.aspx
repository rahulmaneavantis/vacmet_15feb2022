﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="TaskListNew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Reports.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

    <title></title>

    <style type="text/css">
        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 1px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 180px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
    </style>

    <script type="text/javascript">

        setTimeout(printSomething, 1000);

        function printSomething() {
            window.scrollTo(0, document.body.scrollHeight);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            BindType();
            BindPeriod();
            BindBranch();
            BindPriority();
            bindTaskType();
            Bindgrid();
            BindStatus();
        });

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Location
                $('#ClearfilterMain').css('display', 'block');
            }
            if (div == 'filtersstoryboardStatus') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Staus
                $('#ClearfilterMain').css('display', 'block');
            }
            if (div == 'filtersstoryboardPriority') {
                $('#' + div).append('Priority&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//priority
                $('#ClearfilterMain').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                if (buttontest.length > 10) {
                    buttontest = buttontest.substring(0, 10).concat("...");
                }
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB;height: 20px;Color:Gray;margin-left:5px;margin-bottom: 4px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="Clear" aria-label="Clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" title="Clear" aria-label="Clear" style="font-size: 12px;"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
            CheckFilterClearorNot();
        }

        function CheckFilterClearorNot() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)
                && ($($($('#dropdownPriority').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)
                && ($($($('#dropdownStatus').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            //for rebind if any pending filter is present (Main Grid)
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoard('dropdownPriority', 'filtersstoryboardPriority', 'Priority');
            fCreateStoryBoard('dropdownStatus', 'filtersstoryboardStatus', 'status');
            CheckFilterClearorNot();
        };

        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownPriority").data("kendoDropDownTree").value([]);
            $("#dropdownStatus").data("kendoDropDownTree").value([]);
            $("#grid").data("kendoGrid").dataSource.filter({});
            $('#ClearfilterMain').css('display', 'none');
            e.preventDefault();
        }

        function FilterAll() {
            var locationlist = $("#dropdowntree").data("kendoDropDownTree")._values;
            var Statuslist = $("#dropdownStatus").data("kendoDropDownTree")._values;
            var prioritylist = $("#dropdownPriority").data("kendoDropDownTree")._values;

            if (locationlist.length > 0
                || Statuslist.length > 0
                || prioritylist.length > 0) {
                var finalSelectedfilter = { logic: "and", filters: [] };
                if (locationlist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(locationlist, function (i, v) {
                        locFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }
                if (Statuslist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(Statuslist, function (i, v) {
                        locFilter.filters.push({
                            field: "StatusID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }
                if (prioritylist.length > 0) {
                    var locFilter = { logic: "or", filters: [] };

                    $.each(prioritylist, function (i, v) {
                        locFilter.filters.push({
                            field: "PriorityID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(locFilter);
                }


                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }

        }


        function bindTaskType() {
            $("#dropdownTaskType").kendoDropDownList({
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    //Bindgrid();
                },
                dataSource: [
                    { text: "All", value: "All" },
                    { text: "Case", value: "C" },
                    { text: "Notice", value: "N" },
                    { text: "Individual Task", value: "T" }
                ]
            });
        }

        function BindPriority() {
            $("#dropdownPriority").kendoDropDownTree({
                placeholder: "Priority",
                checkboxes: {
                    checkChildren: true
                },
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    FilterAll();
                    fCreateStoryBoard('dropdownPriority', 'filtersstoryboardPriority', 'Priority');
                },
                dataSource: [
                    { text: "High", value: "1" },
                    { text: "Medium", value: "2" },
                    { text: "Low", value: "3" }
                ]
            });
        }


        function BindBranch() {
            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Select Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //checkAll: true,                
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterAll();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                           url: '<% =Path%>Litigation/GetAssignedLocationList?customerId=<% =CustId%>&UserID=<% =UId%>&FlagIsApp=<% =FlagIsApp%>',
                            //url: '<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Litigation/GetLocationList?customerId=<% =CustId%>"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
        }

        function BindStatus() {
            $("#dropdownStatus").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: {
                    checkChildren: true
                },
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    FilterAll();
                    fCreateStoryBoard('dropdownStatus', 'filtersstoryboardStatus', 'status');
                },
                dataSource: [
                    { text: "Pending/Open", value: "1" },
                    { text: "Submitted", value: "2" },
                    { text: "Disposed/Closed", value: "3" }
                ]
            });
            $("#dropdownStatus").data("kendoDropDownTree").value('<% =StatusFlagID%>');

        }

        function BindType() {
            $("#dropdownType").kendoDropDownList({
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    if ($("#dropdownType").val() == "1" || $("#dropdownType").val() == "2") {
                        window.location.href = "../aspxPages/CaseListNew.aspx";
                    }
                    else if ($("#dropdownType").val() == "4") {
                        window.location.href = "../aspxPages/RPACaseStatus.aspx";
                    }
                    else {
                        Bindgrid();
                    }
                },
                dataSource: [
                    { text: "Task", value: "3" },
                    { text: "Notice", value: "1" },
                    { text: "Case", value: "2" },
                    { text: "Draft", value: "4" }
                ]
            });

        }


        function BindPeriod() {
            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    //Bindgrid(); 
                },
                index: 4,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });
        }

        var record = 0;

        var caserefno = 0;

        function Bindgrid() {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/kendomyTaskReport?customerID=<% =CustId%>&loggedInUserID=<% =UId%>&loggedInUserRole=<%=FlagIsApp%>&taskStatus=-1&priority=-1&taskType=' + $("#dropdownTaskType").val() + '&MonthId=' + $("#dropdownPastData").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Litigation/kendomyTaskReport?customerID=<% =CustId%>&loggedInUserID=<% =UId%>&loggedInUserRole=<%=FlagIsApp%>&taskStatus=-1&priority=-1&taskType=' + $("#dropdownTaskType").val() + '&MonthId=' + $("#dropdownPastData").val(),
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBinding: function () {
                    record = 0;
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    if (this.ReminderStatus == 0) { template: "#=pending" }
                },
                columns: [
                    {
                        title: "Sr. No.",
                        template: "#= ++record #",
                        width: 70
                    },
                    {
                        field: "Priority", title: 'Priority',
                        width: "10.7%;",
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "TaskTitle", title: 'Title',
                        width: "22%;",
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: 'TaskDesc', title: 'Description',
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "20%",
                    },
                    {
                        field: "ScheduleOnDate", title: 'Due Date',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        template: "#= kendo.toString(kendo.parseDate(ScheduleOnDate, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        attributes: {
                            style: 'white-space: nowrap;'
                            //}, filterable: { multi: true, search: true }
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        },width:"12%"
                    },
                    {
                        field: "Status", title: 'Status',
                        width: "12%;",
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "AssignToName", title: 'Assigned To',
                        width: "16%;",
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    //{ field:"CustomerBranchID",title:"Customer Branch"},
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-eye", className: "ob-edit" }
                        ], title: "Action", lock:true,width:100, headerAttributes: {
                            style: "text-align: center;"
                        }
                    }
                ]
            });

            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Show Details";
                }
            });


            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                $('#divShowDialog').modal('show');
                $('#showdetails').attr('width', '100%');
                $('#showdetails').attr('height', '550px');
                $('.modal-dialog').css('width', '100%');
                var codeTitle = encodeURIComponent(item.TaskTitle);
                $('#showdetails').attr('src', "../aspxPages/TaskDetailPage.aspx?TaskID=" + item.TaskID + "&NID=" + item.NoticeCaseInstanceID + "&TaskNo=" + codeTitle + "&AssignedTo=" + item.AssignToName);

                return true;
            });
        }


        $(document).ready(function () {
            setactivemenu('leftreportsmenu');
            fhead('My Workspace/Task');
        });
        function ApplyBtnMainFilter(e) {
            Bindgrid();
            FilterAll();
            e.preventDefault();
        }


        function exportReportMain(e) {
            //location details
            var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
            var locationsdetails = [];
            $.each(list1, function (i, v) {
                locationsdetails.push(v);
            });

            //Status details
            var list2 = $("#dropdownStatus").data("kendoDropDownTree")._values;
            var Statusdetails = [];
            $.each(list2, function (i, v) {
                Statusdetails.push(v);
            });

            //Priority details
            var list3 = $("#dropdownPriority").data("kendoDropDownTree")._values;
            var prioritydetails = [];
            $.each(list3, function (i, v) {
                prioritydetails.push(v);
            });


            var UId = document.getElementById('UId').value;
            var customerId = document.getElementById('CustomerId').value;
            var PathName = document.getElementById('Path').value;
            var FlagIsApp = document.getElementById('FlagDetail').value;

            $.ajax({
                type: "GET",
                url: '' + PathName + '//LitigationExportReport/Report',
                data: {
                    UserId: UId,
                    CustomerID: customerId,
                    FlagIsApp: FlagIsApp,
                    MonthId: $("#dropdownPastData").val(),
                    FY: 0,
                    taskStatus: JSON.stringify(Statusdetails),
                    StartDateDetail: '',
                    EndDateDetail: '',
                    taskType: $("#dropdownTaskType").val(),
                    Risk: JSON.stringify(prioritydetails),
                    location: JSON.stringify(locationsdetails)

                },
                success: function (response) {
                    if (response != "Error" && response != "No Record Found" && response != "") {
                        window.location.href = '' + PathName + '/LitigationExportReport/GetFile?userpath=' + response + '';
                    }
                    if (response == "No Record Found") {
                        alert("No Record Found");
                    }
                }
            });
            e.preventDefault();
            return false;
        }

        function lnkAdd_Click(e) {
            $('#divShowAddTaskModal').modal('show');
            $('#IframeAddTask').attr('src', "../../Litigation/aspxPages/AddIndependentTask.aspx?NoticeCaseInstanceID=0&TaskID=0");
            e.preventDefault();
            return false;
        }
        function reloadTaskList() {
            //window.parent.location.reload(true); self.close();
            $('#divShowAddTaskModal').modal('hide');
            window.location.href = "../aspxPages/TaskListNew.aspx";
            e.preventDefault();

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div class="row">
        <input id="Path" type="hidden" value="<% =Path%>" />
        <input id="CustomerId" type="hidden" value="<% =CustId%>" />
        <input id="UId" type="hidden" value="<% =UId%>" />
        <input id="FlagDetail" type="hidden" value="<% =FlagIsApp%>" />

    </div>

    <div style="margin: 0.5% 0.8% 0.7%; width: 99%;">
        <input id="dropdownType" style="width: 15.5%; margin-right: 10px;" />
        <input id="dropdownTaskType" style="width: 15.5%; margin-right: 10px;" />
        <button id="lnkAdd" style="width: 6.2%; height: 30px; margin-right: 0.7%; float: right;" runat="server" onclick="lnkAdd_Click(event)">
            <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</button>
    </div>

    <div style="margin: 0.5% 0.8% 0; width: 99%;">
        <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 32%; margin-right: 0.8%;" />
        <input id="dropdownPriority" style="width: 16%; margin-right: 0.8%;" />
        <input id="dropdownStatus" style="width: 16%; margin-right: 0.8%;" />
        <input id="dropdownPastData" style="width: 16%; margin-right: 0.8%;" />
        <button id="exportReport" style="height: 32px" onclick="exportReportMain(event)" data-placement="bottom"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
        <button id="ApplyBtnMain" style="height: 31px; width: 7.7%; margin-left: 3px;" onclick="ApplyBtnMainFilter(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-1" style="width: 12%; float: right; margin-right: -21px; margin-top: 5px;">
                <button id="ClearfilterMain" style="display: none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
            </div>

        </div>
    </div>


    <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboard">&nbsp;</div>
    <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboardStatus">&nbsp;</div>
    <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboardPriority">&nbsp;</div>

    <div class="row" style="padding-top: 2px;">
        <div id="grid" style="margin-left: 10px; margin-right: 10px;"></div>
    </div>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeCaseModal();">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divShowAddTaskModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="height: 100%; width: 90%">
            <div class="modal-content" style="height: 100%;">
                <div class="modal-header">
                    <label class="modal-header-custom" id="Lawfirmmodel">
                        Add Task</label>
                    <button type="button" class="close" onclick="javascript:reloadTaskList();" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body" style="height: 100%;">
                    <iframe id="IframeAddTask" src="about:blank" width="100%" height="95%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
