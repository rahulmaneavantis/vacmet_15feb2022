﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon;
using System.Configuration;
using Amazon.S3.Model;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class TaskDetailPage : System.Web.UI.Page
    {
        public static string DocumentPath = "";

        protected bool showDiv = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["TaskID"]))
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["NID"]))
                    {
                        var taskID = Request.QueryString["TaskID"];
                        var NID = Request.QueryString["NID"];

                        if (taskID != "" && NID != "")
                        {
                            ViewState["TaskID"] = taskID;
                            ViewState["NoticeCaseInstanceID"] = NID;
                            //btnCloseTask.Visible = false;
                            showTaskDetail(Convert.ToInt32(taskID), Convert.ToInt32(NID));

                            //Re-Bind Responses Log Details
                            //BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]));
                        }
                    }
                }
                BindTaskStatusList();
            }

            //Show Hide Grid Control - Enable/Disable Form Controls
            if (ViewState["taskStatus"] != null)
            {
                if (Convert.ToInt32(ViewState["taskStatus"]) == 3)
                    enableDisableTaskControls(false);
                else
                    enableDisableTaskControls(true);
            }
            else
                enableDisableTaskControls(true);
            if (!string.IsNullOrEmpty(Request.QueryString["TaskSta"]))
            {
                var taskStatus = Request.QueryString["TaskSta"];
                if (taskStatus == "3")
                {
                    enableDisableTaskControls(false);
                    btnMoreDetails.Visible = false;
                }
            }
        }

        public void BindTaskResponses(int taskID)
        {
            try
            {
                clearTaskResponseControls();
                List<tbl_TaskResponse> lstTaskResponses = new List<tbl_TaskResponse>();


                List<dbo_NoticeTaskTransaction> lstTaskResponsesnew = new List<dbo_NoticeTaskTransaction>();
                

                lstTaskResponses = LitigationTaskManagement.GetTaskResponseDetailsnew(taskID);

                if (lstTaskResponses != null && lstTaskResponses.Count > 0)
                {
                    lstTaskResponses = lstTaskResponses.OrderByDescending(entry => entry.UpdatedOn).ThenByDescending(entry => entry.CreatedOn).ToList();
                }


                foreach (var item in lstTaskResponses)
                {
                    dbo_NoticeTaskTransaction TaskResponsesnew = new dbo_NoticeTaskTransaction();

                    tbl_LitigationFileData fileDetailnew = LitigationTaskManagement.GetUploadedFileName(Convert.ToInt32(ViewState["TaskID"]), Convert.ToInt32(item.ID));
                    TaskResponsesnew.ID = Convert.ToInt32(item.ID);
                    TaskResponsesnew.AssingeName = Request.QueryString["AssignedTo"];
                    TaskResponsesnew.StatusName = LitigationTaskManagement.GetStatus(item.StatusID);
                    TaskResponsesnew.CreatedByName = UserManagement.GetUserName(Convert.ToInt32(item.CreatedBy));
                    TaskResponsesnew.CreatedOn = item.CreatedOn;
                    TaskResponsesnew.StatusChangedOn = item.UpdatedOn;
                    TaskResponsesnew.Comments = item.Description;
                    TaskResponsesnew.FileName = fileDetailnew == null?null: fileDetailnew.FileName;
                    TaskResponsesnew.FileID = fileDetailnew == null ? 0 : fileDetailnew.ID;
                    TaskResponsesnew.TaskID = Convert.ToInt32(ViewState["TaskID"]);
                    if (TaskResponsesnew.FileID == 0)
                        showDiv = false;
                    else
                        showDiv = true;
                    lstTaskResponsesnew.Add(TaskResponsesnew);
                }

                //Comment by Vishal
                //grdTaskResponseLog.DataSource = lstTaskResponses;
                //grdTaskResponseLog.DataBind();
                grdTaskResponseLognew.DataSource = lstTaskResponsesnew;
                grdTaskResponseLognew.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskResponse.IsValid = false;
                cvTaskResponse.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static bool CanVisible(int FileID)
        {
            bool result = true;

            if (FileID == 0)
                result = false;
            return result;
        }

        public void clearTaskResponseControls()
        {
            try
            {
                //lblTaskDesc.Text = "";
                //tbxTaskResRemark.Text = "";
                tbxTaskResComment.Text = "";
                ddlStatus.SelectedIndex = -1;
                fuTaskResponseDocUploadnew.Attributes.Clear();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void enableDisableTaskControls(bool flag)
        {
            try
            {
                pnlTaskDetail.Enabled = flag;
                pnlTaskResponse.Enabled = flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveTaskResponse_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["TaskID"] != null)
                {
                    bool validateData = true;
                    bool saveSuccess = false;
                    long newResponseID = 0;
                    long taskID = 0;
                    taskID = Convert.ToInt32(ViewState["TaskID"]);

                    int userID = 0;

                    if (taskID != 0)
                    {
                        userID = AuthenticationHelper.UserID;

                        if (userID != 0)
                        {
                            //commenetd by VIshal
                            if (ddlStatus.SelectedIndex != 0)
                            {
                                validateData = true;
                            }
                            else
                            {
                                cvTaskResponse.IsValid = false;
                                cvTaskResponse.ErrorMessage = "Provide Response Status.";
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ScrollToDiv", "ScrollToDivTask();", true);
                                return;
                            }
                        }
                    }

                    if (validateData)
                    {
                        tbl_TaskResponse newRecord = new tbl_TaskResponse()
                        {

                            TaskID = taskID,
                            StatusID = Convert.ToInt32(ddlStatus.SelectedValue),
                            Description = tbxTaskResComment.Text.Trim(),
                            AssignedTo = userID,
                            RoleID = 0,
                            UpdatedOn = DateTime.Now,
                            IsActive = true,
                            CreatedBy = userID,
                            CreatedOn = DateTime.Now,
                        };

                        //commnetd vishal
                        //tbl_TaskResponse newRecord = new tbl_TaskResponse()
                        //{
                        //    IsActive = true,
                        //    TaskID = taskID,
                        //    ResponseDate = DateTime.Now,
                        //    CreatedBy = userID,
                        //    UserID = userID
                        //};
                        //commnetd vishal
                        //if (tbxTaskResDesc.Text != "")
                        //{
                        //    newRecord.Description = Regex.Replace(tbxTaskResDesc.Text,
                        //    @"^[^A-Z0-9_ -]+|[^A-Z0-9_ -]+$", "", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
                        //    newRecord.Description = Regex.Replace(newRecord.Description, @"\r\n?|\n", "");
                        //}

                        //if (tbxTaskResRemark.Text != "")
                        //{
                        //    newRecord.Remark = Regex.Replace(tbxTaskResRemark.Text,
                        //    @"^[^A-Z0-9_ -]+|[^A-Z0-9_ -]+$", "", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
                        //    newRecord.Remark = Regex.Replace(newRecord.Remark, @"\r\n?|\n", "");
                        //}
                        newResponseID = LitigationTaskManagement.CreateTaskTransactionLogNew(newRecord);

                        if (newResponseID > 0)
                            saveSuccess = true;
                    }

                    //commnetd by VIshal
                    //if (saveSuccess)
                    //{
                    //    var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                    //    if (AWSData != null)
                    //    {
                    //        #region AWS Document

                    //        if (fuTaskResponseDocUploadnew.HasFiles)
                    //        {
                    //            tbl_LitigationFileData objNoticeDoc = new tbl_LitigationFileData()
                    //            {
                    //                NoticeCaseInstanceID = taskID,
                    //                DocTypeInstanceID = newResponseID,
                    //                CreatedBy = AuthenticationHelper.UserID,
                    //                CreatedByText = AuthenticationHelper.User,
                    //                IsDeleted = false,
                    //                DocType = "TR"
                    //            };

                    //            HttpFileCollection fileCollection1 = Request.Files;

                    //            if (fileCollection1.Count > 0)
                    //            {
                    //                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                    //                int customerID = 0;
                    //                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    //                string directoryPath1 = "";

                    //                if (newResponseID > 0)
                    //                {

                    //                    directoryPath1 = "LitigationDocuments\\" + customerID + "\\TaskResponseDocs\\" + Convert.ToInt32(newResponseID);
                    //                    //directoryPath1 = Server.MapPath("~/LitigationDocuments/" + customerID + "/TaskResponseDocs/" + Convert.ToInt32(newResponseID));
                    //                    IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.APSouth1);
                    //                    S3DirectoryInfo di = new S3DirectoryInfo(client, AWSData.BucketName, directoryPath1);
                    //                    if (!di.Exists)
                    //                    {
                    //                        di.Create();
                    //                    }
                    //                    //DocumentManagement.CreateDirectory(directoryPath1);

                    //                    for (int i = 0; i < fileCollection1.Count; i++)
                    //                    {
                    //                        HttpPostedFile uploadedFile = fileCollection1[i];
                    //                        string[] keys1 = fileCollection1.Keys[i].Split('$');
                    //                        String fileName1 = "";
                    //                        if (keys1[keys1.Count() - 1].Equals("fuTaskResponseDocUpload"))
                    //                        {
                    //                            fileName1 = uploadedFile.FileName;
                    //                        }
                    //                        Stream fs = uploadedFile.InputStream;
                    //                        BinaryReader br = new BinaryReader(fs);
                    //                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    //                        string storagedrive = ConfigurationManager.AppSettings["AVACOM_LitigationTemp_Path"];

                    //                        string p_strPath = string.Empty;
                    //                        string dirpath = string.Empty;
                    //                        //p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + uploadedFile.FileName;
                    //                        //dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID;
                    //                        //if (File.Exists(p_strPath))
                    //                        //    File.Delete(p_strPath);

                    //                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                    //                        p_strPath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate + "/" + uploadedFile.FileName;
                    //                        dirpath = @"" + storagedrive + "/" + AuthenticationHelper.UserID + "/" + FileDate;


                    //                        if (!Directory.Exists(dirpath))
                    //                        {
                    //                            Directory.CreateDirectory(dirpath);
                    //                        }
                    //                        FileStream objFileStrm = File.Create(p_strPath);
                    //                        objFileStrm.Close();
                    //                        File.WriteAllBytes(p_strPath, bytes);

                    //                        Guid fileKey1 = Guid.NewGuid();
                    //                        if (uploadedFile.ContentLength > 0)
                    //                        {


                    //                            string AWSpath = directoryPath1 + "\\" + uploadedFile.FileName;
                    //                            objNoticeDoc.FileName = fileName1;
                    //                            objNoticeDoc.FilePath = directoryPath1.Replace(@"\", "/");
                    //                            objNoticeDoc.FileKey = fileKey1.ToString();

                    //                            objNoticeDoc.VersionDate = DateTime.UtcNow;
                    //                            objNoticeDoc.CreatedOn = DateTime.Now.Date;
                    //                            objNoticeDoc.FileSize = uploadedFile.ContentLength;


                    //                            var caseDocVersion = ExistsCaseDocumentReturnVersion(objNoticeDoc);
                    //                            caseDocVersion++;
                    //                            objNoticeDoc.Version = caseDocVersion + ".0";
                    //                            //if (!NoticeManagement.ExistsNoticeDocumentMapping(objNoticeDoc))
                    //                            //{
                    //                            FileInfo localFile = new FileInfo(p_strPath);
                    //                                S3FileInfo s3File = new S3FileInfo(client, AWSData.BucketName, AWSpath);
                    //                                if (!s3File.Exists)
                    //                                {
                    //                                    using (var s3Stream = s3File.Create()) // <-- create file in S3  
                    //                                    {
                    //                                        localFile.OpenRead().CopyTo(s3Stream); // <-- copy the content to S3  
                    //                                    }
                    //                                }
                    //                                //DocumentManagement.Litigation_SaveDocFiles(Filelist1);
                    //                                saveSuccess = NoticeManagement.CreateNoticeDocumentMapping(objNoticeDoc);
                    //                            //}
                    //                        }
                    //                    }//End For Each                                    
                    //                }
                    //            }
                    //        }

                    //        #endregion
                    //    }
                    //    else
                    //    {
                    //        //Save Notice Response Uploaded Documents
                    //        #region Upload Document

                    //        if (fuTaskResponseDocUploadnew.HasFiles)
                    //        {
                    //            tbl_LitigationFileData objNoticeDoc = new tbl_LitigationFileData()
                    //            {
                    //                NoticeCaseInstanceID = taskID,
                    //                DocTypeInstanceID = newResponseID,
                    //                CreatedBy = AuthenticationHelper.UserID,
                    //                CreatedByText = AuthenticationHelper.User,
                    //                IsDeleted = false,
                    //                DocType = "TR"
                    //            };

                    //            HttpFileCollection fileCollection1 = Request.Files;

                    //            if (fileCollection1.Count > 0)
                    //            {
                    //                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                    //                int customerID = 0;
                    //                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    //                string directoryPath1 = "";

                    //                if (newResponseID > 0)
                    //                {
                    //                    directoryPath1 = Server.MapPath("~/LitigationDocuments/" + customerID + "/TaskResponseDocs/" + Convert.ToInt32(newResponseID));

                    //                    DocumentManagement.CreateDirectory(directoryPath1);

                    //                    for (int i = 0; i < fileCollection1.Count; i++)
                    //                    {
                    //                        HttpPostedFile uploadedFile = fileCollection1[i];
                    //                        string[] keys1 = fileCollection1.Keys[i].Split('$');
                    //                        String fileName1 = "";
                    //                        if (keys1[keys1.Count() - 1].Equals("fuTaskResponseDocUpload"))
                    //                        {
                    //                            fileName1 = uploadedFile.FileName;
                    //                        }
                    //                        Guid fileKey1 = Guid.NewGuid();
                    //                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                    //                        Stream fs = uploadedFile.InputStream;
                    //                        BinaryReader br = new BinaryReader(fs);
                    //                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    //                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                    //                        if (uploadedFile.ContentLength > 0)
                    //                        {
                    //                            objNoticeDoc.FileName = fileName1;
                    //                            objNoticeDoc.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                    //                            objNoticeDoc.FileKey = fileKey1.ToString();
                    //                            //Get Document Version
                    //                            var caseDocVersion = ExistsCaseDocumentReturnVersion(objNoticeDoc);

                    //                            caseDocVersion++;
                    //                            objNoticeDoc.Version = caseDocVersion + ".0";
                    //                            objNoticeDoc.VersionDate = DateTime.UtcNow;
                    //                            objNoticeDoc.CreatedOn = DateTime.Now.Date;
                    //                            objNoticeDoc.FileSize = uploadedFile.ContentLength;
                    //                            DocumentManagement.Litigation_SaveDocFiles(Filelist1);
                    //                            saveSuccess = NoticeManagement.CreateNoticeDocumentMapping(objNoticeDoc);
                    //                        }
                    //                    }//End For Each                                    
                    //                }
                    //            }
                    //        }

                    //        #endregion
                    //    }

                    //    if (saveSuccess)
                    //    {
                    //        //Update Task Status to Submitted
                    //        saveSuccess = LitigationTaskManagement.UpdateTaskStatus(taskID, 2, userID, AuthenticationHelper.CustomerID); //Status 2 - Submitted

                    //        clearTaskResponseControls();

                    //        cvTaskResponse.IsValid = false;
                    //        cvTaskResponse.ErrorMessage = "Response Detail Save Successfully.";
                    //        cvTaskResponse.CssClass = "alert alert-success";
                    //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ScrollToDiv", "ScrollToDivTask();", true);
                    //    }

                    //    //Re-Bind Responses Log Details
                    //    BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]));
                    //}

                    if (saveSuccess)
                    {
                        //Save Notice Response Uploaded Documents
                        #region Upload Document

                        if (fuTaskResponseDocUploadnew.HasFiles)
                        {
                            tbl_LitigationFileData objNoticeDoc = new tbl_LitigationFileData()
                            {
                                NoticeCaseInstanceID = taskID,
                                DocTypeInstanceID = newResponseID,
                                CreatedBy = AuthenticationHelper.UserID,
                                CreatedByText = AuthenticationHelper.User,
                                IsDeleted = false,
                                DocType = "TR"
                            };

                            HttpFileCollection fileCollection1 = Request.Files;

                            if (fileCollection1.Count > 0)
                            {
                                List<KeyValuePair<string, Byte[]>> Filelist1 = new List<KeyValuePair<string, Byte[]>>();

                                int customerID = 0;
                                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                string directoryPath1 = "";

                                if (newResponseID > 0)
                                {
                                    directoryPath1 = Server.MapPath("~/LitigationDocuments/" + customerID + "/TaskResponseDocs/" + Convert.ToInt32(newResponseID));

                                    DocumentManagement.CreateDirectory(directoryPath1);

                                    for (int i = 0; i < fileCollection1.Count; i++)
                                    {
                                        HttpPostedFile uploadedFile = fileCollection1[i];
                                        string[] keys1 = fileCollection1.Keys[i].Split('$');
                                        String fileName1 = "";
                                        if (keys1[keys1.Count() - 1].Equals("fuTaskResponseDocUploadnew"))
                                        {
                                            fileName1 = uploadedFile.FileName;
                                        }
                                        Guid fileKey1 = Guid.NewGuid();
                                        string finalPath1 = Path.Combine(directoryPath1, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                        Stream fs = uploadedFile.InputStream;
                                        BinaryReader br = new BinaryReader(fs);
                                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                        Filelist1.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                        if (uploadedFile.ContentLength > 0)
                                        {
                                            objNoticeDoc.FileName = fileName1;
                                            objNoticeDoc.FilePath = directoryPath1.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                            objNoticeDoc.FileKey = fileKey1.ToString();
                                            //Get Document Version
                                            var caseDocVersion = ExistsCaseDocumentReturnVersion(objNoticeDoc);

                                            caseDocVersion++;
                                            objNoticeDoc.Version = caseDocVersion + ".0";
                                            objNoticeDoc.VersionDate = DateTime.UtcNow;
                                            objNoticeDoc.CreatedOn = DateTime.Now.Date;
                                            objNoticeDoc.FileSize = uploadedFile.ContentLength;
                                            DocumentManagement.Litigation_SaveDocFiles(Filelist1);
                                            saveSuccess = NoticeManagement.CreateNoticeDocumentMapping(objNoticeDoc);
                                        }
                                    }//End For Each                                    
                                }
                            }
                        }

                        #endregion                        

                        if (saveSuccess)
                        {
                            //Update Contract Status
                            //if (ViewState["TaskType"] != null)
                            //{
                            //int taskType = Convert.ToInt32(ViewState["TaskType"]);
                            string statusName = ddlStatus.SelectedItem.ToString();//"Submitted";

                                int newStatusID = Convert.ToInt32(ddlStatus.SelectedValue);

                                //if (taskType == 4) //Review Task
                                //{
                                //    newStatusID = 3; //Review Completed
                                //    //roleID = 4;
                                //}
                                //else if (taskType == 6) //Approval Task
                                //{
                                //    newStatusID = 5;  //Approval Completed 
                                //    //roleID = 6;
                                //}

                                if (newStatusID != 0)
                                    //saveSuccess = LitigationTaskManagement.UpdateNoticeTaskStatus(Convert.ToInt32(AuthenticationHelper.CustomerID), taskID, statusName, newStatusID);

                                clearTaskResponseControls();

                                cvTaskResponse.IsValid = false;
                                cvTaskResponse.ErrorMessage = "Task Response Saved Successfully.";
                                vsTaskResponse.CssClass = "alert alert-success";

                                #region Sent Email to Owner and Task Creater

                                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                                {

                                    var query1 = (from row in entities.tbl_TaskResponse
                                                  where row.TaskID == taskID
                                                  select row).FirstOrDefault();

                                    var query = (from row in entities.tbl_TaskResponse
                                                 
                                                 select row).FirstOrDefault();


                                    string emailTemplate = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Contract_Assign_Owner;
                                    string accessURL = string.Empty;
                                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                    if (Urloutput != null)
                                    {
                                        accessURL = Urloutput.URL;
                                    }
                                    else
                                    {
                                        accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                    }
                                    //string accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                    string customerName = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));
                                    var userquery = UserManagement.GetByID(userID);
                                    var userquery1 = UserManagement.GetByID(Convert.ToInt32(query1.CreatedBy));
                                    string username = string.Empty;
                                    string senderEmail = string.Empty;
                                    string senderEmail1 = string.Empty;
                                    if (userquery != null)
                                    {
                                        username = string.Format("{0} {1}", userquery.FirstName, userquery.LastName);

                                        senderEmail = userquery.Email;
                                        senderEmail1 = userquery1.Email;

                                    }
                                    string message = emailTemplate
                                                    .Replace("@User", username)
                                                    //.Replace("@ContractNo", query.ContractNo)
                                                    .Replace("@Title", query1.Description)
                                                    .Replace("@AccessURL", accessURL)
                                                    .Replace("@From", "Team " + customerName)
                                                    .Replace("@PortalURL", accessURL);

                                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { senderEmail, senderEmail1 }), null, null, "Notice Task Notification-Task Status", message);

                                }
                                #endregion
                            //}
                       }

                        //Re-Bind Responses Log Details
                        BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static int ExistsCaseDocumentReturnVersion(tbl_LitigationFileData newDocumentRecord)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                var query = (from row in entities.tbl_LitigationFileData
                             where row.FileName == newDocumentRecord.FileName
                             && row.NoticeCaseInstanceID == newDocumentRecord.NoticeCaseInstanceID
                             && row.DocType.Trim() == newDocumentRecord.DocType
                             && row.IsDeleted == false
                             select row).ToList();

                if (query.Count > 0)
                    return query.Count;
                else
                    return 0;
            }
        }

        protected void btnClearTaskResponse_Click(object sender, EventArgs e)
        {
            try
            {
                clearTaskResponseControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskResponseLog_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["TaskID"] != null)
                {
                    grdTaskResponseLognew.PageIndex = e.NewPageIndex;

                    BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskResponseLognew_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["TaskID"] != null)
                {
                    grdTaskResponseLognew.PageIndex = e.NewPageIndex;

                    BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int taskResponseID = Convert.ToInt32(commandArgs[0]);
                        int taskID = Convert.ToInt32(commandArgs[1]);

                        if (taskResponseID != 0 && taskID != 0)
                        {
                            var lstTaskResponseDocument = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID);

                            if (lstTaskResponseDocument.Count > 0)
                            {
                                var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                                if (AWSData != null)
                                {
                                    #region AWS
                                    using (ZipFile responseDocZip = new ZipFile())
                                    {
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                        string directoryPath = "~/TempDocuments/AWS/" + User;

                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                                        }
                                        foreach (var file in lstTaskResponseDocument)
                                        {
                                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                            {
                                                GetObjectRequest request = new GetObjectRequest();
                                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                                request.Key = file.FileName;
                                                GetObjectResponse response = client.GetObject(request);
                                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                            }
                                        }

                                        int i = 0;
                                        foreach (var file in lstTaskResponseDocument)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                int idx = file.FileName.LastIndexOf('.');
                                                string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                                if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                                }
                                                i++;
                                            }
                                        }

                                        var zipMs = new MemoryStream();
                                        responseDocZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] Filedata = zipMs.ToArray();
                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                        Response.BinaryWrite(Filedata);
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Normal
                                    using (ZipFile responseDocZip = new ZipFile())
                                    {
                                        int i = 0;
                                        foreach (var file in lstTaskResponseDocument)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                int idx = file.FileName.LastIndexOf('.');
                                                string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                                if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                                {
                                                    if (file.EnType == "M")
                                                    {
                                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    else
                                                    {
                                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                }
                                                i++;
                                            }
                                        }

                                        var zipMs = new MemoryStream();
                                        responseDocZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] Filedata = zipMs.ToArray();
                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                        Response.BinaryWrite(Filedata);
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                cvTaskResponse.IsValid = false;
                                cvTaskResponse.ErrorMessage = "No Document Available for Download.";
                                return;
                            }
                        }
                    }
                    else if (e.CommandName.Equals("ViewTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int taskResponseID = Convert.ToInt32(commandArgs[0]);
                        int taskID = Convert.ToInt32(commandArgs[1]);

                        var lstTaskDocument = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID);


                        if (lstTaskDocument != null)
                        {
                            var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                            if (AWSData != null)
                            {
                                #region AWS
                                List<tbl_LitigationFileData> entitiesData = lstTaskDocument.Where(entry => entry.Version != null).ToList();
                                if (lstTaskDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                                    entityData.Version = "1.0";
                                    entityData.NoticeCaseInstanceID = taskID;
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;
                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }
                                    rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptDocmentVersionView.DataBind();

                                    foreach (var file in lstTaskDocument)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(file.FileName);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessage.Text = "";
                                                lblMessage.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                DocumentPath = filePath1;
                                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                                lblMessage.Text = "";
                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                            }
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                        }
                                        break;
                                    }
                                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal
                                List<tbl_LitigationFileData> entitiesData = lstTaskDocument.Where(entry => entry.Version != null).ToList();
                                if (lstTaskDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                                    entityData.Version = "1.0";
                                    entityData.NoticeCaseInstanceID = taskID;
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in lstTaskDocument)
                                    {
                                        rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptDocmentVersionView.DataBind();
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);

                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            DocumentPath = FileName;

                                            DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                            lblMessage.Text = "";

                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                        }
                                        break;
                                    }
                                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                }
                                #endregion
                            }
                        }
                    }
                    else if (e.CommandName.Equals("DeleteTaskResponse"))
                    {
                        DeleteTaskResponse(Convert.ToInt32(e.CommandArgument)); //Parameter - TaskResponseID 

                        //Bind Task Responses
                        if (ViewState["TaskID"] != null)
                        {
                            BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]));
                        }
                    }

                    else if (e.CommandName.Equals("shareDoc"))
                    {


                        if (ViewState["TaskID"] != null)
                        {
                            int fID = Convert.ToInt32(e.CommandArgument);


                            int taskID = Convert.ToInt32(ViewState["TaskID"]);

                            int Fileid = GetTaskFileID(fID, taskID);

                            long CaseID = Convert.ToInt64(ViewState["NoticeCaseInstanceID"].ToString());

                            

                            List <int> fileIds = new List<int>();
                            fileIds.Add(Fileid);
                            var fileID = String.Join(",", fileIds);
                            string TaskNo = Request.QueryString["TaskNo"].ToString();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "showDocInfo", "OpenDocSharePopup('" + fileID + "','" + CaseID + "','" + TaskNo + "');", true);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskResponse.IsValid = false;
                cvTaskResponse.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


        protected void grdTaskResponseLognew_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("DownloadTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        int taskResponseID = Convert.ToInt32(commandArgs[0]);
                        int taskID = Convert.ToInt32(commandArgs[1]);

                        if (taskResponseID != 0 && taskID != 0)
                        {
                            var lstTaskResponseDocument = LitigationTaskManagement.GetTaskResponseDocumentsnew(taskResponseID);

                            if (lstTaskResponseDocument.Count > 0)
                            {
                                var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                                if (AWSData != null)
                                {
                                    #region AWS
                                    using (ZipFile responseDocZip = new ZipFile())
                                    {
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                        string directoryPath = "~/TempDocuments/AWS/" + User;

                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));
                                        }
                                        foreach (var file in lstTaskResponseDocument)
                                        {
                                            using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                            {
                                                GetObjectRequest request = new GetObjectRequest();
                                                request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                                request.Key = file.FileName;
                                                GetObjectResponse response = client.GetObject(request);
                                                response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                            }
                                        }

                                        int i = 0;
                                        foreach (var file in lstTaskResponseDocument)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                int idx = file.FileName.LastIndexOf('.');
                                                string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                                if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                                {
                                                    responseDocZip.AddEntry(file.CreatedByText + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                                }
                                                i++;
                                            }
                                        }

                                        var zipMs = new MemoryStream();
                                        responseDocZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] Filedata = zipMs.ToArray();
                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                        Response.BinaryWrite(Filedata);
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region Normal
                                    using (ZipFile responseDocZip = new ZipFile())
                                    {
                                        int i = 0;
                                        foreach (var file in lstTaskResponseDocument)
                                        {
                                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                            if (file.FilePath != null && File.Exists(filePath))
                                            {
                                                int idx = file.FileName.LastIndexOf('.');
                                                string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                                if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                                {
                                                    if (file.EnType == "M")
                                                    {
                                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    else
                                                    {
                                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                }
                                                i++;
                                            }
                                        }

                                        var zipMs = new MemoryStream();
                                        responseDocZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] Filedata = zipMs.ToArray();
                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                        Response.BinaryWrite(Filedata);
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                cvTaskResponse.IsValid = false;
                                cvTaskResponse.ErrorMessage = "No Document Available for Download.";
                                return;
                            }
                        }
                    }
                    else if (e.CommandName.Equals("ViewTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long taskResponseID = Convert.ToInt32(commandArgs[0]);
                        int taskID = Convert.ToInt32(commandArgs[1]);

                        var lstTaskDocument = LitigationTaskManagement.GetTaskResponseDocumentsnew(taskResponseID);


                        if (lstTaskDocument.Count() > 0)
                        {
                            var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                            if (AWSData != null)
                            {
                                #region AWS
                                List<tbl_LitigationFileData> entitiesData = lstTaskDocument.Where(entry => entry.Version != null).ToList();
                                if (lstTaskDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                                    entityData.Version = "1.0";
                                    entityData.NoticeCaseInstanceID = taskID;
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;
                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }
                                    rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptDocmentVersionView.DataBind();

                                    foreach (var file in lstTaskDocument)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(file.FileName);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessage.Text = "";
                                                lblMessage.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                DocumentPath = filePath1;
                                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                                lblMessage.Text = "";
                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                            }
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                        }
                                        break;
                                    }
                                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal
                                List<tbl_LitigationFileData> entitiesData = lstTaskDocument.Where(entry => entry.Version != null).ToList();
                                if (lstTaskDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                                    entityData.Version = "1.0";
                                    entityData.NoticeCaseInstanceID = taskID;
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in lstTaskDocument)
                                    {
                                        rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                        rptDocmentVersionView.DataBind();
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);

                                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                                            if (!Directory.Exists(DateFolder))
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                                            }

                                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                            string FileName = DateFolder + "/" + User + "" + extension;

                                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                            BinaryWriter bw = new BinaryWriter(fs);
                                            if (file.EnType == "M")
                                            {
                                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            bw.Close();
                                            DocumentPath = FileName;

                                            DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                            lblMessage.Text = "";

                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                        }
                                        break;
                                    }
                                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                }
                                #endregion
                            }
                        }
                    }
                    else if (e.CommandName.Equals("DeleteTaskResponse"))
                    {
                        DeleteTaskResponse(Convert.ToInt32(e.CommandArgument)); //Parameter - TaskResponseID 

                        //Bind Task Responses
                        if (ViewState["TaskID"] != null)
                        {
                            BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]));
                        }
                    }

                    else if (e.CommandName.Equals("shareDoc"))
                    {


                        if (ViewState["TaskID"] != null)
                        {
                            int fID = Convert.ToInt32(e.CommandArgument);


                            int taskID = Convert.ToInt32(ViewState["TaskID"]);

                            int Fileid = GetTaskFileID(fID, taskID);

                            long CaseID = Convert.ToInt64(ViewState["NoticeCaseInstanceID"].ToString());



                            List<int> fileIds = new List<int>();
                            fileIds.Add(Fileid);
                            var fileID = String.Join(",", fileIds);
                            string TaskNo = Request.QueryString["TaskNo"].ToString();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "showDocInfo", "OpenDocSharePopup('" + fileID + "','" + CaseID + "','" + TaskNo + "');", true);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskResponse.IsValid = false;
                cvTaskResponse.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public int GetTaskFileID(int fID, int taskid)
        {
            int result;
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    result = (from row in entities.tbl_LitigationFileData
                              where row.DocTypeInstanceID == fID
                              && row.IsDeleted == false
                              && row.NoticeCaseInstanceID == taskid
                              select row.ID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskResponse.IsValid = false;
                cvTaskResponse.ErrorMessage = "Server Error Occured. Please try again.";
                result = 0;
            }
            return result;
        }
        

        protected void grdTaskResponseLog_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownloadTaskResDoc = (LinkButton) e.Row.FindControl("lnkBtnDownloadTaskResDoc");

            if (lnkBtnDownloadTaskResDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownloadTaskResDoc);
            }

            LinkButton lnkBtnDeleteTaskResponse = (LinkButton) e.Row.FindControl("lnkBtnDeleteTaskResponse");
            if (lnkBtnDeleteTaskResponse != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteTaskResponse);

                if (ViewState["taskStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["taskStatus"]) == 3)
                        lnkBtnDeleteTaskResponse.Visible = false;
                    else
                        lnkBtnDeleteTaskResponse.Visible = true;
                }
            }
        }


        protected void grdTaskResponseLognew_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            LinkButton lnkBtnDownloadTaskResDoc = (LinkButton)e.Row.FindControl("lnkBtnDownloadTaskResDoc");

            if (lnkBtnDownloadTaskResDoc != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDownloadTaskResDoc);
            }

            LinkButton lnkBtnDeleteTaskResponse = (LinkButton)e.Row.FindControl("lnkBtnDeleteTaskResponse");
            if (lnkBtnDeleteTaskResponse != null)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lnkBtnDeleteTaskResponse);

                if (ViewState["taskStatus"] != null)
                {
                    if (Convert.ToInt32(ViewState["taskStatus"]) == 3)
                        lnkBtnDeleteTaskResponse.Visible = false;
                    else
                        lnkBtnDeleteTaskResponse.Visible = true;
                }
            }
        }
        public void DeleteTaskResponse(int taskResponseID)
        {
            try
            {
                if (taskResponseID != 0)
                {
                    //Delete Response with Documents
                    if (LitigationTaskManagement.DeleteTaskResponseLognew(taskResponseID, AuthenticationHelper.UserID))
                    {
                        cvTaskResponse.IsValid = false;
                        cvTaskResponse.ErrorMessage = "Response Deleted Successfully.";
                    }
                    else
                    {
                        cvTaskResponse.IsValid = false;
                        cvTaskResponse.ErrorMessage = "Something went wrong, Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskResponse.IsValid = false;
                cvTaskResponse.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string ShowTaskResponseDocCount(long taskID, long taskResponseID)
        {
            try
            {
                var docCount = LitigationTaskManagement.GetTaskResponseDocuments(taskID, taskResponseID).Count;

                if (docCount == 0)
                    return "No Documents";
                else if (docCount == 1)
                    return "1 File";
                else if (docCount > 1)
                    return docCount + " Files";
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public string ShowTaskDocCount()
        {
            try
            {
                if (ViewState["TaskID"] != null && ViewState["NoticeCaseInstanceID"] != null)
                {
                    long taskID = Convert.ToInt32(ViewState["TaskID"]);
                    long noticeCaseInstanceID = Convert.ToInt32(ViewState["NoticeCaseInstanceID"]);

                    if (taskID != 0 || noticeCaseInstanceID != 0)
                    {
                        var docCount = LitigationTaskManagement.GetTaskDocumentsCount(taskID, noticeCaseInstanceID);

                        if (docCount == 0)
                            return "No Documents";
                        else if (docCount == 1)
                            return "1 File";
                        else if (docCount > 1)
                            return docCount + " Files";
                        else
                            return "";
                    }
                    else
                        return "";
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public void showTaskDetail(int taskID, int noticeCaseInstanceID)
        {
            try
            {
                var taskDetail = LitigationTaskManagement.GetNoticeCaseTaskViewDetails(noticeCaseInstanceID, taskID);
                LblTaskType.Text = string.Empty;
                if (taskDetail != null)
                {
                    if (taskDetail.CreatedBy == AuthenticationHelper.UserID)
                    {
                        //btnCloseTask.Visible = true;
                    }
                    lblNameTag.Visible = true;
                    if (string.IsNullOrEmpty(taskDetail.NoticeOrCaseTitle))
                    {
                        lblNameTag.Visible = false;
                    }
                    lblNoticeOrCaseTitle.Text = taskDetail.NoticeOrCaseTitle;
                    lblTaskTitle.Text = taskDetail.TaskTitle;
                    lblTaskDueDate.Text = taskDetail.ScheduleOnDate.ToString("dd-MM-yyyy");
                    lblAssignBy.Text = taskDetail.CreatedByText;
                    lblPriority.Text = taskDetail.Priority;
                    lblTaskDesc.Text = taskDetail.TaskDesc;
                    lblTaskRemark.Text = taskDetail.Remark;
                    LblTaskType.Text = taskDetail.TaskType;
                    var docCount = ShowTaskDocCount();

                    if (taskDetail.TaskType.Trim() == "T")
                    {
                        btnMoreDetails.Visible = false;
                    }
                    else
                    {
                        btnMoreDetails.Visible = true;
                    }
                    lblTaskDocuments.Text = docCount;

                    if (docCount != "No Documents")
                    {
                        lnkBtnTaskDocuments.Visible = true;
                        lblTaskDocView.Visible = true;
                    }
                    else
                    {
                        lnkBtnTaskDocuments.Visible = false;
                        lblTaskDocView.Visible = false;
                    }

                    ViewState["taskStatus"] = Convert.ToInt32(taskDetail.StatusID);

                    //Bind Task Responses Log Details
                    BindTaskResponses(Convert.ToInt32(ViewState["TaskID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnDownloadTaskDoc_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["TaskID"] != null && ViewState["NoticeCaseInstanceID"] != null)
                {
                    long taskID = Convert.ToInt32(ViewState["TaskID"]);
                    long noticeCaseInstanceID = Convert.ToInt32(ViewState["NoticeCaseInstanceID"]);

                    var lstTaskDocument = LitigationTaskManagement.GetTaskDocuments(taskID, noticeCaseInstanceID);

                    if (lstTaskDocument.Count > 0)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS
                            using (ZipFile responseDocZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }
                                foreach (var file in lstTaskDocument)
                                {
                                    using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                    {
                                        GetObjectRequest request = new GetObjectRequest();
                                        request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                        request.Key = file.FileName;
                                        GetObjectResponse response = client.GetObject(request);
                                        response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                    }
                                }

                                int i = 0;
                                foreach (var file in lstTaskDocument)
                                {
                                    string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        int idx = file.FileName.LastIndexOf('.');
                                        string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                        if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                        }
                                        i++;
                                    }
                                }

                                var zipMs = new MemoryStream();
                                responseDocZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal
                            using (ZipFile responseDocZip = new ZipFile())
                            {
                                int i = 0;
                                foreach (var file in lstTaskDocument)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        //string[] filename = file.Name.Split('.');
                                        //string str = filename[0] + i + "." + filename[1];

                                        int idx = file.FileName.LastIndexOf('.');
                                        string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                        if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                        {
                                            if (file.EnType == "M")
                                            {
                                                responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                        }
                                        i++;
                                    }
                                }

                                var zipMs = new MemoryStream();
                                responseDocZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            }
                            #endregion
                        }
                    }
                    else
                    {
                        cvTaskResponse.IsValid = false;
                        cvTaskResponse.ErrorMessage = "No Document Available for Download.";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptDocmentVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    var AllinOneDocumentList = CaseManagement.GetCaseDocumentByID(Convert.ToInt32(commandArgs[2]));

                    if (AllinOneDocumentList != null)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                        if (AWSData != null)
                        {
                            #region AWS
                            if (AllinOneDocumentList.FilePath != null)
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;

                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + AllinOneDocumentList.FilePath;
                                    request.Key = AllinOneDocumentList.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + AllinOneDocumentList.FileName);
                                }
                                string filePath1 = directoryPath + "/" + AllinOneDocumentList.FileName;
                                DocumentPath = filePath1;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                lblMessage.Text = "";
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal
                            string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                            if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (AllinOneDocumentList.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                DocumentPath = FileName;
                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                lblMessage.Text = "";
                            }
                            else
                            {
                                lblMessage.Text = "There is no file to preview";
                                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptDocmentVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton) e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        protected void lblTaskDocView_Click(object sender, EventArgs e)
        {
            if (ViewState["TaskID"] != null && ViewState["NoticeCaseInstanceID"] != null)
            {
                long taskID = Convert.ToInt32(ViewState["TaskID"]);
                long noticeCaseInstanceID = Convert.ToInt32(ViewState["NoticeCaseInstanceID"]);

                var lstTaskDocument = LitigationTaskManagement.GetTaskDocuments(taskID, noticeCaseInstanceID);

                if (lstTaskDocument != null)
                {
                    var AWSData = AmazonS3.GetAWSStorageDetail(AuthenticationHelper.CustomerID);
                    if (AWSData != null)
                    {
                        #region AWS
                        List<tbl_LitigationFileData> entitiesData = lstTaskDocument.Where(entry => entry.Version != null).ToList();
                        if (lstTaskDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                            entityData.Version = "1.0";
                            entityData.NoticeCaseInstanceID = noticeCaseInstanceID;
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                            string directoryPath = "~/TempDocuments/AWS/" + User;
                            if (!Directory.Exists(directoryPath))
                            {
                                Directory.CreateDirectory(Server.MapPath(directoryPath));
                            }

                            rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptDocmentVersionView.DataBind();

                            foreach (var file in lstTaskDocument)
                            {
                                using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                {
                                    GetObjectRequest request = new GetObjectRequest();
                                    request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                    request.Key = file.FileName;
                                    GetObjectResponse response = client.GetObject(request);
                                    response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                }

                                string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string extension = System.IO.Path.GetExtension(file.FileName);
                                    if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                    {
                                        lblMessage.Text = "";
                                        lblMessage.Text = "Zip file can't view please download it";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReviewReviewerPopUp();", true);
                                    }
                                    else
                                    {
                                        string filePath1 = directoryPath + "/" + file.FileName;
                                        DocumentPath = filePath1;
                                        DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                        lblMessage.Text = "";
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                    }
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                }
                                break;
                            }
                            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                        }
                        #endregion
                    }
                    else
                    {
                        #region Normal
                        List<tbl_LitigationFileData> entitiesData = lstTaskDocument.Where(entry => entry.Version != null).ToList();
                        if (lstTaskDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                            entityData.Version = "1.0";
                            entityData.NoticeCaseInstanceID = noticeCaseInstanceID;
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            foreach (var file in lstTaskDocument)
                            {
                                rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                rptDocmentVersionView.DataBind();
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);

                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    DocumentPath = FileName;

                                    DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                    lblMessage.Text = "";

                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileReview();", true);
                                }
                                break;
                            }
                            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                        }
                        #endregion
                    }
                }
            }
        }

        protected void btnCloseTask_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["TaskID"]))
            {
                long TaskID = Convert.ToInt64(Request.QueryString["TaskID"]);
                long CustomerID = AuthenticationHelper.CustomerID;
                UpdateTaskStatus(TaskID, 3, CustomerID);
                enableDisableTaskControls(false);
            }
        }

        public static bool UpdateTaskStatus(long taskID, int statusID, long CustomerID)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    tbl_TaskScheduleOn taskToUpdate = entities.tbl_TaskScheduleOn.Where(x => x.ID == taskID && x.CustomerID == CustomerID).FirstOrDefault();

                    if (taskToUpdate != null)
                    {
                        taskToUpdate.StatusID = statusID;
                        taskToUpdate.UpdatedBy = AuthenticationHelper.UserID;
                        taskToUpdate.UpdatedOn = DateTime.Now;

                        if (statusID == 3)
                            taskToUpdate.URLExpired = true;

                        entities.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LitigationManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        protected void btnMoreDetails_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["NID"]))
            {
                if (!string.IsNullOrEmpty(LblTaskType.Text.Trim()))
                {
                    if (LblTaskType.Text.Trim() == "N")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "OpenCaseNoticeHistoryPopup(" + Convert.ToString(Request.QueryString["NID"]) + ",'N');", true);
                        // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "OpenRefPopUp", "OpenCaseNoticeHistoryPopup(" + Convert.ToString(Request.QueryString["NID"]) + "," +"Notice"+  ");", true);
                    }
                    if (LblTaskType.Text.Trim() == "C")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "OpenCaseNoticeHistoryPopup(" + Convert.ToString(Request.QueryString["NID"]) + ",'C');", true);
                        // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "OpenRefPopUp", "OpenCaseNoticeHistoryPopup(" + Convert.ToString(Request.QueryString["NID"]) + "," + "Case" + ");", true);
                    }
                }
            }
        }

        protected void lnkBtnShareDocument_Click(object sender, EventArgs e)
        {
            List<int> fileIds = new List<int>();
            if (grdTaskResponseLognew != null)
            {
                for (int i = 0; i < grdTaskResponseLognew.Rows.Count; i++)
                {
                    if (grdTaskResponseLognew.Rows[i].RowType == DataControlRowType.DataRow)
                    {
                        int newFileID = -1;
                        int FileId = -1; 
                        bool flagisallow = false;
                        CheckBox chkRow = (CheckBox)grdTaskResponseLognew.Rows[i].FindControl("chkRow");

                        if (chkRow != null)
                        {
                            if (chkRow.Checked)
                            {
                                Label lblSelectedFileID = (Label)grdTaskResponseLognew.Rows[i].FindControl("lblSelectedFileID");
                                if (lblSelectedFileID != null)
                                {
                                    int taskID = Convert.ToInt32(ViewState["TaskID"]);
                                    FileId = Convert.ToInt32(lblSelectedFileID.Text);
                                    newFileID = GetTaskFileID(FileId, taskID);
                                    
                                    fileIds.Add(newFileID);
                                }
                            }
                            //else
                            //{
                            //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please selet atleast one document to share.');", true);

                            //}
                        }
                    }
                }
            }

            if (fileIds.Count > 0)
            {
                long CaseID = Convert.ToInt64(ViewState["TaskID"].ToString());
                var fileID = String.Join(",", fileIds);
                string TaskNo = Request.QueryString["TaskNo"].ToString();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "showDocInfo", "OpenDocSharePopup('" + fileID + "','" + CaseID + "','" + TaskNo + "');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "showDocInfo", "OpenNoDocSharePopup();", true);
            }
        }
        private void BindTaskStatusList()
        {
            try
            {
                ddlStatus.DataSource = null;
                ddlStatus.DataBind();
                ddlStatus.ClearSelection();

                ddlStatus.DataTextField = "NoticeTaskResponseStatus";
                ddlStatus.DataValueField = "ID";

                var statusList = LitigationTaskManagement.GetStatusList_All();

                bool IsLawyer = LitigationUserManagement.IsExternalLawyer(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID);


                if (IsLawyer == true)
                {
                    statusList = statusList.Where(x => x.ID == 1 || x.ID == 4).ToList();
                }
                else
                {
                    statusList = statusList.Where(x => x.ID == 2 || x.ID == 3).ToList();
                }

                //List<Cont_tbl_StatusMaster> allowedStatusList = null;

                //List<int> finalStatusIDs = ContractTaskManagement.GetFinalStatusByInitialID(statusID, roleID);
                //allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains((int)entry.ID)).OrderBy(entry => entry.StatusName).ToList();

                ddlStatus.DataSource = statusList;
                ddlStatus.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskResponse.IsValid = false;
                cvTaskResponse.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        
        protected void grdTaskResponseLognew_RowDataBound1(object sender, GridViewRowEventArgs e)
        {
            var linkButton = (LinkButton)e.Row.FindControl("lnkBtnDownloadTaskResDoc");
            if (linkButton != null)
            {
                
            }
        }
    }

    public partial class dbo_NoticeTaskTransaction
    {
        public int ID { get; set; }
        public int TaskID { get; set; }
        public int NoticeTaskInstanceID { get; set; }
        public Nullable<int> StatusID { get; set; }

        public string StatusName { get; set; }
        public string Comments { get; set; }
        public long AssignedTo { get; set; }
        public string AssingeName { get; set; }


        public int? FileID { get; set; }
        public string FileName { get; set; }



        public Nullable<int> RoleID { get; set; }
        public Nullable<System.DateTime> StatusChangedOn { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public string CreatedByName { get; set; }

        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    }
}