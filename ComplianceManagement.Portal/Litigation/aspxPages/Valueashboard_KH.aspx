﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Valueashboard_KH.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.Valueashboard_KH" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Total Liability </title>
    <style type="text/css">
        body, div, table, thead, tbody, tfoot, tr, th, td, p {
            font-family: "Calibri !important";
            text-align: right !important;
        }

        a.comment-indicator:hover + comment {
            background: #ffd;
            position: absolute;
            display: block;
            border: 1px solid #ceced2;
            padding: 0.5em;
        }

        a.comment-indicator {
            background: red;
            display: inline-block;
            border: 1px solid #ceced2;
            width: 0.5em;
            height: 0.5em;
        }

        comment {
            display: none;
        }

        .modal-header {
            background-color: #fff !important;
        }

        .modal-body {
            background-color: #fff !important;
        }

        table,
        th,
        td {
            padding: 1px;
            border: 1px solid #ceced2;
            border-collapse: collapse;
        }

        td {
            padding: 5px 10px 5px 10px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div id="Grid">
            <div>
                <asp:LinkButton Text="Apply" runat="server" ID="btnExport1" OnClick="ExportToExcel"  style="width:80%;margin-top: 5px;" data-toggle="tooltip" Visible="false" ToolTip="Export to Excel">
                   <img src="../../Images/Excel _icon.png" alt="Export to Excel" title="Export to Excel" /> 
                </asp:LinkButton>
                <asp:LinkButton Text="Apply" runat="server" ID="LinkButton1" OnClick="ExportToExcel1" Style="width:80%;margin-top: 5px;" data-toggle="tooltip"><img src="../../Images/Excel _icon.png" alt="Export to Excel" title="Export to Excel" /> 
                </asp:LinkButton>
            </div>

            <table id="tbl" runat="server" cellspacing="0" border="0" style="border-collapse: collapse;">
                <colgroup style="width:275px"></colgroup>
                <colgroup style="width:59px"></colgroup>
                <colgroup style="width:64px"></colgroup>
                <colgroup style="width:98px"></colgroup>
                <colgroup style="width:93px"></colgroup>
                <tr>
                    <td rowspan="2" style="padding: 5px 10px 5px 10px; border-collapse: collapse; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; border-collapse: collapse; text-align: left !important; background: #168598;" height="30" align="left" valign="middle"><b><font face="Calibri " color="#FFF">Division</font></b></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-collapse: collapse; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; text-align: center !important;" colspan="4" align="center" valign="middle" bgcolor="#19BFDB"><font face="Calibri" color="#FFF">Figures in Mn INR</font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-collapse: collapse; width: 190px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; border-collapse: collapse; text-align: center !important; background: #168598;" align="left" valign="middle" rowspan="2"><b><font face="Calibri" color="#FFF">No of active cases</font></b></td>
                </tr>
                <tr>
                    <td style="height: 40px; padding: 5px 10px 5px 10px; width: 190px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; border-collapse: collapse; text-align: center !important; background: #168598;" align="left" valign="left"><b><font face="Calibri " color="#FFF">Recovery Amount</font></b></td>
                     <td style="height: 30px; padding: 5px 10px 5px 10px; width: 150px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; border-collapse: collapse; text-align: center !important; background: #168598;" align="left" valign="middle"><b><font face="Calibri" color="#FFF">Claim Amount</font></b></td>
                    <td style="height: 20px; padding: 5px 10px 5px 10px; width: 190px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; border-collapse: collapse; text-align: center !important; background: #168598;" align="left" valign="middle"><b><font face="Calibri " color="#FFF">Provisional Amount</font></b></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; width: 150px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; border-collapse: collapse; text-align: center !important; background: #168598;" align="left" valign="middle"><b><font face="Calibri" color="#FFF">Protest Money</font></b></td>
                   
                </tr>

              <tr>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-collapse: collapse; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; text-align: left !important; background: #EBF7F9;" height="20" colspan="6" align="left" valign="middle"><b><font face="Calibri" color="#000000">Direct Tax (DT)</font></b></td>
                </tr>
                <tr>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; width: 400px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; text-align: left !important;" height="20" align="left" valign="middle"><font face="Calibri" color="#000000">Income Tax - Corporate Office, Mumbai</font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="1000" sdnum="1033;0;0"><font face="Calibri" color="#000000"> <div id="DivREINTAXCor" style="padding:5px 10px 5px 10px; text-align:right;" runat="server">0</div></font></td>
                    <td style="height: 20px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="0" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVTLINTAXCor" style="padding:5px 10px 5px 10px; text-align:right;" runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="1" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DivPBINTAXCor"  style="padding:5px 10px 5px 10px; text-align:right;" runat="server">0</div></font></td>
                    <td style="height: 20px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="0" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DivPDINTAXCor" style="padding:5px 10px 5px 10px; text-align:right;" runat="server">0</div></font></td> 
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="0" sdnum="1033;"><font face="Calibri" color="#000000"><div id="DivNOACINTAXCor" style="padding:5px 10px 5px 10px; text-align:right;" runat="server">0</div></font></td>
                </tr>
                <tr>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; text-align: left !important;" height="20" align="left" valign="middle" bgcolor="#BDD7EE"><font face="Calibri" color="#000000">Total DT</font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#BDD7EE" sdval="1000" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVTDT_RE" style="padding:5px 10px 5px 10px; text-align:right;" runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#BDD7EE" sdval="0" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVTDT_TL" style="padding:5px 10px 5px 10px; text-align:right;" runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#BDD7EE" sdval="1" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVTDT_PB"  style="padding:5px 10px 5px 10px; text-align:right;" runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#BDD7EE" sdval="0" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVTDT_PD" style="padding:5px 10px 5px 10px; text-align:right;" runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#BDD7EE" sdval="0" sdnum="1033;"><font face="Calibri" color="#000000"><div id="DIVTDT_NOAC" style="padding:5px 10px 5px 10px; text-align:right;" runat="server">0</div></font></td>
                </tr>
                <%-- <tr>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; text-align: left !important; background: #EBF7F9;" colspan="6" height="20" align="left" valign="middle"><b><font face="Calibri" color="#000000">Indirect  Tax (IDT)</font></b></td>
                  </tr>

                <tr>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; text-align: left !important;" height="20" align="left" valign="middle"><font face="Calibri" color="#000000">Legrand</font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="90" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVLegrand_RE" style="padding:5px 10px 5px 10px; text-align:right;" runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="3" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVLegrand_TL" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="2" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVLegrand_PB" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="3" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVLegrand_PD" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="2" sdnum="1033;"><font face="Calibri" color="#000000"><div id="DIVLegrand_NOAC" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                </tr>
                <tr>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; text-align: left !important;" height="20" align="left" valign="middle"><font face="Calibri" color="#000000">Indoasian</font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="350" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVIndoasian_RE" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="0" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVIndoasian_TL" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="6" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVIndoasian_PB" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="0" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVIndoasian_PD" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="7" sdnum="1033;"><font face="Calibri" color="#000000"><div id="DIVIndoasian_NOAC" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                </tr>
                <tr>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; text-align: left !important;" height="20" align="left" valign="middle"><font face="Calibri" color="#000000">Numeric</font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="11" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVNumeric_RE" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="12" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVNumeric_TL" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                     <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="2" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVNumeric_PB" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="12" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVNumeric_PD" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="12" sdnum="1033;"><font face="Calibri" color="#000000"><div id="DIVNumeric_NOAC" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                </tr>
                <tr>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; text-align: left !important;" height="20" align="left" valign="middle"><font face="Calibri" color="#000000">Adlec</font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="20" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVAdlec_RE" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="9" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVAdlec_TL" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                     <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="8" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVAdlec_PB" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="9" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVAdlec_PD" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="6" sdnum="1033;"><font face="Calibri" color="#000000"><div id="DIVAdlec_NOAC" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                </tr>
                <tr>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; text-align: left !important;" height="20" align="left" valign="middle" bgcolor="#BDD7EE"><font face="Calibri" color="#000000">Total IDT</font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#BDD7EE" sdval="1361" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVTotalIDT_RE" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#BDD7EE" sdval="48" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVTotalIDT_TL" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>                  
                      <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#BDD7EE" sdval="28" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVTotalIDT_PB" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#BDD7EE" sdval="48" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVTotalIDT_PD" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#BDD7EE" sdval="40" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVTotalIDT_NOAC" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                </tr>--%>
             <%--   <tr>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; text-align: left !important;" height="20" align="left" valign="middle" bgcolor="#D8E4BC"><font face="Calibri" color="#000000">Total (DT + IDT)</font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#D8E4BC" sdval="1415" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVIDT_DT_RE" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#D8E4BC" sdval="77" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVIDT_DT_TL" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                     <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#D8E4BC" sdval="126" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVIDT_DT_PB" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#D8E4BC" sdval="77" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVIDT_DT_PD" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#D8E4BC" sdval="100" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVIDT_DT_NOAC" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                </tr>--%>
             <tr>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; text-align: left !important;" height="20" align="left" valign="middle"><font face="Calibri" color="#000000">HR Cases</font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="54" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVHR_RE" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="29" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVHR_TL" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                     <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="98" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVHR_PB" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="29" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVHR_PD" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>

                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="60" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVHR_NOAC" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                </tr>
                <tr>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; text-align: left !important;" height="20" align="left" valign="middle"><font face="Calibri" color="#000000">Civil Cases</font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="12" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIV_Civil_RE" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                     <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="86" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIV_Civil_TL" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                     <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="39" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIV_Civil_PB"  style="padding:5px 10px 5px 10px; text-align:right;" runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="86" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIV_Civil_PD" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>

                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="27" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIV_Civil_NOAC" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                </tr>
                <tr>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; text-align: left !important;" height="20" align="left" valign="middle"><font face="Calibri" color="#000000">Criminal Cases</font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="18" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIV_Criminal_RE" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                   <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="81" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIV_Criminal_TL" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                     <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="98" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIV_Criminal_PB" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="81" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIV_Criminal_PD" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="45" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIV_Criminal_NOAC" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                </tr>
                <tr>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; text-align: left !important;" height="20" align="left" valign="middle"><font face="Calibri" color="#000000">Other Legal Cases</font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="23" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIV_Other_RE" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                                      <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="87" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIV_Other_TL" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                      <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="20" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIV_Other_PB" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="87" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIV_Other_PD" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>

                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" sdval="99" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIV_Other_NOAC" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                </tr>

                <tr>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; text-align: left !important; width: 600px;" height="20" align="left" valign="middle" bgcolor="#D8E4BC"><font face="Calibri" color="#000000">Total (HR + Civil + Criminal + Other Legal cases)</font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#D8E4BC" sdval="53" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVTotal_RE" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                   <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#D8E4BC" sdval="254" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVTotal_TL" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                     <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#D8E4BC" sdval="157" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVTotal_PB" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#D8E4BC" sdval="254" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVTotal_PD" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#D8E4BC" sdval="171" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVTotal_NOAC" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                </tr>
             
                <tr>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2; text-align: left !important;" height="20" align="left" valign="middle" bgcolor="#FFD700"><font face="Calibri" color="#000000">Total Liability</font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#FFD700" sdval="53" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVAllTotal_RE" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#FFD700" sdval="254" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVAllTotal_TL" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                     <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#FFD700" sdval="157" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVAllTotal_PB" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#FFD700" sdval="254" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVAllTotal_PD" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                   
                    <td style="height: 30px; padding: 5px 10px 5px 10px; border-top: 1px solid #ceced2; border-bottom: 1px solid #ceced2; border-left: 1px solid #ceced2; border-right: 1px solid #ceced2" align="center" valign="middle" bgcolor="#FFD700" sdval="171" sdnum="1033;0;0"><font face="Calibri" color="#000000"><div id="DIVAllTotal_NOAC" style="padding:5px 10px 5px 10px; text-align:right;"  runat="server">0</div></font></td>
                </tr>

            </table>
        </div>
    </form>
</body>
</html>
