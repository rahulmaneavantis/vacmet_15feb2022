﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class ApproveAdvocateBillStatus : System.Web.UI.Page
    {
        protected static int NoticeCaseInstanceID;
        protected static int AdvocateBillId;
        protected static bool IsRemarkCompulsary = false;
        protected static string Email;
        protected static string IViewState;
        protected static decimal InvoiceAmount;
        protected static string InvoiceNo;
        public static string DocumentPath = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InvoiceAmount = 0;
                AdvocateBillId = Convert.ToInt32(Request.QueryString["AdvocateBillId"]);
                NoticeCaseInstanceID = Convert.ToInt32(Request.QueryString["NoticeCaseInstanceID"]);
                Email = Convert.ToString(Request.QueryString["Email"]);
                InvoiceNo = Convert.ToString(Request.QueryString["InvoiceNo"]);
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["InvoiceAmount"]))){
                    InvoiceAmount = Convert.ToDecimal(Request.QueryString["InvoiceAmount"]);
                }
                else
                {
                    InvoiceAmount = 0;
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["IViewState"])))
                {
                    IViewState = Convert.ToString(Request.QueryString["IViewState"]);
                }
                else
                {
                    IViewState = "0";
                }
                GetAdvocateBillData();
                BinAdvDocuments(AdvocateBillId, NoticeCaseInstanceID);
                lnkCaseDetail_Click(sender, e);
            }
        }
        private void GetAdvocateBillData()
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                //var data = (from row in entities.tbl_CaseAdvocateBill
                //            where row.ID == AdvocateBillId && row.NoticeCaseInstanceID == NoticeCaseInstanceID
                //            select row).SingleOrDefault();

                long customerid = AuthenticationHelper.CustomerID;
                var data = (from row in entities.SP_LitigationCaseAdvocateBill(Convert.ToInt32(customerid))
                            where row.AdvocateBillId == AdvocateBillId && row.NoticeCaseInstanceID == NoticeCaseInstanceID
                            select row).SingleOrDefault();

                if (data != null)
                {
                    lblcasetitle.Text = data.Title;
                    lblcasedesc.Text = data.NoticeCaseDesc;
                    lbllocation.Text = data.BranchName;
                    lbldepartment.Text = data.Department;
                    lbllawfirm.Text = data.LawFirmName;
                    lblhearing.Text = data.HearingRef;
                    lblinvoiceamount.Text = Convert.ToString(data.InvoiceAmount);
                    lblinvoiceno.Text = data.InvoiceNo;
                    lblcurrency.Text = data.Currency;
                    tbxRemarks.Text = data.Remark;
                }

             }
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
                {
                    IsRemarkCompulsary = false;
                    List<string> UniqueMail = new List<string>();
                    string Status = "";

                    var data = (from row in entities.tbl_CaseAdvocateBill
                                where row.ID == AdvocateBillId && row.NoticeCaseInstanceID == NoticeCaseInstanceID
                                select row).FirstOrDefault();

                    var dataSp = (from row in entities.SP_LitigationCaseAdvocateBill(Convert.ToInt32(AuthenticationHelper.CustomerID))
                                  where row.AdvocateBillId == AdvocateBillId && row.NoticeCaseInstanceID == NoticeCaseInstanceID
                                  select row).FirstOrDefault();

                    var lstApprover1 = LitigationManagement.GetAllAdvBillApprovers("Approver 1", Convert.ToInt32(AuthenticationHelper.CustomerID)).ToList();

                    var lstApprover2 = LitigationManagement.GetAllAdvBillApprovers("Approver 2", Convert.ToInt32(AuthenticationHelper.CustomerID)).ToList();

                    var lstApprover3 = LitigationManagement.GetAllAdvBillApprovers("Approver 3", Convert.ToInt32(AuthenticationHelper.CustomerID)).ToList();


                    if (lstApprover1 != null)
                    {
                        foreach (var item in lstApprover1)
                        {
                            if (item.ApproverEmail == Email)
                            {
                                data.status = "Approved by Approver 1";

                                UniqueMail.Add(dataSp.Approver2Email);
                                Status = "You have recieved an Advocate Bill for Approval";
                            }
                        }
                    }

                    if (lstApprover2 != null)
                    {
                        foreach (var item in lstApprover2)
                        {
                            if (item.ApproverEmail == Email)
                            {
                                if (InvoiceAmount >= 300000)
                                {
                                    data.status = "Approved by Approver 1 and Approver 2";
                                    UniqueMail.Add(dataSp.Approver3Email);
                                    Status = "You have recieved an Advocate Bill for Approval";
                                }
                                else
                                {
                                    data.status = "Approved";
                                    if (data.CreatedBy != null)
                                    {
                                        UniqueMail.Add(UserManagement.GetEmailByUserID(Convert.ToInt32(data.CreatedBy)));
                                    }
                                    Status = "Advocate Bill is Approved";
                                }
                            }
                        }
                    }

                    if (lstApprover3 != null)
                    {
                        foreach (var item in lstApprover3)
                        {
                            if (item.ApproverEmail == Email)
                            {
                                if (InvoiceAmount >= 300000)
                                {
                                    data.status = "Approved";
                                    if (data.CreatedBy != null)
                                    {
                                        UniqueMail.Add(UserManagement.GetEmailByUserID(Convert.ToInt32(data.CreatedBy)));
                                    }
                                    Status = "Advocate Bill is Approved";
                                }
                            }
                        }
                    }

                    LogLibrary.WriteErrorLog("Check Advocate Bill");
                    data.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                    data.UpdatedOn = System.DateTime.Now;
                    data.Remark = tbxRemarks.Text;
                    entities.SaveChanges();
                    //tbxRemarks.Text = string.Empty;

                    try
                    {
                        tbl_CaseAdvocateBillAuditLog objBill = new tbl_CaseAdvocateBillAuditLog()
                        {
                            NoticeCaseInstanceID = NoticeCaseInstanceID,
                            IsDeleted = false,
                            InvoiceNo = data.InvoiceNo,
                            Remark = tbxRemarks.Text,
                            Status = "Approved",
                            ApprovedOrRejectedBy = Email,
                        };

                        objBill.CreatedOn = DateTime.Now;

                        entities.tbl_CaseAdvocateBillAuditLog.Add(objBill);
                        entities.SaveChanges();

                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }

                    try
                    {
                        LogLibrary.WriteErrorLog("Advocate Bill Email Start");
                        string accessURL = string.Empty;
                        
                        var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));
                        accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);

                        User AdvocateUserID = UserManagement.GetPersonalResponsibleDetailsByEmail((UniqueMail[0].Trim()), Convert.ToInt32(AuthenticationHelper.CustomerID));

                        string PageURL = accessURL + "/AdvocateBillApprovalMailAccess.aspx?NoticeCaseInstanceID=" + NoticeCaseInstanceID + "&AdvocateBillId=" + AdvocateBillId + "&Email=" + UniqueMail[0] + "&InvoiceAmount=" + dataSp.InvoiceAmount + "&UID=" + AdvocateUserID.ID + "&InvoiceNo=" + dataSp.InvoiceNo;
                        PageURL = PageURL.Replace(" ", "");

                        if (Status == "Advocate Bill is Approved")
                        {
                            PageURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                        }

                        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_AdvocateBill
                                                              .Replace("@CaseRefNo", dataSp.CaseNo)
                                                              .Replace("@CaseTitle", dataSp.Title)
                                                              .Replace("@Location", dataSp.BranchName)
                                                              .Replace("@InvoiceNo", dataSp.InvoiceNo)
                                                              .Replace("@InvoiceAmount", dataSp.InvoiceAmount.ToString())
                                                              .Replace("@HearingDate", dataSp.HearingRef)
                                                              .Replace("@HearingDescription", dataSp.NoticeCaseDesc)
                                                              .Replace("@CreatedBy", dataSp.CreatedBy)
                                                              .Replace("@AccessURL", Convert.ToString(PageURL)) //Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])
                                                              .Replace("@From", cname.Trim())
                                                              .Replace("@PortalURL", Convert.ToString(PageURL));

                        LitigationManagement.CreateAuditLog("C", Convert.ToInt32(NoticeCaseInstanceID), "tbl_CaseAdvocateBill", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Advocate Bill Approved.", true);
                       
                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(UniqueMail), null, null, "Litigation Notification  - " + Status, message);

                        LogLibrary.WriteErrorLog("Advocate Bill Email End");
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }

                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Advocate Bill Approved Successfully.";
                    ValidationSummary1.CssClass = "alert alert-success";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                IsRemarkCompulsary = true;
                List<string> UniqueMail = new List<string>();
                string Status = "";

                var data = (from row in entities.tbl_CaseAdvocateBill
                            where row.ID == AdvocateBillId && row.NoticeCaseInstanceID == NoticeCaseInstanceID
                            select row).FirstOrDefault();

                var dataSp = (from row in entities.SP_LitigationCaseAdvocateBill(Convert.ToInt32(AuthenticationHelper.CustomerID))
                              where row.AdvocateBillId == AdvocateBillId && row.NoticeCaseInstanceID == NoticeCaseInstanceID
                              select row).FirstOrDefault();

                if (tbxRemarks.Text != "" && tbxRemarks.Text!=null)
                {
                    if (!string.IsNullOrWhiteSpace(tbxRemarks.Text))
                    {
                        data.status = "Rejected";
                        Status = "Advocate Bill is Rejected";
                        data.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                        data.UpdatedOn = System.DateTime.Now;
                        data.Remark = tbxRemarks.Text;
                        entities.SaveChanges();
                        //tbxRemarks.Text = string.Empty;


                        try
                        {
                            tbl_CaseAdvocateBillAuditLog objBill = new tbl_CaseAdvocateBillAuditLog()
                            {
                                NoticeCaseInstanceID = NoticeCaseInstanceID,
                                IsDeleted = false,
                                InvoiceNo = data.InvoiceNo,
                                Remark = tbxRemarks.Text,
                                Status = "Rejected",
                                ApprovedOrRejectedBy = Email,
                            };

                            objBill.CreatedOn = DateTime.Now;

                            entities.tbl_CaseAdvocateBillAuditLog.Add(objBill);
                            entities.SaveChanges();

                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }


                        LitigationManagement.CreateAuditLog("C", Convert.ToInt32(NoticeCaseInstanceID), "tbl_CaseAdvocateBill", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Advocate Bill Rejected.", true);

                        string accessURL = string.Empty;
                        URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                        var cname = CustomerManagement.CustomerGetByIDName(Convert.ToInt32(AuthenticationHelper.CustomerID));
                        if (Urloutput != null)
                        {
                            accessURL = Urloutput.URL;
                        }
                        else
                        {
                            accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                        }

                        string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Litigation_AdvocateBill
                                                              .Replace("@CaseRefNo", dataSp.CaseNo)
                                                              .Replace("@CaseTitle", dataSp.Title)
                                                              .Replace("@Location", dataSp.BranchName)
                                                              .Replace("@InvoiceNo", dataSp.InvoiceNo)
                                                              .Replace("@InvoiceAmount", dataSp.InvoiceAmount.ToString())
                                                              .Replace("@HearingDate", dataSp.HearingRef)
                                                              .Replace("@CreatedBy", dataSp.CreatedBy)
                                                              .Replace("@HearingDescription", dataSp.NoticeCaseDesc)
                                                              .Replace("@AccessURL", Convert.ToString(accessURL)) //Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])
                                                              .Replace("@From", cname.Trim())
                                                              .Replace("@PortalURL", Convert.ToString(accessURL));

                        if (dataSp.OwnerID != null)
                        {
                            UniqueMail.Add(UserManagement.GetEmailByUserID(Convert.ToInt32(dataSp.OwnerID)));
                        }

                        try
                        {
                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(UniqueMail), null, null, "Litigation Notification  - " + Status, message);
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }

                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Advocate Bill Rejected Successfully.";
                        ValidationSummary1.CssClass = "alert alert-success";
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Remark is Compulsory.";
                        ValidationSummary1.CssClass = "alert alert-danger";
                    }

                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Remark is Compulsory.";
                    ValidationSummary1.CssClass = "alert alert-danger";
                }
               
            }
        }

        public void BinAdvDocuments(int AdvocateBillId, int NoticeCaseInstanceID)
        {
            var lstadvDocument = CaseManagement.GetCaseAdvBillDocuments(AdvocateBillId, NoticeCaseInstanceID, "CA");
            if (lstadvDocument.Count > 0)
            {
                grdAdvocateBillDocuments.DataSource = lstadvDocument;
                grdAdvocateBillDocuments.DataBind();
                divAdveditBilldocument.Visible = true;
            }
        }

        protected void grdAdvocateBillDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
            if (e.CommandName.Equals("DownloadAdvBillEditDocument"))
            {
                string commandArgs = e.CommandArgument.ToString();

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    var file = CaseManagement.ShowandDownloadAdvbillFileById(Convert.ToInt32(commandArgs));

                    if (file != null)
                    {

                        #region Normal 
                        using (ZipFile responseDocZip = new ZipFile())
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                int idx = file.FileName.LastIndexOf('.');
                                string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                {
                                    if (file.EnType == "M")
                                    {
                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                }
                            }


                            var zipMs = new MemoryStream();
                            responseDocZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] Filedata = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=AdvocateBillDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                            Response.BinaryWrite(Filedata);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                        }
                        #endregion

                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "No Document Available for Download.";
                        ValidationSummary1.CssClass = "alert alert-danger";
                        return;
                    }

                    // DownloadCaseDocument(Convert.ToInt32(e.CommandArgument)); //Parameter - ResponseID
                }
            }
            else if (e.CommandName.Equals("ViewAdvBillEditDocument"))
            {
                string commandArgs = e.CommandArgument.ToString();

                if (!string.IsNullOrEmpty(commandArgs))
                {
                    var file = CaseManagement.ShowandDownloadAdvbillFileById(Convert.ToInt32(commandArgs));

                    if (file != null)
                    {

                        #region Normal
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                        if (file.FilePath != null && File.Exists(filePath))
                        {
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                            string DateFolder = Folder + "/" + File;

                            string extension = System.IO.Path.GetExtension(filePath);

                            Directory.CreateDirectory(Server.MapPath(DateFolder));

                            if (!Directory.Exists(DateFolder))
                            {
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                            }

                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                            string FileName = DateFolder + "/" + User + "" + extension;

                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            if (file.EnType == "M")
                            {
                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            bw.Close();
                            DocumentPath = FileName;

                            DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                            lblMessage.Text = "";

                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenAdvBillDocviewer('" + DocumentPath + "');", true);
                        }
                        #endregion

                    }
                    else
                    {
                        lblMessage.Text = "There is no file to preview";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenAdvBilldocfileReview();", true);
                    }
                }
            }
        }
     
        protected void rptAdvBilldocumentview_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                // var AllinOneDocumentList=null;
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    int advbillid = Convert.ToInt32(commandArgs[0]);
                    int casedocumentid = Convert.ToInt32(commandArgs[2]);


                    var AllinOneDocumentList = CaseManagement.GetCaseAdvBillDocumentByID(advbillid, casedocumentid);
                    //var AllinOneDocumentList = CaseManagement.GetCaseAdvBillDocumentByID(Convert.ToInt32(e.CommandArgument));

                    if (AllinOneDocumentList != null)
                    {

                        #region Normal
                        string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                        if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                        {
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + File;
                            string extension = System.IO.Path.GetExtension(filePath);
                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                            if (!Directory.Exists(DateFolder))
                            {
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                            }
                            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                            string FileName = DateFolder + "/" + User + "" + extension;
                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            if (AllinOneDocumentList.EnType == "M")
                            {
                                bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            else
                            {
                                bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                            }
                            bw.Close();
                            DocumentPath = FileName;
                            DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenAdvBillDocviewer('" + DocumentPath + "');", true);
                            lblMessage.Text = "";
                        }
                        #endregion

                    }
                    else
                    {
                        lblMessage.Text = "There is no file to preview";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenAdvBilldocfileReview();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void rptAdvBilldocumentview_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblAdvDocumentVersionView = (LinkButton)e.Item.FindControl("lblAdvDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblAdvDocumentVersionView);
            }
        }

        protected void lnkCaseDetail_Click(object sender, EventArgs e)
        {
            liCaseDetail.Attributes.Add("class", "active");
            liAuditLog.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 0;
        }

        protected void lnkAuditLog_Click(object sender, EventArgs e)
        {
            liAuditLog.Attributes.Add("class", "active");
            liCaseDetail.Attributes.Add("class", "");
            MainView.ActiveViewIndex = 1;
            BindCaseAuditLogs();
        }
        protected void gvCaseAuditLog_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvCaseAuditLog.PageIndex = e.NewPageIndex;
                BindCaseAuditLogs();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindCaseAuditLogs()
        {
            try
            {
                if (NoticeCaseInstanceID != 0)
                {
                    var lstAuditLogs = LitigationManagement.GetAdvocateBillAuditLog(NoticeCaseInstanceID, InvoiceNo);

                    gvCaseAuditLog.DataSource = lstAuditLogs;
                    gvCaseAuditLog.DataBind();
                }
                else
                {
                    gvCaseAuditLog.DataSource = null;
                    gvCaseAuditLog.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}