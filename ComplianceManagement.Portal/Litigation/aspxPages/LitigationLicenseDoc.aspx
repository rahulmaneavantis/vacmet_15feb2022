﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LitigationLicenseDoc.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.LitigationLicenseDoc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

        <link href="../../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>    
    <link href="../../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../../NewCSS/timeline.css" rel="stylesheet" />
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
      <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../../Style/css/StyleSheetTextEditor.css" rel="stylesheet" />

      <script type="text/javascript">
        function fopenpopup() {
            $('#divContDocTypeDialog').modal('show');
        }
        //function OpenDocSharePopup(FID) {
        //    $('#divDocumentsharePopup').modal('show');
        //    $('#ContentPlaceHolder1_Iframe_Docshare').attr('src', "/ContractProduct/aspxPages/ShareDocInfo.aspx?AccessID=" + FID + "&CID=" + CID + "&EditFileID=" + ShareID);
        //}
        function OpenDocSharePopup(filePath) {
            $('#divDocumentsharePopup').modal('show');
            $('#Iframe_Docshare').attr('src', "../../docviewer.aspx?docurl=" + filePath);
            //$('#Iframe_Docshare').attr('src', "/ContractProduct/aspxPages/ShowDocInfo.aspx?AccessID=" + FID + "&CID=" + CID);
        }
        function CloseUploadShareDocumentPopup() {
            $('#divDocumentsharePopup').modal('hide');
            document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
        }

        function RefreshParent() {
            window.location.href = window.location.href;
        }
      </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            fhead('Shared Document Details');
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
     <div class="container" style="background-color: #f7f7f7;">
             <div class="row Dashboard-white-widget">
        <div class="dashboard">
            <div class="row">
                <asp:ValidationSummary ID="vsPageDocType" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                    ValidationGroup="PageDocTypeValidationGroup" />
                <asp:CustomValidator ID="cvPageDocType" runat="server" EnableClientScript="False"
                    ValidationGroup="PageDocTypeValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-6 colpadding0">
                        <asp:TextBox runat="server" ID="tbxFilter" AutoComplete="off" CssClass="form-control" MaxLength="50"
                            PlaceHolder="Type Name to Search" AutoPostBack="true" OnTextChanged="tbxFilter_TextChanged" Width="70%" />
                    </div>
                    <div class="col-md-4 colpadding0">
                        <asp:LinkButton ID="lnkBtn_RebindGrid" OnClick="lnkBtn_RebindGrid_Click" Style="display: none;" runat="server"></asp:LinkButton>
                    </div>
                </div>
            </div>

            <div class="row">
                <asp:GridView runat="server" ID="grdContDoctType" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" AllowSorting="true" GridLines="none" Width="100%"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" DataKeyNames="ID"
                    OnRowCreated="grdContDoctType_RowCreated" OnSorting="grdContDoctType_Sorting"
                    OnRowDataBound="grdContDoctType_RowDataBound"
                    OnRowCommand="grdContDoctType_RowCommand"
                    >
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr.No." ItemStyle-Width="5%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                                <%--<asp:Label ID="lblRowID" runat="server" Text='<%# Eval("RowID") %>'></asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Period" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblPeriodDetail" data-toggle="tooltip" ToolTip='<%# Eval("PeriodDetail") %>' runat="server" Text='<%# Eval("PeriodDetail") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="File Name" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblFileName" data-toggle="tooltip" ToolTip='<%# Eval("FileName") %>' runat="server" Text='<%# Eval("FileName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("StartDate") != DBNull.Value ? Convert.ToDateTime(Eval("StartDate")).ToString("dd-MM-yyyy") : "" %>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("StartDate") != DBNull.Value ? Convert.ToDateTime(Eval("StartDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="End Date" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("EndDate") != null ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'
                                        data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("EndDate") != null ? Convert.ToDateTime(Eval("EndDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEditContractDocType" runat="server" CommandName="EDIT_ContDocShare" ToolTip="View" data-toggle="tooltip"                                   
                                    CommandArgument='<%# Eval("ID")+","+Eval("AssignedDocUID") %>'>
                                     <img src='<%# ResolveUrl("~/img/view-doc.png")%>' alt="View" />
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkDeleteContractDocType" runat="server" CommandName="DownloadContDoc" ToolTip="Download" data-toggle="tooltip"
                                  CommandArgument='<%# Eval("ID")+","+Eval("AssignedDocUID") %>'
                                  Visible='<%# CanViewAndDownloadDoc((long)Eval("FileID"),(long)Eval("AssignedDocUID"),"Download") %>'>
                            <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerTemplate>
                        <table style="display: none">
                            <tr>
                                <td>
                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="row">
                <div class="col-md-12 colpadding0">
                    <div class="col-md-10 colpadding0">
                        <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                            <p style="padding-right: 0px !Important;">
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-1 colpadding0">
                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control" Style="width: 100%; float: right; margin-right: 6%;"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1 colpadding0" style="float: right;">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control" Width="100%" Height="30px">
                        </asp:DropDownListChosen>
                    </div>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </div>
            </div>
        </div>
    </div>
       <div class="modal fade" id="divDocumentsharePopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog p5" style="width: 82%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                            <label class="modal-header-custom">
                                </label>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                               <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                            <iframe src="about:blank" id="Iframe_Docshare" runat="server" width="100%" height="535px"></iframe>
                                        </fieldset>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </form>
</body>
</html>

