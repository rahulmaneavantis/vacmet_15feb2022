﻿<%@ Page Title="Notice :: My Workspace" Language="C#" MasterPageFile="~/LitigationMaster.Master" AutoEventWireup="true" CodeBehind="NoticeList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.NoticeList" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { };

        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            fhead('My Workspace/Notice');
        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $(document).on("click", function (event) {

            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });

function hideDivBranch() {
    $('#divBranches').hide("blind", null, 500, function () { });
}

function openNoticeModal() {
    $('#divAddNoticeModal').modal('show');
}

function closeNoticeModal() {
    document.getElementById('<%= lnkBtnBindGrid.ClientID %>').click();
}

function openAddNewActModal() {
    $('#divAddNewActModal').modal('show');
}

function ShowDialog(NoticeInstanceID) {
    $('#divShowDialog').modal('show');
    $('.modal-dialog').css('width', '95%');
    $('#showdetails').attr('width', '100%');
    $('#showdetails').attr('height', '550px');
    $('#showdetails').attr('src', "../aspxPages/NoticeDetailPage.aspx?AccessID=" + NoticeInstanceID);
};

function ClosePopNoticeDetialPage() {
    $('#divShowDialog').modal('hide');
}
    </script>

    <style type="text/css">
        .customDropDownCheckBoxCSS {
            height: 32px !important;
            width: 70%;
        }
            .AddNewspan1 {
    padding: 5px;
    border: 2px solid #f4f0f0;
    border-radius: 51px;
    display: inline-block;
    height: 26px;
    width: 26px;
}
    </style>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row Dashboard-white-widget">
        <div class="dashboard">

            <div class="col-md-12 colpadding0">
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="DepartmentPageValidationGroup" />
                <asp:CustomValidator ID="cvErrorNoticePage" runat="server" EnableClientScript="False"
                    ValidationGroup="NoticePageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div class="col-md-12 colpadding0">
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 9%; margin-right:10px;">
                    <asp:DropDownListChosen runat="server" ID="ddlTypePage" AllowSingleDeselect="false" DisableSearchThreshold="5" AutoPostBack="true"
                        DataPlaceHolder="Select Type" class="form-control" Width="100%" OnSelectedIndexChanged="ddlTypePage_SelectedIndexChanged">
                        <asp:ListItem Text="Notice" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Case" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Task" Value="3"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 9%; margin-right:10px;">
                    <asp:DropDownListChosen runat="server" ID="ddlStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select Status" class="form-control" Width="100%" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                        <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                        <asp:ListItem Text="Open" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Closed" Value="3"></asp:ListItem>
                      <%--  <asp:ListItem Text="Settled" Value="4"></asp:ListItem>--%>
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 15%; margin-right:10px;">
                    <asp:DropDownListChosen runat="server" ID="ddlNoticeTypePage" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select Status" class="form-control" Width="100%">
                        <asp:ListItem Text="All" Value="B"></asp:ListItem>
                        <asp:ListItem Text="Inward/Defendant" Value="I"></asp:ListItem>
                        <asp:ListItem Text="Outward/Plaintiff" Value="O"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 13%; margin-right:10px;">
                    <asp:DropDownListChosen runat="server" ID="ddlPartyPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                        DataPlaceHolder="Select Opponent" class="form-control" Width="100%" />
                </div>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 13%; margin-right:10px;">
                    <asp:DropDownListChosen runat="server" ID="ddlDeptPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                        DataPlaceHolder="Select Department" class="form-control" Width="100%" />
                </div>

                <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                    <ContentTemplate>
                        <div class="col-md-4 colpadding0 entrycount" style="margin-top: 5px; width: 23%;">
                            <asp:TextBox runat="server" ID="tbxFilterLocation" PlaceHolder="Select Entity/Branch/Location" autocomplete="off" CssClass="clsDropDownTextBox" />
                            <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px; width: 95%" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                                    Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                    OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                </asp:TreeView>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 13%; float: right">
                    <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" ID="btnAddNotice" Style="float: right" OnClick="btnAddNotice_Click" data-toggle="tooltip" ToolTip="Add New Notice">
                        <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</asp:LinkButton>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-12 colpadding0">
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 18.8%; margin-right:10px;">
                    <asp:DropDownListChosen runat="server" ID="ddlType" DataPlaceHolder="Select Type" AllowSingleDeselect="false"
                        class="form-control m-bot15" Width="100%" Height="30px">
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-3 colpadding0" style="margin-top: 5px; width: 15%; margin-right:10px;">
                    <asp:DropDownListChosen runat="server" ID="ddlOwnerlist" DataPlaceHolder="Select Owner" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        CssClass="form-control" Width="100%" />
                </div>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 13%; margin-right:10px;">
                    <asp:DropDownListChosen runat="server" ID="ddlFinancialYear" AllowSingleDeselect="false" DisableSearchThreshold="3"
                        DataPlaceHolder="Select Financial Year" class="form-control" Width="100%" >
                </asp:DropDownListChosen>
                </div>


                <div class="col-md-3 colpadding0" style="margin-top: 5px; width:  29.8%; margin-right:10px;">
                    <asp:TextBox runat="server" ID="tbxtypeTofilter" AutoComplete="off" Width="100%" placeholder="Type to Search" CssClass="form-control" />
                </div>

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 13.5%;float:right">
                    <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lnkBtnApplyFilter" OnClick="lnkBtnApplyFilter_Click" style="width:75px;float:right" data-toggle="tooltip" ToolTip="Apply" data-placement="bottom" />
                    <asp:LinkButton runat="server" ID="btnExport" Style="margin-top: 5px;float:right" OnClick="btnExport_Click"  data-toggle="tooltip" ToolTip="Export to Excel" data-placement="bottom">
                            <img src="../../Images/Excel _icon.png" alt="Export to Excel"/> 
                            </asp:LinkButton>
                     </div>
            </div>
            <div class="clearfix"></div>

            <div class="col-md-12 colpadding0">
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                    <asp:LinkButton ID="lnkBtnBindGrid" OnClick="lnkBtnBindGrid_Click" Style="float: right; display: none;" Width="100%" runat="server">
                    </asp:LinkButton>
                </div>
            </div>

            <div style="margin-bottom: 4px">
                <asp:GridView runat="server" ID="grdNoticeDetails" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="NoticeInstanceID"
                    OnRowCommand="grdNoticeDetails_RowCommand" OnRowDataBound="grdNoticeDetails_RowDataBound" OnSorting="grdNoticeDetails_Sorting" OnRowCreated="grdNoticeDetails_RowCreated">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Type" ItemStyle-Width="8%" SortExpression="NoticeTypeName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("NoticeTypeName") %>' ToolTip='<%# Eval("NoticeTypeName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Ref. No." ItemStyle-Width="20%" SortExpression="RefNo">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("RefNo") %>' ToolTip='<%# Eval("RefNo") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Notice" ItemStyle-Width="27%" SortExpression="NoticeTitle">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("NoticeTitle") %>' ToolTip='<%# Eval("NoticeTitle") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="10%" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("NoticeDetailDesc") %>' ToolTip='<%# Eval("NoticeDetailDesc") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Opponent" ItemStyle-Width="15%" SortExpression="PartyName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("PartyName") %>' ToolTip='<%# Eval("PartyName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Entity" ItemStyle-Width="20%" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("BranchName") %>' ToolTip='<%# Eval("BranchName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Department" ItemStyle-Width="10%" Visible="false">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DeptName") %>' ToolTip='<%# Eval("DeptName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="7%">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEditNotice" runat="server" OnClick="btnChangeStatus_Click" ToolTip="Edit Notice Details" data-toggle="tooltip"
                                    CommandArgument='<%# Eval("NoticeInstanceID") %>'>
                                     <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="View Notice Details" /></asp:LinkButton>
                                <%--CommandName="EDIT_Notice" OnClientClick="openNoticeModal()"--%>
                                <asp:LinkButton ID="lnkDeleteNotice" runat="server" CommandName="DELETE_Notice" ToolTip="Delete Notice" data-toggle="tooltip" Visible="false"
                                    CommandArgument='<%# Eval("NoticeInstanceID") %>' OnClientClick="return confirm('Are you certain you want to Delete this Notice Details?');">
                                    <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Notice" /></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="col-md-12 colpadding0">
                <div class="col-md-8 colpadding0">
                    <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                        <p style="padding-right: 0px !Important;">
                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="col-md-2 colpadding0">
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right; height: 32px !important"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                        <asp:ListItem Text="5"/>
                        <asp:ListItem Text="10" Selected="True" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                <div class="col-md-2 colpadding0" style="float: right;">
                    <div style="float: left; width: 60%">
                        <p class="clsPageNo">Page</p>
                    </div>
                    <div style="float: left; width: 40%">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                        </asp:DropDownListChosen>
                    </div>
                </div>
                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
            </div>
        </div>
        <%--</section>--%>
    </div>

    <%-- </ContentTemplate>
    </asp:UpdatePanel>--%>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 210px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Add/Edit Notice Details</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeNoticeModal();">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
