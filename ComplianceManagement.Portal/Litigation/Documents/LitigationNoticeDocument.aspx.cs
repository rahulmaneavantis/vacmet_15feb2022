﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages
{
    public partial class LitigationNoticeDocument : System.Web.UI.Page
    {
        public static string LitigatinoDocPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            
                FillComplianceDocuments();

            }
        }
        public static object GetAllCompliance(int CaseInstanceID, string type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int typeid = -1;
                if (type == "C")
                {
                    typeid = 1;
                }
                else if (type == "N")
                {
                    typeid = 2;
                }
                var query = (from row in entities.tbl_ActMapping
                             join row1 in entities.Compliances on
                             row.ActID equals row1.ActID
                             where row.IsActive == true && row.CaseNoticeInstanceID == CaseInstanceID
                             && row.Type == typeid && row1.IsDeleted == false && row1.Status == null
                             select row1).ToList();

                return query.OrderBy(entry => entry.ShortDescription).ToList();
            }
        }
        public static object GetAllAct(int CaseInstanceID, string type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                int typeid = -1;
                if (type == "C")
                {
                    typeid = 1;
                }
                else if (type == "N")
                {
                    typeid = 2;
                }
                var query = (from row in entities.tbl_ActMapping
                             join row1 in entities.Acts on
                             row.ActID equals row1.ID
                             where row.IsActive == true && row.CaseNoticeInstanceID == CaseInstanceID
                             && row.Type == typeid
                             select row1).ToList();

                return query.OrderBy(entry => entry.Name).ToList();
            }
        }
       

       
        public List<tbl_LitigationFileData> GetFilteredComplianceDocuments(int CaseInstanceID, DateTime DateFrom, DateTime DateTo, string isnoticeOrcase)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {

                List<tbl_LitigationFileData> FileUploadForNotice = (from row in entities.tbl_LitigationFileData
                                                                        where row.NoticeCaseInstanceID == CaseInstanceID
                                                                          && row.IsDeleted == false
                                                                          select row).ToList(); 
                return FileUploadForNotice;
            }
        }
        public List<tbl_LitigationFileData> GetDocumnets(int CaseInstanceID, int ID, string isnoticeOrcase)
        {
            using (LitigationDataModelContainer entities = new LitigationDataModelContainer())
            {
                List<tbl_LitigationFileData> FileUploadForNotice = (from row in entities.tbl_LitigationFileData
                                                                    where row.NoticeCaseInstanceID == CaseInstanceID
                                                                    && row.ID == ID
                                                                    && row.IsDeleted == false
                                                                    select row).ToList();
                return FileUploadForNotice;
            }
        }
        public void FillComplianceDocuments()
        {
            try
            {

                if (!string.IsNullOrEmpty(Request.QueryString["caseinstanceid"]))
                {
                    var caseInstanceID = Request.QueryString["caseinstanceid"];
                    if (caseInstanceID != "")
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                        {
                            var Type = Request.QueryString["Type"];

                            DateTime DateFrom = new DateTime();
                            DateTime DateTo = new DateTime();

                            

                            var ComplianceDocs = GetFilteredComplianceDocuments(Convert.ToInt32(caseInstanceID), DateFrom, DateTo, Type);
                            grdComplianceDocument.DataSource = ComplianceDocs;
                            grdComplianceDocument.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                FillComplianceDocuments();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceDocument_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            FillComplianceDocuments();
        }
        protected void grdComplianceDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
        }
        protected void grdComplianceDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Download")
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["caseinstanceid"]))
                    {
                        var caseInstanceID = Request.QueryString["caseinstanceid"];
                        if (caseInstanceID != "")
                        {
                            if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                            {
                                var Type = Request.QueryString["Type"];
                                string[] commandArg = e.CommandArgument.ToString().Split(',');
                                List<tbl_LitigationFileData> CMPDocuments = GetDocumnets(Convert.ToInt32(caseInstanceID), Convert.ToInt32(commandArg[0]), Type);
                                Session["ID"] = commandArg[0];

                                if (CMPDocuments != null)
                                {
                                    using (ZipFile ComplianceZip = new ZipFile())
                                    {
                                        List<tbl_LitigationFileData> ComplianceFileData = new List<tbl_LitigationFileData>();
                                        ComplianceFileData = CMPDocuments.ToList();
                                        //var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArg[0]));
                                        ComplianceZip.AddDirectoryByName("Notice Doc");
                                        if (ComplianceFileData.Count > 0)
                                        {
                                            int i = 0;
                                            foreach (var file in ComplianceFileData)
                                            {
                                                string filePath = string.Empty;
                                                //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                                {
                                                    filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                                }
                                                else
                                                {
                                                    filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                                }
                                                if (file.FilePath != null && File.Exists(filePath))
                                                {
                                                    string[] filename = file.FileName.Split('.');
                                                    string str = filename[0] + i + "." + filename[1];
                                                    if (file.EnType == "M")
                                                    {
                                                        ComplianceZip.AddEntry("Notice Doc" + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    else
                                                    {
                                                        ComplianceZip.AddEntry("Notice Doc" + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                    }
                                                    i++;
                                                }
                                            }
                                        }

                                        var zipMs = new MemoryStream();
                                        ComplianceZip.Save(zipMs);
                                        zipMs.Position = 0;
                                        byte[] data = zipMs.ToArray();
                                        Response.Buffer = true;
                                        Response.ClearContent();
                                        Response.ClearHeaders();
                                        Response.Clear();
                                        Response.ContentType = "application/zip";
                                        Response.AddHeader("content-disposition", "attachment; filename=NoticeDocument.zip");
                                        Response.BinaryWrite(data);
                                        Response.Flush();
                                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Document available to Download.";
                                }
                            }
                        }
                    }
                }
                else if (e.CommandName == "View")
                {
                    GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                    int RowIndex = gvr.RowIndex;

                    var caseInstanceID = Request.QueryString["caseinstanceid"];
                    var Type = Request.QueryString["Type"];
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    Session["ID"] = commandArg[0];
                    List<tbl_LitigationFileData> CMPDocuments = GetDocumnets(Convert.ToInt32(caseInstanceID), Convert.ToInt32(commandArg[0]), Type);

                    if (CMPDocuments.Count > 0)
                    {
                        List<tbl_LitigationFileData> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                            entityData.Version = "1.0";
                            entityData.NoticeCaseInstanceID = Convert.ToInt64(commandArg[0]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {

                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles/Litigation";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);

                                    //Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();

                                    LitigatinoDocPath = FileName;
                                    LitigatinoDocPath = LitigatinoDocPath.Substring(2, LitigatinoDocPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation('" + LitigatinoDocPath + "');", true);
                                    
                                }
                                break;
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLitigation();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnkSaveDocument_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlDownloadLink.SelectedValue == "1")
                {
                    List<int> ScheduledOnIDList = new List<int>();
                    int chkDocumentCount = 0;
                    var caseInstanceID = 0;
                    var Type = string.Empty;
                    bool saveSuccess = false;

                    foreach (GridViewRow gvrow in grdComplianceDocument.Rows)
                    {
                        CheckBox chkDocument = (CheckBox)gvrow.FindControl("chkDocument");
                        Label lblScheduledOnID = (Label)gvrow.FindControl("lblScheduledOnID");

                        if (chkDocument.Checked)
                        {
                            ScheduledOnIDList.Add(Convert.ToInt32(lblScheduledOnID.Text));
                            chkDocumentCount++;
                        }
                    }

                    if (chkDocumentCount > 0)
                    {
                        if (!string.IsNullOrEmpty(Request.QueryString["caseinstanceid"]))
                        {
                            caseInstanceID = Convert.ToInt32(Request.QueryString["caseinstanceid"]);
                        }

                        List<FileData> ListofFiles = CaseManagement.GetAllComplianceDocuments(ScheduledOnIDList);

                        if (ListofFiles.Count > 0)
                        {
                            if (caseInstanceID > 0)
                            {
                                #region Upload Document

                                tbl_LitigationFileData objCaseDoc = new tbl_LitigationFileData()
                                {
                                    NoticeCaseInstanceID = caseInstanceID,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    IsDeleted = false,
                                    DocType = "CD",
                                };

                                int customerID = -1;
                                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                                //string directoryPath = "";
                                String fileName = "";

                                if (caseInstanceID > 0)
                                {
                                    for (int i = 0; i < ListofFiles.Count; i++)
                                    {
                                        fileName = ListofFiles[i].Name;
                                        objCaseDoc.FileName = fileName;
                                        objCaseDoc.FilePath = ListofFiles[i].FilePath;
                                        objCaseDoc.FileKey = ListofFiles[i].FileKey;
                                        objCaseDoc.VersionDate = DateTime.Now;
                                        objCaseDoc.CreatedOn = DateTime.Now;
                                        objCaseDoc.FileSize = ListofFiles[i].FileSize;
                                        saveSuccess = CaseManagement.CreateCaseDocumentMapping(objCaseDoc);
                                    }
                                    if (saveSuccess)
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Compliance document saved successfully.";
                                    }

                                }//End For Each  

                                if (saveSuccess)
                                {
                                    LitigationManagement.CreateAuditLog("C", caseInstanceID, "tbl_LitigationFileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Case Document(s) Uploaded", true);
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No Document available.";
                        }
                    }
                }
                else
                {
                    List<int> ScheduledOnIDList = new List<int>();
                    int chkDocumentCount = 0;
                    var Type = string.Empty;

                    foreach (GridViewRow gvrow in grdComplianceDocument.Rows)
                    {
                        CheckBox chkDocument = (CheckBox)gvrow.FindControl("chkDocument");
                        Label lblScheduledOnID = (Label)gvrow.FindControl("lblScheduledOnID");

                        if (chkDocument.Checked)
                        {
                            ScheduledOnIDList.Add(Convert.ToInt32(lblScheduledOnID.Text));
                            chkDocumentCount++;
                        }
                    }
                    if (chkDocumentCount > 0)
                    {
                        List<FileData> ListofFiles = CaseManagement.GetAllComplianceDocuments(ScheduledOnIDList);
                        if (ListofFiles.Count > 0)
                        {
                            using (ZipFile CompliancelitiZip = new ZipFile())
                            {
                                int i = 0;
                                foreach (var file in ListofFiles)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        int idx = file.Name.LastIndexOf('.');
                                        string str = file.Name.Substring(0, idx) + "_" + file.Version + "." + file.Name.Substring(idx + 1);

                                        if (!CompliancelitiZip.ContainsEntry(str))
                                        {
                                            if (file.EnType == "M")
                                            {
                                                CompliancelitiZip.AddEntry(str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                CompliancelitiZip.AddEntry(str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                        }
                                        i++;
                                    }
                                }

                                var zipMs = new MemoryStream();
                                CompliancelitiZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                Response.BinaryWrite(Filedata);
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Downloaded", false);

                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No Document available to Download.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void rptLitigationVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                LinkButton lblIDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblIDocumentVersionView);

            }
        }

      
        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> ScheduledOnIDList = new List<int>();
                int chkDocumentCount = 0;
                var Type = string.Empty;

                foreach (GridViewRow gvrow in grdComplianceDocument.Rows)
                {
                    CheckBox chkDocument = (CheckBox)gvrow.FindControl("chkDocument");
                    Label lblScheduledOnID = (Label)gvrow.FindControl("lblScheduledOnID");

                    if (chkDocument.Checked)
                    {
                        ScheduledOnIDList.Add(Convert.ToInt32(lblScheduledOnID.Text));
                        chkDocumentCount++;
                    }
                }
                if (chkDocumentCount > 0)
                {
                    List<FileData> ListofFiles = CaseManagement.GetAllComplianceDocuments(ScheduledOnIDList);
                    if (ListofFiles.Count > 0)
                    {
                        using (ZipFile CompliancelitiZip = new ZipFile())
                        {
                            int i = 0;
                            foreach (var file in ListofFiles)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    int idx = file.Name.LastIndexOf('.');
                                    string str = file.Name.Substring(0, idx) + "_" + file.Version + "." + file.Name.Substring(idx + 1);

                                    if (!CompliancelitiZip.ContainsEntry(str))
                                    {
                                        if (file.EnType == "M")
                                        {
                                            CompliancelitiZip.AddEntry(str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            CompliancelitiZip.AddEntry(str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                    }
                                    i++;
                                }
                            }

                            var zipMs = new MemoryStream();
                            CompliancelitiZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] Filedata = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=CaseHearingDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                            Response.BinaryWrite(Filedata);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            LitigationManagement.CreateAuditLog("C", Convert.ToInt32(ViewState["CaseInstanceID"]), "tbl_LitigationFileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Hearing Document(s) Downloaded", false);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}