﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LitigationNoticeDocument.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.aspxPages.LitigationNoticeDocument" %>


<!DOCTYPE html>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownListChosen" Namespace="DropDownListChosen" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Litigation Notice Document Detail</title>

    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />

    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>

    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>

    <link href="~/NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <link href="../../NewCSS/litigation_custom_style.css" rel="stylesheet" />

    <script type="text/javascript">


        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls(); CheckBoxChecks();

        });

        function CheckBoxChecks() {


            $("[id*=chkAllDocument]").click(function () {
                var chkHeader = $(this);
                var grid = $(this).closest("table");
                $("input[type=checkbox]", grid).each(function () {
                    if (chkHeader.is(":checked")) {
                        $(this).attr("checked", "checked");
                    } else {
                        $(this).removeAttr("checked");
                    }
                });
            });

            $("[id*=chkDocument]").click(function () {
                var grid = $(this).closest("table");
                var chkHeader = $("[id*=chkAllDocument]", grid);
                if (!$(this).is(":checked")) {
                    chkHeader.removeAttr("checked");
                } else {
                    if ($("[id*=chkDocument]", grid).length == $("[id*=chkDocument]:checked", grid).length) {
                        chkHeader.attr("checked", "checked");
                    }
                }
            });
        }

        function InitializeRequest(sender, args) { }

        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            var startDate = new Date();
            $(function () {
                $('input[id*=txtFromDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                    });
            });

            $(function () {
                $('input[id*=txtEndDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        buttonImageOnly: true,
                    });
            });

        }

        function fopendocfileLitigation(file) {
            $('#divViewDocument').modal('show');
            $('.modal-dialog').css('width', '98%');
            $('#OverViews').attr('src', "../../docviewer.aspx?docurl=" + file);
        }

        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);

        $("html").mouseover(function () {
            $("html").getNiceScroll().resize();
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

    </script>

    <style type="text/css">
        body {
            background: #fff;
        }
    </style>     
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="upLitigationDocument" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="dashboard">
                    <div class="col-md-12 plr0">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="ACTPopupValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="ACTPopupValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                    </div>


                    <div class="row col-md-12" style="height: 350px; overflow-y: auto;">

                        <asp:GridView runat="server" ID="grdComplianceDocument" AutoGenerateColumns="false" CssClass="table" GridLines="none" BorderWidth="0px"
                            OnRowCommand="grdComplianceDocument_RowCommand" CellPadding="4" Width="100%" OnRowDataBound="grdComplianceDocument_RowDataBound"
                            AutoPostBack="true" ShowHeaderWhenEmpty="true">
                            <%--OnPageIndexChanging="grdComplianceDocument_PageIndexChanging"--%>
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="3%">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="File Name" ItemStyle-Width="40%">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                            <asp:Label ID="lblAct" runat="server" Text='<%# Eval("FileName") %>' ToolTip='<%# Eval("FileName") %>'
                                                data-toggle="tooltip" data-placement="bottom"></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Uploaded By" ItemStyle-Width="30%">
                                    <ItemTemplate>
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                            <asp:Label ID="lblShortDesc" runat="server"
                                                data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CreatedByText") %>' ToolTip='<%# Eval("CreatedByText") %>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:UpdatePanel ID="upDownloadFile" runat="server">
                                            <ContentTemplate>

                                                <asp:ImageButton ID="lblViewFile" runat="server" ImageUrl="~/img/View-icon-new.png" CommandName="View"
                                                    CommandArgument='<%# Eval("ID")%>' data-toggle="tooltip" data-placement="bottom" ToolTip="Click to View Notice Document"></asp:ImageButton>

                                                <asp:ImageButton ID="lblDownLoadfile" runat="server" ImageUrl="~/img/icon-download.png" CommandName="Download"
                                                    CommandArgument='<%# Eval("ID")%>' data-toggle="tooltip" data-placement="bottom" ToolTip="Click to Download Notice Document"></asp:ImageButton>


                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lblDownLoadfile" />
                                                <%--<asp:PostBackTrigger ControlID="lblViewfile" />--%>
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                            <PagerStyle HorizontalAlign="Right" />
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                                <table style="display: none">
                                    <tr>
                                        <td>
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                            </PagerTemplate>
                            <EmptyDataTemplate>
                                No Record Found
                            </EmptyDataTemplate>
                        </asp:GridView>

                    </div>

                    <div id="divLinkCaseSaveCount" class="row col-md-12 plr0" style="display: none;">
                        <div class="col-md-2 text-left">
                            <asp:Label runat="server" ID="lblTotalDocumentSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                        </div>
                        <div class="col-md-2 text-left">
                            <asp:DropDownList runat="server" ID="ddlDownloadLink" DataPlaceHolder="Select" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                CssClass="form-control" Width="100%">
                                <asp:ListItem Text="Add to Case" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Download" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-8 text-left">
                            <asp:LinkButton Text="Submit" CssClass="btn btn-primary" runat="server" ID="lnkSaveDocument" OnClick="lnkSaveDocument_Click" />
                            <asp:LinkButton Text="Close" CssClass="btn btn-primary" runat="server" ID="lnkDownload" data-dismiss="modal"/>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lnkSaveDocument" />
            </Triggers>
        </asp:UpdatePanel>
        <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 70%;">
            <div class="modal-content">
                <div class="modal-header" style="border: none;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="height: 570px;">
                    <div style="width: 100%;">
                        <div class="col-md-12 colpadding0">
                            <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                            <fieldset style="height: 550px; width: 100%;">
                                <iframe src="about:blank" id="OverViews" width="100%" height="101%" frameborder="0"></iframe>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>

