﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataLitigation;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Documents
{
    public partial class MyDocumentLitigation : System.Web.UI.Page
    {
        public static string LitigatinoDocPath = "";
        protected bool flag;
        public string CaseNoticeflag;
        public long CustomerID = AuthenticationHelper.CustomerID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                flag = false;
                BindParty();
                BindDepartment();

                applyCSStoFileTag_ListItems();
                //Bind Tree Views
                //var branchList = CustomerBranchManagement.GetAllHierarchy(customerID);
                List<NameValueHierarchy> branches;
                string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                if (CacheHelper.Exists(key))
                {
                    CacheHelper.Get<List<NameValueHierarchy>>(key, out branches);
                }
                else
                {
                    branches = CustomerBranchManagement.GetAllHierarchy(customerID);
                    CacheHelper.Set<List<NameValueHierarchy>>(key, branches);
                }
                BindCustomerBranches(tvFilterLocation, tbxFilterLocation, branches);
                Session.Remove("CaseNoticeflag");
                BindGrid("C");
                BindFileTags("C");
                bindPageNumber();
                BindFinancialYear();
            }
        }

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            ddlDeptPage.DataTextField = "Name";
            ddlDeptPage.DataValueField = "ID";

            ddlDeptPage.DataSource = obj;
            ddlDeptPage.DataBind();

            ddlDeptPage.Items.Insert(0, new ListItem("All", "-1"));
        }

        public void BindParty()
        {
            var obj = LitigationLaw.GetLCPartyDetails(CustomerID);
            ddlPartyPage.DataTextField = "Name";
            ddlPartyPage.DataValueField = "ID";

            ddlPartyPage.DataSource = obj;
            ddlPartyPage.DataBind();

            ddlPartyPage.Items.Insert(0, new ListItem("All", "-1"));
        }

        private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        {
            try
            {
                treetoBind.Nodes.Clear();

                NameValueHierarchy branch = null;

                if (branchList.Count > 0)
                {
                    branch = branchList[0];
                }

                treeTxtBox.Text = "Select Entity/Branch/Location";

                List<TreeNode> nodes = new List<TreeNode>();

                BindBranchesHierarchy(null, branch, nodes);

                foreach (TreeNode item in nodes)
                {
                    treetoBind.Nodes.Add(item);
                }

                treetoBind.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorNoticePage.IsValid = false;
                cvErrorNoticePage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorNoticePage.IsValid = false;
                cvErrorNoticePage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static object FillFnancialYear()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                var query = (from row in entities.FinancialYearDetails
                             where row.IsDeleted==false
                             select row).ToList();
                return query;
            }
        }

        public void BindFinancialYear()
        {

            ddlFinancialYear.DataValueField = "Id";
            ddlFinancialYear.DataTextField = "FinancialYear";
            ddlFinancialYear.DataSource = FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }

        public void BindGrid(string Flag)
        {
            long customerID = -1;
            customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

            int branchID = -1;
            int partyID = -1;
            int deptID = -1;
            int caseStatus = -1;
            string caseType = string.Empty;
            string documentname = string.Empty;
            var type = string.Empty;
            string dlFY = string.Empty;

            if (Flag == "T")
            {
                grdTaskActivity.Visible = true;
                grdTaskActivity.DataSource = null;
                grdTaskActivity.DataBind();
                grdMyDocument.Visible = false;
                grdMyDocument.DataSource = null;
                grdMyDocument.DataBind();
            }
            else
            {
                grdMyDocument.Visible = true;
                grdMyDocument.DataSource = null;
                grdMyDocument.DataBind();
                grdTaskActivity.Visible = false;
                grdTaskActivity.DataSource = null;
                grdTaskActivity.DataBind();
            }
            if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
            {
                caseStatus = Convert.ToInt32(ddlStatus.SelectedValue);
            }
            if (!string.IsNullOrEmpty(txtsearchdoc.Text))
            {
                documentname = txtsearchdoc.Text;
            }
            if (!string.IsNullOrEmpty(ddlNoticeTypePage.SelectedValue))
            {
                caseType = ddlNoticeTypePage.SelectedItem.Text;
            }

            if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
            {
                branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
            }

            if (!string.IsNullOrEmpty(ddlPartyPage.SelectedValue))
            {
                partyID = Convert.ToInt32(ddlPartyPage.SelectedValue);
            }

            if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
            {
                deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
            }
            if (!string.IsNullOrEmpty(ddlTypePage.SelectedValue))
            {
                type = ddlTypePage.SelectedValue;
            }
            //if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue) && ddlFinancialYear.SelectedValue != "-1")
            //{
            //    dlFY = Convert.ToString(ddlFinancialYear.SelectedValue);
            //}
            if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
            {
                if (ddlFinancialYear.SelectedValue != "-1")
                {
                    dlFY = Convert.ToString(ddlFinancialYear.SelectedItem.Text);
                }
            }
            var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
            List<ListItem> selectedItems = LitigationManagement.GetSelectedItems(lstBoxFileTags);
            var selectedFileTags = selectedItems.Select(row => row.Text).ToList();
            // var lstAssignedContracts = LitigationDocumentManagement.GetAllDocumentListofNotice(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, caseStatus, caseType);
            if (Flag == "N")
            {
                type = Flag;
                var DocList = LitigationDocumentManagement.GetAllDocumentListofNotice(Convert.ToInt32(CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, caseStatus, caseType, selectedFileTags, documentname, dlFY);
                if (DocList.Count > 0)
                    DocList = DocList.Where(row => row.DocumentCount > 0).ToList();

                //if (dlFY != "" && dlFY != "-1")
                //{
                //    DocList = DocList.Where(entry => entry.FinancialYear == dlFY).ToList();
                //}
                if (dlFY != "" && dlFY != "-1")
                {
                    DocList = DocList.Where(entry => entry.FYName != "" && entry.FYName != null).ToList();
                    string a = dlFY + "" + ",";
                    DocList = DocList.Where(aa => aa.FYName.Contains(a)).ToList();

                }
                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            DocList = DocList.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            DocList = DocList.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }
               

                Session["TotalRows"] = null;
                if (DocList.Count > 0)
                {
                    grdMyDocument.DataSource = DocList;
                    Session["TotalRows"] = DocList.Count;
                    grdMyDocument.DataBind();

                }
                else
                {
                    grdMyDocument.DataSource = DocList;
                    grdMyDocument.DataBind();
                }
            }
            else if (Flag == "C")
            {
                type = Flag;

                //if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue) && ddlFinancialYear.SelectedValue != "-1")
                //{
                //    dlFY = Convert.ToString(ddlFinancialYear.SelectedValue);
                //}
                //if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                //{
                //    if (ddlFinancialYear.SelectedValue != "-1")
                //    {
                //        dlFY = Convert.ToString(ddlFinancialYear.SelectedItem.Text);
                //    }
                //}
                var DocList = LitigationDocumentManagement.GetAllDocumentListofCase(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, caseStatus, caseType, selectedFileTags, documentname,dlFY);

                if (DocList.Count > 0)
                    DocList = DocList.Where(row => row.DocumentCount > 0).ToList();

                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            DocList = DocList.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            DocList = DocList.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                //if (dlFY != "" && dlFY != "-1")
                //{
                //    DocList = DocList.Where(entry => entry.FinancialYear == dlFY).ToList();
                //}
                if (dlFY != "" && dlFY != "-1")
                {
                    DocList = DocList.Where(entry => entry.FYName != "" && entry.FYName != null).ToList();
                    string a = dlFY + "" + ",";
                    DocList = DocList.Where(aa => aa.FYName.Contains(a)).ToList();

                }
                Session["TotalRows"] = null;
                if (DocList.Count > 0)
                {
                    grdMyDocument.DataSource = DocList;
                    Session["TotalRows"] = DocList.Count;
                    grdMyDocument.DataBind();

                }
                else
                {
                    grdMyDocument.DataSource = DocList;
                    grdMyDocument.DataBind();
                }
            }
            else
            {
                var lstTaskDetails = LitigationTaskManagement.GetAssignedTaskListforTag(AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, -1, partyID, deptID, caseStatus, "B", AuthenticationHelper.CustomerID, selectedFileTags, documentname);
                
                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            lstTaskDetails = lstTaskDetails.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            lstTaskDetails = lstTaskDetails.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                flag = true;
                Session["TotalRows"] = null;
                if (lstTaskDetails.Count > 0)
                {
                    grdTaskActivity.DataSource = lstTaskDetails;
                    grdTaskActivity.DataBind();
                    Session["TotalRows"] = lstTaskDetails.Count;
                }
                else
                {
                    grdTaskActivity.DataSource = null;
                    grdTaskActivity.DataBind();
                    Session["TotalRows"] = lstTaskDetails.Count;

                }

                lstTaskDetails.Clear();
                lstTaskDetails = null;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string caseNotiseval = string.Empty;
                if (Session["CaseNoticeflag"] != null)
                {
                    caseNotiseval = Session["CaseNoticeflag"].ToString();
                }
                if (!string.IsNullOrEmpty(ddlTypePage.SelectedValue))
                {
                    caseNotiseval = ddlTypePage.SelectedValue.ToString();
                }
                if (caseNotiseval == "T")
                {
                    grdTaskActivity.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                }
                else
                {
                    grdMyDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                }

                ShowGridDetail();
                BindGrid(caseNotiseval);
                bindPageNumber();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    if (ddlTypePage.SelectedValue == "T")
                    {
                        int gridindex = grdTaskActivity.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                    else
                    {
                        int gridindex = grdMyDocument.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
                applyCSStoFileTag_ListItems();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }

                ShowGridDetail();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlTypePage_SelectedIndexChanged(object sender, EventArgs e)
        {
            string caseNotiseval = string.Empty;
            if (Session["CaseNoticeflag"] != null)
            {
                caseNotiseval = Session["CaseNoticeflag"].ToString();
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlTypePage.SelectedValue))
                    caseNotiseval = ddlTypePage.SelectedValue;
            }
            if (caseNotiseval != null)
                if (caseNotiseval == "T")
                {
                    grdTaskActivity.Visible = true;
                    grdTaskActivity.DataSource = null;
                    grdTaskActivity.DataBind();
                    grdMyDocument.Visible = false;
                    grdMyDocument.DataSource = null;
                    grdMyDocument.DataBind();
                }
                else
                {
                    grdMyDocument.Visible = true;
                    grdMyDocument.DataSource = null;
                    grdMyDocument.DataBind();
                    grdTaskActivity.Visible = false;
                    grdTaskActivity.DataSource = null;
                    grdTaskActivity.DataBind();
                }
            ViewState["Direction"] = "Ascending";
            ViewState["SortExpression"] = null;

            BindGrid(caseNotiseval);
            bindPageNumber();
            applyCSStoFileTag_ListItems();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);
        }
        protected void grdMyDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string caseNotiseval = string.Empty;
                if (Session["CaseNoticeflag"] != null)
                {
                    caseNotiseval = Session["CaseNoticeflag"].ToString();
                }
                else
                {
                    caseNotiseval = ddlTypePage.SelectedValue;
                }

                List<string> Doctypes = new List<string>();
                var CheckType = string.Empty;
                var NameOfDocu = string.Empty;
                string TypeName = string.Empty;
                //if (ddlTypePage.SelectedValue != "-1")
                //{
                if (caseNotiseval == "N")
                {
                    CheckType = caseNotiseval;
                    NameOfDocu = "Notice";
                }
                else
                {
                    CheckType = caseNotiseval;
                    NameOfDocu = "Case";
                }
                //}

                List<string> Docfiles = new List<string>();

                if (e.CommandName == "DownloadFile")
                {
                    GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                    int RowIndex = gvr.RowIndex;
                    DropDownList ddlDocumentFile = (DropDownList)grdMyDocument.Rows[RowIndex].FindControl("ddlDocumentFile");

                    if (!string.IsNullOrEmpty(ddlDocumentFile.SelectedValue))
                    {
                        if (ddlDocumentFile.SelectedValue != "-1")
                        {
                            Doctypes.Add(ddlDocumentFile.SelectedValue);
                            TypeName = ddlDocumentFile.SelectedItem.Text;
                        }
                        if (ddlDocumentFile.SelectedValue == "A")
                        {
                            if (CheckType == "N")
                            {
                                Doctypes.Add("N");
                                Doctypes.Add("NR");
                                Doctypes.Add("NT");
                                TypeName = "Notice Document";
                            }
                            else
                            {
                                Doctypes.Add("C");
                                Doctypes.Add("CH");
                                Doctypes.Add("CT");
                                TypeName = "Case Document";
                            }
                        }
                        ViewState["LitstOfddlValue"] = Doctypes;
                    }
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    List<tbl_LitigationFileData> CMPDocuments = new List<tbl_LitigationFileData>();

                    if (CMPDocuments != null)
                    {
                        using (ZipFile LitigationZip = new ZipFile())
                        {
                            CMPDocuments = LitigationDocumentManagement.GetDocuments(Convert.ToInt64(commandArg[0]), Doctypes);

                            if (CMPDocuments.Count > 0)
                            {
                                int i = 0;
                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        int idx = file.FileName.LastIndexOf('.');
                                        string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);
                                        string Dates = DateTime.Now.ToString("dd/mm/yyyy");
                                        if (file.EnType == "M")
                                        {
                                            LitigationZip.AddEntry("Litigation Document" + "/" + TypeName + "_" + Dates + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            LitigationZip.AddEntry("Litigation Document" + "/" + TypeName + "_" + Dates + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                                var zipMs = new MemoryStream();
                                LitigationZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] Filedata = zipMs.ToArray();
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=LitigationDocument.zip");
                                Response.BinaryWrite(Filedata);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                            }
                            else
                            {
                                cvErrorNoticePage.IsValid = false;
                                cvErrorNoticePage.ErrorMessage = "No Document available to Download.";
                            }
                        }
                    }
                }
                else if (e.CommandName == "View")
                {
                    GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                    int RowIndex = gvr.RowIndex;
                    DropDownList ddlDocumentFile = (DropDownList)grdMyDocument.Rows[RowIndex].FindControl("ddlDocumentFile");

                    if (!string.IsNullOrEmpty(ddlDocumentFile.SelectedValue))
                    {
                        if (ddlDocumentFile.SelectedValue != "-1")
                        {
                            Doctypes.Add(ddlDocumentFile.SelectedValue);
                            TypeName = ddlDocumentFile.SelectedItem.Text;
                        }
                        if (ddlDocumentFile.SelectedValue == "A")
                        {
                            if (CheckType == "N")
                            {
                                Doctypes.Add("N");
                                Doctypes.Add("NR");
                                Doctypes.Add("NT");
                                TypeName = "Notice Document";
                            }
                            else
                            {
                                Doctypes.Add("C");
                                Doctypes.Add("CH");
                                Doctypes.Add("CT");
                                TypeName = "Case Document";
                            }
                        }
                        ViewState["LitstOfddlValue"] = Doctypes;
                    }
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    Session["NoticeInstanceID"] = commandArg[0];
                    Session["DocTypes"] = Doctypes;
                    List<tbl_LitigationFileData> CMPDocuments = LitigationDocumentManagement.GetDocuments(Convert.ToInt64(commandArg[0]), Doctypes);

                    if (CMPDocuments.Count > 0)
                    {
                        List<tbl_LitigationFileData> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                        if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                            entityData.Version = "1.0";
                            entityData.NoticeCaseInstanceID = Convert.ToInt64(commandArg[0]);
                            entityData.DocTypeInstanceID = Convert.ToInt64(commandArg[2]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            rptLitigationVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                            rptLitigationVersionView.DataBind();

                            foreach (var file in CMPDocuments)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles/Litigation";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);

                                    //Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();

                                    LitigatinoDocPath = FileName;
                                    LitigatinoDocPath = LitigatinoDocPath.Substring(2, LitigatinoDocPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation('" + LitigatinoDocPath + "');", true);
                                    lblMessage.Text = "";
                                }
                                else
                                {
                                    cvErrorNoticePage.IsValid = false;
                                    cvErrorNoticePage.ErrorMessage = "No Document available to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLitigation();", true);
                                }
                                break;
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLitigation();", true);
                    }
                }
                applyCSStoFileTag_ListItems();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorNoticePage.IsValid = false;
                cvErrorNoticePage.ErrorMessage = "Server Error Occured. Please try again.";
            }
            applyCSStoFileTag_ListItems();
        }

        protected void rptLitigationVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                long NoticeInstanceID = Convert.ToInt64(Session["NoticeInstanceID"]);
                List<string> Doctypes = new List<string>();
                var CheckType = string.Empty;
                var NameOfDocu = string.Empty;
                if (ddlTypePage.SelectedValue != "-1")
                {
                    if (ddlTypePage.SelectedValue == "N")
                    {
                        CheckType = ddlTypePage.SelectedValue;
                        NameOfDocu = "Notice";
                    }
                    else
                    {
                        CheckType = ddlTypePage.SelectedValue;
                        NameOfDocu = "Case";
                    }
                }

                if (ViewState["LitstOfddlValue"] != null)
                {
                    Doctypes = ViewState["LitstOfddlValue"] as List<string>;
                }

                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                List<tbl_LitigationFileData> LitigationFileData = new List<tbl_LitigationFileData>();
                List<tbl_LitigationFileData> LitigationDocument = new List<tbl_LitigationFileData>();
                if (ddlTypePage.SelectedValue == "T")
                {
                    LitigationDocument = LitigationDocumentManagement.GetTaskRelatedDoc(Convert.ToInt32(commandArgs[2]));
                }
                else
                {
                    LitigationDocument = LitigationDocumentManagement.GetDocuments(NoticeInstanceID, Doctypes);
                }

                if (commandArgs[1].Equals("1.0"))
                {
                    LitigationFileData = LitigationDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    if (LitigationFileData.Count <= 0)
                    {
                        LitigationFileData = LitigationDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    LitigationFileData = LitigationDocument.Where(entry => entry.FileName == commandArgs[1]).ToList();
                }

                if (e.CommandName.Equals("View"))
                {
                    if (LitigationFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in LitigationFileData)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                i++;
                                LitigatinoDocPath = FileName;

                                LitigatinoDocPath = LitigatinoDocPath.Substring(2, LitigatinoDocPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation('" + LitigatinoDocPath + "');", true);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorNoticePage.IsValid = false;
                cvErrorNoticePage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptLitigationVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                LinkButton lblIDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblIDocumentVersionView);

            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            string caseNotiseval = string.Empty;
            if (Session["CaseNoticeflag"] != null || (!string.IsNullOrEmpty(ddlTypePage.SelectedValue)))
            {
                if (Session["CaseNoticeflag"] != null)
                {
                    caseNotiseval = Session["CaseNoticeflag"].ToString();
                }
                else
                {
                    caseNotiseval = ddlTypePage.SelectedValue;
                }
                int chkSelectedPage;
                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedItem.ToString()))
                {
                    chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
                }
                else
                {
                    chkSelectedPage = 1;
                }

                if (caseNotiseval == "T")
                {
                    grdTaskActivity.PageIndex = chkSelectedPage - 1;
                    grdTaskActivity.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                }
                else
                {
                    grdMyDocument.PageIndex = chkSelectedPage - 1;
                    grdMyDocument.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                }
                if (caseNotiseval != null)
                {
                    BindGrid(caseNotiseval);
                }
                else
                {
                    BindGrid("C");
                }
                ShowGridDetail();
                applyCSStoFileTag_ListItems();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);

            }

        }

        protected void grdMyDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string CaseNotice = string.Empty;
                if (Session["CaseNoticeflag"] != null)
                {
                    CaseNotice = Session["CaseNoticeflag"].ToString();
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlTypePage.SelectedValue))
                        CaseNotice = ddlTypePage.SelectedValue;
                }
                DropDownList ddlDocumentFile = (DropDownList)e.Row.FindControl("ddlDocumentFile");
                if (CaseNotice != "-1" || CaseNotice != null)
                {
                    if (CaseNotice == "N")
                    {
                        ListItem item1 = new ListItem("All", "A");
                        ListItem item2 = new ListItem("Notice Documents", "N");
                        ListItem item3 = new ListItem("Notice Response Documents", "NR");

                        ddlDocumentFile.Items.Add(item1);
                        ddlDocumentFile.Items.Add(item2);
                        ddlDocumentFile.Items.Add(item3);
                    }
                    if (CaseNotice == "C")
                    {
                        ListItem item1 = new ListItem("All", "A");
                        ListItem item2 = new ListItem("Case Documents", "C");
                        ListItem item3 = new ListItem("Case Hearing Documents", "CH");
                        ListItem item4 = new ListItem("Case Task Documents", "CT");

                        ddlDocumentFile.Items.Add(item1);
                        ddlDocumentFile.Items.Add(item2);
                        ddlDocumentFile.Items.Add(item3);
                        ddlDocumentFile.Items.Add(item4);
                    }
                }
            }
        }
        protected void ClearDropDownValue()
        {
            if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
            {
                ddlStatus.Items.Clear();
            }

            if (!string.IsNullOrEmpty(ddlNoticeTypePage.SelectedValue))
            {

                ddlNoticeTypePage.Items.Clear();
            }

            if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
            {
                tvFilterLocation.Nodes.Clear();
            }

            if (!string.IsNullOrEmpty(ddlPartyPage.SelectedValue))
            {
                ddlPartyPage.Items.Clear();
            }

            if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
            {
                ddlDeptPage.Items.Clear();
            }
            if (!string.IsNullOrEmpty(ddlTypePage.SelectedValue))
            {
                ddlTypePage.Items.Clear();
            }
        }
        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["CaseNoticeflag"] != null)
                {
                    Session.Remove("CaseNoticeflag");
                }
                if (!string.IsNullOrEmpty(ddlTypePage.SelectedValue))
                {
                    BindGrid(ddlTypePage.SelectedValue);
                    BindFileTags(ddlTypePage.SelectedValue);
                    bindPageNumber();

                    if (ddlTypePage.SelectedValue == "T")
                    {
                        liCase.Attributes.Add("class", "");
                        liNotice.Attributes.Add("class", "");
                        liTask.Attributes.Add("class", "active");
                        applyCSStoFileTag_ListItems();
                    }
                    else if (ddlTypePage.SelectedValue == "N")
                    {
                        liCase.Attributes.Add("class", "");
                        liNotice.Attributes.Add("class", "active");
                        liTask.Attributes.Add("class", "");
                        applyCSStoFileTag_ListItems();
                    }
                    else if (ddlTypePage.SelectedValue == "C")
                    {
                        liCase.Attributes.Add("class", "active");
                        liNotice.Attributes.Add("class", "");
                        liTask.Attributes.Add("class", "");
                        applyCSStoFileTag_ListItems();
                    }
                }
                bindPageNumber();
                applyCSStoFileTag_ListItems();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "All";
                // tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdMyDocument_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                int branchID = -1;
                int partyID = -1;
                int deptID = -1;
                int caseStatus = -1;
                string DocumentName = string.Empty;
                string caseType = string.Empty;
                var type = string.Empty;
                string dlFY = "-1";
                if (!string.IsNullOrEmpty(ddlTypePage.SelectedValue))
                {
                    if (ddlTypePage.SelectedValue != "-1")
                    {
                        if (ddlTypePage.SelectedValue == "T")
                        {
                            grdTaskActivity.Visible = true;
                            grdTaskActivity.DataSource = null;
                            grdTaskActivity.DataBind();
                            grdMyDocument.Visible = false;
                            grdMyDocument.DataSource = null;
                            grdMyDocument.DataBind();
                        }
                        else
                        {
                            grdMyDocument.Visible = true;
                            grdMyDocument.DataSource = null;
                            grdMyDocument.DataBind();
                            grdTaskActivity.Visible = false;
                            grdTaskActivity.DataSource = null;
                            grdTaskActivity.DataBind();
                        }
                        if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                        {
                            caseStatus = Convert.ToInt32(ddlStatus.SelectedValue);
                        }

                        if (!string.IsNullOrEmpty(ddlNoticeTypePage.SelectedValue))
                        {
                            caseType = ddlNoticeTypePage.SelectedItem.Text;
                        }

                        if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                        {
                            branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                        }

                        if (!string.IsNullOrEmpty(ddlPartyPage.SelectedValue))
                        {
                            partyID = Convert.ToInt32(ddlPartyPage.SelectedValue);
                        }

                        if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                        {
                            deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                        }
                        if (!string.IsNullOrEmpty(txtsearchdoc.Text))
                        {
                            DocumentName = txtsearchdoc.Text.Trim();
                        }
                        var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);
                        List<ListItem> selectedItems = LitigationManagement.GetSelectedItems(lstBoxFileTags);
                        var selectedFileTags = selectedItems.Select(row => row.Text).ToList();
                        if (ddlTypePage.SelectedValue == "N")
                        {
                            type = ddlTypePage.SelectedValue;

                            var DocList = LitigationDocumentManagement.GetAllDocumentListofNotice(Convert.ToInt32(CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, caseStatus, caseType, selectedFileTags, DocumentName,dlFY);

                            if (DocList.Count > 0)
                                DocList = DocList.Where(row => row.DocumentCount > 0).ToList();

                            string SortExpr = string.Empty;
                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                            {
                                SortExpr = Convert.ToString(ViewState["SortExpression"]);
                                if (SortExpr == e.SortExpression)
                                {
                                    if (direction == SortDirection.Ascending)
                                    {
                                        direction = SortDirection.Descending;
                                    }
                                    else
                                    {
                                        direction = SortDirection.Ascending;
                                    }
                                }
                                else
                                {
                                    direction = SortDirection.Ascending;
                                }
                            }

                            if (direction == SortDirection.Ascending)
                            {
                                ViewState["Direction"] = "Ascending";
                                DocList = DocList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                            }
                            else
                            {
                                ViewState["Direction"] = "Descending";
                                DocList = DocList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                            }

                            ViewState["SortExpression"] = e.SortExpression;

                            foreach (DataControlField field in grdMyDocument.Columns)
                            {
                                if (field.SortExpression == e.SortExpression)
                                {
                                    ViewState["SortIndex"] = grdMyDocument.Columns.IndexOf(field);
                                }
                            }
                            Session["TotalRows"] = null;
                            flag = true;
                            if (DocList.Count > 0)
                            {
                                grdMyDocument.DataSource = DocList;
                                Session["TotalRows"] = DocList.Count;
                                grdMyDocument.DataBind();
                            }
                            else
                            {
                                grdMyDocument.DataSource = DocList;
                                grdMyDocument.DataBind();
                            }
                        }
                        else
                        {
                            type = ddlTypePage.SelectedValue;

                            var DocList = LitigationDocumentManagement.GetAllDocumentListofCase(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, caseStatus, caseType, selectedFileTags, DocumentName,dlFY);

                            if (DocList.Count > 0)
                                DocList = DocList.Where(row => row.DocumentCount > 0).ToList();

                            string SortExpr = string.Empty;
                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                            {
                                SortExpr = Convert.ToString(ViewState["SortExpression"]);
                                if (SortExpr == e.SortExpression)
                                {
                                    if (direction == SortDirection.Ascending)
                                    {
                                        direction = SortDirection.Descending;
                                    }
                                    else
                                    {
                                        direction = SortDirection.Ascending;
                                    }
                                }
                                else
                                {
                                    direction = SortDirection.Ascending;
                                }
                            }

                            if (direction == SortDirection.Ascending)
                            {
                                ViewState["Direction"] = "Ascending";
                                DocList = DocList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                            }
                            else
                            {
                                ViewState["Direction"] = "Descending";
                                DocList = DocList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                            }

                            ViewState["SortExpression"] = e.SortExpression;

                            foreach (DataControlField field in grdMyDocument.Columns)
                            {
                                if (field.SortExpression == e.SortExpression)
                                {
                                    ViewState["SortIndex"] = grdMyDocument.Columns.IndexOf(field);
                                }
                            }
                            Session["TotalRows"] = null;
                            flag = true;
                            if (DocList.Count > 0)
                            {
                                grdMyDocument.DataSource = DocList;
                                grdMyDocument.DataBind();
                            }
                            else
                            {
                                grdMyDocument.DataSource = DocList;
                                grdMyDocument.DataBind();
                            }
                        }
                    }
                }
                applyCSStoFileTag_ListItems();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdMyDocument_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                    if (sortColumnIndex != -1)
                    {
                        AddSortImage(sortColumnIndex, e.Row);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdTaskActivity_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                int branchID = -1;
                int partyID = -1;
                int deptID = -1;
                int caseStatus = -1;
                string caseType = string.Empty;
                var type = string.Empty;
                string caseNotiseval = Session["CaseNoticeflag"].ToString();
                if (!string.IsNullOrEmpty(ddlTypePage.SelectedValue) || caseNotiseval != null)
                {
                    if (ddlTypePage.SelectedValue != "-1" || caseNotiseval != null)
                    {
                        if (ddlTypePage.SelectedValue == "T" || caseNotiseval == "T")
                        {
                            grdTaskActivity.Visible = true;
                            grdTaskActivity.DataSource = null;
                            grdTaskActivity.DataBind();
                            grdMyDocument.Visible = false;
                            grdMyDocument.DataSource = null;
                            grdMyDocument.DataBind();
                        }
                        else
                        {
                            grdMyDocument.Visible = true;
                            grdMyDocument.DataSource = null;
                            grdMyDocument.DataBind();
                            grdTaskActivity.Visible = false;
                            grdTaskActivity.DataSource = null;
                            grdTaskActivity.DataBind();
                        }
                        if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                        {
                            caseStatus = Convert.ToInt32(ddlStatus.SelectedValue);
                        }

                        if (!string.IsNullOrEmpty(ddlNoticeTypePage.SelectedValue))
                        {
                            caseType = ddlNoticeTypePage.SelectedItem.Text;
                        }

                        if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                        {
                            branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                        }

                        if (!string.IsNullOrEmpty(ddlPartyPage.SelectedValue))
                        {
                            partyID = Convert.ToInt32(ddlPartyPage.SelectedValue);
                        }

                        if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                        {
                            deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                        }

                        var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                        type = ddlTypePage.SelectedValue;

                        var DocList = LitigationTaskManagement.GetAssignedTaskList(AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, -1, partyID, deptID, caseStatus, "B", AuthenticationHelper.CustomerID);
                        DocList = DocList.Where(entry => entry.NoticeCaseInstanceID == 0).ToList();
                        // var DocList = LitigationDocumentManagement.GetAllDocumentListofCase(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, caseStatus, caseType);

                        //if (DocList.Count > 0)
                        //    DocList = DocList.Where(row => row.DocumentCount > 0).ToList();

                        string SortExpr = string.Empty;
                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                        {
                            SortExpr = Convert.ToString(ViewState["SortExpression"]);
                            if (SortExpr == e.SortExpression)
                            {
                                if (direction == SortDirection.Ascending)
                                {
                                    direction = SortDirection.Descending;
                                }
                                else
                                {
                                    direction = SortDirection.Ascending;
                                }
                            }
                            else
                            {
                                direction = SortDirection.Ascending;
                            }
                        }

                        if (direction == SortDirection.Ascending)
                        {
                            ViewState["Direction"] = "Ascending";
                            DocList = DocList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            ViewState["Direction"] = "Descending";
                            DocList = DocList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        }

                        ViewState["SortExpression"] = e.SortExpression;

                        foreach (DataControlField field in grdTaskActivity.Columns)
                        {
                            if (field.SortExpression == e.SortExpression)
                            {
                                ViewState["SortIndex"] = grdTaskActivity.Columns.IndexOf(field);
                            }
                        }
                        Session["TotalRows"] = null;
                        flag = true;
                        if (DocList.Count > 0)
                        {
                            grdTaskActivity.DataSource = DocList;
                            grdTaskActivity.DataBind();
                        }
                        else
                        {
                            grdTaskActivity.DataSource = DocList;
                            grdTaskActivity.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskActivity_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void grdTaskActivity_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Download")
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');

                    var lstTaskDocument = LitigationTaskManagement.GetTaskDocuments(Convert.ToInt64(commandArg[0]), 0);

                    if (lstTaskDocument.Count > 0)
                    {
                        using (ZipFile responseDocZip = new ZipFile())
                        {
                            int i = 0;
                            foreach (var file in lstTaskDocument)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    int idx = file.FileName.LastIndexOf('.');
                                    string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                    if (!responseDocZip.ContainsEntry(file.CreatedByText + "/" + str))
                                    {
                                        if (file.EnType == "M")
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            responseDocZip.AddEntry(file.CreatedByText + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                    }
                                    i++;
                                }
                            }

                            var zipMs = new MemoryStream();
                            responseDocZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] Filedata = zipMs.ToArray();
                            Response.Buffer = true;
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                            Response.BinaryWrite(Filedata);
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    else
                    {
                        cvErrorNoticePage.IsValid = false;
                        cvErrorNoticePage.ErrorMessage = "No Document Available for Download.";
                        return;
                    }
                }
                else if (e.CommandName == "View")
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');

                    var lstTaskDocument = LitigationTaskManagement.GetTaskDocuments(Convert.ToInt64(commandArg[0]), 0);

                    if (lstTaskDocument != null)
                    {
                        List<tbl_LitigationFileData> entitiesData = lstTaskDocument.Where(entry => entry.Version != null).ToList();
                        if (lstTaskDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                        {
                            tbl_LitigationFileData entityData = new tbl_LitigationFileData();
                            entityData.Version = "1.0";
                            entityData.NoticeCaseInstanceID = Convert.ToInt64(commandArg[0]);
                            entityData.DocTypeInstanceID = Convert.ToInt64(commandArg[2]);
                            entitiesData.Add(entityData);
                        }

                        if (entitiesData.Count > 0)
                        {
                            foreach (var file in lstTaskDocument)
                            {
                                rptLitigationVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                rptLitigationVersionView.DataBind();
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);

                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    LitigatinoDocPath = FileName;

                                    LitigatinoDocPath = LitigatinoDocPath.Substring(2, LitigatinoDocPath.Length - 2);

                                    lblMessage.Text = "";
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileLitigation('" + LitigatinoDocPath + "');", true);
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLitigation();", true);
                                }
                                break;
                            }
                            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                        }
                        else
                        {
                            //lblMessage.Text = "There is no file to preview";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocfileLitigation();", true);
                        }
                    }
                }
                applyCSStoFileTag_ListItems();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorNoticePage.IsValid = false;
                cvErrorNoticePage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lstBoxFileTags_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string caseNotiseval = string.Empty;
                if (Session["CaseNoticeflag"] != null)
                {
                    caseNotiseval = Session["CaseNoticeflag"].ToString();
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlTypePage.SelectedValue))
                    {
                        caseNotiseval = ddlTypePage.SelectedValue;
                    }
                }
                BindGrid(caseNotiseval);
                bindPageNumber();

                List<ListItem> lstSelectedTags = new List<ListItem>();

                foreach (ListItem eachListItem in lstBoxFileTags.Items)
                {
                    if (eachListItem.Selected)
                        lstSelectedTags.Add(eachListItem);
                }

                //Re-Arrange File Tags
                var arrangedListItems = LitigationManagement.ReArrange_FileTags(lstBoxFileTags);

                lstBoxFileTags.DataSource = arrangedListItems;
                lstBoxFileTags.DataBind();

                foreach (ListItem eachListItem in lstSelectedTags)
                {
                    if (lstBoxFileTags.Items.FindByValue(eachListItem.Value) != null)
                        lstBoxFileTags.Items.FindByValue(eachListItem.Value).Selected = true;
                }

                applyCSStoFileTag_ListItems();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindFileTags(string Flag)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                List<sp_LitigationNoticeFileTag_Result> lstTags = new List<sp_LitigationNoticeFileTag_Result>();
                List<sp_LitigationCaseFileTag_Result> lstTagsCase = new List<sp_LitigationCaseFileTag_Result>();
                List<string> lstTagsTask = new List<string>();
                if (Flag == "N")
                {
                    lstTags = LitigationDocumentManagement.GetDistinctFileTagsNoticeMyDocument(customerID, 0, AuthenticationHelper.UserID, AuthenticationHelper.Role);
                    lstBoxFileTags.Items.Clear();
                    if (lstTags.Count > 0)
                    {
                        foreach (var item in lstTags)
                        {
                            lstBoxFileTags.Items.Add(new ListItem(item.FileTag, item.FileTag));
                        }
                        //for (int i = 0; i < lstTags.Count; i++)
                        //{
                        //    lstBoxFileTags.Items.Add(new ListItem(lstTags[i].ToString(), lstTags[i].ToString()));
                        //}
                    }
                    if (lstTags.Count > 0)
                        outerDivFileTags.Visible = true;
                    else
                        outerDivFileTags.Visible = false;
                }
                else if (Flag == "C")
                {
                    lstTagsCase = LitigationDocumentManagement.GetDistinctFileTagsCaseMyDocument(customerID, 0, AuthenticationHelper.UserID, AuthenticationHelper.Role);
                    lstBoxFileTags.Items.Clear();
                    if (lstTagsCase.Count > 0)
                    {
                        foreach (var item in lstTagsCase)
                        {
                            lstBoxFileTags.Items.Add(new ListItem(item.FileTag, item.FileTag));
                        }
                        //for (int i = 0; i < lstTagsCase.Count; i++)
                        //{
                        //    // lstBoxFileTags.Items.Add(new ListItem(lstTagsCase[i].ToString(), lstTagsCase[i].ToString()));
                        //    lstBoxFileTags.Items.Add(new ListItem(lstTagsCase[i].FileTag.ToString(), lstTagsCase[i].FileTag.ToString()));
                        //}
                    }
                    if (lstTagsCase.Count > 0)
                        outerDivFileTags.Visible = true;
                    else
                        outerDivFileTags.Visible = false;
                }
                else if (Flag == "T")
                {
                    lstTagsTask = LitigationDocumentManagement.GetDistinctFileTagsTasks(customerID, 0);
                    lstBoxFileTags.Items.Clear();
                    if (lstTagsTask.Count > 0)
                    {

                        for (int i = 0; i < lstTagsTask.Count; i++)
                        {
                            lstBoxFileTags.Items.Add(new ListItem(lstTagsTask[i].ToString(), lstTagsTask[i].ToString()));
                        }
                    }
                    if (lstTagsTask.Count > 0)
                        outerDivFileTags.Visible = true;
                    else
                        outerDivFileTags.Visible = false;
                }
                applyCSStoFileTag_ListItems();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void applyCSStoFileTag_ListItems()
        {
            foreach (ListItem eachItem in lstBoxFileTags.Items)
            {
                if (eachItem.Selected)
                    eachItem.Attributes.Add("class", "item label label-info-selected");
                else
                    eachItem.Attributes.Add("class", "item label label-info");
            }
        }

        protected void lnkCase_Click(object sender, EventArgs e)
        {
            Session.Remove("CaseNoticeflag");
            liTask.Attributes.Add("class", "");
            liNotice.Attributes.Add("class", "");
            liCase.Attributes.Add("class", "active");
            CaseNoticeflag = "C";
            Session["CaseNoticeflag"] = CaseNoticeflag;
            BindFileTags(CaseNoticeflag);
            BindGrid(CaseNoticeflag);
            bindPageNumber();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);
        }

        protected void lnkNotice_Click(object sender, EventArgs e)
        {
            Session.Remove("CaseNoticeflag");
            liTask.Attributes.Add("class", "");
            liCase.Attributes.Add("class", "");
            liNotice.Attributes.Add("class", "active");
            CaseNoticeflag = "N";
            Session["CaseNoticeflag"] = CaseNoticeflag;
            BindFileTags(CaseNoticeflag);
            BindGrid(CaseNoticeflag);

            bindPageNumber();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);
        }

        protected void LinkTask_Click(object sender, EventArgs e)
        {
            Session.Remove("CaseNoticeflag");
            liCase.Attributes.Add("class", "");
            liNotice.Attributes.Add("class", "");
            liTask.Attributes.Add("class", "active");
            CaseNoticeflag = "T";
            Session["CaseNoticeflag"] = CaseNoticeflag;
            BindFileTags(CaseNoticeflag);
            BindGrid(CaseNoticeflag);
            bindPageNumber();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);
        }
        private void ClearTreeViewSelection(TreeView tree)
        {
            if (tree.SelectedNode != null)
            {
                tree.SelectedNode.Selected = false;
            }
        }
        protected void lnkBtnClearFilter_Click(object sender, EventArgs e)
        {
            ddlStatus.ClearSelection();
            ddlNoticeTypePage.ClearSelection();
            ddlPartyPage.ClearSelection();
            ddlDeptPage.ClearSelection();
            ddlNoticeTypePage.ClearSelection();
            ddlPartyPage.ClearSelection();
            ClearTreeViewSelection(tvFilterLocation);
            applyCSStoFileTag_ListItems();
        }

        protected void lbtsearchDocuments_Click(object sender, EventArgs e)
        {
            try
            {

                BindDocumentsGrid();
                bindPageNumber();

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);
                applyCSStoFileTag_ListItems();

            }
            catch (Exception ex)
            {

            }
        }

        private void BindDocumentsGrid()
        {
            if (Session["CaseNoticeflag"] != null)
            {
                string casenotistaskval = Session["CaseNoticeflag"].ToString();
                BindGrid(casenotistaskval);
                bindPageNumber();
            }
            else
            {
                BindGrid("C");
                bindPageNumber();
            }
            applyCSStoFileTag_ListItems();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "Scrolling();", true);
        }
    }
}