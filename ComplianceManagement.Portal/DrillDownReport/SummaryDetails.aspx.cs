﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal.DrillDownReport
{
    public partial class SummaryDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindSummaryDetails();
            }
        }

        protected void BindSummaryDetails()
        {
            try
            {
                int customerID = -1;
                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                Session["BackURL"] = Request.UrlReferrer.AbsolutePath;
                
                if (Session["FromDate"] != null && Session["EndDate"] != null)
                {
                    DateTime fromdate = Convert.ToDateTime(Session["FromDate"]);
                    DateTime todate = Convert.ToDateTime(Session["EndDate"]);

                    int role = Convert.ToInt32(Session["Role"]);

                    var ComplianceTransaction = DashboardManagement.GetComplianceDashboardSummary(customerID).ToList()
                                                .Where(entry => entry.UserID == AuthenticationHelper.UserID && entry.RoleID == role && entry.ScheduledOn > fromdate && entry.ScheduledOn < todate)
                                                .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault());

                    if(Session["StatusID"] != null){
                        List<int> Statusids= Session["StatusID"].ToString().Split(',').Select(int.Parse).ToList();
                        ComplianceTransaction = ComplianceTransaction.Where(entry => Statusids.Contains((int)entry.ComplianceStatusID));
                    }

                    if (!(Convert.ToString(Session["Branch"]).Equals("-1")))
                    {
                        int branchId = Convert.ToInt32(Session["Branch"]);
                        ComplianceTransaction = ComplianceTransaction.Where(entry => entry.CustomerBranchID == branchId);
                    }

                    if (Session["Filter"] != null)
                    {
                        int typid;

                        if (Convert.ToString(Session["Filter"]).Equals("Category"))
                        {
                            typid = Convert.ToInt32(Session["ID"]);
                            if (Session["ChartCode"] != null)
                            {
                                int chartcode = Convert.ToInt32(Session["ChartCode"]);
                                ComplianceTransaction = ComplianceTransaction.Where(entry => entry.ComplianceCategoryId == typid && entry.Risk == chartcode);
                            }
                            else
                            {
                                ComplianceTransaction = ComplianceTransaction.Where(entry => entry.ComplianceCategoryId == typid);
                            }
                        }
                        else if (Convert.ToString(Session["Filter"]).Equals("Risk"))
                        {
                            typid = Convert.ToInt32(Session["ID"]);
                            if (Session["ChartCode"] != null)
                            {
                                int chartcode = Convert.ToInt32(Session["ChartCode"]);
                                ComplianceTransaction = ComplianceTransaction.Where(entry => entry.Risk == typid && entry.ComplianceCategoryId == chartcode);
                            }
                            else
                            {
                                ComplianceTransaction = ComplianceTransaction.Where(entry => entry.Risk == typid);
                            }
                        }
                        else
                        {
                            if (Convert.ToString(Session["subFilter"]).Equals("Risk"))
                            {
                                typid = Convert.ToInt32(Session["ID"]);
                                if (Session["ChartCode"] != null)
                                {
                                    int chartcode = Convert.ToInt32(Session["ChartCode"]);
                                    ComplianceTransaction = ComplianceTransaction.Where(entry => entry.Risk == typid && entry.ComplianceCategoryId == chartcode);
                                }
                                else
                                {
                                    ComplianceTransaction = ComplianceTransaction.Where(entry => entry.Risk == typid);
                                }
                            }
                            else
                            {
                                typid = Convert.ToInt32(Session["ID"]);
                                if (Session["ChartCode"] != null)
                                {
                                    int chartcode = Convert.ToInt32(Session["ChartCode"]);
                                    ComplianceTransaction = ComplianceTransaction.Where(entry => entry.ComplianceCategoryId == typid && entry.Risk == chartcode);
                                }
                                else
                                {
                                    ComplianceTransaction = ComplianceTransaction.Where(entry => entry.ComplianceCategoryId == typid);
                                }
                            }
                        }
                    }
                   

                    grdSummaryDetails.DataSource = ComplianceTransaction;
                    grdSummaryDetails.DataBind();
                }
               
                

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
               
            }
        }

        protected void btnBack_OnClick(object sender, EventArgs e)
        {
            Response.Redirect(Convert.ToString(Session["BackURL"]), false);
            Session["ChartCode"] = null;
        }
    }
}