﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AzureSAMLResponse.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.AzureSAMLResponse" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="AVANTIS - Development Team" />

    <title>Login :: AVANTIS - Products that simplify</title>  
    <!-- Bootstrap CSS -->
     <link href="NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="NewCSS/bootstrap-theme.css" rel="stylesheet" />             
    <!--external css-->
    <!-- font icon -->
    <link href="NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="NewCSS/style.css" rel="stylesheet" />
    <link href="NewCSS/style-responsive.css" rel="stylesheet" />
    
    <style type="text/css">       
        .login-form h2 {
            text-align : center;
        }
    </style>
    
    <script type="text/javascript">
        function pushDetails() {
            debugger;
            //alert("called");
            var status = document.getElementById('<%=hdnStatus.ClientID%>').value;	    
            var errMessage = document.getElementById('<%=hdnErrMessage.ClientID%>').value;
                      
            var value = { "status": status, "message": errMessage }

            try {
                webkit.messageHandlers.submitToiOS.postMessage(value); //for iOS
            } catch (err) {
                console.log('error');
            }

            try {
                AndroidFunction.showToast(JSON.stringify(value));     // for Android
            } catch (err) {
                console.log('error');
            }
        }

        function CheckUserAgent(IsSuccess) {
             //alert(IsSuccess);
            var standalone = window.navigator.standalone,
                userAgent = window.navigator.userAgent.toLowerCase(),
                safari = /safari/.test(userAgent),
                ios = /iphone|ipod|ipad/.test(userAgent);

            if (ios) {
                if (!standalone && safari) {
                    //alert("safari");
                    document.getElementById('<%= hdnButton.ClientID %>').click();
                } else if (!standalone && !safari) {
                    //alert("iOS webview");// iOS webview
                    pushDetails();
                };
            } else {
                if (userAgent.includes('wv')) {
                    //alert("Android webview");// Android webview
                    pushDetails();
                } else {
                    //alert("Chrome");// Chrome
                    if (IsSuccess == 1) {
                        document.getElementById('<%= hdnButton.ClientID %>').click();
                    }
                }
            };
        }
    </script>
</head>
<body>
    <div class="container">
        <%--<form runat="server" class="login-form" name="login" id="loginForm" autocomplete="off">--%>
            <form runat="server" class="login-form" name="login">
            
                <asp:HiddenField ID="hdnStatus" runat="server" Value="" />
            <asp:HiddenField ID="hdnErrMessage" runat="server" Value="" />

            <asp:Button ID="hdnButton" runat="server" OnClick="dtbutton_Click" Style="display: none" />

            <asp:ScriptManager ID="ScriptManager2" runat="server" />
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="col-md-12 login-form-head">
                        <p class="login-img">
                            <a href="#">
                                <img src="Images/TeamLease.png" /></a>
                        </p>
                    </div>

                    <div class="login-wrap">
                        <div id="divLoading" class="row" runat="server" visible="true">
                            <div style="text-align: center;">
                                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing1.gif" AlternateText="Processing..." ToolTip="Processing ..." />
                            </div>

                            <div class="clearfix"></div>

                            <h2>
                                Please wait while we are authenticating your credentials, upon successful authentication you will to redirected to AVACOM
                            </h2>
                        </div>
                        <div class="clearfix" style="height: 10px"></div>
                    </div>
                    <div class="login-wrap">
                        <div id="divLogin" class="row" runat="server">
                            <h1></h1>

                            <div class="clearfix"></div>

                            <asp:CustomValidator ID="cvLogin" class="alert alert-block alert-danger fade in" EnableClientScript="False" runat="server" Display="None" />
                            <asp:ValidationSummary ID="vsLogin" class="alert alert-block alert-danger fade in" runat="server" />

                        </div>
                        <div class="clearfix" style="height: 10px"></div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </form>
    </div>
    <div class="clearfix" style="height: 10px"></div>
    <script type="text/javascript" src="Newjs/jquery.js"></script>
    <script type='text/javascript' src="Newjs/bootstrap.min.js"></script>
</body>
</html>
