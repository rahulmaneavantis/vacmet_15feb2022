﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Properties;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Web.Routing;
using System.Web.Configuration;
using System.Diagnostics;
using System.Web.Caching;
using System.Net;
using System.Xml;
using System.IO;
using System.Text;
using System.Web.Mvc;
using System.Web.Http;
using BM_Manegment.App_Start;
using Newtonsoft.Json;
using System.Web.Optimization;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public class Global : System.Web.HttpApplication
    {
        private string DummyPageUrl = ConfigurationManager.AppSettings["DummyPageUrl"].ToString();
        private const string DummyCacheItemKey = "GagaGuguGigi";

        protected void Application_Start(object sender, EventArgs e)
        {
            try
            {
                com.VirtuosoITech.Logger.Logger.Instance.Start();
                RegisterCacheEntry();

                AreaRegistration.RegisterAllAreas();

                RegisterRoutes(RouteTable.Routes);
                GlobalConfiguration.Configure(WebApiConfig.Register);
                RouteConfig.RegisterRoutes(RouteTable.Routes);
                //DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(RequiredIfAttribute), typeof(RequiredAttributeAdapter));

                BundleConfig.RegisterBundles(BundleTable.Bundles);

                //RouteConfig.RegisterRoutes(RouteTable.Routes);
                AutofacConfig.ConfigureContainer();
                GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

                GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;

                HttpConfiguration config = GlobalConfiguration.Configuration;
                config.Formatters.JsonFormatter
                            .SerializerSettings
                            .ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                //DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(RequiredIfAttribute),
                //                                      typeof(RequiredAttributeAdapter));
                var binder = new MyDateTimeModelBinder(); ModelBinders.Binders.Add(typeof(DateTime), binder);

                var binderNull = new MyDateTimeNullModelBinder(); ModelBinders.Binders.Add(typeof(DateTime?), binderNull);


                //Register Syncfusion license
                string syncFusionLicenseKey = ConfigurationManager.AppSettings["SyncfusionLicenseKey"].ToString();
                Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense(syncFusionLicenseKey);

                //com.VirtuosoITech.Logger.Logger.Instance.Start();
                //RegisterCacheEntry();
                //RegisterRoutes(RouteTable.Routes);

                //GlobalConfiguration.Configure(WebApiConfig.Register);
                //RouteConfig.RegisterRoutes(RouteTable.Routes);
                ////DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(RequiredIfAttribute), typeof(RequiredAttributeAdapter));
                ////AreaRegistration.RegisterAllAreas();
                //BundleConfig.RegisterBundles(BundleTable.Bundles);

                ////RouteConfig.RegisterRoutes(RouteTable.Routes);
                //AutofacConfig.ConfigureContainer();
                //GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

                //GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;

                //HttpConfiguration config = GlobalConfiguration.Configuration;
                //config.Formatters.JsonFormatter
                //            .SerializerSettings
                //            .ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                ////DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(RequiredIfAttribute),
                ////                                      typeof(RequiredAttributeAdapter));
                //var binder = new MyDateTimeModelBinder(); ModelBinders.Binders.Add(typeof(DateTime), binder);

                //var binderNull = new MyDateTimeNullModelBinder(); ModelBinders.Binders.Add(typeof(DateTime?), binderNull);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void Session_Start(object sender, EventArgs e)
        {
            try
            {                            
                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    FormsAuthentication.SignOut();
                    FormsAuthentication.RedirectToLoginPage("Session=Expired");
                    HttpContext.Current.Response.End();
                }
                Session.Timeout = 20;
                //EncryptWebConfig();
                //DecryptWebConfig();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

            RedirectRoutedURL();
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
        
        private static void RegisterRoutes(RouteCollection routes)
        {            
            routes.MapPageRoute("ContactUs/AboutUs", "ContactUs/AboutUs", "~/ContactUs/AboutUs.aspx");
            routes.MapPageRoute("ContactUs/Product", "ContactUs/Product", "~/ContactUs/Product.aspx");
            routes.MapPageRoute("Default", "Default", "~/Default.aspx");
            routes.MapPageRoute("InternalCompliance/AssignInternalCompliance", "InternalCompliance/AssignInternalCompliance", "~/InternalCompliance/AssignInternalCompliance.aspx");
            routes.MapPageRoute("InternalCompliance/IComplianceTypeList", "InternalCompliance/IComplianceTypeList", "~/InternalCompliance/IComplianceTypeList.aspx");
            routes.MapPageRoute("InternalCompliance/InternalComplianceList", "InternalCompliance/InternalComplianceList", "~/InternalCompliance/InternalComplianceList.aspx");
            routes.MapPageRoute("Compliances/ActList", "Compliances/ActList", "~/Compliances/ActList.aspx");
            routes.MapPageRoute("InternalCompliance/InternalActList", "InternalCompliance/InternalActList", "~/InternalCompliance/InternalActList.aspx");
            routes.MapPageRoute("Compliances/ComplianceList", "Compliances/ComplianceList", "~/Compliances/ComplianceList.aspx");
            routes.MapPageRoute("Account/ChangePassword", "Account/ChangePassword", "~/Account/ChangePassword.aspx");
            routes.MapPageRoute("Account/UploadMasterData", "Account/UploadMasterData", "~/Account/UploadMasterData.aspx");
            routes.MapPageRoute("Common/ComplianceDashboard", "Common/ComplianceDashboard", "~/Common/ComplianceDashboard.aspx");
            routes.MapPageRoute("Common/ComplianceDashboardDisplay/{filter}/{Role}/{Type}", "Common/ComplianceDashboardDisplay/{filter}/{Role}/{Type}", "~/Common/ComplianceDashboardDisplay.aspx");
            routes.MapPageRoute("Controls/CompanyAdminUpCompliances/{filter}/{role}/{type}", "Controls/CompanyAdminUpCompliances/{filter}/{role}/{type}", "~/Controls/CompanyAdminUpCompliances.aspx");
            routes.MapPageRoute("Controls/MYWORKSPACE", "Controls/MYWORKSPACE", "~/Controls/frmUpcomingCompliancess.aspx?type=Statutory&filter=Upcoming&role=Performer");
            routes.MapPageRoute("Controls/UpcomingInternalCompliances/{filter}/{Role}/{Type}", "Controls/UpcomingInternalCompliances/{filter}/{Role}/{Type}", "~/Controls/frmUpcomingInternalCompliance.aspx");
            routes.MapPageRoute("Controls/OverdueCompliances/{filter}/{Role}/{Type}", "Controls/OverdueCompliances/{filter}/{Role}/{Type}", "~/Controls/frmOverdueCompliance.aspx");
            routes.MapPageRoute("Controls/OverdueInternalCompliances/{filter}/{Role}/{Type}", "Controls/OverdueInternalCompliances/{filter}/{Role}/{Type}", "~/Controls/frmOverdueInternalCompliance.aspx");
            routes.MapPageRoute("Controls/PendingForReviewCompliance/{filter}/{Role}/{Type}", "Controls/PendingForReviewCompliance/{filter}/{Role}/{Type}", "~/Controls/frmPendingForReviewCompliance.aspx");
            routes.MapPageRoute("Controls/PendingForReviewInternalCompliance/{filter}/{Role}/{Type}", "Controls/PendingForReviewInternalCompliance/{filter}/{Role}/{Type}", "~/Controls/frmPendingForReviewInternalCompliance.aspx");
            routes.MapPageRoute("Controls/PendingforApprovelsCompliance/{filter}/{Role}/{Type}", "Controls/PendingforApprovelsCompliance/{filter}/{Role}/{Type}", "~/Controls/frmPendingforApprovelsCompliance.aspx");
            routes.MapPageRoute("Controls/PendingforApprovelsInternalCompliance/{filter}/{Role}/{Type}", "Controls/PendingforApprovelsInternalCompliance/{filter}/{Role}/{Type}", "~/Controls/frmPendingforApprovelsInternalCompliance.aspx");
            routes.MapPageRoute("Common/ComplianceManagementReport", "Common/ComplianceManagementReport", "~/Common/ComplianceManagementReport.aspx");
            routes.MapPageRoute("Common/Configuration", "Common/Configuration", "~/Common/Configuration.aspx");
            routes.MapPageRoute("Compliances/AssignCompliance", "Compliances/AssignCompliance", "~/Compliances/AssignCompliance.aspx");
            routes.MapPageRoute("Compliances/AssignEntitiesToManagement", "Compliances/AssignEntitiesToManagement", "~/Compliances/AssignEntitiesToManagement.aspx");
            routes.MapPageRoute("Compliances/CannedReports", "Compliances/CannedReports", "~/Compliances/CannedReports.aspx");            
            routes.MapPageRoute("Compliances/CompanyAdminChList", "Compliances/CompanyAdminChList", "~/Compliances/CompanyAdminChList.aspx");
            routes.MapPageRoute("Compliances/ComplianceCategoryList", "Compliances/ComplianceCategoryList", "~/Compliances/ComplianceCategoryList.aspx");
            routes.MapPageRoute("Compliances/ComplianceTrackingSummary", "Compliances/ComplianceTrackingSummary", "~/Compliances/ComplianceTrackingSummary.aspx");
            routes.MapPageRoute("Compliances/ComplianceTransactionList", "Compliances/ComplianceTransactionList", "~/Compliances/ComplianceTransactionList.aspx");
            routes.MapPageRoute("Compliances/ComplianceTypeList", "Compliances/ComplianceTypeList", "~/Compliances/ComplianceTypeList.aspx");
            routes.MapPageRoute("Customers/CustomerBranchList", "Customers/CustomerBranchList", "~/Customers/CustomerBranchList.aspx");
            routes.MapPageRoute("Customers/CustomerList", "Customers/CustomerList", "~/Customers/CustomerList.aspx");
            routes.MapPageRoute("Management/ManagementGradingReport", "Management/ManagementGradingReport", "~/Management/ManagementGradingReport.aspx");
            routes.MapPageRoute("Management/ManagementSummaryStatusReport", "Management/ManagementSummaryStatusReport", "~/Management/ManagementSummaryStatusReport.aspx");
            routes.MapPageRoute("Management/MangementDashboard", "Management/MangementDashboard", "~/Management/ManagementDashboardNew.aspx");
            routes.MapPageRoute("Users/UserList", "Users/UserList", "~/Users/UserList.aspx");
            routes.MapPageRoute("Users/UserParameterList", "Users/UserParameterList", "~/UserParameterList.aspx");
            routes.MapPageRoute("Users/UserReminders", "Users/UserReminders", "~/Users/UserReminders.aspx");
            routes.MapPageRoute("Users/UserSummary", "Users/UserSummary", "~/Users/UserSummary.aspx");
            routes.MapPageRoute("ContactUs/ContactUs", "ContactUs/ContactUs", "~/ContactUs/ContactUs.aspx");
            routes.MapPageRoute("ContactUs/Features", "ContactUs/Features", "~/ContactUs/Features.aspx");
            routes.MapPageRoute("ContactUs/SignUp", "ContactUs/SignUp", "~/ContactUs/SignUp.aspx");
            routes.MapPageRoute("Compliances/ApproveCompliances", "Compliances/ApproveCompliances", "~/Compliances/ApproveCompliances.aspx");
            // routes.MapPageRoute("ComplianceDocument/ComplianceDocumentList", "ComplianceDocument/ComplianceDocumentList", "~/ComplianceDocument/ComplianceDocumentList.aspx");
            routes.MapPageRoute("ComplianceDocument/ComplianceDocumentList", "ComplianceDocument/ComplianceDocumentList", "~/ComplianceDocument/ComplianceDocumentListNew1.aspx");
            routes.MapPageRoute("Compliances/ReviseCompliances", "Compliances/ReviseCompliances", "~/Compliances/ReviseCompliances.aspx");
            routes.MapPageRoute("Event/EventList", "Event/EventList", "~/Event/EventList.aspx");

            routes.MapPageRoute("Event/EventComplianceAssignDays", "Event/EventComplianceAssignDays", "~/Event/EventComplianceAssignDays.aspx");
            routes.MapPageRoute("Event/EventParentChildRelationship", "Event/EventParentChildRelationship", "~/Event/EventParentChildRelationship.aspx");
            routes.MapPageRoute("Event/EventStructure", "Event/EventStructure", "~/Event/EventStructure.aspx");
            routes.MapPageRoute("Event/EventSequence", "Event/EventSequence", "~/Event/FrmEventSequence.aspx");
            routes.MapPageRoute("Event/EventComplianceList", "Event/EventComplianceList", "~/Event/EventComplianceList.aspx");
            routes.MapPageRoute("Compliances/ComplianceDeactivate", "Compliances/ComplianceDeactivate", "~/Compliances/ComplianceDeactivate.aspx");

            routes.MapPageRoute("Account/ExportCountExcel", "Account/ExportCountExcel", "~/Account/ExportCountExcel.aspx");

            routes.MapPageRoute("Event/AssignEvents", "Event/AssignEvents", "~/Event/AssignEvents.aspx");
            routes.MapPageRoute("Event/EventDetails", "Event/EventDetails", "~/Event/EventDetails.aspx");

            routes.MapPageRoute("SecurityQuestion/AddSecurityQuestions", "SecurityQuestion/AddSecurityQuestions", "~/SecurityQuestion/AddSecurityQuestions.aspx");
            routes.MapPageRoute("SecurityQuestion/ForgotPassword", "SecurityQuestion/ForgotPassword", "~/SecurityQuestion/ForgotPassword.aspx");
            //routes.MapPageRoute("SecurityQuestion/ResetSecurityQuestions", "SecurityQuestion/ResetSecurityQuestions", "~/SecurityQuestion/ResetSecurityQuestions.aspx");
            routes.MapPageRoute("SQ/RSQ", "SQ/RSQ", "~/SecurityQuestion/ResetSecurityQuestions.aspx");

            routes.MapPageRoute("Compliances/ComplianceCannedReports", "Compliances/ComplianceCannedReports", "~/Compliances/ComplianceCannedReports.aspx");
            routes.MapPageRoute("Compliances/EventCannedReports", "Compliances/EventCannedReports", "~/Compliances/EventCannedReports.aspx");
            routes.MapPageRoute("Account/ErrorLogPage", "Account/ErrorLogPage", "~/Account/ErrorLogPage.aspx");
            routes.MapPageRoute("Common/MassMailing", "Common/MassMailing", "~/Common/MassMailing.aspx");
            
            routes.MapPageRoute("ComplianceDocument/InternalComplianceDocumentList", "ComplianceDocument/InternalComplianceDocumentList", "~/ComplianceDocument/InternalCompliance_DocumentList.aspx");            
            routes.MapPageRoute("InternalCompliance/InternalCanned_Reports", "InternalCompliance/InternalCanned_Reports", "~/InternalCompliance/InternalCanned_Reports.aspx");            
            routes.MapPageRoute("InternalCompliance/ICanned_Reports", "InternalCompliance/ICanned_Reports", "~/InternalCompliance/ICanned_Reports.aspx");            
            routes.MapPageRoute("InternalCompliance/InternalCompliance_TransactionList", "InternalCompliance/InternalCompliance_TransactionList", "~/InternalCompliance/InternalCompliance_TransactionList.aspx");

            
            routes.MapPageRoute("InternalCompliance/InternalAssignEntitiesToManagement", "InternalCompliance/InternalAssignEntitiesToManagement", "~/InternalCompliance/AssignEntities_ToManagementInternal.aspx");
            routes.MapPageRoute("Users/InternalUserReminders", "Users/InternalUserReminders", "~/Users/User_RemindersInternal.aspx");            
            routes.MapPageRoute("Account/ExportToExcel", "Account/ExportToExcel", "~/Account/ExportToExcel.aspx");
            routes.MapPageRoute("Account/ExcelExport_UsingFilter", "Account/ExcelExport_UsingFilter", "~/Account/ExcelExport_UsingFilter.aspx");
            routes.MapPageRoute("Account/UpdateCompliancesFromExcel", "Account/UpdateCompliancesFromExcel", "~/Account/UpdateCompliances.aspx");            
            routes.MapPageRoute("Compliances/Assign_Checklist", "Compliances/Assign_Checklist", "~/Compliances/Assign_Checklist.aspx");            
            routes.MapPageRoute("Compliances/Check_List_Reports", "Compliances/Check_List_Reports", "~/Compliances/Check_List_Reports.aspx");
            routes.MapPageRoute("Compliances/ChecklistCompletedNotCompletedReports", "Compliances/ChecklistCompletedNotCompletedReports", "~/Compliances/Completed_NotCompleted_ChecklistReports.aspx");            
            routes.MapPageRoute("Compliances/EditComplianceListMaster", "Compliances/EditComplianceListMaster", "~/Compliances/EditComplianceListMaster.aspx");
            routes.MapPageRoute("Common/CompanyStructure", "Common/CompanyStructure", "~/Common/CompanyStructure.aspx");
            routes.MapPageRoute("Controls/RejectedCompliances/{filter}/{Role}/{Type}", "Controls/RejectedCompliances/{filter}/{Role}/{Type}", "~/Controls/frmRejectedCompliancess.aspx");
            routes.MapPageRoute("Controls/EventUpcomingOverduePerformer/{filter}/{Role}/{Type}", "Controls/EventUpcomingOverduePerformer/{filter}/{Role}/{Type}", "~/Controls/frmEventUpcomingOverduePerformer.aspx");
            routes.MapPageRoute("Controls/EventUpcomingReviewer/{filter}/{Role}/{Type}", "Controls/EventUpcomingReviewer/{filter}/{Role}/{Type}", "~/Controls/frmEventUpcomingReviewer.aspx");
            routes.MapPageRoute("Compliances/Assign_ComplianceNew", "Compliances/Assign_ComplianceNew", "~/Compliances/Assign_ComplianceNew.aspx");
            routes.MapPageRoute("Compliances/Assign_Compliance_Activate", "Compliances/Assign_Compliance_Activate", "~/Compliances/Assign_Compliance_Activate.aspx");
            routes.MapPageRoute("Compliances/Assign_CheckListNew", "Compliances/Assign_CheckListNew", "~/Compliances/Assign_CheckListNew.aspx");            
            routes.MapPageRoute("Compliances/Assign_CheckList_Activate", "Compliances/Assign_CheckList_Activate", "~/Compliances/Assign_CheckList_Activate.aspx");
            routes.MapPageRoute("ProductMapping/ProductMappingDetails", "ProductMapping/ProductMappingDetails", "~/ProductMapping/ProductMappingDetails.aspx");
            routes.MapPageRoute("ProductMapping/MappingStructure", "ProductMapping/MappingStructure", "~/ProductMapping/ProductMappingStructure.aspx");
            routes.MapPageRoute("Customers/CompanyAdminStructure", "Customers/CompanyAdminStructure", "~/Customers/CompanyAdminStructure.aspx");

            routes.MapPageRoute("RiskManagement/AuditTool/RiskControlMatrix", "RiskManagement/AuditTool/RiskControlMatrix", "~/RiskManagement/AuditTool/RiskControlMatrix.aspx");
            routes.MapPageRoute("RiskManagement/AuditTool/RiskRegister", "RiskManagement/AuditTool/RiskRegister", "~/RiskManagement/AuditTool/RiskRegister.aspx");
            routes.MapPageRoute("Management/DepartmentHead_Dashboard", "Management/DepartmentHead_Dashboard", "~/Management/DepartmentHead_Dashboard.aspx");
            routes.MapPageRoute("RiskManagement/AuditTool/TestingStatusReport", "RiskManagement/AuditTool/TestingStatusReport", "~/RiskManagement/AuditTool/TestingStatusReport.aspx");
            routes.MapPageRoute("RiskManagement/AuditTool/PendingTestingReport", "RiskManagement/AuditTool/PendingTestingReport", "~/RiskManagement/AuditTool/PendingTestingReport.aspx");
            routes.MapPageRoute("RiskManagement/AuditTool/ReviewerCommentReport", "RiskManagement/AuditTool/ReviewerCommentReport", "~/RiskManagement/AuditTool/ReviewerCommentReport.aspx");                                   
            routes.MapPageRoute("RiskManagement/Controls/ToBeReviewed", "RiskManagement/Controls/ToBeReviewed", "~/RiskManagement/Controls/frmToBeReviewed.aspx");
            routes.MapPageRoute("Management/PersonResponsible_Dashboard", "Management/PersonResponsible_Dashboard", "~/Management/PersonResponsible_Dashboard.aspx");
            routes.MapPageRoute("Management/Internal_ControlDepartmentHeadDashboard", "Management/Internal_ControlDepartmentHeadDashboard", "~/Management/Internal_ControlDepartmentHeadDashboard.aspx");            
            routes.MapPageRoute("RiskManagement/InternalAuditTool/InternalControlReviewer", "RiskManagement/InternalAuditTool/InternalControlReviewer", "~/RiskManagement/InternalAuditTool/FrmInternalControlReviewer.aspx");
            routes.MapPageRoute("Management/Internal_ManagementDashboard", "Management/Internal_ManagementDashboard", "~/Management/Internal_ManagementDashboard.aspx");            
            routes.MapPageRoute("RiskManagement/Process/Process", "RiskManagement/Process/Process", "~/RiskManagement/Process/Process.aspx");            
            routes.MapPageRoute("RiskManagement/Customers/RiskCategory", "RiskManagement/Customers/RiskCategory", "~/RiskManagement/Customers/RiskCategory.aspx");
            routes.MapPageRoute("RiskManagement/Customers/AuditorMaster", "RiskManagement/Customers/AuditorMaster", "~/RiskManagement/Customers/AuditorMaster.aspx");
            routes.MapPageRoute("RiskManagement/Customers/AuditorUserMaster", "RiskManagement/Customers/AuditorUserMaster", "~/RiskManagement/Customers/AuditorUserMaster.aspx");
            routes.MapPageRoute("RiskManagement/Customers/AuditorPeriodMaster", "RiskManagement/Customers/AuditorPeriodMaster", "~/RiskManagement/Customers/AuditorPeriodMaster.aspx");
            routes.MapPageRoute("RiskManagement/InternalAuditTool/ObservationCategory", "RiskManagement/InternalAuditTool/ObservationCategory", "~/RiskManagement/InternalAuditTool/ObservationCategory.aspx");            
            routes.MapPageRoute("RiskManagement/InternalAuditTool/LocationMaster", "RiskManagement/InternalAuditTool/LocationMaster", "~/RiskManagement/InternalAuditTool/LocationMaster.aspx");
            routes.MapPageRoute("Account/UploadAdditionalRiskCreation", "Account/UploadAdditionalRiskCreation", "~/Account/UploadAdditionalRiskCreation.aspx");
            routes.MapPageRoute("RiskManagement/InternalAuditTool/AdditionalRiskCreation", "RiskManagement/InternalAuditTool/AdditionalRiskCreation", "~/RiskManagement/InternalAuditTool/AdditionalRiskCreation.aspx");            
            routes.MapPageRoute("RiskManagement/InternalAuditTool/AuditAssignment", "RiskManagement/InternalAuditTool/AuditAssignment", "~/RiskManagement/InternalAuditTool/AuditAssignment.aspx");
            routes.MapPageRoute("RiskManagement/InternalAuditTool/AuditKickOff", "RiskManagement/InternalAuditTool/AuditKickOff", "~/RiskManagement/InternalAuditTool/AuditKickOffNEW.aspx");
            routes.MapPageRoute("RiskManagement/InternalAuditTool/AuditScheduling", "RiskManagement/InternalAuditTool/AuditScheduling", "~/RiskManagement/InternalAuditTool/AuditSchedulingNew.aspx");
            routes.MapPageRoute("Management/InternalControlAuditManager_Dashboard", "Management/InternalControlAuditManager_Dashboard", "~/Management/InternalControlAuditManager_Dashboard.aspx");
            routes.MapPageRoute("RiskManagement/Common/AuditDashboard", "RiskManagement/Common/AuditDashboard", "~/RiskManagement/Common/AuditDashboard.aspx");
            routes.MapPageRoute("Account/UploadRiskCreation", "Account/UploadRiskCreation", "~/Account/UploadRiskCreation.aspx");
            routes.MapPageRoute("RiskManagement/AuditTool/RiskAssignment", "RiskManagement/AuditTool/RiskAssignment", "~/RiskManagement/AuditTool/RiskAssignment.aspx");
            routes.MapPageRoute("RiskManagement/AuditTool/AuditUpload", "RiskManagement/AuditTool/AuditUpload", "~/RiskManagement/AuditTool/AuditUpload.aspx");
            routes.MapPageRoute("RiskManagement/Customers/DepartmentMaster", "RiskManagement/Customers/DepartmentMaster", "~/RiskManagement/Customers/DepartmentMaster.aspx");
            routes.MapPageRoute("RiskManagement/InternalAuditTool/AssignEntitiesToAuditManager", "RiskManagement/InternalAuditTool/AssignEntitiesToAuditManager", "~/RiskManagement/InternalAuditTool/AssignEntitiesToAuditManager.aspx");
            routes.MapPageRoute("RiskManagement/Customers/RiskAndControlRating", "RiskManagement/Customers/RiskAndControlRating", "~/RiskManagement/Customers/RiskAndControlRating.aspx");
            routes.MapPageRoute("RiskManagement/InternalAuditTool/ObservationReportWord", "RiskManagement/InternalAuditTool/ObservationReportWord", "~/RiskManagement/InternalAuditTool/FrmObservationReportWord.aspx");
            routes.MapPageRoute("RiskManagement/InternalAuditTool/ProcessRiskRatingReport", "RiskManagement/InternalAuditTool/ProcessRiskRatingReport", "~/RiskManagement/InternalAuditTool/ProcessRiskRatingReport.aspx");
            routes.MapPageRoute("RiskManagement/InternalAuditTool/ObservationReportPowerPoint", "RiskManagement/InternalAuditTool/ObservationReportPowerPoint", "~/RiskManagement/InternalAuditTool/FrmObservationReportPowerPoint.aspx");
            routes.MapPageRoute("RiskManagement/InternalAuditTool/AuditFrequency", "RiskManagement/InternalAuditTool/AuditFrequency", "~/RiskManagement/InternalAuditTool/frmAuditFrequency.aspx");
            routes.MapPageRoute("RiskManagement/InternalAuditTool/AuditMatrix", "RiskManagement/InternalAuditTool/AuditMatrix", "~/RiskManagement/InternalAuditTool/frmAuditMatrix.aspx");
            routes.MapPageRoute("RiskManagement/InternalAuditTool/AuditManager", "RiskManagement/InternalAuditTool/AuditManager", "~/RiskManagement/InternalAuditTool/FrmAuditManager.aspx");
            routes.MapPageRoute("RiskManagement/InternalAuditTool/AuditClosure", "RiskManagement/InternalAuditTool/AuditClosure", "~/RiskManagement/InternalAuditTool/AuditClosure.aspx");
            routes.MapPageRoute("RiskManagement/InternalAuditTool/AuditClosure_Closed", "RiskManagement/InternalAuditTool/AuditClosure_Closed", "~/RiskManagement/InternalAuditTool/AuditClosure_Closed.aspx");
            routes.MapPageRoute("RiskManagement/InternalAuditTool/AuditSchedulingReport", "RiskManagement/InternalAuditTool/AuditSchedulingReport", "~/RiskManagement/InternalAuditTool/AuditSchedulingReportNew.aspx");                                                
            routes.MapPageRoute("RiskManagement/AuditTool/FailedControlReportPerformer", "RiskManagement/AuditTool/FailedControlReportPerformer", "~/RiskManagement/AuditTool/FailedControlReportPerformer.aspx");                                   
            routes.MapPageRoute("Account/UploadProcessCreation", "Account/UploadProcessCreation", "~/Account/UploadProcessCreation.aspx");                                   
            routes.MapPageRoute("Management/InternalControl_Dashboard", "Management/InternalControl_Dashboard", "~/Management/InternalControl_Dashboard.aspx");
            routes.MapPageRoute("Management/Risk_ManagementDashboard", "Management/Risk_ManagementDashboard", "~/Management/Risk_ManagementDashboard.aspx");
            routes.MapPageRoute("Management/InternalControlPersonResponsibleDashboard", "Management/InternalControlPersonResponsibleDashboard", "~/Management/InternalControlPersonResponsibleDashboard.aspx");
            routes.MapPageRoute("RiskManagement/InternalAuditTool/PersonResponsible", "RiskManagement/InternalAuditTool/PersonResponsible", "~/RiskManagement/InternalAuditTool/FrmPersonResponsible.aspx");
            routes.MapPageRoute("Compliances/PerformanceSummary", "Compliances/PerformanceSummary", "~/Compliances/frmPerformanceSummary.aspx");
            routes.MapPageRoute("RiskManagement/Common/InternalControlDashboard", "RiskManagement/Common/InternalControlDashboard", "~/RiskManagement/Common/InternalControlDashboard.aspx");
            routes.MapPageRoute("Compliances/IntermediateCompliance", "Compliances/IntermediateCompliance", "~/Compliances/IntermediateCompliance.aspx");
            routes.MapPageRoute("Compliances/ReassignToPerformer", "Compliances/ReassignToPerformer", "~/Compliances/ReassignReviewerToPerformer.aspx");            
            routes.MapPageRoute("Compliances/CannedReportsCA", "Compliances/CannedReportsCA", "~/Compliances/CannedReportsCA.aspx");
            routes.MapPageRoute("ComplianceDocument/ComplianceDocumentListCA", "ComplianceDocument/ComplianceDocumentListCA", "~/ComplianceDocument/ComplianceDocumentListCA.aspx");

            routes.MapPageRoute("Users/AuditUsers_List", "Users/AuditUsers_List", "~/Users/AuditUsers_List.aspx");
            routes.MapPageRoute("Customers/AuditCustomer_List", "Customers/AuditCustomer_List", "~/Customers/AuditCustomer_List.aspx");
            routes.MapPageRoute("Customers/AuditCustomerBranch_List", "Customers/AuditCustomerBranch_List", "~/Customers/AuditCustomerBranch_List.aspx");

            routes.MapPageRoute("Common/LegalUpdate", "Common/LegalUpdate", "~/Common/LegalUpdateAdmin.aspx");
            routes.MapPageRoute("Common/ResearchPerformer", "Common/ResearchPerformer", "~/Common/ResearchPerformerAdmin.aspx");
            routes.MapPageRoute("Common/ResearchReviewer", "Common/ResearchReviewer", "~/Common/ResearchReviewerAdmin.aspx");

        }

        private void RedirectRoutedURL()
        {                                                
            #region
            string url = HttpContext.Current.Request.Url.AbsolutePath;
            if (HttpContext.Current.Request.Url.AbsolutePath.ToLower().Equals("/dashboard") || HttpContext.Current.Request.Url.AbsolutePath.ToLower().Equals("/dashboard.aspx"))
            {
                HttpContext.Current.RewritePath("~/Common/Dashboard.aspx");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("AuditUsers_List.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Users/AuditUsers_List");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("AuditCustomer_List.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Customers/AuditCustomer_List");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("AuditCustomerBranch_List.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Customers/AuditCustomerBranch_List");
            }
            //else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("OTPVerify.aspx"))
            //{
            //    HttpContext.Current.Response.Redirect("~/Users/OTPVerify");
            //}
            else if (HttpContext.Current.Request.Url.AbsolutePath.ToLower().Equals("/workspace") || HttpContext.Current.Request.Url.AbsolutePath.ToLower().Equals("/myworkspace") || HttpContext.Current.Request.Url.AbsolutePath.ToLower().Equals("/myworkspace.aspx"))
            {

                if (HttpContext.Current.Request.Url.PathAndQuery.ToLower().Contains("?"))
                {
                    HttpContext.Current.RewritePath("~/Controls/frmUpcomingCompliancess.aspx?" + Request.QueryString.ToString());
                }
                else
                {
                    HttpContext.Current.RewritePath("~/Controls/frmUpcomingCompliancess.aspx");
                }
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.ToLower().Equals("/reports") || HttpContext.Current.Request.Url.AbsolutePath.ToLower().Equals("/myreports") || HttpContext.Current.Request.Url.AbsolutePath.ToLower().Equals("/myreports.aspx"))
            {

                if (HttpContext.Current.Request.Url.PathAndQuery.ToLower().Contains("?"))
                {
                    HttpContext.Current.RewritePath("~/Compliances/CannedReports.aspx?" + Request.QueryString.ToString());
                }
                else
                {
                    HttpContext.Current.RewritePath("~/Compliances/CannedReports.aspx");
                }
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.ToLower().Equals("/documents") || HttpContext.Current.Request.Url.AbsolutePath.ToLower().Equals("/mydocuments") || HttpContext.Current.Request.Url.AbsolutePath.ToLower().Equals("/mydocuments.aspx"))
            {

                if (HttpContext.Current.Request.Url.PathAndQuery.ToLower().Contains("?"))
                {
                    HttpContext.Current.RewritePath("~/ComplianceDocument/ComplianceDocumentList?" + Request.QueryString.ToString());
                }
                else
                {
                    HttpContext.Current.RewritePath("~/ComplianceDocument/ComplianceDocumentList");
                }
            }
           else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/InternalCompliance/InternalComplianceList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/InternalCompliance/InternalComplianceList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/InternalCompliance/IComplianceTypeList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/InternalCompliance/IComplianceTypeList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/InternalCompliance/AssignInternalCompliance.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/InternalCompliance/AssignInternalCompliance");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("ContactUs/Product.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/ContactUs/Product");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("ContactUs/AboutUs.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/ContactUs/AboutUs");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("InternalActList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/InternalCompliance/InternalActList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("ActList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/ActList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("ComplianceDashboard.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Common/ComplianceDashboard");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Compliances/ComplianceList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/ComplianceList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Account/ChangePassword.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Account/ChangePassword");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Account/UploadMasterData.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Account/UploadMasterData");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("ComplianceDashboard.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Common/ComplianceDashboard");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("ComplianceManagementReport.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Common/ComplianceManagementReport");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Configuration.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Common/Configuration");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Compliances/AssignCompliance.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/AssignCompliance");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("AssignEntitiesToManagement.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/AssignEntitiesToManagement");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("AssignEntities_ToManagementInternal.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/InternalCompliance/InternalAssignEntitiesToManagement");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("CompanyAdminChList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/CompanyAdminChList");
            }
            //else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("CheckList.aspx"))
            //{
            //    HttpContext.Current.Response.Redirect("~/Compliances/CheckList");
            //}
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Compliances/ComplianceCategoryList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/ComplianceCategoryList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("ApproveCompliances.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/ApproveCompliances");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("ComplianceTransactionList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/ComplianceTransactionList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Compliances/ComplianceTypeList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/ComplianceTypeList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("ComplianceTransactionList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/ComplianceTransactionList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("CustomerBranchList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Customers/CustomerBranchList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("CustomerList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Customers/CustomerList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("ManagementGradingReport.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Management/ManagementGradingReport", false);
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("ManagementSummaryStatusReport.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Management/ManagementSummaryStatusReport", false);
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("ManagementDashboardNew.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Management/MangementDashboard", false);
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("UserList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Users/UserList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("UserParameterList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Users/UserParameterList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("UserReminders.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Users/UserReminders");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("User_RemindersInternal.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Users/InternalUserReminders");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("UserSummary.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Users/UserSummary");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("ContactUs.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/ContactUs/ContactUs");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Features.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/ContactUs/Features");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("SignUp.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/ContactUs/SignUp");
            }
            //else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("ComplianceDocumentList.aspx"))
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("ComplianceDocumentListNew1.aspx"))                
            {
                HttpContext.Current.Response.Redirect("~/ComplianceDocument/ComplianceDocumentList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("InternalCompliance_DocumentList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/ComplianceDocument/InternalComplianceDocumentList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Compliances/ReviseCompliances.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/ReviseCompliances");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Event/EventList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Event/EventList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Event/EventComplianceAssignDays.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Event/EventComplianceAssignDays");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Event/EventParentChildRelationship.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Event/EventParentChildRelationship");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Event/EventStructure.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Event/EventStructure");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Event/EventStructure.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Event/EventStructure");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Event/FrmEventSequence.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Event/EventSequence");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Event/EventComplianceList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Event/EventComplianceList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Compliances/ComplianceDeactivate.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/ComplianceDeactivate");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Event/AssignEvents.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Event/AssignEvents");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Event/EventDetails.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Event/EventDetails");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("SecurityQuestion/AddSecurityQuestions.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/SecurityQuestion/AddSecurityQuestions");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("SecurityQuestion/ForgotPassword.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/SecurityQuestion/ForgotPassword");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("SecurityQuestion/ResetSecurityQuestions.aspx"))
            {
                //HttpContext.Current.Response.Redirect("~/SecurityQuestion/ResetSecurityQuestions");
                HttpContext.Current.Response.Redirect("~/SQ/RSQ");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Compliances/CannedReports.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/CannedReports");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Compliances/ComplianceCannedReports.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/ComplianceCannedReports");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Compliances/EventCannedReports.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/EventCannedReports");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Account/ErrorLogPage.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Account/ErrorLogPage");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Common/MassMailing.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Common/MassMailing");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/InternalCompliance/InternalCanned_Reports.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/InternalCompliance/InternalCanned_Reports");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/InternalCompliance/ICanned_Reports.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/InternalCompliance/ICanned_Reports");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/InternalCompliance/InternalCompliance_TransactionList.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/InternalCompliance/InternalCompliance_TransactionList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Account/ExportToExcel.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Account/ExportToExcel");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Account/ExcelExport_UsingFilter.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Account/ExcelExport_UsingFilter");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("/Compliances/Assign_Checklist.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/Assign_Checklist");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("/Compliances/Check_List_Reports.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/Check_List_Reports");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Compliances/Completed_NotCompleted_ChecklistReports.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/ChecklistCompletedNotCompletedReports");
            }
            //else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Compliances/Check_List_Reports_Performer.aspx"))
            //{
            //    HttpContext.Current.Response.Redirect("~/Compliances/ChecklistCompletedNotCompletedReportsPerformer");
            //}
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Compliances/EditComplianceListMaster.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/EditComplianceListMaster");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Common/CompanyStructure.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Common/CompanyStructure");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Common/LegalUpdateAdmin.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Common/LegalUpdate");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Common/ResearchPerformerAdmin.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Common/ResearchPerformer");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Common/ResearchReviewerAdmin.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Common/ResearchReviewer");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("Account/ExportCountExcel.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Account/ExportCountExcel");
            }
            #endregion

            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Compliances/CannedReportsCA.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/CannedReportsCA");
            }

            else if (HttpContext.Current.Request.Url.AbsolutePath.Contains("ComplianceDocumentListCA.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/ComplianceDocument/ComplianceDocumentListCA");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Compliances/Assign_ComplianceNew.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/Assign_ComplianceNew");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Compliances/Assign_Compliance_Activate.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/Assign_Compliance_Activate");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Compliances/Assign_CheckListNew.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/Assign_CheckListNew");
            }
            //else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Compliances/IntermediateCheckList.aspx"))
            //{
            //    HttpContext.Current.Response.Redirect("~/Compliances/IntermediateCheckList");
            //}
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Compliances/Assign_CheckList_Activate.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/Assign_CheckList_Activate");
            }           
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/ProductMapping/ProductMappingDetails.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/ProductMapping/ProductMappingDetails");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/ProductMapping/ProductMappingStructure.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/ProductMapping/MappingStructure");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/AuditTool/RiskControlMatrix.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/AuditTool/RiskControlMatrix");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/AuditTool/RiskRegister.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/AuditTool/RiskRegister");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Customers/CompanyAdminStructure.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Customers/CompanyAdminStructure");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Management/DepartmentHead_Dashboard.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Management/DepartmentHead_Dashboard");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/AuditTool/TestingStatusReport.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/AuditTool/TestingStatusReport");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/AuditTool/PendingTestingReport.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/AuditTool/PendingTestingReport");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/AuditTool/ReviewerCommentReport.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/AuditTool/ReviewerCommentReport");
            }
            //else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/AuditTool/TestingUI.aspx"))
            //{
            //    HttpContext.Current.Response.Redirect("~/RiskManagement/AuditTool/TestingUI");
            //}            
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/AuditTool/FailedControlReportReviewer.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/AuditTool/FailedControlReportReviewer");
            }
            //else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/AuditTool/FailedControlTesting.aspx"))
            //{
            //    HttpContext.Current.Response.Redirect("~/RiskManagement/AuditTool/FailedControlTesting");
            //}
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/Controls/frmToBeReviewed.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/Controls/ToBeReviewed");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Management/PersonResponsible_Dashboard.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Management/PersonResponsible_Dashboard");
            }
            //else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/AuditMainUI.aspx"))
            //{
            //    HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/AuditMainUI");
            //}
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Management/Internal_ControlDepartmentHeadDashboard.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Management/Internal_ControlDepartmentHeadDashboard");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/FrmInternalControlReviewer.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/InternalControlReviewer");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Management/Internal_ManagementDashboard.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Management/Internal_ManagementDashboard");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Users/AuditUser_List.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Users/AuditUserList");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/Customers/RiskCategory.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/Customers/RiskCategory");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/Process/Process.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/Process/Process");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/Customers/AuditorMaster.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/Customers/AuditorMaster");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/Customers/AuditorUserMaster.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/Customers/AuditorUserMaster");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/Customers/AuditorPeriodMaster.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/Customers/AuditorPeriodMaster");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/ObservationCategory.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/ObservationCategory");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/ObservationCategory.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/ObservationCategory");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/LocationMaster.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/LocationMaster");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Account/UploadAdditionalRiskCreation.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Account/UploadAdditionalRiskCreation");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/AdditionalRiskCreation.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/AdditionalRiskCreation");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/AuditKickOffNEW.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/AuditKickOff");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/AuditSchedulingNew.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/AuditScheduling");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/AuditAssignment.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/AuditAssignment");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Management/InternalControlAuditManager_Dashboard.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Management/InternalControlAuditManager_Dashboard");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/Common/AuditDashboard.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/Common/AuditDashboard");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Account/UploadRiskCreation.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Account/UploadRiskCreation");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/AuditTool/RiskAssignment.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/AuditTool/RiskAssignment");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/AuditTool/AuditUpload.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/AuditTool/AuditUpload");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/Customers/DepartmentMaster.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/Customers/DepartmentMaster");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/AssignEntitiesToAuditManager.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/AssignEntitiesToAuditManager");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/Customers/RiskAndControlRating.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/Customers/RiskAndControlRating");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/FrmObservationReportWord.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/ObservationReportWord");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/ProcessRiskRatingReport.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/ProcessRiskRatingReport");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/FrmObservationReportPowerPoint.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/ObservationReportPowerPoint");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/frmAuditFrequency.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/AuditFrequency");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/frmAuditMatrix.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/AuditMatrix");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/FrmAuditManager.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/AuditManager");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/AuditClosure.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/AuditClosure");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/AuditClosure_Closed.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/AuditClosure_Closed");
            }
            else  if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/AuditSchedulingReportNew.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/AuditSchedulingReport");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/AuditTool/FailedControlReportPerformer.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/AuditTool/FailedControlReportPerformer");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Account/UploadProcessCreation.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Account/UploadProcessCreation");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Management/InternalControl_Dashboard.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Management/InternalControl_Dashboard");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Management/Risk_ManagementDashboard.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Management/Risk_ManagementDashboard");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Management/InternalControlPersonResponsibleDashboard.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Management/InternalControlPersonResponsibleDashboard");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/InternalAuditTool/FrmPersonResponsible.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/InternalAuditTool/PersonResponsible");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Compliances/frmPerformanceSummary.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/PerformanceSummary");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/RiskManagement/Common/InternalControlDashboard.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/RiskManagement/Common/InternalControlDashboard");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Compliances/IntermediateCompliance.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/IntermediateCompliance");
            }
            else if (HttpContext.Current.Request.Url.AbsolutePath.Equals("/Compliances/ReassignReviewerToPerformer.aspx"))
            {
                HttpContext.Current.Response.Redirect("~/Compliances/ReassignToPerformer");
            }          
        }

        public void EncryptWebConfig()
        {
            try
            {
                Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
                ConfigurationSection ConectionStringsection = config.GetSection("connectionStrings");
                ConfigurationSection AppSettingSection = config.GetSection("appSettings");
                if (!(ConectionStringsection.SectionInformation.IsProtected && AppSettingSection.SectionInformation.IsProtected))
                {
                    if (!ConectionStringsection.SectionInformation.IsProtected)
                        ConectionStringsection.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
                    if (!AppSettingSection.SectionInformation.IsProtected)
                        AppSettingSection.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");

                    config.Save();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void DecryptWebConfig()
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath);
            ConfigurationSection ConectionStringsection = config.GetSection("connectionStrings");
            ConfigurationSection AppSettingSection = config.GetSection("appSettings");

            if ((ConectionStringsection.SectionInformation.IsProtected && AppSettingSection.SectionInformation.IsProtected))
            {
                if (ConectionStringsection.SectionInformation.IsProtected)
                    ConectionStringsection.SectionInformation.UnprotectSection();
                if (AppSettingSection.SectionInformation.IsProtected)
                    AppSettingSection.SectionInformation.UnprotectSection();

                config.Save();
            }
        }
        
        /// <summary>
        /// Register a cache entry which expires in 1 minute and gives us a callback.
        /// </summary>
        /// <returns></returns>
        private void RegisterCacheEntry()
        {
            // Prevent duplicate key addition
            if (null != HttpContext.Current.Cache[DummyCacheItemKey]) return;

            HttpContext.Current.Cache.Add(DummyCacheItemKey, "Test", null, DateTime.MaxValue,
                TimeSpan.FromHours(1), CacheItemPriority.NotRemovable,
                new CacheItemRemovedCallback(CacheItemRemovedCallback));
        }


        /// <summary>
        /// Callback method which gets invoked whenever the cache entry expires.
        /// We can do our "service" works here.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="reason"></param>
        public void CacheItemRemovedCallback(
            string key,
            object value,
            CacheItemRemovedReason reason
            )
        {

            // Do the service works
            DoWork();

            // We need to register another cache item which will expire again in one
            // minute. However, as this callback occurs without any HttpContext, we do not
            // have access to HttpContext and thus cannot access the Cache object. The
            // only way we can access HttpContext is when a request is being processed which
            // means a webpage is hit. So, we need to simulate a web page hit and then 
            // add the cache item.
            HitPage();
        }

        /// <summary>
        /// Hits a local webpage in order to add another expiring item in cache
        /// </summary>
        private void HitPage()
        {
            WebClient client = new WebClient();
            client.DownloadData(DummyPageUrl);
        }

        /// <summary>
        /// Asynchronously do the 'service' works
        /// </summary>
        private void DoWork()
        {
            //ProcessNotificationsAsync();
        }
    }
}