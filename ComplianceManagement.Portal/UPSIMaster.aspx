﻿<%@ Page Title="" Language="C#" MasterPageFile="~/InsiderTrade.Master" AutoEventWireup="true" CodeBehind="UPSIMaster.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.UPSIMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

   
    <script type="text/javascript" src="Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="Newjs/jszip.min.js"></script>

    <style type="text/css">

    .k-grid-search {
        float:left !important;
    }
    .k-grid{
        padding:0;
    }
    .customer-photo {
        display: inline-block;
        width: 32px;
        height: 32px;
        border-radius: 50%;
        background-size: 32px 35px;
        background-position: center center;
        vertical-align: middle;
        line-height: 32px;
        box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0,0,0,.2);
        margin-left: 5px;
        cursor:pointer;
    }

    .customer-name {
        display: inline-block;
        vertical-align: middle;
        line-height: 32px;
        padding-left: 3px;
    }
</style>
    <style>
        body{
        color:#666;
        font-weight:300;
    }
        .form-group.required .control-label:after { 
   content:"*";
   color:red;
}
        html .k-grid tr:hover {
    background: #E4F7FB;
}
         .k-state-focused.k-state-selected{
              background: transparent;
              color:#666;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       
    <div class="col-sm-12">
                     <div class="col-sm-12" style="padding:0">
                <div class="toolbar">  
                 <button  style="float:right;"  id="button" type="button" class="btn btn-primary"  onclick="OpenAdvanceSearch(event)" >Add New</button> 
                </div>
                 
                        
            </div>
        <div class="col-sm-12" style="margin-bottom:4%;" >
            
        </div>

    <div class="col-sm-12" id="grid"></div>
  </div>    
        <div id="divAdvanceSearchModel" style="padding-top: 5px;z-index: 999;display:none;">

                <div class="row">
                    <div class="col-md-10" style="padding-left: 0px; padding-bottom: 4px;display:none;">
                        <button id="primaryTextButton1" onclick="ChangeView()">Grid View</button>
                        <button id="primaryTextButton" onclick="ChangeListView()">List View</button>                      
                    </div>
                </div>
                <div  class="col-sm-12" style="padding:0">
                       <form  class="form-inline" method="POST" id="fcontact"  >
                  
            <div class="col-sm-12" style="padding:0;padding-top:10px;">
            <div class="col-sm-8" style="padding:0;margin-top:5px;">
                 <label class="col-sm-3 control-label" style="font-weight:450;padding-top: 5px;" for="firstName">UPSI Name:</label>
                <span class="col-sm-8 k-widget k-textbox" style="width: 50%;"><input id="name" style="width: 100%;" data-role="textbox" aria-disabled="false" class="k-input" placeholder="Name" autocomplete="off" ></span>
             <!--   <input type="text" class="form-control" id="name1" value="<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>">-->
            </div>
                <div class="col-sm-8" style="display:flex;padding-top:20px;"><p>Note: Once the UPSI is created, it can't be deleted.</p>
                    <button id="saveBtn" style="margin-top:30px;" type="submit" onclick="saveDetails(event)" class="btn btn-primary">Save</button>
                </div>
            </div>
            
            
           
           
             <div style="text-align:center">
            
             </div>                      
                       </form>
                      </div>
    
    </div>
    <input type="hidden" id="editFlag" value="0" />
        <script>

            $(document).ready(function () {
                $("#pagetype").text("UPSI Master");
                $("#grid").kendoGrid({

                    dataSource: {
                        transport: {
                            read: '<%=ConfigurationManager.AppSettings["insiderapi"]%>getupsitable/?customer_id=' +<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>
                        },
                        schema: {
                            data: function (response) {
                                return response.data;
                            },
                            total: function (response) {
                                return response.total;
                            },
                            model: {
                                fields: {
                                    UPSIName: { type: "string" },
                                
                                }
                            }
                        },
                        pageSize: 10
                    },
                    sortable: {
                        mode: "single",
                        allowUnsort: false
                    },
                    //height: 550,
                    //toolbar: ["search"],
                    pageable: {
                        refresh: true,
                        pageSizes: true,
                        buttonCount: 5
                    },
                    columns: [{

                        field: "UPSIName",
                        title: " Name",
                        width: 150

                    }
                    /*, {
                        template: "<div class='customer-photo'" +
                            "style='background-image: url(../../Images/edit_icon.png);'" + " onClick='OpenAdvanceSearch(#= JSON.stringify(data) #)' >",
                        field: "",
                        title: "Action",
                        width: 150
                    }*/]
                });
            });
    </script>
<script>

    function OpenAdvanceSearch(dd) {
        //alert(dd.UPSIName);
        $("#name").val(dd.UPSIName);
        if (dd.UPSIName != undefined) {
            $('#editFlag').val(dd.ID);
        } else {
            $('#editFlag').val(0);
        }
        var myWindowAdv = $("#divAdvanceSearchModel");

        function onClose() {

        }

        myWindowAdv.kendoWindow({
            width: "45%",
            height: "25%",
            title: "Add New UPSI",
            visible: false,
            actions: [
                //"Pin",
                //"Minimize",
                "Maximize",
                "Close"
            ],
           
            close: onClose
        });
        $("#divAdvanceSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

        myWindowAdv.data("kendoWindow").center().open();
        //e.preventDefault();
        return false;
    }

    function OpenAdvanceSearchFilter(e) {
        $('#divAdvanceSearchFilterModel').modal('show');
        e.preventDefault();
        return false;
    }
        </script>
    <script>
        function saveDetails(e) {
            e.preventDefault();
            //confirm("Are you sure want to add details?");
            var editFlag = $("#editFlag").val();
            var user_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>;
            var customer_id = <%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>;
            var fname = $("#name").val();
            var created_by = "<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User %>";
            $.ajax({
                type: 'post',
                url: '<%=ConfigurationManager.AppSettings["insiderapi"]%>createupsi/',
                data: { "user_id": user_id, "customer_id": customer_id, "fname": fname, "created_by": created_by,"editflag": editFlag },
                success: function (result) {
                    if (editFlag == 0) {
                        alert("UPSI Successfully Created");

                    } else {
                        alert("UPSI Successfully Edited");

                    }
                    location.reload();
                },
                error: function (e, t) { alert('something went wrong'); }
            });

            return false;
        }
        </script>

</asp:Content>
