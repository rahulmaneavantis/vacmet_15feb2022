﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Web.Security;
using System.Text;
using System.Text.RegularExpressions;
using StackExchange.Redis;
using Newtonsoft.Json;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{

    public partial class ManagementDashboardNew : System.Web.UI.Page
    {
        //protected static DateTime FromFinancialYearSummery;
        //protected static DateTime ToFinancialYearSummery;
        //protected string Performername;
        //protected string Reviewername;
        //protected string InternalPerformername;
        //public static string CalendarDate;
        //public static DateTime CalendarTodayOrNextDate;
        //public static string date = "";
        //protected static string CalenderDateString;
        //protected static string perFunctionChart;
        //protected static string perFunctionPieChart;
        //protected static string perFunctionPieNotcompleted;
        //protected static string perPenaltyStatusPieChart;
        //protected static string perRiskChart;
        //protected static string perdeptChartzomato;
        //protected static string perdeptChartCatzomato;
        //protected static string perImprisonmentPenaltyChart;
        //protected static string perFunctionHIGHPenalty;
        //protected static string perFunctionMEDIUMPenalty;
        //protected static string perFunctionLOWPenalty;
        //protected static string perFunctionCRITICALPenalty;
        //protected static int customerID;
        //protected static int BID;
        //protected static int graphshowhide;        
        //protected static string IsSatutoryInternal;
        //protected static bool IsApprover = false;
        //public static List<long> Branchlist = new List<long>();
        //public static List<long> BranchlistGrading = new List<long>();
        //bool IsPenaltyVisible = true;              
        //protected List<Int32> roles;        
        //protected static string perFunctionChartDEPT;
        //bool IsNotCompiled = false;
        //protected static string UserInformation;
        //protected static string ProcessWiseObservationStatusChart;
        //public string Penaltytextchange;
        //protected static string customeridstring;
        protected static bool customizedChecklist;
        protected int customizedid;
        protected  DateTime FromFinancialYearSummery;
        protected  DateTime ToFinancialYearSummery;
        protected string Performername;
        protected string Reviewername;
        protected string InternalPerformername;
        public  string CalendarDate;
        public  DateTime CalendarTodayOrNextDate;
        public  string date = "";
        protected string CalenderDateString;
        protected static string perFunctionChart;
        protected static string perFunctionPieChart;
        protected static string perFunctionPieNotcompleted;
        protected static string perPenaltyStatusPieChart;
        protected static string perRiskChart;
        protected static string perdeptChartzomato;
        protected static string perdeptChartCatzomato;
        protected static string perImprisonmentPenaltyChart;
        protected static string perFunctionHIGHPenalty;
        protected static string perFunctionMEDIUMPenalty;
        protected static string perFunctionLOWPenalty;
        protected static string perFunctionCRITICALPenalty;
        protected int customerID;
        protected static int BID;
        protected static int graphshowhide;
        protected string IsSatutoryInternal;
        protected static bool IsApprover = false;       
        public static List<long> Branchlist = new List<long>();        
        bool IsPenaltyVisible = true;
        protected List<Int32> roles;
        protected static string perFunctionChartDEPT;
        bool IsNotCompiled = false;
        protected  string UserInformation;
        protected static string ProcessWiseObservationStatusChart;
        public static string Penaltytextchange;
        protected static string customeridstring;
        public bool DisplayGradingChange;
        public bool DisplayGraphChange;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var plainBytes = Encoding.UTF8.GetBytes(Session["Email"].ToString());
                UserInformation = Convert.ToBase64String(CryptographyHandler.Encrypt(plainBytes, CryptographyHandler.GetRijndaelManaged("avantis")));
            }
            catch
            {

            }
            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            DisplayGradingChange = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "ShowGradingchange");
            DisplayGraphChange = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "ChangeGraphcolor");

            if (!IsPostBack)
            {
                try
                {
                    BindStatus();

                    DisplayGradingChange = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "ShowGradingchange");
                    UpdatePanel1.Update();
                    Penaltytextchange = ConfigurationManager.AppSettings["IsPenaltyTextchanged"];
                    customeridstring = Convert.ToString(AuthenticationHelper.CustomerID);
                    if (HttpContext.Current.Request.IsAuthenticated)
                    {
                        if (Session["User_comp_Roles"] != null)
                        {
                            roles = Session["User_comp_Roles"] as List<int>;
                        }
                        else
                        {
                            roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                            Session["User_comp_Roles"] = roles;
                        }

                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {                            
                            if (AuthenticationHelper.Role.Equals("MGMT") || AuthenticationHelper.ComplianceProductType == 3)
                            {
                                IsApprover = false;                              
                            }
                            else
                            {
                                if (roles.Contains(6))
                                {
                                    IsApprover = true;
                                }
                                else
                                {
                                    IsApprover = false;
                                }
                            }                                                                                                              
                            if (AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.Role == "AUDT" || IsApprover == true || AuthenticationHelper.ComplianceProductType == 3)
                            {
                              
                                HiddenField home = (HiddenField)Master.FindControl("Ishome");
                                home.Value = "true";
                                BindMonth(DateTime.UtcNow.Year);
                                BindYear();
                                BindUserColors();
                               
                                fldsCalender.Visible = true;
                                if (customerID == 63)
                                {
                                    ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("0"));
                                    ddlStatus.SelectedValue = "1";
                                    fldsCalender.Visible = false;
                                }                                
                                Branchlist.Clear();                              
                                BindLocationFilter();                             
                                ddlperiod_SelectedIndexChanged(sender, e);
                                string customer = ConfigurationManager.AppSettings["PenaltynotDisplayCustID"].ToString();
                                List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                                if (PenaltyNotDisplayCustomerList.Count > 0)
                                {
                                    foreach (string PList in PenaltyNotDisplayCustomerList)
                                    {
                                        if (PList == customerID.ToString())
                                        {
                                            IsPenaltyVisible = false;
                                        }
                                    }
                                }

                              
                                customizedid = CustomerManagement.GetCustomizedCustomerid(Convert.ToInt32(AuthenticationHelper.CustomerID), "MGMT_Total_Unique");
                                if (IsPenaltyVisible == true)
                                {
                                    PenaltyCriteria.Visible = true;
                                    penaltycount.Visible = true;
                                    penaltycount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB");
                                    entitycount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB");
                                    locationcount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB");
                                    functioncount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB");
                                    compliancecount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB");
                                    usercount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB");
                                    compliancecountNew.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB");
                                }
                                else
                                {
                                    PenaltyCriteria.Visible = false;
                                    entitycount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB1");
                                    locationcount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB1");
                                    functioncount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB1");
                                    compliancecount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB1");
                                    usercount.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB1");
                                    compliancecountNew.Attributes.Add("Class", ".col-lg-3 col-md-3 col-sm-12 col-xs-12 TMB1");
                                    penaltycount.Visible = false;
                                }
                                BindLocationCount(customerID, IsPenaltyVisible, IsApprover);                              
                                BindGradingReportSummaryTree(IsApprover);
                              
                                TbxFilterLocationGridding.Text = "Select Entity/Sub-Entity/Location";
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                                
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilterPenalty", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPenalty');", tbxFilterLocationPenalty.ClientID), true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterPenalty", "$(\"#divFilterLocationPenalty\").hide(\"blind\", null, 500, function () { });", true);

                                int UnreadNotifications = Business.ComplianceManagement.GetUnreadUserNotification(AuthenticationHelper.UserID);

                                if (UnreadNotifications > 0)
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "divNotification", "openNotificationModal();", true);
                                }
                                divTabs.Visible = true;
                                ComplianceCalender.Visible = true;
                                DivOverDueCompliance.Visible = true;                                                             
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    divTabs.Visible = false;
                                    ComplianceCalender.Visible = false;
                                    DivOverDueCompliance.Visible = false;                                                                  
                                }
                                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                                string cashTimeval = string.Empty;
                                if (objlocal == "Local")
                                {
                                    cashTimeval = "MCA_CHE" + AuthenticationHelper.UserID;
                                }
                                else
                                {
                                    cashTimeval = "MCACHE" + AuthenticationHelper.UserID;
                                }
                                if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                {
                                    DateTime ss = (DateTime)StackExchangeRedisExtensions.Get(cashTimeval);
                                    TimeSpan span = DateTime.Now - ss;
                                    if (span.Hours == 0)
                                    {
                                        Label1.Text = "Last updated within the last hour";
                                    }
                                    else
                                    {
                                        Label1.Text = "Last updated " + span.Hours + " hour ago";
                                    }
                                }
                                else
                                {
                                    Label1.Text = "Last updated within the last hour";
                                }
                            }
                            else
                            {
                                //added by rahul on 12 June 2018 Url Sequrity
                                FormsAuthentication.SignOut();
                                Session.Abandon();
                                FormsAuthentication.RedirectToLoginPage();
                            }
                            User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                            ComplianceCertificate.Visible = false;
                            if (AuthenticationHelper.Role.Equals("MGMT"))
                            {
                                if (LoggedUser.IsCertificateVisible == 1)
                                {
                                    //IsMgmtCertificate = true;
                                    ComplianceCertificate.Visible = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        //added by rahul on 12 June 2018 Url Sequrity
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
                catch (Exception ex)
                {
                    var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                    string cashTimeval = string.Empty;
                    if (objlocal == "Local")
                    {
                        cashTimeval = "MCA_CHE" + AuthenticationHelper.UserID;
                    }
                    else
                    {
                        cashTimeval = "MCACHE" + AuthenticationHelper.UserID;
                    }
                    DateTime ss = (DateTime)StackExchangeRedisExtensions.Get(cashTimeval);
                    TimeSpan span = DateTime.Now - ss;
                    if (span.Hours == 0)
                    {                        
                        Label1.Text = "Last updated within the last hour";
                    }
                    else
                    {
                        Label1.Text = "Last updated " + span.Hours + " hour ago";
                    }
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }

           
        }

        protected void lnkShowCustomerWiseDashboard_Click(object sender, EventArgs e)
        {
        }
        protected void lnkSPOCWiseDashboard_Click(object sender, EventArgs e)
        {
            Response.Redirect("/RLCS/RLCS_HRMDashboardNew.aspx",true);
        }
        
        #region Common Function   

       protected string GetPerformer(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 3);
                Performername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
       
        private void BindPenaltyAmount(int customerID, List<SP_GetManagementCompliancesSummary_Result> MasterPenaltyQuarterWiseQuery)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (ddlStatus.SelectedItem.Text == "Statutory" || ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                {
                    entities.Database.CommandTimeout = 180;

                    if (Branchlist.Count > 0)
                    {
                        MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    var a = MasterPenaltyQuarterWiseQuery.Select(t => t.totalvalue ?? 0).Sum();
                    if (MasterPenaltyQuarterWiseQuery != null)
                    {
                        decimal sum = (decimal)a;// MasterPenaltyQuarterWiseQuery.Sum();
                        divPenalty.InnerText = string.Format("{0:#,###0}", Math.Round(sum, 0));
                    }                    
                }
                else
                {
                    divPenalty.InnerText = string.Format("{0:#,###0}", 0);
                }
            }
        }
        private void BindPenaltyAmountApprover(int customerID, List<SP_GetManagementCompliancesSummary_Result> MasterPenaltyQuarterWiseQuery)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (ddlStatus.SelectedItem.Text == "Statutory" || ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                {
                    entities.Database.CommandTimeout = 180;

                    if (Branchlist.Count > 0)
                    {
                        MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    var a = MasterPenaltyQuarterWiseQuery.Select(t => t.totalvalue ?? 0).Sum();
                    if (MasterPenaltyQuarterWiseQuery != null)
                    {
                        decimal sum = (decimal)a;// MasterPenaltyQuarterWiseQuery.Sum();
                        divPenalty.InnerText = string.Format("{0:#,###0}", Math.Round(sum, 0));
                    }
                }
                else
                {
                    divPenalty.InnerText = string.Format("{0:#,###0}", 0);
                }
            }
        }
        private void BindPenaltyAmount(int customerID, List<SP_GetPenaltyQuarterWise_Result> MasterPenaltyQuarterWiseQuery)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (ddlStatus.SelectedItem.Text == "Statutory" || ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                {
                    entities.Database.CommandTimeout = 180;

                    if (Branchlist.Count > 0)
                    {
                        MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    var a = MasterPenaltyQuarterWiseQuery.Select(t => t.totalvalue ?? 0).Sum();
                    if (MasterPenaltyQuarterWiseQuery != null)
                    {
                        decimal sum = (decimal)a;// MasterPenaltyQuarterWiseQuery.Sum();
                        divPenalty.InnerText = string.Format("{0:#,###0}", Math.Round(sum, 0));
                    }                 
                }
                else
                {
                    divPenalty.InnerText = string.Format("{0:#,###0}", 0);
                }
            }
        }
        private void BindPenaltyAmountApprover(int customerID, List<SP_GetPenaltyQuarterWiseApprover_Result> MasterPenaltyQuarterWiseQuery)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (ddlStatus.SelectedItem.Text == "Statutory" || ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                {
                    entities.Database.CommandTimeout = 180;

                    if (Branchlist.Count > 0)
                    {
                        MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    var a = MasterPenaltyQuarterWiseQuery.Select(t => t.totalvalue ?? 0).Sum();
                    if (MasterPenaltyQuarterWiseQuery != null)
                    {
                        decimal sum = (decimal)a;// MasterPenaltyQuarterWiseQuery.Sum();
                        divPenalty.InnerText = string.Format("{0:#,###0}", Math.Round(sum, 0));
                    }
                }
                else
                {
                    divPenalty.InnerText = string.Format("{0:#,###0}", 0);
                }
            }
        }

        public void GetWidgetDashboardNoCompliedCount(int CustomerID, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ProcessWiseObservationStatusChart = string.Empty;
                String newDivName = String.Empty;
                String ProcesswiseObservationSeries = String.Empty;
                List<WidgetMaster> ProcessList = new List<WidgetMaster>();

                ProcessList = (from C in entities.WidgetMasters
                               where C.CustomerID == CustomerID
                               select C).ToList();

                List<SP_GetWidgetCompliancesSummary_Result> master = new List<SP_GetWidgetCompliancesSummary_Result>();
                master = (from C in entities.SP_GetWidgetCompliancesSummary(AuthenticationHelper.UserID, (int)CustomerID)
                          select C).ToList();

                if (AuthenticationHelper.Role != "AUDT")
                {
                    if (FromDate == null && EndDateF == null)
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            master = master.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                }

                if (Branchlist.Count > 0)
                {
                    master = master.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                List<SP_GetWidgetCompliancesSummary_Result> transactionsQuery = new List<SP_GetWidgetCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                DateTime? PassEndValue;
                if (FromDate != null && EndDateF != null)
                {
                    transactionsQuery = (from row in master
                                         where row.CustomerID == CustomerID
                                         && row.ScheduledOn >= FromDate
                                         && row.ScheduledOn <= EndDateF
                                         select row).ToList();

                    PassEndValue = EndDateF;
                }
                else if (FromDate != null)
                {
                    transactionsQuery = (from row in master
                                         where row.CustomerID == CustomerID
                                         && row.ScheduledOn >= FromDate && row.ScheduledOn <= EndDate
                                         select row).ToList();

                    PassEndValue = EndDate;
                }
                else if (EndDateF != null)
                {
                    transactionsQuery = (from row in master
                                         where row.CustomerID == CustomerID && row.ScheduledOn <= EndDateF
                                         select row).ToList();


                    PassEndValue = EndDateF;
                }
                else
                {
                    if (approver == true)
                    {
                        transactionsQuery = (from row in master
                                             where row.CustomerID == CustomerID
                                             && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                             || row.ScheduledOn <= EndDate)
                                             select row).ToList();
                    }
                    else
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            if (AuthenticationHelper.Role == "AUDT")
                            {
                                transactionsQuery = (from row in master
                                                     where row.CustomerID == CustomerID
                                                     && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                     || row.ScheduledOn <= FIEndDate)
                                                     select row).ToList();
                            }
                            else
                            {
                                //For Financial Year
                                transactionsQuery = (from row in master
                                                     where row.CustomerID == CustomerID
                                                     && row.ScheduledOn >= FIFromDate
                                                     && row.ScheduledOn <= FIEndDate
                                                     select row).ToList();
                            }
                        }
                        else
                        {
                            transactionsQuery = (from row in master
                                                 where row.CustomerID == CustomerID
                                                 && row.ScheduledOn <= FIEndDate
                                                 select row).ToList();
                        }
                    }
                    PassEndValue = FIEndDate;
                }
                if (AuthenticationHelper.Role != "AUDT")
                {
                    if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                    {
                        PassEndValue = FIEndDate;
                        FromDate = FIFromDate;
                    }
                }
                if (approver == true)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                }
                try
                {

                    int Count = 1;
                    ProcessList.ForEach(EachProcess =>
                    {
                     
                        var fltertransactionsQuery = transactionsQuery.Where(entry => entry.WidgetID == EachProcess.ID).ToList();
                        if (fltertransactionsQuery.Count > 0)
                        {
                            long totalPieCompletedCritical = 0;
                            long totalPieCompletedHIGH = 0;
                            long totalPieCompletedMEDIUM = 0;
                            long totalPieCompletedLOW = 0;

                            long totalPieAfterDueDateCritical = 0;
                            long totalPieAfterDueDateHIGH = 0;
                            long totalPieAfterDueDateMEDIUM = 0;
                            long totalPieAfterDueDateLOW = 0;

                            long totalPieNotCompletedCritical = 0;
                            long totalPieNotCompletedHIGH = 0;
                            long totalPieNotCompletedMEDIUM = 0;
                            long totalPieNotCompletedLOW = 0;

                            long totalPieNotCompliedCritical = 0;
                            long totalPieNotCompliedHIGH = 0;
                            long totalPieNotCompliedMEDIUM = 0;
                            long totalPieNotCompliedLOW = 0;

                            System.Web.UI.HtmlControls.HtmlGenericControl dynDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                            newDivName = "DivNoCompliedProcessObs" + Count;
                            dynDiv.ID = newDivName;
                            dynDiv.Style.Add(HtmlTextWriterStyle.Height, "400px");
                            dynDiv.Style.Add(HtmlTextWriterStyle.Width, "100%");
                            if (Count != 1)
                            {
                                dynDiv.Style["margin-top"] = "50px";
                            }
                            dynDiv.Attributes.Add("class", "FirstDiv");
                            DivGraphProcessObsStatus.Controls.Add(dynDiv);


                            fltertransactionsQuery = fltertransactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                            //Critical
                            totalPieAfterDueDateCritical = fltertransactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5
                            || entry.ComplianceStatusID == 9)).Count();

                            totalPieCompletedCritical = fltertransactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

                            totalPieNotCompletedCritical = fltertransactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

                            totalPieNotCompliedCritical = fltertransactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceStatusID == 17).Count();



                            //High
                            totalPieAfterDueDateHIGH = fltertransactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

                            totalPieCompletedHIGH = fltertransactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();



                            totalPieNotCompletedHIGH = fltertransactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

                            totalPieNotCompliedHIGH = fltertransactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceStatusID == 17).Count();

                            //Medium
                            totalPieAfterDueDateMEDIUM = fltertransactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

                            totalPieCompletedMEDIUM = fltertransactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

                            totalPieNotCompletedMEDIUM = fltertransactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

                            totalPieNotCompliedMEDIUM = fltertransactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceStatusID == 17).Count();


                            //Low
                            totalPieAfterDueDateLOW = fltertransactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

                            totalPieCompletedLOW = fltertransactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

                            totalPieNotCompletedLOW = fltertransactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

                            totalPieNotCompliedLOW = fltertransactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceStatusID == 17).Count();


                            //EachProcess.WidgetName
                       
                            ProcesswiseObservationSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "'," +
                                "{ chart:{type: 'column'},title:{ align:'left',text: '" + EachProcess.WidgetName + "',style:{display: 'block'},}," +
                                "subtitle:{text: '',style:{}},xAxis:{categories: ['Critical','High', 'Medium', 'Low']},yAxis:{title:{text: 'Number of Compliances',}," +
                                "stackLabels:{enabled: true,style:{fontWeight: 'bold',color: 'gray'}}},tooltip:{" +
                                "hideDelay: 0,backgroundColor: 'rgba(247,247,247,1)',headerFormat: '<b>{point.x}</b><br/>',pointFormat: '{series.name}: {point.y}'}," +
                                "plotOptions:{column:{stacking: 'normal',dataLabels:{enabled: true,color: 'white',style:{textShadow: false,}},cursor: 'pointer'},},";

                          
                            ProcesswiseObservationSeries += "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +

                                     // Not Completed - Critical
                                     "y: " + totalPieNotCompletedCritical + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Critical','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +

                                     "}}},{" +

                                     // Not Completed - High
                                     "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('High','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

                                     "}}},{" +

                                     // Not Completed - Medium
                                     " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Medium','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

                                     "}}},{  " +

                                     // Not Completed - Low

                                     "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Low','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +

                                     "}}}]}," +


                                    "{name: 'Not Complied',color: perRiskStackedColumnChartColorScheme.NotComplied,data: [{ " +

                                    // Not Complied - Critical

                                    "y: " + totalPieNotCompliedCritical + ",events:{click: function(e) { " +
                                    " fpopulatedwidgetdata('Critical','Not complied'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory',3)" +

                                    "}}},{   " +

                                    // Not Complied - High

                                    "y: " + totalPieNotCompliedHIGH + ",events:{click: function(e) { " +
                                     " fpopulatedwidgetdata('Critical','Not complied'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory',0)" +

                                    "}}},{   " +

                                    // Not Complied - Medium
                                    "y: " + totalPieNotCompliedMEDIUM + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Critical','Not complied'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory',1)" +

                                    "}}},{   " +
                                    // Not Complied - Low
                                    "y: " + totalPieNotCompliedLOW + ",events:{click: function(e) {" +
                                    " fpopulatedwidgetdata('Critical','Not complied'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory',2)" +

                                    "}}}]}," +


                                      "{name: 'Closed Delayed',color: perRiskStackedColumnChartColorScheme.closedelay,data: [{" +
                                     // After Due Date - Critical

                                     "y: " + totalPieAfterDueDateCritical + ",events:{click: function(e) { " +
                                     " fpopulatedwidgetdata('Critical','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +

                                     "}}},{   " +


                                     // After Due Date - High

                                     "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                                     " fpopulatedwidgetdata('High','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

                                     "}}},{   " +

                                     // After Due Date - Medium
                                     "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Medium','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

                                     "}}},{   " +
                                     // After Due Date - Low
                                     "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Low','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +


                                     "}}}]}," +
                                     "{name: 'Closed Timely',color: perRiskStackedColumnChartColorScheme.closetimely,data: [{" +


                                     // In Time - Critical
                                     "y: " + totalPieCompletedCritical + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Critical','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +

                                     "}}},{  " +


                                     // In Time - High
                                     "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('High','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

                                     "}}},{  " +


                                     // In Time - Medium
                                     "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Medium','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

                                     "}}},{  " +
                                     // In Time - Low
                                     "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                                      " fpopulatedwidgetdata('Low','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +

                                        "}}}]}]});";



                            ProcessWiseObservationStatusChart += ProcesswiseObservationSeries;

                            Count++;
                        }
                    });
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }

            }

        }

        

        public void GetWidgetDashboardCount(int CustomerID, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                ProcessWiseObservationStatusChart = string.Empty;
                String newDivName = String.Empty;
                String ProcesswiseObservationSeries = String.Empty;
                List<WidgetMaster> ProcessList = new List<WidgetMaster>();

                ProcessList = (from C in entities.WidgetMasters
                               where C.CustomerID == CustomerID
                               select C).ToList();

                List<SP_GetWidgetCompliancesSummary_Result> master = new List<SP_GetWidgetCompliancesSummary_Result>();
                master = (from C in entities.SP_GetWidgetCompliancesSummary(AuthenticationHelper.UserID, (int)CustomerID)
                          select C).ToList();

                if (AuthenticationHelper.Role != "AUDT")
                {
                    if (FromDate == null && EndDateF == null)
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            master = master.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                }

                if (Branchlist.Count > 0)
                {
                    master = master.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                }

                List<SP_GetWidgetCompliancesSummary_Result> transactionsQuery = new List<SP_GetWidgetCompliancesSummary_Result>();
                DateTime EndDate = DateTime.Today.Date;
                DateTime? PassEndValue;
                if (FromDate != null && EndDateF != null)
                {
                    transactionsQuery = (from row in master
                                         where row.CustomerID == CustomerID
                                         && row.ScheduledOn >= FromDate
                                         && row.ScheduledOn <= EndDateF
                                         select row).ToList();

                    PassEndValue = EndDateF;
                }
                else if (FromDate != null)
                {
                    transactionsQuery = (from row in master
                                         where row.CustomerID == CustomerID
                                         && row.ScheduledOn >= FromDate && row.ScheduledOn <= EndDate
                                         select row).ToList();

                    PassEndValue = EndDate;
                }
                else if (EndDateF != null)
                {
                    transactionsQuery = (from row in master
                                         where row.CustomerID == CustomerID && row.ScheduledOn <= EndDateF
                                         select row).ToList();


                    PassEndValue = EndDateF;
                }
                else
                {
                    if (approver == true)
                    {
                        transactionsQuery = (from row in master
                                             where row.CustomerID == CustomerID
                                             && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                             || row.ScheduledOn <= EndDate)
                                             select row).ToList();
                    }
                    else
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            if (AuthenticationHelper.Role == "AUDT")
                            {
                                transactionsQuery = (from row in master
                                                     where row.CustomerID == CustomerID
                                                     && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                     || row.ScheduledOn <= FIEndDate)
                                                     select row).ToList();
                            }
                            else
                            {
                                //For Financial Year
                                transactionsQuery = (from row in master
                                                     where row.CustomerID == CustomerID
                                                     && row.ScheduledOn >= FIFromDate
                                                     && row.ScheduledOn <= FIEndDate
                                                     select row).ToList();
                            }
                        }
                        else
                        {
                            transactionsQuery = (from row in master
                                                 where row.CustomerID == CustomerID
                                                 && row.ScheduledOn <= FIEndDate
                                                 select row).ToList();
                        }
                    }
                    PassEndValue = FIEndDate;
                }
                if (AuthenticationHelper.Role != "AUDT")
                {
                    if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                    {
                        PassEndValue = FIEndDate;
                        FromDate = FIFromDate;
                    }
                }
                if (approver == true)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                }
                try
                {

                    int Count = 1;
                    ProcessList.ForEach(EachProcess =>
                    {
                        var fltertransactionsQuery = transactionsQuery.Where(entry => entry.WidgetID == EachProcess.ID).ToList();                     
                        if (fltertransactionsQuery.Count > 0)
                        {
                            long totalPieCompletedCritical = 0;
                            long totalPieCompletedHIGH = 0;
                            long totalPieCompletedMEDIUM = 0;
                            long totalPieCompletedLOW = 0;

                            long totalPieAfterDueDateCritical = 0;
                            long totalPieAfterDueDateHIGH = 0;
                            long totalPieAfterDueDateMEDIUM = 0;
                            long totalPieAfterDueDateLOW = 0;

                            long totalPieNotCompletedCritical = 0;
                            long totalPieNotCompletedHIGH = 0;
                            long totalPieNotCompletedMEDIUM = 0;
                            long totalPieNotCompletedLOW = 0;

                            long totalPieNotCompliedCritical = 0;
                            long totalPieNotCompliedHIGH = 0;
                            long totalPieNotCompliedMEDIUM = 0;
                            long totalPieNotCompliedLOW = 0;

                            System.Web.UI.HtmlControls.HtmlGenericControl dynDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                            newDivName = "DivProcessObs" + Count;
                            dynDiv.ID = newDivName;
                            dynDiv.Style.Add(HtmlTextWriterStyle.Height, "400px");
                            dynDiv.Style.Add(HtmlTextWriterStyle.Width, "100%");
                            if (Count != 1)
                            {
                                dynDiv.Style["margin-top"] = "50px";
                            }
                            dynDiv.Attributes.Add("class", "FirstDiv");
                            DivGraphProcessObsStatus.Controls.Add(dynDiv);


                            fltertransactionsQuery = fltertransactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                            //Critical
                            totalPieAfterDueDateCritical = fltertransactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5
                            || entry.ComplianceStatusID == 9)).Count();

                            totalPieCompletedCritical = fltertransactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

                            totalPieNotCompletedCritical = fltertransactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

                            totalPieNotCompliedCritical = fltertransactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceStatusID == 17).Count();



                            //High
                            totalPieAfterDueDateHIGH = fltertransactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

                            totalPieCompletedHIGH = fltertransactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

                            totalPieNotCompletedHIGH = fltertransactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

                            totalPieNotCompliedHIGH = fltertransactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceStatusID == 17).Count();

                            //Medium
                            totalPieAfterDueDateMEDIUM = fltertransactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

                            totalPieCompletedMEDIUM = fltertransactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

                            totalPieNotCompletedMEDIUM = fltertransactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

                            totalPieNotCompliedMEDIUM = fltertransactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceStatusID == 17).Count();


                            //Low
                            totalPieAfterDueDateLOW = fltertransactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();

                            totalPieCompletedLOW = fltertransactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();

                            totalPieNotCompletedLOW = fltertransactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14)).Count();

                            totalPieNotCompliedLOW = fltertransactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceStatusID == 17).Count();


                            //EachProcess.WidgetName
                          
                            ProcesswiseObservationSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "'," +
                                "{ chart:{type: 'column'},title:{ align:'left',text: '" + EachProcess.WidgetName + "',style:{display: 'block'},}," +
                                "subtitle:{text: '',style:{}},xAxis:{categories: ['Critical','High', 'Medium', 'Low']},yAxis:{title:{text: 'Number of Compliances',}," +
                                "stackLabels:{enabled: true,style:{fontWeight: 'bold',color: 'gray'}}},tooltip:{" +
                                "hideDelay: 0,backgroundColor: 'rgba(247,247,247,1)',headerFormat: '<b>{point.x}</b><br/>',pointFormat: '{series.name}: {point.y}'}," +
                                "plotOptions:{column:{stacking: 'normal',dataLabels:{enabled: true,color: 'white',style:{textShadow: false,}},cursor: 'pointer'},},";

                           
                            ProcesswiseObservationSeries += "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +

                                     // Not Completed - Critical
                                     "y: " + totalPieNotCompletedCritical + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Critical','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +                                     
                                     "}}},{" +

                                     // Not Completed - High
                                     "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('High','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

                                     "}}},{" +

                                     // Not Completed - Medium
                                     " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Medium','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

                                     "}}},{  " +

                                     // Not Completed - Low

                                     "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Low','Not completed'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +

                                     "}}}]},{name: 'Closed Delayed',color: perRiskStackedColumnChartColorScheme.closedelay,data: [{" +


                                     // After Due Date - Critical

                                     "y: " + totalPieAfterDueDateCritical + ",events:{click: function(e) { " +
                                     " fpopulatedwidgetdata('Critical','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +

                                     "}}},{   " +


                                     // After Due Date - High

                                     "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                                     " fpopulatedwidgetdata('High','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

                                     "}}},{   " +

                                     // After Due Date - Medium
                                     "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Medium','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

                                     "}}},{   " +
                                     // After Due Date - Low
                                     "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Low','After due date'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +


                                     "}}}]},{name: 'Closed Timely',color: perRiskStackedColumnChartColorScheme.closetimely,data: [{" +


                                     // In Time - Critical
                                     "y: " + totalPieCompletedCritical + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Critical','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",3)" +

                                     "}}},{  " +


                                     // In Time - High
                                     "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('High','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",0)" +

                                     "}}},{  " +


                                     // In Time - Medium
                                     "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                                     " fpopulatedwidgetdata('Medium','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",1)" +

                                     "}}},{  " +
                                     // In Time - Low
                                     "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                                      " fpopulatedwidgetdata('Low','In Time'," + CustomerID + ",'" + FromDate + "','" + PassEndValue + "','Statutory'," + EachProcess.ID + ",2)" +

                                        "}}}]}]});";

                            ProcessWiseObservationStatusChart += ProcesswiseObservationSeries;

                            Count++;
                        }
                    });
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        private void BindLocationCount(int customerID, bool IsPenaltyVisible, bool IsApp)
        {
            try
            {
                if (AuthenticationHelper.Role == "AUDT")
                {
                    try
                    {
                        txtAdvStartDate.Text = Convert.ToDateTime(Session["Auditstartdate"].ToString()).Date.ToString("dd-MM-yyyy");
                        txtAdvEndDate.Text = Convert.ToDateTime(Session["Auditenddate"].ToString()).Date.ToString("dd-MM-yyyy");
                    }
                    catch
                    {
                    }
                }
                #region Comment by rahul on 12 JULY 2021
                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                string cashTimeval = string.Empty;
                string cashstatutoryval = string.Empty;
                string cashinternalval = string.Empty;

                customizedChecklist = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "MGMT_Checklist");


                if (objlocal == "Local")
                {
                    cashTimeval = "MCA_CHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MS_PSD" + AuthenticationHelper.UserID;
                    cashinternalval = "MI_PSD" + AuthenticationHelper.UserID;
                }
                else
                {
                    cashTimeval = "MCACHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MSPSD" + AuthenticationHelper.UserID;
                    cashinternalval = "MIPSD" + AuthenticationHelper.UserID;

                }
                #endregion

                bool IsApproverInternal = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    // STT Change- Add Status
                    string customer = ConfigurationManager.AppSettings["NotCompliedCustID"].ToString();
                    List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                    if (PenaltyNotDisplayCustomerList.Count > 0)
                    {
                        foreach (string PList in PenaltyNotDisplayCustomerList)
                        {
                            if (PList == customerID.ToString())
                            {
                                IsNotCompiled = true;
                                break;
                            }
                        }
                    }

                    //Zomato Not Complited Pie Chart
                    graphshowhide = 1;
                    string customer1 = ConfigurationManager.AppSettings["NotcomplitedPieChartGraphDisplay_Zomato"].ToString();
                    List<string> NotcomplitedPiaChartGraphDisplayCustomerList = customer1.Split(',').ToList();
                    if (NotcomplitedPiaChartGraphDisplayCustomerList.Count > 0)
                    {
                        foreach (string PList in NotcomplitedPiaChartGraphDisplayCustomerList)
                        {
                            if (PList == customerID.ToString())
                            {
                                graphshowhide = 0;
                            }
                        }
                    }
                    List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    List<SP_GetManagementInternalCompliancesSummary_Result> MasterManagementInternalCompliancesSummaryQuery = new List<SP_GetManagementInternalCompliancesSummary_Result>();
                    #region Statutory
                    if (ddlStatus.SelectedItem.Text == "Statutory" || ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                    {                        
                        IsSatutoryInternal = "Statutory";
                        #region With Location
                        if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                        {                          
                            try
                            {
                                if (StackExchangeRedisExtensions.KeyExists(cashstatutoryval))
                                {
                                    try
                                    {
                                        MasterManagementCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval);
                                    }
                                    catch (Exception ex)
                                    {
                                        LogLibrary.WriteErrorLog("GET Statutory Error exception :" + cashstatutoryval);
                                        if (IsApp == true)
                                        {
                                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                        }
                                        else
                                        {
                                            if (customizedChecklist)
                                            {
                                                if (ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                                                {
                                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                                }
                                                else
                                                {
                                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                                }
                                            }
                                            else
                                            {
                                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                            }
                                        }
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                                else
                                {
                                    if (IsApp == true)
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                    }
                                    else
                                    {
                                        if (customizedChecklist)
                                        {
                                            if (ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                                            {
                                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                            }
                                            else
                                            {
                                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                            }
                                        }
                                        else
                                        {
                                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                        }
                                    }
                                    try
                                    {
                                        StackExchangeRedisExtensions.SetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval, MasterManagementCompliancesSummaryQuery);
                                        if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                        {
                                            StackExchangeRedisExtensions.Remove(cashTimeval);
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                        else
                                        {
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        LogLibrary.WriteErrorLog("SET Statutory Error exception :" + cashstatutoryval);
                                    }

                                }
                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("KeyExists BindLocationCount Statutory Error exception :" + cashstatutoryval);
                                if (IsApp == true)
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                }
                                else
                                {
                                    if (customizedChecklist)
                                    {
                                        if (ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                                        {
                                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                        }
                                        else
                                        {
                                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                        }
                                    }
                                    else
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                    }
                                }
                            }

                            if (IsPenaltyVisible == true)
                            {
                                GetManagementCompliancePenalitySummary(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), ddlFinancialYear.SelectedItem.Text, MasterManagementCompliancesSummaryQuery, IsApp);
                            }
                            if (IsNotCompiled == true)
                            {
                                GetManagementCompliancesSummaryAddedNotCompliedStatus(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, IsApp, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                                GetWidgetDashboardNoCompliedCount(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue),
                                   "T", FromFinancialYearSummery, ToFinancialYearSummery
                                   , IsApp, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                            }
                            else
                            {
                                if (graphshowhide == 0)
                                {
                                    try
                                    {
                                        GetManagementCompliancesSummaryZomato(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, IsApp, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                                    }
                                    catch (Exception ex)
                                    {
                                        LogLibrary.WriteErrorLog("GetManagementCompliancesSummaryZomato :" + ex);
                                    }
                                }
                                else
                                {
                                    GetManagementCompliancesSummary(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, IsApp, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));

                                }

                                GetImprisonmentPenaltySummary(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, ddlMinimum.SelectedItem.Text, ddlMaximum.SelectedItem.Text, ddlMinimumImprisonment.SelectedItem.Text, ddlMaximumImprisonment.SelectedItem.Text, IsApp, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                                GetWidgetDashboardCount(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue),
                                   "T", FromFinancialYearSummery, ToFinancialYearSummery
                                   , IsApp, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                            }
                            BindLocationCount2(MasterManagementCompliancesSummaryQuery, IsPenaltyVisible, IsApp);
                          
                        }
                        #endregion

                        #region Without Location
                        else
                        {

                            try
                            {

                                if (StackExchangeRedisExtensions.KeyExists(cashstatutoryval))
                                {
                                    try
                                    {

                                        MasterManagementCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval);
                                    }
                                    catch (Exception ex)
                                    {
                                        LogLibrary.WriteErrorLog("GET Statutory Error exception : " + cashstatutoryval);

                                        if (IsApp == true)
                                        {
                                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                        }
                                        else
                                        {
                                            if (customizedChecklist)
                                            {
                                                if (ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                                                {
                                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                                }
                                                else
                                                {
                                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                                }
                                            }
                                            else
                                            {
                                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                            }

                                        }
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                                else
                                {

                                    if (IsApp == true)
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                    }
                                    else
                                    {
                                        if (customizedChecklist)
                                        {
                                            if (ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                                            {
                                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                            }
                                            else
                                            {
                                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();                                                
                                            }
                                        }
                                        else
                                        {
                                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                        }
                                    }

                                    try
                                    {
                                        StackExchangeRedisExtensions.SetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval, MasterManagementCompliancesSummaryQuery);
                                        if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                        {
                                            StackExchangeRedisExtensions.Remove(cashTimeval);
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                        else
                                        {
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        StackExchangeRedisExtensions.SetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval, MasterManagementCompliancesSummaryQuery);
                                        if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                        {
                                            StackExchangeRedisExtensions.Remove(cashTimeval);
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                        else
                                        {
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                        LogLibrary.WriteErrorLog("SET Statutory Error exception :" + cashstatutoryval);
                                    }

                                }


                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("KeyExists BindLocationCount Statutory Error exception :" + cashstatutoryval);
                                if (IsApp == true)
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                }
                                else
                                {
                                    if (customizedChecklist)
                                    {
                                        if (ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                                        {
                                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                        }
                                        else
                                        {
                                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                        }
                                    }
                                    else
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                    }
                                }
                            }
                            if (IsPenaltyVisible == true)
                            {
                                GetManagementCompliancePenalitySummary(customerID, Branchlist, -1, ddlFinancialYear.SelectedItem.Text, MasterManagementCompliancesSummaryQuery, IsApp);
                            }
                            if (IsNotCompiled == true)
                            {
                                GetManagementCompliancesSummaryAddedNotCompliedStatus(customerID, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, IsApp, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                                GetWidgetDashboardNoCompliedCount(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue),
                                  "T", FromFinancialYearSummery, ToFinancialYearSummery
                                  , IsApp, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                            }
                            else
                            {
                                if (graphshowhide == 0)
                                {
                                    try
                                    {
                                        GetManagementCompliancesSummaryZomato(customerID, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, IsApp, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                                    }
                                    catch (Exception ex)
                                    {
                                        LogLibrary.WriteErrorLog("GetManagementCompliancesSummaryZomato :" + ex);
                                    }
                                }
                                else
                                {
                                    GetManagementCompliancesSummary(customerID, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, IsApp, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                                }
                                GetImprisonmentPenaltySummary(customerID, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, ddlMinimum.SelectedItem.Text, ddlMaximum.SelectedItem.Text, ddlMinimumImprisonment.SelectedItem.Text, ddlMaximumImprisonment.SelectedItem.Text, IsApp, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                                GetWidgetDashboardCount(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue),
                                   "T", FromFinancialYearSummery, ToFinancialYearSummery
                                   , IsApp, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                            }
                            BindLocationCount2(MasterManagementCompliancesSummaryQuery, IsPenaltyVisible, IsApp);
                        
                        }
                        #endregion
                        
                    }
                    #endregion
                    #region Internal
                    else
                    {
                        
                        divPenalty.InnerText = string.Format("{0:#,###0}", 0);
                        IsSatutoryInternal = "Internal";

                        if (AuthenticationHelper.Role.Equals("MGMT") || AuthenticationHelper.ComplianceProductType == 3)
                        {
                            IsApproverInternal = false;
                        }
                        else
                        {
                            var GetApproverInternal = (from row in entities.InternalComplianceAssignments
                                                       where row.RoleID == 6
                                                       && row.UserID == AuthenticationHelper.UserID
                                                       select row).ToList();
                            if (GetApproverInternal.Count > 0)
                            {
                                IsApprover = true;
                                IsApproverInternal = true;
                            }
                            else
                            {
                                IsApprover = false;
                                IsApproverInternal = false;
                            }
                        }
                        if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                        {
                            try
                            {
                                if (StackExchangeRedisExtensions.KeyExists(cashinternalval))
                                {
                                    try
                                    {
                                        MasterManagementInternalCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval);
                                    }
                                    catch (Exception ex)
                                    {
                                        LogLibrary.WriteErrorLog("GET Internal Error exception :" + cashinternalval);
                                        if (IsApproverInternal == true)
                                        {
                                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                        }
                                        else
                                        {
                                            //if (customizedChecklist)
                                            //{
                                            //    MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                            //}
                                            //else
                                            //{
                                                MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                            //}
                                        }
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                                else
                                {
                                    if (IsApproverInternal == true)
                                    {
                                        MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                    }
                                    else
                                    {
                                        //if (customizedChecklist)
                                        //{                                            
                                        //    MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                        //}
                                        //else
                                        //{
                                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                        //}
                                    }
                                    try
                                    {
                                        StackExchangeRedisExtensions.SetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval, MasterManagementInternalCompliancesSummaryQuery);
                                        if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                        {
                                            StackExchangeRedisExtensions.Remove(cashTimeval);
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        LogLibrary.WriteErrorLog("SET Internal Error exception :" + cashinternalval);
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("KeyExists BindLocationCount Internal Error exception :" + cashinternalval);
                                if (IsApproverInternal == true)
                                {
                                    MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                }
                                else
                                {
                                    //if (customizedChecklist)
                                    //{
                                    //    MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                    //}
                                    //else
                                    //{
                                        MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                    //}
                                }
                            }
                            if (IsNotCompiled == true)
                            {
                                GetManagementInternalCompliancesSummaryAddedNotCompliedStatus(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementInternalCompliancesSummaryQuery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApproverInternal);
                            }
                            else
                            {
                                if (graphshowhide == 0)
                                {
                                    try
                                    {
                                        GetManagementInternalCompliancesSummaryZomato(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementInternalCompliancesSummaryQuery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApproverInternal);
                                    }
                                    catch (Exception ex)
                                    {
                                        LogLibrary.WriteErrorLog("GetManagementInternalCompliancesSummaryZomato :" + ex);
                                    }
                                }
                                else
                                {
                                    GetManagementInternalCompliancesSummary(customerID, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementInternalCompliancesSummaryQuery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApproverInternal);
                                }
                            }
                            BindLocationCount2(MasterManagementCompliancesSummaryQuery, IsPenaltyVisible, IsApp);
                        }
                        else
                        {
                            try
                            {
                                if (StackExchangeRedisExtensions.KeyExists(cashinternalval))
                                {
                                    try
                                    {
                                        MasterManagementInternalCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval);
                                    }
                                    catch (Exception ex)
                                    {
                                        LogLibrary.WriteErrorLog("GET Internal Error exception :" + cashinternalval);
                                        if (IsApproverInternal == true)
                                        {
                                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                        }
                                        else
                                        {
                                            //if (customizedChecklist)
                                            //{
                                            //    MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                            //}
                                            //else
                                            //{
                                                MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                            //}
                                        }
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                }
                                else
                                {
                                    if (IsApproverInternal == true)
                                    {
                                        MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                    }
                                    else
                                    {
                                        //if (customizedChecklist)
                                        //{
                                        //    MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                        //}
                                        //else
                                        //{
                                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                        //}
                                    }
                                    try
                                    {
                                        StackExchangeRedisExtensions.SetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval, MasterManagementInternalCompliancesSummaryQuery);
                                        if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                        {
                                            StackExchangeRedisExtensions.Remove(cashTimeval);
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        LogLibrary.WriteErrorLog("SET Internal Error exception :" + cashinternalval);
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("KeyExists BindLocationCount Internal Error exception :" + cashinternalval);
                                if (IsApproverInternal == true)
                                {
                                    MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                }
                                else
                                {
                                    //if (customizedChecklist)
                                    //{
                                    //    MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                    //}
                                    //else
                                    //{
                                        MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                    //}
                                }
                            }
                            if (IsNotCompiled == true)
                            {
                                GetManagementInternalCompliancesSummaryAddedNotCompliedStatus(customerID, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementInternalCompliancesSummaryQuery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApproverInternal);
                            }
                            else
                            {
                                if (graphshowhide == 0)
                                {
                                    try
                                    {
                                        GetManagementInternalCompliancesSummaryZomato(customerID, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementInternalCompliancesSummaryQuery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApproverInternal);
                                    }
                                    catch (Exception ex)
                                    {
                                        LogLibrary.WriteErrorLog("GetManagementInternalCompliancesSummaryZomato :" + ex);
                                    }
                                }
                                else
                                {
                                    GetManagementInternalCompliancesSummary(customerID, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementInternalCompliancesSummaryQuery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApproverInternal);
                                }
                            }
                            BindLocationCount2(MasterManagementCompliancesSummaryQuery, IsPenaltyVisible, IsApp);
                        }
                     
                    }
                    #endregion

                    #region Top Count
                    if (ddlStatus.SelectedItem.Text == "Internal")
                    {
                        long internalcategorycount = 0;
                        long internalcompliancecount = 0;
                        long internalTotalcompliancecount = 0;
                        long internallocationcount = 0;
                        long internalentitycount = 0;
                        long internalucount = 0;

                        #region Internal                       
                        if (IsApproverInternal)
                        {
                            #region IsApprover  

                            var countdetails = (from row in entities.PreFiled_MGMT_APPR
                                                where row.UserID == AuthenticationHelper.UserID
                                                && row.IsStatutory == "I"
                                                && row.userRole == "APPR"
                                                select row).FirstOrDefault();
                            if (countdetails == null)
                            {
                                entities.Database.CommandTimeout = 180;
                                var InternalApproverCompliancedetails = (entities.SP_InternalManagementDashboardApproverComplianceCount(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                                InternalApproverCompliancedetails = (from row in InternalApproverCompliancedetails
                                                                     select row).Distinct().ToList();

                                if (InternalApproverCompliancedetails.Count > 0)
                                {
                                    if (Branchlist.Count > 0)
                                    {
                                        InternalApproverCompliancedetails = InternalApproverCompliancedetails.Where(entry => Branchlist.Contains((long)entry.CustomerBranchID)).ToList();
                                        internalcategorycount = InternalApproverCompliancedetails.Select(entry => entry.IComplianceCategoryID).Distinct().ToList().Count();
                                        internalcompliancecount = InternalApproverCompliancedetails.Select(entry => entry.InternalComplianceID).Distinct().ToList().Count();
                                        internallocationcount = InternalApproverCompliancedetails.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                        internalTotalcompliancecount = InternalApproverCompliancedetails.Distinct().ToList().Count();
                                    }
                                    else
                                    {
                                        internalcategorycount = InternalApproverCompliancedetails.Select(entry => entry.IComplianceCategoryID).Distinct().ToList().Count();
                                        internalcompliancecount = InternalApproverCompliancedetails.Select(entry => entry.InternalComplianceID).Distinct().ToList().Count();
                                        internallocationcount = InternalApproverCompliancedetails.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                        internalTotalcompliancecount = InternalApproverCompliancedetails.Distinct().ToList().Count();
                                    }
                                    entities.Database.CommandTimeout = 180;
                                    internalentitycount = (entities.sp_EntitycountApprover((int)AuthenticationHelper.UserID, "INT")).ToList().Count();
                                    entities.Database.CommandTimeout = 180;
                                    internalucount = (from row in entities.Users
                                                      where row.CustomerID == AuthenticationHelper.CustomerID
                                                      && row.IsActive == true && row.IsDeleted == false
                                                      select row.ID).ToList().Count();

                                    divCompliancesCount.InnerText = Convert.ToString(internalcompliancecount);
                                    divUniqueCompliancesCount.InnerText = Convert.ToString(internalcompliancecount);
                                    divTotalCompliancesCount.InnerText = Convert.ToString(internalTotalcompliancecount);
                                    divFunctionCount.InnerText = Convert.ToString(internalcategorycount);
                                    divEntitesCount.InnerText = Convert.ToString(internalentitycount);
                                    divLocationCount.InnerText = Convert.ToString(internallocationcount);
                                    divUsersCount.InnerText = Convert.ToString(internalucount);
                                }
                                else
                                {
                                    divUniqueCompliancesCount.InnerText = "0";
                                    divTotalCompliancesCount.InnerText = "0";
                                    divCompliancesCount.InnerText = "0";
                                    divFunctionCount.InnerText = "0";
                                    divEntitesCount.InnerText = "0";
                                    divLocationCount.InnerText = "0";
                                    divUsersCount.InnerText = "0";
                                }
                            }
                            else
                            {
                                divCompliancesCount.InnerText = Convert.ToString(countdetails.ComplianceCount);
                                divUniqueCompliancesCount.InnerText = Convert.ToString(countdetails.ComplianceCount);
                                divFunctionCount.InnerText = Convert.ToString(countdetails.CategoryCount);
                                divEntitesCount.InnerText = Convert.ToString(countdetails.EntityCount);
                                divLocationCount.InnerText = Convert.ToString(countdetails.LocationCount);
                                divUsersCount.InnerText = Convert.ToString(countdetails.UserCount);
                                try
                                {
                                    customizedid = CustomerManagement.GetCustomizedCustomerid(Convert.ToInt32(AuthenticationHelper.CustomerID), "MGMT_Total_Unique");
                                    if (AuthenticationHelper.CustomerID == customizedid)
                                    {
                                        var ACdetails = (entities.SP_InternalManagementDashboardManagerComplianceCount(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                                        divTotalCompliancesCount.InnerText = Convert.ToString(ACdetails.Count);
                                    }
                                }
                                catch (Exception)
                                {
                                    throw;
                                }

                            }
                            #endregion
                        }
                        else
                        {
                            #region Management                            
                            entities.Database.CommandTimeout = 180;
                            var countdetails = (from row in entities.PreFiled_MGMT_APPR
                                                where row.UserID == AuthenticationHelper.UserID
                                                && row.IsStatutory == "I"
                                                && row.userRole == "MGMT"
                                                select row).FirstOrDefault();
                            if (countdetails == null)
                            {
                                List<SP_InternalManagementDashboardManagerComplianceCount_Result> ManagerInternalComplianceCount = new List<SP_InternalManagementDashboardManagerComplianceCount_Result>();
                                entities.Database.CommandTimeout = 180;
                                var ManagerInternalCompliancedetails = (entities.SP_InternalManagementDashboardManagerComplianceCount(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                                ManagerInternalComplianceCount = (from row in ManagerInternalCompliancedetails
                                                                  select row).Distinct().ToList();
                                if (ManagerInternalComplianceCount.Count > 0)
                                {
                                    if (Branchlist.Count > 0)
                                    {
                                        ManagerInternalComplianceCount = ManagerInternalComplianceCount.Where(entry => Branchlist.Contains((long)entry.CustomerBranchID)).ToList();
                                        internalcategorycount = ManagerInternalComplianceCount.Select(entry => entry.IComplianceCategoryID).Distinct().ToList().Count();
                                        internalcompliancecount = ManagerInternalComplianceCount.Select(entry => entry.InternalComplianceID).Distinct().ToList().Count();
                                        internallocationcount = ManagerInternalComplianceCount.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                        internalTotalcompliancecount = ManagerInternalComplianceCount.Distinct().ToList().Count();
                                    }
                                    else
                                    {
                                        internalcategorycount = ManagerInternalComplianceCount.Select(entry => entry.IComplianceCategoryID).Distinct().ToList().Count();
                                        internalcompliancecount = ManagerInternalComplianceCount.Select(entry => entry.InternalComplianceID).Distinct().ToList().Count();
                                        internallocationcount = ManagerInternalComplianceCount.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                        internalTotalcompliancecount = ManagerInternalComplianceCount.Distinct().ToList().Count();
                                    }
                                    entities.Database.CommandTimeout = 180;
                                    internalentitycount = (entities.sp_Entitycount((int)AuthenticationHelper.UserID, "INT")).ToList().Count();
                                    entities.Database.CommandTimeout = 180;
                                    internalucount = (from row in entities.Users
                                                      where row.CustomerID == AuthenticationHelper.CustomerID
                                                      && row.IsActive == true && row.IsDeleted == false
                                                      select row.ID).ToList().Count();

                                    divCompliancesCount.InnerText = Convert.ToString(internalcompliancecount);
                                    divUniqueCompliancesCount.InnerText = Convert.ToString(internalcompliancecount);
                                    divTotalCompliancesCount.InnerText = Convert.ToString(internalTotalcompliancecount);
                                    divFunctionCount.InnerText = Convert.ToString(internalcategorycount);
                                    divEntitesCount.InnerText = Convert.ToString(internalentitycount);
                                    divLocationCount.InnerText = Convert.ToString(internallocationcount);
                                    divUsersCount.InnerText = Convert.ToString(internalucount);
                                }
                                else
                                {
                                    divCompliancesCount.InnerText = "0";
                                    divUniqueCompliancesCount.InnerText = "0";
                                    divTotalCompliancesCount.InnerText = "0";
                                    divFunctionCount.InnerText = "0";
                                    divEntitesCount.InnerText = "0";
                                    divLocationCount.InnerText = "0";
                                    divUsersCount.InnerText = "0";
                                }
                            }
                            else
                            {
                                divCompliancesCount.InnerText = Convert.ToString(countdetails.ComplianceCount);
                                divUniqueCompliancesCount.InnerText = Convert.ToString(countdetails.ComplianceCount);
                                divFunctionCount.InnerText = Convert.ToString(countdetails.CategoryCount);
                                divEntitesCount.InnerText = Convert.ToString(countdetails.EntityCount);
                                divLocationCount.InnerText = Convert.ToString(countdetails.LocationCount);
                                divUsersCount.InnerText = Convert.ToString(countdetails.UserCount);
                                try
                                {
                                    customizedid = CustomerManagement.GetCustomizedCustomerid(Convert.ToInt32(AuthenticationHelper.CustomerID), "MGMT_Total_Unique");
                                    if (AuthenticationHelper.CustomerID == customizedid)
                                    {
                                        var ACdetails = (entities.SP_InternalManagementDashboardManagerComplianceCount(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                                        divTotalCompliancesCount.InnerText = Convert.ToString(ACdetails.Count);
                                    }
                                }
                                catch (Exception)
                                {
                                    throw;
                                }

                            }
                            #endregion
                        }
                        #endregion
                    }
                    else
                    {
                        long statutorycategorycount = 0;
                        long statutorycompliancecount = 0;
                        long statutoryTotalcompliancecount = 0;
                        long statutorylocationcount = 0;
                        long statutoryentitycount = 0;
                        long statutoryucount = 0;
                        #region Statutory
                        if (IsApp == true)
                        {
                            #region IsApprover
                            entities.Database.CommandTimeout = 180;
                            var countdetails = (from row in entities.PreFiled_MGMT_APPR
                                                where row.UserID == AuthenticationHelper.UserID
                                                && row.IsStatutory == "S"
                                                && row.userRole == "APPR"
                                                select row).FirstOrDefault();
                            if (countdetails == null)
                            {
                                List<SP_ManagementDashboardApproverComplianceCount_Result> ApproverComplianceCount = new List<SP_ManagementDashboardApproverComplianceCount_Result>();
                                var ApproverCompliancedetails = (entities.SP_ManagementDashboardApproverComplianceCount(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();

                                ApproverCompliancedetails = (from row in ApproverCompliancedetails
                                                             select row).Distinct().ToList();
                                if (ApproverCompliancedetails.Count > 0)
                                {
                                    if (Branchlist.Count > 0)
                                    {
                                        ApproverCompliancedetails = ApproverCompliancedetails.Where(entry => Branchlist.Contains((long)entry.CustomerBranchID)).ToList();
                                        statutorycategorycount = ApproverCompliancedetails.Select(entry => entry.ComplianceCategoryId).Distinct().ToList().Count();
                                        statutorycompliancecount = ApproverCompliancedetails.Select(entry => entry.ComplianceID).Distinct().ToList().Count();
                                        statutorylocationcount = ApproverCompliancedetails.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                        statutoryTotalcompliancecount = ApproverCompliancedetails.Distinct().ToList().Count();
                                    }
                                    else
                                    {
                                        statutorycategorycount = ApproverCompliancedetails.Select(entry => entry.ComplianceCategoryId).Distinct().ToList().Count();
                                        statutorycompliancecount = ApproverCompliancedetails.Select(entry => entry.ComplianceID).Distinct().ToList().Count();
                                        statutorylocationcount = ApproverCompliancedetails.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                        statutoryTotalcompliancecount = ApproverCompliancedetails.Distinct().ToList().Count();
                                    }
                                    entities.Database.CommandTimeout = 180;
                                    statutoryentitycount = (entities.sp_EntitycountApprover((int)AuthenticationHelper.UserID, "SAT")).ToList().Count();
                                    entities.Database.CommandTimeout = 180;
                                    statutoryucount = (from row in entities.Users
                                                       where row.CustomerID == AuthenticationHelper.CustomerID
                                                       && row.IsActive == true && row.IsDeleted == false
                                                       select row.ID).ToList().Count();

                                    divCompliancesCount.InnerText = Convert.ToString(statutorycompliancecount);
                                    divUniqueCompliancesCount.InnerText = Convert.ToString(statutorycompliancecount);
                                    divTotalCompliancesCount.InnerText = Convert.ToString(statutoryTotalcompliancecount);
                                    divFunctionCount.InnerText = Convert.ToString(statutorycategorycount);
                                    divEntitesCount.InnerText = Convert.ToString(statutoryentitycount);
                                    divLocationCount.InnerText = Convert.ToString(statutorylocationcount);
                                    divUsersCount.InnerText = Convert.ToString(statutoryucount);
                                }
                                else
                                {
                                    divCompliancesCount.InnerText = "0";
                                    divUniqueCompliancesCount.InnerText = "0";
                                    divTotalCompliancesCount.InnerText = "0";
                                    divFunctionCount.InnerText = "0";
                                    divEntitesCount.InnerText = "0";
                                    divLocationCount.InnerText = "0";
                                    divUsersCount.InnerText = "0";
                                }

                            }
                            else
                            {
                                divCompliancesCount.InnerText = Convert.ToString(countdetails.ComplianceCount);
                                divUniqueCompliancesCount.InnerText = Convert.ToString(countdetails.ComplianceCount);
                                divFunctionCount.InnerText = Convert.ToString(countdetails.CategoryCount);
                                divEntitesCount.InnerText = Convert.ToString(countdetails.EntityCount);
                                divLocationCount.InnerText = Convert.ToString(countdetails.LocationCount);
                                divUsersCount.InnerText = Convert.ToString(countdetails.UserCount);

                                try
                                {
                                    customizedid = CustomerManagement.GetCustomizedCustomerid(Convert.ToInt32(AuthenticationHelper.CustomerID), "MGMT_Total_Unique");
                                    if (AuthenticationHelper.CustomerID == customizedid)
                                    {
                                        var ACdetails = (entities.SP_ManagementDashboardApproverComplianceCount(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                                        divTotalCompliancesCount.InnerText = Convert.ToString(ACdetails.Count);
                                    }
                                }
                                catch (Exception)
                                {
                                    throw;
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Management
                            entities.Database.CommandTimeout = 180;
                            var countdetails = (from row in entities.PreFiled_MGMT_APPR
                                                where row.UserID == AuthenticationHelper.UserID
                                                && row.IsStatutory == "S"
                                                && row.userRole == "MGMT"
                                                select row).FirstOrDefault();
                            if (countdetails == null)
                            {
                                List<SP_ManagementDashboardManagerComplianceCount_Result> ManagerComplianceCount = new List<SP_ManagementDashboardManagerComplianceCount_Result>();
                                entities.Database.CommandTimeout = 180;
                                var ManagerCompliancedetails = (entities.SP_ManagementDashboardManagerComplianceCount(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                                ManagerComplianceCount = (from row in ManagerCompliancedetails
                                                          select row).Distinct().ToList();
                                if (ManagerComplianceCount.Count > 0)
                                {
                                    if (Branchlist.Count > 0)
                                    {
                                        ManagerComplianceCount = ManagerComplianceCount.Where(entry => Branchlist.Contains((long)entry.CustomerBranchID)).ToList();
                                        statutorycategorycount = ManagerComplianceCount.Select(entry => entry.ComplianceCategoryId).Distinct().ToList().Count();
                                        statutorycompliancecount = ManagerComplianceCount.Select(entry => entry.ComplianceID).Distinct().ToList().Count();
                                        statutorylocationcount = ManagerComplianceCount.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                        statutoryTotalcompliancecount = ManagerComplianceCount.Distinct().ToList().Count();
                                    }
                                    else
                                    {
                                        statutorycategorycount = ManagerComplianceCount.Select(entry => entry.ComplianceCategoryId).Distinct().ToList().Count();
                                        statutorycompliancecount = ManagerComplianceCount.Select(entry => entry.ComplianceID).Distinct().ToList().Count();
                                        statutorylocationcount = ManagerComplianceCount.Select(entry => entry.CustomerBranchID).Distinct().ToList().Count();
                                        statutoryTotalcompliancecount = ManagerComplianceCount.Distinct().ToList().Count();
                                    }
                                    entities.Database.CommandTimeout = 180;
                                    statutoryentitycount = (entities.sp_Entitycount((int)AuthenticationHelper.UserID, "SAT")).ToList().Count();
                                    entities.Database.CommandTimeout = 180;
                                    statutoryucount = (from row in entities.Users
                                                       where row.CustomerID == AuthenticationHelper.CustomerID
                                                       && row.IsActive == true && row.IsDeleted == false
                                                       select row.ID).ToList().Count();

                                    divCompliancesCount.InnerText = Convert.ToString(statutorycompliancecount);
                                    divUniqueCompliancesCount.InnerText = Convert.ToString(statutorycompliancecount);
                                    divTotalCompliancesCount.InnerText = Convert.ToString(statutoryTotalcompliancecount);
                                    divFunctionCount.InnerText = Convert.ToString(statutorycategorycount);
                                    divEntitesCount.InnerText = Convert.ToString(statutoryentitycount);
                                    divLocationCount.InnerText = Convert.ToString(statutorylocationcount);
                                    divUsersCount.InnerText = Convert.ToString(statutoryucount);
                                }
                                else
                                {
                                    divCompliancesCount.InnerText = "0";
                                    divUniqueCompliancesCount.InnerText = "0";
                                    divTotalCompliancesCount.InnerText = "0";
                                    divFunctionCount.InnerText = "0";
                                    divEntitesCount.InnerText = "0";
                                    divLocationCount.InnerText = "0";
                                    divUsersCount.InnerText = "0";
                                }
                            }
                            else
                            {
                                divCompliancesCount.InnerText = Convert.ToString(countdetails.ComplianceCount);
                                divUniqueCompliancesCount.InnerText = Convert.ToString(countdetails.ComplianceCount);
                                divFunctionCount.InnerText = Convert.ToString(countdetails.CategoryCount);
                                divEntitesCount.InnerText = Convert.ToString(countdetails.EntityCount);
                                divLocationCount.InnerText = Convert.ToString(countdetails.LocationCount);
                                divUsersCount.InnerText = Convert.ToString(countdetails.UserCount);

                                try
                                {
                                    customizedid = CustomerManagement.GetCustomizedCustomerid(Convert.ToInt32(AuthenticationHelper.CustomerID), "MGMT_Total_Unique");
                                    if (AuthenticationHelper.CustomerID == customizedid)
                                    {
                                        var ACdetails = (entities.SP_ManagementDashboardManagerComplianceCount(Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(AuthenticationHelper.CustomerID))).ToList();
                                        divTotalCompliancesCount.InnerText = Convert.ToString(ACdetails.Count);
                                    }
                                }
                                catch (Exception)
                                {
                                    throw;
                                }
                            }
                            #endregion
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region Dept
         
        public void GetManagementCompliancesSummaryDEPT(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<SP_GetManagementCompliancesSummary_Result> MasterQuery, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1)
        {
            try
            {
                string IsSatutoryInternalORALL = string.Empty;
                if (ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                {
                    IsSatutoryInternalORALL = "Statutory";
                }
                else if (ddlStatus.SelectedItem.Text == "Statutory")
                {
                    IsSatutoryInternalORALL = "StatutoryAll";
                }
                else if (ddlStatus.SelectedItem.Text == "Internal")
                {
                    IsSatutoryInternalORALL = "Internal";
                }
                string tempperFunctionChart = perdeptChartzomato;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perdeptChartzomato = string.Empty;
                        perdeptChartCatzomato = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        perdeptChartzomato = string.Empty;
                        perdeptChartCatzomato = string.Empty;
                    }
                }
                else
                {
                    perdeptChartzomato = string.Empty;
                    perdeptChartCatzomato = string.Empty;
                }

                bool auditor = false;
                if (AuthenticationHelper.Role == "AUDT")
                {
                    auditor = true;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                    if (Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    List<SP_GetManagementCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate
                                             && row.ScheduledOn <= EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate && row.ScheduledOn <= EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.ScheduledOn <= EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                 || row.ScheduledOn <= EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                         || row.ScheduledOn <= FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.ScheduledOn >= FIFromDate
                                                         && row.ScheduledOn <= FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn <= FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    
                    long totalCompletedcount = 0;
                    long totalNotCompletedCount = 0;
                    long totalAfterDueDatecount = 0;
                    long totalOverduecount = 0;
                    long totalPendingforreviewcount = 0;
                    long totalInProgresscount = 0;
                    long totalRejectedcount = 0;
                    long totalNotApplicablecount = 0;
                  

                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("NC", typeof(long));
                    table.Columns.Add("ADD", typeof(long));
                    table.Columns.Add("IT", typeof(long));
                    table.Columns.Add("OD", typeof(long));
                    table.Columns.Add("PFR", typeof(long));
                    table.Columns.Add("IP", typeof(long));
                    table.Columns.Add("REJ", typeof(long));
                    table.Columns.Add("NA", typeof(long));
                   

                    string listCategoryId = "";
                    List<sp_ComplianceAssignedDepartment_Result> CatagoryList = new List<sp_ComplianceAssignedDepartment_Result>();
                    if (approver == true)
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "S", true);
                    }
                    else
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "S");
                    }
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                    string perdeptChartCatzomato1 = "categories: [";

                    string perFunctionNC = "";
                    string perFunctionADD = "";
                    string perFunctionIT = "";
                    string perFunctionOD = "";
                    string perFunctionPFR= "";
                    string perFunctionIP = "";
                    string perFunctionREJ = "";
                    string perFunctionNA = "";
                    //string perFunctionCBDP = "";

                    perFunctionADD = "series: [{name: 'Closed Delayed',id: 'After Due Date',color: perFunctionChartColorScheme.closedelay, data: [";
                    perFunctionIT = "{name: 'Closed Timely',id: 'In Time',color: perFunctionChartColorScheme.closetimely, data: [";
                    perFunctionOD = "{name: 'Overdue',id: 'Overdue',color: perFunctionChartColorScheme.Overdue, data: [";
                    perFunctionPFR = "{name: 'Pending For Review',id: 'Pending For Review',color: perFunctionChartColorScheme.Pendingforreview, data: [";
                    perFunctionIP = "{name: 'In Progress',id: 'In Progress',color: perFunctionChartColorScheme.InProgress, data: [";
                    perFunctionREJ = "{name: 'Rejected',id: 'Rejected',color: perFunctionChartColorScheme.Rejected, data: [";
                    perFunctionNA = "{name: 'Not Applicable',id: 'Not Applicable',color: perFunctionChartColorScheme.NotApplicable, data: [";
                   
                     
                    perdeptChartzomato = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{";
                    foreach (sp_ComplianceAssignedDepartment_Result cc in CatagoryList)
                    {
                        //&& entry.ComplianceStatusID != 11
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.DepartmentID == cc.Id 
                        
                        ).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.DepartmentID == cc.Id 
                        
                        ).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.DepartmentID == cc.Id 
                     
                        ).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.DepartmentID == cc.Id 
                        
                        ).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;
                     
                        if (totalcount != 0)
                        {
                            perdeptChartCatzomato1 += "," + '"' + cc.Name.ToString() + '"';

                            totalAfterDueDatecount = transactionsQuery.Where(entry => entry.FilterStatus == "ClosedDelayed" && entry.DepartmentID == cc.Id).Count();
                            totalCompletedcount = transactionsQuery.Where(entry => entry.FilterStatus == "ClosedTimely" && entry.DepartmentID == cc.Id).Count();
                            //totalAfterDueDatecount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            //totalCompletedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 ) && entry.DepartmentID == cc.Id).Count();
                            totalNotCompletedCount = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            totalOverduecount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 ||  entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 ) && entry.DepartmentID == cc.Id).Count();
                            totalPendingforreviewcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            totalInProgresscount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 10) && entry.DepartmentID == cc.Id).Count();
                            totalRejectedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 6 ) && entry.DepartmentID == cc.Id).Count();
                            totalNotApplicablecount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 15 ) && entry.DepartmentID == cc.Id).Count();
                            
                            if (ColoumnNames[3] == "ADD")
                            {
                                perFunctionADD += "{ name:'" + cc.Name + "', y: " + totalAfterDueDatecount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHADD_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[4] == "IT")
                            {
                                perFunctionIT += "{ name:'" + cc.Name + "', y: " + totalCompletedcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHIT_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[5] == "OD")
                            {
                                perFunctionOD += "{ name:'" + cc.Name + "', y: " + totalOverduecount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHOD_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[6] == "PFR")
                            {
                                perFunctionPFR += "{ name:'" + cc.Name + "', y: " + totalPendingforreviewcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHPFR_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[7] == "IP")
                            {
                                perFunctionIP += "{ name:'" + cc.Name + "', y: " + totalInProgresscount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHIP_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[8] == "REJ")
                            {
                                perFunctionREJ += "{ name:'" + cc.Name + "', y: " + totalRejectedcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHREJ_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[9] == "NA")
                            {
                                perFunctionNA += "{ name:'" + cc.Name + "', y: " + totalNotApplicablecount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHNA_summary')" +
                             "}}},";

                            }
                         
                        }

                    }
                    perdeptChartCatzomato1 += "]";
                 
                    if (perdeptChartCatzomato1.Length > 15)
                    {
                        perdeptChartCatzomato = perdeptChartCatzomato1.Remove(13, 1);
                    }
                    else
                    {
                        perdeptChartCatzomato = perdeptChartCatzomato1;
                    }
                    //perFunctionNC += "],},";
                    perFunctionADD += "],},";
                    perFunctionIT += "],},";
                    perFunctionOD += "],},";
                    perFunctionPFR += "],},";
                    perFunctionIP += "],},";
                    perFunctionREJ += "],},";
                    perFunctionNA += "],},";
                    //perFunctionCBDP += "],},";
                    perdeptChartzomato =  perFunctionADD + "" + perFunctionIT + "" + perFunctionOD + "" + perFunctionPFR + "" + perFunctionIP + "" + perFunctionREJ + "" + perFunctionNA;
                    perdeptChartzomato += "]";
                    

                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "R")
                        {
                          
                            perdeptChartzomato = tempperFunctionChart;

                           
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        public void GetManagementCompliancesSummaryDEPTZomato(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<SP_GetManagementCompliancesSummary_Result> MasterQuery, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1)
        {
            try
            {
                string IsSatutoryInternalORALL = string.Empty;
                if (ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                {
                    IsSatutoryInternalORALL = "Statutory";
                }
                else if (ddlStatus.SelectedItem.Text == "Statutory")
                {
                    IsSatutoryInternalORALL = "StatutoryAll";
                }
                else if (ddlStatus.SelectedItem.Text == "Internal")
                {
                    IsSatutoryInternalORALL = "Internal";
                }
                string tempperFunctionChart = perdeptChartzomato;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perdeptChartzomato = string.Empty;
                        perdeptChartCatzomato = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        perdeptChartzomato = string.Empty;
                        perdeptChartCatzomato = string.Empty;
                    }
                }
                else
                {
                    perdeptChartzomato = string.Empty;
                    perdeptChartCatzomato = string.Empty;
                }

                bool auditor = false;
                if (AuthenticationHelper.Role == "AUDT")
                {
                    auditor = true;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                    if (Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    List<SP_GetManagementCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate
                                             && row.ScheduledOn <= EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate && row.ScheduledOn <= EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.ScheduledOn <= EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                 || row.ScheduledOn <= EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                         || row.ScheduledOn <= FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.ScheduledOn >= FIFromDate
                                                         && row.ScheduledOn <= FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn <= FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    long totalCompletedcount = 0;
                    long totalNotCompletedCount = 0;
                    long totalAfterDueDatecount = 0;
                    long totalOverduecount = 0;
                    long totalPendingforreviewcount = 0;
                    long totalInProgresscount = 0;
                    long totalRejectedcount = 0;
                    long totalNotApplicablecount = 0;
                    long totalCompiledbutdocumentpendingcount = 0;


                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("NC", typeof(long));
                    table.Columns.Add("ADD", typeof(long));
                    table.Columns.Add("IT", typeof(long));
                    table.Columns.Add("OD", typeof(long));
                    table.Columns.Add("PFR", typeof(long));
                    table.Columns.Add("IP", typeof(long));
                    table.Columns.Add("REJ", typeof(long));
                    table.Columns.Add("NA", typeof(long));
                    table.Columns.Add("CBDP", typeof(long));



                    string listCategoryId = "";
                    List<sp_ComplianceAssignedDepartment_Result> CatagoryList = new List<sp_ComplianceAssignedDepartment_Result>();
                    if (approver == true)
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "S", true);
                    }
                    else
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "S");
                    }
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                    string perdeptChartCatzomato1 = "categories: [";

                    string perFunctionNC = "";
                    string perFunctionADD = "";
                    string perFunctionIT = "";
                    string perFunctionOD = "";
                    string perFunctionPFR = "";
                    string perFunctionIP = "";
                    string perFunctionREJ = "";
                    string perFunctionNA = "";
                    string perFunctionCBDP = "";

                    //perFunctionNC = "series: [ {name: 'Not Completed',id: 'Not Completed',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionADD = "series: [{name: 'Closed Delayed',id: 'After Due Date',color: perFunctionChartColorScheme.closedelay, data: [";
                    perFunctionIT = "{name: 'Closed Timely',id: 'In Time',color: perFunctionChartColorScheme.closetimely, data: [";
                    perFunctionOD = "{name: 'Overdue',id: 'Overdue',color: perFunctionChartColorScheme.Overdue, data: [";
                    perFunctionPFR = "{name: 'Pending For Review',id: 'Pending For Review',color: perFunctionChartColorScheme.Pendingforreview, data: [";
                    perFunctionIP = "{name: 'In Progress',id: 'In Progress',color: perFunctionChartColorScheme.InProgress, data: [";
                    perFunctionREJ = "{name: 'Rejected',id: 'Rejected',color: perFunctionChartColorScheme.Rejected, data: [";
                    perFunctionNA = "{name: 'Not Applicable',id: 'Not Applicable',color: perFunctionChartColorScheme.NotApplicable, data: [";
                    perFunctionCBDP = "{name: 'Complied But Document Pending',id: 'Complied But Document Pending',color: perFunctionChartColorScheme.Compiledbutdocumentpending, data: [";


                    perdeptChartzomato = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{";
                    foreach (sp_ComplianceAssignedDepartment_Result cc in CatagoryList)
                    {
                        //LogLibrary.WriteErrorLog(cc.Name + " : Start perdeptChartCatzomato");
                        // listCategoryId += ',' + cc.Id.ToString();


                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.DepartmentID == cc.Id && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.DepartmentID == cc.Id && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.DepartmentID == cc.Id && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.DepartmentID == cc.Id && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;
                     
                        if (totalcount != 0)
                        {
                            perdeptChartCatzomato1 += "," + '"' + cc.Name.ToString() + '"';
                            //totalAfterDueDatecount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            //totalCompletedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.DepartmentID == cc.Id).Count();

                            totalAfterDueDatecount = transactionsQuery.Where(entry => entry.FilterStatus == "ClosedDelayed" && entry.DepartmentID == cc.Id).Count();
                            totalCompletedcount = transactionsQuery.Where(entry => entry.FilterStatus == "ClosedTimely" && entry.DepartmentID == cc.Id).Count();
                            totalNotCompletedCount = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            totalOverduecount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.DepartmentID == cc.Id).Count();
                            totalPendingforreviewcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            totalInProgresscount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 10) && entry.DepartmentID == cc.Id).Count();
                            totalRejectedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 6) && entry.DepartmentID == cc.Id).Count();
                            totalNotApplicablecount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            totalCompiledbutdocumentpendingcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 19) && entry.DepartmentID == cc.Id).Count();


                            // if (ColoumnNames[2] == "NC")
                            // {
                            //     perFunctionNC += "{ name:'" + cc.Name + "', y: " + totalNotCompletedCount + ",events:{click: function(e) {" +
                            //" fpopulateddepartmentdata('All','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Statutory','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHNC_summary')" +
                            //   "}}},";
                            // }
                            if (ColoumnNames[3] == "ADD")
                            {
                                perFunctionADD += "{ name:'" + cc.Name + "', y: " + totalAfterDueDatecount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHADD_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[4] == "IT")
                            {
                                perFunctionIT += "{ name:'" + cc.Name + "', y: " + totalCompletedcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHIT_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[5] == "OD")
                            {
                                perFunctionOD += "{ name:'" + cc.Name + "', y: " + totalOverduecount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHOD_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[6] == "PFR")
                            {
                                perFunctionPFR += "{ name:'" + cc.Name + "', y: " + totalPendingforreviewcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHPFR_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[7] == "IP")
                            {
                                perFunctionIP += "{ name:'" + cc.Name + "', y: " + totalInProgresscount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHIP_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[8] == "REJ")
                            {
                                perFunctionREJ += "{ name:'" + cc.Name + "', y: " + totalRejectedcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHREJ_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[9] == "NA")
                            {
                                perFunctionNA += "{ name:'" + cc.Name + "', y: " + totalNotApplicablecount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHNA_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[10] == "CBDP")
                            {
                                perFunctionCBDP += "{ name:'" + cc.Name + "', y: " + totalCompiledbutdocumentpendingcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHCBDP_summary')" +
                             "}}},";
                            }
                        }

                        //LogLibrary.WriteErrorLog(cc.Name + " : End perdeptChartCatzomato");
                    }
                    perdeptChartCatzomato1 += "]";
                    //perdeptChartCatzomato = perdeptChartCatzomato1.Remove(13, 1);
                    if (perdeptChartCatzomato1.Length > 15)
                    {
                        perdeptChartCatzomato = perdeptChartCatzomato1.Remove(13, 1);
                    }
                    else
                    {
                        perdeptChartCatzomato = perdeptChartCatzomato1;
                    }
                    //perFunctionNC += "],},";
                    perFunctionADD += "],},";
                    perFunctionIT += "],},";
                    perFunctionOD += "],},";
                    perFunctionPFR += "],},";
                    perFunctionIP += "],},";
                    perFunctionREJ += "],},";
                    perFunctionNA += "],},";
                    perFunctionCBDP += "],},";
                    perdeptChartzomato = perFunctionADD + "" + perFunctionIT + "" + perFunctionOD + "" + perFunctionPFR + "" + perFunctionIP + "" + perFunctionREJ + "" + perFunctionNA + "" + perFunctionCBDP;
                    perdeptChartzomato += "]";


                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "R")
                        {
                            // LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : Start perdeptChartCatzomato");
                            perdeptChartzomato = tempperFunctionChart;

                            //LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : End perdeptChartCatzomato");
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
 
        public void GetManagementInternalCompliancesSummaryDEPT(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, DateTime? FromDate = null, DateTime? EndDateF = null, bool approver = false)
        {
            try
            {
                //string tempperFunctionChart = perFunctionChartDEPT;
                string tempperFunctionChart = perdeptChartzomato;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                       // perFunctionChartDEPT = string.Empty;
                        perdeptChartCatzomato = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                      //  perFunctionChartDEPT = string.Empty;
                        perdeptChartCatzomato = string.Empty;
                    }
                }
                else
                {
                  //  perFunctionChartDEPT = string.Empty;
                    perdeptChartCatzomato = string.Empty;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementInternalCompliancesSummary_DeptHead_Result> MasterManagementInternalCompliancesSummaryQuery = new List<SP_GetManagementInternalCompliancesSummary_DeptHead_Result>();
                    entities.Database.CommandTimeout = 180;
                    if (approver == true)
                    {
                        MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary_DeptHead(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                    }
                    else
                    {
                        if (AuthenticationHelper.Role.Equals("MGMT"))
                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary_DeptHead(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                        else
                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary_DeptHead(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "DEPT")).ToList();
                    }
                    if (Branchlist.Count > 0)
                    {
                        MasterManagementInternalCompliancesSummaryQuery = MasterManagementInternalCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }
                    List<SP_GetManagementInternalCompliancesSummary_DeptHead_Result> transactionsQuery = new List<SP_GetManagementInternalCompliancesSummary_DeptHead_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.InternalScheduledOn >= FromDate
                                             && row.InternalScheduledOn <= EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.InternalScheduledOn >= FromDate && row.InternalScheduledOn <= EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.InternalScheduledOn <= EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.InternalComplianceStatusID == 5 || row.InternalComplianceStatusID == 4
                                                 || row.InternalScheduledOn <= EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.InternalComplianceStatusID == 5 || row.InternalComplianceStatusID == 4
                                                         || row.InternalScheduledOn <= FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.InternalScheduledOn >= FIFromDate
                                                         && row.InternalScheduledOn <= FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.InternalScheduledOn <= FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    long totalCompletedcount = 0;
                    long totalNotCompletedCount = 0;
                    long totalAfterDueDatecount = 0;
                    long totalOverduecount = 0;
                    long totalPendingforreviewcount = 0;
                    long totalInProgresscount = 0;
                    long totalRejectedcount = 0;
                    long totalNotApplicablecount = 0;
                    long totalCompiledbutdocumentpendingcount = 0;

                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;

                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("NC", typeof(long));
                    table.Columns.Add("ADD", typeof(long));
                    table.Columns.Add("IT", typeof(long));
                    table.Columns.Add("OD", typeof(long));
                    table.Columns.Add("PFR", typeof(long));
                    table.Columns.Add("IP", typeof(long));
                    table.Columns.Add("REJ", typeof(long));
                    table.Columns.Add("NA", typeof(long));
                    table.Columns.Add("CBDP", typeof(long));

                    string listCategoryId = "";
                    List<sp_ComplianceAssignedDepartment_Result> CatagoryList = new List<sp_ComplianceAssignedDepartment_Result>();
                    if (approver == true)
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "I", true);
                    }
                    else
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "I");
                    }
                    
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                    
                    string perdeptChartCatzomato1 = "categories: [";

                    string perFunctionNC = "";
                    string perFunctionADD = "";
                    string perFunctionIT = "";
                    string perFunctionOD = "";
                    string perFunctionPFR = "";
                    string perFunctionIP = "";
                    string perFunctionREJ = "";
                    string perFunctionNA = "";
                    string perFunctionCBDP = "";

                    //perFunctionNC = "series: [ {name: 'Not Completed',id: 'Not Completed',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionADD = "series: [{name: 'Closed Delayed',id: 'After Due Date',color: perFunctionChartColorScheme.closedelay, data: [";
                    perFunctionIT = "{name: 'Closed Timely',id: 'In Time',color: perFunctionChartColorScheme.closetimely, data: [";
                    perFunctionOD = "{name: 'Overdue',id: 'Overdue',color: perFunctionChartColorScheme.Overdue, data: [";
                    perFunctionPFR = "{name: 'Pending For Review',id: 'Pending For Review',color: perFunctionChartColorScheme.Pendingforreview, data: [";
                    perFunctionIP = "{name: 'In Progress',id: 'In Progress',color: perFunctionChartColorScheme.InProgress, data: [";
                    perFunctionREJ = "{name: 'Rejected',id: 'Rejected',color: perFunctionChartColorScheme.Rejected, data: [";
                    perFunctionNA = "{name: 'Not Applicable',id: 'Not Applicable',color: perFunctionChartColorScheme.NotApplicable, data: [";
                    perFunctionCBDP = "{name: 'Complied But Document Pending',id: 'Complied But Document Pending',color: perFunctionChartColorScheme.Compiledbutdocumentpending, data: [";

                    perdeptChartzomato = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{";

                    foreach (sp_ComplianceAssignedDepartment_Result cc in CatagoryList)
                    {
                        
                        listCategoryId += ',' + cc.Id.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.DepartmentID == cc.Id && entry.InternalComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.DepartmentID == cc.Id && entry.InternalComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.DepartmentID == cc.Id && entry.InternalComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.DepartmentID == cc.Id && entry.InternalComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {
                            perdeptChartCatzomato1 += "," + '"' + cc.Name.ToString() + '"';
                            totalAfterDueDatecount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            totalCompletedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.DepartmentID == cc.Id).Count();
                            totalNotCompletedCount = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10
                            || entry.InternalComplianceStatusID == 12 || entry.InternalComplianceStatusID == 13 || entry.InternalComplianceStatusID == 14 || entry.InternalComplianceStatusID == 11 || entry.InternalComplianceStatusID == 16 || entry.InternalComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            totalOverduecount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 12 || entry.InternalComplianceStatusID == 13 || entry.InternalComplianceStatusID == 14) && entry.DepartmentID == cc.Id).Count();
                            totalPendingforreviewcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11 || entry.InternalComplianceStatusID == 16 || entry.InternalComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            totalInProgresscount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 10) && entry.DepartmentID == cc.Id).Count();
                            totalRejectedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 6) && entry.DepartmentID == cc.Id).Count();
                            totalNotApplicablecount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            totalCompiledbutdocumentpendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 19) && entry.DepartmentID == cc.Id).Count();


                            // if (ColoumnNames[2] == "NC")
                            // {
                            //     perFunctionNC += "{ name:'" + cc.Name + "', y: " + totalNotCompletedCount + ",events:{click: function(e) {" +
                            //" fpopulateddepartmentdata('All','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','"+ cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHNC_summary')" +
                            //   "}}},";
                            // }
                            if (ColoumnNames[3] == "ADD")
                            {
                                perFunctionADD += "{ name:'" + cc.Name + "', y: " + totalAfterDueDatecount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHADD_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[4] == "IT")
                            {
                                perFunctionIT += "{ name:'" + cc.Name + "', y: " + totalCompletedcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHIT_summary')" +
                             "}}},";
                            }

                            if (ColoumnNames[5] == "OD")
                            {
                                perFunctionOD += "{ name:'" + cc.Name + "', y: " + totalOverduecount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHOD_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[6] == "PFR")
                            {
                                perFunctionPFR += "{ name:'" + cc.Name + "', y: " + totalPendingforreviewcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHPFR_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[7] == "IP")
                            {
                                perFunctionIP += "{ name:'" + cc.Name + "', y: " + totalInProgresscount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHIP_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[8] == "REJ")
                            {
                                perFunctionREJ += "{ name:'" + cc.Name + "', y: " + totalRejectedcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHREJ_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[9] == "NA")
                            {
                                perFunctionNA += "{ name:'" + cc.Name + "', y: " + totalNotApplicablecount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHNA_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[10] == "CBDP")
                            {
                                perFunctionCBDP += "{ name:'" + cc.Name + "', y: " + totalCompiledbutdocumentpendingcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHCBDP_summary')" +
                             "}}},";
                            }
                        }
                    }
                    perdeptChartCatzomato1 += "]";
                    //perdeptChartCatzomato = perdeptChartCatzomato1.Remove(13, 1);
                    if (perdeptChartCatzomato1.Length > 15)
                    {
                        perdeptChartCatzomato = perdeptChartCatzomato1.Remove(13, 1);
                    }
                    else
                    {
                        perdeptChartCatzomato = perdeptChartCatzomato1;
                    }
                    //perFunctionNC += "],},";
                    perFunctionADD += "],},";
                    perFunctionIT += "],},";
                    perFunctionOD += "],},";
                    perFunctionPFR += "],},";
                    perFunctionIP += "],},";
                    perFunctionREJ += "],},";
                    perFunctionNA += "],},";
                    perFunctionCBDP += "],},";
                    perdeptChartzomato = perFunctionADD + "" + perFunctionIT + "" + perFunctionOD + "" + perFunctionPFR + "" + perFunctionIP + "" + perFunctionREJ + "" + perFunctionNA + "" + perFunctionCBDP;
                    perdeptChartzomato += "]";


                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "R")
                        {
                            // perFunctionChartDEPT = tempperFunctionChart;
                            perdeptChartzomato = tempperFunctionChart;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
     
        public void GetManagementInternalCompliancesSummaryDEPTAddedNotCompliedStatus(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, DateTime? FromDate = null, DateTime? EndDateF = null, bool approver = false)
        {
            try
            {
                //string tempperFunctionChart = perFunctionChartDEPT;
                string tempperFunctionChart = perdeptChartzomato;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        // perFunctionChartDEPT = string.Empty;
                        perdeptChartCatzomato = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        //perFunctionChartDEPT = string.Empty;
                        perdeptChartCatzomato = string.Empty;
                    }
                }
                else
                {
                   // perFunctionChartDEPT = string.Empty;
                    perdeptChartCatzomato = string.Empty;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementInternalCompliancesSummary_DeptHead_Result> MasterManagementInternalCompliancesSummaryQuery = new List<SP_GetManagementInternalCompliancesSummary_DeptHead_Result>();
                    entities.Database.CommandTimeout = 180;
                    if (approver == true)
                    {
                        MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary_DeptHead(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                    }
                    else
                    {
                        if (AuthenticationHelper.Role.Equals("MGMT"))
                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary_DeptHead(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                        else
                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary_DeptHead(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "DEPT")).ToList();
                    }
                    if (Branchlist.Count > 0)
                    {
                        MasterManagementInternalCompliancesSummaryQuery = MasterManagementInternalCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }
                    List<SP_GetManagementInternalCompliancesSummary_DeptHead_Result> transactionsQuery = new List<SP_GetManagementInternalCompliancesSummary_DeptHead_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.InternalScheduledOn >= FromDate
                                             && row.InternalScheduledOn <= EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.InternalScheduledOn >= FromDate && row.InternalScheduledOn <= EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.InternalScheduledOn <= EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.InternalComplianceStatusID == 5 || row.InternalComplianceStatusID == 4
                                                 || row.InternalScheduledOn <= EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.InternalComplianceStatusID == 5 || row.InternalComplianceStatusID == 4
                                                         || row.InternalScheduledOn <= FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.InternalScheduledOn >= FIFromDate
                                                         && row.InternalScheduledOn <= FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementInternalCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.InternalScheduledOn <= FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    //PieChart
                    long totalCompletedcount = 0;
                    long totalNotCompletedCount = 0;
                    long totalNotCompliedCount = 0;
                    long totalAfterDueDatecount = 0;
                    long totalOverduecount = 0;
                    long totalPendingforreviewcount = 0;
                    long totalInProgresscount = 0;
                    long totalRejectedcount = 0;
                    long totalNotApplicablecount = 0;
                    long totalCompiledbutdocumentpendingcount = 0;
                    
                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("NC", typeof(long));
                    table.Columns.Add("NCP", typeof(long));
                    table.Columns.Add("ADD", typeof(long));
                    table.Columns.Add("IT", typeof(long));
                    table.Columns.Add("OD", typeof(long));
                    table.Columns.Add("PFR", typeof(long));
                    table.Columns.Add("IP", typeof(long));
                    table.Columns.Add("REJ", typeof(long));
                    table.Columns.Add("NA", typeof(long));
                    table.Columns.Add("CBDP", typeof(long));

                    string listCategoryId = "";
                    List<sp_ComplianceAssignedDepartment_Result> CatagoryList = new List<sp_ComplianceAssignedDepartment_Result>();
                    if (approver == true)
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "I", true);
                    }
                    else
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "I");
                    }
                   string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

                    string perdeptChartCatzomato1 = "categories: [";

                    string perFunctionNC = "";
                    string perFunctionNCP = "";
                    string perFunctionADD = "";
                    string perFunctionIT = "";
                    string perFunctionOD = "";
                    string perFunctionPFR = "";
                    string perFunctionIP = "";
                    string perFunctionREJ = "";
                    string perFunctionNA = "";
                    string perFunctionCBDP = "";

                    //perFunctionNC = "series: [ {name: 'Not Completed',id: 'Not Completed',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionNCP = "series: [{name: 'Not complied',id: 'Not Complied',color: perFunctionChartColorScheme.NotComplied, data: [";
                    perFunctionADD = "{name: 'Closed Delayed',id: 'After Due Date',color: perFunctionChartColorScheme.closedelay, data: [";
                    perFunctionIT = "{name: 'Closed Timely',id: 'In Time',color: perFunctionChartColorScheme.closetimely, data: [";
                    perFunctionOD = "{name: 'Overdue',id: 'Overdue',color: perFunctionChartColorScheme.Overdue, data: [";
                    perFunctionPFR = "{name: 'Pending For Review',id: 'Pending For Review',color: perFunctionChartColorScheme.Pendingforreview, data: [";
                    perFunctionIP = "{name: 'In Progress',id: 'In Progress',color: perFunctionChartColorScheme.InProgress, data: [";
                    perFunctionREJ = "{name: 'Rejected',id: 'Rejected',color: perFunctionChartColorScheme.Rejected, data: [";
                    perFunctionNA = "{name: 'Not Applicable',id: 'Not Applicable',color: perFunctionChartColorScheme.NotApplicable, data: [";
                    perFunctionCBDP = "{name: 'Complied But Document Pending',id: 'Complied But Document Pending',color: perFunctionChartColorScheme.Compiledbutdocumentpending, data: [";

                    perdeptChartzomato = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{";


                    foreach (sp_ComplianceAssignedDepartment_Result cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.Id.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.DepartmentID == cc.Id && entry.InternalComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.DepartmentID == cc.Id && entry.InternalComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.DepartmentID == cc.Id && entry.InternalComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.DepartmentID == cc.Id && entry.InternalComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {
                            perdeptChartCatzomato1 += "," + '"' + cc.Name.ToString() + '"';
                            totalAfterDueDatecount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            totalCompletedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7) && entry.DepartmentID == cc.Id).Count();
                            totalNotCompletedCount = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10
                            || entry.InternalComplianceStatusID == 12 || entry.InternalComplianceStatusID == 13 || entry.InternalComplianceStatusID == 14 || entry.InternalComplianceStatusID == 11 || entry.InternalComplianceStatusID == 16 || entry.InternalComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            totalOverduecount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 12 || entry.InternalComplianceStatusID == 13 || entry.InternalComplianceStatusID == 14) && entry.DepartmentID == cc.Id).Count();
                            totalPendingforreviewcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 11 || entry.InternalComplianceStatusID == 16 || entry.InternalComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            totalInProgresscount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 10) && entry.DepartmentID == cc.Id).Count();
                            totalRejectedcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 6) && entry.DepartmentID == cc.Id).Count();
                            totalNotApplicablecount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            totalCompiledbutdocumentpendingcount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 19) && entry.DepartmentID == cc.Id).Count();
                            totalNotCompliedCount = transactionsQuery.Where(entry => (entry.InternalComplianceStatusID == 17) && entry.DepartmentID == cc.Id).Count();

                            // if (ColoumnNames[2] == "NC")
                            // {
                            //     perFunctionNC += "{ name:'" + cc.Name + "', y: " + totalNotCompletedCount + ",events:{click: function(e) {" +
                            //" fpopulateddepartmentdata('All','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','"+ cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHNC_summary')" +
                            //   "}}},";
                            // }
                            if (ColoumnNames[3] == "NCP")
                            {
                                perFunctionNCP += "{ name:'" + cc.Name + "', y: " + totalNotCompliedCount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHNCP_summary')" +
                              "}}},";
                            }
                            if (ColoumnNames[4] == "ADD")
                            {
                                perFunctionADD += "{ name:'" + cc.Name + "', y: " + totalAfterDueDatecount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHADD_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[5] == "IT")
                            {
                                perFunctionIT += "{ name:'" + cc.Name + "', y: " + totalCompletedcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHIT_summary')" +
                             "}}},";
                            }

                            if (ColoumnNames[6] == "OD")
                            {
                                perFunctionOD += "{ name:'" + cc.Name + "', y: " + totalOverduecount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHOD_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[7] == "PFR")
                            {
                                perFunctionPFR += "{ name:'" + cc.Name + "', y: " + totalPendingforreviewcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHPFR_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[8] == "IP")
                            {
                                perFunctionIP += "{ name:'" + cc.Name + "', y: " + totalInProgresscount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHIP_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[9] == "REJ")
                            {
                                perFunctionREJ += "{ name:'" + cc.Name + "', y: " + totalRejectedcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHREJ_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[10] == "NA")
                            {
                                perFunctionNA += "{ name:'" + cc.Name + "', y: " + totalNotApplicablecount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHNA_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[11] == "CBDP")
                            {
                                perFunctionCBDP += "{ name:'" + cc.Name + "', y: " + totalCompiledbutdocumentpendingcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Internal','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHCBDP_summary')" +
                             "}}},";
                            }

                        }
                    }

                    perdeptChartCatzomato1 += "]";
                    //perdeptChartCatzomato = perdeptChartCatzomato1.Remove(13, 1);
                    if (perdeptChartCatzomato1.Length > 15)
                    {
                        perdeptChartCatzomato = perdeptChartCatzomato1.Remove(13, 1);
                    }
                    else
                    {
                        perdeptChartCatzomato = perdeptChartCatzomato1;
                    }
                    //perFunctionNC += "],},";
                    perFunctionNCP += "],},";
                    perFunctionADD += "],},";
                    perFunctionIT += "],},";
                    perFunctionOD += "],},";
                    perFunctionPFR += "],},";
                    perFunctionIP += "],},";
                    perFunctionREJ += "],},";
                    perFunctionNA += "],},";
                    perFunctionCBDP += "],},";
                    perdeptChartzomato = perFunctionNCP + "" + perFunctionADD + "" + perFunctionIT + "" + perFunctionOD + "" + perFunctionPFR + "" + perFunctionIP + "" + perFunctionREJ + "" + perFunctionNA + "" + perFunctionCBDP;
                    perdeptChartzomato += "]";
                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "R")
                        {
                            // perFunctionChartDEPT = tempperFunctionChart;
                            perdeptChartzomato = tempperFunctionChart;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
 
        public void GetManagementCompliancesSummaryDEPTAddedNotCompliedStatus(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<SP_GetManagementCompliancesSummary_Result> MasterQuery, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1)
        {
            try
            {
                string IsSatutoryInternalORALL = string.Empty;
                if (ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                {
                    IsSatutoryInternalORALL = "Statutory";
                }
                else if (ddlStatus.SelectedItem.Text == "Statutory")
                {
                    IsSatutoryInternalORALL = "StatutoryAll";
                }
                else if (ddlStatus.SelectedItem.Text == "Internal")
                {
                    IsSatutoryInternalORALL = "Internal";
                }
                //string tempperFunctionChart = perFunctionChartDEPT;
                string tempperFunctionChart = perdeptChartzomato;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                       // perFunctionChartDEPT = string.Empty;
                        perdeptChartzomato = string.Empty;
                        perdeptChartCatzomato = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        //perFunctionChartDEPT = string.Empty;
                        perdeptChartzomato = string.Empty;
                        perdeptChartCatzomato = string.Empty;
                    }
                }
                else
                {
                    //perFunctionChartDEPT = string.Empty;
                    perdeptChartzomato = string.Empty;
                    perdeptChartCatzomato = string.Empty;
                }

                bool auditor = false;
                if (AuthenticationHelper.Role == "AUDT")
                {
                    auditor = true;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                    if (Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    List<SP_GetManagementCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate
                                             && row.ScheduledOn <= EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate && row.ScheduledOn <= EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.ScheduledOn <= EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                 || row.ScheduledOn <= EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                         || row.ScheduledOn <= FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.ScheduledOn >= FIFromDate
                                                         && row.ScheduledOn <= FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn <= FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    //PieChart
                    long totalCompletedcount = 0;
                    long totalNotCompletedCount = 0;
                    long totalAfterDueDatecount = 0;
                    long totalNotCompliedCount = 0;
                    long totalOverduecount = 0;
                    long totalPendingforreviewcount = 0;
                    long totalInProgresscount = 0;
                    long totalRejectedcount = 0;
                    long totalNotApplicablecount = 0;
                    long totalCompiledbutdocumentpendingcount = 0;



                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("NC", typeof(long));
                    table.Columns.Add("NCP", typeof(long));
                    table.Columns.Add("ADD", typeof(long));
                    table.Columns.Add("IT", typeof(long));
                    table.Columns.Add("OD", typeof(long));
                    table.Columns.Add("PFR", typeof(long));
                    table.Columns.Add("IP", typeof(long));
                    table.Columns.Add("REJ", typeof(long));
                    table.Columns.Add("NA", typeof(long));
                    //table.Columns.Add("CBDP", typeof(long));

                    string listCategoryId = "";
                    List<sp_ComplianceAssignedDepartment_Result> CatagoryList = new List<sp_ComplianceAssignedDepartment_Result>();
                    if (approver == true)
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "S", true);
                    }
                    else
                    {
                        CatagoryList = CompDeptManagement.GetNewDEPTAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, "S");
                    }

                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                    string perdeptChartCatzomato1 = "categories: [";
                    string perFunctionNC = "";
                    string perFunctionNCP = "";
                    string perFunctionADD = "";
                    string perFunctionIT = "";
                    string perFunctionOD = "";
                    string perFunctionPFR = "";
                    string perFunctionIP = "";
                    string perFunctionREJ = "";
                    string perFunctionNA = "";
                    //string perFunctionCBDP = "";

                    //perFunctionNC = "series: [ {name: 'Not Completed',id: 'Not Completed',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionNCP = "series: [ {name: 'Not Complied',id: 'Not Complied',color: perFunctionChartColorScheme.NotComplied, data: [";
                    perFunctionADD = "{name: 'Closed Delayed',id: 'After Due Date',color: perFunctionChartColorScheme.closedelay, data: [";
                    perFunctionIT = "{name: 'Closed Timely',id: 'In Time',color: perFunctionChartColorScheme.closetimely, data: [";
                    perFunctionOD = "{name: 'Overdue',id: 'Overdue',color: perFunctionChartColorScheme.Overdue, data: [";
                    perFunctionPFR = "{name: 'Pending For Review',id: 'Pending For Review',color: perFunctionChartColorScheme.Pendingforreview, data: [";
                    perFunctionIP = "{name: 'In Progress',id: 'In Progress',color: perFunctionChartColorScheme.InProgress, data: [";
                    perFunctionREJ = "{name: 'Rejected',id: 'Rejected',color: perFunctionChartColorScheme.Rejected, data: [";
                    perFunctionNA = "{name: 'Not Applicable',id: 'Not Applicable',color: perFunctionChartColorScheme.NotApplicable, data: [";
                    //perFunctionCBDP = "{name: 'Compiled Dut Document Pending',id: 'Compiled Dut Document Pending',color: perFunctionChartColorScheme.Compiledbutdocumentpending, data: [";


                    perdeptChartzomato = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{";

                    foreach (sp_ComplianceAssignedDepartment_Result cc in CatagoryList)
                    {
                       // listCategoryId += ',' + cc.Id.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.DepartmentID == cc.Id && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.DepartmentID == cc.Id && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.DepartmentID == cc.Id && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.DepartmentID == cc.Id && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;
                       
                        if (totalcount != 0)
                        {
                            perdeptChartCatzomato1 += "," + '"' + cc.Name.ToString() + '"';

                            //totalAfterDueDatecount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.DepartmentID == cc.Id).Count();
                            //totalCompletedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.DepartmentID == cc.Id).Count();

                            totalAfterDueDatecount = transactionsQuery.Where(entry => entry.FilterStatus == "ClosedDelayed" && entry.DepartmentID == cc.Id).Count();
                            totalCompletedcount = transactionsQuery.Where(entry =>  entry.FilterStatus == "ClosedTimely" && entry.DepartmentID == cc.Id).Count();
                            totalNotCompletedCount = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            totalOverduecount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.DepartmentID == cc.Id).Count();
                            totalPendingforreviewcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.DepartmentID == cc.Id).Count();
                            totalInProgresscount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 10) && entry.DepartmentID == cc.Id).Count();
                            totalRejectedcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 6) && entry.DepartmentID == cc.Id).Count();
                            totalNotApplicablecount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 15) && entry.DepartmentID == cc.Id).Count();
                            totalCompiledbutdocumentpendingcount = transactionsQuery.Where(entry => (entry.ComplianceStatusID == 19) && entry.DepartmentID == cc.Id).Count();
                            totalNotCompliedCount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceStatusID == 17 && entry.DepartmentID == cc.Id).Count();

                            // if (ColoumnNames[2] == "NC")
                            // {
                            //     perFunctionNC += "{ name:'" + cc.Name + "', y: " + totalNotCompletedCount + ",events:{click: function(e) {" +
                            //" fpopulateddepartmentdata('All','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Statutory','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHNC_summary')" +
                            //   "}}},";
                            // }

                            if (ColoumnNames[3] == "NCP")
                            {
                                perFunctionNCP += "{ name:'" + cc.Name + "', y: " + totalNotCompliedCount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHNCP_summary')" +
                              "}}},";
                            }
                            if (ColoumnNames[4] == "ADD")
                            {
                                perFunctionADD += "{ name:'" + cc.Name + "', y: " + totalAfterDueDatecount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHADD_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[5] == "IT")
                            {
                                perFunctionIT += "{ name:'" + cc.Name + "', y: " + totalCompletedcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHIT_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[6] == "OD")
                            {
                                perFunctionOD += "{ name:'" + cc.Name + "', y: " + totalOverduecount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHOD_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[7] == "PFR")
                            {
                                perFunctionPFR += "{ name:'" + cc.Name + "', y: " + totalPendingforreviewcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHPFR_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[8] == "IP")
                            {
                                perFunctionIP += "{ name:'" + cc.Name + "', y: " + totalInProgresscount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHIP_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[9] == "REJ")
                            {
                                perFunctionREJ += "{ name:'" + cc.Name + "', y: " + totalRejectedcount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHREJ_summary')" +
                             "}}},";

                            }
                            if (ColoumnNames[10] == "NA")
                            {
                                perFunctionNA += "{ name:'" + cc.Name + "', y: " + totalNotApplicablecount + ",events:{click: function(e) {" +
                           " fpopulateddepartmentdata('All','Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','" + IsSatutoryInternalORALL + "','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHNA_summary')" +
                             "}}},";

                            }
                           // if (ColoumnNames[11] == "CBDP")
                           // {
                           //     perFunctionCBDP += "{ name:'" + cc.Name + "', y: " + totalCompiledbutdocumentpendingcount + ",events:{click: function(e) {" +
                           //" fpopulateddepartmentdata('All','Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','" + cc.Id + "','Statutory','DeptFunctionBarChart','0'," + AuthenticationHelper.UserID + ",'" + approver + "','DHCBDP_summary')" +
                           //  "}}},";
                            //}

                        }
                    }
                    perdeptChartCatzomato1 += "]";
                    //perdeptChartCatzomato = perdeptChartCatzomato1.Remove(13, 1);
                    if (perdeptChartCatzomato1.Length > 15)
                    {
                        perdeptChartCatzomato = perdeptChartCatzomato1.Remove(13, 1);
                    }
                    else
                    {
                        perdeptChartCatzomato = perdeptChartCatzomato1;
                    }
                    //perFunctionNC += "],},";
                    perFunctionADD += "],},";
                    perFunctionNCP += "],},";
                    perFunctionIT += "],},";
                    perFunctionOD += "],},";
                    perFunctionPFR += "],},";
                    perFunctionIP += "],},";
                    perFunctionREJ += "],},";
                    perFunctionNA += "],},";
                    //perFunctionCBDP += "],},";
                    perdeptChartzomato = perFunctionNCP + "" + perFunctionADD + "" +  perFunctionIT + "" + perFunctionOD + "" + perFunctionPFR + "" + perFunctionIP + "" + perFunctionREJ + "" + perFunctionNA ;
                    perdeptChartzomato += "]";
                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "R")
                        {
                           // perFunctionChartDEPT = tempperFunctionChart;
                            perdeptChartzomato = tempperFunctionChart;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindLocationCount2(List<SP_GetManagementCompliancesSummary_Result> masterquery, bool IsPenaltyVisible, bool isapp2)
        {
            try
            {
                int cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                // STT Change- Add Status
                string customer = ConfigurationManager.AppSettings["NotCompliedCustID"].ToString();
                List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                if (PenaltyNotDisplayCustomerList.Count > 0)
                {
                    foreach (string PList in PenaltyNotDisplayCustomerList)
                    {
                        if (PList == cuid.ToString())
                        {
                            IsNotCompiled = true;
                            break;
                        }
                    }
                }

                //Zomato Not Complited Pie Chart
                graphshowhide = 1;
               
                string customer1 = ConfigurationManager.AppSettings["NotcomplitedPieChartGraphDisplay_Zomato"].ToString();
                List<string> NotcomplitedPiaChartGraphDisplayCustomerList = customer1.Split(',').ToList();
                if (NotcomplitedPiaChartGraphDisplayCustomerList.Count > 0)
                {
                    foreach (string PList in NotcomplitedPiaChartGraphDisplayCustomerList)
                    {
                        if (PList == cuid.ToString())
                        {
                          
                            graphshowhide = 0;
                        }
                    }
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (ddlStatus.SelectedItem.Text == "Statutory" || ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                    {
                        #region Statutory
                        IsSatutoryInternal = "Statutory";
                        List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                        if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                        {
                            MasterManagementCompliancesSummaryQuery = (from row in masterquery
                                                                       join row1 in entities.DepartmentMappings
                                                                       on row.DepartmentID equals row1.DepartmentID
                                                                       select row).ToList();
                            if (IsNotCompiled == true)
                            {
                                GetManagementCompliancesSummaryDEPTAddedNotCompliedStatus(cuid, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, isapp2, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                            }
                            else
                            {
                                if (graphshowhide == 0)
                                {
                                    GetManagementCompliancesSummaryDEPTZomato(cuid, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, isapp2, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                                }
                                else
                                {
                                    GetManagementCompliancesSummaryDEPT(cuid, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, isapp2, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                                }
                            }
                        }
                        else
                        {

                            MasterManagementCompliancesSummaryQuery = (from row in masterquery
                                                                       join row1 in entities.DepartmentMappings
                                                                       on row.DepartmentID equals row1.DepartmentID
                                                                       select row).ToList();
                            if (IsNotCompiled == true)
                            {
                                GetManagementCompliancesSummaryDEPTAddedNotCompliedStatus(cuid, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, isapp2, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                            }
                            else
                            {
                                if (graphshowhide == 0)
                                {
                                    GetManagementCompliancesSummaryDEPTZomato(cuid, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, isapp2, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                                }
                                else
                                {
                                    GetManagementCompliancesSummaryDEPT(cuid, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, isapp2, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));

                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Internal
                        perPenaltyStatusPieChart = string.Empty;
                        divPenalty.InnerText = string.Format("{0:#,###0}", 0);
                        IsSatutoryInternal = "Internal";
                        if (AuthenticationHelper.Role.Equals("MGMT") || AuthenticationHelper.ComplianceProductType == 3)
                        {
                            IsApprover = false;
                        }
                        else
                        {
                            var GetApproverInternal = (from row in entities.InternalComplianceAssignments
                                                       where row.RoleID == 6
                                                       && row.UserID == AuthenticationHelper.UserID
                                                       select row).ToList();
                            if (GetApproverInternal.Count > 0)
                            {
                                IsApprover = true;
                           
                            }
                            else
                            {
                                IsApprover = false;
                              
                            }
                        }

                        if (IsNotCompiled == true)
                        {
                            if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                            {
                                GetManagementInternalCompliancesSummaryDEPTAddedNotCompliedStatus(cuid, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApprover);
                            }
                            else
                            {
                                GetManagementInternalCompliancesSummaryDEPTAddedNotCompliedStatus(cuid, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApprover);
                            }
                        }
                        else
                        {
                            if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                            {

                                GetManagementInternalCompliancesSummaryDEPT(cuid, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApprover);

                            }
                            else
                            {

                                GetManagementInternalCompliancesSummaryDEPT(cuid, Branchlist, -1, "T", FromFinancialYearSummery, ToFinancialYearSummery, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text), IsApprover);
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion
        
        #region Top DropDown Selected Index Change

        private void BindUserColors()
        {
            try
            {
                var Cmd = Business.ComplianceManagement.Getcolor(AuthenticationHelper.UserID);
                if (Cmd != null)
                {
                    highcolor.Value = Cmd.High;
                    mediumcolor.Value = Cmd.Medium;
                    lowcolor.Value = Cmd.Low;
                    criticalcolor.Value = Cmd.Critical;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        protected void btnTopSearch_Click(object sender, EventArgs e)
        {
            try
            {
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    
                    if (tvFilterLocation.SelectedValue != "-1" && tvFilterLocation.SelectedValue != "")
                    {
                        Branchlist.Clear();
                        GetAllHierarchy(cuid, Convert.ToInt32(tvFilterLocation.SelectedValue));
                        BID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                        Branchlist.ToList();
                    }
                    else
                    {
                        Branchlist.Clear();
                        BindLocationFilter();
                    }
                    ddlperiod_SelectedIndexChanged(sender, e);
                    if (ddlStatus.SelectedItem.Text == "Statutory" || ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                    {
                        if (Session["User_comp_Roles"] != null)
                        {
                            roles = Session["User_comp_Roles"] as List<int>;
                        }
                        else
                        {
                            roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                            Session["User_comp_Roles"] = roles;
                        }


                        if (AuthenticationHelper.Role.Equals("MGMT") || AuthenticationHelper.ComplianceProductType == 3)
                        {
                            IsApprover = false;
                        }
                        else
                        {
                            if (roles.Contains(6))
                            {
                                IsApprover = true;
                            }
                            else
                            {
                                IsApprover = false;
                            }
                        }
                    }
                    else
                    {
                        if (AuthenticationHelper.Role.Equals("MGMT") || AuthenticationHelper.ComplianceProductType == 3)
                        {
                            IsApprover = false;
                        }
                        else
                        {
                            var GetApproverInternal = (from row in entities.InternalComplianceAssignments
                                                       where row.RoleID == 6
                                                       && row.UserID == AuthenticationHelper.UserID
                                                       select row).ToList();
                            if (GetApproverInternal.Count > 0)
                            {
                                IsApprover = true;                              
                            }
                            else
                            {
                                IsApprover = false;                         
                            }
                        }
                    }
                    customizedid = CustomerManagement.GetCustomizedCustomerid(Convert.ToInt32(AuthenticationHelper.CustomerID), "MGMT_Total_Unique");
                    BindLocationCount(cuid, IsPenaltyVisible, IsApprover);
                    BindGradingReportSummaryTree(IsApprover);
                    DisplayGradingChange = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "ShowGradingchange");
                    UpdatePanel1.Update();
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterFunction", "$(\"#divFilterLocationFunction\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterRisk", "$(\"#divFilterLocationRisk\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                string cashTimeval = string.Empty;
                string cashstatutoryval = string.Empty;
                string cashinternalval = string.Empty;
                if (objlocal == "Local")
                {
                    cashTimeval = "MCA_CHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MS_PSD" + AuthenticationHelper.UserID;
                    cashinternalval = "MI_PSD" + AuthenticationHelper.UserID;
                }
                else
                {
                    cashTimeval = "MCACHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MSPSD" + AuthenticationHelper.UserID;
                    cashinternalval = "MIPSD" + AuthenticationHelper.UserID;
                }               
                try
                {
                    StackExchangeRedisExtensions.Remove(cashstatutoryval);
                    StackExchangeRedisExtensions.Remove(cashinternalval);
                    StackExchangeRedisExtensions.Remove(cashTimeval);
                }
                catch (Exception)
                {
                    LogLibrary.WriteErrorLog("Remove btnRefresh_Click Error exception :" + cashstatutoryval);
                    StackExchangeRedisExtensions.Remove(cashstatutoryval);
                    StackExchangeRedisExtensions.Remove(cashinternalval);
                    StackExchangeRedisExtensions.Remove(cashTimeval);
                }
                int cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                string customer = ConfigurationManager.AppSettings["PenaltynotDisplayCustID"].ToString();
                List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                if (PenaltyNotDisplayCustomerList.Count > 0)
                {
                    foreach (string PList in PenaltyNotDisplayCustomerList)
                    {
                        if (PList == cuid.ToString())
                        {
                            IsPenaltyVisible = false;
                        }
                    }
                }
                ddlperiod_SelectedIndexChanged(sender, e);

                if (ddlStatus.SelectedItem.Text == "Statutory" || ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                {
                    if (Session["User_comp_Roles"] != null)
                    {
                        roles = Session["User_comp_Roles"] as List<int>;
                    }
                    else
                    {
                        roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                        Session["User_comp_Roles"] = roles;
                    }


                    if (AuthenticationHelper.Role.Equals("MGMT") || AuthenticationHelper.ComplianceProductType == 3)
                    {
                        IsApprover = false;
                    }
                    else
                    {
                        if (roles.Contains(6))
                        {
                            IsApprover = true;
                        }
                        else
                        {
                            IsApprover = false;
                        }
                    }
                }
                else
                {
                    if (AuthenticationHelper.Role.Equals("MGMT") || AuthenticationHelper.ComplianceProductType == 3)
                    {
                        IsApprover = false;
                    }
                    else
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var GetApproverInternal = (from row in entities.InternalComplianceAssignments
                                                       where row.RoleID == 6
                                                       && row.UserID == AuthenticationHelper.UserID
                                                       select row).ToList();
                            if (GetApproverInternal.Count > 0)
                            {
                                IsApprover = true;
                            }
                            else
                            {
                                IsApprover = false;
                            }
                        }
                    }
                }

                BindLocationCount(cuid, IsPenaltyVisible, IsApprover);
            
                if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                {
                                                
                    DateTime ss = (DateTime)StackExchangeRedisExtensions.Get(cashTimeval);
                    TimeSpan span = DateTime.Now - ss;
                    if (span.Hours == 0)
                    {                        
                        Label1.Text = "Last updated within the last hour";
                    }
                    else
                    {
                        Label1.Text = "Last updated " + span.Hours + " hour ago";
                    }
                }
                else
                {
                    Label1.Text = "Last updated within the last hour";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region  Function Report

        private void BindLocationFilter()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                    tvFilterLocation.Nodes.Clear();
                    string isstatutoryinternal = "";
                    if (ddlStatus.SelectedValue == "0")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (ddlStatus.SelectedValue == "1")
                    {
                        isstatutoryinternal = "I";
                    }
                    var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                    TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                        tvFilterLocation.Nodes.Add(node);

                    }

                    tvFilterLocation.CollapseAll();
                    tvFilterLocation_SelectedNodeChanged(null, null);
                    BindLocationFilterPenalty(bracnhes, LocationList);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
      
        private void BindLocationFilterPenalty(List<NameValueHierarchy> bracnhes, List<int> LocationList)
        {
            try
            {
                tvFilterLocationPenalty.Nodes.Clear();
                TreeNode node = new TreeNode("", "-1");
                node.Selected = true;
                tvFilterLocationPenalty.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocationPenalty.Nodes.Add(node);
                }

                tvFilterLocationPenalty.CollapseAll();
                tvFilterLocationPenalty_SelectedNodeChanged(null, null);
                BindLocationFilterGraddingReport(bracnhes, LocationList);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
      

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

       
        public DateTime? GetDate(string date)
        {
            if (date != null && date != "")
            {
                string date1 = "";
                if (date.Contains("/"))
                {
                    date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains("-"))
                {
                    date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains(" "))
                {
                    date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
                }
                return Convert.ToDateTime(date1);
            }
            else
            {
                return null;
            }

        }

        #endregion

        #region Internal
        

        public void GetManagementInternalCompliancesSummary(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<SP_GetManagementInternalCompliancesSummary_Result> MasterQuery, DateTime? FromDate = null, DateTime? EndDateF = null, bool approver = false, int userId = -1)
        {
            try
            {
                string tempperFunctionChart = perFunctionChart;
                string tempperFunctionPieChart = perFunctionPieChart;
                string tempperRiskChart = perRiskChart;
                string tempperFunctionPieChartNotCompleted = perFunctionPieNotcompleted;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perFunctionChart = string.Empty;
                        perFunctionPieChart = string.Empty;
                        perRiskChart = string.Empty;
                        perFunctionPieNotcompleted = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        perFunctionChart = string.Empty;
                    }
                    else if (clickflag == "R")
                    {
                        perRiskChart = string.Empty;
                    }
                }
                else
                {
                    perFunctionChart = string.Empty;
                    perFunctionPieChart = string.Empty;
                    perRiskChart = string.Empty;
                    perFunctionPieNotcompleted = string.Empty;
                }
                bool auditor = false;
                if (AuthenticationHelper.Role == "AUDT")
                {
                    auditor = true;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementInternalCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                    if (Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    List<SP_GetManagementInternalCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementInternalCompliancesSummary_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate
                                             && row.ScheduledOn <= EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate && row.ScheduledOn <= EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.ScheduledOn <= EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                 || row.ScheduledOn <= EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                         || row.ScheduledOn <= FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.ScheduledOn >= FIFromDate
                                                         && row.ScheduledOn <= FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn <= FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    #region PieChart internal
                    long totalPieCompletedcount = 0;
                    long totalPieNotCompletedCount = 0;
                    long totalPieAfterDueDatecount = 0;
                    long totalPieNotApplicablecount = 0;
                    long totalPieCompliedbutDocPendingcount = 0;

                    long totalPieCompletedHIGH = 0;
                    long totalPieCompletedMEDIUM = 0;
                    long totalPieCompletedLOW = 0;
                    long totalPieCompletedCRITICAL = 0;

                    long totalPieAfterDueDateHIGH = 0;
                    long totalPieAfterDueDateMEDIUM = 0;
                    long totalPieAfterDueDateLOW = 0;
                    long totalPieAfterDueDateCRITICAL = 0;

                    long totalPieNotCompletedHIGH = 0;
                    long totalPieNotCompletedMEDIUM = 0;
                    long totalPieNotCompletedLOW = 0;
                    long totalPieNotCompletedCRITICAL = 0;

                    long totalPieNotApplicableHIGH = 0;
                    long totalPieNotApplicableMEDIUM = 0;
                    long totalPieNotApplicableLOW = 0;
                    long totalPieNotApplicableCRITICAL = 0;


                    long totalPieCompliedbutDocPendingHIGH = 0;
                    long totalPieCompliedbutDocPendingMEDIUM = 0;
                    long totalPieCompliedbutDocPendingLOW = 0;
                    long totalPieCompliedbutDocPendingCRITICAL = 0;

                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("High", typeof(long));
                    table.Columns.Add("Medium", typeof(long));
                    table.Columns.Add("Low", typeof(long));
                    table.Columns.Add("Critical", typeof(long));

                    string listCategoryId = "";
                    List<InternalCompliancesCategory> CatagoryList = new List<InternalCompliancesCategory>();

                    if (approver == true)
                    {
                        CatagoryList = ComplianceCategoryManagement.GetFunctionDetailsInternal(AuthenticationHelper.UserID, Branchlist, CustomerbranchID, true);
                    }
                    else
                    {
                        CatagoryList = ComplianceCategoryManagement.GetFunctionDetailsInternal(AuthenticationHelper.UserID, Branchlist, CustomerbranchID);
                    }
                    string perFunctionHIGH = "";
                    string perFunctionMEDIUM = "";
                    string perFunctionLOW = "";
                    string perFunctionCRITICAL = "";

                    string perFunctiondrilldown = "";
                    perFunctionCRITICAL = "series: [ {name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";
                    perFunctionHIGH = "{name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOW = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";


                    perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#e6e6e6'},select:{stroke: '#039',fill:'#e6e6e6'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                    //High

                    foreach (InternalCompliancesCategory cc in CatagoryList)
                    {

                        listCategoryId += ',' + cc.ID.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {

                            //High
                            long AfterDueDatecountHigh;
                            long CompletedCountHigh;
                            long NotCompletedcountHigh;
                            long NotApplicableCountHigh;
                            long CompliedbutDocPendingHigh;

                            if (ColoumnNames[2] == "High")
                            {
                                perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                            }
                           
                            //AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            //CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.ID).Count();

                            AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.ID).Count();

                            NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotApplicableCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompliedbutDocPendingHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.ID).Count();


                            perFunctiondrilldown += "{ id: 'high' + '" + cc.Name + "', " +
                                "name: 'High'," +
                                " color: perFunctionChartColorScheme.high," +
                                " data: [ ['Closed Timely', " + CompletedCountHigh + "], " +
                                " ['Closed Delayed', " + AfterDueDatecountHigh + "]," +
                                " ['Not completed', " + NotCompletedcountHigh + "]," +
                                " ['Not Applicable', " + NotApplicableCountHigh + "]," +
                                //" ['Complied But Document Pending', " + CompliedbutDocPendingHigh + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + cc.ID + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FH_summary')" +
                                "}}},";




                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountHigh;
                            totalPieNotCompletedCount += NotCompletedcountHigh;
                            totalPieAfterDueDatecount += AfterDueDatecountHigh;
                            totalPieNotApplicablecount += NotApplicableCountHigh;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingHigh;

                            //pie drill down
                            totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                            totalPieCompletedHIGH += CompletedCountHigh;
                            totalPieNotCompletedHIGH += NotCompletedcountHigh;
                            totalPieNotApplicableHIGH += NotApplicableCountHigh;
                            totalPieCompliedbutDocPendingHIGH += CompliedbutDocPendingHigh;

                            //Medium
                            long AfterDueDatecountMedium;
                            long CompletedCountMedium;
                            long NotCompletedcountMedium;
                            long NotApplicableCountMedium;
                            long CompliedbutDocPendingMedium;

                            if (ColoumnNames[3] == "Medium")
                            {
                                perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                            }
                           

                            AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.ID).Count();
                            //AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            //CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 ) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotApplicableCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompliedbutDocPendingMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.ID).Count();


                          
                            perFunctiondrilldown += "{ " +
                                " id: 'mid' + '" + cc.Name + "', " +
                                " name: 'Medium'," +
                                " color: perFunctionChartColorScheme.medium, " +
                                " data: [['Closed Timely', " + CompletedCountMedium + "]," +
                                " ['Closed Delayed', " + AfterDueDatecountMedium + "], " +
                                " ['Not completed', " + NotCompletedcountMedium + "], " +
                                 " ['Not Applicable', " + NotApplicableCountMedium + "]," +
                                //" ['Complied But Document Pending', " + CompliedbutDocPendingMedium + "]," +
                                " ],events: { " +
                                 " click: function(e){" +
                                " fpopulateddata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + cc.ID + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FM_summary')" +
                                "}}},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountMedium;
                            totalPieNotCompletedCount += NotCompletedcountMedium;
                            totalPieAfterDueDatecount += AfterDueDatecountMedium;
                            totalPieNotApplicablecount += NotApplicableCountMedium;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingMedium;
                            //pie drill down
                            totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                            totalPieCompletedMEDIUM += CompletedCountMedium;
                            totalPieNotCompletedMEDIUM += NotCompletedcountMedium;
                            totalPieNotApplicableMEDIUM += NotApplicableCountMedium;
                            totalPieCompliedbutDocPendingMEDIUM += CompliedbutDocPendingMedium;


                            //Low
                            long AfterDueDatecountLow;
                            long CompletedCountLow;
                            long NotCompletedcountLow;
                            long NotApplicableCountLow;
                            long CompliedbutDocPendingLow;

                            if (ColoumnNames[4] == "Low")
                            {
                                perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                            }

                            AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.ID).Count();
                            //AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            //CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 ) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotApplicableCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 15 ) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompliedbutDocPendingLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.ID).Count();


                            perFunctiondrilldown += "{ id: 'low' + '" + cc.Name + "'," +
                                " name: 'Low'," +
                                " color: perFunctionChartColorScheme.low," +
                                " data: [['Closed Timely', " + CompletedCountLow + "]," +
                                " ['Closed Delayed'," + AfterDueDatecountLow + "]," +
                                " ['Not completed'," + NotCompletedcountLow + "]," +
                                " ['Not Applicable', " + NotApplicableCountLow + "]," +
                                //" ['Complied But Document Pending', " + CompliedbutDocPendingLow + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + cc.ID + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FL_summary')" +
                                "}}},";



                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountLow;
                            totalPieNotCompletedCount += NotCompletedcountLow;
                            totalPieAfterDueDatecount += AfterDueDatecountLow;
                            totalPieNotApplicablecount += NotApplicableCountLow;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingLow;

                            //pie drill down
                            totalPieAfterDueDateLOW += AfterDueDatecountLow;
                            totalPieCompletedLOW += CompletedCountLow;
                            totalPieNotCompletedLOW += NotCompletedcountLow;
                            totalPieNotApplicableLOW += NotApplicableCountLow;
                            totalPieCompliedbutDocPendingLOW += CompliedbutDocPendingLow;

                            //Critical
                            long AfterDueDatecountCritical;
                            long CompletedCountCritical;
                            long NotCompletedcountCritical;
                            long NotApplicableCountCritical;
                            long CompliedbutDocPendingCritical;

                            if (ColoumnNames[5] == "Critical")
                            {
                                perFunctionCRITICAL += "{ name:'" + cc.Name + "', y: " + criticalcount + ",drilldown: 'critical' + '" + cc.Name + "',},";
                            }
                            
                            AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.ID).Count();
                            //AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            //CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotApplicableCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompliedbutDocPendingCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.ID).Count();

                            perFunctiondrilldown += "{ id: 'critical' + '" + cc.Name + "'," +
                                " name: 'Critical'," +
                                " color: perFunctionChartColorScheme.critical," +
                                " data: [['Closed Timely', " + CompletedCountCritical + "]," +
                                " ['Closed Delayed'," + AfterDueDatecountCritical + "]," +
                                " ['Not completed'," + NotCompletedcountCritical + "]," +
                                 " ['Not Applicable', " + NotApplicableCountCritical + "]," +
                                //" ['Complied But Document Pending', " + CompliedbutDocPendingCritical + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + cc.ID + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FC_summary')" +
                                "}}},";



                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountCritical;
                            totalPieNotCompletedCount += NotCompletedcountCritical;
                            totalPieAfterDueDatecount += AfterDueDatecountCritical;
                            totalPieNotApplicablecount += NotApplicableCountCritical;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingCritical;
                            //pie drill down
                            totalPieAfterDueDateCRITICAL += AfterDueDatecountCritical;
                            totalPieCompletedCRITICAL += CompletedCountCritical;
                            totalPieNotCompletedCRITICAL += NotCompletedcountCritical;
                            totalPieNotApplicableCRITICAL += NotApplicableCountCritical;
                            totalPieCompliedbutDocPendingCRITICAL += CompliedbutDocPendingCritical;



                        }
                    }
                    perFunctionCRITICAL += "],},";
                    perFunctionHIGH += "],},";
                    perFunctionMEDIUM += "],},";
                    perFunctionLOW += "],},],";

                    perFunctiondrilldown += "],},";
                    perFunctionChart = perFunctionCRITICAL + "" + perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + perFunctiondrilldown;
                    #endregion

                    #region  PieChartNotCompleted

                    long totalPieOverduecount = 0;
                    long totalPiePendingForReviewCount = 0;
                    long totalPieRejectedcount = 0;
                    long totalPieInProgresscount = 0;


                    long totalPieOverdueHIGH = 0;
                    long totalPieOverdueMEDIUM = 0;
                    long totalPieOverdueLOW = 0;
                    long totalPieOverdueCRITICAL = 0;

                    long totalPiePendingForReviewHIGH = 0;
                    long totalPiePendingForReviewMEDIUM = 0;
                    long totalPiePendingForReviewLOW = 0;
                    long totalPiePendingForReviewCRITICAL = 0;

                    long totalPieRejectedHIGH = 0;
                    long totalPieRejectedMEDIUM = 0;
                    long totalPieRejectedLOW = 0;
                    long totalPieRejectedCRITICAL = 0;

                    long totalPieInProgressHIGH = 0;
                    long totalPieInProgressMEDIUM = 0;
                    long totalPieInProgressLOW = 0;
                    long totalPieInProgressCRITICAL = 0;



                    foreach (InternalCompliancesCategory cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.ID.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {
                            //High
                            long OverduecountHigh;
                            long PendingForReviewCountHigh;
                            long RejectedcountHigh;
                            long InProgresscountHigh;

                            OverduecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.ID).Count();
                            PendingForReviewCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            RejectedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.ID).Count();
                            InProgresscountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID).Count();


                            //per Status pie Chart
                            totalPieOverduecount += OverduecountHigh;
                            totalPiePendingForReviewCount += PendingForReviewCountHigh;
                            totalPieRejectedcount += RejectedcountHigh;
                            totalPieInProgresscount += InProgresscountHigh;
                            //pie drill down
                            totalPieOverdueHIGH += OverduecountHigh;
                            totalPiePendingForReviewHIGH += PendingForReviewCountHigh;
                            totalPieRejectedHIGH += RejectedcountHigh;
                            totalPieInProgressHIGH += InProgresscountHigh;


                            //Medium
                            long OverduecountMedium;
                            long PendingForReviewCountMedium;
                            long RejectedcountMedium;
                            long InProgresscountMedium;

                            OverduecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.ID).Count();
                            PendingForReviewCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            RejectedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 6 ) && entry.ComplianceCategoryId == cc.ID).Count();
                            InProgresscountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID).Count();

                            //per Status pie Chart
                            totalPieOverduecount += OverduecountMedium;
                            totalPiePendingForReviewCount += PendingForReviewCountMedium;
                            totalPieRejectedcount += RejectedcountMedium;
                            totalPieInProgresscount += InProgresscountMedium;

                            //pie drill down 
                            totalPieOverdueMEDIUM += OverduecountMedium;
                            totalPiePendingForReviewMEDIUM += PendingForReviewCountMedium;
                            totalPieRejectedMEDIUM += RejectedcountMedium;
                            totalPieInProgressMEDIUM += InProgresscountMedium;

                            //Low
                            long OverduecountLow;
                            long PendingForReviewCountLow;
                            long RejectedcountLow;
                            long InProgresscountLow;

                            OverduecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.ID).Count();
                            PendingForReviewCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            RejectedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.ID).Count();
                            InProgresscountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID).Count();


                            //per Status pie Chart
                            totalPieOverduecount += OverduecountLow;
                            totalPiePendingForReviewCount += PendingForReviewCountLow;
                            totalPieRejectedcount += RejectedcountLow;
                            totalPieInProgresscount += InProgresscountLow;

                            //pie drill down 
                            totalPieOverdueLOW += OverduecountLow;
                            totalPiePendingForReviewLOW += PendingForReviewCountLow;
                            totalPieRejectedLOW += RejectedcountLow;
                            totalPieInProgressLOW += InProgresscountLow;


                            //CRITICAL 
                            long OverduecountCritical;
                            long PendingForReviewCountCritical;
                            long RejectedcountCritical;
                            long InProgresscountCritical;


                            OverduecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.ID).Count();
                            PendingForReviewCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            RejectedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 6 ) && entry.ComplianceCategoryId == cc.ID).Count();
                            InProgresscountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID).Count();

                            //per Status pie Chart
                            totalPieOverduecount += OverduecountCritical;
                            totalPiePendingForReviewCount += PendingForReviewCountCritical;
                            totalPieRejectedcount += RejectedcountCritical;
                            totalPieInProgresscount += InProgresscountCritical;

                            //pie drill down 
                            totalPieOverdueCRITICAL += OverduecountCritical;
                            totalPiePendingForReviewCRITICAL += PendingForReviewCountCritical;
                            totalPieRejectedCRITICAL += RejectedcountCritical;
                            totalPieInProgressCRITICAL += InProgresscountCritical;

                        }

                    }


                    #endregion

                    #region  perFunctionPieChart internal
                    perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Closed Timely',y: " + totalPieCompletedcount + ",color: perStatusChartColorScheme.closetimely,drilldown: 'inTime',},{name: 'Closed Delayed',y: " + totalPieAfterDueDatecount + ",color: perStatusChartColorScheme.closedelay,drilldown: 'afterDueDate',},{name: 'Not Completed',y: " + totalPieNotCompletedCount + ",color: perStatusChartColorScheme.high,drilldown: 'notCompleted',},{name: 'Not Applicable',y: " + totalPieNotApplicablecount + ",color: perStatusChartColorScheme.NotApplicable,drilldown: 'notApplicable',}],}],";
                    perFunctionPieChart += "drilldown:{activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, " +
                        " series: [{id: 'inTime',name: 'In Time',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        // " // In time - Critical" +
                        " fpopulateddata(e.point.name,'Closed Timely'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INC_Psummary')" +
                         "},},},{name: 'High',y: " + totalPieCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                        // " // In time - High " +
                        // " e.point.name;"+
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INH_Psummary')" +
                        "},},},{name: 'Medium',y: " + totalPieCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        // " // In time - Medium" +
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INM_Psummary')" +
                        "},},},{name: 'Low',y: " + totalPieCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        // " // In time - Low" +
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INL_Psummary')" +



                        " },},}],},{id: 'afterDueDate',name: 'Closed Delayed',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieAfterDueDateCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        // " // After Due Date - Critical"+
                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFC_Psummary')" +
                         " },},},{name: 'High',y: " + totalPieAfterDueDateHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                         // " // After Due Date - High "+
                         " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFH_Psummary')" +
                        " },},},{name: 'Medium',y: " + totalPieAfterDueDateMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        //  " // After Due Date - Medium "+
                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFM_Psummary')" +
                        " },},},{name: 'Low',y: " + totalPieAfterDueDateLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        // " // After Due Date - Low"+
                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFL_Psummary')" +



                        " },},}],},{id: 'notCompleted',name: 'Not Completed',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieNotCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                       // " // Not Completed - Critical"+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                        " },},},{name: 'High',y: " + totalPieNotCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                       // " // Not Completed - High "+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                        " },},},{name: 'Medium',y: " + totalPieNotCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                       // " // Not Completed - Medium "+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                        " },},},{name: 'Low',y: " + totalPieNotCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                       // " // Not Completed - Low"+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +

                         " },},}],}," +
                        " {id: 'notApplicable',name: 'Not Applicable',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieNotApplicableCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        // " // Not Applicable - Critical
                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAC_Psummary')" +
                        " },},},{name: 'High',y: " + totalPieNotApplicableHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                        // " // Not Applicable - High
                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAH_Psummary')" +
                        " },},},{name: 'Medium',y: " + totalPieNotApplicableMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        // " // Not Applicable - Medium
                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAM_Psummary')" +
                        " },},},{name: 'Low',y: " + totalPieNotApplicableLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        // " // Not Applicable - Low
                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAL_Psummary')" +

                        //    " },},}],}," +
                        //" {id: 'compliedbutdocumentpending',name: 'Complied But Document Pending',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieCompliedbutDocPendingCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        //// " // Complied But Document Pending - Critical
                        //" fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPC_Psummary')" +
                        //" },},},{name: 'High',y: " + totalPieCompliedbutDocPendingHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                        //// " // Complied But Document Pending - High
                        //" fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPH_Psummary')" +
                        //" },},},{name: 'Medium',y: " + totalPieCompliedbutDocPendingMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        //// " // Complied But Document Pending - Medium
                        //" fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPM_Psummary')" +
                        //" },},},{name: 'Low',y: " + totalPieCompliedbutDocPendingLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        //// " // Complied But Document Pending - Low
                        //" fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPL_Psummary')" +


                       "},},}],}],},";

                    #endregion

                    #region perFunctionPieNotcompleted internal Zomato
                    perFunctionPieNotcompleted = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Overdue',y: " + totalPieOverduecount + ",color: perStatusChartColorScheme.Overdue,drilldown: 'overdue',},{name: 'Pending For Review',y: " + totalPiePendingForReviewCount + ",color: perStatusChartColorScheme.Pendingforreview,drilldown: 'pendingForReview',},{name: 'InProgress',y: " + totalPieInProgresscount + ",color: perStatusChartColorScheme.InProgress,drilldown: 'inProgress',},{name: 'Rejected',y: " + totalPieRejectedcount + ",color: perStatusChartColorScheme.Rejected,drilldown: 'rejected',}],}],";

                    perFunctionPieNotcompleted += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                       " series: [" +

                                        " {id: 'overdue',name: 'Overdue',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPieOverdueCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // In time - Critical                               
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        "},},},{name: 'High',y: " + totalPieOverdueHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // In time - High                   
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieOverdueMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // In time - Medium                                
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        "},},},{name: 'Low',y: " + totalPieOverdueLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // In time - Low                               
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'pendingForReview',name: 'Pending For Review',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPiePendingForReviewCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // After Due Date - Critical
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPiePendingForReviewHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // After Due Date - High
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPiePendingForReviewMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        //  " // After Due Date - Medium
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPiePendingForReviewLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // After Due Date - Low
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'inProgress',name: 'In Progress',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieInProgressCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Completed - Critical
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieInProgressHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Completed - High
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieInProgressMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Completed - Medium
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieInProgressLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Completed - Low
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +


                                         " },},}],}," +
                                        " {id: 'rejected',name: 'Rejected',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieRejectedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Rejected - Critical
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieRejectedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Rejected - High
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieRejectedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Rejected - Medium
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieRejectedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Rejected - Low
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +


                                        " },},}],}],},";
                    #endregion

                    #region perRiskChart internal

                    perRiskChart = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +
                  // Not Completed - Critical

                  "y: " + totalPieNotCompletedCRITICAL + ",events:{click: function(e) {" +
                    " fpopulateddata('Critical','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCC_Rsummary')" +

                    "}}},{  " +

                  // Not Completed - High
                  "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                   " fpopulateddata('High','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCH_Rsummary')" +

                  "}}},{" +

                  // Not Completed - Medium
                  " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                  " fpopulateddata('Medium','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCM_Rsummary')" +

                  "}}},{  " +

                  // Not Completed - Low

                  "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                    " fpopulateddata('Low','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCL_Rsummary')" +


                  "}}}]},{name: 'Closed Delayed',color: perRiskStackedColumnChartColorScheme.closedelay,data: [{" +

                  // After Due Date - Critical
                  "y: " + totalPieAfterDueDateCRITICAL + ",events:{click: function(e) {" +
                  " fpopulateddata('Critical','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFC_Rsummary')" +

                   "}}},{   " +

                  // After Due Date - High

                  "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                  " fpopulateddata('High','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFH_Rsummary')" +

                  "}}},{   " +

                  // After Due Date - Medium
                  "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                  " fpopulateddata('Medium','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFM_Rsummary')" +

                  "}}},{   " +
                  // After Due Date - Low
                  "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                  " fpopulateddata('Low','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFL_Rsummary')" +


                  "}}}]},{name: 'In Time',color: perRiskStackedColumnChartColorScheme.low,data: [{" +
                  // In Time - Critical
                  "y: " + totalPieCompletedCRITICAL + ",events:{click: function(e) {" +
                  " fpopulateddata('Critical','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INC_Rsummary')" +

                    "}}},{  " +

                  // In Time - High
                  "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                  " fpopulateddata('High','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INH_Rsummary')" +

                  "}}},{  " +
                  // In Time - Medium
                  "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                  " fpopulateddata('Medium','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INM_Rsummary')" +

                  "}}},{  " +
                  // In Time - Low
                  "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                  " fpopulateddata('Low','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INL_Rsummary')" +

                  "}}}]}]";

                    #endregion

                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "F")
                        {
                            perRiskChart = tempperRiskChart;
                        }
                        else if (clickflag == "R")
                        {
                            perFunctionChart = tempperFunctionChart;
                            perFunctionPieChart = tempperFunctionPieChart;
                        }
                    }
                
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetManagementInternalCompliancesSummaryZomato(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<SP_GetManagementInternalCompliancesSummary_Result> MasterQuery, DateTime? FromDate = null, DateTime? EndDateF = null, bool approver = false, int userId = -1)
        {
            try
            {
                string tempperFunctionChart = perFunctionChart;
                string tempperFunctionPieChart = perFunctionPieChart;
                string tempperRiskChart = perRiskChart;
                string tempperFunctionPieChartNotCompleted = perFunctionPieNotcompleted;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perFunctionChart = string.Empty;
                        perFunctionPieChart = string.Empty;
                        perRiskChart = string.Empty;
                        perFunctionPieNotcompleted = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        perFunctionChart = string.Empty;
                    }
                    else if (clickflag == "R")
                    {
                        perRiskChart = string.Empty;
                    }
                }
                else
                {
                    perFunctionChart = string.Empty;
                    perFunctionPieChart = string.Empty;
                    perRiskChart = string.Empty;
                    perFunctionPieNotcompleted = string.Empty;
                }
                bool auditor = false;
                if (AuthenticationHelper.Role == "AUDT")
                {
                    auditor = true;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementInternalCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                    if (Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    List<SP_GetManagementInternalCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementInternalCompliancesSummary_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate
                                             && row.ScheduledOn <= EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate && row.ScheduledOn <= EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.ScheduledOn <= EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                 || row.ScheduledOn <= EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                         || row.ScheduledOn <= FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.ScheduledOn >= FIFromDate
                                                         && row.ScheduledOn <= FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn <= FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    #region PieChart internal
                    long totalPieCompletedcount = 0;
                    long totalPieNotCompletedCount = 0;
                    long totalPieAfterDueDatecount = 0;
                    long totalPieNotApplicablecount = 0;
                    long totalPieCompliedbutDocPendingcount = 0;

                    long totalPieCompletedHIGH = 0;
                    long totalPieCompletedMEDIUM = 0;
                    long totalPieCompletedLOW = 0;
                    long totalPieCompletedCRITICAL = 0;

                    long totalPieAfterDueDateHIGH = 0;
                    long totalPieAfterDueDateMEDIUM = 0;
                    long totalPieAfterDueDateLOW = 0;
                    long totalPieAfterDueDateCRITICAL = 0;

                    long totalPieNotCompletedHIGH = 0;
                    long totalPieNotCompletedMEDIUM = 0;
                    long totalPieNotCompletedLOW = 0;
                    long totalPieNotCompletedCRITICAL = 0;

                    long totalPieNotApplicableHIGH = 0;
                    long totalPieNotApplicableMEDIUM = 0;
                    long totalPieNotApplicableLOW = 0;
                    long totalPieNotApplicableCRITICAL = 0;


                    long totalPieCompliedbutDocPendingHIGH = 0;
                    long totalPieCompliedbutDocPendingMEDIUM = 0;
                    long totalPieCompliedbutDocPendingLOW = 0;
                    long totalPieCompliedbutDocPendingCRITICAL = 0;

                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("High", typeof(long));
                    table.Columns.Add("Medium", typeof(long));
                    table.Columns.Add("Low", typeof(long));
                    table.Columns.Add("Critical", typeof(long));

                    string listCategoryId = "";
                    List<InternalCompliancesCategory> CatagoryList = new List<InternalCompliancesCategory>();

                    if (approver == true)
                    {
                        CatagoryList = ComplianceCategoryManagement.GetFunctionDetailsInternal(AuthenticationHelper.UserID, Branchlist, CustomerbranchID, true);
                    }
                    else
                    {
                        CatagoryList = ComplianceCategoryManagement.GetFunctionDetailsInternal(AuthenticationHelper.UserID, Branchlist, CustomerbranchID);
                    }
                    string perFunctionHIGH = "";
                    string perFunctionMEDIUM = "";
                    string perFunctionLOW = "";
                    string perFunctionCRITICAL = "";

                    string perFunctiondrilldown = "";
                    perFunctionCRITICAL = "series: [ {name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";
                    perFunctionHIGH = "{name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOW = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";


                    perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#e6e6e6'},select:{stroke: '#039',fill:'#e6e6e6'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                    //High

                    foreach (InternalCompliancesCategory cc in CatagoryList)
                    {

                        listCategoryId += ',' + cc.ID.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {

                            //High
                            long AfterDueDatecountHigh;
                            long CompletedCountHigh;
                            long NotCompletedcountHigh;
                            long NotApplicableCountHigh;
                            long CompliedbutDocPendingHigh;

                            if (ColoumnNames[2] == "High")
                            {
                                perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                            }
                         
                            AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.ID).Count();
                            //AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            //CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotApplicableCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompliedbutDocPendingHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.ID).Count();


                            perFunctiondrilldown += "{ id: 'high' + '" + cc.Name + "', " +
                                "name: 'High'," +
                                " color: perFunctionChartColorScheme.high," +
                                " data: [ ['Closed Timely', " + CompletedCountHigh + "], " +
                                " ['Closed Delayed', " + AfterDueDatecountHigh + "]," +
                                " ['Not completed', " + NotCompletedcountHigh + "]," +
                                " ['Not Applicable', " + NotApplicableCountHigh + "]," +
                                " ['Complied But Document Pending', " + CompliedbutDocPendingHigh + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + cc.ID + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FH_summary')" +
                                "}}},";




                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountHigh;
                            totalPieNotCompletedCount += NotCompletedcountHigh;
                            totalPieAfterDueDatecount += AfterDueDatecountHigh;
                            totalPieNotApplicablecount += NotApplicableCountHigh;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingHigh;

                            //pie drill down
                            totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                            totalPieCompletedHIGH += CompletedCountHigh;
                            totalPieNotCompletedHIGH += NotCompletedcountHigh;
                            totalPieNotApplicableHIGH += NotApplicableCountHigh;
                            totalPieCompliedbutDocPendingHIGH += CompliedbutDocPendingHigh;

                            //Medium
                            long AfterDueDatecountMedium;
                            long CompletedCountMedium;
                            long NotCompletedcountMedium;
                            long NotApplicableCountMedium;
                            long CompliedbutDocPendingMedium;

                            if (ColoumnNames[3] == "Medium")
                            {
                                perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                            }
                    

                            AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.ID).Count();

                            //AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            //CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotApplicableCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompliedbutDocPendingMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.ID).Count();



                            perFunctiondrilldown += "{ " +
                                " id: 'mid' + '" + cc.Name + "', " +
                                " name: 'Medium'," +
                                " color: perFunctionChartColorScheme.medium, " +
                                " data: [['Closed Timely', " + CompletedCountMedium + "]," +
                                " ['Closed Delayed', " + AfterDueDatecountMedium + "], " +
                                " ['Not completed', " + NotCompletedcountMedium + "], " +
                                 " ['Not Applicable', " + NotApplicableCountMedium + "]," +
                                " ['Complied But Document Pending', " + CompliedbutDocPendingMedium + "]," +
                                " ],events: { " +
                                 " click: function(e){" +
                                " fpopulateddata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + cc.ID + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FM_summary')" +
                                "}}},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountMedium;
                            totalPieNotCompletedCount += NotCompletedcountMedium;
                            totalPieAfterDueDatecount += AfterDueDatecountMedium;
                            totalPieNotApplicablecount += NotApplicableCountMedium;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingMedium;
                            //pie drill down
                            totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                            totalPieCompletedMEDIUM += CompletedCountMedium;
                            totalPieNotCompletedMEDIUM += NotCompletedcountMedium;
                            totalPieNotApplicableMEDIUM += NotApplicableCountMedium;
                            totalPieCompliedbutDocPendingMEDIUM += CompliedbutDocPendingMedium;


                            //Low
                            long AfterDueDatecountLow;
                            long CompletedCountLow;
                            long NotCompletedcountLow;
                            long NotApplicableCountLow;
                            long CompliedbutDocPendingLow;

                            if (ColoumnNames[4] == "Low")
                            {
                                perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                            }
  

                            AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.ID).Count();
                            //AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            //CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotApplicableCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompliedbutDocPendingLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.ID).Count();


                            perFunctiondrilldown += "{ id: 'low' + '" + cc.Name + "'," +
                                " name: 'Low'," +
                                " color: perFunctionChartColorScheme.low," +
                                " data: [['Closed Timely', " + CompletedCountLow + "]," +
                                " ['Closed Delayed'," + AfterDueDatecountLow + "]," +
                                " ['Not completed'," + NotCompletedcountLow + "]," +
                                " ['Not Applicable', " + NotApplicableCountLow + "]," +
                                " ['Complied But Document Pending', " + CompliedbutDocPendingLow + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + cc.ID + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FL_summary')" +
                                "}}},";



                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountLow;
                            totalPieNotCompletedCount += NotCompletedcountLow;
                            totalPieAfterDueDatecount += AfterDueDatecountLow;
                            totalPieNotApplicablecount += NotApplicableCountLow;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingLow;

                            //pie drill down
                            totalPieAfterDueDateLOW += AfterDueDatecountLow;
                            totalPieCompletedLOW += CompletedCountLow;
                            totalPieNotCompletedLOW += NotCompletedcountLow;
                            totalPieNotApplicableLOW += NotApplicableCountLow;
                            totalPieCompliedbutDocPendingLOW += CompliedbutDocPendingLow;

                            //Critical
                            long AfterDueDatecountCritical;
                            long CompletedCountCritical;
                            long NotCompletedcountCritical;
                            long NotApplicableCountCritical;
                            long CompliedbutDocPendingCritical;

                            if (ColoumnNames[5] == "Critical")
                            {
                                perFunctionCRITICAL += "{ name:'" + cc.Name + "', y: " + criticalcount + ",drilldown: 'critical' + '" + cc.Name + "',},";
                            }
                  

                            AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.ID).Count();
                            //AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            //CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotApplicableCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompliedbutDocPendingCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.ID).Count();

                            perFunctiondrilldown += "{ id: 'critical' + '" + cc.Name + "'," +
                                " name: 'Critical'," +
                                " color: perFunctionChartColorScheme.critical," +
                                " data: [['Closed Timely', " + CompletedCountCritical + "]," +
                                " ['Closed Delayed'," + AfterDueDatecountCritical + "]," +
                                " ['Not completed'," + NotCompletedcountCritical + "]," +
                                 " ['Not Applicable', " + NotApplicableCountCritical + "]," +
                                " ['Complied But Document Pending', " + CompliedbutDocPendingCritical + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + cc.ID + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FC_summary')" +
                                "}}},";



                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountCritical;
                            totalPieNotCompletedCount += NotCompletedcountCritical;
                            totalPieAfterDueDatecount += AfterDueDatecountCritical;
                            totalPieNotApplicablecount += NotApplicableCountCritical;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingCritical;
                            //pie drill down
                            totalPieAfterDueDateCRITICAL += AfterDueDatecountCritical;
                            totalPieCompletedCRITICAL += CompletedCountCritical;
                            totalPieNotCompletedCRITICAL += NotCompletedcountCritical;
                            totalPieNotApplicableCRITICAL += NotApplicableCountCritical;
                            totalPieCompliedbutDocPendingCRITICAL += CompliedbutDocPendingCritical;



                        }
                    }
                    perFunctionCRITICAL += "],},";
                    perFunctionHIGH += "],},";
                    perFunctionMEDIUM += "],},";
                    perFunctionLOW += "],},],";

                    perFunctiondrilldown += "],},";
                    perFunctionChart = perFunctionCRITICAL + "" + perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + perFunctiondrilldown;
                    #endregion

                    #region  PieChartNotCompleted

                    long totalPieOverduecount = 0;
                    long totalPiePendingForReviewCount = 0;
                    long totalPieRejectedcount = 0;
                    long totalPieInProgresscount = 0;


                    long totalPieOverdueHIGH = 0;
                    long totalPieOverdueMEDIUM = 0;
                    long totalPieOverdueLOW = 0;
                    long totalPieOverdueCRITICAL = 0;

                    long totalPiePendingForReviewHIGH = 0;
                    long totalPiePendingForReviewMEDIUM = 0;
                    long totalPiePendingForReviewLOW = 0;
                    long totalPiePendingForReviewCRITICAL = 0;

                    long totalPieRejectedHIGH = 0;
                    long totalPieRejectedMEDIUM = 0;
                    long totalPieRejectedLOW = 0;
                    long totalPieRejectedCRITICAL = 0;

                    long totalPieInProgressHIGH = 0;
                    long totalPieInProgressMEDIUM = 0;
                    long totalPieInProgressLOW = 0;
                    long totalPieInProgressCRITICAL = 0;



                    foreach (InternalCompliancesCategory cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.ID.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {
                            //High
                            long OverduecountHigh;
                            long PendingForReviewCountHigh;
                            long RejectedcountHigh;
                            long InProgresscountHigh;

                            OverduecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.ID).Count();
                            PendingForReviewCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            RejectedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.ID).Count();
                            InProgresscountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID).Count();


                            //per Status pie Chart
                            totalPieOverduecount += OverduecountHigh;
                            totalPiePendingForReviewCount += PendingForReviewCountHigh;
                            totalPieRejectedcount += RejectedcountHigh;
                            totalPieInProgresscount += InProgresscountHigh;
                            //pie drill down
                            totalPieOverdueHIGH += OverduecountHigh;
                            totalPiePendingForReviewHIGH += PendingForReviewCountHigh;
                            totalPieRejectedHIGH += RejectedcountHigh;
                            totalPieInProgressHIGH += InProgresscountHigh;


                            //Medium
                            long OverduecountMedium;
                            long PendingForReviewCountMedium;
                            long RejectedcountMedium;
                            long InProgresscountMedium;

                            OverduecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.ID).Count();
                            PendingForReviewCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            RejectedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.ID).Count();
                            InProgresscountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID).Count();

                            //per Status pie Chart
                            totalPieOverduecount += OverduecountMedium;
                            totalPiePendingForReviewCount += PendingForReviewCountMedium;
                            totalPieRejectedcount += RejectedcountMedium;
                            totalPieInProgresscount += InProgresscountMedium;

                            //pie drill down 
                            totalPieOverdueMEDIUM += OverduecountMedium;
                            totalPiePendingForReviewMEDIUM += PendingForReviewCountMedium;
                            totalPieRejectedMEDIUM += RejectedcountMedium;
                            totalPieInProgressMEDIUM += InProgresscountMedium;

                            //Low
                            long OverduecountLow;
                            long PendingForReviewCountLow;
                            long RejectedcountLow;
                            long InProgresscountLow;

                            OverduecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.ID).Count();
                            PendingForReviewCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            RejectedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.ID).Count();
                            InProgresscountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID).Count();


                            //per Status pie Chart
                            totalPieOverduecount += OverduecountLow;
                            totalPiePendingForReviewCount += PendingForReviewCountLow;
                            totalPieRejectedcount += RejectedcountLow;
                            totalPieInProgresscount += InProgresscountLow;

                            //pie drill down 
                            totalPieOverdueLOW += OverduecountLow;
                            totalPiePendingForReviewLOW += PendingForReviewCountLow;
                            totalPieRejectedLOW += RejectedcountLow;
                            totalPieInProgressLOW += InProgresscountLow;


                            //CRITICAL 
                            long OverduecountCritical;
                            long PendingForReviewCountCritical;
                            long RejectedcountCritical;
                            long InProgresscountCritical;


                            OverduecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.ID).Count();
                            PendingForReviewCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            RejectedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.ID).Count();
                            InProgresscountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID).Count();

                            //per Status pie Chart
                            totalPieOverduecount += OverduecountCritical;
                            totalPiePendingForReviewCount += PendingForReviewCountCritical;
                            totalPieRejectedcount += RejectedcountCritical;
                            totalPieInProgresscount += InProgresscountCritical;

                            //pie drill down 
                            totalPieOverdueCRITICAL += OverduecountCritical;
                            totalPiePendingForReviewCRITICAL += PendingForReviewCountCritical;
                            totalPieRejectedCRITICAL += RejectedcountCritical;
                            totalPieInProgressCRITICAL += InProgresscountCritical;

                        }

                    }


                    #endregion

                    #region  perFunctionPieChart internal
                    perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Closed Timely',y: " + totalPieCompletedcount + ",color: perStatusChartColorScheme.closetimely,drilldown: 'inTime',},{name: 'Closed Delayed',y: " + totalPieAfterDueDatecount + ",color: perStatusChartColorScheme.closedelay,drilldown: 'afterDueDate',},{name: 'Not Completed',y: " + totalPieNotCompletedCount + ",color: perStatusChartColorScheme.high,drilldown: 'notCompleted',},{name: 'Not Applicable',y: " + totalPieNotApplicablecount + ",color: perStatusChartColorScheme.NotApplicable,drilldown: 'notApplicable',},{name: 'Complied But Document Pending',y: " + totalPieCompliedbutDocPendingcount + ",color: perStatusChartColorScheme.Compiledbutdocumentpending,drilldown: 'compliedbutdocumentpending',}],}],";
                    perFunctionPieChart += "drilldown:{activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, " +
                        " series: [{id: 'inTime',name: 'In Time',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        // " // In time - Critical" +
                        " fpopulateddata(e.point.name,'Closed Timely'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INC_Psummary')" +
                         "},},},{name: 'High',y: " + totalPieCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                        // " // In time - High " +
                        // " e.point.name;"+
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INH_Psummary')" +
                        "},},},{name: 'Medium',y: " + totalPieCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        // " // In time - Medium" +
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INM_Psummary')" +
                        "},},},{name: 'Low',y: " + totalPieCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        // " // In time - Low" +
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INL_Psummary')" +



                        " },},}],},{id: 'afterDueDate',name: 'Closed Delayed',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieAfterDueDateCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        // " // After Due Date - Critical"+
                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFC_Psummary')" +
                         " },},},{name: 'High',y: " + totalPieAfterDueDateHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                         // " // After Due Date - High "+
                         " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFH_Psummary')" +
                        " },},},{name: 'Medium',y: " + totalPieAfterDueDateMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        //  " // After Due Date - Medium "+
                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFM_Psummary')" +
                        " },},},{name: 'Low',y: " + totalPieAfterDueDateLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        // " // After Due Date - Low"+
                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFL_Psummary')" +



                        " },},}],},{id: 'notCompleted',name: 'Not Completed',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieNotCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                       // " // Not Completed - Critical"+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                        " },},},{name: 'High',y: " + totalPieNotCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                       // " // Not Completed - High "+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                        " },},},{name: 'Medium',y: " + totalPieNotCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                       // " // Not Completed - Medium "+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                        " },},},{name: 'Low',y: " + totalPieNotCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                       // " // Not Completed - Low"+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +

                         " },},}],}," +
                        " {id: 'notApplicable',name: 'Not Applicable',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieNotApplicableCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        // " // Not Applicable - Critical
                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAC_Psummary')" +
                        " },},},{name: 'High',y: " + totalPieNotApplicableHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                        // " // Not Applicable - High
                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAH_Psummary')" +
                        " },},},{name: 'Medium',y: " + totalPieNotApplicableMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        // " // Not Applicable - Medium
                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAM_Psummary')" +
                        " },},},{name: 'Low',y: " + totalPieNotApplicableLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        // " // Not Applicable - Low
                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAL_Psummary')" +

                            " },},}],}," +
                        " {id: 'compliedbutdocumentpending',name: 'Complied But Document Pending',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieCompliedbutDocPendingCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        // " // Complied But Document Pending - Critical
                        " fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPC_Psummary')" +
                        " },},},{name: 'High',y: " + totalPieCompliedbutDocPendingHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                        // " // Complied But Document Pending - High
                        " fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPH_Psummary')" +
                        " },},},{name: 'Medium',y: " + totalPieCompliedbutDocPendingMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        // " // Complied But Document Pending - Medium
                        " fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPM_Psummary')" +
                        " },},},{name: 'Low',y: " + totalPieCompliedbutDocPendingLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        // " // Complied But Document Pending - Low
                        " fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPL_Psummary')" +


                       "},},}],}],},";

                    #endregion

                    #region perFunctionPieNotcompleted internal Zomato
                    perFunctionPieNotcompleted = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Overdue',y: " + totalPieOverduecount + ",color: perStatusChartColorScheme.Overdue,drilldown: 'overdue',},{name: 'Pending For Review',y: " + totalPiePendingForReviewCount + ",color: perStatusChartColorScheme.Pendingforreview,drilldown: 'pendingForReview',},{name: 'InProgress',y: " + totalPieInProgresscount + ",color: perStatusChartColorScheme.InProgress,drilldown: 'inProgress',},{name: 'Rejected',y: " + totalPieRejectedcount + ",color: perStatusChartColorScheme.Rejected,drilldown: 'rejected',}],}],";

                    perFunctionPieNotcompleted += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                       " series: [" +

                                        " {id: 'overdue',name: 'Overdue',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPieOverdueCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // In time - Critical                               
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        "},},},{name: 'High',y: " + totalPieOverdueHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // In time - High                   
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieOverdueMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // In time - Medium                                
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        "},},},{name: 'Low',y: " + totalPieOverdueLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // In time - Low                               
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'pendingForReview',name: 'Pending For Review',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPiePendingForReviewCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // After Due Date - Critical
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPiePendingForReviewHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // After Due Date - High
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPiePendingForReviewMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        //  " // After Due Date - Medium
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPiePendingForReviewLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // After Due Date - Low
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'inProgress',name: 'In Progress',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieInProgressCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Completed - Critical
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieInProgressHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Completed - High
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieInProgressMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Completed - Medium
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieInProgressLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Completed - Low
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +


                                         " },},}],}," +
                                        " {id: 'rejected',name: 'Rejected',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieRejectedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Rejected - Critical
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieRejectedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Rejected - High
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieRejectedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Rejected - Medium
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieRejectedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Rejected - Low
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +


                                        " },},}],}],},";
                    #endregion

                    #region perRiskChart internal

                    perRiskChart = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +
                  // Not Completed - Critical

                  "y: " + totalPieNotCompletedCRITICAL + ",events:{click: function(e) {" +
                    " fpopulateddata('Critical','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCC_Rsummary')" +

                    "}}},{  " +

                  // Not Completed - High
                  "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                   " fpopulateddata('High','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCH_Rsummary')" +

                  "}}},{" +

                  // Not Completed - Medium
                  " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                  " fpopulateddata('Medium','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCM_Rsummary')" +

                  "}}},{  " +

                  // Not Completed - Low

                  "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                    " fpopulateddata('Low','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCL_Rsummary')" +


                  "}}}]},{name: 'Closed Delayed',color: perRiskStackedColumnChartColorScheme.closedelay,data: [{" +

                  // After Due Date - Critical
                  "y: " + totalPieAfterDueDateCRITICAL + ",events:{click: function(e) {" +
                  " fpopulateddata('Critical','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFC_Rsummary')" +

                   "}}},{   " +

                  // After Due Date - High

                  "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                  " fpopulateddata('High','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFH_Rsummary')" +

                  "}}},{   " +

                  // After Due Date - Medium
                  "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                  " fpopulateddata('Medium','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFM_Rsummary')" +

                  "}}},{   " +
                  // After Due Date - Low
                  "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                  " fpopulateddata('Low','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFL_Rsummary')" +


                  "}}}]},{name: 'In Time',color: perRiskStackedColumnChartColorScheme.low,data: [{" +
                  // In Time - Critical
                  "y: " + totalPieCompletedCRITICAL + ",events:{click: function(e) {" +
                  " fpopulateddata('Critical','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INC_Rsummary')" +

                    "}}},{  " +

                  // In Time - High
                  "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                  " fpopulateddata('High','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INH_Rsummary')" +

                  "}}},{  " +
                  // In Time - Medium
                  "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                  " fpopulateddata('Medium','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INM_Rsummary')" +

                  "}}},{  " +
                  // In Time - Low
                  "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                  " fpopulateddata('Low','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INL_Rsummary')" +

                  "}}}]}]";

                    #endregion

                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "F")
                        {
                            perRiskChart = tempperRiskChart;
                        }
                        else if (clickflag == "R")
                        {
                            perFunctionChart = tempperFunctionChart;
                            perFunctionPieChart = tempperFunctionPieChart;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        
        public void GetManagementInternalCompliancesSummaryAddedNotCompliedStatus(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<SP_GetManagementInternalCompliancesSummary_Result> MasterQuery, DateTime? FromDate = null, DateTime? EndDateF = null, bool approver = false, int userId = -1)
        {
            try
            {
                string tempperFunctionChart = perFunctionChart;
                string tempperFunctionPieChart = perFunctionPieChart;
                string tempperRiskChart = perRiskChart;
                string tempperFunctionPieChartNotCompleted = perFunctionPieNotcompleted;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perFunctionChart = string.Empty;
                        perFunctionPieChart = string.Empty;
                        perRiskChart = string.Empty;
                        perFunctionPieNotcompleted = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        perFunctionChart = string.Empty;
                    }
                    else if (clickflag == "R")
                    {
                        perRiskChart = string.Empty;
                    }
                }
                else
                {
                    perFunctionChart = string.Empty;
                    perFunctionPieChart = string.Empty;
                    perRiskChart = string.Empty;
                    perFunctionPieNotcompleted = string.Empty;
                }
                bool auditor = false;
                if (AuthenticationHelper.Role == "AUDT")
                {
                    auditor = true;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementInternalCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                    if (Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    List<SP_GetManagementInternalCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementInternalCompliancesSummary_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate
                                             && row.ScheduledOn <= EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate && row.ScheduledOn <= EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.ScheduledOn <= EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                 || row.ScheduledOn <= EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                         || row.ScheduledOn <= FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.ScheduledOn >= FIFromDate
                                                         && row.ScheduledOn <= FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn <= FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    #region PieChart internal
                    long totalPieCompletedcount = 0;
                    long totalPieNotCompletedCount = 0;
                    long totalPieNotCompliedCount = 0;
                    long totalPieAfterDueDatecount = 0;
                    long totalPieNotApplicablecount = 0;
                    long totalPieCompliedbutDocPendingcount = 0;

                    long totalPieCompletedHIGH = 0;
                    long totalPieCompletedMEDIUM = 0;
                    long totalPieCompletedLOW = 0;
                    long totalPieCompletedCRITICAL = 0;

                    long totalPieAfterDueDateHIGH = 0;
                    long totalPieAfterDueDateMEDIUM = 0;
                    long totalPieAfterDueDateLOW = 0;
                    long totalPieAfterDueDateCRITICAL = 0;

                    long totalPieNotCompletedHIGH = 0;
                    long totalPieNotCompletedMEDIUM = 0;
                    long totalPieNotCompletedLOW = 0;
                    long totalPieNotCompletedCRITICAL = 0;

                    long totalPieNotCompliedHIGH = 0;
                    long totalPieNotCompliedMEDIUM = 0;
                    long totalPieNotCompliedLOW = 0;
                    long totalPieNotCompliedCRITICAL = 0;

                    long totalPieNotApplicableHIGH = 0;
                    long totalPieNotApplicableMEDIUM = 0;
                    long totalPieNotApplicableLOW = 0;
                    long totalPieNotApplicableCRITICAL = 0;


                    long totalPieCompliedbutDocPendingHIGH = 0;
                    long totalPieCompliedbutDocPendingMEDIUM = 0;
                    long totalPieCompliedbutDocPendingLOW = 0;
                    long totalPieCompliedbutDocPendingCRITICAL = 0;

                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("High", typeof(long));
                    table.Columns.Add("Medium", typeof(long));
                    table.Columns.Add("Low", typeof(long));
                    table.Columns.Add("Critical", typeof(long));

                    string listCategoryId = "";
                    List<InternalCompliancesCategory> CatagoryList = new List<InternalCompliancesCategory>();

                    if (approver == true)
                    {
                        CatagoryList = ComplianceCategoryManagement.GetFunctionDetailsInternal(AuthenticationHelper.UserID, Branchlist, CustomerbranchID, true);
                    }
                    else
                    {
                        CatagoryList = ComplianceCategoryManagement.GetFunctionDetailsInternal(AuthenticationHelper.UserID, Branchlist, CustomerbranchID);
                    }
                    string perFunctionHIGH = "";
                    string perFunctionMEDIUM = "";
                    string perFunctionLOW = "";
                    string perFunctionCRITICAL = "";

                    string perFunctiondrilldown = "";
                    perFunctionCRITICAL = "series: [ {name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";
                    perFunctionHIGH = "{name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOW = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";


                    perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#e6e6e6'},select:{stroke: '#039',fill:'#e6e6e6'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                    //High

                    foreach (InternalCompliancesCategory cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.ID.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;
                        if (totalcount != 0)
                        {
                            //High
                            long AfterDueDatecountHigh;
                            long CompletedCountHigh;
                            long NotCompletedcountHigh;
                            long NotCompliedcountHigh;
                            long NotApplicableCountHigh;
                            long CompliedbutDocPendingHigh;

                            if (ColoumnNames[2] == "High")
                            {
                                perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                            }

                           
                            AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.ID).Count();
                            //AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            //CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompliedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceStatusID == 17 && entry.ComplianceCategoryId == cc.ID).Count();
                            NotApplicableCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompliedbutDocPendingHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.ID).Count();


                            perFunctiondrilldown += "{ id: 'high' + '" + cc.Name + "', " +
                                "name: 'High'," +
                                " color: perFunctionChartColorScheme.high," +
                                " data: [ ['Closed Timely', " + CompletedCountHigh + "], " +
                                " ['Closed Delayed', " + AfterDueDatecountHigh + "]," +
                                 " ['Not complied', " + NotCompliedcountHigh + "]," +
                                " ['Not completed', " + NotCompletedcountHigh + "]," +
                                " ['Not Applicable', " + NotApplicableCountHigh + "]," +
                                " ['Complied But Document Pending', " + CompliedbutDocPendingHigh + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + cc.ID + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FH_summary')" +
                                "}}},";




                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountHigh;
                            totalPieNotCompletedCount += NotCompletedcountHigh;
                            totalPieAfterDueDatecount += AfterDueDatecountHigh;
                            totalPieNotCompliedCount += NotCompliedcountHigh;
                            totalPieNotApplicablecount += NotApplicableCountHigh;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingHigh;

                            //pie drill down
                            totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                            totalPieCompletedHIGH += CompletedCountHigh;
                            totalPieNotCompletedHIGH += NotCompletedcountHigh;
                            totalPieNotCompliedHIGH += NotCompliedcountHigh;
                            totalPieNotApplicableHIGH += NotApplicableCountHigh;
                            totalPieCompliedbutDocPendingHIGH += CompliedbutDocPendingHigh;

                            //Medium
                            long AfterDueDatecountMedium;
                            long CompletedCountMedium;
                            long NotCompletedcountMedium;
                            long NotCompliedcountMedium;
                            long NotApplicableCountMedium;
                            long CompliedbutDocPendingMedium;

                            if (ColoumnNames[3] == "Medium")
                            {
                                perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                            }

                           
                            AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.ID).Count();
                            //AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            //CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompliedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceStatusID == 17 && entry.ComplianceCategoryId == cc.ID).Count();
                            NotApplicableCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompliedbutDocPendingMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.ID).Count();



                            perFunctiondrilldown += "{ " +
                                " id: 'mid' + '" + cc.Name + "', " +
                                " name: 'Medium'," +
                                " color: perFunctionChartColorScheme.medium, " +
                                " data: [['Closed Timely', " + CompletedCountMedium + "]," +
                                " ['Closed Delayed', " + AfterDueDatecountMedium + "], " +
                                " ['Not complied', " + NotCompliedcountMedium + "], " +
                                " ['Not completed', " + NotCompletedcountMedium + "], " +
                                 " ['Not Applicable', " + NotApplicableCountMedium + "]," +
                                //" ['Complied But Document Pending', " + CompliedbutDocPendingMedium + "]," +
                                " ],events: { " +
                                 " click: function(e){" +
                                " fpopulateddata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + cc.ID + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FM_summary')" +
                                "}}},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountMedium;
                            totalPieNotCompletedCount += NotCompletedcountMedium;
                            totalPieAfterDueDatecount += AfterDueDatecountMedium;
                            totalPieNotCompliedCount += NotCompliedcountMedium;
                            totalPieNotApplicablecount += NotApplicableCountMedium;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingMedium;
                            //pie drill down
                            totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                            totalPieCompletedMEDIUM += CompletedCountMedium;
                            totalPieNotCompletedMEDIUM += NotCompletedcountMedium;
                            totalPieNotCompliedMEDIUM += NotCompliedcountMedium;
                            totalPieNotApplicableMEDIUM += NotApplicableCountMedium;
                            totalPieCompliedbutDocPendingMEDIUM += CompliedbutDocPendingMedium;


                            //Low
                            long AfterDueDatecountLow;
                            long CompletedCountLow;
                            long NotCompletedcountLow;
                            long NotCompliedcountLow;
                            long NotApplicableCountLow;
                            long CompliedbutDocPendingLow;

                            if (ColoumnNames[4] == "Low")
                            {
                                perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                            }
                           

                            AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.ID).Count();
                            //AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            //CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompliedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceStatusID == 17 && entry.ComplianceCategoryId == cc.ID).Count();
                            NotApplicableCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompliedbutDocPendingLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.ID).Count();


                            perFunctiondrilldown += "{ id: 'low' + '" + cc.Name + "'," +
                                " name: 'Low'," +
                                " color: perFunctionChartColorScheme.low," +
                                " data: [['Closed Timely', " + CompletedCountLow + "]," +
                                " ['Closed Delayed'," + AfterDueDatecountLow + "]," +
                                 " ['Not complied'," + NotCompliedcountLow + "]," +
                                " ['Not completed'," + NotCompletedcountLow + "]," +
                                " ['Not Applicable', " + NotApplicableCountLow + "]," +
                                //" ['Complied But Document Pending', " + CompliedbutDocPendingLow + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + cc.ID + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FL_summary')" +
                                "}}},";



                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountLow;
                            totalPieNotCompletedCount += NotCompletedcountLow;
                            totalPieAfterDueDatecount += AfterDueDatecountLow;
                            totalPieNotCompliedCount += NotCompliedcountLow;
                            totalPieNotApplicablecount += NotApplicableCountLow;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingLow;

                            //pie drill down
                            totalPieAfterDueDateLOW += AfterDueDatecountLow;
                            totalPieCompletedLOW += CompletedCountLow;
                            totalPieNotCompletedLOW += NotCompletedcountLow;
                            totalPieNotCompliedLOW += NotCompliedcountLow;
                            totalPieNotApplicableLOW += NotApplicableCountLow;
                            totalPieCompliedbutDocPendingLOW += CompliedbutDocPendingLow;

                            //Critical
                            long AfterDueDatecountCritical;
                            long CompletedCountCritical;
                            long NotCompletedcountCritical;
                            long NotCompliedcountCritical;
                            long NotApplicableCountCritical;
                            long CompliedbutDocPendingCritical;

                            if (ColoumnNames[5] == "Critical")
                            {
                                perFunctionCRITICAL += "{ name:'" + cc.Name + "', y: " + criticalcount + ",drilldown: 'critical' + '" + cc.Name + "',},";
                            }
                         
                            AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.ID).Count();
                            CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.ID).Count();
                            //AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.ID).Count();
                            //CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            NotCompliedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceStatusID == 17 && entry.ComplianceCategoryId == cc.ID).Count();
                            NotApplicableCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.ID).Count();
                            CompliedbutDocPendingCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.ID).Count();

                            perFunctiondrilldown += "{ id: 'critical' + '" + cc.Name + "'," +
                                " name: 'Critical'," +
                                " color: perFunctionChartColorScheme.critical," +
                                " data: [['Closed Timely', " + CompletedCountCritical + "]," +
                                " ['Closed Delayed'," + AfterDueDatecountCritical + "]," +
                                 " ['Not complied'," + NotCompliedcountCritical + "]," +
                                " ['Not completed'," + NotCompletedcountCritical + "]," +
                                 " ['Not Applicable', " + NotApplicableCountCritical + "]," +
                                //" ['Complied But Document Pending', " + CompliedbutDocPendingCritical + "]," +
                                " ],events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.ID + ",'Internal','Functionbarchart','" + cc.ID + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FC_summary')" +
                                "}}},";



                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountCritical;
                            totalPieNotCompletedCount += NotCompletedcountCritical;
                            totalPieAfterDueDatecount += AfterDueDatecountCritical;
                            totalPieNotCompliedCount += NotCompliedcountCritical;
                            totalPieNotApplicablecount += NotApplicableCountCritical;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingCritical;
                            //pie drill down
                            totalPieAfterDueDateCRITICAL += AfterDueDatecountCritical;
                            totalPieCompletedCRITICAL += CompletedCountCritical;
                            totalPieNotCompletedCRITICAL += NotCompletedcountCritical;
                            totalPieNotCompliedCRITICAL += NotCompliedcountCritical;
                            totalPieNotApplicableCRITICAL += NotApplicableCountCritical;
                            totalPieCompliedbutDocPendingCRITICAL += CompliedbutDocPendingCritical;



                        }
                    }
                    perFunctionCRITICAL += "],},";
                    perFunctionHIGH += "],},";
                    perFunctionMEDIUM += "],},";
                    perFunctionLOW += "],},],";

                    perFunctiondrilldown += "],},";
                    perFunctionChart = perFunctionCRITICAL + "" + perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + perFunctiondrilldown;
                    #endregion

                    #region  PieChartNotCompleted

                    long totalPieOverduecount = 0;
                    long totalPiePendingForReviewCount = 0;
                    long totalPieRejectedcount = 0;
                    long totalPieInProgresscount = 0;


                    long totalPieOverdueHIGH = 0;
                    long totalPieOverdueMEDIUM = 0;
                    long totalPieOverdueLOW = 0;
                    long totalPieOverdueCRITICAL = 0;

                    long totalPiePendingForReviewHIGH = 0;
                    long totalPiePendingForReviewMEDIUM = 0;
                    long totalPiePendingForReviewLOW = 0;
                    long totalPiePendingForReviewCRITICAL = 0;

                    long totalPieRejectedHIGH = 0;
                    long totalPieRejectedMEDIUM = 0;
                    long totalPieRejectedLOW = 0;
                    long totalPieRejectedCRITICAL = 0;

                    long totalPieInProgressHIGH = 0;
                    long totalPieInProgressMEDIUM = 0;
                    long totalPieInProgressLOW = 0;
                    long totalPieInProgressCRITICAL = 0;



                    foreach (InternalCompliancesCategory cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.ID.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.ID && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {
                            //High
                            long OverduecountHigh;
                            long PendingForReviewCountHigh;
                            long RejectedcountHigh;
                            long InProgresscountHigh;

                            OverduecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.ID).Count();
                            PendingForReviewCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            RejectedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.ID).Count();
                            InProgresscountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID).Count();


                            //per Status pie Chart
                            totalPieOverduecount += OverduecountHigh;
                            totalPiePendingForReviewCount += PendingForReviewCountHigh;
                            totalPieRejectedcount += RejectedcountHigh;
                            totalPieInProgresscount += InProgresscountHigh;
                            //pie drill down
                            totalPieOverdueHIGH += OverduecountHigh;
                            totalPiePendingForReviewHIGH += PendingForReviewCountHigh;
                            totalPieRejectedHIGH += RejectedcountHigh;
                            totalPieInProgressHIGH += InProgresscountHigh;


                            //Medium
                            long OverduecountMedium;
                            long PendingForReviewCountMedium;
                            long RejectedcountMedium;
                            long InProgresscountMedium;

                            OverduecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.ID).Count();
                            PendingForReviewCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            RejectedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.ID).Count();
                            InProgresscountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID).Count();

                            //per Status pie Chart
                            totalPieOverduecount += OverduecountMedium;
                            totalPiePendingForReviewCount += PendingForReviewCountMedium;
                            totalPieRejectedcount += RejectedcountMedium;
                            totalPieInProgresscount += InProgresscountMedium;

                            //pie drill down 
                            totalPieOverdueMEDIUM += OverduecountMedium;
                            totalPiePendingForReviewMEDIUM += PendingForReviewCountMedium;
                            totalPieRejectedMEDIUM += RejectedcountMedium;
                            totalPieInProgressMEDIUM += InProgresscountMedium;

                            //Low
                            long OverduecountLow;
                            long PendingForReviewCountLow;
                            long RejectedcountLow;
                            long InProgresscountLow;

                            OverduecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.ID).Count();
                            PendingForReviewCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            RejectedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.ID).Count();
                            InProgresscountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID).Count();


                            //per Status pie Chart
                            totalPieOverduecount += OverduecountLow;
                            totalPiePendingForReviewCount += PendingForReviewCountLow;
                            totalPieRejectedcount += RejectedcountLow;
                            totalPieInProgresscount += InProgresscountLow;

                            //pie drill down 
                            totalPieOverdueLOW += OverduecountLow;
                            totalPiePendingForReviewLOW += PendingForReviewCountLow;
                            totalPieRejectedLOW += RejectedcountLow;
                            totalPieInProgressLOW += InProgresscountLow;


                            //CRITICAL 
                            long OverduecountCritical;
                            long PendingForReviewCountCritical;
                            long RejectedcountCritical;
                            long InProgresscountCritical;


                            OverduecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.ID).Count();
                            PendingForReviewCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.ID).Count();
                            RejectedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.ID).Count();
                            InProgresscountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.ID).Count();

                            //per Status pie Chart
                            totalPieOverduecount += OverduecountCritical;
                            totalPiePendingForReviewCount += PendingForReviewCountCritical;
                            totalPieRejectedcount += RejectedcountCritical;
                            totalPieInProgresscount += InProgresscountCritical;

                            //pie drill down 
                            totalPieOverdueCRITICAL += OverduecountCritical;
                            totalPiePendingForReviewCRITICAL += PendingForReviewCountCritical;
                            totalPieRejectedCRITICAL += RejectedcountCritical;
                            totalPieInProgressCRITICAL += InProgresscountCritical;

                        }

                    }


                    #endregion

                    #region  perFunctionPieChart internal
                    perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Closed Timely',y: " + totalPieCompletedcount + ",color: perStatusChartColorScheme.closetimely,drilldown: 'inTime',},{name: 'Closed Delayed',y: " + totalPieAfterDueDatecount + ",color: perStatusChartColorScheme.closedelay,drilldown: 'afterDueDate',},{name: 'Not Complied',y: " + totalPieNotCompliedCount + ",color: perStatusChartColorScheme.NotComplied,drilldown: 'notComplied',},{name: 'Not Completed',y: " + totalPieNotCompletedCount + ",color: perStatusChartColorScheme.high,drilldown: 'notCompleted',},{name: 'Not Applicable',y: " + totalPieNotApplicablecount + ",color: perStatusChartColorScheme.NotApplicable,drilldown: 'notApplicable',}],}],";
                    perFunctionPieChart += "drilldown:{activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, " +
                        " series: [{id: 'inTime',name: 'Closed Timely',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        // " // In time - Critical" +
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INC_Psummary')" +
                         "},},},{name: 'High',y: " + totalPieCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                        // " // In time - High " +
                        // " e.point.name;"+
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INH_Psummary')" +
                        "},},},{name: 'Medium',y: " + totalPieCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        // " // In time - Medium" +
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INM_Psummary')" +
                        "},},},{name: 'Low',y: " + totalPieCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        // " // In time - Low" +
                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INL_Psummary')" +



                        " },},}],},{id: 'afterDueDate',name: 'Closed Delayed',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieAfterDueDateCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        // " // After Due Date - Critical"+
                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFC_Psummary')" +
                         " },},},{name: 'High',y: " + totalPieAfterDueDateHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                         // " // After Due Date - High "+
                         " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFH_Psummary')" +
                        " },},},{name: 'Medium',y: " + totalPieAfterDueDateMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        //  " // After Due Date - Medium "+
                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFM_Psummary')" +
                        " },},},{name: 'Low',y: " + totalPieAfterDueDateLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        // " // After Due Date - Low"+
                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFL_Psummary')" +

                          " },},}],},{id: 'notComplied',name: 'Not Complied',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPieNotCompliedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        // " // Not Complied - Critical"+
                        " fpopulateddata(e.point.name,'Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCOC_Psummary')" +
                            " },},},{name: 'High',y: " + totalPieNotCompliedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                            // " // Not Complied - High "+
                            " fpopulateddata(e.point.name,'Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCOH_Psummary')" +
                        " },},},{name: 'Medium',y: " + totalPieNotCompliedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        //  " // Not Complied - Medium "+
                        " fpopulateddata(e.point.name,'Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCOM_Psummary')" +
                        " },},},{name: 'Low',y: " + totalPieNotCompliedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        // " // Not Complied - Low"+
                        " fpopulateddata(e.point.name,'Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCOL_Psummary')" +

                        
                        " },},}],},{id: 'notCompleted',name: 'Not Completed',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieNotCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                       // " // Not Completed - Critical"+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                        " },},},{name: 'High',y: " + totalPieNotCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                       // " // Not Completed - High "+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                        " },},},{name: 'Medium',y: " + totalPieNotCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                       // " // Not Completed - Medium "+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                        " },},},{name: 'Low',y: " + totalPieNotCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                       // " // Not Completed - Low"+
                       " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +

                         " },},}],}," +
                        " {id: 'notApplicable',name: 'Not Applicable',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieNotApplicableCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        // " // Not Applicable - Critical
                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAC_Psummary')" +
                        " },},},{name: 'High',y: " + totalPieNotApplicableHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                        // " // Not Applicable - High
                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAH_Psummary')" +
                        " },},},{name: 'Medium',y: " + totalPieNotApplicableMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        // " // Not Applicable - Medium
                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAM_Psummary')" +
                        " },},},{name: 'Low',y: " + totalPieNotApplicableLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        // " // Not Applicable - Low
                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAL_Psummary')" +

                        //    " },},}],}," +
                        //" {id: 'compliedbutdocumentpending',name: 'Complied But Document Pending',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieCompliedbutDocPendingCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                        //// " // Complied But Document Pending - Critical
                        //" fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPC_Psummary')" +
                        //" },},},{name: 'High',y: " + totalPieCompliedbutDocPendingHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                        //// " // Complied But Document Pending - High
                        //" fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPH_Psummary')" +
                        //" },},},{name: 'Medium',y: " + totalPieCompliedbutDocPendingMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                        //// " // Complied But Document Pending - Medium
                        //" fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPM_Psummary')" +
                        //" },},},{name: 'Low',y: " + totalPieCompliedbutDocPendingLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                        //// " // Complied But Document Pending - Low
                        //" fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPL_Psummary')" +


                       "},},}],}],},";

                    #endregion

                    #region perFunctionPieNotcompleted internal Zomato
                    perFunctionPieNotcompleted = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Overdue',y: " + totalPieOverduecount + ",color: perStatusChartColorScheme.Overdue,drilldown: 'overdue',},{name: 'Pending For Review',y: " + totalPiePendingForReviewCount + ",color: perStatusChartColorScheme.Pendingforreview,drilldown: 'pendingForReview',},{name: 'InProgress',y: " + totalPieInProgresscount + ",color: perStatusChartColorScheme.InProgress,drilldown: 'inProgress',},{name: 'Rejected',y: " + totalPieRejectedcount + ",color: perStatusChartColorScheme.Rejected,drilldown: 'rejected',}],}],";

                    perFunctionPieNotcompleted += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                       " series: [" +

                                        " {id: 'overdue',name: 'Overdue',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPieOverdueCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // In time - Critical                               
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        "},},},{name: 'High',y: " + totalPieOverdueHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // In time - High                   
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieOverdueMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // In time - Medium                                
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        "},},},{name: 'Low',y: " + totalPieOverdueLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // In time - Low                               
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'pendingForReview',name: 'Pending For Review',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPiePendingForReviewCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // After Due Date - Critical
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPiePendingForReviewHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // After Due Date - High
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPiePendingForReviewMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        //  " // After Due Date - Medium
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPiePendingForReviewLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // After Due Date - Low
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'inProgress',name: 'In Progress',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieInProgressCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Completed - Critical
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieInProgressHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Completed - High
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieInProgressMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Completed - Medium
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieInProgressLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Completed - Low
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +


                                         " },},}],}," +
                                        " {id: 'rejected',name: 'Rejected',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieRejectedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Rejected - Critical
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieRejectedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Rejected - High
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieRejectedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Rejected - Medium
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieRejectedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Rejected - Low
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +


                                        " },},}],}],},";
                    #endregion

                    #region perRiskChart internal

                    perRiskChart = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +
                  // Not Completed - Critical


                  "y: " + totalPieNotCompletedCRITICAL + ",events:{click: function(e) {" +
                    " fpopulateddata('Critical','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCC_Rsummary')" +

                    "}}},{  " +

                  // Not Completed - High
                  "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                   " fpopulateddata('High','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCH_Rsummary')" +

                  "}}},{" +

                  // Not Completed - Medium
                  " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                  " fpopulateddata('Medium','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCM_Rsummary')" +

                  "}}},{  " +

                  // Not Completed - Low

                  "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                    " fpopulateddata('Low','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCL_Rsummary')" +


                     "}}}]},{name: 'Not Complied',color: perRiskStackedColumnChartColorScheme.NotComplied,data: [{" +
                  // Not Complied - Critical
                  "y: " + totalPieNotCompliedCRITICAL + ",events:{click: function(e) {" +
                  " fpopulateddata('Critical','Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCOC_Rsummary')" +

                   "}}},{   " +

                  // Not Complied - High

                  "y: " + totalPieNotCompliedHIGH + ",events:{click: function(e) { " +
                  " fpopulateddata('High','Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCOH_Rsummary')" +

                  "}}},{   " +

                  // Not Complied - Medium
                  "y: " + totalPieNotCompliedMEDIUM + ",events:{click: function(e) {" +
                  " fpopulateddata('Medium','Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCOM_Rsummary')" +

                  "}}},{   " +
                  // Not Complied - Low
                  "y: " + totalPieNotCompliedLOW + ",events:{click: function(e) {" +
                  " fpopulateddata('Low','Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCOL_Rsummary')" +

                  "}}}]},{name: 'Closed Delayed',color: perRiskStackedColumnChartColorScheme.closedelay,data: [{" +

                  // After Due Date - Critical
                  "y: " + totalPieAfterDueDateCRITICAL + ",events:{click: function(e) {" +
                  " fpopulateddata('Critical','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFC_Rsummary')" +

                   "}}},{   " +

                  // After Due Date - High

                  "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                  " fpopulateddata('High','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFH_Rsummary')" +

                  "}}},{   " +

                  // After Due Date - Medium
                  "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                  " fpopulateddata('Medium','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFM_Rsummary')" +

                  "}}},{   " +
                  // After Due Date - Low
                  "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                  " fpopulateddata('Low','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFL_Rsummary')" +


                  "}}}]},{name: 'Closed Timely',color: perRiskStackedColumnChartColorScheme.closetimely,data: [{" +
                  // In Time - Critical
                  "y: " + totalPieCompletedCRITICAL + ",events:{click: function(e) {" +
                  " fpopulateddata('Critical','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INC_Rsummary')" +

                    "}}},{  " +

                  // In Time - High
                  "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                  " fpopulateddata('High','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INH_Rsummary')" +

                  "}}},{  " +
                  // In Time - Medium
                  "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                  " fpopulateddata('Medium','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INM_Rsummary')" +

                  "}}},{  " +
                  // In Time - Low
                  "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                  " fpopulateddata('Low','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Internal','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INL_Rsummary')" +


        "}}}]}]";

                    #endregion

                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "F")
                        {
                            perRiskChart = tempperRiskChart;
                        }
                        else if (clickflag == "R")
                        {
                            perFunctionChart = tempperFunctionChart;
                            perFunctionPieChart = tempperFunctionPieChart;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        #endregion


        #region Statutory
        public void GetPenaltyQuarterWise(int customerid, int CustomerbranchID, string QuarterName, List<SP_GetManagementCompliancesSummary_Result> masterquarterquery, DateTime? startDate = null, DateTime? EndDate = null, bool approver = false)
        {
            List<SP_GetManagementCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementCompliancesSummary_Result>();
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (approver == true)
                {
                    transactionsQuery = (from row in masterquarterquery
                                         where row.CustomerID == customerid
                                         && row.ScheduledOn >= startDate
                                         && row.ScheduledOn <= EndDate
                                         && row.PenaltySubmit == "S" && row.RoleID == 4
                                         select row).ToList();

                }
                else
                {
                    transactionsQuery = (from row in masterquarterquery
                                         where row.CustomerID == customerid
                                         && row.ScheduledOn >= startDate
                                         && row.ScheduledOn <= EndDate
                                         && row.PenaltySubmit == "S" && row.RoleID == 4
                                         && (row.NonComplianceType == 2 || row.NonComplianceType == 0)
                                         select row).ToList();

                }

                if (approver == true)
                {
                    transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                }

                transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();


                decimal highcount;
                decimal mediumCount;
                decimal lowcount;
                decimal criticalcount;
                decimal totalcount;
                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("Quarter", typeof(string));
                table.Columns.Add("High", typeof(long));
                table.Columns.Add("Medium", typeof(long));
                table.Columns.Add("Low", typeof(long));
                table.Columns.Add("Critical", typeof(long));

                string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceStatusID != 11).Sum(a => (decimal)a.totalvalue);
                mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceStatusID != 11).Sum(a => (decimal)a.totalvalue);
                lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceStatusID != 11).Sum(a => (decimal)a.totalvalue);
                criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceStatusID != 11).Sum(a => (decimal)a.totalvalue);
                totalcount = highcount + mediumCount + lowcount + criticalcount;
                if (ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                {
                    IsSatutoryInternal = "Statutory";
                }
                else if (ddlStatus.SelectedItem.Text == "Statutory")
                {
                    IsSatutoryInternal = "StatutoryAll";
                }
                else if (ddlStatus.SelectedItem.Text == "Internal")
                {
                    IsSatutoryInternal = "Internal";
                }
                if (totalcount != 0)
                {
                    //High
                    decimal AfterDueDatecountHigh;
                    decimal CompletedCountHigh;
                    decimal NotCompletedcountHigh;
                    if (ColoumnNames[2] == "High")
                    {
                        perFunctionHIGHPenalty += "{ name:'" + QuarterName + "', y: " + highcount + ",drilldown: 'high' + '" + QuarterName + "',events:{click: function(e){fpopulatedPenaltydata(e.point.name, 'High', " + customerid + ", " + CustomerbranchID + ", '" + startDate + "', '" + EndDate + "', '" + approver + "','" + IsSatutoryInternal + "','PNH_psummary')}},},";
                    }

                   
                    AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.FilterStatus == "ClosedDelayed").Sum(a => (decimal)a.totalvalue);
                    CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.FilterStatus == "ClosedTimely").Sum(a => (decimal)a.totalvalue);
                    //AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Sum(a => (decimal)a.totalvalue);
                    //CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Sum(a => (decimal)a.totalvalue);
                    NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)).Sum(a => (decimal)a.totalvalue);
                    //Medium
                    decimal AfterDueDatecountMedium;
                    decimal CompletedCountMedium;
                    decimal NotCompletedcountMedium;

                    if (ColoumnNames[3] == "Medium")
                    {
                        perFunctionMEDIUMPenalty += "{ name:'" + QuarterName + "', y: " + mediumCount + ",drilldown: 'mid' + '" + QuarterName + "',events: { click: function(e){fpopulatedPenaltydata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + startDate + "','" + EndDate + "','" + approver + "','" + IsSatutoryInternal + "','PNM_psummary')}},},";

                    }
                 
                    AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.FilterStatus == "ClosedDelayed").Sum(a => (decimal)a.totalvalue);
                    CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.FilterStatus == "ClosedTimely").Sum(a => (decimal)a.totalvalue);
                    //AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Sum(a => (decimal)a.totalvalue);
                    //CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Sum(a => (decimal)a.totalvalue);
                    NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)).Sum(a => (decimal)a.totalvalue);

                    //Low
                    decimal AfterDueDatecountLow;
                    decimal CompletedCountLow;
                    decimal NotCompletedcountLow;

                    if (ColoumnNames[4] == "Low")
                    {
                        perFunctionLOWPenalty += "{ name:'" + QuarterName + "', y: " + lowcount + ",drilldown: 'low' + '" + QuarterName + "',events: { click: function(e){ fpopulatedPenaltydata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + startDate + "','" + EndDate + "','" + approver + "','" + IsSatutoryInternal + "','PNL_psummary')}},},";

                    }
               
                    AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.FilterStatus == "ClosedDelayed").Sum(a => (decimal)a.totalvalue);
                    CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.FilterStatus == "ClosedTimely").Sum(a => (decimal)a.totalvalue);
                    //AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Sum(a => (decimal)a.totalvalue);
                    //CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Sum(a => (decimal)a.totalvalue);
                    NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)).Sum(a => (decimal)a.totalvalue);

                    //Critical
                    decimal AfterDueDatecountCritical;
                    decimal CompletedCountCritical;
                    decimal NotCompletedcountCritical;

                    if (ColoumnNames[5] == "Critical")
                    {
                        perFunctionCRITICALPenalty += "{ name:'" + QuarterName + "', y: " + criticalcount + ",drilldown: 'critical' + '" + QuarterName + "',events: { click: function(e){ fpopulatedPenaltydata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + startDate + "','" + EndDate + "','" + approver + "','" + IsSatutoryInternal + "','PNC_psummary')}},},";

                    }
          
                    AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.FilterStatus == "ClosedDelayed").Sum(a => (decimal)a.totalvalue);
                    CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.FilterStatus == "ClosedTimely").Sum(a => (decimal)a.totalvalue);
                    //AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Sum(a => (decimal)a.totalvalue);
                    //CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Sum(a => (decimal)a.totalvalue);
                    NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)).Sum(a => (decimal)a.totalvalue);

                }
            }
        }        
        public void GetManagementCompliancePenalitySummary(int customerid, List<long> Branchlist, int CustomerbranchID, string financialYear, List<SP_GetManagementCompliancesSummary_Result> MasterQuery, bool approver, int userId = -1)
        {
            try
            {
                string QuarterName = string.Empty;
                int year = 0;
                List<string> q = new List<string>();
                q.Add("Q1");
                q.Add("Q2");
                q.Add("Q3");
                q.Add("Q4");
                perPenaltyStatusPieChart = string.Empty;
                perPenaltyStatusPieChart = string.Empty;
                string[] y = financialYear.Split('-');
                year = Convert.ToInt32(y[0]);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementCompliancesSummary_Result> MasterPenaltyQuarterWiseQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    if (approver == true)
                    {
                        entities.Database.CommandTimeout = 180;
                         MasterPenaltyQuarterWiseQuery = (from row in MasterQuery.Where(entry => entry.PenaltySubmit == "S"
                                                            && entry.IsPenaltySave == false && entry.RoleID == 6
                                                            && (entry.NonComplianceType == 2 || entry.NonComplianceType == 0)
                                                            && (entry.Penalty != null || entry.Interest != null))
                                                             select row).ToList();

                        MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        BindPenaltyAmount(customerid, MasterPenaltyQuarterWiseQuery);
                        if (Branchlist.Count > 0)
                        {
                            MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                        }

                        approver = true;
                        IsApprover = true;
                        BindPenaltyAmountApprover(customerid, MasterPenaltyQuarterWiseQuery);
                    }
                    else
                    {


                        approver = false;
                        IsApprover = false;

                        entities.Database.CommandTimeout = 180;
                         MasterPenaltyQuarterWiseQuery = (from row in MasterQuery.Where(entry => entry.PenaltySubmit == "S"
                                                            && entry.IsPenaltySave == false && entry.RoleID == 4
                                                            && (entry.NonComplianceType == 2 || entry.NonComplianceType == 0)
                                                            && (entry.Penalty != null || entry.Interest != null))
                                                             select row).ToList();

                        MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        BindPenaltyAmount(customerid, MasterPenaltyQuarterWiseQuery);
                        if (Branchlist.Count > 0)
                        {
                            MasterPenaltyQuarterWiseQuery = MasterPenaltyQuarterWiseQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                        }

                    }






                    perFunctionCRITICALPenalty = "series: [ {name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";
                    perFunctionHIGHPenalty = "{name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUMPenalty = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOWPenalty = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";
                   
                    DateTime startDate = Convert.ToDateTime("1/1/1900 12:00:00 AM");
                    DateTime EndDate = Convert.ToDateTime("1/1/1900 12:00:00 AM");


                    for (int i = 0; i < q.Count; i++)
                    {
                        if (q[i] == "Q1")
                        {
                            QuarterName = "Apr-Jun";
                            startDate = new DateTime(year, 4, 1);
                            EndDate = new DateTime(year, 6, DateTime.DaysInMonth(year, 6));
                            if (approver == true)
                            {
                                GetPenaltyQuarterWise(customerid, CustomerbranchID, QuarterName, MasterPenaltyQuarterWiseQuery, startDate, EndDate, true);
                            }
                            else
                            {
                                GetPenaltyQuarterWise(customerid, CustomerbranchID, QuarterName, MasterPenaltyQuarterWiseQuery, startDate, EndDate, false);
                            }
                        }
                        else if (q[i] == "Q2")
                        {
                            QuarterName = "Jul-Sep";
                            startDate = new DateTime(year, 7, 1);
                            EndDate = new DateTime(year, 9, DateTime.DaysInMonth(year, 9));
                            if (approver == true)
                            {
                                GetPenaltyQuarterWise(customerid, CustomerbranchID, QuarterName, MasterPenaltyQuarterWiseQuery, startDate, EndDate, true);
                            }
                            else
                            {
                                GetPenaltyQuarterWise(customerid, CustomerbranchID, QuarterName, MasterPenaltyQuarterWiseQuery, startDate, EndDate, false);
                            }
                        }
                        else if (q[i] == "Q3")
                        {
                            QuarterName = "Oct-Dec";
                            startDate = new DateTime(year, 10, 1);
                            EndDate = new DateTime(year, 12, DateTime.DaysInMonth(year, 12));
                            if (approver == true)
                            {
                                GetPenaltyQuarterWise(customerid, CustomerbranchID, QuarterName, MasterPenaltyQuarterWiseQuery, startDate, EndDate, true);
                            }
                            else
                            {
                                GetPenaltyQuarterWise(customerid, CustomerbranchID, QuarterName, MasterPenaltyQuarterWiseQuery, startDate, EndDate, false);
                            }
                        }
                        else if (q[i] == "Q4")
                        {
                            QuarterName = "Jan-Mar";
                            startDate = new DateTime((year + 1), 1, 1);
                            EndDate = new DateTime((year + 1), 3, DateTime.DaysInMonth((year + 1), 3));
                            if (approver == true)
                            {
                                GetPenaltyQuarterWise(customerid, CustomerbranchID, QuarterName, MasterPenaltyQuarterWiseQuery, startDate, EndDate, true);
                            }
                            else
                            {
                                GetPenaltyQuarterWise(customerid, CustomerbranchID, QuarterName, MasterPenaltyQuarterWiseQuery, startDate, EndDate, false);
                            }
                        }

                    }
                    perFunctionCRITICALPenalty += "],},";
                    perFunctionHIGHPenalty += "],},";
                    perFunctionMEDIUMPenalty += "],},";
                    perFunctionLOWPenalty += "],},],";
                   

                    perPenaltyStatusPieChart = perFunctionCRITICALPenalty + "" + perFunctionHIGHPenalty + "" + perFunctionMEDIUMPenalty  + "" + perFunctionLOWPenalty;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        

        public void GetManagementCompliancesSummary(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<SP_GetManagementCompliancesSummary_Result> MasterQuery, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1)
        {
            try
            {
                string IsSatutoryInternalORALL = string.Empty;
                if (ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                {
                    IsSatutoryInternalORALL = "Statutory";
                }
                else if (ddlStatus.SelectedItem.Text == "Statutory")
                {
                    IsSatutoryInternalORALL = "StatutoryAll";
                }
                else if (ddlStatus.SelectedItem.Text == "Internal")
                {
                    IsSatutoryInternalORALL = "Internal";
                }
                string tempperFunctionChart = perFunctionChart;
                string tempperFunctionPieChart = perFunctionPieChart;
                string tempperRiskChart = perRiskChart;
                string tempperFunctionPieChartNotCompleted = perFunctionPieNotcompleted;
             
                if (1 == 2)
                {                   
                    if (clickflag == "T")
                    {
                        perFunctionChart = string.Empty;
                        perFunctionPieChart = string.Empty;
                        perRiskChart = string.Empty;
                        perFunctionPieNotcompleted = string.Empty;                       
                    }
                    else if (clickflag == "F")
                    {
                        perFunctionChart = string.Empty;
                    }
                    else if (clickflag == "R")
                    {
                        perRiskChart = string.Empty;                       
                    }
                }
                else
                {
                    perFunctionChart = string.Empty;
                    perFunctionPieChart = string.Empty;
                    perRiskChart = string.Empty;
                    perFunctionPieNotcompleted = string.Empty;           
                }

                bool auditor = false;
                if (AuthenticationHelper.Role == "AUDT")
                {
                    auditor = true;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;
                    entities.Database.CommandTimeout = 180;

                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                    if (Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    List<SP_GetManagementCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate
                                             && row.ScheduledOn <= EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate && row.ScheduledOn <= EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.ScheduledOn <= EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                 || row.ScheduledOn <= EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                         || row.ScheduledOn <= FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.ScheduledOn >= FIFromDate
                                                         && row.ScheduledOn <= FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn <= FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    #region  PieChart
                    long totalPieCompletedcount = 0;
                    long totalPieNotCompletedCount = 0;
                    long totalPieAfterDueDatecount = 0;
                    long totalPieNotApplicablecount = 0;
                    long totalPieCompliedbutDocPendingcount = 0;


                    long totalPieCompletedHIGH = 0;
                    long totalPieCompletedMEDIUM = 0;
                    long totalPieCompletedLOW = 0;
                    long totalPieCompletedCRITICAL = 0;

                    long totalPieAfterDueDateHIGH = 0;
                    long totalPieAfterDueDateMEDIUM = 0;
                    long totalPieAfterDueDateLOW = 0;
                    long totalPieAfterDueDateCRITICAL = 0;

                    long totalPieNotCompletedHIGH = 0;
                    long totalPieNotCompletedMEDIUM = 0;
                    long totalPieNotCompletedLOW = 0;
                    long totalPieNotCompletedCRITICAL = 0;

                    long totalPieNotApplicableHIGH = 0;
                    long totalPieNotApplicableMEDIUM = 0;
                    long totalPieNotApplicableLOW = 0;
                    long totalPieNotApplicableCRITICAL = 0;


                    long totalPieCompliedbutDocPendingHIGH = 0;
                    long totalPieCompliedbutDocPendingMEDIUM = 0;
                    long totalPieCompliedbutDocPendingLOW = 0;
                    long totalPieCompliedbutDocPendingCRITICAL = 0;


                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("High", typeof(long));
                    table.Columns.Add("Medium", typeof(long));
                    table.Columns.Add("Low", typeof(long));
                    table.Columns.Add("Critical", typeof(long));

                    string listCategoryId = "";
                    List<sp_ComplianceAssignedCategory_Result> CatagoryList = new List<sp_ComplianceAssignedCategory_Result>();
                    if (approver == true)
                    {
                        CatagoryList = ComplianceCategoryManagement.GetNewAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, true);
                    }
                    else
                    {
                        CatagoryList = ComplianceCategoryManagement.GetNewAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist);
                    }

                    string perFunctionHIGH = "";
                    string perFunctionMEDIUM = "";
                    string perFunctionLOW = "";
                    string perFunctionCRITICAL = "";

                    string perFunctiondrilldown = "";

                    perFunctionCRITICAL = "series: [ {name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";
                    perFunctionHIGH = "{name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOW = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";



                    perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#f7f7f7'},select:{stroke: '#039',fill:'#f7f7f7'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

                    foreach (sp_ComplianceAssignedCategory_Result cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.Id.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.Id 
                        //&& entry.ComplianceStatusID != 11
                        ).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.Id 
                        //&& entry.ComplianceStatusID != 11
                        ).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.Id 
                        //&& entry.ComplianceStatusID != 11
                        ).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.Id 
                        //&& entry.ComplianceStatusID != 11
                        ).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {
                            //High
                            long AfterDueDatecountHigh;
                            long CompletedCountHigh;
                            long NotCompletedcountHigh;
                            long NotApplicableCountHigh;
                            long CompliedbutDocPendingHigh;

                            if (ColoumnNames[2] == "High")
                            {
                                perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                            }
                            //AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();                            
                            //CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();
                           
                            AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.FilterStatus== "ClosedDelayed" && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 &&  entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.Id).Count();


                            NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 ||   entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotApplicableCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 15 ) && entry.ComplianceCategoryId == cc.Id).Count(); /*|| entry.ComplianceStatusID == 18*/
                            CompliedbutDocPendingHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 19 ) && entry.ComplianceCategoryId == cc.Id).Count();
                            perFunctiondrilldown += "{" +
                                " id: 'high' + '" + cc.Name + "'," +
                                " name: 'High'," +
                                " color: perFunctionChartColorScheme.high," +
                                " data: [ ['Closed Timely', " + CompletedCountHigh + "], " +
                                " ['Closed Delayed', " + AfterDueDatecountHigh + "]," +
                                " ['Not completed', " + NotCompletedcountHigh + "]," +
                                " ['Not Applicable', " + NotApplicableCountHigh + "],]," +                               
                                " events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'"+ IsSatutoryInternalORALL + "','Functionbarchart','" + cc.Id + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FH_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountHigh;
                            totalPieNotCompletedCount += NotCompletedcountHigh;
                            totalPieAfterDueDatecount += AfterDueDatecountHigh;
                            totalPieNotApplicablecount += NotApplicableCountHigh;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingHigh;

                            //pie drill down
                            totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                            totalPieCompletedHIGH += CompletedCountHigh;
                            totalPieNotCompletedHIGH += NotCompletedcountHigh;
                            totalPieNotApplicableHIGH += NotApplicableCountHigh;
                            totalPieCompliedbutDocPendingHIGH += CompliedbutDocPendingHigh;

                            //Medium
                            long AfterDueDatecountMedium;
                            long CompletedCountMedium;
                            long NotCompletedcountMedium;
                            long NotApplicableCountMedium;
                            long CompliedbutDocPendingMedium;

                            if (ColoumnNames[3] == "Medium")
                            {
                                perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                            }
                            //AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            //CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 ) && entry.ComplianceCategoryId == cc.Id).Count();

                            AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.Id).Count();

                            NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotApplicableCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 15 ) && entry.ComplianceCategoryId == cc.Id).Count(); /*|| entry.ComplianceStatusID == 18*/
                            CompliedbutDocPendingMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.Id).Count();

                            perFunctiondrilldown += "{ " +
                                " id: 'mid' + '" + cc.Name + "'," +
                                " name: 'Medium'," +
                                " color: perFunctionChartColorScheme.medium," +
                                " data: [['Closed Timely', " + CompletedCountMedium + "]," +
                                " ['Closed Delayed', " + AfterDueDatecountMedium + "]," +
                                " ['Not completed', " + NotCompletedcountMedium + "], " +
                                 " ['Not Applicable', " + NotApplicableCountMedium + "],]," +                            
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "','Function'," + cc.Id + ",'" + IsSatutoryInternalORALL + "','Functionbarchart','" + cc.Id + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FM_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountMedium;
                            totalPieNotCompletedCount += NotCompletedcountMedium;
                            totalPieAfterDueDatecount += AfterDueDatecountMedium;
                            totalPieNotApplicablecount += NotApplicableCountMedium;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingMedium;

                            //pie drill down 
                            totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                            totalPieCompletedMEDIUM += CompletedCountMedium;
                            totalPieNotCompletedMEDIUM += NotCompletedcountMedium;
                            totalPieNotApplicableMEDIUM += NotApplicableCountMedium;
                            totalPieCompliedbutDocPendingMEDIUM += CompliedbutDocPendingMedium;

                            //Low
                            long AfterDueDatecountLow;
                            long CompletedCountLow;
                            long NotCompletedcountLow;
                            long NotApplicableCountLow;
                            long CompliedbutDocPendingLow;


                            if (ColoumnNames[4] == "Low")
                            {
                                perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                            }

                            //AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            //CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 ) && entry.ComplianceCategoryId == cc.Id).Count();

                            AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.Id).Count();

                            
                            NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotApplicableCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 15 ) && entry.ComplianceCategoryId == cc.Id).Count();  /*|| entry.ComplianceStatusID == 18*/
                            CompliedbutDocPendingLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.Id).Count();


                            perFunctiondrilldown += "{" +
                                " id: 'low' + '" + cc.Name + "', " +
                                " name: 'Low', " +
                                " color: perFunctionChartColorScheme.low, " +
                                " data: [['Closed Timely', " + CompletedCountLow + "], " +
                                " ['Closed Delayed'," + AfterDueDatecountLow + "], " +
                                " ['Not completed'," + NotCompletedcountLow + "], " +
                                 " ['Not Applicable', " + NotApplicableCountLow + "],]," +                 
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'" + IsSatutoryInternalORALL + "','Functionbarchart','" + cc.Id + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FL_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountLow;
                            totalPieNotCompletedCount += NotCompletedcountLow;
                            totalPieAfterDueDatecount += AfterDueDatecountLow;
                            totalPieNotApplicablecount += NotApplicableCountLow;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingLow;

                            //pie drill down
                            totalPieAfterDueDateLOW += AfterDueDatecountLow;
                            totalPieCompletedLOW += CompletedCountLow;
                            totalPieNotCompletedLOW += NotCompletedcountLow;
                            totalPieNotApplicableLOW += NotApplicableCountLow;
                            totalPieCompliedbutDocPendingLOW += CompliedbutDocPendingLow;

                            //CRITICAL
                            long AfterDueDatecountCritical;
                            long CompletedCountCritical;
                            long NotCompletedcountCritical;
                            long NotApplicableCountCritical;
                            long CompliedbutDocPendingCritical;


                            if (ColoumnNames[5] == "Critical")
                            {
                                perFunctionCRITICAL += "{ name:'" + cc.Name + "', y: " + criticalcount + ",drilldown: 'critical' + '" + cc.Name + "',},";
                            }

                            //AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            //CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();


                            AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.Id).Count();

                             

                            NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotApplicableCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 15 ) && entry.ComplianceCategoryId == cc.Id).Count();   /*|| entry.ComplianceStatusID == 18*/
                            CompliedbutDocPendingCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.Id).Count();


                            perFunctiondrilldown += "{" +
                                " id: 'critical' + '" + cc.Name + "', " +
                                " name: 'Critical', " +
                                " color: perFunctionChartColorScheme.critical, " +
                                " data: [['Closed Timely', " + CompletedCountCritical + "], " +
                                " ['Closed Delayed'," + AfterDueDatecountCritical + "], " +
                                " ['Not completed'," + NotCompletedcountCritical + "], " +
                                 " ['Not Applicable', " + NotApplicableCountCritical + "],]," +                      
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'" + IsSatutoryInternalORALL + "','Functionbarchart','" + cc.Id + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FC_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountCritical;
                            totalPieNotCompletedCount += NotCompletedcountCritical;
                            totalPieAfterDueDatecount += AfterDueDatecountCritical;
                            totalPieNotApplicablecount += NotApplicableCountCritical;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingCritical;

                            //pie drill down
                            totalPieAfterDueDateCRITICAL += AfterDueDatecountCritical;
                            totalPieCompletedCRITICAL += CompletedCountCritical;
                            totalPieNotCompletedCRITICAL += NotCompletedcountCritical;
                            totalPieNotApplicableCRITICAL += NotApplicableCountCritical;
                            totalPieCompliedbutDocPendingCRITICAL += CompliedbutDocPendingCritical;

                        }
                    }
                    perFunctionCRITICAL += "],},";
                    perFunctionHIGH += "],},";
                    perFunctionMEDIUM += "],},";
                    perFunctionLOW += "],},],";

                    perFunctiondrilldown += "],},";
                    perFunctionChart = perFunctionCRITICAL + "" + perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + perFunctiondrilldown;

                    #endregion

                    #region  PieChartNotCompleted

                    long totalPieOverduecount = 0;
                    long totalPiePendingForReviewCount = 0;
                    long totalPieRejectedcount = 0;
                    long totalPieInProgresscount = 0;


                    long totalPieOverdueHIGH = 0;
                    long totalPieOverdueMEDIUM = 0;
                    long totalPieOverdueLOW = 0;
                    long totalPieOverdueCRITICAL = 0;

                    long totalPiePendingForReviewHIGH = 0;
                    long totalPiePendingForReviewMEDIUM = 0;
                    long totalPiePendingForReviewLOW = 0;
                    long totalPiePendingForReviewCRITICAL = 0;

                    long totalPieRejectedHIGH = 0;
                    long totalPieRejectedMEDIUM = 0;
                    long totalPieRejectedLOW = 0;
                    long totalPieRejectedCRITICAL = 0;

                    long totalPieInProgressHIGH = 0;
                    long totalPieInProgressMEDIUM = 0;
                    long totalPieInProgressLOW = 0;
                    long totalPieInProgressCRITICAL = 0;

                    

                    foreach (sp_ComplianceAssignedCategory_Result cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.Id.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {
                            //High
                            long OverduecountHigh;
                            long PendingForReviewCountHigh;
                            long RejectedcountHigh;
                            long InProgresscountHigh;

                            OverduecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 ) && entry.ComplianceCategoryId == cc.Id).Count();
                            PendingForReviewCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            RejectedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8) && entry.ComplianceCategoryId == cc.Id).Count();
                            InProgresscountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && ( entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();



                            //NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (
                            //entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 
                            //|| entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14
                            //|| entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18
                          //  || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                          //) && entry.ComplianceCategoryId == cc.Id).Count();
                           
                            //per Status pie Chart
                            totalPieOverduecount  += OverduecountHigh;
                            totalPiePendingForReviewCount += PendingForReviewCountHigh;
                            totalPieRejectedcount += RejectedcountHigh;
                            totalPieInProgresscount += InProgresscountHigh;
                            //pie drill down
                            totalPieOverdueHIGH += OverduecountHigh;
                            totalPiePendingForReviewHIGH += PendingForReviewCountHigh;
                            totalPieRejectedHIGH += RejectedcountHigh;
                            totalPieInProgressHIGH += InProgresscountHigh;
                            

                            //Medium
                            long OverduecountMedium;
                            long PendingForReviewCountMedium;
                            long RejectedcountMedium;
                            long InProgresscountMedium;
                            
                            OverduecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 ) && entry.ComplianceCategoryId == cc.Id).Count();
                            PendingForReviewCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            RejectedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 6  ) && entry.ComplianceCategoryId == cc.Id).Count();
                            InProgresscountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && ( entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();

                            //per Status pie Chart
                            totalPieOverduecount += OverduecountMedium;
                            totalPiePendingForReviewCount += PendingForReviewCountMedium;
                            totalPieRejectedcount += RejectedcountMedium;
                            totalPieInProgresscount += InProgresscountMedium;

                            //pie drill down 
                            totalPieOverdueMEDIUM += OverduecountMedium;
                            totalPiePendingForReviewMEDIUM += PendingForReviewCountMedium;
                            totalPieRejectedMEDIUM += RejectedcountMedium;
                            totalPieInProgressMEDIUM += InProgresscountMedium;

                            //Low
                            long OverduecountLow;
                            long PendingForReviewCountLow;
                            long RejectedcountLow;
                            long InProgresscountLow;
                            
                            OverduecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 ) && entry.ComplianceCategoryId == cc.Id).Count();
                            PendingForReviewCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            RejectedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 6  ) && entry.ComplianceCategoryId == cc.Id).Count();
                            InProgresscountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();


                            //per Status pie Chart
                            totalPieOverduecount += OverduecountLow;
                            totalPiePendingForReviewCount += PendingForReviewCountLow;
                            totalPieRejectedcount += RejectedcountLow;
                            totalPieInProgresscount += InProgresscountLow;

                            //pie drill down 
                            totalPieOverdueLOW += OverduecountLow;
                            totalPiePendingForReviewLOW += PendingForReviewCountLow;
                            totalPieRejectedLOW += RejectedcountLow;
                            totalPieInProgressLOW += InProgresscountLow;


                            //CRITICAL 
                            long OverduecountCritical;
                            long PendingForReviewCountCritical;
                            long RejectedcountCritical;
                            long InProgresscountCritical;

                            
                            OverduecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.Id).Count();
                            PendingForReviewCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            RejectedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 6 ) && entry.ComplianceCategoryId == cc.Id).Count();
                            InProgresscountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();
                            
                            //per Status pie Chart
                            totalPieOverduecount += OverduecountCritical;
                            totalPiePendingForReviewCount += PendingForReviewCountCritical;
                            totalPieRejectedcount += RejectedcountCritical;
                            totalPieInProgresscount += InProgresscountCritical;

                            //pie drill down 
                            totalPieOverdueCRITICAL += OverduecountCritical;
                            totalPiePendingForReviewCRITICAL += PendingForReviewCountCritical;
                            totalPieRejectedCRITICAL += RejectedcountCritical;
                            totalPieInProgressCRITICAL += InProgresscountCritical;

                        }
                    }


                    #endregion

                    #region Previous Working                 
                    perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Not Completed',y: " + totalPieNotCompletedCount + ",color: perStatusChartColorScheme.high,drilldown: 'notCompleted',},{name: 'Closed Delayed',y: " + totalPieAfterDueDatecount + ",color: perStatusChartColorScheme.medium,drilldown: 'afterDueDate',},{name: 'Closed Timely',y: " + totalPieCompletedcount + ",color: perStatusChartColorScheme.low,drilldown: 'inTime',},{name: 'Not Applicable',y: " + totalPieNotApplicablecount + ",color: perStatusChartColorScheme.NotApplicable,drilldown: 'notApplicable',}],}],";
                    
                    perFunctionPieChart += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                       " series: [" +

                                        " {id: 'inTime',name: 'Closed Timely',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPieCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // In time - Critical                               
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INC_Psummary')" +
                                        "},},},{name: 'High',y: " + totalPieCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // In time - High                   
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // In time - Medium                                
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INM_Psummary')" +
                                        "},},},{name: 'Low',y: " + totalPieCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // In time - Low                               
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'afterDueDate',name: 'Closed Delayed',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPieAfterDueDateCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // After Due Date - Critical
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieAfterDueDateHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // After Due Date - High
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieAfterDueDateMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        //  " // After Due Date - Medium
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieAfterDueDateLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // After Due Date - Low
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'notCompleted',name: 'Not Completed',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieNotCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Completed - Critical
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieNotCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Completed - High
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieNotCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Completed - Medium
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieNotCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Completed - Low
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +

                                         " },},}],}," +
                                        " {id: 'notApplicable',name: 'Not Applicable',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieNotApplicableCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Applicable - Critical
                                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieNotApplicableHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Applicable - High
                                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieNotApplicableMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Applicable - Medium
                                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieNotApplicableLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Applicable - Low
                                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAL_Psummary')" +

                                        

                                        " },},}],}],},";
                
                    #endregion


                    #region perFunctionPieNotcompleted Zomato
                    perFunctionPieNotcompleted = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Overdue',y: " + totalPieOverduecount + ",color: perStatusChartColorScheme.Overdue,drilldown: 'overdue',},{name: 'Pending For Review',y: " + totalPiePendingForReviewCount + ",color: perStatusChartColorScheme.Pendingforreview,drilldown: 'pendingForReview',},{name: 'In Progress',y: " + totalPieInProgresscount + ",color: perStatusChartColorScheme.InProgress,drilldown: 'inProgress',},{name: 'Rejected',y: " + totalPieRejectedcount + ",color: perStatusChartColorScheme.Rejected,drilldown: 'rejected',}],}],";

                    perFunctionPieNotcompleted += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                       " series: [" +

                                        " {id: 'overdue',name: 'Overdue',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPieOverdueCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // In time - Critical                               
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        "},},},{name: 'High',y: " + totalPieOverdueHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // In time - High                   
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieOverdueMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // In time - Medium                                
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        "},},},{name: 'Low',y: " + totalPieOverdueLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // In time - Low                               
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'pendingForReview',name: 'Pending For Review',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPiePendingForReviewCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // After Due Date - Critical
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPiePendingForReviewHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // After Due Date - High
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPiePendingForReviewMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        //  " // After Due Date - Medium
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPiePendingForReviewLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // After Due Date - Low
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'inProgress',name: 'In Progress',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieInProgressCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Completed - Critical
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieInProgressHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Completed - High
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieInProgressMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Completed - Medium
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieInProgressLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Completed - Low
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +


                                         " },},}],}," +
                                        " {id: 'rejected',name: 'Rejected',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieRejectedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Completed - Critical
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieRejectedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Completed - High
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieRejectedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Completed - Medium
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieRejectedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Completed - Low
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +


                                        " },},}],}],},";

                    LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : End perFunctionPieNotcompleted Zomato");
                    #endregion

                    #region perRiskChart
                    LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : Start perRiskChart");
                    perRiskChart = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +
                    // Not Completed - Critical

                    "y: " + totalPieNotCompletedCRITICAL + ",events:{click: function(e) {" +
                      " fpopulateddata('Critical','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCC_Rsummary')" +

                       "}}},{  " +

                    // Not Completed - High
                    "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                     " fpopulateddata('High','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCH_Rsummary')" +

                    "}}},{" +

                    // Not Completed - Medium
                    " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                    " fpopulateddata('Medium','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCM_Rsummary')" +

                    "}}},{  " +

                    // Not Completed - Low

                    "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                      " fpopulateddata('Low','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCL_Rsummary')" +



                    "}}}]},{name: 'Closed Delayed',color: perRiskStackedColumnChartColorScheme.closedelay,data: [{" +

                    // After Due Date - Critical
                    "y: " + totalPieAfterDueDateCRITICAL + ",events:{click: function(e) {" +
                    " fpopulateddata('Critical','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFC_Rsummary')" +

                    "}}},{   " +

                    // After Due Date - High

                    "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                    " fpopulateddata('High','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFH_Rsummary')" +

                    "}}},{   " +

                    // After Due Date - Medium
                    "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                    " fpopulateddata('Medium','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFM_Rsummary')" +

                    "}}},{   " +
                    // After Due Date - Low
                    "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                    " fpopulateddata('Low','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFL_Rsummary')" +





                    "}}}]},{name: 'Closed Timely',color: perRiskStackedColumnChartColorScheme.closetimely,data: [{" +

                    // In Time - Critical
                    "y: " + totalPieCompletedCRITICAL + ",events:{click: function(e) {" +
                    " fpopulateddata('Critical','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INC_Rsummary')" +

                     "}}},{  " +

                    // In Time - High
                    "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                    " fpopulateddata('High','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INH_Rsummary')" +

                    "}}},{  " +
                    // In Time - Medium
                    "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                    " fpopulateddata('Medium','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INM_Rsummary')" +

                    "}}},{  " +
                    // In Time - Low
                    "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                    " fpopulateddata('Low','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INL_Rsummary')" +



                    "}}}]}]";
                    LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : End perRiskChart");
                    #endregion

                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "F")
                        {
                            perRiskChart = tempperRiskChart;
                            //perImprisonmentPenaltyChart = tempperImprisonmentPenaltyChart;
                        }
                        else if (clickflag == "R")
                        {
                            perFunctionChart = tempperFunctionChart;
                            perFunctionPieChart = tempperFunctionPieChart;
                            LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : " + perFunctionPieNotcompleted);
                            perFunctionPieNotcompleted = tempperFunctionPieChartNotCompleted;

                            LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID+" : "+perFunctionPieNotcompleted);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                //LogLibrary.WriteErrorLog("GetManagementCompliancesSummaryZomato :" + ex);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetManagementCompliancesSummaryZomato(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<SP_GetManagementCompliancesSummary_Result> MasterQuery, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1)
        {
            try
            {
                string IsSatutoryInternalORALL = string.Empty;
                if (ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                {
                    IsSatutoryInternalORALL = "Statutory";
                }
                else if (ddlStatus.SelectedItem.Text == "Statutory")
                {
                    IsSatutoryInternalORALL = "StatutoryAll";
                }
                else if (ddlStatus.SelectedItem.Text == "Internal")
                {
                    IsSatutoryInternalORALL = "Internal";
                }
                string tempperFunctionChart = perFunctionChart;
                string tempperFunctionPieChart = perFunctionPieChart;
                string tempperRiskChart = perRiskChart;
                string tempperFunctionPieChartNotCompleted = perFunctionPieNotcompleted;
                //string tempperImprisonmentPenaltyChart = perImprisonmentPenaltyChart;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perFunctionChart = string.Empty;
                        perFunctionPieChart = string.Empty;
                        perRiskChart = string.Empty;
                        perFunctionPieNotcompleted = string.Empty;
                        //perImprisonmentPenaltyChart = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        perFunctionChart = string.Empty;
                    }
                    else if (clickflag == "R")
                    {
                        perRiskChart = string.Empty;
                        //perImprisonmentPenaltyChart = string.Empty;
                    }
                }
                else
                {
                    perFunctionChart = string.Empty;
                    perFunctionPieChart = string.Empty;
                    perRiskChart = string.Empty;
                    perFunctionPieNotcompleted = string.Empty;
                    //perImprisonmentPenaltyChart = string.Empty;
                }

                bool auditor = false;
                if (AuthenticationHelper.Role == "AUDT")
                {
                    auditor = true;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;
                    entities.Database.CommandTimeout = 180;

                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                    if (Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    List<SP_GetManagementCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate
                                             && row.ScheduledOn <= EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate && row.ScheduledOn <= EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.ScheduledOn <= EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                 || row.ScheduledOn <= EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                         || row.ScheduledOn <= FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.ScheduledOn >= FIFromDate
                                                         && row.ScheduledOn <= FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn <= FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    #region  PieChart
                    long totalPieCompletedcount = 0;
                    long totalPieNotCompletedCount = 0;
                    long totalPieAfterDueDatecount = 0;
                    long totalPieNotApplicablecount = 0;
                    long totalPieCompliedbutDocPendingcount = 0;


                    long totalPieCompletedHIGH = 0;
                    long totalPieCompletedMEDIUM = 0;
                    long totalPieCompletedLOW = 0;
                    long totalPieCompletedCRITICAL = 0;

                    long totalPieAfterDueDateHIGH = 0;
                    long totalPieAfterDueDateMEDIUM = 0;
                    long totalPieAfterDueDateLOW = 0;
                    long totalPieAfterDueDateCRITICAL = 0;

                    long totalPieNotCompletedHIGH = 0;
                    long totalPieNotCompletedMEDIUM = 0;
                    long totalPieNotCompletedLOW = 0;
                    long totalPieNotCompletedCRITICAL = 0;

                    long totalPieNotApplicableHIGH = 0;
                    long totalPieNotApplicableMEDIUM = 0;
                    long totalPieNotApplicableLOW = 0;
                    long totalPieNotApplicableCRITICAL = 0;


                    long totalPieCompliedbutDocPendingHIGH = 0;
                    long totalPieCompliedbutDocPendingMEDIUM = 0;
                    long totalPieCompliedbutDocPendingLOW = 0;
                    long totalPieCompliedbutDocPendingCRITICAL = 0;


                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("High", typeof(long));
                    table.Columns.Add("Medium", typeof(long));
                    table.Columns.Add("Low", typeof(long));
                    table.Columns.Add("Critical", typeof(long));

                    string listCategoryId = "";
                    List<sp_ComplianceAssignedCategory_Result> CatagoryList = new List<sp_ComplianceAssignedCategory_Result>();
                    if (approver == true)
                    {
                        CatagoryList = ComplianceCategoryManagement.GetNewAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, true);
                    }
                    else
                    {
                        CatagoryList = ComplianceCategoryManagement.GetNewAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist);
                    }

                    string perFunctionHIGH = "";
                    string perFunctionMEDIUM = "";
                    string perFunctionLOW = "";
                    string perFunctionCRITICAL = "";

                    string perFunctiondrilldown = "";

                    perFunctionCRITICAL = "series: [ {name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";
                    perFunctionHIGH = "{name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOW = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";



                    perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#f7f7f7'},select:{stroke: '#039',fill:'#f7f7f7'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

                    foreach (sp_ComplianceAssignedCategory_Result cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.Id.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {
                            //High
                            long AfterDueDatecountHigh;
                            long CompletedCountHigh;
                            long NotCompletedcountHigh;
                            long NotApplicableCountHigh;
                            long CompliedbutDocPendingHigh;

                            if (ColoumnNames[2] == "High")
                            {
                                perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                            }
                            //AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            //CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)  && entry.ComplianceCategoryId == cc.Id).Count();

                            AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 &&  entry.FilterStatus== "ClosedDelayed" && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.Id).Count();

                            NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotApplicableCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count(); /*|| entry.ComplianceStatusID == 18*/
                            CompliedbutDocPendingHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.Id).Count();
                            perFunctiondrilldown += "{" +
                                " id: 'high' + '" + cc.Name + "'," +
                                " name: 'High'," +
                                " color: perFunctionChartColorScheme.high," +
                                " data: [ ['Closed Timely', " + CompletedCountHigh + "], " +
                                " ['Closed Delayed', " + AfterDueDatecountHigh + "]," +
                                " ['Not completed', " + NotCompletedcountHigh + "]," +
                                " ['Not Applicable', " + NotApplicableCountHigh + "]," +
                                " ['Complied But Document Pending', " + CompliedbutDocPendingHigh + "],]," +
                                " events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'" + IsSatutoryInternalORALL + "','Functionbarchart','" + cc.Id + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FH_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountHigh;
                            totalPieNotCompletedCount += NotCompletedcountHigh;
                            totalPieAfterDueDatecount += AfterDueDatecountHigh;
                            totalPieNotApplicablecount += NotApplicableCountHigh;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingHigh;

                            //pie drill down
                            totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                            totalPieCompletedHIGH += CompletedCountHigh;
                            totalPieNotCompletedHIGH += NotCompletedcountHigh;
                            totalPieNotApplicableHIGH += NotApplicableCountHigh;
                            totalPieCompliedbutDocPendingHIGH += CompliedbutDocPendingHigh;

                            //Medium
                            long AfterDueDatecountMedium;
                            long CompletedCountMedium;
                            long NotCompletedcountMedium;
                            long NotApplicableCountMedium;
                            long CompliedbutDocPendingMedium;

                            if (ColoumnNames[3] == "Medium")
                            {
                                perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                            }
                            //AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            //CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();

                            AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.Id).Count();
                      
                            NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotApplicableCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count(); /*|| entry.ComplianceStatusID == 18*/
                            CompliedbutDocPendingMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.Id).Count();

                            perFunctiondrilldown += "{ " +
                                " id: 'mid' + '" + cc.Name + "'," +
                                " name: 'Medium'," +
                                " color: perFunctionChartColorScheme.medium," +
                                " data: [['Closed Timely', " + CompletedCountMedium + "]," +
                                " ['Closed Delayed', " + AfterDueDatecountMedium + "]," +
                                " ['Not completed', " + NotCompletedcountMedium + "], " +
                                 " ['Not Applicable', " + NotApplicableCountMedium + "]," +
                                " ['Complied But Document Pending', " + CompliedbutDocPendingMedium + "],]," +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "','Function'," + cc.Id + ",'" + IsSatutoryInternalORALL + "','Functionbarchart','" + cc.Id + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FM_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountMedium;
                            totalPieNotCompletedCount += NotCompletedcountMedium;
                            totalPieAfterDueDatecount += AfterDueDatecountMedium;
                            totalPieNotApplicablecount += NotApplicableCountMedium;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingMedium;

                            //pie drill down 
                            totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                            totalPieCompletedMEDIUM += CompletedCountMedium;
                            totalPieNotCompletedMEDIUM += NotCompletedcountMedium;
                            totalPieNotApplicableMEDIUM += NotApplicableCountMedium;
                            totalPieCompliedbutDocPendingMEDIUM += CompliedbutDocPendingMedium;

                            //Low
                            long AfterDueDatecountLow;
                            long CompletedCountLow;
                            long NotCompletedcountLow;
                            long NotApplicableCountLow;
                            long CompliedbutDocPendingLow;


                            if (ColoumnNames[4] == "Low")
                            {
                                perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                            }
                           
                            //AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            //CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();

                            AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.Id).Count();

                            NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotApplicableCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();  /*|| entry.ComplianceStatusID == 18*/
                            CompliedbutDocPendingLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.Id).Count();


                            perFunctiondrilldown += "{" +
                                " id: 'low' + '" + cc.Name + "', " +
                                " name: 'Low', " +
                                " color: perFunctionChartColorScheme.low, " +
                                " data: [['Closed Timely', " + CompletedCountLow + "], " +
                                " ['Closed Delayed'," + AfterDueDatecountLow + "], " +
                                " ['Not completed'," + NotCompletedcountLow + "], " +
                                 " ['Not Applicable', " + NotApplicableCountLow + "]," +
                                " ['Complied But Document Pending', " + CompliedbutDocPendingLow + "],]," +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'" + IsSatutoryInternalORALL + "','Functionbarchart','" + cc.Id + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FL_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountLow;
                            totalPieNotCompletedCount += NotCompletedcountLow;
                            totalPieAfterDueDatecount += AfterDueDatecountLow;
                            totalPieNotApplicablecount += NotApplicableCountLow;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingLow;

                            //pie drill down
                            totalPieAfterDueDateLOW += AfterDueDatecountLow;
                            totalPieCompletedLOW += CompletedCountLow;
                            totalPieNotCompletedLOW += NotCompletedcountLow;
                            totalPieNotApplicableLOW += NotApplicableCountLow;
                            totalPieCompliedbutDocPendingLOW += CompliedbutDocPendingLow;

                            //CRITICAL
                            long AfterDueDatecountCritical;
                            long CompletedCountCritical;
                            long NotCompletedcountCritical;
                            long NotApplicableCountCritical;
                            long CompliedbutDocPendingCritical;


                            if (ColoumnNames[5] == "Critical")
                            {
                                perFunctionCRITICAL += "{ name:'" + cc.Name + "', y: " + criticalcount + ",drilldown: 'critical' + '" + cc.Name + "',},";
                            }

                            //AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            //CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();


                            AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.Id).Count();
                    
                            NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotApplicableCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();   /*|| entry.ComplianceStatusID == 18*/
                            CompliedbutDocPendingCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.Id).Count();


                            perFunctiondrilldown += "{" +
                                " id: 'critical' + '" + cc.Name + "', " +
                                " name: 'Critical', " +
                                " color: perFunctionChartColorScheme.critical, " +
                                " data: [['Closed Timely', " + CompletedCountCritical + "], " +
                                " ['Closed Delayed'," + AfterDueDatecountCritical + "], " +
                                " ['Not completed'," + NotCompletedcountCritical + "], " +
                                 " ['Not Applicable', " + NotApplicableCountCritical + "]," +
                                " ['Complied But Document Pending', " + CompliedbutDocPendingCritical + "],]," +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'" + IsSatutoryInternalORALL + "','Functionbarchart','" + cc.Id + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FC_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountCritical;
                            totalPieNotCompletedCount += NotCompletedcountCritical;
                            totalPieAfterDueDatecount += AfterDueDatecountCritical;
                            totalPieNotApplicablecount += NotApplicableCountCritical;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingCritical;

                            //pie drill down
                            totalPieAfterDueDateCRITICAL += AfterDueDatecountCritical;
                            totalPieCompletedCRITICAL += CompletedCountCritical;
                            totalPieNotCompletedCRITICAL += NotCompletedcountCritical;
                            totalPieNotApplicableCRITICAL += NotApplicableCountCritical;
                            totalPieCompliedbutDocPendingCRITICAL += CompliedbutDocPendingCritical;

                        }
                    }
                    perFunctionCRITICAL += "],},";
                    perFunctionHIGH += "],},";
                    perFunctionMEDIUM += "],},";
                    perFunctionLOW += "],},],";

                    perFunctiondrilldown += "],},";
                    perFunctionChart = perFunctionCRITICAL + "" + perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + perFunctiondrilldown;

                    #endregion

                    #region  PieChartNotCompleted

                    long totalPieOverduecount = 0;
                    long totalPiePendingForReviewCount = 0;
                    long totalPieRejectedcount = 0;
                    long totalPieInProgresscount = 0;


                    long totalPieOverdueHIGH = 0;
                    long totalPieOverdueMEDIUM = 0;
                    long totalPieOverdueLOW = 0;
                    long totalPieOverdueCRITICAL = 0;

                    long totalPiePendingForReviewHIGH = 0;
                    long totalPiePendingForReviewMEDIUM = 0;
                    long totalPiePendingForReviewLOW = 0;
                    long totalPiePendingForReviewCRITICAL = 0;

                    long totalPieRejectedHIGH = 0;
                    long totalPieRejectedMEDIUM = 0;
                    long totalPieRejectedLOW = 0;
                    long totalPieRejectedCRITICAL = 0;

                    long totalPieInProgressHIGH = 0;
                    long totalPieInProgressMEDIUM = 0;
                    long totalPieInProgressLOW = 0;
                    long totalPieInProgressCRITICAL = 0;



                    foreach (sp_ComplianceAssignedCategory_Result cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.Id.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {
                            //High
                            long OverduecountHigh;
                            long PendingForReviewCountHigh;
                            long RejectedcountHigh;
                            long InProgresscountHigh;

                            OverduecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.Id).Count();
                            PendingForReviewCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            RejectedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.Id).Count();
                            InProgresscountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();



                            //per Status pie Chart
                            totalPieOverduecount += OverduecountHigh;
                            totalPiePendingForReviewCount += PendingForReviewCountHigh;
                            totalPieRejectedcount += RejectedcountHigh;
                            totalPieInProgresscount += InProgresscountHigh;
                            //pie drill down
                            totalPieOverdueHIGH += OverduecountHigh;
                            totalPiePendingForReviewHIGH += PendingForReviewCountHigh;
                            totalPieRejectedHIGH += RejectedcountHigh;
                            totalPieInProgressHIGH += InProgresscountHigh;


                            //Medium
                            long OverduecountMedium;
                            long PendingForReviewCountMedium;
                            long RejectedcountMedium;
                            long InProgresscountMedium;

                            OverduecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.Id).Count();
                            PendingForReviewCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            RejectedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.Id).Count();
                            InProgresscountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();

                            //per Status pie Chart
                            totalPieOverduecount += OverduecountMedium;
                            totalPiePendingForReviewCount += PendingForReviewCountMedium;
                            totalPieRejectedcount += RejectedcountMedium;
                            totalPieInProgresscount += InProgresscountMedium;

                            //pie drill down 
                            totalPieOverdueMEDIUM += OverduecountMedium;
                            totalPiePendingForReviewMEDIUM += PendingForReviewCountMedium;
                            totalPieRejectedMEDIUM += RejectedcountMedium;
                            totalPieInProgressMEDIUM += InProgresscountMedium;

                            //Low
                            long OverduecountLow;
                            long PendingForReviewCountLow;
                            long RejectedcountLow;
                            long InProgresscountLow;

                            OverduecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.Id).Count();
                            PendingForReviewCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            RejectedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.Id).Count();
                            InProgresscountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();


                            //per Status pie Chart
                            totalPieOverduecount += OverduecountLow;
                            totalPiePendingForReviewCount += PendingForReviewCountLow;
                            totalPieRejectedcount += RejectedcountLow;
                            totalPieInProgresscount += InProgresscountLow;

                            //pie drill down 
                            totalPieOverdueLOW += OverduecountLow;
                            totalPiePendingForReviewLOW += PendingForReviewCountLow;
                            totalPieRejectedLOW += RejectedcountLow;
                            totalPieInProgressLOW += InProgresscountLow;


                            //CRITICAL 
                            long OverduecountCritical;
                            long PendingForReviewCountCritical;
                            long RejectedcountCritical;
                            long InProgresscountCritical;


                            OverduecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.Id).Count();
                            PendingForReviewCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            RejectedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.Id).Count();
                            InProgresscountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();

                            //per Status pie Chart
                            totalPieOverduecount += OverduecountCritical;
                            totalPiePendingForReviewCount += PendingForReviewCountCritical;
                            totalPieRejectedcount += RejectedcountCritical;
                            totalPieInProgresscount += InProgresscountCritical;

                            //pie drill down 
                            totalPieOverdueCRITICAL += OverduecountCritical;
                            totalPiePendingForReviewCRITICAL += PendingForReviewCountCritical;
                            totalPieRejectedCRITICAL += RejectedcountCritical;
                            totalPieInProgressCRITICAL += InProgresscountCritical;

                        }
                    }


                    #endregion

                    #region Previous Working
                    //LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : start perFunctionPieChart");
                    perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Not Completed',y: " + totalPieNotCompletedCount + ",color: perStatusChartColorScheme.high,drilldown: 'notCompleted',},{name: 'Closed Delayed',y: " + totalPieAfterDueDatecount + ",color: perStatusChartColorScheme.medium,drilldown: 'afterDueDate',},{name: 'Closed Timely',y: " + totalPieCompletedcount + ",color: perStatusChartColorScheme.low,drilldown: 'inTime',},{name: 'Not Applicable',y: " + totalPieNotApplicablecount + ",color: perStatusChartColorScheme.NotApplicable,drilldown: 'notApplicable',},{name: 'Complied But Document Pending',y: " + totalPieCompliedbutDocPendingcount + ",color: perStatusChartColorScheme.Compiledbutdocumentpending,drilldown: 'compliedbutdocumentpending',}],}],";

                    perFunctionPieChart += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                       " series: [" +

                                        " {id: 'inTime',name: 'Closed Timely',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPieCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // In time - Critical                               
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INC_Psummary')" +
                                        "},},},{name: 'High',y: " + totalPieCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // In time - High                   
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // In time - Medium                                
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INM_Psummary')" +
                                        "},},},{name: 'Low',y: " + totalPieCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // In time - Low                               
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'afterDueDate',name: 'Closed Delayed',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPieAfterDueDateCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // After Due Date - Critical
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieAfterDueDateHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // After Due Date - High
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieAfterDueDateMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        //  " // After Due Date - Medium
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieAfterDueDateLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // After Due Date - Low
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'notCompleted',name: 'Not Completed',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieNotCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Completed - Critical
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieNotCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Completed - High
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieNotCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Completed - Medium
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieNotCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Completed - Low
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +

                                         " },},}],}," +
                                        " {id: 'notApplicable',name: 'Not Applicable',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieNotApplicableCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Applicable - Critical
                                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieNotApplicableHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Applicable - High
                                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieNotApplicableMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Applicable - Medium
                                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieNotApplicableLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Applicable - Low
                                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAL_Psummary')" +

                                         " },},}],}," +
                                        " {id: 'compliedbutdocumentpending',name: 'Complied But Document Pending',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieCompliedbutDocPendingCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Complied But Document Pending - Critical
                                        " fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieCompliedbutDocPendingHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Complied But Document Pending - High
                                        " fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieCompliedbutDocPendingMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Complied But Document Pending - Medium
                                        " fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieCompliedbutDocPendingLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Complied But Document Pending - Low
                                        " fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPL_Psummary')" +


                                        " },},}],}],},";
                    // LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : End perFunctionPieChart");
                    #endregion


                    #region perFunctionPieNotcompleted Zomato

                    //LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : start perFunctionPieNotcompleted Zomato");
                    perFunctionPieNotcompleted = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Overdue',y: " + totalPieOverduecount + ",color: perStatusChartColorScheme.Overdue,drilldown: 'overdue',},{name: 'Pending For Review',y: " + totalPiePendingForReviewCount + ",color: perStatusChartColorScheme.Pendingforreview,drilldown: 'pendingForReview',},{name: 'In Progress',y: " + totalPieInProgresscount + ",color: perStatusChartColorScheme.InProgress,drilldown: 'inProgress',},{name: 'Rejected',y: " + totalPieRejectedcount + ",color: perStatusChartColorScheme.Rejected,drilldown: 'rejected',}],}],";

                    perFunctionPieNotcompleted += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                       " series: [" +

                                        " {id: 'overdue',name: 'Overdue',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPieOverdueCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // In time - Critical                               
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        "},},},{name: 'High',y: " + totalPieOverdueHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // In time - High                   
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieOverdueMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // In time - Medium                                
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        "},},},{name: 'Low',y: " + totalPieOverdueLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // In time - Low                               
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'pendingForReview',name: 'Pending For Review',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPiePendingForReviewCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // After Due Date - Critical
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPiePendingForReviewHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // After Due Date - High
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPiePendingForReviewMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        //  " // After Due Date - Medium
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPiePendingForReviewLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // After Due Date - Low
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'inProgress',name: 'In Progress',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieInProgressCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Completed - Critical
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieInProgressHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Completed - High
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieInProgressMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Completed - Medium
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieInProgressLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Completed - Low
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +


                                         " },},}],}," +
                                        " {id: 'rejected',name: 'Rejected',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieRejectedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Completed - Critical
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieRejectedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Completed - High
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieRejectedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Completed - Medium
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieRejectedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Completed - Low
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +


                                        " },},}],}],},";

                    LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : End perFunctionPieNotcompleted Zomato");
                    #endregion

                    #region perRiskChart
                    LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : Start perRiskChart");
                    perRiskChart = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +
                    // Not Completed - Critical

                    "y: " + totalPieNotCompletedCRITICAL + ",events:{click: function(e) {" +
                      " fpopulateddata('Critical','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCC_Rsummary')" +

                       "}}},{  " +

                    // Not Completed - High
                    "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                     " fpopulateddata('High','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCH_Rsummary')" +

                    "}}},{" +

                    // Not Completed - Medium
                    " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                    " fpopulateddata('Medium','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCM_Rsummary')" +

                    "}}},{  " +

                    // Not Completed - Low

                    "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                      " fpopulateddata('Low','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCL_Rsummary')" +



                    "}}}]},{name: 'Closed Delayed',color: perRiskStackedColumnChartColorScheme.closedelay,data: [{" +

                    // After Due Date - Critical
                    "y: " + totalPieAfterDueDateCRITICAL + ",events:{click: function(e) {" +
                    " fpopulateddata('Critical','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFC_Rsummary')" +

                    "}}},{   " +

                    // After Due Date - High

                    "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                    " fpopulateddata('High','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFH_Rsummary')" +

                    "}}},{   " +

                    // After Due Date - Medium
                    "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                    " fpopulateddata('Medium','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFM_Rsummary')" +

                    "}}},{   " +
                    // After Due Date - Low
                    "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                    " fpopulateddata('Low','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFL_Rsummary')" +





                    "}}}]},{name: 'Closed Timely',color: perRiskStackedColumnChartColorScheme.closetimely,data: [{" +

                    // In Time - Critical
                    "y: " + totalPieCompletedCRITICAL + ",events:{click: function(e) {" +
                    " fpopulateddata('Critical','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INC_Rsummary')" +

                     "}}},{  " +

                    // In Time - High
                    "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                    " fpopulateddata('High','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INH_Rsummary')" +

                    "}}},{  " +
                    // In Time - Medium
                    "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                    " fpopulateddata('Medium','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INM_Rsummary')" +

                    "}}},{  " +
                    // In Time - Low
                    "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                    " fpopulateddata('Low','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INL_Rsummary')" +



                    "}}}]}]";
                    LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : End perRiskChart");
                    #endregion

                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "F")
                        {
                            perRiskChart = tempperRiskChart;
                            //perImprisonmentPenaltyChart = tempperImprisonmentPenaltyChart;
                        }
                        else if (clickflag == "R")
                        {
                            perFunctionChart = tempperFunctionChart;
                            perFunctionPieChart = tempperFunctionPieChart;
                            LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : " + perFunctionPieNotcompleted);
                            perFunctionPieNotcompleted = tempperFunctionPieChartNotCompleted;

                            LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : " + perFunctionPieNotcompleted);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                //LogLibrary.WriteErrorLog("GetManagementCompliancesSummaryZomato :" + ex);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void GetImprisonmentPenaltySummary(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<SP_GetManagementCompliancesSummary_Result> MasterQuery, string Minimum, string Maximum, string MinimumImprisonment, string MaximumImprisonment, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1)
        {
            try
            {
                string IsSatutoryInternalORALL = string.Empty;
                if (ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                {
                    IsSatutoryInternalORALL = "Statutory";
                }
                else if (ddlStatus.SelectedItem.Text == "Statutory")
                {
                    IsSatutoryInternalORALL = "StatutoryAll";
                }
                else if (ddlStatus.SelectedItem.Text == "Internal")
                {
                    IsSatutoryInternalORALL = "Internal";
                }
                string tempperImprisonmentPenaltyChart = perImprisonmentPenaltyChart;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perImprisonmentPenaltyChart = string.Empty;
                    }
                    else if (clickflag == "R")
                    {
                        perImprisonmentPenaltyChart = string.Empty;
                    }
                }
                else
                {
                    perImprisonmentPenaltyChart = string.Empty;
                }

                bool auditor = false;
                if (AuthenticationHelper.Role == "AUDT")
                {
                    auditor = true;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;
                    entities.Database.CommandTimeout = 180;

                    //Monetary Compliance
                    MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.NonComplianceType == 0 || entry.NonComplianceType == 2).ToList();
                    
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                    if (Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    if (Minimum !="Min")
                    {
                        int FixedMinimum = Convert.ToInt32(Minimum);
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.FixedMinimum >= FixedMinimum).ToList();
                    }
                    if (Maximum != "Max")
                    {
                        int FixedMaximum = Convert.ToInt32(Maximum);
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.FixedMaximum <= FixedMaximum).ToList();
                    }
                    if (MinimumImprisonment != "Min")
                    {
                        int MinimumYears = Convert.ToInt32(MinimumImprisonment);
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.MinimumYears >= MinimumYears).ToList();
                    }
                    if (MaximumImprisonment != "Max")
                    {
                        int MaximumYears = Convert.ToInt32(MaximumImprisonment);
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.MaximumYears <= MaximumYears).ToList();
                    }
                    List<SP_GetManagementCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate
                                             && row.ScheduledOn <= EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate && row.ScheduledOn <= EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.ScheduledOn <= EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                 || row.ScheduledOn <= EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                         || row.ScheduledOn <= FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.ScheduledOn >= FIFromDate
                                                         && row.ScheduledOn <= FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn <= FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    long totalPieCompletedHIGH = 0;
                    long totalPieCompletedMEDIUM = 0;
                    long totalPieCompletedLOW = 0;
                    long totalPieCompletedCRITICAL = 0;

                    long totalPieAfterDueDateHIGH = 0;
                    long totalPieAfterDueDateMEDIUM = 0;
                    long totalPieAfterDueDateLOW = 0;
                    long totalPieAfterDueDateCRITICAL = 0;

                    long totalPieNotCompletedHIGH = 0;
                    long totalPieNotCompletedMEDIUM = 0;
                    long totalPieNotCompletedLOW = 0;
                    long totalPieNotCompletedCRITICAL = 0;

                   

                    //totalPieAfterDueDateHIGH = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();
                    //totalPieCompletedHIGH = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15)).Count();


                    totalPieAfterDueDateHIGH = transactionsQuery.Where(entry => entry.Risk == 0 &&  entry.FilterStatus == "ClosedDelayed").Count();
                    totalPieCompletedHIGH = transactionsQuery.Where(entry => entry.Risk == 0 &&  (entry.FilterStatus == "ClosedTimely" || entry.FilterStatus == "Not Applicable")).Count();

                    totalPieNotCompletedHIGH = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                        || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18)).Count();


                    //totalPieAfterDueDateMEDIUM = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();
                    //totalPieCompletedMEDIUM = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15)).Count();

                    totalPieAfterDueDateMEDIUM = transactionsQuery.Where(entry => entry.Risk == 1 && entry.FilterStatus == "ClosedDelayed").Count();
                    totalPieCompletedMEDIUM = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.FilterStatus == "ClosedTimely" || entry.FilterStatus == "Not Applicable")).Count();

                    totalPieNotCompletedMEDIUM = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                        || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18)).Count();


                    //totalPieAfterDueDateLOW = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();
                    //totalPieCompletedLOW = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15)).Count();
                    totalPieAfterDueDateLOW = transactionsQuery.Where(entry => entry.Risk == 2 && entry.FilterStatus == "ClosedDelayed").Count();
                    totalPieCompletedLOW = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.FilterStatus == "ClosedTimely" || entry.FilterStatus == "Not Applicable")).Count();
                    totalPieNotCompletedLOW = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                        || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18)).Count();


                    //totalPieAfterDueDateCRITICAL = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();
                    //totalPieCompletedCRITICAL = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7 || entry.ComplianceStatusID == 15)).Count();
                    totalPieAfterDueDateCRITICAL = transactionsQuery.Where(entry => entry.Risk == 3 && entry.FilterStatus == "ClosedDelayed").Count();
                    totalPieCompletedCRITICAL = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.FilterStatus == "ClosedTimely" || entry.FilterStatus == "Not Applicable")).Count();
                    totalPieNotCompletedCRITICAL = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                        || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 18)).Count();


                    //ImprisonmentPenalty
                    perImprisonmentPenaltyChart = "series: [{name: 'Not Completed',color: perImprisonmentPenaltyStackedColumnChartColorScheme.high,data: [{" +
                   // Not Completed - Critical

                   "y: " + totalPieNotCompletedCRITICAL + ",events:{click: function(e) {" +
                     " fImprisonmentPenaltydata('Critical','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','ImprisonmentPenaltyBARchart'," + AuthenticationHelper.UserID + ",'" + approver + "','" + ddlMinimum.SelectedItem.Text + "','" + ddlMaximum.SelectedItem.Text + "','" + ddlMinimumImprisonment.SelectedItem.Text + "','" + ddlMaximumImprisonment.SelectedItem.Text + "','R_NCC_Rsummary')" +

                      "}}},{  " +

                   // Not Completed - High
                   "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                    " fImprisonmentPenaltydata('High','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','ImprisonmentPenaltyBARchart'," + AuthenticationHelper.UserID + ",'" + approver + "','" + ddlMinimum.SelectedItem.Text + "','" + ddlMaximum.SelectedItem.Text + "','" + ddlMinimumImprisonment.SelectedItem.Text + "','" + ddlMaximumImprisonment.SelectedItem.Text + "','R_NCH_Rsummary')" +

                   "}}},{" +

                   // Not Completed - Medium
                   " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                   " fImprisonmentPenaltydata('Medium','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','ImprisonmentPenaltyBARchart'," + AuthenticationHelper.UserID + ",'" + approver + "','" + ddlMinimum.SelectedItem.Text + "','" + ddlMaximum.SelectedItem.Text + "','" + ddlMinimumImprisonment.SelectedItem.Text + "','" + ddlMaximumImprisonment.SelectedItem.Text + "','R_NCM_Rsummary')" +

                   "}}},{  " +

                   // Not Completed - Low

                   "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                     " fImprisonmentPenaltydata('Low','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','ImprisonmentPenaltyBARchart'," + AuthenticationHelper.UserID + ",'" + approver + "','" + ddlMinimum.SelectedItem.Text + "','" + ddlMaximum.SelectedItem.Text + "','" + ddlMinimumImprisonment.SelectedItem.Text + "','" + ddlMaximumImprisonment.SelectedItem.Text + "','R_NCL_Rsummary')" +


                   "}}}]},{name: 'Closed Delayed',color: perImprisonmentPenaltyStackedColumnChartColorScheme.closedelay,data: [{" +

                   // After Due Date - Critical
                   "y: " + totalPieAfterDueDateCRITICAL + ",events:{click: function(e) {" +
                   " fImprisonmentPenaltydata('Critical','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','ImprisonmentPenaltyBARchart'," + AuthenticationHelper.UserID + ",'" + approver + "','" + ddlMinimum.SelectedItem.Text + "','" + ddlMaximum.SelectedItem.Text + "','" + ddlMinimumImprisonment.SelectedItem.Text + "','" + ddlMaximumImprisonment.SelectedItem.Text + "','R_AFC_Rsummary')" +

                     "}}},{   " +
                   // After Due Date - High

                   "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                   " fImprisonmentPenaltydata('High','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','ImprisonmentPenaltyBARchart'," + AuthenticationHelper.UserID + ",'" + approver + "','" + ddlMinimum.SelectedItem.Text + "','" + ddlMaximum.SelectedItem.Text + "','" + ddlMinimumImprisonment.SelectedItem.Text + "','" + ddlMaximumImprisonment.SelectedItem.Text + "','R_AFH_Rsummary')" +

                   "}}},{   " +

                   // After Due Date - Medium
                   "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                   " fImprisonmentPenaltydata('Medium','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','ImprisonmentPenaltyBARchart'," + AuthenticationHelper.UserID + ",'" + approver + "','" + ddlMinimum.SelectedItem.Text + "','" + ddlMaximum.SelectedItem.Text + "','" + ddlMinimumImprisonment.SelectedItem.Text + "','" + ddlMaximumImprisonment.SelectedItem.Text + "','R_AFM_Rsummary')" +

                   "}}},{   " +
                   // After Due Date - Low
                   "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                   " fImprisonmentPenaltydata('Low','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','ImprisonmentPenaltyBARchart'," + AuthenticationHelper.UserID + ",'" + approver + "','" + ddlMinimum.SelectedItem.Text + "','" + ddlMaximum.SelectedItem.Text + "','" + ddlMinimumImprisonment.SelectedItem.Text + "','" + ddlMaximumImprisonment.SelectedItem.Text + "','R_AFL_Rsummary')" +


                   "}}}]},{name: 'Closed Timely',color: perImprisonmentPenaltyStackedColumnChartColorScheme.closetimely,data: [{" +
                   // In Time - Critical
                   "y: " + totalPieCompletedCRITICAL + ",events:{click: function(e) {" +
                   " fImprisonmentPenaltydata('Critical','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','ImprisonmentPenaltyBARchart'," + AuthenticationHelper.UserID + ",'" + approver + "','" + ddlMinimum.SelectedItem.Text + "','" + ddlMinimum.SelectedItem.Text + "','" + ddlMinimumImprisonment.SelectedItem.Text + "','" + ddlMaximumImprisonment.SelectedItem.Text + "','R_INC_Rsummary')" +
                        
                   "}}},{  " +
                   // In Time - High
                   "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                   " fImprisonmentPenaltydata('High','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','ImprisonmentPenaltyBARchart'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INH_Rsummary')" +

                   "}}},{  " +
                   // In Time - Medium
                   "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                   " fImprisonmentPenaltydata('Medium','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','ImprisonmentPenaltyBARchart'," + AuthenticationHelper.UserID + ",'" + approver + "','" + ddlMinimum.SelectedItem.Text + "','" + ddlMinimum.SelectedItem.Text + "','" + ddlMinimumImprisonment.SelectedItem.Text + "','" + ddlMaximumImprisonment.SelectedItem.Text + "','R_INM_Rsummary')" +

                   "}}},{  " +
                   // In Time - Low
                   "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                   " fImprisonmentPenaltydata('Low','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','ImprisonmentPenaltyBARchart'," + AuthenticationHelper.UserID + ",'" + approver + "','" + ddlMinimum.SelectedItem.Text + "','" + ddlMinimum.SelectedItem.Text + "','" + ddlMinimumImprisonment.SelectedItem.Text + "','" + ddlMaximumImprisonment.SelectedItem.Text + "','R_INL_Rsummary')" +

              
                 
                   "}}}]}]";

                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "F")
                        {
                            perImprisonmentPenaltyChart = tempperImprisonmentPenaltyChart;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
       
        public void GetManagementCompliancesSummaryAddedNotCompliedStatus(int customerid, List<long> Branchlist, int CustomerbranchID, string clickflag, DateTime FIFromDate, DateTime FIEndDate, List<SP_GetManagementCompliancesSummary_Result> MasterQuery, bool approver = false, DateTime? FromDate = null, DateTime? EndDateF = null, int userId = -1)
        {
            try
            {
                string IsSatutoryInternalORALL = string.Empty;
                if (ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                {
                    IsSatutoryInternalORALL = "Statutory";
                }
                else if (ddlStatus.SelectedItem.Text == "Statutory")
                {
                    IsSatutoryInternalORALL = "StatutoryAll";
                }
                else if (ddlStatus.SelectedItem.Text == "Internal")
                {
                    IsSatutoryInternalORALL = "Internal";
                }
                string tempperFunctionChart = perFunctionChart;
                string tempperFunctionPieChart = perFunctionPieChart;
                string tempperRiskChart = perRiskChart;
                string tempperFunctionPieChartNotCompleted = perFunctionPieNotcompleted;
                //string tempperImprisonmentPenaltyChart = perImprisonmentPenaltyChart;
                if (1 == 2)
                {
                    //if graph wise filter is required make 1==1
                    if (clickflag == "T")
                    {
                        perFunctionChart = string.Empty;
                        perFunctionPieChart = string.Empty;
                        perRiskChart = string.Empty;
                        perFunctionPieNotcompleted = string.Empty;
                        //perImprisonmentPenaltyChart = string.Empty;
                    }
                    else if (clickflag == "F")
                    {
                        perFunctionChart = string.Empty;
                    }
                    else if (clickflag == "R")
                    {
                        perRiskChart = string.Empty;
                        //perImprisonmentPenaltyChart = string.Empty;
                    }
                }
                else
                {
                    perFunctionChart = string.Empty;
                    perFunctionPieChart = string.Empty;
                    perRiskChart = string.Empty;
                    perFunctionPieNotcompleted = string.Empty;
                    //perImprisonmentPenaltyChart = string.Empty;
                }

                bool auditor = false;
                if (AuthenticationHelper.Role == "AUDT")
                {
                    auditor = true;
                }
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = MasterQuery;
                    entities.Database.CommandTimeout = 180;

                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => entry.ScheduledOn >= FIFromDate && entry.ScheduledOn <= FIEndDate).ToList();
                        }
                    }

                    if (Branchlist.Count > 0)
                    {
                        MasterManagementCompliancesSummaryQuery = MasterManagementCompliancesSummaryQuery.Where(entry => Branchlist.Contains(entry.CustomerBranchID)).ToList();
                    }

                    List<SP_GetManagementCompliancesSummary_Result> transactionsQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    DateTime EndDate = DateTime.Today.Date;
                    DateTime? PassEndValue;
                    if (FromDate != null && EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate
                                             && row.ScheduledOn <= EndDateF
                                             select row).ToList();

                        PassEndValue = EndDateF;
                    }
                    else if (FromDate != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid
                                             && row.ScheduledOn >= FromDate && row.ScheduledOn <= EndDate
                                             select row).ToList();

                        PassEndValue = EndDate;
                    }
                    else if (EndDateF != null)
                    {
                        transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                             where row.CustomerID == customerid && row.ScheduledOn <= EndDateF
                                             select row).ToList();


                        PassEndValue = EndDateF;
                    }
                    else
                    {
                        if (approver == true)
                        {
                            transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                 where row.CustomerID == customerid
                                                 && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                 || row.ScheduledOn <= EndDate)
                                                 select row).ToList();
                        }
                        else
                        {
                            if (FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                            {
                                if (AuthenticationHelper.Role == "AUDT")
                                {
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && (row.ComplianceStatusID == 5 || row.ComplianceStatusID == 4
                                                         || row.ScheduledOn <= FIEndDate)
                                                         select row).ToList();
                                }
                                else
                                {
                                    //For Financial Year
                                    transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                         where row.CustomerID == customerid
                                                         && row.ScheduledOn >= FIFromDate
                                                         && row.ScheduledOn <= FIEndDate
                                                         select row).ToList();
                                }
                            }
                            else
                            {
                                transactionsQuery = (from row in MasterManagementCompliancesSummaryQuery
                                                     where row.CustomerID == customerid
                                                     && row.ScheduledOn <= FIEndDate
                                                     select row).ToList();
                            }
                        }
                        PassEndValue = FIEndDate;
                    }
                    if (AuthenticationHelper.Role != "AUDT")
                    {
                        if (FromDate == null && EndDateF == null && FIFromDate.Year != 1900 && FIEndDate.Year != 1900)
                        {
                            PassEndValue = FIEndDate;
                            FromDate = FIFromDate;
                        }
                    }
                    if (approver == true)
                    {
                        transactionsQuery = transactionsQuery.Where(entry => entry.RoleID == 6 && entry.UserID == AuthenticationHelper.UserID).ToList();
                    }
                    transactionsQuery = transactionsQuery.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                    #region  PieChart
                    long totalPieCompletedcount = 0;
                    long totalPieNotCompletedCount = 0;
                    long totalPieAfterDueDatecount = 0;
                    long totalPieNotCompliedCount = 0;
                    long totalPieNotApplicablecount = 0;
                    long totalPieCompliedbutDocPendingcount = 0;


                    long totalPieCompletedHIGH = 0;
                    long totalPieCompletedMEDIUM = 0;
                    long totalPieCompletedLOW = 0;
                    long totalPieCompletedCRITICAL = 0;

                    long totalPieAfterDueDateHIGH = 0;
                    long totalPieAfterDueDateMEDIUM = 0;
                    long totalPieAfterDueDateLOW = 0;
                    long totalPieAfterDueDateCRITICAL = 0;

                    long totalPieNotCompletedHIGH = 0;
                    long totalPieNotCompletedMEDIUM = 0;
                    long totalPieNotCompletedLOW = 0;
                    long totalPieNotCompletedCRITICAL = 0;

                    long totalPieNotApplicableHIGH = 0;
                    long totalPieNotApplicableMEDIUM = 0;
                    long totalPieNotApplicableLOW = 0;
                    long totalPieNotApplicableCRITICAL = 0;


                    long totalPieCompliedbutDocPendingHIGH = 0;
                    long totalPieCompliedbutDocPendingMEDIUM = 0;
                    long totalPieCompliedbutDocPendingLOW = 0;
                    long totalPieCompliedbutDocPendingCRITICAL = 0;


                    long totalPieNotCompliedHIGH = 0;
                    long totalPieNotCompliedMEDIUM = 0;
                    long totalPieNotCompliedLOW = 0;
                    long totalPieNotCompliedCRITICAL = 0;

                    long highcount;
                    long mediumCount;
                    long lowcount;
                    long criticalcount;
                    long totalcount;
                    DataTable table = new DataTable();
                    table.Columns.Add("ID", typeof(int));
                    table.Columns.Add("Catagory", typeof(string));
                    table.Columns.Add("High", typeof(long));
                    table.Columns.Add("Medium", typeof(long));
                    table.Columns.Add("Low", typeof(long));
                    table.Columns.Add("Critical", typeof(long));

                    string listCategoryId = "";
                    List<sp_ComplianceAssignedCategory_Result> CatagoryList = new List<sp_ComplianceAssignedCategory_Result>();
                    if (approver == true)
                    {
                        CatagoryList = ComplianceCategoryManagement.GetNewAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist, true);
                    }
                    else
                    {
                        CatagoryList = ComplianceCategoryManagement.GetNewAll(AuthenticationHelper.UserID, CustomerbranchID, Branchlist);
                    }

                    string perFunctionHIGH = "";
                    string perFunctionMEDIUM = "";
                    string perFunctionLOW = "";
                    string perFunctionCRITICAL = "";

                    string perFunctiondrilldown = "";

                    perFunctionCRITICAL = "series: [ {name: 'Critical',id: 'Critical',color: perFunctionChartColorScheme.critical, data: [";
                    perFunctionHIGH = "{name: 'High',id: 'High',color: perFunctionChartColorScheme.high, data: [";
                    perFunctionMEDIUM = "{name: 'Medium',id: 'Medium',color: perFunctionChartColorScheme.medium, data: [";
                    perFunctionLOW = "{name: 'Low',id: 'Low',color: perFunctionChartColorScheme.low, data: [";



                    perFunctiondrilldown = "drilldown:{drillUpButton:{relativeTo: 'spacingBox',position:{y: 0,x:0},theme:{fill: 'white','stroke-width': 1,stroke: 'silver',r: 0,states:{hover:{fill:'#f7f7f7'},select:{stroke: '#039',fill:'#f7f7f7'}}}},activeDataLabelStyle:{textDecoration: 'none',color: 'gray',}, series: [";
                    string[] ColoumnNames = table.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

                    foreach (sp_ComplianceAssignedCategory_Result cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.Id.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {
                            //High
                            long AfterDueDatecountHigh;
                            long CompletedCountHigh;
                            long NotCompletedcountHigh;
                            long NotCompliedcountHigh;
                            long NotApplicableCountHigh;
                            long CompliedbutDocPendingHigh;

                            if (ColoumnNames[2] == "High")
                            {
                                perFunctionHIGH += "{ name:'" + cc.Name + "', y: " + highcount + ",drilldown: 'high' + '" + cc.Name + "',},";
                            }
                           

                            //AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            //CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();

                            AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompliedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceStatusID == 17 && entry.ComplianceCategoryId == cc.Id).Count();
                            NotApplicableCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count(); /*|| entry.ComplianceStatusID == 18*/
                            CompliedbutDocPendingHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.Id).Count();

                            perFunctiondrilldown += "{" +
                                " id: 'high' + '" + cc.Name + "'," +
                                " name: 'High'," +
                                " color: perFunctionChartColorScheme.high," +
                                " data: [ ['Closed Timely', " + CompletedCountHigh + "], " +
                                " ['Closed Delayed', " + AfterDueDatecountHigh + "]," +
                                " ['Not complied', " + NotCompliedcountHigh + "]," +
                                " ['Not completed', " + NotCompletedcountHigh + "]," +
                                " ['Not Applicable', " + NotApplicableCountHigh + "],]," +
                                //" ['Complied But Document Pending', " + CompliedbutDocPendingHigh + "],]," +
                                " events: {" +
                                " click: function(e){" +
                                " fpopulateddata(e.point.name,'High'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'" + IsSatutoryInternalORALL + "','Functionbarchart','" + cc.Id + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FH_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountHigh;
                            totalPieNotCompletedCount += NotCompletedcountHigh;
                            totalPieAfterDueDatecount += AfterDueDatecountHigh;
                            totalPieNotCompliedCount += NotCompliedcountHigh;
                            totalPieNotApplicablecount += NotApplicableCountHigh;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingHigh;

                            //pie drill down
                            totalPieAfterDueDateHIGH += AfterDueDatecountHigh;
                            totalPieCompletedHIGH += CompletedCountHigh;
                            totalPieNotCompletedHIGH += NotCompletedcountHigh;
                            totalPieNotCompliedHIGH += NotCompliedcountHigh;
                            totalPieNotApplicableHIGH += NotApplicableCountHigh;
                            totalPieCompliedbutDocPendingHIGH += CompliedbutDocPendingHigh;

                            //Medium
                            long AfterDueDatecountMedium;
                            long CompletedCountMedium;
                            long NotCompletedcountMedium;
                            long NotCompliedcountMedium;
                            long NotApplicableCountMedium;
                            long CompliedbutDocPendingMedium;

                            if (ColoumnNames[3] == "Medium")
                            {
                                perFunctionMEDIUM += "{ name:'" + cc.Name + "', y: " + mediumCount + ",drilldown: 'mid' + '" + cc.Name + "',},";
                            }
                        

                            //AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            //CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();
                            AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompliedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceStatusID == 17 && entry.ComplianceCategoryId == cc.Id).Count();
                            NotApplicableCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count(); /*|| entry.ComplianceStatusID == 18*/
                            CompliedbutDocPendingMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.Id).Count();

                            perFunctiondrilldown += "{ " +
                                " id: 'mid' + '" + cc.Name + "'," +
                                " name: 'Medium'," +
                                " color: perFunctionChartColorScheme.medium," +
                                " data: [['Closed Timely', " + CompletedCountMedium + "]," +
                                " ['Closed Delayed', " + AfterDueDatecountMedium + "]," +
                                " ['Not complied', " + NotCompliedcountMedium + "], " +
                                " ['Not completed', " + NotCompletedcountMedium + "], " +
                                 " ['Not Applicable', " + NotApplicableCountMedium + "],]," +
                                //" ['Complied But Document Pending', " + CompliedbutDocPendingMedium + "],]," +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Medium'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "','Function'," + cc.Id + ",'" + IsSatutoryInternalORALL + "','Functionbarchart','" + cc.Id + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FM_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountMedium;
                            totalPieNotCompletedCount += NotCompletedcountMedium;
                            totalPieAfterDueDatecount += AfterDueDatecountMedium;
                            totalPieNotCompliedCount += NotCompliedcountMedium;
                            totalPieNotApplicablecount += NotApplicableCountMedium;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingMedium;

                            //pie drill down 
                            totalPieAfterDueDateMEDIUM += AfterDueDatecountMedium;
                            totalPieCompletedMEDIUM += CompletedCountMedium;
                            totalPieNotCompletedMEDIUM += NotCompletedcountMedium;
                            totalPieNotCompliedMEDIUM += NotCompliedcountMedium;
                            totalPieNotApplicableMEDIUM += NotApplicableCountMedium;
                            totalPieCompliedbutDocPendingMEDIUM += CompliedbutDocPendingMedium;

                            //Low
                            long AfterDueDatecountLow;
                            long CompletedCountLow;
                            long NotCompletedcountLow;
                            long NotCompliedcountLow;
                            long NotApplicableCountLow;
                            long CompliedbutDocPendingLow;


                            if (ColoumnNames[4] == "Low")
                            {
                                perFunctionLOW += "{ name:'" + cc.Name + "', y: " + lowcount + ",drilldown: 'low' + '" + cc.Name + "',},";
                            }

                            //AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            //CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();

                            AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompliedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 17) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotApplicableCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();  /*|| entry.ComplianceStatusID == 18*/
                            CompliedbutDocPendingLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.Id).Count();


                            perFunctiondrilldown += "{" +
                                " id: 'low' + '" + cc.Name + "', " +
                                " name: 'Low', " +
                                " color: perFunctionChartColorScheme.low, " +
                                " data: [['Closed Timely', " + CompletedCountLow + "], " +
                                " ['Closed Delayed'," + AfterDueDatecountLow + "], " +
                                 " ['Not complied'," + NotCompliedcountLow + "], " +
                                " ['Not completed'," + NotCompletedcountLow + "], " +
                                 " ['Not Applicable', " + NotApplicableCountLow + "],]," +
                                //" ['Complied But Document Pending', " + CompliedbutDocPendingLow + "],]," +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Low'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'" + IsSatutoryInternalORALL + "','Functionbarchart','" + cc.Id + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FL_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountLow;
                            totalPieNotCompletedCount += NotCompletedcountLow;
                            totalPieAfterDueDatecount += AfterDueDatecountLow;
                            totalPieNotCompliedCount += NotCompliedcountLow;
                            totalPieNotApplicablecount += NotApplicableCountLow;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingLow;

                            //pie drill down
                            totalPieAfterDueDateLOW += AfterDueDatecountLow;
                            totalPieCompletedLOW += CompletedCountLow;
                            totalPieNotCompletedLOW += NotCompletedcountLow;
                            totalPieNotCompliedLOW += NotCompliedcountLow;
                            totalPieNotApplicableLOW += NotApplicableCountLow;
                            totalPieCompliedbutDocPendingLOW += CompliedbutDocPendingLow;

                            //CRITICAL
                            long AfterDueDatecountCritical;
                            long CompletedCountCritical;
                            long NotCompletedcountCritical;
                            long NotCompliedcountCritical;
                            long NotApplicableCountCritical;
                            long CompliedbutDocPendingCritical;


                            if (ColoumnNames[5] == "Critical")
                            {
                                perFunctionCRITICAL += "{ name:'" + cc.Name + "', y: " + criticalcount + ",drilldown: 'critical' + '" + cc.Name + "',},";
                            }                          
                            AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.FilterStatus == "ClosedDelayed" && entry.ComplianceCategoryId == cc.Id).Count();
                            CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.FilterStatus == "ClosedTimely" && entry.ComplianceCategoryId == cc.Id).Count();
                            //AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) && entry.ComplianceCategoryId == cc.Id).Count();
                            //CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10
                            || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            NotCompliedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceStatusID == 17 && entry.ComplianceCategoryId == cc.Id).Count();
                            NotApplicableCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 15) && entry.ComplianceCategoryId == cc.Id).Count();   /*|| entry.ComplianceStatusID == 18*/
                            CompliedbutDocPendingCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 19) && entry.ComplianceCategoryId == cc.Id).Count();


                            perFunctiondrilldown += "{" +
                                " id: 'critical' + '" + cc.Name + "', " +
                                " name: 'Critical', " +
                                " color: perFunctionChartColorScheme.critical, " +
                                " data: [['Closed Timely', " + CompletedCountCritical + "], " +
                                " ['Closed Delayed'," + AfterDueDatecountCritical + "], " +
                                 " ['Not complied', " + NotCompliedcountCritical + "]," +
                                " ['Not completed'," + NotCompletedcountCritical + "], " +
                                 " ['Not Applicable', " + NotApplicableCountCritical + "],]," +
                                //" ['Complied But Document Pending', " + CompliedbutDocPendingCritical + "],]," +
                                " events: {" +
                                " click: function(e){ " +
                                " fpopulateddata(e.point.name,'Critical'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function'," + cc.Id + ",'" + IsSatutoryInternalORALL + "','Functionbarchart','" + cc.Id + "'," + AuthenticationHelper.UserID + ",'" + approver + "','FC_summary')}" +
                                " }},";

                            //per Status pie Chart
                            totalPieCompletedcount += CompletedCountCritical;
                            totalPieNotCompletedCount += NotCompletedcountCritical;
                            totalPieAfterDueDatecount += AfterDueDatecountCritical;
                            totalPieNotCompliedCount += NotCompliedcountCritical;
                            totalPieNotApplicablecount += NotApplicableCountCritical;
                            totalPieCompliedbutDocPendingcount += CompliedbutDocPendingCritical;

                            //pie drill down
                            totalPieAfterDueDateCRITICAL += AfterDueDatecountCritical;
                            totalPieCompletedCRITICAL += CompletedCountCritical;
                            totalPieNotCompletedCRITICAL += NotCompletedcountCritical;
                            totalPieNotCompliedCRITICAL += NotCompliedcountCritical;
                            totalPieNotApplicableCRITICAL += NotApplicableCountCritical;
                            totalPieCompliedbutDocPendingCRITICAL += CompliedbutDocPendingCritical;

                        }
                    }
                    perFunctionCRITICAL += "],},";
                    perFunctionHIGH += "],},";
                    perFunctionMEDIUM += "],},";
                    perFunctionLOW += "],},],";

                    perFunctiondrilldown += "],},";
                    perFunctionChart = perFunctionCRITICAL + "" + perFunctionHIGH + "" + perFunctionMEDIUM + "" + perFunctionLOW + perFunctiondrilldown;

                    #endregion

                    #region  PieChartNotCompleted

                    long totalPieOverduecount = 0;
                    long totalPiePendingForReviewCount = 0;
                    long totalPieRejectedcount = 0;
                    long totalPieInProgresscount = 0;


                    long totalPieOverdueHIGH = 0;
                    long totalPieOverdueMEDIUM = 0;
                    long totalPieOverdueLOW = 0;
                    long totalPieOverdueCRITICAL = 0;

                    long totalPiePendingForReviewHIGH = 0;
                    long totalPiePendingForReviewMEDIUM = 0;
                    long totalPiePendingForReviewLOW = 0;
                    long totalPiePendingForReviewCRITICAL = 0;

                    long totalPieRejectedHIGH = 0;
                    long totalPieRejectedMEDIUM = 0;
                    long totalPieRejectedLOW = 0;
                    long totalPieRejectedCRITICAL = 0;

                    long totalPieInProgressHIGH = 0;
                    long totalPieInProgressMEDIUM = 0;
                    long totalPieInProgressLOW = 0;
                    long totalPieInProgressCRITICAL = 0;



                    foreach (sp_ComplianceAssignedCategory_Result cc in CatagoryList)
                    {
                        listCategoryId += ',' + cc.Id.ToString();
                        highcount = transactionsQuery.Where(entry => entry.Risk == 0 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        mediumCount = transactionsQuery.Where(entry => entry.Risk == 1 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        lowcount = transactionsQuery.Where(entry => entry.Risk == 2 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        criticalcount = transactionsQuery.Where(entry => entry.Risk == 3 && entry.ComplianceCategoryId == cc.Id && entry.ComplianceStatusID != 11).Count();
                        totalcount = highcount + mediumCount + lowcount + criticalcount;

                        if (totalcount != 0)
                        {
                            //High
                            long OverduecountHigh;
                            long PendingForReviewCountHigh;
                            long RejectedcountHigh;
                            long InProgresscountHigh;

                            OverduecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.Id).Count();
                            PendingForReviewCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            RejectedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.Id).Count();
                            InProgresscountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();



                            //per Status pie Chart
                            totalPieOverduecount += OverduecountHigh;
                            totalPiePendingForReviewCount += PendingForReviewCountHigh;
                            totalPieRejectedcount += RejectedcountHigh;
                            totalPieInProgresscount += InProgresscountHigh;
                            //pie drill down
                            totalPieOverdueHIGH += OverduecountHigh;
                            totalPiePendingForReviewHIGH += PendingForReviewCountHigh;
                            totalPieRejectedHIGH += RejectedcountHigh;
                            totalPieInProgressHIGH += InProgresscountHigh;


                            //Medium
                            long OverduecountMedium;
                            long PendingForReviewCountMedium;
                            long RejectedcountMedium;
                            long InProgresscountMedium;

                            OverduecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.Id).Count();
                            PendingForReviewCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            RejectedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.Id).Count();
                            InProgresscountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();

                            //per Status pie Chart
                            totalPieOverduecount += OverduecountMedium;
                            totalPiePendingForReviewCount += PendingForReviewCountMedium;
                            totalPieRejectedcount += RejectedcountMedium;
                            totalPieInProgresscount += InProgresscountMedium;

                            //pie drill down 
                            totalPieOverdueMEDIUM += OverduecountMedium;
                            totalPiePendingForReviewMEDIUM += PendingForReviewCountMedium;
                            totalPieRejectedMEDIUM += RejectedcountMedium;
                            totalPieInProgressMEDIUM += InProgresscountMedium;

                            //Low
                            long OverduecountLow;
                            long PendingForReviewCountLow;
                            long RejectedcountLow;
                            long InProgresscountLow;

                            OverduecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.Id).Count();
                            PendingForReviewCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            RejectedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.Id).Count();
                            InProgresscountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();


                            //per Status pie Chart
                            totalPieOverduecount += OverduecountLow;
                            totalPiePendingForReviewCount += PendingForReviewCountLow;
                            totalPieRejectedcount += RejectedcountLow;
                            totalPieInProgresscount += InProgresscountLow;

                            //pie drill down 
                            totalPieOverdueLOW += OverduecountLow;
                            totalPiePendingForReviewLOW += PendingForReviewCountLow;
                            totalPieRejectedLOW += RejectedcountLow;
                            totalPieInProgressLOW += InProgresscountLow;


                            //CRITICAL 
                            long OverduecountCritical;
                            long PendingForReviewCountCritical;
                            long RejectedcountCritical;
                            long InProgresscountCritical;


                            OverduecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 12 || entry.ComplianceStatusID == 13 || entry.ComplianceStatusID == 14) && entry.ComplianceCategoryId == cc.Id).Count();
                            PendingForReviewCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11 || entry.ComplianceStatusID == 16 || entry.ComplianceStatusID == 18) && entry.ComplianceCategoryId == cc.Id).Count();
                            RejectedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 6) && entry.ComplianceCategoryId == cc.Id).Count();
                            InProgresscountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 10) && entry.ComplianceCategoryId == cc.Id).Count();

                            //per Status pie Chart
                            totalPieOverduecount += OverduecountCritical;
                            totalPiePendingForReviewCount += PendingForReviewCountCritical;
                            totalPieRejectedcount += RejectedcountCritical;
                            totalPieInProgresscount += InProgresscountCritical;

                            //pie drill down 
                            totalPieOverdueCRITICAL += OverduecountCritical;
                            totalPiePendingForReviewCRITICAL += PendingForReviewCountCritical;
                            totalPieRejectedCRITICAL += RejectedcountCritical;
                            totalPieInProgressCRITICAL += InProgresscountCritical;

                        }
                    }


                    #endregion

                    #region Previous Working
                    //LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : start perFunctionPieChart");
                    perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',}," +
                        "data: [" +
                        "{name: 'Not Completed',y: " + totalPieNotCompletedCount + ",color: perStatusChartColorScheme.high,drilldown: 'notCompleted',},"+
                         "{name: 'Not Complied',y: " + totalPieNotCompliedCount + ",color: perStatusChartColorScheme.NotComplied,drilldown: 'notComplied',}," +
                        " {name: 'Closed Delayed',y: " + totalPieAfterDueDatecount + ",color: perStatusChartColorScheme.medium,drilldown: 'afterDueDate',}," +
                        "{ name: 'Closed Timely',y: " + totalPieCompletedcount + ",color: perStatusChartColorScheme.low,drilldown: 'inTime',},"+
                       " { name: 'Not Applicable',y: " + totalPieNotApplicablecount + ",color: perStatusChartColorScheme.NotApplicable,drilldown: 'notApplicable',}],}],";

                    perFunctionPieChart += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                       " series: [" +

                                        " {id: 'inTime',name: 'Closed Timely',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPieCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // In time - Critical                               
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INC_Psummary')" +
                                        "},},},{name: 'High',y: " + totalPieCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // In time - High                   
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // In time - Medium                                
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INM_Psummary')" +
                                        "},},},{name: 'Low',y: " + totalPieCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // In time - Low                               
                                        " fpopulateddata(e.point.name,'In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_INL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'afterDueDate',name: 'Closed Delayed',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPieAfterDueDateCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // After Due Date - Critical
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieAfterDueDateHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // After Due Date - High
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieAfterDueDateMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        //  " // After Due Date - Medium
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieAfterDueDateLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // After Due Date - Low
                                        " fpopulateddata(e.point.name,'After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_AFL_Psummary')" +


                                        " },},}],}," +
                                        " {id: 'notComplied',name: 'Not Complied',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPieNotCompliedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Complied - Critical
                                        " fpopulateddata(e.point.name,'Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCOC_Psummary')" +
                                            " },},},{name: 'High',y: " + totalPieNotCompliedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Complied - High
                                        " fpopulateddata(e.point.name,'Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCOH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieNotCompliedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Complied - Medium
                                        " fpopulateddata(e.point.name,'Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCOM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieNotCompliedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Complied - Low
                                        " fpopulateddata(e.point.name,'Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCOL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'notCompleted',name: 'Not Completed',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieNotCompletedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Completed - Critical
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieNotCompletedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Completed - High
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieNotCompletedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Completed - Medium
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieNotCompletedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Completed - Low
                                        " fpopulateddata(e.point.name,'Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +

                                         " },},}],}," +
                                        " {id: 'notApplicable',name: 'Not Applicable',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieNotApplicableCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Applicable - Critical
                                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieNotApplicableHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Applicable - High
                                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieNotApplicableMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Applicable - Medium
                                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieNotApplicableLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Applicable - Low
                                        " fpopulateddata(e.point.name,'Not Applicable'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NAL_Psummary')" +

                                        // " },},}],}," +
                                        //" {id: 'compliedbutdocumentpending',name: 'Complied But Document Pending',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieCompliedbutDocPendingCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        //// " // Complied But Document Pending - Critical
                                        //" fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPC_Psummary')" +
                                        //" },},},{name: 'High',y: " + totalPieCompliedbutDocPendingHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        //// " // Complied But Document Pending - High
                                        //" fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPH_Psummary')" +
                                        //" },},},{name: 'Medium',y: " + totalPieCompliedbutDocPendingMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        //// " // Complied But Document Pending - Medium
                                        //" fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPM_Psummary')" +
                                        //" },},},{name: 'Low',y: " + totalPieCompliedbutDocPendingLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        //// " // Complied But Document Pending - Low
                                        //" fpopulateddata(e.point.name,'Complied But Document Pending'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','Statutory','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_CBDPL_Psummary')" +


                                        " },},}],}],},";
                    // LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : End perFunctionPieChart");
                    #endregion


                    #region perFunctionPieNotcompleted Zomato

                    //LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : start perFunctionPieNotcompleted Zomato");
                    perFunctionPieNotcompleted = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Overdue',y: " + totalPieOverduecount + ",color: perStatusChartColorScheme.Overdue,drilldown: 'overdue',},{name: 'Pending For Review',y: " + totalPiePendingForReviewCount + ",color: perStatusChartColorScheme.Pendingforreview,drilldown: 'pendingForReview',},{name: 'In Progress',y: " + totalPieInProgresscount + ",color: perStatusChartColorScheme.InProgress,drilldown: 'inProgress',},{name: 'Rejected',y: " + totalPieRejectedcount + ",color: perStatusChartColorScheme.Rejected,drilldown: 'rejected',}],}],";

                    perFunctionPieNotcompleted += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                       " series: [" +

                                        " {id: 'overdue',name: 'Overdue',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPieOverdueCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // In time - Critical                               
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        "},},},{name: 'High',y: " + totalPieOverdueHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // In time - High                   
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieOverdueMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // In time - Medium                                
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        "},},},{name: 'Low',y: " + totalPieOverdueLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // In time - Low                               
                                        " fpopulateddata(e.point.name,'Overdue'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'pendingForReview',name: 'Pending For Review',cursor: 'pointer',data: [{name: 'Critical',y: " + totalPiePendingForReviewCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // After Due Date - Critical
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPiePendingForReviewHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // After Due Date - High
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPiePendingForReviewMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        //  " // After Due Date - Medium
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPiePendingForReviewLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // After Due Date - Low
                                        " fpopulateddata(e.point.name,'Pending For Review'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +



                                        " },},}],}," +
                                        " {id: 'inProgress',name: 'In Progress',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieInProgressCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Completed - Critical
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieInProgressHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Completed - High
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieInProgressMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Completed - Medium
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieInProgressLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Completed - Low
                                        " fpopulateddata(e.point.name,'In Progress'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +


                                         " },},}],}," +
                                        " {id: 'rejected',name: 'Rejected',cursor: 'pointer',data: [ {name: 'Critical',y: " + totalPieRejectedCRITICAL + ",color: perStatusChartColorScheme.critical,events:{click: function(e) { " +
                                        // " // Not Completed - Critical
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCC_Psummary')" +
                                        " },},},{name: 'High',y: " + totalPieRejectedHIGH + ",color: perStatusChartColorScheme.high,events:{click: function(e) { " +
                                        // " // Not Completed - High
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCH_Psummary')" +
                                        " },},},{name: 'Medium',y: " + totalPieRejectedMEDIUM + ",color: perStatusChartColorScheme.medium,events:{click: function(e) { " +
                                        // " // Not Completed - Medium
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCM_Psummary')" +
                                        " },},},{name: 'Low',y: " + totalPieRejectedLOW + ",color: perStatusChartColorScheme.low,events:{click: function(e) { " +
                                        // " // Not Completed - Low
                                        " fpopulateddata(e.point.name,'Rejected'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','FunctionPiechart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','P_NCL_Psummary')" +


                                        " },},}],}],},";

                    LogLibrary.WriteErrorLog(AuthenticationHelper.CustomerID + " : End perFunctionPieNotcompleted Zomato");
                    #endregion

                    #region perRiskChart

                    perRiskChart = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +

                        // Not Completed - Critical

                        "y: " + totalPieNotCompletedCRITICAL + ",events:{click: function(e) {" +
                            " fpopulateddata('Critical','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCC_Rsummary')" +
                            "}}},{  " +

                        // Not Completed - High
                        "y: " + totalPieNotCompletedHIGH + ",events:{click: function(e) {" +
                        " fpopulateddata('High','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCH_Rsummary')" +

                        "}}},{" +

                        // Not Completed - Medium
                        " y: " + totalPieNotCompletedMEDIUM + ",events:{click: function(e) {" +
                        " fpopulateddata('Medium','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCM_Rsummary')" +

                        "}}},{  " +

                        // Not Completed - Low

                        "y: " + totalPieNotCompletedLOW + ",events:{click: function(e) {" +
                            " fpopulateddata('Low','Not completed'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCL_Rsummary')" +




                        "}}}]},{name: 'Not Complied',color: perRiskStackedColumnChartColorScheme.NotComplied,data: [{" +

                        // Not Complied - Critical
                        "y: " + totalPieNotCompliedCRITICAL + ",events:{click: function(e) {" +
                        " fpopulateddata('Critical','Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCOC_Rsummary')" +

                            "}}},{   " +

                        // Not Complied - High

                        "y: " + totalPieNotCompliedHIGH + ",events:{click: function(e) { " +
                        " fpopulateddata('High','Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCOH_Rsummary')" +

                        "}}},{   " +

                        // Not Complied - Medium
                        "y: " + totalPieNotCompliedMEDIUM + ",events:{click: function(e) {" +
                        " fpopulateddata('Medium','Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCOM_Rsummary')" +

                        "}}},{   " +
                        // Not Complied - Low
                        "y: " + totalPieNotCompliedLOW + ",events:{click: function(e) {" +
                        " fpopulateddata('Low','Not complied'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_NCOL_Rsummary')" +



                        "}}}]},{name: 'Closed Delayed',color: perRiskStackedColumnChartColorScheme.closedelay,data: [{" +

                        // After Due Date - Critical
                        "y: " + totalPieAfterDueDateCRITICAL + ",events:{click: function(e) {" +
                        " fpopulateddata('Critical','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFC_Rsummary')" +

                        "}}},{   " +

                        // After Due Date - High

                        "y: " + totalPieAfterDueDateHIGH + ",events:{click: function(e) { " +
                        " fpopulateddata('High','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFH_Rsummary')" +

                        "}}},{   " +

                        // After Due Date - Medium
                        "y: " + totalPieAfterDueDateMEDIUM + ",events:{click: function(e) {" +
                        " fpopulateddata('Medium','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFM_Rsummary')" +

                        "}}},{   " +
                        // After Due Date - Low
                        "y: " + totalPieAfterDueDateLOW + ",events:{click: function(e) {" +
                        " fpopulateddata('Low','After due date'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_AFL_Rsummary')" +



                        "}}}]},{name: 'Closed Timely',color: perRiskStackedColumnChartColorScheme.closetimely,data: [{" +
                        // In Time - Critical
                        "y: " + totalPieCompletedCRITICAL + ",events:{click: function(e) {" +
                        " fpopulateddata('Critical','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INC_Rsummary')" +

                        "}}},{  " +

                        // In Time - High
                        "y: " + totalPieCompletedHIGH + ",events:{click: function(e) {" +
                        " fpopulateddata('High','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INH_Rsummary')" +

                        "}}},{  " +
                        // In Time - Medium
                        "y: " + totalPieCompletedMEDIUM + ",events:{click: function(e) {" +
                        " fpopulateddata('Medium','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INM_Rsummary')" +

                        "}}},{  " +
                        // In Time - Low
                        "y: " + totalPieCompletedLOW + ",events:{click: function(e) {" +
                        " fpopulateddata('Low','In Time'," + customerid + "," + CustomerbranchID + ",'" + FromDate + "','" + PassEndValue + "' ,'Function','0','" + IsSatutoryInternalORALL + "','RiskBARchart','" + listCategoryId.Trim(',') + "'," + AuthenticationHelper.UserID + ",'" + approver + "','R_INL_Rsummary')" +


                        "}}}]}]";
                    #endregion

                    if (1 == 2)
                    {
                        //if graph wise filter is required make 1==1
                        if (clickflag == "F")
                        {
                            perRiskChart = tempperRiskChart;
                            //perImprisonmentPenaltyChart = tempperImprisonmentPenaltyChart;
                        }
                        else if (clickflag == "R")
                        {
                            perFunctionChart = tempperFunctionChart;
                            perFunctionPieChart = tempperFunctionPieChart;
                           
                            perFunctionPieNotcompleted = tempperFunctionPieChartNotCompleted;

                           
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                //LogLibrary.WriteErrorLog("GetManagementCompliancesSummaryZomato :" + ex);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Grading Report
        private void BindMonth(int year)
        {
            try
            {
                ddlmonthsGrading.DataTextField = "Name";
                ddlmonthsGrading.DataValueField = "ID";
                int selectedMonth = 0;
                List<NameValue> months;
                if (year == DateTime.UtcNow.Year)
                {
                    months = Enumerations.GetAll<Month>().Where(entry => entry.ID <= DateTime.UtcNow.Month).ToList();
                    selectedMonth = months.Count;
                }
                else
                {
                    months = Enumerations.GetAll<Month>();
                    selectedMonth = 1;
                }
                ddlmonthsGrading.DataSource = months;
                ddlmonthsGrading.DataBind();
                ddlmonthsGrading.SelectedValue = Convert.ToString(selectedMonth);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindStatus()
        {
            try
            {
                customizedChecklist = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "MGMT_Checklist");
                if (customizedChecklist)
                {
                    ddlStatus.Items.Add(new ListItem("Statutory Excluding Checklist", "0"));
                    ddlStatus.Items.Add(new ListItem("Internal", "1"));
                    ddlStatus.Items.Add(new ListItem("Statutory", "2"));

                    ddlStatus.ClearSelection(); //making sure the previous selection has been cleared
                    ddlStatus.Items.FindByValue("2").Selected = true;

                }
                else
                {
                    ddlStatus.Items.Add(new ListItem("Statutory Excluding Checklist", "0"));
                    ddlStatus.Items.Add(new ListItem("Internal", "1"));

                    ddlStatus.ClearSelection(); //making sure the previous selection has been cleared
                    ddlStatus.Items.FindByValue("0").Selected = true;
                }

                //customizedChecklist = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "MGMT_Checklist");
                //if (customizedChecklist)
                //{
                //    ddlStatus.Items.Add(new ListItem("Statutory", "0"));
                //    ddlStatus.Items.Add(new ListItem("Internal", "1"));
                //    ddlStatus.Items.Add(new ListItem("Statutory All", "2"));

                //    ddlStatus.ClearSelection(); //making sure the previous selection has been cleared
                //    ddlStatus.Items.FindByValue("2").Selected = true;

                //}
                //else
                //{
                //    ddlStatus.Items.Add(new ListItem("Statutory", "0"));
                //    ddlStatus.Items.Add(new ListItem("Internal", "1"));

                //    ddlStatus.ClearSelection(); //making sure the previous selection has been cleared
                //    ddlStatus.Items.FindByValue("0").Selected = true;
                //}                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindYear()
        {
            try
            {
                int CurrentYear = DateTime.Now.Year;
                for (int i = 1; i < 10; ++i)
                {
                    System.Web.UI.WebControls.ListItem tmp = new System.Web.UI.WebControls.ListItem();
                    tmp.Value = CurrentYear.ToString();
                    tmp.Text = CurrentYear.ToString();
                    ddlYearGrading.Items.Add(tmp);
                    CurrentYear = DateTime.Now.AddYears(-i).Year;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnGradingSearch_Click(object sender, EventArgs e)
        {
            try
            {
                
                BindGradingReportSummaryTree(false);
                DisplayGradingChange = CaseManagement.CheckForClientNew(Convert.ToInt32(AuthenticationHelper.CustomerID), "ShowGradingchange");
                UpdatePanel1.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFiltergradingscroll1", "fsetscroll();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        private void BindGradingReportSummaryTree(bool isappGrading)
        {
            try
            {
                string statutoryinternal = string.Empty;
                string perRahul = string.Empty;
                int year = Convert.ToInt32(ddlYearGrading.SelectedValue);
                int month = Convert.ToInt32(ddlmonthsGrading.SelectedValue);
                int cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int customerBranchID = -1;
                if (TreeGraddingReport.SelectedValue != "-1")
                {
                    customerBranchID = Convert.ToInt32(TreeGraddingReport.SelectedValue);
                }
                if (ddlStatus.SelectedItem.Text == "Statutory" || ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                {
                    perRahul = string.Empty;
                    #region Statutory Grading Report
                    statutoryinternal = "Statutory";
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int uid = Convert.ToInt32(AuthenticationHelper.UserID);
                        int period = 12;
                        if (ddlPeriodGrading.SelectedValue != "12")
                        {
                            period = Convert.ToInt32(ddlPeriodGrading.SelectedValue);
                        }
                        else
                        {
                            int numberofmonthdifference = 12;
                            if (isappGrading)
                            {
                                numberofmonthdifference = (from row in entities.SP_GetGradingReportPeriod("S", "APPR", uid)
                                                           select (int)row.Numberofmonth).FirstOrDefault();
                            }
                            else
                            {
                                numberofmonthdifference = (from row in entities.SP_GetGradingReportPeriod("S", "MGMT", uid)
                                                           select (int)row.Numberofmonth).FirstOrDefault();
                            }
                            if (numberofmonthdifference <= 3)
                            {
                                ddlPeriodGrading.SelectedValue = "3";
                            }
                            else if (numberofmonthdifference > 3 && numberofmonthdifference < 9)
                            {
                                ddlPeriodGrading.SelectedValue = "6";
                            }
                            else
                            {
                                ddlPeriodGrading.SelectedValue = "12";
                            }
                            period = Convert.ToInt32(ddlPeriodGrading.SelectedValue);
                        }
                        var MasterQuery = (from row in entities.GradingPreFillDatas
                                           where row.CustomerID == cuid
                                           && row.UserID == uid && row.IsStatutory == "S"
                                           select row).ToList();
                        if (MasterQuery.Count == 0)
                        {
                            bool sucess = StatutoryGrading.FillStatutoryGrading(cuid, uid);
                            if (sucess)
                            {
                                MasterQuery = (from row in entities.GradingPreFillDatas
                                               where row.CustomerID == cuid
                                               && row.UserID == uid && row.IsStatutory == "S"
                                               select row).ToList();
                            }
                        }

                        if (customerBranchID != -1)
                        {
                            entities.Database.CommandTimeout = 180;
                            var branchIDs = (from row in entities.SP_RLCS_GetChildBranchesFromParentBranch((int)customerBranchID, (int)cuid)
                                             select row).ToList();


                            MasterQuery = MasterQuery.Where(entry => branchIDs.Contains((int)entry.LocationID)).ToList();
                        }
                        var transactionsQuery = MasterQuery;
                        DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).AddMonths(1);

                        List<Tuple<string, long, string>> list = new List<Tuple<string, long, string>>();

                        DataTable table = new DataTable();
                        table.Columns.Add("Location", typeof(string));
                        table.Columns.Add("Node", typeof(long));
                        table.Columns.Add("PNode", typeof(long));
                        table.Columns.Add("LocationID", typeof(long));
                        table.Columns.Add("Year", typeof(long));
                        for (int i = 1; i <= period; i++)
                        {
                            list.Add(new Tuple<string, long, string>("C" + EndDate.AddMonths(-i).Month, Convert.ToInt64(EndDate.AddMonths(-i).ToString("yyyy")), EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy")));
                            table.Columns.Add(EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy"), typeof(long));
                        }

                        var alist = transactionsQuery.Where(entry => entry.Year == year).ToList();
                        foreach (var item in alist)
                        {
                            DataRow tableRow = table.NewRow();
                            tableRow["Location"] = item.LocationName;
                            tableRow["Node"] = item.Cnode;
                            tableRow["PNode"] = item.Pnode;
                            tableRow["LocationID"] = item.LocationID;
                            tableRow["Year"] = item.Year;
                            foreach (var item1 in list)
                            {
                                var columname = item1.Item1;
                                var fYear = item1.Item2;
                                var griddisplayname = item1.Item3;
                                var transactionfilter = transactionsQuery.Where(aa => aa.Year == fYear && aa.LocationID == item.LocationID).FirstOrDefault();

                                if (columname == "C1")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C1;
                                }
                                else if (columname == "C2")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C2;
                                }
                                else if (columname == "C3")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C3;
                                }
                                else if (columname == "C4")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C4;
                                }
                                else if (columname == "C5")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C5;
                                }
                                else if (columname == "C6")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C6;
                                }
                                else if (columname == "C7")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C7;
                                }
                                else if (columname == "C8")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C8;
                                }
                                else if (columname == "C9")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C9;
                                }
                                else if (columname == "C10")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C10;
                                }
                                else if (columname == "C11")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C11;
                                }
                                else if (columname == "C12")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C12;
                                }
                            }
                            table.Rows.Add(tableRow);
                        }

                        System.Text.StringBuilder stringbuilder = new System.Text.StringBuilder();
                        stringbuilder.Append(@"<table id='basic' width='100%' cellpadding='5' cellspacing='5' ><tr><td class='locationheadbg'>Legal Entity / Location </td>");
                        for (int i = 0; i < table.Columns.Count; i++)
                        {
                            string temp2 = table.Columns[i].ColumnName.ToString();
                            if (temp2 != "Location" && temp2 != "Node" && temp2 != "PNode" && temp2 != "LocationID" && temp2 != "Year")
                            {
                                string fstring = string.Empty;
                                List<string> headerval = table.Columns[i].ToString().Split('/').ToList();
                                if (headerval[0] == "1")
                                {
                                    fstring = "Jan" + " " + headerval[1];
                                }
                                else if (headerval[0] == "2")
                                {
                                    fstring = "Feb" + " " + headerval[1];
                                }
                                else if (headerval[0] == "3")
                                {
                                    fstring = "Mar" + " " + headerval[1];
                                }
                                else if (headerval[0] == "4")
                                {
                                    fstring = "Apr" + " " + headerval[1];
                                }
                                else if (headerval[0] == "5")
                                {
                                    fstring = "May" + " " + headerval[1];
                                }
                                else if (headerval[0] == "6")
                                {
                                    fstring = "Jun" + " " + headerval[1];
                                }
                                else if (headerval[0] == "7")
                                {
                                    fstring = "Jul" + " " + headerval[1];
                                }
                                else if (headerval[0] == "8")
                                {
                                    fstring = "Aug" + " " + headerval[1];
                                }
                                else if (headerval[0] == "9")
                                {
                                    fstring = "Sep" + " " + headerval[1];
                                }
                                else if (headerval[0] == "10")
                                {
                                    fstring = "Oct" + " " + headerval[1];
                                }
                                else if (headerval[0] == "11")
                                {
                                    fstring = "Nov" + " " + headerval[1];
                                }
                                else if (headerval[0] == "12")
                                {
                                    fstring = "Dec" + " " + headerval[1];
                                }
                                stringbuilder.Append("<td class='locationheadbg'>" + fstring + "</td>");
                            }
                        }
                        stringbuilder.Append("</tr>");


                        for (int i = 0; i < table.Rows.Count; i++)
                        {
                            DataRow dr = table.Rows[i];
                            stringbuilder.Append("<tr data-node-id=" + Convert.ToString(dr[1]) + " data-node-pid=" + Convert.ToString(dr[2]) + " >" +
                            " <td class='locationheadLocationbg'>" + Convert.ToString(dr[0]) + "</td>");


                            #region 5
                            List<string> splitvalue = table.Columns[5].ToString().Split('/').ToList();
                            string years = 20 + "" + splitvalue[1];
                            DateTime date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                            DateTime date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));
                            if (Convert.ToString(dr[5]) == "4")
                            {
                                //stringbuilder.Append("<td style='background-color: #e6e6e6; border:1px solid #c4c4c4;' onclick='GradingReportPopPup(" + customerID + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'>" + Convert.ToString(dr[5]) + "</td>");
                                stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[5]) == "3")
                            {
                                stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[5]) == "2")
                            {
                                stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[5]) == "1")
                            {
                                stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            #endregion

                            #region 6
                            splitvalue = null;
                            date1 = new DateTime();
                            date2 = new DateTime();
                            splitvalue = table.Columns[6].ToString().Split('/').ToList();

                            years = 20 + "" + splitvalue[1];
                            date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                            date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                            if (Convert.ToString(dr[6]) == "4")
                            {
                                stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[6]) == "3")
                            {
                                stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[6]) == "2")
                            {
                                stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[6]) == "1")
                            {
                                stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            #endregion

                            #region 7
                            splitvalue = null;
                            date1 = new DateTime();
                            date2 = new DateTime();
                            splitvalue = table.Columns[7].ToString().Split('/').ToList();

                            years = 20 + "" + splitvalue[1];
                            date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                            date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                            if (Convert.ToString(dr[7]) == "4")
                            {
                                stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[7]) == "3")
                            {
                                stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[7]) == "2")
                            {
                                stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[7]) == "1")
                            {
                                stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            #endregion
                            if (period == 12 || period == 6)
                            {
                                #region 8
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[8].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));



                                if (Convert.ToString(dr[8]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[8]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[8]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[8]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 9
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[9].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[9]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[9]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[9]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[9]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 10

                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[10].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[10]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[10]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[10]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[10]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion
                            }
                            if (period == 12)
                            {
                                #region 11
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[11].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[11]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[11]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[11]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[11]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 12
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[12].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[12]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[12]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[12]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[12]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 13
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[13].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[13]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[13]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[13]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[13]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 14
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[14].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[14]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[14]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[14]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[14]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 15
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[15].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[15]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[15]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[15]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[15]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 16
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[16].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[16]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[16]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[16]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[16]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion
                            }
                            stringbuilder.Append("</tr>");
                        }
                        stringbuilder.Append("</table>");
                        perRahul = stringbuilder.ToString().Trim();

                        g_rading.InnerHtml = perRahul.ToString();
                        //= perRahul.ToString();
                    }//using End
                    #endregion
                }
                else
                {
                    perRahul = string.Empty;
                    #region Internal Grading Report
                    statutoryinternal = "Internal";


                    bool IsApproverInternal = false;

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        if (AuthenticationHelper.Role.Equals("MGMT") || AuthenticationHelper.ComplianceProductType == 3)
                        {
                            IsApproverInternal = false;
                        }
                        else
                        {
                            var GetApproverInternal = (from row in entities.InternalComplianceAssignments
                                                       where row.RoleID == 6
                                                       && row.UserID == AuthenticationHelper.UserID
                                                       select row).ToList();
                            if (GetApproverInternal.Count > 0)
                            {
                                IsApprover = true;
                                IsApproverInternal = true;
                            }
                            else
                            {
                                IsApprover = false;
                                IsApproverInternal = false;
                            }
                        }


                        int uid = Convert.ToInt32(AuthenticationHelper.UserID);
                        int period = 12;
                        if (ddlPeriodGrading.SelectedValue != "12")
                        {
                            period = Convert.ToInt32(ddlPeriodGrading.SelectedValue);
                        }
                        else
                        {
                            int numberofmonthdifference = 12;
                            if (IsApproverInternal)
                            {
                                numberofmonthdifference = (from row in entities.SP_GetGradingReportPeriod("S", "APPR", uid)
                                                           select (int)row.Numberofmonth).FirstOrDefault();
                            }
                            else
                            {
                                numberofmonthdifference = (from row in entities.SP_GetGradingReportPeriod("S", "MGMT", uid)
                                                           select (int)row.Numberofmonth).FirstOrDefault();
                            }
                            if (numberofmonthdifference <= 3)
                            {
                                ddlPeriodGrading.SelectedValue = "3";
                            }
                            else if (numberofmonthdifference > 3 && numberofmonthdifference < 9)
                            {
                                ddlPeriodGrading.SelectedValue = "6";
                            }
                            else
                            {
                                ddlPeriodGrading.SelectedValue = "12";
                            }
                            period = Convert.ToInt32(ddlPeriodGrading.SelectedValue);
                        }
                        var MasterQuery = (from row in entities.GradingPreFillDatas
                                           where row.CustomerID == cuid
                                           && row.UserID == uid && row.IsStatutory == "I"
                                           select row).ToList();
                        if (MasterQuery.Count == 0)
                        {
                            bool sucess = InternalGrading.FillInternalGrading(cuid, uid);
                            if (sucess)
                            {
                                MasterQuery = (from row in entities.GradingPreFillDatas
                                               where row.CustomerID == cuid
                                               && row.UserID == uid && row.IsStatutory == "I"
                                               select row).ToList();
                            }
                        }
                        if (customerBranchID != -1)
                        {
                            entities.Database.CommandTimeout = 180;
                            var branchIDs = (from row in entities.SP_RLCS_GetChildBranchesFromParentBranch((int)customerBranchID, (int)cuid)
                                             select row).ToList();


                            MasterQuery = MasterQuery.Where(entry => branchIDs.Contains((int)entry.LocationID)).ToList();
                        }
                        var transactionsQuery = MasterQuery;
                        DateTime EndDate = new DateTime(year, month, DateTime.DaysInMonth(year, month)).AddMonths(1);

                        List<Tuple<string, long, string>> list = new List<Tuple<string, long, string>>();

                        DataTable table = new DataTable();
                        table.Columns.Add("Location", typeof(string));
                        table.Columns.Add("Node", typeof(long));
                        table.Columns.Add("PNode", typeof(long));
                        table.Columns.Add("LocationID", typeof(long));
                        table.Columns.Add("Year", typeof(long));
                        for (int i = 1; i <= period; i++)
                        {
                            list.Add(new Tuple<string, long, string>("C" + EndDate.AddMonths(-i).Month, Convert.ToInt64(EndDate.AddMonths(-i).ToString("yyyy")), EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy")));
                            table.Columns.Add(EndDate.AddMonths(-i).Month + "/" + EndDate.AddMonths(-i).ToString("yy"), typeof(long));
                        }

                        var alist = transactionsQuery.Where(entry => entry.Year == year).ToList();
                        foreach (var item in alist)
                        {
                            DataRow tableRow = table.NewRow();
                            tableRow["Location"] = item.LocationName;
                            tableRow["Node"] = item.Cnode;
                            tableRow["PNode"] = item.Pnode;
                            tableRow["LocationID"] = item.LocationID;
                            tableRow["Year"] = item.Year;
                            foreach (var item1 in list)
                            {
                                var columname = item1.Item1;
                                var fYear = item1.Item2;
                                var griddisplayname = item1.Item3;
                                var transactionfilter = transactionsQuery.Where(aa => aa.Year == fYear && aa.LocationID == item.LocationID).FirstOrDefault();

                                if (columname == "C1")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C1;
                                }
                                else if (columname == "C2")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C2;
                                }
                                else if (columname == "C3")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C3;
                                }
                                else if (columname == "C4")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C4;
                                }
                                else if (columname == "C5")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C5;
                                }
                                else if (columname == "C6")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C6;
                                }
                                else if (columname == "C7")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C7;
                                }
                                else if (columname == "C8")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C8;
                                }
                                else if (columname == "C9")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C9;
                                }
                                else if (columname == "C10")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C10;
                                }
                                else if (columname == "C11")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C11;
                                }
                                else if (columname == "C12")
                                {
                                    tableRow[griddisplayname] = transactionfilter.C12;
                                }
                            }
                            table.Rows.Add(tableRow);
                        }

                        System.Text.StringBuilder stringbuilder = new System.Text.StringBuilder();
                        //stringbuilder.Append(@"<table width='100%' id='basic'><tr><td>Legal Entity / Location </td>");
                        stringbuilder.Append(@"<table id='basic' width='100%' cellpadding='5' cellspacing='5'><tr><td class='locationheadbg'>Legal Entity / Location </td>");
                        for (int i = 0; i < table.Columns.Count; i++)
                        {
                            string temp2 = table.Columns[i].ColumnName.ToString();
                            if (temp2 != "Location" && temp2 != "Node" && temp2 != "PNode" && temp2 != "LocationID" && temp2 != "Year")
                            {

                                string fstring = string.Empty;
                                List<string> headerval = table.Columns[i].ToString().Split('/').ToList();
                                if (headerval[0] == "1")
                                {
                                    fstring = "Jan" + " " + headerval[1];
                                }
                                else if (headerval[0] == "2")
                                {
                                    fstring = "Feb" + " " + headerval[1];
                                }
                                else if (headerval[0] == "3")
                                {
                                    fstring = "Mar" + " " + headerval[1];
                                }
                                else if (headerval[0] == "4")
                                {
                                    fstring = "Apr" + " " + headerval[1];
                                }
                                else if (headerval[0] == "5")
                                {
                                    fstring = "May" + " " + headerval[1];
                                }
                                else if (headerval[0] == "6")
                                {
                                    fstring = "Jun" + " " + headerval[1];
                                }
                                else if (headerval[0] == "7")
                                {
                                    fstring = "Jul" + " " + headerval[1];
                                }
                                else if (headerval[0] == "8")
                                {
                                    fstring = "Aug" + " " + headerval[1];
                                }
                                else if (headerval[0] == "9")
                                {
                                    fstring = "Sep" + " " + headerval[1];
                                }
                                else if (headerval[0] == "10")
                                {
                                    fstring = "Oct" + " " + headerval[1];
                                }
                                else if (headerval[0] == "11")
                                {
                                    fstring = "Nov" + " " + headerval[1];
                                }
                                else if (headerval[0] == "12")
                                {
                                    fstring = "Dec" + " " + headerval[1];
                                }

                                stringbuilder.Append("<td>" + fstring + "</td>");
                            }
                        }
                        stringbuilder.Append("</tr>");
                        for (int i = 0; i < table.Rows.Count; i++)
                        {
                            DataRow dr = table.Rows[i];                          
                            stringbuilder.Append("<tr data-node-id=" + Convert.ToString(dr[1]) + " data-node-pid=" + Convert.ToString(dr[2]) + " >" +
                           " <td class='locationheadLocationbg'>" + Convert.ToString(dr[0]) + "</td>");



                            #region 5
                            List<string> splitvalue = table.Columns[5].ToString().Split('/').ToList();
                            string years = 20 + "" + splitvalue[1];
                            DateTime date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                            DateTime date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));
                            if (Convert.ToString(dr[5]) == "4")
                            {
                                stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[5]) == "3")
                            {
                                stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[5]) == "2")
                            {
                                stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[5]) == "1")
                            {
                                stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            #endregion

                            #region 6
                            splitvalue = null;
                            date1 = new DateTime();
                            date2 = new DateTime();
                            splitvalue = table.Columns[6].ToString().Split('/').ToList();

                            years = 20 + "" + splitvalue[1];
                            date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                            date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                            if (Convert.ToString(dr[6]) == "4")
                            {
                                stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[6]) == "3")
                            {
                                stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[6]) == "2")
                            {
                                stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[6]) == "1")
                            {
                                stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            #endregion

                            #region 7
                            splitvalue = null;
                            date1 = new DateTime();
                            date2 = new DateTime();
                            splitvalue = table.Columns[7].ToString().Split('/').ToList();

                            years = 20 + "" + splitvalue[1];
                            date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                            date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                            if (Convert.ToString(dr[7]) == "4")
                            {
                                stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[7]) == "3")
                            {
                                stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[7]) == "2")
                            {
                                stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            else if (Convert.ToString(dr[7]) == "1")
                            {
                                stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                            }
                            #endregion
                            if (period == 12 || period == 6)
                            {
                                #region 8
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[8].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[8]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[8]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[8]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[8]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 9
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[9].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[9]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[9]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[9]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[9]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 10

                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[10].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[10]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[10]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[10]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[10]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion
                            }
                            if (period == 12)
                            {
                                #region 11
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[11].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[11]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[11]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[11]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[11]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 12
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[12].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[12]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[12]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[12]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[12]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 13
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[13].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[13]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[13]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[13]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[13]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 14
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[14].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[14]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[14]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[14]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[14]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 15
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[15].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[15]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[15]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[15]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[15]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion

                                #region 16
                                splitvalue = null;
                                date1 = new DateTime();
                                date2 = new DateTime();
                                splitvalue = table.Columns[16].ToString().Split('/').ToList();

                                years = 20 + "" + splitvalue[1];
                                date1 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), 1);
                                date2 = new DateTime(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0]), DateTime.DaysInMonth(Convert.ToInt32(years.Trim()), Convert.ToInt32(splitvalue[0])));

                                if (Convert.ToString(dr[16]) == "4")
                                {
                                    stringbuilder.Append("<td class='GradingRating4' onclick='GradingReportPopPup(" + cuid + ",\"NA\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[16]) == "3")
                                {
                                    stringbuilder.Append("<td class='GradingRating3' onclick='GradingReportPopPup(" + cuid + ",\"High\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[16]) == "2")
                                {
                                    stringbuilder.Append("<td class='GradingRating2' onclick='GradingReportPopPup(" + cuid + ",\"Medium\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                else if (Convert.ToString(dr[16]) == "1")
                                {
                                    stringbuilder.Append("<td class='GradingRating1' onclick='GradingReportPopPup(" + cuid + ",\"Low\",\"" + statutoryinternal + "\",\"" + date1.ToString("dd-MMM-yyyy") + "\",\"" + date2.ToString("dd-MMM-yyyy") + "\"," + Convert.ToInt64(dr[3]) + ")'></td>");
                                }
                                #endregion
                            }
                            stringbuilder.Append("</tr>");
                        }
                        stringbuilder.Append("</table>");
                        perRahul = stringbuilder.ToString().Trim();
                    }
                    #endregion
                    g_rading.InnerHtml = perRahul.ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
        protected void ddlYearGrading_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindMonth(Convert.ToInt32(ddlYearGrading.SelectedValue));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void TreeGraddingReport_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {

                if (TreeGraddingReport.SelectedNode != null)
                {
                    TbxFilterLocationGridding.Text = TreeGraddingReport.SelectedNode.Text;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void upateGradingReport_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilterGraddingReport", string.Format("initializeJQueryUI('{0}', 'divFilterLocationGradding');", TbxFilterLocationGridding.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterGraddingReport", "$(\"#divFilterLocationGradding\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindLocationFilterGraddingReport(List<NameValueHierarchy> bracnhes, List<int> LocationList)
        {
            try
            {
                TreeGraddingReport.Nodes.Clear();
                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                TreeGraddingReport.Nodes.Add(node);
                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    TreeGraddingReport.Nodes.Add(node);
                }
                TreeGraddingReport.CollapseAll();
                TreeGraddingReport_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion
        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                //Top
                DateTime TopStartdate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TopStartdate))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopStartdate", string.Format("initializeDatePickerTopStartdate(new Date({0}, {1}, {2}));", TopStartdate.Year, TopStartdate.Month - 1, TopStartdate.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopStartdate", "initializeDatePickerTopStartdate(null);", true);
                }

                DateTime TopEnddate = DateTime.MinValue;
                if (DateTime.TryParseExact(txtAdvEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TopEnddate))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopEnddate", string.Format("initializeDatePickerTopEnddate(new Date({0}, {1}, {2}));", TopEnddate.Year, TopEnddate.Month - 1, TopEnddate.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerTopEnddate", "initializeDatePickerTopEnddate(null);", true);
                }

               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }       
        protected void upDivLocationPenalty_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilterPenalty", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPenalty');", tbxFilterLocationPenalty.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterPenalty", "$(\"#divFilterLocationPenalty\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
      
        protected void tvFilterLocationPenalty_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocationPenalty.Text = tvFilterLocationPenalty.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnSearchPenalty_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                Branchlist.Clear();

                if (tvFilterLocationPenalty.SelectedValue != "-1")
                {
                    GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocationPenalty.SelectedValue));
                    Branchlist.ToList();
                }
                customizedChecklist = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "MGMT_Checklist");

                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                string cashTimeval = string.Empty;
                string cashstatutoryval = string.Empty;
                if (objlocal == "Local")
                {
                    cashTimeval = "MCA_CHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MS_PSD" + AuthenticationHelper.UserID;
                }
                else
                {
                    cashTimeval = "MCACHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MSPSD" + AuthenticationHelper.UserID;
                }


                if (Session["User_comp_Roles"] != null)
                {
                    roles = Session["User_comp_Roles"] as List<int>;
                }
                else
                {
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                    Session["User_comp_Roles"] = roles;
                }


                if (AuthenticationHelper.Role.Equals("MGMT") || AuthenticationHelper.ComplianceProductType == 3)
                {
                    IsApprover = false;
                }
                else
                {
                    if (roles.Contains(6))
                    {
                        IsApprover = true;
                    }
                    else
                    {
                        IsApprover = false;
                    }
                }


                if (ddlStatus.SelectedItem.Text == "Statutory" || ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                        try
                        {
                            if (StackExchangeRedisExtensions.KeyExists(cashstatutoryval))
                            {
                                try
                                {
                                    MasterManagementCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval);
                                }
                                catch (Exception)
                                {
                                    LogLibrary.WriteErrorLog("GET btnSearchPenalty_Click Statutory Error exception :" + cashstatutoryval);
                                    if (IsApprover == true)
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                    }
                                    else
                                    {
                                        if (customizedChecklist)
                                        {
                                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                        }
                                        else
                                        {
                                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (IsApprover == true)
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                }
                                else
                                {
                                    if (customizedChecklist)
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                    }
                                    else
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                    }
                                }
                                try
                                {
                                    StackExchangeRedisExtensions.SetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval, MasterManagementCompliancesSummaryQuery);
                                    if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                    {
                                        StackExchangeRedisExtensions.Remove(cashTimeval);
                                        StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                    }
                                    else
                                    {
                                        StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                    }
                                }
                                catch (Exception)
                                {
                                    LogLibrary.WriteErrorLog("SET btnSearchPenalty_Click Statutory Error exception :" + cashstatutoryval);
                                }

                            }
                        }
                        catch (Exception)
                        {
                            LogLibrary.WriteErrorLog("KeyExists btnSearchPenalty_Click Statutory Error exception :" + cashstatutoryval);
                            if (IsApprover == true)
                            {
                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                            }
                            else
                            {
                                if (customizedChecklist)
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                }
                                else
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                }
                            }
                        }

                        if (IsPenaltyVisible == true)
                        {
                            GetManagementCompliancePenalitySummary(customerID, Branchlist, Convert.ToInt32(tvFilterLocationPenalty.SelectedValue), ddlFinancialYear.SelectedItem.Text, MasterManagementCompliancesSummaryQuery, IsApprover);
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterFunction", "$(\"#divFilterLocationFunction\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterRisk", "$(\"#divFilterLocationRisk\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterPenalty", "$(\"#divFilterLocationPenalty\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterPenaltyscroll", "fsetscroll();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }      
        protected string GetPerformerInternal(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserNameInternal(complianceinstanceid, 3);
                InternalPerformername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }

        }        
        protected void btnCreateICS_Click(object sender, EventArgs e)
        {
            try
            {                
                DataTable statutorydatatable = new DataTable();
                DataTable Internaldatatable = new DataTable();
                DataTable mgmtstatutorydatatable = new DataTable();
                DataTable mgmtInternaldatatable = new DataTable();

                string finalitem = string.Empty;
                string CalendarItem = string.Empty;
                string FileName = "CalendarItem";
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    int userid = AuthenticationHelper.UserID;

                    var masterbranchlist = (from row in entities.CustomerBranches
                                            where row.CustomerID == customerid
                                            select row).ToList();
                 
                    var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                    string cashTimeval = string.Empty;
                    string cashstatutoryval = string.Empty;
                    string cashinternalval = string.Empty;
                    if (objlocal == "Local")
                    {
                        cashTimeval = "MCA_CHE" + AuthenticationHelper.UserID;
                        cashstatutoryval = "MS_PSD" + AuthenticationHelper.UserID;
                        cashinternalval = "MI_PSD" + AuthenticationHelper.UserID;
                    }
                    else
                    {
                        cashTimeval = "MCACHE" + AuthenticationHelper.UserID;
                        cashstatutoryval = "MSPSD" + AuthenticationHelper.UserID;
                        cashinternalval = "MIPSD" + AuthenticationHelper.UserID;
                    }
                    #region Statutory                                      
                    List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                    try
                    {
                        if (StackExchangeRedisExtensions.KeyExists(cashstatutoryval))
                        {
                            try
                            {
                                MasterManagementCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval);
                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("GET btnCreateICS_Click Statutory Error exception :" + cashstatutoryval);
                                entities.Database.CommandTimeout = 180;
                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                            }                            
                        }
                        else
                        {
                            entities.Database.CommandTimeout = 180;
                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                            try
                            {
                                StackExchangeRedisExtensions.SetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval, MasterManagementCompliancesSummaryQuery);
                                if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                {
                                    StackExchangeRedisExtensions.Remove(cashTimeval);
                                    StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                }
                                else
                                {
                                    StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                }
                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("SET btnCreateICS_Click Statutory Error exception :" + cashstatutoryval);
                            }
                            
                        }
                    }
                    catch (Exception)
                    {
                        LogLibrary.WriteErrorLog("KeyExists btnSearchPenalty_Click Statutory Error exception :" + cashstatutoryval);
                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                    }
                    

                    //var QueryStatutory = (from row in entities.ComplianceInstanceTransactionViewCustomerWiseManagement(customerid, userid, "All")
                    //                      select row).ToList();

                    var QueryStatutory = MasterManagementCompliancesSummaryQuery.ToList();
                    if (QueryStatutory.Count > 0)
                    {
                        //QueryStatutory = QueryStatutory.Where(entry => entry.ComplianceType != 1).ToList();
                        QueryStatutory = QueryStatutory.Where(entry => entry.ComplianceStatusID == 1).ToList();
                        QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    if (QueryStatutory.Count > 0)
                    {
                        var Statutory = (from row in QueryStatutory
                                         select new
                                         {
                                             ScheduledOn = row.PerformerScheduledOn,
                                             ComplianceStatusID = row.ComplianceStatusID,
                                             RoleID = row.RoleID,
                                             ScheduledOnID = row.ScheduledOnID,
                                             CustomerBranchID = row.CustomerBranchID,
                                             ShortDescription = row.ShortDescription,
                                         }).Distinct().ToList();


                        statutorydatatable = Statutory.ToDataTable();
                    }
                    #endregion

                    #region Internal                                                        

                 
                    List<SP_GetManagementInternalCompliancesSummary_Result> MasterManagementInternalCompliancesSummaryQuery = new List<SP_GetManagementInternalCompliancesSummary_Result>();
                    try
                    {
                        if (StackExchangeRedisExtensions.KeyExists(cashinternalval))
                        {
                            try
                            {
                                MasterManagementInternalCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval);
                            }
                            catch (Exception)
                            {
                                entities.Database.CommandTimeout = 180;
                                MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                LogLibrary.WriteErrorLog("GET btnCreateICS_Click Internal Error exception :" + cashinternalval);
                            }
                            
                        }
                        else
                        {
                            entities.Database.CommandTimeout = 180;
                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                            try
                            {
                                StackExchangeRedisExtensions.SetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval, MasterManagementInternalCompliancesSummaryQuery);
                                if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                {
                                    StackExchangeRedisExtensions.Remove(cashTimeval);
                                    StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                }
                                else
                                {
                                    StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                }
                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("SET btnCreateICS_Click Internal Error exception :" + cashinternalval);
                            }                           
                        }
                    }
                    catch (Exception)
                    {
                        LogLibrary.WriteErrorLog("KeyExists btnSearchPenalty_Click Internal Error exception :"+ cashinternalval);
                        MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                    }
                    

                    //var QueryInternal = (from row in entities.InternalComplianceInstanceTransactionmanagement(customerid, userid, "All")
                    //                     select row).ToList();
                    var QueryInternal = MasterManagementInternalCompliancesSummaryQuery.ToList();
                    if (QueryInternal.Count > 0)
                    {                        
                        QueryInternal = QueryInternal.Where(entry => entry.ComplianceStatusID == 1).ToList();
                        QueryInternal = QueryInternal.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    if (QueryInternal.Count > 0)
                    {
                        var Internal = (from row in QueryInternal
                                        select new
                                        {
                                            ScheduledOn = row.ScheduledOn,
                                            ComplianceStatusID = row.ComplianceStatusID,
                                            RoleID = row.RoleID,
                                            ScheduledOnID = row.ScheduledOnID,
                                            CustomerBranchID = row.CustomerBranchID,
                                            ShortDescription = row.ShortDescription,
                                        }).Distinct().ToList();

                        Internaldatatable = Internal.ToDataTable();
                    }
                    #endregion
                    DataTable newtable = new DataTable();
                    // Performer or reviewer
                    if (statutorydatatable.Rows.Count > 0)
                    {
                        newtable.Merge(statutorydatatable);
                    }
                    if (Internaldatatable.Rows.Count > 0)
                    {
                        newtable.Merge(Internaldatatable);
                    }
                    var listEmployee = newtable;
                    if (listEmployee.Rows.Count > 0)
                    {
                        string removableChars = Regex.Escape(@"@&'()<>#,");
                        string pattern = "[" + removableChars + "]";

                        //create a new stringbuilder instance
                        StringBuilder sb = new StringBuilder();
                        //start the calendar item
                        sb.AppendLine("BEGIN:VCALENDAR");
                        sb.AppendLine("VERSION:2.0");
                        sb.AppendLine("PRODID:avantis.co.in");
                        sb.AppendLine("CALSCALE:GREGORIAN");
                        sb.AppendLine("METHOD:PUBLISH");                     
                        foreach (DataRow item in listEmployee.Rows)
                        {
                            var branchname = masterbranchlist.Where(entry => entry.ID == Convert.ToInt32(item["CustomerBranchID"])).Select(en => en.Name).FirstOrDefault();
                            DateTime DateStart = Convert.ToDateTime(item["ScheduledOn"]);
                            DateTime DateEnd = DateStart.AddDays(1);
                            string Summary = Regex.Replace(item["ShortDescription"].ToString(), pattern, ""); //item["ShortDescription"].ToString();// item.Item1;
                            string Location = Regex.Replace(branchname.ToString(), pattern, ""); // branchname.Replace(',', ' ').Trim();//item.Item3;
                            string Description = Regex.Replace(item["ShortDescription"].ToString(), pattern, ""); //item["ShortDescription"].ToString().Replace(',', ' ').Trim();// item.Item1;                                                  

                            string finalsummary = string.Empty;
                            var nsummary = Regex.Replace(Summary, @"\t|\n|\r", "");
                            if (nsummary.Length > 75)
                            {
                                finalsummary = nsummary.Substring(0, 75);
                            }
                            else
                            {
                                finalsummary = nsummary;
                            }
                          
                            //create a new stringbuilder instance
                            StringBuilder sbevent = new StringBuilder();
                            //add the event
                            sbevent.AppendLine("BEGIN:VEVENT");
                            sbevent.AppendLine("DTSTART:" + DateStart.ToString("yyyyMMddTHHmm00"));
                            sbevent.AppendLine("DTEND:" + DateEnd.ToString("yyyyMMddTHHmm00"));
                            sbevent.AppendLine("SUMMARY:" + finalsummary + "");
                            sbevent.AppendLine("LOCATION:" + Location + "");
                            //  sbevent.AppendLine("DESCRIPTION:" + Regex.Replace(Description, @"\t|\n|\r", "")   + "");
                            sbevent.AppendLine("DESCRIPTION:" + finalsummary + "");
                            sbevent.AppendLine("PRIORITY:3");
                            sbevent.AppendLine("END:VEVENT");
                            //create a string from the stringbuilder
                            CalendarItem += sbevent.ToString();
                        }

                        StringBuilder endval = new StringBuilder();
                        endval.AppendLine("END:VCALENDAR");

                        finalitem = sb.ToString() + CalendarItem.ToString() + endval.ToString();


                        //send the calendar item to the browser
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.Buffer = true;
                        Response.ContentType = "text/calendar";                        
                        Response.AddHeader("content-disposition", "attachment; filename=\"" + FileName + ".ics\"");
                        Response.Write(finalitem);
                        Response.Flush();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkShowTLDashboard_Click(object sender, EventArgs e)
        {
            try
            {
                var userRecord = RLCSManagement.GetRLCSUserRecord(Convert.ToInt32(AuthenticationHelper.UserID), AuthenticationHelper.ProfileID);

                if (userRecord != null)
                {
                    if (userRecord.AVACOM_UserRole != null)
                    {
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                        ProductMappingStructure _obj = new ProductMappingStructure();
                        _obj.ReAuthenticate_User(customerID, userRecord.AVACOM_UserRole);
                        Response.Redirect("~/RLCS/RLCS_HRMDashboardNew.aspx", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnImprisonmentPenalty_Click(object sender, EventArgs e)
        {
            try
            {
                int cuid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                string cashTimeval = string.Empty;
                string cashstatutoryval = string.Empty;
                if (objlocal == "Local")
                {
                    cashTimeval = "MCA_CHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MS_PSD" + AuthenticationHelper.UserID;
                }
                else
                {
                    cashTimeval = "MCACHE" + AuthenticationHelper.UserID;
                    cashstatutoryval = "MSPSD" + AuthenticationHelper.UserID;
                }

                if (Session["User_comp_Roles"] != null)
                {
                    roles = Session["User_comp_Roles"] as List<int>;
                }
                else
                {
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                    Session["User_comp_Roles"] = roles;
                }


                if (AuthenticationHelper.Role.Equals("MGMT") || AuthenticationHelper.ComplianceProductType == 3)
                {
                    IsApprover = false;
                }
                else
                {
                    if (roles.Contains(6))
                    {
                        IsApprover = true;
                    }
                    else
                    {
                        IsApprover = false;
                    }
                }
                if (ddlStatus.SelectedItem.Text == "Statutory" || ddlStatus.SelectedItem.Text == "Statutory Excluding Checklist")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                        try
                        {
                            if (StackExchangeRedisExtensions.KeyExists(cashstatutoryval))
                            {
                                try
                                {
                                    MasterManagementCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval);
                                }
                                catch (Exception)
                                {
                                    LogLibrary.WriteErrorLog("GET btnSearchPenalty_Click Statutory Error exception :" + cashstatutoryval);
                                    if (IsApprover == true)
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                    }
                                    else
                                    {
                                        if (customizedChecklist)
                                        {
                                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                        }
                                        else
                                        {
                                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (IsApprover == true)
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                                }
                                else
                                {
                                    if (customizedChecklist)
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                    }
                                    else
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                    }
                                }
                                try
                                {
                                    StackExchangeRedisExtensions.SetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval, MasterManagementCompliancesSummaryQuery);
                                    if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                    {
                                        StackExchangeRedisExtensions.Remove(cashTimeval);
                                        StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                    }
                                    else
                                    {
                                        StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                    }
                                }
                                catch (Exception)
                                {
                                    LogLibrary.WriteErrorLog("SET btnSearchPenalty_Click Statutory Error exception :" + cashstatutoryval);
                                }

                            }
                        }
                        catch (Exception)
                        {
                            LogLibrary.WriteErrorLog("KeyExists btnSearchPenalty_Click Statutory Error exception :" + cashstatutoryval);
                            if (IsApprover == true)
                            {
                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "APPR")).ToList();
                            }
                            else
                            {
                                if (customizedChecklist)
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                }
                                else
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                }
                            }
                        }

                        GetImprisonmentPenaltySummary(cuid, Branchlist, Convert.ToInt32(tvFilterLocation.SelectedValue), "T", FromFinancialYearSummery, ToFinancialYearSummery, MasterManagementCompliancesSummaryQuery, ddlMinimum.SelectedItem.Text, ddlMaximum.SelectedItem.Text, ddlMinimumImprisonment.SelectedItem.Text, ddlMaximumImprisonment.SelectedItem.Text, IsApprover, GetDate(txtAdvStartDate.Text), GetDate(txtAdvEndDate.Text));
                    }
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterFunction", "$(\"#divFilterLocationFunction\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterRisk", "$(\"#divFilterLocationRisk\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterPenalty", "$(\"#divFilterLocationPenalty\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilterPenaltyscroll", "fsetscroll();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlperiod_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int period = Convert.ToInt32(ddlperiod.SelectedValue);

                if (period == 0) //Current YTD
                {
                    if (DateTime.Today.Month > 3)
                    {
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + DateTime.Now.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        DateTime dtprev = DateTime.Now.AddYears(-1);
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                }
                else if (period == 1) //Current + Previous YTD
                {
                    if (DateTime.Today.Month > 3)
                    {
                        DateTime dtprev = DateTime.Now.AddYears(-1);
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        DateTime dtprev = DateTime.Now.AddYears(-2);
                        string dtfrom = Convert.ToString("01" + "-04" + "-" + dtprev.Year);
                        FromFinancialYearSummery = DateTime.ParseExact(dtfrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                }
                else if (period == 2) //All
                {
                    string dtfrom = Convert.ToString("01-01-1900");
                    string dtto = Convert.ToString("01-01-1900");
                    FromFinancialYearSummery = DateTime.ParseExact(dtfrom.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    ToFinancialYearSummery = DateTime.ParseExact(DateTime.Today.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else if (period == 3)// Last Month
                {
                    FromFinancialYearSummery = DateTime.Now.AddDays(-30);
                    ToFinancialYearSummery = DateTime.Now;
                }
                else if (period == 4)//Last 3 Month
                {
                    FromFinancialYearSummery = DateTime.Now.AddDays(-90);
                    ToFinancialYearSummery = DateTime.Now;
                }
                else if (period == 5)//Last 6 Month
                {
                    FromFinancialYearSummery = DateTime.Now.AddDays(-180);
                    ToFinancialYearSummery = DateTime.Now;
                }
                else if (period == 6)//This Month
                {
                    DateTime now = DateTime.Now;
                    FromFinancialYearSummery = new DateTime(now.Year, now.Month, 1);
                    ToFinancialYearSummery = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}