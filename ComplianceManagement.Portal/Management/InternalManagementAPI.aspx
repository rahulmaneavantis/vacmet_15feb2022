﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InternalManagementAPI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.InternalManagementAPI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white; margin-left: 12px;">
<head runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min1.1.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>

    <style type="text/css">
        button, html input[type=button], input[type=reset], input[type=submit] {
            -webkit-appearance: button;
            cursor: pointer;
        }

        .modal, .modal-backdrop {
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .modal-content, .popover {
            background-clip: padding-box;
        }

        .modal-open .navbar-fixed-bottom, .modal-open .navbar-fixed-top, body.modal-open {
            margin-right: 15px;
        }

        .modal {
            display: none;
            overflow: auto;
            overflow-y: scroll;
            position: fixed;
            z-index: 1040;
        }

            .modal.fade .modal-dialog {
                -webkit-transform: translate(0,-25%);
                -ms-transform: translate(0,-25%);
                transform: translate(0,-25%);
                -webkit-transition: -webkit-transform .3s ease-out;
                -moz-transition: -moz-transform .3s ease-out;
                -o-transition: -o-transform .3s ease-out;
                transition: transform .3s ease-out;
            }

            .modal.in .modal-dialog {
                -webkit-transform: translate(0,0);
                -ms-transform: translate(0,0);
                transform: translate(0,0);
            }

        .modal-dialog {
            margin-left: auto;
            margin-right: auto;
            width: auto;
            padding: 10px;
            z-index: 1050;
        }

        .modal-content {
            position: relative;
            background-color: #fff;
            border: 1px solid #999;
            border: 1px solid rgba(0,0,0,.2);
            border-radius: 6px;
            -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
            box-shadow: 0 3px 9px rgba(0,0,0,.5);
            outline: 0;
        }

        .modal-backdrop {
            position: fixed;
            z-index: 1030;
            background-color: #000;
        }

            .modal-backdrop.fade {
                opacity: 0;
                filter: alpha(opacity=0);
            }

            .carousel-control, .modal-backdrop.in {
                opacity: .5;
                filter: alpha(opacity=50);
            }

        .modal-header {
            padding: 15px;
            border-bottom: 1px solid #e5e5e5;
            min-height: 16.43px;
        }

            .modal-header .close {
                margin-top: -2px;
            }

        .modal-title {
            margin: 0;
            line-height: 1.428571429;
        }

        .modal-body {
            position: relative;
            padding: 20px;
        }


        .modal-footer {
            margin-top: 15px;
            padding: 19px 20px 20px;
            text-align: right;
            border-top: 1px solid #e5e5e5;
        }



            .modal-footer:after, .modal-footer:before {
                content: " ";
                display: table;
            }

            .modal-footer .btn + .btn {
                margin-left: 5px;
                margin-bottom: 0;
            }

            .modal-footer .btn-group .btn + .btn {
                margin-left: -1px;
            }

            .modal-footer .btn-block + .btn-block {
                margin-left: 0;
            }

        @media screen and (min-width:768px) {
            .modal-dialog {
                left: 50%;
                right: auto;
                width: 600px;
                padding-top: 30px;
                padding-bottom: 30px;
            }

            .modal-content {
                -webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);
                box-shadow: 0 5px 15px rgba(0,0,0,.5);
            }
        }

        .btn-group-vertical > .btn-group:after, .btn-toolbar:after, .clearfix:after, .container:after, .dropdown-menu > li > a, .form-horizontal .form-group:after, .modal-footer:after, .nav:after, .navbar-collapse:after, .navbar-header:after, .navbar:after, .pager:after, .panel-body:after, .row:after {
            clear: both;
        }

        .media, .media-body, .modal-open, .progress {
            overflow: hidden;
        }

        .close, .list-group-item > .badge {
            float: right;
        }

        .close {
            font-size: 21px;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            opacity: .2;
            filter: alpha(opacity=20);
        }

            .close:focus, .close:hover {
                color: #000;
                text-decoration: none;
                cursor: pointer;
                opacity: .5;
                filter: alpha(opacity=50);
            }

        button.close {
            padding: 0;
            cursor: pointer;
            background: 0 0;
            border: 0;
            -webkit-appearance: none;
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: inset 0 0 1px 1px #14699f;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px #d7dae0;
            box-shadow: inset 0 0 1px 1px white;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
            overflow: hidden;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: -2px;
            color: inherit;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 0px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-multiselect-wrap .k-input {
            /*padding-top:6px;*/
            display: inherit !important;
        }
        .k-filter-menu .k-button {
            width: 27%;
            background-color: #E9EAEA;
            color: black;
            border: 2px solid rgba(0, 0, 0, 0.5);
        }
    </style>

    <title></title>

    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>

    <script type="text/x-kendo-template" id="template">      
               
    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            BindInternalType();
            BindLocation();
            BindRisk();
            BindUser();
            BindFunction();
            BindDeptData();
            BindFinancialYear();
            BindStatus();
            BindPastData();
            BindGrid();
            FilterGrid();

            $("#txtSearchComplianceID").on('input', function (e) {
                FilterGrid();
            });
            $("#Startdatepicker").kendoDatePicker({
                change: FilterGrid,
                format: "dd-MMM-yyyy"
            });
            $("#Lastdatepicker").kendoDatePicker({
                change: FilterGrid,
                format: "dd-MMM-yyyy"
            });

            $("#dropdownSequence").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: false,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Label",
                change: function (e) {
                    FilterGrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=I&CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }
            });
            $("#dropdownlistStatus").data("kendoDropDownTree").value(["<%=StatusID%>", "<%=StatusID1%>", "<%=StatusID2%>", "<%=StatusID3%>", "<%=StatusID5%>"]);
        });

        function BindGrid() {
            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport:
                    {
                        read: {
                            url: '<% =Path%>/Data/GetInternalManagementDashboardPages?Uid=<% =UId%>&ptname=All&attr=All&Cid=<% =CustId%>&bid=<% =branchid%>&FDte=<% =FromDate%>&EDte=<% =Enddate%>&Fltr=<% =Filter%>&fid=-1&ChartName=<% =ChartName%>&INTorSAT=<% =Internalsatutory%>&lstcid=<% =listcategoryid%>&DptHead=<% =IsDeptHead%>&Status=All&IsAppr=<% =IsApprover%>&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                ComplianceID: { type: "string", },
                                ScheduledOn: { type: "date" }
                            }
                        },
                        data: function (response) {
                            if (<% =ChartNameID%> == 1) {
                                return response[0].FunctionPieChart;
                            }
                            else if (<% =ChartNameID%> == 2) {
                                return response[0].FunctionBarChart;
                            }
                            else if (<% =ChartNameID%> == 3) {
                                return response[0].RiskBarChart;
                            }
                            else if (<% =ChartNameID%> == 4) {
                                return response[0].DeptFunctionPieChart;
                            }
                            else if (<% =ChartNameID%> == 5) {
                                return response[0].DeptFunctionBarChart;
                            }
                            else if (<% =ChartNameID%> == 6) {
                                return response[0].DeptRiskBarChart;
                            }
                            //return response[0].FunctionPieChart.length;
                        },
                        total: function (response) {
                            if (<% =ChartNameID%> == 1) {
                                return response[0].FunctionPieChart.length;
                            }
                            else if (<% =ChartNameID%> == 2) {
                                return response[0].FunctionBarChart.length;
                            }
                            else if (<% =ChartNameID%> == 3) {
                                return response[0].RiskBarChart.length;
                            }
                            else if (<% =ChartNameID%> == 4) {
                                return response[0].DeptFunctionPieChart.length;
                            }
                            else if (<% =ChartNameID%> == 5) {
                                return response[0].DeptFunctionBarChart.length;
                            }
                            else if (<% =ChartNameID%> == 6) {
                                return response[0].DeptRiskBarChart.length;
                            }
                            //return response[0].FunctionPieChart.length;
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                dataBinding: function () {
                    window.parent.forchild($("body").height() + 40);
                },
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { hidden: true, field: "ComplianceID", title: "Compliance ID", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "DepartmentID1", title: "Department ID", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "DepartmentName", title: "Department&nbsp;Name", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "ShortForm", title: "Short Form", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "RiskCategory", title: "Risk", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "CustomerBranchID", title: "Branch ID", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "Performer", title: "Performer", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "Reviewer", title: "Reviewer", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "ComCategoryName", title: "Category", filterable: { multi: true, search: true }, width: "10%" },
                    { hidden: true, field: "InternalType", title: "Internal Type", filterable: { multi: true, search: true }, width: "10%" },
                    {
                        field: "Branch", title: 'Location',
                        width: "20%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "25%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ScheduledOn", title: 'Due&nbsp;Date',
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                        filterable: {
                            multi: true,
                            search: true,
                            extra: false,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MMM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "15%"
                    },
                    {
                        field: "ForMonth", title: 'Period',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        field: "Status", title: 'Status',
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "12%"
                    },
                    {
                        command: [
                            {
                                name: "edit5", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-edit",
                                visible: function (dataItem) {
                                    if (dataItem.ComplianceStatusID != "1") {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            },
                            {
                                name: "edit4", text: "", iconClass: "k-icon k-i-download", className: "ob-download",
                                visible: function (dataItem) {
                                    if (dataItem.ComplianceStatusID != "1") {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            },
                            {
                                name: "edit2", text: "", width: "150px", iconClass: "k-icon k-i-eye", className: "ob-overview",
                            }
                        ], title: "Action", width: "150px", headerAttributes: {
                            style: "border-right: solid 1px #ceced2;text-align:center;"
                        }
                    }
                ]
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit5",
                content: function (e) {
                    return "View";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit4",
                content: function (e) {
                    return "Download";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Overview";
                }
            });

            $("#grid").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "th",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenOverViewpupMain(item.ScheduledOnID, item.ComplianceInstanceID);
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-download", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                $('#downloadfile').attr('src', "../ComplianceDocument/DownloadMGMTDocAPI.aspx?ComplianceScheduleID=" + item.ScheduledOnID + "&IsFlag=0");
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                OpenDocumentOverviewpup(item.ScheduledOnID)
                return true;
            });
        }

        function OpenDocumentOverviewpup(scheduledonid) {
            $('#divOverView').modal('show');
            $('#OverViews').attr('width', '1150px');
            $('#OverViews').attr('height', '600px');
            $('.modal-dialog').css('width', '1200px');
            $('#OverViews').attr('src', "../Common/DocumentOverviewAPI.aspx?ComplianceScheduleID=" + scheduledonid + "&ISStatutoryflag=0");
        }

        function exportReport(e) {
            if (document.getElementById('IsLabel').value == 1) {

                var ReportName = "Report of Internal Compliances";
                var customerName = document.getElementById('CustName').value;
                var todayDate = moment().format('DD-MMM-YYYY');
                var grid = $("#grid").getKendoGrid();

                var rows = [
                    {
                        cells: [
                            { value: "Entity/ Location:", bold: true },
                            { value: customerName }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Name:", bold: true },
                            { value: ReportName }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Generated On:", bold: true },
                            { value: todayDate }
                        ]
                    },
                    {
                        cells: [
                            { value: "" }
                        ]
                    },
                    {
                        cells: [
                            { value: "Compliance ID", bold: true },
                            { value: "Location", bold: true },
                            { value: "Act Name", bold: true },
                            { value: "Short Description", bold: true },
                            { value: "Short Form", bold: true },
                            { value: "Performer", bold: true },
                            { value: "Reviewer", bold: true },
                            { value: "Due Date", bold: true },
                            { value: "For Month", bold: true },
                            { value: "Status", bold: true },
                            { value: "Risk Category", bold: true },
                            { value: "Label", bold: true },
                            { value: "Compliance Category Name", bold: true },
                            { value: "Internal Type", bold: true }

                        ]
                    }
                ];

                var trs = grid.dataSource;
                var filteredDataSource = new kendo.data.DataSource({
                    data: trs.data(),
                    filter: trs.filter()
                });

                filteredDataSource.read();
                var data = filteredDataSource.view();
                for (var i = 0; i < data.length; i++) {
                    var dataItem = data[i];
                    rows.push({
                        cells: [
                            { value: dataItem.ComplianceID },
                            { value: dataItem.Branch },
                            { value: dataItem.Name },
                            { value: dataItem.ShortDescription },
                            { value: dataItem.ShortForm },
                            { value: dataItem.Performer },
                            { value: dataItem.Reviewer },
                            { value: dataItem.ScheduledOn, format: "dd-MMM-yyyy" },
                            { value: dataItem.ForMonth },
                            { value: dataItem.Status },
                            { value: dataItem.RiskCategory },
                            { value: dataItem.SequenceID },
                            { value: dataItem.ComCategoryName },
                            { value: dataItem.InternalType },
                        ]
                    });
                }
                for (var i = 4; i < rows.length; i++) {
                    for (var j = 0; j < 13; j++) {
                        rows[i].cells[j].borderBottom = "#000000";
                        rows[i].cells[j].borderLeft = "#000000";
                        rows[i].cells[j].borderRight = "#000000";
                        rows[i].cells[j].borderTop = "#000000";
                        rows[i].cells[j].hAlign = "left";
                        rows[i].cells[j].vAlign = "top";
                        rows[i].cells[j].wrap = true;
                    }
                }
                excelExport(rows, ReportName);
                e.preventDefault();
            }
            else {
                var ReportName = "Report of Internal Compliances";
                var customerName = document.getElementById('CustName').value;
                var todayDate = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
                var grid = $("#grid").getKendoGrid();

                var rows = [
                    {
                        cells: [
                            { value: "Entity/ Location:", bold: true },
                            { value: customerName }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Name:", bold: true },
                            { value: ReportName }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Generated On:", bold: true },
                            { value: todayDate }
                        ]
                    },
                    {
                        cells: [
                            { value: "" }
                        ]
                    },
                    {
                        cells: [
                            { value: "Compliance ID", bold: true },
                            { value: "Location", bold: true },
                            { value: "Act Name", bold: true },
                            { value: "Short Description", bold: true },
                            { value: "Short Form", bold: true },
                            { value: "Performer", bold: true },
                            { value: "Reviewer", bold: true },
                            { value: "Due Date", bold: true },
                            { value: "For Month", bold: true },
                            { value: "Status", bold: true },
                            { value: "Risk Category", bold: true },
                            { value: "Category", bold: true },
                            { value: "InternalType", bold: true }
                        ]
                    }
                ];

                var trs = grid.dataSource;
                var filteredDataSource = new kendo.data.DataSource({
                    data: trs.data(),
                    filter: trs.filter()
                });

                filteredDataSource.read();
                var data = filteredDataSource.view();
                for (var i = 0; i < data.length; i++) {
                    var dataItem = data[i];
                    rows.push({
                        cells: [
                            { value: dataItem.ComplianceID },
                            { value: dataItem.Branch },
                            { value: dataItem.Name },
                            { value: dataItem.ShortDescription },
                            { value: dataItem.ShortForm },
                            { value: dataItem.Performer },
                            { value: dataItem.Reviewer },
                            { value: dataItem.ScheduledOn, format: "dd-MMM-yyyy" },
                            { value: dataItem.ForMonth },
                            { value: dataItem.Status },
                            { value: dataItem.RiskCategory },
                            { value: dataItem.ComCategoryName },
                            { value: dataItem.InternalType }
                        ]
                    });
                }
                for (var i = 4; i < rows.length; i++) {
                    for (var j = 0; j < 12; j++) {
                        rows[i].cells[j].borderBottom = "#000000";
                        rows[i].cells[j].borderLeft = "#000000";
                        rows[i].cells[j].borderRight = "#000000";
                        rows[i].cells[j].borderTop = "#000000";
                        rows[i].cells[j].hAlign = "left";
                        rows[i].cells[j].vAlign = "top";
                        rows[i].cells[j].wrap = true;
                    }
                }
                excelExport(rows, ReportName);
                e.preventDefault();
            }
        }

        function excelExport(rows, ReportName) {
            if (document.getElementById('IsLabel').value == 1) {
                var workbook = new kendo.ooxml.Workbook({
                    sheets: [
                        {
                            columns: [
                                { width: 180 },
                                { width: 180 },
                                { width: 300 },
                                { width: 300 },
                                { width: 300 },
                                { width: 180 },
                                { width: 180 },
                                { width: 100 },
                                { width: 100 },
                                { width: 150 },
                                { width: 100 },
                                { width: 100 },
                                { width: 100 }
                            ],
                            title: "Report",
                            rows: rows
                        },
                    ]
                });

                var nameOfPage = "InternalComplianceReport";
                kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });
            }
            else {
                var workbook = new kendo.ooxml.Workbook({
                    sheets: [
                        {
                            columns: [
                                { width: 180 },
                                { width: 180 },
                                { width: 300 },
                                { width: 300 },
                                { width: 300 },
                                { width: 180 },
                                { width: 180 },
                                { width: 100 },
                                { width: 100 },
                                { width: 150 },
                                { width: 100 },
                                { width: 100 }
                            ],
                            title: "Report",
                            rows: rows
                        },
                    ]
                });

                var nameOfPage = "InternalComplianceReport";
                kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });
            }

        }

        function FilterGrid() {
            
            //Risk Details
            var Riskdetails = [];
            if ($("#dropdownlistRisk").data("kendoDropDownTree") != undefined) {
                Riskdetails = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
            }

            //status Details
            var Statusdetails = [];
            if ($("#dropdownlistStatus").data("kendoDropDownTree") != undefined) {
                var Statusdetails = $("#dropdownlistStatus").data("kendoDropDownTree")._values;

                if (Statusdetails.length >= 1) {
                    for (var i = 0; i < Statusdetails.length; i++) {
                        if (Statusdetails[i] === "No" || Statusdetails[i] === "") {
                            Statusdetails.splice(i, 1);
                        }
                    }
                }
            }

            //user Details  
            var Userdetails = [];
            if ($("#dropdownUser").data("kendoDropDownTree") != undefined) {
                Userdetails = $("#dropdownUser").data("kendoDropDownTree")._values;
            }

            //location details
            var locationsdetails = [];
            if ($("#dropdowntree").data("kendoDropDownTree") != undefined) {
                locationsdetails = $("#dropdowntree").data("kendoDropDownTree")._values;
            }

            var Departmentdetails = -1;
            if (DisplayDepartment.value > 0) {
                Departmentdetails = DisplayDepartment.value;
            }

            var Categorydetails = [];
            if ($("#dropdownfunction").data("kendoDropDownTree") != undefined) {
                Categorydetails = $("#dropdownfunction").data("kendoDropDownTree")._values;
            }

            var finalSelectedfilter = { logic: "and", filters: [] };

            if (Riskdetails.length > 0 || Statusdetails.length > 0 || locationsdetails.length > 0 || Userdetails.length > 0 ||
                ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") ||
                ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") ||
                ($("#txtSearchComplianceID").val() != "") || 
                ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") ||
                ($("#dropdownType").val() != "" && $("#dropdownType").val() != null && $("#dropdownType").val() != undefined) ||
                ($("#dropdownDept").val() != "" && $("#dropdownDept").val() != null && $("#dropdownDept").val() != undefined) ||
                Categorydetails.length > 0 ||
                Departmentdetails > 0) {
                if ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != "" && $("#dropdownSequence").val() != null) {
                    var SeqFilter = { logic: "or", filters: [] };
                    SeqFilter.filters.push({
                        field: "SequenceID", operator: "eq", value: $("#dropdownSequence").val()
                    });

                    finalSelectedfilter.filters.push(SeqFilter);
                }
                if ($("#dropdownType").val() != "" && $("#dropdownType").val() != null && $("#dropdownType").val() != undefined) {
                    var TypeFilter = { logic: "or", filters: [] };
                    TypeFilter.filters.push({
                        field: "InternalType", operator: "eq", value: $("#dropdownType").val()
                    });
                    finalSelectedfilter.filters.push(TypeFilter);
                }
                if ($("#txtSearchComplianceID").val() != "" && $("#txtSearchComplianceID").val() != undefined && $("#txtSearchComplianceID").val() != null) {
                    var RiskFilter = { logic: "or", filters: [] };
                    RiskFilter.filters.push({
                        field: "ComplianceID", operator: "contains", value: $("#txtSearchComplianceID").val()
                    });
                    finalSelectedfilter.filters.push(RiskFilter);
                }
                if ($("#dropdownDept").val() != undefined && $("#dropdownDept").val() != "" && $("#dropdownDept").val() != null) {

                    var SeqFilter = { logic: "or", filters: [] };

                    SeqFilter.filters.push({
                        field: "DepartmentID1", operator: "eq", value: parseInt($("#dropdownDept").val())
                    });

                    finalSelectedfilter.filters.push(SeqFilter);
                }
                if (Departmentdetails > 0) {

                    var FunctionDepartmentFilter = { logic: "or", filters: [] };

                    FunctionDepartmentFilter.filters.push({
                        field: "DepartmentID", operator: "eq", value: parseInt(Departmentdetails)
                    });

                    finalSelectedfilter.filters.push(FunctionDepartmentFilter);
                }


                if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                    var DateFilter = { logic: "or", filters: [] };
                    DateFilter.filters.push({
                        field: "ScheduledOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'dd-MMM-yyyy')
                    });

                    finalSelectedfilter.filters.push(DateFilter);
                }

                if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                    var DateFilter = { logic: "or", filters: [] };
                    DateFilter.filters.push({
                        field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'dd-MMM-yyyy')
                    });

                    finalSelectedfilter.filters.push(DateFilter);
                }

                if (Userdetails.length > 0) {
                    var UserFilter = { logic: "or", filters: [] };

                    $.each(Userdetails, function (i, v) {
                        UserFilter.filters.push({
                            field: "UserID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(UserFilter);
                }
                //if (InternalTypedetails.length > 0) {
                //    var TypeFilter = { logic: "or", filters: [] };

                //    $.each(InternalTypedetails, function (i, v) {
                //        TypeFilter.filters.push({
                //            field: "Internaltypeid", operator: "eq", value: parseInt(v)
                //        });
                //    });

                //    finalSelectedfilter.filters.push(InternalTypedetails);
                //}

                if (locationsdetails.length > 0) {
                    var LocationFilter = { logic: "or", filters: [] };

                    $.each(locationsdetails, function (i, v) {
                        LocationFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(LocationFilter);
                }
                if (Categorydetails.length > 0) {
                    var CatFilter = { logic: "or", filters: [] };

                    $.each(Categorydetails, function (i, v) {
                        CatFilter.filters.push({
                            field: "ComplianceCategoryId", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(CatFilter);
                }

                if (Statusdetails.length > 0) {
                    var StatusFilter = { logic: "or", filters: [] };

                    $.each(Statusdetails, function (i, v) {
                        StatusFilter.filters.push({
                            field: "FilterStatus", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(StatusFilter);
                }
                if (Riskdetails.length > 0) {
                    var CategoryFilter = { logic: "or", filters: [] };

                    $.each(Riskdetails, function (i, v) {

                        CategoryFilter.filters.push({
                            field: "Risk", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(CategoryFilter);
                }
                if (finalSelectedfilter.filters.length > 0) {
                    if ($("#grid").data("kendoGrid") != undefined) {
                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter(finalSelectedfilter);
                    }
                }
                else {
                    if ($("#grid").data("kendoGrid") != undefined) {
                        $("#grid").data("kendoGrid").dataSource.filter({});
                    }
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});

            }

            if ($("#grid").data("kendoGrid") != undefined) {
                var dataSource = $("#grid").data("kendoGrid").dataSource;
                if (dataSource._total > 20 && dataSource.pageSize == undefined) {
                    dataSource.pageSize(total);
                }
            }

        }

        function fCreateStoryBoard(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterUser') {
                $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            }
            else if (div == 'filterstatus') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterCategory') {
                $('#' + div).append('Category&nbsp;&nbsp;:');
            }
            else if (div == 'filterrisk') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                //if (buttontest.length > 10) {
                //    buttontest = buttontest.substring(0, 10).concat("...");
                //}
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;border-radius:10px;margin-left:4px;margin-top:7px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="clear" aria-label="clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close" style="font-size: 12px;"></span></span></li>');
                //$('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB; height: 20px;Color:Gray;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');

            }
        }

        function fcloseStory(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
            fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function');
            fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
        }

        function CloseClearPopup() {
            $('#APIOverView').attr('src', "../Common/blank.html");
        }

        function OpenOverViewpupMain(scheduledonid, instanceid) {
            $('#divApiOverView').modal('show');
            $('#APIOverView').attr('width', '1150px');
            $('#APIOverView').attr('height', '600px');
            $('.modal-dialog').css('width', '1200px');
            $('#APIOverView').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
        }

        function BindFinancialYear() {
            $("#dropdownFY").kendoDropDownList({
                autoWidth: true,
                dataTextField: "FinancialYear",
                dataValueField: "FinancialYear",
                optionLabel: "Financial Year",
                change: function () {
                    if ($("#dropdownFY").val() != "") {
                        $("#dropdownPastData").data("kendoDropDownList").select(4);
                        fCreateStoryBoard('dropdownFY', 'filterFY', 'FY');
                    }
                    BindGrid();
                    FilterGrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetFYDetail',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }
            });
        }

        function BindRisk() {
            $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    FilterGrid();
                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });
            //$("#dropdownlistRisk").data("kendoDropDownTree").value('<%= riskid%>');
            $("#dropdownlistRisk").data("kendoDropDownTree").value(["<%=riskid%>", "<%=riskid1%>", "<%=riskid2%>", "<%=riskid3%>"]);

        }

        function BindInternalType() {
            $("#dropdownType").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "Id",
                optionLabel: "Select Type",
                change: function (e) {
                    FilterGrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindInternalType?customerID=<% =CustId%>',
                            dataType: "json",
                           <%-- beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },--%>
                        }

                    }
                }
            });
        }

        function BindFunction() {
            $("#dropdownfunction").kendoDropDownTree({
                placeholder: "Category",
                filter: "contains",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "Id",
                change: function (e) {
                    FilterGrid();
                    fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindInternalFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=<%=rolename%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }
            });
            var dropdownlist = $("#dropdownfunction").data("kendoDropDownTree");
            dropdownlist.bind("dataBound", dropdownlist_dataBound);

            function dropdownlist_dataBound(e) {
                if ("<%=functionid%>" != "" && "<%=functionid%>" != "0") {
                    $("#dropdownfunction").data("kendoDropDownTree").value([<%=functionid%>]);
                }
            }
        }

        function BindDeptData() {
            $("#dropdownDept").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Department",
                change: function (e) {
                    FilterGrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindDeptList?UId=<% =UId%>&CId=<% =CustId%>&ComplianceType=0',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }

                    }
                }
            });
      <%--  <%if (graphfiltershowhide == 0)%>
        <%{%>
            $("#dropdownDept").data("kendoDropDownList").value('<%=Departmentid%>'); 
            <%}%>  
     --%>    

            $("#dropdownDept").data("kendoDropDownList").value('<%=Departmentid%>');
        }

        function BindPastData() {
            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    if ($("#dropdownPastData").val() != "All") {
                        $("#dropdownFY").data("kendoDropDownList").select(0);
                        fCreateStoryBoard('dropdownPastData', 'filterpstData1', 'pastdata');
                    }
                    BindGrid();
                    FilterGrid();
                },
                index: 4,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });
        }

        function BindStatus() {
            $("#dropdownlistStatus").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    FilterGrid();
                    fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
                },

                dataSource: [
                    { text: "Upcoming", value: "Upcoming" },
                    { text: "Overdue", value: "Overdue" },
                    { text: "Pending For Review", value: "PendingForReview" },
                    { text: "Rejected", value: "Rejected" },
                      <%if (IsNotCompiled == true)%>
                    <%{%>
                    { text: "Not Complied", value: "Not Complied" },
                    <%}%>
                    { text: "Closed Timely", value: "ClosedTimely" },
                    { text: "Closed Delayed", value: "ClosedDelayed" },
                    { text: "In Progress", value: "InProgress" },
                    { text: "Complied But Document Pending", value: "Complied But Document Pending" },
                    { text: "Not Applicable", value: "Not Applicable" },

                ]
            });
        }

        function BindUser() {

            $("#dropdownUser").kendoDropDownTree({
                placeholder: "User",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "FullName",
                dataValueField: "UID",
                change: function () {
                    FilterGrid();
                    fCreateStoryBoard('dropdownUser', 'filterUser', 'user');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =rolename%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                //read: "<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =rolename%>"
                    },
                }
            });
        }

        function BindLocation() {
            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //filter: "contains",
                autoClose: false,
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterGrid();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=MGMT&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: "<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=MGMT&IsStatutoryInternal=S"
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });
        }

        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
            $("#dropdownUser").data("kendoDropDownTree").value([]);
            $("#dropdownfunction").data("kendoDropDownTree").value([]);
            $("#dropdownDept").data("kendoDropDownList").select(0);
            if ($("#dropdownPastData").val() != "All") {
                $("#dropdownFY").data("kendoDropDownList").select(0);
                $("#dropdownPastData").data("kendoDropDownList").select(4);
                $('#Startdatepicker').val('');
                $('#Lastdatepicker').val('');
                BindGrid();
            }
            if ($("#dropdownFY").val() != "") {
                $("#dropdownPastData").data("kendoDropDownList").select(4);
                $("#dropdownFY").data("kendoDropDownList").select(0);
                $('#Startdatepicker').val('');
                $('#Lastdatepicker').val('');
                BindGrid();
            }
            $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
            $("#txtSearchComplianceID").val('');
            <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%>
            <%{%>
            $("#dropdownSequence").data("kendoDropDownList").select(0);
            <%}%>
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
        }

        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

    </script>

</head>
<body style="overflow-x: hidden;">
    <form>
        <div class="row">
            <div class="col-lg-12 col-md-12 colpadding0" style="width:99.2%">
                <h1 style="height: 30px; background-color: #f8f8f8; margin-top: 0px; font-weight: bold; font-size: 19px; color: #666; margin-bottom: 12px; padding-left: 5px; padding-top: 5px;">Internal Compliance List</h1>
                <h1 id="display" runat="server" style="display: none; margin-top: 2px; font-size: 20px; font-weight: 100; font-size: 17px; color: #666; font-weight: 500; margin-bottom: 17px;"></h1>
            </div>
        </div>
           <div id="example">
            <div style="margin: 0.5% 1.4% 0.5% 0px;width: 100%;">
                <input id="CustName" type="hidden" value="<% =CustomerName%>" />
                <input id="DisplayDepartment" type="hidden" value="<% =Departmentid%>" />
                <input id="IsLabel" type="hidden" value="<% =com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable%>" />
                <input id="dropdowntree" style="width: 25%; margin-right: 0.8%;" />
                <input id="dropdownPastData" style="width: 11.5%; margin-right: 0.8%;" />
                <input id="dropdownFY" style="width: 12%; margin-right: 0.8%;" />
                <input id="dropdownlistRisk" style="width: 11%; margin-right: 0.8%;" />
                <input id="dropdownUser" style="width: 11%; margin-right: 0.8%;" />
                <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" style="width: 11%; margin-right: 0.8%;" />
                <input id="Lastdatepicker" placeholder="End Date" style="width: 11%;" />
            </div>

              <div style="margin: 0 1.4% 9px 0;width: 100%;">
                <input id="dropdownType" style="width: 25%; margin-right: 0.8%;" />
                <input id="dropdownfunction" style="width: 11.5%; margin-right: 0.8%;" />
                <input id="txtSearchComplianceID" class="k-textbox" onkeydown="return (event.keyCode!=13);" placeholder="Compliance ID" style="width: 12%; margin-right: 0.8%" />
                <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%>
                <%{%>
                <%if (graphfiltershowhide == 0)%>
                <%{%>
                <div style="margin: 0.5% 0 0.5%; display: none">
                    <input id="dropdownSequence" style="width: 11%; margin-right: 0.8%; display: none" />
                </div>
                <%}
                    else
                    {%>
                <div style="margin: 0.5% 0 0.5%;">
                    <input id="dropdownSequence" style="width: 11%; margin-right: 0.8%;" />

                </div>
                <%}%>

                <%}%>

                <input id="dropdownlistStatus" style="width: 11%; margin-right: 0.8%;" />
                <input id="dropdownDept" style="width: 11%; margin-right: 0.8%;" />
                <button id="ClearfilterMain" style="float: right; height: 30px;margin-right: 0.8%;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear"></span>Clear</button>
                <button id="export" onclick="exportReport(event)" style="height: 30px; float: right; margin-right: 0.8%;"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
            </div>

              <div class="clearfix"></div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; font-weight: bold; display: none;" id="filtersstoryboard">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; font-weight: bold; display: none;" id="filterrisk">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; font-weight: bold; display: none;" id="filterstatus">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; font-weight: bold; display: none;" id="filterUser">&nbsp;</div>
            <div class="row" style="padding-bottom: 4px; font-size: 12px; font-weight: bold; display: none;" id="filterCategory">&nbsp;</div>
            <div id="grid" style="width:99%;margin-bottom: 10px"></div>
        </div>
        <div>
            <div class="modal fade" id="divApiOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" style="overflow: hidden;">
                <div class="modal-dialog" style="width: 1200px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopup();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="APIOverView" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px; z-index: 9999">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopupView();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <iframe id="downloadfile" src="about:blank" width="0" height="0" style="display"></iframe>
    </form>
</body>
</html>
