﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageEntity.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.ManageEntity" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-ui-1.10.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-ui-1.9.2.custom.min.js"></script>

    <style type="text/css">
        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        #caption
        {
            padding :10px;
            margin-bottom : 10px;
            width :  120px;
            color : black;
            font-size : 13px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .h1, h1 
        {
            font-size: 25px;
            margin-inline-end: -726px;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script type="text/javascript">     
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        $(document).ready(function () {
            window.parent.forchild($("body").height()+5);
        });
    </script>
</head>
<body style="overflow-x: hidden; height: 100%;">
    <form id="form1" runat="server">

        <header>
       <%-- <div class="col-lg-5 col-md-5 colpadding0">
                                    <h1 id="pagetype" style="height: 30px;background-color: #f8f8f8;margin-top: 0px;font-weight:bold;color: #666;font-size: 19px;margin-bottom: 12px;padding-left:5px;padding-top:5px;" >Entities</h1>
                                </div>--%>
            </header>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>        
        
            <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional">  <%--OnLoad="upDivLocation_Load"--%>
                <ContentTemplate>

                   

                        <table id="tbDashboardSummary" runat="server" style="width:100%;">
                            <tr>
                                <td style="vertical-align: top; width:50%;">
                                    <asp:UpdatePanel ID="dsf" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TreeView runat="server" ID="tvLocation" BackColor="White" SelectedNodeStyle-Font-Bold="true"
                                                NodeStyle-ForeColor="Black" Style="overflow: auto" ShowLines="true" OnSelectedNodeChanged="tvLocation_SelectedNodeChanged"
                                                NodeStyle-NodeSpacing="5px">
                                            </asp:TreeView>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="tvLocation" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                                <td style=" width:50%;">
                                    <table id="companyOverview" runat="server" style=" border: 1px solid #c7c7cc; border-collapse: collapse; width:100%;">
                                        <tr>
                                            <th style="border-color: #c7c7cc;" class="ui-widget-header">Company Overview
                                            </th>
                                        </tr>
                                        <tr style="height: 20px;">
                                            <td style="border: 1px solid #c7c7cc;">
                                                <div style="margin-bottom: 7px; margin-left: 5px">
                                                    <label style="width: 126px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Location :
                                                    </label>
                                                    <asp:Label ID="lblLocation" runat="server"></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="height: 20px;">
                                            <td style="border: 1px solid #c7c7cc;">
                                                <div style="margin-bottom: 7px; margin-left: 5px">
                                                    <label style="width: 126px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Total No of Users :
                                                    </label>
                                                    <asp:Label ID="lblTotalNoofUsers" runat="server"></asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="height: 70px; vertical-align: top;">
                                            <td style="border: 1px solid #c7c7cc;">
                                                <div style="margin-bottom: 7px; margin-left: 5px">
                                                    <label style="width: 126px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Approver Name(s) :  </label>
                                                    <%--<asp:Label ID="lblReviewerName" runat="server"></asp:Label>--%>
                                                    <div id="divApproverName" runat="server" style="float: left">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="height: 70px; vertical-align: top;">
                                            <td style="border: 1px solid #c7c7cc;">
                                                <div style="margin-bottom: 7px; margin-left: 5px">
                                                    <label style="width: 126px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Performer Name(s) :</label>
                                                    <div id="divPerformerName" runat="server" style="float: left">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border: 1px solid #c7c7cc; vertical-align: top;">
                                                <div style="margin-bottom: 7px; margin-left: 5px">
                                                    <label style="width: 126px; display: block; float: left; font-size: 13px; color: #333;">
                                                        Reviewer Name(s) :</label>
                                                    <div id="divReviewerName" runat="server" style="float: left">
                                                    </div>
                                                </div>
                                                   <script type="text/javascript">
                                                   function fhead(Compliances)
                                                   { 
                                                      $('#pagetype').html(val);
                                                          //  $('#sppagetype').html(val)   
                                                   }
                                                  </script>       
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                     
                </ContentTemplate>
            </asp:UpdatePanel>
        
       
    </form>
</body>
</html>
