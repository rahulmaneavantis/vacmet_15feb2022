﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base href="https://demos.telerik.com/kendo-ui/slider/events">
    <style>html { font-size: 14px; font-family: Arial, Helvetica, sans-serif; }</style>
    <title></title>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2020.2.617/styles/kendo.default-v2.min.css" />

    <script src="https://kendo.cdn.telerik.com/2020.2.617/js/jquery.min.js"></script>
    
    
    <script src="https://kendo.cdn.telerik.com/2020.2.617/js/kendo.all.min.js"></script>
    
    

    <link rel="stylesheet" href="../content/shared/styles/examples-offline.css">
    <script src="../content/shared/js/console.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="example">
            <div class="demo-section k-content">
                <ul id="fieldlist">
                    <li>
                        <label>Temperature</label>
                        <input id="slider" class="temperature" />
                    </li>
                    <li>
                        <label>Humidity</label>
                        <div id="rangeslider" class="humidity" >
                            <input style="Display:none;" />
                            <input style="Display:none;" />
                        </div>
                    </li>
                </ul>
                


            </div>

            <div class="box">
                <h4>Console log</h4>
                <div class="console"></div>
            </div>

            <script>
                function sliderOnSlide(e) {
                    kendoConsole.log("Slide :: new slide value is: " + e.value);
                }

                function sliderOnChange(e) {
                    kendoConsole.log("Change :: new value is: " + e.value);
                }

                function rangeSliderOnSlide(e) {
                    kendoConsole.log("Slide :: new slide values are: " + e.value.toString().replace(",", " - "));
                }

                function rangeSliderOnChange(e) {
                    kendoConsole.log("Change :: new values are: " + e.value.toString().replace(",", " - "));
                }

                $(document).ready(function() {
                    $("#slider").kendoSlider({
                        change: sliderOnChange,
                        slide: sliderOnSlide,
                        min: 0,
                        max: 100000,
                        smallStep: 500,
                        largeStep: 10,
                        value: 18
                    });

                    $("#rangeslider").kendoRangeSlider({
                        change: rangeSliderOnChange,
                        slide: rangeSliderOnSlide,
                        min: 0,
                        max: 100000,
                        smallStep: 1,
                        largeStep: 2,
                        tickPlacement: "both"
                    });
                });
            </script>
            <style>

               #fieldlist {
                   margin: 0 0 -2em;
                   padding: 0;
                   text-align: center;
               }

               #fieldlist > li {
                   list-style: none;
                   padding-bottom: 2em;
               }

               #fieldlist label {
                   display: block;
                   padding-bottom: 1em;
                   font-weight: bold;
                   text-transform: uppercase;
                   font-size: 12px;
                   color: #444;
               }

            </style>

        </div>


</asp:Content>
