﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ICFRObservationDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.ICFRObservationDetails" %>
<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Observation Details</title>

    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

     <script type="text/javascript">
        $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
    </script>

    <style type="text/css">
        .search-choice-close {
           visibility: hidden !important;
         }*

        .modal-dialog {
            display: table;
            overflow-y: auto;
            overflow-x: auto;
            width: 1000px;
            min-width: 700px;
        }

        /*.modal-body {           
            overflow-y: auto;
            max-height: 600px;
            padding: 15px;
        }*/

        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>
     <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
    </style> 
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional">
                <%--OnLoad="upDivLocation_Load"--%>
                <ContentTemplate>

                    <asp:UpdateProgress ID="updateProgress" runat="server">
                        <ProgressTemplate>
                            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.2;">
                                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>

                    <div class="panel-body">
                        <div class="col-md-12 colpadding0">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in"
                                ValidationGroup="ComplianceInstanceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                        </div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <div class="col-md-2 colpadding0">
                                    <p style="color: #999; margin-top: 5px;">Show </p>
                                </div>
                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                    <asp:ListItem Text="5" Selected="True" />
                                    <asp:ListItem Text="10" />
                                    <asp:ListItem Text="20" />
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                <%--<asp:DropDownListChosen runat="server" ID="ddlLegalEntity" PlaceHolder="Select Unit"
                                     class="form-control m-bot15 select_location" Style="float: left; width: 90%;"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                </asp:DropDownListChosen>--%>
                                 <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" DataPlaceHolder="Unit"
                                        class="form-control m-bot15 select_location" Width="90%" Height="32px"
                                        AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                                 <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" DataPlaceHolder="Sub Unit" 
                                    AllowSingleDeselect="false" DisableSearchThreshold="3"  class="form-control m-bot15" Width="95%" Height="32px"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged"> 
                                </asp:DropDownListChosen>
                                <%--<asp:DropDownList runat="server" ID="ddlSubEntity1" PlaceHolder="Select Sub Unit"
                                     class="form-control m-bot15 select_location" Style="float: left; width: 90%;"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged"> 
                                </asp:DropDownList>--%>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                               <%-- <asp:DropDownList runat="server" ID="ddlSubEntity2" PlaceHolder="Select Sub Unit" class="form-control m-bot15 select_location" Style="float: left; width: 90%;"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged"> 
                                </asp:DropDownList>--%>
                                 <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" DataPlaceHolder="Sub Unit" class="form-control m-bot15" Width="95%" Height="32px"
                                    AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                </asp:DropDownListChosen>
                            </div>

                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                              <%--  <asp:DropDownList runat="server" ID="ddlSubEntity3" class="form-control m-bot15 select_location" Style="float: left; width: 90%;"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged"> 
                                </asp:DropDownList>--%>
                                  <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" DataPlaceHolder="Sub Unit" class="form-control m-bot15" Width="95%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3"  AutoPostBack="true"  OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged"> 
                                </asp:DropDownListChosen>
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                               <%-- <asp:DropDownList runat="server" ID="ddlFilterLocation" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged"
                                   class="form-control m-bot15 select_location" Style="float: left; width: 90%;">
                                </asp:DropDownList> --%>
                                 <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged"
                                     DataPlaceHolder="Sub Unit" class="form-control m-bot15" Width="95%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" >
                                </asp:DropDownListChosen> 
                            </div>

                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                              <%--  <asp:DropDownList ID="ddlFinancialYear" runat="server"  class="form-control m-bot15 select_location"
                                     Style="float: left; width: 90%;">                                    
                                </asp:DropDownList>--%>
                                <asp:DropDownListChosen ID="ddlFinancialYear" runat="server" AutoPostBack="true"
                                     DataPlaceHolder="Financial Year" class="form-control m-bot15" Width="95%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3" >
                                    <%--OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged"--%>
                                </asp:DropDownListChosen>

                            </div>
                              <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                              <%--  <asp:DropDownList runat="server" ID="ddlProcess" AutoPostBack="true"
                                     class="form-control m-bot15 select_location"
                                    Style="float: left; width: 90%;">
                                </asp:DropDownList>--%>
                                  <asp:DropDownListChosen runat="server" ID="ddlProcess" AutoPostBack="true" 
                                      DataPlaceHolder="Process" class="form-control m-bot15" Width="95%" Height="32px"
                                   AllowSingleDeselect="false" DisableSearchThreshold="3">
                                </asp:DropDownListChosen>
                            </div>
                         
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0">                                                                              
                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; float: right;">
                                <div>
                                    <asp:Button ID="btnFilter" class="btn btn-search" runat="server" Text="Apply Filter(s)"  OnClick="btnTopSearch_Click" />                                    
                                </div>
                                <div style="float: right; margin-right: 10%;">
                                    <asp:Button ID="lbtnExportExcel" class="btn btn-search" runat="server" Text="Export To Excel" OnClick="lbtnExportExcel_Click"></asp:Button>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div>
                            <asp:GridView runat="server" ID="grdSummaryDetailsAuditCoverage" AutoGenerateColumns="false" 
                                GridLines="None" AllowSorting="true"
                                CssClass="table" CellPadding="4" ForeColor="Black" AllowPaging="true" PageSize="5" Width="100%"
                                Font-Size="12px" > 
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="RiskId"  Visible="false">
                                        <ItemTemplate>                                            
                                            <asp:Label ID="lblRiskCreationId" runat="server" Text='<%#Eval("RiskCreationId")%>'></asp:Label> 
                                            <asp:Label ID="lblProcessId" runat="server" Text='<%# Eval("ProcessId")%>'></asp:Label> 
                                            <asp:Label ID="lblSubProcessId" runat="server" Text='<%# Eval("SubProcessId")%>'></asp:Label>  
                                            <asp:Label ID="lblCustomerBranchId" runat="server" Text='<%# Eval("CustomerBranchId")%>'></asp:Label> 
                                             <asp:Label ID="lblKeyName" runat="server" Text='<%#Eval("KeyName")%>'></asp:Label>                                                                                                                                   
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ControlNo" HeaderText="Control&nbsp;No" />

                                    <asp:TemplateField HeaderText="Location">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                            <asp:Label ID="Label2" runat="server" data-toggle="tooltip" data-placement="bottom"  Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Process">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                            <asp:Label ID="Label3" runat="server" data-toggle="tooltip" data-placement="bottom"  Text='<%# Eval("ProcessName") %>' ToolTip='<%# Eval("ProcessName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sub&nbsp;Process">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 60px;">
                                            <asp:Label ID="Label4" runat="server" data-toggle="tooltip" data-placement="bottom"  Text='<%# Eval("SubProcessName") %>' ToolTip='<%# Eval("SubProcessName") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Risk">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                            <asp:Label ID="Label5" runat="server" data-toggle="tooltip" data-placement="bottom"  Text='<%# Eval("ActivityDescription") %>' ToolTip='<%# Eval("ActivityDescription") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Control">
                                        <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                            <asp:Label ID="Label5" runat="server" data-toggle="tooltip" data-placement="bottom"  Text='<%# Eval("ControlDescription") %>' ToolTip='<%# Eval("ControlDescription") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                                                  
                                </Columns>
                                <RowStyle CssClass="clsROWgrid" />
                                <HeaderStyle CssClass="clsheadergrid" />
                                    <PagerSettings Visible="false" />    
                                <PagerTemplate>
                                    <%--<table style="display: none">
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                    </table>--%>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    No Records Found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                                <div style="float: right;">
                  <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                      class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                  </asp:DropDownListChosen>  
                </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p>
                                            <asp:Label runat="server" ID="lblTotalSelected" Text="" Style="color: #999; margin-right: 10px;"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0" style="float: right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="lBPrevious_Click" />--%>
                                    <div class="table-paging-text" style="margin-top:-35px;margin-left:19px;">
                                        <p>Page
                                          <%--  <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="lBNext_Click" />--%>
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                    </div>

                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnFilter" />
                    <asp:PostBackTrigger ControlID="lbtnExportExcel" />
                </Triggers>
            </asp:UpdatePanel>            
        </div>
    </form>
</body>
</html>
