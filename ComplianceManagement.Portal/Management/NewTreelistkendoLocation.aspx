﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewTreelistkendoLocation.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.NewTreelistkendoLocation" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" style="overflow-x: hidden;">
<head runat="server">
    <title></title>
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>

    <style type="text/css">
        input[type=checkbox], input[type=radio] {
            margin: 4px 4px 0;
            line-height: normal;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
        }

        #grid1 .k-grid-content {
            min-height: 320px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #666666;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        #grid .k-grouping-header {
            font-style: italic;
            margin-top: -7px;
            background-color: white;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-multiselect-wrap .k-input {
            /*padding-top:6px;*/
            display: inherit !important;
        }
        .k-filter-menu .k-button {
            width: 27%;
            background-color: #E9EAEA;
            color: black;
            border: 2px solid rgba(0, 0, 0, 0.5);
        }
    </style>

    <script>
        $(document).ready(function () {

            $("#grid").kendoTreeList({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>/Data/GetMGMTTopTopLocations?Customerid=<% =CustId%>&UserID=<% =UId%>&Flag=<%=ComplianceTypeFlag%>&Role=<%=Role%>',
                            dataType: "json",
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            parentId: "ParentID",
                            fields: {
                                ParentID: { field: "ParentID", nullable: true },
                                ID: { field: "ID", type: "number",defaultValue: 0 }
                            },
                            expanded: false
                        },
                    },
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                toolbar:false,
                pageable: {
                    refresh: true
                },
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBound: function (e) {
                    window.parent.forchild($("body").height() + 50);
                },
                columns:
                    [
                        {
                            field: "Location", title: "Location",
                            attributes: {
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "Address", title: "Address",
                            attributes: {
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "EmailID", title: "Email",
                            attributes: {
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        },
                        {
                            field: "ContactPerson", title: "Contact Person",
                            attributes: {
                                style: 'white-space: nowrap;'
                            },
                            filterable: {
                                multi: true,
                                extra: false,
                                search: true,
                                operators: {
                                    string: {
                                        eq: "Is equal to",
                                        neq: "Is not equal to",
                                        contains: "Contains"
                                    }
                                }
                            }
                        }
                    ],

            });

            $("#grid").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "th",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

        });

        function ClearAllFilterMain(e) {
            $("#grid").data("kendoTreeList").dataSource.filter({});
            e.preventDefault();
        }
    </script>
</head>
<body>
    <div class="row">
        <div class="col-lg-12 col-md-12 colpadding0">
            <h1 style="height: 25px; background-color: #f8f8f8; margin-top: 0px; font-weight: bold; font-size: 19px; color: #666; margin-bottom: 12px; padding-left: 5px; padding-top: 5px;">Locations</h1>
        </div>
    </div>
    <div id="example">
        <div style="margin: 0.5% 0 0.5%;">
            <button id="ClearfilterMain" style="height: 30px;margin-left: 94.6%;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
        </div>
          <div class="clearfix"></div>
       
            <div id="grid" style="margin-bottom:10px"></div>
       </div>

</body>
</html>
