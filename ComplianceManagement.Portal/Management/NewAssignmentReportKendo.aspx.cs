﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class NewAssignmentReportKendo : System.Web.UI.Page
    {
        protected  int CustId;
        protected  int UId;
        protected  string roles;
        public  string CompDocReviewPath = "";
        protected  string Path;
        protected  string CustomerName;
        protected  string Authorization;
        protected bool DeptHead = false;        
        protected  string Internalsatutory;
        protected int StatusFlagID;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
                string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
                Authorization = (string)HttpContext.Current.Cache[CacheName];
                if (Authorization == null)
                {
                    Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                    HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                }
                Path = ConfigurationManager.AppSettings["KendoPathApp"];
                UId = Convert.ToInt32(AuthenticationHelper.UserID);
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var ishead = GetIsDeptHead(UId);
                if (ishead)
                {
                    roles = "DEPT";
                }
                else if (AuthenticationHelper.Role == "EXCT")
                {
                    roles = "PRA";
                }
                else
                {
                    roles = Convert.ToString(AuthenticationHelper.Role);
                }

                CustomerName = GetCustomerName(CustId);

                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    var Internalsatutory1 = Request.QueryString["Internalsatutory"];
                    if (Internalsatutory1 == "Statutory")
                    {
                        Internalsatutory = "S";
                    }
                    else if (Internalsatutory1 == "StatutoryAll")
                    {
                        Internalsatutory = "S";
                    }
                    else
                    {
                        Internalsatutory = "I";
                    }
                }

                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    var Internalsatutory2 = Request.QueryString["Internalsatutory"];
                    if (Internalsatutory2 == "Statutory")
                    {
                        StatusFlagID = -1;
                    }
                    else if (Internalsatutory2 == "StatutoryAll")
                    {
                        StatusFlagID = -1;
                    }
                    else
                    {
                        StatusFlagID = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public static bool GetIsDeptHead(int uid)
        {
            try
            {
                bool CName = false;
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var bval = (from row in entities.Users
                                where row.ID == uid && row.IsHead != null
                                select row.IsHead).FirstOrDefault();
                    if (bval != null)
                    {
                        CName = (bool)bval;
                    }
                    return CName;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
        public static string GetCustomerName(int CID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {

                string CName = (from row in entities.Customers
                                where row.ID == CID
                                select row.Name).FirstOrDefault();

                return CName;
            }
        }
    }
}