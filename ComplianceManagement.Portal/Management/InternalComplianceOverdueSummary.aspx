﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InternalComplianceOverdueSummary.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.InternalComplianceOverdueSummary" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>

    <style type="text/css">
        div.k-grid-footer, div.k-grid-header {
            padding-right: 17px;
            border-top-width: 1px;
            margin-right: 1px;
        }
        .k-grid-content {
            max-height: 100Px !important;
        }

            .k-grid-content k-auto-scrollable {
                height: auto;
            }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1em;
            border-bottom-width: 1px;
        }

        .k-grid td {
            border-style: solid;
            padding: .4em .6em;
            overflow: hidden;
            line-height: 1.6em;
            vertical-align: middle;
            text-overflow: ellipsis;
            background-color: white;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: white;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: white;
            display: none;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            display: none;
            margin-top: -3px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-edge .k-pager-info, .k-ff .k-pager-info, .k-ie11 .k-pager-info, .k-safari .k-pager-info, .k-webkit .k-pager-info {
            display: none;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
            background-color: white;
            border-color: white;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }


        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-weight: bold;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        td.k-command-cell {
            border-width: 0px 1px 1px 1px;
            text-align: center;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }

        .change-condition {
            color: blue;
        }
    </style>


    <script type="text/x-kendo-template" id="template">      
               
    </script>

    <script type="text/javascript">
        function BindGrid() {
            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                columnMenuInit(e) {
                    e.container.find('li[role="menuitemcheckbox"]:nth-child(8)').remove();
                },
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetMGMTOverdueComplianceSummary?Userid=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID %>&Customerid=<%= com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID %>&IsFlag=1&MonthId=All&FY=0&Isdept=0&IsApprover=<% =isapprover%>&RiskId=0',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Data/GetMGMTOverdueComplianceSummary?Userid=<% =UId%>&Customerid=<% =CustId%>&IsFlag=1&MonthId=All&FY=0&Isdept=0&IsApprover=<% =isapprover%>&RiskId=0'

                    },
                    schema: {
                        data: function (response) {
                            return response[0].IList;
                        },
                        total: function (response) {
                            return response[0].IList.length;
                        }
                    },
                    pageSize: 2
                },
                excel: {
                    allPages: true,
                },

                toolbar: kendo.template($("#template").html()),
                height: 180,
                sortable: false,
                filterable: false,
                columnMenu: false,
                reorderable: false,
                resizable: false,
                multi: true,
                selectable: false,
                columns: [

                    { hidden: true, field: "RiskCategory", title: "Risk" },
                    { hidden: true, field: "CustomerBranchID", title: "BranchID" },

                    {
                        field: "ShortDescription", title: 'Compliance Items',
                        width: "25%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Performer", title: 'Responsibility',
                        width: "20%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "InternalScheduledOn", title: 'Due&nbsp;Date',
                        type: "date",
                        width: "10%",
                        template: "#= kendo.toString(kendo.parseDate(InternalScheduledOn, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    //{
                    //    field: "ForMonth", title: 'Period',
                    //    width: "10%",
                    //    filterable: {
                    //        extra: false,
                    //        operators: {
                    //            string: {
                    //                eq: "Is equal to",
                    //                neq: "Is not equal to",
                    //                contains: "Contains"
                    //            }
                    //        }
                    //    }
                    //},
                    {
                        field: "RiskCategory", title: 'Risk',
                        width: "10%",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            {
                                name: "edit2", text: "  ", iconClass: "k-icon k-i-eye", className: "ob-overview",
                            }
                        ], title: "Action", width: "7%;", headerAttributes: {
                            style: "text-align: center;"
                        }
                    }
                ]
            });
            $("#grid").kendoTooltip({
                filter: "th",
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Overview";
                }
            });
            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "" && content != "  ") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");
            $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                parent.OpenOverViewpup(item.InternalScheduledOnID, item.InternalComplianceInstanceID, 'Internal');
                return true;
            });

        }

        $(document).ready(function () {
            BindGrid();
        });
    </script>

    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div id="example">
                <div id="grid" style="border: none;"></div>
            </div>
        </div>
    </form>
</body>
</html>
