﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Sendemail.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.Sendemail" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" style="margin-top: -47px;">
<head id="Head1" runat="server">


    <title></title>
    <style type="text/css">
        /*.auto-style1 {
            width: 367px;
        }*/
        txttomail:focus {
            border: 1px solid #78adf5;
        }
          .label-info-selected {
            color: #007aff;
            border: 1px solid;
            border-color: #007aff;
            background: 0 0;
        }
          
        span input[type=checkbox]:checked {
            color: #007aff;
            border: 1px solid;
            border-color: #007aff;
            background: none;
        }

        .label-info, .label-info-selected {
            font-size: 100%;
        }

            .label-info:active, .label-info:focus, .label-info:hover {
                color: #007aff;
                border: 1px solid;
                border-color: #007aff;
                background: 0 0;
            }

    .form-control:focus {
    color: #151617;
    outline: 0;
    background-color: #f7f7f7;
    border-color: #007aff;
    box-shadow: none;
}
        .form-control {
            padding: 5px 0px;
            font-size: 14px;
            color: black;
            background-color: #fff;
                border: 1px solid #007aff;
            border: 1px solid #c7c7cc;
            border-radius: 4px;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" aria-invalid="spelling" style="color: #f2f2f2">
        <br />
        <br />
        <table style="width: 100%; margin-right: 0px; margin-top: 0px;">



            <div style="margin: 5px 0px">

                <asp:Label ID="labelId" Style="font-family: sans-serif,'Roboto';display:inline-block;width:80px;font-size: 17px;color: black;" runat="server">To</asp:Label>


                <span>
                    <asp:TextBox ID="txttomail" autocomplete="off" data-role="tagsinput" CssClass="form-control" runat="server" Height="35px" Width="91%" display="block" float="left" Font-Size="15px" border-radius="8px" margin="0px 5px" color="#666666" font-weight="normal" font-family=" 'Roboto',sans-serif" CausesValidation="True"></asp:TextBox></span>

            </div>
            <div style="margin: 5px 0px">
                <asp:Label ID="label2" Style="font-family: sans-serif,'Roboto';display:inline-block;width:80px;font-size: 17px;color: black;" runat="server">CC</asp:Label>
                <span>
                    <asp:TextBox ID="txtcc" CssClass="form-control" autocomplete="off" runat="server" Height="35px" Width="91%" border-radius="8px" color="#666666" font-weight="normal" Font-Size="15px" font-family=" 'Roboto',sans-serif"></asp:TextBox></span>

            </div>



            <div style="margin: 5px 0px">
                <asp:Label ID="label3" Style="font-family: sans-serif,'Roboto';display:inline-block;width:80px;font-size: 17px;color: black;"  runat="server">Subject</asp:Label>
                <span>
                    <asp:TextBox ID="txtsub" CssClass="form-control" autocomplete="off" runat="server" Height="30px" Width="91%" border-radius="8px" Font-Size="15px" color="#666666" font-weight="normal" font-family=" 'Roboto',sans-serif"></asp:TextBox></span>


            </div>
            <div style="margin: 5px 0px">
                <asp:Label ID="label4" Style="font-family: sans-serif,'Roboto';display:inline-block; vertical-align: 60px; width:80px;font-size: 17px;color: black;"  runat="server">Message</asp:Label>
                <span>
                    <asp:TextBox ID="txtmsg" runat="server" TextMode="MultiLine" Height="105px" margin-left="84px" margin-top="-19px" Width="91%" border-radius="8px" CssClass="form-control" Font-Size="15px" color="#666666" font-weight="normal" font-family=" 'Roboto',sans-serif"></asp:TextBox>
                </span>
            </div>

          <%--  <div class="checkbox" style="">
                <label style="color: black;font-family: sans-serif,'Roboto';/* display: inline-block; */width: 80px;margin-left: 61px;font-size: 15px;color: black;">
                    <input type="checkbox">
                    Send a copy of this email to myself
                </label>
            </div>--%>
             <asp:Button ID="Button3" runat="server" BackColor="#F0F0F0" Font-Bold="False" Font-Overline="False" Font-Size="Medium" Font-Strikeout="False" Font-Underline="False" ForeColor="Black" Style="margin-left: 500px" OnClick="btnsend_Click" Text="Send" Width="106px" />
            <div>
            <asp:Label ID="Label1" runat="server" Text="Label" Visible="false" ForeColor="Green" Font-Size="20px" font-family=" 'Roboto',sans-serif"></asp:Label>
        </div>
        </table>
        <br />
        <br />
    </form>
</body>
</html>
