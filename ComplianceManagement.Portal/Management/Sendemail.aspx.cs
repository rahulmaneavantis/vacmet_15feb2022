﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using Newtonsoft.Json;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Properties;
using System.Text;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class Sendemail : System.Web.UI.Page
    {

        protected List<string> ToSENTUSER;
        string _actName;
        string _ShortCompliance;
        string _PerFormer;
        string _ReViewer;
        DateTime _ScheduledOn;
        string _OverdueBy;
        string _Status;
        String _user;
        String _Message;
        long ComplianceInstID;
        int Userid;
        int customerid;      
        long _ScheduleOnid;

        int RiskId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    Userid = Convert.ToInt32(AuthenticationHelper.UserID);
                    var keys = Request.QueryString["MailChecklist"];
                    if (keys != null)
                    {
                        #region Multi select
                        dynamic key = JsonConvert.DeserializeObject(keys);
                        List<string> PerRev = new List<string>();
                        List<string> Apr = new List<string>();

                        foreach (var item in key)
                        {
                            ComplianceInstID = Convert.ToInt64(item["instanceid"].Value);
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                long instanceid = ComplianceInstID;
                                var Email = (from row in entities.ComplianceAssignments
                                             join row1 in entities.Users
                                             on row.UserID equals row1.ID
                                             where row.ComplianceInstanceID == instanceid
                                             select new
                                             {
                                                 email = row1.Email,
                                                 roleid = row.RoleID
                                             }).ToList();

                                var p = Email.Where(en => en.roleid == 3).Select(ent => ent.email).Distinct().FirstOrDefault();
                                if (p!=null)
                                {
                                    PerRev.Add(p);
                                }
                                var r = Email.Where(en => en.roleid == 4).Select(ent => ent.email).Distinct().FirstOrDefault();
                                if (r != null)
                                {
                                    PerRev.Add(r);
                                }
                                var a = Email.Where(en => en.roleid == 6).Select(ent => ent.email).Distinct().FirstOrDefault();
                                if (a != null)
                                {
                                    Apr.Add(a);
                                }
                            }
                        }

                      
                        if (PerRev.Count > 0)
                        {
                            var a = PerRev.Distinct().ToList();
                            txttomail.Text = string.Join(",", a.Select(p => p.ToString()));
                        }
                        txttomail.Enabled = false;
                        
                        if (Apr.Count > 0)
                        {
                            var b= Apr.Distinct().ToList();
                            txtcc.Text = string.Join(",", b.Select(p => p.ToString()));
                        }
                        txtcc.Enabled = false;

                        #endregion
                    }
                    else
                    {

                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {

                            var ComplianceInstID = Convert.ToInt32(Request.QueryString["CIID"]);

                            var Email = (from row in entities.ComplianceAssignments
                                         join row1 in entities.Users
                                         on row.UserID equals row1.ID
                                         where row.ComplianceInstanceID == ComplianceInstID
                                         select new
                                         {
                                             email = row1.Email,
                                             roleid = row.RoleID
                                         }).ToList();


                            var PerRev = Email.Where(en => en.roleid == 3 || en.roleid == 4).Select(ent => ent.email).Distinct().ToList();
                            if (PerRev.Count > 0)
                            {
                                txttomail.Text = string.Join(",", PerRev.Select(p => p.ToString()));
                            }
                            var Apr = Email.Where(en => en.roleid == 6).Select(ent => ent.email).Distinct().ToList();
                            if (Apr.Count > 0 && Apr != null)
                            {
                                txtcc.Text = string.Join(",", Apr.Select(p => p.ToString()));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
          
        }

        protected void btnsend_Click(object sender, EventArgs e)
        {
            try
            {
                List<long> schedulonids = new List<long>();
                var keys = Request.QueryString["MailChecklist"];
                if (keys != null)
                {
                    #region Multipal Email
                    try
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            int cusid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            int userid = AuthenticationHelper.UserID;
                            dynamic key = JsonConvert.DeserializeObject(keys);
                            var userdata = (from row in entities.Users
                                               where row.ID == userid
                                               select row).FirstOrDefault();

                            ToSENTUSER = new List<string>();
                            ToSENTUSER.Add(userdata.Email);
                            //List<string> toEmail = txttomail.Text.Trim(',').Split(',').ToList<string>();

                            List<string> ccId = new List<string>();
                            //if (!string.IsNullOrEmpty(txtcc.Text))
                            //{
                            //    ccId = txtcc.Text.Trim(',').Split(',').ToList<string>();
                            //}

                      
                            List<long> sid = new List<long>();
                           
                            foreach (var item in key)
                            {
                                sid.Add(Convert.ToInt64(item["IDSheduleOn"].Value));
                            }

                            var ss =string.Join(",", sid.Select(p => p.ToString()));

                            var masterdatalist = (from row in entities.SP_OverdueEmail(cusid, "M", 0, ss)
                                        select row).ToList();

                            #region indvidual Email
                            var plist = masterdatalist.Select(en => en.PerformerName).Distinct().ToList();
                            foreach (var item in plist)
                            {
                                
                             
                                var EscalationEmail = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_OverDue_OverdueMail
                                                           .Replace("@_user", item)
                                                           .Replace("@_Message", txtmsg.Text.Trim());

                                StringBuilder details = new StringBuilder();
                                var adata = masterdatalist.Where(wns => wns.PerformerName == item).ToList();

                                var instanceid = adata.Select(en => en.ComplianceInstanceID).FirstOrDefault();

                                var userEmail = (from row in entities.ComplianceAssignments
                                             join row1 in entities.Users
                                             on row.UserID equals row1.ID
                                             where row.ComplianceInstanceID == instanceid
                                             select new
                                             {
                                                 email = row1.Email,
                                                 roleid = row.RoleID
                                             }).ToList();


                                string cc = string.Empty;
                                var PerRev = userEmail.Where(en => en.roleid == 3 || en.roleid == 4).Select(ent => ent.email).Distinct().ToList();
                                var Apr = userEmail.Where(en => en.roleid == 6).Select(ent => ent.email).Distinct().ToList();
                                if (Apr.Count > 0 && Apr != null)
                                {
                                    cc = string.Join(",", Apr.Select(p => p.ToString()));
                                }
                              
                                if (!string.IsNullOrEmpty(cc))
                                {
                                    ccId = cc.Trim(',').Split(',').ToList<string>();
                                }


                                foreach (var item1 in adata)
                                {                                                                                                   
                                    details.AppendLine(Settings.Default.EmailTemplate_OverDue_Overdue_MailRows
                                                   .Replace("@Act_Name", item1.ActName)
                                                   .Replace("@Compliance", item1.ShortDescription)
                                                   .Replace("@Performer", item1.PerformerName)
                                                   .Replace("@Reviwer", item1.ReviewerName)
                                                   .Replace("@DueDate", item1.ScheduledOn.ToString("dd-MMM-yyyy"))
                                                   .Replace("@OverDue_By", Convert.ToString(item1.OverdueBy))
                                                   .Replace("@Status", item1.Status)
                                                   );
                                }
                                var EmailSubj1 = Convert.ToString(txtsub.Text);
                                string message1 = EscalationEmail.Replace("@_user", "User").Replace("@Details", details.ToString());
                                if (ccId.Count > 0)
                                {
                                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), PerRev, ccId, null, EmailSubj1, message1);
                                }
                                else
                                {
                                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), PerRev, null, null, EmailSubj1, message1);
                                }
                            }
                            #endregion


                            #region Sender Email
                            var EscalationEmailSender = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_OverDue_OverdueMail
                                                       .Replace("@_user", userdata.FirstName + ' ' + userdata.LastName)
                                                       .Replace("@_Message", txtmsg.Text.Trim());
                            StringBuilder senderdetails = new StringBuilder();
                            foreach (var item in masterdatalist)
                            {
                                senderdetails.AppendLine(Settings.Default.EmailTemplate_OverDue_Overdue_MailRows
                                                   .Replace("@Act_Name", item.ActName)
                                                   .Replace("@Compliance", item.ShortDescription)
                                                   .Replace("@Performer", item.PerformerName)
                                                   .Replace("@Reviwer", item.ReviewerName)
                                                   .Replace("@DueDate", item.ScheduledOn.ToString("dd-MMM-yyyy"))
                                                   .Replace("@OverDue_By", Convert.ToString(item.OverdueBy))
                                                   .Replace("@Status", item.Status)
                                                   );
                            }
                            var EmailSubj = Convert.ToString(txtsub.Text);
                            string message = EscalationEmailSender.Replace("@_user", "User").Replace("@Details", senderdetails.ToString());
                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), ToSENTUSER, null, null, EmailSubj, message);
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                    #endregion
                }
                else
                {
                    try
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            int cusid =Convert.ToInt32(AuthenticationHelper.CustomerID);
                            int userid = AuthenticationHelper.UserID;
                            var ComplianceInstID = Convert.ToInt32(Request.QueryString["CIID"]);
                            var ComplianceScheduleonID = Convert.ToInt32(Request.QueryString["CSOID"]);
                            var data = (from row in entities.SP_OverdueEmail(cusid,"S", ComplianceScheduleonID,"")
                                               select row).FirstOrDefault();
                            if (data !=null)
                            {

                                StringBuilder details = new StringBuilder();
                                var userEmailid = (from row in entities.Users
                                                   where row.ID == userid
                                                   select row.Email).FirstOrDefault();

                                ToSENTUSER = new List<string>();
                                ToSENTUSER.Add(userEmailid);

                                var EscalationEmail = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_OverDue_OverdueMail
                                                        .Replace("@_user", data.PerformerName)
                                                        .Replace("@_Message", txtmsg.Text.Trim());

                                details.AppendLine(Settings.Default.EmailTemplate_OverDue_Overdue_MailRows
                                               .Replace("@Act_Name", data.ActName)
                                               .Replace("@Compliance", data.ShortDescription)
                                               .Replace("@Performer", data.PerformerName)
                                               .Replace("@Reviwer", data.ReviewerName)
                                               .Replace("@DueDate", data.ScheduledOn.ToString("dd-MMM-yyyy"))
                                               .Replace("@OverDue_By", Convert.ToString(data.OverdueBy))
                                               .Replace("@Status", data.Status)
                                               );


                                List<string> toEmail = txttomail.Text.Trim(',').Split(',').ToList<string>();
                                List<string> ccId = new List<string>();
                                if (!string.IsNullOrEmpty(txtcc.Text))
                                {
                                  ccId = txtcc.Text.Trim(',').Split(',').ToList<string>();
                                }
                               

                                var EmailSubj = Convert.ToString(txtsub.Text);

                                string message = EscalationEmail.Replace("@_user", "User").Replace("@Details", details.ToString());
                                if (ccId.Count>0)
                                {
                                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), toEmail, ccId, null, EmailSubj, message);
                                }
                                else
                                {
                                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), toEmail, null, null, EmailSubj, message);
                                }

                                EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), ToSENTUSER, null, null, EmailSubj, message);
                            }                       
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }

                Label1.Visible = true;
                Label1.Text = "Your email has been sent successfully";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private string createEmailBody()
        {
          var  EmailMsg = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_OverDue_OverdueMail
                    .Replace("@_actName", _actName)
                    .Replace("@_ShortCompliance", _ShortCompliance)
                    .Replace("@_PerFormer", _PerFormer)
                    .Replace("@_ReViewer", _ReViewer)
                    //.Replace("@_ScheduledOn", _ScheduledOn)
                    .Replace("@_OverdueBy", _OverdueBy)
                    .Replace("@_Status", _Status)
                    .Replace("@_user", _user)
                    .Replace("@_Message", _Message);

            return EmailMsg;

        }
    }
}

