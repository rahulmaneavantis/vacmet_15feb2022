﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Saplin.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class AuditManagementDashboard : System.Web.UI.Page
    {
        protected List<long> CHKBranchlist = new List<long>();
        public static string ObservationStatusChart;
        public static string ProcessWiseObservationStatusChart;
        public static string ObservationProcessWiseAgingChart;
        protected static String[] items;
        public  List<long> Branchlist = new List<long>();
        protected int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            items = new String[] { "Not Started", "In Progress", "Under Review", "Closed" };

            if (!IsPostBack)
            {
                HiddenField home = (HiddenField)Master.FindControl("Ishome");
                home.Value = "true";
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindProcess("P");
                BindVertical();
                BindFinancialYear();
                BindLegalEntityData();                               
                ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling", "-1"));
                ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFinancialYearMultiSelect.ClearSelection();
                    ddlFinancialYearMultiSelect.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }
                BindGraphData(null);               
            }
        }
        private void BindProcess(string flag)
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                ddlProcessMultiSelect.Items.Clear();
                ddlProcessMultiSelect.DataTextField = "Name";
                ddlProcessMultiSelect.DataValueField = "Id";
                ddlProcessMultiSelect.DataSource = ProcessManagement.FillProcessDropdownManagement(CustomerId, Portal.Common.AuthenticationHelper.UserID);
                ddlProcessMultiSelect.DataBind();
                int ddlProcessCount = ddlProcessMultiSelect.Items.Count;
                Session["ProcessCount"] = ddlProcessCount;
                //ddlProcessMultiSelect.Items.Insert(0, new ListItem("Select Process", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindVertical()
        {
            try
            {
                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                //var Vertical_List = UserManagementRisk.FillVerticalListFromRiskActTrasa();

                ddlVerticalList.DataTextField = "VerticalName";
                ddlVerticalList.DataValueField = "VerticalsId";
                ddlVerticalList.Items.Clear();
                ddlVerticalList.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasaBYCustomer(CustomerId);
                ddlVerticalList.DataBind();
                //ddlVertical.Items.Insert(0, new ListItem("Select Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindFinancialYear()
        {
            ddlFinancialYearMultiSelect.DataTextField = "Name";
            ddlFinancialYearMultiSelect.DataValueField = "ID";
            ddlFinancialYearMultiSelect.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYearMultiSelect.DataBind();
            Session["FinancialYearCount"] = ddlFinancialYearMultiSelect.Items.Count;
            //ddlFinancialYearMultiSelect.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }        
        public void BindLegalEntityData()
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
            ddlLegalEntityMultiSelect.DataTextField = "Name";
            ddlLegalEntityMultiSelect.DataValueField = "ID";
            ddlLegalEntityMultiSelect.Items.Clear();
            ddlLegalEntityMultiSelect.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataManagement(CustomerId,UserID);
            ddlLegalEntityMultiSelect.DataBind();
            Session["LegalEntityCount"] = ddlLegalEntityMultiSelect.Items.Count;
            //ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }        
        public void BindSubEntityDataList(DropDownCheckBoxes DRP, List<long> Parentlist)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityDataList(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, CustomerId, Parentlist);
            DRP.DataBind();
            if (DRP.ID == "ddlSubEntity1")
            {
                Session["SubEntity1Count"] = DRP.Items.Count;
            }
        }            
        public void BindSchedulingType()
        {
            //int branchid = -1;

            //if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            //{
            //    if (ddlLegalEntity.SelectedValue != "-1")
            //    {
            //        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
            //    }
            //}
            //if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            //{
            //    if (ddlSubEntity1.SelectedValue != "-1")
            //    {
            //        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
            //    }
            //}
            //if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            //{
            //    if (ddlSubEntity2.SelectedValue != "-1")
            //    {
            //        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
            //    }
            //}
            //if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            //{
            //    if (ddlSubEntity3.SelectedValue != "-1")
            //    {
            //        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
            //    }
            //}
            //if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            //{
            //    if (ddlFilterLocation.SelectedValue != "-1")
            //    {
            //        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
            //    }
            //}
            //ddlSchedulingType.DataTextField = "Name";
            //ddlSchedulingType.DataValueField = "ID";
            //ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            //ddlSchedulingType.DataBind();
            //ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
            int branchid = -1;
            if (!String.IsNullOrEmpty(ddlLegalEntityMultiSelect.SelectedValue))
            {
                if (ddlLegalEntityMultiSelect.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntityMultiSelect.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }
            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }        
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }        
        protected void btnTopSearch_Click(object sender, EventArgs e)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    List<SP_PrequsiteCount_Result> Masterrecord = new List<SP_PrequsiteCount_Result>();
                    List<SP_AuditCountView_Dashboard_Result> dataAuditSummary = new List<SP_AuditCountView_Dashboard_Result>();
                    List<SP_ImplimentInternalAuditClosedCountView_Result> dataImpliment = new List<SP_ImplimentInternalAuditClosedCountView_Result>();
                    List<SP_AuditCountView_Dashboard_Result> PRdataAuditSummary = new List<SP_AuditCountView_Dashboard_Result>();
                    List<SP_ImplimentInternalAuditClosedCountView_Result> PRdataImpliment = new List<SP_ImplimentInternalAuditClosedCountView_Result>();
                    string statusListwithComma = string.Empty;
                    List<string> StatusListAudit = new List<string>();
                    for (int i = 0; i < ddlStatusList.Items.Count; i++)
                    {
                        if (ddlStatusList.Items[i].Selected == true)
                        {
                            StatusListAudit.Add(ddlStatusList.Items[i].Value);
                        }
                    }
                    if (StatusListAudit.Count > 0)
                    {
                        statusListwithComma = string.Join(",", StatusListAudit);
                    }

                    //Process
                    entities.Database.CommandTimeout = 300;
                    var ProcessRecord = (from c in entities.SP_AuditCountView_Dashboard((int)Portal.Common.AuthenticationHelper.CustomerID)
                                         select c).ToList();


                    entities.Database.CommandTimeout = 300;
                    Masterrecord = (from row in entities.SP_PrequsiteCount(Portal.Common.AuthenticationHelper.UserID)
                                    select row).ToList();

                    if (ProcessRecord.Count > 0)
                    {
                        PRdataAuditSummary = ProcessRecord;
                        dataAuditSummary = (from C in ProcessRecord
                                            join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                            on C.CustomerBranchID equals EAAMR.BranchID
                                            where C.ProcessId == EAAMR.ProcessId
                                            && C.CustomerID == CustomerId
                                            && EAAMR.ISACTIVE == true
                                            && EAAMR.UserID == Portal.Common.AuthenticationHelper.UserID
                                            select C).ToList();
                    }
                    //Implementation
                    entities.Database.CommandTimeout = 300;
                    var ImplementationRecord = (from c in entities.SP_ImplimentInternalAuditClosedCountView(CustomerId)
                                                select c).ToList();

                    if (ImplementationRecord.Count > 0)
                    {
                        PRdataImpliment = ImplementationRecord;
                        dataImpliment = (from C in ImplementationRecord
                                         join EAAMR in entities.EntitiesAssignmentAuditManagerRisks
                                         on C.CustomerBranchID equals EAAMR.BranchID
                                         where C.ProcessID == EAAMR.ProcessId
                                         && C.CustomerID == CustomerId
                                         && EAAMR.UserID == Portal.Common.AuthenticationHelper.UserID
                                         && EAAMR.ISACTIVE == true
                                         select C).ToList();
                    }

                    List<string> FinancialYearList = new List<string>();
                    for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                    {
                        if (ddlFinancialYearMultiSelect.Items[i].Selected == true)
                        {
                            FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                        }
                    }
                    if (FinancialYearList.Count > 0)
                    {
                        dataAuditSummary = dataAuditSummary.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();
                        dataImpliment = dataImpliment.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();
                        Masterrecord = Masterrecord.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();
                        PRdataAuditSummary = PRdataAuditSummary.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();
                        PRdataImpliment = PRdataImpliment.Where(entry => FinancialYearList.Contains(entry.FinancialYear)).ToList();
                    }
                    BindGraphData(statusListwithComma);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (CustomerId == 0)
            //{
            //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            //}
            //CHKBranchlist.Clear();
            //if (ddlLegalEntity.SelectedValue != "-1")
            //{
            //    ddlSubEntity1.Items.Clear();
            //    ddlSubEntity2.Items.Clear();
            //    ddlSubEntity3.Items.Clear();
            //    ddlFilterLocation.Items.Clear();

            //    CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntity.SelectedValue));                
            //    BindSubEntityDataList(ddlSubEntity1, CHKBranchlist);
            //}
            //else
            //{
            //    if (ddlSubEntity1.Items.Count > 0)
            //    {
            //        ddlSubEntity1.Items.Clear();
            //    }
            //    if (ddlSubEntity2.Items.Count > 0)
            //    {
            //        ddlSubEntity2.Items.Clear();
            //    }
            //    if (ddlSubEntity3.Items.Count > 0)
            //    {
            //        ddlSubEntity3.Items.Clear();
            //    }
            //    if (ddlFilterLocation.Items.Count > 0)
            //    {
            //        ddlFilterLocation.Items.Clear();
            //    }
            //}
            //collapseDivFilters.Attributes.Remove("class");
            //collapseDivFilters.Attributes.Add("class", "panel-collapse in");
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            CHKBranchlist.Clear();
            if (ddlLegalEntityMultiSelect.SelectedValue != "-1")
            {
                CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.SelectedValue));
                BindSubEntityDataList(ddlSubEntity1, CHKBranchlist);
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                }
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                }
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                }
            }
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }

        protected void ddlLegalEntityMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            CHKBranchlist.Clear();

            for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
            {
                if (ddlLegalEntityMultiSelect.Items[i].Selected)
                {
                    CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                }
            }

            if (CHKBranchlist.Count > 0)
            {
                //IsHiddenSubEntity = string.Join(",", CHKBranchlist);
                BindSubEntityDataList(ddlSubEntity1, CHKBranchlist);
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                }
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                }
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                }
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                }
            }
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        //Region
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                CHKBranchlist.Clear();
                for (int i = 0; i < ddlSubEntity1.Items.Count; i++)
                {
                    if (ddlSubEntity1.Items[i].Selected)
                    {
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1.Items[i].Value));
                    }
                }
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                }
            }
            BindSubEntityDataList(ddlSubEntity2, CHKBranchlist);
            BindSchedulingType();
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        //State
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                CHKBranchlist.Clear();
                for (int i = 0; i < ddlSubEntity2.Items.Count; i++)
                {
                    if (ddlSubEntity2.Items[i].Selected)
                    {
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2.Items[i].Value));
                    }
                }
                BindSubEntityDataList(ddlSubEntity3, CHKBranchlist);
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                }
            }
            BindSchedulingType();
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        //Hub
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                CHKBranchlist.Clear();
                for (int i = 0; i < ddlSubEntity3.Items.Count; i++)
                {
                    if (ddlSubEntity3.Items[i].Selected)
                    {
                        CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3.Items[i].Value));
                    }
                }
                BindSubEntityDataList(ddlFilterLocation, CHKBranchlist);
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                }
            }
            BindSchedulingType();

            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        //Loction
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            CHKBranchlist.Clear();
            for (int i = 0; i < ddlFilterLocation.Items.Count; i++)
            {
                if (ddlFilterLocation.Items[i].Selected)
                {
                    CHKBranchlist.Add(Convert.ToInt32(ddlFilterLocation.Items[i].Value));
                }
            }
            BindSchedulingType();
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {           
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        protected void ddlFinancialYearMultiSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> FinancialYearList = new List<string>();
            FinancialYearList.Clear();
            string commaSeparatedFinancialYear = string.Empty;
            for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
            {
                if (ddlFinancialYearMultiSelect.Items[i].Selected)
                {
                    FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                }
            }
            commaSeparatedFinancialYear = string.Join(",", FinancialYearList);
            //IsHiddenFY = string.Join(",", FinancialYearList);
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");

        }
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
                {
                    BindAuditSchedule("S", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                    {
                        if (ddlFilterLocation.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                        }
                    }
                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                }
            }
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }
        public  List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(long customerID, List<long> customerbranchlist)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && customerbranchlist.Contains(row.ID)
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }
        public  List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(long customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }
        public  void LoadSubEntities(long customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }                
        public void BindGraphData(string statusListwithComma)
        {
            try
            {
                #region oldCode
                //if (CustomerId == 0)
                //{
                //    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                //}
                //long UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                //int branchid = -1;
                //int ProcessID = -1;
                //int VerticalID = -1;

                //String FinYear = String.Empty;
                //String Period = String.Empty;

                //List<string> StatusListAudit = new List<string>();
                //for (int i = 0; i < ddlStatusList.Items.Count; i++)
                //{
                //    if (ddlStatusList.Items[i].Selected == true)
                //    {
                //        StatusListAudit.Add(ddlStatusList.Items[i].Value);
                //    }
                //}


                //if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                //{
                //    if (ddlLegalEntity.SelectedValue != "-1")
                //    {
                //        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                //    }
                //}
                //if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                //{
                //    CHKBranchlist.Clear();
                //    for (int i = 0; i < ddlSubEntity1.Items.Count; i++)
                //    {
                //        if (ddlSubEntity1.Items[i].Selected)
                //        {
                //            CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1.Items[i].Value));
                //        }
                //    }
                //}
                //if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                //{
                //    CHKBranchlist.Clear();
                //    for (int i = 0; i < ddlSubEntity2.Items.Count; i++)
                //    {
                //        if (ddlSubEntity2.Items[i].Selected)
                //        {
                //            CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2.Items[i].Value));
                //        }
                //    }
                //}
                //if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                //{
                //    CHKBranchlist.Clear();
                //    for (int i = 0; i < ddlSubEntity3.Items.Count; i++)
                //    {
                //        if (ddlSubEntity3.Items[i].Selected)
                //        {
                //            CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3.Items[i].Value));
                //        }
                //    }
                //}

                //if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                //{
                //    CHKBranchlist.Clear();
                //    for (int i = 0; i < ddlFilterLocation.Items.Count; i++)
                //    {
                //        if (ddlFilterLocation.Items[i].Selected)
                //        {
                //            CHKBranchlist.Add(Convert.ToInt32(ddlFilterLocation.Items[i].Value));
                //        }
                //    }
                //}

                //if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                //{
                //    if (ddlFinancialYear.SelectedValue != "-1")
                //    {
                //        FinYear = ddlFinancialYear.SelectedItem.Text;
                //    }
                //}
                //if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                //{
                //    if (ddlPeriod.SelectedValue != "-1")
                //    {
                //        Period = ddlPeriod.SelectedItem.Text;
                //    }
                //}

                //if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                //{
                //    if (ddlProcess.SelectedValue != "-1")
                //    {
                //        ProcessID = Convert.ToInt32(ddlProcess.SelectedValue);
                //    }
                //}              
                //if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                //{
                //    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                //    if (vid != -1)
                //    {
                //        VerticalID = vid;
                //    }
                //}
                //else
                //{
                //    if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                //    {
                //        if (ddlVertical.SelectedValue != "-1")
                //        {
                //            VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                //        }
                //    }
                //}
                //List<int?> ListofBranch = new List<int?>();
                //if (CHKBranchlist.Count > 0)
                //{
                //    Branchlist.Clear();
                //    GetAllHierarchy(CustomerId, CHKBranchlist);
                //    Session["BranchList"] = null;
                //    if (Branchlist.Count > 0)
                //    {
                //        ListofBranch = Branchlist.Select(x => (int?) x).ToList();
                //        Session["BranchList"] = ListofBranch;
                //    }
                //}
                //else
                //{
                //    Branchlist.Clear();
                //    GetAllHierarchy(CustomerId, branchid);
                //    Session["BranchList"] = null;
                //    if (Branchlist.Count > 0)
                //    {
                //        ListofBranch = Branchlist.Select(x => (int?) x).ToList();
                //        Session["BranchList"] = ListofBranch;
                //    }
                //}
                #endregion

                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                long UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                int branchid = -1;
                int ProcessID = -1;
                int VerticalID = -1;
                bool clearLegalBranchList = false;
                //String FinYear = String.Empty;
                String Period = String.Empty;
                List<string> ProcessIDList = new List<string>();
                List<int> VerticalIDList = new List<int>();
                List<string> FinancialYearList = new List<string>();
                List<string> StatusListAudit = new List<string>();
                for (int i = 0; i < ddlStatusList.Items.Count; i++)
                {
                    if (ddlStatusList.Items[i].Selected == true)
                    {
                        StatusListAudit.Add(ddlStatusList.Items[i].Value);
                    }
                }

                for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                {
                    if (ddlLegalEntityMultiSelect.Items[i].Selected)
                    {
                        clearLegalBranchList = true;
                        CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    //CHKBranchlist.Clear();
                    for (int i = 0; i < ddlSubEntity1.Items.Count; i++)
                    {
                        if (ddlSubEntity1.Items[i].Selected)
                        {
                            CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1.Items[i].Value));
                        }
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    //CHKBranchlist.Clear();
                    for (int i = 0; i < ddlSubEntity2.Items.Count; i++)
                    {
                        if (ddlSubEntity2.Items[i].Selected)
                        {
                            CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2.Items[i].Value));
                        }
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    //CHKBranchlist.Clear();
                    for (int i = 0; i < ddlSubEntity3.Items.Count; i++)
                    {
                        if (ddlSubEntity3.Items[i].Selected)
                        {
                            CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3.Items[i].Value));
                        }
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    // CHKBranchlist.Clear();
                    for (int i = 0; i < ddlFilterLocation.Items.Count; i++)
                    {
                        if (ddlFilterLocation.Items[i].Selected)
                        {
                            CHKBranchlist.Add(Convert.ToInt32(ddlFilterLocation.Items[i].Value));
                        }
                    }
                }

                if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        Period = ddlPeriod.SelectedItem.Text;
                    }
                }

                for (int i = 0; i < ddlProcessMultiSelect.Items.Count; i++)
                {
                    if (ddlProcessMultiSelect.Items[i].Selected == true)
                    {
                        ProcessIDList.Add(Convert.ToString(ddlProcessMultiSelect.Items[i].Text));
                    }
                }

                for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                {
                    if (ddlFinancialYearMultiSelect.Items[i].Selected == true)
                    {
                        FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                    }
                }
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        VerticalID = vid;
                    }
                }
                else
                {
                    for (int i = 0; i < ddlVerticalList.Items.Count; i++)
                    {
                        if (ddlVerticalList.Items[i].Selected == true)
                        {
                            VerticalIDList.Add(Convert.ToInt32(ddlVerticalList.Items[i].Value));
                        }
                    }
                }
                List<long> ListofBranch = new List<long>();
                //if (CHKBranchlist.Count > 0)
                //{
                //    Branchlist.Clear();
                //    GetAllHierarchy(CustomerId, CHKBranchlist);
                //    Session["BranchList"] = null;
                //    if (Branchlist.Count > 0)
                //    {
                //        ListofBranch = Branchlist.Select(x => (long)x).ToList();
                //        Session["BranchList"] = ListofBranch;
                //    }
                //}
                //else
                //{
                //    Branchlist.Clear();
                //    GetAllHierarchy(CustomerId, branchid);
                //    Session["BranchList"] = null;
                //    if (Branchlist.Count > 0)
                //    {
                //        ListofBranch = Branchlist.Select(x => (long)x).ToList();
                //        Session["BranchList"] = ListofBranch;
                //    }
                //}

                ListofBranch = CHKBranchlist;

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    List<SP_AuditCountView_Dashboard_Result> dataAuditSummary = new List<SP_AuditCountView_Dashboard_Result>();
                    entities.Database.CommandTimeout = 300;
                    dataAuditSummary = (from C in entities.SP_AuditCountView_Dashboard(CustomerId)
                                        join EAAMR in entities.EntitiesAssignmentManagementRisks
                                        on C.CustomerBranchID equals EAAMR.BranchID
                                        where C.ProcessId == EAAMR.ProcessId
                                        && C.CustomerID == CustomerId
                                        && EAAMR.ISACTIVE == true
                                        && EAAMR.UserID == Portal.Common.AuthenticationHelper.UserID
                                        select C).ToList();
                    
                    entities.Database.CommandTimeout = 300;
                    var dataObservationStatus = (from C in entities.SP_ObservationStatus_DisplayView_New(CustomerId)
                                                 join EAAMR in entities.EntitiesAssignmentManagementRisks
                                                 on C.CustomerBranchID equals EAAMR.BranchID
                                                 where C.ProcessId == EAAMR.ProcessId
                                                 && C.CustomerID == CustomerId
                                                 && EAAMR.UserID == Portal.Common.AuthenticationHelper.UserID
                                                 && EAAMR.ISACTIVE == true
                                                 select C).ToList();
                  
                    entities.Database.CommandTimeout = 300;
                    
                    var dataAuditPlanned = (from row in entities.Sp_AuditPlannedActualDetailDisplayView(Portal.Common.AuthenticationHelper.UserID, "MA")
                                            where row.CustomerID == CustomerId && FinancialYearList.Contains(row.FinancialYear)
                                            select row).ToList();
                    dataAuditPlanned = dataAuditPlanned.GroupBy(a => a.AuditID).Select(a => a.First()).ToList();

                    entities.Database.CommandTimeout = 300;
                    var dataObservationAging = (from c in entities.SP_ObservationAging_DisplayView(CustomerId)
                                                join EAAMR in entities.EntitiesAssignmentManagementRisks
                                                on c.CustomerBranchID equals EAAMR.BranchID
                                                where c.ProcessId == EAAMR.ProcessId
                                                && c.CustomerID == CustomerId
                                                && EAAMR.UserID == Portal.Common.AuthenticationHelper.UserID
                                                && EAAMR.ISACTIVE == true 
                                                select c).ToList();

                    if (CHKBranchlist.Count > 0)
                    {
                        dataAuditSummary = dataAuditSummary.Where(Entry => CHKBranchlist.Contains(Entry.CustomerBranchID)).ToList();
                        dataObservationStatus = dataObservationStatus.Where(Entry => CHKBranchlist.Contains(Entry.CustomerBranchID)).ToList();
                        dataObservationAging = dataObservationAging.Where(Entry => CHKBranchlist.Contains(Entry.CustomerBranchID)).ToList();
                        dataAuditPlanned = dataAuditPlanned.Where(Entry => CHKBranchlist.Contains(Entry.CustomerBranchId)).ToList();
                    }

                    if (StatusListAudit.Count > 0)
                    {
                        if (StatusListAudit.Count == 1)
                        {
                            List<int> OpenstutusList = new List<int>();
                            OpenstutusList.Add(4); OpenstutusList.Add(5);
                            if (StatusListAudit.Contains("0"))
                            {
                                dataAuditSummary = dataAuditSummary.Where(entry => entry.Timeline < DateTime.Now && (entry.ImplementationAuditstatusID != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                                dataObservationStatus = dataObservationStatus.Where(entry => entry.StatusTimeline < DateTime.Now && (entry.AuditImplementStatus != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                                dataObservationAging = dataObservationAging.Where(entry => entry.StatusTimeline < DateTime.Now && (entry.AuditImplementStatus != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                                dataAuditPlanned = dataAuditPlanned.Where(entry => entry.Timeline < DateTime.Now && (entry.ImplementationAuditStatusID != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                            }
                            else if (StatusListAudit.Contains("1"))
                            {
                                dataAuditSummary = dataAuditSummary.Where(entry => entry.Timeline >= DateTime.Now && (entry.ImplementationAuditstatusID != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                                dataObservationStatus = dataObservationStatus.Where(entry => entry.StatusTimeline >= DateTime.Now && (entry.AuditImplementStatus != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                                dataObservationAging = dataObservationAging.Where(entry => entry.StatusTimeline >= DateTime.Now && (entry.AuditImplementStatus != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                                dataAuditPlanned = dataAuditPlanned.Where(entry => entry.Timeline >= DateTime.Now && (entry.ImplementationAuditStatusID != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                            }
                            else if (StatusListAudit.Contains("2"))
                            {
                                dataAuditSummary = dataAuditSummary.Where(entry => entry.ImplementationAuditstatusID == 3 && OpenstutusList.Contains((int)entry.ImplementationStatus)).ToList();
                                dataObservationStatus = dataObservationStatus.Where(entry => entry.AuditImplementStatus == 3 && OpenstutusList.Contains((int)entry.ImplementationStatus)).ToList();
                                dataObservationAging = dataObservationAging.Where(entry => entry.AuditImplementStatus == 3 && OpenstutusList.Contains((int)entry.ImplementationStatus)).ToList();
                                dataAuditPlanned = dataAuditPlanned.Where(entry => entry.ImplementationAuditStatusID == 3 && OpenstutusList.Contains((int)entry.ImplementationStatus)).ToList();
                            }
                        }
                        else if (StatusListAudit.Count == 2)
                        {
                            List<int> OpenstutusList = new List<int>();
                            OpenstutusList.Add(4); OpenstutusList.Add(5);
                            if (StatusListAudit.Contains("0") && StatusListAudit.Contains("1"))
                            {
                                dataAuditSummary = dataAuditSummary.Where(entry => (entry.ImplementationAuditstatusID != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                                dataObservationStatus = dataObservationStatus.Where(entry => (entry.AuditImplementStatus != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                                dataObservationAging = dataObservationAging.Where(entry => (entry.AuditImplementStatus != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                                dataAuditPlanned = dataAuditPlanned.Where(entry => (entry.ImplementationAuditStatusID != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                            }
                            else if (StatusListAudit.Contains("1") && StatusListAudit.Contains("2"))
                            {
                                dataAuditSummary = dataAuditSummary.Where(entry => entry.Timeline >= DateTime.Now).ToList();
                                dataObservationStatus = dataObservationStatus.Where(entry => entry.StatusTimeline >= DateTime.Now).ToList();
                                dataObservationAging = dataObservationAging.Where(entry => entry.StatusTimeline >= DateTime.Now).ToList();
                                dataAuditPlanned = dataAuditPlanned.Where(entry => entry.Timeline >= DateTime.Now).ToList();
                            }
                            else if (StatusListAudit.Contains("2") && StatusListAudit.Contains("0"))
                            {
                                dataAuditSummary = dataAuditSummary.Where(entry => entry.Timeline < DateTime.Now).ToList();
                                dataObservationStatus = dataObservationStatus.Where(entry => entry.StatusTimeline < DateTime.Now).ToList();
                                dataObservationAging = dataObservationAging.Where(entry => entry.StatusTimeline < DateTime.Now).ToList();
                                dataAuditPlanned = dataAuditPlanned.Where(entry => entry.Timeline < DateTime.Now).ToList();
                            }
                        }
                    }


                    //GetObservationStatusDashboardCount(dataObservationStatus, CustomerId, ListofBranch, FinancialYearList, Period, ProcessID, VerticalID);

                    //GetProcessWiseObservationDashboardCount(dataObservationStatus, CustomerId, ListofBranch, FinancialYearList, Period, ProcessID, VerticalID);

                    //GetAuditTrackerSummary(dataAuditPlanned, CustomerId, ListofBranch, FinancialYearList, Period, VerticalID);

                    //GetAuditObservationAgingSummary(dataObservationAging, CustomerId, ListofBranch, FinancialYearList, Period, ProcessID, VerticalID);

                    //GetAuditStatusSummary(dataAuditSummary, CustomerId, ListofBranch, FinancialYearList, Period, VerticalID);

                    GetObservationStatusDashboardCount(dataObservationStatus, CustomerId, ListofBranch, FinancialYearList, Period, ProcessIDList, VerticalIDList, statusListwithComma);

                    GetProcessWiseObservationDashboardCount(dataObservationStatus, CustomerId, ListofBranch, FinancialYearList, Period, ProcessIDList, VerticalIDList, statusListwithComma);

                    GetAuditTrackerSummary(dataAuditPlanned, CustomerId, ListofBranch, FinancialYearList, Period, VerticalIDList, statusListwithComma);

                    GetAuditObservationAgingSummary(dataObservationAging, CustomerId, ListofBranch, FinancialYearList, Period, ProcessIDList, VerticalIDList, statusListwithComma);

                    GetAuditStatusSummary(dataAuditSummary, CustomerId, ListofBranch, FinancialYearList, Period, VerticalIDList, statusListwithComma);

                }         
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //public void GetObservationStatusDashboardCount(List<ObservationStatus_DisplayView_New> ObservationStatusDisplayViewNewMasterList, long CustomerID, List<int?> BranchList, String FinYear, String Period, int ProcessID, int VerticalID)
        public void GetObservationStatusDashboardCount(List<SP_ObservationStatus_DisplayView_New_Result> ObservationStatusDisplayViewNewMasterList, long CustomerID, List<long> BranchList, List<string> FinYearList, String Period, List<string> ProcessIDlist, List<int> VerticalID, string statusListwithComma)
        {
            try
            {
                //ObservationStatusChart = string.Empty;
                //string ObservationStatusChartSeries = string.Empty;
                //string ObservationStatusChartDrillDown = string.Empty;

                //long highcount = 0;
                //long mediumCount = 0;
                //long lowcount = 0;
                //long totalcount = 0;

                //ObservationStatusChartSeries = "series: [{ name: 'Category', colorByPoint: true, data: [";
                //ObservationStatusChartDrillDown = "drilldown: { series: [ ";

                //using (AuditControlEntities entities = new AuditControlEntities())
                //{
                //    var ObservationList = ObservationSubcategory.GetObservationCategoryAll(CustomerID);

                //    var Records = (from row in ObservationStatusDisplayViewNewMasterList
                //                   where row.CustomerID == CustomerID                            
                //                   select row).ToList();

                //    if (BranchList.Count > 0)
                //        Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                //    if (FinYear != "")
                //        Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                //    else
                //    {
                //        FinYear = GetCurrentFinancialYear(DateTime.Now.Date);
                //        Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                //    }

                //    if (Period != "")
                //        Records = Records.Where(Entry => Entry.ForMonth == Period).ToList();

                //    if (ProcessID != -1)
                //        Records = Records.Where(Entry => Entry.ProcessId == ProcessID).ToList();

                //    if (VerticalID != -1)
                //        Records = Records.Where(Entry => Entry.VerticalID == VerticalID).ToList();

                //    foreach (Mst_ObservationCategory EachObsCat in ObservationList)
                //    {
                //        highcount = (from row in Records
                //                     where row.ObservatioRating == 1
                //                      && row.ObservationCategory == EachObsCat.ID
                //                     select row).Count(); 

                //        mediumCount = (from row in Records
                //                       where row.ObservatioRating == 2
                //                        && row.ObservationCategory == EachObsCat.ID
                //                       select row).Count();

                //        lowcount = (from row in Records
                //                    where row.ObservatioRating == 3
                //                     && row.ObservationCategory == EachObsCat.ID
                //                    select row).Count();

                //        totalcount = highcount + mediumCount + lowcount;

                //        ObservationStatusChartSeries += "{ name: '" + EachObsCat.Name + "', y: " + totalcount + ", drilldown: '" + EachObsCat.Name + "' },";

                //        ObservationStatusChartDrillDown += "{ name: '" + EachObsCat.Name + "', id: '" + EachObsCat.Name + "'," +
                //           " data: [['Major'," + highcount + "],['Moderate', " + mediumCount + "],['Minor'," + lowcount + "]]," +
                //            " events: {" +
                //                " click: function(e){" +
                //                " ShowObservationDetails(e.point.name," + CustomerID + ",-1,'" + FinYear + "'," + EachObsCat.ID + "," + ProcessID + ",'MG')}" +
                //                " }},";
                //    }

                //    ObservationStatusChartSeries = ObservationStatusChartSeries.Trim(',');
                //    ObservationStatusChartDrillDown = ObservationStatusChartDrillDown.Trim(',');
                //    ObservationStatusChartSeries += "] }],";
                //    ObservationStatusChartDrillDown += "] } ";

                //    ObservationStatusChart = ObservationStatusChartSeries + ObservationStatusChartDrillDown;
                //}
                ObservationStatusChart = string.Empty;
                string ObservationStatusChartSeries = string.Empty;
                string ObservationStatusChartDrillDown = string.Empty;

                long highcount = 0;
                long mediumCount = 0;
                long lowcount = 0;
                long totalcount = 0;

                string branchIdCommaSeparatedList = string.Empty;
                List<string> BranchIdList = new List<string>();
                string VerticalIDstring = string.Empty;
                string processIdCommaSeparated = string.Empty;
                for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                {
                    if (ddlLegalEntityMultiSelect.Items[i].Selected)
                    {
                        BranchIdList.Add(ddlLegalEntityMultiSelect.Items[i].Value);
                    }
                }

                if (BranchIdList.Count > 0)
                {
                    branchIdCommaSeparatedList = string.Join(",", BranchIdList);
                }
                else
                {
                    branchIdCommaSeparatedList = "-1";
                }

                List<string> SubEntityOneIdList = new List<string>();
                string subEntityOneIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlSubEntity1.Items.Count; i++)
                {
                    if (ddlSubEntity1.Items[i].Selected)
                    {
                        SubEntityOneIdList.Add(ddlSubEntity1.Items[i].Value);
                    }
                }
                if (SubEntityOneIdList.Count > 0)
                {
                    subEntityOneIdCommaSeparatedList = string.Join(",", SubEntityOneIdList);
                }
                else
                {
                    subEntityOneIdCommaSeparatedList = "-1";
                }

                List<string> SubEntityTwoIdList = new List<string>();
                string SubEntityTwoIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlSubEntity2.Items.Count; i++)
                {
                    if (ddlSubEntity2.Items[i].Selected)
                    {
                        SubEntityTwoIdList.Add(ddlSubEntity2.Items[i].Value);
                    }
                }
                if (SubEntityTwoIdList.Count > 0)
                {
                    SubEntityTwoIdCommaSeparatedList = string.Join(",", SubEntityTwoIdList);
                }
                else
                {
                    SubEntityTwoIdCommaSeparatedList = "-1";
                }
                List<string> SubEntityThreeIdList = new List<string>();
                string SubEntityThreeIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlSubEntity3.Items.Count; i++)
                {
                    if (ddlSubEntity3.Items[i].Selected)
                    {
                        SubEntityThreeIdList.Add(ddlSubEntity3.Items[i].Value);
                    }
                }
                if (SubEntityThreeIdList.Count > 0)
                {
                    SubEntityThreeIdCommaSeparatedList = string.Join(",", SubEntityThreeIdList);
                }
                else
                {
                    SubEntityThreeIdCommaSeparatedList = "-1";
                }

                List<string> ddlFilterLocationIdList = new List<string>();
                string FilterLocationIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlFilterLocation.Items.Count; i++)
                {
                    if (ddlFilterLocation.Items[i].Selected)
                    {
                        ddlFilterLocationIdList.Add(ddlFilterLocation.Items[i].Value);
                    }
                }
                if (ddlFilterLocationIdList.Count > 0)
                {
                    FilterLocationIdCommaSeparatedList = string.Join(",", ddlFilterLocationIdList);
                }
                else
                {
                    FilterLocationIdCommaSeparatedList = "-1";
                }

                ObservationStatusChartSeries = "series: [{ name: 'Category', colorByPoint: true, data: [";
                ObservationStatusChartDrillDown = "drilldown: { series: [ ";

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var ObservationList = ObservationSubcategory.GetObservationCategoryAll(CustomerID);

                    var Records = (from row in ObservationStatusDisplayViewNewMasterList
                                   where row.CustomerID == CustomerID
                                   select row).ToList();

                    if (BranchList.Count > 0)
                        Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                    if (FinYearList.Count > 0)
                    {
                        Records = Records.Where(Entry => FinYearList.Contains(Entry.FinancialYear)).ToList();
                    }
                    else
                    {
                        string FinYear = GetCurrentFinancialYear(DateTime.Now.Date);
                        Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                    }

                    if (Period != "")
                        Records = Records.Where(Entry => Entry.ForMonth == Period).ToList();

                    if (ProcessIDlist.Count > 0)
                    {
                        Records = Records.Where(Entry => ProcessIDlist.Contains(Entry.ProcessName)).ToList();
                        var Processlistname = Records.Select(e => e.ProcessName).Distinct().ToList();
                        processIdCommaSeparated = string.Join(",", Processlistname);
                    }
                    else
                    {
                        var Processlistname = Records.Select(e => e.ProcessName).Distinct().ToList();
                        processIdCommaSeparated = string.Join(",", Processlistname);
                    }

                    if (VerticalID.Count > 0)
                    {
                        Records = Records.Where(Entry => VerticalID.Contains((int)Entry.VerticalID)).ToList();
                        VerticalIDstring = string.Join(",", VerticalID);
                    }

                    string finYear = string.Join(",", FinYearList);

                    foreach (Mst_ObservationCategory EachObsCat in ObservationList)
                    {
                        highcount = (from row in Records
                                     where row.ObservatioRating == 1
                                      && row.ObservationCategory == EachObsCat.ID
                                     select row).Count();

                        mediumCount = (from row in Records
                                       where row.ObservatioRating == 2
                                        && row.ObservationCategory == EachObsCat.ID
                                       select row).Count();

                        lowcount = (from row in Records
                                    where row.ObservatioRating == 3
                                     && row.ObservationCategory == EachObsCat.ID
                                    select row).Count();

                        totalcount = highcount + mediumCount + lowcount;

                        ObservationStatusChartSeries += "{ name: '" + EachObsCat.Name + "', y: " + totalcount + ", drilldown: '" + EachObsCat.Name + "' },";

                        ObservationStatusChartDrillDown += "{ name: '" + EachObsCat.Name + "', id: '" + EachObsCat.Name + "'," +
                           " data: [['Major'," + highcount + "],['Moderate', " + mediumCount + "],['Minor'," + lowcount + "]]," +
                            " events: {" +
                                " click: function(e){" +
                                " ShowObservationDetails(e.point.name,'" + CustomerID + "','" + branchIdCommaSeparatedList + "','" + finYear + "','" + EachObsCat.Name + "','" + processIdCommaSeparated + "','MG','" + subEntityOneIdCommaSeparatedList + "','" + SubEntityTwoIdCommaSeparatedList + "','" + SubEntityThreeIdCommaSeparatedList + "','" + FilterLocationIdCommaSeparatedList + "','" + VerticalIDstring + "','" + statusListwithComma + "')}" +
                                " }},";
                    }

                    ObservationStatusChartSeries = ObservationStatusChartSeries.Trim(',');
                    ObservationStatusChartDrillDown = ObservationStatusChartDrillDown.Trim(',');
                    ObservationStatusChartSeries += "] }],";
                    ObservationStatusChartDrillDown += "] } ";

                    ObservationStatusChart = ObservationStatusChartSeries + ObservationStatusChartDrillDown;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //public void GetProcessWiseObservationDashboardCount(List<ObservationStatus_DisplayView_New> ObservationStatus_DisplayView_New, long CustomerID, List<int?> BranchList, String FinYear, String Period, int ProcessID, int VerticalID)
        public void GetProcessWiseObservationDashboardCount(List<SP_ObservationStatus_DisplayView_New_Result> ObservationStatus_DisplayView_New, long CustomerID, List<long> BranchList, List<string> FinYearList, String Period, List<string> ProcessIDList, List<int> VerticalID, string statusListwithComma)
        {
            try
            {
                //ProcessWiseObservationStatusChart = String.Empty;

                //String ProcesswiseObservationSeries = String.Empty;
                //String ListofProcesses = String.Empty;

                //String HighCountList = String.Empty;
                //String MediumCountList = String.Empty;
                //String LowCountList = String.Empty;

                //long highcount = 0;
                //long mediumCount = 0;
                //long lowcount = 0;
                //long totalcount = 0;
                //long UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                //String newDivName = String.Empty;

                //int CustomerBranchID = -1;

                //using (AuditControlEntities entities = new AuditControlEntities())
                //{
                //    List<Mst_Process> ProcessList = new List<Mst_Process>();

                //    if (BranchList.Count > 0)
                //    {
                //        List<long> ListofBranch = new List<long>();
                //        ListofBranch = Branchlist.Select(x => (long) x).ToList();

                //        ProcessList = ProcessManagement.GetAssingedProcessManagement(CustomerID, ListofBranch, UserID);
                //    }
                //    else
                //        ProcessList = ProcessManagement.GetAssingedProcessManagement(CustomerID, UserID);

                //    if (ProcessID != -1)
                //        ProcessList = ProcessList.Where(Entry => Entry.Id == ProcessID).ToList();

                //    if (ProcessList.Count > 0)
                //    {


                //        int Count = 1;

                //        ProcessList.ForEach(EachProcess =>
                //        {
                //            var Records = (from row in ObservationStatus_DisplayView_New
                //                           where row.CustomerID == CustomerID                                           
                //                           && row.ProcessId == EachProcess.Id
                //                           select row).ToList();

                //            if (BranchList.Count > 0)
                //                Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();


                //            if (FinYear != "")
                //                Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                //            else
                //            {
                //                FinYear = GetCurrentFinancialYear(DateTime.Now.Date);
                //                Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                //            }

                //            if (Period != "")
                //                Records = Records.Where(Entry => Entry.ForMonth == Period).ToList();

                //            if (VerticalID != -1)
                //                Records = Records.Where(Entry => Entry.VerticalID == VerticalID).ToList();

                //            highcount = (from row in Records
                //                         where row.ObservatioRating == 1
                //                         select row).Count();

                //            mediumCount = (from row in Records
                //                           where row.ObservatioRating == 2
                //                           select row).Count();

                //            lowcount = (from row in Records
                //                        where row.ObservatioRating == 3
                //                        select row).Count();

                //            totalcount = highcount + mediumCount + lowcount;

                //            if (totalcount != 0)
                //            {
                //                ListofProcesses += "'" + EachProcess.Name + "',";



                //                System.Web.UI.HtmlControls.HtmlGenericControl newDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                //                newDivName = "DivProcessObs" + Count;
                //                newDiv.ID = newDivName;

                //                String newDivClientID = newDiv.ClientID;

                //                //newDiv.Style["height"] = "300px";
                //                //newDiv.Style["width"] = "25%";

                //                newDiv.Style["height"] = "200px";
                //                newDiv.Style["width"] = "20%";

                //                newDiv.Style["float"] = "left";

                //                DivGraphProcessObsStatus.Controls.Add(newDiv); /*format: '<b>{point.name}</b>: {y}',*/


                //                ProcesswiseObservationSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "', { chart: { plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false, type: 'pie',marginBottom: 30, }," +
                //                "title: {text: '" + EachProcess.Name + "',style: {fontSize: '12px' }},  legend: { itemDistance:0,itemStyle:{fontSize:'10px'}, }, tooltip: { pointFormat: '{series.name}: <b>{point.y}</b>'}," +
                //                "plotOptions: {pie: {size: '100%', showInLegend: true, allowPointSelect: true, cursor: 'pointer', dataLabels: { enabled: true, format: '{y}', distance: 1," +
                //                "style: { color: (Highcharts.theme &&     Highcharts.theme.contrastTextColor) || 'black'} } } }," +
                //                "series: [{ name: '" + EachProcess.Name + "', colorByPoint: true, " +
                //                "data: [{ name: 'Major', color: '#f45b5b', y: " + highcount + ", events:{click: function(e) { ShowObservationDetails(e.point.name," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "',-1," + EachProcess.Id + ",'MG') } } }," +
                //                " { name: 'Moderate', color: '#e4d354', y:" + mediumCount + ", events:{click: function(e) { ShowObservationDetails(e.point.name," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "',-1," + EachProcess.Id + ",'MG') } } }," +
                //                " { name: 'Minor', color: '#90ed7d', y:" + lowcount + ", events:{click: function(e) { ShowObservationDetails(e.point.name," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "',-1," + EachProcess.Id + ",'MG') } } }] }] });";

                //                ProcessWiseObservationStatusChart += ProcesswiseObservationSeries;

                //                Count++;
                //            }
                //        });


                //    }
                //}
                ProcessWiseObservationStatusChart = String.Empty;

                String ProcesswiseObservationSeries = String.Empty;
                String ListofProcesses = String.Empty;

                String HighCountList = String.Empty;
                String MediumCountList = String.Empty;
                String LowCountList = String.Empty;

                long highcount = 0;
                long mediumCount = 0;
                long lowcount = 0;
                long totalcount = 0;
                long UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                String newDivName = String.Empty;

                int CustomerBranchID = -1;

                List<string> processIdList1 = new List<string>();
                string VerticalListString = string.Empty;

                if (ProcessIDList.Count > 0)
                {
                    foreach (string processname in ProcessIDList)
                    {
                        processIdList1.Add(Convert.ToString(processname));
                    }
                }

                List<string> branchIdListStringList = new List<string>();
                string branchIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                {
                    if (ddlLegalEntityMultiSelect.Items[i].Selected)
                    {
                        branchIdListStringList.Add(Convert.ToString(ddlLegalEntityMultiSelect.Items[i].Value));
                    }
                }
                if (branchIdListStringList.Count > 0)
                {
                    branchIdCommaSeparatedList = string.Join(",", branchIdListStringList);
                }

                List<string> SubEntityOneIdList = new List<string>();
                string subEntityOneIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlSubEntity1.Items.Count; i++)
                {
                    if (ddlSubEntity1.Items[i].Selected)
                    {
                        SubEntityOneIdList.Add(ddlSubEntity1.Items[i].Value);
                    }
                }
                if (SubEntityOneIdList.Count > 0)
                {
                    subEntityOneIdCommaSeparatedList = string.Join(",", SubEntityOneIdList);
                }
                else
                {
                    subEntityOneIdCommaSeparatedList = "-1";
                }

                List<string> SubEntityTwoIdList = new List<string>();
                string SubEntityTwoIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlSubEntity2.Items.Count; i++)
                {
                    if (ddlSubEntity2.Items[i].Selected)
                    {
                        SubEntityTwoIdList.Add(ddlSubEntity2.Items[i].Value);
                    }
                }
                if (SubEntityTwoIdList.Count > 0)
                {
                    SubEntityTwoIdCommaSeparatedList = string.Join(",", SubEntityTwoIdList);
                }
                else
                {
                    SubEntityTwoIdCommaSeparatedList = "-1";
                }

                List<string> SubEntityThreeIdList = new List<string>();
                string SubEntityThreeIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlSubEntity3.Items.Count; i++)
                {
                    if (ddlSubEntity3.Items[i].Selected)
                    {
                        SubEntityThreeIdList.Add(ddlSubEntity3.Items[i].Value);
                    }
                }
                if (SubEntityThreeIdList.Count > 0)
                {
                    SubEntityThreeIdCommaSeparatedList = string.Join(",", SubEntityThreeIdList);
                }
                else
                {
                    SubEntityThreeIdCommaSeparatedList = "-1";
                }

                List<string> ddlFilterLocationIdList = new List<string>();
                string FilterLocationIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlFilterLocation.Items.Count; i++)
                {
                    if (ddlFilterLocation.Items[i].Selected)
                    {
                        ddlFilterLocationIdList.Add(ddlFilterLocation.Items[i].Value);
                    }
                }
                if (ddlFilterLocationIdList.Count > 0)
                {
                    FilterLocationIdCommaSeparatedList = string.Join(",", ddlFilterLocationIdList);
                }
                else
                {
                    FilterLocationIdCommaSeparatedList = "-1";
                }

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    List<Mst_Process> ProcessList = new List<Mst_Process>();

                    if (BranchList.Count > 0)
                    {
                        List<long> ListofBranch = new List<long>();
                        ListofBranch = Branchlist.Select(x => (long)x).ToList();

                        ProcessList = ProcessManagement.GetAssingedProcessManagement(CustomerID, ListofBranch, UserID);
                    }
                    else
                        ProcessList = ProcessManagement.GetAssingedProcessManagement(CustomerID, UserID);

                    if (processIdList1.Count > 0)
                        ProcessList = ProcessList.Where(Entry => processIdList1.Contains(Entry.Name)).ToList();

                    var ProcessNameList = ProcessList.Select(Entry => Entry.Name).Distinct().ToList();
                    string finYear = string.Join(",", FinYearList);

                    //if (ProcessID != -1)
                    //    ProcessList = ProcessList.Where(Entry => Entry.Id == ProcessID).ToList();

                    if (ProcessNameList.Count > 0)
                    {
                        int Count = 1;

                        ProcessNameList.ForEach(EachProcess =>
                        {
                            var Records = (from row in ObservationStatus_DisplayView_New
                                           where row.CustomerID == CustomerID
                                           && row.ProcessName == EachProcess
                                           select row).ToList();

                            if (BranchList.Count > 0)
                                Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();


                            if (FinYearList.Count > 0)
                                Records = Records.Where(Entry => FinYearList.Contains(Entry.FinancialYear)).ToList();
                            else
                            {
                                string FinYear = GetCurrentFinancialYear(DateTime.Now.Date);
                                Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                            }

                            if (Period != "")
                                Records = Records.Where(Entry => Entry.ForMonth == Period).ToList();

                            if (VerticalID.Count > 0)
                            {
                                Records = Records.Where(Entry => VerticalID.Contains((int)Entry.VerticalID)).ToList();
                                VerticalListString = string.Join(",", VerticalID);
                            }

                            highcount = (from row in Records
                                         where row.ObservatioRating == 1
                                         select row).Count();

                            mediumCount = (from row in Records
                                           where row.ObservatioRating == 2
                                           select row).Count();

                            lowcount = (from row in Records
                                        where row.ObservatioRating == 3
                                        select row).Count();

                            totalcount = highcount + mediumCount + lowcount;

                            if (totalcount != 0)
                            {
                                ListofProcesses += "'" + EachProcess + "',";
                                string PName = string.Empty;
                                PName = EachProcess;

                                System.Web.UI.HtmlControls.HtmlGenericControl newDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                                newDivName = "DivProcessObs" + Count;
                                newDiv.ID = newDivName;

                                String newDivClientID = newDiv.ClientID;
                                newDiv.Style["height"] = "200px";
                                newDiv.Style["width"] = "20%";

                                newDiv.Style["float"] = "left";

                                DivGraphProcessObsStatus.Controls.Add(newDiv); /*format: '<b>{point.name}</b>: {y}',*/


                                ProcesswiseObservationSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "', { chart: { plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false, type: 'pie',marginBottom: 30, }," +
                                "title: {text: '" + EachProcess + "',style: {fontSize: '12px' }},  legend: { itemDistance:0,itemStyle:{fontSize:'10px'}, }, tooltip: { pointFormat: '{series.name}: <b>{point.y}</b>'}," +
                                "plotOptions: {pie: {size: '100%', showInLegend: true, allowPointSelect: true, cursor: 'pointer', dataLabels: { enabled: true, format: '{y}', distance: 1," +
                                "style: { color: (Highcharts.theme &&     Highcharts.theme.contrastTextColor) || 'black'} } } }," +
                                "series: [{ name: '" + EachProcess + "', colorByPoint: true, " +
                                "data: [{ name: 'Major', color: '#f45b5b', y: " + highcount + ", events:{click: function(e) { ShowObservationDetails(e.point.name,'" + CustomerID + "','" + branchIdCommaSeparatedList + "','" + finYear + "','null','" + PName + "','MG','" + subEntityOneIdCommaSeparatedList + "','" + SubEntityTwoIdCommaSeparatedList + "','" + SubEntityThreeIdCommaSeparatedList + "','" + FilterLocationIdCommaSeparatedList + "','" + VerticalListString + "','" + statusListwithComma + "') } } }," +
                                " { name: 'Moderate', color: '#e4d354', y:" + mediumCount + ", events:{click: function(e) { ShowObservationDetails(e.point.name,'" + CustomerID + "','" + branchIdCommaSeparatedList + "','" + finYear + "','null','" + PName + "','MG','" + subEntityOneIdCommaSeparatedList + "','" + SubEntityTwoIdCommaSeparatedList + "','" + SubEntityThreeIdCommaSeparatedList + "','" + FilterLocationIdCommaSeparatedList + "','" + VerticalListString + "','" + statusListwithComma + "') } } }," +
                                " { name: 'Minor', color: '#90ed7d', y:" + lowcount + ", events:{click: function(e) { ShowObservationDetails(e.point.name,'" + CustomerID + "','" + branchIdCommaSeparatedList + "','" + finYear + "','null','" + PName + "','MG','" + subEntityOneIdCommaSeparatedList + "','" + SubEntityTwoIdCommaSeparatedList + "','" + SubEntityThreeIdCommaSeparatedList + "','" + FilterLocationIdCommaSeparatedList + "','" + VerticalListString + "','" + statusListwithComma + "') } } }] }] });";

                                ProcessWiseObservationStatusChart += ProcesswiseObservationSeries;

                                Count++;
                            }
                        });
                    }
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }


        //public void GetAuditTrackerSummary(List<Sp_AuditPlannedActualDetailDisplayView_Result> AuditPlannedActualDetailDisplayRecord, long CustomerID, List<long> BranchList, List<string> FinYear, String Period, int VerticalID)
        public void GetAuditTrackerSummary(List<Sp_AuditPlannedActualDetailDisplayView_Result> AuditPlannedActualDetailDisplayRecord, long CustomerID, List<long> BranchList, List<string> FinYearList, String Period, List<int> VerticalIDList, string statusListwithComma)
        {
            try
            {

                //int TotalCount = 0;

                //String PartialFromDate = String.Empty;
                //String PartialToDate = String.Empty;
                //String[] FinancialYear = new String[2];

                //DataTable table = new DataTable();
                //table.Columns.Add("Name", typeof(string));

                //if (FinYear != "")
                //{
                //    FinancialYear = FinYear.Split('-');

                //    PartialFromDate = "01/04/" + FinancialYear[0];
                //    PartialToDate = "31/03/" + FinancialYear[1];
                //}
                //else
                //{
                //    FinYear = GetCurrentFinancialYear(DateTime.Now.Date);

                //    if (FinYear != "")
                //    {
                //        FinancialYear = FinYear.Split('-');

                //        PartialFromDate = "01/04/" + FinancialYear[0];
                //        PartialToDate = "31/03/" + FinancialYear[1];
                //    }
                //}

                //DateTime FROMDate = Convert.ToDateTime(GetDate(PartialFromDate));
                //DateTime TODate = Convert.ToDateTime(GetDate(PartialToDate));

                //for (int i = 0; i < 12; i++)
                //{
                //    table.Columns.Add(FROMDate.AddMonths(i).ToString("MMM") + "-" + FROMDate.AddMonths(i).ToString("yy"), typeof(String));
                //}

                //table.Columns.Add("Total", typeof(string));

                //using (AuditControlEntities entities = new AuditControlEntities())
                //{
                //    //Planned Audits
                //    DataRow tableRow = table.NewRow();

                //    tableRow["Name"] = "Planned";

                //    for (int i = 0; i < 12; i++)
                //    {
                //        DateTime MonthStartDate = FROMDate.AddMonths(i);
                //        DateTime MonthEndDate = InternalControlManagementDashboardRisk.GetLastDayOfMonth(MonthStartDate);

                //        var transactionsQuery = (from row in AuditPlannedActualDetailDisplayRecord
                //                                 where MonthStartDate <= row.StartDate
                //                                 && row.StartDate <= MonthEndDate                                                                                       
                //                                 select row).ToList();


                //        if (BranchList.Count > 0)
                //            transactionsQuery = transactionsQuery.Where(Entry => BranchList.Contains(Entry.CustomerBranchId)).ToList();


                //        if (Period != "")
                //            transactionsQuery = transactionsQuery.Where(entry => entry.ForMonth == Period).ToList();


                //        if (VerticalID != -1)
                //            transactionsQuery = transactionsQuery.Where(entry => entry.VerticalID == VerticalID).ToList();

                //        tableRow[FROMDate.AddMonths(i).ToString("MMM") + "-" + FROMDate.AddMonths(i).ToString("yy")] = transactionsQuery.Count;

                //        TotalCount += transactionsQuery.Count;
                //    }

                //    tableRow["Total"] = TotalCount;
                //    table.Rows.Add(tableRow);


                //    //Actual Audits
                //    TotalCount = 0;
                //    DataRow tableActualRow = table.NewRow();

                //    tableActualRow["Name"] = "Actual";

                //    for (int i = 0; i < 12; i++)
                //    {
                //        DateTime MonthStartDate = FROMDate.AddMonths(i);
                //        DateTime MonthEndDate = GetLastDayOfMonth(MonthStartDate);

                //        var transactionsQuery = (from row in AuditPlannedActualDetailDisplayRecord
                //                                 where MonthStartDate <= row.ExpectedStartDate
                //                                  && row.ExpectedStartDate <= MonthEndDate

                //                                 select row).ToList();



                //        if (BranchList.Count > 0)
                //            transactionsQuery = transactionsQuery.Where(Entry => BranchList.Contains(Entry.CustomerBranchId)).ToList();


                //        if (Period != "")
                //            transactionsQuery = transactionsQuery.Where(entry => entry.ForMonth == Period).ToList();

                //        if (VerticalID != -1)
                //            transactionsQuery = transactionsQuery.Where(entry => entry.VerticalID == VerticalID).ToList();

                //        tableActualRow[FROMDate.AddMonths(i).ToString("MMM") + "-" + FROMDate.AddMonths(i).ToString("yy")] = transactionsQuery.Count;

                //        TotalCount += transactionsQuery.Count;
                //    }

                //    tableActualRow["Total"] = TotalCount;
                //    table.Rows.Add(tableActualRow);

                //    grdAuditTrackerSummary.DataSource = table;
                //    grdAuditTrackerSummary.DataBind();
                //}
                int TotalCount = 0;

                String PartialFromDate = String.Empty;
                String PartialToDate = String.Empty;
                String[] FinancialYear = new String[2];

                DataTable table = new DataTable();
                table.Columns.Add("Name", typeof(string));

                foreach (string FinYear in FinYearList)
                {

                    if (FinYear != "")
                    {
                        FinancialYear = FinYear.Split('-');

                        PartialFromDate = "01/04/" + FinancialYear[0];
                        PartialToDate = "31/03/" + FinancialYear[1];
                    }
                    else
                    {

                        string FinYear1 = GetCurrentFinancialYear(DateTime.Now.Date);

                        //FinYear = GetCurrentFinancialYear(DateTime.Now.Date);

                        //if (FinYear != "")
                        //{
                        //    FinancialYear = FinYear.Split('-');

                        //    PartialFromDate = "01/04/" + FinancialYear[0];
                        //    PartialToDate = "31/03/" + FinancialYear[1];
                        //}

                        if (FinYear1 != "")
                        {
                            FinancialYear = FinYear1.Split('-');

                            PartialFromDate = "01/04/" + FinancialYear[0];
                            PartialToDate = "31/03/" + FinancialYear[1];
                        }
                    }
                }

                DateTime FROMDate = Convert.ToDateTime(GetDate(PartialFromDate));
                DateTime TODate = Convert.ToDateTime(GetDate(PartialToDate));

                for (int i = 0; i < 12; i++)
                {
                    table.Columns.Add(FROMDate.AddMonths(i).ToString("MMM") + "-" + FROMDate.AddMonths(i).ToString("yy"), typeof(String));
                }

                table.Columns.Add("Total", typeof(string));

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    //Planned Audits
                    DataRow tableRow = table.NewRow();

                    tableRow["Name"] = "Planned";

                    for (int i = 0; i < 12; i++)
                    {
                        DateTime MonthStartDate = FROMDate.AddMonths(i);
                        DateTime MonthEndDate = InternalControlManagementDashboardRisk.GetLastDayOfMonth(MonthStartDate);

                        var transactionsQuery = (from row in AuditPlannedActualDetailDisplayRecord
                                                 where MonthStartDate <= row.StartDate
                                                 && row.StartDate <= MonthEndDate
                                                 select row).ToList();


                        if (BranchList.Count > 0)
                            transactionsQuery = transactionsQuery.Where(Entry => BranchList.Contains(Entry.CustomerBranchId)).ToList();


                        if (Period != "")
                            transactionsQuery = transactionsQuery.Where(entry => entry.ForMonth == Period).ToList();


                        if (VerticalIDList.Count > 0)
                            transactionsQuery = transactionsQuery.Where(entry => VerticalIDList.Contains((int)entry.VerticalID)).ToList();

                        tableRow[FROMDate.AddMonths(i).ToString("MMM") + "-" + FROMDate.AddMonths(i).ToString("yy")] = transactionsQuery.Count;

                        TotalCount += transactionsQuery.Count;
                    }

                    tableRow["Total"] = TotalCount;
                    table.Rows.Add(tableRow);


                    //Actual Audits
                    TotalCount = 0;
                    DataRow tableActualRow = table.NewRow();

                    tableActualRow["Name"] = "Actual";

                    for (int i = 0; i < 12; i++)
                    {
                        DateTime MonthStartDate = FROMDate.AddMonths(i);
                        DateTime MonthEndDate = GetLastDayOfMonth(MonthStartDate);

                        var transactionsQuery = (from row in AuditPlannedActualDetailDisplayRecord
                                                 where MonthStartDate <= row.ExpectedStartDate
                                                  && row.ExpectedStartDate <= MonthEndDate

                                                 select row).ToList();



                        if (BranchList.Count > 0)
                            transactionsQuery = transactionsQuery.Where(Entry => BranchList.Contains(Entry.CustomerBranchId)).ToList();


                        if (Period != "")
                            transactionsQuery = transactionsQuery.Where(entry => entry.ForMonth == Period).ToList();

                        if (VerticalIDList.Count > 0)
                            transactionsQuery = transactionsQuery.Where(entry => VerticalIDList.Contains((int)entry.VerticalID)).ToList();

                        tableActualRow[FROMDate.AddMonths(i).ToString("MMM") + "-" + FROMDate.AddMonths(i).ToString("yy")] = transactionsQuery.Count;

                        TotalCount += transactionsQuery.Count;
                    }

                    tableActualRow["Total"] = TotalCount;
                    table.Rows.Add(tableActualRow);

                    grdAuditTrackerSummary.DataSource = table;
                    grdAuditTrackerSummary.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        

        protected void grdAuditTrackerSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //if (e.Row.RowType == DataControlRowType.Header)
                //{
                //    e.Row.Cells[0].Text = "";
                //}

                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    int index = e.Row.RowIndex;


                //    int branchid = -1;
                //    int VerticalID = -1;

                //    String FinYear = String.Empty;
                //    String Period = String.Empty;

                //    String PartialFromDate = String.Empty;
                //    String PartialToDate = String.Empty;
                //    String[] FinancialYear = new String[2];

                //    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                //    {
                //        if (ddlLegalEntity.SelectedValue != "-1")
                //        {
                //            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                //        }
                //    }

                //    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                //    {
                //        if (ddlSubEntity1.SelectedValue != "-1")
                //        {
                //            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                //        }
                //    }

                //    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                //    {
                //        if (ddlSubEntity2.SelectedValue != "-1")
                //        {
                //            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                //        }
                //    }

                //    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                //    {
                //        if (ddlSubEntity3.SelectedValue != "-1")
                //        {
                //            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                //        }
                //    }

                //    if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                //    {
                //        if (ddlFilterLocation.SelectedValue != "-1")
                //        {
                //            branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                //        }
                //    }

                //    if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                //    {
                //        int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                //        if (vid != -1)
                //        {
                //            VerticalID = vid;
                //        }
                //    }
                //    else
                //    {
                //        if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                //        {
                //            if (ddlVertical.SelectedValue != "-1")
                //            {
                //                VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                //            }
                //        }
                //    }

                //    if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                //    {
                //        if (ddlFinancialYear.SelectedValue != "-1")
                //        {
                //            FinYear = ddlFinancialYear.SelectedItem.Text;
                //        }
                //    }

                //    if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                //    {
                //        if (ddlPeriod.SelectedValue != "-1")
                //        {
                //            Period = ddlPeriod.SelectedItem.Text;
                //        }
                //    }

                //    if (FinYear != "")
                //    {
                //        FinancialYear = FinYear.Split('-');

                //        PartialFromDate = "01/04/" + FinancialYear[0];
                //        PartialToDate = "31/03/" + FinancialYear[1];
                //    }
                //    else
                //    {
                //        FinYear = GetCurrentFinancialYear(DateTime.Now.Date);

                //        if (FinYear != "")
                //        {
                //            FinancialYear = FinYear.Split('-');

                //            PartialFromDate = "01/04/" + FinancialYear[0];
                //            PartialToDate = "31/03/" + FinancialYear[1];
                //        }
                //    }

                //    if (Period == "")
                //        Period = "Period";

                //    DateTime FROMDate = Convert.ToDateTime(GetDate(PartialFromDate));
                //    DateTime TODate = Convert.ToDateTime(GetDate(PartialToDate));

                //    for (int i = 1; i <= 12; i++)
                //    {
                //        DateTime MonthStartDate = FROMDate.AddMonths(i - 1);
                //        DateTime MonthEndDate = GetLastDayOfMonth(MonthStartDate);

                //        //e.Row.Cells[i].Style.Add("background", lowcolor.Value);
                //        //e.Row.Cells[i].Style.Add("color", "white");
                //        e.Row.Cells[i].Style.Add("font-weight", "500");
                //        e.Row.Cells[i].Style.Add("cursor", "pointer");
                //        e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Center;

                //        if (index == 0)
                //            e.Row.Cells[i].Attributes.Add("onclick", "PlannedActualAuditDetails('Planned'," + branchid + ",'" + Period + "','" + MonthStartDate.Date + "','" + MonthEndDate.Date + "'," + VerticalID + ",'MG')");
                //        else if (index == 1)
                //            e.Row.Cells[i].Attributes.Add("onclick", "PlannedActualAuditDetails('Actual'," + branchid + ",'" + Period + "','" + MonthStartDate.Date + "','" + MonthEndDate.Date + "'," + VerticalID + ",'MG')");
                //    }

                //    if (index == 0)
                //    {
                //        //e.Row.Cells[13].Style.Add("background", lowcolor.Value);
                //        //e.Row.Cells[13].Style.Add("color", "white");
                //        e.Row.Cells[13].Style.Add("cursor", "pointer");
                //        e.Row.Cells[13].HorizontalAlign = HorizontalAlign.Center;
                //        e.Row.Cells[13].Attributes.Add("onclick", "PlannedActualAuditDetails('Planned'," + branchid + ",'" + Period + "','" + FROMDate.Date + "','" + TODate.Date + "'," + VerticalID + ",'MG')");
                //    }
                //    else if (index == 1)
                //    {
                //        //e.Row.Cells[13].Style.Add("background", lowcolor.Value);
                //        //e.Row.Cells[13].Style.Add("color", "white");
                //        e.Row.Cells[13].Style.Add("cursor", "pointer");
                //        e.Row.Cells[13].HorizontalAlign = HorizontalAlign.Center;
                //        e.Row.Cells[13].Attributes.Add("onclick", "PlannedActualAuditDetails('Actual'," + branchid + ",'" + Period + "','" + FROMDate.Date + "','" + TODate.Date + "'," + VerticalID + ",'MG')");
                //    }
                //}
                List<string> FinancialYearList = new List<string>();
                List<int> VerticalIDList = new List<int>();
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[0].Text = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int index = e.Row.RowIndex;
                    string VerticalIDstring = string.Empty;

                    int branchid = -1;
                    int VerticalID = -1;

                    String Period = String.Empty;

                    String PartialFromDate = String.Empty;
                    String PartialToDate = String.Empty;
                    String[] FinancialYear = new String[2];

                    List<string> BranchIdList1 = new List<string>();
                    for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                    {
                        if (ddlLegalEntityMultiSelect.Items[i].Selected)
                        {
                            BranchIdList1.Add(ddlLegalEntityMultiSelect.Items[i].Value);
                        }
                    }

                    string branchIdCommaSeparatedList = string.Empty;
                    if (BranchIdList1.Count > 0)
                    {
                        branchIdCommaSeparatedList = string.Join(",", BranchIdList1);
                    }
                    else
                    {
                        branchIdCommaSeparatedList = "-1";
                    }

                    List<string> SubEntityOneIdList = new List<string>();
                    string subEntityOneIdCommaSeparatedList = string.Empty;
                    for (int i = 0; i < ddlSubEntity1.Items.Count; i++)
                    {
                        if (ddlSubEntity1.Items[i].Selected)
                        {
                            SubEntityOneIdList.Add(ddlSubEntity1.Items[i].Value);
                        }
                    }
                    if (SubEntityOneIdList.Count > 0)
                    {
                        subEntityOneIdCommaSeparatedList = string.Join(",", SubEntityOneIdList);
                    }
                    else
                    {
                        subEntityOneIdCommaSeparatedList = "-1";
                    }

                    List<string> SubEntityTwoIdList = new List<string>();
                    string SubEntityTwoIdCommaSeparatedList = string.Empty;
                    for (int i = 0; i < ddlSubEntity2.Items.Count; i++)
                    {
                        if (ddlSubEntity2.Items[i].Selected)
                        {
                            SubEntityTwoIdList.Add(ddlSubEntity2.Items[i].Value);
                        }
                    }
                    if (SubEntityTwoIdList.Count > 0)
                    {
                        SubEntityTwoIdCommaSeparatedList = string.Join(",", SubEntityTwoIdList);
                    }
                    else
                    {
                        SubEntityTwoIdCommaSeparatedList = "-1";
                    }


                    List<string> SubEntityThreeIdList = new List<string>();
                    string SubEntityThreeIdCommaSeparatedList = string.Empty;
                    for (int i = 0; i < ddlSubEntity3.Items.Count; i++)
                    {
                        if (ddlSubEntity3.Items[i].Selected)
                        {
                            SubEntityThreeIdList.Add(ddlSubEntity3.Items[i].Value);
                        }
                    }
                    if (SubEntityThreeIdList.Count > 0)
                    {
                        SubEntityThreeIdCommaSeparatedList = string.Join(",", SubEntityThreeIdList);
                    }
                    else
                    {
                        SubEntityThreeIdCommaSeparatedList = "-1";
                    }

                    List<string> ddlFilterLocationIdList = new List<string>();
                    string FilterLocationIdCommaSeparatedList = string.Empty;
                    for (int i = 0; i < ddlFilterLocation.Items.Count; i++)
                    {
                        if (ddlFilterLocation.Items[i].Selected)
                        {
                            ddlFilterLocationIdList.Add(ddlFilterLocation.Items[i].Value);
                        }
                    }
                    if (ddlFilterLocationIdList.Count > 0)
                    {
                        FilterLocationIdCommaSeparatedList = string.Join(",", ddlFilterLocationIdList);
                    }
                    else
                    {
                        FilterLocationIdCommaSeparatedList = "-1";
                    }

                    for (int i = 0; i < ddlVerticalList.Items.Count; i++)
                    {
                        if (ddlVerticalList.Items[i].Selected)
                        {
                            VerticalIDList.Add(Convert.ToInt32(ddlVerticalList.Items[i].Value));
                        }
                    }
                    if (VerticalIDList.Count > 0)
                    {
                        VerticalIDstring = string.Join(",", VerticalIDList);
                    }

                    for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                    {
                        if (ddlFinancialYearMultiSelect.Items[i].Selected)
                        {
                            FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                    {
                        if (ddlPeriod.SelectedValue != "-1")
                        {
                            Period = ddlPeriod.SelectedItem.Text;
                        }
                    }

                    if (FinancialYearList.Count > 0)
                    {
                        string FinancialYearCommaSeparatedList = string.Join(",", FinancialYearList);
                        foreach (string FinYear in FinancialYearList)
                        {
                            if (FinYear != "")
                            {
                                FinancialYear = FinYear.Split('-');

                                PartialFromDate = "01/04/" + FinancialYear[0];
                                PartialToDate = "31/03/" + FinancialYear[1];
                            }
                            else
                            {
                                string FinYear1 = GetCurrentFinancialYear(DateTime.Now.Date);

                                if (FinYear1 != "")
                                {
                                    FinancialYear = FinYear1.Split('-');

                                    PartialFromDate = "01/04/" + FinancialYear[0];
                                    PartialToDate = "31/03/" + FinancialYear[1];
                                }
                            }
                        }
                    }

                    if (Period == "")
                        Period = "Period";

                    DateTime FROMDate = Convert.ToDateTime(GetDate(PartialFromDate));
                    DateTime TODate = Convert.ToDateTime(GetDate(PartialToDate));
                    string financialYearListCommanSeparatedList = string.Join(",", FinancialYearList);
                    for (int i = 1; i <= 12; i++)
                    {
                        DateTime MonthStartDate = FROMDate.AddMonths(i - 1);
                        DateTime MonthEndDate = GetLastDayOfMonth(MonthStartDate);

                        //e.Row.Cells[i].Style.Add("background", lowcolor.Value);
                        //e.Row.Cells[i].Style.Add("color", "white");
                        e.Row.Cells[i].Style.Add("font-weight", "500");
                        e.Row.Cells[i].Style.Add("cursor", "pointer");
                        e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Center;

                        //if (index == 0)
                        //    e.Row.Cells[i].Attributes.Add("onclick", "PlannedActualAuditDetails('Planned'," + branchid + ",'" + Period + "','" + MonthStartDate.Date + "','" + MonthEndDate.Date + "'," + VerticalID + ",'MG')");
                        //else if (index == 1)
                        //    e.Row.Cells[i].Attributes.Add("onclick", "PlannedActualAuditDetails('Actual'," + branchid + ",'" + Period + "','" + MonthStartDate.Date + "','" + MonthEndDate.Date + "'," + VerticalID + ",'MG')");

                        if (index == 0)
                            e.Row.Cells[i].Attributes.Add("onclick", "PlannedActualAuditDetails('Planned','" + branchIdCommaSeparatedList + "','" + Period + "','" + MonthStartDate.Date + "','" + MonthEndDate.Date + "','" + VerticalIDstring + "','" + CustomerId + "','" + financialYearListCommanSeparatedList + "','MG','" + subEntityOneIdCommaSeparatedList + "','" + SubEntityTwoIdCommaSeparatedList + "','" + SubEntityThreeIdCommaSeparatedList + "','" + FilterLocationIdCommaSeparatedList + "')");
                        else if (index == 1)
                            e.Row.Cells[i].Attributes.Add("onclick", "PlannedActualAuditDetails('Actual','" + branchIdCommaSeparatedList + "','" + Period + "','" + MonthStartDate.Date + "','" + MonthEndDate.Date + "','" + VerticalIDstring + "','" + CustomerId + "','" + financialYearListCommanSeparatedList + "','MG','" + subEntityOneIdCommaSeparatedList + "','" + SubEntityTwoIdCommaSeparatedList + "','" + SubEntityThreeIdCommaSeparatedList + "','" + FilterLocationIdCommaSeparatedList + "')");
                    }

                    if (index == 0)
                    {
                        //e.Row.Cells[13].Style.Add("background", lowcolor.Value);
                        //e.Row.Cells[13].Style.Add("color", "white");
                        e.Row.Cells[13].Style.Add("cursor", "pointer");
                        e.Row.Cells[13].HorizontalAlign = HorizontalAlign.Center;
                        //e.Row.Cells[13].Attributes.Add("onclick", "PlannedActualAuditDetails('Planned'," + branchid + ",'" + Period + "','" + FROMDate.Date + "','" + TODate.Date + "'," + VerticalID + ",'MG')");
                        e.Row.Cells[13].Attributes.Add("onclick", "PlannedActualAuditDetails('Planned','" + branchIdCommaSeparatedList + "','" + Period + "','" + FROMDate.Date + "','" + TODate.Date + "','" + VerticalIDstring + "','" + CustomerId + "','" + financialYearListCommanSeparatedList + "','MG','" + subEntityOneIdCommaSeparatedList + "','" + SubEntityTwoIdCommaSeparatedList + "','" + SubEntityThreeIdCommaSeparatedList + "','" + FilterLocationIdCommaSeparatedList + "')");
                    }
                    else if (index == 1)
                    {
                        //e.Row.Cells[13].Style.Add("background", lowcolor.Value);
                        //e.Row.Cells[13].Style.Add("color", "white");
                        e.Row.Cells[13].Style.Add("cursor", "pointer");
                        e.Row.Cells[13].HorizontalAlign = HorizontalAlign.Center;
                        e.Row.Cells[13].Attributes.Add("onclick", "PlannedActualAuditDetails('Planned','" + branchIdCommaSeparatedList + "','" + Period + "','" + FROMDate.Date + "','" + TODate.Date + "','" + VerticalIDstring + "','" + CustomerId + "','" + financialYearListCommanSeparatedList + "','MG','" + subEntityOneIdCommaSeparatedList + "','" + SubEntityTwoIdCommaSeparatedList + "','" + SubEntityThreeIdCommaSeparatedList + "','" + FilterLocationIdCommaSeparatedList + "')");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);              
            }
        }
        public static List<Sp_GetAuditTrackerDisplayAuditManagement_Result> GetAuditManagerDashboardAudits(List<long> BranchList, List<string> FYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                List<Sp_GetAuditTrackerDisplayAuditManagement_Result> record = new List<Sp_GetAuditTrackerDisplayAuditManagement_Result>();

                record = (from C in entities.Sp_GetAuditTrackerDisplayAuditManagement(Portal.Common.AuthenticationHelper.UserID)
                          select C).Distinct().ToList();

                if (BranchList.Count > 0)
                    record = record.Where(Entry => BranchList.Contains((int)Entry.CustomerBranchID)).ToList();

                if (FYear.Count > 0)
                {
                    record = record.Where(Entry => FYear.Contains(Entry.FinancialYear)).ToList();
                }
                if (record.Count > 0)
                    record = record.OrderBy(entry => entry.Branch)
                        .ThenBy(entry => entry.VerticalName).ToList();

                return record;
            }
        }

        //public void GetAuditStatusSummary(List<AuditCountView> AuditSummaryCountRecord, int CustomerID, List<int?> BranchList, String FinYear, String Period, int VerticalID)
        public void GetAuditStatusSummary(List<SP_AuditCountView_Dashboard_Result> AuditSummaryCountRecord, int CustomerID, List<long> BranchList, List<string> FinYearList, String Period, List<int> VerticalID, string statusListwithComma)
        {
            //try
            //{
            //    String newDivName = String.Empty;
            //    String newListName = String.Empty;
            //    String newLableName = String.Empty;
            //    string FYear = string.Empty;
            //    int Count = 1;
            //    if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
            //    {
            //        if (ddlFinancialYear.SelectedValue != "-1")
            //        {
            //            FYear = ddlFinancialYear.SelectedItem.Text;
            //        }
            //    }
            //    String[] FinancialYear = new String[2];

            //    if (FinYear == "")
            //        FinYear = GetCurrentFinancialYear(DateTime.Now.Date);

            //    using (AuditControlEntities entities = new AuditControlEntities())
            //    {
            //        var AuditList = GetAuditManagerDashboardAudits(BranchList, FYear);

            //        var MasterRecords = (from row in entities.Sp_InternalAuditInstanceTransactionView(CustomerID, 4)
            //                                 //where row.CustomerID == CustomerID                                 
            //                                 //&& row.RoleID == 4
            //                             select row).ToList();

            //        if (AuditList.Count > 0)
            //        {
            //            AuditList.ForEach(EachAudit =>
            //            {
            //                //Create New Master Div To Contains Div that Contains Label and BulletedList 
            //                System.Web.UI.HtmlControls.HtmlGenericControl newDivMaster = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
            //                newDivName = "DivAuditStatusMaster" + Count;
            //                newDivMaster.ID = newDivName;

            //                newDivMaster.Style["width"] = "100%";
            //                newDivMaster.Style["float"] = "left";

            //                //Create New Div To Contains Label 
            //                System.Web.UI.HtmlControls.HtmlGenericControl newDivLabel = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
            //                newDivName = "DivAuditStatusLabel" + Count;
            //                newDivLabel.ID = newDivName;
            //                newDivLabel.Style["margin-top"] = "25px";
            //                newDivLabel.Style["width"] = "50%";//"35%";
            //                newDivLabel.Style["float"] = "left";

            //                //Create New Label
            //                System.Web.UI.WebControls.Label newLabel = new System.Web.UI.WebControls.Label();
            //                newLableName = "Label" + Count;
            //                newLabel.ID = newLableName;
            //                newLabel.Font.Bold = true;

            //                //Create New Div To Contains BulletedList
            //                System.Web.UI.HtmlControls.HtmlGenericControl newDivBulletList = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
            //                newDivName = "DivAuditStatusBulletedList" + Count;
            //                newDivBulletList.ID = newDivName;
            //                newDivBulletList.Style["width"] = "50%"; //"65%";
            //                newDivBulletList.Style["float"] = "left";

            //                //Create New BulletedList
            //                System.Web.UI.WebControls.BulletedList newBulletedList = new System.Web.UI.WebControls.BulletedList();
            //                newListName = "BulletedList" + Count;
            //                newBulletedList.ID = newListName;
            //                newBulletedList.CssClass = "progtrckr";
            //                newBulletedList.BulletStyle = BulletStyle.Numbered;

            //                var ProgressResult = CheckAuditStatusReviewWorkingManagementDashboard(MasterRecords, EachAudit.FinancialYear, EachAudit.ForMonth, (int)EachAudit.CustomerID, (int)EachAudit.CustomerBranchID, (int)EachAudit.VerticalId, (int)EachAudit.AuditID);
            //                newLabel.Text = EachAudit.Branch + "/" + EachAudit.VerticalName + "/" + EachAudit.FinancialYear + "/" + EachAudit.ForMonth;
            //                AddSteps(items, newBulletedList);
            //                SetProgress(ProgressResult, newBulletedList);
            //                newDivLabel.Controls.Add(newLabel);
            //                newDivBulletList.Controls.Add(newBulletedList);
            //                newDivMaster.Controls.Add(newDivLabel);
            //                newDivMaster.Controls.Add(newDivBulletList);
            //                DivGraphAuditStatus.Controls.Add(newDivMaster);

            //                Count++;
            //            });
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //}
            try
            {
                String newDivName = String.Empty;
                String newListName = String.Empty;
                String newLableName = String.Empty;
                string FYear = string.Empty;
                int Count = 1;
                //if (!string.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                //{
                //    if (ddlFinancialYear.SelectedValue != "-1")
                //    {
                //        FYear = ddlFinancialYear.SelectedItem.Text;
                //    }
                //}
                if (!string.IsNullOrEmpty(ddlFinancialYearMultiSelect.SelectedValue))
                {
                    if (ddlFinancialYearMultiSelect.SelectedValue != "-1")
                    {
                        FYear = ddlFinancialYearMultiSelect.SelectedItem.Text;
                    }
                }
                String[] FinancialYear = new String[2];

                //if (FinYearList.Count > 0)
                //    FinYearList.Add(GetCurrentFinancialYear(DateTime.Now.Date));

                //if (FinYear == "")
                //    FinYear = GetCurrentFinancialYear(DateTime.Now.Date);

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var AuditList = GetAuditManagerDashboardAudits(BranchList, FinYearList);

                    var MasterRecords = (from row in entities.Sp_InternalAuditInstanceTransactionView(CustomerID, 4)
                                             //where row.CustomerID == CustomerID                                 
                                             //&& row.RoleID == 4
                                         select row).ToList();

                    if (FinYearList.Count > 0)
                    {
                        MasterRecords = MasterRecords.Where(entry => FinYearList.Contains(entry.FinancialYear)).ToList();
                    }

                    if (AuditList.Count > 0)
                    {
                        AuditList.ForEach(EachAudit =>
                        {
                            //Create New Master Div To Contains Div that Contains Label and BulletedList 
                            System.Web.UI.HtmlControls.HtmlGenericControl newDivMaster = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                            newDivName = "DivAuditStatusMaster" + Count;
                            newDivMaster.ID = newDivName;

                            newDivMaster.Style["width"] = "100%";
                            newDivMaster.Style["float"] = "left";

                            //Create New Div To Contains Label 
                            System.Web.UI.HtmlControls.HtmlGenericControl newDivLabel = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                            newDivName = "DivAuditStatusLabel" + Count;
                            newDivLabel.ID = newDivName;
                            newDivLabel.Style["margin-top"] = "25px";
                            newDivLabel.Style["width"] = "50%";//"35%";
                            newDivLabel.Style["float"] = "left";

                            //Create New Label
                            System.Web.UI.WebControls.Label newLabel = new System.Web.UI.WebControls.Label();
                            newLableName = "Label" + Count;
                            newLabel.ID = newLableName;
                            newLabel.Font.Bold = true;

                            //Create New Div To Contains BulletedList
                            System.Web.UI.HtmlControls.HtmlGenericControl newDivBulletList = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                            newDivName = "DivAuditStatusBulletedList" + Count;
                            newDivBulletList.ID = newDivName;
                            newDivBulletList.Style["width"] = "50%"; //"65%";
                            newDivBulletList.Style["float"] = "left";

                            //Create New BulletedList
                            System.Web.UI.WebControls.BulletedList newBulletedList = new System.Web.UI.WebControls.BulletedList();
                            newListName = "BulletedList" + Count;
                            newBulletedList.ID = newListName;
                            newBulletedList.CssClass = "progtrckr";
                            newBulletedList.BulletStyle = BulletStyle.Numbered;

                            var ProgressResult = CheckAuditStatusReviewWorkingManagementDashboard(MasterRecords, EachAudit.FinancialYear, EachAudit.ForMonth, (int)EachAudit.CustomerID, (int)EachAudit.CustomerBranchID, (int)EachAudit.VerticalId, (int)EachAudit.AuditID);
                            newLabel.Text = EachAudit.Branch + "/" + EachAudit.VerticalName + "/" + EachAudit.FinancialYear + "/" + EachAudit.ForMonth;
                            AddSteps(items, newBulletedList);
                            SetProgress(ProgressResult, newBulletedList);
                            newDivLabel.Controls.Add(newLabel);
                            newDivBulletList.Controls.Add(newBulletedList);
                            newDivMaster.Controls.Add(newDivLabel);
                            newDivMaster.Controls.Add(newDivBulletList);
                            DivGraphAuditStatus.Controls.Add(newDivMaster);

                            Count++;
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //public static List<int?> GetDistinctAuditsStatusManagementDashboard(List<AuditCountView> AuditSummaryCountRecord, String FinYear, String Period, long Customerid, int CustBranchid, int VerticalID)
        //{
        //    using (AuditControlEntities entities = new AuditControlEntities())
        //    {
        //        List<int?> AuditStatusList = new List<int?>();

        //        var record = (from row in entities.InternalAuditInstanceTransactionViews
        //                      where row.CustomerID == Customerid
        //                      && row.CustomerBranchID == CustBranchid
        //                      && row.VerticalID == VerticalID
        //                      && row.FinancialYear == FinYear
        //                      && row.ForMonth == Period
        //                      select row.AuditStatusID).Distinct().ToList();
                 
        //        return record;
        //    }
        //}        
        public static long CheckAuditStatusReviewWorkingManagementDashboard(List<Sp_InternalAuditInstanceTransactionView_Result> InternalAuditInstanceTransactionViewRecord, String FinYear, String Period, long Customerid, int CustBranchid, int VerticalID, int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                int Result = 0;
                int TotalATBD = 0;

                List<int?> ReviewStatusList = new List<int?> { 3, 4, 5, 6 };

                var Records = (from row in InternalAuditInstanceTransactionViewRecord
                               where row.CustomerID == Customerid
                               && row.CustomerBranchID == CustBranchid
                               && row.VerticalID == VerticalID
                               && row.FinancialYear == FinYear
                               && row.ForMonth == Period
                               && row.AuditID == AuditID
                               && row.RoleID == 4
                               select row).ToList();

                var RecordsClose = (from row in InternalAuditInstanceTransactionViewRecord
                                    where row.CustomerID == Customerid
                                    && row.CustomerBranchID == CustBranchid
                                    && row.VerticalID == VerticalID
                                    && row.FinancialYear == FinYear
                                    && row.ForMonth == Period
                                     && row.AuditID == AuditID
                                    && row.RoleID == 4 && row.AuditStatusID == 3
                                    select row).ToList();

                var Records1 = (from row in InternalAuditInstanceTransactionViewRecord
                                where row.CustomerID == Customerid
                                && row.CustomerBranchID == CustBranchid
                                && row.VerticalID == VerticalID
                                && row.FinancialYear == FinYear
                                && row.ForMonth == Period
                                 && row.AuditID == AuditID
                                && row.RoleID == 4 && ReviewStatusList.Contains(row.AuditStatusID)
                                select row).ToList();

                var auditRecords = (from row in entities.AuditClosureDetails
                                    where row.ID == AuditID
                                    select row).FirstOrDefault();

                if (auditRecords != null)
                {
                    if (auditRecords.Total != null)
                    {
                        TotalATBD = Convert.ToInt32(auditRecords.Total);
                    }
                }

                if (Records.Count > 0)
                {

                    var ReviewedATBD2 = Records.Where(Entry => Entry.AuditStatusID == 2).Count();
                    var ReviewedATBD3 = RecordsClose.Count();
                    var ReviewedATBD4 = Records1.Count();


                    var reviewedPer = ((Convert.ToDecimal(ReviewedATBD4) / Convert.ToDecimal(TotalATBD)) * 100);
                    var closedPer = ((Convert.ToDecimal(ReviewedATBD3) / Convert.ToDecimal(TotalATBD)) * 100);

                    if (closedPer >= 100)
                        Result = 4;
                    else if (reviewedPer >= 75 && reviewedPer <= 100)
                        Result = 2;
                    else if (ReviewedATBD2 > 0) //Submitted Count
                        Result = 1;
                    else
                        Result = 0;
                }

                return Result;
            }
        }


        //public void GetAuditObservationAgingSummary(List<ObservationAging_DisplayView> ObservationAging_DisplayRecord, long CustomerID, List<int?> BranchList, String FinYear, String Period, int ProcessID, int VerticalID)
        public void GetAuditObservationAgingSummary(List<SP_ObservationAging_DisplayView_Result> ObservationAging_DisplayRecord, long CustomerID, List<long> BranchList, List<String> FinYearList, String Period, List<string> ProcessIDListFilter, List<int> VerticalIDList, string statusListwithComma)
        {
            try
            {
                //ObservationProcessWiseAgingChart = String.Empty;

                //String ProcesswiseObservationAgingSeries = String.Empty;
                //String ListofProcesses = String.Empty;

                //String NotDueList = String.Empty;
                //String LessThan45List = String.Empty;
                //String GreaterThan45andLessThan90List = String.Empty;
                //String GreaterThan90List = String.Empty;

                //long NotDuecount = 0;
                //long LessThan45Count = 0;
                //long GreaterThan45andLessThan90count = 0;
                //long GreaterThan90count = 0;

                //String newDivName = String.Empty;
                //int CustomerBranchID = -1;
                //List<int?> ImplementedStatusList = new List<int?> { 2, 4, 5 };

                //using (AuditControlEntities entities = new AuditControlEntities())
                //{
                //    List<Mst_Process> ProcessList = new List<Mst_Process>();

                //    if (BranchList.Count > 0)
                //    {
                //        List<long> ListofBranch = new List<long>();
                //        ListofBranch = Branchlist.Select(x => (long) x).ToList();

                //        ProcessList = ProcessManagement.GetAssingedProcessManagement(CustomerID, ListofBranch, Portal.Common.AuthenticationHelper.UserID);
                //    }
                //    else
                //        ProcessList = ProcessManagement.GetAssingedProcessManagement(CustomerID, Portal.Common.AuthenticationHelper.UserID);

                //    if (ProcessID != -1)
                //        ProcessList = ProcessList.Where(Entry => Entry.Id == ProcessID).ToList();

                //    if (ProcessList.Count > 0)
                //    {
                //        int Count = 1;

                //        ProcessList.ForEach(EachProcess =>
                //        {
                //            var Records = (from row in ObservationAging_DisplayRecord
                //                           where row.CustomerID == CustomerID                                       
                //                           && row.ProcessId == EachProcess.Id
                //                           select row).ToList();

                //            if (BranchList.Count > 0)
                //                Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                //            if (FinYear != "")
                //                Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                //            else
                //            {
                //                FinYear = GetCurrentFinancialYear(DateTime.Now.Date);
                //                Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                //            }

                //            if (Period != "")
                //                Records = Records.Where(Entry => Entry.ForMonth == Period).ToList();

                //            if (VerticalID != -1)
                //                Records = Records.Where(Entry => Entry.VerticalID == VerticalID).ToList();

                //            if (Records.Count > 0)
                //            {


                //                NotDuecount = Records.Where(Entry => Entry.TimeLine >= DateTime.Now.Date && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).Count();

                //                LessThan45Count = Records.Where(Entry => Entry.TimeLine >= DateTime.Now.AddDays(-45) && Entry.TimeLine < DateTime.Now.Date && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).Count();

                //                GreaterThan45andLessThan90count = Records.Where(Entry => Entry.TimeLine >= DateTime.Now.AddDays(-90) && Entry.TimeLine < DateTime.Now.AddDays(-45) && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).Count();

                //                GreaterThan90count = Records.Where(Entry => Entry.TimeLine <= DateTime.Now.AddDays(-90) && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).Count();


                //                ListofProcesses += "'" + EachProcess.Name + "',";

                //                System.Web.UI.HtmlControls.HtmlGenericControl newDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                //                newDivName = "DivObsAging" + Count;
                //                newDiv.ID = newDivName;

                //                String newDivClientID = newDiv.ClientID;

                //                //newDiv.Style["height"] = "250px";
                //                //newDiv.Style["width"] = "25%"; comment by rahul on 5 may 2017 for size reduce
                //                newDiv.Style["height"] = "200px";
                //                newDiv.Style["width"] = "20%";
                //                newDiv.Style["float"] = "left";

                //                DivObsAgingProcess.Controls.Add(newDiv); /*format: '<b>{point.name}</b>: {y}',*/

                //                ProcesswiseObservationAgingSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "', { chart: { plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false, type: 'pie',marginBottom: 30, }," +
                //                    "title: {text: '" + EachProcess.Name + "',style: {fontSize: '12px' }}, legend: { itemDistance:0,itemStyle:{fontSize:'10px'},}, tooltip: { pointFormat: '{series.name}: <b>{point.y}</b>'}," +
                //                    "plotOptions: {pie: {size: '165%', showInLegend: true, allowPointSelect: true, cursor: 'pointer', dataLabels: { enabled: true, format: '{y}', distance: -25," +
                //                    "style: { color: (Highcharts.theme &&     Highcharts.theme.contrastTextColor) || 'black'} }, startAngle: -90, endAngle: 90,center: ['50%', '75%'] } }," +
                //                    "series: [{ type: 'pie', name: '" + EachProcess.Name + "', innerSize: '50%', colorByPoint: true, " +
                //                    "data: [{ name: 'Not Due', 	color: '#7cb5ec', y: " + NotDuecount + ", events:{click: function(e) { ShowOpenObservationDetails('NotDue'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + Period + "'," + EachProcess.Id + "," + VerticalID + ",'MG') } } }," +
                //                    " { name: '0-45 Days', color: '#90ed7d', y:" + LessThan45Count + ", events:{click: function(e) { ShowOpenObservationDetails('LessThan45'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + Period + "'," + EachProcess.Id + "," + VerticalID + ",'MG') } } }," +
                //                    " { name: '46-90 Days', color: '#e4d354', y: " + GreaterThan45andLessThan90count + ", events: { click: function(e) { ShowOpenObservationDetails('GreaterThan45'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + Period + "'," + EachProcess.Id + "," + VerticalID + ",'MG') } } }," +
                //                    " { name: '>90 Days', color: '#f45b5b', y:" + GreaterThan90count + ", events:{click: function(e) { ShowOpenObservationDetails('GreaterThan90'," + CustomerID + "," + CustomerBranchID + ",'" + FinYear + "','" + Period + "'," + EachProcess.Id + "," + VerticalID + ",'MG') } } }] }] });";

                //                ObservationProcessWiseAgingChart += ProcesswiseObservationAgingSeries;

                //                Count++;
                //            }
                //        });
                //    }
                //}
                List<string> branchIdListStringList = new List<string>();
                string branchIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                {
                    if (ddlLegalEntityMultiSelect.Items[i].Selected)
                    {
                        branchIdListStringList.Add(Convert.ToString(ddlLegalEntityMultiSelect.Items[i].Value));
                    }
                }
                if (branchIdListStringList.Count > 0)
                {
                    branchIdCommaSeparatedList = string.Join(",", branchIdListStringList);
                }

                List<string> SubEntityOneIdList = new List<string>();
                string subEntityOneIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlSubEntity1.Items.Count; i++)
                {
                    if (ddlSubEntity1.Items[i].Selected)
                    {
                        SubEntityOneIdList.Add(ddlSubEntity1.Items[i].Value);
                    }
                }
                if (SubEntityOneIdList.Count > 0)
                {
                    subEntityOneIdCommaSeparatedList = string.Join(",", SubEntityOneIdList);
                }
                else
                {
                    subEntityOneIdCommaSeparatedList = "-1";
                }

                List<string> SubEntityTwoIdList = new List<string>();
                string SubEntityTwoIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlSubEntity2.Items.Count; i++)
                {
                    if (ddlSubEntity2.Items[i].Selected)
                    {
                        SubEntityTwoIdList.Add(ddlSubEntity2.Items[i].Value);
                    }
                }
                if (SubEntityTwoIdList.Count > 0)
                {
                    SubEntityTwoIdCommaSeparatedList = string.Join(",", SubEntityTwoIdList);
                }
                else
                {
                    SubEntityTwoIdCommaSeparatedList = "-1";
                }


                List<string> SubEntityThreeIdList = new List<string>();
                string SubEntityThreeIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlSubEntity3.Items.Count; i++)
                {
                    if (ddlSubEntity3.Items[i].Selected)
                    {
                        SubEntityThreeIdList.Add(ddlSubEntity3.Items[i].Value);
                    }
                }
                if (SubEntityThreeIdList.Count > 0)
                {
                    SubEntityThreeIdCommaSeparatedList = string.Join(",", SubEntityThreeIdList);
                }
                else
                {
                    SubEntityThreeIdCommaSeparatedList = "-1";
                }

                List<string> ddlFilterLocationIdList = new List<string>();
                string FilterLocationIdCommaSeparatedList = string.Empty;
                for (int i = 0; i < ddlFilterLocation.Items.Count; i++)
                {
                    if (ddlFilterLocation.Items[i].Selected)
                    {
                        ddlFilterLocationIdList.Add(ddlFilterLocation.Items[i].Value);
                    }
                }
                if (ddlFilterLocationIdList.Count > 0)
                {
                    FilterLocationIdCommaSeparatedList = string.Join(",", ddlFilterLocationIdList);
                }
                else
                {
                    FilterLocationIdCommaSeparatedList = "-1";
                }
                ObservationProcessWiseAgingChart = String.Empty;

                String ProcesswiseObservationAgingSeries = String.Empty;
                String ListofProcesses = String.Empty;



                // String ProcesswiseObservationAgingSeries = String.Empty;
                //String ListofProcesses = String.Empty;

                String NotDueList = String.Empty;
                String LessThan45List = String.Empty;
                String GreaterThan45andLessThan90List = String.Empty;
                String GreaterThan90List = String.Empty;

                long NotDuecount = 0;
                long LessThan45Count = 0;
                long GreaterThan45andLessThan90count = 0;
                long GreaterThan90count = 0;

                String newDivName = String.Empty;
                int CustomerBranchID = -1;
                List<int?> ImplementedStatusList = new List<int?> { 2, 4, 5 };

                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    List<Mst_Process> ProcessList = new List<Mst_Process>();

                    if (BranchList.Count > 0)
                    {
                        List<long> ListofBranch = new List<long>();
                        ListofBranch = Branchlist.Select(x => (long)x).ToList();

                        ProcessList = ProcessManagement.GetAssingedProcessManagement(CustomerID, ListofBranch, Portal.Common.AuthenticationHelper.UserID);
                    }
                    else
                        ProcessList = ProcessManagement.GetAssingedProcessManagement(CustomerID, Portal.Common.AuthenticationHelper.UserID);

                    if (ProcessIDListFilter.Count > 0)
                    {
                        ProcessList = ProcessList.Where(Entry => ProcessIDListFilter.Contains(Entry.Name)).ToList();
                    }

                    var ProcessNameList = ProcessList.Select(Entry => Entry.Name).Distinct().ToList();
                    string customerid = string.Join(",", CustomerID);
                    string finYear = string.Join(",", FinYearList);

                    if (ProcessList.Count > 0)
                    {
                        int Count = 1;
                        string VerticalIDListComma = string.Empty;
                        string FinYear = string.Empty;

                        ProcessList.ForEach(EachProcess =>
                        {
                            var Records = (from row in ObservationAging_DisplayRecord
                                           where row.CustomerID == CustomerID
                                           && row.ProcessId == EachProcess.Id
                                           select row).ToList();

                            if (BranchList.Count > 0)
                                Records = Records.Where(Entry => BranchList.Contains(Entry.CustomerBranchID)).ToList();

                            if (FinYearList.Count > 0)
                            {
                                Records = Records.Where(Entry => FinYearList.Contains(Entry.FinancialYear)).ToList();
                                finYear = string.Join(",", FinYearList);
                            }
                            else
                            {
                                FinYear = GetCurrentFinancialYear(DateTime.Now.Date);
                                Records = Records.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                            }

                            if (Period != "")
                                Records = Records.Where(Entry => Entry.ForMonth == Period).ToList();

                            if (VerticalIDList.Count > 0)
                            {
                                Records = Records.Where(Entry => VerticalIDList.Contains((int)Entry.VerticalID)).ToList();
                                VerticalIDListComma = string.Join(",", VerticalIDList);
                            }

                            if (Records.Count > 0)
                            {
                                NotDuecount = Records.Where(Entry => Entry.TimeLine >= DateTime.Now.Date && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).Count();

                                LessThan45Count = Records.Where(Entry => Entry.TimeLine >= DateTime.Now.AddDays(-45) && Entry.TimeLine < DateTime.Now.Date && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).Count();

                                GreaterThan45andLessThan90count = Records.Where(Entry => Entry.TimeLine >= DateTime.Now.AddDays(-90) && Entry.TimeLine < DateTime.Now.AddDays(-45) && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).Count();

                                GreaterThan90count = Records.Where(Entry => Entry.TimeLine <= DateTime.Now.AddDays(-90) && (!ImplementedStatusList.Contains(Entry.ImplementationStatus))).Count();

                                ListofProcesses += "'" + EachProcess.Name + "',";

                                System.Web.UI.HtmlControls.HtmlGenericControl newDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                                newDivName = "DivObsAging" + Count;
                                newDiv.ID = newDivName;

                                String newDivClientID = newDiv.ClientID;

                                //newDiv.Style["height"] = "250px";
                                //newDiv.Style["width"] = "25%"; comment by rahul on 5 may 2017 for size reduce
                                newDiv.Style["height"] = "200px";
                                newDiv.Style["width"] = "20%";
                                newDiv.Style["float"] = "left";
                                string ProcessListwithComma = EachProcess.Name;
                                DivObsAgingProcess.Controls.Add(newDiv); /*format: '<b>{point.name}</b>: {y}',*/

                                ProcesswiseObservationAgingSeries = "var container_" + newDivName + "=Highcharts.chart('ContentPlaceHolder1_" + newDivName + "', { chart: { plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false, type: 'pie',marginBottom: 30, }," +
                                    "title: {text: '" + EachProcess.Name + "',style: {fontSize: '12px' }}, legend: { itemDistance:0,itemStyle:{fontSize:'10px'},}, tooltip: { pointFormat: '{series.name}: <b>{point.y}</b>'}," +
                                    "plotOptions: {pie: {size: '165%', showInLegend: true, allowPointSelect: true, cursor: 'pointer', dataLabels: { enabled: true, format: '{y}', distance: -25," +
                                    "style: { color: (Highcharts.theme &&     Highcharts.theme.contrastTextColor) || 'black'} }, startAngle: -90, endAngle: 90,center: ['50%', '75%'] } }," +
                                    "series: [{ type: 'pie', name: '" + EachProcess.Name + "', innerSize: '50%', colorByPoint: true, " +
                                    "data: [{ name: 'Not Due', 	color: '#7cb5ec', y: " + NotDuecount + ", events:{click: function(e) { ShowOpenObservationDetails('NotDue','" + CustomerID + "','" + branchIdCommaSeparatedList + "','" + finYear + "','" + Period + "','" + ProcessListwithComma + "','" + VerticalIDListComma + "','MG','" + subEntityOneIdCommaSeparatedList + "','" + SubEntityTwoIdCommaSeparatedList + "','" + SubEntityThreeIdCommaSeparatedList + "','" + FilterLocationIdCommaSeparatedList + "','" + statusListwithComma + "') } } }," +
                                    " { name: '0-45 Days', color: '#90ed7d', y:" + LessThan45Count + ", events:{click: function(e) { ShowOpenObservationDetails('LessThan45','" + CustomerID + "','" + branchIdCommaSeparatedList + "','" + finYear + "','" + Period + "','" + ProcessListwithComma + "','" + VerticalIDListComma + "','MG','" + subEntityOneIdCommaSeparatedList + "','" + SubEntityTwoIdCommaSeparatedList + "','" + SubEntityThreeIdCommaSeparatedList + "','" + FilterLocationIdCommaSeparatedList + "','" + statusListwithComma + "') } } }," +
                                    " { name: '46-90 Days', color: '#e4d354', y: " + GreaterThan45andLessThan90count + ", events: { click: function(e) { ShowOpenObservationDetails('GreaterThan45','" + CustomerID + "','" + branchIdCommaSeparatedList + "','" + finYear + "','" + Period + "','" + ProcessListwithComma + "','" + VerticalIDListComma + "','MG','" + subEntityOneIdCommaSeparatedList + "','" + SubEntityTwoIdCommaSeparatedList + "','" + SubEntityThreeIdCommaSeparatedList + "','" + FilterLocationIdCommaSeparatedList + "','" + statusListwithComma + "') } } }," +
                                    " { name: '>90 Days', color: '#f45b5b', y:" + GreaterThan90count + ", events:{click: function(e) { ShowOpenObservationDetails('GreaterThan90','" + CustomerID + "','" + branchIdCommaSeparatedList + "','" + finYear + "','" + Period + "','" + ProcessListwithComma + "','" + VerticalIDListComma + "','MG','" + subEntityOneIdCommaSeparatedList + "','" + SubEntityTwoIdCommaSeparatedList + "','" + SubEntityThreeIdCommaSeparatedList + "','" + FilterLocationIdCommaSeparatedList + "','" + statusListwithComma + "') } } }] }] });";

                                ObservationProcessWiseAgingChart += ProcesswiseObservationAgingSeries;
                                Count++;
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }

        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if(ForDate!=null)
            {
                if (ForDate.Month <= 3)
                    FinYear=(ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear=(ForDate.Year) + "-" + (ForDate.Year+1);
            }

            return FinYear;                 
        }

        public DateTime? GetDate(string date)
        {
            if (date != null && date != "")
            {
                string date1 = "";

                if (date.Contains("/"))
                {
                    date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" +date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains("-"))
                {
                    date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" +date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
                }
                else if (date.Trim().Contains(" "))
                {
                    date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" +date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
                }

                return Convert.ToDateTime(date1);
            }
            else
            {
                return null;
            }
        }
        public DateTime GetLastDayOfMonth(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, DateTime.DaysInMonth(dateTime.Year, dateTime.Month));
        }      
        protected void AddSteps(string[] items, BulletedList ListName)
        {
            ListName.Items.Clear();
            ListName.Attributes["data-progtrckr-steps"] = items.Length.ToString();

            for (int i = 0; i < items.Length; i++)
            {
                ListName.Items.Add(new ListItem(items[i]));
            }
        }
        protected void SetProgress(long current, BulletedList ListName)
        {
            for (int i = 0; i <ListName.Items.Count; i++)
            {
                ListName.Items[i].Attributes["class"] =
                    (i < current) ? "progtrckr-done" : (i == current) ? "progtrckr-current" : "progtrckr-todo";  
                
                if(i==3 && i>current)
                    ListName.Items[i].Attributes["class"] = "progtrckr-todo-closed";
            }
            if (current == 4)
                ListName.Items[Convert.ToInt32(current) - 1].Attributes["class"] = "progtrckr-closed";
        }

        protected void ddlStatusList_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            collapseDivFilters.Attributes.Remove("class");
            collapseDivFilters.Attributes.Add("class", "panel-collapse in");
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    String FileName = String.Empty;
                    FileName = "Observation Report";
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                    DataTable ExcelData = null;
                    #region Filter

                    if (CustomerId == 0)
                    {
                        CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                    }
                    int CustBranchID = -1;
                    int CatID = -1;
                    List<long> ProcessIDList = new List<long>();
                    List<long?> SubProcessIDList = new List<long?>();
                    int UserID = -1;
                    bool IsAuditManager = false;
                    bool IsDepartmentHead = false;
                    bool IsManagement = true;
                    List<String> FinancialYearList = new List<string>();
                    List<String> PeriodList = new List<String>();
                    String Type = String.Empty;
                    string ObjCatName = string.Empty;
                    bool clearLegalBranchList = false;
                    bool clearSubEntity1List = false;
                    bool clearSubEntity2List = false;
                    bool clearSubEntity3List = false;
                    bool clearFilterLocation = false;
                    List<int> VerticalIDList = new List<int>();
                    List<long> CHKBranchlist = new List<long>();
                    CHKBranchlist.Clear();
                    for (int i = 0; i < ddlLegalEntityMultiSelect.Items.Count; i++)
                    {
                        if (ddlLegalEntityMultiSelect.Items[i].Selected)
                        {
                            clearLegalBranchList = true;
                            CHKBranchlist.Add(Convert.ToInt32(ddlLegalEntityMultiSelect.Items[i].Value));
                        }
                    }

                    for (int i = 0; i < ddlSubEntity1.Items.Count; i++)
                    {
                        if (ddlSubEntity1.Items[i].Selected)
                        {
                            if (clearLegalBranchList && CHKBranchlist.Count > 0)
                            {
                                //CHKBranchlist.Clear();
                                clearLegalBranchList = false;
                            }
                            clearSubEntity1List = true;
                            CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity1.Items[i].Value));
                        }
                    }

                    for (int i = 0; i < ddlSubEntity2.Items.Count; i++)
                    {
                        if (ddlSubEntity2.Items[i].Selected)
                        {
                            if (clearSubEntity1List && CHKBranchlist.Count > 0)
                            {
                                //CHKBranchlist.Clear();
                                clearSubEntity1List = false;
                            }
                            clearSubEntity2List = true;
                            CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity2.Items[i].Value));
                        }
                    }

                    for (int i = 0; i < ddlSubEntity3.Items.Count; i++)
                    {
                        if (ddlSubEntity3.Items[i].Selected)
                        {
                            if (clearSubEntity2List && CHKBranchlist.Count > 0)
                            {
                                //CHKBranchlist.Clear();
                                clearSubEntity2List = false;
                            }
                            clearSubEntity3List = true;
                            CHKBranchlist.Add(Convert.ToInt32(ddlSubEntity3.Items[i].Value));
                        }
                    }
                    for (int i = 0; i < ddlFilterLocation.Items.Count; i++)
                    {
                        if (ddlFilterLocation.Items[i].Selected)
                        {
                            if (clearSubEntity3List && CHKBranchlist.Count > 0)
                            {
                                //CHKBranchlist.Clear();
                                clearSubEntity3List = false;
                            }
                            clearFilterLocation = true;
                            CHKBranchlist.Add(Convert.ToInt32(ddlFilterLocation.Items[i].Value));
                        }
                    }
                    for (int i = 0; i < ddlFinancialYearMultiSelect.Items.Count; i++)
                    {
                        if (ddlFinancialYearMultiSelect.Items[i].Selected)
                        {
                            FinancialYearList.Add(ddlFinancialYearMultiSelect.Items[i].Text);
                        }
                    }
                    for (int i = 0; i < ddlProcessMultiSelect.Items.Count; i++)
                    {
                        if (ddlProcessMultiSelect.Items[i].Selected)
                        {
                            ProcessIDList.Add(Convert.ToInt32(ddlProcessMultiSelect.Items[i].Value));
                        }
                    }
                    for (int i = 0; i < ddlVerticalList.Items.Count; i++)
                    {
                        if (ddlVerticalList.Items[i].Selected)
                        {
                            VerticalIDList.Add(Convert.ToInt32(ddlVerticalList.Items[i].Value));
                        }
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["Type"]))
                    {
                        Type = Request.QueryString["Type"];
                    }
                    UserID = Common.AuthenticationHelper.UserID;
                    List<long> ListofBranch = new List<long>();
                    if (CustBranchID == -1)
                    {
                        if (Session["BranchList"] != null)
                        {
                            ListofBranch = (List<long>)(Session["BranchList"]);
                        }
                    }
                    else
                    {
                        Branchlist.Clear();
                        GetAllHierarchy(CustomerId, CustBranchID);
                        if (Branchlist.Count > 0)
                        {
                            ListofBranch = Branchlist.Select(x => (long)x).ToList();
                        }
                    }
                    List<int> ObsRating = new List<int>();
                    if (Type.Trim().Equals("Major"))
                        ObsRating.Add(1);
                    else if (Type.Trim().Equals("Moderate"))
                        ObsRating.Add(2);
                    else if (Type.Trim().Equals("Minor"))
                        ObsRating.Add(3);
                    else
                    {
                        ObsRating.Add(1);
                        ObsRating.Add(2);
                        ObsRating.Add(3);
                    }
                    List<string> StatusListAudit = new List<string>();
                    for (int i = 0; i < ddlStatusList.Items.Count; i++)
                    {
                        if (ddlStatusList.Items[i].Selected == true)
                        {
                            StatusListAudit.Add(ddlStatusList.Items[i].Value);
                        }
                    }
                    var detailView = InternalControlManagementDashboardRisk.GetObservationDetailView(CustomerId, CHKBranchlist, FinancialYearList, PeriodList, ObsRating, ObjCatName, ProcessIDList, UserID, IsAuditManager, IsManagement, IsDepartmentHead, SubProcessIDList, VerticalIDList);
                    if (StatusListAudit.Count > 0)
                    {
                        if (StatusListAudit.Count == 1)
                        {
                            List<int> OpenstutusList = new List<int>();
                            OpenstutusList.Add(4); OpenstutusList.Add(5);
                            if (StatusListAudit.Contains("0"))
                            {
                                detailView = detailView.Where(entry => entry.StatusTimeline < DateTime.Now && (entry.AuditImplementStatus != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                            }
                            else if (StatusListAudit.Contains("1"))
                            {
                                detailView = detailView.Where(entry => entry.StatusTimeline >= DateTime.Now && (entry.AuditImplementStatus != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                            }
                            else if (StatusListAudit.Contains("2"))
                            {
                                detailView = detailView.Where(entry => entry.AuditImplementStatus == 3 && OpenstutusList.Contains((int)entry.ImplementationStatus)).ToList();
                            }
                        }
                        else if (StatusListAudit.Count == 2)
                        {
                            List<int> OpenstutusList = new List<int>();
                            OpenstutusList.Add(4); OpenstutusList.Add(5);
                            if (StatusListAudit.Contains("0") && StatusListAudit.Contains("1"))
                            {
                                detailView = detailView.Where(entry => (entry.AuditImplementStatus != 3 && !OpenstutusList.Contains((int)entry.ImplementationStatus))).ToList();
                            }
                            else if (StatusListAudit.Contains("1") && StatusListAudit.Contains("2"))
                            {
                                detailView = detailView.Where(entry => entry.StatusTimeline >= DateTime.Now).ToList();
                            }
                            else if (StatusListAudit.Contains("2") && StatusListAudit.Contains("0"))
                            {
                                detailView = detailView.Where(entry => entry.StatusTimeline < DateTime.Now).ToList();
                            }
                        }
                    }
                    Session["ExportData"] = detailView;
                    #endregion

                    DataView view = new System.Data.DataView(detailView.ToDataTable());
                    ExcelData = view.ToTable("Selected", false, "Branch", "FinancialYear", "ForMonth", "ProcessName", "SubProcessName", "ActivityDescription", "Observation", "ManagementResponse", "Recomendation", "TimeLine", "ObservationCategoryName", "ObservatioRating", "StatusTimeline", "AuditImplementStatus", "ImplementationStatus", "PersonResponsibleName", "PersonResponsibleEmail", "OwnerName", "OwnerEmail", "Age", "AgeBracket");
                    ExcelData.Columns.Add("Rating");
                    ExcelData.Columns.Add("Status");
                    var customer = UserManagementRisk.GetCustomerName(Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID));

                    exWorkSheet.Cells["A1"].Value = "Report Generated On:";
                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;

                    exWorkSheet.Cells["B1"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;

                    exWorkSheet.Cells["A2"].Value = customer;
                    exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A2"].Style.Font.Size = 12;

                    foreach (DataRow item in ExcelData.Rows)
                    {
                        DateTime StatusTimeline = new DateTime();
                        int AuditImplementStatus = 0;
                        int ImplementationStatus = 0;
                        string Status = string.Empty;

                        if (item["ObservatioRating"] != null)
                        {
                            if (item["ObservatioRating"].ToString() == "1")
                                item["Rating"] = "Major";
                            else if (item["ObservatioRating"].ToString() == "2")
                                item["Rating"] = "Moderate";
                            else if (item["ObservatioRating"].ToString() == "3")
                                item["Rating"] = "Minor";
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(item["StatusTimeline"])))
                        {
                            StatusTimeline = Convert.ToDateTime(item["StatusTimeline"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(item["AuditImplementStatus"])))
                        {
                            AuditImplementStatus = Convert.ToInt32(item["AuditImplementStatus"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(item["ImplementationStatus"])))
                        {
                            ImplementationStatus = Convert.ToInt32(item["ImplementationStatus"]);
                        }
                        List<int> OpenstutusList = new List<int>();
                        OpenstutusList.Add(4); OpenstutusList.Add(5);
                        if (StatusTimeline < DateTime.Now && (AuditImplementStatus != 3 && !OpenstutusList.Contains((int)ImplementationStatus)))
                        {
                            Status = "Open-Due";
                        }
                        else if (StatusTimeline >= DateTime.Now && (AuditImplementStatus != 3 && !OpenstutusList.Contains((int)ImplementationStatus)))
                        {
                            Status = "Open-NOt Due";
                        }
                        else if (AuditImplementStatus == 3 && OpenstutusList.Contains((int)ImplementationStatus))
                        {
                            Status = "Closed";
                        }
                        item["Status"] = Status;
                    }

                    ExcelData.Columns.Remove("ObservatioRating");
                    ExcelData.Columns.Remove("StatusTimeline");
                    ExcelData.Columns.Remove("AuditImplementStatus");
                    ExcelData.Columns.Remove("ImplementationStatus");

                    exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                    //exWorkSheet.Cells["A3"].Value = lblDetailViewTitle.Text + " Report";
                    exWorkSheet.Cells["A3"].Value = "Observation Report";
                    exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A3"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A3"].AutoFitColumns(50);

                    exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A5"].Value = "Location";
                    exWorkSheet.Cells["A5"].AutoFitColumns(50);

                    exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B5"].Value = "Financial Year";
                    exWorkSheet.Cells["B5"].AutoFitColumns(15);

                    exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C5"].Value = "Period";
                    exWorkSheet.Cells["C5"].AutoFitColumns(15);

                    exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D5"].Value = "Process";
                    exWorkSheet.Cells["D5"].AutoFitColumns(25);

                    exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E5"].Value = "SubProcess";
                    exWorkSheet.Cells["E5"].AutoFitColumns(25);

                    exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F5"].Value = "Activity Description";
                    exWorkSheet.Cells["F5"].AutoFitColumns(50);

                    exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G5"].Value = "Observation";
                    exWorkSheet.Cells["G5"].AutoFitColumns(50);

                    exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H5"].Value = "Management Response";
                    exWorkSheet.Cells["H5"].AutoFitColumns(50);

                    exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["I5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["I5"].Value = "Recommandation";
                    exWorkSheet.Cells["I5"].AutoFitColumns(50);

                    exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["J5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["J5"].Value = "Time Line";
                    exWorkSheet.Cells["J5"].AutoFitColumns(15);

                    exWorkSheet.Cells["K5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["K5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["K5"].Value = "Observation Category";
                    exWorkSheet.Cells["K5"].AutoFitColumns(25);

                    exWorkSheet.Cells["L5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["L5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["L5"].Value = "Personresponsible Name";
                    exWorkSheet.Cells["L5"].AutoFitColumns(20);

                    exWorkSheet.Cells["M5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["M5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["M5"].Value = "Personresponsible Email Address";
                    exWorkSheet.Cells["M5"].AutoFitColumns(20);

                    exWorkSheet.Cells["N5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["N5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["N5"].Value = "Owner Name";
                    exWorkSheet.Cells["N5"].AutoFitColumns(20);

                    exWorkSheet.Cells["O5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["O5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["O5"].Value = "Owner Email Address";
                    exWorkSheet.Cells["O5"].AutoFitColumns(20);

                    exWorkSheet.Cells["P5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["P5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["P5"].Value = "Age";
                    exWorkSheet.Cells["P5"].AutoFitColumns(20);

                    exWorkSheet.Cells["Q5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["Q5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["Q5"].Value = "Age Bracket";
                    exWorkSheet.Cells["Q5"].AutoFitColumns(20);

                    exWorkSheet.Cells["R5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["R5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["R5"].Value = "Observation Rating";
                    exWorkSheet.Cells["R5"].AutoFitColumns(20);

                    exWorkSheet.Cells["S5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["S5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["S5"].Value = "Status";
                    exWorkSheet.Cells["S5"].AutoFitColumns(20);

                    //Assign borders
                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 19])
                    {
                        col.Style.Numberformat.Format = "dd/MM/yyyy";
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Justify;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        col.Style.WrapText = true;
                        //col.AutoFitColumns(20);
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        //col.Style.Border.Right.Styl
                    }

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=" + FileName + ".xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    Response.End();
                    //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}