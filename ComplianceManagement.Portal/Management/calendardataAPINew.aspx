﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="calendardataAPINew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Litigation.Common.calendardataAPINew" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">
    
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
    <title></title>
     <style type="text/css">
        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 1px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 210px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 0px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
    </style>

          
    <script type="text/javascript">

        $(document).ready(function ()
        {
            $("#exportReport").kendoTooltip({
                content: "Download Upcoming Hearing"
            });
            bindgrid();
        });

        function bindgrid() {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Litigation/GetCalenderComplianceDetails?UserID=<% =UId%>&customerID=<% =CustId%>&datevalue=<% =date%>&FlagIsApp=<% =FlagIsApp%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                        //read: '<% =Path%>Litigation/GetCalenderComplianceDetails?UserID=<% =UId%>&customerID=<% =CustId%>&datevalue=<% =date%>&FlagIsApp=<% =FlagIsApp%>'
                    },
                     schema: {
                        data: function (response) {   
                            window.parent.hideloader();                         
                            return response;
                        }
                    },
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                dataBinding: function () {
                     record = (this.dataSource.page() - 1) * this.dataSource.pageSize();                      
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                pageable: {
                    pageSize: 5,
                    input: true
                },
                columns: [
                {
                    title: "Sr. No.",
                    template: "#= ++record #",
                    width: 70
                    },
                    {
                        field: "ReminderDate", title: 'Hearing',
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }
                    },
                    {
                        field: "Title", title: 'Case',
                       width: "25%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "CourtName", title: 'Court',
                        width: "25%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                 
                     {
                        command: [
                            { name: "edit1", text: "", iconClass: "k-icon k-i-eye", className: "ob-view" }],
                         title: "Action", lock: true, width: 80, headerAttributes: {
                             style: "text-align: center;"
                         }
                    }
                ]
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit1",
                content: function (e) {
                    return "Show Hearing Details";
                }
            });

            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

              $(document).on("click", "#grid tbody tr .ob-view", function (e) {
                  var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));                    
                 parent.ShowUpcomingHearingPop(item.NoticeCaseInstanceID, "C");
                return true;
            });
        }

         function exportReportMain(e) {
          
            var UId = document.getElementById('UId').value;
            var customerId = document.getElementById('CustomerId').value;
            var PathName = document.getElementById('Path').value;
            var FlagIsApp = document.getElementById('FlagDetail').value;
            var FlagDate = document.getElementById('Flagdate').value;
            
             $.ajax({
                 type: "GET",
                 url: '' + PathName + '//LitigationExportReport/GetCalenderDetailsReport',
                 data: {
                     UserID: UId,
                     customerID: customerId,
                     datevalue: FlagDate,
                     FlagIsApp: FlagIsApp
                 },
                 success: function (response) {
                     if (response != "Error" && response != "No Record Found" && response != "") {
                         window.location.href = '' + PathName + '/LitigationExportReport/GetFile?userpath=' + response + '';
                     }
                     if (response == "No Record Found") {
                         alert("No Record Found");
                     }
                 }
             });
            e.preventDefault();
            return false;
        }
        
         function ShowNoticeCaseDialog(noticeCaseInstanceID, type) {
            parent.ShowUpcomingHearingPop(noticeCaseInstanceID, type);
        };
       
    </script>
    
  </head>
    <body>
  <form>     
      <div id="example">
          
       <div id="grid" style="margin-bottom: 10px;margin-right: 5px;"></div>
          <button id="exportReport" onclick="exportReportMain(event)"  data-tooltip="Hello World" style="cursor: pointer;min-width: 38px;min-height: 33px;border-radius: 35px;border-width: 0px;"><span class="k-icon k-i-download"></span></button>
          <%--<button id="exportReport" onclick="exportReportMain(event)" class="k-button k-button-icontext hidden-on-narrow" style="background-image: url(/Images/ExcelK.png); background-repeat: no-repeat; width: 35px; height: 30px; background-color: white; border: none;"></button>--%>
      </div> 
      <div class="col-lg-8 col-md-8">
                <input id="Path" type="hidden" value="<% =Path%>" />
                <input id="CustomerId" type="hidden" value="<% =CustId%>" />
                <input id="UId" type="hidden" value="<% =UId%>" />
                <input id="FlagDetail" type="hidden" value="<% =FlagIsApp%>" />
                <input id="Flagdate" type="hidden" value="<% =date%>" />
            </div>
  </form>
        </body>
 </html>



