﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GradingDisplayKendo.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.GradingDisplayKendo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white; margin-right: -21px; margin-left: 16px; overflow: hidden">
<head runat="server">
    <title></title>

    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <script src="../../Newjs/bootstrap.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>

    <style type="text/css">
        button, html input[type=button], input[type=reset], input[type=submit] {
            -webkit-appearance: button;
            cursor: pointer;
        }

        .modal, .modal-backdrop {
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .modal-content, .popover {
            background-clip: padding-box;
        }

        .modal-open .navbar-fixed-bottom, .modal-open .navbar-fixed-top, body.modal-open {
            margin-right: 15px;
        }

        .modal {
            display: none;
            overflow: auto;
            overflow-y: scroll;
            position: fixed;
            z-index: 1040;
        }

            .modal.fade .modal-dialog {
                -webkit-transform: translate(0,-25%);
                -ms-transform: translate(0,-25%);
                transform: translate(0,-25%);
                -webkit-transition: -webkit-transform .3s ease-out;
                -moz-transition: -moz-transform .3s ease-out;
                -o-transition: -o-transform .3s ease-out;
                transition: transform .3s ease-out;
            }

            .modal.in .modal-dialog {
                -webkit-transform: translate(0,0);
                -ms-transform: translate(0,0);
                transform: translate(0,0);
            }

        .modal-dialog {
            margin-left: auto;
            margin-right: auto;
            width: auto;
            padding: 10px;
            z-index: 1050;
        }

        .modal-content {
            position: relative;
            background-color: #fff;
            border: 1px solid #999;
            border: 1px solid rgba(0,0,0,.2);
            border-radius: 6px;
            -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
            box-shadow: 0 3px 9px rgba(0,0,0,.5);
            outline: 0;
        }

        .modal-backdrop {
            position: fixed;
            z-index: 1030;
            background-color: #000;
        }

            .modal-backdrop.fade {
                opacity: 0;
                filter: alpha(opacity=0);
            }

            .carousel-control, .modal-backdrop.in {
                opacity: .5;
                filter: alpha(opacity=50);
            }

        .modal-header {
            padding: 15px;
            border-bottom: 1px solid #e5e5e5;
            min-height: 16.43px;
        }

            .modal-header .close {
                margin-top: -2px;
            }

        .modal-title {
            margin: 0;
            line-height: 1.428571429;
        }

        .modal-body {
            position: relative;
            padding: 20px;
        }

        .modal-footer {
            margin-top: 15px;
            padding: 19px 20px 20px;
            text-align: right;
            border-top: 1px solid #e5e5e5;
        }



            .modal-footer:after, .modal-footer:before {
                content: " ";
                display: table;
            }

            .modal-footer .btn + .btn {
                margin-left: 5px;
                margin-bottom: 0;
            }

            .modal-footer .btn-group .btn + .btn {
                margin-left: -1px;
            }

            .modal-footer .btn-block + .btn-block {
                margin-left: 0;
            }

        /*@media screen and (min-width:768px) {*/
        @media screen and (min-width:800px) {
            .modal-dialog {
                left: 50%;
                right: auto;
                width: 600px;
                padding-top: 30px;
                padding-bottom: 30px;
            }

            .modal-content {
                -webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);
                box-shadow: 0 5px 15px rgba(0,0,0,.5);
            }
        }

        .btn-group-vertical > .btn-group:after, .btn-toolbar:after, .clearfix:after, .container:after, .dropdown-menu > li > a, .form-horizontal .form-group:after, .modal-footer:after, .nav:after, .navbar-collapse:after, .navbar-header:after, .navbar:after, .pager:after, .panel-body:after, .row:after {
            clear: both;
        }

        .media, .media-body, .modal-open, .progress {
            overflow: hidden;
        }

        .close, .list-group-item > .badge {
            float: right;
        }

        .close {
            font-size: 21px;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            opacity: .2;
            filter: alpha(opacity=20);
        }

            .close:focus, .close:hover {
                color: #000;
                text-decoration: none;
                cursor: pointer;
                opacity: .5;
                filter: alpha(opacity=50);
            }

        button.close {
            padding: 0;
            cursor: pointer;
            background: 0 0;
            border: 0;
            -webkit-appearance: none;
        }

        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .dashboard h1 {
            color: #555;
            padding: 0px 0 0px;
            margin: 0 0 0px;
        }

        .Dashboard-white-widget {
            padding: 0px 10px 10px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .modal-header, .panel-heading {
            background: #fff;
            color: #688a7e;
            margin-inline-end: -1676px;
            height: 27px;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

        .k-block, .k-draghandle, .k-inline-block, .k-widget {
            border-style: solid;
            /* border-width: 1px; */
            -webkit-appearance: none;
        }

        ul#rblRole1 {
            list-style-type: none;
            margin-left: -23px;
        }

        li.active {
            color: #1fd9e1 !important;
            float: left;
            margin-right: 2%;
            border-bottom: 2px solid #1fd9e1;
        }

        li.inactive1 {
            color: #666 !important;
        }
    </style>
    <style type="text/css">
        input[type=checkbox], input[type=radio] {
            margin: 4px 2px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: inset 0 0 1px 1px #14699f;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px #d7dae0;
            box-shadow: inset 0 0 1px 1px white;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 320px !important;
            overflow: hidden;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
            font-size:12px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            border-color: #1fd9e1;
            background-color: white;
            color: gray;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: -2px;
            color: inherit;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 0px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 0px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        li {
            cursor: pointer;
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 5px 0px;
            margin-top: 1px\9;
            line-height: normal;
        }

        .k-multiselect-wrap .k-input {
            /*padding-top:6px;*/
            display: inherit !important;
        }
        .k-filter-menu .k-button {
            width: 27%;
            background-color: #E9EAEA;
            color: black;
            border: 2px solid rgba(0, 0, 0, 0.5);
        }
    </style>

    <script>
        var perGradingRiskChart = "";
        function BindGridApply(e) {
            Bindgrid();
            FilterGridGD();
            e.preventDefault();
        }
        $(document).ready(function () {
            $("#txtSearchComplianceID").on('input', function (e) {
                FilterGridGD();
            });
            $("#Startdatepicker").kendoDatePicker({
                format: "dd-MM-yyyy"
            });

            $("#Lastdatepicker").kendoDatePicker({
                format: "dd-MM-yyyy"
            });
            BindStatus();
            BindPastData();
            BindLocation();
            BindCategory();
            BindAct();
            BindType();
            $('li#liDetails').addClass('active');
            $('li#liGraph').addClass('inactive1');
            var sdate = new Date("<%=sdate%>");
            var edate = new Date("<%=edate%>");

            $("#Startdatepicker").data("kendoDatePicker").value(sdate);
            $("#Lastdatepicker").data("kendoDatePicker").value(edate);
            Bindgrid();
        });

        function BindType() {

            var IsApprover = 0;
            if ('<%=IsApprover%>' == 'False') {
                IsApprover = 0;
            }
            else {
                IsApprover = 1;
            }
            var DeptHead = 0;
            if ('<% =DeptHead%>' == 'False') {
                DeptHead = 0;
            }
            else {
                DeptHead = 1;
            }
            <%if (ComplianceTypeFlag == "I")%><%{%>
            $("#dropdownType").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "Id",
                optionLabel: "Select Type",
                change: function (e) {
                    FilterGridGD();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindInternalComplianceTypeDashboradComplianceDetails?Userid=<% =UserId%>&Customerid=<% =customerid%>&bid=<% =Customerbanchid%>&catid=-1&StatusFlag=0&IsApprover=' + IsApprover + '&Isdept=' + DeptHead,
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }

                    }
                }
            });
            <%}%>
        }

        function BindCategory() {
            
            var Role = "<%=Falg%>";

            <%if (DeptHead == true){%>
            Role = "DEPT";
            <%}%>

            <% if (ComplianceTypeFlag == "I"){%>  
            $("#dropdownfunction").kendoDropDownTree({
                filter: "startswith",
                placeholder: "Category",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "Id",
                change: function (e) {
                    FilterGridGD();
                    fCreateStoryBoardGD('dropdownfunction', 'filterCategory', 'function');
                },
                dataSource: {
                    severFiltering: true,

                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindInternalFunctionList?UserID=<% =UserId%>&customerID=<% =customerid%>&Flag=' + Role,
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }
            });
            <%}%>

            <% if (ComplianceTypeFlag == "S"){%>  
            $("#dropdownfunction").kendoDropDownTree({
                filter: "startswith",
                placeholder: "Category",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "Id",
                change: function (e) {
                    FilterGridGD();
                    fCreateStoryBoardGD('dropdownfunction', 'filterCategory', 'function');
                },
                dataSource: {
                    severFiltering: true,

                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindStatutoryFunctionList?UserID=<% =UserId%>&customerID=<% =customerid%>&Flag=' + Role,
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }
            });
            <%}%> 
        }

        function BindStatus() {
            $("#dropdownlistStatus").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: false,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    Bindgrid();
                    fCreateStoryBoardGD('dropdownlistStatus', 'filterstatus', 'status');
                },
                dataSource: [
                    { text: "Open", value: "1" },
                    { text: "Complied but pending review", value: "2" },
                    { text: "Complied Delayed but pending review", value: "3" },
                    { text: "Closed Timely", value: "4" },
                    { text: "Closed Delayed", value: "5" },
                    { text: "Rejected", value: "6" },
                    { text: "Approved", value: "9" },
                    { text: "In Progress", value: "10" },
                    { text: "Revise Compliance", value: "11" },
                    { text: "Not Applicable", value: "18" }
                ]
            });

        }

        function BindPastData() {
            $("#dropdownlistTypePastdata").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    Bindgrid();
                },
                index: 4,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });
        }

        function BindAct() {
            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: false,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    FilterGridGD();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindActList?UId=<% =UserId%>&CId=<% =customerid%>&Flag=MGMT',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                },
                dataBound: function (e) {
                    e.sender.list.width("500");
                }
            });

        }

        function BindLocation() {

            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    Bindgrid();
                    fCreateStoryBoardGD('dropdowntree', 'filtersstoryboard', 'loc');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CId%>&userId=<% =UserId%>&Flag=<% =Falg%>&IsStatutoryInternal=<%=ComplianceTypeFlag%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            id: "ID",
                            children: "Children"
                        }
                    }
                }
            });

            var dropdownlist = $("#dropdowntree").data("kendoDropDownTree");
            dropdownlist.bind("dataBound", dropdownlist_dataBound);
        }

        function dropdownlist_dataBound(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([<%=Customerbanchids%>]);
        }

        var record = 0;

        function Bindgrid() {

            var list1 = $("#dropdowntree").data("kendoDropDownTree")._values;
            var locationsdetails = [];
            $.each(list1, function (i, v) {
                locationsdetails.push(v);
            });
            var list2 = $("#dropdownlistStatus").data("kendoDropDownTree")._values;
            var statusdetails = [];
            $.each(list2, function (i, v) {

                statusdetails.push(v);
            });

            if (locationsdetails == null || locationsdetails == []) {
                locationsdetails = -1;
            }
            if (statusdetails == null || statusdetails == []) {
                statusdetails = -1;
            }
            var item = 'customerid=<%=CId%>&Customerbanchid=' + JSON.stringify(locationsdetails) + '&sdate=' + $("#Startdatepicker").val() + '&edate=' + $("#Lastdatepicker").val() +'&IsApprover=<%=IsApprover%>&ComplianceTypeFlag=<%=ComplianceTypeFlag%>&MonthId=' + $("#dropdownlistTypePastdata").val() + '&IsDeptHead=<%=DeptHead%>&UserID=<%=UserId%>&StatusID=' + JSON.stringify(statusdetails);
            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GradingDisplayKendoPopup?' + item,
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                ComplianceID: { type: "string", },
                                InternalComplianceID: { type: "string" },
                                ScheduledOn: { type: "date", },
                                InternalScheduledOn: { type: "date" }
                            }
                        },
                        data: function (response) {
                            if (response.length != 0) {
                                if ('<%=DeptHead%>' == 'True' && "<%=ComplianceTypeFlag%>" == "S") {
                                    return response[0].StatutoryDept;
                                }
                                else if ("<%=ComplianceTypeFlag%>" == "S") {
                                    return response[0].Statutory;
                                }
                                else if ('<%=DeptHead%>' == 'True' && "<%=ComplianceTypeFlag%>" == "I") {
                                    return response[0].InternalDept;
                                }
                                else if ("<%=ComplianceTypeFlag%>" == "I") {
                                    return response[0].Internal;
                                }
                            }
                            else
                                return '';
                        },
                        total: function (response) {
                            if (response.length != 0) {
                                perGradingRiskChart = response[0].perGradingRiskChart;

                                if ('<%=DeptHead%>' == 'true' && "<%=ComplianceTypeFlag%>" == "S") {
                                    return response[0].StatutoryDept.length;
                                }
                                else if ("<%=ComplianceTypeFlag%>" == "S") {
                                    return response[0].Statutory.length;
                                }
                                else if ('<%=DeptHead%>' == 'true' && "<%=ComplianceTypeFlag%>" == "I") {
                                    return response[0].InternalDept.length;
                                }
                                else if ("<%=ComplianceTypeFlag%>" == "I") {
                                    return response[0].Internal.length;
                                }
                            }
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                noRecords: true,
                messages: {
                    noRecords: "No records found"
                },
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBound: function (e) {
                    window.parent.forchild($("body").height() + 100);
                },
                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                columns: [
                    {
                        title: "Sr.No.",
                        template: "#= ++record #",
                        width: 70
                    },
                    {
                        field: "Branch", title: 'Location',
                        width: "15%;",
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },

                    <% if (ComplianceTypeFlag == "I")
        {%>
                    { field: "InternalComplianceID", title: "Compliance ID", filterable: { multi: true, extra: false, search: true, }, width: "15%" },
                      <%}%>
                    <% if (ComplianceTypeFlag == "S")
        {%>
                    { field: "ComplianceID", title: "Compliance ID", filterable: { multi: true, extra: false, search: true, }, width: "15%" },
                    <%}%>
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "18%;",
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: 'User', title: 'Performer',
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: 'ReviewerName', title: 'Reviewer',
                        hidden: true,
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    <% if (ComplianceTypeFlag == "I")
        {%>
                    {

                        field: "InternalScheduledOn", title: 'Due Date',
                        template: "#= kendo.toString(kendo.parseDate(InternalScheduledOn, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }, width: "10%",
                    },
                    <%}%>
                    <% if (ComplianceTypeFlag == "S")
        {%>
                    {
                        field: "ScheduledOn", title: 'Due Date',
                        template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }, width: "10%",
                    },
                    <%}%>
                    {
                        field: "ForMonth", title: 'For Period',
                        width: "12%;",
                        type: "string",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: 'Status', title: 'Status',
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            { name: "edit", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" },
                        ], title: "Action", lock: true, width: 100,// width: 150,
                        headerAttributes: {
                            style: "text-align: center;font-style:bold"
                        }
                    }
                ]
            });
            $("#grid").kendoTooltip({
                filter: "td",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "th",
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Overview";
                }
            });

            $(document).on("click", "#grid tbody tr .ob-overview", function (e) {

                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                $('#divOverView').modal('show');
                $('#OverViews').attr('width', '1150px');
                $('#OverViews').attr('height', '600px');
                $('.modal-dialog').css('width', '1200px');
                <% if (ComplianceTypeFlag == "S")
        {%>
                $('#OverViews').attr('src', "../Common/ComplianceOverview.aspx?ComplianceScheduleID=" + item.ScheduledOnID + "&ComplainceInstatnceID=" + item.ComplianceInstanceID);
                <%}%>
                 <% if (ComplianceTypeFlag == "I")
        {%>
                $('#OverViews').attr('src', "../Common/ComplianceOverviewInternal.aspx?ComplianceScheduleID=" + item.InternalScheduledOnID + "&ComplainceInstatnceID=" + item.InternalComplianceInstanceID);
                <%}%>
                return true;
            });
        }

        function fCreateStoryBoardGD(Id, div, filtername) {

            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            $('#' + div).css('display', 'block');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;&nbsp:&nbsp;');
            }
            if (div == 'filterstatus') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp:&nbsp;');
            }
            if (div == 'filterCategory') {
                $('#' + div).append('Category&nbsp;&nbsp;&nbsp;&nbsp:&nbsp;');
            }
            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                //if (buttontest.length > 10) {
                //    buttontest = buttontest.substring(0, 10).concat("...");
                //}
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#EBEBEB;height: 20px;Color:Gray;border-radius:10px;margin-left:5px;margin-bottom: 4px;"><span unselectable="on" title="' + $($(button).find('span')[0]).text() + '">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory11(this)" title="Clear" aria-label="Clear" class="k-select" style="padding-left: 6px;"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory11(this)"  class="k-icon k-i-close" title="Clear" aria-label="Clear" style="font-size: 12px;"></span></span></li>');
            }

            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            }
        }

        function fcloseStory11(obj) {

            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);
            $(upperli).remove();

            fCreateStoryBoardGD('dropdowntree', 'filtersstoryboard', 'loc');
            fCreateStoryBoardGD('dropdownlistStatus', 'filterstatus', 'status');
            fCreateStoryBoardGD('dropdownfunction', 'filterCategory', 'function');

        }

        function FilterGridGD() {
            var InternalStatutoryCategory = "";
            var InternalstatutoryDate = "";
            var InternalType = "";
            var ActId = "";
            var ComplianceID = "";

            <%if (ComplianceTypeFlag == "I")
        {%>

            InternalStatutoryCategory = "IComplianceCategoryID";
            InternalstatutoryDate = "InternalScheduledOn";
            InternalType = "InternalComplianceTypeName";
            ActId = "ActID";
            ComplianceID = "InternalComplianceID";
            <%}%>
            <%if (ComplianceTypeFlag == "S")
            {%>
            InternalStatutoryCategory = "ComplianceCategoryId";
            InternalstatutoryDate = "ScheduledOn";
            ActId = "ActID";
            ComplianceID = "ComplianceID";
            <%}%>
            //risk Details
            var Riskdetails = [];
            if ($("#dropdownlistRisk").data("kendoDropDownTree") != undefined) {
                Riskdetails = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
            }
            //user Details
            var userdetails = [];
            if ($("#dropdownUser").data("kendoDropDownTree") != undefined) {
                userdetails = $("#dropdownUser").data("kendoDropDownTree")._values;
            }
            //datefilter
            var datedetails = [];
            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                datedetails.push({
                    field: "ScheduledOn", operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'dd-MM-yyyy')
                });
            }
            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                datedetails.push({
                    field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'dd-MM-yyyy')
                });
            }
            var catdetails = [];
            if ($("#dropdownfunction").data("kendoDropDownTree") != undefined) {
                catdetails = $("#dropdownfunction").data("kendoDropDownTree")._values;
            }

            var finalSelectedfilter = { logic: "and", filters: [] };

            if (Riskdetails.length > 0
                || $("#txtSearchComplianceID").val() != ""
                || catdetails.length > 0
                || ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined)
                || ($("#dropdownType").val() != undefined && $("#dropdownType").val() != null && $("#dropdownType").val() != "")
                || datedetails.length > 0
                || userdetails.length > 0) {

                if (catdetails.length > 0) {
                    var catFilter = { logic: "or", filters: [] };

                    $.each(catdetails, function (i, v) {
                        catFilter.filters.push({
                            field: InternalStatutoryCategory, operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(catFilter);
                }

                if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
                    var ActFilter = { logic: "or", filters: [] };
                    ActFilter.filters.push({
                        field: ActId, operator: "eq", value: parseInt($("#dropdownACT").val())
                    });
                    finalSelectedfilter.filters.push(ActFilter);
                }

                if ($("#dropdownType").val() != "" && $("#dropdownType").val() != null && $("#dropdownType").val() != undefined) {
                    var TypeFilter = { logic: "or", filters: [] };
                    TypeFilter.filters.push({
                        field: InternalType, operator: "eq", value: $("#dropdownType").val()
                    });
                    finalSelectedfilter.filters.push(TypeFilter);
                }

                if (datedetails.length > 0) {
                    if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                        var DateFilter = { logic: "or", filters: [] };
                        DateFilter.filters.push({
                            field: InternalstatutoryDate, operator: "gte", value: kendo.parseDate($("#Startdatepicker").val(), 'dd-MM-yyyy')
                        });

                        finalSelectedfilter.filters.push(DateFilter);
                    }

                    if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                        var DateFilter = { logic: "or", filters: [] };
                        DateFilter.filters.push({
                            field: InternalstatutoryDate, operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'dd-MM-yyyy')
                        });

                        finalSelectedfilter.filters.push(DateFilter);
                    }
                }

                if (userdetails.length > 0) {
                    var UserFilter = { logic: "or", filters: [] };

                    $.each(userdetails, function (i, v) {
                        UserFilter.filters.push({
                            field: "UserID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(UserFilter);
                }
                if ($("#txtSearchComplianceID").val() != "") {
                    var RiskFilter = { logic: "or", filters: [] };
                    RiskFilter.filters.push({
                        field: ComplianceID, operator: "contains", value: $("#txtSearchComplianceID").val()
                    });
                    finalSelectedfilter.filters.push(RiskFilter);
                }
                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }
        }
        function exportReport(e) {

            if (document.getElementById('IsLabel').value == 1) {

                var ReportName = "Grading Report Details";
                var customerName = document.getElementById('CustName').value;
                // var todayDate = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
                var todayDate = moment().format('DD-MMM-YYYY');
                var grid = $("#grid").getKendoGrid();

                var rows = [
                    {
                        cells: [
                            { value: "Entity/ Location:", bold: true },
                            { value: customerName }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Name:", bold: true },
                            { value: ReportName }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Generated On:", bold: true },
                            { value: todayDate },

                        ]
                    },
                    {
                        cells: [
                            { value: "" }
                        ]
                    },
                    {
                        cells: [
                            { value: "Compliance ID", bold: true },
                            { value: "Location", bold: true },
                            { value: "Act Name", bold: true },
                            { value: "Short Description", bold: true },
                            { value: "For Period", bold: true },
                            { value: "Performer", bold: true },
                            { value: "Due Date", bold: true },
                            { value: "For Month", bold: true },
                            { value: "Status", bold: true },
                            { value: "Label", bold: true }
                        ]
                    }
                ];

                var trs = grid.dataSource;
                var filteredDataSource = new kendo.data.DataSource({
                    data: trs.data(),
                    filter: trs.filter()
                });

                filteredDataSource.read();
                var data = filteredDataSource.view();
                for (var i = 0; i < data.length; i++) {
                    var dataItem = data[i];
                    rows.push({
                        cells: [
                            { value: dataItem.ComplianceID },
                            { value: dataItem.Branch },
                            { value: dataItem.Name },
                            { value: dataItem.ShortDescription },
                            { value: dataItem.ForMonth },
                            { value: dataItem.User },

                            { value: dataItem.ScheduledOn, format: "dd-MMM-yyyy" },
                            { value: dataItem.ForMonth },
                            { value: dataItem.Status },

                            { value: dataItem.SequenceID }
                        ]
                    });
                }
                for (var i = 4; i < rows.length; i++) {
                    for (var j = 0; j < 10; j++) {
                        rows[i].cells[j].borderBottom = "#000000";
                        rows[i].cells[j].borderLeft = "#000000";
                        rows[i].cells[j].borderRight = "#000000";
                        rows[i].cells[j].borderTop = "#000000";
                        rows[i].cells[j].hAlign = "left";
                        rows[i].cells[j].vAlign = "top";
                        rows[i].cells[j].wrap = true;
                    }
                }
                excelExport(rows, ReportName);
                e.preventDefault();
            }
            else {

                var ReportName = "Report of Compliances";
                var customerName = document.getElementById('CustName').value;
                //  var todayDate = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
                //  todayDate.Cells(x,y).NumberFormat = 'dd-mm-yyyy';
                var todayDate = moment().format('DD-MMM-YYYY');
                var grid = $("#grid").getKendoGrid();

                var rows = [
                    {
                        cells: [
                            { value: "Entity/ Location:", bold: true },
                            { value: customerName }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Name:", bold: true },
                            { value: ReportName }
                        ]
                    },
                    {
                        cells: [
                            { value: "Report Generated On:", bold: true },
                            { value: todayDate },
                            //  {date:"dd-MM-yy"}
                        ]
                    },
                    {
                        cells: [
                            { value: "" }
                        ]
                    },
                    {
                        cells: [
                            { value: "Compliance ID", bold: true },
                            { value: "Location", bold: true },
                            { value: "Act Name", bold: true },
                            { value: "Short Description", bold: true },
                            { value: "For Period", bold: true },
                            { value: "Performer", bold: true },

                            { value: "Due Date", bold: true },
                            { value: "For Month", bold: true },
                            { value: "Status", bold: true },

                        ]
                    }
                ];

                var trs = grid.dataSource;
                var filteredDataSource = new kendo.data.DataSource({
                    data: trs.data(),
                    filter: trs.filter()
                });

                filteredDataSource.read();
                var data = filteredDataSource.view();
                for (var i = 0; i < data.length; i++) {
                    var dataItem = data[i];
                    rows.push({
                        cells: [
                            { value: dataItem.ComplianceID },
                            { value: dataItem.Branch },
                            { value: dataItem.Name },
                            { value: dataItem.ShortDescription },
                            { value: dataItem.ForMonth },
                            { value: dataItem.User },

                            { value: dataItem.ScheduledOn, format: "dd-MMM-yyyy" },
                            { value: dataItem.ForMonth },
                            { value: dataItem.Status },

                        ]
                    });
                }
                for (var i = 4; i < rows.length; i++) {
                    for (var j = 0; j < 9; j++) {
                        rows[i].cells[j].borderBottom = "#000000";
                        rows[i].cells[j].borderLeft = "#000000";
                        rows[i].cells[j].borderRight = "#000000";
                        rows[i].cells[j].borderTop = "#000000";
                        rows[i].cells[j].hAlign = "left";
                        rows[i].cells[j].vAlign = "top";
                        rows[i].cells[j].wrap = true;
                    }
                }
                excelExport(rows, ReportName);
                e.preventDefault();

            }

        }

        function excelExport(rows, ReportName) {


            if (document.getElementById('IsLabel').value == 1) {
                var workbook = new kendo.ooxml.Workbook({
                    sheets: [
                        {
                            columns: [
                                { width: 180 },
                                { width: 180 },
                                { width: 300 },
                                { width: 300 },
                                { width: 180 },
                                { width: 180 },
                                { width: 180 },
                                { width: 100 },
                                { width: 100 },
                                { width: 150 },
                                { width: 100 },
                                { width: 100 }
                            ],
                            title: "Grading Report Details",
                            rows: rows
                        },
                    ]
                });

                var nameOfPage = "Grading Report Details";
                kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });
            }
            else {
                var workbook = new kendo.ooxml.Workbook({
                    sheets: [
                        {
                            columns: [
                                { width: 180 },
                                { width: 180 },
                                { width: 180 },
                                { width: 180 },
                                { width: 300 },
                                { width: 300 },
                                { width: 180 },
                                { width: 180 },
                                { width: 180 },
                                { width: 100 },
                                { width: 100 },
                                { width: 150 },
                                { width: 100 }
                            ],
                            title: "Grading Report Details",
                            rows: rows
                        },
                    ]
                });

                var nameOfPage = "Grading Report Details";
                kendo.saveAs({ dataURI: workbook.toDataURL(), fileName: nameOfPage + " .xlsx" });
            }
        }

        function ClearAllFilterMain(e) {
            $("#txtSearchComplianceID").val('');
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownlistTypePastdata").data("kendoDropDownList").select(4);
            $("#dropdownfunction").data("kendoDropDownTree").value([]);
            $("#dropdownACT").data("kendoDropDownList").select(0);
            <%if (ComplianceTypeFlag == "I")%>
            <%{%>
            $("#dropdownType").data("kendoDropDownList").select(0);
            <%}%>
            $("#Startdatepicker").val('');
            $("#Lastdatepicker").val('');
            $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
            $("#grid").data("kendoGrid").dataSource.filter({});
            Bindgrid();
            e.preventDefault();
        }

        function ShowGraph(e) {
            $('#IFGradingGraphDisplay').attr('src', '../Management/DisplayGradingGraph.aspx?perGradingRiskChart=' + perGradingRiskChart);
            document.getElementById('Details').style = "display:none;";
            document.getElementById('Graph').style = "display:block;";

            $('li#liGraph').removeClass('inactive1');
            $('li#liGraph').addClass('active')
            $('li#liDetails').removeClass('active');
            $('li#liDetails').addClass('inactive1');
            e.preventDefault();
        }

        function ShowDetails(e) {
            document.getElementById('Graph').style = "display:none;";
            document.getElementById('Details').style = "display:block;margin:1%";
            $('li#liDetails').removeClass('inactive1');
            $('li#liDetails').addClass('active')
            $('li#liGraph').removeClass('active');
            $('li#liGraph').addClass('inactive1');
            e.preventDefault();
        }
        function CloseClearPopupView() {
            $('#divOverView').modal('hide');
            Bindgrid();
        }
    </script>
</head>
<body>

    <form id="form1" runat="server" style="margin-left: -14px;">

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <h1 id="display" runat="server" style="height: 30px; width: 98%; background-color: #f8f8f8; margin-top: -8px; font-weight: 100; font-size: 21px; color: #666; margin-bottom: -9px; padding-left: 14px; padding-top: 3px; margin-left: 11px;">Grading Reports</h1>

        <ul id="rblRole1" class="nav nav-tabs">
            <li class="" id="liDetails" onclick="ShowDetails(event);" style="color: blue; float: left; margin-right: 1%;">Details</li>
            <li id="liGraph" style="color: blue;" onclick="ShowGraph(event);">Graph</li>
        </ul>

        <div id="example">
            <div id="Details" style="margin-left: 1%; margin-right: 13px;" runat="server">
                <div style="margin: 0% 1% 1%; width: 99%;">
                    <input id="CustName" type="hidden" value="<% =CustomerName%>" />
                    <input id="IsLabel" type="hidden" value="<% =com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable%>" />
                    <input id="dropdowntree" style="width: 17.5%; margin-right: 10px; margin-left: -12px;" />
                    <input id="dropdownlistTypePastdata" style="width: 15.5%; margin-right: 8px;" />
                    <input id="dropdownACT" style="width: 20.5%; margin-right: 0.8%; margin-left: 2px" />
                    <input id="dropdownlistStatus" data-placeholder="Status" style="width: 195px; margin-right: 10px" />
                    <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" style="width: 13%; margin-right: 0.8%;" />
                    <input id="Lastdatepicker" placeholder="End Date" style="width: 12.8%;" />
                </div>
                <div style="margin: 0% 0% 1%; width: 99%;">
                    <input id="dropdownfunction" style="width: 17.5%; margin-right: 0.8%;" />
                    <input id="txtSearchComplianceID" class="k-textbox" onkeydown="return (event.keyCode!=13);" placeholder="Compliance ID" style="width: 15.5%; margin-right: 0.8%" />

                    <%if (ComplianceTypeFlag == "I")%>
                    <%{%>
                    <input id="dropdownType" style="width: 15.5%; margin-right: 0.8%;" />
                    <%}%>

                    <button id="ClearfilterMain" style="float: right; height: 30px; margin-right: -0.8%;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear"></span>Clear</button>
                    <button id="export" onclick="exportReport(event)" style="height: 30px; float: right; margin-right: 0.8%;"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
                    <button id="Applyfilter" style="height: 30px; float: right; margin-right: 0.8%;" onclick="BindGridApply(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
                </div>
                <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filtersstoryboard">&nbsp;</div>
                <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterstatus">&nbsp;</div>
                <div class="row" style="display: none; padding-bottom: 4px; font-size: 12px; font-weight: bold; color: black;" id="filterCategory">&nbsp;</div>
                <div id="grid"></div>
            </div>

            <div id="Graph" runat="server" style="display: none;">
                <div style="margin-bottom: 4px">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="col-md-2"></div>
                            <iframe id="IFGradingGraphDisplay" src="about:blank" scrolling="no" frameborder="0" width="100%" height="500px"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px; z-index: 9999">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="background-color: #f7f7f7; height: 30px; width: 1170px">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearPopupView();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>