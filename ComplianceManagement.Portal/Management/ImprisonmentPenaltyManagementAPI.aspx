﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImprisonmentPenaltyManagementAPI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.ImprisonmentPenaltyManagementAPI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white; margin-right: 16px;">
<head runat="server">
     <script src="../Scripts/KendoPage/StatutoryMGMT_v1.js"></script>
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
   
  
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
  
   
    <title></title>

    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>
      <style type="text/css">
        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 1px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 350px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
        .k-icon.k-i-calendar {
    margin-top: 0px;
}
    </style>


    <script type="text/x-kendo-template" id="template">      
               
    </script>

    <script type="text/javascript">
        $(window).resize(function () {
            window.parent.forchild($("body").height() + 15);
        });
        function fclosebtn(tbn) {
            $('#' + tbn).css('display', 'none');
            $('#' + tbn).html('');
        }

        var record = 0;

        function BindGrid() {

            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                columnMenuInit(e) {
                    e.container.find('li[role="menuitemcheckbox"]:nth-child(12)').remove();
                },
                dataSource: {
                    transport:
                    {
                        read: {
                            url: '<% =Path%>/Data/GetImprisonmentPenaltyManagementDashboard?Uid=<% =UId%>&ptname=All&attr=All&Cid=<% =CustId%>&bid=<% =branchid%>&FDte=<% =FromDate%>&EDte=<% =Enddate%>&Fltr=<% =Filter%>&fid=-1&ChartName=<% =ChartName%>&INTorSAT=<% =Internalsatutory%>&DptHead=<% =IsDeptHead%>&Status=All&IsAppr=<% =IsApprover%>&Minimum=<% = minimum%>&Maximum=<% = maximum%>&MinimumImprisonment=<% = minimumImprisonment%>&MaximumImprisonment=<% = maximumImprisonment%>&MonthId=' + $("#dropdownPastData").val() + '&FY=' + $("#dropdownFY").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }

                    },
                    schema: {
                        data: function (response) {
                            return response[0].RiskBarChart;
                        },
                        total: function (response) {
                            return response[0].RiskBarChart.length;
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                toolbar: kendo.template($("#template").html()),

                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                pageable: {
                    numeric: true,
                    pageSizes: [5, 10, 20, 'All'],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBinding: function () {
                    var total = this.dataSource._pristineTotal;
                    if (this.dataSource.pageSize() == undefined) {
                        this.dataSource.pageSize(total);
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                    else {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    }
                },
                columns: [
                    { hidden: true, field: "ShortForm", title: "Short&nbsp;Form", width: "10%" },
                    { hidden: true, field: "RiskCategory", title: "Risk", width: "10%" },
                    { hidden: true, field: "CustomerBranchID", title: "BranchID", width: "10%" },
                    { hidden: true, field: "Performer", title: "Performer", width: "10%" },
                    { hidden: true, field: "Reviewer", title: "Reviewer", width: "10%" },
                    {
                        field: "Branch", title: 'Location',
                        width: "10%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ActName", title: 'Act&nbsp;Name',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ShortDescription", title: 'Compliance',
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    //{
                    //    field: "PenaltyDescription", title: 'Penalty Description',
                    //    width: "20%",
                    //    attributes: {
                    //        style: 'white-space: nowrap;'
                    //    },
                    //    filterable: {
                    //        multi: true,
                    //        extra: false,
                    //        search: true,
                    //        operators: {
                    //            string: {
                    //                eq: "Is equal to",
                    //                neq: "Is not equal to",
                    //                contains: "Contains"
                    //            }
                    //        }
                    //    }
                    //},
                    {
                        field: "FixedMinimum", title: 'Fixed Minimum',
                        width: "15%",
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "FixedMaximum", title: 'Fixed Maximum',
                        width: "17%",
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "MinimumYears", title: 'Minimum Years',
                        width: "17%",
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },


                    {
                        field: "MaximumYears", title: 'Maximum Years',
                        width: "17%",
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {

                        field: "ScheduledOn", title: 'Due Date',
                        template: "#= kendo.toString(kendo.parseDate(ScheduledOn, 'yyyy-MM-dd'), 'dd-MM-yyyy') #",
                        type: "date",
                        format: "{0:dd-MM-yyyy}",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    type: "date",
                                    format: "{0:dd-MM-yyyy}",
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            },
                        }, width: "10%",
                    },
                    {
                        field: "ForMonth", title: 'Period', filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }, width: "10%"
                    },
                    {
                        field: "Status", title: 'Status',
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        },width:"10%"
                    }
                ]
            });

            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            window.parent.forchild($("body").height() + 15);
        }

        function BindGridApply(e) {
            var setStartDate = $("#Startdatepicker").val();
            var setEndDate = $("#Lastdatepicker").val();


            BindGrid();
            FilterGridDemo();

            if (setStartDate != null) {
                $("#Startdatepicker").data("kendoDatePicker").value(setStartDate);
            }
            if (setEndDate != null) {
                $("#Lastdatepicker").data("kendoDatePicker").value(setEndDate);
            }
            e.preventDefault();
        }

        function FilterGridDemo() {


            var Riskdetails = [];
            if ($("#dropdownlistRisk").data("kendoDropDownTree") != undefined) {
                Riskdetails = $("#dropdownlistRisk").data("kendoDropDownTree")._values;
            }

            //status Details
            var Statusdetails = [];
            if ($("#dropdownlistStatus").data("kendoDropDownTree") != undefined) {
                var Statusdetails = $("#dropdownlistStatus").data("kendoDropDownTree")._values;

                if (Statusdetails.length >= 1) {
                    for (var i = 0; i < Statusdetails.length; i++) {
                        if (Statusdetails[i] === "No" || Statusdetails[i] === "") {
                            Statusdetails.splice(i, 1);
                        }
                    }
                }
            }

            //user Details      
            var Userdetails = [];
            if ($("#dropdownUser").data("kendoDropDownTree") != undefined) {
                Userdetails = $("#dropdownUser").data("kendoDropDownTree")._values;
            }

            //location details
            var locationsdetails = [];
            if ($("#dropdowntree").data("kendoDropDownTree") != undefined) {
                locationsdetails = $("#dropdowntree").data("kendoDropDownTree")._values;
            }

            //datefilter
            var datedetails = [];
            if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                datedetails.push({
                    field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                });
            }
            if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                datedetails.push({
                    field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                });
            }

            var Departmentdetails = -1;
            if (DisplayDepartment.value > 0) {
                Departmentdetails = DisplayDepartment.value;
            }

            var finalSelectedfilter = { logic: "and", filters: [] };
            debugger;
            if (Riskdetails.length > 0 || Statusdetails.length > 0 || locationsdetails.length > 0 ||
                Userdetails.length > 0 || datedetails.length > 0 ||
                ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) ||
                ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) ||
                ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != null && $("#dropdownSequence").val() != "") ||
                Departmentdetails > 0) {
                if ($("#dropdownSequence").val() != undefined && $("#dropdownSequence").val() != "" && $("#dropdownSequence").val() != null) {

                    var SeqFilter = { logic: "or", filters: [] };

                    SeqFilter.filters.push({
                        field: "SequenceID", operator: "eq", value: $("#dropdownSequence").val()
                    });

                    finalSelectedfilter.filters.push(SeqFilter);
                }

                if (Departmentdetails > 0) {

                    var FunctionDepartmentFilter = { logic: "or", filters: [] };

                    FunctionDepartmentFilter.filters.push({
                        field: "DepartmentID", operator: "eq", value: parseInt(Departmentdetails)
                    });

                    finalSelectedfilter.filters.push(FunctionDepartmentFilter);
                }
                if (datedetails.length > 0) {
                    var DateFilter = { logic: "or", filters: [] };

                    if ($("#Startdatepicker").val() != null && $("#Startdatepicker").val() != "") {
                        DateFilter.filters.push({
                            field: "ScheduledOn", operator: "gt", value: kendo.parseDate($("#Startdatepicker").val(), 'MM/dd/yyyy')
                        });
                    }
                    if ($("#Lastdatepicker").val() != null && $("#Lastdatepicker").val() != "") {
                        DateFilter.filters.push({
                            field: "ScheduledOn", operator: "lte", value: kendo.parseDate($("#Lastdatepicker").val(), 'MM/dd/yyyy')
                        });
                    }
                    finalSelectedfilter.filters.push(DateFilter);
                }
                if (Userdetails.length > 0) {
                    var UserFilter = { logic: "or", filters: [] };

                    $.each(Userdetails, function (i, v) {
                        UserFilter.filters.push({
                            field: "UserID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(UserFilter);
                }

                if ($("#dropdownACT").val() != "" && $("#dropdownACT").val() != null && $("#dropdownACT").val() != undefined) {
                    var ActFilter = { logic: "or", filters: [] };

                    ActFilter.filters.push({
                        field: "ActID", operator: "eq", value: parseInt($("#dropdownACT").val())
                    });

                    finalSelectedfilter.filters.push(ActFilter);
                }
                if (locationsdetails.length > 0) {
                    var LocationFilter = { logic: "or", filters: [] };

                    $.each(locationsdetails, function (i, v) {
                        LocationFilter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });

                    finalSelectedfilter.filters.push(LocationFilter);
                }
                if ($("#dropdownfunction").val() != "" && $("#dropdownfunction").val() != null && $("#dropdownfunction").val() != undefined) {
                    //if ($("#dropdownfunction").val() != 0) {

                    //    var FunctionFilter = { logic: "or", filters: [] };

                    //    FunctionFilter.filters.push({
                    //        field: "ComplianceCategoryId", operator: "eq", value: parseInt($("#dropdownfunction").val())
                    //    });

                    //    finalSelectedfilter.filters.push(FunctionFilter);
                    //}
                }
                if (Statusdetails.length > 0) {
                    var StatusFilter = { logic: "or", filters: [] };

                    $.each(Statusdetails, function (i, v) {
                        StatusFilter.filters.push({
                            field: "FilterStatus", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(StatusFilter);
                }

                if (Riskdetails.length > 0) {
                    //var CategoryFilter = { logic: "or", filters: [] };

                    //$.each(Riskdetails, function (i, v) {

                    //    CategoryFilter.filters.push({
                    //        field: "Risk", operator: "eq", value: parseInt(v)
                    //    });
                    //});

                    //finalSelectedfilter.filters.push(CategoryFilter);
                }
                if (finalSelectedfilter.filters.length > 0) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                }
                else {
                    $("#grid").data("kendoGrid").dataSource.filter({});
                }
            }
            else {
                $("#grid").data("kendoGrid").dataSource.filter({});
            }


        }

        $(document).ready(function () {

            $("#Startdatepicker").kendoDatePicker({
                autoWidth: false,
            });
            $("#Lastdatepicker").kendoDatePicker({
                autoWidth: false,
            });

            $("#dropdownfunction").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: false,
                dataTextField: "Name",
                dataValueField: "Id",
                optionLabel: "Select Category",
                change: function (e) {
                    BindGrid();
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownfunction', 'filterCategory', 'function');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindStatutoryFunctionList?UserID=<% =UId%>&customerID=<% =CustId%>&Flag=MGMT',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }
            });

            $("#dropdownfunction").data("kendoDropDownList").value('<%=functionid%>');

            $("#dropdownfunction").val('<%=functionid%>');

            BindGrid();

            $("#dropdownFY").kendoDropDownList({

                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    if ($("#dropdownFY").val() != "0") {
                        $("#dropdownPastData").data("kendoDropDownList").select(4);
                        fCreateStoryBoard('dropdownFY', 'filterFY', 'FY');
                    }
                },
                dataSource: [
                    { text: "Financial Year", value: "0" },
                    { text: "2020-2021", value: "2020-2021" },
                    { text: "2019-2020", value: "2019-2020" },
                    { text: "2018-2019", value: "2018-2019" },
                    { text: "2017-2018", value: "2017-2018" },
                    { text: "2016-2017", value: "2016-2017" },
                ]
            });


            $("#dropdownlistRisk").kendoDropDownTree({
                placeholder: "Risk",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');
                },
                dataSource: [
                    { text: "Critical", value: "3" },
                    { text: "High", value: "0" },
                    { text: "Medium", value: "1" },
                    { text: "Low", value: "2" }
                ]
            });
            $("#dropdownlistRisk").data("kendoDropDownTree").value('<%= riskid%>');

            $("#dropdownlistStatus").kendoDropDownTree({
                placeholder: "Status",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "text",
                dataValueField: "value",

                change: function () {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownlistStatus', 'filterstatus', 'status');
                },
                dataSource: [
                    { text: "Overdue", value: "Overdue" },
                    { text: "Pending For Review", value: "PendingForReview" },
                    { text: "Rejected", value: "Rejected" },
                    <%if (IsNotCompiled == true)%>
                    <%{%>
                    { text: "Not Complied", value: "NotComplied" },
                    <%}%>
                     <%if (IsCompliedButDocumentPending == true)%>
                    <%{%>
                    { text: "Complied But Document Pending", value: "Complied But Document Pending" },
                    <%}%>
                    { text: "Closed Timely", value: "ClosedTimely" },
                    { text: "Closed Delayed", value: "ClosedDelayed" },
                    { text: "In Progress", value: "InProgress" }
                ]
            });

            $("#dropdownlistStatus").data("kendoDropDownTree").value(["<%=StatusID%>", "<%=StatusID1%>", "<%=StatusID2%>", "<%=StatusID3%>"]);


            $("#dropdownPastData").kendoDropDownList({
                placeholder: "Period",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    if ($("#dropdownPastData").val() != "All") {
                        $("#dropdownFY").data("kendoDropDownList").select(0);
                        fCreateStoryBoard('dropdownPastData', 'filterpstData1', 'pastdata');
                    }
                },
                index: 4,
                dataSource: [
                    { text: "Last Month", value: "1" },
                    { text: "Last Three Months", value: "3" },
                    { text: "Last Six Months", value: "6" },
                    { text: "Last Year", value: "12" },
                    { text: "All Period", value: "All" }
                ]
            });

            $("#dropdownSequence").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: false,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Label",
                change: function (e) {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownSequence', 'filterCompSubType', 'sequence');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetSequenceDetail?Flag=S&CustomerID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }
            });


            $("#dropdownACT").kendoDropDownList({
                filter: "startswith",
                autoClose: false,
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Act",
                change: function (e) {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownACT', 'filterAct', 'act');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/BindActList?UId=<% =UId%>&CId=<% =CustId%>&Flag=MGMT',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    }
                }
            });


            $("#dropdownUser").kendoDropDownTree({
                placeholder: "User",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "FullName",
                dataValueField: "UID",

                change: function () {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdownUser', 'filterUser1', 'user');

                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/KendoUserList?UserId=<% =UId%>&CustId=<% =CustId%>&Flag=<% =rolename%>',
                            dataType: "json",
                           beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                }
            });


            $("#dropdowntree").kendoDropDownTree({
                placeholder: "Entity/Sub-Entity/Location",
                checkboxes: {
                    checkChildren: true
                },
                //filter: "contains",
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                change: function (e) {
                    FilterGridDemo();
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<% =Path%>Data/GetLocationList?customerId=<% =CustId%>&userId=<% =UId%>&Flag=MGMT&IsStatutoryInternal=S',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response[0].locationList;
                        },
                        model: {
                            children: "Children"
                        }
                    }
                }
            });

        });


        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
            $("#dropdownlistRisk").data("kendoDropDownTree").value([]);
            $("#dropdownlistStatus").data("kendoDropDownTree").value([]);
            $("#dropdownUser").data("kendoDropDownTree").value([]);
            $("#dropdownACT").data("kendoDropDownList").value([]);
            if ($("#dropdownSequence").data("kendoDropDownList") != undefined) {
                $("#dropdownSequence").data("kendoDropDownList").value([]);
            }
            $("#dropdownPastData").data("kendoDropDownList").select(4);
            $("#dropdownFY").data("kendoDropDownList").select(0);
            $("#dropdownfunction").data("kendoDropDownList").value([]);
            $('#ClearfilterMain').css('display', 'none');
            $("#grid").data("kendoGrid").dataSource.filter({});
            BindGrid();
            e.preventDefault();
        }

        $("#export").kendoTooltip({
            filter: ".k-grid-edit3",
            content: function (e) {
                return "Export to Excel";
            }
        });

    </script>

</head>
<body>
    <form>

        <div class="row">
            <div class="col-lg-12 col-md-12 colpadding0">
                <h1 style="height: 30px; background-color: #f8f8f8; margin-top: 0px; font-weight: bold; font-size: 19px; color: #666; margin-bottom: 12px; padding-left: 5px; padding-top: 5px;">Imprisonment Penalty</h1>

                <h1 id="display" runat="server" style="display: none; margin-top: 2px; font-size: 20px; font-weight: 100; font-size: 17px; color: #666; font-weight: 500; margin-bottom: 17px;"></h1>
            </div>
        </div>

        <div id="example">

            <div style="margin: 0.4% 0.5% 1%;">
                <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 24%; margin-right: 0.8%;" />
                <input id="dropdownFY" style="width: 15%; margin-right: 0.8%;" />
                <input id="dropdownlistStatus" style="width: 15%; margin-right: 0.8%;" />
                <input id="dropdownlistRisk" style="width: 14%; margin-right: 0.8%;" />
                <input id="Startdatepicker" placeholder="Start Date" cssclass="clsROWgrid" title="startdatepicker" style="width: 13%; margin-right: 0.8%;" />
                <input id="Lastdatepicker" placeholder="End Date" title="enddatepicker" style="width: 13%;" />
            </div>

            <div style="margin: 0.4% 0.5% 1%;">
                <input id="dropdownACT" style="width: 24%; margin-right: 0.8%;" />
                <input id="dropdownPastData" style="width: 15%; margin-right: 0.8%;" />
                <div style="display:none;">
                <input id="dropdownfunction" style="width: 15%; margin-right: 0.8%;" />
                      <input id="dropdownUser" style="width: 14%; margin-right: 0.8%;" />
                    </div>
              
                <button id="Applyfilter" style="height: 30px;margin-right: 0.8%;" class="btn btn-primary" onclick="BindGridApply(event)"><span class="k-icon k-i-filter" onclick="javascript:return false;"></span>Apply</button>
                <button id="export" class="btn btn-primary" onclick="exportReport(event)" style="height: 30px;width: 6.6%; display:none;"><span class="k-icon k-i-excel k-grid-edit3"></span>Export</button>
                <button id="ClearfilterMain" style="margin-right: 4px;height: 32px;float: right;display: none;" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
            </div>

            <input id="CustName" type="hidden" value="<% =CustomerName%>" />
            <input id="DisplayDepartment" type="hidden" value="<% =Departmentid%>" />
            <input id="IsLabel" type="hidden" value="<% =com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable%>" />
      
        </div>
        <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsLabelApplicable == 1)%>
        <%{%>
        <div style="margin: 0.4% 0.5% 1%;">
            <input id="dropdownSequence" style="width: 24%; margin-right: 0.8%;" />
        </div>
        <%}%>
        <div class="clearfix"></div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filtersstoryboard">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold;" id="filtertype">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; margin-top: 1px;" id="filterrisk">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; margin-top: -1px;" id="filterstatus">&nbsp;</div>
        <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; font-weight: bold; color: #535b6a; margin-top: -1px;" id="filterUser1">&nbsp;</div>
        <div id="grid"></div>

    </form>
</body>
</html>


