﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class GradingDisplay : System.Web.UI.Page
    {
        protected static int customerid;
        protected static string ComplianceTypeFlag;
        protected static string Type;
        protected static DateTime  sdate;
        protected static DateTime edate;
        protected static int Customerbanchid;
        protected static string perGradingRiskChart;
        protected static bool IsApprover = false;
        protected List<Int32> roles;
        protected bool DeptHead = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    //Graph.Visible = false;
                    //Details.Visible = true ;
                    BindDetailView();
                    BindUserColors();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
               
            }           
        }

        private void BindUserColors()
        {
            try
            {
                var Cmd = Business.ComplianceManagement.Getcolor(AuthenticationHelper.UserID);

                if (Cmd != null)
                {
                    highcolor.Value = Cmd.High;
                    mediumcolor.Value = Cmd.Medium;
                    lowcolor.Value = Cmd.Low;
                    criticalcolor.Value = Cmd.Critical;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
           
        }
        private void BindDetailView()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                {
                    customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["type"]))
                {
                    Type = Request.QueryString["type"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["StartDate"]))
                {
                    sdate = Convert.ToDateTime(Request.QueryString["StartDate"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["EndDate"]))
                {
                    edate = Convert.ToDateTime(Request.QueryString["EndDate"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Customerbanchid"]))
                {
                    Customerbanchid = Convert.ToInt32(Request.QueryString["Customerbanchid"]);
                }               
                if (Session["User_comp_Roles"] != null)
                {
                    roles = Session["User_comp_Roles"] as List<int>;
                }
                else
                {
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                    Session["User_comp_Roles"] = roles;
                }
                if (Session["User_dept"] != null)
                {
                    DeptHead = (bool)Session["User_dept"];
                }
                else
                {
                    User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                    if (!string.IsNullOrEmpty(Convert.ToString(LoggedUser.IsHead)))
                    {
                        DeptHead = (bool)LoggedUser.IsHead;
                    }
                }
                if (AuthenticationHelper.Role.Equals("MGMT"))
                {
                    IsApprover = false;
                }
                else if (DeptHead)
                {
                    IsApprover = false;
                }
                else
                {
                    if (roles.Contains(6))
                    {
                        IsApprover = true;
                    }
                    else
                    {
                        IsApprover = false;
                    }
                }
                //if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                //{
                //    IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                //}
                if (ComplianceTypeFlag == "Statutory")
                {
                    if (Convert.ToString(Request.QueryString["IsDeptHead"]) == "1")
                    {
                        var detailsview = GetGradingComplianceDetailsDashboard_DeptHeadNew(customerid, Customerbanchid, sdate, edate, AuthenticationHelper.UserID, IsApprover);
                        Session["TotalRows"] = detailsview.Count;

                        GridStatutory.DataSource = detailsview;
                        GridStatutory.DataBind();

                        GridStatutory.Visible = true;
                        GridInternalCompliance.Visible = false;
                    }
                    else
                    {
                        var detailsview = GetGradingComplianceDetailsDashboard(customerid, Customerbanchid, sdate, edate, AuthenticationHelper.UserID, IsApprover);
                        Session["TotalRows"] = detailsview.Count;

                        GridStatutory.DataSource = detailsview;
                        GridStatutory.DataBind();

                        GridStatutory.Visible = true;
                        GridInternalCompliance.Visible = false;
                    }
                }
                else if (ComplianceTypeFlag == "Internal")
                {
                    if (Convert.ToString(Request.QueryString["IsDeptHead"]) == "1")
                    {
                        var detailsview = GetGradingInternalComplianceDetailsDashboard_DeptHeadNew(customerid, Customerbanchid, sdate, edate, AuthenticationHelper.UserID, IsApprover);

                        Session["TotalRows"] = detailsview.Count;

                        GridInternalCompliance.DataSource = detailsview;
                        GridInternalCompliance.DataBind();

                        GridStatutory.Visible = false;
                        GridInternalCompliance.Visible = true;
                    }
                    else
                    {
                        var detailsview = GetGradingInternalComplianceDetailsDashboard(customerid, Customerbanchid, sdate, edate, AuthenticationHelper.UserID, IsApprover);

                        Session["TotalRows"] = detailsview.Count;

                        GridInternalCompliance.DataSource = detailsview;
                        GridInternalCompliance.DataBind();

                        GridStatutory.Visible = false;
                        GridInternalCompliance.Visible = true;
                    }
                }
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ShowGraph(object sender, EventArgs e)
        {
            try
            {
                liDetails.Attributes.Add("class", "");
                liGraph.Attributes.Add("class", "active");
                Graph.Style.Add("display", "block");
                Details.Style.Add("display", "none");
                //Graph.Visible = true;
                //Details.Visible = false;
                upDocumentDownload.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ShowDetails(object sender, EventArgs e)
        {
            try
            {
                liGraph.Attributes.Add("class", "");
                liDetails.Attributes.Add("class", "active");
                Details.Style.Add("display", "block");
                Graph.Style.Add("display", "none");
                //Details.Visible = true;
                //Graph.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
                
        }

       
        public static List<ComplianceInstanceTransactionView> GetGradingComplianceDetailsDashboard(int customerid, int customerbranchid, DateTime sdate, DateTime edate, int userId, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime startDate = new DateTime(sdate.Year, sdate.Month, sdate.Day);
                DateTime EndDate = new DateTime(edate.Year, edate.Month, edate.Day);

                List<ComplianceInstanceTransactionView> transactionsQuery = new List<ComplianceInstanceTransactionView>();
                if (approver == true)
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         where row.CustomerID == customerid
                                         && row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         && row.RoleID == 6 && row.UserID == userId
                                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.ComplianceInstanceTransactionViews
                                         join row2 in entities.EntitiesAssignments
                                         on row.CustomerBranchID equals row2.BranchID
                                         where row.CustomerID == customerid && row2.UserID == userId
                                         && row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                         && (row.IsActive != false || row.IsUpcomingNotDeleted != false)
                                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                if (customerbranchid !=-1)
                {
                    entities.Database.CommandTimeout = 180;
                    var branchIDs = (from row in entities.SP_RLCS_GetChildBranchesFromParentBranch((int)customerbranchid, (int)customerid)
                                     select row).ToList();
                    transactionsQuery = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID)).ToList();                    
                }               
                //High
                long AfterDueDatecountHigh;
                long CompletedCountHigh;
                long NotCompletedcountHigh;
                AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9) ).Count();
                CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();
                NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)).Count();

                //Medium
                long AfterDueDatecountMedium;
                long CompletedCountMedium;
                long NotCompletedcountMedium;
                AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();
                CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();
                NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)).Count();


                //Low
                long AfterDueDatecountLow;
                long CompletedCountLow;
                long NotCompletedcountLow;

                AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();
                CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();
                NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)).Count();


                //Critical
                long AfterDueDatecountCritical;
                long CompletedCountCritical;
                long NotCompletedcountCritical;

                AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();
                CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();
                NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)).Count();


                perGradingRiskChart = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +

             // Not Completed - Critical

             "y: " + NotCompletedcountCritical + ",events:{click: function(e) {" +
              // " fpopulateddata('Low','Not completed'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +
              "}}},{  " +

             // Not Completed - High
             "y: " + NotCompletedcountHigh + ",events:{click: function(e) {" +
              //" fpopulateddata('High','Not completed'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{" +

             // Not Completed - Medium
             " y: " + NotCompletedcountMedium + ",events:{click: function(e) {" +
             //" fpopulateddata('Medium','Not completed'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{  " +

             // Not Completed - Low

             "y: " + NotCompletedcountLow + ",events:{click: function(e) {" +
              // " fpopulateddata('Low','Not completed'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

              
             "}}}]},{name: 'After Due Date',color: perRiskStackedColumnChartColorScheme.medium,data: [{" +
             
             // After Due Date - Critical
             "y: " + AfterDueDatecountCritical + ",events:{click: function(e) {" +
              // " fpopulateddata('Low','After due date'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

              "}}},{   " +

             // After Due Date - High

             "y: " + AfterDueDatecountHigh + ",events:{click: function(e) { " +
             //" fpopulateddata('High','After due date'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{   " +

             // After Due Date - Medium
             "y: " + AfterDueDatecountMedium + ",events:{click: function(e) {" +
             //" fpopulateddata('Medium','After due date'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{   " +
             // After Due Date - Low
             "y: " + AfterDueDatecountLow + ",events:{click: function(e) {" +
             // " fpopulateddata('Low','After due date'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             
            
             "}}}]},{name: 'In Time',color: perRiskStackedColumnChartColorScheme.low,data: [{" +
                          
             // In Time - Critical
             "y: " + CompletedCountCritical + ",events:{click: function(e) {" +
             //" fpopulateddata('Low','In Time'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +
             "}}},{  " +

             // In Time - High
             "y: " + CompletedCountHigh + ",events:{click: function(e) {" +
             //" fpopulateddata('High','In Time'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{  " +
             // In Time - Medium
             "y: " + CompletedCountMedium + ",events:{click: function(e) {" +
             //" fpopulateddata('Medium','In Time'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{  " +
             // In Time - Low
             "y: " + CompletedCountLow + ",events:{click: function(e) {" +
             //" fpopulateddata('Low','In Time'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +




             "}}}]}]";

                return transactionsQuery;
            }
        }

        public static List<Sp_DeptHead_ComplianceInstanceTransactionView_Result> GetGradingComplianceDetailsDashboard_DeptHeadNew(int customerid, int customerbranchid, DateTime sdate, DateTime edate, int userId, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime startDate = new DateTime(sdate.Year, sdate.Month, sdate.Day);
                DateTime EndDate = new DateTime(edate.Year, edate.Month, edate.Day);

                List<Sp_DeptHead_ComplianceInstanceTransactionView_Result> transactionsQuery = new List<Sp_DeptHead_ComplianceInstanceTransactionView_Result>();
                if (approver == true)
                {
                    transactionsQuery = (from row in entities.Sp_DeptHead_ComplianceInstanceTransactionView(userId, customerid, "APPR")
                                         where row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate                                        
                                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                else
                {
                    transactionsQuery = (from row in entities.Sp_DeptHead_ComplianceInstanceTransactionView(userId, customerid, "DEPT")
                                         where row.ScheduledOn >= startDate && row.ScheduledOn <= EndDate
                                         select row).GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                   
                }
                if (customerbranchid != -1)
                {
                    entities.Database.CommandTimeout = 180;
                    var branchIDs = (from row in entities.SP_RLCS_GetChildBranchesFromParentBranch((int)customerbranchid, (int)customerid)
                                     select row).ToList();
                    transactionsQuery = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID)).ToList();                    
                }
                //High
                long AfterDueDatecountHigh;
                long CompletedCountHigh;
                long NotCompletedcountHigh;
                AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();
                CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();
                NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)).Count();

                //Medium
                long AfterDueDatecountMedium;
                long CompletedCountMedium;
                long NotCompletedcountMedium;
                AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();
                CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();
                NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)).Count();


                //Low
                long AfterDueDatecountLow;
                long CompletedCountLow;
                long NotCompletedcountLow;

                AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();
                CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();
                NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)).Count();

                //Critical
                long AfterDueDatecountCritical;
                long CompletedCountCritical;
                long NotCompletedcountCritical;

                AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 5 || entry.ComplianceStatusID == 9)).Count();
                CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 4 || entry.ComplianceStatusID == 7)).Count();
                NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 6 || entry.ComplianceStatusID == 8 || entry.ComplianceStatusID == 10)).Count();


                perGradingRiskChart = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +
                    

             // Not Completed - Critical

             "y: " + NotCompletedcountCritical + ",events:{click: function(e) {" +
              // " fpopulateddata('Critical','Not completed'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +
              "}}},{  " +
             // Not Completed - High
             "y: " + NotCompletedcountHigh + ",events:{click: function(e) {" +
             //" fpopulateddata('High','Not completed'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{" +

             // Not Completed - Medium
             " y: " + NotCompletedcountMedium + ",events:{click: function(e) {" +
             //" fpopulateddata('Medium','Not completed'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{  " +

             // Not Completed - Low

             "y: " + NotCompletedcountLow + ",events:{click: function(e) {" +
              // " fpopulateddata('Low','Not completed'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             

             "}}}]},{name: 'After Due Date',color: perRiskStackedColumnChartColorScheme.medium,data: [{" +
            
             // After Due Date - Critical
             "y: " + AfterDueDatecountCritical + ",events:{click: function(e) {" +
              // " fpopulateddata('Critical','After due date'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +
              "}}},{   " +
             // After Due Date - High

             "y: " + AfterDueDatecountHigh + ",events:{click: function(e) { " +
             //" fpopulateddata('High','After due date'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{   " +

             // After Due Date - Medium
             "y: " + AfterDueDatecountMedium + ",events:{click: function(e) {" +
             //" fpopulateddata('Medium','After due date'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{   " +
             // After Due Date - Low
             "y: " + AfterDueDatecountLow + ",events:{click: function(e) {" +
              // " fpopulateddata('Low','After due date'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

              

             "}}}]},{name: 'In Time',color: perRiskStackedColumnChartColorScheme.low,data: [{" +
            
             // In Time - Critical
             "y: " + CompletedCountCritical + ",events:{click: function(e) {" +
               //" fpopulateddata('Critical','In Time'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +
               "}}},{  " +
             // In Time - High
             "y: " + CompletedCountHigh + ",events:{click: function(e) {" +
             //" fpopulateddata('High','In Time'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{  " +
             // In Time - Medium
             "y: " + CompletedCountMedium + ",events:{click: function(e) {" +
             //" fpopulateddata('Medium','In Time'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{  " +
             // In Time - Low
             "y: " + CompletedCountLow + ",events:{click: function(e) {" +
             //" fpopulateddata('Low','In Time'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

            
             "}}}]}]";

                return transactionsQuery;
            }
        }
        public static List<InternalComplianceInstanceTransactionView> GetGradingInternalComplianceDetailsDashboard(int customerid, int customerbranchid, DateTime sdate, DateTime edate, int userId, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime startDate = new DateTime(sdate.Year, sdate.Month, sdate.Day);
                DateTime EndDate = new DateTime(edate.Year, edate.Month, edate.Day);
                List<InternalComplianceInstanceTransactionView> transactionsQuery = new List<InternalComplianceInstanceTransactionView>();
                if (approver == true)
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         where row.CustomerID == customerid
                                         && row.InternalScheduledOn >= startDate && row.InternalScheduledOn <= EndDate
                                         && row.RoleID == 6 && row.UserID == userId
                                         select row).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                else
                {
                    transactionsQuery = (from row in entities.InternalComplianceInstanceTransactionViews
                                         join row2 in entities.EntitiesAssignmentInternals
                                         on (long)row.CustomerBranchID equals row2.BranchID
                                         where row.CustomerID == customerid && row2.UserID == userId
                                         && row.InternalScheduledOn >= startDate && row.InternalScheduledOn <= EndDate
                                         select row).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                if (customerbranchid != -1)
                {
                    // transactionsQuery = transactionsQuery.Where(entry => entry.CustomerBranchID == customerbranchid).ToList();                    

                    entities.Database.CommandTimeout = 180;
                    var branchIDs = (from row in entities.SP_RLCS_GetChildBranchesFromParentBranch((int)customerbranchid, (int)customerid)
                                     select row).ToList();
                    transactionsQuery = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID)).ToList();

                }
                
                //High
                long AfterDueDatecountHigh;
                long CompletedCountHigh;
                long NotCompletedcountHigh;
                AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9)).Count();
                CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7)).Count();
                NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10)).Count();

                //Medium
                long AfterDueDatecountMedium;
                long CompletedCountMedium;
                long NotCompletedcountMedium;
                AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9)).Count();
                CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7)).Count();
                NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10)).Count();


                //Low
                long AfterDueDatecountLow;
                long CompletedCountLow;
                long NotCompletedcountLow;

                AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9)).Count();
                CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7)).Count();
                NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10)).Count();

                //Critical
                long AfterDueDatecountCritical;
                long CompletedCountCritical;
                long NotCompletedcountCritical;

                AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9)).Count();
                CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7)).Count();
                NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10)).Count();


                perGradingRiskChart = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +
             // Not Completed - Critical

             "y: " + NotCompletedcountCritical + ",events:{click: function(e) {" +
              // " fpopulateddata('Critical','Not completed'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +
              "}}},{  " +
             // Not Completed - High
             "y: " + NotCompletedcountHigh + ",events:{click: function(e) {" +
             //" fpopulateddata('High','Not completed'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{" +

             // Not Completed - Medium
             " y: " + NotCompletedcountMedium + ",events:{click: function(e) {" +
             //" fpopulateddata('Medium','Not completed'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{  " +

             // Not Completed - Low

             "y: " + NotCompletedcountLow + ",events:{click: function(e) {" +
              // " fpopulateddata('Low','Not completed'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             

            

             "}}}]},{name: 'After Due Date',color: perRiskStackedColumnChartColorScheme.medium,data: [{" +
               
             // After Due Date - Critical
             "y: " + AfterDueDatecountCritical + ",events:{click: function(e) {" +
              // " fpopulateddata('Critical','After due date'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

              "}}},{   " +

             // After Due Date - High

             "y: " + AfterDueDatecountHigh + ",events:{click: function(e) { " +
             //" fpopulateddata('High','After due date'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{   " +

             // After Due Date - Medium
             "y: " + AfterDueDatecountMedium + ",events:{click: function(e) {" +
             //" fpopulateddata('Medium','After due date'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{   " +
             // After Due Date - Low
             "y: " + AfterDueDatecountLow + ",events:{click: function(e) {" +
              // " fpopulateddata('Low','After due date'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

           

             "}}}]},{name: 'In Time',color: perRiskStackedColumnChartColorScheme.low,data: [{" +
             
             // In Time - Critical
             "y: " + CompletedCountCritical + ",events:{click: function(e) {" +
              //" fpopulateddata('Critical','In Time'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +
              "}}},{  " +
             // In Time - High
             "y: " + CompletedCountHigh + ",events:{click: function(e) {" +
             //" fpopulateddata('High','In Time'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{  " +
             // In Time - Medium
             "y: " + CompletedCountMedium + ",events:{click: function(e) {" +
             //" fpopulateddata('Medium','In Time'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{  " +
             // In Time - Low
             "y: " + CompletedCountLow + ",events:{click: function(e) {" +
             //" fpopulateddata('Low','In Time'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +


            

             "}}}]}]";
                return transactionsQuery;
            }
        }


        public static List<Sp_DeptHead_InternalComplianceInstanceTransactionView_Result> GetGradingInternalComplianceDetailsDashboard_DeptHeadNew(int customerid, int customerbranchid, DateTime sdate, DateTime edate, int userId, bool approver = false)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime startDate = new DateTime(sdate.Year, sdate.Month, sdate.Day);
                DateTime EndDate = new DateTime(edate.Year, edate.Month, edate.Day);
                List<Sp_DeptHead_InternalComplianceInstanceTransactionView_Result> transactionsQuery = new List<Sp_DeptHead_InternalComplianceInstanceTransactionView_Result>();                
                if (approver == true)
                {
                    transactionsQuery = (from row in entities.Sp_DeptHead_InternalComplianceInstanceTransactionView(userId, customerid, "APPR")
                                         where row.InternalScheduledOn >= startDate && row.InternalScheduledOn <= EndDate                                         
                                         select row).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                }
                else
                {
                    transactionsQuery = (from row in entities.Sp_DeptHead_InternalComplianceInstanceTransactionView(userId, customerid, "DEPT")                                        
                                         where row.InternalScheduledOn >= startDate && row.InternalScheduledOn <= EndDate
                                         select row).GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                if (customerbranchid != -1)
                {
                    entities.Database.CommandTimeout = 180;
                    var branchIDs = (from row in entities.SP_RLCS_GetChildBranchesFromParentBranch((int)customerbranchid, (int)customerid)
                                     select row).ToList();
                    transactionsQuery = transactionsQuery.Where(entry => branchIDs.Contains((int)entry.CustomerBranchID)).ToList();                    
                }

                //High
                long AfterDueDatecountHigh;
                long CompletedCountHigh;
                long NotCompletedcountHigh;
                AfterDueDatecountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9)).Count();
                CompletedCountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7)).Count();
                NotCompletedcountHigh = transactionsQuery.Where(entry => entry.Risk == 0 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10)).Count();

                //Medium
                long AfterDueDatecountMedium;
                long CompletedCountMedium;
                long NotCompletedcountMedium;
                AfterDueDatecountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9)).Count();
                CompletedCountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7)).Count();
                NotCompletedcountMedium = transactionsQuery.Where(entry => entry.Risk == 1 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10)).Count();


                //Low
                long AfterDueDatecountLow;
                long CompletedCountLow;
                long NotCompletedcountLow;

                AfterDueDatecountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9)).Count();
                CompletedCountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7)).Count();
                NotCompletedcountLow = transactionsQuery.Where(entry => entry.Risk == 2 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10)).Count();

                //Critical
                long AfterDueDatecountCritical;
                long CompletedCountCritical;
                long NotCompletedcountCritical;

                AfterDueDatecountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.InternalComplianceStatusID == 5 || entry.InternalComplianceStatusID == 9)).Count();
                CompletedCountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.InternalComplianceStatusID == 4 || entry.InternalComplianceStatusID == 7)).Count();
                NotCompletedcountCritical = transactionsQuery.Where(entry => entry.Risk == 3 && (entry.InternalComplianceStatusID == 1 || entry.InternalComplianceStatusID == 2 || entry.InternalComplianceStatusID == 3 || entry.InternalComplianceStatusID == 6 || entry.InternalComplianceStatusID == 8 || entry.InternalComplianceStatusID == 10)).Count();


                perGradingRiskChart = "series: [{name: 'Not Completed',color: perRiskStackedColumnChartColorScheme.high,data: [{" +
                    

             // Not Completed - Critical

             "y: " + NotCompletedcountCritical + ",events:{click: function(e) {" +
              // " fpopulateddata('Critical','Not completed'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

              "}}},{  " +

             // Not Completed - High
             "y: " + NotCompletedcountHigh + ",events:{click: function(e) {" +
             //" fpopulateddata('High','Not completed'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{" +

             // Not Completed - Medium
             " y: " + NotCompletedcountMedium + ",events:{click: function(e) {" +
             //" fpopulateddata('Medium','Not completed'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{  " +

             // Not Completed - Low

             "y: " + NotCompletedcountLow + ",events:{click: function(e) {" +
              // " fpopulateddata('Low','Not completed'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             

             "}}}]},{name: 'After Due Date',color: perRiskStackedColumnChartColorScheme.medium,data: [{" +
             
             // After Due Date - Critical
             "y: " + AfterDueDatecountCritical + ",events:{click: function(e) {" +
              // " fpopulateddata('Critical','After due date'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

              "}}},{   " +

             // After Due Date - High

             "y: " + AfterDueDatecountHigh + ",events:{click: function(e) { " +
             //" fpopulateddata('High','After due date'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{   " +

             // After Due Date - Medium
             "y: " + AfterDueDatecountMedium + ",events:{click: function(e) {" +
             //" fpopulateddata('Medium','After due date'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{   " +
             // After Due Date - Low
             "y: " + AfterDueDatecountLow + ",events:{click: function(e) {" +
             // " fpopulateddata('Low','After due date'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

            

             "}}}]},{name: 'In Time',color: perRiskStackedColumnChartColorScheme.low,data: [{" +
             
             // In Time - Critical
             "y: " + CompletedCountCritical + ",events:{click: function(e) {" +
              //" fpopulateddata('Critical','In Time'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +
              "}}},{  " +
             // In Time - High
             "y: " + CompletedCountHigh + ",events:{click: function(e) {" +
             //" fpopulateddata('High','In Time'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{  " +
             // In Time - Medium
             "y: " + CompletedCountMedium + ",events:{click: function(e) {" +
             //" fpopulateddata('Medium','In Time'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             "}}},{  " +
             // In Time - Low
             "y: " + CompletedCountLow + ",events:{click: function(e) {" +
              //" fpopulateddata('Low','In Time'," + customerid + "," + customerbranchid + "," + edate.Year + "," + edate.Month + ",'Function','0','Statutory','RiskBARchart')" +

             

             "}}}]}]";
                return transactionsQuery;
            }
        }

    
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                if (ComplianceTypeFlag == "Statutory")
                {
                    GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (ComplianceTypeFlag == "Internal")
                {
                    GridInternalCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid

                BindDetailView();


                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "")
                        SelectedPageNo.Text = "1";

                    if (SelectedPageNo.Text == "0")
                        SelectedPageNo.Text = "1";
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (ComplianceTypeFlag == "Statutory")
                {
                    GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (ComplianceTypeFlag == "Internal")
                {
                    GridInternalCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid

                BindDetailView();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                if (ComplianceTypeFlag == "Statutory")
                {
                    GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (ComplianceTypeFlag == "Internal")
                {
                    GridInternalCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid

                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        protected void GridInternalCompliance_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledOn");
                    if (lblStatus != null)
                    {
                        if (lblScheduledOn != null)
                        {
                            DateTime CurrentDate = DateTime.Today.Date;
                            if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date > CurrentDate)
                                lblStatus.Text = "Upcoming";
                            else if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date <= CurrentDate)
                                lblStatus.Text = "Overdue";
                            else if (lblStatus.Text.Trim() == "Complied but pending review")
                                lblStatus.Text = "Pending For Review";
                            else if (lblStatus.Text.Trim() == "Complied Delayed but pending review")
                                lblStatus.Text = "Pending For Review";
                            else if (lblStatus.Text.Trim() == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date > CurrentDate)
                                lblStatus.Text = "Upcoming";
                            else if (lblStatus.Text.Trim() == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date <= CurrentDate)
                                lblStatus.Text = "Overdue";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);              
            }
        }

        protected void GridStatutory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {                   
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledOn");
                    if (lblStatus != null)
                    {
                        if (lblScheduledOn != null)
                        {
                            DateTime CurrentDate = DateTime.Today.Date;
                            if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date > CurrentDate)
                                lblStatus.Text = "Upcoming";
                            else if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date <= CurrentDate)
                                lblStatus.Text = "Overdue";
                            else if (lblStatus.Text.Trim() == "Complied but pending review")
                                lblStatus.Text = "Pending For Review";
                            else if (lblStatus.Text.Trim() == "Complied Delayed but pending review")
                                lblStatus.Text = "Pending For Review";
                            else if (lblStatus.Text.Trim() == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date > CurrentDate)
                                lblStatus.Text = "Upcoming";
                            else if (lblStatus.Text.Trim() == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date <= CurrentDate)
                                lblStatus.Text = "Overdue";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
              
            }
        }
    }
}