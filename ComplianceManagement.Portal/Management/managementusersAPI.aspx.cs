﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Management
{
    public partial class managementusersAPI : System.Web.UI.Page
    {
      
        protected  int UId;
        protected  int CustId;
        protected  int BID;
        protected  int dhead;
        protected  int catid;
        protected  int isapprover;
        protected  int statutotyinternal;
        protected  int FlagID;      
        protected  string Path;       
        protected  string Internalsatutory;
        protected static string Authorization;

        protected void Page_Load(object sender, EventArgs e)
        {
            int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
            string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
            Authorization = (string)HttpContext.Current.Cache[CacheName];
            if (Authorization == null)
            {
                Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
            }

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                if (AuthenticationHelper.Role.Equals("MGMT") || AuthenticationHelper.ComplianceProductType == 3)
                {
                    isapprover = 0;
                }
                else
                {
                    var GetApprover = (entities.Sp_GetApproverUsers(AuthenticationHelper.UserID)).ToList();
                    if (GetApprover.Count > 0)
                    {
                        isapprover = 1;
                    }
                }
                dhead = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["IsDeptHead"]))
                {
                    dhead = Convert.ToInt32(Request.QueryString["IsDeptHead"]);
                }
                if (AuthenticationHelper.Role == "MGMT" || AuthenticationHelper.Role == "AUDT" || isapprover==1 || dhead==1 || AuthenticationHelper.ComplianceProductType == 3)
                {

                    Path = ConfigurationManager.AppSettings["KendoPathApp"];
                    UId = Convert.ToInt32(AuthenticationHelper.UserID);
                    CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    FlagID = 0;
                    if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                    {
                        Internalsatutory = Request.QueryString["Internalsatutory"];
                        if (Internalsatutory == "Statutory" || Internalsatutory == "StatutoryAll")
                        {
                            FlagID = 0;
                        }
                        else
                        {
                            FlagID = 1;
                        }
                    }
                    BID = 0;
                    catid = 0;
                    if (!string.IsNullOrEmpty(Request.QueryString["Branchid"]))
                    {
                        BID = Convert.ToInt32(Request.QueryString["Branchid"]);
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["Category"]))
                    {
                        catid = Convert.ToInt32(Request.QueryString["Category"]);
                    }

                }
            }
        }
    }
}