﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="managementusers.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Management.managementusers" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="../NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <%--   <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-ui-1.10.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-ui-1.9.2.custom.min.js"></script>--%>
    <script type="text/javascript" src="../Newjs/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="../Newjs/bootstrap.min.js"></script>
    <style type="text/css">


        .ui-widget-header {
            border: 0px !important;
            background: inherit;
            font-size: 20px;
            color: #666666;
            font-weight: normal;
            padding-top: 0px;
            margin-top: 5px;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

            .table > tbody > tr > th > a {
                vertical-align: bottom;
                color: #666666 !important;
                text-decoration: none !important;
                border-bottom: 2px solid #dddddd;
            }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script type="text/javascript">

        $(window).resize(function(){
            window.parent.forchild($("body").height()+50);  
        });
        
         


        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });

        }
      
    </script>


</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
            <ContentTemplate>
                <asp:UpdateProgress ID="updateProgress" runat="server">
                    <ProgressTemplate>
                        <div id="updateProgress1" style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                                AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <div id="divSummaryDetails">
                    <div class="col-md-12 colpadding0">
                        <div class="col-md-2 colpadding0 entrycount">
                            <div class="col-md-3 colpadding0">
                                <p style="color: #999; margin-top: 5px;">Show </p>
                            </div>
                            <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                <asp:ListItem Text="10" Selected="True" />
                                <asp:ListItem Text="20" />
                                <asp:ListItem Text="50" />
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-4 colpadding0 entrycount">
                        </div>
                        <div class="col-md-2 colpadding0 entrycount">
                        </div>
                        <div class="col-md-4 colpadding0 entrycount" runat="server" id="DivRecordsScrum" style="float: right;">
                            <p style="padding-right: 0px !Important;">
                                <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                    <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                    <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>

                    </div>
                    <div class="clearfix"></div>


                    <asp:GridView runat="server" ID="grdSummaryDetails" AutoGenerateColumns="false" GridLines="none" AllowSorting="true"
                        CellPadding="4" CssClass="table" AllowPaging="true" PageSize="10" Width="100%">
                        <Columns>

                            <asp:TemplateField HeaderText="Sr">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblUserName" runat="server" Text='<%#GetUser((long)Eval("ID"))%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Role">
                                <ItemTemplate>
                                    <asp:Label ID="lblRole" runat="server" Text='<%# GetRole((long)Eval("ID")) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Location">
                                <ItemTemplate>
                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                        <asp:Label ID="lblBranch" runat="server" data-toggle="tooltip" data-placement="bottom"
                                            Text='<%# Eval("ID")!=null?GetCustomerBranch((long)Eval("ID")):"" %>'
                                            ToolTip='<%# Eval("ID")!=null?GetCustomerBranch((long)Eval("ID")):"" %>'>
                                        </asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Email">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Contact No">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("ContactNumber") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <RowStyle CssClass="clsROWgrid" />
                        <HeaderStyle CssClass="clsheadergrid" />
                        <PagerTemplate>
                            <table style="display: none">
                                <tr>
                                    <td>
                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </PagerTemplate>

                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>

                    <div class="clearfix"></div>

                    <div class="col-md-12 colpadding0">
                        <div class="col-md-6 colpadding0">
                            <div class="table-Selecteddownload">
                            </div>
                        </div>

                        <div class="col-md-6 colpadding0">
                            <div class="table-paging" style="margin-bottom: 20px;">
                                <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                                <div class="table-paging-text">
                                    <p>
                                        <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>
                                        /
                                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                    </p>
                                </div>

                                <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                            </div>
                        </div>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
         <script>
             $('#updateProgress1').show();
             $(document).ready(function () {
                 $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
                 $('#updateProgress1').hide();
             });
         </script>
    </form>
</body>
</html>
