﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SAMLAuth.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.SAMLAuth" %>

<!DOCTYPE html>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<%@ Register Assembly="GoogleReCaptcha" Namespace="GoogleReCaptcha" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="AVANTIS - Development Team" />

    <title>Login :: AVANTIS - Products that simplify</title>
    <!-- Bootstrap CSS -->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="https://avacdn.azureedge.net/newcss/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="https://avacdn.azureedge.net/newcss/elegant-icons-style.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="https://avacdn.azureedge.net/newcss/style.css" rel="stylesheet" />
    <link href="https://avacdn.azureedge.net/newcss/style-responsive.css" rel="stylesheet" />
    <style type="text/css">
        .otpdiv > span > label {
            border: 0px;
            color: black;
            /* margin-bottom: 5px; */
            margin-left: -17px;
            margin-top: 2px;
            position: absolute;
            width: 111px;
        }

        .otpdiv > span {
            width: 233px;
            height: 26px;
            margin: auto;
            position: relative;
            /* background: #fff; */
            /* border: 1px solid #2e2e2e; */
            border-radius: 2px;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
        }

            .otpdiv > span > input {
                width: 80px;
                height: 26px;
                margin: auto;
                position: relative;
                border-radius: 2px;
                -webkit-border-radius: 2px;
                -moz-border-radius: 2px;
                margin-left: -9px;
            }
    </style>
    <script type="text/javascript">

        var _paq = window._paq || [];
        /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function () {
            var u = "//analytics.avantis.co.in/";
            _paq.push(['setTrackerUrl', u + 'matomo.php']);
            _paq.push(['setSiteId', '1']);
            var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
            g.type = 'text/javascript'; g.async = true; g.defer = true; g.src = u + 'matomo.js'; s.parentNode.insertBefore(g, s);
        })();

        function settracknew(e, t, n, r) {
            debugger;
            try {
                _paq.push(['trackEvent', e, t, n])
            } catch (t) { } return !0
        }

        function settracknewForSuccess() {

            settracknew('Login', 'Login', 'Success', '');
        }
        function settracknewForFailed() {
            settracknew('Login', 'Login', 'Failed', '');
        }
        function settracknewRememberUnchecked() {
            settracknew('Login', 'Login', 'RememberUnchecked', '');
        }
        function settracknewRememberchecked() {
            settracknew('Login', 'Login', 'Rememberchecked', '');
        }
    </script>

</head>
<body>
    <div class="container">
        <%--<form runat="server" class="login-form" name="login" id="loginForm" autocomplete="off">--%>
        <form runat="server" class="login-form" name="login">
            <asp:Panel ID="Panel2" runat="server" DefaultButton="Submit">

                <asp:ScriptManager ID="ScriptManager2" runat="server" />
                <cc2:NoBot ID="PageNoBot" runat="server" Enabled="true" ResponseMinimumDelaySeconds="5" />
                <%--CutoffMaximumInstances="1" CutoffWindowSeconds="5"--%>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="col-md-12 login-form-head">
                            <p class="login-img">
                                <a href="https://teamleaseregtech.com">
                                    <img src="Images/TeamLease1.png" /></a>
                            </p>
                        </div>

                        <div class="login-wrap">
                            <div id="divLogin" class="row" runat="server">
                                <h1>Sign in</h1>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon_profile"></i></span>
                                    <asp:TextBox ID="txtemail" CssClass="form-control" runat="server" placeholder="Username" data-toggle="tooltip" MaxLength="100" data-placement="right" ToolTip="Please enter Username" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="None" ErrorMessage="Username cannot be empty."
                                        ControlToValidate="txtemail" runat="server" />
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon_lock"></i></span>
                                    <asp:TextBox ID="txtpass" runat="server" CssClass="form-control" TextMode="Password" placeholder="Password" MaxLength="50" data-toggle="tooltip" data-placement="right" ToolTip="Please enter Password" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="None" ErrorMessage="Password cannot be empty."
                                        ControlToValidate="txtpass" runat="server" />
                                </div>
                                <div style="float: left;">
                                    <asp:CheckBox ID="cbRememberMe" Checked="true" CssClass="pull-left" Style="color: #688a7e !important; margin-bottom: 0px;" Text="&nbsp;Remember me" runat="server" Visible="false" />
                                </div>
                                <div class="clearfix"></div>
                                <div style="float: left;">
                                    <asp:LinkButton ID="lbtResetPassword" CausesValidation="false" UseSubmitBehavior="false" runat="server"
                                        Text="Forgot your Password?" CssClass="pull-left"></asp:LinkButton>
                                </div>

                                <br />

                                <div style="float: left;">
                                    <asp:LinkButton ID="lbtUnlockAccount" CausesValidation="false" UseSubmitBehavior="false" runat="server" data-toggle="tooltip" data-placement="right" ToolTip="If your account got locked, click here to unlock it."
                                        Text="Account Locked?" CssClass="pull-left" Visible="true"></asp:LinkButton>
                                </div>

                                <div class="clearfix"></div>

                                <div style="margin-top: 15px;">
                                    <asp:Button ID="Submit" CssClass="btn btn-primary btn-lg btn-block" Text="Sign in" runat="server"></asp:Button>
                                </div>

                                <div style="margin-top: 15px;">
                                    <asp:Button ID="btnAD" Visible="false" CssClass="btn btn-primary btn-lg btn-block" Text="Sign in with AD" runat="server"></asp:Button>
                                    <div style="border: 1px solid #e0e0e0; border-radius: 3px; cursor: pointer; display: inline-block; padding: 6px 18px;" onclick="socaillogin('https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=http%3A%2F%2Flocalhost:18156%2Fexternalogin.aspx&client_id=551609060837-4kvqjmskko7kkiqn13eibo2gl8vbkojg.apps.googleusercontent.com&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&access_type=online&approval_prompt=auto','google')" class="social-login g-login flt_left-resend">
                                        <span class="imgspan">
                                            <img src="/images/login-signup-gmail.png" alt="Google" style="width: 22px; height: 22px;"></span>
                                        <span class="R13_42 fbText" style="font-family: Roboto-Regular; font-size: 13px; color: #424242; line-height: 18px;">GOOGLE</span>
                                    </div>
                                </div>
                                <div style="margin-top: 15px;">
                                    <asp:HyperLink ID="hlnkHelp" NavigateUrl="~/Loginhelp.aspx" runat="server" data-toggle="tooltip" data-placement="right" ToolTip="Need Help to Login, Click here" OnClick="settracknew('Login','Login','Loginhelp','');">Login Help?</asp:HyperLink>
                                </div>
                            </div>

                            <div class="otpdiv" style="float: left; margin-bottom: 10px; margin-top: 5px;" runat="server" visible="false">
                                <asp:CheckBox runat="server" ID="chkOTP" Style="width: 114px;" Text="Request OTP" Checked="true" Visible="false" />
                            </div>

                            <div class="clearfix" style="height: 10px"></div>

                            <asp:CustomValidator ID="cvLogin" class="alert alert-block alert-danger fade in" EnableClientScript="False" runat="server" Display="None" />
                            <asp:ValidationSummary ID="vsLogin" class="alert alert-block alert-danger fade in" runat="server" />
                            <div style="float: left;">
                                <asp:LinkButton ID="lbtnAcctloked" CausesValidation="false" UseSubmitBehavior="false" runat="server" data-toggle="tooltip" data-placement="right"
                                    Text="Your account got locked, click here to unlock it." CssClass="pull-left" Visible="false"></asp:LinkButton>
                            </div>

                            <div class="alert alert-block alert-danger fade in" id="lblAcctLocked" runat="server" visible="false">
                                <asp:Label runat="server">Your account is locked, <a href="SecurityQuestion/UnlockAccount.aspx" target="_self" style="cursor:pointer; color:red !important;text-decoration:underline"><i>click here</i></a> to unlock your account.</asp:Label>
                            </div>

                        </div>

                        <div style="clear: both; height: 10px; background: #eeeeee;"></div>
                        <div class="col-md-12 login-form-head" style="display: none;">
                            <div style="float: left; width: 177px; padding: 5px; text-align: left; color: #000;">Scan QR code to download our app for IOS and Android </div>
                            <div style="float: right; padding-top: 5px;">
                                <img src="Images/TeamLease.png">
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="Submit" />
                        <asp:PostBackTrigger ControlID="btnAD" />
                    </Triggers>
                </asp:UpdatePanel>
            </asp:Panel>
        </form>
    </div>
    <div class="clearfix" style="height: 10px"></div>
    <!--js-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jquery.js"></script>
    <script type='text/javascript' src="https://avacdn.azureedge.net/newjs/bootstrap.min.js"></script>
    <%--    <script src="js/jquery-ui-1.10.4.min.js"></script>--%>

    <script type="text/javascript">
        function socaillogin(data, type) { var DomainName = location.hostname.toLowerCase(); window.location.href = data }
        $(function () {
            $("#chkOTP").click(function () {
                if ($(this).is(":checked")) {
                    $("#dvCaptcha").hide();
                    $("#dvOr").hide();
                } else {
                    $("#dvCaptcha").show();
                    $("#dvOr").show();
                }
            });
        });
        $(document).tooltip({ selector: '[data-toggle="tooltip"]' });
    </script>

</body>
</html>
