﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="securitypolicy.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.SecurityPolicy" %>

<!DOCTYPE html>

<%@ Register Assembly="GoogleReCaptcha" Namespace="GoogleReCaptcha" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="FortuneCookie - Development Team" />

    <title>Security Policy :: AVANTIS - Products that simplify</title>  
    <!-- Bootstrap CSS -->
     <link href="NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="NewCSS/bootstrap-theme.css" rel="stylesheet" />             
    <!--external css-->
    <!-- font icon -->
    <link href="NewCSS/elegant-icons-style.css" rel="stylesheet" />
    <link href="NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="NewCSS/style.css" rel="stylesheet" />
    <link href="NewCSS/style-responsive.css" rel="stylesheet" />
    <style>
        .otpdiv > span > label {
            border: 0px;
            color: black;
            /* margin-bottom: 5px; */
            margin-left: -17px;
            margin-top: 3px;
            position: absolute;
            width: 111px;

        }
      .otpdiv > span  {
            width: 233px;
            height: 26px;
            margin: auto;
            position: relative;
            /* background: #fff; */
            /* border: 1px solid #2e2e2e; */
            border-radius: 2px;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
        }
        .otpdiv > span > input {
            width: 80px;
            height: 26px;
            margin: auto;
            position: relative;           
            border-radius: 2px;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            margin-left: -9px;
        }
    </style>
</head>
<body>
    <div class="container">
        <form runat="server" class="login-form" name="login" id="loginForm" autocomplete="off">
   
                        <div class="col-md-12 login-form-head">
                            <p class="login-img">  <a href="https://teamleaseregtech.com">
                                <img src="Images/avantil-logo1.png" /></a>
                            </p>
                        </div>
                        <div class="login-wrap">
                            <h1>Security Policy</h1>
                           
                            <div  style="color: #666;">
                              <ul style="margin-left: -13%;">
                                    <li>
                                      <b> Security Questions</b> <br />
                                      For each user the security questions and answer are mandatory to access the Avantis system<br /><br />
                                    </li><li>
                                     <b>Renewal password</b> <br />
	                                    User of the system has to change the password every 60 days. If user did not change the password within 60 days  he/she will be landed on change password screen automatically. Once user change the password he/she can continue using the system with new password configured.
                                       <br /><br /></li><li>
                                     <b>Change password</b> <br />
	                                     User can change the password from this screen he/she has to enter old password to create new password
                                        <br /><br /></li><li>
                                     <b>One Time Password</b> <br />
	                                    User will get the OTP on his/her phone for logging into Avantis compliance, this    password will valid for only half an hour. Once OTP used will allowed to use again.
                                        <br /><br /></li><li>
                                     <b>Captcha details</b> <br />
	                                    For security reason user has to select the captcha before proceeding to access the system
                                         <br /><br /></li><li>
                                     <b>Password</b> <br />
	                                    Password should be eight-character long alpha numeric
                                         <br /><br /></li><li>
                                     <b>Log out</b> <br />
	                                    User can choose the log out option after finish his/her work. If the user is Ideal for 15 mins he/she will be automatically logged out of the system.
                                         <br /><br /> </li> 
                                    </ul>
                            </div>
                        </div>
             <div class="login-wrap">
                    <asp:LinkButton ID="lnklogin" CausesValidation="false" runat="server" Text="Click here to go back"  OnClientClick="back();"></asp:LinkButton>
                   
             </div>
                    
        </form>
    </div>
    <div class="clearfix" style="height: 10px"></div>
    <!--js-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
   <script type="text/javascript" src="../Newjs/jquery.js"></script>
    <script type='text/javascript' src="Newjs/bootstrap.min.js"></script>
   <%-- <script type='text/javascript' src="Newjs/google1113_jquery.min.js"></script>--%>
    <script src="js/jquery-ui-1.10.4.min.js"></script>
  <script>
      function back() {
          window.history.back();
      };
  </script>
</body>
</html>
