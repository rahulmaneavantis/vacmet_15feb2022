﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using AjaxControlToolkit;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;
using System.Net;
using System.Web.Script.Serialization;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Data.Services.Client;
using System.Threading.Tasks;
using System.Security.Claims;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;


namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class LoginStr : System.Web.UI.Page
    {
        public static string key = "avantis";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ProcessAuthenticationInformation(Business.Data.User user)
        {
            try
            {
                mst_User objmstuser = UserManagementRisk.GetByID(Convert.ToInt32(user.ID));
                string mstrole = RoleManagementRisk.GetByID(objmstuser.RoleID).Code;
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagement.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                int checkTaskapplicable = 0;
                int checkVerticalapplicable = 0;
                int checkLabelApplicable = 0;
                bool IsPaymentCustomer = false;
                int complianceProdType = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                    checkTaskapplicable = 2;
                    checkVerticalapplicable = 2;
                    checkLabelApplicable = 2;
                }
                else
                {
                    Customer c = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                    var IsInternalComplianceApplicable = c.IComplianceApplicable;
                    if (IsInternalComplianceApplicable != -1)
                    {
                        checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                    }
                    var IsTaskApplicable = c.TaskApplicable;
                    if (IsTaskApplicable != -1)
                    {
                        checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                    }
                    var IsVerticlApplicable = c.VerticalApplicable;
                    if (IsVerticlApplicable != null)
                    {
                        if (IsVerticlApplicable != -1)
                        {
                            checkVerticalapplicable = Convert.ToInt32(IsVerticlApplicable);
                        }
                    }
                    if (c.IsPayment != null)
                    {
                        IsPaymentCustomer = Convert.ToBoolean(c.IsPayment);
                    }

                    var IsLabelApplicable = c.IsLabelApplicable;
                    if (IsLabelApplicable != -1)
                    {
                        checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                    }

                    if (c.ComplianceProductType != null)
                    {
                        complianceProdType = Convert.ToInt32(c.ComplianceProductType);
                    }
                }

                Business.Data.User userToUpdate = new Business.Data.User();
                userToUpdate.ID = user.ID;
                userToUpdate.LastLoginTime = DateTime.UtcNow;
                userToUpdate.WrongAttempt = 0;

                Business.DataRisk.mst_User userToUpdate1 = new Business.DataRisk.mst_User();
                userToUpdate1.ID = user.ID;
                userToUpdate1.LastLoginTime = DateTime.UtcNow;

                UserManagement.Update(userToUpdate);
                UserManagementRisk.Update(userToUpdate1);

                if (role.Equals("SADMN"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Users/UserSummary.aspx", false);
                }
                else if (role.Equals("IMPT"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/CompanyStructure.aspx", false);
                }
                else if (role.Equals("UPDT"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/LegalUpdateAdmin.aspx", false);
                }
                else if (role.Equals("RPER"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/ResearchPerformerAdmin.aspx", false);
                }
                else if (role.Equals("RREV"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/Common/ResearchReviewerAdmin.aspx", false);
                }
                else if (role.Equals("HVADM"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, 'C', user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                    Response.Redirect("~/RLCSVendorAudit/RLCSUploadChecklist.aspx", false);
                }
                else
                {
                    ProductMappingStructure _obj = new ProductMappingStructure();
                    var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(user.CustomerID));
                    if (ProductMappingDetails.Count == 1)
                    {
                        if (ProductMappingDetails.Contains(1))
                        {
                            _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(2))
                        {
                            _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(3))
                        {
                            _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(4))
                        {
                            _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(5))
                        {
                            _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(6))
                        {
                            _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                        else if (ProductMappingDetails.Contains(7))
                        {
                            _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                        }
                    }
                    else if (ProductMappingDetails.Count > 1)
                    {
                        if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(2) && user.LitigationRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(5) && user.ContractRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(6) && user.LicenseRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else if (ProductMappingDetails.Count == 2 && ProductMappingDetails.Contains(7) && user.VendorRoleID == null)
                        {
                            if (ProductMappingDetails.Contains(1))
                            {
                                _obj.FormsAuthenticationRedirect_Compliance(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(2))
                            {
                                _obj.FormsAuthenticationRedirect_Litigation(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(3))
                            {
                                _obj.FormsAuthenticationRedirect_IFC(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(4))
                            {
                                _obj.FormsAuthenticationRedirect_ARS(objmstuser, mstrole, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(5))
                            {
                                _obj.FormsAuthenticationRedirect_Contract(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(6))
                            {
                                _obj.FormsAuthenticationRedirect_License(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                            else if (ProductMappingDetails.Contains(7))
                            {
                                _obj.FormsAuthenticationRedirect_RLCSVendor(user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                            }
                        }
                        else
                        {
                            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                            Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                            Response.Redirect("~/ProductMapping/ProductMappingStructure.aspx", false);
                        }
                    }
                    else
                    {
                        FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), Convert.ToBoolean(Session["RM"]));
                        FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.ID, role, name, checkInternalapplicable, "C", user.CustomerID, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                        TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                        Session["LastLoginTime"] = user.LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(user.LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now, destinationTimeZone);
                        Response.Redirect("~/ProductMapping/ProductMappingStructure.aspx", false);
                    }
                }

                DateTime LastPasswordChangedDate = Convert.ToDateTime(UserManagement.GetByID(Convert.ToInt32(user.ID)).ChangPasswordDate);
                DateTime currentDate = DateTime.Now;
                LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                if (user.CustomerID == 6)
                {
                    noDays = 90;
                }
                int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                if (dateDifference == noDays || dateDifference > noDays)
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
                else
                {
                    Session["ChangePassword"] = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void RLCSProcessAuthenticationInformation(RLCS_User_Mapping user, string ipaddress, string Macaddress)
        {
            try
            {
                int cid = UserManagement.GetCustomerIDRLCS(user.CustomerID);
                if (cid != -1)
                {
                    string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                    string role = user.AVACOM_UserRole;
                    int checkInternalapplicable = 2;
                    int checkTaskapplicable = 2;
                    int checkVerticalapplicable = 2;
                    int checkLabelApplicable = 2;
                    bool IsPaymentCustomer = false;

                    int complianceProdType = 1;
                    //complianceProdType = RLCS_Master_Management.GetComplianceProductType(cid);
                    
                    Business.Data.Customer c = CustomerManagement.GetByID(cid);

                    if (c != null)
                    {
                        var IsInternalComplianceApplicable = c.IComplianceApplicable;
                        if (IsInternalComplianceApplicable != null && IsInternalComplianceApplicable != -1)
                        {
                            checkInternalapplicable = Convert.ToInt32(IsInternalComplianceApplicable);
                        }

                        var IsTaskApplicable = c.TaskApplicable;
                        if (IsTaskApplicable != null && IsTaskApplicable != -1)
                        {
                            checkTaskapplicable = Convert.ToInt32(IsTaskApplicable);
                        }

                        var IsVerticlApplicable = c.VerticalApplicable;
                        if (IsVerticlApplicable != null && IsVerticlApplicable != -1)
                        {
                            checkVerticalapplicable = Convert.ToInt32(IsVerticlApplicable);
                        }

                        if (c.IsPayment != null)
                        {
                            IsPaymentCustomer = Convert.ToBoolean(c.IsPayment);
                        }

                        var IsLabelApplicable = c.IsLabelApplicable;
                        if (IsLabelApplicable != -1)
                        {
                            checkLabelApplicable = Convert.ToInt32(IsLabelApplicable);
                        }

                        if (c.ComplianceProductType != null && c.ServiceProviderID != 94)
                        {
                            complianceProdType = Convert.ToInt32(c.ComplianceProductType);
                        }
                    }

                    if (complianceProdType == 1)
                    {

                        if (role.Equals("SADMN"))
                        {
                            FormsAuthentication.RedirectFromLoginPage(user.AVACOM_UserID.ToString(), Convert.ToBoolean(Session["RM"]));
                            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.AVACOM_UserID, role, name, checkInternalapplicable, 'C', cid, checkTaskapplicable, user.AVACOM_UserID, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                            Response.Redirect("~/Users/UserSummary.aspx", false);
                        }
                        else if (role.Equals("IMPT"))
                        {
                            FormsAuthentication.RedirectFromLoginPage(user.AVACOM_UserID.ToString(), Convert.ToBoolean(Session["RM"]));
                            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.AVACOM_UserID, role, name, checkInternalapplicable, 'C', cid, checkTaskapplicable, user.AVACOM_UserID, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                            Response.Redirect("~/Common/CompanyStructure.aspx", false);
                        }
                        else
                        {
                            ProductMappingStructure _obj = new ProductMappingStructure();
                            var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(cid));
                            if (ProductMappingDetails.Count >= 1)
                            {
                                if (ProductMappingDetails.Contains(1))
                                {
                                    _obj.FormsAuthenticationRedirect_RLCSCompliance(cid, user, role, name, checkInternalapplicable, checkTaskapplicable, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType);
                                }
                            }
                            else
                            {
                                FormsAuthentication.RedirectFromLoginPage(user.AVACOM_UserID.ToString(), Convert.ToBoolean(Session["RM"]));
                                FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12}", user.AVACOM_UserID, role, name, checkInternalapplicable, "C", cid, checkTaskapplicable, string.Empty, string.Empty, checkVerticalapplicable, IsPaymentCustomer, checkLabelApplicable, complianceProdType), false);
                                TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                                Response.Redirect("~/ProductMapping/ProductMappingStructure.aspx", false);
                            }
                        }

                        Session["ChangePassword"] = false;
                    }
                    else
                    {
                        if (user.AVACOM_UserID != null)
                        {
                            User avacomUserRecord = UserManagement.GetByID(Convert.ToInt32(user.AVACOM_UserID));

                            if (avacomUserRecord != null)
                            {
                                ProceedLogin(avacomUserRecord, ipaddress, Macaddress);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnAD_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("https://myapps.microsoft.com/signin/avantisprod/4406f761-8c47-474e-8645-662714fa10bd?tenantId=c0408565-7970-454d-be62-a8fcd708eb27", false);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            string ipaddress = string.Empty;
            try
            {
                string Macaddress = string.Empty;
                Macaddress = Util.GetMACAddress();
                ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (ipaddress == "" || ipaddress == null)
                    ipaddress = Request.ServerVariables["REMOTE_ADDR"];

                try
                {
                    UserLoginTrack obj = new UserLoginTrack()
                    {
                        Email = txtemail.Text,
                        LoginDate = DateTime.Now,
                        IPAddress = ipaddress,
                        MACAddress = Macaddress,
                        LoginFlag = false
                    };
                    UserManagement.CreateUserLoginTrack(obj);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertErrorMsg_DBLog(ex.ToString() + " IP: " + ipaddress, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }


                NoBotState state;
                //if (PageNoBot.IsValid(out state))
                if (true)
                {
                    Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>> blockip = null;
                    lblAcctLocked.Visible = false;
                    bool Success = true;
                    if (txtemail.Text.Trim().Contains('@'))
                    {
                        if (!UserManagement.EmailIDExists(txtemail.Text.Trim()))
                        {
                            #region If EmailID                        
                            Business.Data.User user = null;
                            //if (UserManagement.IsValidUser(txtemail.Text.Trim(), Util.CalculateAESHash(txtpass.Text.Trim()), out user))                            
                            if (UserManagement.IsValidUser(txtemail.Text.Trim(), txtpass.Text.Trim(), out user))
                            {
                                if (user != null)
                                {
                                    try
                                    {
                                        if (user.CustomerID != null)
                                        {
                                            blockip = UserManagement.GetBlockIpAddress((int)user.CustomerID, user.ID, ipaddress);
                                            if (!blockip.Item1)
                                            {
                                                cvLogin.IsValid = false;
                                                cvLogin.ErrorMessage = "Your Account is Disabled. Please Contact to Admin.";
                                                return;
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                    try
                                    {
                                        if (user.CustomerID != null)
                                        {
                                            var custRecord = SAMLManagement.CheckCustomerExists(Convert.ToInt32(user.CustomerID));
                                            if (custRecord != null)
                                            {
                                                if (!string.IsNullOrEmpty(custRecord.SAMLLoginPageURL) && custRecord.IdentityProvider == "Microsoft")
                                                {
                                                    Response.Redirect(custRecord.SAMLLoginPageURL + "?ID=" + user.CustomerID, true);
                                                }
                                            }
                                            else
                                            {
                                                string customer = ConfigurationManager.AppSettings["NotAuthorisedCustID"].ToString();
                                                if (!string.IsNullOrEmpty(customer))
                                                {
                                                    if (Convert.ToInt32(customer) == user.CustomerID)
                                                    {
                                                        cvLogin.IsValid = false;
                                                        cvLogin.ErrorMessage = "Request you to login through Google or your Company AD";
                                                        return;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                    bool Auditorexpired = false;
                                    if (user.RoleID == 9)
                                    {
                                        if (user.Enddate == null)
                                        {
                                            Auditorexpired = true;
                                        }
                                        if (DateTime.Now.Date > Convert.ToDateTime(user.Enddate).Date || DateTime.Now < Convert.ToDateTime(user.Startdate).Date)
                                        {
                                            Auditorexpired = true;
                                        }
                                    }
                                    if (!Auditorexpired)
                                    {
                                        if (!(user.WrongAttempt >= 3)) /*UserManagement.WrongAttemptCount(txtemail.Text.Trim())*/
                                        {
                                            if (user.IsActive)
                                            {
                                                Session["userID"] = user.ID;
                                                Session["ContactNo"] = user.ContactNumber;
                                                Session["Email"] = user.Email;
                                                Session["CustomerID_new"] = user.CustomerID;
                                                if (user.RoleID == 9)
                                                {
                                                    Session["Auditstartdate"] = user.AuditStartPeriod;
                                                    Session["Auditenddate"] = user.AuditEndPeriod;
                                                }
                                                DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate); /*UserManagement.GetByID(Convert.ToInt32(user.ID)*/
                                                DateTime currentDate = DateTime.Now;
                                                LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                                                int noDays = 0;
                                                if (blockip != null && blockip.Item2.Count > 0)
                                                {
                                                    var data = blockip.Item2.Where(a => a.PasswordExpiry != null).ToList();
                                                    if (data.Count > 0)
                                                    {
                                                        noDays = blockip.Item2.Select(entry => (int)entry.PasswordExpiry).FirstOrDefault();
                                                    }
                                                    else
                                                    {
                                                        noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                                                    }
                                                }
                                                else
                                                {
                                                    noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                                                }
                                                int customerID = 0;
                                                customerID = Convert.ToInt32(user.CustomerID);
                                                if (customerID == 6 || customerID == 5 || customerID == 14)
                                                {
                                                    noDays = 90;
                                                }
                                                int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                                                if (dateDifference >= noDays)
                                                {
                                                    Session["ChangePassword"] = true;
                                                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                                                }
                                                else if (user.LastLoginTime != null)
                                                {
                                                    //Generate Random Number 6 Digit For OTP.
                                                    Random random = new Random();
                                                    int value = random.Next(1000000);
                                                    if (value == 0)
                                                    {
                                                        value = random.Next(1000000);
                                                    }
                                                    Session["ResendOTP"] = Convert.ToString(value);
                                                    long Contact;
                                                    VerifyOTP OTPData = new VerifyOTP()
                                                    {
                                                        UserId = Convert.ToInt32(user.ID),
                                                        EmailId = txtemail.Text,
                                                        OTP = Convert.ToInt32(value),
                                                        CreatedOn = DateTime.Now,
                                                        IsVerified = false,
                                                        IPAddress = ipaddress
                                                    };
                                                    VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.                                                   
                                                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                                                    try
                                                    {
                                                        if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                                                        {
                                                            //Send Email on User Mail Id.
                                                            if (user.CustomerID != 5)
                                                            {
                                                                string ReplyEmailAddressName = ConfigurationManager.AppSettings["ReplyEmailAddressName"].ToString();
                                                                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Teamlease");
                                                               // SendGridEmailManager.SendGridMail(SenderEmailAddress, ReplyEmailAddressName, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Teamlease");
                                                            }
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Success = false;
                                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                    }
                                                    bool OTPresult = false;
                                                    try
                                                    {
                                                        OTPresult = long.TryParse(user.ContactNumber, out Contact);
                                                        if (OTPresult)
                                                        {
                                                            OTPData.MobileNo = Contact;
                                                        }
                                                        else
                                                        {
                                                            OTPData.MobileNo = 0;
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        OTPData.MobileNo = 0;
                                                        OTPresult = false;
                                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                    }

                                                    if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7") || user.ContactNumber.StartsWith("6")))
                                                    {
                                                        try
                                                        {
                                                            if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                                                            {
                                                                //Send SMS on User Mobile No.

                                                                var data = VerifyOTPManagement.GetSMSConfiguration("Avacom");
                                                                if (data != null)
                                                                {
                                                                    SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", data.TEMPLATEID_DLT_TE_ID, data.authkey, data.Header_sender, data.route);
                                                                }
                                                                else
                                                                {
                                                                    SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", "1207161856353769674");
                                                                }
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            Success = false;
                                                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                        }
                                                    }
                                                    MaintainLoginDetail objData = new MaintainLoginDetail()
                                                    {
                                                        UserId = Convert.ToInt32(user.ID),
                                                        Email = txtemail.Text,
                                                        CreatedOn = DateTime.Now,
                                                        IPAddress = ipaddress,
                                                        MACAddress = Macaddress,
                                                        LoginFrom = "WC",
                                                        //ProfileID=user.ID
                                                    };
                                                    UserManagement.Create(objData);
                                                    if (Success)
                                                    {
                                                        Session["RM"] = cbRememberMe.Checked;
                                                        Session["EA"] = Util.CalculateAESHash(txtemail.Text.Trim());
                                                        Session["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForSuccess();", true);
                                                        Response.Redirect("~/Users/OTPVerify.aspx", false);
                                                    }
                                                    else
                                                    {
                                                        if (UserManagement.HasUserSecurityQuestion(user.ID))
                                                        {
                                                            Session["RM"] = cbRememberMe.Checked;
                                                            Session["EA"] = Util.CalculateAESHash(txtemail.Text.Trim());
                                                            Session["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                                                            Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                                        }
                                                        else
                                                        {
                                                            Session["QuestionBank"] = true;
                                                            Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                                                }
                                            }
                                            else
                                            {
                                                cvLogin.IsValid = false;
                                                cvLogin.ErrorMessage = "Your Account is Disabled.";
                                            }
                                        }
                                        else
                                        {
                                            lblAcctLocked.Visible = true;
                                            Session["otpvrifyWattp"] = true;
                                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn1", "settracknewForFailed();", true);
                                            //sandesh

                                            var plainBytes = Encoding.UTF8.GetBytes(txtemail.Text.Trim());
                                            string EncryptEmail = Convert.ToBase64String(DataEncryption.Encrypt(plainBytes, DataEncryption.GetRijndaelManaged(key)));
                                            userlogout u1 = new userlogout() { email = EncryptEmail };
                                            JavaScriptSerializer js = new JavaScriptSerializer();
                                            var EncryptEmailbody = js.Serialize(u1);
                                            string requestUrl = "https://api.avantis.co.in/api/v2/logout/";
                                            ServicePointManager.Expect100Continue = true;
                                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                            string responseData = WebAPIUtility.Invoke("POST", requestUrl, EncryptEmailbody.ToString());

                                        }
                                    }
                                    else
                                    {
                                        cvLogin.IsValid = false;
                                        cvLogin.ErrorMessage = "Your Account is Disabled.";
                                    }

                                }
                                else
                                {
                                    UserManagement.WrongUpdate(txtemail.Text.Trim());
                                    cvLogin.IsValid = false;
                                    cvLogin.ErrorMessage = "Please enter valid username or password.";
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);
                                }
                            }
                            else
                            {
                                UserManagement.WrongUpdate(txtemail.Text.Trim());
                                if (UserManagement.WrongAttemptCount(txtemail.Text.Trim()) >= 3)
                                {
                                    lblAcctLocked.Visible = true;
                                    Session["otpvrifyWattp"] = true;
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);
                                    //sandesh

                                    var plainBytes = Encoding.UTF8.GetBytes(txtemail.Text.Trim());
                                    string EncryptEmail = Convert.ToBase64String(DataEncryption.Encrypt(plainBytes, DataEncryption.GetRijndaelManaged(key)));
                                    userlogout u1 = new userlogout() { email = EncryptEmail };
                                    JavaScriptSerializer js = new JavaScriptSerializer();
                                    var EncryptEmailbody = js.Serialize(u1);
                                    string requestUrl = "https://api.avantis.co.in/api/v2/logout/";
                                    ServicePointManager.Expect100Continue = true;
                                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                    string responseData = WebAPIUtility.Invoke("POST", requestUrl, EncryptEmailbody.ToString());
                                }
                                else
                                {
                                    cvLogin.IsValid = false;
                                    cvLogin.ErrorMessage = "Please enter valid username or password.";
                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);

                                }
                            }
                            #endregion
                        }
                        else
                        {
                            //RLCS_User_Login(txtemail.Text.Trim(), Util.CalculateAESHash(txtpass.Text.Trim()));                             
                            RLCS_User_Login(txtemail.Text.Trim(), txtpass.Text.Trim());
                        }
                    }
                    else
                    {
                        #region If No EmailID
                        RLCS_User_Mapping user = null;
                        // if (UserManagement.IsValidUserID(txtemail.Text.Trim(), Util.CalculateAESHash(txtpass.Text.Trim()), out user))                       
                        if (UserManagement.IsValidUserID(txtemail.Text.Trim(), txtpass.Text.Trim(), out user))
                        {
                            if (user != null)
                            {
                                try
                                {
                                    Macaddress = Util.GetMACAddress();
                                    ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                                    if (ipaddress == "" || ipaddress == null)
                                        ipaddress = Request.ServerVariables["REMOTE_ADDR"];

                                    //var blockip = UserManagement.GetBlockIpAddress(-1, (long)user.AVACOM_UserID, ipaddress);
                                    //if (!blockip)
                                    //{
                                    //    cvLogin.IsValid = false;
                                    //    cvLogin.ErrorMessage = "Your Account is Disabled. Please Contact to Admin.";
                                    //    return;
                                    //}
                                }
                                catch (Exception ex)
                                {
                                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                }
                                if (user.IsActive)
                                {
                                    Session["userID"] = user.AVACOM_UserID;
                                    Session["ContactNo"] = user.ContactNumber;
                                    Session["Email"] = user.UserID;
                                    //added by rahul on 29 OCT 2018
                                    Session["RLCS_userID"] = null;
                                    Session["RLCS_userID"] = user.UserID;
                                    Session["RLCS_ProfileID"] = null;
                                    Session["RLCS_ProfileID"] = user.ProfileID;
                                    MaintainLoginDetail objData = new MaintainLoginDetail()
                                    {
                                        UserId = Convert.ToInt32(user.AVACOM_UserID),
                                        Email = txtemail.Text,
                                        CreatedOn = DateTime.Now,
                                        IPAddress = ipaddress,
                                        MACAddress = Macaddress,
                                        LoginFrom = "WT",
                                        ProfileID = user.ProfileID
                                    };
                                    UserManagement.Create(objData);
                                    Session["RM"] = cbRememberMe.Checked;
                                    Session["EA"] = Util.CalculateAESHash(txtemail.Text.Trim());
                                    Session["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                                    //Customer c = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                                    //var Expirydays = CustomerManagement.GetExpirydays(c.ID);
                                    //int? Expirydays = CustomerManagement.GetExpirydays(Convert.ToInt32(c.ID));
                                    int? Expirydays = 60;
                                    int expdays;
                                    if (Expirydays == null || Expirydays == 0)
                                    {
                                        expdays = 60;
                                    }
                                    else
                                    {
                                        expdays = Convert.ToInt32(Expirydays);
                                    }


                                    if (cbRememberMe.Checked)
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "CheckOrNot", "settracknewRememberUnchecked();", true);
                                        if (FormsAuthentication.CookiesSupported)
                                        {
                                            //LoginCookie
                                            HttpCookie loginCookie = new HttpCookie("ALC");
                                            //let the cookie expire after 60 days
                                            loginCookie.Expires = DateTime.Now.AddDays(expdays);
                                            // loginCookie.Expires = DateTime.Now.AddDays(60);
                                            loginCookie.Values["EA"] = Util.CalculateAESHash(txtemail.Text.Trim());
                                            loginCookie.Values["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                                            //loginCookie.Secure = true;
                                            loginCookie.HttpOnly = true;
                                            Response.Cookies.Add(loginCookie);
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "CheckOrNot", "settracknewRememberchecked();", true);
                                        Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                                    }
                                    RLCSProcessAuthenticationInformation(user, ipaddress, Macaddress);
                                    Session["RM"] = null;
                                    Session["EA"] = null;
                                    Session["MA"] = null;
                                }
                                else
                                {
                                    cvLogin.IsValid = false;
                                    cvLogin.ErrorMessage = "Your Account is Disabled.";
                                }
                            }
                            else
                            {
                                cvLogin.IsValid = false;
                                cvLogin.ErrorMessage = "Please enter valid username or password.";
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);
                            }
                        }
                        else
                        {
                            cvLogin.IsValid = false;
                            cvLogin.ErrorMessage = "Please enter valid username or password.";
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertErrorMsg_DBLog(ex.ToString() + " IP: " + ipaddress, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void RLCS_User_Login(string email, string Password)
        {
            try
            {
                #region If No EmailID

                string ipaddress = string.Empty;
                string Macaddress = string.Empty;

                RLCS_User_Mapping user = null;
                if (UserManagement.IsValidUserID(email, Password, out user))
                {
                    if (user != null)
                    {
                        try
                        {
                            Macaddress = Util.GetMACAddress();
                            ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                            if (ipaddress == "" || ipaddress == null)
                                ipaddress = Request.ServerVariables["REMOTE_ADDR"];

                            int? CID = CustomerManagement.GetByCustID(Convert.ToInt32(user.AVACOM_UserID));
                            if (CID != null)
                            {
                                var blockip = UserManagement.GetBlockIpAddress((int)CID, (long)user.AVACOM_UserID, ipaddress);
                                if (!blockip.Item1)
                                {
                                    cvLogin.IsValid = false;
                                    cvLogin.ErrorMessage = "Your Account is Disabled. Please Contact to Admin.";
                                    return;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        }
                        if (user.IsActive)
                        {
                            Session["userID"] = user.AVACOM_UserID;
                            Session["ContactNo"] = user.ContactNumber;
                            Session["Email"] = user.Email;
                            //added by rahul on 29 OCT 2018
                            Session["RLCS_userID"] = null;
                            Session["RLCS_userID"] = user.UserID;
                            Session["RLCS_ProfileID"] = null;
                            Session["RLCS_ProfileID"] = user.ProfileID;
                            MaintainLoginDetail objData = new MaintainLoginDetail()
                            {
                                UserId = Convert.ToInt32(user.AVACOM_UserID),
                                Email = email,
                                CreatedOn = DateTime.Now,
                                IPAddress = ipaddress,
                                MACAddress = Macaddress,
                                LoginFrom = "WT",
                                ProfileID = user.ProfileID
                            };
                            UserManagement.Create(objData);
                            Session["RM"] = cbRememberMe.Checked;
                            Session["EA"] = Util.CalculateAESHash(email);
                            Session["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                            if (cbRememberMe.Checked)
                            {
                                if (FormsAuthentication.CookiesSupported)
                                {
                                    //LoginCookie
                                    HttpCookie loginCookie = new HttpCookie("ALC");
                                    //let the cookie expire after 60 days
                                    loginCookie.Expires = DateTime.Now.AddDays(60);
                                    loginCookie.Values["EA"] = Util.CalculateAESHash(email);
                                    loginCookie.Values["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                                    //loginCookie.Secure = true;
                                    loginCookie.HttpOnly = true;
                                    Response.Cookies.Add(loginCookie);
                                }
                            }
                            else
                            {
                                Response.Cookies["ALC"].Expires = DateTime.Now.AddDays(-1);
                            }
                            RLCSProcessAuthenticationInformation(user, ipaddress, Macaddress);
                            Session["RM"] = null;
                            Session["EA"] = null;
                            Session["MA"] = null;
                        }
                        else
                        {
                            cvLogin.IsValid = false;
                            cvLogin.ErrorMessage = "Your Account is Disabled.";
                        }
                    }
                    else
                    {
                        cvLogin.IsValid = false;
                        cvLogin.ErrorMessage = "Please enter valid username or password.";
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);
                    }
                }
                else
                {
                    cvLogin.IsValid = false;
                    cvLogin.ErrorMessage = "Please enter valid username or password.";
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);
                }
                #endregion
            }
            catch (Exception ex)
            {

            }
        }

        protected void IfCookieExists(string email, string maddress1)
        {
            string ipaddress = string.Empty;
            try
            {
                string Macaddress = string.Empty;
                Macaddress = Util.GetMACAddress();

                try
                {
                    ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (ipaddress == "" || ipaddress == null)
                        ipaddress = Request.ServerVariables["REMOTE_ADDR"];

                    //ipaddress = "42.108.254.12";
                    //var blockip = UserManagement.GetBlockIpAddress();
                    //if (blockip.Contains(ipaddress))
                    //{
                    //    cvLogin.IsValid = false;
                    //    cvLogin.ErrorMessage = "Your Account is Disabled. Please Contact to Admin.";
                    //    return;
                    //}
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertErrorMsg_DBLog(ex.ToString() + " IP: " + ipaddress, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
                if (ipaddress == maddress1)
                {
                    lblAcctLocked.Visible = false;

                    if (email.Trim().Contains('@'))
                    {
                        if (!UserManagement.EmailIDExists(txtemail.Text.Trim()))
                        {
                            #region If EmailID    
                            Business.Data.User user = null;
                            if (UserManagement.IsValidUser(email.Trim(), out user))
                            {
                                Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>> blockip = null;
                                if (user != null)
                                {
                                    try
                                    {
                                        if (user.CustomerID != null)
                                        {
                                            blockip = UserManagement.GetBlockIpAddress((int)user.CustomerID, user.ID, ipaddress);
                                            if (!blockip.Item1)
                                            {
                                                cvLogin.IsValid = false;
                                                cvLogin.ErrorMessage = "Your Account is Disabled. Please Contact to Admin.";
                                                return;
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerMessage.InsertErrorMsg_DBLog(ex.ToString() + " IP: " + ipaddress, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }


                                    bool Auditorexpired = false;
                                    if (user.RoleID == 9)
                                    {
                                        if (user.Enddate == null)
                                        {
                                            Auditorexpired = true;
                                        }
                                        if (DateTime.Now.Date > Convert.ToDateTime(user.Enddate).Date || DateTime.Now < Convert.ToDateTime(user.Startdate).Date)
                                        {
                                            Auditorexpired = true;
                                        }
                                    }
                                    if (!Auditorexpired)
                                    {
                                        if (!(user.WrongAttempt >= 3))
                                        {
                                            if (user.IsActive)
                                            {
                                                Session["userID"] = user.ID;
                                                Session["ContactNo"] = user.ContactNumber;
                                                Session["Email"] = user.Email;
                                                if (user.RoleID == 9)
                                                {
                                                    Session["Auditstartdate"] = user.AuditStartPeriod;
                                                    Session["Auditenddate"] = user.AuditEndPeriod;
                                                }
                                                DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate);
                                                DateTime currentDate = DateTime.Now;
                                                LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;

                                                int noDays = 0;
                                                if (blockip != null && blockip.Item2.Count > 0)
                                                {
                                                    var data = blockip.Item2.Where(a => a.PasswordExpiry != null).ToList();
                                                    if (data.Count > 0)
                                                    {
                                                        noDays = blockip.Item2.Select(entry => (int)entry.PasswordExpiry).FirstOrDefault();
                                                    }
                                                    else
                                                    {
                                                        noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                                                    }
                                                }
                                                else
                                                {
                                                    noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                                                }

                                                //int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                                                int customerID = 0;
                                                customerID = Convert.ToInt32(user.CustomerID);
                                                if (customerID == 6 || customerID == 5 || customerID == 14)
                                                {
                                                    noDays = 90;
                                                }
                                                int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                                                if (dateDifference >= noDays)
                                                {
                                                    Session["ChangePassword"] = true;
                                                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                                                }
                                                else if (user.LastLoginTime != null)
                                                {
                                                    
                                                    bool Success = true;
                                                    //Generate Random Number 6 Digit For OTP.
                                                    Random random = new Random();
                                                    int value = random.Next(1000000);
                                                    if (value == 0)
                                                    {
                                                        value = random.Next(1000000);
                                                    }
                                                    Session["ResendOTP"] = Convert.ToString(value);
                                                    long Contact;
                                                    VerifyOTP OTPData = new VerifyOTP()
                                                    {
                                                        UserId = Convert.ToInt32(user.ID),
                                                        EmailId = user.Email,
                                                        OTP = Convert.ToInt32(value),
                                                        CreatedOn = DateTime.Now,
                                                        IsVerified = false,
                                                        IPAddress = ipaddress
                                                    };
                                                    bool OTPresult = false;
                                                    try
                                                    {
                                                        OTPresult = long.TryParse(user.ContactNumber, out Contact);
                                                        if (OTPresult)
                                                        {
                                                            OTPData.MobileNo = Contact;
                                                        }
                                                        else
                                                        {
                                                            OTPData.MobileNo = 0;
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        OTPData.MobileNo = 0;
                                                        OTPresult = false;
                                                        LoggerMessage.InsertErrorMsg_DBLog(ex.ToString() + " IP: " + ipaddress, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                    }
                                                    VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.                                                   
                                                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                                                    try
                                                    {
                                                        if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                                                        {
                                                            //Send Email on User Mail Id.
                                                            if (user.CustomerID != 5)
                                                            {
                                                                string ReplyEmailAddressName = ConfigurationManager.AppSettings["ReplyEmailAddressName"].ToString();
                                                                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Teamlease");
                                                            }
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Success = false;
                                                        LoggerMessage.InsertErrorMsg_DBLog(ex.ToString() + " IP: " + ipaddress, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                    }
                                                 

                                                    if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7")))
                                                    {
                                                        try
                                                        {
                                                            if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                                                            {
                                                                //Send SMS on User Mobile No.

                                                                var data = VerifyOTPManagement.GetSMSConfiguration("Avacom");
                                                                if (data != null)
                                                                {
                                                                    SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", data.TEMPLATEID_DLT_TE_ID, data.authkey, data.Header_sender, data.route);
                                                                }
                                                                else
                                                                {
                                                                    SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", "1207161856353769674");
                                                                }
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            Success = false;
                                                            LoggerMessage.InsertErrorMsg_DBLog(ex.ToString() + " IP: " + ipaddress, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                                        }
                                                    }

                                                    MaintainLoginDetail objdata = new MaintainLoginDetail()
                                                    {
                                                        UserId = Convert.ToInt32(user.ID),
                                                        Email = user.Email,
                                                        CreatedOn = DateTime.Now,
                                                        IPAddress = ipaddress,
                                                        MACAddress = Macaddress,
                                                        LoginFrom = "WC",
                                                        //ProfileID = user.ID
                                                    };
                                                    UserManagement.Create(objdata);
                                                    if (Success)
                                                    {
                                                        Session["RM"] = cbRememberMe.Checked;
                                                        Session["EA"] = Util.CalculateAESHash(txtemail.Text.Trim());
                                                        Session["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForSuccess();", true);
                                                        Response.Redirect("~/Users/OTPVerify.aspx", false);
                                                    }
                                                    else
                                                    {
                                                        if (UserManagement.HasUserSecurityQuestion(user.ID))
                                                        {
                                                            Session["RM"] = cbRememberMe.Checked;
                                                            Session["EA"] = Util.CalculateAESHash(txtemail.Text.Trim());
                                                            Session["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                                                            Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                                        }
                                                        else
                                                        {
                                                            Session["QuestionBank"] = true;
                                                            Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                                        }
                                                    }
                                                    ProcessAuthenticationInformation(user);
                                                }
                                                else
                                                {
                                                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                                                }
                                            }
                                            else
                                            {
                                                cvLogin.IsValid = false;
                                                cvLogin.ErrorMessage = "Your Account is Disabled.";
                                            }
                                        }
                                        else
                                        {
                                            lblAcctLocked.Visible = true;
                                            Session["otpvrifyWattp"] = true;
                                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);

                                            //sandesh

                                            var plainBytes = Encoding.UTF8.GetBytes(txtemail.Text.Trim());
                                            string EncryptEmail = Convert.ToBase64String(DataEncryption.Encrypt(plainBytes, DataEncryption.GetRijndaelManaged(key)));
                                            userlogout u1 = new userlogout() { email = EncryptEmail };
                                            JavaScriptSerializer js = new JavaScriptSerializer();
                                            var EncryptEmailbody = js.Serialize(u1);
                                            string requestUrl = "https://api.avantis.co.in/api/v2/logout/";
                                            ServicePointManager.Expect100Continue = true;
                                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                            string responseData = WebAPIUtility.Invoke("POST", requestUrl, EncryptEmailbody.ToString());
                                        }
                                    }
                                    else
                                    {
                                        cvLogin.IsValid = false;
                                        cvLogin.ErrorMessage = "Your Account is Disabled.";
                                    }
                                }
                            }
                            else
                            {
                                UserManagement.WrongUpdate(txtemail.Text.Trim());
                                cvLogin.IsValid = false;
                                cvLogin.ErrorMessage = "Please enter valid username or password.";
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);
                            }
                            #endregion
                        }
                        else
                        {
                            #region If   EmailID  In RLCS User Mapping
                            RLCS_User_Mapping user = null;
                            if (UserManagement.IsValidUserIDRLCS(email.Trim(), out user))
                            {
                                if (user != null)
                                {
                                    int? CID = CustomerManagement.GetByCustID(Convert.ToInt32(user.AVACOM_UserID));
                                    if (CID != null)
                                    {
                                        var blockip = UserManagement.GetBlockIpAddress((int)CID, (long)user.AVACOM_UserID, ipaddress);
                                        if (!blockip.Item1)
                                        {
                                            cvLogin.IsValid = false;
                                            cvLogin.ErrorMessage = "Your Account is Disabled. Please Contact to Admin.";
                                            return;
                                        }
                                    }
                                    if (user.IsActive)
                                    {
                                        Session["userID"] = user.AVACOM_UserID;
                                        Session["ContactNo"] = user.ContactNumber;
                                        Session["Email"] = user.Email;
                                        MaintainLoginDetail objdata = new MaintainLoginDetail()
                                        {
                                            UserId = Convert.ToInt32(user.ID),
                                            Email = user.Email,
                                            CreatedOn = DateTime.Now,
                                            IPAddress = ipaddress,
                                            MACAddress = Macaddress,
                                            LoginFrom = "WT",
                                            ProfileID = user.ProfileID
                                        };
                                        UserManagement.Create(objdata);
                                        RLCSProcessAuthenticationInformation(user, ipaddress, Macaddress);
                                    }
                                    else
                                    {
                                        cvLogin.IsValid = false;
                                        cvLogin.ErrorMessage = "Your Account is Disabled.";
                                    }
                                }
                            }
                            else
                            {
                                UserManagement.WrongUpdate(txtemail.Text.Trim());
                                cvLogin.IsValid = false;
                                cvLogin.ErrorMessage = "Please enter valid username or password.";
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);
                            }
                            #endregion
                        }
                        //else
                        //{
                        //    ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknew('Login', 'LoginUnsucessful', 'Page-Login', '0');", true);
                        //}
                    }
                    else
                    {
                        #region If  No EmailID    
                        RLCS_User_Mapping user = null;
                        if (UserManagement.IsValidUserID(email.Trim(), out user))
                        {
                            if (user != null)
                            {
                                if (user.IsActive)
                                {
                                    Session["userID"] = user.AVACOM_UserID;
                                    Session["ContactNo"] = user.ContactNumber;
                                    Session["Email"] = user.UserID;
                                    MaintainLoginDetail objdata = new MaintainLoginDetail()
                                    {
                                        UserId = Convert.ToInt32(user.ID),
                                        Email = user.Email,
                                        CreatedOn = DateTime.Now,
                                        IPAddress = ipaddress,
                                        MACAddress = Macaddress,
                                        LoginFrom = "WT",
                                        ProfileID = user.ProfileID
                                    };
                                    UserManagement.Create(objdata);
                                    RLCSProcessAuthenticationInformation(user, ipaddress, Macaddress);
                                }
                                else
                                {
                                    cvLogin.IsValid = false;
                                    cvLogin.ErrorMessage = "Your Account is Disabled.";
                                }
                            }
                        }
                        else
                        {
                            UserManagement.WrongUpdate(txtemail.Text.Trim());
                            cvLogin.IsValid = false;
                            cvLogin.ErrorMessage = "Please enter valid username or password.";
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);
                        }
                        #endregion
                    }
                }
                else
                {
                    if (txtemail.Text.Trim().Contains('@'))
                    {
                        UserManagement.WrongUpdate(txtemail.Text.Trim());
                        if (UserManagement.WrongAttemptCount(txtemail.Text.Trim()) >= 3)
                        {
                            lblAcctLocked.Visible = true;
                            Session["otpvrifyWattp"] = true;
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);
                            //sandesh

                            var plainBytes = Encoding.UTF8.GetBytes(txtemail.Text.Trim());
                            string EncryptEmail = Convert.ToBase64String(DataEncryption.Encrypt(plainBytes, DataEncryption.GetRijndaelManaged(key)));
                            userlogout u1 = new userlogout() { email = EncryptEmail };
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            var EncryptEmailbody = js.Serialize(u1);
                            string requestUrl = "https://api.avantis.co.in/api/v2/logout/";
                            ServicePointManager.Expect100Continue = true;
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            string responseData = WebAPIUtility.Invoke("POST", requestUrl, EncryptEmailbody.ToString());
                        }
                        else
                        {
                            cvLogin.IsValid = false;
                            cvLogin.ErrorMessage = "Please enter valid username or password.";
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertErrorMsg_DBLog(ex.ToString() + " IP: " + ipaddress, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lbtResetPassword_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SecurityQuestion/ForgotPassword.aspx", false);
        }

        protected void lbtUnlockAccount_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SecurityQuestion/UnlockAccount.aspx", false);
        }
        protected String ConvertEmail(String Email)
        {
            string[] spl = Email.Split('@');
            int lenthusername = (spl[0].Length / 2);
            int mod = spl[0].Length % 2;
            if (mod > 0)
            {
                lenthusername = lenthusername - mod;
            }
            string beforemanupilatedemail = "";
            if (lenthusername == 1)
            {
                beforemanupilatedemail = spl[0].Substring(lenthusername);
            }
            else
            {
                beforemanupilatedemail = spl[0].Substring(lenthusername + 1);
            }
            string appendstar = "";
            for (int i = 0; i < beforemanupilatedemail.Length; i++) /*lenthusername*/
            {
                appendstar += "*";
            }
            string manupilatedemail = spl[0].Replace(beforemanupilatedemail, appendstar);

            string[] spl1 = spl[1].Split('.');
            int lenthatname = (spl1[0].Length / 2);
            int modat = spl1[0].Length % 2;
            if (modat > 0)
            {
                lenthatname = lenthatname - modat;
            }
            string beforatemail = "";
            if (lenthatname == 1)
            {
                beforatemail = spl1[0].Substring(lenthatname);
            }
            else
            {
                beforatemail = spl1[0].Substring(lenthatname + 1);
            }
            string appendstar1 = "";
            for (int i = 0; i < beforatemail.Length; i++) /*lenthatname*/
            {
                appendstar1 += "*";
            }

            string manupilatedatemail = spl1[0].Replace(beforatemail, appendstar1);
            string emailid = manupilatedemail;

            DateTime NewTime = DateTime.Now.AddMinutes(30);
            return Email.Replace(spl[0], manupilatedemail).Replace(spl1[0], manupilatedatemail);
        }

        private void ProceedLogin(User user, string ipaddress, string Macaddress1)
        {
            try
            {
                if (user != null)
                {
                    Tuple<bool, List<SP_CheckValidUserForIPAddress_Result>> blockip = null;
                    bool Success = true;
                    try
                    {         
                        if (user.CustomerID != null)
                        {
                            blockip = UserManagement.GetBlockIpAddress((int)user.CustomerID, user.ID, ipaddress);
                            if (!blockip.Item1)
                            {
                                cvLogin.IsValid = false;
                                cvLogin.ErrorMessage = "Your Account is Disabled. Please Contact to Admin.";
                                return;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }

                    bool Auditorexpired = false;
                    if (user.RoleID == 9)
                    {
                        if (user.Enddate == null)
                        {
                            Auditorexpired = true;
                        }
                        if (DateTime.Now.Date > Convert.ToDateTime(user.Enddate).Date || DateTime.Now < Convert.ToDateTime(user.Startdate).Date)
                        {
                            Auditorexpired = true;
                        }
                    }
                    if (!Auditorexpired)
                    {
                        if (!(user.WrongAttempt >= 3)) /*UserManagement.WrongAttemptCount(txtemail.Text.Trim())*/
                        {
                            if (user.IsActive)
                            {
                                Session["userID"] = user.ID;
                                Session["ContactNo"] = user.ContactNumber;
                                Session["Email"] = user.Email;
                                Session["CustomerID_new"] = user.CustomerID;
                                if (user.RoleID == 9)
                                {
                                    Session["Auditstartdate"] = user.AuditStartPeriod;
                                    Session["Auditenddate"] = user.AuditEndPeriod;
                                }
                                DateTime LastPasswordChangedDate = Convert.ToDateTime(user.ChangPasswordDate); /*UserManagement.GetByID(Convert.ToInt32(user.ID)*/
                                DateTime currentDate = DateTime.Now;
                                LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                                int noDays = 0;
                                if (blockip != null && blockip.Item2.Count > 0)
                                {
                                    var data = blockip.Item2.Where(a => a.PasswordExpiry != null).ToList();
                                    if (data.Count > 0)
                                    {
                                        noDays = blockip.Item2.Select(entry => (int)entry.PasswordExpiry).FirstOrDefault();
                                    }
                                    else
                                    {
                                        noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                                    }
                                }
                                else
                                {
                                    noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                                }
                                int customerID = 0;
                                customerID = Convert.ToInt32(user.CustomerID);
                                if (customerID == 6 || customerID == 5 || customerID == 14)
                                {
                                    noDays = 90;
                                }
                                int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);
                                if (dateDifference >= noDays)
                                {
                                    Session["ChangePassword"] = true;
                                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                                }
                                else if (user.LastLoginTime != null)
                                {
                                    //Generate Random Number 6 Digit For OTP.
                                    Random random = new Random();
                                    int value = random.Next(1000000);
                                    if (value == 0)
                                    {
                                        value = random.Next(1000000);
                                    }
                                    Session["ResendOTP"] = Convert.ToString(value);
                                    long Contact;
                                    VerifyOTP OTPData = new VerifyOTP()
                                    {
                                        UserId = Convert.ToInt32(user.ID),
                                        EmailId = txtemail.Text,
                                        OTP = Convert.ToInt32(value),
                                        CreatedOn = DateTime.Now,
                                        IsVerified = false,
                                        IPAddress = ipaddress
                                    };
                                    bool OTPresult = false;
                                    try
                                    {
                                        OTPresult = long.TryParse(user.ContactNumber, out Contact);
                                        if (OTPresult)
                                        {
                                            OTPData.MobileNo = Contact;
                                        }
                                        else
                                        {
                                            OTPData.MobileNo = 0;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        OTPData.MobileNo = 0;
                                        OTPresult = false;
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                    VerifyOTPManagement.Create(OTPData); // Insert Data in OTP Table.                                                   
                                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                                    try
                                    {
                                        if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                                        {
                                            //Send Email on User Mail Id.
                                            if (user.CustomerID != 5)
                                            {
                                                string ReplyEmailAddressName = ConfigurationManager.AppSettings["ReplyEmailAddressName"].ToString();
                                                //EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Teamlease");
                                                EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { user.Email }), null, null, "OTP Verification", "Your One Time Password for Avantis login is:" + Convert.ToString(value) + "<br>" + "<br>" + "Thank you," + "<br>" + "Team Teamlease");
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Success = false;
                                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                   

                                    if (OTPresult && user.RoleID != 12 && user.ContactNumber.Length == 10 && (user.ContactNumber.StartsWith("9") || user.ContactNumber.StartsWith("8") || user.ContactNumber.StartsWith("7") || user.ContactNumber.StartsWith("6")))
                                    {
                                        try
                                        {
                                            if (ConfigurationManager.AppSettings["SkipOTPQA"].ToString() != "1")
                                            {
                                                //Send SMS on User Mobile No.

                                                var data = VerifyOTPManagement.GetSMSConfiguration("Avacom");
                                                if (data != null)
                                                {
                                                    SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", data.TEMPLATEID_DLT_TE_ID, data.authkey, data.Header_sender, data.route);
                                                }
                                                else
                                                {
                                                    SendSms.sendsmsto(user.ContactNumber, "Your One Time Password for Avantis login is: " + Convert.ToString(value) + ".  " + "Thank you, " + "Avantis Team.", "1207161856353769674");
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Success = false;
                                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                        }
                                    }
                                    MaintainLoginDetail objData = new MaintainLoginDetail()
                                    {
                                        UserId = Convert.ToInt32(user.ID),
                                        Email = txtemail.Text,
                                        CreatedOn = DateTime.Now,
                                        IPAddress = ipaddress,
                                        MACAddress = ipaddress,
                                        LoginFrom = "WC",
                                        //ProfileID=user.ID
                                    };
                                    UserManagement.Create(objData);
                                    if (Success)
                                    {
                                        Session["RM"] = cbRememberMe.Checked;
                                        Session["EA"] = Util.CalculateAESHash(txtemail.Text.Trim());
                                        Session["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                                        Response.Redirect("~/Users/OTPVerify.aspx", false);
                                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForSuccess();", true);
                                    }
                                    else
                                    {
                                        if (UserManagement.HasUserSecurityQuestion(user.ID))
                                        {
                                            Session["RM"] = cbRememberMe.Checked;
                                            Session["EA"] = Util.CalculateAESHash(txtemail.Text.Trim());
                                            Session["MA"] = Util.CalculateAESHash(ipaddress.Trim());
                                            Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                        }
                                        else
                                        {
                                            Session["QuestionBank"] = true;
                                            Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                        }
                                    }
                                }
                                else
                                {
                                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                                }
                            }
                            else
                            {
                                cvLogin.IsValid = false;
                                cvLogin.ErrorMessage = "Your Account is Disabled.";
                            }
                        }
                        else
                        {
                            lblAcctLocked.Visible = true;
                            Session["otpvrifyWattp"] = true;
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "SignIn", "settracknewForFailed();", true);
                            //sandesh

                            var plainBytes = Encoding.UTF8.GetBytes(txtemail.Text.Trim());
                            string EncryptEmail = Convert.ToBase64String(DataEncryption.Encrypt(plainBytes, DataEncryption.GetRijndaelManaged(key)));
                            userlogout u1 = new userlogout() { email = EncryptEmail };
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            var EncryptEmailbody = js.Serialize(u1);
                            string requestUrl = "https://api.avantis.co.in/api/v2/logout/";
                            ServicePointManager.Expect100Continue = true;
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            string responseData = WebAPIUtility.Invoke("POST", requestUrl, EncryptEmailbody.ToString());
                        }
                    }
                    else
                    {
                        cvLogin.IsValid = false;
                        cvLogin.ErrorMessage = "Your Account is Disabled.";
                    }
                }
            }
            catch(Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

       public class userlogout
        {
            public string  email {get;set;}
        }
    }
}