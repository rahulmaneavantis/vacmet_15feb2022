﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommonDropdown.ascx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.CommonDropdown" %>
<asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional"
        ><%--OnLoad="upComplianceDetails_Load"--%>
        <ContentTemplate>


            <div style="margin: 5px">

                 <div class="col-md-8 colpadding0 entrycount" style="margin-top: 5px; color:#999;">     
                                    <asp:RadioButtonList runat="server" ID="rdRiskActivityProcess" RepeatDirection="Horizontal" AutoPostBack="true" Style="margin-left: 0px"
                                        RepeatLayout="Flow" OnSelectedIndexChanged="rdRiskActivityProcess_SelectedIndexChanged">
                                        <asp:ListItem Text="Process" Value="Process" Selected="True" />
                                        <asp:ListItem Text="Others" Value="Others" style="margin-left:20px"/>
                                    </asp:RadioButtonList>              
                                </div> 

                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceValidationGroup1" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" class="alert alert-block alert-danger fade in" EnableClientScript="true"
                    ValidationGroup="ComplianceValidationGroup1" Display="None" />
                    <asp:Label ID="Label1" Visible="false" runat="server"></asp:Label>                  
                </div>               
                <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 90px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                    Location</label>
                    <asp:DropDownList runat="server" ID="ddlFilterLocation" AutoPostBack="true"
                    OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" class="form-control m-bot15" Style="width: 220px; float: left">
                    </asp:DropDownList>
                </div>
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 80px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                    Process</label>
                    <asp:DropDownList ID="ddlFilterProcess" runat="server" AutoPostBack="true" class="form-control m-bot15" Style="width: 220px; float: left"
                    OnSelectedIndexChanged="ddlFilterProcess_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ErrorMessage="Please Select Process." ControlToValidate="ddlFilterProcess" ID="rfvProcess"
                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                </div>
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 90px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                    Sub Process</label>
                    <asp:DropDownList ID="ddlFilterSubProcess" runat="server" AutoPostBack="true" class="form-control m-bot15" Style="width: 220px; float: left"
                        OnSelectedIndexChanged="ddlFilterSubProcess_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ErrorMessage="Please Select Sub Process." ControlToValidate="ddlFilterSubProcess" ID="rfvSubProcess"
                    runat="server" InitialValue="-1" ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />  
                </div>                              
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 90px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                    Location Type</label>
                    <asp:DropDownList runat="server" ID="ddlFilterLocationType" AutoPostBack="true"
                        OnSelectedIndexChanged="ddlFilterLocationType_SelectedIndexChanged" class="form-control m-bot15" Style="width: 220px; float: left">
                    </asp:DropDownList>                                  
                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                </div>     
             </div>                                                                                           
       </ContentTemplate>      
    </asp:UpdatePanel>