﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PerformanceSummaryforPerformer.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.Performance_SummaryforPerformer" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>


<script type="text/javascript">

    $(function () {
        $('#divSummaryDetails').dialog({
            height: 500,
            width: 1200,
            autoOpen: false,
            draggable: true,
            title: "Detail View",
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });
    });


    function SelectheaderCheckboxes(headerchk, gridname) {
        var rolecolumn;
        var chkheaderid = headerchk.id.split("_");


        var gvcheck = document.getElementById("<%=grdSummaryDetails.ClientID %>");


        var i;

        if (headerchk.checked) {
            for (i = 0; i < gvcheck.rows.length; i++) {
                gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
            }
        }

        else {
            for (i = 0; i < gvcheck.rows.length; i++) {
                gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
            }
        }
    }

    function Selectchildcheckboxes(header, gridname) {
        var i;
        var count = 0;
        var rolecolumn;

        var gvcheck = document.getElementById("<%=grdSummaryDetails.ClientID %>");

        var headerchk = document.getElementById(header);
        var chkheaderid = header.split("_");

        var rowcount = gvcheck.rows.length;

        for (i = 1; i < gvcheck.rows.length - 1; i++) {
            if (gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked) {
                count++;
            }
        }

        if (count == gvcheck.rows.length - 2) {
            headerchk.checked = true;
        }
        else {
            headerchk.checked = false;
        }
    }


    function initializeDatePicker(date) {

        var startDate = new Date();
        $("#<%= txtStartDate.ClientID %>").datepicker({
            dateFormat: 'dd-mm-yy',
            setDate: '01-04-14',
            defaultDate: startDate,
            numberOfMonths: 1,
            onClose: function (startDate) {
                $("#<%= chrtDelayedCompliances.ClientID %>").val();
            }
        });

            $("#<%= txtEndDate.ClientID %>").datepicker({
            dateFormat: 'dd-mm-yy',
            defaultDate: startDate,
            numberOfMonths: 1
        });


        if (date != null) {
            $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date);
            $("#<%= txtEndDate.ClientID %>").datepicker("option", "defaultDate", date);
        }
    }

    function initializeJQueryUI(textBoxID, divID) {
        $("#" + textBoxID).unbind('click');

        $("#" + textBoxID).click(function () {
            $("#" + divID).toggle("blind", null, 500, function () { });
        });
    }

    function DownloadDoc() {
        var count = 0;
        var gvcheck = document.getElementById("<%=grdSummaryDetails.ClientID %>");
        for (var i = 1; i < gvcheck.rows.length; i++) {
            if (gvcheck.rows[i].cells[0].getElementsByTagName("INPUT")[0].checked)
                count++;
        }
        if (count > 0)
            return true;
        else {
            alert("please select at least one record to download.");
            return false;
        }
    }

    function initializeCombobox() {
        $("#<%= ddlLocations.ClientID %>").combobox();
    }


</script>

<asp:UpdateProgress ID="updateProgress" runat="server">
    <ProgressTemplate>
        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel ID="PerformanceSummaryforPerformer" runat="server">
    <ContentTemplate>
        <div style="margin-bottom: 4px">
            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
        </div>
        <div style="margin-left: 39px; margin-top: 10px;">
            <asp:DataList runat="server" ID="dlFilters" RepeatDirection="Horizontal" OnSelectedIndexChanged="dlFilters_SelectedIndexChanged"
                DataKeyField="ID">
                <SeparatorTemplate>
                    <span style="margin: 0 5px 0 5px">|</span>
                </SeparatorTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="select" Style="text-decoration: none; color: Black">  
                                    <%# DataBinder.Eval(Container.DataItem, "Name") %>
                    </asp:LinkButton>
                </ItemTemplate>
                <ItemStyle Font-Names="Tahoma" Font-Size="13px" VerticalAlign="Middle" />
                <SelectedItemStyle Font-Names="Tahoma" Font-Size="13px" Font-Bold="True" VerticalAlign="Middle"
                    Font-Underline="true" />
            </asp:DataList>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="grdPanelSummary" runat="server" OnLoad="grdPanelSummary_Load">
    <ContentTemplate>
        <div>
            <div style="width: 100%; float: left">
                <div style="margin: 20px 0px -22px 40px; float: left">
                    <div style="margin-bottom: 7px" runat="server" id="divfilterforLocation">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                            Location</label>
                        <asp:DropDownList runat="server" ID="ddlLocations" Style="padding: 0px; margin: 0px; height: 22px; width: 204px;"
                            CssClass="txtbox" />

                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                            From Date</label>
                        <asp:TextBox runat="server" ID="txtStartDate" Style="height: 16px; width: 200px;"
                            ReadOnly="true" MaxLength="200" />
                    </div>
                    <div style="margin-bottom: 50px">
                        <label style="width: 100px; display: block; float: left; font-size: 13px; color: #333;">
                            To Date</label>
                        <asp:TextBox runat="server" ID="txtEndDate" Style="height: 16px; width: 200px;" ReadOnly="true"
                            MaxLength="200" />
                        <asp:Button ID="btnGetSummary" runat="server" Text="Get Summary" Style="height: 25px; width: 100px;"
                            OnClick="btnGetSummary_Click" />
                    </div>
                    <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" SelectedIndex="0"
                        AllowSorting="true" OnRowDataBound="grdComplianceTransactions_RowDataBound" GridLines="Vertical" OnRowCommand="grdComplianceTransactions_RowCommand"
                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" OnRowCreated="grdComplianceTransactions_RowCreated"
                        BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" OnSelectedIndexChanged="OnSelectedIndexChanged"
                        Width="500px" Size="12px" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging"
                        DataKeyNames="RiskCatagory">

                        <Columns>
                            <asp:BoundField DataField="RiskCatagory" HeaderText="" ItemStyle-Height="20px" />
                            <asp:TemplateField HeaderText="Delayed" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnDelayed" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# Eval("ID") + "; Delayed;" + Eval("RiskCatagory")%>'
                                        Text='<%# Eval("Delayed") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="In Time" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnInTime" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# Eval("ID") + "; InTime;" + Eval("RiskCatagory")%>'
                                        Text='<%# Eval("InTime") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pending" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnPending" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# Eval("ID") + "; Pending;" + Eval("RiskCatagory")%>'
                                        Text='<%# Eval("Pending") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                        <PagerSettings Position="Top" />
                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                        <AlternatingRowStyle BackColor="#E6EFF7" />
                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div style="float: left; margin-top: 35px; margin-left: 99px;">
                    <table style="height: 300px">
                        <tr>
                            <td width="250px" style="text-align: center">
                                <asp:Label ID="titleDelayedCompliances" runat="server" Text="" Style="font-family: Verdana; font-size: 12px;"></asp:Label>
                                <asp:Chart ID="chrtDelayedCompliances" runat="server" Width="300px" Height="300px" ImageStorageMode="UseImageLocation"
                                    ImageLocation="~/TempImages/Chart_#UID">


                                    <Legends>
                                        <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                            LegendStyle="Table" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartType="Doughnut" XValueMember="Name" YValueMembers="Quantity" IsValueShownAsLabel="true">
                                            <EmptyPointStyle IsValueShownAsLabel="false" IsVisibleInLegend="false" />
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Area3DStyle-Enable3D="false">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>

                            </td>
                            <td style="text-align: center; padding-left: 50px;" width="250px">
                                <asp:Label ID="titlePendingCompliances" runat="server" Text="" Style="font-family: Verdana; font-size: 12px;"></asp:Label>
                                <asp:Chart ID="chrtPendingCompliances" runat="server" Width="300px" Height="300px" ImageStorageMode="UseImageLocation"
                                    ImageLocation="~/TempImages/Chart_#UID">

                                    <Legends>
                                        <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                            LegendStyle="Table" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartType="Doughnut" XValueMember="Name" YValueMembers="Quantity" IsValueShownAsLabel="true">
                                            <EmptyPointStyle IsValueShownAsLabel="false" IsVisibleInLegend="false" />
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Area3DStyle-Enable3D="false">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <%-- for Location filter--%>
            <div id="divLocation" runat="server" visible="false" style="width: 100%; float: left">
                <div style="margin: 100px 0px 0px 40px; float: left">
                    <asp:GridView runat="server" ID="grdLocationCatagory" AutoGenerateColumns="false" SelectedIndex="0"
                        AllowSorting="true" GridLines="Vertical" OnRowDataBound="grdLocationCatagory_RowDataBound" OnRowCommand="grdLocationCatagory_RowCommand"
                        BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" OnRowCreated="grdLocationCatagory_RowCreated"
                        BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" OnSelectedIndexChanged="grdLocationCatagory_SelectedIndexChanged"
                        Width="500px" Size="12px"
                        DataKeyNames="RiskCatagory">

                        <Columns>
                            <asp:BoundField DataField="RiskCatagory" HeaderText="" ItemStyle-Height="20px" />
                            <asp:TemplateField HeaderText="Delayed" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnDelayed" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# Eval("ID") + "; Delayed;" + Eval("RiskCatagory") %>'
                                        Text='<%# Eval("Delayed") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="In Time" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnInTime" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# Eval("ID") + "; InTime;" + Eval("RiskCatagory")%>'
                                        Text='<%# Eval("InTime") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pending" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnPending" runat="server" CommandName="DRILLDOWN" CommandArgument='<%# Eval("ID") + "; Pending;"  + Eval("RiskCatagory")%>'
                                        Text='<%# Eval("Pending") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                        <AlternatingRowStyle BackColor="#E6EFF7" />
                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div style="margin: 50px 0px 0px 99px; float: left">
                    <table style="height: 300px">
                        <tr>
                            <td width="250px" style="text-align: center">
                                <asp:Label ID="titleDelayedCompliances1" runat="server" Text="Category Wise Delayed Compliances" Style="font-family: Verdana; font-size: 12px;"></asp:Label>
                                <asp:Chart ID="chrtLocationCatagoryDelay" runat="server" Width="300px" Height="300px" ImageStorageMode="UseImageLocation"
                                    ImageLocation="~/TempImages/Chart_#UID">

                                    <Legends>
                                        <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                            LegendStyle="Table" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartType="Doughnut" XValueMember="Name" YValueMembers="Quantity" IsValueShownAsLabel="true">
                                            <EmptyPointStyle IsValueShownAsLabel="false" IsVisibleInLegend="false" />
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Area3DStyle-Enable3D="false">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </td>
                            <td style="padding-left: 50px; text-align: center" width="250px">
                                <asp:Label ID="titlePendingCompliances1" runat="server" Text="Category Wise Pending Compliances" Style="font-family: Verdana; font-size: 12px;"></asp:Label>
                                <asp:Chart ID="chrtLocationCatagoryPending" runat="server" Width="300px" Height="300px" ImageStorageMode="UseImageLocation"
                                    ImageLocation="~/TempImages/Chart_#UID">

                                    <Legends>
                                        <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                            LegendStyle="Table" />
                                    </Legends>
                                    <Series>
                                        <asp:Series ChartType="Doughnut" XValueMember="Name" YValueMembers="Quantity" IsValueShownAsLabel="true">
                                            <EmptyPointStyle IsValueShownAsLabel="false" IsVisibleInLegend="false" />
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Area3DStyle-Enable3D="false">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>

                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="float: left; margin: 10px 0px 0px 40px; padding-top: 40px; padding-bottom: 20px;">
                <asp:Label ID="lblNonComplincePenalty" runat="server" Text="" Style="font-family: Verdana; font-size: 14; font-weight: bold"></asp:Label>
                <asp:Label ID="lblNote" runat="server" Text="*Please click on a row to generate the graph." Style="font-family: Verdana; font-size: 12; float: left; padding-top: 10px;"
                    Visible="false"></asp:Label>
            </div>
        </div>
    </ContentTemplate>

</asp:UpdatePanel>
<div id="divSummaryDetails">
    <div style="float: right; margin-bottom: 5px; margin-left: 10px">
        <asp:Button Text="Download" runat="server" ID="btnDownload" OnClick="btnDownload_Click" ToolTip="Click here to download documnet for selected compliances."
            OnClientClick="javascript:return DownloadDoc();" CssClass="button" ValidationGroup="ModifyAsignmentValidationGroup" CausesValidation="false" />
    </div>
    <div style="float: right">
        <asp:LinkButton runat="server" ID="lbtnExportExcel" Style="margin-top: 15px; margin-top: -5px;" OnClick="lbtnExportExcel_Click"><img src="../Images/excel.png" alt="Export to Excel"
                    title="Export to Excel" width="30px" height="30px"/></asp:LinkButton>
    </div>
    <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="margin-bottom: 10px; font-weight: bold; font-size: 12px;">
                <asp:Label runat="server" ID="lblDetailViewTitle"></asp:Label>
            </div>

            <asp:GridView runat="server" ID="grdSummaryDetails" AutoGenerateColumns="false" GridLines="Vertical" AllowSorting="true"
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" OnRowCommand="grdSummaryDetails_RowCommand" OnPageIndexChanging="grdSummaryDetails_PageIndexChanging"
                CellPadding="4" ForeColor="Black" AllowPaging="true" PageSize="12" Width="100%" OnRowDataBound="grdSummaryDetails_RowDataBound"
                Font-Size="12px" DataKeyNames="ComplianceInstanceID">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkCompliancesHeader" runat="server" onclick="javascript:SelectheaderCheckboxes(this,'grdSummaryDetails')" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkCompliances" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Compliance" ItemStyle-Width="30%" SortExpression="ShortDescription">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 450px;">
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Branch" HeaderText="Location" SortExpression="Branch" />
                    <asp:TemplateField HeaderText="Due Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn">
                        <ItemTemplate>
                            <%# Eval("ScheduledOn")!= null?((DateTime)Eval("ScheduledOn")).ToString("dd-MMM-yyyy"):""%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ForMonth" HeaderText="For Month" ItemStyle-HorizontalAlign="Center" SortExpression="ForMonth" />
                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                    <asp:BoundField DataField="RiskCategory" HeaderText="Risk Category" SortExpression="RiskCategory" />
                    <asp:TemplateField ItemStyle-Width="150px" HeaderText="Documents" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="lblDownLoadfile" runat="server" CommandName="Download" CommandArgument='<%# Eval("ScheduledOnID") %>'
                                ToolTip="Please download file from here." Visible='<%# ViewDownload((int)Eval("ComplianceStatusID")) %>'>Download</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

