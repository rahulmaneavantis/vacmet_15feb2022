﻿    <%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true"
    CodeBehind="frmUpcomingCompliancess.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.frmUpcomingCompliancess" EnableEventValidation="false" %>

<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>

<%--<%@ Register Src="~/Controls/ComplianceStatusTransaction.ascx" TagName="ComplianceStatusTransaction"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/ComplianceReviewerStatusTransaction.ascx" TagName="ComplianceReviewerStatusTransaction"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/InternalComplianceStatusTransaction.ascx" TagName="InternalComplianceStatusTransaction"
    TagPrefix="vit" %>
<%@ Register Src="~/Controls/InternalComplianceReviewerStatusTransaction.ascx" TagName="InternalComplianceReviewerStatusTransaction"
    TagPrefix="vit" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <%--<script type="text/javascript">
    $(document).ready(function () {
        $('#basic').simpleTreeTable({
            collapsed: true
        });
    });
</script>--%>

    <script type="text/javascript">

        //function fopenCotract() {

        //    window.open('/InternalCompliance/contract-management1.html', "ModalPopUp", "toolbar=no, scrollbars=no, location=no, statusbar=no, menubar=no, resizable=0, width=1250, height=700, left = 10, top=10")
        //}

        //function fshowCotract() {
        //    $('#linkc').hide();
        //    if (document.getElementById('BodyContent_Iscontract').checked) {
        //        $('#linkc').show();
        //    }
        //}

        $(document).on("click", "#ContentPlaceHolder1_UpdatePanel1", function (event) {

            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            }
            else if (event.target.id != "ContentPlaceHolder1_tbxFilterLocation") {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == "ContentPlaceHolder1_tbxFilterLocation") {
                $("#ContentPlaceHolder1_tbxFilterLocation").unbind('click');

                $("#ContentPlaceHolder1_tbxFilterLocation").click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });
        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            var filterbytype = ReadQuerySt('filter');
            var filterbyrole = ReadQuerySt('type');
            if (filterbytype == '') {
                fhead('My Workspace');
            } else {
                $('#pagetype').css("font-size", "20px")
                if (filterbytype == 'PendingForReview') {
                    filterbytype = 'Pending For Review';
                } else if (filterbytype == 'DueButNotSubmitted') {
                    filterbytype = 'Due But Not Submitted';
                }
                fhead('My Workspace / ' + filterbytype + ' ' + filterbyrole);
            }
        });

        function initializeJQueryUI(textBoxID, divID) {

            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

  function initializeDatePicker11(date2) {
            var startDate = new Date();
            $('#<%= txtStartDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                maxDate: startDate,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true

            });

            if (date2 != null) {
                $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date2);
            }
        }

        function initializeDatePicker12(date1) {
            var startDate = new Date();
            $('#<%= txtEndDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',
                //maxDate: startDate,
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true
            });

            if (date1 != null) {
                $("#<%= txtEndDate.ClientID %>").datepicker("option", "defaultDate", date1);
            }
        }

        function hidediv() {
            var div = document.getElementById('AdvanceSearch');
            div.style.display == "none" ? "block" : "none";
            $('.modal-backdrop').hide();
            return true;
        }

        function openModal1(obj) {
            debugger;
            var scId = $(obj).attr('data-sid');
            var InId = $(obj).attr('data-inid');

            $('#ComplaincePerformer').modal('show');
            $('#iPerformerFrame').attr('src','/controls/compliancestatusperformer.aspx?sId=' + scId + '&InId=' + InId);

            return true;
        }

        function openModal() {
            if (Displays() == true) {
                $('#ComplaincePerformer').modal('show');
            }
            return true;
        }
       
        //function openModalInternalPer() {
        //    if (Displays() == true) {
        //        $('#ComplainceInternalPerformaer').modal('show');
        //    }
        //    return true;
        //}
        function openModalInternalPer1(obj) {
            debugger;
            var scId = $(obj).attr('data-sid');
            var InId = $(obj).attr('data-inid');

            $('#ComplainceInternalPerformaer').modal('show');
            $('#iInternalPerformerFrame').attr('src', '/controls/InternalComplianceStatusperformer.aspx?sId=' + scId + '&InId=' + InId);

            return true;
        }
        function openModalReviewer() {

            $('#ComplainceReviewer').modal('show');
            return true;
        }
        function openModalReviewer1(obj) {
            debugger;
            var scId = $(obj).attr('data-sid');
            var InId = $(obj).attr('data-inid');

            $('#ComplainceReviewer').modal('show');
            $('#iReviewerFrame').attr('src', '/controls/compliancestatusreviewer.aspx?sId=' + scId + '&InId=' + InId);

            return true;
        }
        function openModalInternalReviewer() {
            $('#ComplainceInternalReviewer').modal('show');
            return true;
        }

        function openModalInternalReviewer1(obj) {
            debugger;
            var scId = $(obj).attr('data-sid');
            var InId = $(obj).attr('data-inid');
            $('#ComplainceInternalReviewer').modal('show');
            $('#iInternalReviewerFrame').attr('src', '/controls/InternalComplianceStatusReviewer.aspx?sId=' + scId + '&InId=' + InId);
            return true;
        }

        function CloseModalInternalPerformer() {
            alert();
               $('#ComplainceInternalPerformaer').modal('hide');
            document.getElementById('<%= lnkBtnBindGrid.ClientID %>').click();
            return true;
           }

        function CloseModalInternalReviewer() {
            alert(2);
            $('#ComplainceInternalReviewer').modal('hide');
            document.getElementById('<%= lnkBtnBindGrid.ClientID %>').click();
            return true;
        }

         function CloseModalPerFormer() {
             $('#ComplaincePerformer').modal('hide');
             alert(1);
             //document.getElementById('<%= lnkBtnBindGrid.ClientID %>').click();
             $("#grid").data("kendoGrid").dataSource.read();
             $("#grid").data("kendoGrid").refresh();

             fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
             fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');


             $("#grid1").data("kendoGrid").dataSource.read();
             $("#grid1").data("kendoGrid").refresh();

             fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')
             fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');
             //return true;
            return true;
         }

        function fcloseandcallcal() {
           
            $('#ComplaincePerformer').modal('hide');

            $("#grid").data("kendoGrid").dataSource.read();
            $("#grid").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
            fCreateStoryBoard('dropdownlistRisk', 'filterrisk', 'risk');


            $("#grid1").data("kendoGrid").dataSource.read();
            $("#grid1").data("kendoGrid").refresh();

            fCreateStoryBoard('dropdowntree1', 'filtersstoryboard1', 'loc1')
            fCreateStoryBoard('dropdownlistRisk1', 'filterrisk1', 'risk1');
            return true;
        }
        function CloseModalReviewer() {
            $('#ComplainceReviewer').modal('hide');
            document.getElementById('<%= lnkBtnBindGrid.ClientID %>').click();
            return true;
          }

        function openModalEventBased() {

            $('#ComplainceEventBased').modal('show');
            return true;
        }
        function openModalEventBasedReviewer() {

            $('#ComplainceEventBasedReviewer').modal('show');
            return true;
        }
        
        function OpenDocumentOverviewpup(ActID,ID) {
            $('#divOverView').modal('show');
            $('#OverViews').attr('width', '1150px');
            $('#OverViews').attr('height', '600px');
            $('.modal-dialog').css('width', '1200px');
            $('#OverViews').attr('src', "../Common/ActOverview.aspx?ActID=" + ActID +"&ID="+ID);
        }

        function OpenDocumentDowmloadOverviewpup(ActID, ID) {
            $('#DownloadViews').attr('src', "../Common/DownloadActOverview.aspx?ActID=" + ActID + "&ID=" + ID);
        }
   
    </script>

    <style type="text/css">

        .clspenaltysave
        {
            font-weight:bold;
            margin-left :15px;
        }
        .btnss {
            background-image: url(../Images/edit_icon_new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > thead > tr > th > a {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }

          .Inforamative{
        color:blue   !important;
        border-top: 1px solid #dddddd !important;
    }
    tr.Inforamative > td{
        color:blue   !important;
        border-top: 1px solid #dddddd !important;
    }
       

        .circle {
            /*background-color: green;*/
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            margin-right: 20px;
        }

     /*.modal-body {
    max-height: calc(100vh - 210px);
    overflow-y: auto;
}*/

    </style>

    <script type="text/javascript">
        function setTabActive(id) {
            $('.dummyval').removeClass("active");
            $('#' + id).addClass("active");
        };
    

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
             
               <asp:LinkButton ID="lnkBtnBindGrid" OnClick="lnkBtnBindGrid_Click" Style="float: right; display: none;" Width="100%" runat="server"> </asp:LinkButton>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">
                                <%--Header Section--%>
                                <header class="panel-heading tab-bg-primary ">
                                          <ul id="rblRole1" class="nav nav-tabs">
                                              <%if (roles != null)
                                                  {
                                                   %>                                           
                                               <% if (roles.Contains(3))%>
                                                <%{%>
                                                   <li class="dummyval">                                                     
                                                        <asp:LinkButton ID="liPerformer" OnClick="btnPerformer_Click" runat="server">Performer</asp:LinkButton>
                                                    </li>
                                               <%}%>
                                               <% if (roles.Contains(4))%>
                                                <%{%>
                                                  <li class="dummyval">                                                      
                                                           <asp:LinkButton ID="liReviewer" OnClick="btnReviewer_Click" runat="server">Reviewer</asp:LinkButton>
                                                    </li>
                                                <%}%>    
                                                <%}%>                                    
                                        </ul>
                                </header>
                                <div class="clearfix"></div>

                                <div class="panel-body">
                                    <div class="col-md-12 colpadding0">

                                        <div class="col-md-12 colpadding0" style="text-align: right; float: right">
                                            
                                            <div class="col-md-10 colpadding0" style="width:69.666667%;">
                                                <div class="col-md-2 colpadding0 entrycount">
                                            <div class="col-md-3 colpadding0">
                                                <p style="color:#999; margin-top:5px"> Show </p>
                                            </div>
                                            <asp:DropDownList runat="server" ID="ddlpageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlpageSize_SelectedIndexChanged" class="form-control m-bot15" Style="width: 64px; float: left;margin-left:10px;">
                                                <asp:ListItem Text="5"/>
                                                <asp:ListItem Text="10" Selected="True" />
                                                <asp:ListItem Text="20" />
                                                <asp:ListItem Text="50" />
                                            </asp:DropDownList>
                                        </div>
                                                  
                                                <div class="col-md-2 colpadding0 entrycount" style="float:left;margin-right: -1%;">
                                                     <asp:DropDownList runat="server" ID="ddlComplianceType" class="form-control m-bot15 search-select" style="width:105px;" 
                                                         OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged" AutoPostBack="true">                
                                                        <asp:ListItem Text="Statutory" Value="-1" />
                                                        <asp:ListItem Text="Internal" Value="0" />
                                                        <asp:ListItem Text="Event Based" Value="1" />
                                                    </asp:DropDownList>         
                                                    </div>
                                                   <div class="col-md-2 colpadding0 entrycount" style="float:left;margin-right: 1%; margin-left:5px; width:13% !important;">
                                                    <asp:DropDownList runat="server" ID="ddlRiskType" class="form-control m-bot15 search-select"  style="width:100%;" >               
                                                        <asp:ListItem Text="Risk" Value="-1" />
                                                        <asp:ListItem Text="High" Value="0" />                                                       
                                                        <asp:ListItem Text="Medium" Value="1" />
                                                         <asp:ListItem Text="Low" Value="2" />
                                                    </asp:DropDownList>
                                                   </div>
                                                   <div class="col-md-3 colpadding0 entrycount" style="float:left;margin-right: 1%; width:20%">
                                                        <asp:DropDownList runat="server" ID="ddlStatus" class="form-control m-bot15 search-select"  style="width:100%; max-width: 135px !important;" >
                                                        </asp:DropDownList>
                                                   </div>

                                                  <div class="col-md-5 colpadding0 entrycount" style="float:left;margin-right: -2%; width:33.667% !important">
                                                     <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocation" Style="padding: 0px;padding-left: 10px; margin: 0px; height: 35px; width: 320px; border: 1px solid #c7c7cc;border-radius: 4px;color:#8e8e93"
                                                        CssClass="txtbox" />   
                                                    <div style="margin-left: 1px; position: absolute; z-index: 10;display: inherit;" id="divFilterLocation">
                                                        <asp:TreeView runat="server" ID="tvFilterLocation"   SelectedNodeStyle-Font-Bold="true"  Width="320px"   NodeStyle-ForeColor="#8e8e93"
                                                        Style="overflow: auto; border-left:1px solid #c7c7cc; border-right:1px solid #c7c7cc; border-bottom:1px solid #c7c7cc; background-color: #ffffff; color:#8e8e93 !important;" ShowLines="true" 
                                                            OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                                        </asp:TreeView>
                                                    </div>  
                                               </div>
                                            </div>    
                                             <div class="col-md-2 colpadding0" style="width: 22.333333%; float:right">
                                                <div class="col-md-6 colpadding0">                                                    
                                                    <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search" OnClick="btnSearch_Click" Style="margin-left:33px !important;" runat="server" Text="Apply" />
                                                </div>

                                                <div class="col-md-6" style="margin-left: -10%;">
                                                    <button class="form-control m-bot15" type="button" style="margin-top: -40px; position: absolute; width: 150px;"  data-toggle="dropdown">More Links
                                                                <span class="caret" style="border-top-color: #a4a7ab"></span></button>
                                                                <ul class="dropdown-menu" style="top:0px !important;width:151px !important;margin: -9px 15px 0 !important; min-width:150px !important; ">                                                   
                                                                  <li style="text-align: left !important;">  <a id="btnPenaltyReviewer" runat="server"  href="../Penalty/PenaltyUpdationReviewer.aspx">Update Penalty</a></li>                               
                                                                  <li style="text-align: left !important;">   <a id="btnPenalty" runat="server"  href="../Penalty/PenaltyUpdation.aspx">Update Penalty</a>  </li>                
                                                                   <% if (roles.Contains(3))%>
                                                                   <%{%>
                                                                    <li style="text-align: left !important;">   <a id="btnReviseCompliance" runat="server"  href="../Compliances/ReviseCompliances.aspx">Revise&nbsp;Compliance</a>   </li>
                                                                    <li style="text-align: left !important;">    <a id="btnReviseComplianceInternal" runat="server"  href="../InternalCompliance/ReviseCompliancesInternal.aspx">Revise&nbsp;Compliance</a></li>                                            
                                                                    
                                                                   <%}%>
                                                                    <li style="text-align: left !important;">   <a id="bntPerformerOnLeave" runat="server"  href="../Users/PerformerOnLeave.aspx">My Leave</a></li>
                                                                    <li style="text-align: left !important;">   <a id="btnReassignPerformer" runat="server"  href="../Compliances/ReassignReviewerToPerformer.aspx">Reassign&nbsp;Performer</a></li>  
                                                                  <li style="text-align: left !important;">   <a id="btnTaskDetails" runat="server"  href="../Task/myTask.aspx?type=Statutory">Tasks</a></li>
                                                                  <li style="text-align: left !important;">   <a id="bntLicneseWorkspace" runat="server"  href="../Compliances/ComplianceLicenseList.aspx">License Workspace</a></li>
                                                                  <%--  <li>  <a id="btnTaskDetails" runat="server"  href="../Task/PerformerTaskDetail.aspx">Task Details</a></li>   --%>
                                                                    </ul>                                                                                                                                                                                                                                 
                                                    <a class="btn btn-advanceSearch" data-toggle="modal" href="#AdvanceSearch" title="Search" style="width: 150px;">Advanced Search</a>
                                                </div>
                                            </div>              
                                        </div>

                                        
                                         <div class="clearfix"> 
                                         <div class="col-md-12 colpadding0" style="text-align: right; float: right">
                                         <div id="divEvent" class="col-md-10 colpadding0" style="text-align: right; float: right" runat="server" visible="false">
                                                    <div style="float:left;margin-right: 2%;">
                                                     <asp:DropDownList runat="server" ID="ddlEvent"  OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" AutoPostBack="true" class="form-control m-bot15" style="width:255px;">                
                                                    </asp:DropDownList>         
                                                    </div>
                                                    <div style="float:left;margin-right: 2%;">
                                                    <asp:DropDownList runat="server" ID="ddlEventNature"  class="form-control m-bot15" style="width:255px;" >               
                                                    </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div> 

                                        <!--advance search starts-->   
                                         <div class="col-md-12 AdvanceSearchScrum">                                  
                                        <div id="divAdvSearch" runat="server" visible="false">
                                        <p>
                                            <asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label></p>
                                        <p>
                                         <asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceSearch_Click" runat="server">Clear Advanced Search</asp:LinkButton>
                                        </p>
                                       </div>

                                         <div runat="server" id="DivRecordsScrum" style="float: right;">
             
             <p style="padding-right: 0px !Important;">
                    <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                </p>
            </div>
   </div>
                                        <div class="modal fade" id="AdvanceSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" style="width: 1000px">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    </div>
                                                    <div class="modal-body" style="margin-left: 40px;">
                                                        <h2 style="text-align: center">Advanced Search</h2>
                                                        
                                                        <div class="col-md-12 colpadding0">
                                                            <div class="table-advanceSearch-selectOpt">
                                                                <asp:DropDownList runat="server" ID="ddlType" class="form-control m-bot15">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="table-advanceSearch-selectOpt">
                                                                <asp:DropDownList runat="server" ID="ddlCategory" class="form-control m-bot15">
                                                                </asp:DropDownList>
                                                            </div>
                                                           <asp:Panel ID="PanelAct" runat="server">
                                                            <div id="DivAct" runat="server" class="table-advanceSearch-selectOpt">
                                                                <asp:DropDownList runat="server" ID="ddlAct" class="form-control m-bot15">
                                                                </asp:DropDownList>
                                                            </div>
                                                            </asp:Panel>
                                                             <asp:Panel ID="Panelsubtype" runat="server">
                                                            <div id="DivComplianceSubTypeList" runat="server" class="table-advanceSearch-selectOpt">
                                                                <asp:DropDownList runat="server" ID="ddlComplianceSubType" class="form-control m-bot15">
                                                                </asp:DropDownList>
                                                            </div>
                                                            </asp:Panel>
                                                              <div class="clearfix"></div>
                                                            <asp:Panel ID="PanelPerformerList" runat="server">
                                                            <div id="Div1" runat="server" class="table-advanceSearch-selectOpt">
                                                                <asp:DropDownList runat="server" ID="ddlperformer" class="form-control m-bot15">
                                                                </asp:DropDownList>
                                                            </div></asp:Panel>
                                                               <asp:Panel ID="PanelSearchType" runat="server">
                                                            <div id="Div2" runat="server" class="table-advanceSearch-selectOpt">
                                                                 <asp:TextBox runat="server"  style="padding-left:7px;" placeholder="Type to Filter" class="form-group form-control" ID="txtSearchType" CssClass="form-control"  onkeydown = "return (event.keyCode!=13);"/>
                                                            </div></asp:Panel>
                                                             <asp:Panel ID="Panel1" runat="server">
                                                                <div id="Div3" runat="server" class="table-advanceSearch-selectOpt">
                                                                 <asp:TextBox runat="server" Height="35px" Width="200px" style="padding-left:7px; border-radius: 5px;" placeholder="From Date" class="form-group form-control" ID="txtStartDate" CssClass="StartDate"/>
                                                              </div>  </asp:Panel>

                                                            <asp:Panel ID="Panel2" runat="server">
                                                                <div id="Div4" runat="server" class="table-advanceSearch-selectOpt">
                                                                <asp:TextBox runat="server" Height="35px" Width="200px" style="padding-left:7px; border-radius: 5px;" placeholder="To Date" class="form-group form-control" ID="txtEndDate" CssClass="StartDate"/>
                                                               </div> </asp:Panel>
                                                          
                                                            <div class="clearfix"></div>
                                                             <div class="table-advanceSearch-buttons" style="height:30px;margin:10px auto;">
                                                                 <table id="tblSearch"> 
                                                                    <tr>
                                                                         <td>
                                                                           <asp:Button  ID="btnAdvSearch" Text="Search"  class="btn btn-search" OnClick="btnAdvSearch_Click" runat="server" OnClientClick="return hidediv();" />   
                                                                         </td>
                                                                         <td>
                                                                         <button id ="btnAdvClose" type="button" class="btn btn-search" data-dismiss="modal">Close</button>
                                                                         </td>
                                                                    </tr>
                                                                </table>
                                                             </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--advance search ends-->
                                    </div>
                                </div>
                                <div class="tab-content ">
                                   <div runat="server" id="performerdocuments"  class="tab-pane active">

                                    <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" AllowSorting="true"
                                        GridLines="None" CssClass ="table" AllowPaging="True" PageSize="10" OnSorting="grdComplianceTransactions_Sorting"
                                        DataKeyNames="ScheduledOnID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging"
                                        OnRowDataBound="grdComplianceTransactions_RowDataBound" OnRowCommand="grdComplianceTransactions_RowCommand" OnRowCreated="grdComplianceTransactions_RowCreated">
                                        <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                            <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                <asp:Label ID="LabelD" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                 <asp:Label ID="lblInterimdays" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Interimdays") %>' Visible="false" ></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                <asp:Label ID="lbllocation" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>' ></asp:Label>
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                                     
                                        <asp:TemplateField HeaderText="Reviewer">
                                            <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">                                                                                                                                                                                                 
                                                        <asp:Label ID="lblReviewer" runat="server"  data-toggle="tooltip" data-placement="bottom" Text='<%# GetReviewer((long)Eval("ComplianceInstanceID")) %>' ToolTip='<%#Reviewername%>'></asp:Label>                                                             
                                                    </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Due Date">
                                            <ItemTemplate>
                                                 <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">   
                                                       <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom"  Text=' <%# Convert.ToDateTime(Eval("PerformerScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Event Name">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">     
                                                    <asp:Label ID="lblEventName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("EventName") %>' ToolTip='<%# Eval("EventName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Event Nature">
                                            <ItemTemplate>
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                <asp:Label ID="lblEventNature" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("EventNature") %>' ToolTip='<%# Eval("EventNature") %>'></asp:Label>
                                            </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>  
                                        <asp:TemplateField HeaderText="Period">
                                            <ItemTemplate>
                                                <%# Eval("ForMonth") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                                            
                                        <asp:TemplateField HeaderText="Status">                                                                
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">                                                         
                                                    <asp:Label ID="lblStatus" runat="server"  data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>                                                             
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">     
                                            <ItemTemplate> 
                                                <asp:Button runat="server"  OnClientClick="return openModal1(this)" ID="btnChangeStatus" data-SId='<%# Eval("ScheduledOnID")%>' data-InId='<%#Eval("ComplianceInstanceID")%>'  CssClass="btnss"  Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("ComplianceStatusID")) %>'
                                                 CommandArgument='<%# Eval("ScheduledOnID") + "," + Eval("ComplianceInstanceID") + "," + Eval("Interimdays") + "," + Eval("Status") + "," + Eval("PerformerScheduledOn")  %>' />                                                           
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Right" />
                                        <RowStyle CssClass="clsROWgrid"   />
                                        <HeaderStyle CssClass="clsheadergrid"    />
                                        <PagerTemplate>
                                            <table style="display: none">
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>
                                        </PagerTemplate>
                                         <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                        </asp:GridView> 
                                                     
                                    <asp:GridView runat="server" ID="grdInternalComplinacePerformer" AutoGenerateColumns="false" AllowSorting="true"
                                        GridLines="None" BorderWidth="0px" CssClass ="table"  PageSize="10" DataKeyNames="InternalScheduledOnID" 
                                        OnPageIndexChanging="grdInternalComplinacePerformer_PageIndexChanging" AllowPaging="True" 
                                        OnRowDataBound="grdInternalComplinacePerformer_RowDataBound"
                                         OnRowCommand="grdInternalComplinacePerformer_RowCommand"
                                         OnRowCreated="grdInternalComplinacePerformer_RowCreated">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                                <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                    <asp:Label ID="Label2" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;"> 
                                                        <asp:Label ID="lbllocation" runat="server" Text='<%# Eval("Branch") %>' data-toggle="tooltip" data-placement="bottom" ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                        </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>  
                                            <asp:TemplateField HeaderText="Reviewer">
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;"> 
                                                    <asp:Label ID="lblReviewerInternal" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# GetReviewerInternal((long)Eval("InternalComplianceInstanceID")) %>' ToolTip='<%#InternalReviewername%>'></asp:Label>
                                                        </div>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>           
                                            <asp:TemplateField HeaderText="Due Date">
                                                <ItemTemplate>
                                                     <asp:Label ID="lblScheduledPerformerOn" runat="server" data-toggle="tooltip" data-placement="bottom"  Text=' <%# Convert.ToDateTime(Eval("InternalScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                                
                                            <asp:TemplateField HeaderText="Period">
                                                <ItemTemplate>
                                                            <%# Eval("ForMonth") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">                                                                
                                                <ItemTemplate>
                                                    <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">                                                         
                                                        <asp:Label ID="lblInternalPerformerStatus" runat="server"  data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>                                                             
                                                    </div>
                                                </ItemTemplate>
                                           </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action">    
                                                <ItemTemplate> 
                                                    <asp:Button runat="server"  OnClientClick="return openModalInternalPer1(this)" ID="btnChangeStatusIntCompPer" CssClass="btnss"  Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("InternalComplianceStatusID")) %>'
                                                     data-sid='<%# Eval("InternalScheduledOnID")%>' data-inid='<%# Eval("InternalComplianceInstanceID") %>'  CommandArgument='<%# Eval("InternalScheduledOnID") + "," + Eval("InternalComplianceInstanceID") %>' />                                    
                                                </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Right" />
                                        <RowStyle CssClass="clsROWgrid"   />
                                         <HeaderStyle CssClass="clsheadergrid"    />
                                        <PagerTemplate>
                                            <table style="display: none">
                                                <tr>
                                                    <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </table>
                                        </PagerTemplate>
                                         <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                        </asp:GridView>  
                                           
                                        <div style="margin-bottom: 4px">
                                                <table width="100%" style="height: 10px">
                                                    <tr>
                                                        <td>
                                                            <div style="margin-bottom: 4px">
                                                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                                            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                                    <asp:Label ID="lbltagLine" runat="server" Style="font-weight: bold; font-size: 12px; margin-left: 35px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                      <div class="clearfix"> </div>                                                   
                                    </div>
                                   <div runat="server" id="reviewerdocuments" class="tab-pane">                                                                                            
                                        <asp:GridView runat="server" ID="grdComplianceTransactionsRev" AutoGenerateColumns="false" AllowSorting="true"
                                                GridLines="None" BorderWidth="0px" CssClass="table" AllowPaging="True" PageSize="10" OnSorting="grdComplianceTransactionsRev_Sorting"
                                                DataKeyNames="ScheduledOnID" OnPageIndexChanging="grdComplianceTransactionsRev_PageIndexChanging" 
                                                OnRowDataBound="grdComplianceTransactionsRev_RowDataBound" OnRowCommand="grdComplianceTransactionsRev_RowCommand" OnRowCreated="grdComplianceTransactionsRev_RowCreated">
                                                <Columns>                                             
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                                        <ItemTemplate>
                                                            <%#Container.DataItemIndex+1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                                                <asp:Label ID="LabelDesc" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                                                  <asp:Label ID="lblInterimdaysReview" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Interimdays") %>' Visible="false" ></asp:Label>
                                                                 </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Location">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="LabelBranch" runat="server"  data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                                </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Performer">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                <asp:Label ID="lblPerformer" runat="server"  data-toggle="tooltip" data-placement="bottom"  Text='<%# GetPerformer((long)Eval("ComplianceInstanceID")) %>' ToolTip='<%#Performername%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Due Date">
                                                        <ItemTemplate>
                                                             <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">                                                         
                                                                 <asp:Label ID="lblScheduledReviewerOn" runat="server" data-toggle="tooltip" data-placement="bottom"  Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                              </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Event Name">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">     
                                                                 <asp:Label ID="lblEventName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("EventName") %>' ToolTip='<%# Eval("EventName") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Event Nature">
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                <asp:Label ID="lblEventNature" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("EventNature") %>' ToolTip='<%# Eval("EventNature") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Period">
                                                        <ItemTemplate>
                                                            <%# Eval("ForMonth") %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status">                                                                
                                                        <ItemTemplate>
                                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">                                                         
                                                            <asp:Label ID="lblStatusReviewer" runat="server"  data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>                                                             
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Action">    
                                                        <ItemTemplate>
                                                            <asp:Button runat="server"  OnClientClick="return openModalReviewer1(this)" ID="btnChangeStatusCompRev"  OnClick="btnChangeStatusReviewer_Click"  CssClass="btnss"  Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("ComplianceStatusID")) %>'
                                                             data-sid='<%# Eval("ScheduledOnID")%>' data-inid='<%# Eval("ComplianceInstanceID") %>' CommandArgument='<%# Eval("ScheduledOnID") + "," + Eval("ComplianceInstanceID") + "," + Eval("Interimdays") + "," + Eval("Status") %>' />  
                                                        </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle HorizontalAlign="Right" />
                                                <RowStyle CssClass="clsROWgrid"   />
                                                <HeaderStyle CssClass="clsheadergrid"    />
                                                <PagerTemplate>
                                                    <table style="display: none">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </PagerTemplate>
                                             <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                        </asp:GridView> 

                                        <asp:GridView runat="server" ID="grdInternalComplianceReviewer" AutoGenerateColumns="false" AllowSorting="true"
                                                    GridLines="None"  AllowPaging="True" CssClass="table" PageSize="10" DataKeyNames="InternalScheduledOnID" 
                                            OnPageIndexChanging="grdInternalComplianceReviewer_PageIndexChanging" 
                                                    OnRowDataBound="grdInternalComplianceReviewer_RowDataBound" 
                                            OnRowCommand="grdInternalComplianceReviewer_RowCommand"
                                             OnRowCreated="grdInternalComplianceReviewer_RowCreated">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Description">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                        <asp:Label ID="Label2" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Location">
                                                            <ItemTemplate>
                                                                    <asp:Label ID="LabelBranch" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>  
                                                        <asp:TemplateField HeaderText="Performer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                        <asp:Label ID="lblInternalPerformer" runat="server"  data-toggle="tooltip" data-placement="bottom"  Text='<%# GetPerformerInternal((long)Eval("InternalComplianceInstanceID")) %>' ToolTip='<%#InternalPerformername%>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>          
                                                            <asp:TemplateField HeaderText="ScheduledOn">
                                                            <ItemTemplate>                                                               
                                                                  <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">                                                         
                                                                 <asp:Label ID="lblScheduledInternalReviewerOn" runat="server" data-toggle="tooltip" data-placement="bottom"  Text=' <%# Convert.ToDateTime(Eval("InternalScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                              </div>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Month">
                                                            <ItemTemplate>
                                                                 <%# Eval("ForMonth") %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>                                        
                                                        <asp:TemplateField HeaderText="Status">                                                                
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">                                                         
                                                                        <asp:Label ID="lblStatusInternalReviewer" runat="server"  data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>                                                             
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center" HeaderText="Action">    
                                                            <ItemTemplate>
                                                                <asp:Button runat="server"  OnClientClick="return openModalInternalReviewer1(this)" ID="btnChangeStatusIntCompRev"    CssClass="btnss"  Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("InternalComplianceStatusID")) %>'
                                                               data-sid='<%# Eval("InternalScheduledOnID")%>' data-inid='<%# Eval("InternalComplianceInstanceID")%>' CommandArgument='<%# Eval("InternalScheduledOnID") + "," + Eval("InternalComplianceInstanceID") %>' />  
                                                            </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" />
                                                    <RowStyle CssClass="clsROWgrid"   />
                                                    <HeaderStyle CssClass="clsheadergrid"    />
                                                    <PagerTemplate>
                                                        <table style="display: none">
                                                            <tr>
                                                                <td>
                                                                        <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </PagerTemplate>
                                             <EmptyDataTemplate>
                                        No Records Found.
                                    </EmptyDataTemplate>
                                        </asp:GridView> 
                                    </div>
                                   <div class="col-md-12 colpadding0">
                                            <div class="col-md-6 colpadding0">
                                            </div>
                                            <div class="col-md-6 colpadding0">
                                                <div class="table-paging" style="margin-bottom:20px">
                                                    <asp:ImageButton ID="lBPrevious" CssClass ="table-paging-left"  runat="server" ImageUrl ="~/img/paging-left-active.png" OnClick ="Previous_Click"/>

                                                    <div class="table-paging-text">
                                                        <p><asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label></p> 
                                                    </div>
                                                    <asp:ImageButton ID="lBNext" CssClass ="table-paging-right"  runat="server" ImageUrl ="~/img/paging-right-active.png" OnClick ="Next_Click"/>
                                                </div>
                                            </div>
                                   </div>
                            </div>                              
                        </section>
                    </div>
                </div>
            </div>
            <tr id="trErrorMessage" runat="server" visible="true">
                <td colspan="3" style="background-color: #e9e1e1;">
                    <asp:Label ID="GridViewPagingError" runat="server" Font-Names="Verdana" Font-Size="9pt"
                        ForeColor="Red"></asp:Label>
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                </td>
            </tr>

            <asp:HiddenField ID="hdnTitle" runat="server" />
            <asp:HiddenField ID="IsActiveControl" runat="server" Value="false" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div>
        <div>
            <div class="modal fade" id="ComplainceInternalReviewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 85%">

                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #f7f7f7; height: 36px;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        </div>
                        <div class="modal-body" style="background-color: #f7f7f7; width:100%; max-height:750px; overflow-y:auto;">
                          <%--  <vit:InternalComplianceReviewerStatusTransaction runat="server" ID="udcStatusTranscationInternal" Visible="false" />--%>
                             <iframe id="iInternalReviewerFrame" src="about:blank" width="100%" height="650px" frameborder="0" ></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <div class="modal fade" id="ComplainceReviewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 85%">

                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #f7f7f7; height: 36px;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        </div>
                        <div class="modal-body" style="background-color: #f7f7f7; width:100%; max-height:750px; overflow-y:auto;">
                       <%--     <vit:ComplianceReviewerStatusTransaction runat="server" ID="udcReviewerStatusTranscatopn" Visible="false" />--%>
                            <iframe id="iReviewerFrame" src="about:blank" width="100%" height="650px" frameborder="0" ></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <div class="modal fade" id="ComplaincePerformer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 85%">

                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #f7f7f7; height: 36px;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        </div>
                        <div class="modal-body" style="background-color: #f7f7f7; width:100%; max-height:750px; overflow-y:hidden; border-radius: 6px;">                          
                            <iframe id="iPerformerFrame" src="about:blank" width="100%" height="650px" frameborder="0" ></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div>
            <div class="modal fade" id="ComplainceInternalPerformaer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 85%">

                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #f7f7f7; height: 36px;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        </div>
                        <div class="modal-body" style="background-color: #f7f7f7; width:100%; max-height:750px; overflow-y:auto;" >
                         <%--   <vit:InternalComplianceStatusTransaction runat="server" ID="udcInternalPerformerStatusTranscation" Visible="false" />--%>
                              <iframe id="iInternalPerformerFrame" src="about:blank" width="100%" height="650px" frameborder="0" ></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
            <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 1150px;">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header" style="border-bottom: none;">
                            <button type="button" class="close" data-dismiss="modal" onclick="CloseClearOV();" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>


        <div class="modal fade" id="divDownloadView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 650px;">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header" style="border-bottom: none;">
                        <button type="button" class="close" data-dismiss="modal" onclick="CloseClearDV();" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <iframe id="DownloadViews" src="about:blank" width="535px" height="350px" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            var filterbytype = ReadQuerySt('filter');
            var filterbyrole = ReadQuerySt('type');
            if (filterbytype == '') {
                fhead('My Workspace');
            } else {
                $('#pagetype').css("font-size", "20px")
                if (filterbytype == 'PendingForReview') {
                    filterbytype = 'Pending For Review';
                } else if (filterbytype == 'DueButNotSubmitted') {
                    filterbytype = 'Due But Not Submitted';
                }
                fhead('My Workspace / ' + filterbytype + ' ' + filterbyrole);
            }
        });
  
        function instructionsDashboard() {
            var enjoyhint_instance = new EnjoyHint({});
            var enjoyhint_script_steps = [
              {
                  "next .btnss": "Click to Complete Compliances/tasks",
                  showSkip: true,
              }, {
                  "next #ContentPlaceHolder1_ddlComplianceType": "Select type of compliance internal/Statutory",
                  showSkip: true,
              }, {
                  "next #ContentPlaceHolder1_tbxFilterLocation": "Select location to see filtered data by location",
                  showSkip: true,
              }, {
                  "next #ContentPlaceHolder1_btnSearch": "Click to filter data based on selection",
                  showSkip: true,
              }, {
                  "next #ContentPlaceHolder1_ddlpageSize": "Change no. of tasks/compliance in the table",
                  showSkip: true,
              }
              , {
                  "next .btn-advanceSearch": "Perform advanced search",
                  showSkip: true,
              }

            ];
        }
      
    </script>


</asp:Content>
