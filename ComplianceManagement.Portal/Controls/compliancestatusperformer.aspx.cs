﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Configuration;
using Ionic.Zip;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;
using WinSCP;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class compliancestatusperformer : System.Web.UI.Page
    {

        static string sampleFormPath = "";
        public static string subTaskDocViewPath = "";
        public static List<long> Tasklist = new List<long>();
        protected static string ActDocString;
        bool IsNotCompiled = false;
        bool IsCompliedButDocumentPending = false;
        protected bool IsPenaltyVisible = true;
        protected static bool IsRemarkCompulsary = false;
        protected string UploadDocumentLink;
        protected int CustomerID = 0;
        public static string ActName;
        public static int ActID;
        protected static string KendoPath;
        protected static int CustId;
        protected static int UId;
        protected static int compInstanceID;
        protected static string Authorization;
        public string ispenaltytext;
        public string Status;
        public string ComplainceType;
        public string ISmail; 
        protected static bool IsComplianceupdatedCustomer = false;
        public static bool LockUnlockCustomerEnable;
        public decimal a;
        public decimal b;
        public static bool IsRemark;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LockUnlockCustomerEnable = false;
                int CacheClearTime = Convert.ToInt32(ConfigurationManager.AppSettings["CacheClearTimeToken"]);
                string CacheName = "CacheGetTokenData_" + Convert.ToString(AuthenticationHelper.UserID) + "_" + Convert.ToString(AuthenticationHelper.CustomerID);
                Authorization = (string)HttpContext.Current.Cache[CacheName];
                if (Authorization == null)
                {
                    Authorization = Business.ComplianceManagement.getToken(Convert.ToString(AuthenticationHelper.UserID));
                    HttpContext.Current.Cache.Insert(CacheName, Authorization, null, DateTime.Now.AddMinutes(CacheClearTime), System.Web.Caching.Cache.NoSlidingExpiration); // add it to cache
                }
                ispenaltytext = ConfigurationManager.AppSettings["IsPenaltyTextchanged"];
                if (ispenaltytext == Convert.ToString(AuthenticationHelper.CustomerID))
                {
                 lblpenaltytext.InnerText = "Penalty/Additional Fee(INR)";
                }
                string listCustomer = ConfigurationManager.AppSettings["UploadUsingLinkCustomerlist"];
                string[] listCust = new string[] { };
                if (!string.IsNullOrEmpty(listCustomer))
                    listCust = listCustomer.Split(',');

                if (listCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                    UploadDocumentLink = "True";
                else
                    UploadDocumentLink = "False";

                Status = Convert.ToString(Request.QueryString["Status"]);
                ComplainceType = Convert.ToString(Request.QueryString["ComplainceType"]);
                IsRemark = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "RemarkList");
                ISmail = "0";
                try
                {
                    ISmail = Convert.ToString(Request.QueryString["ISM"]);                    
                    if (!string.IsNullOrEmpty(ISmail) && ISmail == "1")
                    {
                        lnkgotoportal.Visible = true;
                    }
                    else
                    {
                        lnkgotoportal.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString() + "ISmail", MethodBase.GetCurrentMethod().Name);
                    ISmail = "0";
                }
                
                if (!this.IsPostBack)
                {
                    KendoPath = ConfigurationManager.AppSettings["KendoPathApp"];
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    UId = Convert.ToInt32(AuthenticationHelper.UserID);
                    CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    IsComplianceupdatedCustomer = false;
                    var details = Business.ComplianceManagement.GetCustomerID(Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (details != null)
                        IsComplianceupdatedCustomer = true;

                    DateTime date = DateTime.MinValue;
                    if (DateTime.TryParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer2", string.Format("initializeDatePickerforPerformer2(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer2", "initializeDatePickerforPerformer2(null);", true);
                    }                    
                    int scheduleOnId = Convert.ToInt32(Request.QueryString["sId"].ToString());
                    int instanceId = Convert.ToInt32(Request.QueryString["InId"].ToString());
                    OpenTransactionPage(scheduleOnId, instanceId);
                    Labellockingmsg.Visible = false;
                    string listLockingCustomer = ConfigurationManager.AppSettings["LockingDaysCustomerlist"];
                    string[] listLockCust = new string[] { };
                    if (!string.IsNullOrEmpty(listLockingCustomer))
                        listLockCust = listLockingCustomer.Split(',');

                    if (listLockCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                    {
                        if (Business.ComplianceManagement.GetCurrentStatusByScheduleONID(scheduleOnId, "S", Convert.ToInt32(AuthenticationHelper.CustomerID)))
                        {
                            Labellockingmsg.Visible = true;
                            btnSave.Enabled = false;
                            btnSaveDOCNotCompulsory.Enabled = false;
                        }
                        else
                        {
                            Labellockingmsg.Visible = false;
                            btnSave.Enabled = true;
                            btnSaveDOCNotCompulsory.Enabled = true;
                        }
                    }
                    LockUnlockCustomerEnable = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "LockUnlock");
                    if (LockUnlockCustomerEnable)
                    {
                        if (Status == "Overdue")
                        {
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                var query = (from row in entities.SP_DetailLockingDayReviewer(ComplainceType, CustId)
                                             where row.ComplianceScheduleOnID == scheduleOnId
                                             select row).FirstOrDefault();

                                if (query !=null)
                                {
                                    var UpdateComplianceUnlockStatus = (from row in entities.ComplianceUnlockStatus
                                                                        where row.ComplianceScheduleOnID == scheduleOnId
                                                                        select row).ToList();

                                    if (UpdateComplianceUnlockStatus !=null)
                                    {
                                        DateTime AddFiveDaysGraceToOverdueCompliances = Convert.ToDateTime(query.ScheduleOn).AddDays(5);

                                        if (AddFiveDaysGraceToOverdueCompliances > DateTime.Today.Date)
                                        {

                                        }
                                        else if (query != null)
                                        {
                                            if (query.Unlock == false)
                                            {
                                                Labellockingmsg.Visible = true;
                                                btnSave.Enabled = false;
                                                btnSaveDOCNotCompulsory.Enabled = false;
                                            }

                                            else if (query.Unlock == true && DateTime.Today.Date > query.Dated)
                                            {
                                                Labellockingmsg.Visible = true;
                                                btnSave.Enabled = false;
                                                btnSaveDOCNotCompulsory.Enabled = false;

                                                if (UpdateComplianceUnlockStatus.Count > 0)
                                                {
                                                    foreach (var item in UpdateComplianceUnlockStatus)
                                                    {
                                                        item.Unlock = false;
                                                        entities.SaveChanges();
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Labellockingmsg.Visible = false;
                                                btnSave.Enabled = true;
                                                btnSaveDOCNotCompulsory.Enabled = true;
                                            }
                                        }
                                        else
                                        {
                                            Labellockingmsg.Visible = true;
                                            btnSave.Enabled = false;
                                            btnSaveDOCNotCompulsory.Enabled = false;
                                        }
                                    }
                                    
                                }
                               
                            }
                        }
                    }

                    var customizedid= CustomerManagement.GetCustomizedCustomerid(Convert.ToInt32(AuthenticationHelper.CustomerID),"TaxReportFields");
                    if(customizedid==AuthenticationHelper.CustomerID)
                    {
                        divbankdetails.Visible = true;
                    }
                    else
                    {
                        divbankdetails.Visible = false;
                    }
                    if(customizedid == AuthenticationHelper.CustomerID)
                    {
                        lbldate.InnerText = "Date of Compliance";
                    }
                    else
                    {
                        lbldate.InnerText = "Date";
                    }
                    
                    if (customizedid == AuthenticationHelper.CustomerID)
                    {
                        TRGSTNo.Visible = true;
                    }
                    else
                    {
                        TRGSTNo.Visible = false;
                    }
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforChallanPaidDate", "initializeDatePickerforChallanPaidDate(null);", true);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //public static int GetCustomizedCustomerid(int custid)
        //{
        //    using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //    {
        //        var data = (from row in entities.ClientCustomizations
        //                    where row.CustomizationName== "TaxReportFields"
        //                    && row.ClientID==custid
        //                    select row.ClientID).FirstOrDefault();

        //        return data;
        //    }
        //}
        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        private void BindTransactionDetails(int ScheduledOnID, int complianceInstanceID)
        {
            var CSOID = Convert.ToInt64(ScheduledOnID);
            var RCT = Business.ComplianceManagement.GetCurrentStatusByComplianceID((int)CSOID);
            if (RCT.ComplianceStatusID == 2 || RCT.ComplianceStatusID == 3 || RCT.ComplianceStatusID==4 || RCT.ComplianceStatusID == 5 || RCT.ComplianceStatusID == 18)
            {
                btnSave.Visible = false;
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "This compliance is already submitted for review or already closed.";
            }
            else
            {
                btnSave.Visible = true;
                try
                {

                    grdDocument.DataSource = null;
                    grdDocument.DataBind();

                    lblPenalty.Text = string.Empty;
                    lblRisk.Text = string.Empty;
                    lbDownloadSample.Text = string.Empty;

                    chkPenaltySave.Checked = false;
                    txtInterest.Text = "";
                    txtPenalty.Text = "";

                    txtInterest.ReadOnly = false;
                    txtPenalty.ReadOnly = false;

                    fieldsetpenalty.Visible = false;
                    int customerID = -1;
                    int customerbranchID = -1;

                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var AllComData = Business.ComplianceManagement.GetPeriodBranchLocation(ScheduledOnID, complianceInstanceID);
                    var OriginalDueDateData = Business.ComplianceManagement.GetOriginalDueDate(ScheduledOnID, complianceInstanceID);

                    Session["Interimdays"] = "";
                    Session["Status"] = "";

                    //Session["Interimdays"]
                    if (RCT.Status == "Open" && Convert.ToDateTime(AllComData.PerformerScheduledOn).Date > DateTime.Today.Date || RCT.Status == "Submitted For Interim Review" || RCT.Status == "Interim Review Approved" || RCT.Status == "Interim Rejected")
                    {
                        if (AllComData.Interimdays != null)
                        {
                            Session["Interimdays"] = AllComData.Interimdays;
                            Session["Status"] = RCT.Status;
                        }
                    }

                    TROriginalDuedate.Visible = false;
                    duedatelbl.Visible = true;
                    if (OriginalDueDateData != null)
                    {
                        duedatelbl1.Visible = true;
                        duedatelbl.Visible = false;
                        TROriginalDuedate.Visible = true;
                        lblOriginalDuedate.Text = Convert.ToDateTime(OriginalDueDateData.OldDueDate).ToString("dd-MMM-yyyy");
                    }

                    if (AllComData != null)
                    {
                        ActID= AllComData.ActID;
                        customerbranchID = AllComData.CustomerBranchID;
                        lblLocation.Text = AllComData.Branch;

                        var customizedid = CustomerManagement.GetCustomizedCustomerid(Convert.ToInt32(AuthenticationHelper.CustomerID), "GSTFields");
                        if (customizedid == AuthenticationHelper.CustomerID)
                        {
                            lblGSTno.Text = AllComData.GSTNumber;
                        }
                        lblDueDate.Text = Convert.ToDateTime(AllComData.PerformerScheduledOn).ToString("dd-MMM-yyyy");
                        hiddenDueDate.Value = Convert.ToDateTime(AllComData.PerformerScheduledOn).ToString("dd-MM-yyyy");
                        lblPeriod.Text = AllComData.ForMonth;

                        if (AllComData.ActualScheduleon != null)
                        {
                            TRactualduedate.Visible = true;
                            lblActualDueDate.Text = Convert.ToDateTime(AllComData.ActualScheduleon).ToString("dd-MMM-yyyy");
                        }
                    }
                    var IsAfter = TaskManagment.IsTaskAfter(ScheduledOnID, 3, lblPeriod.Text, customerbranchID, customerID);
                    lbltask.Text = "";
                    var showHideButton = false;
                    if (IsAfter == false)
                    {
                        showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, customerbranchID);
                        if (showHideButton == false)
                        {
                            lbltask.Text = "Compliance releted task is not yet completed.";
                        }
                        btnSave.Enabled = showHideButton;
                    }
                    else
                    {
                        showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, customerbranchID);
                        lbltask.Text = "";
                        btnSave.Enabled = true;
                    }

                    BindTempDocumentData(ScheduledOnID);


                    ViewState["IsAfter"] = IsAfter;
                    ViewState["Period"] = lblPeriod.Text;
                    ViewState["CustomerBrachID"] = customerbranchID;
                    var complianceInfo = Business.ComplianceManagement.GetComplianceByInstanceID(ScheduledOnID);
                    var complianceForm = Business.ComplianceManagement.GetComplianceFormByID(complianceInfo.ID);

                   
                    var gstclist= ComplianceManagement.Business.ComplianceManagement.GetGSTApplicableCustomer(customerID, complianceInfo.ID);
                    if (gstclist !=null)
                    {
                        BindGSTCompliance(complianceInfo.ID, complianceInstanceID, 3, lblPeriod.Text, gstclist.GSTGroup);
                    }
                    else
                    {
                        divGSTComplianceList.Visible = false;
                    }

                    if (complianceInfo != null)
                    {

                        lblComplianceID.Text = Convert.ToString(complianceInfo.ID);
                        lnkSampleForm.Text = Convert.ToString(complianceInfo.SampleFormLink);
                        lblComplianceDiscriptionNew.Text = complianceInfo.ShortDescription;
                        lblComplianceDiscription.Text = complianceInfo.ShortDescription;
                        lblDetailedDiscription.Text = complianceInfo.Description;
                        lblFrequency.Text = Enumerations.GetEnumByID<Frequency>(Convert.ToInt32(complianceInfo.Frequency != null ? (int)complianceInfo.Frequency : -1));
                        lblRefrenceText.Text = WebUtility.HtmlEncode(complianceInfo.ReferenceMaterialText);
                        lblPenalty.Text = complianceInfo.PenaltyDescription;
                        string risk = Business.ComplianceManagement.GetRiskType(complianceInfo, complianceInstanceID);
                        lblRiskType.Text = Business.ComplianceManagement.GetRisk(complianceInfo, complianceInstanceID);
                        lblRisk.Text = risk;
                        lblshortform.Text = complianceInfo.ShortForm;

                        var ActInfo = Business.ActManagement.GetByID(complianceInfo.ActID);

                        if (ActInfo != null)
                        {
                            btnActName.Text = ActInfo.Name;
                            ActName = ActInfo.Name;
                        }
                       
                        lblNatureofcompliance.Text = "";
                        //txtValueAsPerSystem.Text = AllComData.ValuesAsPerSystem.ToString();
                        //txtValueAsPerReturn.Text = AllComData.ValuesAsPerReturn.ToString();
                        txtLiabilityPaid.Text = "";
                        txtDiffAB.Text = "";
                        txtDiffBC.Text = "";

                        if (complianceInfo.NatureOfCompliance != null)
                        {
                            lblNatureofcomplianceID.Text = Convert.ToString(complianceInfo.NatureOfCompliance);

                            if(lblNatureofcomplianceID.Text == "")
                            {
                                lblNatureofcomplianceID.Text = "0";
                            }
                            string Nature = Business.ComplianceManagement.GetNatureOfComplianceFromID(Convert.ToInt32(complianceInfo.NatureOfCompliance));
                            lblNatureofcompliance.Text = Nature.ToString();
                            if (complianceInfo.NatureOfCompliance == 1)
                            {
                                trreturn.Visible = true;
                                lblValueAsPerSystem.InnerText = "Liability as per system (A)";
                                lblValueAsPerReturn.InnerText = "Liability as per return (B)";
                            }
                            else if (complianceInfo.NatureOfCompliance == 0)
                            {
                                fieldsetTDS.Visible = true;
                                lblValueAsPerSystem.InnerText = "Values as per system (A)";
                                lblValueAsPerReturn.InnerText = "Values as per return (B)";
                                trreturn.Visible = false;
                            }
                            else
                            {
                                fieldsetTDS.Visible = false;
                            }
                        }
                        else
                        {
                            lblNatureofcomplianceID.Text = "0";
                            fieldsetTDS.Visible = false;
                        }
                        txtValueAsPerSystem.Text = AllComData.ValuesAsPerSystem.ToString();
                        txtValueAsPerReturn.Text = AllComData.ValuesAsPerReturn.ToString();
                       
                        if (txtValueAsPerSystem.Text != "")
                        {
                             a = Convert.ToDecimal(txtValueAsPerSystem.Text);
                        }
                        if (txtValueAsPerSystem.Text != "")
                        {
                             b = Convert.ToDecimal(txtValueAsPerReturn.Text);
                            txtDiffAB.Text = (a - b).ToString();
                        }
                       
                       
                       
                        if (risk == "HIGH")
                        {
                            divRiskType.Attributes["style"] = "background-color:red;";
                        }
                        else if (risk == "MEDIUM")
                        {
                            divRiskType.Attributes["style"] = "background-color:yellow;";
                        }
                        else if (risk == "LOW")
                        {
                            divRiskType.Attributes["style"] = "background-color:green;";
                        }
                        else if (risk == "CRITICAL")
                        {
                            divRiskType.Attributes["style"] = "background-color:#CC0900;";
                        }
                        lblRule.Text = complianceInfo.Sections;
                        lblFormNumber.Text = complianceInfo.RequiredForms;
                    }

                    var AuditChecklistName = Business.ComplianceManagement.GetAuditChecklistName(complianceInfo.ID, Convert.ToInt32(AuthenticationHelper.CustomerID));

                    if (!string.IsNullOrEmpty(AuditChecklistName))
                    {
                        lblAuditChecklist.Text = AuditChecklistName;
                        trAuditChecklist.Visible = true;
                    }
                    else
                    {
                        trAuditChecklist.Visible = false;
                    }
                    if (RCT.ComplianceStatusID == 10)
                    {
                        BindDocument(ScheduledOnID);
                    }
                    else
                    {
                        divDeleteDocument.Visible = false;
                        rptComplianceDocumnets.DataSource = null;
                        rptComplianceDocumnets.DataBind();
                        rptWorkingFiles.DataSource = null;
                        rptWorkingFiles.DataBind();
                    }

                    if (complianceInfo.UploadDocument == true && complianceForm != null)
                    {
                        lbDownloadSample.Text = "Download";
                        lbDownloadSample.CommandArgument = complianceForm.ComplianceID.ToString();
                        sampleFormPath = complianceForm.FilePath;
                        sampleFormPath = sampleFormPath.Substring(2, sampleFormPath.Length - 2);
                        lblNote.Visible = true;
                        lnkViewSampleForm.Visible = true;
                        lblSlash.Visible = true;
                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                        {
                            lblpathsample.Text = sampleFormPath;
                        }
                        else
                        {
                            lblpathsample.Text = sampleFormPath;
                        }
                        var complianceform = Business.ComplianceManagement.GetComplianceFileNameMultiple(Convert.ToInt32(lbDownloadSample.CommandArgument));
                        rptComplianceSampleView.DataSource = complianceform;
                        rptComplianceSampleView.DataBind();
                    }
                    else
                    {
                        lblNote.Visible = false;
                        lblSlash.Visible = false;
                        lnkViewSampleForm.Visible = false;
                        sampleFormPath = "";
                    }

                    btnSave.Attributes.Remove("disabled");
                    if (RCT.ComplianceStatusID == 1 || RCT.ComplianceStatusID == 6 || RCT.ComplianceStatusID == 10 || RCT.ComplianceStatusID == 13 || RCT.ComplianceStatusID == 14  || RCT.ComplianceStatusID == 19)
                    {
                        if (complianceInfo.EventFlag == true)
                        {
                            if (complianceInfo.UpDocs == true)
                            {
                                divUploadDocument.Visible = true;
                                divWorkingfiles.Visible = true;
                                btnSave.Visible = true;
                                btnSaveDOCNotCompulsory.Visible = false;
                                lblDocComplasary.Visible = true;
                                ViewState["btnSave"] = true;
                                ViewState["btnSaveDOCNotCompulsory"] = false;
                                ViewState["lblDocComplasary"] = true;
                            }
                            else
                            {
                                btnSaveDOCNotCompulsory.Visible = true;
                                btnSave.Visible = false;
                                divUploadDocument.Visible = true;
                                divWorkingfiles.Visible = true;
                                lblDocComplasary.Visible = false;
                                ViewState["btnSave"] = false;
                                ViewState["btnSaveDOCNotCompulsory"] = true;
                                ViewState["lblDocComplasary"] = false;
                            }
                        }
                        else
                        {
                            divUploadDocument.Visible = true;
                            divWorkingfiles.Visible = true;
                            btnSave.Visible = true;
                            btnSaveDOCNotCompulsory.Visible = false;
                            lblDocComplasary.Visible = true;
                            ViewState["btnSave"] = true;
                            ViewState["btnSaveDOCNotCompulsory"] = false;
                            ViewState["lblDocComplasary"] = true;
                        }
                    }
                    else if (RCT.ComplianceStatusID != 1)
                    {
                        divUploadDocument.Visible = false;
                        divWorkingfiles.Visible = false;
                        if ((complianceInfo.UploadDocument ?? false))
                        {
                            btnSave.Attributes.Add("disabled", "disabled");
                        }
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["Interimdays"])))
                    {
                        if (Session["Interimdays"] != null)
                        {
                            int EscDays = Convert.ToInt32(Session["Interimdays"]);
                        }

                        if (Session["Interimdays"].ToString() != "0")
                        {
                            BindEscalationStatusList();
                            fuSampleFile.Enabled = false;
                            FileUpload1.Enabled = true;
                        }
                        else
                        {
                            fuSampleFile.Enabled = true;
                            FileUpload1.Enabled = true;
                            BindStatusList(Convert.ToInt32(RCT.ComplianceStatusID));
                        }
                    }
                    else
                    {
                        fuSampleFile.Enabled = true;
                        FileUpload1.Enabled = true;
                        BindStatusList(Convert.ToInt32(RCT.ComplianceStatusID));
                    }
                    //interim approve
                    if (RCT.ComplianceStatusID == 13)
                    {
                        fuSampleFile.Enabled = true;
                        FileUpload1.Enabled = false;
                        BindStatusList(Convert.ToInt32(RCT.ComplianceStatusID));
                    }


                    //interim reject
                    if (RCT.ComplianceStatusID == 14)
                    {
                        fuSampleFile.Enabled = false;
                        FileUpload1.Enabled = true;
                        BindEscalationStatusList();
                    }
                   
                    BindTransactions(ScheduledOnID);
                    tbxRemarks.Text = string.Empty;
                    hdnComplianceInstanceID.Value = complianceInstanceID.ToString();
                    hdnComplianceScheduledOnId.Value = ScheduledOnID.ToString();

                  
                       
                    #region Act_Document

                    PopulateTreeView(complianceInfo.ActID);

                    #endregion

                    int CustID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var details = Business.ComplianceManagement.GetCustomerID(CustID);
                    if (details != null)
                    {
                        var Compliancedetails = Business.ComplianceManagement.GetDetailsCustomerID(CustID, Convert.ToInt32(complianceInfo.ID));
                        if (Compliancedetails != null)
                        {
                            int scheduleOnId = Convert.ToInt32(Request.QueryString["sId"].ToString());
                            int instanceId = Convert.ToInt32(Request.QueryString["InId"].ToString());
                            bool checkisrequired = Business.ComplianceManagement.GetLogDetails(Convert.ToInt32(complianceInfo.ID), instanceId, scheduleOnId, CustID);

                            if (checkisrequired)
                            {
                                divUploadDocument.Visible = true;
                                divWorkingfiles.Visible = true;
                                btnSave.Visible = true;
                                btnSaveDOCNotCompulsory.Visible = false;
                                lblDocComplasary.Visible = true;
                                ViewState["btnSave"] = true;
                                ViewState["btnSaveDOCNotCompulsory"] = false;
                                ViewState["lblDocComplasary"] = true;
                            }
                            else
                            {
                                btnSaveDOCNotCompulsory.Visible = true;
                                btnSave.Visible = false;
                                divUploadDocument.Visible = true;
                                divWorkingfiles.Visible = true;
                                lblDocComplasary.Visible = false;
                                ViewState["btnSave"] = false;
                                ViewState["btnSaveDOCNotCompulsory"] = true;
                                ViewState["lblDocComplasary"] = false;
                            }
                        }
                    }                    
                    upComplianceDetails.Update();

                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
        }

        public void PopulateTreeView(int ActID)
        {

            var MasterQuery = ActManagement.getFileNamebyID(ActID);
            var DistinctTransQuery = MasterQuery;
            DistinctTransQuery = DistinctTransQuery.GroupBy(x => x.DocumentTypeID).Select(x => x.FirstOrDefault()).ToList();
            System.Text.StringBuilder stringbuilder = new System.Text.StringBuilder();
            stringbuilder.Append(@"<table id='basic' width='100%'>");
            foreach (var item in DistinctTransQuery)
            {
                stringbuilder.Append("<tr data-node-id=" + Convert.ToString(item.DocumentTypeID) + ">" +
                            " <td class='locationheadLocationbg'>" + Convert.ToString(item.DocumentType) + "</td>");
                stringbuilder.Append("</tr>");
                var actDocVersionData = MasterQuery.Where(entry => entry.Act_ID == ActID && entry.DocumentTypeID == item.DocumentTypeID).ToList();
                int i = 0;
                foreach (var item1 in actDocVersionData)
                {
                    i += 1;
                    string id = Convert.ToString(item.DocumentTypeID) + "." + i;
                    string FileName = "";
                    string[] filename = item1.FileName.Split('.');
                    string str = filename[0] + i + "." + filename[1];
                    if (filename[0].Length > 60)
                    {
                        FileName = filename[0].Substring(0, 60) + "." + filename[1];
                    }
                    else
                    {
                        FileName = item1.FileName;
                    }
                    if (item.Act_TypeVersionDate != null)
                    {
                        FileName = FileName + " on " + Convert.ToDateTime(item1.Act_TypeVersionDate).ToString("MMM-yy");
                    }
                    stringbuilder.Append("<tr data-node-id=" + Convert.ToString(id) + " data-node-pid=" + Convert.ToString(item.DocumentTypeID) + " >" +
                    " <td class='locationheadLocationbg'>" + Convert.ToString(FileName) + "</td>");                    
                    stringbuilder.Append("<td class='downloadcss' onclick ='PerActODDOPopPup(" + ActID + "," + item1.ID + ")'> Download </td>");
                    stringbuilder.Append("<td class='Viewcss' onclick='PerActODOPopPup(" + ActID + "," + item1.ID + ")'> View </td>");                    
                    stringbuilder.Append("</tr>");
                }
            }
            stringbuilder.Append("</table>");
            ActDocString = stringbuilder.ToString().Trim();            
        }
        public void BindDocument(int ScheduledOnID)
        {
            try
            {
                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();
                divDeleteDocument.Visible = false;
                ComplianceDocument = DocumentManagement.GetFileData1(ScheduledOnID).Where(entry => entry.Version.Equals("1.0")).ToList();
                rptComplianceDocumnets.DataSource = ComplianceDocument.Where(entry => entry.FileType == 1).ToList();
                rptComplianceDocumnets.DataBind();

                rptWorkingFiles.DataSource = ComplianceDocument.Where(entry => entry.FileType == 2).ToList();
                rptWorkingFiles.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        void AddSortImage(int columnIndex, GridViewRow headerRow)
        {           
        }

        protected void grdTransactionHistory_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var complianceTransactionHistory = Business.ComplianceManagement.GetAllTransactionLog(Convert.ToInt32(ViewState["complianceInstanceID"]));
                if (direction == SortDirection.Ascending)
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdTransactionHistory.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndexHistory"] = grdTransactionHistory.Columns.IndexOf(field);
                    }
                }

                grdTransactionHistory.DataSource = complianceTransactionHistory;
                grdTransactionHistory.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTransactionHistory.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["complianceInstanceID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactions(int ScheduledOnID)
        {
            try
            {
                grdTransactionHistory.DataSource = Business.ComplianceManagement.GetAllTransactionLog(ScheduledOnID);
                grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindEscalationStatusList()
        {
            try
            {
                ddlStatus.DataSource = null;
                ddlStatus.DataBind();
                ddlStatus.ClearSelection();

                ddlStatus.DataTextField = "Name";
                ddlStatus.DataValueField = "ID";
                var details = Business.ComplianceManagement.GetCustomerID(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (details != null)
                {
                    int ComplianceinstanceID = Convert.ToInt32(Request.QueryString["InId"].ToString());
                    int CompID = Convert.ToInt32(Business.ComplianceManagement.GetComplianceID(ComplianceinstanceID));
                    var Compliancedetails = Business.ComplianceManagement.GetDetailsCustomerID(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(CompID));
                    if (Compliancedetails != null)
                    {
                        int scheduleOnId = Convert.ToInt32(Request.QueryString["sId"].ToString());

                        var Compliancesceduledetails = Business.ComplianceManagement.GetDetailsComplianceScheduled(Convert.ToInt32(ComplianceinstanceID), Convert.ToInt32(scheduleOnId));

                        if (Compliancesceduledetails != null)
                        {
                            if (Compliancesceduledetails.RoleID == 3)
                            {
                                var statusList = ComplianceStatusManagement.GetStatusListByCustomer(false, "3");
                                ddlStatus.DataSource = statusList;
                                ddlStatus.DataBind();
                                ddlStatus.Items.Insert(0, new ListItem("< Select >", "-1"));
                            }
                            else
                            {
                                var statusList = ComplianceStatusManagement.GetStatusListByCustomer(true, "3");
                                ddlStatus.DataSource = statusList;
                                ddlStatus.DataBind();
                                ddlStatus.Items.Insert(0, new ListItem("< Select >", "-1"));
                            }
                        }
                        else
                        {
                            var statusList = ComplianceStatusManagement.GetStatusListByCustomer(Compliancedetails.IsApproverCompulsory, "3");
                            ddlStatus.DataSource = statusList;
                            ddlStatus.DataBind();
                            ddlStatus.Items.Insert(0, new ListItem("< Select >", "-1"));
                        }
                    }
                }
                else
                {
                    var statusList = ComplianceStatusManagement.GetEscalationStatusList();

                    ddlStatus.DataSource = statusList;
                    ddlStatus.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
   private void BindRemark(int RemarkID)
        {
            try
            {
               // var statusList = "";
                Remarkddl.DataSource = null;
                Remarkddl.DataBind();
                Remarkddl.ClearSelection();   
                Remarkddl.DataTextField = "Remark";
                Remarkddl.DataValueField = "ID";
                int ComplianceinstanceID = Convert.ToInt32(Request.QueryString["InId"].ToString());
                int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
               // int CompID = Convert.ToInt32(ComplianceStatusManagement.GetComplinceDeptID(ComplianceinstanceID));
               if(RemarkID == 18)
                {
                    bool ISDept = ComplianceStatusManagement.GetComplinceDeptID(ComplianceinstanceID, CustomerID);
                    if (ISDept == true)
                    {
                        var statusList = ComplianceStatusManagement.GetRemarkListByDepartment(RemarkID);
                        Remarkddl.DataSource = statusList;
                        //Remarkddl.DataBind();
                        //Remarkddl.Items.Insert(0, new ListItem("< Select Remark>", "-1"));
                    }
                    else
                    {
                        var statusList = ComplianceStatusManagement.GetRemarkListByStatus(RemarkID);
                        Remarkddl.DataSource = statusList;
                        //Remarkddl.DataBind();
                        //Remarkddl.Items.Insert(0, new ListItem("< Select Remark>", "-1"));
                    }
                }
                else
                {
                    var statusList = ComplianceStatusManagement.GetRemarkListByStatus(RemarkID);
                    Remarkddl.DataSource = statusList;
                    //Remarkddl.DataBind();
                    //Remarkddl.Items.Insert(0, new ListItem("< Select Remark>", "-1"));
                }
                Remarkddl.DataBind();
                Remarkddl.Items.Insert(0, new ListItem("< Select Remark>", "-1"));


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindStatusList(int statusID)
        {
            try
            {
                ddlStatus.DataSource = null;
                ddlStatus.DataBind();
                ddlStatus.ClearSelection();

                ddlStatus.DataTextField = "Name";
                ddlStatus.DataValueField = "ID";

                //for sony change
                var details = Business.ComplianceManagement.GetCustomerID(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (details != null)
                {
                    int ComplianceinstanceID = Convert.ToInt32(Request.QueryString["InId"].ToString());
                    int CompID = Convert.ToInt32(Business.ComplianceManagement.GetComplianceID(ComplianceinstanceID));
                    var Compliancedetails = Business.ComplianceManagement.GetDetailsCustomerID(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt32(CompID));                    
                    if (Compliancedetails != null)
                    {
                        int scheduleOnId = Convert.ToInt32(Request.QueryString["sId"].ToString());

                        var Compliancesceduledetails = Business.ComplianceManagement.GetDetailsComplianceScheduled(Convert.ToInt32(ComplianceinstanceID), Convert.ToInt32(scheduleOnId));

                        if (Compliancesceduledetails != null)
                        {
                            if (Compliancesceduledetails.RoleID == 3)
                            {
                                var statusList = ComplianceStatusManagement.GetStatusListByCustomer(false, "3");
                                ddlStatus.DataSource = statusList;
                                ddlStatus.DataBind();
                                ddlStatus.Items.Insert(0, new ListItem("< Select >", "-1"));
                            }
                            else
                            {
                                var statusList = ComplianceStatusManagement.GetStatusListByCustomer(true, "3");
                                ddlStatus.DataSource = statusList;
                                ddlStatus.DataBind();
                                ddlStatus.Items.Insert(0, new ListItem("< Select >", "-1"));
                            }
                        }
                        else
                        {
                            var statusList = ComplianceStatusManagement.GetStatusListByCustomer(Compliancedetails.IsApproverCompulsory, "3");
                            ddlStatus.DataSource = statusList;
                            ddlStatus.DataBind();
                            ddlStatus.Items.Insert(0, new ListItem("< Select >", "-1"));
                        }
                    }
                }
                else
                {
                    // STT Change- Add Status
                    string customer = ConfigurationManager.AppSettings["NotCompliedCustID"].ToString();
                    List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                    if (PenaltyNotDisplayCustomerList.Count > 0)
                    {
                        if (CustomerID == 0)
                        {
                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        }
                        foreach (string PList in PenaltyNotDisplayCustomerList)
                        {
                            if (PList == CustomerID.ToString())
                            {
                                IsNotCompiled = true;
                                break;
                            }
                        }
                    }

                    // Zomato Change- Add Status -CompliedButDocumentPending
                    string customerList = ConfigurationManager.AppSettings["CompliedButDocumentPendingCustID"].ToString();
                    List<string> CompliedButDocumentPendingCustIDCustomerList = customerList.Split(',').ToList();
                    if (CompliedButDocumentPendingCustIDCustomerList.Count > 0)
                    {
                        if (CustomerID == 0)
                        {
                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        }
                        foreach (string PList in CompliedButDocumentPendingCustIDCustomerList)
                        {
                            if (PList == CustomerID.ToString())
                            {
                                IsCompliedButDocumentPending = true;
                                break;
                            }
                        }
                    }

                    if (IsNotCompiled == true)
                    {
                        var statusList = ComplianceStatusManagement.GetNotCompliedStatusList();
                        List<ComplianceStatu> allowedStatusList = null;
                        List<ComplianceStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
                        List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

                        allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.Name).ToList();

                        ddlStatus.DataSource = allowedStatusList;
                        ddlStatus.DataBind();

                        ddlStatus.Items.Insert(0, new ListItem("< Select >", "-1"));
                    }
                    else if (IsCompliedButDocumentPending == true)
                    {
                        var statusList = ComplianceStatusManagement.GetCompliedButDocumentPendingList();
                        List<ComplianceStatu> allowedStatusList = null;
                        List<ComplianceStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
                        List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

                        allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.Name).ToList();

                        ddlStatus.DataSource = allowedStatusList;
                        ddlStatus.DataBind();

                        ddlStatus.Items.Insert(0, new ListItem("< Select >", "-1"));
                    }
                    else
                    {
                        var statusList = ComplianceStatusManagement.GetStatusList();
                        List<ComplianceStatu> allowedStatusList = null;
                        List<ComplianceStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
                        List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

                        allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.Name).ToList();

                        ddlStatus.DataSource = allowedStatusList;
                        ddlStatus.DataBind();

                        ddlStatus.Items.Insert(0, new ListItem("< Select >", "-1"));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void btnSave_Click(object sender, EventArgs e)
        {

            var CSOID = Convert.ToInt64(hdnComplianceScheduledOnId.Value);
            var RCT = Business.ComplianceManagement.GetCurrentStatusByComplianceID((int)CSOID);            
            if (RCT.ComplianceStatusID == 2 || RCT.ComplianceStatusID == 3 || RCT.ComplianceStatusID == 4 || RCT.ComplianceStatusID == 5 || RCT.ComplianceStatusID == 18)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "This compliance is already submitted for review or already closed.";
            }
            else
            {
                #region Save Code
                List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                List<KeyValuePair<string, Byte[]>> GSTFilelist = new List<KeyValuePair<string, Byte[]>>();
                int RemarkID = -1;
                try
                {
                    Labelmsg.Text = "";
                    Labelmsg.Style.Add("display", "none");
                    Labelmsg.Style.Remove("class");

                    #region File
                    if (txtInterest.Text == "")
                        txtInterest.Text = "0";
                    if (txtPenalty.Text == "")
                        txtPenalty.Text = "0";
                    if (txtValueAsPerSystem.Text == "")
                        txtValueAsPerSystem.Text = "0";
                    if (txtValueAsPerReturn.Text == "")
                        txtValueAsPerReturn.Text = "0";
                    if (txtLiabilityPaid.Text == "")
                        txtLiabilityPaid.Text = "0";
                    long? StatusID = ComplianceManagement.Business.ComplianceManagement.GetCurrentStatusByComplianceID(Convert.ToInt32(hdnComplianceScheduledOnId.Value)).ComplianceStatusID;
                    var PenaltySubmit = "";
                    if (ddlStatus.SelectedValue == "3")
                    {
                        PenaltySubmit = "P";
                    }  
                    //if(IsRemark == true)
                    //{
                    //    if (Remarkddl.SelectedValue != null || Remarkddl.SelectedValue != "")
                    //    {
                    //        RemarkID = Convert.ToInt32(Remarkddl.SelectedValue);
                    //    }
                    //}                                                           
                  
                    string tbxDate1 = Request.Form[tbxDate.UniqueID];
                    ComplianceTransaction transaction = new ComplianceTransaction()
                    {
                        ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                        ComplianceInstanceId = Convert.ToInt64(hdnComplianceInstanceID.Value),
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedByText = AuthenticationHelper.User,
                        StatusId = Convert.ToInt32(ddlStatus.SelectedValue),
                        StatusChangedOn = DateTime.ParseExact(tbxDate1, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        Remarks = tbxRemarks.Text,
                        Interest = Convert.ToDecimal(txtInterest.Text),
                        Penalty = Convert.ToDecimal(txtPenalty.Text),
                        ComplianceSubTypeID = Convert.ToInt32(lblNatureofcomplianceID.Text),
                        ValuesAsPerSystem = Convert.ToDecimal(txtValueAsPerSystem.Text),
                        ValuesAsPerReturn = Convert.ToDecimal(txtValueAsPerReturn.Text),
                        LiabilityPaid = Convert.ToDecimal(txtLiabilityPaid.Text),
                        IsPenaltySave = chkPenaltySave.Checked,
                        PenaltySubmit = PenaltySubmit,
                        RemarkTypeID = RemarkID,  
                    };
                    if(IsRemark == true)
                    {
                        if (ddlStatus.SelectedValue == "18" || ddlStatus.SelectedValue == "3")
                        {
                            if (Remarkddl.SelectedItem.Text != "Others")
                            {
                                transaction.Remarks = Remarkddl.SelectedItem.Text;
                            }
                        }
                    }
                   
                    var customizedid = CustomerManagement.GetCustomizedCustomerid(Convert.ToInt32(AuthenticationHelper.CustomerID), "TaxReportFields");
                    if (customizedid == AuthenticationHelper.CustomerID)
                    {
                        transaction.ChallanNo = txtChallanNo.Text;
                        transaction.ChallanAmount = txtChallanAmount.Text;
                        if (tbxChallanDate.Text!=null && !string.IsNullOrEmpty(tbxChallanDate.Text))
                        {
                            transaction.Challanpaiddate = DateTime.ParseExact(tbxChallanDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                        }
                        transaction.BankName = txtBankName.Text;
                    }
                        string fileNameGST = "";
                    string filepathvalueGST = "";
                    string fileKeyGST = "";
                    string versionGST = "";
                    long? FileSizeGST = null;
                    bool IsLinkGST =false;
                    string directoryPath = null;

                    var leavedetails = Business.ComplianceManagement.GetUserLeavePeriodExists(AuthenticationHelper.UserID, "P");
                    if (leavedetails != null)
                    {
                        transaction.OUserID = leavedetails.OldPerformerID;
                    }
                    List<FileData> files = new List<FileData>();
                    List<FileData> GSTfiles = new List<FileData>();
                    List<KeyValuePair<string, int>> listGST = new List<KeyValuePair<string, int>>();
                    List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                    bool blankfileCount = true;
                    var TempDocData = Business.ComplianceManagement.GetTempComplianceDocumentData(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                    if (TempDocData.Count > 0)
                    {
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            int? customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                            var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));
                            string version = null;
                            if (StatusID == 6) //if previous status is rejected(not complied) than for uploading new documents new version is created
                            {
                                version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(hdnComplianceScheduledOnId.Value));
                                directoryPath = "avacomdocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                            }
                            else
                            {
                                version = "1.0";
                                directoryPath = "avacomdocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                            }
                            foreach (var item in TempDocData)
                            {
                                if (item.ISLink)
                                {
                                    String fileName = "";
                                    if (item.DocType == "S")
                                    {
                                        fileName = "ComplianceDoc_" + item.DocName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                        listGST.Add(new KeyValuePair<string, int>(fileName, 1));
                                    }
                                    else
                                    {
                                        fileName = "WorkingFiles_" + item.DocName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 2));
                                        listGST.Add(new KeyValuePair<string, int>(fileName, 2));
                                    }

                                    fileNameGST = fileName;
                                    filepathvalueGST = item.DocPath;
                                    IsLinkGST = true;
                                    versionGST = version;

                                    FileData file = new FileData()
                                    {
                                        Name = fileName,
                                        FilePath = item.DocPath,
                                        VersionDate = DateTime.UtcNow,
                                        ISLink = true,
                                        Version = version
                                    };
                                    files.Add(file);

                                    FileData file1 = new FileData()
                                    {
                                        Name = fileNameGST,
                                        FilePath = filepathvalueGST,
                                        VersionDate = DateTime.UtcNow,
                                        ISLink = true,
                                        Version = versionGST
                                    };

                                    GSTfiles.Add(file1);
                                }
                                else
                                {
                                    String fileName = "";
                                    if (item.DocType == "S")
                                    {
                                        fileName = "ComplianceDoc_" + item.DocName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                    }
                                    else
                                    {
                                        fileName = "WorkingFiles_" + item.DocName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 2));
                                    }
                                    Guid fileKey = Guid.NewGuid();
                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));
                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));
                                    if (item.DocData.Length > 0)
                                    {
                                        string filepathvalue = string.Empty;
                                        string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                        filepathvalue = vale.Replace(@"\", "/");
                                        

                                        fileNameGST = fileName;
                                        filepathvalueGST = filepathvalue;
                                        fileKeyGST = fileKey.ToString();
                                        versionGST = version;
                                        FileSizeGST = item.FileSize;

                                        FileData file = new FileData()
                                        {
                                            Name = fileName,
                                            FilePath = filepathvalue,
                                            FileKey = fileKey.ToString(),
                                            Version = version,
                                            VersionDate = DateTime.UtcNow,
                                            FileSize = item.FileSize,
                                        };
                                        files.Add(file);

                                        FileData gstfiles2 = new FileData()
                                        {
                                            Name = fileNameGST,
                                            FilePath = filepathvalueGST,
                                            FileKey = fileKeyGST.ToString(),
                                            Version = versionGST,
                                            VersionDate = DateTime.UtcNow,
                                            FileSize = FileSizeGST,
                                        };

                                        GSTfiles.Add(gstfiles2);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(item.DocName))
                                            blankfileCount = false;
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            int? customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                            var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));
                            string version = null;
                            if (StatusID == 6) //if previous status is rejected(not complied) than for uploading new documents new version is created
                            {
                                version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(hdnComplianceScheduledOnId.Value));
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {
                                    directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                                }
                                else
                                {
                                    directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/" + version);
                                }
                            }
                            else
                            {
                                version = "1.0";
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {
                                    directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                                }
                                else
                                {
                                    directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/" + version);
                                }
                            }
                            DocumentManagement.CreateDirectory(directoryPath);
                            foreach (var item in TempDocData)
                            {
                                if (item.ISLink)
                                {
                                    String fileName = "";
                                    if (item.DocType == "S")
                                    {
                                        fileName = "ComplianceDoc_" + item.DocName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                        listGST.Add(new KeyValuePair<string, int>(fileName, 1));
                                    }
                                    else
                                    {
                                        fileName = "WorkingFiles_" + item.DocName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 2));
                                        listGST.Add(new KeyValuePair<string, int>(fileName, 2));
                                    }

                                    fileNameGST = fileName;
                                    filepathvalueGST = item.DocPath;
                                    IsLinkGST = true;
                                    versionGST = version;

                                    FileData file = new FileData()
                                    {
                                        Name = fileName,
                                        FilePath = item.DocPath,
                                        VersionDate = DateTime.UtcNow,
                                        ISLink = true,
                                        Version = version
                                    };
                                    files.Add(file);

                                    FileData file1 = new FileData()
                                    {
                                        Name = fileNameGST,
                                        FilePath = filepathvalueGST,
                                        VersionDate = DateTime.UtcNow,
                                        ISLink = true,
                                        Version = versionGST
                                    };

                                    GSTfiles.Add(file1);
                                }
                                else
                                {
                                    //HttpPostedFile uploadfile = fileCollection[i];
                                    //string[] keys = fileCollection.Keys[i].Split('$');
                                    String fileName = "";
                                    if (item.DocType == "S")
                                    {
                                        fileName = "ComplianceDoc_" + item.DocName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                    }
                                    else
                                    {
                                        fileName = "WorkingFiles_" + item.DocName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 2));
                                    }
                                    Guid fileKey = Guid.NewGuid();
                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));
                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));
                                    //GSTFilelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));

                                    if (item.DocData.Length > 0)
                                    {
                                        string filepathvalue = string.Empty;
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                            filepathvalue = vale.Replace(@"\", "/");
                                        }
                                        else
                                        {
                                            filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        }

                                        fileNameGST = fileName;
                                        filepathvalueGST = filepathvalue;
                                        fileKeyGST = fileKey.ToString();
                                        versionGST = version;
                                        FileSizeGST = item.FileSize;

                                        FileData file = new FileData()
                                        {
                                            Name = fileName,
                                            FilePath = filepathvalue,
                                            FileKey = fileKey.ToString(),
                                            //Version = "1.0",
                                            Version = version,
                                            VersionDate = DateTime.UtcNow,
                                            FileSize = item.FileSize,
                                        };
                                        files.Add(file);

                                        FileData gstfiles2 = new FileData()
                                        {
                                            Name = fileNameGST,
                                            FilePath = filepathvalueGST,
                                            FileKey = fileKeyGST.ToString(),
                                            Version = versionGST,
                                            VersionDate = DateTime.UtcNow,
                                            FileSize = FileSizeGST,
                                        };

                                        GSTfiles.Add(gstfiles2);
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(item.DocName))
                                            blankfileCount = false;
                                    }
                                }
                            }
                            #endregion
                        }
                    }

                    #region Comment
                    //List<FileData> files = new List<FileData>();
                    //    List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();

                    //    HttpFileCollection fileCollection = Request.Files;
                    //    bool blankfileCount = true;

                    //    if (fileCollection.Count > 0)
                    //    {
                    //        int? customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                    //        var InstanceData = DocumentManagement.GetComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));
                    //        string directoryPath = null;
                    //        string version = null;
                    //        if (StatusID == 6) //if previous status is rejected(not complied) than for uploading new documents new version is created
                    //        {
                    //            version = DocumentManagement.GetDocumnetVersion(Convert.ToInt32(hdnComplianceScheduledOnId.Value));
                    //            //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    //            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    //            {
                    //                directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value +"\\"+ version;
                    //            }
                    //            else
                    //            {
                    //                directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value +"/"+ version);
                    //            }
                    //        }
                    //        else
                    //        {
                    //            version = "1.0";
                    //            //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    //            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    //            {
                    //                directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "\\AvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                    //            }
                    //            else
                    //            {
                    //                directoryPath = Server.MapPath("~/AvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/" + version);
                    //            }
                    //        }

                    //        DocumentManagement.CreateDirectory(directoryPath);

                    //        for (int i = 0; i < fileCollection.Count; i++)
                    //        {
                    //            HttpPostedFile uploadfile = fileCollection[i];
                    //            string[] keys = fileCollection.Keys[i].Split('$');
                    //            String fileName = "";
                    //            if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                    //            {
                    //                fileName = "ComplianceDoc_" + uploadfile.FileName;
                    //                list.Add(new KeyValuePair<string, int>(fileName, 1));
                    //            }
                    //            else
                    //            {
                    //                fileName = "WorkingFiles_" + uploadfile.FileName;
                    //                list.Add(new KeyValuePair<string, int>(fileName, 2));
                    //            }

                    //            Guid fileKey = Guid.NewGuid();
                    //            string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(uploadfile.FileName));

                    //            Stream fs = uploadfile.InputStream;
                    //            BinaryReader br = new BinaryReader(fs);
                    //            Byte[] bytes = br.ReadBytes((Int32) fs.Length);

                    //            Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, bytes));

                    //            if (uploadfile.ContentLength > 0)
                    //            {
                    //                //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    //                string filepathvalue = string.Empty;
                    //                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    //                {
                    //                    string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                    //                    filepathvalue = vale.Replace(@"\", "/");
                    //                }
                    //                else
                    //                {
                    //                    filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                    //                }
                    //                FileData file = new FileData()
                    //                {
                    //                    Name = fileName,
                    //                    FilePath = filepathvalue,
                    //                    FileKey = fileKey.ToString(),
                    //                    //Version = "1.0",
                    //                    Version = version,
                    //                    VersionDate = DateTime.UtcNow,
                    //                };

                    //                files.Add(file);
                    //            }
                    //            else
                    //            {
                    //                if (!string.IsNullOrEmpty(uploadfile.FileName))
                    //                    blankfileCount = false;
                    //            }
                    //        }
                    //    }
                    #endregion

                    bool flag = false;
                    bool flag1 = false;
                    if (blankfileCount)
                    {
                        flag = Business.ComplianceManagement.CreateTransaction(transaction, files, list, Filelist, directoryPath, Convert.ToInt64(hdnComplianceScheduledOnId.Value), CustId);

                        #region GST Bulk Compliance Perform
                        if (flag == true)
                        {
                            foreach (GridViewRow gvrow in grdGstMappedCompliance.Rows)
                            {
                                CheckBox chk = (CheckBox)gvrow.FindControl("chkCompliance");
                                Label InstanceID = (Label)gvrow.FindControl("lblInstanceID");
                                Label ComplianceScheduleOnID = (Label)gvrow.FindControl("lblComplianceScheduleOnID");
                                Label CustomerBranchID = (Label)gvrow.FindControl("lblCustomerBranchID");
                                if (chk != null & chk.Checked)
                                {
                                    ComplianceTransaction GSTtransaction = new ComplianceTransaction()
                                    {
                                        ComplianceScheduleOnID = Convert.ToInt64(ComplianceScheduleOnID.Text),
                                        ComplianceInstanceId = Convert.ToInt64(InstanceID.Text),
                                        CreatedBy = AuthenticationHelper.UserID,
                                        CreatedByText = AuthenticationHelper.User,
                                        StatusId = Convert.ToInt32(ddlStatus.SelectedValue),
                                        StatusChangedOn = DateTime.ParseExact(tbxDate1, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                        Remarks = tbxRemarks.Text,
                                        Interest = Convert.ToDecimal(txtInterest.Text),
                                        Penalty = Convert.ToDecimal(txtPenalty.Text),
                                        ComplianceSubTypeID = Convert.ToInt32(lblNatureofcomplianceID.Text),
                                        ValuesAsPerSystem = Convert.ToDecimal(txtValueAsPerSystem.Text),
                                        ValuesAsPerReturn = Convert.ToDecimal(txtValueAsPerReturn.Text),
                                        LiabilityPaid = Convert.ToDecimal(txtLiabilityPaid.Text),
                                        IsPenaltySave = chkPenaltySave.Checked,
                                        PenaltySubmit = PenaltySubmit,
                                        RemarkTypeID = RemarkID
                                    };
                                    if (IsRemark == true)
                                    {
                                        if (ddlStatus.SelectedValue == "18" || ddlStatus.SelectedValue == "3")
                                        {
                                            if (Remarkddl.SelectedItem.Text != "Others")
                                            {
                                                GSTtransaction.Remarks = Remarkddl.SelectedItem.Text;
                                            }
                                        }
                                    }

                                    flag = Business.ComplianceManagement.CreateTransactionBulkGST(GSTtransaction, GSTfiles, listGST);
                                }
                            }
                        }
                        #endregion

                        //try
                        //{
                        //    foreach (var item in TempDocData)
                        //    {
                        //        if (item.ISLink == false)
                        //        {
                        //            string path = Server.MapPath(item.DocPath);
                        //            FileInfo file = new FileInfo(path);
                        //            if (file.Exists) //check file exsit or not
                        //            {
                        //                file.Delete();
                        //            }
                        //        }
                        //    }
                        //}
                        //catch (Exception)
                        //{
                        //}
                        
  
                        flag1 = Business.ComplianceManagement.DeleteTempDocumentFileFromScheduleOnID(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload blank files.')", true);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please do not upload blank files.";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "topFunction", "topFunction();", true);

                    }

                    //bool flag = true;
                    if (flag != true)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "topFunction", "topFunction();", true);
                    }
                    try
                    {
                        Business.ComplianceManagement.BindDatainRedis(Convert.ToInt64(hdnComplianceInstanceID.Value), Convert.ToInt32(AuthenticationHelper.CustomerID));
                    }
                    catch { }

                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('save successfully')", true);

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "CloseAndBindData();", true);
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseCalenderPERPop();", true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "topFunction", "topFunction();", true);

                    #endregion
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "topFunction", "topFunction();", true);
                }
                #endregion
            }

        }

        protected void grdTransactionHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DOWNLOAD_FILE"))
                {
                    int fileID = Convert.ToInt32(e.CommandArgument);
                    var file = Business.ComplianceManagement.GetFile(fileID);
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                    //Response.BinaryWrite(file.Data); // create the file
                    Response.Flush(); // send it to the client to download
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(Convert.ToInt32(hdlSelectedDocumentID.Value));

                Response.Buffer = true;
                //Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.Data); // create the file
                Response.End();
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void lbDownloadSample_Click(object sender, EventArgs e)
        {
            try
            {
                var complianceform = Business.ComplianceManagement.GetComplianceFileNameMultiple(Convert.ToInt32(lbDownloadSample.CommandArgument));
                int complianceID1 = Convert.ToInt32(lbDownloadSample.CommandArgument);
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    int i = 0;
                    foreach (var file in complianceform)
                    {
                        if (file.FilePath != null)
                        {
                            string[] filename = file.Name.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            ComplianceZip.AddEntry(str, Business.DocumentManagement.ReadDocFiles(Server.MapPath(file.FilePath)));
                            i++;
                        }
                    }

                    string fileName = "ComplianceID_" + complianceID1 + "_SampleForms.zip";
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = 0;
                    byte[] data = zipMs.ToArray();

                    Response.Buffer = true;

                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename= " + fileName);
                    Response.BinaryWrite(data);
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }

                //var file = Business.ComplianceManagement.GetComplianceFormByID(Convert.ToInt32(lbDownloadSample.CommandArgument));
                //Response.Buffer = true;
                //Response.Clear();
                //Response.ClearContent();
                //Response.ContentType = "application/octet-stream";
                //Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.FileData); // create the file
                //Response.Flush(); // send it to the client to download

                //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {

                DateTime date1 = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date1))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer2", string.Format("initializeDatePickerforPerformer2(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer2", "initializeDatePickerforPerformer2(null);", true);
                }
                DateTime DueDate = DateTime.ParseExact(hiddenDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                DateTime dtcurrent = DateTime.Now;
                if (DueDate > dtcurrent)  //Upcoming
                {
                }
                else
                {
                    if (lblDueDate.Text != "")
                    {
                        if (ddlStatus.SelectedValue == "3")
                        {
                            DateTime Date = DateTime.ParseExact(hiddenDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            Date = Date.AddDays(1);
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerOverDue", string.Format("initializeDatePickerOverDue(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                        }
                        if (ddlStatus.SelectedValue == "2")
                        {
                            DateTime Date = DateTime.ParseExact(hiddenDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerInTime", string.Format("initializeDatePickerInTime(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforChallanPaidDate", "initializeDatePickerforChallanPaidDate(null);", true);

                // ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Script", "initializeComboboxUpcoming();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndexHistory"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        public void OpenTransactionPage(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {
                tbxRemarks.Text = tbxDate.Text = string.Empty;
                BindTransactionDetails(ScheduledOnID, complianceInstanceID);
                ViewState["ScheduledOnID"] = ScheduledOnID;
                ViewState["complianceInstanceID"] = complianceInstanceID;

                //upUsers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public event EventHandler OnSaved;

        protected void btnUpload_Click(object sender, EventArgs e)
        {

            try
            {
                HttpFileCollection fileCollection = Request.Files;
                for (int i = 0; i < fileCollection.Count; i++)
                {
                    HttpPostedFile uploadfile = fileCollection[i];
                    string fileName = Path.GetFileName(uploadfile.FileName);
                    if (uploadfile.ContentLength > 0)
                    {
                        //uploadfile.SaveAs(Server.MapPath("~/UploadFiles/") + fileName);
                        //lblMessage.Text += fileName + "Saved Successfully<br>";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    DeleteFile(Convert.ToInt32(e.CommandArgument));
                    BindDocument(Convert.ToInt32(ViewState["complianceInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    DeleteFile(Convert.ToInt32(e.CommandArgument));
                    BindDocument(Convert.ToInt32(ViewState["complianceInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = string.Empty;
                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {

                        string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                        filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);

                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void DeleteFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);
                if (file != null)
                {
                    string path = string.Empty;
                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        string pathvalue = file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name);
                        path = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);

                        // path = file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name);
                    }
                    else
                    {
                        path = Server.MapPath(file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name));
                    }
                    DocumentManagement.DeleteFile(path, fileId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lbtLinkDocbutton = (LinkButton)e.Item.FindControl("lbtLinkDocbutton");
                scriptManager.RegisterAsyncPostBackControl(lbtLinkDocbutton);

            }
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lbtLinkbutton = (LinkButton)e.Item.FindControl("lbtLinkbutton");
                scriptManager.RegisterAsyncPostBackControl(lbtLinkbutton);
            }
        }

        protected void lnkSampleForm_Click(object sender, EventArgs e)
        {
            try
            {
                if (lnkSampleForm.Text != "")
                {
                    string url = lnkSampleForm.Text;

                    string fullURL = "window.open('" + url + "', '_blank', 'height=500,width=800,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
                    lnkSampleForm.Attributes.Add("OnClick", fullURL);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkViewSampleForm_Click(object sender, EventArgs e)
        {

        }

        //protected void chkPenaltySave_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkPenaltySave.Checked == true)
        //    {
        //        txtInterest.Text = "0";
        //        txtPenalty.Text = "0";
        //        txtInterest.ReadOnly = true;
        //        txtPenalty.ReadOnly = true;
        //    }
        //    else
        //    {
        //        txtInterest.Text = "";
        //        txtPenalty.Text = "";
        //        txtInterest.ReadOnly = false;
        //        txtPenalty.ReadOnly = false;
        //    }

        //}


        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                chkPenaltySave.Checked = false;
                txtInterest.Text = "";
                txtPenalty.Text = "";

                txtInterest.ReadOnly = false;
                txtPenalty.ReadOnly = false;
                if (IsRemark == true)
                {
                    if (ddlStatus.SelectedValue == "18" || ddlStatus.SelectedValue == "3" )
                    {
                        BindRemark(Convert.ToInt32(ddlStatus.SelectedValue));
                        //    ddlRemarkNew.ClearSelection();
                        Remarkddl.Visible = true;
                        tbxRemarks.Visible = false;
                    }
                    else
                    {
                        tbxRemarks.Visible = true;
                        Remarkddl.Visible = false;
                    }
                }
                else
                {
                    tbxRemarks.Visible = true;
                }
                if (ddlStatus.SelectedValue == "3")
                {
                    // Penalty visible for customer
                    string customer = ConfigurationManager.AppSettings["PenaltynotDisplayCustID"].ToString();
                    List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                    if (PenaltyNotDisplayCustomerList.Count > 0)
                    {
                        int customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                        foreach (string PList in PenaltyNotDisplayCustomerList)
                        {
                            if (PList == customerID.ToString())
                            {
                                IsPenaltyVisible = false;
                                break;
                            }
                        }
                    }
                    if (IsPenaltyVisible == true)
                    {
                        var data = Business.ComplianceManagement.CheckMonetary(Convert.ToInt32(lblComplianceID.Text));
                        if (data == null)
                        {
                            fieldsetpenalty.Visible = false;
                        }
                        else
                        {
                            if (data.NonComplianceType == 1)
                            {
                                fieldsetpenalty.Visible = false;
                            }
                            else
                            {
                                fieldsetpenalty.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        fieldsetpenalty.Visible = false;
                    }
                    // Penalty visible for customer
                }
                else
                {
                    fieldsetpenalty.Visible = false;
                }


                if (lblDueDate.Text != "")
                {
                    DateTime Date = DateTime.ParseExact(hiddenDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    DateTime dtcurrent = DateTime.Now;
                    if (Date > dtcurrent)  //Upcoming
                    {
                        //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerforPerformer2", "initializeDatePickerforPerformer2(null);", true);                        
                        //if (ddlStatus.SelectedValue == "3")
                        //{
                        //    Date = Date.AddDays(1);
                        //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerUpcoming", string.Format("initializeDatePickerUpcoming(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                        //}
                        //if (ddlStatus.SelectedValue == "2")
                        //{
                        //    //DateTime Date = DateTime.ParseExact(hiddenDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerInTime", string.Format("initializeDatePickerInTime(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                        //}
                    }
                    else // Overdue
                    {
                        if (ddlStatus.SelectedValue == "3")
                        {
                            Date = Date.AddDays(1);
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerOverDue", string.Format("initializeDatePickerOverDue(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                        }
                        if (ddlStatus.SelectedValue == "2")
                        {
                            //DateTime Date = DateTime.ParseExact(hiddenDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerInTime", string.Format("initializeDatePickerInTime(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                        }
                    }
                }

                btnSave.Visible = Convert.ToBoolean(ViewState["btnSave"]);
                btnSaveDOCNotCompulsory.Visible = Convert.ToBoolean(ViewState["btnSaveDOCNotCompulsory"]);
                lblDocComplasary.Visible = Convert.ToBoolean(ViewState["lblDocComplasary"]);


                lblMandatoryDoc.Visible = true;

                if (ddlStatus.SelectedValue == "16" || ddlStatus.SelectedValue == "18" || ddlStatus.SelectedValue == "19")
                {
                    btnSaveDOCNotCompulsory.Visible = true;
                    btnSave.Visible = false;
                    IsRemarkCompulsary = false;
                    lblDocComplasary.Visible = false;
                    lblMandatoryDoc.Visible = false;
                }
                if (ddlStatus.SelectedValue == "3" || ddlStatus.SelectedValue == "18")
                {
                    IsRemarkCompulsary = true;
                }
                else
                {
                    IsRemarkCompulsary = false;
                }

                DateTime currentdate = DateTime.Now;
                DateTime dueDate = DateTime.ParseExact(hiddenDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                if (dueDate < currentdate)
                {
                    if (ddlStatus.SelectedValue != "10")
                    {
                        IsRemarkCompulsary = false;
                    }
                    else
                    {
                        IsRemarkCompulsary = true;
                    }
                }
                if (ddlStatus.SelectedValue == "18")
                {
                    tbxDate.Text = DateTime.Today.Date.ToString("dd-MM-yyyy");
                    //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerInTime", string.Format("initializeDatePickerInTime(new Date({0}, {1}, {2}));", currentdate.Year, currentdate.Month, currentdate.Day), true);
                }
                var details = Business.ComplianceManagement.GetCustomerID(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (details != null)
                {
                    if (ddlStatus.SelectedValue == "18" || ddlStatus.SelectedValue == "16")
                    {
                        IsRemarkCompulsary = true;
                        btnSaveDOCNotCompulsory.Visible = true;
                        btnSave.Visible = false;
                        lblDocComplasary.Visible = false;
                        lblMandatoryDoc.Visible = false;
                    }
                }

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fCalltreeCollapsed();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private bool BindSubTasks(long ComplianceScheduleOnID, int roleID, string period, int CustomerBranchid)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var documentData = (from row in entities.SP_TaskInstanceTransactionStatutoryView(customerID)
                                        where row.ParentID == null
                                        && row.ForMonth == period
                                        && row.RoleID == roleID
                                        && row.CustomerBranchID == CustomerBranchid
                                        && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                        select row).ToList();

                    if (documentData.Count != 0)
                    {
                        divTask.Visible = true;
                        gridSubTask.DataSource = documentData;
                        gridSubTask.DataBind();

                        var closedSubTaskCount = documentData.Where(entry => (entry.TaskStatusID == 4 || entry.TaskStatusID == 5)).Count();

                        if (documentData.Count == closedSubTaskCount)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        divTask.Visible = false;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }


        protected string GetUserName(long taskInstanceID, long taskScheduleOnID, int roleID, byte taskType)
        {
            try
            {
                string result = "";

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                result = TaskManagment.GetTaskAssignedUser(customerID, taskType, taskInstanceID, taskScheduleOnID, roleID);

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cv.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }
        }

        protected void gridSubTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (e.CommandName.Equals("Download"))
                    {
                        int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                string directoryPath = "~/TempDocuments/AWS/" + User;

                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(Server.MapPath(directoryPath));
                                }

                                if (taskDocument.Count > 0)
                                {
                                    foreach (var item in taskDocument)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + item.FilePath;
                                            request.Key = item.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + item.FileName);
                                        }
                                    }

                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(directoryPath), eachFile.FileName);
                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = eachFile.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                           
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, DocumentManagement.ReadDocFiles(filePath));
                                          
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                            using (ZipFile ComplianceZip = new ZipFile())
                            {
                                if (taskDocument.Count > 0)
                                {
                                    string fileName = string.Empty;

                                    GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                    Label lblTaskTitle = null;

                                    if (row != null)
                                    {
                                        lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                        if (lblTaskTitle != null)
                                            fileName = lblTaskTitle.Text + "-Documents";

                                        if (fileName.Length > 250)
                                            fileName = "TaskDocuments";
                                    }

                                    ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                    int i = 0;
                                    foreach (var eachFile in taskDocument)
                                    {
                                        //comment by rahul on 20 JAN 2017
                                        string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                        //string filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));

                                        if (eachFile.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = eachFile.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            if (eachFile.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }

                                var zipMs = new MemoryStream();
                                ComplianceZip.Save(zipMs);
                                zipMs.Position = 0;
                                byte[] data = zipMs.ToArray();

                                Response.Buffer = true;

                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/zip";
                                Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                                Response.BinaryWrite(data);
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                            #endregion
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptComplianceVersionView.DataSource = entitiesData;
                                        rptComplianceVersionView.DataBind();


                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                        string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                        string directoryPath = "~/TempDocuments/AWS/" + User;
                                        if (!Directory.Exists(directoryPath))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(directoryPath));

                                        }

                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(file.FileName);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessage.Text = "";
                                                lblMessage.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;
                                                subTaskDocViewPath = filePath1;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);
                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview('" + subTaskDocViewPath + "');", true);
                                                lblMessage.Text = "";
                                                UpdatePanel4.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        rptComplianceVersionView.DataSource = entitiesData;
                                        rptComplianceVersionView.DataBind();

                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessage.Text = "";
                                                lblMessage.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();

                                                subTaskDocViewPath = FileName;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview('" + subTaskDocViewPath + "');", true);
                                                lblMessage.Text = "";
                                                UpdatePanel4.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                        #region

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);
                int taskFileidID = Convert.ToInt32(commandArgs[2]);
                if (taskScheduleOnID != 0)
                {
                    if (e.CommandName.Equals("View"))
                    {
                        #region
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();
                        List<GetTaskDocumentView> taskFileData = new List<GetTaskDocumentView>();
                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
                        taskFileData = taskDocumenttoView;
                        taskDocumenttoView = taskDocumenttoView.Where(entry => entry.FileID == taskFileidID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }
                            int CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                            var AWSData = AmazonS3.GetAWSStorageDetail(CustId);
                            if (AWSData != null)
                            {
                                #region AWS Storage
                                if (entitiesData.Count > 0)
                                {
                                    rptComplianceVersionView.DataSource = taskFileData;
                                    rptComplianceVersionView.DataBind();

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + AuthenticationHelper.CustomerID + "" + FileDate;
                                    string directoryPath = "~/TempDocuments/AWS/" + User;

                                    if (!Directory.Exists(directoryPath))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(directoryPath));
                                    }
                                    
                                    foreach (var file in taskDocumenttoView)
                                    {
                                        using (IAmazonS3 client = new AmazonS3Client(AWSData.AccessKeyID, AWSData.SecretKeyID, RegionEndpoint.USEast1))
                                        {
                                            GetObjectRequest request = new GetObjectRequest();
                                            request.BucketName = AWSData.BucketName + @"/" + file.FilePath;
                                            request.Key = file.FileName;
                                            GetObjectResponse response = client.GetObject(request);
                                            response.WriteResponseStreamToFile(Server.MapPath(directoryPath) + "\\" + file.FileName);
                                        }

                                        string filePath = Path.Combine(Server.MapPath(directoryPath), file.FileName);

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessage.Text = "";
                                                lblMessage.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                            }
                                            else
                                            {
                                                string filePath1 = directoryPath + "/" + file.FileName;                                           
                                                subTaskDocViewPath = filePath1;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview('" + subTaskDocViewPath + "');", true);
                                                lblMessage.Text = "";
                                                //UpdatePanel4.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Normal Storage
                                if (entitiesData.Count > 0)
                                {
                                    rptComplianceVersionView.DataSource = taskFileData;
                                    rptComplianceVersionView.DataBind();

                                    foreach (var file in taskDocumenttoView)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string Folder = "~/TempFiles";
                                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                            string DateFolder = Folder + "/" + File;

                                            string extension = System.IO.Path.GetExtension(filePath);
                                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                            {
                                                lblMessage.Text = "";
                                                lblMessage.Text = "Zip file can't view please download it";
                                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                                if (!Directory.Exists(DateFolder))
                                                {
                                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                                }

                                                string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                                string FileName = DateFolder + "/" + User + "" + extension;

                                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (file.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();

                                                subTaskDocViewPath = FileName;
                                                subTaskDocViewPath = subTaskDocViewPath.Substring(2, subTaskDocViewPath.Length - 2);

                                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendoctaskfileReview('" + subTaskDocViewPath + "');", true);
                                                lblMessage.Text = "";
                                                //UpdatePanel4.Update();
                                            }
                                        }
                                        else
                                        {
                                            lblMessage.Text = "There is no file to preview";
                                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendoctaskfileReviewPopUp();", true);
                                        }
                                        break;
                                    }
                                }
                                #endregion
                            }
                        }
                        #endregion                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gridSubTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    Label lblSlashReview = (Label)e.Row.FindControl("lblSlashReview");
                    LinkButton btnSubTaskDocView = (LinkButton)e.Row.FindControl("btnSubTaskDocView");
                    LinkButton btnSubTaskDocDownload = (LinkButton)e.Row.FindControl("btnSubTaskDocDownload");
                    CheckBox chkTask = (CheckBox)e.Row.FindControl("chkTask");
                    Label lblIsTaskClose = (Label)e.Row.FindControl("lblIsTaskClose");

                    if (lblStatus != null && btnSubTaskDocDownload != null && lblSlashReview != null && btnSubTaskDocView != null) /*&& lblSlashReview != null*/
                    {
                        if (lblStatus.Text != "")
                        {
                            if (lblStatus.Text == "Open")
                            {
                                btnSubTaskDocDownload.Visible = false;
                                lblSlashReview.Visible = false;
                                btnSubTaskDocView.Visible = false;
                            }
                            else
                            {
                                if (lblIsTaskClose.Text == "True")
                                {
                                    chkTask.Enabled = false;
                                    chkTask.Checked = true;
                                    btnSubTaskDocDownload.Visible = false;
                                    lblSlashReview.Visible = false;
                                    btnSubTaskDocView.Visible = false;
                                }
                                else
                                {
                                    btnSubTaskDocDownload.Visible = true;
                                    lblSlashReview.Visible = true;
                                    btnSubTaskDocView.Visible = true;
                                }
                            }
                        }
                    }
                }

                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    Label lblStatus = (Label) e.Row.FindControl("lblStatus");
                //    Label lblSlashReview = (Label) e.Row.FindControl("lblSlashReview");
                //    LinkButton btnSubTaskDocView = (LinkButton) e.Row.FindControl("btnSubTaskDocView");
                //    LinkButton btnSubTaskDocDownload = (LinkButton) e.Row.FindControl("btnSubTaskDocDownload");

                //    if (lblStatus != null && btnSubTaskDocDownload != null && lblSlashReview != null && btnSubTaskDocView != null)
                //    {
                //        if (lblStatus.Text != "")
                //        {
                //            if (lblStatus.Text == "Open")
                //            {
                //                btnSubTaskDocDownload.Visible = false;
                //                lblSlashReview.Visible = false;
                //                btnSubTaskDocView.Visible = false;
                //            }
                //            else
                //            {
                //                btnSubTaskDocDownload.Visible = true;
                //                lblSlashReview.Visible = true;
                //                btnSubTaskDocView.Visible = true;
                //            }
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        public static List<NameValueHierarchy> GetAllHierarchyTask(int TaskID = -1)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.Tasks
                             where row.ID == TaskID
                             select row);

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Title }).OrderBy(entry => entry.Name).ToList();
                foreach (var item in hierarchy)
                {
                    Tasklist.Add(item.ID);
                    LoadSubEntitiesTask(item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntitiesTask(NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            var query = (from row in entities.Tasks
                         where row.ParentID != null
                         && row.ParentID == nvp.ID
                         select row);

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = (int)entry.ID, Name = entry.Title }).OrderBy(entry => entry.Name).ToList();

            foreach (var item in subEntities)
            {
                nvp.Children.Add(item);
                Tasklist.Add(item.ID);
                LoadSubEntitiesTask(item, false, entities);
            }
        }

        protected void lblNo_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow gvrow in gridSubTask.Rows)
                {
                    CheckBox chk = (CheckBox)gvrow.FindControl("chkTask");
                    if (chk != null & chk.Checked)
                    {
                        int TaskID = Convert.ToInt32(gridSubTask.DataKeys[gvrow.RowIndex].Value.ToString());
                        string str = gridSubTask.DataKeys[gvrow.RowIndex].Value.ToString();
                        Label lblTaskScheduledOnID = (Label)gvrow.FindControl("lblTaskScheduledOnID");
                        int TaskScheduledOnID = Convert.ToInt32(lblTaskScheduledOnID.Text);
                        Label lblComplianceScheduleOnID = (Label)gvrow.FindControl("lblComplianceScheduleOnID");
                        int ComplianceScheduleOnID = Convert.ToInt32(lblComplianceScheduleOnID.Text);
                        Label lbllblTaskInstanceID = (Label)gvrow.FindControl("lblTaskInstanceID");
                        int TaskInstanceID = Convert.ToInt32(lbllblTaskInstanceID.Text);
                        Label lblMainTaskID = (Label)gvrow.FindControl("lblMainTaskID");
                        int MainTaskID = Convert.ToInt32(lblMainTaskID.Text);
                        Label lblForMonth = (Label)gvrow.FindControl("lblForMonth");
                        string ForMonth = Convert.ToString(lblForMonth.Text);


                        if (TaskID != -1)
                        {
                            long? parentID = TaskID;
                            List<NameValueHierarchy> hierarchy = new List<NameValueHierarchy>();
                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                            {
                                Tasklist.Clear();
                                GetAllHierarchyTask(TaskID);
                                int customerID = -1;
                                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                                var documentData = (from row in entities.TaskInstanceTransactionViews
                                                    where Tasklist.Contains(row.TaskID)
                                                    && row.ForMonth == ForMonth
                                                    && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                                    && row.RoleID == 4
                                                    && row.CustomerID == customerID
                                                    select row).ToList();

                                for (int i = 0; i < documentData.Count; i++)
                                {
                                    var IsTaskTransactionPresent = TaskManagment.CheckTaskTransacion(Convert.ToInt64(documentData[i].TaskInstanceID), Convert.ToInt64(documentData[i].TaskScheduledOnID), Convert.ToInt64(documentData[i].ComplianceScheduleOnID));
                                    if (IsTaskTransactionPresent == false)
                                    {
                                        int StatusID = 4;
                                        TaskTransaction transaction = new TaskTransaction()
                                        {
                                            ComplianceScheduleOnID = Convert.ToInt64(documentData[i].ComplianceScheduleOnID),
                                            TaskScheduleOnID = Convert.ToInt64(documentData[i].TaskScheduledOnID),
                                            TaskInstanceId = Convert.ToInt64(documentData[i].TaskInstanceID),
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedByText = AuthenticationHelper.User,
                                            StatusId = StatusID,
                                            StatusChangedOn = DateTime.ParseExact(DateTime.Now.Date.ToString("dd-MM-yyyy"), "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                            Remarks = "Not applicable",
                                            IsTaskClose = true,
                                        };
                                        bool sucess = TaskManagment.CreateTaskTransaction(transaction, null, null, null);
                                    }
                                }
                            }
                        }
                    }
                }
                var IsAfter = "";
                var Period = "";
                int CustomerBrachID = -1;
                int ScheduledOnID = -1;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["IsAfter"])))
                {
                    IsAfter = Convert.ToString(ViewState["IsAfter"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Period"])))
                {
                    Period = Convert.ToString(ViewState["Period"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomerBrachID"])))
                {
                    CustomerBrachID = Convert.ToInt32(ViewState["CustomerBrachID"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["complianceInstanceID"])))
                {
                    ScheduledOnID = Convert.ToInt32(ViewState["complianceInstanceID"]);
                }

                var showHideButton = false;
                if (IsAfter == "false")
                {
                    showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, CustomerBrachID);
                    btnSave.Enabled = showHideButton;
                }
                else
                {
                    showHideButton = BindSubTasks(ScheduledOnID, 3, lblPeriod.Text, CustomerBrachID);
                    btnSave.Enabled = true;
                }
                upComplianceDetails.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rptComplianceSampleView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    ComplianceForm CMPDocuments = Business.ComplianceManagement.GetSelectedComplianceFileName(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[1]));

                    if (CMPDocuments != null)
                    {
                        string fullfilePath = Path.Combine(Server.MapPath(CMPDocuments.FilePath));
                        string filePath = CMPDocuments.FilePath;
                        string CompDocPath = filePath.Substring(2, filePath.Length - 2);
                        if (CMPDocuments.FilePath != null && File.Exists(fullfilePath))
                        {
                            string extension = System.IO.Path.GetExtension(CompDocPath);

                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFile();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenSampleFile('" + CompDocPath + "');", true);
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFile();", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceSampleView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblSampleView = (LinkButton)e.Item.FindControl("lblSampleView");
                scriptManager.RegisterAsyncPostBackControl(lblSampleView);
            }
        }

        protected void UploadDocument_Click(object sender, EventArgs e)
        {
            try
            {
                grdDocument.DataSource = null;
                grdDocument.DataBind();
                long ScheduledOnID = Convert.ToInt64(ViewState["ScheduledOnID"]);
                long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

                TempComplianceDocument tempComplianceDocument = null;
                HttpFileCollection fileCollection = Request.Files;
                if (fileCollection.Count > 0)
                {
                    #region file upload
                    bool isBlankFile = false;                   
                    string[] validFileTypes = { "exe", "bat", "dll", "css", "js", "jsp", "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp", };
                    for (int i = 0; i < fileCollection.Count; i++)
                    {
                        HttpPostedFile uploadfile = null;
                        uploadfile = fileCollection[i];
                        int filelength = uploadfile.ContentLength;
                        string fileName = Path.GetFileName(uploadfile.FileName);
                        string ext = System.IO.Path.GetExtension(uploadfile.FileName);
                        if (!string.IsNullOrEmpty(fileName))
                        {
                            if (filelength == 0)
                            {
                                isBlankFile = true;
                                break;
                            }
                            else if (ext == "")
                            {
                                isBlankFile = true;
                                break;
                            }
                            else
                            {
                                if (ext != "")
                                {
                                    for (int j = 0; j < validFileTypes.Length; j++)
                                    {
                                        if (ext == "." + validFileTypes[j])
                                        {
                                            isBlankFile = true;
                                            break;
                                        }
                                    }
                                }

                            }
                        }
                    }
                    if (isBlankFile == false)
                    {
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            string directoryPath = null;

                            if (!string.IsNullOrEmpty(fileName))
                            {
                                string[] keys = fileCollection.Keys[i].Split('$');
                                if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                {
                                    directoryPath = Server.MapPath("~/TempDocuments/Statutory/");
                                }
                                else
                                {
                                    directoryPath = Server.MapPath("~/TempDocuments/Working/");
                                }

                                DocumentManagement.CreateDirectory(directoryPath);
                                string finalPath = Path.Combine(directoryPath, fileName);
                               // string finalPathWinSCP = Path.Combine(directoryPath, fileName);

                                finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                                fileCollection[i].SaveAs(Server.MapPath(finalPath));


                                //// Setup session options
                                //SessionOptions sessionOptions = new SessionOptions
                                //{
                                //    Protocol = WinSCP.Protocol.Sftp,
                                //    HostName = "139.59.17.37",
                                //    UserName = "root",
                                //    Password = "RHNCWFB8k4RUMMgYFFhweTAMNdhs3ykXKrDYRKxpxF2H4cQdm6vBtxFmvWgRXd",
                                //    SshHostKeyFingerprint = "ssh-rsa 3072 EW/hp5UB0371ROFsteKpaVs7+boMVx+fZn+cm5EkKLg="
                                //};

                                //using (Session session = new Session())
                                //{
                                //    // Connect
                                //    session.Open(sessionOptions);

                                //    // Upload files
                                //    TransferOptions transferOptions = new TransferOptions();
                                //    transferOptions.TransferMode = TransferMode.Binary;

                                //    TransferOperationResult transferResult;
                                //    if (!string.IsNullOrEmpty(finalPathWinSCP))
                                //    {
                                //        transferResult =
                                //                  session.PutFiles(finalPathWinSCP, "/Complaince/Statutory/Performer/", false, transferOptions);

                                //        // Throw on any error
                                //        transferResult.Check();

                                //        // Print results
                                //        foreach (TransferEventArgs transfer in transferResult.Transfers)
                                //        {
                                //            Console.WriteLine("Upload of {0} succeeded", transfer.FileName);
                                //        }
                                //    }
                                //}

                                Stream fs = uploadfile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                tempComplianceDocument = new TempComplianceDocument()
                                {
                                    ScheduleOnID = ScheduledOnID,
                                    ComplianceInstanceID = complianceInstanceID,
                                    DocPath = finalPath,
                                    DocData = bytes,
                                    DocName = fileCollection[i].FileName,
                                    FileSize = uploadfile.ContentLength,
                                };

                                if (keys[keys.Count() - 1].Equals("fuSampleFile"))
                                {
                                    tempComplianceDocument.DocType = "S";
                                }
                                else
                                {
                                    tempComplianceDocument.DocType = "W";
                                }

                                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                                if (_objTempDocumentID > 0)
                                {
                                    BindTempDocumentData(ScheduledOnID);
                                   
                                }
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Invalid file error. System does not support uploaded file.Please upload another file.";
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "topFunction", "topFunction();", true);
                    }
                    #endregion
                }

                BindTempDocumentData(ScheduledOnID);

                //if (lblDueDate.Text != "")
                //{
                //    if (ddlStatus.SelectedValue == "3")
                //    {
                //        DateTime Date = DateTime.ParseExact(hiddenDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //        Date = Date.AddDays(1);
                //        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerOverDue", string.Format("initializeDatePickerOverDue(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                //    }
                //    if (ddlStatus.SelectedValue == "2")
                //    {
                //        DateTime Date = DateTime.ParseExact(hiddenDueDate.Value, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePickerInTime", string.Format("initializeDatePickerInTime(new Date({0}, {1}, {2}));", Date.Year, Date.Month - 1, Date.Day), true);
                //    }
                //}
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "DisplayProgress();", true);

                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindTempDocumentData(long ScheduledOnID)
        {
            try
            {
                grdDocument.DataSource = null;
                grdDocument.DataBind();
                var DocData = DocumentManagement.GetTempComplianceDocumentData(ScheduledOnID);
                if (DocData.Count > 0)
                {
                    grdDocument.Visible = true;
                    grdDocument.DataSource = DocData;
                    grdDocument.DataBind();
                }
                else
                {
                    grdDocument.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Delete Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
                        //SessionOptions sessionOptions = new SessionOptions
                        //{
                        //    Protocol =  WinSCP.Protocol.Sftp,
                        //    HostName = "139.59.17.37",
                        //    UserName = "root",
                        //    Password = "RHNCWFB8k4RUMMgYFFhweTAMNdhs3ykXKrDYRKxpxF2H4cQdm6vBtxFmvWgRXd",
                        //    SshHostKeyFingerprint = "ssh-rsa 3072 EW/hp5UB0371ROFsteKpaVs7+boMVx+fZn+cm5EkKLg="
                        //};
                        if (!string.IsNullOrEmpty(file.DocName))
                        {
                            using (Session session = new Session())
                            {
                                // Connect
                                //session.Open(sessionOptions);

                                //// Upload files
                                //TransferOptions transferOptions = new TransferOptions();
                                //transferOptions.TransferMode = TransferMode.Binary;

                                //RemovalEventArgs DeleteResult;
                                if (!string.IsNullOrEmpty(file.DocName))
                                {
                                    //DeleteResult =
                                    //    session.RemoveFile(@"/Complaince/Statutory/Performer/" + file.DocName);

                                    //if (DeleteResult.Error == null)
                                    //{
                                    //    if (Business.ComplianceManagement.DeleteTempDocumentFile(FileID))
                                    //    {
                                    //        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);

                                    //        BindTempDocumentData(Convert.ToInt64(ViewState["ScheduledOnID"]));
                                    //    }
                                    //}
                                    if (Business.ComplianceManagement.DeleteTempDocumentFile(FileID))
                                    {
                                        if (Business.ComplianceManagement.DeleteTempDocumentFile(FileID))
                                        {
                                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);

                                            BindTempDocumentData(Convert.ToInt64(ViewState["ScheduledOnID"]));
                                        }
                                    }

                                }
                            }
                        }
                       
                    }
                }
                else if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
                            if (file != null)
                            {
                                // Setup session options
                                //SessionOptions sessionOptions = new SessionOptions
                                //{
                                //    Protocol = WinSCP.Protocol.Sftp,
                                //    HostName = "139.59.17.37",
                                //    UserName = "root",
                                //    Password = "RHNCWFB8k4RUMMgYFFhweTAMNdhs3ykXKrDYRKxpxF2H4cQdm6vBtxFmvWgRXd",
                                //    SshHostKeyFingerprint = "ssh-rsa 3072 EW/hp5UB0371ROFsteKpaVs7+boMVx+fZn+cm5EkKLg="
                                //};

                                //using (Session session = new Session())
                                //{
                                //    // Connect
                                //    session.Open(sessionOptions);

                                //    // Upload files
                                //    TransferOptions transferOptions = new TransferOptions();
                                //    transferOptions.TransferMode = TransferMode.Binary;

                                //    TransferOperationResult transferResult;
                                //    if (!string.IsNullOrEmpty(file.DocName))
                                //    {

                                //        transferResult =
                                //                    session.GetFiles(@"/Complaince/Statutory/Performer/" + file.DocName, Server.MapPath(file.DocPath), false, transferOptions);

                                //        // Throw on any error
                                //        transferResult.Check();

                                //    }
                                //}


                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename= " + file.DocName);
                                Response.TransmitFile(Server.MapPath(file.DocPath));
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                        }
                        BindTempDocumentData(Convert.ToInt64(ViewState["ScheduledOnID"]));
                    }
                }
                else if (e.CommandName.Equals("View Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
                        if (file != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.DocPath));
                            if (file.DocName != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Zip file can't view please download it.')", true);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Zip file can't view please download it.";
                                }
                                else
                                {
                                    string CompDocReviewPath = file.DocPath;

                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenDocumentPriview('" + CompDocReviewPath + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('There is no file to preview.')", true);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "There is no file to preview.";
                            }
                        }
                    }
                }

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fCalltreeCollapsed();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again."; ;
            }
        }
        protected void grdDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblDocumentName = (Label)e.Row.FindControl("lblDocumentName");
                    Label lblIsLinkTrue = (Label)e.Row.FindControl("lblIsLinkTrue");
                    Label lblDocType = (Label)e.Row.FindControl("lblDocType");

                    if (lblDocType.Text.Trim() == "S")
                    {
                        lblDocType.Text = "Compliance Document";
                    }
                    else
                    {
                        lblDocType.Text = "Working Files";
                    }

                    LinkButton lnkRedirectDocument = (LinkButton)e.Row.FindControl("lnkRedirectDocument");
                    LinkButton lnkDownloadDocument = (LinkButton)e.Row.FindControl("lnkDownloadDocument");
                    LinkButton lnkViewDocument = (LinkButton)e.Row.FindControl("lnkViewDocument");
                    //LinkButton lnkDeleteDocument = (LinkButton)e.Row.FindControl("lnkDeleteDocument");

                    if (lblIsLinkTrue.Text == "True")
                    {
                        lnkDownloadDocument.Visible = false;
                        lnkViewDocument.Visible = false;
                        lnkRedirectDocument.Visible = true;
                        lblDocumentName.Visible = false;
                        //lnkDeleteDocument.Visible = false;
                    }
                    else
                    {
                        lnkDownloadDocument.Visible = true;
                        lnkViewDocument.Visible = true;
                        lnkRedirectDocument.Visible = false;
                        lblDocumentName.Visible = true;
                    }
                }
                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    Label lblDocType = (Label)e.Row.FindControl("lblDocType");

                //    if (lblDocType.Text.Trim() == "S")
                //    {
                //        lblDocType.Text = "Compliance Document";
                //    }
                //    else
                //    {
                //        lblDocType.Text = "Working Files";
                //    }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void UploadlinkWorkingfile_Click(object sender, EventArgs e)
        {
            grdDocument.DataSource = null;
            grdDocument.DataBind();
            long ScheduledOnID = Convert.ToInt64(ViewState["ScheduledOnID"]);
            long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

            string url = TxtworkingdocumentlnkStatutory.Text;
            string fileName = DocumentManagement.getFileName(url);
            if (!string.IsNullOrEmpty(TxtworkingdocumentlnkStatutory.Text))
            {
                TempComplianceDocument tempComplianceDocument = null;
                var bytes = new byte[] { };// { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };               
                tempComplianceDocument = new TempComplianceDocument()
                {
                    ScheduleOnID = ScheduledOnID,
                    ComplianceInstanceID = complianceInstanceID,
                    DocPath = url,//userpath,
                    DocData = bytes,
                    DocName = fileName,
                    DocType = "S",
                    ISLink = true
                };

                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                if (_objTempDocumentID > 0)
                {
                    BindTempDocumentData(ScheduledOnID);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
            TxtworkingdocumentlnkStatutory.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
        }

        protected void UploadlinkCompliancefile_Click(object sender, EventArgs e)
        {
            grdDocument.DataSource = null;
            grdDocument.DataBind();
            long ScheduledOnID = Convert.ToInt64(ViewState["ScheduledOnID"]);
            long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

            string url = TxtCompliancedocumentlnkStatutory.Text;
            string fileName = DocumentManagement.getFileName(url);
            if (!string.IsNullOrEmpty(TxtCompliancedocumentlnkStatutory.Text))
            {
                TempComplianceDocument tempComplianceDocument = null;
                var bytes = new byte[] { };// { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };   
                tempComplianceDocument = new TempComplianceDocument()
                {
                    ScheduleOnID = ScheduledOnID,
                    ComplianceInstanceID = complianceInstanceID,
                    DocPath = url,//userpath,
                    DocData = bytes,
                    DocName = fileName,
                    DocType = "W",
                    ISLink = true
                };

                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                if (_objTempDocumentID > 0)
                {
                    BindTempDocumentData(ScheduledOnID);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
            TxtCompliancedocumentlnkStatutory.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "CloseAndBindData();", true);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseCalenderPERPop();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void lnkgotoportal_Click(object sender, EventArgs e)
        {
            try
            {
                ProductMappingStructure _obj = new ProductMappingStructure();              
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    _obj.ReAuthenticate_UserNew();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
      
        private void BindGSTCompliance(long ComplianceID, long ComplianceInstanceID, int roleID, string period,string gstgroupname)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                int UserID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
                //var GstGroupName = ComplianceManagement.Business.ComplianceManagement.GetGSTCOmplianceGroupName(customerID, ComplianceID);
                var InstanceList = ComplianceManagement.Business.ComplianceManagement.GetGSTCOmplianceInstanceList(customerID, ComplianceID, ComplianceInstanceID, gstgroupname);
               
                string InputFilter = Request.QueryString["filter"];
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var documentData = (from row in entities.SP_GSTComplianceStatutory(customerID)
                                        where row.ForMonth == period
                                        && row.RoleID == roleID
                                        && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 6)
                                        && InstanceList.Contains(row.ComplianceInstanceID)
                                        && row.UserID == UserID
                                        select row).ToList();

                    if (documentData.Count != 0)
                    {
                        divGSTComplianceList.Visible = true;
                        grdGstMappedCompliance.DataSource = documentData;
                        grdGstMappedCompliance.DataBind();
                    }
                    else
                    {
                        divGSTComplianceList.Visible = false;
                    }
                    Session["filtertype"] = "";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void Remarkddl_SelectedIndexChanged(object sender, EventArgs e)
        {
               if(Remarkddl.SelectedValue == "14" || Remarkddl.SelectedValue == "21")
                {
                      tbxRemarks.Visible = true;
                      Remarkddl.Visible = false;
                }
                else
                {
                     tbxRemarks.Visible = false;
                     Remarkddl.Visible = true;
                }
        }
    }

}