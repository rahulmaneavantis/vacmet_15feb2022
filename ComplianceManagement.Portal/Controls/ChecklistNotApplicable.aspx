﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewCompliance.Master" AutoEventWireup="true" CodeBehind="ChecklistNotApplicable.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.ChecklistNotApplicable" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jszip.min.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.min.js"></script>
     <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment.min.js"></script>

      <style type="text/css">

        input[type=checkbox], input[type=radio] {
    margin: 4px 4px 0;
    line-height: normal;
}
        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: -2px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 380px !important;
        }

        #grid1 .k-grid-content {
            min-height: 320px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 6px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }
        #grid .k-grouping-header {
            font-style: italic;
            margin-top: 1px;
            background-color: white;
        }
        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }
        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }
      .k-animation-container {
    width: 1165px;
    height: 206px;
    box-sizing: content-box;
    overflow: visible;
    top: 167px;
    z-index: 10002;
    left: 243px;
    display: block;
    position: absolute;
}
    </style>

    <title></title>

     <script type="text/x-kendo-template" id="template"> 
      
    </script>
     <script type="text/javascript">
        $(document).ready(function () {
            fhead('My Workspace/Checklist Remark');
            setactivemenu('My Workspace');
            fmaters1();
     });

    </script>
      <script type="text/javascript">

        $(document).ready(function () {
            debugger;
          BindGrid();
         
        
      
        });
        function ClosePopRemarkDetialPage()
        {
             $("#grid").data("kendoGrid").dataSource.read();
             $("#grid").data("kendoGrid").refresh();
        }
        var record = 0;
        var total = 0;
    


       


        function BindGrid() {

            debugger;

            var gridexist = $('#grid').data("kendoGrid");
            if (gridexist != undefined || gridexist != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport:
                    {
                        read: {
                          
                            url: '<% =Path%>/Litigation/ChecklistRemark?CustId=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                       
                    },
                    schema: {
                        data: function (response) {
                            return response;
                        },
                        total: function (response) {
                            return response.length;
                        }
                    },
                    pageSize: 10
                },
                excel: {
                    allPages: true,
                },
                sortable: true,
                groupable: true,
                filterable: true,
                columnMenu: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                pageable: {
                    numeric: true,
                    pageSizes: ['All', 5, 10, 20],
                    pageSize: 10,
                    buttonCount: 3,
                },
                dataBound: function (e) {
                    record = 0;
                    total = this.dataSource._total;

                    if (this.dataSource.pageSize() == undefined || this.dataSource.pageSize() == null) {
                        this.dataSource.pageSize(this.dataSource._total);
                    }
                    if (this.dataSource.pageSize() > 10 && this.dataSource.pageSize() > 20 && this.dataSource.pageSize() != this.dataSource._total) {
                        if (this.dataSource._total <= 10) {
                            this.dataSource.pageSize(10)

                        }
                        else if (this.dataSource._total >= 10 && this.dataSource._total <= 20) {
                            this.dataSource.pageSize(20)
                        }
                        else {
                            this.dataSource.pageSize(total);
                        }
                    }
                    if (this.dataSource.pageSize() < 10 && this.dataSource.pageSize() != total) {
                        this.dataSource.pageSize(10)
                    }
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                   
                },
                columns: [
                     {
                         title: "Sr. No.",
                         template: "#= ++record #",
                         width: 70
                     },
                      {
                          field: "remark", title: 'Remark',
                        
                          width: "30%;",
                          attributes: {
                              style: 'white-space: nowrap;'
                          }, filterable: {
                              multi: true,
                              extra: false,
                              search: true,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                      },
                      {
                          field: "CreatedUser", title: 'CreatedBy',
                        width: "30%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }, autoWidth: true,
                        }

                    },

                    //{
                    //    field: "Createon", title: 'CreateOn',
                    //    width: "15%;",
                    //    attributes: {
                    //        style: 'white-space: nowrap;'
                    //    }, filterable: {
                    //        multi: true,
                    //        extra: false,
                    //        search: true,
                    //        operators: {
                    //            string: {
                    //                eq: "Is equal to",
                    //                neq: "Is not equal to",
                    //                contains: "Contains"
                    //            }
                    //        }
                    //    }
                    //},
                     
                      

                   {
                       command: [
                           { name: "edit", text: "", iconClass: "k-icon k-i-edit", className: "ob-edit" },
                             { name: "delete", text: "", iconClass: "k-icon k-i-delete k-i-trash", className: "ob-delete" }
                          
                       ], title: "Action", lock: true, width: 100, headerAttributes: {
                           style: "text-align: center;"
                       }
                   }
                   


                ]
            });

            $(document).on("click", "#grid tbody tr .ob-edit", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                debugger;
                //if (item.TypeCaseNotice != '') {
                    $('#divShowDialog').modal('show');
                   

                    if (item != '') {
                        //$('#ContentPlaceHolder1_showReminderDetail').attr('src', "../Event/ViewPageActivated.aspx?ID=" + item.ID + '&Type=' + item.Type + '&EventScheduledOnID=' + item.EventScheduledOnId + '&CustomerBranchID=' + item.CustomerBranchID + '&EventClassificationID=' + item.EventClassificationID + '&EventDescription=' + item.EventDescription);

                        $('#showdetails').attr('src', "../Controls/AddChecklistRemark.aspx?ID="+item.ID);
                    }
                    
                //}
                return true;
            });
              
            function OnGridDataBoundAdvanced(e) {
                for (var i = 0; i < this.columns.length; i++) {
                    this.autoWidth;
                }
                var grid = $("#grid").data("kendoGrid");
                var gridData = grid.dataSource.view();
                for (var i = 0; i < gridData.length; i++) {

                    var currentUid = gridData[i].uid;
                    var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    if (<% =flagDelVal%> == 0) {
                        var deleteButton = $(currentRow).find(".ob-delete");
                        //deleteButton.hide();
                    }
                }
            }

            $("#grid").kendoTooltip({
                filter: ".k-grid-delete",
                width: "150px",
                content: function (e) {
                    return "Delete " + "Checklist Remark ";
                }
            });

            $(document).on("click", "#grid tbody tr .ob-delete", function (e) {
                var retVal = confirm("Do you want to Delete Record..?");
                if (retVal == true) {
                    var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                    var $tr = $(this).closest("tr");
                    $.ajax({
                        type: 'POST',
                        url: '<% =Path%>Data/Delete_ChecklistRemark?ID=' + item.ID  ,
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                        },
                        success: function (result) {
                            // notify the data source that the request succeeded
                            grid = $("#grid").data("kendoGrid");
                            grid.removeRow($tr);
                        },
                        error: function (result) {
                            // notify the data source that the request failed
                            console.log(result);
                        }
                    });
                }
                return true;
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                width: "150px",
                content: function (e) {
                    return "Edit ";
                }
            });
            $("#grid").kendoTooltip({
                filter: "th",
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown 
                    return $(target).text();
                }
            });

            $("#grid").kendoTooltip({
                filter: "td", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

        }

        

     
        function ApplyBtnAdvancedFilter() {
            BindGrid();
        }

       
        function lnkAdd_Click(e) {
            $('#divShowDialog').modal('show');
            $('#showdetails').attr('src', "../../Controls/AddChecklistRemark.aspx");
            e.preventDefault();
            return false;
        }
  

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div id="example">
         <div class="row">
             <div class="col-md-12 colpadding0">

             <div class="col-md-3" style="width: 13%; padding-left: 0px; float: right;">
                  
       
                 <button id="lnkAdd" style="width: 64.2%; height: 30px; margin-right: 0.7%; float: right;" runat="server" onclick="lnkAdd_Click(event)">
                 <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New</button>
   
                     
                 </div>
                 <div class="col-md-3" style="width: 13%; padding-left: 0px; float: right;">
                  
       
                <%-- <button id="btnback" style="width: 64.2%; height: 30px; margin-right: 0.7%; float: right;" runat="server"  onclick="btnback_onclick">
                 <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;Back</button>--%>

                     <div style="float: right; margin-right: 18px;">
                         <asp:LinkButton runat="server" ID="lnkbutton" CssClass="btn btn-search" OnClick="linkbutton_onclick">Back</asp:LinkButton>

                     </div>
   
                     
                 </div>
                </div>
           </div>
              
              
                <div class="clearfix" style="height: 10px;"></div>

            <div id="grid"></div>

          <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header" style="height: 60px;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 210px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                            Checklist Remark</label>
                        <button id="btnAddEditcase" type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="ClosePopRemarkDetialPage();">&times;</button>
                    </div>

                    <div class="modal-body" >
                        <iframe id="showdetails" src="about:blank" width="100%" height="240px" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>

         
           

         
         </div>
</asp:Content>
