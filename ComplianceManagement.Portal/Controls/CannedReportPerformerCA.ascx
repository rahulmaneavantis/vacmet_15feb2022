﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CannedReportPerformerCA.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.CannedReportPerformerCA" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div style="float: right; margin-right: 10px; margin-top: -35px;">
            <asp:DataList runat="server" ID="dlFilters" RepeatDirection="Horizontal" OnSelectedIndexChanged="dlFilters_SelectedIndexChanged"
                DataKeyField="ID">
                <SeparatorTemplate>
                    <span style="margin: 0 5px 0 5px">|</span>
                </SeparatorTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="select" Style="text-decoration: none; color: Black">  
                                    <%# DataBinder.Eval(Container.DataItem, "Name") %>
                    </asp:LinkButton>
                </ItemTemplate>
                <ItemStyle Font-Names="Tahoma" Font-Size="13px" VerticalAlign="Middle" />
                <SelectedItemStyle Font-Names="Tahoma" Font-Size="13px" Font-Bold="True" VerticalAlign="Middle"
                    Font-Underline="true" />
            </asp:DataList>
        </div>
    
                
                 <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" AllowSorting="true"
                GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" OnRowCreated="grdComplianceTransactions_RowCreated" OnRowCommand="grdComplianceTransactions_RowCommand"
                BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="100" OnSorting="grdComplianceTransactions_Sorting" OnRowDataBound="grdComplianceTransactions_RowDataBound"
                Width="100%" Font-Size="12px" DataKeyNames="ComplianceInstanceID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="ComplianceID" HeaderText="ID" ItemStyle-Width="5%" SortExpression="ComplianceID" />
                    <asp:TemplateField HeaderText="Location" ItemStyle-Width="20%" SortExpression="Branch">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" ItemStyle-Width="30%" SortExpression="Description">
                        <ItemTemplate>
                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px;">
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>                   
                    <asp:TemplateField HeaderText="Reviewer" ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblReviewer" runat="server" Text='<%# GetReviewer((long)Eval("ComplianceInstanceID")) %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Approver" ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblApprover" runat="server" Text='<%# GetApprover((long)Eval("ComplianceInstanceID")) %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <%-- <asp:BoundField DataField="Role" HeaderText="Role" ItemStyle-Width="10%" SortExpression="Role" />--%>
                    <%--   <asp:BoundField DataField="User" HeaderText="User" ItemStyle-Width="10%" />--%>
                    <asp:TemplateField HeaderText="Scheduled On" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn">
                        <ItemTemplate>
                            <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>
                            <asp:Label ID="lblScheduledOnID" runat="server" Text='<%# Eval("ScheduledOnID") %>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ForMonth" HeaderText="For Month" ItemStyle-HorizontalAlign="Center" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="ForMonth"
                        ItemStyle-Width="15%" />
                    <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Height="20px" HeaderStyle-Height="20px" SortExpression="Status"
                        ItemStyle-Width="15%" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkbtnHistory" runat="server" CommandName="View_History" Text="View_History" CommandArgument='<%# Eval("ScheduledOnID") %>' Visible="false"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>
<div id="divActDialog111">
    <asp:UpdatePanel ID="UpDetailView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
           
                <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false"
                    GridLines="Both" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid"
                    BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="50"
                    Width="100%" Font-Size="12px" DataKeyNames="ComplianceTransactionID">
                    <Columns>
                        <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" SortExpression="CreatedByText" />
                        <asp:TemplateField HeaderText="Date" SortExpression="StatusChangedOn">
                            <ItemTemplate>
                                <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField DataField="Remarks" HeaderText="Remarks" SortExpression="Remarks" />
                        <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>

           
        </ContentTemplate>
    </asp:UpdatePanel>
</div>