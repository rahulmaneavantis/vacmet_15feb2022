﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="calendardataAPI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.calendardataAPI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" /> 
        
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

   <title></title>
    <style type="text/css">
         .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-pager-wrap .k-textbox, .k-pager-wrap .k-widget {
            margin: -1px 0.4em 0px 20px;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 1px;
            margin-top: -7px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            min-height: 210px !important;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        #grid .k-grid-toolbar {
            background: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: -2px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: 6px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px white;
            box-shadow: inset 0 0 3px 1px white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 250px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
        }

        .k-grid-toolbar {
            background: white;
            border: none;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-multicheck-wrap 
        {
          overflow: auto;
          overflow-x: hidden;
          white-space: nowrap;
          max-height: 176px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
    position: relative;
    width: 100.1%;
    overflow: hidden;
    border-style: solid;
    border-width: 0 0px 0 0;
    zoom: 1;
}
    </style>
    <script id="templateTooltip" type="text/x-kendo-template">
                <div>
                <div> #:value ? value : "N/A" #</div>
                </div>
    </script>
      
    <script type="text/x-kendo-template" id="template">      
               
    </script>
    
    <script id="StatusTemplate" type="text/x-kendo-template">                 
            <span class='k-progress'></span>
            <div class='file-wrapper'> 
            #=GetStatusType(ComplianceStatusID,ScheduledOn)# 
    </script>

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <script type="text/javascript">

        function GetStatusType(value, ScheduledOn) {

            if (value == 5) {
                return '<img src="/Images/delayed.png"  />';
            }
            else if (value == 4 || value == 7 || value == 15) {
                return '<img src="/Images/completed.png"  />';
            }
            else if (value != 5 || value != 4) {

                var todaydate = new Date();
                var Scheduleddate = new Date(ScheduledOn);
                if (Scheduleddate < todaydate) {
                    return '<img src="/Images/overdue.png"  />';
                }
                else if (Scheduleddate >= todaydate) {
                    return '<img src="/Images/upcoming.png"  />';
                }
            }
            else {
                return "";
            }
        }

        function BindGrid() {
            var grid = $('#grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#grid').empty();

            var grid = $("#grid").kendoGrid({
                dataSource: {
                    transport:
                    {
                        read: {
                            url: '<% =Path%>Data/GetCalenderComplianceDetails?Userid=<% =UId%>&Customerid=<% =CustId%>&IsApprover=<% =IsApprover%>&datevalue=<% =date%>&isallorsi=<% =isallorsi%>&isdh=<%=dhead%>&ismflag=' + $("#dropdownlistUserRole").val(),
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '<% =Authorization%>');
                            },
                        }
                    },
                    schema: {
                        data: function (response) {
                            window.parent.hideloader();
                            if ($("#dropdownlistUserRole").val() != undefined) {
                                if ($("#dropdownlistUserRole").val() == 3) {
                                    return response[0].PList;
                                }
                                else if ($("#dropdownlistUserRole").val() == 4) {
                                    return response[0].RList;
                                }
                            }
                            else {
                                return response[0].PList;
                            }
                        },
                        total: function (response) {
                            if ($("#dropdownlistUserRole").val() != undefined) {
                                if ($("#dropdownlistUserRole").val() == 3) {
                                    return response[0].PList.length;
                                }
                                else if ($("#dropdownlistUserRole").val() == 4) {
                                    return response[0].RList.length;
                                }
                            }
                            else {
                                return response[0].PList.length;
                            }
                        }
                    },
                },
                toolbar: kendo.template($("#template").html()),
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    pageSize: 5,
                    input: true
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    {
                        field: "CType", title: 'Type',
                        width: "7%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ComplianceStatusID", title: "Status",
                        width: "7%",
                        template: kendo.template($('#StatusTemplate').html()),
                        filterable: false,
                        sortable: false,
                        attributes: {
                            "class": "table-cell",
                            style: "text-align: Center;"
                        },
                        operators: {
                            string: {
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        }
                    },
                    {
                        field: "ShortDescription", title: 'Name',
                        width: "13%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "SLType", title: 'SLType',
                        width: "7%",
                        hidden: true,
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: [
                            {
                                name: "edit2", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-overview",
                                visible: function (dataItem) {
                                    if (dataItem.flag === true) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                    <%--if (<% =UploadDocumentLink%> == 0)
                                    {
                                        if (dataItem.flag === true) {
                                            return true;
                                        }
                                        else {
                                            return false;
                                        }
                                    }
                                    else {
                                        return false;
                                    }--%>
                                }
                            },
                            {
                                name: "edit3", text: "", iconClass: "k-icon k-i-eye", className: "ob-overviewMGMT",
                                visible: function () {
                                    if (<% =UploadDocumentLink%> == 1) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            }
                        ], title: "Action", lock: true, width: "5%", headerAttributes: {
                            style: "text-align: center;"
                        }
                    }
                ]
            });

            $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                parent.OpenPerrevpopup(item.ScheduledOnID, item.ComplianceInstanceID, item.Interimdays,
                    item.CType, item.RoleID, item.ScheduledOn, item.SLType);
                return true;
            });

            $(document).on("click", "#grid tbody tr .ob-overviewMGMT", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                parent.OpenOverViewpup(item.ScheduledOnID, item.ComplianceInstanceID, item.CType);
                return true;
            });

            $("#grid").kendoTooltip({
                filter: "th", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    if (content != "") {
                        return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                    }
                    else
                        e.preventDefault();
                }
            }).data("kendoTooltip");

            
            $("#grid").kendoTooltip({
                filter: "td:nth-child(1)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: "td:nth-child(3)", //this filter selects the second column's cells
                position: "down",
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");

            $("#grid").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "Edit";
                }
            });
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit3",
                content: function (e) {
                    return "Overview";
                }
            });
        }

        $(document).ready(function () {

            $("#dropdownlistUserRole").kendoDropDownList({
                placeholder: "Role",
                checkboxes: true,
                checkAll: true,
                autoClose: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: function (e) {
                    BindGrid();
                },
                dataSource: [
                 <%if (PerformerFlagID == 1)%>
                    <%{%>
                    { text: "Performer", value: "3" },
                     <%}%>
                    <%if (ReviewerFlagID == 1)%>
                    <%{%>
                    { text: "Reviewer", value: "4" }
                    <%}%>
                ]
            });

            BindGrid();
            var grid = $("#grid").data("kendoGrid");
            grid.dataSource.sort({ field: "ComplianceStatusID", dir: "asc" });
        });

    </script>
    
  </head>
    <body>
  <form>     
      <div id="example">
          <div id="roledisplay" runat="server" >
            <input id="dropdownlistUserRole" data-placeholder="Role"  style="width:242px;"/>    
          </div>
          <div id="grid"></div>
      </div>       
  </form>
        </body>
 </html>