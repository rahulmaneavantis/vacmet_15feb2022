﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class frmRejectedCompliancess : System.Web.UI.Page
    {
        public string Role;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //ViewState["SortOrder"] = "Asc";
                //ViewState["SortExpression"] = "ShortDescription";
                if (Page.RouteData.Values["filter"] != null)
                {

                    if (Page.RouteData.Values["filter"].ToString().Equals("Rejected"))
                    {
                        if (Page.RouteData.Values["Type"].ToString().Equals("Statutory"))
                        {
                            ViewState["Role"] = Page.RouteData.Values["Role"].ToString();
                            hdnTitle.Value = Page.RouteData.Values["filter"].ToString() + " Compliances";
                            lbltagLine.Text = Page.RouteData.Values["filter"].ToString() + " Compliances for " + Page.RouteData.Values["Role"].ToString();
                        }
                    }
                }
                //ViewState["Role"] = Role;
                BindComplianceTransactions();

            }
            udcStatusTranscatopn.OnSaved += (inputForm, args) => { BindComplianceTransactions(); };
        }
        protected void UpdatePanel1_Load(object sender, EventArgs e)
        {

        }
        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                BindComplianceTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                List<ComplianceInstanceTransactionView> assignmentList = null;
                if (ViewState["Role"].Equals("Performer"))
                {
                    assignmentList = DashboardManagement.DashboardDataForPerformerUpcoming(AuthenticationHelper.UserID, "C", CannedReportFilterForPerformer.Rejected);
                }
                else if (ViewState["Role"].Equals("Reviewer"))
                {
                    assignmentList = DashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, "C", CannedReportFilterForPerformer.Upcoming);
                }
                //else if (ViewState["Role"].Equals("Approver"))
                //{
                //    assignmentList = DashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming);
                //}

                if (direction == SortDirection.Ascending)
                {
                    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    //ViewState["SortOrder"] = "Asc";
                    //ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    //ViewState["SortOrder"] = "Desc";
                    //ViewState["SortExpression"] = e.SortExpression.ToString();
                }

                foreach (DataControlField field in grdComplianceTransactions.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdComplianceTransactions.Columns.IndexOf(field);
                    }
                }

                grdComplianceTransactions.DataSource = assignmentList;
                grdComplianceTransactions.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ComplianceInstanceTransactionView rowView = (ComplianceInstanceTransactionView)e.Row.DataItem;
                    if (rowView.ComplianceInstanceID != -1)
                    {

                        Label lblImpact = (Label)e.Row.FindControl("lblImpact");
                        Label lblRisk = (Label)e.Row.FindControl("lblRisk");
                        Label lblComplianceId = (Label)e.Row.FindControl("lblComplianceId");
                        Image imtemplat = (Image)e.Row.FindControl("imtemplat");
                        LinkButton lnkDownload = (LinkButton)e.Row.FindControl("lnkDownload");
                        Label lblReferenceMaterialText = (Label)e.Row.FindControl("lblReferenceMaterialText");
                        Label lblFilePath = (Label)e.Row.FindControl("lblFilePath");
                        Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledOn");

                        if (!string.IsNullOrEmpty(lblImpact.Text))
                        {
                            if (Convert.ToInt32(lblImpact.Text) == 0)
                            {
                                lblImpact.Text = "Monetary";
                            }
                            else if (Convert.ToInt32(lblImpact.Text) == 1)
                            {
                                lblImpact.Text = "Non-Monetary";
                            }
                            else if (Convert.ToInt32(lblImpact.Text) == 2)
                            {
                                lblImpact.Text = "Monetary & Non-Monetary";
                            }
                        }
                        else
                        {
                            lblImpact.Text = "";
                        }

                        long Complianceid = Convert.ToInt64(lblComplianceId.Text);
                        lblImpact.ToolTip = ComplianceManagement.Business.ComplianceManagement.GetPanalty(ComplianceManagement.Business.ComplianceManagement.GetByID(Complianceid));

                        if (Convert.ToInt32(lblRisk.Text) == 0)
                        {
                            imtemplat.ImageUrl = "~/Images/red.png";
                        }
                        else if (Convert.ToInt32(lblRisk.Text) == 1)
                        {
                            imtemplat.ImageUrl = "~/Images/yellow.png";
                        }
                        else if (Convert.ToInt32(lblRisk.Text) == 2)
                        {
                            imtemplat.ImageUrl = "~/Images/green.png";
                        }

                        if ((lblFilePath.Text != "") && (lblFilePath.Text != null))
                        {
                            lnkDownload.Visible = true;
                        }
                        else
                        {
                            lnkDownload.Visible = false;
                        }
                        string CheckDate = "1/1/0001 12:00:00 AM";
                        if (lblScheduledOn != null)
                        {
                            if (Convert.ToDateTime(lblScheduledOn.Text) == Convert.ToDateTime(CheckDate))
                            {
                                //lblEventID.Visible = false;
                                lnkDownload.Visible = false;
                                lblReferenceMaterialText.Visible = false;
                            }
                        }
                        else
                        {
                            lnkDownload.Visible = false;
                            lblReferenceMaterialText.Visible = false;
                        }

                        //LinkButton btnChangeStatus = (LinkButton)e.Row.FindControl("btnChangeStatus");
                        //if (btnChangeStatus.Visible == true)
                        //{
                        //    //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(grdComplianceTransactions, "CHANGE_STATUS$" + btnChangeStatus.CommandArgument);
                        //    e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(btnChangeStatus, "");
                        //    e.Row.ToolTip = "Click on row to change Compliance Status";
                        //}
                    }
                    else
                    {
                        e.Row.Cells[0].Style.Add("font-weight", "700");
                        e.Row.Cells[1].Style.Add("border-style", "none");
                        e.Row.Cells[2].Style.Add("border-style", "none");
                        e.Row.Cells[3].Style.Add("border-style", "none");
                        e.Row.Cells[3].Text = string.Empty;
                        e.Row.Cells[4].Text = string.Empty;//added by Sachin
                        e.Row.Cells[6].Text = string.Empty;
                        e.Row.Cells[7].Text = string.Empty;//added by Manisha
                        e.Row.Cells[8].Text = string.Empty;//added by Manisha
                        e.Row.Cells[9].Text = string.Empty;//added by Sachin
                        e.Row.Cells[10].Text = string.Empty;//added by Sachin
                        e.Row.Cells[12].Text = string.Empty;//added by Sachin
                        e.Row.Cells[13].Text = string.Empty;//added by Sachin
                        e.Row.Cells[4].Style.Add("border-style", "none");
                        e.Row.Cells[5].Style.Add("border-style", "none");
                        e.Row.Style.Add("background-color", "#9FCBEA");
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                //if (sortColumnIndex != -1)
                //{
                //    AddSortImage(sortColumnIndex, e.Row);
                //}
            }
        }

        protected void grdComplianceTransactions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandName.Equals("Sort")))
                {
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                    int complianceInstanceID = Convert.ToInt32(commandArgs[1]);
                    udcStatusTranscatopn.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function bind the upcoming compliances to the grid view.
        /// </summary>
        protected void BindComplianceTransactions()
        {
            try
            {
                if (ViewState["Role"] != null)
                {
                    List<ComplianceInstanceTransactionView> assignmentList = null;
                    if (ViewState["Role"].Equals("Performer"))
                    {
                        //Session["UPerformer"] = null;
                        //var UPerformer = DashboardManagement.DashboardDataForPerformer(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming);
                        //Session["UPerformer"] = UPerformer;
                        //grdComplianceTransactions.DataSource = UPerformer;
                        assignmentList = DashboardManagement.DashboardDataForPerformerUpcoming(AuthenticationHelper.UserID, "A", CannedReportFilterForPerformer.Rejected);
                    }
                    else if (ViewState["Role"].Equals("Reviewer"))
                    {
                        //Session["UReviewer"] = null;
                        //var UReviewer = DashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming);
                        //Session["UReviewer"] = UReviewer;
                        //grdComplianceTransactions.DataSource = UReviewer;
                        assignmentList = DashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, "A", CannedReportFilterForPerformer.Rejected);
                    }
                    else if (ViewState["Role"].Equals("Approver"))
                    {
                        //Session["UApprover"] = null;
                        //var UApprover = DashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, CannedReportFilterForPerformer.Upcoming);
                        //Session["UApprover"] = UApprover;
                        //grdComplianceTransactions.DataSource = UApprover;

                        assignmentList = DashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, "A", CannedReportFilterForPerformer.Rejected);
                    }

                    //if (ViewState["SortOrder"].ToString() == "Asc")
                    //{
                    //    if (ViewState["SortExpression"].ToString() == "ShortDescription")
                    //    {                           
                    //        //assignmentList = assignmentList.Where(entry => (entry.ComplianceStatusID != -1)).ToList();
                    //        assignmentList = assignmentList.OrderBy(entry => entry.ShortDescription).ToList();
                    //    }
                    //    if (ViewState["SortExpression"].ToString() == "NonComplianceType")
                    //    {
                    //        //assignmentList = assignmentList.Where(entry => (entry.ComplianceStatusID != -1)).ToList();
                    //        assignmentList = assignmentList.OrderBy(entry => entry.NonComplianceType).ToList();
                    //    }
                    //    if (ViewState["SortExpression"].ToString() == "ScheduledOn")
                    //    {
                    //        //assignmentList = assignmentList.Where(entry => (entry.ComplianceStatusID != -1)).ToList();
                    //        assignmentList = assignmentList.OrderBy(entry => entry.ScheduledOn).ToList();
                    //    }
                    //    if (ViewState["SortExpression"].ToString() == "ForMonth")
                    //    {
                    //        //assignmentList = assignmentList.Where(entry => (entry.ComplianceStatusID != -1)).ToList();
                    //        assignmentList = assignmentList.OrderBy(entry => entry.ForMonth).ToList();
                    //    }
                    //    if (ViewState["SortExpression"].ToString() == "Status")
                    //    {
                    //        //assignmentList = assignmentList.Where(entry => (entry.ComplianceStatusID != -1)).ToList();
                    //        assignmentList = assignmentList.OrderBy(entry => entry.Status).ToList();
                    //    }
                    //    if (ViewState["SortExpression"].ToString() == "EventName")
                    //    {
                    //        //assignmentList = assignmentList.Where(entry => (entry.ComplianceStatusID != -1)).ToList();
                    //        assignmentList = assignmentList.OrderBy(entry => entry.EventName).ToList();
                    //    }
                    //    if (ViewState["SortExpression"].ToString() == "ReferenceMaterialText")
                    //    {
                    //        //assignmentList = assignmentList.Where(entry => (entry.ComplianceStatusID != -1)).ToList();
                    //        assignmentList = assignmentList.OrderBy(entry => entry.ReferenceMaterialText).ToList();
                    //    }

                    //    direction = SortDirection.Descending;
                    //}
                    //else
                    //{
                    //    if (ViewState["SortExpression"].ToString() == "Description")
                    //    {
                    //        assignmentList = assignmentList.OrderByDescending(entry => entry.Description).ToList();
                    //    }
                    //    if (ViewState["SortExpression"].ToString() == "NonComplianceType")
                    //    {
                    //        assignmentList = assignmentList.OrderByDescending(entry => entry.NonComplianceType).ToList();
                    //    }
                    //    if (ViewState["SortExpression"].ToString() == "ScheduledOn")
                    //    {
                    //        assignmentList = assignmentList.OrderByDescending(entry => entry.ScheduledOn).ToList();
                    //    }
                    //    if (ViewState["SortExpression"].ToString() == "ForMonth")
                    //    {
                    //        assignmentList = assignmentList.OrderByDescending(entry => entry.ForMonth).ToList();
                    //    }
                    //    if (ViewState["SortExpression"].ToString() == "Status")
                    //    {
                    //        assignmentList = assignmentList.OrderByDescending(entry => entry.Status).ToList();
                    //    }
                    //    if (ViewState["SortExpression"].ToString() == "EventName")
                    //    {
                    //        assignmentList = assignmentList.OrderByDescending(entry => entry.EventName).ToList();
                    //    }
                    //    if (ViewState["SortExpression"].ToString() == "ReferenceMaterialText")
                    //    {
                    //        assignmentList = assignmentList.OrderByDescending(entry => entry.ReferenceMaterialText).ToList();
                    //    }
                    //    direction = SortDirection.Ascending;
                    //}

                    grdComplianceTransactions.DataSource = assignmentList;
                    grdComplianceTransactions.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function return true false value for status image.
        /// </summary>
        /// <param name="userID">long</param>
        /// <param name="roleID">int</param>
        /// <param name="statusID">int</param>
        /// <returns></returns>
        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1 || statusID == 6 || statusID == 10;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        /// <summary>
        /// this function is set sorting images.
        /// </summary>
        /// <param name="columnIndex"></param>
        /// <param name="headerRow"></param>
        void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        /// <summary>
        /// this function is set the sorting direction.
        /// </summary>
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        /// <summary>
        /// this function is return the string values for compliance type.
        /// </summary>
        /// <param name="userID">long</param>
        protected string ShowType(long? eventID)
        {
            try
            {
                if (eventID == null)
                {
                    return "Non-Event Based";
                }
                else
                {
                    return "Event Based";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }


        //added by Manisha
        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkbtn = sender as LinkButton;
                GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
                Label lblFilePath = (Label)gvrow.FindControl("lblFilePath");
                Label lblFileName = (Label)gvrow.FindControl("lblFileName");

                Response.ContentType = "application/octet-stream";
                string filePath = lblFilePath.Text;

                Response.AddHeader("Content-Disposition", "attachment;filename=\"" + filePath + "\"");
                Response.TransmitFile(Server.MapPath(filePath));
                Response.End();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton btn = (LinkButton)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                int complianceInstanceID = Convert.ToInt32(commandArgs[1]);
                udcStatusTranscatopn.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                BindComplianceTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected string GetPerformer(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 3);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }

        }
        protected string GetReviewer(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 4);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }

        }
        protected string GetApprover(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 6);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }

        }
  
    }
}