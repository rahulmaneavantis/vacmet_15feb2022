﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.IO;
using System.Net;
using System.Configuration;
using Ionic.Zip;
using com.VirtuosoITech.ComplianceManagement.Business.AWS;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class internalCheckliststatusperformer : System.Web.UI.Page
    {
        static string sampleFormPath4 = "";
        protected static bool IsDocumentCompulsary = false;
        protected string UploadDocumentLink;
        public static string InternalChecklistTaskDocViewPath = "";
        bool IsNotCompiled = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                IsNotCompiled = false;
                string listCustomer = ConfigurationManager.AppSettings["UploadUsingLinkCustomerlist"];
                string[] listCust = new string[] { };
                if (!string.IsNullOrEmpty(listCustomer))
                    listCust = listCustomer.Split(',');

                if (listCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                    UploadDocumentLink = "True";

                // STT Change- Add Status
                string customer = ConfigurationManager.AppSettings["NotCompliedCustID"].ToString();
                long customerID = AuthenticationHelper.CustomerID;
                List<string> NotCompliedCustIDList = customer.Split(',').ToList();
                if (NotCompliedCustIDList.Count > 0)
                {
                    foreach (string PList in NotCompliedCustIDList)
                    {
                        if (PList == customerID.ToString())
                        {
                            IsNotCompiled = true;
                            break;
                        }
                    }
                }
                if (IsNotCompiled == true)
                {
                    btnNotComplied.Visible = true;
                }

                if (!this.IsPostBack)
                {
                    int scheduleOnId = Convert.ToInt32(Request.QueryString["sId"].ToString());
                    int instanceId = Convert.ToInt32(Request.QueryString["InId"].ToString());
                    OpenTransactionPage(scheduleOnId, instanceId);
                    Labellockingmsg.Visible = false;
                    string listLockingCustomer = ConfigurationManager.AppSettings["LockingDaysCustomerlist"];
                    string[] listLockCust = new string[] { };
                    if (!string.IsNullOrEmpty(listLockingCustomer))
                        listLockCust = listLockingCustomer.Split(',');

                    if (listLockCust.Contains(Convert.ToString(AuthenticationHelper.CustomerID)))
                    {
                        if (Business.ComplianceManagement.GetCurrentStatusByScheduleONID(Convert.ToInt32(Request.QueryString["sId"].ToString()), "I", Convert.ToInt32(AuthenticationHelper.CustomerID)))
                        {
                            Labellockingmsg.Visible = true;
                            btnSave2.Enabled = false;
                        }
                        else
                        {
                            Labellockingmsg.Visible = false;
                            btnSave2.Enabled = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;
                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        private void BindTransactionDetails(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {
                grdDocument.DataSource = null;
                grdDocument.DataBind();

                lblRisk2.Text = string.Empty;

                ViewState["ScheduledOnID"] = ScheduledOnID;
                var complianceInfo = Business.InternalComplianceManagement.GetInternalComplianceByInstanceID(ScheduledOnID);
                var complianceForm = Business.InternalComplianceManagement.GetInternalComplianceFormByID(complianceInfo.ID);
                var RecentComplianceTransaction = Business.InternalComplianceManagement.GetCurrentStatusByInternalComplianceID(ScheduledOnID);
                lblComplianceID2.Text = Convert.ToString(complianceInfo.ID);
                lblComplianceDiscription2.Text = complianceInfo.IShortDescription;
                lblshortForm.Text = complianceInfo.IShortForm;   

                long CustomerID = 0;
                CustomerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                if (CustomerID == 691) // Customer All Time Plastic
                {
                    var IsData = Business.InternalComplianceManagement.GetInternalcomplianceByType(complianceInfo.ID, CustomerID);

                    if (IsData == true)
                    {
                        IsDocumentCompulsary = true;
                    }
                    else
                    {
                        IsDocumentCompulsary = false;
                    }
                }
                else
                {
                    IsDocumentCompulsary = false;
                }
                //var AllComData = Business.ComplianceManagement.GetPeriodBranchLocationPerformerInternalCheckList(ScheduledOnID, complianceInstanceID);

                var complinaceinstance = Business.ComplianceManagement.GetBranchLocationInternal(complianceInstanceID);
                var customerbranchID = complinaceinstance.CustomerBranchID;
                var showHideButton = BindSubTasks(ScheduledOnID, 3, customerbranchID);
                ViewState["complianceInstanceID"] = complinaceinstance.ID;
                lblFormNumber2.Text = complianceInfo.IRequiredFrom;
                //if (AllComData.Count > 0)
                //{
                //    foreach (var item in AllComData)
                //    {
                //        lblLocation.Text = item.Branch;
                //        lblDueDate.Text = Convert.ToDateTime(item.InternalScheduledOn).ToString("dd-MMM-yyyy");
                //        lblPeriod.Text = item.ForMonth;
                //    }
                //}

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    var AllComData = (from row in entities.InternalComplianceScheduledOns
                                      join row1 in entities.InternalComplianceInstances
                                        on row.InternalComplianceInstanceID equals row1.ID

                                      join CB in entities.CustomerBranches
                                      on row1.CustomerBranchID equals CB.ID
                                      where row.ID == ScheduledOnID
                                      select new
                                      {
                                          row.ForPeriod,
                                          row.ForMonth,
                                          CB.Name,
                                          row.ScheduledOn
                                      }).FirstOrDefault();
                    if (AllComData != null)
                    {

                        lblLocation.Text = AllComData.Name;
                        lblDueDate.Text = Convert.ToDateTime(AllComData.ScheduledOn).ToString("dd-MMM-yyyy");
                        lblPeriod.Text = AllComData.ForMonth;

                    }
                }
                string risk = Business.InternalComplianceManagement.GetRiskType(complianceInfo);
                lblRiskType2.Text = Business.InternalComplianceManagement.GetRisk(complianceInfo);
                lblRisk2.Text = risk;
                BindTempDocumentData(ScheduledOnID);
                if (risk == "HIGH")
                {
                    divRiskType2.Attributes["style"] = "background-color:red;";
                }
                else if (risk == "MEDIUM")
                {
                    divRiskType2.Attributes["style"] = "background-color:yellow;";
                }
                else if (risk == "LOW")
                {
                    divRiskType2.Attributes["style"] = "background-color:green;";
                }
                else if (risk == "CRITICAL")
                {
                    //divRiskType2.Attributes["style"] = "background-color:orange;";
                    divRiskType2.Attributes["style"] = "background-color:#CC0900;";
                }

                var AuditChecklistName = Business.ComplianceManagement.GetAuditChecklistName(complianceInfo.ID, Convert.ToInt32(AuthenticationHelper.CustomerID));

                if (!string.IsNullOrEmpty(AuditChecklistName))
                {
                    lblAuditChecklist.Text = AuditChecklistName;
                    trAuditChecklist.Visible = true;
                }
                else
                {
                    trAuditChecklist.Visible = false;
                }

                if (RecentComplianceTransaction.ComplianceStatusID == 10)
                {
                    BindDocument(ScheduledOnID);
                }
                else
                {
                    divDeleteDocument.Visible = false;
                    rptComplianceDocumnets.DataSource = null;
                    rptComplianceDocumnets.DataBind();
                    rptWorkingFiles.DataSource = null;
                    rptWorkingFiles.DataBind();
                }

                if (complianceForm != null)
                {
                    lbDownloadSample2.Text = "Download";  // "<u style=color:blue>Click here</u> to download sample form.";
                    lbDownloadSample2.CommandArgument = complianceForm.IComplianceID.ToString();

                    lblNote.Visible = true;

                    #region save samle form
                    string Filename = complianceForm.Name;
                    string filePath = sampleFormPath4;
                    if (filePath != null)
                    {
                        try
                        {
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + File;
                            string extension = System.IO.Path.GetExtension(filePath);
                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                            if (!Directory.Exists(DateFolder))
                            {
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                            }
                            int customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                            string FileName = DateFolder + "/" + User + "" + extension;
                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            bw.Write(complianceForm.FileData);
                            bw.Close();
                            lblpathsample.Text = FileName;
                        }
                        catch (Exception ex)
                        {

                        }

                        var complianceform = Business.InternalComplianceManagement.GetMultipleInternalComplianceForm(Convert.ToInt32(lbDownloadSample2.CommandArgument));

                        rptComplianceSampleView.DataSource = complianceform;
                        rptComplianceSampleView.DataBind();
                    }
                    #endregion
                }
                else
                {
                    lblNote.Visible = false;
                }

                btnSave2.Attributes.Remove("disabled");
                if (RecentComplianceTransaction.ComplianceStatusID == 1 || RecentComplianceTransaction.ComplianceStatusID == 6 || RecentComplianceTransaction.ComplianceStatusID == 10 || RecentComplianceTransaction.ComplianceStatusID == 19)
                {
                    divUploadDocument.Visible = true;
                }
                BindTransactions(ScheduledOnID);
                tbxRemarks.Text = string.Empty;
                hdnComplianceInstanceID.Value = complianceInstanceID.ToString();
                hdnComplianceScheduledOnId.Value = ScheduledOnID.ToString();

                upComplianceDetails.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindDocument(int ScheduledOnID)
        {
            try
            {
                List<GetInternalComplianceDocumentsView> ComplianceDocument = new List<GetInternalComplianceDocumentsView>();
                ComplianceDocument = DocumentManagement.GetFileDataInternal(ScheduledOnID).Where(entry => entry.Version.Equals("1.0")).ToList();
                rptComplianceDocumnets.DataSource = ComplianceDocument.Where(entry => entry.FileType == 1).ToList();
                rptComplianceDocumnets.DataBind();

                rptWorkingFiles.DataSource = ComplianceDocument.Where(entry => entry.FileType == 2).ToList();
                rptWorkingFiles.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
        }

        protected void grdTransactionHistory2_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var complianceTransactionHistory = Business.InternalComplianceManagement.GetAllTransactionLog(Convert.ToInt32(ViewState["ScheduledOnID"]));
                if (direction == SortDirection.Ascending)
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdTransactionHistory2.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndexHistory"] = grdTransactionHistory2.Columns.IndexOf(field);
                    }
                }

                grdTransactionHistory2.DataSource = complianceTransactionHistory;
                grdTransactionHistory2.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory2_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTransactionHistory2.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["ScheduledOnID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactions(int ScheduledOnID)
        {
            try
            {
                grdTransactionHistory2.DataSource = Business.InternalComplianceManagement.GetAllTransactionLog(ScheduledOnID);
                grdTransactionHistory2.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
            try
            {
                Boolean isDocument = false;
                if (IsDocumentCompulsary == true)
                {
                    // Document mandatory
                    if (grdDocument.Rows.Count > 0)
                    {
                        for (int i = 0; i < grdDocument.Rows.Count; i++)
                        {
                            Label lblDocType = (Label)grdDocument.Rows[i].FindControl("lblDocType");
                            if (lblDocType.Text == "Compliance Document")
                            {
                                isDocument = true;
                            }
                        }
                    }
                    else
                    {
                        isDocument = false;
                    }
                }
                else
                {
                    // Document not mandatory
                    isDocument = true;
                }

                if (isDocument == true)
                {
                    InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                    {
                        InternalComplianceScheduledOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                        InternalComplianceInstanceID = Convert.ToInt64(hdnComplianceInstanceID.Value),
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedByText = AuthenticationHelper.User,
                        StatusId = 4,
                        StatusChangedOn = DateTime.Now, 
                        Remarks = tbxRemarks.Text
                    };

                    List<InternalFileData> files = new List<InternalFileData>();
                    List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();

                    var httpRequest = System.Web.HttpContext.Current.Request;
                    HttpFileCollection fileCollection1 = httpRequest.Files;
                    string directoryPath = null;
                    bool blankfileCount = true;
                    var TempDocData = Business.ComplianceManagement.GetTempInternalChecklistDocumentData(Convert.ToInt64(hdnComplianceScheduledOnId.Value));

                    if (TempDocData.Count > 0)
                    {
                        var InstanceData = DocumentManagement.GetInternalComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));
                        int customerID =Convert.ToInt32(AuthenticationHelper.CustomerID);
                        var AWSData = AmazonS3.GetAWSStorageDetail(customerID);
                        if (AWSData != null)
                        {
                            #region AWS Storage
                         
                          
                            string version = DocumentManagement.GetDocumnetVersionInternal(Convert.ToInt32(hdnComplianceScheduledOnId.Value));
                            directoryPath = "InternalAvacomDocuments\\" + customerID + "\\" + InstanceData.CustomerBranchID + "\\" + InstanceData.ID.ToString() + "\\" + hdnComplianceScheduledOnId.Value + "\\" + version;
                            foreach (var item in TempDocData)
                            {
                                if (item.ISLink == true)
                                {
                                    String fileName = "";
                                    if (item.DocType == "IC")
                                    {
                                        fileName = "InternalComplianceDoc_" + item.DocName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                    }
                                    InternalFileData file = new InternalFileData()
                                    {
                                        Name = fileName,
                                        Version = "1.0",
                                        FilePath = item.DocPath,
                                        VersionDate = DateTime.UtcNow,
                                        ISLink = true
                                    };
                                    files.Add(file);
                                }
                                else
                                {
                                    String fileName = "";
                                    if (item.DocType == "IC")
                                    {
                                        fileName = "InternalComplianceDoc_" + item.DocName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                    }
                                    Guid fileKey = Guid.NewGuid();
                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));
                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));

                                    if (item.DocData.Length > 0)
                                    {
                                        string filepathvalue = string.Empty;
                                        string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                        filepathvalue = vale.Replace(@"\", "/");

                                        InternalFileData file = new InternalFileData()
                                        {
                                            Name = fileName,
                                            FilePath = filepathvalue, // directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                            FileKey = fileKey.ToString(),
                                            Version = "1.0",
                                            VersionDate = DateTime.UtcNow,
                                        };

                                        files.Add(file);

                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(item.DocName))
                                            blankfileCount = false;
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region Normal Storage
                           
                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                            {
                                directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "/InternalAvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/1.0";
                            }
                            else
                            {
                                directoryPath = Server.MapPath("~/InternalAvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/1.0");
                            }

                            DocumentManagement.CreateDirectory(directoryPath);
                            foreach (var item in TempDocData)
                            {
                                if (item.ISLink == true)
                                {
                                    String fileName = "";
                                    if (item.DocType == "IC")
                                    {
                                        fileName = "InternalComplianceDoc_" + item.DocName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                    }
                                    InternalFileData file = new InternalFileData()
                                    {
                                        Name = fileName,
                                        Version = "1.0",
                                        FilePath = item.DocPath,
                                        VersionDate = DateTime.UtcNow,
                                        ISLink = true
                                    };
                                    files.Add(file);
                                }
                                else
                                {
                                    String fileName = "";
                                    if (item.DocType == "IC")
                                    {
                                        fileName = "InternalComplianceDoc_" + item.DocName;
                                        list.Add(new KeyValuePair<string, int>(fileName, 1));
                                    }
                                    Guid fileKey = Guid.NewGuid();
                                    string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));
                                    Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));

                                    if (item.DocData.Length > 0)
                                    {
                                        string filepathvalue = string.Empty;
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                            filepathvalue = vale;
                                        }
                                        else
                                        {
                                            filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                        }
                                        InternalFileData file = new InternalFileData()
                                        {
                                            Name = fileName,
                                            FilePath = filepathvalue, // directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                            FileKey = fileKey.ToString(),
                                            Version = "1.0",
                                            VersionDate = DateTime.UtcNow,
                                        };

                                        files.Add(file);

                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(item.DocName))
                                            blankfileCount = false;
                                    }
                                }
                            }
                            #endregion
                        }
                    }

                    bool flag = false;
                    bool flag1 = false;
                    if (blankfileCount)
                    {
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        flag = Business.InternalComplianceManagement.CreateTransaction(transaction, files, list, Filelist, directoryPath, Convert.ToInt64(hdnComplianceScheduledOnId.Value), customerID);
                        flag1 = Business.ComplianceManagement.DeleteTempInternalChecklistDocumentFileFromScheduleOnID(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                        ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                    }

                    if (flag != true)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                    try
                    {
                        Business.InternalComplianceManagement.BindInternalStatutoryCheckListDatainRedis(Convert.ToInt64(hdnComplianceInstanceID.Value), Convert.ToInt32(AuthenticationHelper.CustomerID));
                    }
                    catch { }
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('save successfully')", true);

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "CloseAndBindData();", true);
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseCalenderPERPop();", true);
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select documents for upload.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdTransactionHistory2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DOWNLOAD_FILE"))
                {
                    int fileID = Convert.ToInt32(e.CommandArgument);
                    var file = Business.ComplianceManagement.GetFile(fileID);
                    Response.Buffer = true;
                    Response.Clear();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                    Response.Flush(); // send it to the client to download
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnDownload2_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(Convert.ToInt32(hdlSelectedDocumentID.Value));
                Response.Buffer = true;
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                Response.End();
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void lbDownloadSample2_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.InternalComplianceManagement.GetInternalComplianceFormByID(Convert.ToInt32(lbDownloadSample2.CommandArgument));

                Response.Buffer = true;
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                Response.BinaryWrite(file.FileData); // create the file
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails2_Load(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory2_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndexHistory"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        public void OpenTransactionPage(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {
                BindTransactionDetails(ScheduledOnID, complianceInstanceID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public event EventHandler OnSaved;

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            HttpFileCollection fileCollection = Request.Files;
            for (int i = 0; i < fileCollection.Count; i++)
            {
                HttpPostedFile uploadfile = fileCollection[i];
                string fileName = Path.GetFileName(uploadfile.FileName);
                if (uploadfile.ContentLength > 0)
                {
                }
            }
        }

        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Download"))
                {
                    DownloadFile(Convert.ToInt32(e.CommandArgument));
                }
                else if (e.CommandName.Equals("Delete"))
                {
                    DeleteFile(Convert.ToInt32(e.CommandArgument));
                    BindDocument(Convert.ToInt32(ViewState["ScheduledOnID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("Download"))
            {
                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            else if (e.CommandName.Equals("Delete"))
            {
                DeleteFile(Convert.ToInt32(e.CommandArgument));
                BindDocument(Convert.ToInt32(ViewState["ScheduledOnID"]));
            }
        }

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = string.Empty;
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        string pathvalue = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.Name));
                        filePath = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
                    }
                    else
                    {
                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    }
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void DeleteFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);
                if (file != null)
                {
                    string path = string.Empty;
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        string pathvalue = file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name);
                        path = pathvalue.Replace("~", ConfigurationManager.AppSettings["DriveUrl"]);
                    }
                    else
                    {
                        path = Server.MapPath(file.FilePath + "/" + file.FileKey + Path.GetExtension(file.Name));
                    }
                    DocumentManagement.DeleteFile(path, fileId);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lbtLinkDocbutton = (LinkButton)e.Item.FindControl("lbtLinkDocbutton");
                scriptManager.RegisterAsyncPostBackControl(lbtLinkDocbutton);
            }
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lbtLinkbutton = (LinkButton)e.Item.FindControl("lbtLinkbutton");
                scriptManager.RegisterAsyncPostBackControl(lbtLinkbutton);
            }
        }



        private bool BindSubTasks(long ComplianceScheduleOnID, int roleID, int CustomerBranchid)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    entities.Database.CommandTimeout = 360;
                    var documentData = (from row in entities.SP_TaskInstanceTransactionInternalView(customerID)
                                        where row.ParentID == null
                                        //&& row.ForMonth == period
                                        && row.RoleID == roleID
                                        && row.CustomerBranchID == CustomerBranchid
                                        && row.ComplianceScheduleOnID == ComplianceScheduleOnID
                                        select row).ToList();
                    if (documentData.Count != 0)
                    {
                        divTask.Visible = true;
                        gridSubTaskInternal.DataSource = documentData;
                        gridSubTaskInternal.DataBind();
                        var closedSubTaskCount = documentData.Where(entry => (entry.TaskStatusID == 4 || entry.TaskStatusID == 5)).Count();
                        if (documentData.Count == closedSubTaskCount)
                            return true;
                        else
                            return false;
                    }
                    else
                    {
                        divTask.Visible = false;
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return false;
            }
        }
        protected string GetUserName(long taskInstanceID, long taskScheduleOnID, int roleID, byte taskType)
        {
            try
            {
                string result = "";
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                result = TaskManagment.GetTaskAssignedUser(customerID, taskType, taskInstanceID, taskScheduleOnID, roleID);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }

        }

        protected void gridSubTaskInternal_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);

                if (taskScheduleOnID != 0)
                {
                    List<GetTaskDocumentView> taskDocument = new List<GetTaskDocumentView>();

                    taskDocument = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                    if (e.CommandName.Equals("Download"))
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            if (taskDocument.Count > 0)
                            {
                                string fileName = string.Empty;

                                GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

                                Label lblTaskTitle = null;

                                if (row != null)
                                {
                                    lblTaskTitle = (Label)row.FindControl("lblTaskTitle");
                                    if (lblTaskTitle != null)
                                        fileName = lblTaskTitle.Text + "-Documents";

                                    if (fileName.Length > 250)
                                        fileName = "TaskDocuments";
                                }

                                ComplianceZip.AddDirectoryByName(commandArgs[0]);

                                int i = 0;
                                foreach (var eachFile in taskDocument)
                                {
                                    string filePath = Path.Combine(Server.MapPath(eachFile.FilePath), eachFile.FileKey + Path.GetExtension(eachFile.FileName));
                                    if (eachFile.FilePath != null && File.Exists(filePath))
                                    {
                                        string[] filename = eachFile.FileName.Split('.');
                                        string str = filename[0] + i + "." + filename[1];
                                        ComplianceZip.AddEntry(commandArgs[0] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        i++;
                                    }
                                }
                            }

                            var zipMs = new MemoryStream();
                            ComplianceZip.Save(zipMs);
                            zipMs.Position = 0;
                            byte[] data = zipMs.ToArray();

                            Response.Buffer = true;

                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=TaskDocuments.zip");
                            Response.BinaryWrite(data);
                            Response.Flush();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        }
                    }
                    else if (e.CommandName.Equals("View"))
                    {
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();

                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                foreach (var file in taskDocumenttoView)
                                {
                                    rptInternalChecklistVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptInternalChecklistVersionView.DataBind();

                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);

                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        bw.Close();

                                        InternalChecklistTaskDocViewPath = FileName;
                                        InternalChecklistTaskDocViewPath = InternalChecklistTaskDocViewPath.Substring(2, InternalChecklistTaskDocViewPath.Length - 2);

                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocInternalChecklisttaskfileReview('" + InternalChecklistTaskDocViewPath + "');", true);
                                        lblMessageInternalChecklist.Text = "";
                                        UpdatePanel4.Update();
                                    }
                                    else
                                    {
                                        lblMessageInternalChecklist.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocInternalChecklisttaskfileReview();", true);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptInternalChecklistVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                int taskScheduleOnID = Convert.ToInt32(commandArgs[0]);
                int taskFileidID = Convert.ToInt32(commandArgs[2]);
                if (taskScheduleOnID != 0)
                {
                    if (e.CommandName.Equals("View"))
                    {
                        #region
                        List<GetTaskDocumentView> taskDocumenttoView = new List<GetTaskDocumentView>();
                        List<GetTaskDocumentView> taskFileData = new List<GetTaskDocumentView>();
                        taskDocumenttoView = TaskManagment.GetTaskRelatedFileData(taskScheduleOnID).ToList();
                        taskFileData = taskDocumenttoView;
                        taskDocumenttoView = taskDocumenttoView.Where(entry => entry.FileID == taskFileidID).ToList();

                        Session["ScheduleOnID"] = taskScheduleOnID;

                        if (taskDocumenttoView != null && taskDocumenttoView.Count > 0)
                        {
                            List<GetTaskDocumentView> entitiesData = taskDocumenttoView.Where(entry => entry.Version != null).ToList();

                            if (taskDocumenttoView.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                GetTaskDocumentView entityData = new GetTaskDocumentView();
                                entityData.Version = "1.0";
                                entityData.TaskScheduleOnID = Convert.ToInt64(taskScheduleOnID);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                rptInternalChecklistVersionView.DataSource = taskFileData.OrderBy(entry => entry.Version);
                                rptInternalChecklistVersionView.DataBind();

                                foreach (var file in taskDocumenttoView)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);

                                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string customerID = Convert.ToString(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        bw.Close();

                                        InternalChecklistTaskDocViewPath = FileName;
                                        InternalChecklistTaskDocViewPath = InternalChecklistTaskDocViewPath.Substring(2, InternalChecklistTaskDocViewPath.Length - 2);

                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocInternalChecklisttaskfileReview('" + InternalChecklistTaskDocViewPath + "');", true);
                                        lblMessageInternalChecklist.Text = "";
                                    }
                                    else
                                    {
                                        lblMessageInternalChecklist.Text = "There is no file to preview";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopendocInternalChecklisttaskfileReview();", true);
                                    }
                                    break;
                                }
                            }
                        }
                        #endregion                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gridSubTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    Label lblSlashReview = (Label)e.Row.FindControl("lblSlashReview");
                    LinkButton btnSubTaskDocView = (LinkButton)e.Row.FindControl("btnSubTaskDocView");
                    LinkButton btnSubTaskDocDownload = (LinkButton)e.Row.FindControl("btnSubTaskDocDownload");

                    if (lblStatus != null && btnSubTaskDocDownload != null && lblSlashReview != null && btnSubTaskDocView != null)
                    {
                        if (lblStatus.Text != "")
                        {
                            if (lblStatus.Text == "Open")
                            {
                                btnSubTaskDocDownload.Visible = false;
                                lblSlashReview.Visible = false;
                                btnSubTaskDocView.Visible = false;
                            }
                            else
                            {
                                btnSubTaskDocDownload.Visible = true;
                                lblSlashReview.Visible = true;
                                btnSubTaskDocView.Visible = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lbDownloadSample2_Click1(object sender, EventArgs e)
        {
            try
            {
                var complianceform = Business.InternalComplianceManagement.GetMultipleInternalComplianceFormByID(Convert.ToInt32(lbDownloadSample2.CommandArgument));
                int complianceID1 = Convert.ToInt32(lbDownloadSample2.CommandArgument);
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    int i = 0;
                    foreach (var file in complianceform)
                    {
                        if (file.FileData != null)
                        {
                            string[] filename = file.Name.Split('.');
                            string str = filename[0] + i + "." + filename[1];
                            ComplianceZip.AddEntry(str, file.FileData);
                            i++;
                        }
                    }
                    string fileName = "ComplianceID_" + complianceID1 + "_InternalSampleForms.zip";
                    var zipMs = new MemoryStream();
                    ComplianceZip.Save(zipMs);
                    zipMs.Position = 0;
                    byte[] data = zipMs.ToArray();
                    Response.Buffer = true;
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename= " + fileName);
                    Response.BinaryWrite(data);
                    Response.Flush();
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceSampleView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                if (e.CommandName.Equals("View"))
                {
                    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    InternalComplianceForm complianceForm = Business.InternalComplianceManagement.GetSelectedInternalComplianceFileName(Convert.ToInt32(commandArgs[0]), Convert.ToInt32(commandArgs[1]));

                    string Filename = complianceForm.Name;
                    string filePath = Filename;
                    if (filePath != null)
                    {
                        try
                        {
                            string Folder = "~/TempFiles";
                            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                            string DateFolder = Folder + "/" + File;
                            string extension = System.IO.Path.GetExtension(filePath);
                            //string[] path = filePath.Split('/');
                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                            if (!Directory.Exists(DateFolder))
                            {
                                Directory.CreateDirectory(Server.MapPath(DateFolder));
                            }
                            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                            string User = AuthenticationHelper.UserID + "" + FileDate;
                            string FileName = DateFolder + "/" + User + "" + extension;
                            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                            BinaryWriter bw = new BinaryWriter(fs);
                            bw.Write(complianceForm.FileData);
                            bw.Close();
                            lblpathsample.Text = FileName;

                            if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "fopenSampleFile();", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfile4('" + lblpathsample.Text + "');", true);
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceSampleView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblSampleView = (LinkButton)e.Item.FindControl("lblSampleView");
                scriptManager.RegisterAsyncPostBackControl(lblSampleView);
            }
        }

        protected void UploadDocument_Click(object sender, EventArgs e)
        {
            try
            {
                grdDocument.DataSource = null;
                grdDocument.DataBind();
                long ScheduledOnID = Convert.ToInt64(ViewState["ScheduledOnID"]);
                long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

                TempComplianceDocument tempComplianceDocument = null;
                HttpFileCollection fileCollection = Request.Files;
                if (fileCollection.Count > 0)
                {
                    #region file upload

                    bool isBlankFile = false;
                    for (int i = 0; i < fileCollection.Count; i++)
                    {
                        HttpPostedFile uploadfile = null;
                        uploadfile = fileCollection[i];
                        int filelength = uploadfile.ContentLength;
                        if (filelength == 0)
                        {
                            isBlankFile = true;
                        }
                    }
                    if (isBlankFile == false)
                    {
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadfile = null;
                            uploadfile = fileCollection[i];
                            string fileName = Path.GetFileName(uploadfile.FileName);
                            string directoryPath = null;

                            if (!string.IsNullOrEmpty(fileName))
                            {
                                string[] keys = fileCollection.Keys[i].Split('$');
                                if (keys[keys.Count() - 1].Equals("fuSampleFile2"))
                                {
                                    directoryPath = Server.MapPath("~/TempDocuments/InternalCheckList/");
                                }

                                DocumentManagement.CreateDirectory(directoryPath);
                                string finalPath = Path.Combine(directoryPath, fileName);
                                finalPath = finalPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");

                                fileCollection[i].SaveAs(Server.MapPath(finalPath));

                                Stream fs = uploadfile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);

                                tempComplianceDocument = new TempComplianceDocument()
                                {
                                    ScheduleOnID = ScheduledOnID,
                                    ComplianceInstanceID = complianceInstanceID,
                                    DocPath = finalPath,
                                    DocData = bytes,
                                    DocName = fileCollection[i].FileName,
                                };

                                if (keys[keys.Count() - 1].Equals("fuSampleFile2"))
                                {
                                    tempComplianceDocument.DocType = "IC";
                                }

                                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                                if (_objTempDocumentID > 0)
                                {
                                    BindTempDocumentData(ScheduledOnID);
                                }

                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                    }
                    #endregion
                }

                BindTempDocumentData(ScheduledOnID);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void BindTempDocumentData(long ScheduledOnID)
        {
            try
            {
                grdDocument.DataSource = null;
                grdDocument.DataBind();
                var DocData = DocumentManagement.GetTempInternalCheckListDocumentData(ScheduledOnID);
                if (DocData.Count > 0)
                {
                    grdDocument.Visible = true;
                    grdDocument.DataSource = DocData;
                    grdDocument.DataBind();
                }
                else
                {
                    grdDocument.Visible = false;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdDocument_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Delete Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        if (Business.ComplianceManagement.DeleteTempDocumentFile(FileID))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Document deleted successfully.')", true);

                            BindTempDocumentData(Convert.ToInt64(ViewState["ScheduledOnID"]));
                        }
                    }
                }
                else if (e.CommandName.Equals("Download Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        using (ZipFile ComplianceZip = new ZipFile())
                        {
                            TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
                            if (file != null)
                            {
                                Response.Buffer = true;
                                Response.ClearContent();
                                Response.ClearHeaders();
                                Response.Clear();
                                Response.ContentType = "application/octet-stream";
                                Response.AddHeader("content-disposition", "attachment; filename= " + file.DocName);
                                Response.TransmitFile(Server.MapPath(file.DocPath));
                                Response.Flush();
                                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                            }
                        }
                        BindTempDocumentData(Convert.ToInt64(ViewState["ScheduledOnID"]));
                    }
                }
                else if (e.CommandName.Equals("View Document"))
                {
                    long FileID = -1;
                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    FileID = Convert.ToInt64(commandArgs[0]);
                    if (FileID > 0)
                    {
                        TempComplianceDocument file = Business.ComplianceManagement.GetTempComplianceDocument(FileID);
                        if (file != null)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.DocPath));
                            if (file.DocName != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                string DateFolder = Folder + "/" + File;
                                string extension = System.IO.Path.GetExtension(filePath);
                                if (extension.ToUpper() == ".ZIP" || extension.ToUpper() == ".7Z")
                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Zip file can't view please download it.')", true);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Zip file can't view please download it.";
                                }
                                else
                                {
                                    string CompDocReviewPath = file.DocPath;

                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopenDocumentPriview('" + CompDocReviewPath + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('There is no file to preview.')", true);
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "There is no file to preview.";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again."; ;
            }
        }

        protected void grdDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblRedirectDocumentTrue = (Label)e.Row.FindControl("lblRedirectDocument");
                    Label lblIsLinkTrue = (Label)e.Row.FindControl("lblIsLinkTrue");

                    Label lblDocType = (Label)e.Row.FindControl("lblDocType");

                    if (lblDocType.Text.Trim() == "IC")
                    {
                        lblDocType.Text = "Compliance Document";
                    }

                    LinkButton lnkDownloadDocument = (LinkButton)e.Row.FindControl("lnkDownloadDocument");
                    LinkButton lnkViewDocument = (LinkButton)e.Row.FindControl("lnkViewDocument");
                    LinkButton lnkRedirectDocument = (LinkButton)e.Row.FindControl("lnkRedierctDocument");
                    //LinkButton lnkDeleteDocument = (LinkButton)e.Row.FindControl("lnkDeleteDocument");

                    if (lblIsLinkTrue.Text == "True")
                    {
                        lnkDownloadDocument.Visible = false;
                        lnkViewDocument.Visible = false;
                        lnkRedirectDocument.Visible = true;
                        lblRedirectDocumentTrue.Visible = false;
                    }
                    else
                    {
                        lnkDownloadDocument.Visible = true;
                        lnkViewDocument.Visible = true;
                        lnkRedirectDocument.Visible = false;
                        lblRedirectDocumentTrue.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "CloseAndBindData();", true);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseCalenderPERPop();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnNotApplicable_Click(object sender, EventArgs e)
        {
            try
            {
                List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                {
                    InternalComplianceScheduledOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                    InternalComplianceInstanceID = Convert.ToInt64(hdnComplianceInstanceID.Value),
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedByText = AuthenticationHelper.User,
                    StatusId = 15,
                    StatusChangedOn = DateTime.Now,
                    Remarks = "Not Applicable"
                };

                List<InternalFileData> files = new List<InternalFileData>();
                List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                var httpRequest = System.Web.HttpContext.Current.Request;
                HttpFileCollection fileCollection1 = httpRequest.Files;
                bool blankfileCount = true;
                var TempDocData = Business.ComplianceManagement.GetTempInternalChecklistDocumentData(Convert.ToInt64(hdnComplianceScheduledOnId.Value));

                if (TempDocData.Count > 0)
                {
                    int? customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                    var InstanceData = DocumentManagement.GetInternalComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));
                    string directoryPath = null;
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "/InternalAvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/1.0";
                    }
                    else
                    {
                        directoryPath = Server.MapPath("~/InternalAvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/1.0");
                    }
                    DocumentManagement.CreateDirectory(directoryPath);
                    foreach (var item in TempDocData)
                    {
                        String fileName = "";
                        if (item.DocType == "IC")
                        {
                            fileName = "InternalComplianceDoc_" + item.DocName;
                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                        }
                        Guid fileKey = Guid.NewGuid();
                        string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));
                        Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));

                        if (item.DocData.Length > 0)
                        {
                            string filepathvalue = string.Empty;
                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                            {
                                string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                filepathvalue = vale;
                            }
                            else
                            {
                                filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                            }
                            InternalFileData file = new InternalFileData()
                            {
                                Name = fileName,
                                FilePath = filepathvalue, // directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                FileKey = fileKey.ToString(),
                                Version = "1.0",
                                VersionDate = DateTime.UtcNow,
                            };
                            files.Add(file);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(item.DocName))
                                blankfileCount = false;
                        }
                    }
                }
                bool flag = false;
                bool flag1 = false;
                if (blankfileCount)
                {
                    flag = Business.InternalComplianceManagement.CreateTransaction(transaction, files, list, Filelist);
                    flag1 = Business.ComplianceManagement.DeleteTempInternalChecklistDocumentFileFromScheduleOnID(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                }

                if (flag != true)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
                try
                {
                    Business.InternalComplianceManagement.BindInternalStatutoryCheckListDatainRedis(Convert.ToInt64(hdnComplianceInstanceID.Value), Convert.ToInt32(AuthenticationHelper.CustomerID));
                }
                catch { }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "CloseAndBindData();", true);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseCalenderPERPop();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static string getFileName(string url)
        {
            string[] arr = url.Split('/');
            return arr[arr.Length - 1];
        }
        protected void Uploadlingchecklistfile_Click(object sender, EventArgs e)
        {
            grdDocument.DataSource = null;
            grdDocument.DataBind();
            long ScheduledOnID = Convert.ToInt64(ViewState["ScheduledOnID"]);
            long complianceInstanceID = Convert.ToInt64(ViewState["complianceInstanceID"]);

            string url = TxtChecklistDocument.Text;
            string fileName = getFileName(url);
            if (!string.IsNullOrEmpty(TxtChecklistDocument.Text))
            {
                TempComplianceDocument tempComplianceDocument = null;
                var bytes = new byte[] { };// { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 };   
                tempComplianceDocument = new TempComplianceDocument()
                {
                    ScheduleOnID = ScheduledOnID,
                    ComplianceInstanceID = complianceInstanceID,
                    DocPath = url,//userpath,
                    DocData = bytes,
                    DocName = fileName,
                    DocType = "IC",
                    ISLink = true
                };

                long _objTempDocumentID = DocumentManagement.CreateTempComplinaceDocument(tempComplianceDocument);

                if (_objTempDocumentID > 0)
                {
                    BindTempDocumentData(ScheduledOnID);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
            }
            TxtChecklistDocument.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "openModal();", true);

        }

        protected void btnNotComplied_Click(object sender, EventArgs e)
        {
            try
            {
                List<KeyValuePair<string, Byte[]>> Filelist = new List<KeyValuePair<string, Byte[]>>();
                InternalComplianceTransaction transaction = new InternalComplianceTransaction()
                {
                    InternalComplianceScheduledOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                    InternalComplianceInstanceID = Convert.ToInt64(hdnComplianceInstanceID.Value),
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedByText = AuthenticationHelper.User,
                    StatusId = 17,
                    StatusChangedOn = DateTime.Now,
                    Remarks = "Not Complied"
                };

                List<InternalFileData> files = new List<InternalFileData>();
                List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
                var httpRequest = System.Web.HttpContext.Current.Request;
                HttpFileCollection fileCollection1 = httpRequest.Files;
                bool blankfileCount = true;
                var TempDocData = Business.ComplianceManagement.GetTempInternalChecklistDocumentData(Convert.ToInt64(hdnComplianceScheduledOnId.Value));

                if (TempDocData.Count > 0)
                {
                    int? customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                    var InstanceData = DocumentManagement.GetInternalComplianceInstanceData(Convert.ToInt32(hdnComplianceInstanceID.Value));
                    string directoryPath = null;
                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    {
                        directoryPath = ConfigurationManager.AppSettings["DriveUrl"] + "/InternalAvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/1.0";
                    }
                    else
                    {
                        directoryPath = Server.MapPath("~/InternalAvacomDocuments/" + customerID + "/" + InstanceData.CustomerBranchID + "/" + InstanceData.ID.ToString() + "/" + hdnComplianceScheduledOnId.Value + "/1.0");
                    }
                    DocumentManagement.CreateDirectory(directoryPath);
                    foreach (var item in TempDocData)
                    {
                        String fileName = "";
                        if (item.DocType == "IC")
                        {
                            fileName = "InternalComplianceDoc_" + item.DocName;
                            list.Add(new KeyValuePair<string, int>(fileName, 1));
                        }
                        Guid fileKey = Guid.NewGuid();
                        string finalPath = Path.Combine(directoryPath, fileKey + Path.GetExtension(item.DocName));
                        Filelist.Add(new KeyValuePair<string, Byte[]>(finalPath, item.DocData));

                        if (item.DocData.Length > 0)
                        {
                            string filepathvalue = string.Empty;
                            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                            {
                                string vale = directoryPath.Replace(ConfigurationManager.AppSettings["DriveUrl"], "~");
                                filepathvalue = vale;
                            }
                            else
                            {
                                filepathvalue = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                            }
                            InternalFileData file = new InternalFileData()
                            {
                                Name = fileName,
                                FilePath = filepathvalue, // directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/"),
                                FileKey = fileKey.ToString(),
                                Version = "1.0",
                                VersionDate = DateTime.UtcNow,
                            };
                            files.Add(file);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(item.DocName))
                                blankfileCount = false;
                        }
                    }
                }
                bool flag = false;
                bool flag1 = false;
                if (blankfileCount)
                {
                    flag = Business.InternalComplianceManagement.CreateTransaction(transaction, files, list, Filelist);
                    flag1 = Business.ComplianceManagement.DeleteTempInternalChecklistDocumentFileFromScheduleOnID(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please do not upload virus file or blank files.";
                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "missingAlert", "alert('Please do not upload virus file or blank files.')", true);
                }

                if (flag != true)
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
                try
                {
                    Business.InternalComplianceManagement.BindInternalStatutoryCheckListDatainRedis(Convert.ToInt64(hdnComplianceInstanceID.Value), Convert.ToInt32(AuthenticationHelper.CustomerID));
                }
                catch { }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "CloseAndBindData();", true);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "CloseCalenderPERPop();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }

}