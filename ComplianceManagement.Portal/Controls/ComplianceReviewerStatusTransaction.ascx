﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComplianceReviewerStatusTransaction.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.ComplianceReviewerStatusTransaction" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>

<style type="text/css">
    .reviewdoc {
        height: 100%;
        width: auto;
        display: inline-block;
        font-size: 22px;
        position: relative;
        margin: 0;
        line-height: 34px;
        font-weight: 400;
        letter-spacing: 0;
        color: #666;
    }

    tr.spaceUnder > td {
        padding-bottom: 1em;
    }
</style>

<div id="divReviewerComplianceDetailsDialog">

    <asp:UpdatePanel ID="upComplianceDetails1" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceDetails1_Load">
        <ContentTemplate>
            <div style="margin: 5px">
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Style="padding-left: 5%" class="alert alert-block alert-danger fade in" ValidationGroup="ReviewerComplianceValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" class="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                        ValidationGroup="ReviewerComplianceValidationGroup" Display="None" />
                    <asp:Label ID="Labelmsgrev" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
                    <asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
                    <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />
                </div>
                <div>
                    <asp:Label ID="Label2" Text="This is a  " Style="width: 300px; font-weight: bold; font-size: 13px; color: #333;"
                        maximunsize="300px" autosize="true" runat="server" />
                    <div id="divRiskType1" runat="server" class="circle"></div>
                    <asp:Label ID="lblRiskType1" Style="width: 300px; margin-left: -17px; font-weight: bold; font-size: 13px; color: #333;"
                        maximunsize="300px" autosize="true" runat="server" />
                </div>
                <div id="ActDetails1" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails1">
                                            <h2>Act Details</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails1"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div id="collapseActDetails1" class="collapse">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                            <table style="width: 100%;">
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Act Name</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblActName1" Style="width: 88%; font-size: 13px; color: #333;"
                                                            autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Section /Rule</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblRule1" Style="width: 88%; font-size: 13px; color: #333;"
                                                            autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                 <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold; vertical-align: top;">Act Document(s)</td>
                                                    <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                    <td style="width: 83%;">

                                                         <div style="width: 90%; margin-top: 15px;">
                                                            <%=ActReviewDocString%>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="ComplianceDetails1" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">

                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails1">
                                            <h2>Compliance Details</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails1"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="collapseComplianceDetails1" class="collapse">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                            <table style="width: 100%">
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Compliance Id</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblComplianceID1" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Short Description</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblComplianceDiscription1" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Detailed Description</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblDetailedDiscription1" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Penalty</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblPenalty1" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Frequency</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblFrequency1" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div id="OthersDetails1" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails1">
                                            <h2>Additional Details</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails1"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="collapseOthersDetails1" class="collapse">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                            <table style="width: 100%">
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Risk Type</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblRisk1" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Sample Form/Attachment</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">

                                                        <asp:UpdatePanel ID="upsample1" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Label ID="lblFormNumber1" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                                <asp:LinkButton ID="lbDownloadSample1" Style="width: 300px; font-size: 13px; color: blue"
                                                                    runat="server" Font-Underline="false" OnClick="lbDownloadSample1_Click" />
                                                                <asp:Label ID="lblSlash1" Text="/" Style="color: blue;" runat="server" />
                                                                <asp:LinkButton ID="lnkViewSampleForm1" Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                    runat="server" Font-Underline="false" OnClientClick="fopendocReviewfile();" />
                                                                <asp:Label ID="lblpathsample1" runat="server" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Regulatory website link</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:LinkButton ID="lnkSampleForm1" Text="" Style="width: 300px; font-size: 13px; color: blue"
                                                            runat="server" Font-Underline="false" OnClick="lnkSampleForm_Click" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Additional/Reference Text </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblRefrenceText1" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Location</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblLocation" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Period</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblPeriod" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 25%; font-weight: bold;">Due Date</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblDueDate" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                        <asp:HiddenField ID="hiddenReviewDueDate" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder" id="trAuditChecklist1" runat="server">
                                                    <td style="width: 25%; font-weight: bold;">Audit Checklist</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 73%;">
                                                        <asp:Label ID="lblAuditChecklist1" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div runat="server" id="divTask" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                        <h2>Main Task Details</h2>
                                    </a>
                                    <div class="panel-actions">
                                        <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                    </div>
                                </div>

                                <div id="collapseTaskSubTask" class="collapse in">
                                    <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                        <asp:GridView runat="server" ID="gridSubTaskReviewer" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                            AllowPaging="false" PageSize="50" CssClass="table" GridLines="None" BorderWidth="0px" DataKeyNames="TaskID"
                                            OnRowCommand="gridSubTaskReviewer_RowCommand" OnRowDataBound="gridSubTaskReviewer_RowDataBound" AutoPostBack="true">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                        <asp:Label ID="lblIsTaskClose" Visible="false" runat="server" Text='<%# Eval("IsTaskClose") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Task">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblTaskTitle" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Performer">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                            <asp:Label ID="lblPerformer" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'
                                                                Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reviewer">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                            <asp:Label ID="lblReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'
                                                                Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Due Date">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                            <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                        <asp:UpdatePanel ID="upSubTaskDownloadView" runat="server">
                                                            <ContentTemplate>
                                                                <asp:LinkButton ID="btnSubTaskDocDownload" runat="server" CommandName="Download" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="Download Documents" Text="Download" Style="color: blue;">
                                                                </asp:LinkButton>
                                                                <asp:Label ID="lblTaskSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                <asp:LinkButton CommandName="View" runat="server" ID="btnSubTaskDocView" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                    data-toggle="tooltip" data-placement="bottom" ToolTip="View Documents"
                                                                    Text="View" Style="width: 150px; font-size: 13px; color: blue" />
                                                                <asp:Label ID="CompDocReviewPath" runat="server" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnSubTaskDocDownload" />
                                                                <%-- <asp:PostBackTrigger ControlID="btnSubTaskDocView" />--%>
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Right" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="Step1Download" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Step1DownloadReview1">
                                            <%--<h2>Review Compliance Document</h2>--%>
                                            <asp:Label ID="lblReviewDoc" runat="server" CssClass="reviewdoc" Text="Review Compliance Document"></asp:Label>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#Step1DownloadReview1"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="Step1DownloadReview1" class="panel-collapse collapse in">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;" id="fieldsetdownloadreview" runat="server">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 15%; font-weight: bold;">Versions</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <table width="100%" style="text-align: left">
                                                            <thead>
                                                                <tr>
                                                                    <td valign="top">
                                                                        <asp:Repeater ID="rptComplianceVersion" runat="server" OnItemCommand="rptComplianceVersion_ItemCommand"
                                                                            OnItemDataBound="rptComplianceVersion_ItemDataBound">
                                                                            <HeaderTemplate>
                                                                                <table id="tblComplianceDocumnets">
                                                                                    <thead>
                                                                                        <%-- <th>Versions</th>--%>
                                                                                    </thead>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' ID="lblDocumentVersion"
                                                                                                    runat="server" Text='<%# Eval("Version")%>' Style="color: blue;"></asp:LinkButton></td>
                                                                                            <td>
                                                                                                <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' OnClientClick='javascript:enableControls1()'
                                                                                                    ID="btnComplinceVersionDoc" runat="server" Text="Download" Style="color: blue;">
                                                                                                </asp:LinkButton>
                                                                                                <asp:Label ID="lblSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                                                <asp:LinkButton CommandName="View" ID="lnkViewDoc" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                                                    runat="server" Font-Underline="false" />
                                                                                                <%--OnClientClick="fopendocfileReview();"--%>
                                                                                                <asp:Label ID="lblpathReviewDoc" runat="server" Style="display: none"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="btnComplinceVersionDoc" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </table>
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                                                            OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                                                            <HeaderTemplate>
                                                                                <table id="tblComplianceDocumnets">
                                                                                    <thead>
                                                                                        <th>Compliance Related Documents</th>
                                                                                    </thead>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:LinkButton
                                                                                            CommandArgument='<%# Eval("FileID")%>'
                                                                                            OnClientClick='javascript:enableControls1()'
                                                                                            ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                        </asp:LinkButton></td>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </table>
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                                        <asp:Repeater ID="rptWorkingFiles" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                                                            OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                                                            <HeaderTemplate>
                                                                                <table id="tblWorkingFiles">
                                                                                    <thead>
                                                                                        <th>Compliance Working Files</th>
                                                                                    </thead>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:LinkButton
                                                                                            CommandArgument='<%# Eval("FileID")%>'
                                                                                            ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                                                        </asp:LinkButton></td>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </table>
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                                    </td>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="Step1UpdateCompliance" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Step1UpdateComplianceStatus">
                                            <h2>Update Compliance Status </h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#Step1UpdateComplianceStatus"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div id="Step1UpdateComplianceStatus" class="panel-collapse collapse in">
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                        <div style="margin-bottom: 7px">
                                            <table style="width: 100%">
                                                <tr>
                                                    <label id="lblStatus1" runat="server" style="width: 200px; display: block; float: left; font-size: 13px; color: #333;" />
                                                    <td style="width: 21%;">
                                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                        <label style="font-weight: bold; vertical-align: text-top;">Status</label>
                                                    </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:UpdatePanel ID="upsample" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:HiddenField ID="hdnfieldPanaltyDisplay" runat="server" />
                                                                <asp:RadioButtonList ID="rdbtnStatus1" AutoPostBack="true" OnSelectedIndexChanged="rdbtnStatus1_SelectedIndexChanged" onchange="hideDiv1()" runat="server" RepeatDirection="Horizontal">
                                                                </asp:RadioButtonList>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <div style="margin-bottom: 7px" id="divDated1" runat="server">
                                                        <td style="width: 25%;">
                                                            <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                            <label style="font-weight: bold; vertical-align: text-top;">Date</label>
                                                        </td>
                                                        <td style="width: 2%; font-weight: bold;">: </td>
                                                        <td style="width: 83%;">
                                                            <asp:TextBox runat="server" ID="tbxDate1" ReadOnly="true" placeholder="DD-MM-YYYY" class="form-control" Style="width: 115px; cursor: text;" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Please select Date." ControlToValidate="tbxDate1"
                                                                runat="server" ID="RequiredFieldValidator3" ValidationGroup="ReviewerComplianceValidationGroup" Display="None" />
                                                        </td>
                                                    </div>
                                                </tr>
                                            </table>
                                            <div style="margin-bottom: 7px">
                                                <fieldset id="fieldsetpenalty" runat="server" style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                    <table style="width: 100%">
                                                        <tr id="trPenalty" runat="server">
                                                            <td style="width: 11%; font-weight: bold;">
                                                                <asp:CheckBox ID="chkPenaltySaveReview" class="clspenaltysave" onclick="fillValuesInTextBoxesReview1()"
                                                                    Text="Value is not known at this moment" runat="server" />
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;"></td>
                                                            <td style="width: 10%;"></td>

                                                            <td style="width: 9%;"></td>
                                                            <td style="width: 2%; font-weight: bold;"></td>
                                                            <td style="width: 10%;"></td>
                                                        </tr>

                                                        <tr>
                                                            <td style="width: 11%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Interest(INR) </label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 10%;">
                                                                <asp:TextBox runat="server" ID="txtInterestReview" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                    TargetControlID="txtInterestReview" />
                                                            </td>

                                                            <td style="width: 9%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Penalty Amount(INR) </label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 10%;">
                                                                <asp:TextBox runat="server" ID="txtPenaltyReview" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                    TargetControlID="txtPenaltyReview" />
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </fieldset>
                                                <br />

                                                <fieldset id="fieldsetTDS" runat="server" visible="false" style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                    <table style="width: 100%">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 11%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Nature of compliance </label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 10%;">
                                                                <asp:Label ID="lblNatureofcompliance" Style="width: 88%; font-size: 13px; color: #333;" autosize="true" runat="server" />
                                                                <asp:Label ID="lblNatureofcomplianceID" Visible="false" runat="server" />
                                                            </td>
                                                            <td style="width: 9%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label id="lblValueAsPerSystem" runat="server" style="font-weight: bold; vertical-align: text-top;">Liability as per system (A) </label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 10%;">
                                                                <asp:TextBox runat="server" ID="txtValueAsPerSystem" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                    TargetControlID="txtValueAsPerSystem" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 11%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label id="lblValueAsPerReturn" runat="server" style="font-weight: bold; vertical-align: text-top;">Liability as per return (B) </label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 10%;">
                                                                <asp:TextBox runat="server" ID="txtValueAsPerReturn" onkeyup="Diff1();" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                    TargetControlID="txtValueAsPerReturn" />
                                                            </td>
                                                            <td style="width: 9%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label id="lbldiffAB" runat="server" style="font-weight: bold; vertical-align: text-top;">Difference (A)-(B) </label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">:</td>
                                                            <td style="width: 10%;">
                                                                <asp:TextBox runat="server" ID="txtDiffAB" ReadOnly="true" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                            </td>
                                                        </tr>

                                                        <tr id="trreturn" runat="server" class="spaceUnder">
                                                            <td style="width: 11%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label id="lblLiabilityPaid" runat="server" style="font-weight: bold; vertical-align: text-top;">Liability Paid (C) </label>
                                                            </td>
                                                            <td id="tdLiabilityPaidSC" runat="server" style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 10%;">
                                                                <asp:TextBox ID="txtLiabilityPaid" runat="server" onkeyup="Diff1();" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterType="Custom,Numbers" ValidChars="."
                                                                    TargetControlID="txtLiabilityPaid" />
                                                            </td>
                                                            <td style="width: 9%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label id="lbldiffBC" runat="server" style="font-weight: bold; vertical-align: text-top;">Difference (B)-(C) </label>
                                                            </td>
                                                            <td id="tddiffBCSC" runat="server" style="width: 2%; font-weight: bold;">:</td>
                                                            <td style="width: 10%;">
                                                                <asp:TextBox runat="server" ID="txtDiffBC" ReadOnly="true" class="form-control" Style="width: 115px; margin-left: -10%;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>

                                            </div>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 25%;">
                                                          <label id="remarkcompulsory" style="width: 10px; display: none; float: left; font-size: 13px; color: red;">*</label>
                                                        <label style="font-weight: bold; vertical-align: text-top;">Remarks</label>
                                                    </td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:TextBox runat="server" ID="tbxRemarks1" TextMode="MultiLine" class="form-control" Rows="2" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <div style="margin-bottom: 7px; margin-left: 34%; margin-top: 10px;">
                                                <asp:Button Text="Approve" runat="server" ID="btnSave1" OnClientClick="javascript:return PenaltyValidateReview1()" OnClick="btnSave1_Click" CssClass="btn btn-search"
                                                    ValidationGroup="ReviewerComplianceValidationGroup" />
                                                <asp:Button Text="Reject" runat="server" ID="btnReject1" Style="margin-left: 15px" OnClientClick="javascript:return RejectRemark()" OnClick="btnReject1_Click" CssClass="btn btn-search"
                                                    ValidationGroup="ReviewerComplianceValidationGroup" CausesValidation="false" />
                                                <asp:Button Text="Close" runat="server" OnClientClick="fcloseandcallcal();" Style="margin-left: 15px" ID="Button3" CssClass="btn btn-search" data-dismiss="modal" />
                                            </div>
                                        </div>
                                        </label>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="Log1" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#AuditLog1">
                                            <h2>Audit Log</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#AuditLog1"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div id="AuditLog1" class="collapse">
                                    <div runat="server" id="log" style="text-align: left;">
                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false" AllowSorting="true"
                                                    AllowPaging="true" PageSize="5" GridLines="none" OnPageIndexChanging="grdTransactionHistory_OnPageIndexChanging"
                                                    OnRowCreated="grdTransactionHistory_RowCreated" BorderWidth="0px" CssClass="table" OnSorting="grdTransactionHistory_Sorting"
                                                    DataKeyNames="ComplianceTransactionID" OnRowCommand="grdTransactionHistory_RowCommand">
                                                    <Columns>
                                                        <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />
                                                        <asp:TemplateField HeaderText="Date">
                                                            <ItemTemplate>
                                                                <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Interest" HeaderText="Interest" />
                                                        <asp:BoundField DataField="Penalty" HeaderText="Penalty" />
                                                        <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                                        <asp:BoundField DataField="Status" HeaderText="Status" />
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" />
                                                    <PagerTemplate>
                                                        <table style="display: none">
                                                            <tr>
                                                                <td>
                                                                    <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </PagerTemplate>
                                                </asp:GridView>
                                            </div>
                                        </fieldset>
                                        <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-size: 10px; color: #666;" Visible="false"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                <asp:Button ID="btnDownload" runat="server" Style="display: none" OnClick="btnDownload_Click" />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDownload" />
            <asp:PostBackTrigger ControlID="btnSave1" />
            <asp:PostBackTrigger ControlID="lbDownloadSample1" />
            <%--<asp:PostBackTrigger ControlID="txtInterestReview" />--%>
            <asp:AsyncPostBackTrigger ControlID="txtInterestReview" />
            <%-- <asp:PostBackTrigger ControlID="rdbtnStatus1"/>--%>
        </Triggers>
    </asp:UpdatePanel>


</div>

<div>
    <div class="modal fade" id="DocumentReviewPopUp1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
        <div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="height: 570px;">
                    <div style="width: 100%;">
                        <div style="float: left; width: 10%">
                            <table width="100%" style="text-align: left; margin-left: 5%;">
                                <thead>
                                    <tr>
                                        <td valign="top">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rptComplianceVersionView" runat="server" OnItemCommand="rptComplianceVersionView_ItemCommand"
                                                        OnItemDataBound="rptComplianceVersionView_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="tblComplianceDocumnets">
                                                                <thead>
                                                                    <th>Versions</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblDocumentVersionView"
                                                                                runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="lblDocumentVersionView" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="rptComplianceVersionView" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div style="float: left; width: 90%">
                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="lblMessageReviewer1" runat="server" Style="color: red;"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                <iframe src="about:blank" id="docViewerReviewAll" runat="server" width="100%" height="535px"></iframe>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%--<div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="docViewerReviewAll" runat="server" width="100%" height="550px"></iframe>
                </div>
            </div>
        </div>--%>
    </div>
</div>


<div class="modal fade" id="SampleFileReviewPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
    <div class="modal-dialog" style="width: 100%;">
        <div class="modal-content">
            <div class="modal-header">
                <%-- data-dismiss-modal="modal2"--%>
                <button type="button" class="close" onclick="$('#SampleFileReviewPopUp').modal('hide');" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="height: 570px;">
                <%-- <div style="width: 100%;">--%>
                <div style="float: left; width: 10%">
                    <table width="100%" style="text-align: left; margin-left: 5%;">
                        <thead>
                            <tr>
                                <td valign="top">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdatleMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Repeater ID="rptComplianceSampleView1" runat="server" OnItemCommand="rptComplianceSampleView1_ItemCommand"
                                                OnItemDataBound="rptComplianceSampleView1_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table id="tblComplianceDocumnets1">
                                                        <thead>
                                                            <th>Sample Forms</th>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("ComplianceID") %>' ID="lblSampleView"
                                                                        runat="server" ToolTip='<%# Eval("Name")%>' Text='<%# Eval("Name") %>'></asp:LinkButton>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="lblSampleView" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="rptComplianceSampleView1" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div style="float: left; width: 90%">
                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                        <iframe src="about:blank" id="docViewerAll" runat="server" width="100%" height="550px"></iframe>
                    </fieldset>
                </div>
                <%-- </div>--%>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="modal fade" id="modalDocumentReviewerViewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
        <div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="height: 570px;">
                    <div style="width: 100%;">
                        <div style="float: left; width: 10%">
                            <table width="100%" style="text-align: left; margin-left: 5%;">
                                <thead>
                                    <tr>
                                        <td valign="top">
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rptTaskVersionView" runat="server" OnItemCommand="rptTaskVersionView_ItemCommand">
                                                        <HeaderTemplate>
                                                            <table id="tblComplianceDocumnets">
                                                                <thead>
                                                                    <th>Versions</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblTaskDocumentVersionView"
                                                                                runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="lblTaskDocumentVersionView" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="rptTaskVersionView" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div style="float: left; width: 90%">
                            <asp:Label runat="server" ID="lblMessagetask" Style="color: red;"></asp:Label>
                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                <iframe src="about:blank" id="docTaskViewerReviewAll" runat="server" width="100%" height="535px"></iframe>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


    //$(function () {
    //    $('#divComplianceDetailsDialog').dialog({
    //        height: 600,
    //        width: 800,
    //        autoOpen: false,
    //        draggable: true,
    //        title: "Change Status",
    //        open: function (type, data) {
    //            $(this).parent().appendTo("form");
    //        }
    //    });

    //});

    function initializeDatePicker(date) {
        var startDate = new Date();
        $('#<%= tbxDate1.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: startDate,
            numberOfMonths: 1,
        });

        if (date != null) {
            $("#<%= tbxDate1.ClientID %>").datepicker("option", "defaultDate", date);
        }
    }

    function initializeDatePickerOverDueReview(dateOverDue) {
        $('#<%= tbxDate1.ClientID %>').datepicker('destroy');
        $('#<%= tbxDate1.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: dateOverDue,
            numberOfMonths: 1,
        });

        <%--if (dateOverDue != null) {
            $("#<%= tbxDate1.ClientID %>").datepicker("option", "defaultDate", dateOverDue);
        }--%>
    }

    function initializeDatePickerInTimeReview(dateInTime) {
        $('#<%= tbxDate1.ClientID %>').datepicker('destroy');
       $('#<%= tbxDate1.ClientID %>').datepicker({
           dateFormat: 'dd-mm-yy',
           maxDate: dateInTime,
           numberOfMonths: 1,
       });

       <%--if (dateInTime != null) {
           $("#<%= tbxDate1.ClientID %>").datepicker("option", "maxDate", dateInTime);
        }--%>
    }
    function enableControls1() {
        $("#<%= btnSave1.ClientID %>").removeAttr("disabled");
        $("#<%= rdbtnStatus1.ClientID %>").removeAttr("disabled");
        $("#<%= tbxRemarks1.ClientID %>").removeAttr("disabled");
        $("#<%= tbxDate1.ClientID %>").removeAttr("disabled");
        $("#<%= btnReject1.ClientID %>").removeAttr("disabled");
    }

    function fopendocfileReviewReviewer(file) {
        $('#DocumentReviewPopUp1').modal('show'); enableControls1();
        $('#ContentPlaceHolder1_udcReviewerStatusTranscatopn_docViewerReviewAll').attr('src', "../docviewer.aspx?docurl=" + file);
    }

    function fopenSampleFileReviewer(file) {
        $('#ContentPlaceHolder1_udcReviewerStatusTranscatopn_docViewerAll').attr('src', "../docviewer.aspx?docurl=" + file);
    }

    function fopendocReviewfile() {
        $('#SampleFileReviewPopUp').modal('show'); 
        $('#ContentPlaceHolder1_udcReviewerStatusTranscatopn_docViewerAll').attr('src', "../docviewer.aspx?docurl=" + $("#<%= lblpathsample1.ClientID %>").text());
    }

    function fopendoctaskfileReview1(file) {
        $('#modalDocumentReviewerViewer').modal('show');
        $('#ContentPlaceHolder1_udcReviewerStatusTranscatopn_docTaskViewerReviewAll').attr('src', "../docviewer.aspx?docurl=" + file);
    }
    function fopendoctaskfileReview1PopUp() {
        $('#modalDocumentReviewerViewer').modal('show');
    }
    function fopendocfileReviewReviewerPopUp() {
        enableControls1();
        $('#DocumentReviewPopUp1').modal('show');
    }

    $(document).ready(function () {
        $("button[data-dismiss-modal=modal2]").click(function () {
            $('#DocumentReviewPopUp1').modal('hide');
            $('#modalDocumentReviewerViewer').modal('hide');
            $('#SampleFileReviewPopUp').modal('hide');
            $('#divActFilePopUp').modal('hide');
        });

    });

    function PenaltyValidateReview1() {
        var RB1 = document.getElementById("<%=rdbtnStatus1.ClientID%>");
         var radio = RB1.getElementsByTagName("input");
         var isChecked = false;
         for (var i = 0; i < radio.length; i++) {
             if (radio[i].checked) {
                 isChecked = true;
                 break;
             }
         }
         if (isChecked == false) {
             $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_Labelmsgrev").css('display', 'block');
             $('#ContentPlaceHolder1_udcReviewerStatusTranscatopn_Labelmsgrev').text("Please select Status.");
             $('#ContentPlaceHolder1_udcReviewerStatusTranscatopn_ValidationSummary1').IsValid = false;
             document.getElementById("ContentPlaceHolder1_udcReviewerStatusTranscatopn_ValidationSummary1").value = "Please select Status.";
             return false;
         }
         var ispenaltyDisplay = document.getElementById("<%=hdnfieldPanaltyDisplay.ClientID%>").value;
        if (ispenaltyDisplay == "true") {
            if (document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_rdbtnStatus1_0') != null) {
                if (document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_rdbtnStatus1_0').checked) {
                    {
                        var checkedRadio = $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_rdbtnStatus1 input[type=radio]:checked");
                        if (checkedRadio.length > 0) {
                            var selectedText = checkedRadio.next().html();
                            if (selectedText == "Closed-Delayed") {

                                var chk = $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_chkPenaltySaveReview").is(":checked");
                                if (chk == false) {
                                    var txtInterestReview = $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtInterestReview").val();
                                    var txtPenaltyReview = $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtPenaltyReviewReview").val();
                                    if (txtInterestReview == "" || txtPenaltyReview == "" || txtInterestReview == "" || txtPenaltyReview == "0") {
                                        $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_Labelmsgrev").css('display', 'block');
                                        $('#ContentPlaceHolder1_udcReviewerStatusTranscatopn_Labelmsgrev').text("Please enter interest and penalty");
                                        $('#ContentPlaceHolder1_udcReviewerStatusTranscatopn_ValidationSummary1').IsValid = false;
                                        document.getElementById("ContentPlaceHolder1_udcReviewerStatusTranscatopn_ValidationSummary1").value = "Please enter interest and penalty";
                                        return false;
                                    }
                                }

                                if (!$.trim($("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_tbxRemarks1").val())) {
                                    $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_Labelmsgrev").css('display', 'block');
                                    $('#ContentPlaceHolder1_udcReviewerStatusTranscatopn_Labelmsgrev').text("Please enter remark");
                                    $('#ContentPlaceHolder1_udcReviewerStatusTranscatopn_ValidationSummary1').IsValid = false;
                                    document.getElementById("ContentPlaceHolder1_udcReviewerStatusTranscatopn_ValidationSummary1").value = "Please enter remark";
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    function RejectRemark() {
        if (!$.trim($("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_tbxRemarks1").val())) {
            $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_Labelmsgrev").css('display', 'block');
            $('#ContentPlaceHolder1_udcReviewerStatusTranscatopn_Labelmsgrev').text("Please enter remark");
            $('#ContentPlaceHolder1_udcReviewerStatusTranscatopn_ValidationSummary1').IsValid = false;
            document.getElementById("ContentPlaceHolder1_udcReviewerStatusTranscatopn_ValidationSummary1").value = "Please enter remark";
            return false;
        }
        
    }

    function hideDiv1() {
        $('#remarkcompulsory').css('display', 'none');
        if (document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_rdbtnStatus1_0') != null) {
            if (document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_rdbtnStatus1_0').checked) {
                var checkedRadio = $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_rdbtnStatus1 input[type=radio]:checked");
                if (checkedRadio.length > 0) {
                    var selectedText = checkedRadio.next().html();
                    if (selectedText == "Closed-Delayed") {
                        $('#remarkcompulsory').css('display', 'block');
                    }
                    else {
                        $('#remarkcompulsory').css('display', 'none');
                    }
                }
            }
        }

        try
        {
            var ispenaltyDisplay = document.getElementById("<%=hdnfieldPanaltyDisplay.ClientID%>").value;
            if (ispenaltyDisplay == "true") {
                if (document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_fieldsetpenalty') != null) {
                    document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_fieldsetpenalty').style.display = 'none';
                }

                if (document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_rdbtnStatus1_1') != null) {
                    if (document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_rdbtnStatus1_1').checked) {
                        var checkedRadio = $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_rdbtnStatus1 input[type=radio]:checked");
                        if (checkedRadio.length > 0) {
                            var selectedText = checkedRadio.next().html();
                            if (selectedText == "Closed-Timely") {
                                $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_chkPenaltySaveReview").prop('checked', false);
                            }
                        }
                    }
                }

                if (document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_rdbtnStatus1_0') != null) {
                    if (document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_rdbtnStatus1_0').checked) {
                        var checkedRadio = $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_rdbtnStatus1 input[type=radio]:checked");
                        var chk = $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_chkPenaltySaveReview").is(":checked");
                        if (checkedRadio.length > 0) {
                            var selectedText = checkedRadio.next().html();
                            if (selectedText == "Closed-Delayed" && chk == false) {
                                $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtInterestReview").attr("disabled", false);
                                $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtPenaltyReview").attr("disabled", false);
                                document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_fieldsetpenalty').style.display = 'block';
                            }
                            else {
                                document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_fieldsetpenalty').style.display = 'block';
                                $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtInterestReview").attr("disabled", true);
                                $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtPenaltyReview").attr("disabled", true);
                            }
                        }
                    }
                    else {
                        document.getElementById("ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtInterestReview").value = "0";
                        document.getElementById("ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtPenaltyReview").value = "0";
                    }
                }
            }
            else {
                document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_fieldsetpenalty').style.display = 'none';
            }
        }
        catch(err)
        {
        }
    }

    function fillValuesInTextBoxesReview1() {
        var chk = $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_chkPenaltySaveReview").is(":checked");
        if (chk == true) {
            document.getElementById("ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtInterestReview").value = "0";
            document.getElementById("ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtPenaltyReview").value = "0";
            $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtInterestReview").attr("disabled", true);
            $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtPenaltyReview").attr("disabled", true);
        }
        else {
            document.getElementById("ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtInterestReview").value = "";
            document.getElementById("ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtPenaltyReview").value = "";
            $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtInterestReview").attr("disabled", false);
            $("#ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtPenaltyReview").attr("disabled", false);
        }
    }

    function Diff1() {
        document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtDiffAB').value = "";
        var ValueAsPerSystem = document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtValueAsPerSystem').value;
        var ValueAsPerReturn = document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtValueAsPerReturn').value;
        var AB = parseInt(ValueAsPerSystem) - parseInt(ValueAsPerReturn);
        if (!isNaN(AB)) {
            document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtDiffAB').value = AB;
        }
        else {
            document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtDiffAB').value = "";
        }
        document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtDiffBC').value = ""
        var LiabilityPaid = document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtLiabilityPaid').value;
        var BC = parseInt(ValueAsPerReturn) - parseInt(LiabilityPaid);
        if (!isNaN(BC)) {
            document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtDiffBC').value = BC;
        }
        else {
            document.getElementById('ContentPlaceHolder1_udcReviewerStatusTranscatopn_txtDiffBC').value = "";
        }
    }

    function fopendocfileReviewerAct(file) {

        $('#divActRviewerFilePopUp').modal('show');
        $('#ContentPlaceHolder1_udcReviewerStatusTranscatopn_IframeActReviewerFile').attr('src', "../docviewer.aspx?docurl=" + file);
    }

    function fopendocfileReviewerActPopUp() {
        $('#divActRviewerFilePopUp').modal('show');
        $('#ContentPlaceHolder1_udcReviewerStatusTranscatopn_IframeActReviewerFile').attr('src', "../docviewer.aspx?docurl=");
    }
</script>

