﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Globalization;
using System.Configuration;
using System.IO;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class CustomerDetailsControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ChkSp();
                    BindCustomerStatus();
                    BindLocationType();
                    BindServiceProvider();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }            
        }
        public void BindLocationType()
        {
            try
            {
                ddlLocationType.DataTextField = "Name";
                ddlLocationType.DataValueField = "ID";
                ddlLocationType.Items.Clear();
                ddlLocationType.DataSource = ProcessManagement.FillLocationType();
                ddlLocationType.DataBind();
                ddlLocationType.Items.Insert(0, new ListItem("< All >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void ChkSp()
        {
            if (chkSp.Checked == true)
            {
                lblast.Visible = false;
                lblsp.Visible = false;
                ddlSPName.Visible = false;
            }
            else
            {
                lblast.Visible = true;
                lblsp.Visible = true;
                ddlSPName.Visible = true;
            }
        }
        public void BindServiceProvider()
        {
            try
            {
                int serviceProviderID = -1;
                if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                ddlSPName.DataTextField = "Name";
                ddlSPName.DataValueField = "ID";
                ddlSPName.Items.Clear();
                ddlSPName.DataSource = CustomerManagement.FillServiceProvider(serviceProviderID);
                ddlSPName.DataBind();               
                ddlSPName.Items.Insert(0, new ListItem("< Select Service Provider >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void EditCustomerInformation(int customerID)
        {
            try
            {
                lblErrorMassage.Text = "";
                ViewState["Mode"] = 1;
                ViewState["CustomerID"] = customerID;

                Customer customer = CustomerManagement.GetByID(customerID);
                tbxName.Text = customer.Name;
                tbxAddress.Text = customer.Address;
                tbxBuyerName.Text = customer.BuyerName;
                tbxBuyerContactNo.Text = customer.BuyerContactNumber;
                tbxBuyerEmail.Text = customer.BuyerEmail;
                txtStartDate.Text = customer.StartDate != null ? customer.StartDate.Value.ToString("dd-MM-yyyy") : " ";
                txtEndDate.Text = customer.EndDate != null ? customer.EndDate.Value.ToString("dd-MM-yyyy") : " ";
                txtDiskSpace.Text = customer.DiskSpace;
                if (!string.IsNullOrEmpty(customer.LogoPath))
                {
                    lblLogoName.Text = customer.LogoPath.Replace("~/UserPhotos/", "");
                }
                else
                {
                    lblLogoName.Text = string.Empty;
                }
                if (customer.LocationType != null)
                {
                    ddlLocationType.SelectedValue = Convert.ToString(customer.LocationType);
                }
                if (customer.IComplianceApplicable != null)
                {
                    ddlComplianceApplicable.SelectedValue = Convert.ToString(customer.IComplianceApplicable);
                }
                if (customer.TaskApplicable != null)
                {
                    ddlTaskApplicable.SelectedValue = Convert.ToString(customer.TaskApplicable);
                }
                if (customer.IsLabelApplicable != null)
                {
                    ddlLabelApplicable.SelectedValue = Convert.ToString(customer.IsLabelApplicable);
                }

                if (customer.ComplianceProductType != null)
                {
                    if (ddlComplianceProductType.Items.FindByValue(Convert.ToString(customer.ComplianceProductType)) != null)
                    {
                        ddlComplianceProductType.ClearSelection();
                        ddlComplianceProductType.Items.FindByValue(Convert.ToString(customer.ComplianceProductType)).Selected = true;
                    }                        
                }
                if (customer.DocManNonMan != null)
                {
                    ddlChecklistDocFlow.SelectedValue = Convert.ToString(customer.DocManNonMan);
                }
                if (customer.Status != null)
                {
                    ddlCustomerStatus.SelectedValue = Convert.ToString(customer.Status);
                }
                if (customer.IsServiceProvider == true)
                {
                    chkSp.Checked = true;
                    ChkSp();
                    customer.ServiceProviderID = null;
                }
                else
                {
                    BindServiceProvider();
                    chkSp.Checked = false;
                    ChkSp();
                    ddlSPName.SelectedValue = Convert.ToString(customer.ServiceProviderID);
                }
                upCustomers.Update();
                ScriptManager.RegisterStartupScript(this.upCustomers, this.upCustomers.GetType(), "OpenDialog", "$(\"#divCustomersDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void AddCustomer()
        {
            try
            {
                lblErrorMassage.Text = string.Empty;
                ViewState["Mode"] = 0;
                chkSp.Checked = false;
                tbxName.Text = tbxAddress.Text = tbxBuyerName.Text = tbxBuyerContactNo.Text = tbxBuyerEmail.Text = txtStartDate.Text = txtEndDate.Text = txtDiskSpace.Text = string.Empty;
                ddlCustomerStatus.SelectedIndex = -1;
                upCustomers.Update();
                ScriptManager.RegisterStartupScript(this.upCustomers, this.upCustomers.GetType(), "OpenDialog", "$(\"#divCustomersDialog\").dialog('open');initializeDatePicker(null); ", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }      
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Customer customer = new Customer();
                customer.Name = tbxName.Text.Trim();
                customer.Address = tbxAddress.Text;                
                customer.BuyerName = tbxBuyerName.Text;
                customer.BuyerContactNumber = tbxBuyerContactNo.Text;
                customer.BuyerEmail = tbxBuyerEmail.Text;
                string strtdate = Request[txtStartDate.UniqueID].ToString().Trim();
                string enddate = Request[txtEndDate.UniqueID].ToString().Trim();
                if (strtdate != "" && enddate != "")
                {
                    customer.StartDate = DateTime.ParseExact(strtdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    customer.EndDate = DateTime.ParseExact(enddate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                customer.DiskSpace = txtDiskSpace.Text;
                if ((int)ViewState["Mode"] == 1)
                {
                    customer.ID = Convert.ToInt32(ViewState["CustomerID"]);
                }
                if (CustomerManagement.Exists(customer))
                {
                    cvDuplicateEntry.ErrorMessage = "Customer name already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }
                if(chkSp.Checked==false)
                {
                   if(ddlSPName.SelectedIndex == 0)
                    {
                        cvDuplicateEntry.ErrorMessage = "Please Select Service Provider Name.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }                   
                }
                customer.LocationType = Convert.ToInt32(ddlLocationType.SelectedValue);
                customer.Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue);
                customer.IComplianceApplicable = Convert.ToInt32(ddlComplianceApplicable.SelectedValue);
                customer.TaskApplicable = Convert.ToInt32(ddlTaskApplicable.SelectedValue);
                customer.IsLabelApplicable = Convert.ToInt32(ddlLabelApplicable.SelectedValue);
                customer.DocManNonMan = Convert.ToInt32(ddlChecklistDocFlow.SelectedValue);               
                customer.IsServiceProvider= chkSp.Checked;
                customer.CreatedBy = AuthenticationHelper.UserID;                
                if (chkSp.Checked == true)
                {
                    customer.IsServiceProvider = true;
                    customer.ServiceProviderID = null;
                }
                else
                {
                    customer.ParentID = Convert.ToInt32(ddlSPName.SelectedValue);
                    customer.IsServiceProvider = false;
                    customer.ServiceProviderID = Convert.ToInt32(ddlSPName.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlComplianceProductType.SelectedValue))
                {
                    customer.ComplianceProductType = Convert.ToInt32(ddlComplianceProductType.SelectedValue);
                }
                else
                    customer.ComplianceProductType = 0;

                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_Customer mstCustomer = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_Customer();
                mstCustomer.Name = tbxName.Text.Trim();
                mstCustomer.Address = tbxAddress.Text;
                mstCustomer.BuyerName = tbxBuyerName.Text;
                mstCustomer.BuyerContactNumber = tbxBuyerContactNo.Text;
                mstCustomer.BuyerEmail = tbxBuyerEmail.Text;
                if (strtdate != "" && enddate != "")
                {
                    mstCustomer.StartDate = DateTime.ParseExact(strtdate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    mstCustomer.EndDate = DateTime.ParseExact(enddate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                if ((int)ViewState["Mode"] == 1)
                {
                    mstCustomer.ID = Convert.ToInt32(ViewState["CustomerID"]);
                }
                if (CustomerManagementRisk.Exists(mstCustomer))
                {
                    cvDuplicateEntry.ErrorMessage = "Customer name already exists.";
                    cvDuplicateEntry.IsValid = false;
                    return;
                }               
                mstCustomer.LocationType = Convert.ToInt32(ddlLocationType.SelectedValue);
                mstCustomer.Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue);
                mstCustomer.IComplianceApplicable = Convert.ToInt32(ddlComplianceApplicable.SelectedValue);
                mstCustomer.TaskApplicable = Convert.ToInt32(ddlTaskApplicable.SelectedValue);
                mstCustomer.IsLabelApplicable = Convert.ToInt32(ddlLabelApplicable.SelectedValue);
                mstCustomer.DocManNonMan = Convert.ToInt32(ddlChecklistDocFlow.SelectedValue);
                

                mstCustomer.IsServiceProvider = chkSp.Checked; 
                if (chkSp.Checked == true)
                {
                    mstCustomer.IsServiceProvider = true;
                    mstCustomer.ServiceProviderID = null;
                }
                else
                {
                    mstCustomer.ParentID = Convert.ToInt32(ddlSPName.SelectedValue);
                    mstCustomer.IsServiceProvider = false;
                    mstCustomer.ServiceProviderID = Convert.ToInt32(ddlSPName.SelectedValue);
                }
                 
                if (!string.IsNullOrEmpty(ddlComplianceProductType.SelectedValue))
                {
                    mstCustomer.ComplianceProductType = Convert.ToInt32(ddlComplianceProductType.SelectedValue);
                }
                else
                    mstCustomer.ComplianceProductType = 0;

                if ((int)ViewState["Mode"] == 0)
                {
                      bool resultDataChk = false;
                    int resultData = 0;  
                    bool fileupload = true;
                    if (UserImageUpload.HasFile)
                    {
                        string[] ExtList = { ".pptx",".docx",".pdf",".ppt",".word",".exe", ".bat", ".dll", ".docx", ".xlsx", ".html", ".css", ".js", ".txt", ".doc", ".gif", ".jsp",
                                        ".php5",".pht",".phtml",".shtml",".asa",".cer",".asax",".swf",".xap",".aspx",".asp",".zip",".rar",".php",".reg",".rdp" };
                        var fileExtention = Path.GetExtension(UserImageUpload.PostedFile.FileName);
                        if (!ExtList.Contains(fileExtention))
                        {
                            string fileName = customer.ID + "-" + customer.Name + Path.GetExtension(UserImageUpload.PostedFile.FileName);
                            var filePathcheck = Server.MapPath("~/UserPhotos/" + fileName);
                            if (File.Exists(filePathcheck))
                            {
                                File.Delete(filePathcheck);
                            }
                            UserImageUpload.PostedFile.SaveAs(Server.MapPath("~/UserPhotos/") + fileName);
                            string filepath = "~/UserPhotos/" + fileName;
                            customer.LogoPath = filepath;
                            mstCustomer.LogoPath = filepath;
                        }
                        else
                        {
                            fileupload = false;
                            ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "alert('Invalid file extension. format not supported.');", true);                         
                            return;
                        }
                    }
                    if (fileupload)
                    {
                        resultData = CustomerManagement.Create(customer);
                    }                 
                    if (resultData > 0)
                    {
                        resultDataChk = CustomerManagementRisk.Create(mstCustomer);
                        if (resultDataChk == false)
                        {
                            CustomerManagement.deleteCustReset(resultData);
                        }
                    }
                   

                    // Added by SACHIN 28 April 2016
                    string ReplyEmailAddressName = "Avantis";                    

                    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NewCustomerCreaded
                                        .Replace("@NewCustomer", customer.Name)
                                        .Replace("@LoginUser", AuthenticationHelper.User)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    ;
                  
                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                    string CustomerCreatedEmail = ConfigurationManager.AppSettings["CustomerCreatedEmail"].ToString();               
                    EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { CustomerCreatedEmail }), null, null, "AVACOM customer account created.", message);

                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    bool CheckFile = true;
                    if (UserImageUpload.HasFile)
                    {
                        string[] ExtList = { ".pptx",".docx",".pdf",".ppt",".word",".exe", ".bat", ".dll", ".docx", ".xlsx", ".html", ".css", ".js", ".txt", ".doc", ".gif", ".jsp",
                                ".php5",".pht",".phtml",".shtml",".asa",".cer",".asax",".swf",".xap",".aspx",".asp",".zip",".rar",".php",".reg",".rdp" };
                        var fileExtention = Path.GetExtension(UserImageUpload.PostedFile.FileName);
                        if (!ExtList.Contains(fileExtention))
                        {
                            string fileName = customer.ID + "-" + customer.Name + Path.GetExtension(UserImageUpload.PostedFile.FileName);
                            var filePathcheck = Server.MapPath("~/UserPhotos/" + fileName);
                            if (File.Exists(filePathcheck))
                            {
                                File.Delete(filePathcheck);
                            }
                            UserImageUpload.PostedFile.SaveAs(Server.MapPath("~/UserPhotos/") + fileName);
                            string filepath = "~/UserPhotos/" + fileName;
                            UserManagement.UpdateCustomerLogo(Convert.ToInt32(customer.ID), filepath);                        
                        }
                        else
                        {
                            CheckFile = false;
                            ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "alert('Invalid file extension. format not supported.');", true);                        
                            return;
                        }
                    }

                    if (CheckFile)
                    {
                        CustomerManagement.Update(customer);
                        CustomerManagementRisk.Update(mstCustomer);//added by rahul on 18 April 2016
                    }
                    else
                    {
                        CustomerManagement.Update(customer);
                        CustomerManagementRisk.Update(mstCustomer);//added by rahul on 18 April 2016
                    }
                   
                }

                //Add entry in RLCS corporate mapping
                CustomerManagement.AddCustomerCorporateMapping(customer.ID);

                ScriptManager.RegisterStartupScript(this.upCustomers, this.upCustomers.GetType(), "CloseDialog", "$(\"#divCustomersDialog\").dialog('close')", true);
                if (OnSaved != null)
                {
                    OnSaved(this, null);
                }

                if (customer.ComplianceProductType == 2 || customer.ComplianceProductType == 4)
                {
                    Business.Data.ProductMapping HRproductmapping = new Business.Data.ProductMapping()
                    {
                        CustomerID = customer.ID,
                        ProductID = 9,
                        IsActive = false,
                        CreatedOn = DateTime.Now,
                        CreatedBy = AuthenticationHelper.UserID
                    };
                    Business.RLCS.RLCS_Master_Management.CreateUpdate_ProductMapping_ComplianceDB(HRproductmapping);
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);

                BindServiceProvider();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void upCustomers_Load(object sender, EventArgs e)
        {
            try
            {

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date) || DateTime.TryParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeCombobox", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// This method for binding customer status.
        /// </summary>
        public void BindCustomerStatus()
        {
            try
            {
                ddlCustomerStatus.DataTextField = "Name";
                ddlCustomerStatus.DataValueField = "ID";

                ddlCustomerStatus.DataSource = Enumerations.GetAll<CustomerStatus>();
                ddlCustomerStatus.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public event EventHandler OnSaved;

        protected void chkSp_CheckedChanged(object sender, EventArgs e)
        {
            ChkSp();
        }
    }
}