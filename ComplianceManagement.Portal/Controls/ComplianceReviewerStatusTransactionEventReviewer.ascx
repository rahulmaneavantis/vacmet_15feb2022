﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComplianceReviewerStatusTransactionEventReviewer.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.ComplianceReviewerStatusTransactionEventReviewer" %>

<script type="text/javascript">
    $(function () {
        $('#divReviewerComplianceDetailsDialog').dialog({
            height: 680,
            width: 800,
            autoOpen: false,
            draggable: true,
            title: "Change Status",
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });

    });

    function initializeDatePicker(date) {
        var startDate = new Date();
        $('#<%= tbxDate.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: startDate,
            numberOfMonths: 1
        });

        if (date != null) {
            $("#<%= tbxDate.ClientID %>").datepicker("option", "defaultDate", date);
        }
    }

    function enableControls() {
        $("#<%= btnSave.ClientID %>").removeAttr("disabled");
        $("#<%= rdbtnStatus.ClientID %>").removeAttr("disabled");
        $("#<%= tbxRemarks.ClientID %>").removeAttr("disabled");
        $("#<%= tbxDate.ClientID %>").removeAttr("disabled");
        $("#<%= btnReject.ClientID %>").removeAttr("disabled");
    }

    function initializeJQueryUIReviewer() {
    }
    function initializeJQueryUI() {
    }

</script>

<div id="divReviewerComplianceDetailsDialog" style="display: none">

    <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceDetails_Load">
        <ContentTemplate>
            <div style="margin: 5px">
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="ReviewerComplianceValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="ReviewerComplianceValidationGroup" Display="None" />
                    <asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
                    <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />
                </div>
                <div style="display: none;">
                    <fieldset style="border-style: solid; border-width: 1px; border-color: gray; width: 735px;">
                        <legend>Act Details</legend>
                        <table style="width: 735px;">
                            <tr>
                                <td style="width: 110px;">Act Name</td>
                                <td style="width: 3px;">: </td>
                                <td style="width: 622px;">
                                    <asp:Label ID="lblActName" Style="width: 300px; font-size: 13px; color: #333;"
                                        maximunsize="300px" autosize="true" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 110px;">Section /Rule</td>
                                <td style="width: 3px;">: </td>
                                <td style="width: 622px;">
                                    <asp:Label ID="lblRule" Style="width: 300px; font-size: 13px; color: #333;"
                                        maximunsize="300px" autosize="true" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 110px;">Form Number</td>
                                <td style="width: 3px;">:</td>
                                <td style="width: 622px;">
                                    <asp:Label ID="lblFormNumber" Style="width: 300px; font-size: 13px; color: #333;"
                                        maximunsize="300px" autosize="true" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <fieldset style="border-style: solid; border-width: 1px; border-color: gray;">
                    <legend>Compliance Details</legend>
                    <div style="margin-bottom: 7px">
                        <asp:Label ID="lblComplianceDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                            maximunsize="300px" autosize="true" runat="server" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <asp:Label ID="lblPenalty" Style="width: 300px; font-size: 13px; color: #333;" maximunsize="300px"
                            autosize="true" runat="server" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <asp:Label ID="lblRisk" Style="width: 300px; font-size: 13px; color: #333;" maximunsize="300px"
                            autosize="true" runat="server" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <asp:LinkButton ID="lbDownloadSample" Style="width: 300px; font-size: 13px; color: #333;"
                            runat="server" Font-Underline="false" OnClick="lbDownloadSample_Click" />

                    </div>
                </fieldset>
                <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;" id="fieldsetdownloadreview" runat="server">
                    <legend>Step 1 Download Review</legend>
                    <table width="100%" style="text-align: left">
                        <thead>
                            <tr>
                                <td valign="top">

                                    <asp:Repeater ID="rptComplianceVersion" runat="server" OnItemCommand="rptComplianceVersion_ItemCommand"
                                        OnItemDataBound="rptComplianceVersion_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="tblComplianceDocumnets">
                                                <thead>
                                                    <th>Versions</th>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' ID="lblDocumentVersion"
                                                        runat="server" Text='<%# Eval("Version")%>'></asp:LinkButton></td>
                                                <td>
                                                    <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' OnClientClick='javascript:enableControls()'
                                                        ID="btnComplinceVersionDoc" runat="server" Text="Download">
                                                    </asp:LinkButton></td>

                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>

                                </td>
                                <td valign="top">
                                    <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                        OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="tblComplianceDocumnets">
                                                <thead>
                                                    <th>Compliance Related Documents</th>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:LinkButton
                                                        CommandArgument='<%# Eval("FileID")%>'
                                                        OnClientClick='javascript:enableControls()'
                                                        ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                    </asp:LinkButton></td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <asp:Repeater ID="rptWorkingFiles" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                        OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                        <HeaderTemplate>
                                            <table id="tblWorkingFiles">
                                                <thead>
                                                    <th>Compliance Working Files</th>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:LinkButton
                                                        CommandArgument='<%# Eval("FileID")%>'
                                                        ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                    </asp:LinkButton></td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                        </thead>
                    </table>
                </fieldset>

                <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px">
                    <legend>Step 2 </legend>
                    <div style="margin-bottom: 7px; margin-top: 10px;">
                        <label id="lblStatus" runat="server" style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Status</label>
                        <asp:RadioButtonList ID="rdbtnStatus" runat="server" RepeatDirection="Horizontal">
                        </asp:RadioButtonList>

                        <asp:RequiredFieldValidator ID="rfvRdoStatusButton" runat="server" ValidationGroup="ReviewerComplianceValidationGroup" ControlToValidate="rdbtnStatus"
                            ErrorMessage="Please select Status."> </asp:RequiredFieldValidator>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Remarks</label>
                        <asp:TextBox runat="server" ID="tbxRemarks" TextMode="MultiLine" Style="height: 50px; width: 388px;" />
                    </div>
                    <div style="margin-bottom: 7px" id="divDated" runat="server">
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            Date</label>
                        <asp:TextBox runat="server" ID="tbxDate" Style="height: 20px; width: 390px;" />
                        <asp:RequiredFieldValidator ErrorMessage="Please select Date." ControlToValidate="tbxDate"
                            runat="server" ID="RequiredFieldValidator1" ValidationGroup="ReviewerComplianceValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; margin-left: 200px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                            ValidationGroup="ReviewerComplianceValidationGroup" />
                        <asp:Button Text="Reject" runat="server" ID="btnReject" OnClick="btnReject_Click" CssClass="button"
                            ValidationGroup="ReviewerComplianceValidationGroup" CausesValidation="false" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" OnClick="btnCancel_Click" CssClass="button" OnClientClick="$('#divReviewerComplianceDetailsDialog').dialog('close');" />
                    </div>
                </fieldset>


                <div style="margin-bottom: 7px; clear: both; margin-top: 10px;">
                    <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false" AllowSorting="true"
                        AllowPaging="true" PageSize="12" GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE"
                        BorderStyle="Solid" OnPageIndexChanging="grdTransactionHistory_OnPageIndexChanging" OnRowCreated="grdTransactionHistory_RowCreated"
                        BorderWidth="1px" CellPadding="4" ForeColor="Black" Width="100%" Font-Size="12px" OnSorting="grdTransactionHistory_Sorting"
                        DataKeyNames="ComplianceTransactionID" OnRowCommand="grdTransactionHistory_RowCommand">
                        <Columns>
                            <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" SortExpression="CreatedByText" />
                            <asp:TemplateField HeaderText="Date" SortExpression="StatusChangedOn">
                                <ItemTemplate>
                                    <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="Remarks" HeaderText="Remarks" SortExpression="Remarks" />
                            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                        <PagerSettings Position="Top" />
                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                        <AlternatingRowStyle BackColor="#e1e1e1" />
                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-family: Verdana; font-size: 10px;" Visible="false"></asp:Label>
                </div>
                <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                <asp:Button ID="btnDownload" runat="server" Style="display: none" OnClick="btnDownload_Click" />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDownload" />
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="lbDownloadSample" />
        </Triggers>
    </asp:UpdatePanel>


</div>

