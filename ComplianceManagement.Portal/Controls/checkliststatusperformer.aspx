﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="checkliststatusperformer.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.checkliststatusperformer" %>

<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html>
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="AVANTIS - Products that simplify">
    <title>DASHBOARD :: AVANTIS - Products that simplify</title>
</head>
<body style="overflow-y: scroll; max-height: 500px;">
    <form runat="server" style="background: #f7f7f7;">
        <asp:ScriptManager ID="Isdf" runat="server" LoadScriptsBeforeUI="true" EnablePageMethods="true" ScriptMode="Release"></asp:ScriptManager>
        <link href="/newcss/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap theme -->
        <link href="/newcss/bootstrap-theme.css" rel="stylesheet" type="text/css" />
        <link href="/newcss/responsive-calendar.css" rel="stylesheet" type="text/css" />
        <link href="/newcss/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- full calendar css-->
        <link href="/newcss/bootstrap-fullcalendar.css" rel="stylesheet" type="text/css" />
        <!-- owl carousel -->
        <link rel="stylesheet" href="/newcss/owl.carousel.css" type="text/css" />
        <!-- Custom styles -->
        <link rel="stylesheet" href="/newcss/fullcalendar.css" type="text/css" />
        <link href="/newcss/stylenew.css" rel="stylesheet" type="text/css" />
        <link href="/newcss/jquery-ui-1.10.7.min.css" rel="stylesheet" type="text/css" />
        <link href="/newcss/jquery.bxslider.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="/newjs/jquery.js"></script>
        <script type="text/javascript" src="/newjs/jquery-ui-1.9.2.custom.min.js"></script>
        <link href="../tree/jquerysctipttop.css" rel="stylesheet" />
        <script type="text/javascript" src="../tree/jquery-simple-tree-table.js"></script>
        <link rel="stylesheet" href="../tree/jquery-simple-tree-table.css" />

        <link href="https://avacdn.azureedge.net/newcss/kendo.common1.2.min.css" rel="stylesheet" />
        <link href="https://avacdn.azureedge.net/newcss/kendo.rtl.min.css" rel="stylesheet" />
        <link href="https://avacdn.azureedge.net/newcss/kendo.silver.min.css" rel="stylesheet" />
        <link href="https://avacdn.azureedge.net/newcss/kendo.mobile.all.min.css" rel="stylesheet" />

        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jszip.min.js"></script>
        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/kendo.all.min.js"></script>

        <script type="text/javascript">

            $(document).ready(function () {

                kendogridupdate();

            })

            function kendogridupdate() {
                debugger;
                var gridUpdate = $("#gridUpdate").kendoGrid({
                    dataSource: {
                        transport: {
                            read: {
                                url: '<% =KendoPath%>/Data/KendoComplianceOverviewUpdates?UserId=<% =com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID%>&CustId=<% =com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID%>&complianceInstanceID=<% =compInstanceID%>',
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '<% =Authorization%>');
                        },
                    }
         
                },
                pageSize: 5,
            },
            //height: 150,
            sortable: true,
            filterable: true,
            columnMenu: false,
            pageable: true,
            reorderable: true,
            resizable: true,
            multi: true,

            noRecords: {
                template: "No records available"
            },
            columns: [
                {
                    field: "Title", title: 'Title',
                    width: "65%;",
                    attributes: {
                        style: 'white-space: nowrap;'
                    }, filterable: { multi: true, search: true }
                },
                //{
                //    field: "Date", title: 'Date',
                //    type: "date",
                //    template: "#= kendo.toString(kendo.parseDate(Date, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                //    attributes: {
                //        style: 'white-space: nowrap;'
                //    }, filterable: { multi: true, search: true }
                //},
                {
                    field: "Date", title: 'Date',
                    type: "date",
                    format: "{0:dd-MMM-yyyy}",
                    template: "#= kendo.toString(kendo.parseDate(Date, 'yyyy-MM-dd'), 'dd-MMM-yyyy') #",
                    attributes: {
                        style: 'white-space: nowrap;'
                        //}, filterable: { multi: true, search: true }
                    },
                    filterable: {
                        multi: true,
                        extra: false,
                        search: true,
                        operators: {
                            string: {
                                type: "date",
                                format: "{0:dd-MMM-yyyy}",
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains"
                            }
                        },
                    }
                },
                {
                    command: [
                        { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                    ], title: "Action", lock: true,// width: 150,
                }
            ]
        });

                $("#gridUpdate").kendoTooltip({
                    filter: ".k-grid-edit2",
                    content: function (e) {
                        return "Overview";
                    }
                });

                $("#gridUpdate").kendoTooltip({
                    filter: "td:nth-child(1)", //this filter selects the second column's cells
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        return content;
                    }
                }).data("kendoTooltip");

                $("#gridUpdate").kendoTooltip({
                    filter: "td:nth-child(2)", //this filter selects the second column's cells
                    position: "down",
                    content: function (e) {
                        var content = e.target.context.textContent;
                        return content;
                    }
                }).data("kendoTooltip");
            }

            $(document).on("click", "#gridUpdate tbody tr .ob-overview", function (e) {

                var item = $("#gridUpdate").data("kendoGrid").dataItem($(this).closest("tr"));
                //OpendocumentsUpdates(item.Title);
                OpendocumentsUpdates(item.Description);

                return true;
            });


            function OpendocumentsUpdates(title) {

                document.getElementById('detailUpdate').innerHTML = title;// 'your tip has been submitted!';


                var myWindowAdv = $("#ViewUpdateDetails");

                function onClose() {

                }

                myWindowAdv.kendoWindow({
                    width: "85%",
                    height: "50%",
                    title: "Legal Updates",
                    visible: false,
                    actions: [
                        //"Pin",
                        //"Minimize",
                        //"Maximize",
                        "Close"
                    ],

                    close: onClose
                });

                //$("#divAdvanceSearchModel").data("kendoWindow").wrapper.addClass("myKendoCustomClass");

                myWindowAdv.data("kendoWindow").center().open();
                //e.preventDefault();
                return false;
            }

        </script>

        <script type="text/javascript">
            function openInNewTab(url) {
                var win = window.open(url, '_blank');
                win.focus();
            }
            function fCalltreeCollapsed() {
                $(document).ready(function () {
                    $('#basic').simpleTreeTable({
                        collapsed: true
                    });
                });
            }
            function CloseAndBindData() {
                debugger;
                try {
                    window.parent.CloseModalPerFormer();
                } catch (e) {

                }
            }

            function CloseCalenderPERPop() {
                debugger;
                try {
                    window.parent.fcloseandcallcal();
                } catch (e) {

                }
            }

            function fFilesubmit() {
                fileUpload = document.getElementById('fuSampleFile');
                if (fileUpload.value != '') {
                    document.getElementById("<%=UploadDocument.ClientID %>").click();
                }
            }

            function fopenDocumentPriview(file) {
                $('#DocumentPriview').modal('show');
                $('#docPriview').attr('src', "../docviewer.aspx?docurl=" + file);
            }

            function fopendoctaskfileReview(file) {
                $('#modalDocumentChecklistViewer').modal('show');
                $('#docChecklistViewerPerformerAll').attr('src', "../docviewer.aspx?docurl=" + file);
            }

            $(document).ready(function () {

                $("button[data-dismiss-modal=modal2]").click(function () {
                    $('#SampleFilePopUp').modal('hide');
                    $('#modalDocumentChecklistViewer').modal('hide');
                });

            });

            //var validFilesTypes = ["exe", "bat", "zip", "rar", "dll"];
            var validFilesTypes = ["exe", "bat", "dll"];
            function ValidateFile() {

                var label = document.getElementById("<%=Label1.ClientID%>");
            var fuSampleFile = $("#<%=fuSampleFile.ClientID%>").get(0).files;
       <%-- var FileUpload1 = $("#<%=FileUpload1.ClientID%>").get(0).files;--%>
                var isValidFile = true;

                for (var i = 0; i < fuSampleFile.length; i++) {
                    var fileExtension = fuSampleFile[i].name.split('.').pop();
                    if (validFilesTypes.indexOf(fileExtension) != -1) {
                        isValidFile = false;
                        break;
                    }
                }
                if (!isValidFile) {
                    label.style.color = "red";
                    //label.innerHTML = "Invalid file uploded. .exe,.zip,.bat formats not supported.";
                    label.innerHTML = "Invalid file uploded. .exe,.bat formats not supported.";
                }
                return isValidFile;
            }

        </script>

        <style>
            tr.spaceUnder > td {
                padding-bottom: 1em;
            }
        </style>
        <style type="text/css">
            .table > thead > tr > th {
                vertical-align: bottom;
                border-bottom: 2px solid #dddddd;
            }

            .table > tbody > tr > th {
                vertical-align: bottom;
                border-bottom: 2px solid #dddddd;
            }

            .table > thead > tr > th > a {
                vertical-align: bottom;
                border-bottom: 2px solid #dddddd;
            }

            .table tr th {
                color: #666666;
                font-size: 15px;
                font-weight: normal;
                font-family: 'Roboto',sans-serif;
            }

            .clsheadergrid {
                padding: 8px !important;
                line-height: 1.428571429 !important;
                vertical-align: bottom !important;
                border-bottom: 2px solid #dddddd !important;
                color: #666666;
                font-size: 15px;
                font-weight: normal;
                font-family: 'Roboto',sans-serif;
                text-align: left;
            }

            .clsROWgrid {
                padding: 8px !important;
                line-height: 1.428571429 !important;
                vertical-align: top !important;
                border-top: 1px solid #dddddd !important;
                color: #666666 !important;
                font-size: 14px !important;
                font-family: 'Roboto', sans-serif !important;
            }

            .clspenaltysave {
                font-weight: bold;
                margin-left: 15px;
            }

            .btnss {
                background-image: url(../Images/edit_icon_new.png);
                border: 0px;
                width: 24px;
                height: 24px;
                background-color: transparent;
            }

            .circle {
                /*background-color: green;*/
                width: 15px;
                height: 15px;
                border-radius: 50%;
                display: inline-block;
                margin-right: 20px;
            }
        </style>

        <style type="text/css">
            /*.k-window div.k-window-content {
                overflow: hidden;
            }*/

            div.k-widget.k-window {
                top: 643px !important;
            }

            .k-combobox-clearable .k-input, .k-dropdowntree-clearable .k-dropdown-wrap .k-input, .k-dropdowntree-clearable .k-multiselect-wrap, .k-multiselect-clearable .k-multiselect-wrap {
                padding-right: 2em;
                text-align: center;
                display: grid;
            }

            .k-dropdown, .k-textbox {
                text-align: center;
            }
            /*.div.k-grid-footer, div.k-grid-header {
            border-top-style: solid;
            border-top-width: 1px;
        }*/
            .k-grid-content {
                min-height: 150px !important;
            }

            html {
                color: #666666;
                font-size: 15px;
                font-weight: normal;
                font-family: 'Roboto',sans-serif;
            }

            .k-checkbox-label, .k-radio-label {
                display: inline;
            }

            .myKendoCustomClass {
                z-index: 999 !important;
            }

            .k-grid-toolbar {
                background: white;
            }

            #grid .k-grid-toolbar {
                padding: .6em 1.3em .6em .4em;
            }

            .k-grid td {
                line-height: 1em;
                border-bottom-width: 1px;
            }

            .k-i-more-vertical:before {
                content: "\e006";
            }

            .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
                background-color: #1fd9e1;
                background-image: none;
            }

            k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
                color: #000000;
                border-color: #1fd9e1;
                background-color: #f6f6f6;
            }

            .k-pager-wrap > .k-link > .k-icon {
                margin-top: 5px;
                color: inherit;
            }

            .toolbar {
                float: left;
            }

            html .k-grid tr:hover {
                background: #E4F7FB;
            }

            html .k-grid tr.k-alt:hover {
                background: #E4F7FB;
                min-height: 30px;
            }

            .k-grid tbody .k-button {
                min-width: 30px;
                min-height: 30px;
                border-radius: 35px;
            }

            .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
                margin-right: 0px;
                margin-right: 0px;
                margin-left: 0px;
                margin-left: 0px;
            }

            .k-auto-scrollable {
                overflow: hidden;
            }

            .k-grid-header {
                padding-right: 0px !important;
            }

            /*.k-multicheck-wrap .k-item {
                line-height: 1.2em;
                font-size: 14px;
                margin-bottom: 5px;
            }*/

            label {
                display: inline-block;
                margin-bottom: 0px;
            }

            .k-filter-menu .k-button {
                width: 27%;
            }

            .k-label input[type="checkbox"] {
                margin: 0px 5px 0 !important;
            }

            .k-filter-row th, .k-grid-header th.k-header {
                font-size: 15px;
                background: #f8f8f8;
                font-family: 'Roboto',sans-serif;
                color: #212121;
                font-weight: 400;
            }

            .k-primary {
                border-color: #1fd9e1;
                background-color: #1fd9e1;
            }

            .k-pager-wrap {
                background-color: #f8f8f8;
                color: #2b2b2b;
            }

            /*td.k-command-cell {
                border-width: 0 1px 1px 1px;
            }*/

            /*.k-grid-pager {
                border-width: 1px 1px 1px 1px;
            }*/

            span.k-icon.k-i-calendar {
                margin-top: 6px;
            }

            .col-md-2 {
                width: 20%;
            }

            /*.k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
                border-left-width: 1px;
                line-height: 14px;
            }*/

            .k-grid tbody tr {
                height: 38px;
            }

            .k-textbox .k-icon {
                top: 50%;
                margin: -8px 0 0;
                margin-left: 56px;
                position: absolute;
            }

            label.k-label {
                font-family: roboto,sans-serif !important;
                color: #515967;
                /* font-stretch: 100%; */
                font-style: normal;
                font-weight: 400;
                min-width: 362px;
                white-space: pre-wrap;
            }

            .k-multicheck-wrap .k-item {
                line-height: 1.2em;
                font-size: 14px;
                margin-bottom: 5px;
            }
            /*label {
            display: flex;
            margin-bottom: 0px;
        }*/


            .k-animation-container {
                width: 400px !important;
                white-space: pre-wrap;
                /*height: 255px !important;*/
                /*left: 735px !important;*/
            }

            k-filter-menu k-popup k-group k-reset k-state-border-up {
                height: 255px !important;
            }

            .k-multicheck-wrap {
                max-height: 125px !important;
            }
        </style>

        <div id="divComplianceDetailsDialog">
            <asp:UpdatePanel ID="upComplianceDetails" runat="server" UpdateMode="Conditional"
                OnLoad="upComplianceDetails_Load">
                <ContentTemplate>
                    <div style="margin: 5px">
                        <div style="margin-bottom: 4px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceValidationGroup1" />
                            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" class="alert alert-block alert-danger fade in" EnableClientScript="true"
                                ValidationGroup="ComplianceValidationGroup1" Display="None" />
                            <asp:Label ID="Label1" Visible="false" runat="server"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
                            <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />
                             <asp:HiddenField runat="server" ID="hdnisdocumentcompulsory" />
                        </div>
                        <asp:Label ID="Labellockingmsg" Style="border-color: #ffd0e1; color: #ff2d55;" Text="Compliance is locked,contact to management user" runat="server"></asp:Label>
                        <div class="clearfix" style="margin-bottom: 10px"></div>
                        <div>
                            <asp:Label ID="Label2" Text="This is a  " Style="width: 300px; font-weight: bold; font-size: 13px; color: #333;"
                                maximunsize="300px" autosize="true" runat="server" />
                            <div id="divRiskType" runat="server" class="circle"></div>
                            <asp:Label ID="lblRiskType" Style="width: 300px; margin-left: -17px; font-weight: bold; font-size: 13px; color: #333;"
                                maximunsize="300px" autosize="true" runat="server" />
                        </div>
                        <div id="ActDetails" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px; background-color: #f7f7f7;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails">
                                                    <h2>Act Details</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseActDetails"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseActDetails" class="collapse">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                    <table style="width: 100%;">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Act Name</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblActName" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Section /Rule</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblRule" Style="width: 88%; font-size: 13px; color: #333;"
                                                                    autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Act Document(s)</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <table style="width: 100%; text-align: left">
                                                                    <thead>
                                                                        <tr>
                                                                            <td style="vertical-align: top">
                                                                                <asp:Repeater ID="rptActChkPfDocVersion" runat="server" OnItemCommand="rptActChkPfDocVersion_ItemCommand"
                                                                                    OnItemDataBound="rptActChkPfDocVersion_ItemDataBound">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                                            <ContentTemplate>
                                                                                                <tr>
                                                                                                    <td style="width: 40%">
                                                                                                        <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("Act_ID") + ","+ Eval("Version") %>' ID="lblDocumentActChkPfVersion"
                                                                                                            runat="server" Text='<%# Eval("Act_TypeVersionDate") != null ? Eval("DocumentType").ToString() +" " + ((DateTime)Eval("Act_TypeVersionDate")).ToString("MMM-yyyy") : Eval("DocumentType").ToString() %>'></asp:LinkButton></td>
                                                                                                    <td style="width: 60%">
                                                                                                        <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("Act_ID") + ","+ Eval("Version") %>'
                                                                                                            ID="btnActChkPfVersionDoc" runat="server" Text="Download" Style="color: blue;">
                                                                                                        </asp:LinkButton>
                                                                                                        <asp:Label ID="lblSlashActChkPerformer" Text="/" Style="color: blue;" runat="server" />
                                                                                                        <asp:LinkButton CommandName="View" ID="lnkViewActChkPfDoc" CommandArgument='<%# Eval("Act_ID") + ","+ Eval("Version") %>' Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                                                            runat="server" Font-Underline="false" />
                                                                                                        <asp:Label ID="lblpathActChkPfReviewDoc" runat="server" Style="display: none"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="btnActChkPfVersionDoc" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>
                                                                            </td>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="ComplianceDetails" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails">
                                                    <h2>Compliance Details</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <%--gaurav--%>
                                        <div id="collapseComplianceDetails" class="panel-collapse collapse in">
                                            <%--gaurav--%>
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                    <table style="width: 100%">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Compliance Id</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblComplianceID" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Short Description</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblComplianceDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 15%; font-weight: bold; vertical-align: top;">Detailed Description</td>
                                                            <td style="width: 2%; font-weight: bold; vertical-align: top;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblDetailedDiscription" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Penalty</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblPenalty" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Frequency</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblFrequency" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </div>
                                            </fieldset>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="OthersDetails" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails">
                                                    <h2>Additional Details</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseOthersDetails" class="collapse">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                    <table style="width: 100%">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Risk Type</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblRisk" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Sample Form</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:UpdatePanel ID="upsample" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Label ID="lblFormNumber" Style="width: 150px; font-size: 13px; color: #333;"
                                                                            maximunsize="300px" autosize="true" runat="server" />
                                                                        <asp:LinkButton ID="lbDownloadSample" Style="width: 150px; font-size: 13px; color: blue"
                                                                            runat="server" Font-Underline="false" OnClick="lbDownloadSample_Click" />
                                                                        <asp:Label ID="lblSlash" Text="/" Style="color: blue;" runat="server" />
                                                                        <asp:LinkButton ID="lnkViewSampleForm" Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                            runat="server" Font-Underline="false" OnClientClick="fopendocfile();" />
                                                                        <asp:Label ID="lblpathsample" runat="server" Style="display: none"></asp:Label>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Regulatory website link</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:LinkButton ID="lnkSampleForm" Text="" Style="width: 300px; font-size: 13px; color: blue"
                                                                    runat="server" Font-Underline="false" OnClick="lnkSampleForm_Click" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 25%; font-weight: bold;">Additional/Reference Text </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblRefrenceText" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Location</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblLocation" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Period</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblPeriod" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Due Date</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblDueDate" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder" runat="server" id="TRactualduedate" visible="false">
                                                            <td style="width: 15%; font-weight: bold;">Revise Due Date (Holiday)</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblActualDueDate" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder" id="trAuditChecklist" runat="server">
                                                            <td style="width: 25%; font-weight: bold;">Audit Checklist</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblAuditChecklist" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div runat="server" id="divTask" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                                <h2>Main Task Details</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>

                                        <div id="collapseTaskSubTask" class="collapse in">
                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                <asp:GridView runat="server" ID="gridSubTask" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                    AllowPaging="true" PageSize="5" CssClass="table" GridLines="None" BorderWidth="0px" DataKeyNames="TaskID"
                                                    OnRowCommand="gridSubTask_RowCommand" OnRowDataBound="gridSubTask_RowDataBound" AutoPostBack="true">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Task">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblTaskTitle" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Performer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblPerformer" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'
                                                                        Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'></asp:Label>

                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Reviewer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'
                                                                        Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Due Date">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                    <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="upSubTaskDownloadView" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="btnSubTaskDocDownload" runat="server" CommandName="Download" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Download Documents" Text="Download" Style="color: blue;">
                                                                        </asp:LinkButton>
                                                                        <asp:Label ID="lblSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                        <asp:LinkButton CommandName="View" runat="server" ID="btnSubTaskDocView" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="View Documents"
                                                                            Text="View" Style="width: 150px; font-size: 13px; color: blue" />
                                                                        <asp:Label ID="CompDocReviewPath" runat="server" Style="display: none"></asp:Label>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnSubTaskDocDownload" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="UpdateComplianceStatus" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateComplianceStatus">
                                                    <h2>Update Compliance Status</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseUpdateComplianceStatus"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseUpdateComplianceStatus" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div style="margin-bottom: 7px" runat="server" id="divUploadDocument">


                                                    <table style="width: 100%">

                                                        <% if (UploadDocumentLink == "True")
                                                            {%>
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <% if (hdnisdocumentcompulsory.Value=="1")
                                                                    {%>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <%}
                                                                    else
                                                                    {%>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <%}%>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Compliance Document(s)</label>
                                                            </td>

                                                            <td style="width: 2%; font-weight: bold; vertical-align: text-top;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:TextBox runat="server" ID="TxtChecklistDocument" class="form-control" />
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="Uploadlingchecklistfile" runat="server" Text="Add Link" Style="" OnClick="Uploadlingchecklistfile_Click"
                                                                    class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Add Link" />

                                                            </td>
                                                        </tr>
                                                        <%}%>
                                                        <% if (UploadDocumentLink == "False")
                                                            {%>

                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%;">

                                                                <% if (hdnisdocumentcompulsory.Value=="1")
                                                                    {%>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <%}
                                                                    else
                                                                    {%>
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                                                <%}%>
                                                                <asp:Label ID="lblDocComplasary" runat="server" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</asp:Label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Upload Compliance Document(s)</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 60%;">
                                                                <asp:FileUpload ID="fuSampleFile" Multiple="Multiple" runat="server" onchange="fFilesubmit()" Style="color: black" />
                                                            </td>
                                                            <td style="width: 13%;">
                                                                <asp:Button ID="UploadDocument" runat="server" Text="Upload Document" Style="display: none;" OnClick="UploadDocument_Click"
                                                                    class="btn btn-search" data-toggle="tooltip" data-placement="top" ToolTip="Upload Document" CausesValidation="true" />
                                                            </td>
                                                        </tr>

                                                        <%}%>
                                                    </table>



                                                </div>



                                                <div style="margin-bottom: 7px" runat="server" id="divgrdFiles">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%;">
                                                                <asp:GridView runat="server" ID="grdDocument" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                                    PageSize="100" AllowPaging="true" OnRowCommand="grdDocument_RowCommand" OnRowDataBound="grdDocument_RowDataBound" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                                                                            <ItemTemplate>
                                                                                <%#Container.DataItemIndex+1 %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Document Name" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 500px;">
                                                                                    <asp:LinkButton ID="lnkRedierctDocument" Style="color: blue; text-decoration: underline;" runat="server" Text='<%# Eval("DocName") %>' OnClientClick=<%# "openInNewTab('" + Eval("DocPath") + "')" %>>                                                                                                                                                                             
                                                                                    </asp:LinkButton>
                                                                                    <asp:Label ID="lblRedirectDocument" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("DocName") %>' ToolTip='<%# Eval("DocName") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Document Type" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                                                    <asp:Label ID="lblIsLinkTrue" Visible="false" runat="server" data-placement="bottom" Text='<%# Eval("ISLink") %>' ToolTip='<%# Eval("ISLink") %>'></asp:Label>
                                                                                    <asp:Label ID="lblDocType" runat="server" data-placement="bottom" Text='<%# Eval("DocType") %>' ToolTip='<%# Eval("DocType") %>'></asp:Label>
                                                                                    <asp:Label ID="lblScheduleOnID" runat="server" Visible="false" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ScheduleOnID") %>'></asp:Label>
                                                                                    <asp:Label ID="lblComplianceInstanceID" runat="server" Visible="false" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ComplianceInstanceID") %>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                                            <ItemTemplate>
                                                                                <asp:UpdatePanel ID="upgrid" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:LinkButton ID="lnkDownloadDocument" runat="server" CommandName="Download Document" ToolTip="Download Document" data-toggle="tooltip"
                                                                                            CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/download_icon_new.png")%>' alt="Download Document" /></asp:LinkButton>
                                                                                        <asp:LinkButton ID="lnkViewDocument" runat="server" CommandName="View Document" ToolTip="View Document" data-toggle="tooltip"
                                                                                            CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/View-icon-new.png")%>' alt="View Document" /></asp:LinkButton>
                                                                                        <asp:LinkButton ID="lnkDeleteDocument" runat="server" CommandName="Delete Document" ToolTip="Delete Document" data-toggle="tooltip"
                                                                                            CommandArgument='<%# Eval("Id") %>'>
                                                        <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Document" /></asp:LinkButton>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:PostBackTrigger ControlID="lnkDownloadDocument" />
                                                                                        <asp:PostBackTrigger ControlID="lnkViewDocument" />

                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>
                                                                    <RowStyle CssClass="clsROWgrid" />
                                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                                    <PagerSettings Visible="false" />
                                                                    <PagerTemplate>
                                                                    </PagerTemplate>
                                                                    <EmptyDataTemplate>
                                                                        No Record Found
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Remarks</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                  
                                                                        <asp:TextBox runat="server" ID="tbxRemarks" TextMode="MultiLine" class="form-control" Rows="2" />
                                                                               
                                                                
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div style="margin-bottom: 7px; margin-left: 42%; margin-top: 10px;">
                                                    <asp:Button Text="Submit" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-search"
                                                        ValidationGroup="ComplianceValidationGroup1" />

                                                    <asp:Button Text="Not Complied" runat="server" ID="btnNotComplied" Visible="false" Style="margin-left: 15px;" OnClick="btnNotComplied_Click" CssClass="btn btn-search"
                                                        ValidationGroup="ComplianceValidationGroup1" />

                                                    <asp:Button Text="Not Applicable" runat="server" ID="btnNotApplicable" Style="margin-left: 15px;" OnClick="btnNotApplicable_Click" CssClass="btn btn-search"
                                                        ValidationGroup="ComplianceValidationGroup1" />

                                                    <asp:Button Text="Close" ID="btnCancel" OnClick="btnCancel_Click" Style="margin-left: 15px;" CssClass="btn btn-search" data-dismiss="modal" runat="server" />
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div runat="server" id="divDeleteDocument" style="text-align: left;">
                            <fieldset style="border-style: solid; border-width: 1px; border-color: gray; margin-top: 5px;">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 50%">
                                            <asp:UpdatePanel runat="server">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rptComplianceDocumnets" runat="server" OnItemCommand="rptComplianceDocumnets_ItemCommand"
                                                        OnItemDataBound="rptComplianceDocumnets_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="tblComplianceDocumnets">
                                                                <thead>
                                                                    <th>Compliance Related Documents</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                        ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                    </asp:LinkButton></td>
                                                                <td>
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>' CommandName="Delete"
                                                                        OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                        ID="lbtLinkDocbutton" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                                    </asp:LinkButton></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rptWorkingFiles" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                                        OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="tblWorkingFiles">
                                                                <thead>
                                                                    <th>Compliance Working Files</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td style="width: 50%">
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>' CommandName="Download"
                                                                        ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                                    </asp:LinkButton></td>
                                                                <td>
                                                                    <asp:LinkButton
                                                                        CommandArgument='<%# Eval("FileID")%>' CommandName="Delete"
                                                                        OnClientClick="return confirm('Are you certain you want to delete this file?');"
                                                                        ID="lbtLinkbutton" runat="server"><img src='<%# ResolveUrl("~/Images/delete_icon.png")%>' alt="Delete" title="Delete" width="15px" height="15px" />
                                                                    </asp:LinkButton></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </div>
                        <div id="LegalUpdates" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">

                                        <div class="panel panel-default" style="margin-bottom: 12px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseLegalUpdates">
                                                    <h2>Legal Updates</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseLegalUpdates"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="collapseLegalUpdates" class="collapse">

                                            <div id="gridUpdate"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="AuditLog" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog">
                                                    <h2>Audit Log</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseAuditLog"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="collapseAuditLog" class="collapse">
                                            <div runat="server" id="log" style="text-align: left;">
                                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                                    <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                        <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false" AllowSorting="true"
                                                            AllowPaging="true" PageSize="5" CssClass="table" GridLines="Horizontal" OnPageIndexChanging="grdTransactionHistory_OnPageIndexChanging"
                                                            OnRowCreated="grdTransactionHistory_RowCreated" BorderWidth="0px" OnSorting="grdTransactionHistory_Sorting"
                                                            DataKeyNames="ComplianceTransactionID" OnRowCommand="grdTransactionHistory_RowCommand">
                                                            <Columns>
                                                                <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />
                                                                <asp:TemplateField HeaderText="Date">
                                                                    <ItemTemplate>
                                                                        <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString("dd-MM-yyyy") : ""%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                                                <asp:BoundField DataField="Status" HeaderText="Status" />
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Right" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </div>
                                                </fieldset>
                                                <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="font-size: 10px; color: #666;" Visible="false"></asp:Label>
                                                <div></div>
                                                <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                                                <asp:Button ID="btnDownload" runat="server" Style="display: none" OnClick="btnDownload_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="ViewUpdateDetails">
                            <label id="detailUpdate"></label>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnDownload" />
                    <asp:PostBackTrigger ControlID="btnSave" />
                    <asp:PostBackTrigger ControlID="lbDownloadSample" />
                    <asp:PostBackTrigger ControlID="UploadDocument" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div>
            <div class="modal fade" id="modalDocumentChecklistViewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                <div class="modal-dialog" style="width: 100%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="height: 570px;">
                            <div style="width: 100%;">
                                <div style="float: left; width: 10%">
                                    <table width="100%" style="text-align: left; margin-left: 5%;">
                                        <thead>
                                            <tr>
                                                <td valign="top">
                                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Repeater ID="rptChecklistVersionView" runat="server" OnItemCommand="rptComplianceVersionView_ItemCommand">
                                                                <HeaderTemplate>
                                                                    <table id="tblComplianceDocumnets">
                                                                        <thead>
                                                                            <th>Versions</th>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblChecklistDocumentVersionView"
                                                                                        runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="lblChecklistDocumentVersionView" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </table>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="rptChecklistVersionView" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div style="float: left; width: 90%">
                                    <asp:Label runat="server" ID="lblMessageChecklist" Style="color: red;"></asp:Label>
                                    <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                        <iframe src="about:blank" id="docChecklistViewerPerformerAll" runat="server" width="100%" height="535px"></iframe>
                                    </fieldset>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="modal fade" id="SampleFilePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
                <div class="modal-dialog" style="width: 100%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" onclick="$('#SampleFilePopUp').modal('hide');" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body" style="height: 570px;">
                            <div style="float: left; width: 10%">
                                <table width="100%" style="text-align: left; margin-left: 5%;">
                                    <thead>
                                        <tr>
                                            <td valign="top">
                                                <%-- <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdatleMode="Conditional">
                                                    <ContentTemplate>--%>
                                                <asp:Repeater ID="rptComplianceSampleView" runat="server" OnItemCommand="rptComplianceSampleView_ItemCommand"
                                                    OnItemDataBound="rptComplianceSampleView_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="tblComplianceDocumnets">
                                                            <thead>
                                                                <th>Sample Forms</th>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("ComplianceID") %>' ID="lblSampleView"
                                                                            runat="server" ToolTip='<%# Eval("Name")%>' Text='<%# Eval("Name") %>'></asp:LinkButton>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="lblSampleView" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                <%-- </ContentTemplate>
                                                </asp:UpdatePanel>--%>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div style="float: left; width: 90%">
                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="docViewerRamAll" runat="server" width="100%" height="550px"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="divActChkPfFilePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
            <div class="modal-dialog" style="width: 100%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="$('#divActChkPfFilePopUp').modal('hide');" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div style="float: left; width: 10%">
                                <table style="width: 100%; text-align: left; margin-left: 5%;">
                                    <thead>
                                        <tr>
                                            <td style="vertical-align: top">
                                                <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Repeater ID="rptActChkPfDocVersionView" runat="server" OnItemCommand="rptActChkPfDocVersionView_ItemCommand" OnItemDataBound="rptActChkPfDocVersionView_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <table id="tblActChkPfViewDocumnets">
                                                                    <thead>
                                                                        <th>Versions</th>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("Act_ID") + ","+ Eval("Version") %>' ID="lblActChkPfDocumentVersionView"
                                                                                    runat="server" ToolTip='<%# Eval("Act_TypeVersionDate") != null ? Eval("DocumentType").ToString() +" " + ((DateTime)Eval("Act_TypeVersionDate")).ToString("MMM-yyyy") : Eval("DocumentType").ToString() %>' Text='<%# Eval("Act_TypeVersionDate") != null ? Eval("DocumentType").ToString().Substring(0,4) +" " + ((DateTime)Eval("Act_TypeVersionDate")).ToString("MMM-yyyy") : Eval("DocumentType").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="lblActChkPfDocumentVersionView" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="rptActChkPfDocVersionView" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div style="float: left; width: 90%">
                                <%--  <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdatleMode="Conditional">
                                    <ContentTemplate>--%>
                                <asp:Label ID="lblMessageChkPfAct" runat="server" Style="color: red;"></asp:Label>
                                <%-- </ContentTemplate>
                                </asp:UpdatePanel>--%>
                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="IframeChkPfActFile" runat="server" width="100%" height="550px"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="DocumentPriview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
            <div class="modal-dialog" style="width: 100%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="$('#DocumentPriview').modal('hide');" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                <iframe src="about:blank" id="docPriview" runat="server" width="100%" height="535px"></iframe>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            function fopendocfile() {
                $('#SampleFilePopUp').modal('show');
                $('#docViewerRamAll').attr('src', "../docviewer.aspx?docurl=" + $("#<%= lblpathsample.ClientID %>").text());
            }

            function fram(file) {
                debugger;
                $('#docViewerRamAll').attr('src', "../docviewer.aspx?docurl=" + file);
            }
            function fopendocfileChkPfAct(file) {

                $('#divActChkPfFilePopUp').modal('show');
                $('#IframeChkPfActFile').attr('src', "../docviewer.aspx?docurl=" + file);
            }

            function fopendocfileChkPfActPopUp() {
                $('#divActChkPfFilePopUp').modal('show');
                $('#IframeChkPfActFile').attr('src', "../docviewer.aspx?docurl=");
            }
        </script>

        <script type="text/javascript" src="/newjs/jquery.bxslider.js"></script>
        <script type="text/javascript" src="/newjs/bootstrap.min.js"></script>
        <!-- nice scroll -->

        <!-- charts scripts -->
        <script type="text/javascript" src="../assets/jquery-knob/js/jquery.knob.js"></script>
        <script type="text/javascript" src="/newjs/owl.carousel.js"></script>
        <!-- jQuery full calendar -->
        <script type="text/javascript" src="/newjs/fullcalendar.min.js"></script>
        <!--script for this page only-->
        <script type="text/javascript" src="/newjs/calendar-custom.js"></script>
        <script type="text/javascript" src="/newjs/jquery.rateit.min.js"></script>
        <!-- custom select -->
        <script type="text/javascript" src="/newjs/jquery.customSelect.min.js"></script>
        <!--custome script for all page-->
        <%-- <script type="text/javascript" src="/newjs/scripts.js"></script>--%>
    </form>

</body>

</html>

