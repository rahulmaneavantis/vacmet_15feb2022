﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddChecklistRemark.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.AddChecklistRemark" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
    <style>
        .center {
  margin-left: auto;
  margin-right: auto;
}
        td {
  padding: 5px;
}

    </style>   
<body>
    <form id="form1" runat="server">
    <div>
    <asp:ScriptManager ID="Isdf" runat="server" LoadScriptsBeforeUI="true" EnablePageMethods="true"></asp:ScriptManager>
        <link href="https://avacdn.azureedge.net/newcss/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap theme -->
        <link href="https://avacdn.azureedge.net/newcss/bootstrap-theme.css" rel="stylesheet" type="text/css" />
        <link href="https://avacdn.azureedge.net/newcss/responsive-calendar.css" rel="stylesheet" type="text/css" />
        <link href="https://avacdn.azureedge.net/newcss/font-awesome.min.css" rel="stylesheet" type="text/css" />
      
    <div>
     <div style="margin-bottom: 4px">
         <asp:ValidationSummary ID="ValidationSummary1" Style="padding-left: 5%" runat="server" Display="none"
             class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceValidationGroup1" />

         <asp:CustomValidator ID="cvDuplicateEntry" runat="server" class="alert alert-block alert-danger fade in"
             EnableClientScript="true" ValidationGroup="ComplianceValidationGroup1" Style="display: none; padding-left: 40px;" />

         <asp:Label ID="Label1" class="alert alert-block alert-danger fade in" Visible="false" runat="server"></asp:Label>
         <asp:Label ID="Labelmsg" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
        <%-- <asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
         <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />--%>
     </div>

 
     
        <div class="row">
            <div class="col-md-2" style="margin-left: 58px;">
                <label style="display: block; float: left; font-size: 13px; color: red;">*</label>
                
                 <label style="font-weight: bold; vertical-align: text-top;">Remark</label>
            </div>
            <div class="col-md-6" style="float: right;margin-right: 141px;margin-top: -24px;">
                <asp:TextBox runat="server" ID="tbxRemarks" CssClass="form-control"  TextMode="MultiLine" Style="width: 259px; margin-right: -61px;" />
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Please Enter Remark."
                       ControlToValidate="tbxRemarks" runat="server" ValidationGroup="ComplianceValidationGroup1" Display="None" />
         
            </div>
            
        </div>
        <div class="row" style="margin-right: 260px;margin-top: 71px;">
            <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary pull-right"  ValidationGroup="ComplianceValidationGroup1" Onclick="btnSave_Click" ></asp:Button>
       
        </div>
    </div>
    </div>
    </form>
</body>
</html>
