﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Configuration;
using System.Threading;
using System.Drawing;
using System.Web.UI.HtmlControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class ActiveEventDashboardsCA : System.Web.UI.UserControl
    {
        static int Type = 0;
        static int glParentID = 0;
        static int glIntermediateID = 0;
        static int glsubEventID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Page.RouteData.Values["filter"] != null)
                {
                    if (Page.RouteData.Values["filter"].ToString().Equals("AssignedEvents"))
                    {
                        if (Page.RouteData.Values["Type"].ToString().Equals("AssignedEvents"))
                        {
                            ViewState["Role"] = Page.RouteData.Values["Role"].ToString();
                        }
                    }
                }
                
                Session["EventRoleID"] = 10;

                #region User Role
               
                //var AssignedRoles = ComplianceManagement.Business.ComplianceManagement.GetUserRoles(AuthenticationHelper.UserID);
                //int EventOwnerRoleID = 0;
                //int EventReviewerRoleID = 0;
                //foreach (var ar in AssignedRoles)
                //{
                //    string name = ar.Name;
                //    if (name.Equals("Event Owner"))
                //    {
                //        EventOwnerRoleID = 10;
                //    }
                //    if (name.Equals("Event Reviewer"))
                //    {
                //        EventReviewerRoleID = 11;
                //    }
                //}
                //if (EventOwnerRoleID == 10 && EventReviewerRoleID == 11)
                //{
                //    Session["EventRoleID"] = 10;
                //}
                //if (EventOwnerRoleID == 10 && EventReviewerRoleID == 0)
                //{
                //    Session["EventRoleID"] = 10;
                //}
                //else
                //{
                //    Session["EventRoleID"] = 11;
                //}

                //BindEventInstances(Convert.ToInt32(Session["EventRoleID"]));
                #endregion
            }
        }

        public void BindEventInstances(int eventRoleID,int pageIndex = 0, bool isBranchChanged = false)
        {
            try
            {
                try
                {
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    //var dataSource = EventManagement.GetAllAssignedInstancesRahul(eventRoleID, -1, AuthenticationHelper.UserID, Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID));
                    var dataSource = entities.SP_GetAllActivatedEvent(eventRoleID, AuthenticationHelper.UserID, Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID)); // EventManagement.GetAllAssignedInstancesRahul(eventRoleID, -1, AuthenticationHelper.UserID, Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID));
                    grdEventList1.PageIndex = pageIndex;
                    grdEventList1.DataSource = dataSource;
                    grdEventList1.DataBind();
                    //upEventInstanceList.Update();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void grdEventList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
            //    if (sortColumnIndex != -1)
            //    {
            //        AddSortImage(sortColumnIndex, e.Row);
            //    }
            //}
        }

        protected void grdEventList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
               // ComplianceDBEntities entities = new ComplianceDBEntities();
               // var assignmentList = entities.SP_GetAllActivatedEvent(Convert.ToInt32(Session["EventRoleID"]), AuthenticationHelper.UserID, Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID)); 
               //// var assignmentList = EventManagement.GetAllAssignedInstancesRahul(-1, AuthenticationHelper.UserID, Convert.ToInt32(UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID));
               // if (direction == SortDirection.Ascending)
               // {
               //     assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
               //     direction = SortDirection.Descending;
               // }
               // else
               // {
               //     assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
               //     direction = SortDirection.Ascending;
               // }
               // foreach (DataControlField field in grdEventList1.Columns)
               // {
               //     if (field.SortExpression == e.SortExpression)
               //     {
               //         ViewState["SortIndex"] = grdEventList1.Columns.IndexOf(field);
               //     }
               // }
               // grdEventList1.DataSource = assignmentList;
               // grdEventList1.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindEventInstances(Convert.ToInt32(Session["EventRoleID"]), pageIndex: e.NewPageIndex);
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;
            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdEventList_RowEditing(object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
        {
            grdEventList1.EditIndex = e.NewEditIndex;
            DateTime date = DateTime.Now;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            BindEventInstances(Convert.ToInt32(Session["EventRoleID"]));
        }

        protected void grdEventList_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
        {
            try
            {
                DateTime date1 = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);

                 Label txtDate1 = (Label)grdEventList1.Rows[e.RowIndex].FindControl("lblHeldOn");
                 string HeldOn = Convert.ToDateTime(txtDate1.Text).ToString("dd-MM-yyyy");

                    Session["HeldOn"] = null;
                    Session["HeldOn"] = HeldOn;

                    int branch = Convert.ToInt32((grdEventList1.Rows[e.RowIndex].FindControl("lblBranch") as Label).Text);
                    Session["Rbranch"] = null;
                    Session["Rbranch"] = branch;

                    Type = EventManagement.GetCompanyType(Convert.ToInt32(branch));

                    long EventScheduledOnID = Convert.ToInt64((grdEventList1.Rows[e.RowIndex].FindControl("lblEventScheduleOnID") as Label).Text);
                    Session["EventScheduledOnID"] = null;
                    Session["EventScheduledOnID"] = EventScheduledOnID;

                    long assignmnetID = Convert.ToInt64((grdEventList1.Rows[e.RowIndex].FindControl("lblAssignmnetId") as Label).Text);
                    Session["assignmnetID"] = null;
                    Session["assignmnetID"] = assignmnetID;
                    string branchName = (grdEventList1.Rows[e.RowIndex].FindControl("lblBranchName") as Label).Text;
                    Session["RbranchName"] = null;
                    Session["RbranchName"] = branchName;
                    string eventname = (grdEventList1.Rows[e.RowIndex].FindControl("lbleventName") as Label).Text;
                    Session["Reventname"] = null;
                    Session["Reventname"] = eventname;
                    int UserId = Convert.ToInt32((grdEventList1.Rows[e.RowIndex].FindControl("lblUserID") as Label).Text);
                    Session["RUserId"] = null;
                    Session["RUserId"] = UserId;
                    int eventId = Convert.ToInt32(grdEventList1.DataKeys[e.RowIndex].Value);
                    Session["eventId"] = null;
                    Session["eventId"] = eventId;

                    Session["EventInstanceID"] = null;
                    Session["EventInstanceID"] = Convert.ToInt64((grdEventList1.Rows[e.RowIndex].FindControl("lblEventInstanceID") as Label).Text);

                    txtDescription.Text = Convert.ToString((grdEventList1.Rows[e.RowIndex].FindControl("lbleventDesc") as Label).Text);

                    #region Not Assigned Copliance Mail
                    //var exceptComplianceIDs = EventManagement.CheckAllEventCompliance(eventId, branch);
                    //if (exceptComplianceIDs.Count > 0)
                    //{
                    //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('Associated compliances of this event are not assigned to location. Please assign compliances first and then update event held date.')", true);
                    //    int customerID = -1;
                    //    string ReplyEmailAddressName = "";
                    //    if (AuthenticationHelper.Role.Equals("SADMN"))
                    //        ReplyEmailAddressName = "Avantis";
                    //    else
                    //    {
                    //        customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    //        ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                    //    }
                    //    new Thread(() =>
                    //    {
                    //        User user = UserManagement.GetByID(UserId);
                    //        List<User> emailsId = UserManagement.GetAdminUsers(CustomerBranchManagement.GetByID(branch).CustomerID);
                    //        emailsId.Add(user);
                    //        foreach (var us in emailsId)
                    //        {
                    //            string message = "<html><head><title>Your attention required</title><style>td{padding: 5px;}.label{font-weight: bold;}</style></head><body style='font-family:verdana; font-size: 14px;'>"
                    //                             + "Dear " + us.FirstName + " " + us.LastName + ",<br /><br />"
                    //                             + "Associated compliance of " + "'" + eventname + " have not been assigned to " + branchName + "'.<br> Please assign the compliance and update your event held on date.<br /><br />Thanks and Regards,<br />Team " + ReplyEmailAddressName + "</body></html>";
                    //            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], new List<String>(new String[] { us.Email }), null, null, "AVACOM Alert :: Missing compliances.", message);
                    //        }
                    //    }).Start();
                    //}
                    #endregion

                    BindParentEventData(Convert.ToInt32(eventId));

                    //DateTime date = DateTime.Now;
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);

                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divOptionalCompliances", "$(\"#divOptionalCompliances\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        List<long> CheckedMandatoryCompliances = new List<long>();

        List<long> UnCheckedOptionalCompliances = new List<long>();

        List<long> ActualCheckedOptionalCompliances = new List<long>();
        
        protected void grdEventList_RowCancelingEdit(object sender, System.Web.UI.WebControls.GridViewCancelEditEventArgs e)
        {
            //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
            grdEventList1.EditIndex = -1;
            BindEventInstances(Convert.ToInt32(Session["EventRoleID"]));
        }

        protected bool visibleEdit(long roleID)
        {
            try
            {
                bool result = true;
                //if (roleID == 11)
                //{
                //    result = false;
                //}
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool visibleParentEventAddButton(int ParentEventID, int EventScheduledOnId)
        {
            try
            {
                try
                {
                    bool result = false;
                    ComplianceDBEntities entities = new ComplianceDBEntities();

                    int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);

                    var data = (from row in entities.EventCompAssignDays
                                join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == 0 && row1.ComplinceVisible == true
                                && row4.ID == CustomerBranchID
                                select row).Distinct().FirstOrDefault();

                    if (data == null)
                     {
                         result = false;
                     }
                     else
                     {
                         var data1 = (from row in entities.ComplianceScheduleOns
                                     join row1 in entities.ComplianceTransactions
                                     on row.ComplianceInstanceID equals row1.ComplianceInstanceId
                                     where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                     && row.SubEventID == 0 && row.EventScheduledOnID == EventScheduledOnId
                                     && row1.StatusId != 1
                                     select row).FirstOrDefault();

                         if (data1 == null)
                         {
                             result = EventManagement.CheckParentEventDateAssigned(ParentEventID, EventScheduledOnId);

                             return result;
                         }
                         else
                         {
                             result = false;
                         }
                     }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                }
                return false;
    
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool visibleParentEventUpdateButton(int ParentEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();

                var data1 = (from row in entities.EventCompAssignDays
                            join row1 in entities.EventAssignDates
                            on row.ParentEventID equals row1.ParentEventID
                            where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                            && row.SubEventID == 0 && row1.EventScheduleOnID == EventScheduledOnId
                             select row).FirstOrDefault();
                if (data1 == null)
                {
                    result = false;
                }
                else
                {
                     var data = (from row in entities.ComplianceScheduleOns
                                join row1 in entities.ComplianceTransactions
                                on row.ID equals row1.ComplianceScheduleOnID 
                                where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == 0 && row.EventScheduledOnID == EventScheduledOnId
                                && row1.StatusId != 1
                                 select row).FirstOrDefault();

                    if (data == null)
                    {
                        result = EventManagement.CheckParentEventDateAssigned(ParentEventID, EventScheduledOnId);

                        if (result == false)
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool CheckSubEventTransactionComplete(int ParentEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);

                var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                            join row1 in entities.ComplianceTransactions
                            on row.ID equals row1.ComplianceScheduleOnID
                            where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                            && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                            && (row1.StatusId == 4 || row1.StatusId ==5) 
                            select row).ToList();

                var SubeventComplainceList = (from row in entities.EventCompAssignDays
                                              join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                              join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                              join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                              join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                              where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                                              && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                              && row4.ID == CustomerBranchID
                                              select row).Distinct().ToList();


                if (ComplainceTransCompleteList.Count >= SubeventComplainceList.Count)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool CheckIntermediateEventTransactionComplete(int ParentEventID, int IntermediateID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);

                var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                                                   join row1 in entities.ComplianceTransactions
                                                   on row.ID equals row1.ComplianceScheduleOnID
                                                   where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateID
                                                   && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                                   && (row1.StatusId == 4 || row1.StatusId == 5)
                                                   select row).ToList();

                var SubeventComplainceList = (from row in entities.EventCompAssignDays
                                              join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                                              join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                                              join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                                              join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                                              where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateID
                                              && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                                              && row4.ID == CustomerBranchID
                                              select row).Distinct().ToList();


                if (ComplainceTransCompleteList.Count >= SubeventComplainceList.Count)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }


        protected bool enableSubEventUpdateButton(int ParentEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();

                var data = (from row in entities.ComplianceScheduleOns
                            join row1 in entities.ComplianceTransactions
                            on row.ID equals row1.ComplianceScheduleOnID
                            where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                            && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                            && row1.StatusId != 1
                            select row).FirstOrDefault();

                if (data == null)
                {
                    result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnId);
                    if (result == false)
                    {
                        result = true;
                    }
                }
                else
                {
                    result = false;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool visibleTxtgvChildGridDate(int ParentEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {

                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();

                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);

                var data = (from row in entities.EventCompAssignDays
                            join row1 in entities.Compliances on row.ComplianceID equals row1.ID
                            join row2 in entities.IndustryMappings on row1.ID equals row2.ComplianceId
                            join row3 in entities.CustomerBranchIndustryMappings on row2.IndustryID equals row3.IndustryID
                            join row4 in entities.CustomerBranches on row3.CustomerBranchID equals row4.ID
                            where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                            && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                            && row4.ID == CustomerBranchID
                            select row).Distinct().FirstOrDefault();

                if (data == null)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool visibleSubEventAddButton(int ParentEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {

                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();

                var data = (from row in entities.EventCompAssignDays
                            join row1 in entities.Compliances
                            on row.ComplianceID equals row1.ID
                            where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                            && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                            select row).FirstOrDefault();

                if (data == null)
                {
                    result = false;
                }
                else
                {
                  
                    var data1 = (from row in entities.ComplianceScheduleOns
                                 join row1 in entities.ComplianceTransactions
                                 on row.ComplianceInstanceID equals row1.ComplianceInstanceId
                                 where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                 && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                 && row1.StatusId != 1
                                 select row).FirstOrDefault();

                    if (data1 == null)
                    {
                        result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnId);
                    }
                    else
                    {
                        result = false;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool visibleSubEventUpdateButton(int ParentEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();
                
                var data = (from row in entities.EventCompAssignDays
                            join row1 in entities.Compliances
                            on row.ComplianceID equals row1.ID
                            where row.ParentEventID == ParentEventID && row.IntermediateEventID == 0
                            && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                            select row).FirstOrDefault();

                if (data == null)
                {
                    result = false;
                }
                else
                {

                    var data1 = (from row in entities.ComplianceScheduleOns
                                join row1 in entities.ComplianceTransactions
                                on row.ID equals row1.ComplianceScheduleOnID
                                where row.ParentEventD == ParentEventID && row.IntermediateEventID == 0
                                && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                && row1.StatusId != 1
                                select row).FirstOrDefault();

                    if (data1 == null)
                    {
                        result = EventManagement.CheckSubEventDateAssigned(ParentEventID, SubEventID, EventScheduledOnId);

                        if (result == true)
                        {
                            result = false;
                        }
                        else
                        {
                            result = true;
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool enableIntermediateSubEventUpdateButton(int ParentEventID, int IntermediateEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();

                var data = (from row in entities.ComplianceScheduleOns
                             join row1 in entities.ComplianceTransactions
                             on row.ID equals row1.ComplianceScheduleOnID
                             where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                             && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                             && row1.StatusId != 1
                             select row).FirstOrDefault();

                if (data == null)
                {
                    result = EventManagement.CheckIntermediateSubEventDateAssigned(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId);
                    if (result == false)
                    {
                        result = true;
                    }
                }
                else
                {
                    result = false;
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
     
        protected bool visibleIntermediateSubEventAddButton(int ParentEventID, int IntermediateEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;

                ComplianceDBEntities entities = new ComplianceDBEntities();
                var data = (from row in entities.EventCompAssignDays
                            join row1 in entities.Compliances
                            on row.ComplianceID equals row1.ID
                            where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                            && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                            select row).FirstOrDefault();

               if (data == null)
               {
                   result = false;
               }
               else
               {
                   var data1 = (from row in entities.ComplianceScheduleOns
                               join row1 in entities.ComplianceTransactions
                               on row.ID equals row1.ComplianceScheduleOnID
                               where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                               && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                               && row1.StatusId != 1
                               select row).FirstOrDefault();

                   if (data1 == null)
                   {
                       result = EventManagement.CheckIntermediateSubEventDateAssigned(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId);
                   }
                   else
                   {
                       result = false;
                   }
               }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool visibleTxtgvIntermediateSubEventGridDate(int ParentEventID, int IntermediateEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;

                ComplianceDBEntities entities = new ComplianceDBEntities();
                var data = (from row in entities.EventCompAssignDays
                            join row1 in entities.Compliances
                            on row.ComplianceID equals row1.ID
                            where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                            && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                            select row).FirstOrDefault();

                if (data == null)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool visibleIntermediateSubEventUpdateButton(int ParentEventID,int IntermediateEventID, int SubEventID, int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();

                 var data = (from row in entities.EventCompAssignDays
                            join row1 in entities.Compliances
                            on row.ComplianceID equals row1.ID
                             where row.ParentEventID == ParentEventID && row.IntermediateEventID == IntermediateEventID
                            && row.SubEventID == SubEventID && row1.ComplinceVisible == true
                            select row).FirstOrDefault();

                 if (data == null)
                 {
                     result = false;
                 }
                 else
                 {
                     var data1 = (from row in entities.ComplianceScheduleOns
                                 join row1 in entities.ComplianceTransactions
                                 on row.ID equals row1.ComplianceScheduleOnID
                                 where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                 && row.SubEventID == SubEventID && row.EventScheduledOnID == EventScheduledOnId
                                 && row1.StatusId != 1
                                 select row).FirstOrDefault();

                     if (data1 == null)
                     {
                         result = EventManagement.CheckIntermediateSubEventDateAssigned(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnId);

                         if (result == true)
                         {
                             result = false;
                         }
                         else
                         {
                             result = true;
                         }
                     }
                     else
                     {
                         result = false;
                     }
                 }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
       
        protected void AddInstancesSortImage(int columnIndex, GridViewRow headerRow)
        {
            try
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../Images/SortAsc.gif";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../Images/SortDesc.gif";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindParentEventData(int ParentEventID)
        {
            try
            {
                ComplianceDBEntities entities = new ComplianceDBEntities();
                var ParentEvent = entities.SP_GetParentActivatedEvent(ParentEventID, Convert.ToInt32(Session["EventScheduledOnID"]), Type).ToList();
                gvParentGrid.DataSource = ParentEvent;
                gvParentGrid.DataBind();
  
                upOptionalCompliances.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gvParentGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                //TextBox txtDate = (TextBox)e.Row.FindControl("txtgvParentGridDate");
                //if (txtDate.Text != null)
                //{
                //    if (txtDate.Text == "")
                //    {
                //        //string dt =Convert.ToString(Convert.ToDateTime(Session["HeldOn"]).Day + "-" + Convert.ToDateTime(Session["HeldOn"]).Month + "-" + Convert.ToDateTime(Session["HeldOn"]).Year);
                //        DateTime dt = DateTime.ParseExact(Convert.ToString(Session["HeldOn"]), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                //        txtDate.Text = dt.ToString("dd-MM-yyyy");
                //    }
                //}

                int ParentEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                glParentID = ParentEventID;
                ViewState["ParentEventID"] = ParentEventID;

                ComplianceDBEntities entities = new ComplianceDBEntities();
                GridView gv1 = (GridView)e.Row.FindControl("gvParentToComplianceGrid");
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);
                //var compliance = entities.SP_GetParentEventToComplianceAssignDays(ParentEventID, Type).ToList();
                var compliance = entities.SP_GetParentEventToComplianceAssignWithShortNoticeDays(ParentEventID,Convert.ToInt32(Session["EventScheduledOnID"]), Type, CustomerBranchID).ToList();
                gv1.DataSource = compliance;
                gv1.DataBind();

                GridView gv = (GridView)e.Row.FindControl("gvChildGrid");
                string type = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

                var SubEvent = entities.SP_GetActivatedSubEvent(ParentEventID, Convert.ToInt32(Session["EventScheduledOnID"]), Type).ToList();
                gv.DataSource = SubEvent;
                gv.DataBind();

                GridView gv2 = (GridView)e.Row.FindControl("gvIntermediateGrid");
                string type2 = Convert.ToString(gvParentGrid.DataKeys[e.Row.RowIndex]["Type"]);

                var Intermediate = entities.SP_GetActivatedIntermediateEvent(ParentEventID, Convert.ToInt32(Session["EventScheduledOnID"]), Type).ToList();
                gv2.DataSource = Intermediate;
                gv2.DataBind();
            }
        }

        protected void gvIntermediateSubEventGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                    GridView gv = (GridView)e.Row.FindControl("gvIntermediateComplainceGrid");
                    int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                    glsubEventID = SubEventID;
                    GridView childGrid1 = (GridView)sender;
                    int intermediateEventID = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                    ComplianceDBEntities entities = new ComplianceDBEntities();
                    int parentEventID = Convert.ToInt32(Session["eventId"]);
                    int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);

                    // var Compliance = entities.SP_GetIntermediateComplianceAssignDays(parentEventID, intermediateEventID, SubEventID, Type).ToList();
                    var Compliance = entities.SP_GetIntermediateComplianceAssignWithShortNoticeDays(parentEventID, intermediateEventID, SubEventID,Convert.ToInt32(Session["EventScheduledOnID"]), Type, CustomerBranchID).ToList();
                    gv.DataSource = Compliance;
                    gv.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gvIntermediateGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                GridView gv = (GridView)e.Row.FindControl("gvIntermediateSubEventGrid");
                int IntermediateEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                glIntermediateID = IntermediateEventID;
                GridView childGrid1 = (GridView)sender;
                int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                ComplianceDBEntities entities = new ComplianceDBEntities();

                var Compliance = entities.SP_GetActivatedIntermediateSubEvent(IntermediateEventID, Convert.ToInt32(Session["EventScheduledOnID"]), Parentid, Type).ToList();
                gv.DataSource = Compliance;
                gv.DataBind();
            }
        }

        protected void gvChildGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

                GridView gv = (GridView)e.Row.FindControl("gvComplianceGrid");
                int SubEventID = Convert.ToInt32(e.Row.Cells[1].Text);
                glsubEventID = SubEventID;
                GridView childGrid1 = (GridView)sender;
                //Retreiving the GridView DataKey Value
                int Parentid = Convert.ToInt32(childGrid1.DataKeys[e.Row.RowIndex].Value.ToString());
                ComplianceDBEntities entities = new ComplianceDBEntities();

                //var Compliance = entities.SP_GetComplianceWithDays(Parentid, SubEventID, Type).ToList();
                int CustomerBranchID = Convert.ToInt32(Session["Rbranch"]);

                var Compliance = entities.SP_GetComplianceWithShortNoticeDays(Parentid, SubEventID, Convert.ToInt32(Session["EventScheduledOnID"]),Type, CustomerBranchID).ToList();
                gv.DataSource = Compliance;
                gv.DataBind();
            }
        }

        protected void gvParentToComplianceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int CompliaceID = Convert.ToInt32(e.Row.Cells[1].Text);

                string type = Business.EventManagement.CheckEventComplianceType(CompliaceID);

                Label lblParentStatus = (e.Row.FindControl("lblParentStatus") as Label);

                if (type == "Informational")
                {
                    e.Row.ForeColor = Color.FromName("#bbbbbb");
                    lblParentStatus.Visible = false;
                }
                else if (type == "Actionable")
                {
                    e.Row.ForeColor = Color.FromName("#6A5ACD");
                    lblParentStatus.Visible = true;
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, 0, 0, CompliaceID, Convert.ToInt32(Session["EventScheduledOnID"]));
                    if (transcomplete == true)
                    {
                        lblParentStatus.Visible = true;
                    }
                    else
                    {
                        lblParentStatus.Visible = false;
                    }
                }
                else
                {
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, 0, 0, CompliaceID, Convert.ToInt32(Session["EventScheduledOnID"]));
                    if (transcomplete == true)
                    {
                        lblParentStatus.Visible = true;
                    }
                    else
                    {
                        lblParentStatus.Visible = false;
                    }
                }

            }
        }

        protected void gvComplianceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int CompliaceID = Convert.ToInt32(e.Row.Cells[0].Text);

                string type = Business.EventManagement.CheckEventComplianceType(CompliaceID);


                Label lblStatus = (e.Row.FindControl("lblStatus") as Label);

                if (type == "Informational")
                {
                    e.Row.ForeColor = Color.FromName("#bbbbbb");
                    lblStatus.Visible = false;
                }
                else if (type == "Actionable")
                {
                    e.Row.ForeColor = Color.FromName("#6A5ACD");
                    lblStatus.Visible = false;
                    lblStatus.Visible = true;
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, 0, glsubEventID, CompliaceID, Convert.ToInt32(Session["EventScheduledOnID"]));
                    if (transcomplete == true)
                    {
                        lblStatus.Visible = true;
                    }
                    else
                    {
                        lblStatus.Visible = false;
                    }
                }
                else
                {
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, 0, glsubEventID, CompliaceID, Convert.ToInt32(Session["EventScheduledOnID"]));
                    if (transcomplete == true)
                    {
                        lblStatus.Visible = true;
                    }
                    else
                    {
                        lblStatus.Visible = false;
                    }
                }

            }
        }

        protected bool CheckCompleteTransactionComplete(int ParentEventID, int IntermediateEventID, int SubEventID,int ComplianceID,int EventScheduledOnId)
        {
            try
            {
                bool result = false;
                ComplianceDBEntities entities = new ComplianceDBEntities();

                var ComplainceTransCompleteList = (from row in entities.ComplianceScheduleOns
                                                   join row1 in entities.ComplianceTransactions
                                                   on row.ID equals row1.ComplianceScheduleOnID
                                                   join row2 in entities.ComplianceInstances
                                                   on row1.ComplianceInstanceId equals row2.ID
                                                   where row.ParentEventD == ParentEventID && row.IntermediateEventID == IntermediateEventID
                                                   && row.SubEventID == SubEventID && row2.ComplianceId == ComplianceID && row.EventScheduledOnID == EventScheduledOnId
                                                   && (row1.StatusId == 4 || row1.StatusId == 5)
                                                   select row).ToList();

                if (ComplainceTransCompleteList.Count >= 1)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected void gvIntermediateComplainceGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int CompliaceID = Convert.ToInt32(e.Row.Cells[0].Text);

                string type = Business.EventManagement.CheckEventComplianceType(CompliaceID);

                Label lblIntermediateStatus = (e.Row.FindControl("lblIntermediateStatus") as Label);

                if (type == "Informational")
                {
                    e.Row.ForeColor = Color.FromName("#bbbbbb");
                    lblIntermediateStatus.Visible = false;
                }
                else if (type == "Actionable")
                {
                    e.Row.ForeColor = Color.FromName("#6A5ACD");
                    lblIntermediateStatus.Visible = true;
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, glIntermediateID, glsubEventID, CompliaceID, Convert.ToInt32(Session["EventScheduledOnID"]));
                    if (transcomplete == true)
                    {
                        lblIntermediateStatus.Visible = true;
                    }
                    else
                    {
                        lblIntermediateStatus.Visible = false;
                    }
                }
                else
                {
                    Boolean transcomplete = CheckCompleteTransactionComplete(glParentID, glIntermediateID, glsubEventID, CompliaceID, Convert.ToInt32(Session["EventScheduledOnID"]));
                    if (transcomplete == true)
                    {
                        lblIntermediateStatus.Visible = true;
                    }
                    else
                    {
                        lblIntermediateStatus.Visible = false;
                    }
                }

            }
        }

        protected void gvParentGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    int ParentEventID = Convert.ToInt32(gvParentGrid.Rows[e.RowIndex].Cells[1].Text);
                     
                    int EventScheduledOnID = Convert.ToInt32(Session["EventScheduledOnID"]);

                    ViewState["interIndex"] = 0;
                    ViewState["RowIndex"] = e.RowIndex;
                    ViewState["ParentEventID"] = ParentEventID;
                    ViewState["IntermediateEventID"] = 0;
                    ViewState["SubEventID"] = 0;
                    ViewState["EventScheduledOnID"] = EventScheduledOnID;

                    //Check shorter notice Compliance
                    List<long> ShorterNoticeComplianceList = Business.ComplianceManagement.GetShorterNotice();
                    Boolean shorterFlag = false;
                    List<long> lstShortCompliance = new List<long>();
                    foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                    {
                        int ComplinaceID = Convert.ToInt32(Eventrow.Cells[0].Text);
                        bool ExistShorterNotice = ShorterNoticeComplianceList.Contains(ComplinaceID);

                        if (ExistShorterNotice == true)
                        {
                            shorterFlag = true;
                            lstShortCompliance.Add(ComplinaceID);
                            //break;
                        }
                    }

                    if (shorterFlag == true)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divShortNotice", "$(\"#divShortNotice\").dialog('open')", true);
                    }
                    else
                    {

                        foreach (GridViewRow Eventrow in gvParentGrid.Rows)
                        {
                            var textbox2 = Eventrow.FindControl("txtgvParentGridDate") as TextBox;
                            DateTime Date = DateTime.ParseExact(textbox2.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            // int ParentEventID = Convert.ToInt32(Eventrow.Cells[1].Text);
                            EventAssignDate eventAssignDate = new EventAssignDate()
                            {
                                ParentEventID = ParentEventID,
                                EventScheduleOnID = EventScheduledOnID,
                                IntermediateEventID = 0,
                                SubEventID = 0,
                                Date = Date,
                                IsActive = true,
                                CreatedDate = DateTime.Now,
                                CreatedBy = Convert.ToInt32(Session["userID"]),
                                Type = Type,
                            };
                            Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                            GridView gvEvenToCompliance = Eventrow.FindControl("gvParentToComplianceGrid") as GridView;
                            // Direct Compliance 
                            foreach (GridViewRow EvenToCompliancerow in gvEvenToCompliance.Rows)
                            {
                                int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[1].Text);

                                Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);

                                if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                                {
                                    string days = Convert.ToString(EvenToCompliancerow.Cells[3].Text);
                                    int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(Session["Rbranch"])).ID);
                                    Boolean FlgCheck = false;
                                    FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, 0, 0, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                    if (FlgCheck == false)
                                    {
                                        EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, 0, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                                    }
                                    else
                                    {
                                        int IncludeDays = 0;
                                        int Days = Convert.ToInt32(days);
                                        if (Days == 0)
                                        {
                                            IncludeDays = 0;
                                        }
                                        else
                                        {
                                            if (Days > 0)
                                            {
                                                IncludeDays = Days - 1;
                                            }
                                            else
                                            {
                                                IncludeDays = Days + 1;
                                            }
                                        }
                                        DateTime UpdatedDate = Date.AddDays(IncludeDays);
                                        EventManagement.UpdateReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                                        EventManagement.UpdateComplianceSheduleOnDates(ParentEventID, 0, 0, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                                    }
                                }
                            }
                        }
                        BindParentEventData(Convert.ToInt32(ParentEventID));
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Saved Successfully";
                    }
                }
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        protected void gvChildGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    GridView childGrid = (GridView)gvParentGrid.Rows[0].Cells[0].FindControl("gvChildGrid");

                    int ParentEventID = Convert.ToInt32(childGrid.DataKeys[e.RowIndex].Value.ToString());

                    int SubEventID = Convert.ToInt32(childGrid.Rows[e.RowIndex].Cells[1].Text);

                    int EventScheduledOnID = Convert.ToInt32(Session["EventScheduledOnID"]);

                    ViewState["interIndex"] = 0;
                    ViewState["RowIndex"] = e.RowIndex;
                    ViewState["ParentEventID"] = ParentEventID;
                    ViewState["IntermediateEventID"] = 0;
                    ViewState["SubEventID"] = SubEventID;
                    ViewState["EventScheduledOnID"] = EventScheduledOnID;

                    GridView gvEvenToCompliance = (GridView)childGrid.Rows[e.RowIndex].Cells[0].FindControl("gvComplianceGrid");

                    //Check shorter notice Compliance
                    List<long> ShorterNoticeComplianceList = Business.ComplianceManagement.GetShorterNotice();
                    Boolean shorterFlag = false;
                    List<long> lstShortCompliance = new List<long>();
                    foreach (GridViewRow EvenToCompliancerow in gvEvenToCompliance.Rows)
                    {
                        int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);
                    
                        bool ExistShorterNotice = ShorterNoticeComplianceList.Contains(ComplinaceID);

                        if (ExistShorterNotice == true)
                        {
                            shorterFlag = true;
                            lstShortCompliance.Add(ComplinaceID);
                            //break;
                        }
                    }

                    if (shorterFlag == true)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divShortNotice", "$(\"#divShortNotice\").dialog('open')", true);
                    }
                    else
                    {
                        var textbox2 = childGrid.Rows[e.RowIndex].FindControl("txtgvChildGridDate") as TextBox;
                        DateTime Date = DateTime.ParseExact(textbox2.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        EventAssignDate eventAssignDate = new EventAssignDate()
                        {
                            ParentEventID = ParentEventID,
                            EventScheduleOnID = EventScheduledOnID,
                            IntermediateEventID = 0,
                            SubEventID = SubEventID,
                            Date = Date,
                            IsActive = true,
                            CreatedDate = DateTime.Now,
                            CreatedBy = Convert.ToInt32(Session["userID"]),
                            Type = Type,
                        };
                        Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                        // Child Compliance 
                        foreach (GridViewRow EvenToCompliancerow in gvEvenToCompliance.Rows)
                        {
                            int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);

                            Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                            if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                            {
                                int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(Session["Rbranch"])).ID);

                                string days = Convert.ToString(EvenToCompliancerow.Cells[2].Text);
                                Boolean FlgCheck = false;
                                FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, 0, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                if (FlgCheck == false)
                                {
                                    EventManagement.GenerateEventComplianceScheduele(ParentEventID, 0, SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                                }
                                else
                                {
                                    int IncludeDays = 0;
                                    int Days = Convert.ToInt32(days);
                                    if (Days == 0)
                                    {
                                        IncludeDays = 0;
                                    }
                                    else
                                    {
                                        if (Days > 0)
                                        {
                                            IncludeDays = Days - 1;
                                        }
                                        else
                                        {
                                            IncludeDays = Days + 1;
                                        }
                                    }
                                    DateTime UpdatedDate = Date.AddDays(IncludeDays);
                                    EventManagement.UpdateReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                                    EventManagement.UpdateComplianceSheduleOnDates(ParentEventID, 0, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                                }
                            }
                        }
                        BindParentEventData(Convert.ToInt32(ParentEventID));
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Saved Successfully.";
                    }
                }
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void gvIntermediateSubEventGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    GridViewRow masterrow = (GridViewRow)(sender as Control).Parent.Parent;
            
                    int interIndex = masterrow.RowIndex;
                 
                    GridView intermediateGrid = (GridView)gvParentGrid.Rows[0].Cells[0].FindControl("gvIntermediateGrid");

                    int ParentEventID = Convert.ToInt32(intermediateGrid.DataKeys[interIndex].Value.ToString());

                    GridView childGrid = (GridView)intermediateGrid.Rows[interIndex].Cells[0].FindControl("gvIntermediateSubEventGrid");
                                     
                    int SubEventID = Convert.ToInt32(childGrid.Rows[e.RowIndex].Cells[1].Text);

                    int lblIntermediateEventID = Convert.ToInt32(childGrid.DataKeys[e.RowIndex].Value.ToString());
                    
                    int EventScheduledOnID = Convert.ToInt32(Session["EventScheduledOnID"]);

                    ViewState["interIndex"] = interIndex;
                    ViewState["RowIndex"] = e.RowIndex;
                    ViewState["ParentEventID"] = ParentEventID;
                    ViewState["IntermediateEventID"] = lblIntermediateEventID;
                    ViewState["SubEventID"] = SubEventID;
                    ViewState["EventScheduledOnID"] = EventScheduledOnID;

                    GridView gvIntermediateComplainceGrid = (GridView)childGrid.Rows[e.RowIndex].Cells[0].FindControl("gvIntermediateComplainceGrid");

                    //Check shorter notice Compliance
                    List<long> ShorterNoticeComplianceList = Business.ComplianceManagement.GetShorterNotice();
                    Boolean shorterFlag = false;
                    List<long> lstShortCompliance = new List<long>();
                    foreach (GridViewRow EvenToCompliancerow in gvIntermediateComplainceGrid.Rows)
                    {
                        int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);
                        bool ExistShorterNotice = ShorterNoticeComplianceList.Contains(ComplinaceID);

                        if (ExistShorterNotice == true)
                        {
                            shorterFlag = true;
                            lstShortCompliance.Add(ComplinaceID);
                            //break;
                        }
                    }

                    if (shorterFlag == true)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divShortNotice", "$(\"#divShortNotice\").dialog('open')", true);
                    }
                    else
                    {

                        var textbox2 = childGrid.Rows[e.RowIndex].FindControl("txtgvIntermediateSubEventGridDate") as TextBox;
                        DateTime Date = DateTime.ParseExact(textbox2.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        EventAssignDate eventAssignDate = new EventAssignDate()
                        {
                            ParentEventID = ParentEventID,
                            EventScheduleOnID = EventScheduledOnID,
                            IntermediateEventID = Convert.ToInt32(lblIntermediateEventID),
                            SubEventID = SubEventID,
                            Date = Date,
                            IsActive = true,
                            CreatedDate = DateTime.Now,
                            CreatedBy = Convert.ToInt32(Session["userID"]),
                            Type = Type,
                        };
                        Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                        //GridView gvIntermediateComplainceGrid = (GridView)childGrid.Rows[e.RowIndex].Cells[0].FindControl("gvIntermediateComplainceGrid");
                        // Intermediate Sub event Compliance 
                        foreach (GridViewRow EvenToCompliancerow in gvIntermediateComplainceGrid.Rows)
                        {
                            int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);

                             Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);

                             if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                             {
                                 int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(Session["Rbranch"])).ID);

                                 string days = Convert.ToString(EvenToCompliancerow.Cells[2].Text);
                                 Boolean FlgCheck = false;
                                 FlgCheck = EventManagement.CheckEventComplianceAssigned(ParentEventID, lblIntermediateEventID, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                 if (FlgCheck == false)
                                 {
                                     EventManagement.GenerateEventComplianceScheduele(ParentEventID, Convert.ToInt32(lblIntermediateEventID), SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                                 }
                                 else
                                 {
                                     int IncludeDays = 0;
                                    int Days = Convert.ToInt32(days);
                                     if (Days == 0)
                                     {
                                         IncludeDays = 0;
                                     }
                                     else
                                     {
                                         if (Days > 0)
                                         {
                                             IncludeDays = Days - 1;
                                         }
                                         else
                                         {
                                             IncludeDays = Days + 1;
                                         }
                                     }
                                     DateTime UpdatedDate = Date.AddDays(IncludeDays);
                                     EventManagement.UpdateReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                                     EventManagement.UpdateComplianceSheduleOnDates(ParentEventID, Convert.ToInt32(lblIntermediateEventID), SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                                 }
                             }
                        }
                        BindParentEventData(Convert.ToInt32(ParentEventID));
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Saved Successfully";
                    }
                }
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
              
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void SaveShortNotice()
        {
            try
            {
                int interIndex = Convert.ToInt32(ViewState["interIndex"]);
                int RowIndex = Convert.ToInt32(ViewState["RowIndex"]);
                int ParentEventID = Convert.ToInt32(ViewState["ParentEventID"]);
                int IntermediateEventID = Convert.ToInt32(ViewState["IntermediateEventID"]);
                int SubEventID = Convert.ToInt32(ViewState["SubEventID"]);
                int EventScheduledOnID = Convert.ToInt32(ViewState["EventScheduledOnID"]);

                GridView complianceGrid = new GridView();
                GridView childGrid = new GridView();
                Button btnSave = new Button();
                Button btnUpdate = new Button();
                var textbox2 = new TextBox();
                Boolean UpdateFlag = false;


                if (ParentEventID != 0 && IntermediateEventID == 0 && SubEventID == 0) //Parent
                {
                    complianceGrid = (GridView)childGrid.Rows[RowIndex].Cells[0].FindControl("gvParentToComplianceGrid");

                    btnSave = (Button)childGrid.Rows[RowIndex].Cells[0].FindControl("BtnAddgvParentGrid");
                    btnUpdate = (Button)childGrid.Rows[RowIndex].Cells[0].FindControl("BtnUpdategvParentGrid");

                    if (btnSave.Visible == true)
                    {
                        UpdateFlag = false;
                    }
                    else if (btnUpdate.Visible == true)
                    {
                        UpdateFlag = true;
                    }

                    textbox2 = childGrid.Rows[RowIndex].FindControl("txtgvParentGridDate") as TextBox;
                }
                else if (ParentEventID != 0 && IntermediateEventID == 0 && SubEventID != 0) //ParentToSub
                {
                    childGrid = (GridView)gvParentGrid.Rows[0].Cells[0].FindControl("gvChildGrid");

                    textbox2 = childGrid.Rows[RowIndex].FindControl("txtgvChildGridDate") as TextBox;

                    btnSave = (Button)childGrid.Rows[RowIndex].Cells[0].FindControl("BtnAddgvChildGrid");
                    btnUpdate = (Button)childGrid.Rows[RowIndex].Cells[0].FindControl("BtnUpdategvChildGrid");

                    if (btnSave.Visible == true)
                    {
                        UpdateFlag = false;
                    }
                    else if (btnUpdate.Visible == true)
                    {
                        UpdateFlag = true;
                    }

                    complianceGrid = (GridView)childGrid.Rows[RowIndex].Cells[0].FindControl("gvComplianceGrid");

                }
                else if (ParentEventID != 0 && IntermediateEventID != 0 && SubEventID != 0) //ParentToIntermediateToSub
                {
                    GridView intermediateGrid = (GridView)gvParentGrid.Rows[0].Cells[0].FindControl("gvIntermediateGrid");

                    childGrid = (GridView)intermediateGrid.Rows[interIndex].Cells[0].FindControl("gvIntermediateSubEventGrid");

                    complianceGrid = (GridView)childGrid.Rows[RowIndex].Cells[0].FindControl("gvIntermediateComplainceGrid");

                    btnSave = (Button)childGrid.Rows[RowIndex].Cells[0].FindControl("BtnAddIntermediateSubEventGrid");
                    btnUpdate = (Button)childGrid.Rows[RowIndex].Cells[0].FindControl("BtnUpdateIntermediateSubEventGrid");

                    if (btnSave.Visible == true)
                    {
                        UpdateFlag = false;
                    }
                    else if (btnUpdate.Visible == true)
                    {
                        UpdateFlag = true;
                    }

                    textbox2 = childGrid.Rows[RowIndex].FindControl("txtgvIntermediateSubEventGridDate") as TextBox;
                }


                DateTime Date = DateTime.ParseExact(textbox2.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                EventAssignDate eventAssignDate = new EventAssignDate()
                {
                    ParentEventID = ParentEventID,
                    EventScheduleOnID = EventScheduledOnID,
                    IntermediateEventID = IntermediateEventID,
                    SubEventID = SubEventID,
                    Date = Date,
                    IsActive = true,
                    CreatedDate = DateTime.Now,
                    CreatedBy = Convert.ToInt32(Session["userID"]),
                    Type = Type,
                };
                Business.EventManagement.UpdateEventAssignDates(eventAssignDate);

                // Child Compliance 

                foreach (GridViewRow EvenToCompliancerow in complianceGrid.Rows)
                {
                    int ComplinaceID = Convert.ToInt32(EvenToCompliancerow.Cells[0].Text);

                     Boolean VisibleFlag = Business.EventManagement.CheckComplianceVisible(ComplinaceID);
                     if (VisibleFlag == true)  //If complaince visible true then Schudule generated
                     {
                         int ComplianceInstanceID = Convert.ToInt32(EventManagement.GetComplianceInstance(ComplinaceID, Convert.ToInt32(Session["Rbranch"])).ID);
                         string days = Convert.ToString(EvenToCompliancerow.Cells[2].Text);
                         Boolean FlgCheck = false;

                         if (rbtnShortnotice.SelectedValue.Equals("1")) //Short Notice Selected 
                         {
                             FlgCheck = EventManagement.CheckShortnoticeCompliance(ComplinaceID);
                             if (FlgCheck == false)
                             {
                                 // Normal Schedule Save
                                 Boolean flgSheduelePresetnt = false;
                                 flgSheduelePresetnt = EventManagement.CheckEventComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);

                                 if (flgSheduelePresetnt == false) //Save
                                 {
                                     EventManagement.GenerateEventComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                                 }
                                 else //Update
                                 {
                                     int IncludeDays = 0;
                                    int Days = Convert.ToInt32(days);
                                     if (Days == 0)
                                     {
                                         IncludeDays = 0;
                                     }
                                     else
                                     {
                                         if (Days > 0)
                                         {
                                             IncludeDays = Days - 1;
                                         }
                                         else
                                         {
                                             IncludeDays = Days + 1;
                                         }
                                     }
                                     DateTime UpdatedDate = Date.AddDays(IncludeDays);
                                     EventManagement.UpdateReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                                     EventManagement.UpdateComplianceSheduleOnDates(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                                 }
                             }
                             else
                             {
                                 // Short Notice Schedule 
                                 Boolean flgShortNoticeSheduelePresetnt = false;
                                 flgShortNoticeSheduelePresetnt = EventManagement.CheckShortnoticePresent(ParentEventID, IntermediateEventID, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);
                                 int shortnoticedays = Convert.ToInt32(txtShortNoticeDays.Text);
                                 int shortdays = shortnoticedays * -1;
                                 if (flgShortNoticeSheduelePresetnt == false) //SaveShortNotice 
                                 {
                                     EventManagement.SaveShortNoticeDays(ParentEventID, IntermediateEventID, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID, shortdays);
                                     if (UpdateFlag == false) //SaveSchedule
                                     {
                                         EventManagement.GenerateEventComplianceSchedueleForShortNotice(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, shortdays);
                                     }
                                     else if (UpdateFlag == true) //Update Schedule
                                     {
                                         int IncludeDays = 0;
                                         if (shortdays == 0 || shortdays == -0)
                                         {
                                             IncludeDays = 0;
                                         }
                                         else
                                         {
                                             if (shortdays > 0)
                                             {
                                                 IncludeDays = shortdays - 1;
                                             }
                                             else
                                             {
                                                 IncludeDays = shortdays + 1;
                                             }
                                         }
                                         DateTime UpdatedDate = Date.AddDays(IncludeDays);
                                         EventManagement.UpdateReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                                         EventManagement.UpdateComplianceSheduleOnDates(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);

                                     }
                                 }
                                 else //Update
                                 {
                                     int IncludeDays = 0;
                                     if (shortdays == 0 || shortdays == -0)
                                     {
                                         IncludeDays = 0;
                                     }
                                     else
                                     {
                                         if (shortdays > 0)
                                         {
                                             IncludeDays = shortdays - 1;
                                         }
                                         else
                                         {
                                             IncludeDays = shortdays + 1;
                                         }
                                     }
                                     DateTime UpdatedDate = Date.AddDays(IncludeDays);
                                     EventManagement.UpdateShortNotice(ParentEventID, IntermediateEventID, SubEventID, ComplinaceID, EventScheduledOnID, ComplianceInstanceID, shortdays);
                                     EventManagement.UpdateReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                                     EventManagement.UpdateComplianceSheduleOnDates(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                                 }

                             }
                         }
                         else //Short Notice Not selected 
                         {
                             // Normal Schedule Save
                             Boolean flgSheduelePresetnt = false;
                             flgSheduelePresetnt = EventManagement.CheckEventComplianceAssigned(ParentEventID, IntermediateEventID, SubEventID, Type, ComplinaceID, EventScheduledOnID, ComplianceInstanceID);

                             if (flgSheduelePresetnt == false) //Save
                             {
                                 EventManagement.GenerateEventComplianceScheduele(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, Date, ComplinaceID, ComplianceInstanceID, days);
                             }
                             else //Update
                             {
                                 int IncludeDays = 0;
                                int Days = Convert.ToInt32(days);
                                 if (Days == 0)
                                 {
                                     IncludeDays = 0;
                                 }
                                 else
                                 {
                                     if (Days > 0)
                                     {
                                         IncludeDays = Days - 1;
                                     }
                                     else
                                     {
                                         IncludeDays = Days + 1;
                                     }
                                 }
                                 DateTime UpdatedDate = Date.AddDays(IncludeDays);
                                 EventManagement.UpdateReminderSheduleOnDates(EventScheduledOnID, ComplinaceID, UpdatedDate, ComplianceInstanceID, Date);
                                 EventManagement.UpdateComplianceSheduleOnDates(ParentEventID, IntermediateEventID, SubEventID, EventScheduledOnID, ComplinaceID, ComplianceInstanceID, UpdatedDate);
                             }
                         }
                     }
                }
                txtShortNoticeDays.Text = "";
                BindParentEventData(Convert.ToInt32(ParentEventID));
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Saved Successfully.";

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnBackMyEvent_Click(object sender, EventArgs e)
        {
            try
            {
                Session["MyEventBack"] = "true";
                Response.Redirect("~/Common/ComplianceDashboard.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveShortNotice_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtShortNoticeDays.Text == "" && rbtnShortnotice.SelectedValue.Equals("1"))
                {
                    lblMessage.Text = "Please enter short notice days";
                }
                else
                {
                    lblMessage.Text = "";
                    SaveShortNotice();
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divShortNotice", "$(\"#divShortNotice\").dialog('close')", true);
                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnShortClose_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "divShortNotice", "$(\"#divShortNotice\").dialog('close')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rbtnShortnotice_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                if (rbtnShortnotice.SelectedValue.Equals("1"))
                {
                    txtShortNoticeDays.Text = "";
                    divDays.Visible = true;
                }
                else
                {
                    txtShortNoticeDays.Text = "";
                    divDays.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}