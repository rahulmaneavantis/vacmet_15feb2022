﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    
    public partial class calendarstandalone : System.Web.UI.Page
    {
        public static string CalendarDate;
        public static DateTime CalendarTodayOrNextDate;
        public static string date = "";
        protected static string CalenderDateString;
        protected static bool IsApprover = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["type"] != "undefined")
                {
                    try
                    {
                        string type = Convert.ToString(Request.QueryString["type"]);
                        string month = Convert.ToString(Request.QueryString["month"]);
                        string year = Convert.ToString(Request.QueryString["year"]);

                        GetCalender(type, month, year);
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "fcal", "fcal('" + CalendarTodayOrNextDate.ToString("yyyy-MM-dd") + "');", true);
                    }
                    catch (Exception ex) { }
                }
            }
        }
        public DataTable fetchData(int customerid, int userid, string Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {                
                DataTable statutorydatatable = new DataTable();                
                DataTable Internaldatatable = new DataTable();
                DataTable mgmtstatutorydatatable = new DataTable();
                DataTable mgmtInternaldatatable = new DataTable();

                string isallorsi = string.Empty;
                if (Type == "0")
                {
                    isallorsi = "SI";
                }
                else
                {
                    isallorsi = "All";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["m"]) && Request.QueryString["m"].ToString() == "8")
                {

                    #region Management
                    if (AuthenticationHelper.Role.Equals("MGMT"))
                    {
                        IsApprover = false;
                    }
                    else
                    {
                        var GetApprover = (entities.Sp_GetApproverUsers(AuthenticationHelper.UserID)).ToList();
                        if (GetApprover.Count > 0)
                        {
                            IsApprover = true;
                        }
                    }

                    if (IsApprover == false)
                    {
                        #region Statutory
                        if (Convert.ToString(Request.QueryString["dhead"]) == "1")
                        {
                            #region Department Head
                            entities.Database.CommandTimeout = 180;
                            var mgmtQueryStatutory = (from row in entities.SP_ComplianceCalender_DeptHead(customerid, userid, isallorsi)
                                                      select row).ToList();
                            if (mgmtQueryStatutory.Count > 0)
                            {
                                mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                            }
                            if (mgmtQueryStatutory.Count > 0)
                            {
                                var Statutory = (from row in mgmtQueryStatutory
                                                 select new
                                                 {
                                                     ScheduledOn = row.PerformerScheduledOn,
                                                     ComplianceStatusID = row.ComplianceStatusID,
                                                     RoleID = row.RoleID,
                                                     ScheduledOnID = row.ScheduledOnID,
                                                     CustomerBranchID = row.CustomerBranchID,
                                                 }).Distinct().ToList();
                                mgmtstatutorydatatable = Statutory.ToDataTable();
                            }
                            #endregion
                        }
                        else
                        {
                            #region Management              

                            var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                            string cashTimeval = string.Empty;
                            string cashstatutoryval = string.Empty;
                            //string cashinternalval = string.Empty;
                            if (objlocal == "Local")
                            {
                                cashTimeval = "MCA_CHE" + AuthenticationHelper.UserID;
                                cashstatutoryval = "MS_PSD" + AuthenticationHelper.UserID;
                                //cashinternalval = "MI_PSD" + AuthenticationHelper.UserID;
                            }
                            else
                            {
                                cashTimeval = "MCACHE" + AuthenticationHelper.UserID;
                                cashstatutoryval = "MSPSD" + AuthenticationHelper.UserID;
                                //cashinternalval = "MIPSD" + AuthenticationHelper.UserID;
                            }

                            //string cashstatutoryval = "MSPSD" + AuthenticationHelper.UserID;
                            //string cashTimeval = "MCACHE" + AuthenticationHelper.UserID;
                            List<SP_GetManagementCompliancesSummary_Result> MasterManagementCompliancesSummaryQuery = new List<SP_GetManagementCompliancesSummary_Result>();
                            try
                            {
                                if (isallorsi == "All")
                                {
                                    #region ALL 
                                    StackExchangeRedisExtensions.Remove(cashstatutoryval);
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                    try
                                    {
                                        StackExchangeRedisExtensions.SetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval, MasterManagementCompliancesSummaryQuery);
                                        if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                        {
                                            StackExchangeRedisExtensions.Remove(cashTimeval);
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                        else
                                        {
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        LogLibrary.WriteErrorLog("SET fetchData MGMT Statutory Error exception :" + cashstatutoryval);
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region SI

                                    StackExchangeRedisExtensions.Remove(cashstatutoryval);
                                    if (StackExchangeRedisExtensions.KeyExists(cashstatutoryval))
                                    {
                                        try
                                        {
                                            MasterManagementCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval);
                                        }
                                        catch (Exception)
                                        {
                                            LogLibrary.WriteErrorLog("GET fetchData MGMT Statutory Error exception :" + cashstatutoryval);
                                            if (isallorsi == "SI")
                                            {
                                                //MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                                var customizedChecklist = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "MGMT_Checklist");
                                                if (customizedChecklist)
                                                {
                                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                                }
                                                else
                                                {
                                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                                }
                                            }
                                            else
                                            {
                                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (isallorsi == "SI")
                                        {

                                            var customizedChecklist = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "MGMT_Checklist");
                                            if (customizedChecklist)
                                            {
                                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                            }
                                            else
                                            {
                                                MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                            }
                                        }
                                        else
                                        {
                                            MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                        }
                                        try
                                        {
                                            StackExchangeRedisExtensions.SetList<SP_GetManagementCompliancesSummary_Result>(cashstatutoryval, MasterManagementCompliancesSummaryQuery);
                                            if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                            {
                                                StackExchangeRedisExtensions.Remove(cashTimeval);
                                                StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                            }
                                            else
                                            {
                                                StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            LogLibrary.WriteErrorLog("SET fetchData MGMT Statutory Error exception :" + cashstatutoryval);
                                        }

                                    }
                                    #endregion
                                }
                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("KeyExists fetchData MGMT Statutory Error exception :" + cashstatutoryval);
                                if (isallorsi == "SI")
                                {
                                   
                                    var customizedChecklist = CustomerManagement.CheckForClient(Convert.ToInt32(AuthenticationHelper.CustomerID), "MGMT_Checklist");
                                    if (customizedChecklist)
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                    }
                                    else
                                    {
                                        MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                    }
                                }
                                else
                                {
                                    MasterManagementCompliancesSummaryQuery = (entities.SP_GetManagementCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                }
                            }                          

                            //entities.Database.CommandTimeout = 180;
                            //var mgmtQueryStatutory = (from row in entities.ComplianceInstanceTransactionViewCustomerWiseManagement(customerid, userid, isallorsi)
                            //                          select row).ToList();

                            var mgmtQueryStatutory = MasterManagementCompliancesSummaryQuery.ToList();

                            if (mgmtQueryStatutory.Count > 0)
                            {
                                mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                            }
                            if (mgmtQueryStatutory.Count > 0)
                            {
                                var Statutory = (from row in mgmtQueryStatutory
                                                 select new
                                                 {
                                                     ScheduledOn = row.PerformerScheduledOn,
                                                     ComplianceStatusID = row.ComplianceStatusID,
                                                     RoleID = row.RoleID,
                                                     ScheduledOnID = row.ScheduledOnID,
                                                     CustomerBranchID = row.CustomerBranchID,
                                                 }).Distinct().ToList();
                                mgmtstatutorydatatable = Statutory.ToDataTable();
                            }
                            #endregion
                        }
                        #endregion

                        #region Internal
                        if (Convert.ToString(Request.QueryString["dhead"]) == "1")
                        {
                            #region Department Head
                            entities.Database.CommandTimeout = 180;
                            var mgmtQueryInternal = (from row in entities.SP_InternalComplianceCalender_DeptHead(customerid, userid, isallorsi)
                                                     select row).ToList();

                            if (mgmtQueryInternal.Count > 0)
                            {
                                mgmtQueryInternal = mgmtQueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                            }

                            if (mgmtQueryInternal.Count > 0)
                            {
                                var Internal = (from row in mgmtQueryInternal
                                                select new
                                                {
                                                    ScheduledOn = row.InternalScheduledOn,
                                                    ComplianceStatusID = row.InternalComplianceStatusID,
                                                    RoleID = row.RoleID,
                                                    ScheduledOnID = row.InternalScheduledOnID,
                                                    CustomerBranchID = row.CustomerBranchID,
                                                }).Distinct().ToList();
                                mgmtInternaldatatable = Internal.ToDataTable();
                            }
                            #endregion
                        }
                        else
                        {
                            #region Management 
                            var objlocal = ConfigurationManager.AppSettings["WORKINGDIRECTORY"];
                            string cashTimeval = string.Empty;
                            string cashinternalval = string.Empty;
                            if (objlocal == "Local")
                            {
                                cashTimeval = "MCA_CHE" + AuthenticationHelper.UserID;                                
                                cashinternalval = "MI_PSD" + AuthenticationHelper.UserID;
                            }
                            else
                            {
                                cashTimeval = "MCACHE" + AuthenticationHelper.UserID;                                
                                cashinternalval = "MIPSD" + AuthenticationHelper.UserID;
                            }
                            List<SP_GetManagementInternalCompliancesSummary_Result> MasterManagementInternalCompliancesSummaryQuery = new List<SP_GetManagementInternalCompliancesSummary_Result>();                            
                            try
                            {

                                if (isallorsi == "All")
                                {
                                    #region ALL
                                    StackExchangeRedisExtensions.Remove(cashinternalval);
                                    entities.Database.CommandTimeout = 180;
                                    MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                    try
                                    {
                                        StackExchangeRedisExtensions.SetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval, MasterManagementInternalCompliancesSummaryQuery);
                                        if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                        {
                                            StackExchangeRedisExtensions.Remove(cashTimeval);
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                        else
                                        {
                                            StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        LogLibrary.WriteErrorLog("SET fetchData MGMT Internal Error exception :" + cashinternalval);
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region SI
                                    StackExchangeRedisExtensions.Remove(cashinternalval);
                                    if (StackExchangeRedisExtensions.KeyExists(cashinternalval))
                                    {
                                        try
                                        {
                                            MasterManagementInternalCompliancesSummaryQuery = StackExchangeRedisExtensions.GetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval);
                                        }
                                        catch (Exception)
                                        {
                                            LogLibrary.WriteErrorLog("GET fetchData MGMT Internal Error exception :" + cashinternalval);
                                            if (isallorsi == "SI")
                                            {
                                                entities.Database.CommandTimeout = 180;
                                                MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                            }
                                            else
                                            {
                                                entities.Database.CommandTimeout = 180;
                                                MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (isallorsi == "SI")
                                        {
                                            entities.Database.CommandTimeout = 180;
                                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                        }
                                        else
                                        {
                                            entities.Database.CommandTimeout = 180;
                                            MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                        }
                                        try
                                        {
                                            StackExchangeRedisExtensions.SetList<SP_GetManagementInternalCompliancesSummary_Result>(cashinternalval, MasterManagementInternalCompliancesSummaryQuery);
                                            if (StackExchangeRedisExtensions.KeyExists(cashTimeval))
                                            {
                                                StackExchangeRedisExtensions.Remove(cashTimeval);
                                                StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                            }
                                            else
                                            {
                                                StackExchangeRedisExtensions.Set(cashTimeval, DateTime.Now);
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            LogLibrary.WriteErrorLog("SET fetchData MGMT Internal Error exception :" + cashinternalval);
                                        }
                                    }
                                    #endregion
                                }
                            }
                            catch (Exception)
                            {
                                LogLibrary.WriteErrorLog("KeyExists fetchData MGMT Internal Error exception :" + cashinternalval);
                                if (isallorsi == "SI")
                                {
                                    entities.Database.CommandTimeout = 180;
                                    MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMT")).ToList();
                                }
                                else
                                {
                                    entities.Database.CommandTimeout = 180;
                                    MasterManagementInternalCompliancesSummaryQuery = (entities.SP_GetManagementInternalCompliancesSummary(AuthenticationHelper.UserID, (int)AuthenticationHelper.CustomerID, "MGMTALL")).ToList();
                                }
                            }                            
                            var mgmtQueryInternal = MasterManagementInternalCompliancesSummaryQuery.ToList();
                            //var mgmtQueryInternal = (from row in entities.InternalComplianceInstanceTransactionmanagement(customerid, userid, isallorsi)
                            //                         select row).ToList();

                            if (mgmtQueryInternal.Count > 0)
                            {
                                mgmtQueryInternal = mgmtQueryInternal.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                            }

                            if (mgmtQueryInternal.Count > 0)
                            {
                                var Internal = (from row in mgmtQueryInternal
                                                select new
                                                {
                                                    ScheduledOn = row.ScheduledOn,
                                                    ComplianceStatusID = row.ComplianceStatusID,
                                                    RoleID = row.RoleID,
                                                    ScheduledOnID = row.ScheduledOnID,
                                                    CustomerBranchID = row.CustomerBranchID,
                                                }).Distinct().ToList();
                                mgmtInternaldatatable = Internal.ToDataTable();
                            }
                            #endregion
                        }
                        #endregion
                    }
                    else
                    {
                        #region Statutory
                        entities.Database.CommandTimeout = 180;
                        var QueryStatutory = (from row in entities.Sp_ComplianceInstanceTransactionApprover(customerid, userid, isallorsi)
                                              select row).ToList();

                        if (QueryStatutory.Count > 0)
                        {
                            QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        }

                        if (QueryStatutory.Count > 0)
                        {
                            var Statutory = (from row in QueryStatutory
                                             select new
                                             {
                                                 ScheduledOn = row.PerformerScheduledOn,
                                                 ComplianceStatusID = row.ComplianceStatusID,
                                                 RoleID = row.RoleID,
                                                 ScheduledOnID = row.ScheduledOnID,
                                                 CustomerBranchID = row.CustomerBranchID,
                                             }).Distinct().ToList();


                            statutorydatatable = Statutory.ToDataTable();
                        }
                        #endregion

                        #region Internal
                        entities.Database.CommandTimeout = 180;
                        var QueryInternal = (from row in entities.Sp_InternalComplianceInstanceTransactionApprover(customerid, userid, isallorsi)
                                             select row).ToList();
                        if (QueryInternal.Count > 0)
                        {
                            QueryInternal = QueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                        }
                        if (QueryInternal.Count > 0)
                        {

                            var Internal = (from row in QueryInternal
                                            select new
                                            {
                                                ScheduledOn = row.InternalScheduledOn,
                                                ComplianceStatusID = row.InternalComplianceStatusID,
                                                RoleID = row.RoleID,
                                                ScheduledOnID = row.InternalScheduledOnID,
                                                CustomerBranchID = row.CustomerBranchID,
                                            }).Distinct().ToList();

                            Internaldatatable = Internal.ToDataTable();
                        }
                        #endregion
                    }
                    #endregion
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["m"]) && Request.QueryString["m"].ToString() == "3")
                {
                    //performer as well as reviewer
                    //DateTime date = DateTime.Today.AddDays(7);
                    #region Statutory
                    entities.Database.CommandTimeout = 180;
                    var QueryStatutory = (from row in entities.ComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                          select row).ToList();

                    if (QueryStatutory.Count > 0)
                    {
                        QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }

                    if (QueryStatutory.Count > 0)
                    {                                               
                        var Statutory = (from row in QueryStatutory
                                              select new
                                              {
                                                  ScheduledOn = row.PerformerScheduledOn,
                                                  ComplianceStatusID = row.ComplianceStatusID,
                                                  RoleID = row.RoleID,
                                                  ScheduledOnID = row.ScheduledOnID,
                                                  CustomerBranchID = row.CustomerBranchID,
                                              }).Distinct().ToList();

                       
                        statutorydatatable = Statutory.ToDataTable();
                    }
                    #endregion

                    #region Internal
                    entities.Database.CommandTimeout = 180;
                    var QueryInternal = (from row in entities.InternalComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                         select row).ToList();                    
                    if (QueryInternal.Count > 0)
                    {
                        QueryInternal = QueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                    }
                    if (QueryInternal.Count > 0)
                    {
                       
                        var Internal = (from row in QueryInternal
                                         select new
                                         {
                                             ScheduledOn = row.InternalScheduledOn,
                                             ComplianceStatusID = row.InternalComplianceStatusID,
                                             RoleID = row.RoleID,
                                             ScheduledOnID = row.InternalScheduledOnID,
                                             CustomerBranchID = row.CustomerBranchID,
                                         }).Distinct().ToList();
                       
                        Internaldatatable = Internal.ToDataTable();
                    }
                    #endregion                  
                }
                DataTable newtable = new DataTable();
                // Performer or reviewer
                if (statutorydatatable.Rows.Count > 0)
                {
                    newtable.Merge(statutorydatatable);
                }
                if (Internaldatatable.Rows.Count > 0)
                {
                    newtable.Merge(Internaldatatable);
                }

                //management
                if (mgmtstatutorydatatable.Rows.Count > 0)
                {
                    newtable.Merge(mgmtstatutorydatatable);
                }
                if (mgmtInternaldatatable.Rows.Count > 0)
                {
                    newtable.Merge(mgmtInternaldatatable);
                }
                return newtable;
            }
        }        
        public void GetCalender(string type,string month1,string year1)

        {

            DateTime dt1 = DateTime.Now.Date;
            string month = dt1.Month.ToString();
            if (Convert.ToInt32(month) < 10)
            {
                month = "0" + month;
            }
            string year = dt1.Year.ToString();
            if (string.IsNullOrEmpty(month1))
            {
                month1 = month;
            }else
            {
                if (Convert.ToInt32(month1) < 10)
                {
                    month1 = "0" + month1;
                }
            }
            if (string.IsNullOrEmpty(year1))
            {
                year1 = year;
            }
            var firstDayOfMonth = new DateTime(Convert.ToInt32(year1), Convert.ToInt32(month1), 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            CalendarDate = year1 + "-" + month1;            
            List<Compliancecalendar> events = new List<Compliancecalendar>();
            int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userid = AuthenticationHelper.UserID;
            DataTable dt = fetchData(customerid, userid, type);

            var list = (from r in dt.AsEnumerable()
                        where r.Field<DateTime>("ScheduledOn") >= firstDayOfMonth && r.Field<DateTime>("ScheduledOn") <= lastDayOfMonth
                        select r["ScheduledOn"]).Distinct().ToList();

            var datetodayornext = (from r in dt.AsEnumerable()
                                   orderby r.Field<DateTime>("ScheduledOn")
                                   where r.Field<DateTime>("ScheduledOn") >= DateTime.Now.AddDays(-1)
                                   select r["ScheduledOn"]).Distinct().FirstOrDefault();


            CalendarTodayOrNextDate = Convert.ToDateTime(datetodayornext);
            CalenderDateString = "";            
         //   for (int i = 0; i < list.Count; i++)
         foreach(var item in list)
            {
                Compliancecalendar _Event = new Compliancecalendar();
                string clor = "";
                int assignedCount = 0;
                DateTime date = Convert.ToDateTime(item);
                //DateTime date = DateTime.Today.AddDays(7);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {                   
                    var Assigned = (from dts in dt.AsEnumerable()
                                    where dts.Field<DateTime>("ScheduledOn").Year == date.Year
                                    && dts.Field<DateTime>("ScheduledOn").Month == date.Month
                                    && dts.Field<DateTime>("ScheduledOn").Day == date.Day                                                                        
                                    select dts.Field<long>("ScheduledOnID")).Distinct().ToList();

                    var ClosedDelaycount = (from dts in dt.AsEnumerable()
                                            where dts.Field<DateTime>("ScheduledOn").Year == date.Year
                                           && dts.Field<DateTime>("ScheduledOn").Month == date.Month
                                           && dts.Field<DateTime>("ScheduledOn").Day == date.Day
                                           && dts.Field<int>("ComplianceStatusID") == 5
                                    select dts.Field<long>("ScheduledOnID")).Distinct().ToList();

                    var Completed = (from dts in dt.AsEnumerable()
                                     where dts.Field<DateTime>("ScheduledOn").Year == date.Year
                                    && dts.Field<DateTime>("ScheduledOn").Month == date.Month
                                    && dts.Field<DateTime>("ScheduledOn").Day == date.Day
                                     && (dts.Field<int>("ComplianceStatusID") == 4
                                     || dts.Field<int>("ComplianceStatusID") == 5
                                      || dts.Field<int>("ComplianceStatusID") == 15
                                     || dts.Field<int>("ComplianceStatusID") == 7)
                                     select dts.Field<long>("ScheduledOnID")).Distinct().ToList();

                    assignedCount = Assigned.Count;
                    if (date > DateTime.Now)
                    {
                        clor = "";
                        if (date == DateTime.Today.Date)
                        {
                            clor = "upcoming";
                        }
                        else
                        {
                            if (Assigned.Count > Completed.Count)
                            {
                                clor = "upcoming";
                            }
                            else if ((Assigned.Count) == (Completed.Count))
                            {
                                if (ClosedDelaycount.Count > 0)
                                {
                                    clor = "delayed";
                                }
                                else
                                {
                                    clor = "complete";
                                }
                            }
                        }                                                
                    }
                    else
                    {
                        if (date == DateTime.Today.Date)
                        {
                            clor = "upcoming";
                        }
                        else
                        {

                            if (Assigned.Count > Completed.Count)
                            {
                                clor = "overdue";
                            }
                            else if ((Assigned.Count) == (Completed.Count))
                            {
                                if (ClosedDelaycount.Count > 0)
                                {
                                    clor = "delayed";
                                }
                                else
                                {
                                    clor = "complete";
                                }
                            }
                        }                                                
                    }              
                }
                DateTime dtdate = Convert.ToDateTime(item);
                var Date = dtdate.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                CalenderDateString += "\"" + Date.ToString() + "\": { \"number\" :" + assignedCount + ", \"badgeClass\": \"badge-warning " + clor + "\",\"url\": \"javascript: fcal('" + Date.ToString() + "');\"},";
            }
            CalenderDateString = CalenderDateString.Trim(',');
        }


        #region Not In Use
        public DataTable fetchPerformer(int customerid, int userid, string Type)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DataTable statutorydatatable = new DataTable();
                DataTable statutoryCheckListdatatable = new DataTable();
                DataTable Internaldatatable = new DataTable();
                DataTable InternalChecklistdatatable = new DataTable();

                #region statutory Performer And Reviewer
                var Query = (from row in entities.SP_ComplianceInstanceTransactionCount(userid)
                             where row.CustomerID == customerid
                             select new
                             {
                                 ScheduledOn = row.PerformerScheduledOn,
                                 ComplianceStatusID = row.ComplianceStatusID,
                                 RoleID = row.RoleID,
                                 ScheduledOnID = row.ScheduledOnID,
                                 CustomerBranchID = row.CustomerBranchID,
                             }).Distinct().ToList();

                if (Query.Count > 0)
                {
                    statutorydatatable = Query.ToDataTable();
                }
                #endregion

                #region Internal Performer And Reviewer
                var Query2 = (from row in entities.SP_InternalComplianceInstanceTransactionCount(userid)
                              where row.CustomerID == customerid
                              select new
                              {
                                  ScheduledOn = row.InternalScheduledOn,
                                  ComplianceStatusID = row.InternalComplianceStatusID,
                                  RoleID = row.RoleID,
                                  ScheduledOnID = row.InternalScheduledOnID,
                                  CustomerBranchID = row.CustomerBranchID,
                              }).Distinct().ToList();
                if (Query2.Count > 0)
                {
                    Internaldatatable = Query2.ToDataTable();
                }
                #endregion

                #region Check List
                if (Type == "1")
                {
                    //  statutory Checklist Performer
                    var ChecklistStatutoryPerpormer = (from row in entities.SP_CheckListInstanceTransactionCount(userid)
                                                       where row.CustomerID == customerid
                                                       select new
                                                       {
                                                           ScheduledOn = row.ScheduledOn,
                                                           ComplianceStatusID = row.ComplianceStatusID,
                                                           RoleID = row.RoleID,
                                                           ScheduledOnID = row.ScheduledOnID,
                                                           CustomerBranchID = row.CustomerBranchID,
                                                       }).Distinct().ToList();

                    if (ChecklistStatutoryPerpormer.Count > 0)
                    {
                        statutoryCheckListdatatable = ChecklistStatutoryPerpormer.ToDataTable();
                    }
                    //Internal Checklist
                    var ChecklistInternal = (from row in entities.Sp_InternalComplianceInstanceCheckListTransactionCount(userid)
                                             where row.CustomerID == customerid
                                             select new
                                             {
                                                 ScheduledOn = row.InternalScheduledOn,
                                                 ComplianceStatusID = row.InternalComplianceStatusID,
                                                 RoleID = row.RoleID,
                                                 ScheduledOnID = row.InternalScheduledOnID,
                                                 CustomerBranchID = row.CustomerBranchID,
                                             }).Distinct().ToList();
                    if (ChecklistInternal.Count > 0)
                    {
                        InternalChecklistdatatable = ChecklistInternal.ToDataTable();
                    }
                }
                #endregion

                DataTable newtable = new DataTable();
                if (statutorydatatable.Rows.Count > 0)
                {
                    newtable.Merge(statutorydatatable);
                }
                if (statutoryCheckListdatatable.Rows.Count > 0)
                {
                    newtable.Merge(statutoryCheckListdatatable);
                }
                if (Internaldatatable.Rows.Count > 0)
                {
                    newtable.Merge(Internaldatatable);
                }
                if (InternalChecklistdatatable.Rows.Count > 0)
                {
                    newtable.Merge(InternalChecklistdatatable);
                }
                return newtable;
            }
        }
        #endregion
    }
}