﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="CompanyAdminUpCompliances.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.CompanyAdminUpCompliances" %>

<%@ Register Src="~/Controls/ComplianceStatusTransactionCA.ascx" TagName="ComplianceStatusTransactionCA"
    TagPrefix="vit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function initializeDatePicker(date) {

            var startDate = new Date();
            $(".StartDate").datepicker({
                dateFormat: 'dd-mm-yy',
                setDate: startDate,
                numberOfMonths: 1
            });
        }

        function setDate() {
            $(".StartDate").datepicker();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="margin-bottom: 4px">


                <table width="100%" style="height: 35px">
                    <tr>
                        <td>
                            <div style="margin-bottom: 4px">
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lbltagLine" runat="server" Style="font-weight: bold; font-size: 12px; margin-left: 35px"></asp:Label>
                        </td>
                    </tr>
                </table>

                <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" AllowSorting="true"
                    GridLines="Both" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid"
                    BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="15" OnSorting="grdComplianceTransactions_Sorting"
                    Width="100%" Font-Size="12px" DataKeyNames="ScheduledOnID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging"
                    OnRowDataBound="grdComplianceTransactions_RowDataBound" OnRowCommand="grdComplianceTransactions_RowCommand" OnRowCreated="grdComplianceTransactions_RowCreated">
                    <Columns>
                        <asp:TemplateField HeaderText="ID" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <asp:Label ID="lblComplianceId" runat="server" Text='<%# Eval("ComplianceID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location-wise Compliance" ItemStyle-Width="30%" SortExpression="Description" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 450px;">
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>                     
                        <asp:TemplateField HeaderText="Reviewer" ItemStyle-Width="10%" ItemStyle-Height="22px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblReviewer" runat="server" Text='<%# GetReviewer((long)Eval("ComplianceInstanceID")) %>'></asp:Label>
                            </ItemTemplate>                            
                        </asp:TemplateField>                     
                        <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" HeaderText="Risk Category">
                            <ItemTemplate>
                                <asp:Image runat="server" ID="imtemplat" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Impact" ItemStyle-Width="10%" SortExpression="NonComplianceType" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblImpact" runat="server" Text='<%# Eval("NonComplianceType") %>'></asp:Label>
                                <asp:Label ID="lblRisk" runat="server" Text='<%# Eval("Risk") %>' Visible="false"></asp:Label>                               
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Due Date" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn">
                            <ItemTemplate>
                                <asp:Label ID="lblScheduledOn" runat="server" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="For Month" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="ForMonth">
                            <ItemTemplate>
                                <%# Eval("ForMonth") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Height="20px" ItemStyle-HorizontalAlign="Center" SortExpression="Status" ItemStyle-Width="7%" />
                        <%--<asp:TemplateField HeaderText="Event" ItemStyle-Width="30%" SortExpression="EventID">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label ID="lblEventID" runat="server" Text='<%# ShowType((long?)Eval("EventID")) %>' ToolTip='<%# ShowType((long?)Eval("EventID")) %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Event Name" ItemStyle-Width="30%" SortExpression="EventName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label ID="lblEventName" runat="server" Text='<%# Eval("EventName") %>' ToolTip='<%# Eval("EventName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Event Nature" ItemStyle-Width="30%" SortExpression="EventNature">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label ID="lblEventNature" runat="server" Text='<%# Eval("EventNature") %>' ToolTip='<%# Eval("EventNature") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Reference Material" ItemStyle-Width="30%" SortExpression="ReferenceMaterialText">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label ID="lblReferenceMaterialText" runat="server" Text="Text" ToolTip='<%# Eval("ReferenceMaterialText") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnChangeStatus" runat="server" Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("ComplianceStatusID")) %>'
                                    CommandName="CHANGE_STATUS" OnClick="btnChangeStatus_Click" CommandArgument='<%# Eval("ScheduledOnID") + "," + Eval("ComplianceInstanceID") %>'><img src='<%# ResolveUrl("~/Images/change_status_icon.png")%>' alt="Change Status" title="Change Status" /></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                    <PagerSettings Position="Top" />
                    <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                    <AlternatingRowStyle BackColor="#E6EFF7" />
                    <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>

                <asp:HiddenField ID="hdnTitle" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <vit:ComplianceStatusTransactionCA runat="server" ID="udcStatusTranscatopn" />
</asp:Content>
