﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="compliancestatusreviewernew.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.compliancestatusreviewernew" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.Web" Assembly="GleamTech.DocumentUltimate" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server" style="background: #f7f7f7;">
        <asp:ScriptManager ID="Isdf" runat="server"></asp:ScriptManager>

        <link href="https://avacdn.azureedge.net/newcss/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap theme -->
        <link href="https://avacdn.azureedge.net/newcss/bootstrap-theme.css" rel="stylesheet" type="text/css" />
        <link href="https://avacdn.azureedge.net/newcss/responsive-calendar.css" rel="stylesheet" type="text/css" />
        <link href="https://avacdn.azureedge.net/newcss/font-awesome.min.css" rel="stylesheet" type="text/css" />      
        <!-- owl carousel -->
        <link rel="stylesheet" href="https://avacdn.azureedge.net/newcss/owl.carousel.css" type="text/css" />
        <!-- Custom styles -->
        <link href="https://avacdn.azureedge.net/newcss/stylenew.css" rel="stylesheet" type="text/css" />
        <link href="https://avacdn.azureedge.net/newcss/jquery-ui-1.10.7.min.css" rel="stylesheet" type="text/css" />
        <link href="https://avacdn.azureedge.net/newcss/jquery.bxslider.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jquery.js"></script>
        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jquery-ui-1.9.2.custom.min.js"></script>
        <link href="../tree/jquerysctipttop.css" rel="stylesheet" />
   

        <style type="text/css">
            .reviewdoc {
                height: 100%;
                width: auto;
                display: inline-block;
                font-size: 22px;
                position: relative;
                margin: 0;
                line-height: 34px;
                font-weight: 400;
                letter-spacing: 0;
                color: #666;
            }

            tr.spaceUnder > td {
                padding-bottom: 1em;
            }

            .clspenaltysave {
                font-weight: bold;
                margin-left: 15px;
            }

            .input-disabled {
                background-color: #f7f7f7;
                border: 1px solid #c7c7cc;
                padding: 6px 12px;
            }

            .btnss {
                background-image: url(../Images/edit_icon_new.png);
                border: 0px;
                width: 24px;
                height: 24px;
                background-color: transparent;
            }

            .table > thead > tr > th {
                vertical-align: bottom;
                border-bottom: 2px solid #dddddd;
            }

            .table > tbody > tr > th {
                vertical-align: bottom;
                border-bottom: 2px solid #dddddd;
            }

            .table > thead > tr > th > a {
                vertical-align: bottom;
                border-bottom: 2px solid #dddddd;
            }

            .table tr th {
                color: #666666;
                font-size: 15px;
                font-weight: normal;
                font-family: 'Roboto',sans-serif;
            }

            .clsheadergrid {
                padding: 8px !important;
                line-height: 1.428571429 !important;
                vertical-align: bottom !important;
                border-bottom: 2px solid #dddddd !important;
                color: #666666;
                font-size: 15px;
                font-weight: normal;
                font-family: 'Roboto',sans-serif;
                text-align: left;
            }

            .clsROWgrid {
                padding: 8px !important;
                line-height: 1.428571429 !important;
                vertical-align: top !important;
                border-top: 1px solid #dddddd !important;
                color: #666666 !important;
                font-size: 14px !important;
                font-family: 'Roboto', sans-serif !important;
            }

            .Inforamative {
                color: blue !important;
                border-top: 1px solid #dddddd !important;
            }

            tr.Inforamative > td {
                color: blue !important;
                border-top: 1px solid #dddddd !important;
            }

            .circle {
                width: 15px;
                height: 15px;
                border-radius: 50%;
                display: inline-block;
                margin-right: 20px;
            }
        </style>

        <style type="text/css">
            .clspenaltysave {
                font-weight: bold;
                margin-left: 15px;
            }

            .btnss {
                background-image: url(../Images/edit_icon_new.png);
                border: 0px;
                width: 24px;
                height: 24px;
                background-color: transparent;
            }

            .table > thead > tr > th {
                vertical-align: bottom;
                border-bottom: 2px solid #dddddd;
            }

            .table > tbody > tr > th {
                vertical-align: bottom;
                border-bottom: 2px solid #dddddd;
            }

            .table > thead > tr > th > a {
                vertical-align: bottom;
                border-bottom: 2px solid #dddddd;
            }

            .table tr th {
                color: #666666;
                font-size: 15px;
                font-weight: normal;
                font-family: 'Roboto',sans-serif;
            }

            .clsheadergrid {
                padding: 8px !important;
                line-height: 1.428571429 !important;
                vertical-align: bottom !important;
                border-bottom: 2px solid #dddddd !important;
                color: #666666;
                font-size: 15px;
                font-weight: normal;
                font-family: 'Roboto',sans-serif;
                text-align: left;
            }

            .clsROWgrid {
                padding: 8px !important;
                line-height: 1.428571429 !important;
                vertical-align: top !important;
                border-top: 1px solid #dddddd !important;
                color: #666666 !important;
                font-size: 14px !important;
                font-family: 'Roboto', sans-serif !important;
            }

            .Inforamative {
                color: blue !important;
                border-top: 1px solid #dddddd !important;
            }

            tr.Inforamative > td {
                color: blue !important;
                border-top: 1px solid #dddddd !important;
            }

            .circle {
                /*background-color: green;*/
                width: 15px;
                height: 15px;
                border-radius: 50%;
                display: inline-block;
                margin-right: 20px;
            }
        </style>
        <style type="text/css">
            table#basic > tr, td {
                border-radius: 5px;
            }

            table#basic {
                border-collapse: unset;
                border-spacing: 3px;
            }

            .locationheadbg {
                background-color: #999;
                color: #fff;
                border: #666;
            }

            .locationheadLocationbg {
                background-color: #fff;
            }

            td.locationheadLocationbg > span.tree-icon {
                background-color: #1976d2 !important;
                padding-right: 12px;
                color: white;
            }

            .GradingRating1 {
                background-color: #8fc156;
            }

            .GradingRating2 {
                background-color: #ffc107;
            }

            .GradingRating3 {
                background-color: #ef9a9a;
            }

            .Viewcss {
                width: 50px;
                color: blue;
                text-align: center;
                cursor: pointer;
            }

            .downloadcss {
                width: 100px;
                color: blue;
                text-align: center;
                cursor: pointer;
            }
        </style>
    

        <div id="divReviewerComplianceDetailsDialog" style="width: 97%; margin-left: 20px; margin-top: -10px;">

            <asp:UpdatePanel ID="upComplianceDetails1" runat="server" UpdateMode="Conditional"
                OnLoad="upComplianceDetails1_Load">
                <ContentTemplate>
                    <div style="margin: 5px">
                        <div style="margin-bottom: 4px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Style="padding-left: 5%" class="alert alert-block alert-danger fade in" ValidationGroup="ReviewerComplianceValidationGroup" />
                            <asp:CustomValidator ID="cvDuplicateEntry" class="alert alert-block alert-danger fade in" runat="server" EnableClientScript="False"
                                Style="padding-left: 40px" ValidationGroup="ReviewerComplianceValidationGroup" Display="None" />
                            <asp:Label ID="Labelmsgrev" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
                        </div>

                        <div id="Step1UpdateCompliance" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Step1UpdateComplianceStatus">
                                                    <h2>Update Compliance Status </h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#Step1UpdateComplianceStatus"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="Step1UpdateComplianceStatus" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <label id="lblStatus1" runat="server" style="width: 200px; display: block; float: left; font-size: 13px; color: #333;" />
                                                            <td style="width: 21%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Status</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:DropDownList runat="server" ID="ddlFilterType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                                                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterType_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <div style="margin-bottom: 7px">
                                                                <td style="width: 25%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">Remark</label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 83%;">
                                                                    <asp:TextBox runat="server" ID="tbxRemarks1" TextMode="MultiLine" class="form-control" Rows="2" />
                                                                    <asp:RequiredFieldValidator ErrorMessage="Please select Remark." ControlToValidate="tbxRemarks1"
                                                                        runat="server" ID="RequiredFieldValidator3" ValidationGroup="ReviewerComplianceValidationGroup" Display="None" />
                                                                </td>
                                                            </div>
                                                        </tr>
                                                    </table>
                                                    <div style="margin-bottom: 7px; margin-left: 34%; margin-top: 10px;">
                                                        <asp:Button Text="Save" runat="server" ID="btnSave1" OnClick="btnSave1_Click" CssClass="btn btn-search"
                                                            ValidationGroup="ReviewerComplianceValidationGroup" />
                                                        <asp:Button Text="Close" runat="server" OnClick="btnReviewClose_Click" Style="margin-left: 15px" ID="btnReviewClose" OnClientClick="closeWin()" CssClass="btn btn-search" data-dismiss="modal" />
                                                    </div>
                                                </div>
                                                </label>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSave1" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jquery.bxslider.js"></script>
        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/bootstrap.min.js"></script>
        <!-- nice scroll -->
        <!-- charts scripts -->
        <script type="text/javascript" src="../assets/jquery-knob/js/jquery.knob.js"></script>
        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/owl.carousel.js"></script>         
        <script type="text/javascript" src="https://avacdn.azureedge.net/newjs/jquery.rateit.min.js"></script>      
    </form>
</body>

<script type="text/javascript" language="javascript">
    function closeWin() {
        window.close();
    }
    function topFunction() {
        $("html,body").animate({ scrollTop: 0 }, "slow");
    }

</script>
</html>
