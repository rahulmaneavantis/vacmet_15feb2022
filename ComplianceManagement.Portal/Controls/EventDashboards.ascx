﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventDashboards.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.EventDashboards" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<script type="text/javascript">

    $(document).on("click", "#ContentPlaceHolder1_ucEventDashboards_upEventInstanceListAssigned", function (event) {

        if (event.target.id == "") {
            var idvid = $(event.target).closest('div');
            if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocationAssigned') > -1) {
                $("#ContentPlaceHolder1_ucEventDashboards_divFilterLocationAssigned").show();
            } else {
                $("#ContentPlaceHolder1_ucEventDashboards_divFilterLocationAssigned").hide();
            }
        }
        else if (event.target.id != "ContentPlaceHolder1_ucEventDashboards_tbxFilterLocationAssigned") {
            $("#ContentPlaceHolder1_ucEventDashboards_divFilterLocationAssigned").hide();
        } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocationAssigned') > -1) {
            $("#ContentPlaceHolder1_ucEventDashboards_divFilterLocationAssigned").show();
        } else if (event.target.id == "ContentPlaceHolder1_ucEventDashboards_tbxFilterLocationAssigned") {
            $("#ContentPlaceHolder1_ucEventDashboards_tbxFilterLocationAssigned").unbind('click');

            $("#ContentPlaceHolder1_ucEventDashboards_tbxFilterLocationAssigned").click(function () {
                $("#ContentPlaceHolder1_ucEventDashboards_divFilterLocationAssigned").toggle("blind", null, 500, function () { });
            });

        }
    });

    function openmodaelShortNotice1() {
        $('#modalShortNotice1').modal('show');
        return true;
    }

    function NotAssignedComplianceListModal() {
        $('#NotAssignedComplianceList').modal('show');
        return true;
    };


    function AssignedComplianceListModal() {
        $('#AssignedComplianceList').modal('show');
        return true;
    };

    //function CloseNotAssignedComplianceListModal() {
    //    $('#NotAssignedComplianceList').modal('hide');
    //    return true;
    //}

    //function CloseAssignedComplianceListModal() {
    //    $('#AssignedComplianceList').modal('hide');
    //    return true;
    //}

    function initializeDatePicker(date) {
        //var startDate = new Date();
        //$(".StartDate").datepicker({
        //    dateFormat: 'dd-mm-yy',
        //    setDate: startDate,
        //    numberOfMonths: 1
        //});
    }
    function initializeDatePicker11(date2) {
        var startDate = new Date();
        $('#<%= tbxStartDate.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: date2,
            numberOfMonths: 1
        });

        if (date2 != null) {
            $("#<%= tbxStartDate.ClientID %>").datepicker("option", "defaultDate", date2);
        }
    }

    function initializeJQueryUI(textBoxID, divID) {
        $("#" + textBoxID).unbind('click');

        $("#" + textBoxID).click(function () {
            $("#" + divID).toggle("blind", null, 500, function () { });
        });
    }
    function postbackOnCheck() {
        var o = window.event.srcElement;
        if (o.tagName == 'INPUT' && o.type == 'checkbox' && o.name != null && o.name.indexOf('CheckBox') > -1)
        { __doPostBack("", ""); }
    }

    function divexpandcollapse(divname) {
        var div = document.getElementById(divname);
        var img = document.getElementById('img' + divname);
        if (div.style.display == "none") {
            div.style.display = "inline";
            img.src = "../Images/minus.gif";
        } else {
            div.style.display = "none";
            img.src = "../Images/plus.gif";
        }
    }
    function divexpandcollapseChild(divname) {
        var div1 = document.getElementById(divname);
        var img = document.getElementById('img' + divname);
        if (div1.style.display == "none") {
            div1.style.display = "inline";
            img.src = "../Images/minus.gif";
        } else {
            div1.style.display = "none";
            img.src = "../Images/plus.gif";;
        }
    }
</script>

<script type="text/javascript">
    function checkAll(objRef) {

        var GridView = objRef.parentNode.parentNode.parentNode;
        var inputList = GridView.getElementsByTagName("input");
        for (var i = 0; i < inputList.length; i++) {
            var row = inputList[i].parentNode.parentNode;
            if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                if (objRef.checked) {

                    inputList[i].checked = true;
                }
                else {

                    inputList[i].checked = false;
                }
            }
        }
    }
</script>

<script type="text/javascript">
    function DeleteItem() {
        if (confirm("Are you sure you want to delete ...?")) {
            return true;
        }
        return false;
    }
</script>

<style type="text/css">
    .btnss {
        background-image: url(../Images/edit_icon_new.png);
        border: 0px;
        width: 24px;
        height: 24px;
        background-color: transparent;
    }

    .btnview {
        background-image: url(../Images/view-icon-new.png);
        border: 0px;
        width: 24px;
        height: 24px;
        background-color: transparent;
    }
</style>
<%--<asp:UpdateProgress ID="updateProgress" runat="server">
    <ProgressTemplate>
        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>--%>
<div style="margin-top: 0px">

    <div runat="server" id="divEventInstance">
        <asp:UpdatePanel ID="upEventInstanceListAssigned" runat="server" UpdateMode="Conditional" OnLoad="upEventInstanceListAssigned_Load">
            <ContentTemplate>
                <div class="panel-body">
                    <div class="col-md-12 colpadding0">

                        <div class="col-md-12 colpadding0" style="text-align: right; float: right">
                            <div style="margin-bottom: 4px">
                                <asp:CustomValidator ID="cvDuplicateEntry1" runat="server" EnableClientScript="False"
                                    ValidationGroup="ComplianceInstanceValidationGroup1" Display="None" />
                            </div>

                            <div class="col-md-2 colpadding0 entrycount">
                                <div class="col-md-3 colpadding0">
                                    <p style="color: #999; margin-top: 5px">Show </p>
                                </div>
                                <asp:DropDownList runat="server" ID="ddlpageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlpageSize_SelectedIndexChanged" class="form-control m-bot15" Style="width: 64px; float: left; margin-left: 10px;">
                                    <asp:ListItem Text="5" />
                                    <asp:ListItem Text="10" Selected="True" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-2 colpadding0 entrycount" style="float: left; margin-right: -2%; width: 60.667% !important">
                                <asp:DropDownList runat="server" ID="ddlCheckListType" AutoPostBack="true" OnSelectedIndexChanged="ddlCheckListType_SelectedIndexChanged" class="form-control m-bot15 search-select">
                                     <asp:ListItem Text="<Select>" Value="-1" />
                                    <asp:ListItem Text="Secretrial" Value="1" />
                                    <asp:ListItem Text="Non Secretrial" Value="2" />
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-4 colpadding0 entrycount" style="float: left; margin-left: -50%; width: 31.667% !important">
                                <asp:TextBox runat="server" AutoCompleteType="None" ID="tbxFilterLocationAssigned" Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 280px; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                    CssClass="txtbox" />
                                <div style="margin-left: 11%; position: absolute; z-index: 10;" id="divFilterLocationAssigned" runat="server">
                                    <asp:TreeView runat="server" ID="tvFilterLocationAssigned" SelectedNodeStyle-Font-Bold="true" Width="280px" NodeStyle-ForeColor="#8e8e93"
                                        Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                        OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                    </asp:TreeView>
                                </div>
                            </div>
                            <div class="col-md-4 colpadding0 entrycount" style="float: left; margin-left: -17%; width: 13.333333% !important;">
                                <asp:TextBox runat="server" Style="padding-left: 7px; width: 300px;" placeholder="Type to Filter" class="form-group form-control" ID="txtSearchType" CssClass="form-control" onkeydown="return (event.keyCode!=13);" />
                            </div>

                            <div class="col-md-2 colpadding0" style="width: 17.333333%; float: right">
                                <div class="col-md-6 colpadding0">
                                    <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-search" OnClick="btnSearch_Click" Style="margin-left: 100px !important;" runat="server" Text="Apply" />
                                </div>
                            </div>
                        </div>

                        <div runat="server" id="DivRecordsScrum" style="float: right;">

                            <p style=" color: #999; padding-right: 0px !Important;">
                                <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>
                </div>

                <asp:Button ID="btnBackMyEvent" CssClass="button" Style="display: none;" runat="server" Text="Back" OnClick="btnBackMyEvent_Click" />
                <div class="col-md-12 colpadding0">

                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 26%">
                    </div>

                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 22%">
                    </div>

                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 22%">
                        <asp:TextBox runat="server" ID="txtEventNature" AutoPostBack="true" OnTextChanged="txtEventNature_TextChanged" class="form-control" Style="width: 90%;" placeholder="Nature of Event" />
                    </div>
                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 17%;">
                        <asp:TextBox runat="server" ID="txtActivateDate" AutoPostBack="true" OnTextChanged="txtActivateDate_TextChanged" class="form-control" Style="width: 90%; margin-left: -7px;" placeholder="Activate Date" />
                    </div>
                </div>

                <asp:GridView runat="server" ID="grdEventList" AutoGenerateColumns="false" CssClass="table"
                    OnRowUpdating="grdEventList_RowUpdating" OnRowCommand="grdEventList_RowCommand" GridLines="None"
                    AllowPaging="True" PageSize="10" OnSorting="grdEventList_Sorting" DataKeyNames="ID" OnPageIndexChanging="grdEventList_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this);" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="CheckBox1" runat="server" />
                                <%--onclick="Check_Click(this)"--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                    <asp:Label ID="lbleventName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location" SortExpression="CustomerBranchName">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px;">
                                    <asp:Label ID="lblUserID" runat="server" Visible="false" Text='<%# Eval("UserID") %>'></asp:Label>
                                    <asp:Label ID="lblAssignmnetId" runat="server" Visible="false" Text='<%# Eval("EventAssignmentID") %>'></asp:Label>
                                    <asp:Label ID="lblEventInstanceID" runat="server" Visible="false" Text='<%# Eval("EventInstanceID") %>'></asp:Label>
                                    <asp:Label ID="lblBranch" runat="server" Visible="false" Text='<%# Eval("CustomerBranchID") %>'></asp:Label>
                                    <asp:Label ID="lblEventClassificationID" runat="server" Visible="false" Text='<%# Eval("EventClassificationID") %>'></asp:Label>
                                    <asp:Label ID="lblBranchName" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nature of event">
                            <ItemTemplate>
                                <asp:TextBox ID="txteventNatureGrid" runat="server" CssClass="form-control" Style="text-align: left;"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Activate Date">
                            <ItemTemplate>
                                <asp:TextBox ID="txtActivateDateGrid" runat="server" CssClass="form-control" Style="text-align: left; width: 80%;"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                    <asp:Button ID="btn_Update" runat="server" ToolTip="Activate" CommandName="Update" CssClass="btnss" />
                                    <asp:Button ID="btn_View" runat="server" Style="margin-left: 8px;" ToolTip="View Event" CommandName="View" CssClass="btnview" CommandArgument='<%# ((GridViewRow) Container).RowIndex + "," + Eval("CustomerBranchID") %>' />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>

                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerStyle HorizontalAlign="Right" />
                    <PagerTemplate>
                        <table style="display: none">
                            <tr>
                                <td>
                                    <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                </td>
                            </tr>
                        </table>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Records Found.
                    </EmptyDataTemplate>
                </asp:GridView>

                <div class="col-md-12 colpadding0">
                    <div class="col-md-6 colpadding0"></div>

                    <div class="col-md-6 colpadding0">
                        <div class="table-paging">

                            <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                            <div class="table-paging-text">
                                <p>
                                    <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                </p>
                            </div>

                            <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                        </div>
                    </div>
                </div>

                <tr id="trErrorMessage" runat="server" visible="true">
                    <td colspan="3" style="background-color: #e9e1e1;">
                        <asp:Label ID="GridViewPagingError" runat="server" Font-Names="Verdana" Font-Size="9pt"
                            ForeColor="Red"></asp:Label>
                        <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                    </td>
                </tr>

                <div>
                    <asp:Button Text="Save" runat="server" ID="btnAllSavechk" ValidationGroup="ComplianceInstanceValidationGroup1" OnClick="btnAllsave_click" CssClass="btn btn-search" />

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div runat="server" id="divOptionalCompliance">
        <div id="divOptionalCompliances">
            <asp:UpdatePanel ID="upOptionalCompliances" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary runat="server" CssClass="vdsummary"
                            ValidationGroup="ComplianceValidationGroup" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceValidationGroup" Display="None" />
                        <asp:Label runat="server" ID="lblErrorMassage" ForeColor="Red"></asp:Label>
                    </div>
                    <div style="margin: 5px">

                        <asp:Panel ID="Panel2" ScrollBars="Auto" runat="server">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <div style="margin-bottom: 7px;" runat="server" id="divSubEventmode">
                                            <div style="z-index: 10" id="divSubevent">
                                                <asp:GridView ID="gvParentGrid" runat="server" GridLines="None" AutoGenerateColumns="false"
                                                    ShowFooter="true" Width="630px" DataKeyNames="Type,SequenceID"
                                                    OnRowDataBound="gvParentGrid_OnRowDataBound">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-Width="20px">
                                                            <ItemTemplate>
                                                                <a href="JavaScript:divexpandcollapse('div<%# Eval("EventType") %>');">
                                                                    <img id="imgdiv<%# Eval("EventType") %>" width="9px" border="0"
                                                                        src="../Images/plus.gif" alt="" /></a>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                        </asp:TemplateField>
                                                        <asp:BoundField ItemStyle-Width="150px" DataField="ParentEventID" HeaderText="ID" />
                                                        <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Parent Event Name" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td colspan="100%">
                                                                        <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: visible">
                                                                            <asp:GridView ID="gvParentToComplianceGrid" GridLines="None" runat="server" Width="95%" DataKeyNames="EventType,SequenceID"
                                                                                AutoGenerateColumns="false">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                                                        <ItemTemplate>
                                                                                            <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                                <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/minus.gif"
                                                                                                    alt="" /></a>
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField ItemStyle-Width="122px" DataField="ComplianceID" HeaderText="ID" />
                                                                                    <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Compliance Name" />
                                                                                    <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" />
                                                                                </Columns>
                                                                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                <%-- <AlternatingRowStyle BackColor="#E6EFF7" />--%>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td colspan="100%">
                                                                        <div id="div<%# Eval("EventType") %>" style="overflow: auto; display: inline; position: relative; left: 15px; overflow: auto">
                                                                            <asp:GridView ID="gvChildGrid" runat="server" Width="95%" GridLines="None"
                                                                                AutoGenerateColumns="false" DataKeyNames="ParentEventID,SequenceID"
                                                                                OnRowDataBound="gvChildGrid_OnRowDataBound">
                                                                                <Columns>
                                                                                    <asp:TemplateField ItemStyle-Width="20px">
                                                                                        <ItemTemplate>
                                                                                            <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                                <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                    alt="" /></a>
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField ItemStyle-Width="130px" DataField="SubEventID" HeaderText="ID" />
                                                                                    <asp:BoundField ItemStyle-Width="750px" DataField="Name" HeaderText="Sub Event Name" />
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="IntermediateEventID" runat="server" Text='<%# Eval("IntermediateEventID") %>' Style="display: none;"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <%--Intermdaite Event--%>
                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                            <tr>
                                                                                                <td colspan="100%">
                                                                                                    <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                        <asp:GridView ID="gvIntermediateSubEventGrid" GridLines="None" OnRowDataBound="gvIntermediateSubEventGrid_RowDataBound" runat="server" Width="95%" DataKeyNames="IntermediateEventID"
                                                                                                            AutoGenerateColumns="false">
                                                                                                            <Columns>
                                                                                                                <asp:TemplateField ItemStyle-Width="20px">
                                                                                                                    <ItemTemplate>
                                                                                                                        <a href="JavaScript:divexpandcollapse('div1<%# Eval("EventType") %>');">
                                                                                                                            <img id="imgdiv1<%# Eval("EventType") %>" width="9px" border="0" src="../Images/plus.gif"
                                                                                                                                alt="" /></a>
                                                                                                                    </ItemTemplate>
                                                                                                                    <ItemStyle Width="20px" VerticalAlign="Middle"></ItemStyle>
                                                                                                                </asp:TemplateField>
                                                                                                                <asp:BoundField ItemStyle-Width="120px" DataField="SubEventID" HeaderText="ID" />
                                                                                                                <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Sub Event Name" />
                                                                                                                <asp:TemplateField>
                                                                                                                    <ItemTemplate>
                                                                                                                        <tr>
                                                                                                                            <td colspan="100%">
                                                                                                                                <div id="div1<%# Eval("EventType") %>" style="overflow: auto; display: none; position: relative; left: 15px; overflow: auto">
                                                                                                                                    <asp:GridView ID="gvIntermediateComplainceGrid" GridLines="None" runat="server" Width="95%"
                                                                                                                                        AutoGenerateColumns="false">
                                                                                                                                        <Columns>
                                                                                                                                            <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="ID" />
                                                                                                                                            <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance Name" />
                                                                                                                                            <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" />
                                                                                                                                        </Columns>
                                                                                                                                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                                                    </asp:GridView>
                                                                                                                                 
                                                                                                                                </div>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </ItemTemplate>
                                                                                                                </asp:TemplateField>
                                                                                                            </Columns>
                                                                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                        </asp:GridView>
                                                                                                        <%--Complaince--%>
                                                                                                        <asp:GridView ID="gvComplianceGrid" GridLines="None" runat="server" Width="95%"
                                                                                                            AutoGenerateColumns="false" DataKeyNames="SequenceID">
                                                                                                            <Columns>
                                                                                                                <asp:BoundField ItemStyle-Width="120px" DataField="ComplianceID" HeaderText="ID" />
                                                                                                                <asp:BoundField ItemStyle-Width="650px" DataField="Name" HeaderText="Compliance Name" />
                                                                                                                <asp:BoundField ItemStyle-Width="50px" DataField="Days" HeaderText="Days" />
                                                                                                                <asp:BoundField ItemStyle-Width="50px" DataField="StateID" Visible="false" HeaderText="StateID" />
                                                                                                            </Columns>
                                                                                                            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                                                        </asp:GridView>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <%-- <HeaderStyle BackColor="#0063A6" ForeColor="White" />--%>
                                                    <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                                    <AlternatingRowStyle BackColor="#E6EFF7" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="Panel1" Height="70px" ScrollBars="Auto" runat="server">
                            <table width="100%">
                                <tr>
                                    <td style="width: 400px;">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 390px; display: block; float: left; font-size: 13px; color: #333;">
                                            Kindly describe nature of event
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 400px;">
                                        <asp:TextBox ID="txtDescription" Width="632px" runat="server" TextMode="MultiLine"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvDescription" ErrorMessage="Please enter event description."
                                            ControlToValidate="txtDescription" runat="server" ValidationGroup="ComplianceValidationGroup"
                                            Display="None" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <div style="margin-bottom: 7px; float: right; margin-right: 240px; margin-top: 10px; clear: both">
                            <asp:Button Text="Proceed" runat="server" ValidationGroup="ComplianceValidationGroup" ID="btnOptionalComplianceSave" OnClick="btnOptionalComplianceSave_Click"
                                CssClass="button" />
                            <asp:Button Text="Close" runat="server" ID="Button2" CssClass="button" OnClick="btnClose_Click" />
                        </div>
                    </div>
                    <asp:HiddenField ID="hdnTitle" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>

<div>
    <div class="modal fade" id="NotAssignedComplianceList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 80%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="height: 615px;">
                    <div id="divNotAssignedCompliances">
                        <asp:UpdatePanel ID="upNotAssignedCompliances" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="margin: 5px; color: red; font-size: 17px; font-weight: bold;">
                                    Listed compliance are not assigned to performer and reviewer, kindly assign to process further for activation. Once assignmemt completed please click save at main page.
                                </div>
                                <div style="margin: 5px">
                                    <asp:Panel ID="Panel4" Height="490px" ScrollBars="Auto" runat="server">
                                        <asp:GridView runat="server" ID="grdNoAsignedComplinace" CssClass="table" GridLines="none"
                                            OnPageIndexChanging="grdNoAsignedComplinace_PageIndexChanging" AutoGenerateColumns="false"
                                            AllowSorting="true" PageSize="500">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                    <ItemTemplate>
                                                        <%#Container.DataItemIndex+1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-Height="22px" HeaderText="ID">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Event Name">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px;">
                                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("EvnetName") %>' ToolTip='<%# Eval("EvnetName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location" ItemStyle-Height="22px">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                            <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                                            <asp:Label ID="lblCustomerBranchID" Visible="false" runat="server" Text='<%# Eval("CustomerBranchID") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Compliance Name">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 350px;">
                                                            <asp:Label ID="lblName1" runat="server" Text='<%# Eval("ComplianceName") %>' ToolTip='<%# Eval("ComplianceName") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Right" />
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <PagerTemplate>
                                                <table style="display: none">
                                                    <tr>
                                                        <td>
                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Record Found
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </asp:Panel>
                                    <div style="margin-bottom: 7px; float: right; margin-right: 45%; margin-top: 10px; clear: both">
                                        <asp:Button Text="Assign Compliance" runat="server" Width="166px" ID="btnComplianceList" CssClass="btn btn-search" OnClick="btnComplianceList_Click" />
                                        <asp:Button Text="Close" Style="margin-left: 15px;" runat="server" ID="btnDvNotAssignedClose" data-dismiss="modal" CssClass="btn btn-search" />
                                    </div>
                                </div>
                                <asp:HiddenField ID="HiddenField1" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="modal fade" id="AssignedComplianceList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 80%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="height: 615px;">
                    <div id="divAssignedCompliances">
                        <asp:UpdatePanel ID="upCompliance" runat="server" UpdateMode="Conditional" OnLoad="upCompliance_Load">
                            <ContentTemplate>
                                <table runat="server" width="100%">
                                    <tr>
                                        <td class="td1">
                                            <label style="width: 110px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                                Select Performer:
                                            </label>
                                        </td>
                                        <td class="td2">
                                            <asp:DropDownList runat="server" ID="ddlFilterPerformer" class="form-control" Style="padding: 0px; width: 250px; margin: 0px; min-height: 22px; min-width: 50px;">
                                            </asp:DropDownList>
                                            <asp:CompareValidator ErrorMessage="Please select Performer." ControlToValidate="ddlFilterPerformer"
                                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="AssignComplaiceValidationGroup"
                                                Display="None" />
                                        </td>
                                        <td class="td3">
                                            <label style="width: 110px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                                Select Reviewer:
                                            </label>
                                        </td>
                                        <td class="td4">
                                            <asp:DropDownList runat="server" ID="ddlFilterReviewer" class="form-control" Style="padding: 0px; width: 250px; margin: 0px; min-height: 22px; min-width: 50px;">
                                            </asp:DropDownList>
                                            <asp:CompareValidator ErrorMessage="Please select Reviewer." ControlToValidate="ddlFilterReviewer"
                                                runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="AssignComplaiceValidationGroup"
                                                Display="None" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td1">
                                            <label style="width: 110px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                                Select Approver:
                                            </label>
                                        </td>
                                        <td class="td2">
                                            <asp:DropDownList runat="server" ID="ddlFilterApprover" class="form-control" Style="padding: 0px; width: 250px; margin: 0px; min-height: 22px; min-width: 50px;">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="td3">
                                            <label style="width: 110px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                                Start Date:
                                            </label>
                                        </td>
                                        <td class="td4">
                                            <asp:TextBox runat="server" ID="tbxStartDate" placeholder="DD-MM-YYYY" class="form-control" Style="width: 250px;" AutoPostBack="true" />
                                            <asp:RequiredFieldValidator ErrorMessage="Please select Start date." ControlToValidate="tbxStartDate"
                                                runat="server" ValidationGroup="AssignComplaiceValidationGroup" Display="None" />
                                        </td>
                                    </tr>
                                    <div class="clearfix"></div>
                                    <tr>
                                        <td class="td1">
                                            <label style="width: 80px; display: block; font-size: 13px; color: #333; float: left; margin-top: 4px;">
                                                Event Name:
                                            </label>
                                        </td>
                                        <td class="td2">
                                            <asp:DropDownList runat="server" ID="ddlEvent" AutoPostBack="true" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" Style="padding: 0px; width: 250px; margin: 0px; min-height: 22px; min-width: 50px;">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="td3" colspan="2">
                                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="vdsummary" ValidationGroup="AssignComplaiceValidationGroup" Style="color: Red" />
                                            <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                                                ValidationGroup="AssignComplaiceValidationGroup" Display="None" Style="color: Red" />
                                            <asp:Label ID="Label1" runat="server" Style="color: Red"></asp:Label>
                                        </td>
                                        <%-- <td class="td4"></td>--%>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center">
                                            <asp:Panel ID="Panel3" Height="430px" ScrollBars="Auto" runat="server">
                                                <asp:GridView runat="server" ID="gvComplianceListAssign" AutoGenerateColumns="false" CssClass="table" GridLines="none"
                                                    PageSize="500">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-Height="22px" HeaderText="ID">
                                                            <ItemTemplate>
                                                                <%--<div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">--%>
                                                                <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                                <%-- </div>--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Event Name" ItemStyle-Height="15px" SortExpression="EvnetName">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("EvnetName") %>' ToolTip='<%# Eval("EvnetName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Location" ItemStyle-Height="22px" SortExpression="Location">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                                                    <asp:Label ID="lblLocation" runat="server" Text='<%# Eval("CustomerBranchName") %>' ToolTip='<%# Eval("CustomerBranchName") %>'></asp:Label>
                                                                    <asp:Label ID="lblCustomerBranchID" Visible="false" runat="server" Text='<%# Eval("CustomerBranchID") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Compliance Name" ItemStyle-Height="22px" SortExpression="ComplianceName">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                                    <asp:Label ID="lblName1" runat="server" Text='<%# Eval("ComplianceName") %>' ToolTip='<%# Eval("ComplianceName") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkComplianceCheck" Text="Assign" runat="server" AutoPostBack="true" OnCheckedChanged="chkComplianceCheck_CheckedChanged" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkCompliance" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" />
                                                    <RowStyle CssClass="clsROWgrid" />
                                                    <HeaderStyle CssClass="clsheadergrid" />
                                                    <PagerTemplate>
                                                        <table style="display: none">
                                                            <tr>
                                                                <td>
                                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </PagerTemplate>
                                                    <EmptyDataTemplate>
                                                        No Record Found
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </asp:Panel>
                                            <br />
                                            <asp:Button Text="Save" runat="server" ID="SaveComplianceList" CssClass="btn btn-search" OnClick="SaveComplianceList_Click" Style="margin-left: 45%;"
                                                ValidationGroup="AssignComplaiceValidationGroup" />
                                            <asp:Button Text="Close" Style="margin-left: 15px;" runat="server" ID="Button1" data-dismiss="modal" CssClass="btn btn-search" />

                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalShortNotice1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 450px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="width: 450px;">
                <asp:UpdatePanel ID="upshortnotice1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="margin-bottom: 4px; margin-left: 95px">
                            <asp:Label runat="server" Text="Do you want to conduct meeting by short notice?" ID="Label2" Style="color: #999; margin-left: -72px"></asp:Label>
                        </div>

                        <div style="margin-bottom: 4px; margin-left: 95px">
                            <asp:Label runat="server" ID="lblMessage" ForeColor="Red"></asp:Label>
                        </div>
                        <div>
                            <div style="margin: 5px; margin-left: 165px;">
                                <asp:RadioButtonList ID="rbtnShortnotice1" runat="server" RepeatDirection="Horizontal" AutoPostBack="true"
                                    OnSelectedIndexChanged="rbtnShortnotice1_SelectedIndexChanged">
                                    <asp:ListItem Text="Yes" Value="1" Selected="True" />
                                    <asp:ListItem Text="No" Value="0" />
                                </asp:RadioButtonList>
                            </div>
                            <div style="margin-top: 25px; margin-left: 70px;" id="divDays" runat="server">
                                <asp:Label ID="lbl" Text="Short Notice Days" Style="color: #999;" Width="125px" runat="server"></asp:Label>
                                <%-- <asp:Label ID="Label1" Font-Size="Large" Font-Bold="true" Text ="(-)" Width="16px" runat="server"></asp:Label>--%>
                                <asp:TextBox ID="txtShortNoticeDays1" Width="80px" runat="server" MaxLength="2"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                    TargetControlID="txtShortNoticeDays1" />
                            </div>
                            <div style="margin-top: 50px; margin-left: 125px">
                                <asp:Button Text="Save" runat="server" class="btn btn-search" Style="margin-top: -30px;" ValidationGroup="ComplianceValidationGroup" ID="btnSaveShortNotice" OnClick="btnSaveShortNotice_Click" />
                                <asp:Button Text="Close" runat="server" class="btn btn-search" Style="margin-top: -30px; margin-left: 15px" data-dismiss="modal" ID="btnShortClose" />
                                <%--OnClick="btnShortClose_Click"--%>
                            </div>
                        </div>
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    function initializeDatePickerGrid() {
        var grid = document.getElementById("<%= grdEventList.ClientID%>");
        for (var i = 0; i < grid.rows.length - 1; i++) {
            var txtDate = $("input[id*=txtActivateDateGrid]")
            txtDate.datepicker({
                dateFormat: 'dd-mm-yy',
                numberOfMonths: 1
            });
        }
    }

    function initializeConfirmDatePickerActivation(date) {
        var startDate = new Date();
        $('#<%= txtActivateDate.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            numberOfMonths: 1
        });
    }

</script>




