﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InternalApproverSummaryStatusReport.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.InternalApproverSummaryStatusReport" %>
<style type="text/css">
    table th {
        border: 1px solid White;
        height: 20px;
        color: White;
        font-size: 12px;
        font-family: Tahoma;
    }
</style>
<asp:UpdateProgress ID="updateProgress" runat="server">
    <ProgressTemplate>
        <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<div style="margin: 10px 20px 10px 30px">
    <div style="margin-bottom: 4px">
        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
            ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
    </div>
    <div style="margin-bottom: 7px; margin-left: 20px;">
        <asp:UpdatePanel ID="upManagementDashboard" runat="server" UpdateMode="Conditional" OnLoad="upManagementDashboard_Load">
            <ContentTemplate>
                <label style="width: 200px; padding-top: 5px; display: block; float: left; font-size: 13px; color: #333;">
                    Summary Status Report as on</label>
                <label style="margin-left: 65px;">Select Year</label>
                <asp:DropDownList runat="server" ID="ddlyear" Style="height: 22px; margin-left: 10px; width: 200px;"
                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlyear_SelectedIndexChanged" />
                <label style="margin-left: 10px;">Select Month </label>
                <asp:DropDownList runat="server" ID="ddlmonths" Style="margin-left: 10px; padding: 0px; height: 22px; width: 200px;"
                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlmonths_SelectedIndexChanged" />
                <label style="margin-left: 10px;">Select Period</label>
                <asp:DropDownList runat="server" ID="ddlPeriod" Style="margin-left: 10px; padding: 0px; height: 22px; width: 200px;"
                    CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged">
                    <asp:ListItem Value="3" Text="3 Months" />
                    <asp:ListItem Value="6" Text="6 Months" />
                </asp:DropDownList>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div style="float: right; margin-right: 18px; margin-top: -25px;">
            <asp:ImageButton ID="btnExcel" runat="server" AutoPostBack="true" Text="Excel" OnClick="btnExcel_Click" ImageUrl="~/Images/excel.png" Width="30px" Height="30px"></asp:ImageButton>
            <asp:ImageButton ID="btnPrint" runat="server" Text="Print" AutoPostBack="true" OnClick="btnPrint_Click" ImageUrl="~/Images/print.png" Width="30px" Height="30px"></asp:ImageButton>
            <asp:ImageButton ID="btnEmail" runat="server" Text="Email" AutoPostBack="true" OnClick="btnEmail_Click" ImageUrl="~/Images/email.png" Width="30px" Height="30px"></asp:ImageButton>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="margin-top: 20px;">
                <tr>
                    <th class="ui-widget-header">
                        <asp:Label ID="lblTableTitel" runat="server" Text="Past 3 Months's Entity-wise Compliance Summary"></asp:Label>
                    </th>
                </tr>
                <tr>
                    <td>
                        <div id="divSumaaryReport" runat="server" style="width: 1300px; height: 400px; margin-top: 15px; overflow: scroll">
                            <asp:GridView runat="server" ID="grdSummaryStatusReport" AutoGenerateColumns="true"
                                AllowSorting="true" GridLines="Both" OnRowCreated="grdSummaryStatusReport_RowCreated"
                                BackColor="White" BorderColor="Gray" BorderStyle="Solid" OnRowDataBound="grdSummaryStatusReport_RowDataBound"
                                BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="false"
                                Size="8px" Width="100%">
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                                <RowStyle HorizontalAlign="Center" />
                                <AlternatingRowStyle BackColor="#E6EFF7" />
                                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                                <EmptyDataTemplate>
                                    No Records Found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
                <tr>
                    <table>
                        <tr>
                            <td style="background-color: #00CC33; width: 50px;"></td>
                            <td style="font-size: 11px;">indicates 100% High Risk and Medium Risk compliances are completed in time and atleast 75% Low Risk compliances are completed in time.</td>
                        </tr>
                        <tr>
                            <td style="background-color: #FF9933; width: 50px;"></td>
                            <td style="font-size: 11px;">indicates 75% High Risk compliances are completed in time and atleast 50% Medium Risk and Low Risk compliances are completed in time.</td>
                        </tr>
                        <tr>
                            <td style="background-color: #FF0033; width: 50px;"></td>
                            <td style="font-size: 11px;">otherwise red.</td>
                        </tr>
                    </table>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
