﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardUpcomingCompliancesNew.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.DashboardUpcomingCompliancesNew" %>

<%@ Register Src="~/Controls/ComplianceStatusTransaction.ascx" TagName="ComplianceStatusTransaction"
    TagPrefix="vit" %>
<script type="text/javascript">
    function initializeDatePicker(date) {

        var startDate = new Date();
        $(".StartDate").datepicker({
            dateFormat: 'dd-mm-yy',
            setDate: startDate,
            numberOfMonths: 1
        });
    }

    function setDate() {
        $(".StartDate").datepicker();
    }
</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" OnLoad="UpdatePanel1_Load">
    <ContentTemplate>
        <div style="margin-bottom: 4px">
            <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
        </div>
        <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" AllowSorting="true"
            GridLines="Both" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid"
            BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="13" OnSorting="grdComplianceTransactions_Sorting"
            Width="100%" Font-Size="12px" DataKeyNames="ScheduledOnID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging"
            OnRowDataBound="grdComplianceTransactions_RowDataBound" OnRowCommand="grdComplianceTransactions_RowCommand" OnRowCreated="grdComplianceTransactions_RowCreated">
            <Columns>
                <asp:TemplateField HeaderText="Location-wise Compliance" ItemStyle-Width="30%" SortExpression="Description">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 450px;">
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("ShortDescription") %>' ToolTip='<%# Eval("Description") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" HeaderText="Risk Category">
                    <ItemTemplate>
                        <asp:Image runat="server" ID="imtemplat" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Impact" ItemStyle-Width="20%" SortExpression="NonComplianceType">
                    <ItemTemplate>
                        <asp:Label ID="lblImpact" runat="server" Text='<%# Eval("NonComplianceType") %>'></asp:Label>
                        <asp:Label ID="lblRisk" runat="server" Text='<%# Eval("Risk") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblComplianceId" runat="server" Text='<%# Eval("ComplianceID") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblFilePath" runat="server" Text='<%# Eval("FilePath") %>' Visible="false"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Due Date" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="ScheduledOn">
                    <ItemTemplate>
                        <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="For Month" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="ForMonth">
                    <ItemTemplate>
                        <%# Eval("ForMonth") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Height="20px" SortExpression="Status" ItemStyle-Width="20%" />
                <asp:TemplateField HeaderText="Event" ItemStyle-Width="30%" SortExpression="EventID">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                            <asp:Label ID="lblEventID" runat="server" Text='<%# ShowType((long?)Eval("EventID")) %>' ToolTip='<%# ShowType((long?)Eval("EventID")) %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Event Name" ItemStyle-Width="30%" SortExpression="EventName">
                    <ItemTemplate>
                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                            <asp:Label ID="lblEventName" runat="server" Text='<%# Eval("EventName") %>' ToolTip='<%# Eval("EventName") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="Event Nature" ItemStyle-Width="30%" SortExpression="EventNature">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                    <asp:Label ID="lblEventNature" runat="server" Text='<%# Eval("EventNature") %>' ToolTip='<%# Eval("EventNature") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                <asp:TemplateField HeaderText="Reference Material" ItemStyle-Width="30%" SortExpression="ReferenceMaterialText">
                    <ItemTemplate>
                        <asp:UpdatePanel ID="Updownload" runat="server">
                            <ContentTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 200px;">
                                    <asp:LinkButton ID="lnkDownload" runat="server" Text="Download" OnClick="lnkDownload_Click"></asp:LinkButton>
                                    <asp:Label ID="lblReferenceMaterialText" runat="server" Text="Text" ToolTip='<%# Eval("ReferenceMaterialText") %>'></asp:Label>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="lnkDownload" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField ItemStyle-Width="5%" ItemStyle-Height="22px" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:LinkButton ID="btnChangeStatus" runat="server" Visible='<%# CanChangeStatus((long)Eval("UserID"), (int)Eval("RoleID"), (int)Eval("ComplianceStatusID")) %>'
                            CommandName="CHANGE_STATUS" OnClick="btnChangeStatus_Click" CommandArgument='<%# Eval("ScheduledOnID") + "," + Eval("ComplianceInstanceID") %>'><img src='<%# ResolveUrl("~/Images/change_status_icon.png")%>' alt="Change Status" title="Change Status" /></asp:LinkButton>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:TemplateField>


            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
            <PagerSettings Position="Top" />
            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
            <AlternatingRowStyle BackColor="#E6EFF7" />
            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
            <EmptyDataTemplate>
                No Records Found.
            </EmptyDataTemplate>
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>
<vit:ComplianceStatusTransaction runat="server" ID="udcStatusTranscatopn" />
