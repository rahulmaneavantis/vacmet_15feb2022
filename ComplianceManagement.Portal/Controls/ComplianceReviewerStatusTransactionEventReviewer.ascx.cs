﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.IO;
using System.Dynamic;
using Ionic.Zip;
using com.VirtuosoITech.ComplianceManagement.Portal.Properties;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class ComplianceReviewerStatusTransactionEventReviewer : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
        }

        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        private void BindTransactionDetails(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {
                lblPenalty.Text = string.Empty;
                lblRisk.Text = string.Empty;
                lbDownloadSample.Text = string.Empty;

                ViewState["complianceInstanceID"] = ScheduledOnID;
                var complianceInfo = Business.ComplianceManagement.GetComplianceByInstanceID(ScheduledOnID);
                var complianceForm = Business.ComplianceManagement.GetComplianceFormByID(complianceInfo.ID);
                var RecentComplianceTransaction = Business.ComplianceManagement.GetCurrentStatusByComplianceID(ScheduledOnID);

                if (complianceInfo != null)
                {
                    lblComplianceDiscription.Text = complianceInfo.Description;
                    string Penalty = Business.ComplianceManagement.GetPanalty(complianceInfo);
                    lblPenalty.Text = Penalty;
                    string risk = Business.ComplianceManagement.GetRisk(complianceInfo, complianceInstanceID);
                    lblRisk.Text = risk;

                    var ActInfo = Business.ActManagement.GetByID(complianceInfo.ActID);
                    if (ActInfo != null)
                    {
                        lblActName.Text = ActInfo.Name;
                    }
                    lblRule.Text = complianceInfo.Sections;
                    lblFormNumber.Text = complianceInfo.RequiredForms;
                }
                if (complianceInfo.UploadDocument == true && complianceForm != null)
                {
                    lbDownloadSample.Text = "Click <u>here</u> to download sample form.";
                    lbDownloadSample.CommandArgument = complianceForm.ComplianceID.ToString();
                    lblNote.Visible = true;
                }
                else
                {
                    lblNote.Visible = false;
                }


                var documentVersionData = DocumentManagement.GetFileData1(Convert.ToInt32(RecentComplianceTransaction.ComplianceScheduleOnID)).Select(x => new
                {
                    ID = x.ID,
                    ScheduledOnID = x.ScheduledOnID,
                    Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                    VersionDate = x.VersionDate,
                    VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();


                rptComplianceVersion.DataSource = documentVersionData;
                rptComplianceVersion.DataBind();

                rptComplianceDocumnets.DataSource = null;
                rptComplianceDocumnets.DataBind();

                rptWorkingFiles.DataSource = null;
                rptWorkingFiles.DataBind();


                BindStatusList(Convert.ToInt32(RecentComplianceTransaction.ComplianceStatusID));
                BindTransactions(ScheduledOnID);
                tbxRemarks.Text = string.Empty;
                tbxDate.Text = RecentComplianceTransaction.StatusChangedOn != null ? RecentComplianceTransaction.StatusChangedOn.Value.ToString("dd-MM-yyyy") : " ";
                hdnComplianceInstanceID.Value = complianceInstanceID.ToString();
                hdnComplianceScheduledOnId.Value = ScheduledOnID.ToString();

                if (complianceInfo.UpDocs == true)
                {
                    rdbtnStatus.Attributes.Add("disabled", "disabled");
                    tbxRemarks.Attributes.Add("disabled", "disabled");
                    tbxDate.Attributes.Add("disabled", "disabled");
                    btnSave.Attributes.Add("disabled", "disabled");
                    btnReject.Attributes.Add("disabled", "disabled");
                    fieldsetdownloadreview.Visible = true;
                }
                else
                {
                    rdbtnStatus.Attributes.Remove("disabled");                   
                    tbxRemarks.Attributes.Remove("disabled");
                    tbxDate.Attributes.Remove("disabled");
                    btnSave.Attributes.Remove("disabled");
                    btnReject.Attributes.Remove("disabled");

                    fieldsetdownloadreview.Visible = false;
                    rdbtnStatus.Attributes.Add("enabled", "enabled");
                    tbxRemarks.Attributes.Add("enabled", "enabled");
                    tbxDate.Attributes.Add("enabled", "enabled");
                    btnSave.Attributes.Add("enabled", "enabled");
                    btnReject.Attributes.Add("enabled", "enabled");
                }
               
                upComplianceDetails.Update();
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "$(\"#divReviewerComplianceDetailsDialog\").dialog('open');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceDocumnets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
               
                DownloadFile(Convert.ToInt32(e.CommandArgument));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();
                ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();

                if (commandArgs[1].Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                }

                if (e.CommandName.Equals("version"))
                {
                    if (e.CommandName.Equals("version"))
                    {
                        rptComplianceDocumnets.DataSource = ComplianceFileData.Where(entry => entry.FileType == 1).ToList();
                        rptComplianceDocumnets.DataBind();

                        rptWorkingFiles.DataSource = ComplianceFileData.Where(entry => entry.FileType == 2).ToList();
                        rptWorkingFiles.DataBind();

                        var documentVersionData = ComplianceDocument.Select(x => new
                        {
                            ID = x.ID,
                            ScheduledOnID = x.ScheduledOnID,
                            Version = string.IsNullOrEmpty(x.Version) ? "1.0" : x.Version,
                            VersionDate = x.VersionDate,
                            VersionComment = string.IsNullOrEmpty(x.VersionComment) ? "" : x.VersionComment
                        }).GroupBy(entry => entry.Version).Select(entry => entry.FirstOrDefault()).ToList();

                        rptComplianceVersion.DataSource = documentVersionData;
                        rptComplianceVersion.DataBind();

                        upComplianceDetails.Update();

                       
                    }
                }
                else if (e.CommandName.Equals("Download"))
                {

                    using (ZipFile ComplianceZip = new ZipFile())
                    {

                        var ComplianceData = DocumentManagement.GetForMonth(Convert.ToInt32(commandArgs[0]));
                        ComplianceZip.AddDirectoryByName(ComplianceData.ForMonth + "/" + commandArgs[1]);


                        if (ComplianceFileData.Count > 0)
                        {
                            int i = 0;
                            foreach (var file in ComplianceFileData)
                            {
                                string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string[] filename = file.FileName.Split('.');
                                    string str = filename[0] + i + "." + filename[1];
                                    if (file.EnType == "M")
                                    {
                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        ComplianceZip.AddEntry(ComplianceData.ForMonth + "/" + commandArgs[1] + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    i++;
                                }
                            }
                        }

                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = 0;
                        byte[] data = zipMs.ToArray();

                        Response.Buffer = true;

                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocuments.zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        Response.End();
                    }
                }

               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptWorkingFiles_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            DownloadFile(Convert.ToInt32(e.CommandArgument));
        }

        void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdTransactionHistory_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                var complianceTransactionHistory = Business.ComplianceManagement.GetAllTransactionLog(Convert.ToInt32(ViewState["complianceInstanceID"]));
                if (direction == SortDirection.Ascending)
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    complianceTransactionHistory = complianceTransactionHistory.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdTransactionHistory.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndexHistory"] = grdTransactionHistory.Columns.IndexOf(field);
                    }
                }

                grdTransactionHistory.DataSource = complianceTransactionHistory;
                grdTransactionHistory.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdTransactionHistory.PageIndex = e.NewPageIndex;
                BindTransactions(Convert.ToInt32(ViewState["complianceInstanceID"]));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindTransactions(int ScheduledOnID)
        {
            try
            {
                // throw new NotImplementedException();
                grdTransactionHistory.DataSource = Business.ComplianceManagement.GetAllTransactionLog(ScheduledOnID);
                grdTransactionHistory.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindStatusList(int statusID)
        {
            try
            {

                rdbtnStatus.Items.Clear();  
                var statusList = ComplianceStatusManagement.GetStatusList();

                List<ComplianceStatu> allowedStatusList = null;

                List<ComplianceStatusTransition> ComplianceStatusTransitionList = ComplianceStatusManagement.GetStatusTransitionListByInitialId(statusID);
                List<int> finalStatusIDs = ComplianceStatusTransitionList.Select(entry => entry.FinalStateID).ToList();

                allowedStatusList = statusList.Where(entry => finalStatusIDs.Contains(entry.ID)).OrderBy(entry => entry.Name).ToList();

                foreach (ComplianceStatu st in allowedStatusList)
                {
                    if (!(st.ID == 6))
                    {
                        rdbtnStatus.Items.Add(new ListItem(st.Name, st.ID.ToString()));
                    }
                }

                lblStatus.Visible = allowedStatusList.Count>0?true:false;
                divDated.Visible = allowedStatusList.Count > 0 ? true : false;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (OnSaved != null)
            {
                OnSaved(this, null);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                 int StatusID = 0;
                if(lblStatus.Visible)
                    StatusID = Convert.ToInt32(rdbtnStatus.SelectedValue);
                else
                    StatusID= Convert.ToInt32(ComplianceManagement.Business.ComplianceManagement.GetClosedTransaction(Convert.ToInt32(hdnComplianceScheduledOnId.Value)).ComplianceStatusID);

                ComplianceTransaction transaction = new ComplianceTransaction()
                {
                    ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                    ComplianceInstanceId = Convert.ToInt64(hdnComplianceInstanceID.Value),
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedByText = AuthenticationHelper.User,
                    StatusId = StatusID,
                    StatusChangedOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                    Remarks = tbxRemarks.Text
                };

                Business.ComplianceManagement.CreateTransaction(transaction);

                if (OnSaved != null)
                {
                    OnSaved(this, null);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            try
            {

                ComplianceTransaction transaction = new ComplianceTransaction()
                {
                    ComplianceScheduleOnID = Convert.ToInt64(hdnComplianceScheduledOnId.Value),
                    ComplianceInstanceId = Convert.ToInt64(hdnComplianceInstanceID.Value),
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedByText = AuthenticationHelper.User,
                    StatusId = 6,
                    StatusChangedOn = DateTime.ParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                    Remarks = tbxRemarks.Text
                };

                Business.ComplianceManagement.CreateTransaction(transaction);

                if (OnSaved != null)
                {
                    OnSaved(this, null);
                }

                var Compliance = ComplianceManagement.Business.ComplianceManagement.GetInstanceTransactionCompliance(Convert.ToInt64(hdnComplianceScheduledOnId.Value));
                if (Compliance != null)
                {
                    int customerID = -1;
                    customerID = UserManagement.GetByID(Convert.ToInt32(Compliance.UserID)).CustomerID ?? 0;
                    string ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                    var user = UserManagement.GetByID(Convert.ToInt32(Compliance.UserID));
                    string accessURL = string.Empty;
                    URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                    if (Urloutput != null)
                    {
                        accessURL = Urloutput.URL;
                    }
                    else
                    {
                        accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                    }
                    string message = Settings.Default.EMailTemplate_RejectedCompliance
                                   .Replace("@User", Compliance.User)
                                   .Replace("@ComplianceDescription", Compliance.ShortDescription)
                                   .Replace("@Role", Compliance.Role)
                                   .Replace("@ScheduledOn", Compliance.ScheduledOn.ToString("dd-MMM-yyyy"))
                                   .Replace("@From", ReplyEmailAddressName)
                                   .Replace("@URL", Convert.ToString(accessURL));
                    //string message = Settings.Default.EMailTemplate_RejectedCompliance
                    //                    .Replace("@User", Compliance.User)
                    //                    .Replace("@ComplianceDescription", Compliance.ShortDescription)
                    //                    .Replace("@Role", Compliance.Role)
                    //                    .Replace("@ScheduledOn", Compliance.ScheduledOn.ToString("dd-MMM-yyyy"))
                    //                    .Replace("@From", ReplyEmailAddressName)
                    //                    .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"], (new string[] { user.Email }).ToList(), null, null, "AVACOM Notification :: Compliance has been rejected.", message);

                }

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OpenDialog", "$(\"#divReviewerComplianceDetailsDialog\").dialog('close');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName.Equals("DOWNLOAD_FILE"))
                {
                    int fileID = Convert.ToInt32(e.CommandArgument);
                    var file = Business.ComplianceManagement.GetFile(fileID);

                    if (file.FilePath == null)
                    {
                        using (FileStream fs = File.OpenRead(Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name))))
                        {
                            int length = (int)fs.Length;
                            byte[] buffer;

                            using (BinaryReader br = new BinaryReader(fs))
                            {
                                buffer = br.ReadBytes(length);
                            }

                            Response.Buffer = true;
                            Response.Clear();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(buffer)); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(buffer)); // create the file
                            }
                            Response.Flush(); // send it to the client to download
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(Convert.ToInt32(hdlSelectedDocumentID.Value));

                Response.Buffer = true;
                //Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                //Response.BinaryWrite(file.Data); // create the file
                Response.End();
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void lbDownloadSample_Click(object sender, EventArgs e)
        {
            try
            {
                var file = Business.ComplianceManagement.GetComplianceFormByID(Convert.ToInt32(lbDownloadSample.CommandArgument));

                Response.Buffer = true;
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                Response.BinaryWrite(file.FileData); // create the file
                Response.Flush(); // send it to the client to download
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }

                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "Script", "initializeComboboxUpcoming();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdTransactionHistory_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndexHistory"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        public void OpenTransactionPage(int ScheduledOnID, int complianceInstanceID)
        {
            try
            {
                
                tbxRemarks.Text = tbxDate.Text = string.Empty;
                BindTransactionDetails(ScheduledOnID, complianceInstanceID);
                //upUsers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public event EventHandler OnSaved;

        public void DownloadFile(int fileId)
        {
            try
            {
                var file = Business.ComplianceManagement.GetFile(fileId);

                if (file.FilePath != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                    if (filePath != null && File.Exists(filePath))
                    {
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                        if (file.EnType == "M")
                        {
                            Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }
                        else
                        {
                            Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                        }

                        Response.Flush(); // send it to the client to download
                        Response.End();
                        
                    }
                }

              
            }
            catch (Exception)
            {
                throw;
                //LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptComplianceDocumnets_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplianceDocumnets");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }

        protected void rptComplianceVersion_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnComplinceVersionDoc");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);

                LinkButton lblDocumentVersion = (LinkButton)e.Item.FindControl("lblDocumentVersion");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersion);
            }
        }

        protected void rptWorkingFiles_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lblDownLoadfile = (LinkButton)e.Item.FindControl("btnWorkingFiles");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lblDownLoadfile);
            }
        }   

        protected void btnUpload_Click(object sender, EventArgs e)
        {


            HttpFileCollection fileCollection = Request.Files;
            for (int i = 0; i < fileCollection.Count; i++)
            {
                HttpPostedFile uploadfile = fileCollection[i];
                string fileName = Path.GetFileName(uploadfile.FileName);
                if (uploadfile.ContentLength > 0)
                {
                    //uploadfile.SaveAs(Server.MapPath("~/UploadFiles/") + fileName);
                    //lblMessage.Text += fileName + "Saved Successfully<br>";
                }
            }
        }
    
    }

}