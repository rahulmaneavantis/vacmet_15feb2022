﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;
using System.IO;
using System.Dynamic;
using Ionic.Zip;
using com.VirtuosoITech.ComplianceManagement.Portal.Properties;
using System.Configuration;
using System.Net;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class compliancestatusreviewernew : System.Web.UI.Page
    {    
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    BindStatusList();
                    int complianceScheduleOnID = Convert.ToInt32(Request.QueryString["sId"].ToString());
                    ViewState["complianceScheduleOnID"] = complianceScheduleOnID;
                    int cominstanceid = Convert.ToInt32(Request.QueryString["InId"].ToString());
                    ViewState["cominstanceid"] = cominstanceid;                   
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }                             
            }
        }

        public static List<Log_ComplianceStatus> GetAllComplianceType(string filter = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var complianceTypes = (from row in entities.Log_ComplianceStatus
                                       select row);

               
                return complianceTypes.ToList();
            }
        }

        protected void ddlFilterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindStatusList()
        {
            try
            {
                ddlFilterType.DataTextField = "Name";
                ddlFilterType.DataValueField = "ComplianceStatusID";
                ddlFilterType.DataSource = GetAllComplianceType();
                ddlFilterType.DataBind();
                ddlFilterType.Items.Insert(0, new ListItem("< Select Status >", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave1_Click(object sender, EventArgs e)
        {
            try
             {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    if (!string.IsNullOrEmpty(ddlFilterType.SelectedValue) && ddlFilterType.SelectedValue != "-1")
                    {
                        long CSOID = 0;
                        long CIID = 0;
                        int StatusID = 0;
                        if (ViewState["complianceScheduleOnID"] != null)
                        {
                            CSOID = Convert.ToInt32(ViewState["complianceScheduleOnID"]);
                        }
                        if (ViewState["cominstanceid"] != null)
                        {
                            CIID = Convert.ToInt32(ViewState["cominstanceid"]);
                        }
                        StatusID = Convert.ToInt32(ddlFilterType.SelectedValue);
                        var RecentComplianceTransaction = Business.ComplianceManagement.GetCurrentStatusByComplianceID((int)CSOID);
                        #region Save Code
                        try
                        {
                            string ISrejectdelete = "A";
                            var ctdetatls = (from row in entities.ComplianceTransactions
                                             where row.ID == RecentComplianceTransaction.ComplianceTransactionID
                                             select row).FirstOrDefault();
                            if (ctdetatls != null)
                            {

                                ComplianceTransaction transaction = new ComplianceTransaction()
                                {
                                    ComplianceScheduleOnID = ctdetatls.ComplianceScheduleOnID,
                                    ComplianceInstanceId = ctdetatls.ComplianceInstanceId,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedByText = AuthenticationHelper.User,
                                    StatusId = StatusID,
                                    Remarks = tbxRemarks1.Text,
                                    ComplianceSubTypeID = ctdetatls.ComplianceSubTypeID,
                                };
                                if (StatusID == 1)
                                {
                                    transaction.Interest = null;
                                    transaction.Penalty = null;
                                    transaction.StatusChangedOn = null;
                                    transaction.ValuesAsPerSystem = null;
                                    transaction.ValuesAsPerReturn = null;
                                    transaction.LiabilityPaid = null;
                                    transaction.IsPenaltySave = null;
                                    transaction.PenaltySubmit = null;
                                }
                                else
                                {
                                    transaction.Interest = ctdetatls.Interest;
                                    transaction.Penalty = ctdetatls.Penalty;
                                    transaction.StatusChangedOn = ctdetatls.StatusChangedOn;
                                    transaction.ValuesAsPerSystem = ctdetatls.ValuesAsPerSystem;
                                    transaction.ValuesAsPerReturn = ctdetatls.ValuesAsPerReturn;
                                    transaction.LiabilityPaid = ctdetatls.LiabilityPaid;
                                    transaction.IsPenaltySave = ctdetatls.IsPenaltySave;
                                    transaction.PenaltySubmit = ctdetatls.PenaltySubmit;
                                }
                                Business.ComplianceManagement.CreateTransaction(transaction);
                                CreateLogForScheduleOn(Convert.ToInt64(CSOID), "S", ISrejectdelete, (int)RecentComplianceTransaction.ComplianceStatusID);
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "closeWin();", true);
                            }
                        }
                        catch (Exception ex)
                        {
                            LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "topFunction", "topFunction();", true);
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }         
        public static void CreateLogForScheduleOn(long scheduleid, string flag, string ISrejectdelete,int statusid)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Log_RejectDeleteSchedule logchange = new Log_RejectDeleteSchedule()
                    {
                        ScheduleOnID = scheduleid,
                        IsStatutory = flag,
                        IsRejectDelete = ISrejectdelete,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        StatusID=statusid,
                    };
                    entities.Log_RejectDeleteSchedule.Add(logchange);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
               
            }
        }
      
        protected void upComplianceDetails1_Load(object sender, EventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }                             
        protected void btnReviewClose_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "closeWin();", true);                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }       
    }
}