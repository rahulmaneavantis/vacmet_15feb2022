﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Configuration;
using System.Threading;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Users;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class AuditUserDetailsControl : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rblAuditRole.Enabled = true;
                BindCustomers();
                BindRoles();
                BindRolesRisk();
                BindDepartment();
                UserImageUpload.Attributes["onchange"] = "UploadFile(this)";
                ddlRole.Enabled = true;
            }
        }

        protected void Upload(object sender, EventArgs e)
        {
            if (UserImageUpload.HasFile)
            {
                string[] validFileTypes = { "bmp", "gif", "png", "jpg", "jpeg" };

                string ext = System.IO.Path.GetExtension(UserImageUpload.PostedFile.FileName);
                bool isValidFile = false;
                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidFile = true;
                        break;
                    }
                }
                if (!isValidFile)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Invalid File. Please upload a File with extension " + string.Join(",", validFileTypes);
                }

            }
        }

        protected void upUsers_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tvBranches.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rblAuditRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlRole.SelectedIndex = -1;
            ddlRiskRole.SelectedIndex = -1;

            if (rblAuditRole.SelectedItem.Text == "Is Audit Head" || rblAuditRole.SelectedItem.Text == "Is Audit Manager")
            {
                ddlRole.Enabled = false;
                //ddlRole.Items.FindByText("Auditor-Auditee").Selected = true;

                ddlRiskRole.Enabled = false;
                ddlRiskRole.Items.FindByText("Auditor-Auditee").Selected = true;
            }
        }
        private void BindDepartment()
        {
            try
            {

                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "ID";
                ddlDepartment.DataSource = CompDeptManagement.FillDepartment(Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID));
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("Select Department", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindRoles()
        {
            try
            {
                ddlRole.DataTextField = "Name";
                ddlRole.DataValueField = "ID";

                var roles = RoleManagement.GetAll(false);
                if (AuthenticationHelper.Role == "CADMN")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN") && !entry.Code.Equals("IMPT")).ToList();
                }

                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("IMPT")).ToList();
                }
                ddlRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlRole.DataBind();
                ddlRole.Items.Insert(0, new ListItem("Select Role", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindRolesRisk()
        {
            try
            {
                ddlRiskRole.DataTextField = "Name";
                ddlRiskRole.DataValueField = "ID";

                var roles = RoleManagementRisk.GetLimitedRoleAudit();
                if (AuthenticationHelper.Role == "CADMN")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN")).ToList();
                }

                ddlRiskRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlRiskRole.DataBind();

                ddlRiskRole.Items.Insert(0, new ListItem("Select Role", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void ddlRiskRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlRiskRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }
                divReportingTo.Visible = divCustomer.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                divCustomerBranch.Visible = roleCode.Equals("EXCT");
                if (divCustomer.Visible && AuthenticationHelper.Role != "CADMN")
                {
                    // ddlCustomer.SelectedValue = "-1";
                    ddlCustomer_SelectedIndexChanged(null, null);
                }
                if (divCustomerBranch.Visible)
                {
                    ddlCustomer_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tvBranches.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                }

                if (divReportingTo.Visible)
                {
                    BindReportingTo();
                }

                if (Convert.ToInt32(ddlRiskRole.SelectedValue) == 2 || Convert.ToInt32(ddlRiskRole.SelectedValue) == 1 || Convert.ToInt32(ddlRiskRole.SelectedValue) == 8 || Convert.ToInt32(ddlRiskRole.SelectedValue) == 12)
                {
                    rblAuditRole.Enabled = false;
                    rblAuditRole.ClearSelection();
                }
                else
                {
                    rblAuditRole.Enabled = true;

                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }
        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }
                divReportingTo.Visible = divCustomer.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                divCustomerBranch.Visible = roleCode.Equals("EXCT");
                if (divCustomer.Visible && AuthenticationHelper.Role != "CADMN")
                {
                    //ddlCustomer.SelectedValue = "-1";
                    ddlCustomer_SelectedIndexChanged(null, null);
                }
                if (divCustomerBranch.Visible)
                {
                    ddlCustomer_SelectedIndexChanged(null, null);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tvBranches.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                }

                if (divReportingTo.Visible)
                {
                    BindReportingTo();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCustomers()
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);


                ddlCustomer.DataSource = CustomerManagement.GetAll(customerID);
                ddlCustomer.DataBind();

                if (AuthenticationHelper.Role != "CADMN")
                    ddlCustomer.Items.Insert(0, new ListItem("Select Customer", "-1"));
                ddlCustomer.SelectedValue = Convert.ToString(AuthenticationHelper.CustomerID);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = string.Empty;
                BindCustomerBranches();
                BindReportingTo();
                if (divCustomerBranch.Visible)
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tvBranches.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindReportingTo()
        {
            try
            {
                ddlReportingTo.DataTextField = "Name";
                ddlReportingTo.DataValueField = "ID";

                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }

                ddlReportingTo.DataSource = UserManagement.GetAllByCustomerID(Convert.ToInt32(ddlCustomer.SelectedValue), roleCode);
                ddlReportingTo.DataBind();

                ddlReportingTo.Items.Insert(0, new ListItem("Select Reporting to person", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "Select Branch";
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                tvBranches.Nodes.Clear();
                NameValueHierarchy branch = null;
                var branchs = CustomerBranchManagement.GetAllHierarchy(Convert.ToInt32(ddlCustomer.SelectedValue));
                if (branchs.Count > 0)
                {
                    branch = branchs[0];
                }
                tbxBranch.Text = "Select Branch";
                List<TreeNode> nodes = new List<TreeNode>();
                BindBranchesHierarchy(null, branch, nodes);
                foreach (TreeNode item in nodes)
                {
                    tvBranches.Nodes.Add(item);
                }

                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindParameters(List<UserParameterValueInfo> userParameterValues = null)
        {
            try
            {
                if (userParameterValues == null)
                {
                    userParameterValues = new List<UserParameterValueInfo>();
                    var userParameters = UserManagement.GetAllUserParameters();
                    userParameters.ForEach(entry =>
                    {
                        userParameterValues.Add(new UserParameterValueInfo()
                        {
                            UserID = -1,
                            ParameterID = entry.ID,
                            ValueID = -1,
                            Name = entry.Name,
                            DataType = (DataType)entry.DataType,
                            Length = (int)entry.Length,
                            Value = string.Empty
                        });
                    });
                }

                repParameters.DataSource = userParameterValues;
                repParameters.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void AddNewUser(int CustomerID)
        {
            try
            {
                ViewState["Mode"] = 0;
                tbxFirstName.Text = tbxLastName.Text = tbxDesignation.Text = tbxEmail.Text = tbxContactNo.Text = tbxAddress.Text = string.Empty;
                //tbxUsername.Enabled = true;
                tbxEmail.Enabled = true;
                ddlCustomer.Enabled = true;
                rblAuditRole.Enabled = true;
                rblAuditRole.ClearSelection();
                ddlDepartment.ClearSelection();
                ddlRiskRole.ClearSelection();
                ddlRole.ClearSelection();
                ddlRole.SelectedValue = "-1";
                ddlRole_SelectedIndexChanged(null, null);
                chkHead.Checked = false;
                BindParameters();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool ab = false;
                    int customerID = -1;
                    if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                    {
                        customerID = CustomerID;
                    }
                    else
                    {

                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    }
                    bool ac = UserManagementRisk.CheckProductmapping(customerID, 1);
                    if (ac == true)
                    {
                        divComplianceRole.Visible = true;
                        ddlRole.Enabled = true;
                    }
                    else
                    {
                        divComplianceRole.Visible = false;
                        ddlRole.Enabled = false;
                    }
                    bool a = UserManagementRisk.CheckProductmapping(customerID, 3);
                    if (a == true)
                    {
                        divRiskRole.Visible = true;
                        ddlRiskRole.Enabled = true;
                        ddlRiskRole.Attributes.Remove("disabled");
                    }
                    else
                    {
                        ab = UserManagementRisk.CheckProductmapping(customerID, 4);
                        if (ab == true)
                        {
                            divRiskRole.Visible = true;
                            ddlRiskRole.Enabled = true;
                            ddlRiskRole.Attributes.Remove("disabled");
                        }
                        else
                        {
                            divRiskRole.Visible = false;
                            ddlRiskRole.Enabled = false;
                        }
                    }
                    if (ac == false && a == false && ab == false)
                    {
                        divComplianceRole.Visible = true;
                        ddlRole.Enabled = true;
                    }

                }
                upUsers.Update(); upUsers_Load(null, null);
                //ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "OpenDialog", "$(\"#divUsersDialog\").dialog('open');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void AddNewUser()
        {
            try
            {
                ViewState["Mode"] = 0;

                tbxFirstName.Text = tbxLastName.Text = tbxDesignation.Text = tbxEmail.Text = tbxContactNo.Text = tbxAddress.Text = string.Empty;
                //tbxUsername.Enabled = true;
                tbxEmail.Enabled = true;
                ddlCustomer.Enabled = true;
                rblAuditRole.Enabled = true;
                rblAuditRole.ClearSelection();
                ddlDepartment.ClearSelection();
                ddlRiskRole.ClearSelection();
                ddlRole.ClearSelection();
                ddlRole.SelectedValue = "-1";
                ddlRole_SelectedIndexChanged(null, null);
                chkHead.Checked = false;
                BindParameters();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool ab = false;
                    int customerID = -1;
                    //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    bool ac = UserManagementRisk.CheckProductmapping(customerID, 1);
                    if (ac == true)
                    {
                        divComplianceRole.Visible = true;
                    }
                    else
                    {
                        divComplianceRole.Visible = false;
                    }
                    bool a = UserManagementRisk.CheckProductmapping(customerID, 3);
                    if (a == true)
                    {
                        divRiskRole.Visible = true;
                    }
                    else
                    {
                        ab = UserManagementRisk.CheckProductmapping(customerID, 4);
                        if (ab == true)
                        {
                            divRiskRole.Visible = true;
                        }
                        else
                        {
                            divRiskRole.Visible = false;
                        }
                    }
                    if (ac == false && a == false && ab == false)
                    {
                        divComplianceRole.Visible = true;
                    }
                }
                upUsers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void EditUserInformation(int userID)
        {
            try
            {

                //ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox(0);", true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "disableCombobox", "disableCombobox();", true);
                ViewState["Mode"] = 1;
                ViewState["UserID"] = userID;

                User user = UserManagement.GetByID(userID);
                List<UserParameterValueInfo> userParameterValues = UserManagement.GetParameterValuesByUserID(userID);
                if (user.RoleID == 1 || user.RoleID == 2 || user.RoleID == 8 || user.RoleID == 12)
                {
                    rblAuditRole.Enabled = false;
                }
                tbxFirstName.Text = user.FirstName;
                tbxLastName.Text = user.LastName;
                tbxDesignation.Text = user.Designation;
                tbxEmail.Text = user.Email;
                tbxContactNo.Text = user.ContactNumber;
                //tbxUsername.Text = user.Username;
                tbxAddress.Text = user.Address;

                //tbxUsername.Enabled = false;
                //tbxEmail.Enabled = false;
                ddlRole.Enabled = false;

                ddlDepartment.SelectedValue = user.DepartmentID != null ? user.DepartmentID.ToString() : "-1";
                //ddlRole.SelectedValue = user.RoleID != null ? user.RoleID.ToString() : "-1";
                if (ddlRole.Items.FindByValue(Convert.ToString(user.RoleID)) != null)
                {
                    ddlRole.SelectedValue = Convert.ToString(user.RoleID);
                }
                
                ddlRole_SelectedIndexChanged(null, null);
                ddlRole.Attributes.Add("disabled", "disabled");

                if (user.IsHead == true)
                    chkHead.Checked = true;
                else
                    chkHead.Checked = false;
                //risk role

                if (user.IsAuditHeadOrMgr == "AH")
                    rblAuditRole.Items.FindByValue("IAH").Selected = true;

                if (user.IsAuditHeadOrMgr == "AM")
                    rblAuditRole.Items.FindByValue("IAM").Selected = true;

                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(userID);
                ddlRiskRole.SelectedValue = mstuser.RoleID != null ? mstuser.RoleID.ToString() : "-1";
                ddlRiskRole_SelectedIndexChanged(null, null);
                //ddlRiskRole.Attributes.Add("disabled", "disabled");

                if (divCustomer.Visible && user.CustomerID.HasValue)
                {
                    ddlCustomer.SelectedValue = user.CustomerID.Value.ToString();
                    ddlCustomer_SelectedIndexChanged(null, null);
                }

                ddlReportingTo.SelectedValue = user.ReportingToID != null ? user.ReportingToID.ToString() : "-1";

                if (divCustomerBranch.Visible && user.CustomerBranchID.HasValue)
                {
                    Queue<TreeNode> queue = new Queue<TreeNode>();
                    foreach (TreeNode node in tvBranches.Nodes)
                    {
                        queue.Enqueue(node);
                    }
                    while (queue.Count > 0)
                    {
                        TreeNode node = queue.Dequeue();
                        if (node.Value == user.CustomerBranchID.Value.ToString())
                        {
                            node.Selected = true;
                            break;
                        }
                        else
                        {
                            foreach (TreeNode child in node.ChildNodes)
                            {
                                queue.Enqueue(child);
                            }
                        }
                    }
                    tvBranches_SelectedNodeChanged(null, null);
                }

                if (user.ImagePath != null)


                    BindParameters(userParameterValues);
                upUsers.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tvBranches.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool a = false;
                int customerID = -1;
                customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                int getproductrisk = -1;

                getproductrisk = Convert.ToInt32(ddlRiskRole.SelectedValue);

                #region Compliance User
                User user = new User()
                {
                    FirstName = tbxFirstName.Text,
                    LastName = tbxLastName.Text,
                    Designation = tbxDesignation.Text,
                    Email = tbxEmail.Text,
                    ContactNumber = tbxContactNo.Text,
                    Address = tbxAddress.Text,
                    DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                    //LitigationRoleID= getproductcOMPLIANCE,
                    IsExternal = false,
                    CreatedFrom = 3
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    if (!string.IsNullOrEmpty(ddlRole.SelectedValue))
                    {
                        user.RoleID = Convert.ToInt32(ddlRole.SelectedValue);
                    }
                    else
                    {
                        user.RoleID = 7;
                    }
                }
                else
                {
                    user.RoleID = 7;
                }


                if (chkHead.Checked)
                    user.IsHead = true;
                else
                    user.IsHead = false;


                if (divCustomer.Visible)
                {
                    user.CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }

                if (divCustomerBranch.Visible)
                {
                    if (!string.IsNullOrEmpty(tvBranches.SelectedValue))
                    {
                        user.CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                    }
                }

                if (ddlReportingTo.SelectedValue != "-1" && ddlReportingTo.SelectedValue != "")
                {
                    user.ReportingToID = Convert.ToInt64(ddlReportingTo.SelectedValue);
                }

                if (ddlRole.SelectedValue == "9" && Auditor1.Visible)
                {
                    try
                    {
                        user.Startdate = DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        user.Enddate = DateTime.ParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    catch { }
                }

                if (ddlRole.SelectedValue == "9" && Auditor2.Visible)
                {
                    try
                    {
                        user.AuditStartPeriod = DateTime.ParseExact(txtperiodStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                        user.AuditEndPeriod = DateTime.ParseExact(txtperiodEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                    }
                    catch { }
                }
                List<UserParameterValue> parameters = new List<UserParameterValue>();
                foreach (var item in repParameters.Items)
                {
                    RepeaterItem entry = item as RepeaterItem;
                    TextBox tbxValue = ((TextBox)entry.FindControl("tbxValue"));
                    HiddenField hdnEntityParameterID = ((HiddenField)entry.FindControl("hdnEntityParameterID"));
                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));

                    parameters.Add(new UserParameterValue()
                    {
                        ID = Convert.ToInt32(hdnID.Value),
                        UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                        Value = tbxValue.Text
                    });
                }

                if (rblAuditRole.SelectedIndex != -1)
                {
                    if (rblAuditRole.SelectedItem.Text == "Is Audit Head")
                    {
                        user.IsAuditHeadOrMgr = "AH";
                    }

                    if (rblAuditRole.SelectedItem.Text == "Is Audit Manager")
                    {
                        user.IsAuditHeadOrMgr = "AM";
                    }
                }
                #endregion

                #region Risk User

                com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                {
                    FirstName = tbxFirstName.Text,
                    LastName = tbxLastName.Text,
                    Designation = tbxDesignation.Text,
                    Email = tbxEmail.Text,
                    ContactNumber = tbxContactNo.Text,
                    Address = tbxAddress.Text,
                    RoleID = getproductrisk,
                    DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                    //LitigationRoleID = getproductrisk,
                    IsExternal = false,
                    CreatedFrom = 3
                };

                if (chkHead.Checked)
                    mstUser.IsHead = true;
                else
                    mstUser.IsHead = false;

                if (divCustomer.Visible)
                {
                    mstUser.CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }

                if (divCustomerBranch.Visible)
                {
                    if (!string.IsNullOrEmpty(tvBranches.SelectedValue))
                    {
                        mstUser.CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                    }
                }

                List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk> parametersRisk = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk>();
                foreach (var item in repParameters.Items)
                {
                    RepeaterItem entry = item as RepeaterItem;
                    TextBox tbxValue = ((TextBox)entry.FindControl("tbxValue"));
                    HiddenField hdnEntityParameterID = ((HiddenField)entry.FindControl("hdnEntityParameterID"));
                    HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));

                    parametersRisk.Add(new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk()
                    {
                        ID = Convert.ToInt32(hdnID.Value),
                        UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                        Value = tbxValue.Text
                    });
                }

                if (rblAuditRole.SelectedIndex != -1)
                {
                    if (rblAuditRole.SelectedItem.Text == "Is Audit Head")
                    {
                        mstUser.IsAuditHeadOrMgr = "AH";
                    }
                    else if (rblAuditRole.SelectedItem.Text == "Is Audit Manager")
                    {
                        mstUser.IsAuditHeadOrMgr = "AM";
                    }
                }
                #endregion

                //if role is company admin
                if (getproductrisk == 2)
                {
                    var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
                    //Litigation Management
                    if (ProductMappingDetails.Contains(2))
                    {
                        if (user.LitigationRoleID == null)
                        {
                            user.LitigationRoleID = 2;
                            mstUser.LitigationRoleID = 2;
                        }
                    }
                    //Contract Management
                    if (ProductMappingDetails.Contains(5))
                    {
                        if (user.ContractRoleID == null)
                        {
                            user.ContractRoleID = 2;
                            mstUser.ContractRoleID = 2;
                        }
                    }
                    //License Management
                    if (ProductMappingDetails.Contains(6))
                    {
                        if (user.LicenseRoleID == null)
                        {
                            user.LicenseRoleID = 2;
                            mstUser.LicenseRoleID = 2;
                        }
                    }
                }
                if ((int)ViewState["Mode"] == 1)
                {
                    user.ID = Convert.ToInt32(ViewState["UserID"]);
                    mstUser.ID = Convert.ToInt32(ViewState["UserID"]);
                }

                bool emailExists;
                UserManagement.Exists(user, out emailExists);
                if (emailExists)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User with Same Email Already Exists.";
                    return;
                }
                UserManagementRisk.Exists(mstUser, out emailExists);
                if (emailExists)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User with Same Email Already Exists.";
                    return;
                }
                bool result = false;
                int resultValue = 0;
                if ((int)ViewState["Mode"] == 0)
                {
                    user.CreatedBy = AuthenticationHelper.UserID;
                    user.CreatedByText = AuthenticationHelper.User;
                    string passwordText = Util.CreateRandomPassword(10);
                    user.Password = Util.CalculateMD5Hash(passwordText);
                    string message = SendNotificationEmail(user, passwordText);
                    user.RoleID = 7;
                    //if (mstUser.RoleID != -1)
                    //{
                    //    user.RoleID = mstUser.RoleID;
                    //}

                    mstUser.CreatedBy = AuthenticationHelper.UserID;
                    mstUser.CreatedByText = AuthenticationHelper.User;
                    mstUser.CreatedFrom = 3;
                    mstUser.Password = Util.CalculateMD5Hash(passwordText);
                    //result = UserManagement.Create(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                    resultValue = UserManagement.CreateNew(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                    if (resultValue > 0)
                    {
                        result = UserManagementRisk.Create(mstUser, parametersRisk, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                        if (result == false)
                        {
                            UserManagement.deleteUser(resultValue);
                        }
                    }
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User Created Successfully.";
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    int userID = -1;
                    userID = Convert.ToInt32(ViewState["UserID"]);

                    //Get Existing Users Auditor ID and Compliance-Audit Product RoleID
                    User complianceUser = UserManagement.GetByID(userID);
                    mst_User auditUser = UserManagementRisk.GetByID(userID);

                    if (complianceUser != null)
                    {
                        user.AuditorID = complianceUser.AuditorID;
                        //user.RoleID = complianceUser.RoleID;
                    }

                    if (auditUser != null)
                    {
                        mstUser.AuditorID = auditUser.AuditorID;
                        //mstUser.RoleID = auditUser.RoleID;
                    }

                    User User = UserManagement.GetByID(Convert.ToInt32(user.ID));
                    if (tbxEmail.Text.Trim() != User.Email)
                    {
                        string message = SendNotificationEmailChanged(user);
                        string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                        EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Email ID Changed.", message);
                    }
                    result = UserManagement.Update(user, parameters);
                    result = UserManagementRisk.Update(mstUser, parametersRisk);
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "User Updated Successfully.";
                }
                else
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
                }

                if (result)
                {
                    if (UserImageUpload.HasFile)
                    {
                        string fileName = user.ID + "-" + user.FirstName + " " + user.LastName + Path.GetExtension(UserImageUpload.PostedFile.FileName);
                        UserImageUpload.PostedFile.SaveAs(Server.MapPath("~/UserPhotos/") + fileName);

                        string filepath = "~/UserPhotos/" + fileName;

                        UserManagement.UpdateUserPhoto(Convert.ToInt32(user.ID), filepath, fileName);
                        UserManagementRisk.UpdateUserPhoto(Convert.ToInt32(mstUser.ID), filepath, fileName);
                    }
                }

                //ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "CloseDialog", "$(\"#divUsersDialog\").dialog('close')", true);

                if (OnSaved != null)
                {
                    OnSaved(this, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private string SendNotificationEmail(User user, string passwordText)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }
                string accessURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    accessURL = Urloutput.URL;
                }
                else
                {
                    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(accessURL))
                                        .Replace("@Password", passwordText)
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(accessURL));
                                    
                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@Password", passwordText)
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                    ;

                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        private string SendNotificationEmailChanged(User user)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string accessURL = string.Empty;
                URL_Customization Urloutput = CustomerManagement.GetURLCustomization(Convert.ToInt32(AuthenticationHelper.CustomerID));
                if (Urloutput != null)
                {
                    accessURL = Urloutput.URL;
                }
                else
                {
                    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                }
                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(accessURL))
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(accessURL));
                //string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                //                        .Replace("@Username", user.Email)
                //                        .Replace("@User", username)
                //                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                //                        .Replace("@From", ReplyEmailAddressName)
                //                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        public event EventHandler OnSaved;

    }
}