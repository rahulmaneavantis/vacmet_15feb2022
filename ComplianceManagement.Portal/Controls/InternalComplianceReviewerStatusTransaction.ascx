﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InternalComplianceReviewerStatusTransaction.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.InternalComplianceReviewerStatusTransaction" %>

<script type="text/javascript">

    function fopendocfile3() {
        $('#DocumentPopUpInternalReviewer').modal();
        $('#ContentPlaceHolder1_udcStatusTranscationInternal_docViewerAll3').attr('src', "../docviewer.aspx?docurl=" + $("#<%= lblpathsample3.ClientID %>").text());
    }

    function fopendocInternalfileReview(file) {
        debugger;
        $('#DocumentReviewPopUp').modal('show'); enableControls();
        $('#ContentPlaceHolder1_udcStatusTranscationInternal_docViewerReviewInternal').attr('src', "../docviewer.aspx?docurl=" + file);
    }

    function fopendocInternalfileReviewPopUp(file) {
        enableControls();
        $('#DocumentReviewPopUp').modal('show');
    }

    function fopendoctaskfileReviewInternal1(file) {
        $('#modalDocumentReviewerViewerInternal').modal('show');
        $('#ContentPlaceHolder1_udcStatusTranscationInternal_docTaskViewerReviewAllInternal').attr('src', "../docviewer.aspx?docurl=" + file);
    }
    $(document).ready(function () {

        $("button[data-dismiss-modal=modal2]").click(function () {
            $('#DocumentPopUpInternalReviewer').modal('hide');
            $('#DocumentReviewPopUp').modal('hide');
            $('#modalDocumentReviewerViewerInternal').modal('hide');
        });

    });

    function initializeDatePickerInternal(date) {
        var startDate = new Date();
        $('#<%= tbxDate3.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: startDate,
            numberOfMonths: 1,
        });

        if (date != null) {
            $("#<%= tbxDate3.ClientID %>").datepicker("option", "defaultDate", date);
        }
    }

    function initializeDatePickerOverDueReviewInternal(dateOverDue) {
        $('#<%= tbxDate3.ClientID %>').datepicker('destroy');
        $('#<%= tbxDate3.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            minDate: dateOverDue,
            numberOfMonths: 1,
        });

       <%-- if (dateOverDue != null) {
            $("#<%= tbxDate3.ClientID %>").datepicker("option", "defaultDate", dateOverDue);
       }--%>
   }

   function initializeDatePickerInTimeReviewInternal(dateInTime) {
       $('#<%= tbxDate3.ClientID %>').datepicker('destroy');
        $('#<%= tbxDate3.ClientID %>').datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: dateInTime,
            numberOfMonths: 1,
        });

       <%-- if (dateInTime != null) {
            $("#<%= tbxDate3.ClientID %>").datepicker("option", "maxDate", dateInTime);
        }--%>
    }
    function enableControls() {
        debugger;
        $("#<%= btnSave3.ClientID %>").removeAttr("disabled");
        $("#<%= rdbtnStatus3.ClientID %>").removeAttr("disabled");
        $("#<%= tbxRemarks3.ClientID %>").removeAttr("disabled");
        $("#<%= tbxDate3.ClientID %>").removeAttr("disabled");
        $("#<%= btnReject3.ClientID %>").removeAttr("disabled");
    }
    function SelectheaderInternalDOCCheckboxes(headerchk) {
        $('#taskIntRevDocslower').hide();
        var spanparent = $(headerchk).parent('div');
        var subtaskDocumentlistobj = $(spanparent).find('.subtaskIntDocumentlist');
        var count = 0;
        $('#taskIntRevDocslower').html($(subtaskDocumentlistobj).html());
        $('#ConfirmationInternalDocumentModel').modal('show');
        $('#taskIntRevDocslower').show();
    }
    function downloadTaskSummary(obj) {
        var formonth = $(obj).attr('data-formonth');
        var instanceId = $(obj).attr('data-inid');
        var scheduleOnID = $(obj).attr('data-scid');
        $('#filedownload').attr("src", "/task/downloadtaskdoc.aspx?taskScheduleOnID=" + scheduleOnID + "&r=" + Math.random());

    }

    function RejectInternalRemark() {
        debugger;
        if (!$.trim($("#ContentPlaceHolder1_udcStatusTranscationInternal_tbxRemarks3").val())) {
            $("#ContentPlaceHolder1_udcStatusTranscationInternal_LabelmsgrevInternal").css('display', 'block');
            $('#ContentPlaceHolder1_udcStatusTranscationInternal_LabelmsgrevInternal').text("Please enter remark");
            $('#ContentPlaceHolder1_udcStatusTranscationInternal_ValidationSummary1').IsValid = false;
            document.getElementById("ContentPlaceHolder1_udcStatusTranscationInternal_ValidationSummary1").value = "Please enter remark";
            return false;
        }

    }

   function ValidationInternal() {
        debugger;
        var RB1 = document.getElementById("<%=rdbtnStatus3.ClientID%>");
        var radio = RB1.getElementsByTagName("input");
        var isChecked = false;
        for (var i = 0; i < radio.length; i++) {
            if (radio[i].checked) {
                isChecked = true;
                break;
            }
        }
        if (isChecked == false) {
            $("#ContentPlaceHolder1_udcStatusTranscationInternal_LabelmsgrevInternal").css('display', 'block');
            $('#ContentPlaceHolder1_udcStatusTranscationInternal_LabelmsgrevInternal').text("Please select Status.");
            $('#ContentPlaceHolder1_udcStatusTranscationInternal_ValidationSummary1').IsValid = false;
            document.getElementById("ContentPlaceHolder1_udcStatusTranscationInternal_ValidationSummary1").value = "Please select Status.";
            return false;
        }

        if (document.getElementById('ContentPlaceHolder1_udcStatusTranscationInternal_rdbtnStatus3_0') != null) {
            if (document.getElementById('ContentPlaceHolder1_udcStatusTranscationInternal_rdbtnStatus3_0').checked) {
                {
                    var checkedRadio = $("#ContentPlaceHolder1_udcStatusTranscationInternal_rdbtnStatus3 input[type=radio]:checked");
                    if (checkedRadio.length > 0) {
                        var selectedText = checkedRadio.next().html();
                        if (selectedText == "Closed-Delayed") {
                            $('#remarkcompulsory1').css('display', 'block');
                            if (!$.trim($("#ContentPlaceHolder1_udcStatusTranscationInternal_tbxRemarks3").val())) {
                                $("#ContentPlaceHolder1_udcStatusTranscationInternal_LabelmsgrevInternal").css('display', 'block');
                                $('#ContentPlaceHolder1_udcStatusTranscationInternal_LabelmsgrevInternal').text("Please enter remark");
                                $('#ContentPlaceHolder1_udcStatusTranscationInternal_ValidationSummary1').IsValid = false;
                                document.getElementById("ContentPlaceHolder1_udcStatusTranscationInternal_ValidationSummary1").value = "Please enter remark";
                                return false;
                            }
                        }
                    }
                }
            }
        }
   }

    function MendatoryRemark() {
        $('#remarkcompulsory1').css('display', 'none');
         if (document.getElementById('ContentPlaceHolder1_udcStatusTranscationInternal_rdbtnStatus3_0') != null) {
             if (document.getElementById('ContentPlaceHolder1_udcStatusTranscationInternal_rdbtnStatus3_0').checked) {
                 var checkedRadio = $("#ContentPlaceHolder1_udcStatusTranscationInternal_rdbtnStatus3 input[type=radio]:checked");
                      if (checkedRadio.length > 0) {
                        var selectedText = checkedRadio.next().html();
                        if (selectedText == "Closed-Delayed") {
                            $('#remarkcompulsory1').css('display', 'block');
                        }
                        else {
                            $('#remarkcompulsory1').css('display', 'none');
                        }
                    }
                }
         }
        else {
            $('#remarkcompulsory1').css('display', 'none');
        }
    }
    function CloseInternalReviewerPopPup() {
        window.parent.CloseModalInternalReviewer();        
    }
</script>
<style>
    tr.spaceUnder > td {
        padding-bottom: 1em;
    }
</style>

<div id="divReviewerInternalComplianceDetailsDialog">
    <iframe id="filedownload" style="display: none;" src="about:blank"></iframe>
    <asp:UpdatePanel ID="upComplianceDetails3" runat="server" UpdateMode="Conditional"
        OnLoad="upComplianceDetails3_Load">
        <ContentTemplate>
            <div style="margin: 5px">
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="ReviewerComplianceValidationGroup2" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" class="alert alert-block alert-danger fade in" EnableClientScript="False"
                        ValidationGroup="ReviewerComplianceValidationGroup2" Display="None" />
                    <asp:Label ID="LabelmsgrevInternal" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
                    <asp:HiddenField runat="server" ID="hdnComplianceInstanceID" />
                    <asp:HiddenField runat="server" ID="hdnComplianceScheduledOnId" />
                </div>

                <div class="clearfix" style="margin-bottom: 10px"></div>
                <div>
                    <asp:Label ID="Label3" Text="This is a  " Style="width: 300px; font-size: 13px; color: #333;"
                        maximunsize="300px" autosize="true" runat="server" />
                    <div id="divRiskType3" runat="server" class="circle"></div>
                    <asp:Label ID="lblRiskType3" Style="width: 300px; margin-left: -17px; font-size: 13px; color: #333;"
                        maximunsize="300px" autosize="true" runat="server" />
                </div>

                <div id="ComplianceDetails3" class="row Dashboard-white-widget">
                    <div class="dashboard">
                        <div class="col-lg-12 col-md-12">
                            <div class="panel panel-default">

                                <div class="panel panel-default" style="margin-bottom: 1px;">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails3">
                                            <h2>Compliance Details</h2>
                                        </a>
                                        <div class="panel-actions">
                                            <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseComplianceDetails3"><i class="fa fa-chevron-up"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == 63)
                                    { %>
                                <div id="collapseComplianceDetails3" class="in">
                                    <%}
                                        else
                                        { %>
                                    <div id="collapseComplianceDetails3" class="collapse">
                                        <%} %>

                                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                            <table style="width: 100%;">
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Compliance ID</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblComplianceID3" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Short Description</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblComplianceDiscription3" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr class="spaceUnder">
                                                    <td style="width: 15%; font-weight: bold;">Frequency</td>
                                                    <td style="width: 2%; font-weight: bold;">: </td>
                                                    <td style="width: 83%;">
                                                        <asp:Label ID="lblFrequency3" Style="width: 300px; font-size: 13px; color: #333;"
                                                            maximunsize="300px" autosize="true" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="OthersDetails3" class="row Dashboard-white-widget">
                        <div class="dashboard">
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel panel-default" style="margin-bottom: 1px;">
                                        <div class="panel-heading">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails3">
                                                <h2>Additional Details</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#collapseOthersDetails3"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID == 63)
                                        { %>
                                    <div id="collapseOthersDetails3" class="in">
                                        <%}
                                            else
                                            { %>
                                        <div id="collapseOthersDetails3" class="collapse">
                                            <%} %>


                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                    <table style="width: 100%">
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Risk Type</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:Label ID="lblRisk3" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 15%; font-weight: bold;">Sample Form/Attachment</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:UpdatePanel ID="upsample1" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Label ID="lblFormNumber3" Style="width: 300px; font-size: 13px; color: #333;"
                                                                            maximunsize="300px" autosize="true" runat="server" />
                                                                        <asp:LinkButton ID="lbDownloadSample3" Style="width: 300px; font-size: 13px; color: blue"
                                                                            runat="server" Font-Underline="false" OnClick="lbDownloadSample3_Click" />
                                                                        <asp:Label ID="lblSlash1" Text="/" Style="color: blue;" runat="server" />
                                                                        <asp:LinkButton ID="lnkViewSampleForm3" Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                            runat="server" Font-Underline="false" OnClientClick="fopendocfile3();" />
                                                                        <asp:Label ID="lblpathsample3" runat="server" Style="display: none"></asp:Label>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Location</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblLocation" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Period</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblPeriod" Style="width: 300px; font-size: 13px; color: #333; display: none;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                                <asp:Label ID="lblPeriod1" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder">
                                                            <td style="width: 25%; font-weight: bold;">Due Date</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblDueDate" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                                <asp:HiddenField ID="hiddenDueDateReviewInternal" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr class="spaceUnder" id="trAuditChecklist4" runat="server">
                                                            <td style="width: 25%; font-weight: bold;">Audit Checklist</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 73%;">
                                                                <asp:Label ID="lblAuditChecklist4" Style="width: 300px; font-size: 13px; color: #333;"
                                                                    maximunsize="300px" autosize="true" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div runat="server" id="divTask" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTaskSubTask">
                                                <h2>Main Task Details</h2>
                                            </a>
                                            <div class="panel-actions">
                                                <a class="btn-minimize" onclick="btnminimize(this)"><i class="fa fa-chevron-up"></i></a>
                                            </div>
                                        </div>

                                        <div id="collapseTaskSubTask" class="collapse in">
                                            <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                <asp:GridView runat="server" ID="gridSubTask" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                                                    AllowPaging="false" PageSize="50" CssClass="table" GridLines="None" BorderWidth="0px" DataKeyNames="TaskID"
                                                    OnRowCommand="gridSubTask_RowCommand" OnRowDataBound="gridSubTask_RowDataBound" AutoPostBack="true">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                                                            <ItemTemplate>
                                                                <%#Container.DataItemIndex+1 %>
                                                                <asp:Label ID="lblTaskScheduledOnID" runat="server" Visible="false" Text='<%# Eval("TaskScheduledOnID") %>'></asp:Label>
                                                                <asp:Label ID="lblIsTaskClose" Visible="false" runat="server" Text='<%# Eval("IsTaskClose") %>'></asp:Label>
                                                                <asp:Label ID="lblTaskInstanceID" runat="server" Visible="false" Text='<%# Eval("TaskInstanceID") %>'></asp:Label>
                                                                <asp:Label ID="lblMainTaskID" runat="server" Visible="false" Text='<%# Eval("MainTaskID") %>'></asp:Label>
                                                                <asp:Label ID="lblForMonth" runat="server" Visible="false" Text='<%# Eval("ForMonth") %>'></asp:Label>
                                                                <asp:Label ID="lblComplianceScheduleOnID" runat="server" Visible="false" Text='<%# Eval("ComplianceScheduleOnID") %>'></asp:Label>
                                                                <asp:Label ID="lblTaskID" runat="server" Visible="false" Text='<%# Eval("TaskID") %>'></asp:Label>
                                                                <asp:Label ID="lblCbranchId" runat="server" Visible="false" Text='<%# Eval("CustomerBranchID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Task">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblTaskTitle" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        Text='<%# Eval("TaskTitle") %>' ToolTip='<%# Eval("TaskTitle") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Performer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblPerformer" runat="server" data-toggle="tooltip" data-placement="bottom"
                                                                        ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'
                                                                        Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),3,(byte)Eval("TaskType")) %>'></asp:Label>

                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Reviewer">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 70px;">
                                                                    <asp:Label ID="lblReviewer" runat="server" data-toggle="tooltip" data-placement="bottom" ToolTip='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'
                                                                        Text='<%# GetUserName((long)Eval("TaskInstanceID"),(long)Eval("TaskScheduledOnID"),4,(byte)Eval("TaskType")) %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Due Date">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                                    <asp:Label ID="lblScheduledOn" runat="server" data-toggle="tooltip" data-placement="bottom" Text=' <%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 50px;">
                                                                    <asp:Label ID="lblStatus" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Status") %>' ToolTip='<%# Eval("Status") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Action">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="upSubTaskDownloadView" runat="server">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton ID="btnSubTaskDocDownload" runat="server" CommandName="Download" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="Download Documents" Text="Download" Style="color: blue;">
                                                                        </asp:LinkButton>
                                                                        <asp:Label ID="lblTaskSlashReview" Text="/" Style="color: blue;" runat="server" />
                                                                        <asp:LinkButton CommandName="View" runat="server" ID="btnSubTaskDocView" CommandArgument='<%# Eval("TaskScheduledOnID") %>'
                                                                            data-toggle="tooltip" data-placement="bottom" ToolTip="View Documents"
                                                                            Text="View" Style="width: 150px; font-size: 13px; color: blue" />
                                                                        <asp:Label ID="CompDocReviewPath" runat="server" Style="display: none"></asp:Label>

                                                                        <asp:Image ID="chkIntDocument" ImageUrl="../Images/View-icon-new.png" data-toggle="tooltip" CssClass="DocumentIntsbtask" ToolTip="Click to download subtasks documents" runat="server" OnClick="javascript:SelectheaderInternalDOCCheckboxes(this)" />
                                                                        <asp:Label ID="lblsubtaskIntDocuments" CssClass="subtaskIntDocumentlist" runat="server" Style="display: none;"></asp:Label>

                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnSubTaskDocDownload" />
                                                                        <%-- <asp:PostBackTrigger ControlID="btnSubTaskDocView" />--%>
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="Step1Download3" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Step1DownloadReview3">
                                                    <h2>Review Compliance Document</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#Step1DownloadReview3"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="Step1DownloadReview3" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;" id="fieldsetdownloadreview" runat="server">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 colpadding0 fixwidth" style="width: 100%;">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 15%; font-weight: bold;">Versions</td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <table width="100%" style="text-align: left">
                                                                    <thead>
                                                                        <tr>
                                                                            <td valign="top">
                                                                                <asp:Repeater ID="rptComplianceVersion3" runat="server" OnItemCommand="rptComplianceVersion3_ItemCommand"
                                                                                    OnItemDataBound="rptComplianceVersion3_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <table id="tblComplianceDocumnets">
                                                                                            <thead>
                                                                                                <%-- <th>Versions</th>--%>
                                                                                            </thead>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                                                            <ContentTemplate>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:LinkButton CommandName="version" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' ID="lblDocumentVersion1"
                                                                                                            runat="server" Text='<%# Eval("Version")%>' Style="color: blue;"></asp:LinkButton></td>
                                                                                                    <td>
                                                                                                        <asp:LinkButton CommandName="Download" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") %>' OnClientClick='javascript:enableControls()'
                                                                                                            ID="btnComplinceVersionDoc1" runat="server" Text="Download" Style="color: blue;">
                                                                                                        </asp:LinkButton>
                                                                                                        <asp:Label ID="lblSlashInternalReview" Text="/" Style="color: blue;" runat="server" />
                                                                                                        <asp:LinkButton CommandName="View" ID="lnkViewDocInternal" CommandArgument='<%# Eval("ScheduledOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' Text="View" Style="width: 150px; font-size: 13px; color: blue"
                                                                                                            runat="server" Font-Underline="false" OnClientClick="fopendocInternalfileReview();" />
                                                                                                        <asp:Label ID="lblpathReviewInternalDoc" runat="server" Style="display: none"></asp:Label>
                                                                                                    </td>

                                                                                                </tr>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:PostBackTrigger ControlID="btnComplinceVersionDoc1" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>

                                                                            </td>
                                                                            <td valign="top">
                                                                                <asp:Repeater ID="rptComplianceDocumnets3" runat="server" OnItemCommand="rptComplianceDocumnets3_ItemCommand"
                                                                                    OnItemDataBound="rptComplianceDocumnets3_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <table id="tblComplianceDocumnets">
                                                                                            <thead>
                                                                                                <th>Compliance Related Documents</th>
                                                                                            </thead>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:LinkButton
                                                                                                    CommandArgument='<%# Eval("FileID")%>'
                                                                                                    OnClientClick='javascript:enableControls()'
                                                                                                    ID="btnComplianceDocumnets" runat="server" Text='<%# Eval("FileName") %>'>
                                                                                                </asp:LinkButton></td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>
                                                                                <asp:Repeater ID="rptWorkingFiles" runat="server" OnItemCommand="rptWorkingFiles_ItemCommand"
                                                                                    OnItemDataBound="rptWorkingFiles_ItemDataBound">
                                                                                    <HeaderTemplate>
                                                                                        <table id="tblWorkingFiles">
                                                                                            <thead>
                                                                                                <th>Compliance Working Files</th>
                                                                                            </thead>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:LinkButton
                                                                                                    CommandArgument='<%# Eval("FileID")%>'
                                                                                                    ID="btnWorkingFiles" runat="server" Text='<%# Eval("FileName")%>'>
                                                                                                </asp:LinkButton></td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        </table>
                                                                                    </FooterTemplate>
                                                                                </asp:Repeater>
                                                                            </td>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="Step1UpdateCompliance3" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#Step1UpdateComplianceStatus3">
                                                    <h2>Update Compliance Status </h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#Step1UpdateComplianceStatus3"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="Step1UpdateComplianceStatus3" class="panel-collapse collapse in">
                                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; width: 100%;">
                                                <div style="margin-bottom: 7px">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <label id="lblStatus3" runat="server" style="width: 200px; display: block; float: left; font-size: 13px; color: #333;" />
                                                            <td style="width: 25%;">
                                                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Status</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:UpdatePanel ID="upsample" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:RadioButtonList ID="rdbtnStatus3" AutoPostBack="true"  onchange="MendatoryRemark()" OnSelectedIndexChanged="rdbtnStatus3_SelectedIndexChanged" runat="server" RepeatDirection="Horizontal">
                                                                        </asp:RadioButtonList>
                                                                        <asp:RequiredFieldValidator ID="rfvRdoStatusButton" Style="display: none" runat="server" ValidationGroup="ReviewerComplianceValidationGroup2" ControlToValidate="rdbtnStatus3"
                                                                            ErrorMessage="Please select Status."> </asp:RequiredFieldValidator>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <div style="margin-bottom: 7px" id="divDated3" runat="server">
                                                                <td style="width: 25%;">
                                                                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp</label>
                                                                    <label style="font-weight: bold; vertical-align: text-top;">Date</label>
                                                                </td>
                                                                <td style="width: 2%; font-weight: bold;">: </td>
                                                                <td style="width: 83%;">
                                                                    <asp:TextBox runat="server" ID="tbxDate3" placeholder="DD-MM-YYYY" ReadOnly="true" class="form-control" Style="width: 115px; cursor: text;" />
                                                                    <asp:RequiredFieldValidator ErrorMessage="Please select Date." ControlToValidate="tbxDate3"
                                                                        runat="server" ID="RequiredFieldValidator1" ValidationGroup="ReviewerComplianceValidationGroup2" Display="None" />
                                                                </td>
                                                            </div>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <label id="remarkcompulsory1" style="width: 10px; display: none; float: left; font-size: 13px; color: red;">*</label>
                                                                <label style="font-weight: bold; vertical-align: text-top;">Remarks</label>
                                                            </td>
                                                            <td style="width: 2%; font-weight: bold;">: </td>
                                                            <td style="width: 83%;">
                                                                <asp:TextBox runat="server" ID="tbxRemarks3" ErrorMessage="Compulsory" TextMode="MultiLine" class="form-control" Rows="2" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div style="margin-bottom: 7px; margin-left: 34%; margin-top: 10px;">
                                                        <asp:Button Text="Approve" runat="server" ID="btnSave3" OnClientClick="javascript:return ValidationInternal()" OnClick="btnSave3_Click" CssClass="btn btn-search"
                                                            ValidationGroup="ReviewerComplianceValidationGroup2" />
                                                        <asp:Button Text="Reject" runat="server" ID="btnReject3" OnClientClick="javascript:return RejectInternalRemark()" OnClick="btnReject3_Click" CssClass="btn btn-search" Style="margin-left: 17px"
                                                            ValidationGroup="ReviewerComplianceValidationGroup2" CausesValidation="false" />
                                                        <asp:Button Text="Close" Style="margin-left: 15px;" runat="server" ID="btnCancel" CssClass="btn btn-search" data-dismiss="modal" />
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="Log1" class="row Dashboard-white-widget">
                            <div class="dashboard">
                                <div class="col-lg-12 col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel panel-default" style="margin-bottom: 1px;">
                                            <div class="panel-heading">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#AuditLog3">
                                                    <h2>Audit Log</h2>
                                                </a>
                                                <div class="panel-actions">
                                                    <a class="btn-minimize" onclick="btnminimize(this)" data-toggle="collapse" data-parent="#accordion" href="#AuditLog3"><i class="fa fa-chevron-up"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="AuditLog3" class="collapse">
                                            <div runat="server" id="log" style="text-align: left;">
                                                <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; margin-top: 5px;">
                                                    <div style="margin-bottom: 7px; clear: both; margin-top: 10px">
                                                        <asp:GridView runat="server" ID="grdTransactionHistory3" AutoGenerateColumns="false" AllowSorting="true"
                                                            AllowPaging="true" PageSize="5" GridLines="Horizontal" OnPageIndexChanging="grdTransactionHistory3_OnPageIndexChanging"
                                                            OnRowCreated="grdTransactionHistory3_RowCreated" CssClass="table" BorderWidth="0px" OnSorting="grdTransactionHistory3_Sorting"
                                                            DataKeyNames="ComplianceTransactionID" OnRowCommand="grdTransactionHistory3_RowCommand">
                                                            <Columns>
                                                                <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" />
                                                                <asp:TemplateField HeaderText="Date">
                                                                    <ItemTemplate>
                                                                        <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                                                <asp:BoundField DataField="Status" HeaderText="Status" />
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Right" />
                                                            <PagerTemplate>
                                                                <table style="display: none">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </div>
                                                </fieldset>
                                                <asp:Label ID="lblNote" runat="server" Text="*Please download the attached document to verify and then changed the status." Style="color: #999; font-size: 12px;" Visible="false"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:HiddenField runat="server" ID="hdlSelectedDocumentID" />
                        <asp:Button ID="btnDownload3" runat="server" Style="display: none" OnClick="btnDownload3_Click" />
                    </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDownload3" />
            <asp:PostBackTrigger ControlID="btnSave3" />
            <asp:PostBackTrigger ControlID="lbDownloadSample3" />
        </Triggers>
    </asp:UpdatePanel>
</div>

<div class="modal fade" id="DocumentReviewPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
    <div class="modal-dialog" style="width: 100%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="height: 570px;">
                <div style="width: 100%;">
                    <div style="float: left; width: 10%">
                        <table width="100%" style="text-align: left; margin-left: 5%;">
                            <thead>
                                <tr>
                                    <td valign="top">

                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Repeater ID="rptIComplianceVersionView" runat="server" OnItemCommand="rptIComplianceVersionView_ItemCommand"
                                                    OnItemDataBound="rptIComplianceVersionView_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="tblComplianceDocumnets">
                                                            <thead>
                                                                <th>Versions</th>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("InternalComplianceScheduledOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblDocumentVersionView"
                                                                    runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="rptIComplianceVersionView" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div style="float: left; width: 90%">
                        <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdatleMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="lblMessage" runat="server" Style="color: red;"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                            <iframe src="about:blank" id="docViewerReviewInternal" runat="server" width="100%" height="535px"></iframe>
                        </fieldset>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <%--<div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="docViewerReviewInternal" runat="server" width="100%" Height="550px"  ></iframe>
                </div>
            </div>
        </div>--%>
</div>


<div class="modal fade" id="ConfirmationInternalDocumentModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 85%;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header">
                <div style="width: 10%; float: right">
                    <button type="button" class="close" onclick="$('#ConfirmationInternalDocumentModel').modal('hide');" aria-hidden="true">×</button>
                </div>
                <div style="width: 90%; align-content: center; margin-left: 39%; float: left;">
                    <p style="font-size: 20px">Document(s)</p>
                </div>
            </div>
            <div class="modal-body">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div>
                            <div style="width: 100%; margin-left: 3%;">
                                <div id="taskIntRevDocslower" style="padding: 4px; width: 98%; overflow: auto; color: #333; padding-left: 0px;">
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <%--<asp:PostBackTrigger ControlID="lblNo" />--%>
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</div>

<div>
    <div class="modal fade" id="modalDocumentReviewerViewerInternal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
        <div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="height: 570px;">
                    <div style="width: 100%;">
                        <div style="float: left; width: 10%">
                            <table width="100%" style="text-align: left; margin-left: 5%;">
                                <thead>
                                    <tr>
                                        <td valign="top">
                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rptTaskVersionViewInternal1" runat="server" OnItemCommand="rptTaskVersionViewInternal1_ItemCommand">
                                                        <HeaderTemplate>
                                                            <table id="tblComplianceDocumnets">
                                                                <thead>
                                                                    <th>Versions</th>
                                                                </thead>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("TaskScheduleOnID") + ","+ Eval("Version") + ","+ Eval("FileID") %>' ID="lblTaskDocumentVersionViewInternal"
                                                                                runat="server" ToolTip='<%# Eval("FileName")%>' Text='<%# Eval("Version") +" "+ Eval("FileName").ToString().Substring(0,4) %>'></asp:LinkButton>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="lblTaskDocumentVersionViewInternal" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="rptTaskVersionViewInternal1" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div style="float: left; width: 90%">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdatleMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="lblMessagetask" runat="server" Style="color: red;"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <fieldset style="border-style: solid; border-width: 1px; border-color: #dddddd; height: 550px; width: 100%;">
                                <iframe src="about:blank" id="docTaskViewerReviewAllInternal" runat="server" width="100%" height="535px"></iframe>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div>
    <div class="modal fade" id="DocumentPopUpInternalReviewer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow-y: hidden; top: 5%;">
        <div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss-modal="modal2" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" style="height: 570px;">
                    <div style="float: left; width: 10%">
                        <table width="100%" style="text-align: left; margin-left: 5%;">
                            <thead>
                                <tr>
                                    <td valign="top">
                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdatleMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Repeater ID="rptComplianceSampleView1" runat="server" OnItemCommand="rptComplianceSampleView1_ItemCommand"
                                                    OnItemDataBound="rptComplianceSampleView1_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table id="tblComplianceDocumnets">
                                                            <thead>
                                                                <th>Sample Forms</th>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdatleMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:LinkButton CommandName="View" CommandArgument='<%# Eval("ID") + ","+ Eval("ComplianceID") %>' ID="lblSampleView1"
                                                                            runat="server" ToolTip='<%# Eval("Name")%>' Text='<%# Eval("Name") %>'></asp:LinkButton>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="lblSampleView1" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="rptComplianceSampleView1" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div style="float: left; width: 90%">
                        <iframe src="about:blank" id="docViewerAll3" runat="server" width="100%" height="550px"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
