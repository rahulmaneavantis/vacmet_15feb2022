﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.License;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class frmUpcomingCompliancess : System.Web.UI.Page
    {
        public string Role;      
        protected static List<Int32> roles;
        protected static string ClickChangeflag = "P";
        protected static string queryStringFlag= "";
        protected string Reviewername;
        protected string Performername;
        protected string InternalReviewername;
        protected string InternalPerformername;
        protected int CustomerID = 0;
        static int UserRoleID;
        bool IsPenaltyVisible = true;
        List<sp_ComplianceInstanceTransaction_Result> spcompliancetransaction = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    UserRoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);

                    //BindLocation();

                    string type = Request.QueryString["type"];
                    if (type == "Internal")
                    {
                        ddlComplianceType.SelectedValue = "0";

                        if (AuthenticationHelper.CustomerID == 63)
                        {
                            PanelAct.Enabled = true;
                        }
                        else
                        {
                             PanelAct.Enabled = false;

                        }

                        Panelsubtype.Enabled = false;
                    }
                    else if (type == "Statutory")
                    {
                        ddlComplianceType.SelectedValue = "-1";
                        PanelAct.Enabled = true;
                        Panelsubtype.Enabled = true;
                    }
                    else if (type == "assigned" || type == "active")
                    {
                        ddlComplianceType.SelectedValue = "1";
                        PanelAct.Enabled = true;
                        Panelsubtype.Enabled = true;
                    }
                    else
                    {
                        ddlComplianceType.SelectedValue = "-1";
                        PanelAct.Enabled = true;
                        Panelsubtype.Enabled = true;
                    }
                    if(string.IsNullOrEmpty(Request.QueryString["type"]))
                    {
                        if (AuthenticationHelper.CustomerID == 63)
                        {
                            ddlComplianceType.SelectedValue = "0";
                            if (AuthenticationHelper.CustomerID == 63)
                            {
                                PanelAct.Enabled = true;
                            }
                            else
                            {
                                PanelAct.Enabled = false;
                            }
                            Panelsubtype.Enabled = false;

                        
                        }
                    }
                    BindLocationFilter();
                   
                    if (AuthenticationHelper.CustomerID == 63)
                    {
                        ddlComplianceType.Items.Remove(ddlComplianceType.Items.FindByValue("-1"));
                        ddlComplianceType.Items.Remove(ddlComplianceType.Items.FindByValue("1"));
                        BindTypesInternal();
                        BindCategoriesInternal();
                        BindActListInternal();
                    }
                    else
                    {
                        BindTypes();
                        BindCategories();
                        BindActList();
                    }
                    BindComplianceSubTypeList();
                    btnPenaltyReviewer.Visible = false;
                    btnPenalty.Visible = false;
                    btnReassignPerformer.Visible = false;
                    btnReviseCompliance.Visible = false;
                    btnReviseComplianceInternal.Visible = false;
                    btnTaskDetails.Visible = false;
                    //if (!string.IsNullOrEmpty(Request.QueryString["ScheduledOn"])) { 
                    //    QueryStringScheduledOn= Request.QueryString["ScheduledOn"];
                    //}


                    string filter = Request.QueryString["filter"];
                    string role = Request.QueryString["role"];
                    //roles = CustomerBranchManagement.GetDashboardAssignedRoleid(AuthenticationHelper.UserID);
                    //   roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                    if (Session["User_comp_Roles"] != null)
                    {
                        roles = Session["User_comp_Roles"] as List<int>;
                    }
                    else
                    {
                        roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                        Session["User_comp_Roles"] = roles;
                    }
                    if (roles.Contains(10))
                    { 
                        BindEvents();
                    }

                  
                    queryStringFlag = "A";

                    var lcnt = LicenseMgmt.GetAssignedroleid(AuthenticationHelper.UserID);
                    if (lcnt.Count > 0)
                    {
                        bntLicneseWorkspace.Visible = true;
                    }
                    else
                    {
                        bntLicneseWorkspace.Visible = false;
                    }
                    // Penalty visible for customer
                    string customer = ConfigurationManager.AppSettings["PenaltynotDisplayCustID"].ToString();
                    List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                    if (PenaltyNotDisplayCustomerList.Count > 0)
                    {

                        foreach (string PList in PenaltyNotDisplayCustomerList)
                        {
                            if (PList == CustomerID.ToString())
                            {
                                IsPenaltyVisible = false;
                                break;
                            }
                        }
                    }

                    if (role != null)
                    {
                        if (role == "Performer")
                        {
                            ClickChangeflag = "P";
                            BindStatusPerformer();

                            btnTaskDetails.Visible = true;
                            if (ddlComplianceType.SelectedValue == "0")
                            {
                                btnReviseComplianceInternal.Visible = true;
                                btnReviseCompliance.Visible = false;
                            }
                            else
                            {
                                btnReviseCompliance.Visible = true;
                                btnReviseComplianceInternal.Visible = false;
                            }
                            if (IsPenaltyVisible == true)
                            {
                                btnPenalty.Visible = true;
                                btnPenaltyReviewer.Visible = false;
                            }
                            else
                            {
                                btnPenalty.Visible = false;
                                btnPenaltyReviewer.Visible = false;
                            }
                            PanelPerformerList.Visible = false;
                        }
                        else
                        {
                            btnTaskDetails.Visible = false;
                            ClickChangeflag = "R";
                            BindStatusReviewer();
                            btnReassignPerformer.Visible = true;
                            if (IsPenaltyVisible == true)
                            {
                                btnPenalty.Visible = false;
                                btnPenaltyReviewer.Visible = true;
                            }
                            else
                            {
                                btnPenalty.Visible = false;
                                btnPenaltyReviewer.Visible = false;
                            }
                            PanelPerformerList.Visible = true;
                        }
                        if (type != null && filter != "" && filter != null)
                        {
                            try
                            {
                                ddlComplianceType.ClearSelection();
                                ddlComplianceType.Items.FindByText(type).Selected = true;
                                ddlStatus.ClearSelection();
                                ddlStatus.Items.FindByText(filter).Selected = true;
                                Session["filter"] = filter;
                                if (ddlComplianceType.SelectedValue == "0")
                                {
                                    btnReviseCompliance.Visible = false;
                                    //btnReviseComplianceInternal.Visible = false;
                                    btnReviseComplianceInternal.Visible = true; //15 March 2019
                                }
                                else if (ddlComplianceType.SelectedValue == "-1" && roles.Contains(3))
                                {
                                    btnReviseCompliance.Visible = true;
                                }
                                else
                                {
                                    btnReviseCompliance.Visible = false;
                                    btnReviseComplianceInternal.Visible = false;
                                }
                            }
                            catch (Exception ex)
                            {

                                throw ex;
                            }

                        }
                    }
                    else
                    {
                        queryStringFlag = "";
                        if (roles.Contains(3) && roles.Contains(4))
                        {
                            btnTaskDetails.Visible = true;
                            ClickChangeflag = "P";
                            BindStatusPerformer();
                            if (ddlComplianceType.SelectedValue == "0")
                            {
                                btnReviseComplianceInternal.Visible = true;
                                btnReviseCompliance.Visible = false;
                            }
                            else
                            {
                                btnReviseCompliance.Visible = true;
                                btnReviseComplianceInternal.Visible = false;
                            }
                            if (IsPenaltyVisible == true)
                            {
                                btnPenalty.Visible = true;
                                btnPenaltyReviewer.Visible = false;
                            }
                            else
                            {
                                btnPenalty.Visible = false;
                                btnPenaltyReviewer.Visible = false;
                            }
                        }
                        else if (roles.Contains(3))
                        {
                            btnTaskDetails.Visible = true;
                            ClickChangeflag = "P";
                            BindStatusPerformer();
                            if (ddlComplianceType.SelectedValue == "0")
                            {
                                btnReviseComplianceInternal.Visible = true;
                                btnReviseCompliance.Visible = false;
                            }
                            else
                            {
                                btnReviseCompliance.Visible = true;
                                btnReviseComplianceInternal.Visible = false;
                            }
                            if (IsPenaltyVisible == true)
                            {
                                btnPenalty.Visible = true;
                                btnPenaltyReviewer.Visible = false;
                            }
                            else
                            {
                                btnPenalty.Visible = false;
                                btnPenaltyReviewer.Visible = false;
                            }
                            PanelPerformerList.Visible = false;
                        }
                        else if (roles.Contains(4))
                        {
                            btnTaskDetails.Visible = false;
                            ClickChangeflag = "R";
                            BindStatusReviewer();
                            btnReassignPerformer.Visible = true;
                            if (IsPenaltyVisible == true)
                            {
                                btnPenalty.Visible = false;
                                btnPenaltyReviewer.Visible = true;
                            }
                            else
                            {
                                btnPenalty.Visible = false;
                                btnPenaltyReviewer.Visible = false;
                            }
                            PanelPerformerList.Visible = true;
                        }
                    }

                    //if (!string.IsNullOrEmpty(Request.QueryString["ScheduledOn"]))
                    //{
                    //    BindComplianceInstanceGridDateWise(Request.QueryString["ScheduledOn"]);
                    //}
                    //else
                    //{
                        BindComplianceTransactions(ClickChangeflag, queryStringFlag);
                    //}
                    if (roles.Contains(4))
                    {
                        //spcompliancetransaction
                        if (spcompliancetransaction !=null)
                        {
                            BindPerformerddl(spcompliancetransaction);
                        }
                       
                    }

                    if (SelectedPageNo.Text == "")
                    {
                        SelectedPageNo.Text = "1";
                    }

                    if (Session["TotalComplianceRows"].ToString() != "")
                        TotalRows.Value = Session["TotalComplianceRows"].ToString();

                    GetPageDisplaySummary();

                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                }

                if (roles.Contains(3) && ClickChangeflag == "P")
                {
                   // udcStatusTranscatopn.OnSaved += (inputForm, args) => { BindComplianceTransactions("P", queryStringFlag); };
                    //udcInternalPerformerStatusTranscation.OnSaved += (inputForm, args) => { BindComplianceTransactions("P", queryStringFlag); };
                }
                else if (roles.Contains(4))
                {
                   // udcReviewerStatusTranscatopn.OnSaved += (inputForm, args) => { BindComplianceTransactions("R", queryStringFlag); };
                    //udcStatusTranscationInternal.OnSaved += (inputForm, args) => { BindComplianceTransactions("R", queryStringFlag); };
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkBtnBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                UserRoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);                
                string role = Request.QueryString["role"];
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                queryStringFlag = "A";
                if (role != null)
                {
                    if (role == "Performer")
                    {
                        ClickChangeflag = "P";
                    }
                    else
                    {
                        ClickChangeflag = "R";
                    }
                }
                else
                {
                    queryStringFlag = "";
                    if (roles.Contains(3) && roles.Contains(4))
                    {
                        btnTaskDetails.Visible = true;
                        ClickChangeflag = "P";                                              
                    }
                    else if (roles.Contains(3))
                    {                      
                        ClickChangeflag = "P";                                          
                    }
                    else if (roles.Contains(4))
                    {                      
                        ClickChangeflag = "R";                      
                    }
                }
                BindComplianceTransactions(ClickChangeflag, queryStringFlag);
                if (SelectedPageNo.Text == "")
                {
                    SelectedPageNo.Text = "1";
                }
                if (Session["TotalComplianceRows"].ToString() != "")
                    TotalRows.Value = Session["TotalComplianceRows"].ToString();
                GetPageDisplaySummary();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindPerformerddl()
        {
            try
            {
                if (CustomerID==0)
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                ddlperformer.DataSource = null;
                ddlperformer.DataBind();
                ddlperformer.DataTextField = "Name";
                ddlperformer.DataValueField = "ID";
                var TypeList = ActManagement.GetPerformerList(AuthenticationHelper.UserID, UserRoleID, CustomerID);
                if (TypeList != null)
                {
                    ddlperformer.DataSource = TypeList;
                    ddlperformer.DataBind();
                }

                ddlperformer.Items.Insert(0, new ListItem("Performer Name", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindPerformerddl(List<sp_ComplianceInstanceTransaction_Result> MasterList)
        {
            try
            {
                if (CustomerID == 0)
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                ddlperformer.DataSource = null;
                ddlperformer.DataBind();
                ddlperformer.DataTextField = "Name";
                ddlperformer.DataValueField = "ID";
                DateTime now = DateTime.UtcNow.Date;
                DateTime nextOneMonth = DateTime.UtcNow.AddMonths(1);

                var TypeList = (from row in MasterList
                                where row.RoleID == 3
                                && (row.IsActive != false || row.IsUpcomingNotDeleted != false) && row.ScheduledOn <= nextOneMonth
                                && (row.ComplianceStatusID == 1 || row.ComplianceStatusID == 2 || row.ComplianceStatusID == 10 || row.ComplianceStatusID == 3 || row.ComplianceStatusID == 6 || row.ComplianceStatusID == 11 || row.ComplianceStatusID == 12)
                                select new { ID = row.UserID, Name = row.User }).OrderBy(entry => entry.Name).Distinct().ToList<object>();

                if (TypeList != null)
                {
                    ddlperformer.DataSource = TypeList;
                    ddlperformer.DataBind();
                }

                ddlperformer.Items.Insert(0, new ListItem("Performer Name", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public void BindComplianceInstanceGridDateWise(string ScheduleOnDate)
        {
            if (CustomerID == 0)
            {
                CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            int UserID = AuthenticationHelper.UserID;
            DateTime ScheduleOnIDTemp;
            ScheduleOnIDTemp = Convert.ToDateTime(ScheduleOnDate);

            if (ddlComplianceType.SelectedValue == "0")
            {
                var GridAllData = DashboardManagement.BindGridInternalScheduleOnWise(ScheduleOnIDTemp, CustomerID, UserID);
                if (roles.Contains(3) && roles.Contains(4))
                {
                    var RolData = GridAllData.Where(entry => (entry.RoleID == 3)).ToList();
                    if (roles.Contains(3))
                    {
                        if (RolData.Count > 0)
                        {
                            grdInternalComplinacePerformer.DataSource = RolData;
                            Session["TotalComplianceRows"] = RolData.Count();
                            grdInternalComplinacePerformer.DataBind();
                            grdComplianceTransactions.Visible = false;
                            grdComplianceTransactionsRev.Visible = false;
                            grdInternalComplinacePerformer.Visible = true;
                            grdInternalComplianceReviewer.Visible = false;
                            liReviewer.Attributes.Add("class", "");
                            liPerformer.Attributes.Add("class", "active");

                            performerdocuments.Attributes.Remove("class");
                            performerdocuments.Attributes.Add("class", "tab-pane active");
                            reviewerdocuments.Attributes.Remove("class");
                            reviewerdocuments.Attributes.Add("class", "tab-pane");
                        }
                    }
                    var RolDataReviewer = GridAllData.Where(entry => (entry.RoleID == 4)).ToList();
                    if (roles.Contains(4))
                    {
                        if (RolDataReviewer.Count > 0)
                        {
                            grdInternalComplianceReviewer.DataSource = RolDataReviewer;
                            Session["TotalComplianceRows"] = RolDataReviewer.Count();
                            grdInternalComplianceReviewer.DataBind();
                            grdComplianceTransactions.Visible = false;
                            grdComplianceTransactionsRev.Visible = false;
                            grdInternalComplinacePerformer.Visible = false;
                            grdInternalComplianceReviewer.Visible = true;
                            liPerformer.Attributes.Add("class", "");
                            liReviewer.Attributes.Add("class", "active");

                            performerdocuments.Attributes.Remove("class");
                            performerdocuments.Attributes.Add("class", "tab-pane");
                            reviewerdocuments.Attributes.Remove("class");
                            reviewerdocuments.Attributes.Add("class", "tab-pane active");
                        }
                    }

                    if (RolData.Count > 0 && RolDataReviewer.Count > 0)
                    {
                        liReviewer.Attributes.Add("class", "");
                        liPerformer.Attributes.Add("class", "active");

                        performerdocuments.Attributes.Remove("class");
                        performerdocuments.Attributes.Add("class", "tab-pane active");
                        reviewerdocuments.Attributes.Remove("class");
                        reviewerdocuments.Attributes.Add("class", "tab-pane");
                        grdInternalComplinacePerformer.Visible = true;
                    }
                }

                else if (roles.Contains(3))
                {
                    grdInternalComplinacePerformer.DataSource = GridAllData;
                    Session["TotalComplianceRows"] = GridAllData.Count();
                    grdInternalComplinacePerformer.DataBind();
                    grdComplianceTransactions.Visible = false;
                    grdComplianceTransactionsRev.Visible = false;
                    grdInternalComplinacePerformer.Visible = true;
                    grdInternalComplianceReviewer.Visible = false;
                }
                else if (roles.Contains(4))
                {
                    grdInternalComplianceReviewer.DataSource = GridAllData;
                    Session["TotalComplianceRows"] = GridAllData.Count();
                    grdInternalComplianceReviewer.DataBind();
                    grdComplianceTransactions.Visible = false;
                    grdComplianceTransactionsRev.Visible = false;
                    grdInternalComplinacePerformer.Visible = false;
                    grdInternalComplianceReviewer.Visible = true;
                }
                else
                {
                    grdInternalComplinacePerformer.DataSource = GridAllData;
                    Session["TotalComplianceRows"] = GridAllData.Count();
                    grdInternalComplinacePerformer.DataBind();
                    grdComplianceTransactions.Visible = false;
                    grdComplianceTransactionsRev.Visible = false;
                    grdInternalComplinacePerformer.Visible = true;
                    grdInternalComplianceReviewer.Visible = false;
                }
            }
            else
            {
                var GridAllData = DashboardManagement.BindGridScheduleOnDateWise(ScheduleOnIDTemp, CustomerID, UserID);
                if (GridAllData.Count == 0)
                {
                    ddlComplianceType.SelectedValue = "0";

                    var GridAllData1 = DashboardManagement.BindGridInternalScheduleOnWise(ScheduleOnIDTemp, CustomerID, UserID);
                    if (roles.Contains(3) && roles.Contains(4))
                    {
                        var RolData = GridAllData1.Where(entry => (entry.RoleID == 3)).ToList();
                        if (roles.Contains(3))
                        {
                            if (RolData.Count > 0)
                            {
                                grdInternalComplinacePerformer.DataSource = RolData;
                                Session["TotalComplianceRows"] = RolData.Count();
                                grdInternalComplinacePerformer.DataBind();
                                grdComplianceTransactions.Visible = false;
                                grdComplianceTransactionsRev.Visible = false;
                                grdInternalComplinacePerformer.Visible = true;
                                grdInternalComplianceReviewer.Visible = false;
                                liReviewer.Attributes.Add("class", "");
                                liPerformer.Attributes.Add("class", "active");

                                performerdocuments.Attributes.Remove("class");
                                performerdocuments.Attributes.Add("class", "tab-pane active");
                                reviewerdocuments.Attributes.Remove("class");
                                reviewerdocuments.Attributes.Add("class", "tab-pane");
                            }
                        }

                        var RolDataReviewer = GridAllData1.Where(entry => (entry.RoleID == 4)).ToList();
                        if (roles.Contains(4))
                        {
                            if (RolDataReviewer.Count > 0)
                            {
                                grdInternalComplianceReviewer.DataSource = RolDataReviewer;
                                Session["TotalComplianceRows"] = RolDataReviewer.Count();
                                grdInternalComplianceReviewer.DataBind();
                                grdComplianceTransactions.Visible = false;
                                grdComplianceTransactionsRev.Visible = false;
                                grdInternalComplinacePerformer.Visible = false;
                                grdInternalComplianceReviewer.Visible = true;
                                liPerformer.Attributes.Add("class", "");
                                liReviewer.Attributes.Add("class", "active");

                                performerdocuments.Attributes.Remove("class");
                                performerdocuments.Attributes.Add("class", "tab-pane");
                                reviewerdocuments.Attributes.Remove("class");
                                reviewerdocuments.Attributes.Add("class", "tab-pane active");
                            }
                        }
                        if (RolData.Count > 0 && RolDataReviewer.Count > 0)
                        {
                            liReviewer.Attributes.Add("class", "");
                            liPerformer.Attributes.Add("class", "active");

                            performerdocuments.Attributes.Remove("class");
                            performerdocuments.Attributes.Add("class", "tab-pane active");
                            reviewerdocuments.Attributes.Remove("class");
                            reviewerdocuments.Attributes.Add("class", "tab-pane");
                            grdInternalComplinacePerformer.Visible = true;
                        }
                    }
                    else if (roles.Contains(3))
                    {
                        grdInternalComplinacePerformer.DataSource = GridAllData1;
                        Session["TotalComplianceRows"] = GridAllData1.Count();
                        grdInternalComplinacePerformer.DataBind();
                        grdComplianceTransactions.Visible = false;
                        grdComplianceTransactionsRev.Visible = false;
                        grdInternalComplinacePerformer.Visible = true;
                        grdInternalComplianceReviewer.Visible = false;
                    }
                    else if (roles.Contains(4))
                    {
                        grdInternalComplianceReviewer.DataSource = GridAllData1;
                        Session["TotalComplianceRows"] = GridAllData1.Count();
                        grdInternalComplianceReviewer.DataBind();
                        grdComplianceTransactions.Visible = false;
                        grdComplianceTransactionsRev.Visible = false;
                        grdInternalComplinacePerformer.Visible = false;
                        grdInternalComplianceReviewer.Visible = true;
                    }
                    else
                    {
                        grdInternalComplinacePerformer.DataSource = GridAllData1;
                        Session["TotalComplianceRows"] = GridAllData1.Count();
                        grdInternalComplinacePerformer.DataBind();
                        grdComplianceTransactions.Visible = false;
                        grdComplianceTransactionsRev.Visible = false;
                        grdInternalComplinacePerformer.Visible = true;
                        grdInternalComplianceReviewer.Visible = false;
                    }
                }
                else
                {
                    if (roles.Contains(3) && roles.Contains(4))
                    {
                        var RolData = GridAllData.Where(entry => (entry.RoleID == 3)).ToList();
                        if (roles.Contains(3))
                        {
                            if (RolData.Count > 0)
                            {
                                grdComplianceTransactions.DataSource = RolData;
                                Session["TotalComplianceRows"] = RolData.Count();
                                grdComplianceTransactions.DataBind();
                                grdComplianceTransactions.Visible = true;
                                grdComplianceTransactionsRev.Visible = false;
                                grdInternalComplinacePerformer.Visible = false;
                                grdInternalComplianceReviewer.Visible = false;
                                liReviewer.Attributes.Add("class", "");
                                liPerformer.Attributes.Add("class", "active");

                                performerdocuments.Attributes.Remove("class");
                                performerdocuments.Attributes.Add("class", "tab-pane active");
                                reviewerdocuments.Attributes.Remove("class");
                                reviewerdocuments.Attributes.Add("class", "tab-pane");
                            }
                        }
                        var RolDataReviewer = GridAllData.Where(entry => (entry.RoleID == 4)).ToList();
                        if (roles.Contains(4))
                        {
                            if (RolDataReviewer.Count > 0)
                            {
                                grdComplianceTransactionsRev.DataSource = RolDataReviewer;
                                Session["TotalComplianceRows"] = RolDataReviewer.Count();
                                grdComplianceTransactionsRev.DataBind();
                                grdComplianceTransactions.Visible = false;
                                grdComplianceTransactionsRev.Visible = true;
                                grdInternalComplinacePerformer.Visible = false;
                                grdInternalComplianceReviewer.Visible = false;
                                liPerformer.Attributes.Add("class", "");
                                liReviewer.Attributes.Add("class", "active");

                                performerdocuments.Attributes.Remove("class");
                                performerdocuments.Attributes.Add("class", "tab-pane");
                                reviewerdocuments.Attributes.Remove("class");
                                reviewerdocuments.Attributes.Add("class", "tab-pane active");
                            }
                        }
                        if (RolData.Count > 0 && RolDataReviewer.Count > 0)
                        {
                            liReviewer.Attributes.Add("class", "");
                            liPerformer.Attributes.Add("class", "active");

                            performerdocuments.Attributes.Remove("class");
                            performerdocuments.Attributes.Add("class", "tab-pane active");
                            reviewerdocuments.Attributes.Remove("class");
                            reviewerdocuments.Attributes.Add("class", "tab-pane");
                            grdComplianceTransactions.Visible = true;
                        }
                    }
                    else if (roles.Contains(4))
                    {
                        grdComplianceTransactionsRev.DataSource = GridAllData;
                        Session["TotalComplianceRows"] = GridAllData.Count();
                        grdComplianceTransactionsRev.DataBind();
                        grdComplianceTransactions.Visible = false;
                        grdComplianceTransactionsRev.Visible = true;
                        grdInternalComplinacePerformer.Visible = false;
                        grdInternalComplianceReviewer.Visible = false;


                        //Added by rahul on 7 OCT 2017
                        liPerformer.Attributes.Add("class", "");
                        liReviewer.Attributes.Add("class", "active");
                        
                        performerdocuments.Attributes.Remove("class");
                        performerdocuments.Attributes.Add("class", "tab-pane");
                        reviewerdocuments.Attributes.Remove("class");
                        reviewerdocuments.Attributes.Add("class", "tab-pane active");

                    }
                    else
                    {
                        grdComplianceTransactions.DataSource = GridAllData;
                        Session["TotalComplianceRows"] = GridAllData.Count();
                        grdComplianceTransactions.DataBind();
                        grdComplianceTransactions.Visible = true;
                        grdComplianceTransactionsRev.Visible = false;
                        grdInternalComplinacePerformer.Visible = false;
                        grdInternalComplianceReviewer.Visible = false;
                    }
                }
            }
        }



        protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindEventNature();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindEvents()
        {
            try
            {
                if (CustomerID == 0)
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                ddlEvent.DataSource = null;
                ddlEvent.DataBind();
                ddlEvent.DataTextField = "EventName";
                ddlEvent.DataValueField = "eventid";
                var TypeList = ActManagement.GetActiveEvents(10, CustomerID, AuthenticationHelper.UserID, UserRoleID);
                ddlEvent.DataSource = TypeList;
                ddlEvent.DataBind();

                ddlEvent.Items.Insert(0, new ListItem("Event Name", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindEventNature()
        {
            try
            {
                if (CustomerID == 0)
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                ddlEventNature.DataSource = null;
                ddlEventNature.DataBind();
                ddlEventNature.DataTextField = "EventNature";
                ddlEventNature.DataValueField = "EventScheduleOnid";
                var TypeList = ActManagement.GetEventNature(10, CustomerID, AuthenticationHelper.UserID, Convert.ToInt32(ddlEvent.SelectedValue), UserRoleID);
                ddlEventNature.DataSource = TypeList;
                ddlEventNature.DataBind();

                ddlEventNature.Items.Insert(0, new ListItem("Event Nature", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindLocationFilter()
        {
            try
            {
                if (CustomerID == 0)
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(CustomerID);
                string isstatutoryinternal = "";
                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {
                    isstatutoryinternal = "I";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Event Based")
                {
                    isstatutoryinternal = "S";
                }

                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, CustomerID, "EXCT", isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                   // BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }

        public  void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<int> LocationList)
        {
            foreach (var item in nvp.Children)
            {
                TreeNode node = new TreeNode(item.Name, item.ID.ToString());

                if (FindNodeExists(item, LocationList))
                {
                    BindBranchesHierarchy(node, item, LocationList);
                    parent.ChildNodes.Add(node);
                }
            }
        }

        public  bool FindNodeExists(NameValueHierarchy item, List<int> ListIDs)
        {            
            bool result = false;
            try
            {
                if (ListIDs.Contains(item.ID))
                {                    
                    result = true;
                    return result;
                }
                else
                {
                    foreach (var childNode in item.Children)
                    {
                        if (ListIDs.Contains(childNode.ID))
                        {
                            result = true;
                        }
                        else
                        {                           
                            result= FindNodeExists(childNode, ListIDs);
                        }

                        if (result)
                        {
                            break;
                        }
                    }

                  
                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        
        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", string.Format("initializeDatePicker11(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker11", "initializeDatePicker11(null);", true);
                }

                DateTime date1 = DateTime.MinValue;
                if (DateTime.TryParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker12", string.Format("initializeDatePicker12(new Date({0}, {1}, {2}));", date1.Year, date1.Month - 1, date1.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "InitializeDatePicker12", "initializeDatePicker12(null);", true);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        

        #region Search

        private void BindStatusPerformer()
        {
            try
            {
                ddlStatus.Items.Clear();
                foreach (Performer r in Enum.GetValues(typeof(Performer)))
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(Performer), r), r.ToString());
                    ddlStatus.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindStatusReviewer()
        {
            try
            {
                ddlStatus.Items.Clear();
                foreach (Reviewer r in Enum.GetValues(typeof(Reviewer)))
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(Reviewer), r), r.ToString());
                    ddlStatus.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCategories()
        {
            try
            {
                if (CustomerID == 0)
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();

                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";
                var CatList = ActManagement.GetAllAssignedCategory(CustomerID, AuthenticationHelper.UserID);
                ddlCategory.DataSource = CatList;
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private void BindCategoriesInternal()
        {
            try
            {
                if (CustomerID == 0)
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                var CatList = ActManagement.GetAllAssignedInternalCategory(CustomerID, AuthenticationHelper.UserID);

                ddlCategory.DataSource = CatList;
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindTypesInternal()
        {
            try
            {
                if (CustomerID == 0)
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                ddlType.DataSource = null;
                ddlType.DataBind();

                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";

                var TypleList = ActManagement.GetAllAssignedInternalType(CustomerID, AuthenticationHelper.UserID);
                ddlType.DataSource = TypleList;
                ddlType.DataBind();

                ddlType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private void BindTypes()
        {
            try
            {
                if (CustomerID == 0)
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                ddlType.DataSource = null;
                ddlType.DataBind();
                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";
                var TypeList = ActManagement.GetAllAssignedType(CustomerID, AuthenticationHelper.UserID);
                ddlType.DataSource = TypeList;
                ddlType.DataBind();

                ddlType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private void BindActList()
        {
            try
            {
                ddlAct.Items.Clear();
                int complianceTypeID = Convert.ToInt32(ddlType.SelectedValue);
                int complianceCategoryID = Convert.ToInt32(ddlCategory.SelectedValue);

                List<SP_GetActsByUserID_Result> ActList = ActManagement.GetActsByUserID(AuthenticationHelper.UserID);
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindActListInternal()
        {
            try
            {
                ddlAct.Items.Clear();

                List<SP_GetInternalActsByUserID_Result> ActList = ActManagement.GetInternalActsByUserID(AuthenticationHelper.UserID);
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindComplianceSubTypeList()
        {
            try
            {
                ddlComplianceSubType.Items.Clear();
               
                List<SP_GetComplianceSubTypeID_Result> ComplianceSubTypeList = ActManagement.GetSubTypeByUserID(AuthenticationHelper.UserID);
                ddlComplianceSubType.DataTextField = "Name";
                ddlComplianceSubType.DataValueField = "ID";
                ddlComplianceSubType.DataSource = ComplianceSubTypeList;
                ddlComplianceSubType.DataBind();

                ddlComplianceSubType.Items.Insert(0, new ListItem("Compliance Subtype", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindAct()
        {
            try
            {
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";

                ddlAct.DataSource = CustomerManagement.GetAct();
                ddlAct.DataBind();

                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                //BindLocationFilter();
                SelectedPageNo.Text = "1";
                if (ddlComplianceType.SelectedItem.Text == "Statutory" && ddlStatus.SelectedValue != "Status")
                {
                    queryStringFlag = "";
                }


                BindComplianceTransactions(ClickChangeflag, queryStringFlag);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        static public void EnumToListBox(Type EnumType, ListControl TheListBox)
        {
            Array Values = System.Enum.GetValues(EnumType);

            foreach (long Value in Values)
            {
                string Display = Enum.GetName(EnumType, Value);
                ListItem Item = new ListItem(Display, Value.ToString());
                TheListBox.Items.Add(Item);
            }
        }

        protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ddlComplianceSubType.SelectedValue = "-1";
                ddlType.SelectedValue = "-1";
                ddlCategory.SelectedValue = "-1";
                ddlAct.SelectedValue = "-1";
                txtStartDate.Text = string.Empty;
                txtEndDate.Text = string.Empty;
                divAdvSearch.Visible = false;
                txtSearchType.Text = string.Empty;
                ddlperformer.SelectedValue = "-1";
                //if (!string.IsNullOrEmpty(Request.QueryString["ScheduledOn"]))
                //{
                //    BindComplianceInstanceGridDateWise(Request.QueryString["ScheduledOn"]);
                //}
                //else
                //{
                    BindComplianceTransactions(ClickChangeflag, queryStringFlag);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
                
        protected void ddlpageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageSize = int.Parse(((DropDownList)sender).SelectedValue);

                //if (!IsValid()) { return; };
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                //grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                //grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                if ((ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based") && ClickChangeflag == "P")
                {
                    grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else if ((ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based") && ClickChangeflag == "R")
                {
                    grdComplianceTransactionsRev.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactionsRev.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal" && ClickChangeflag == "P")
                {
                    grdInternalComplinacePerformer.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdInternalComplinacePerformer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal" && ClickChangeflag == "R")
                {
                    grdInternalComplianceReviewer.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdInternalComplianceReviewer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                //if (!string.IsNullOrEmpty(Request.QueryString["ScheduledOn"]))
                //{
                //    BindComplianceInstanceGridDateWise(Request.QueryString["ScheduledOn"]);
                //}
                //else
                //{
                    BindComplianceTransactions(ClickChangeflag, queryStringFlag);
                //}

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) > Convert.ToInt32(Session["TotalComplianceRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalComplianceRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalComplianceRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if ((ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based") && ClickChangeflag == "P")
                {
                    grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else if ((ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based") && ClickChangeflag == "R")
                {
                    grdComplianceTransactionsRev.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactionsRev.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal" && ClickChangeflag == "P")
                {
                    grdInternalComplinacePerformer.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdInternalComplinacePerformer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal" && ClickChangeflag == "R")
                {
                    grdInternalComplianceReviewer.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdInternalComplianceReviewer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                //if (!string.IsNullOrEmpty(Request.QueryString["ScheduledOn"]))
                //{
                //    BindComplianceInstanceGridDateWise(Request.QueryString["ScheduledOn"]);
                //}
                //else
                //{
                    BindComplianceTransactions(ClickChangeflag, queryStringFlag);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlpageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalComplianceRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalComplianceRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if ((ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based") && ClickChangeflag == "P")
                {
                    grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else if ((ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based") && ClickChangeflag == "R")
                {
                    grdComplianceTransactionsRev.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactionsRev.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal" && ClickChangeflag == "P")
                {
                    grdInternalComplinacePerformer.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdInternalComplinacePerformer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal" && ClickChangeflag == "R")
                {
                    grdInternalComplianceReviewer.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdInternalComplianceReviewer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                //if (!string.IsNullOrEmpty(Request.QueryString["ScheduledOn"]))
                //{
                //    BindComplianceInstanceGridDateWise(Request.QueryString["ScheduledOn"]);
                //}
                //else
                //{
                    BindComplianceTransactions(ClickChangeflag, queryStringFlag);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                //DivRecordsScrum.Attributes.Remove("disabled");
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalComplianceRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlpageSize.SelectedValue) > Convert.ToInt32(Session["TotalComplianceRows"].ToString())))
                            lblEndRecord.Text = ddlpageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalComplianceRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private int GetTotalPagesCount()
        {
            try
            {              

                TotalRows.Value = Session["TotalComplianceRows"].ToString();
                
                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlpageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlpageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divEvent.Visible = false;

                if (ddlComplianceType.SelectedValue == "0")
                {
                    btnReviseComplianceInternal.Visible = true;
                    btnReviseCompliance.Visible = false;
                }
                else
                {
                    btnReviseCompliance.Visible = true;
                    btnReviseComplianceInternal.Visible = false;
                }

                if (ddlComplianceType.SelectedValue == "0")
                {
                    ddlEventNature.SelectedValue = "-1";
                    ddlEvent.SelectedValue = "-1";
                    ddlAct.ClearSelection();
                    if (AuthenticationHelper.CustomerID == 63)
                    {
                        PanelAct.Enabled = true;
                    }
                    else
                    {
                        PanelAct.Enabled = false;
                    }
                    Panelsubtype.Enabled = false;
                    DivAct.Attributes.Add("disabled", "true");

                    BindTypesInternal();
                    BindCategoriesInternal();
                }
                else if (ddlComplianceType.SelectedValue == "-1")
                {
                    ddlEventNature.SelectedValue = "-1";
                    ddlEvent.SelectedValue = "-1";
                    ddlAct.ClearSelection();
                    PanelAct.Enabled = true;
                    Panelsubtype.Enabled = true;
                    DivAct.Attributes.Remove("disabled");
                    BindTypes();
                    BindCategories();
                }
                else if (ddlComplianceType.SelectedValue == "1")
                {
                    divEvent.Visible = true;
                    ddlAct.ClearSelection();
                    PanelAct.Enabled = true;
                    Panelsubtype.Enabled = true;
                    DivAct.Attributes.Remove("disabled");
                    BindTypes();
                    BindCategories();
                }
                SelectedPageNo.Text = "1";
               
                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);
                Performer status = (Performer)Convert.ToInt16(ddlStatus.SelectedIndex);
                int location = Convert.ToInt32(tvFilterLocation.SelectedValue);

                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {
                    queryStringFlag = "C";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Event Based")
                {
                    queryStringFlag = "E";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {
                    queryStringFlag = "I";
                }
                BindLocationFilter();
                //if (!string.IsNullOrEmpty(Request.QueryString["ScheduledOn"]))
                //{
                //    BindComplianceInstanceGridDateWise(Request.QueryString["ScheduledOn"]);
                //}
                //else
                //{
                    BindComplianceTransactions(ClickChangeflag, queryStringFlag);
                //}
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAdvSearch_Click(object sender, EventArgs e)
        {
            try
            {
                lblAdvanceSearchScrum.Text = String.Empty;

                int type = 0;
                int Category = 0;
                int Act = 0;
                int SubCategory = 0;
                SelectedPageNo.Text = "1";
                if (txtStartDate.Text != "" && txtEndDate.Text == "" || txtStartDate.Text == "" && txtEndDate.Text != "")
                {
                    if (txtStartDate.Text == "")
                        txtStartDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                    else if (txtEndDate.Text == "")
                        txtEndDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");

                    return;
                }

                if (ddlType.SelectedValue != "-1")
                {
                    type = ddlType.SelectedItem.Text.Length;

                    if (type >= 20)
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text.Substring(0, 20) + "...";
                    else
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text;
                }

                if (ddlCategory.SelectedValue != "-1")
                {
                    Category = ddlCategory.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                    else
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                }

                if (ddlAct.SelectedValue != "-1")
                {
                    Act = ddlAct.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                    else
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                }

                if (ddlComplianceSubType.SelectedValue != "-1")
                {
                    SubCategory = ddlComplianceSubType.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (SubCategory >= 30)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Sub Category : </b>" + ddlComplianceSubType.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Sub Category: </b>" + ddlComplianceSubType.SelectedItem.Text;
                    }
                    else
                    {
                        if (SubCategory >= 30)
                            lblAdvanceSearchScrum.Text += "<b>Sub Category: </b>" + ddlComplianceSubType.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Sub Category: </b>" + ddlComplianceSubType.SelectedItem.Text;
                    }
                }

                if (txtStartDate.Text != "" && txtEndDate.Text != "")
                {
                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        lblAdvanceSearchScrum.Text += "     " + "<b>Start Date: </b>" + txtStartDate.Text;
                        lblAdvanceSearchScrum.Text += "     " + "<b>End Date: </b>" + txtEndDate.Text;
                    }
                    else
                    {
                        lblAdvanceSearchScrum.Text += "<b>Start Date: </b>" + txtStartDate.Text;
                        lblAdvanceSearchScrum.Text += "<b> End Date: </b>" + txtEndDate.Text;
                    }
                }

                if (ddlperformer.SelectedValue != "-1")
                {
                    lblAdvanceSearchScrum.Text += "<b>Performer: </b>" + ddlperformer.SelectedItem.Text;
                }

                if (txtSearchType.Text != "")
                {
                    lblAdvanceSearchScrum.Text += "<b>Type To Search: </b>" + txtSearchType.Text;
                }


                //DateTime ScheduleOnCalender = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture); ;
                //if (!string.IsNullOrEmpty(Request.QueryString["ScheduleOn"]))
                //{
                //    ScheduleOnCalender = Convert.ToDateTime(Request.QueryString["ScheduleOn"]);

                //    lblAdvanceSearchScrum.Text += "<b>Due Date: </b>" + ScheduleOnCalender;
                //}

                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                }
                else
                {
                    divAdvSearch.Visible = false;
                }

                //if (!string.IsNullOrEmpty(Request.QueryString["ScheduledOn"]))
                //{
                //    BindComplianceInstanceGridDateWise(Request.QueryString["ScheduledOn"]);
                //}
                //else
                //{
                    BindComplianceTransactions(ClickChangeflag, queryStringFlag);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void UpdatePanel1_Load(object sender, EventArgs e)
        {
           
        }

        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                //if (!string.IsNullOrEmpty(Request.QueryString["ScheduledOn"]))
                //{
                //    BindComplianceInstanceGridDateWise(Request.QueryString["ScheduledOn"]);
                //}
                //else
                //{
                    BindComplianceTransactions(ClickChangeflag, queryStringFlag);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                //List<ComplianceInstanceTransactionView> assignmentList = null;
                //if (ViewState["Role"].Equals("Performer"))
                //{
                //    assignmentList = DashboardManagement.DashboardDataForPerformerUpcoming(AuthenticationHelper.UserID, "C", CannedReportFilterForPerformer.Upcoming);
                //}
                //else if (ViewState["Role"].Equals("Reviewer"))
                //{
                //    assignmentList = DashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, "C", CannedReportFilterForPerformer.Upcoming);
                //}
                //else if (ViewState["Role"].Equals("Approver"))
                //{
                //    assignmentList = DashboardManagement.DashboardDataForApprover(AuthenticationHelper.UserID, "C", CannedReportFilterForPerformer.Upcoming);
                //}

                //if (direction == SortDirection.Ascending)
                //{
                //    assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //    direction = SortDirection.Descending;

                //}
                //else
                //{
                //    assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //    direction = SortDirection.Ascending;
                //}

                //foreach (DataControlField field in grdComplianceTransactions.Columns)
                //{
                //    if (field.SortExpression == e.SortExpression)
                //    {
                //        ViewState["SortIndex"] = grdComplianceTransactions.Columns.IndexOf(field);
                //    }
                //}

                //grdComplianceTransactions.DataSource = assignmentList;
                //grdComplianceTransactions.DataBind();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    //Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledOn");
                    Label lblScheduledPerformerOn = (Label) e.Row.FindControl("lblScheduledOn");
                    Label lblDays = (Label) e.Row.FindControl("lblInterimdays");
                    Button btnChangeStatus = (Button) e.Row.FindControl("btnChangeStatus");
                    DateTime CurrentDate = DateTime.Today.Date;

                    if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledPerformerOn.Text).Date > CurrentDate || lblStatus.Text.Trim() == "Submitted For Interim Review" || lblStatus.Text.Trim() == "Interim Review Approved" || lblStatus.Text.Trim() == "Interim Rejected")
                    {
                        if (!string.IsNullOrEmpty(lblDays.Text))
                        {
                            e.Row.ForeColor = System.Drawing.Color.Blue;
                            e.Row.CssClass = "Inforamative";
                        }
                    }

                    btnChangeStatus.Enabled = true;
                    if (lblStatus.Text.Trim() == "Submitted For Interim Review")
                    {
                        btnChangeStatus.Enabled = false;
                    }

                    if (lblStatus.Text.Trim() == "Interim Review Approved")
                    {
                        btnChangeStatus.Enabled = true;
                        btnChangeStatus.Visible = true;
                    }
                    if (lblStatus.Text.Trim() == "Interim Rejected")
                    {
                        btnChangeStatus.Enabled = true;
                        btnChangeStatus.Visible = true;
                    }

                    if (lblStatus != null)
                    {
                        if (lblScheduledPerformerOn != null)
                        {
                            //DateTime CurrentDate = DateTime.Today.Date;
                            if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledPerformerOn.Text).Date >= CurrentDate)
                                lblStatus.Text = "Upcoming";
                            else if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledPerformerOn.Text).Date <= CurrentDate)
                                lblStatus.Text = "Overdue";
                            else if (lblStatus.Text.Trim() == "Complied but pending review")
                                lblStatus.Text = "Pending For Review";
                            else if (lblStatus.Text.Trim() == "Complied Delayed but pending review")
                                lblStatus.Text = "Pending For Review";
                            else if (lblStatus.Text.Trim() == "In Progress" && Convert.ToDateTime(lblScheduledPerformerOn.Text).Date >= CurrentDate)
                                lblStatus.Text = "Upcoming";
                            else if (lblStatus.Text.Trim() == "In Progress" && Convert.ToDateTime(lblScheduledPerformerOn.Text).Date <= CurrentDate)
                                lblStatus.Text = "Overdue";
                        }
                    }
                }               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void grdComplianceTransactions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandName.Equals("Sort")))
                {
                    if (e.CommandName.Equals("CHANGE_STATUS"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                        int complianceInstanceID = Convert.ToInt32(commandArgs[1]);
                        string Interimdays = Convert.ToString(commandArgs[2]);
                        string Status = Convert.ToString(commandArgs[3]);

                        Session["Days"] = "";
                        if (!string.IsNullOrEmpty(Interimdays))
                        {
                            Session["Days"] = Interimdays;
                            Session["Status"] = Interimdays;
                        }

                       // udcStatusTranscatopn.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        /// <summary>
        /// this function bind the upcoming compliances to the grid view.
        /// </summary>
        protected void BindComplianceTransactions(string FlagPR,string queryStringFlag)
        {
            try
            {
                
                List<long> InternalLicenseScheduleOnID = InternalLicenseMgmt.GetLicenseScheduleonIdList(AuthenticationHelper.CustomerID);
                List<long> StatutoryLicenseScheduleOnID = LicenseMgmt.GetLicenseScheduleonIdList(AuthenticationHelper.CustomerID);
                Session["TotalComplianceRows"] = 0;                
                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);
                int location = Convert.ToInt32(tvFilterLocation.SelectedValue);
                int type = Convert.ToInt32(ddlType.SelectedValue);
                int category = Convert.ToInt32(ddlCategory.SelectedValue);
                int ActID = Convert.ToInt32(ddlAct.SelectedValue);
                int SubTypeID = Convert.ToInt32(ddlComplianceSubType.SelectedValue);
                int UserIDPerformer = -1;
                int EventID = -1;
                int EventScheduleID = -1;

                DateTime ScheduleOnCalender = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture); ;
                if (!string.IsNullOrEmpty(Request.QueryString["ScheduleOn"]))
                {
                    ScheduleOnCalender = Convert.ToDateTime(Request.QueryString["ScheduleOn"]);
                }

                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;
                string StringType = "";
                StringType = txtSearchType.Text;
                if (txtStartDate.Text == "")
                {
                    dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtfrom = DateTime.ParseExact(txtStartDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                if (txtEndDate.Text == "")
                {
                    dtTo = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtTo = DateTime.ParseExact(txtEndDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                
                if (!string.IsNullOrEmpty(ddlEvent.SelectedValue))
                {
                    if (ddlEvent.SelectedValue != "-1")
                        EventID = Convert.ToInt32(ddlEvent.SelectedValue);
                }                
                if (!string.IsNullOrEmpty(ddlEventNature.SelectedValue))                    
                {
                    if (ddlEventNature.SelectedValue != "-1")
                        EventScheduleID = Convert.ToInt32(ddlEventNature.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlperformer.SelectedValue))
                {
                    if (ddlperformer.SelectedValue != "-1")
                        UserIDPerformer = Convert.ToInt32(ddlperformer.SelectedValue);
                }
                grdComplianceTransactions.Visible = false;
                grdComplianceTransactionsRev.Visible = false;
                grdInternalComplinacePerformer.Visible = false;
                grdInternalComplianceReviewer.Visible = false;
                //grdComplianceTransactionsUpcoming.Visible = false;
                //grdComplianceEventReviewer.Visible = false;
                if (roles.Contains(3) && roles.Contains(4))
                {
                    if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based")
                    {
                        if (FlagPR == "P")
                        {                            
                            string status = ddlStatus.SelectedItem.Text;
                            var ComplianceList = DashboardManagement.DashboardDataForPerformerUpcomingNewSp(AuthenticationHelper.UserID, queryStringFlag, risk, status, location, type, category, ActID, dtfrom, dtTo, EventID, EventScheduleID, SubTypeID, StringType, UserIDPerformer, ScheduleOnCalender);
                            spcompliancetransaction = ComplianceList;
                            ComplianceList = ComplianceList.Where(entry => !StatutoryLicenseScheduleOnID.Contains(entry.ScheduledOnID)).ToList();
                            
                            grdComplianceTransactions.DataSource = ComplianceList;
                            Session["TotalComplianceRows"] = ComplianceList.Count();
                            grdComplianceTransactions.DataBind();
                            GetPageDisplaySummary();
                            grdComplianceTransactions.Visible = true;
                            performerdocuments.Visible = true;
                            reviewerdocuments.Visible = false;

                            liReviewer.Attributes.Add("class", "");
                            liPerformer.Attributes.Add("class", "active");
                            performerdocuments.Attributes.Remove("class");
                            performerdocuments.Attributes.Add("class", "tab-pane active");
                            reviewerdocuments.Attributes.Remove("class");
                            reviewerdocuments.Attributes.Add("class", "tab-pane");



                            if (ddlComplianceType.SelectedItem.Text == "Statutory" && (queryStringFlag == "A" || queryStringFlag == ""))
                            {
                                grdComplianceTransactions.Columns[4].Visible = true;
                                grdComplianceTransactions.Columns[5].Visible = true;
                                grdComplianceTransactions.Columns[6].Visible = true;
                                grdComplianceTransactions.Columns[7].Visible = true;
                            }
                            else if (ddlComplianceType.SelectedItem.Text == "Statutory")
                            {
                                grdComplianceTransactions.Columns[4].Visible = true;
                                grdComplianceTransactions.Columns[5].Visible = false;
                                grdComplianceTransactions.Columns[6].Visible = false;
                                grdComplianceTransactions.Columns[7].Visible = true;
                            }
                            else if (ddlComplianceType.SelectedItem.Text == "Event Based")
                            {
                                grdComplianceTransactions.Columns[4].Visible = true;
                                grdComplianceTransactions.Columns[5].Visible = true;
                                grdComplianceTransactions.Columns[6].Visible = true;
                                grdComplianceTransactions.Columns[7].Visible = false;
                            }

                        }
                        else
                        {
                            string status = ddlStatus.SelectedItem.Text;
                            var ComplianceList = DashboardManagement.DashboardDataForReviewerNewSp(AuthenticationHelper.UserID, queryStringFlag, risk, status, location, type, category, ActID, dtfrom, dtTo, EventID, EventScheduleID, SubTypeID, UserIDPerformer, StringType, ScheduleOnCalender);
                            spcompliancetransaction = ComplianceList;
                            ComplianceList = ComplianceList.Where(entry => !StatutoryLicenseScheduleOnID.Contains(entry.ScheduledOnID)).ToList();

                            grdComplianceTransactionsRev.DataSource = ComplianceList;
                            Session["TotalComplianceRows"] = ComplianceList.Count();
                            grdComplianceTransactionsRev.DataBind();
                            GetPageDisplaySummary();
                            grdComplianceTransactionsRev.Visible = true;
                            reviewerdocuments.Visible = true;
                            performerdocuments.Visible = false;

                            liPerformer.Attributes.Add("class", "");
                            liReviewer.Attributes.Add("class", "active");
                            performerdocuments.Attributes.Remove("class");
                            performerdocuments.Attributes.Add("class", "tab-pane");
                            reviewerdocuments.Attributes.Remove("class");
                            reviewerdocuments.Attributes.Add("class", "tab-pane active");

                            if (ddlComplianceType.SelectedItem.Text == "Statutory" && (queryStringFlag == "A" || queryStringFlag == ""))
                            {
                                grdComplianceTransactionsRev.Columns[4].Visible = true;
                                grdComplianceTransactionsRev.Columns[5].Visible = true;
                                grdComplianceTransactionsRev.Columns[6].Visible = true;
                                grdComplianceTransactionsRev.Columns[7].Visible = true;
                            }
                            else if (ddlComplianceType.SelectedItem.Text == "Statutory")
                            {
                                grdComplianceTransactionsRev.Columns[4].Visible = true;
                                grdComplianceTransactionsRev.Columns[5].Visible = false;
                                grdComplianceTransactionsRev.Columns[6].Visible = false;
                                grdComplianceTransactionsRev.Columns[7].Visible = true;
                            }
                            else if (ddlComplianceType.SelectedItem.Text == "Event Based")
                            {
                                grdComplianceTransactionsRev.Columns[4].Visible = true;
                                grdComplianceTransactionsRev.Columns[5].Visible = true;
                                grdComplianceTransactionsRev.Columns[6].Visible = true;
                                grdComplianceTransactionsRev.Columns[7].Visible = false;
                            }
                        }
                    }
                    else if (ddlComplianceType.SelectedItem.Text == "Internal")
                    {
                        if (FlagPR == "P")
                        {
                            string status = ddlStatus.SelectedItem.Text;
                            var ComplianceList = InternalDashboardManagement.DashboardDataForPerformerNew(AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, StringType, ScheduleOnCalender);

                            ComplianceList = ComplianceList.Where(entry => !InternalLicenseScheduleOnID.Contains(entry.InternalScheduledOnID)).ToList();

                            grdInternalComplinacePerformer.DataSource = ComplianceList;
                            Session["TotalComplianceRows"] = ComplianceList.Count();
                            grdInternalComplinacePerformer.DataBind();
                            GetPageDisplaySummary();

                            grdInternalComplinacePerformer.Visible = true;
                            performerdocuments.Visible = true;
                            reviewerdocuments.Visible = false;

                            liReviewer.Attributes.Add("class", "");
                            liPerformer.Attributes.Add("class", "active");
                            performerdocuments.Attributes.Remove("class");
                            performerdocuments.Attributes.Add("class", "tab-pane active");
                            reviewerdocuments.Attributes.Remove("class");
                            reviewerdocuments.Attributes.Add("class", "tab-pane");

                        }
                        else
                        {
                            string status = ddlStatus.SelectedItem.Text;
                            var ComplianceList = InternalDashboardManagement.DashboardDataForReviewerNew(AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, StringType, UserIDPerformer, ScheduleOnCalender);

                            ComplianceList = ComplianceList.Where(entry => !InternalLicenseScheduleOnID.Contains(entry.InternalScheduledOnID)).ToList();


                            grdInternalComplianceReviewer.DataSource = ComplianceList;
                            Session["TotalComplianceRows"] = ComplianceList.Count();
                            grdInternalComplianceReviewer.DataBind();
                            GetPageDisplaySummary();

                            grdInternalComplianceReviewer.Visible = true;
                            reviewerdocuments.Visible = true;
                            performerdocuments.Visible = false;
                            liPerformer.Attributes.Add("class", "");
                            liReviewer.Attributes.Add("class", "active");

                            performerdocuments.Attributes.Remove("class");
                            performerdocuments.Attributes.Add("class", "tab-pane");
                            reviewerdocuments.Attributes.Remove("class");
                            reviewerdocuments.Attributes.Add("class", "tab-pane active");
                        }
                    }

                }
                else if (roles.Contains(3))
                {
                    if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based")
                    {
                        string status = ddlStatus.SelectedItem.Text;
                        var ComplianceList = DashboardManagement.DashboardDataForPerformerUpcomingNewSp(AuthenticationHelper.UserID, queryStringFlag, risk, status, location, type, category, ActID, dtfrom, dtTo, EventID, EventScheduleID, SubTypeID, StringType, UserIDPerformer, ScheduleOnCalender);
                        spcompliancetransaction = ComplianceList;
                        ComplianceList = ComplianceList.Where(entry => !StatutoryLicenseScheduleOnID.Contains(entry.ScheduledOnID)).ToList();

                        grdComplianceTransactions.DataSource = ComplianceList;
                        Session["TotalComplianceRows"] = ComplianceList.Count();
                        grdComplianceTransactions.DataBind();
                        GetPageDisplaySummary();
                        grdComplianceTransactions.Visible = true;
                        if (ddlComplianceType.SelectedItem.Text == "Statutory" && (queryStringFlag == "A" || queryStringFlag == ""))
                        {
                            grdComplianceTransactions.Columns[4].Visible = true;
                            grdComplianceTransactions.Columns[5].Visible = true;
                            grdComplianceTransactions.Columns[6].Visible = true;
                            grdComplianceTransactions.Columns[7].Visible = true;
                        }
                        else if (ddlComplianceType.SelectedItem.Text == "Statutory")
                        {
                            grdComplianceTransactions.Columns[4].Visible = true;
                            grdComplianceTransactions.Columns[5].Visible = false;
                            grdComplianceTransactions.Columns[6].Visible = false;
                            grdComplianceTransactions.Columns[7].Visible = true;
                        }
                        else if (ddlComplianceType.SelectedItem.Text == "Event Based")
                        {
                            grdComplianceTransactions.Columns[4].Visible = true;
                            grdComplianceTransactions.Columns[5].Visible = true;
                            grdComplianceTransactions.Columns[6].Visible = true;
                            grdComplianceTransactions.Columns[7].Visible = false;
                        }
                    }
                    else if (ddlComplianceType.SelectedItem.Text == "Internal")
                    {
                        string status = ddlStatus.SelectedItem.Text;
                        var ComplianceList = InternalDashboardManagement.DashboardDataForPerformerNew(AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, StringType, ScheduleOnCalender);

                        ComplianceList = ComplianceList.Where(entry => !InternalLicenseScheduleOnID.Contains(entry.InternalScheduledOnID)).ToList();


                        grdInternalComplinacePerformer.DataSource = ComplianceList;
                        Session["TotalComplianceRows"] = ComplianceList.Count();
                        grdInternalComplinacePerformer.DataBind();
                        GetPageDisplaySummary();
                        grdInternalComplinacePerformer.Visible = true;
                    }

                    liReviewer.Attributes.Add("class", "");
                    liPerformer.Attributes.Add("class", "active");
                    performerdocuments.Attributes.Remove("class");
                    performerdocuments.Attributes.Add("class", "tab-pane active");
                    reviewerdocuments.Attributes.Remove("class");
                    reviewerdocuments.Attributes.Add("class", "tab-pane");
                    performerdocuments.Visible = true;
                    reviewerdocuments.Visible = false;

                }
                else if (roles.Contains(4))
                {
                    if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based")
                    {
                        string status = ddlStatus.SelectedItem.Text;
                        var ComplianceList = DashboardManagement.DashboardDataForReviewerNewSp(AuthenticationHelper.UserID, queryStringFlag, risk, status, location, type, category, ActID, dtfrom, dtTo, EventID, EventScheduleID, SubTypeID, UserIDPerformer, StringType, ScheduleOnCalender);
                        spcompliancetransaction = ComplianceList;
                        ComplianceList = ComplianceList.Where(entry => !StatutoryLicenseScheduleOnID.Contains(entry.ScheduledOnID)).ToList();

                        grdComplianceTransactionsRev.DataSource = ComplianceList;
                        Session["TotalComplianceRows"] = ComplianceList.Count();
                        grdComplianceTransactionsRev.DataBind();
                        GetPageDisplaySummary();

                        grdComplianceTransactionsRev.Visible = true;

                        if (ddlComplianceType.SelectedItem.Text == "Statutory" && (queryStringFlag == "A" || queryStringFlag == ""))
                        {
                            grdComplianceTransactionsRev.Columns[4].Visible = true;
                            grdComplianceTransactionsRev.Columns[5].Visible = true;
                            grdComplianceTransactionsRev.Columns[6].Visible = true;
                            grdComplianceTransactionsRev.Columns[7].Visible = true;
                        }
                        else if (ddlComplianceType.SelectedItem.Text == "Statutory")
                        {
                            grdComplianceTransactionsRev.Columns[4].Visible = true;
                            grdComplianceTransactionsRev.Columns[5].Visible = false;
                            grdComplianceTransactionsRev.Columns[6].Visible = false;
                            grdComplianceTransactionsRev.Columns[7].Visible = true;
                        }
                        else if (ddlComplianceType.SelectedItem.Text == "Event Based")
                        {
                            grdComplianceTransactionsRev.Columns[4].Visible = true;
                            grdComplianceTransactionsRev.Columns[5].Visible = true;
                            grdComplianceTransactionsRev.Columns[6].Visible = true;
                            grdComplianceTransactionsRev.Columns[7].Visible = false;
                        }

                        //if (queryStringFlag == "C")
                        //{
                        //    grdComplianceTransactionsRev.Columns[5].Visible = false;
                        //    grdComplianceTransactionsRev.Columns[6].Visible = false;
                        //    grdComplianceTransactionsRev.Columns[7].Visible = true;
                        //}
                        //else if (queryStringFlag == "E")
                        //{
                        //    grdComplianceTransactionsRev.Columns[5].Visible = true;
                        //    grdComplianceTransactionsRev.Columns[6].Visible = true;
                        //    grdComplianceTransactionsRev.Columns[7].Visible = false;
                        //}
                    }
                    else if (ddlComplianceType.SelectedItem.Text == "Internal")
                    {
                        string status = ddlStatus.SelectedItem.Text;
                        var ComplianceList = InternalDashboardManagement.DashboardDataForReviewerNew(AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, StringType, UserIDPerformer, ScheduleOnCalender);

                        ComplianceList = ComplianceList.Where(entry => !InternalLicenseScheduleOnID.Contains(entry.InternalScheduledOnID)).ToList();


                        grdInternalComplianceReviewer.DataSource = ComplianceList;
                        Session["TotalComplianceRows"] = ComplianceList.Count();
                        grdInternalComplianceReviewer.DataBind();
                        GetPageDisplaySummary();
                        grdInternalComplianceReviewer.Visible = true;

                    }
                    reviewerdocuments.Visible = true;
                    performerdocuments.Visible = false;
                    liPerformer.Attributes.Add("class", "");
                    liReviewer.Attributes.Add("class", "active");
                    performerdocuments.Attributes.Remove("class");
                    performerdocuments.Attributes.Add("class", "tab-pane");
                    reviewerdocuments.Attributes.Remove("class");
                    reviewerdocuments.Attributes.Add("class", "tab-pane active");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        

        /// <summary>
        /// this function return true false value for status image.
        /// </summary>
        /// <param name="userID">long</param>
        /// <param name="roleID">int</param>
        /// <param name="statusID">int</param>
        /// <returns></returns>
        /// 

        

        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;
                // roles = CustomerBranchManagement.GetDashboardAssignedRoleid(AuthenticationHelper.UserID);
                roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                if (roles.Contains(3) && ClickChangeflag == "P" && (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based"))
                {
                    result = CanChangePerformerStatus(userID, roleID, statusID );
                }
                else if (roles.Contains(4) && ClickChangeflag == "R" && (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based"))
                {
                    result = CanChangeReviewerStatus(userID, roleID, statusID);
                }
                else if (roles.Contains(3) && ClickChangeflag == "P" && ddlComplianceType.SelectedItem.Text == "Internal")
                {
                    result = CanChangeInternalPerformerStatus(userID, roleID, statusID);
                }
                else if (roles.Contains(4) && ClickChangeflag == "R" && ddlComplianceType.SelectedItem.Text == "Internal")
                {
                    result = CanChangeInternalReviewerStatus(userID, roleID, statusID);
                }
                              
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool CanChangePerformerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1 || statusID == 6 || statusID == 10 || statusID == 12;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        
        protected bool CanChangeReviewerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3 || statusID == 11 || statusID == 12; 
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool CanChangeInternalPerformerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        // result = statusID == 1 || statusID == 10;
                        result = statusID == 1 || statusID == 6 || statusID == 10 || statusID == 12;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool CanChangeInternalReviewerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        //result = statusID == 2 || statusID == 3 || statusID == 11;
                        result = statusID == 2 || statusID == 3 || statusID == 11 || statusID == 12;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
        /// <summary>
        /// this function is set sorting images.
        /// </summary>
        /// <param name="columnIndex"></param>
        /// <param name="headerRow"></param>
        void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            //System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            //sortImage.ImageAlign = ImageAlign.AbsMiddle;

            //if (direction == SortDirection.Ascending)
            //{
            //    sortImage.ImageUrl = "../Images/SortAsc.gif";
            //    sortImage.AlternateText = "Ascending Order";
            //}
            //else
            //{
            //    sortImage.ImageUrl = "../Images/SortDesc.gif";
            //    sortImage.AlternateText = "Descending Order";
            //}
            //headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        /// <summary>
        /// this function is set the sorting direction.
        /// </summary>
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        /// <summary>
        /// this function is return the string values for compliance type.
        /// </summary>
        /// <param name="userID">long</param>
        protected string ShowType(long? eventID)
        {
            try
            {
                if (eventID == null)
                {
                    return "Non-Event Based";
                }
                else
                {
                    return "Event Based";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }


        //added by Manisha
        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            try
            {


                LinkButton lnkbtn = sender as LinkButton;
                GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
                Label lblFilePath = (Label)gvrow.FindControl("lblFilePath");
                Label lblFileName = (Label)gvrow.FindControl("lblFileName");

                Response.ContentType = "application/octet-stream";
                string filePath = lblFilePath.Text;

                Response.AddHeader("Content-Disposition", "attachment;filename=\"" + filePath + "\"");
                Response.TransmitFile(Server.MapPath(filePath));
                Response.End();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }



        protected void btnChangeStatusInternalPer_Click(object sender, EventArgs e)
        {
            try
            {

                Button btn = (Button)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                int complianceInstanceID = Convert.ToInt32(commandArgs[1]);
               // udcInternalPerformerStatusTranscation.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                //if (!string.IsNullOrEmpty(Request.QueryString["ScheduledOn"]))
                //{
                //    BindComplianceInstanceGridDateWise(Request.QueryString["ScheduledOn"]);
                //}
                //else
                //{
                    BindComplianceTransactions(ClickChangeflag, queryStringFlag);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }       
        

        protected void btnChangeStatusReviewer_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                int complianceInstanceID = Convert.ToInt32(commandArgs[1]);

                string Interimdays = Convert.ToString(commandArgs[2]);
                string lblStatusReviewer = Convert.ToString(commandArgs[3]);

                Session["InterimdaysReview"] = "";
                Session["ComplianceStatus"] = "";
                if (!string.IsNullOrEmpty(Interimdays) && lblStatusReviewer == "Submitted For Interim Review")
                {
                    Session["InterimdaysReview"] = Interimdays;
                    Session["ComplianceStatus"] = lblStatusReviewer;
                }
               // udcReviewerStatusTranscatopn.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                //if (!string.IsNullOrEmpty(Request.QueryString["ScheduledOn"]))
                //{
                //    BindComplianceInstanceGridDateWise(Request.QueryString["ScheduledOn"]);
                //}
                //else
                //{
                    BindComplianceTransactions(ClickChangeflag, queryStringFlag);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnChangeStatusInternalReviewer_Click(object sender, EventArgs e)
        {
            try
            {

                Button btn = (Button)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                int complianceInstanceID = Convert.ToInt32(commandArgs[1]);
              //  udcStatusTranscationInternal.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                //if (!string.IsNullOrEmpty(Request.QueryString["ScheduledOn"]))
                //{
                //    BindComplianceInstanceGridDateWise(Request.QueryString["ScheduledOn"]);
                //}
                //else
                //{
                    BindComplianceTransactions(ClickChangeflag, queryStringFlag);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        

        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {

                Button btn = (Button)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                int complianceInstanceID = Convert.ToInt32(commandArgs[1]);
                string Interimdays = Convert.ToString(commandArgs[2]);
                string Status = Convert.ToString(commandArgs[3]);
                string lblScheduledOn = Convert.ToString(commandArgs[4]);
                Session["Interimdays"] = "";
                Session["Status"] = "";

                DateTime CurrentDate = DateTime.Today.Date;

                if (Status == "Open" && Convert.ToDateTime(lblScheduledOn).Date > CurrentDate || Status == "Submitted For Interim Review" || Status == "Interim Review Approved" || Status == "Interim Rejected")
                {

                    if (!string.IsNullOrEmpty(Interimdays))
                    {
                        Session["Interimdays"] = Interimdays;
                        Session["Status"] = Status;
                    }
                }

               // udcStatusTranscatopn.OpenTransactionPage(ScheduledOnID, complianceInstanceID);

                BindComplianceTransactions(ClickChangeflag, queryStringFlag);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected string GetPerformerInternal(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserNameInternal(complianceinstanceid, 3);
                InternalPerformername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }

        }
        protected string GetReviewerInternal(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserNameInternal(complianceinstanceid, 4);
                InternalReviewername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }

        }
        protected string GetPerformer(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 3);
                Performername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }

        }

       
        protected string GetReviewer(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 4);
                Reviewername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                return "";
            }

        }
       

        protected void btnPerformer_Click(object sender, EventArgs e)
        {
            try
            {
                btnReassignPerformer.Visible = false;
                if (ddlComplianceType.SelectedValue == "0")
                {
                    btnReviseComplianceInternal.Visible = true;
                    btnReviseCompliance.Visible = false;
                }
                else
                {
                    btnReviseCompliance.Visible = true;
                    btnReviseComplianceInternal.Visible = false;
                }
                BindStatusPerformer();
                ClickChangeflag = "P";

                if (!string.IsNullOrEmpty(Request.QueryString["type"]) && !string.IsNullOrEmpty(Request.QueryString["filter"]))
                {
                    string type = Request.QueryString["type"];
                    string filter = Request.QueryString["filter"];


                    ddlComplianceType.ClearSelection();
                    ddlComplianceType.Items.FindByText(type).Selected = true;
                    ddlStatus.ClearSelection();
                    if (filter != "DueButNotSubmitted")
                    {
                        ddlStatus.Items.FindByText(filter).Selected = true;
                    }
                }
                BindComplianceTransactions(ClickChangeflag, queryStringFlag);
                
                performerdocuments.Visible = true;
                reviewerdocuments.Visible = false;

                // Penalty visible for customer
                string customer = ConfigurationManager.AppSettings["PenaltynotDisplayCustID"].ToString();
                List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                if (PenaltyNotDisplayCustomerList.Count > 0)
                {
                    if (CustomerID == 0)
                    {
                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }
                    foreach (string PList in PenaltyNotDisplayCustomerList)
                    {
                        if (PList == CustomerID.ToString())
                        {
                            IsPenaltyVisible = false;
                            break;
                        }
                    }
                }
                btnTaskDetails.Visible = true;
                if (IsPenaltyVisible == true)
                {
                    btnPenalty.Visible = true;
                    btnPenaltyReviewer.Visible = false;
                }
                else
                {
                    btnPenalty.Visible = false;
                    btnPenaltyReviewer.Visible = false;
                }

                liReviewer.Attributes.Add("class", "");
                liPerformer.Attributes.Add("class", "active");

                performerdocuments.Attributes.Remove("class");
                performerdocuments.Attributes.Add("class", "tab-pane active");
                reviewerdocuments.Attributes.Remove("class");
                reviewerdocuments.Attributes.Add("class", "tab-pane");

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnReviewer_Click(object sender, EventArgs e)
        {
            try
            {
                btnReassignPerformer.Visible = true;
                if (ddlComplianceType.SelectedValue == "0")
                {
                    btnReviseComplianceInternal.Visible = true;
                    btnReviseCompliance.Visible = false;
                }
                else
                {
                    btnReviseCompliance.Visible = true;
                    btnReviseComplianceInternal.Visible = false;
                }
                // Penalty visible for customer
                string customer = ConfigurationManager.AppSettings["PenaltynotDisplayCustID"].ToString();
                List<string> PenaltyNotDisplayCustomerList = customer.Split(',').ToList();
                if (PenaltyNotDisplayCustomerList.Count > 0)
                {
                    if (CustomerID == 0)
                    {
                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    }
                    foreach (string PList in PenaltyNotDisplayCustomerList)
                    {
                        if (PList == CustomerID.ToString())
                        {
                            IsPenaltyVisible = false;
                            break;
                        }
                    }
                }
                btnTaskDetails.Visible = false;
                if (IsPenaltyVisible == true)
                {
                    btnPenalty.Visible = false;
                    btnPenaltyReviewer.Visible = true;
                }
                else
                {
                    btnPenalty.Visible = false;
                    btnPenaltyReviewer.Visible = false;
                }
                BindStatusReviewer();
                ClickChangeflag = "R";

                if (!string.IsNullOrEmpty(Request.QueryString["type"]) && !string.IsNullOrEmpty(Request.QueryString["filter"]))
                {
                    string type = Request.QueryString["type"];
                    string filter = Request.QueryString["filter"];

                    ddlComplianceType.ClearSelection();
                    ddlComplianceType.Items.FindByText(type).Selected = true;
                    ddlStatus.ClearSelection();
                    if (filter != "Overdue")
                    {
                        ddlStatus.Items.FindByText(filter).Selected = true;
                    }
                }

                BindComplianceTransactions(ClickChangeflag, queryStringFlag);

                performerdocuments.Visible = false;
                reviewerdocuments.Visible = true;                
                liPerformer.Attributes.Add("class", "");
                liReviewer.Attributes.Add("class", "active");

                performerdocuments.Attributes.Remove("class");
                performerdocuments.Attributes.Add("class", "tab-pane");
                reviewerdocuments.Attributes.Remove("class");
                reviewerdocuments.Attributes.Add("class", "tab-pane active");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkClearAdvanceList_Click(object sender, EventArgs e)
        {
            try
            {
                ddlComplianceSubType.SelectedValue = "-1";
                ddlType.SelectedValue = "-1";
                ddlCategory.SelectedValue = "-1";
                ddlAct.SelectedValue = "-1";
                txtStartDate.Text ="";
                txtEndDate.Text = "";
                //if (!string.IsNullOrEmpty(Request.QueryString["ScheduledOn"]))
                //{
                //    BindComplianceInstanceGridDateWise(Request.QueryString["ScheduledOn"]);
                //}
                //else
                //{
                    BindComplianceTransactions(ClickChangeflag, queryStringFlag);
                //}
                divAdvSearch.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        #region Reviewer

        protected void grdComplianceTransactionsRev_Sorting(object sender, GridViewSortEventArgs e)
        {
            //try
            //{
            //    List<ComplianceInstanceTransactionView> assignmentList = null;
            //    if (ViewState["Role"].Equals("Reviewer"))
            //    {
            //        assignmentList = DashboardManagement.DashboardDataForReviewer(AuthenticationHelper.UserID, "C", CannedReportFilterForPerformer.Upcoming);
            //    }

            //    if (direction == SortDirection.Ascending)
            //    {
            //        assignmentList = assignmentList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
            //        direction = SortDirection.Descending;

            //    }
            //    else
            //    {
            //        assignmentList = assignmentList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
            //        direction = SortDirection.Ascending;
            //    }

            //    foreach (DataControlField field in grdComplianceTransactions.Columns)
            //    {
            //        if (field.SortExpression == e.SortExpression)
            //        {
            //            ViewState["SortIndex"] = grdComplianceTransactions.Columns.IndexOf(field);
            //        }
            //    }

            //    grdComplianceTransactions.DataSource = assignmentList;
            //    grdComplianceTransactions.DataBind();

            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}
        }

        protected void grdComplianceTransactionsRev_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactionsRev.PageIndex = e.NewPageIndex;
                //if (!string.IsNullOrEmpty(Request.QueryString["ScheduledOn"]))
                //{
                //    BindComplianceInstanceGridDateWise(Request.QueryString["ScheduledOn"]);
                //}
                //else
                //{
                    BindComplianceTransactions(ClickChangeflag, queryStringFlag);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactionsRev_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatusReviewer");
                    Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledReviewerOn");
                    Label InterimdaysReview = (Label) e.Row.FindControl("lblInterimdaysReview");
                  
                    if (!string.IsNullOrEmpty(InterimdaysReview.Text) && lblStatus.Text.Trim() == "Submitted For Interim Review")
                    {
                        e.Row.ForeColor = System.Drawing.Color.Blue;
                        e.Row.CssClass = "Inforamative";
                    }

                    if (lblStatus != null)
                    {
                        if (lblScheduledOn != null)
                        {
                            DateTime CurrentDate = DateTime.Today.Date;
                            if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                                lblStatus.Text = "Upcoming";
                            else if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date <= CurrentDate)
                                lblStatus.Text = "Overdue";
                            else if (lblStatus.Text.Trim() == "Complied but pending review")
                                lblStatus.Text = "Pending For Review";
                            else if (lblStatus.Text.Trim() == "Complied Delayed but pending review")
                                lblStatus.Text = "Pending For Review";
                            else if (lblStatus.Text.Trim() == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                                lblStatus.Text = "Upcoming";
                            else if (lblStatus.Text.Trim() == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date <= CurrentDate)
                                lblStatus.Text = "Overdue";
                        }
                    }

                    sp_ComplianceInstanceTransaction_Result rowView = (sp_ComplianceInstanceTransaction_Result)e.Row.DataItem;
                    if (rowView.ComplianceInstanceID != -1)
                    {
                        Label lblComplianceId = (Label)e.Row.FindControl("lblComplianceId");
                        Button btnChangeStatus = (Button)e.Row.FindControl("btnChangeStatusCompRev");
                        if (btnChangeStatus.Visible == true)
                        {
                            e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(btnChangeStatus, "");
                            e.Row.ToolTip = "Click on row to change Compliance Status";
                        }
                    }                  
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactionsRev_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandName.Equals("Sort")))
                {
                    if (e.CommandName.Equals("CHANGE_STATUSCompRev"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                        int complianceInstanceID = Convert.ToInt32(commandArgs[1]);

                        string Interimdays = Convert.ToString(commandArgs[2]);
                        string lblStatusReviewer = Convert.ToString(commandArgs[3]);

                        Session["InterimdaysReview"] = "";
                        Session["ComplianceStatus"] = "";
                        if (!string.IsNullOrEmpty(Interimdays) && lblStatusReviewer == "Submitted For Interim Review")
                        {
                            Session["InterimdaysReview"] = Interimdays;
                            Session["ComplianceStatus"] = lblStatusReviewer;
                        }

                        //if (ClickChangeflag == "R" && ddlComplianceType.SelectedItem.Text == "Statutory")
                        //{
                       // udcReviewerStatusTranscatopn.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                        //}
                        //else if (ClickChangeflag == "R" && ddlComplianceType.SelectedItem.Text == "Internal")
                        //{
                        //    udcStatusTranscationInternal.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdComplianceTransactionsRev_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
            //    if (sortColumnIndex != -1)
            //    {
            //        AddSortImage(sortColumnIndex, e.Row);
            //    }
            //}
        }

        #endregion


#region InternalPerformer

        protected void grdInternalComplinacePerformer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                //if (!string.IsNullOrEmpty(Request.QueryString["ScheduledOn"]))
                //{
                //    BindComplianceInstanceGridDateWise(Request.QueryString["ScheduledOn"]);
                //}
                //else
                //{
                    BindComplianceTransactions(ClickChangeflag, queryStringFlag);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdInternalComplinacePerformer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblInternalPerformerStatus");
                    Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledPerformerOn");
                    if (lblStatus != null)
                    {
                        if (lblScheduledOn != null)
                        {
                            DateTime CurrentDate = DateTime.Today.Date;
                            if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                                lblStatus.Text = "Upcoming";
                            else if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date <= CurrentDate)
                                lblStatus.Text = "Overdue";
                            else if (lblStatus.Text.Trim() == "Complied but pending review")
                                lblStatus.Text = "Pending For Review";
                            else if (lblStatus.Text.Trim() == "Complied Delayed but pending review")
                                lblStatus.Text = "Pending For Review";
                            else if (lblStatus.Text.Trim() == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                                lblStatus.Text = "Upcoming";
                            else if (lblStatus.Text.Trim() == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date <= CurrentDate)
                                lblStatus.Text = "Overdue";
                        }
                    }

                    InternalComplianceInstanceTransactionView rowView = (InternalComplianceInstanceTransactionView)e.Row.DataItem;
                    if (rowView.InternalComplianceInstanceID != -1)
                    {                       
                        Button btnChangeStatus = (Button)e.Row.FindControl("btnChangeStatusIntCompPer");
                        if (btnChangeStatus.Visible == true)
                        {
                            //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(grdComplianceTransactions, "CHANGE_STATUS$" + btnChangeStatus.CommandArgument);
                            e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(btnChangeStatus, "");
                            e.Row.ToolTip = "Click on row to change Compliance Status";

                        }
                    }
                    else
                    {

                        e.Row.Cells[0].Style.Add("border-style", "none");
                        e.Row.Cells[0].Style.Add("font-weight", "700");
                        e.Row.Cells[1].Style.Add("border-style", "none");
                        e.Row.Cells[2].Style.Add("border-style", "none");
                        e.Row.Cells[3].Style.Add("border-style", "none");
                        e.Row.Cells[3].Text = "";
                        e.Row.Cells[6].Text = "";
                        e.Row.Cells[4].Style.Add("border-style", "none");
                        e.Row.Cells[5].Style.Add("border-style", "none");
                        e.Row.Style.Add("background-color", "#9FCBEA");
                        e.Row.Cells[0].Style.Add("Height", "20px");
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdInternalComplinacePerformer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
                 
            try
            {
                if (!(e.CommandName.Equals("Sort")))
                {
                    if (e.CommandName.Equals("CHANGE_STATUSIntCompPer"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                        int complianceInstanceID = Convert.ToInt32(commandArgs[1]);
                        //if (ClickChangeflag == "P" && ddlComplianceType.SelectedItem.Text == "Statutory")
                        //{
                        //    udcStatusTranscatopn.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                        //}
                        //else if (ClickChangeflag == "P" && ddlComplianceType.SelectedItem.Text == "Internal")
                        //{
                         //   udcInternalPerformerStatusTranscation.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                        //}
                        
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdInternalComplinacePerformer_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
            //    if (sortColumnIndex != -1)
            //    {
            //        AddSortImage(sortColumnIndex, e.Row);
            //    }
            //}
        }

#endregion


        #region Internal Reviewer

        protected void grdInternalComplianceReviewer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                //if (!string.IsNullOrEmpty(Request.QueryString["ScheduledOn"]))
                //{
                //    BindComplianceInstanceGridDateWise(Request.QueryString["ScheduledOn"]);
                //}
                //else
                //{
                    BindComplianceTransactions(ClickChangeflag, queryStringFlag);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdInternalComplianceReviewer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatusInternalReviewer");
                    Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledInternalReviewerOn");
                    if (lblStatus != null)
                    {
                        if (lblScheduledOn != null)
                        {
                            DateTime CurrentDate = DateTime.Today.Date;
                            if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                                lblStatus.Text = "Upcoming";
                            else if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date <= CurrentDate)
                                lblStatus.Text = "Overdue";
                            else if (lblStatus.Text.Trim() == "Complied but pending review")
                                lblStatus.Text = "Pending For Review";
                            else if (lblStatus.Text.Trim() == "Complied Delayed but pending review")
                                lblStatus.Text = "Pending For Review";
                            else if (lblStatus.Text.Trim() == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                                lblStatus.Text = "Upcoming";
                            else if (lblStatus.Text.Trim() == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date <= CurrentDate)
                                lblStatus.Text = "Overdue";
                        }
                    }

                    InternalComplianceInstanceTransactionView rowView = (InternalComplianceInstanceTransactionView)e.Row.DataItem;
                    if (rowView.InternalComplianceInstanceID != -1)
                    {                       
                        Button btnChangeStatus = (Button)e.Row.FindControl("btnChangeStatusIntCompRev");
                        if (btnChangeStatus.Visible == true)
                        {
                            //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(grdComplianceTransactions, "CHANGE_STATUS$" + btnChangeStatus.CommandArgument);
                            e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(btnChangeStatus, "");
                            e.Row.ToolTip = "Click on row to change Compliance Status";
                        }
                    }
                    else
                    {
                        e.Row.Cells[0].Style.Add("border-style", "none");
                        e.Row.Cells[0].Style.Add("font-weight", "700");

                        e.Row.Cells[1].Style.Add("border-style", "none");
                        e.Row.Cells[2].Style.Add("border-style", "none");
                        e.Row.Cells[3].Style.Add("border-style", "none");
                        e.Row.Cells[3].Text = "";
                        e.Row.Cells[4].Text = "";
                        e.Row.Cells[5].Style.Add("border-style", "none");
                        e.Row.Cells[6].Style.Add("border-style", "none");
                        e.Row.Style.Add("background-color", "#9FCBEA");
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdInternalComplianceReviewer_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandName.Equals("Sort")))
                {
                    if (e.CommandName.Equals("CHANGE_STATUSIntCompRev"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                        int complianceInstanceID = Convert.ToInt32(commandArgs[1]);
                       // udcStatusTranscationInternal.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdInternalComplianceReviewer_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
            //    if (sortColumnIndex != -1)
            //    {
            //        AddSortImage(sortColumnIndex, e.Row);
            //    }
            //}
        }

        #endregion

        #region Event based Grid



        #endregion

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
    }
}