﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.Controls
{
    public partial class calendardata : System.Web.UI.Page
    {
        public static string date = "";
        public static string Overviewflag = "";
        protected static List<Int32> roles;
        protected static string mroles;
        Boolean flag = true;
        protected static bool IsApprover = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["date"] != "undefined")
                {
                    date = Convert.ToDateTime(Request.QueryString["date"]).ToString("yyyy-dd-MM");
                    DateTime dt = Business.DateTimeExtensions.GetDateCalender(date);
                    roles = CustomerBranchManagement.GetAssignedCalenderRoleId(AuthenticationHelper.UserID, dt);
                    if (flag == true)
                    {
                        if (Request.QueryString["m"].ToString() == "8" && (roles.Contains(3) || roles.Contains(4) || roles.Contains(6)))
                        {
                            mroles = "MGMT";
                            Overviewflag = "Managment";
                            liPerformer1.Attributes.Add("class", "active");
                            liReviewer1.Attributes.Add("class", "");
                            performerCalender.Attributes.Remove("class");
                            performerCalender.Attributes.Add("class", "tab-pane active");
                            reviewerCalender.Attributes.Remove("class");
                            reviewerCalender.Attributes.Add("class", "tab-pane");
                        }
                        else
                        {
                            mroles = "NA";
                            if (roles.Contains(3) && roles.Contains(4))
                            {
                                liPerformer1.Attributes.Add("class", "active");
                                liReviewer1.Attributes.Add("class", "");
                                performerCalender.Attributes.Remove("class");
                                performerCalender.Attributes.Add("class", "tab-pane active");
                                reviewerCalender.Attributes.Remove("class");
                                reviewerCalender.Attributes.Add("class", "tab-pane");
                            }
                            else if (roles.Contains(3))
                            {
                                liPerformer1.Attributes.Add("class", "active");
                                liReviewer1.Attributes.Add("class", "");
                                performerCalender.Attributes.Remove("class");
                                performerCalender.Attributes.Add("class", "tab-pane active");
                                reviewerCalender.Attributes.Remove("class");
                                reviewerCalender.Attributes.Add("class", "tab-pane");
                            }
                            else if (roles.Contains(4))
                            {
                                liReviewer1.Attributes.Add("class", "active");
                                liPerformer1.Attributes.Add("class", "");
                                performerCalender.Attributes.Remove("class");
                                performerCalender.Attributes.Add("class", "tab-pane");
                                reviewerCalender.Attributes.Remove("class");
                                reviewerCalender.Attributes.Add("class", "tab-pane active");
                            }
                            else if (!string.IsNullOrEmpty(Request.QueryString["m"]) && Request.QueryString["m"].ToString() == "8")
                            {
                                Overviewflag = "Managment";
                                liPerformer1.Attributes.Add("class", "active");
                                liReviewer1.Attributes.Add("class", "");
                                performerCalender.Attributes.Remove("class");
                                performerCalender.Attributes.Add("class", "tab-pane active");
                                reviewerCalender.Attributes.Remove("class");
                                reviewerCalender.Attributes.Add("class", "tab-pane");
                            }
                        }
                    }                
                    flag = false;
                    BindDataPerformer();                    
                    if (!string.IsNullOrEmpty(Request.QueryString["m"]) && Request.QueryString["m"].ToString() == "3")
                    {
                        BindDataReviewer();
                        if (string.IsNullOrEmpty(Session["TotalComplianceRowsReviewer"] as string))
                        {
                            if (Session["TotalComplianceRowsReviewer"] != null && Session["TotalComplianceRowsReviewer"] as string != "")
                            {
                                TotalRowsReviewer.Value = Session["TotalComplianceRowsReviewer"].ToString();
                                GetPageDisplaySummaryReviewer();
                            }
                        }
                    }
                    if (string.IsNullOrEmpty(Session["TotalComplianceRows"] as string))
                    {
                        if (Session["TotalComplianceRows"] != null && Session["TotalComplianceRows"] as string != "")
                        {
                            TotalRows.Value = Session["TotalComplianceRows"].ToString();
                            GetPageDisplaySummary();
                        }
                    }
                }
            }
        }
        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();
                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalComplianceRows"].ToString();
                int totalPages = Convert.ToInt32(TotalRows.Value) / 5;
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % 5;
                if (pageItemRemain > 0)
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        private void GetPageDisplaySummaryReviewer()
        {
            try
            {
                lTotalCountReviewer.Text = GetTotalPagesCountReviewer().ToString();

                if (lTotalCountReviewer.Text != "0")
                {
                    if (SelectedPageNoReviewer.Text == "" || SelectedPageNoReviewer.Text == "0" || SelectedPageNoReviewer.Text == "1")
                    {
                        SelectedPageNoReviewer.Text = "1";
                    }
                }
                else if (lTotalCountReviewer.Text == "0")
                {
                    SelectedPageNoReviewer.Text = "0";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private int GetTotalPagesCountReviewer()
        {
            try
            {
                TotalRowsReviewer.Value = Session["TotalComplianceRowsReviewer"].ToString();
                int totalPages = Convert.ToInt32(TotalRowsReviewer.Value) / 5;
                int pageItemRemain = Convert.ToInt32(TotalRowsReviewer.Value) % 5;
                if (pageItemRemain > 0)
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                liPerformer1.Attributes.Add("class", "active");
                liReviewer1.Attributes.Add("class", "");

                performerCalender.Attributes.Remove("class");
                performerCalender.Attributes.Add("class", "tab-pane active");
                reviewerCalender.Attributes.Remove("class");
                reviewerCalender.Attributes.Add("class", "tab-pane");
                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }
                grdCompliancePerformer.PageSize = 5; 
                grdCompliancePerformer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                BindDataPerformer();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void PreviousReviewer_Click(object sender, EventArgs e)
        {
            try
            {
                liReviewer1.Attributes.Add("class", "active");
                liPerformer1.Attributes.Add("class", "");

                performerCalender.Attributes.Remove("class");
                performerCalender.Attributes.Add("class", "tab-pane");
                reviewerCalender.Attributes.Remove("class");
                reviewerCalender.Attributes.Add("class", "tab-pane active");

                if (Convert.ToInt32(SelectedPageNoReviewer.Text) > 1)
                {
                    SelectedPageNoReviewer.Text = (Convert.ToInt32(SelectedPageNoReviewer.Text) - 1).ToString();
                }
                grdComplianceReviewer.PageSize = 5;
                grdComplianceReviewer.PageIndex = Convert.ToInt32(SelectedPageNoReviewer.Text) - 1;
                BindDataReviewer();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                liPerformer1.Attributes.Add("class", "active");
                liReviewer1.Attributes.Add("class", "");

                performerCalender.Attributes.Remove("class");
                performerCalender.Attributes.Add("class", "tab-pane active");
                reviewerCalender.Attributes.Remove("class");
                reviewerCalender.Attributes.Add("class", "tab-pane");
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }
                grdCompliancePerformer.PageSize = 5;
                grdCompliancePerformer.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                BindDataPerformer();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void NextReviewer_Click(object sender, EventArgs e)
        {
            try
            {
                liReviewer1.Attributes.Add("class", "active");
                liPerformer1.Attributes.Add("class", "");

                performerCalender.Attributes.Remove("class");
                performerCalender.Attributes.Add("class", "tab-pane");
                reviewerCalender.Attributes.Remove("class");
                reviewerCalender.Attributes.Add("class", "tab-pane active");

                int currentPageNo = Convert.ToInt32(SelectedPageNoReviewer.Text);
                if (currentPageNo < GetTotalPagesCountReviewer())
                {
                    SelectedPageNoReviewer.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }
                grdComplianceReviewer.PageSize = 5;
                grdComplianceReviewer.PageIndex = Convert.ToInt32(SelectedPageNoReviewer.Text) - 1;
                BindDataReviewer();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }              
        protected void grdCompliancePerformer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCompliancePerformer.PageIndex = e.NewPageIndex;
                BindDataPerformer();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdComplianceReviewer_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceReviewer.PageIndex = e.NewPageIndex;
                BindDataReviewer();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public DataTable fetchDataManagement_DeptHead(int customerid, int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime dt = Business.DateTimeExtensions.GetDateCalender(date);
                string Type = Request.QueryString["Type"].ToString();
                string isallorsi = string.Empty;
                if (Type == "0")
                {
                    isallorsi = "SI";
                }
                else
                {
                    isallorsi = "All";
                }

                #region Statutory
                entities.Database.CommandTimeout = 180;
                var mgmtQueryStatutory = (from row in entities.SP_ComplianceCalender_DeptHead(customerid, userid, isallorsi)
                                          select row).ToList();
                if (mgmtQueryStatutory.Count > 0)
                {
                    mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                #endregion

                #region Internal
                entities.Database.CommandTimeout = 180;
                var mgmtQueryInternal = (from row in entities.SP_InternalComplianceCalender_DeptHead(customerid, userid, isallorsi)
                                         select row).ToList();

                if (mgmtQueryInternal.Count > 0)
                {
                    mgmtQueryInternal = mgmtQueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                #endregion



                var QueryStatutoryPerformer = (from row in mgmtQueryStatutory
                                               where row.PerformerScheduledOn == dt
                                               select new
                                               {
                                                   Interimdays = row.Interimdays != null ? row.Interimdays.ToString() : "",
                                                   UserID = row.UserID,
                                                   ComplianceInstanceID = row.ComplianceInstanceID,
                                                   ScheduledOn = row.PerformerScheduledOn,
                                                   ScheduledOnID = row.ScheduledOnID,
                                                   ShortDescription = row.ShortDescription,
                                                   CustomerBranchID = row.CustomerBranchID,
                                                   ComplianceStatusID = row.ComplianceStatusID,
                                                   CType = row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based",
                                                   RoleID = row.RoleID,
                                                   Risk = row.Risk,
                                               }).ToList();


                var QueryInternalPerformer = (from row in mgmtQueryInternal
                                              where row.InternalScheduledOn.Year == dt.Year
                                             && row.InternalScheduledOn.Month == dt.Month
                                             && row.InternalScheduledOn.Day == dt.Day
                                              select new
                                              {
                                                  Interimdays = "",
                                                  UserID = row.UserID,
                                                  ComplianceInstanceID = row.InternalComplianceInstanceID,
                                                  ScheduledOn = row.InternalScheduledOn,
                                                  ScheduledOnID = row.InternalScheduledOnID,
                                                  ShortDescription = row.ShortDescription,
                                                  CustomerBranchID = row.CustomerBranchID,
                                                  ComplianceStatusID = row.InternalComplianceStatusID,
                                                  RoleID = row.RoleID,
                                                  CType = row.ComplianceType == 1 ? "Internal Checklist" : "Internal",
                                                  Risk = row.Risk,
                                              }).ToList();


                if (mgmtQueryStatutory.Count > 0)
                {
                    mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                DataTable table = new DataTable();
                table.Columns.Add("Interimdays", typeof(string));
                table.Columns.Add("UserID", typeof(int));
                table.Columns.Add("ComplianceInstanceID", typeof(long));
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                table.Columns.Add("ShortDescription", typeof(string));
                table.Columns.Add("ComplianceStatusID", typeof(int));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("ScheduledOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(int));
                table.Columns.Add("CType", typeof(string));
                table.Columns.Add("Risk", typeof(int));

                if (QueryStatutoryPerformer.Count > 0)
                {
                    foreach (var item in QueryStatutoryPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }
                if (QueryInternalPerformer.Count > 0)
                {
                    foreach (var item in QueryInternalPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }
                return table;
            }
        }
        public void BindDataPerformer()
        {
            try
            {
                Session["TotalComplianceRows"] = 0;
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userid = AuthenticationHelper.UserID;

                string date = Convert.ToDateTime(Request.QueryString["date"]).ToString("yyyy-MM-dd");
                DataTable dt = null;
                if (!string.IsNullOrEmpty(Request.QueryString["m"]) && Request.QueryString["m"].ToString() == "8")
                {
                    if (Convert.ToString(Request.QueryString["dhead"]) == "1")
                    {
                        dt = fetchDataManagement_DeptHead(customerid, userid);
                    }
                    else
                    {
                        dt = fetchDataManagement(customerid, userid);
                    }
                }
                else
                {
                    dt = fetchDataPerformer(customerid, userid);
                }

                grdCompliancePerformer.DataSource = dt;
                Session["TotalComplianceRows"] = dt.Rows.Count;
                grdCompliancePerformer.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //public void BindDataPerformer()
        //{
        //    try
        //    {
        //        Session["TotalComplianceRows"] = 0;

        //        int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //        int userid = AuthenticationHelper.UserID;

        //        string date = Convert.ToDateTime(Request.QueryString["date"]).ToString("yyyy-MM-dd");
        //        DataTable dt = null;
        //        if (!string.IsNullOrEmpty(Request.QueryString["m"]) && Request.QueryString["m"].ToString() == "8")
        //        {
        //            if (AuthenticationHelper.Role.Equals("MGMT"))
        //            {
        //                IsApprover = false;
        //            }
        //            else
        //            {
        //                using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //                {
        //                    var GetApprover = (entities.Sp_GetApproverUsers(AuthenticationHelper.UserID)).ToList();
        //                    if (GetApprover.Count > 0)
        //                    {
        //                        IsApprover = true;
        //                    }
        //                }
        //            }

        //            if (IsApprover == false)
        //            {
        //                dt = fetchDataManagement(customerid, userid);
        //            }
        //            else
        //            {
        //                dt = fetchDataApprover(customerid, userid);
        //            }
        //        }
        //        else
        //        {
        //            dt = fetchDataPerformer(customerid, userid);
        //        }
        //        grdCompliancePerformer.DataSource = dt;
        //        Session["TotalComplianceRows"] = dt.Rows.Count;
        //        grdCompliancePerformer.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        public void BindDataReviewer()
        {
            try
            {
                Session["TotalComplianceRowsReviewer"] = 0;
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int userid = AuthenticationHelper.UserID;                
                DataTable dt1 = fetchDataReviewer(customerid, userid);
                grdComplianceReviewer.DataSource = dt1;
                Session["TotalComplianceRowsReviewer"] = dt1.Rows.Count;
                grdComplianceReviewer.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        public DataTable fetchDataPerformer(int customerid, int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime dt = Business.DateTimeExtensions.GetDateCalender(date);
                string Type = Request.QueryString["Type"].ToString();
                string isallorsi = string.Empty;
                if (Type == "0")
                {
                    isallorsi = "SI";
                }
                else
                {
                    isallorsi = "All";
                }
                #region Statutory
                entities.Database.CommandTimeout = 180;
                var QueryStatutory = (from row in entities.ComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                      select row).ToList();
                if (QueryStatutory.Count > 0)
                {
                    QueryStatutory = QueryStatutory.Where(entry => entry.RoleID == 3 
                    && entry.PerformerScheduledOn.Value.Year == dt.Year 
                    && entry.PerformerScheduledOn.Value.Month == dt.Month
                    && entry.PerformerScheduledOn.Value.Day == dt.Day).ToList();

                    QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                #region Internal
                entities.Database.CommandTimeout = 180;
                var QueryInternal = (from row in entities.InternalComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                     select row).ToList();
                if (QueryInternal.Count > 0)
                {
                    QueryInternal = QueryInternal.Where(entry => entry.RoleID == 3 && entry.InternalScheduledOn.Year == dt.Year
                                              && entry.InternalScheduledOn.Month == dt.Month
                                              && entry.InternalScheduledOn.Day == dt.Day).ToList();

                    QueryInternal = QueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                var QueryStatutoryPerformer = (from row in QueryStatutory                                               
                                               select new
                                               {
                                                   Interimdays = row.Interimdays != null ? row.Interimdays.ToString() : "",
                                                   UserID = row.UserID,
                                                   ComplianceInstanceID = row.ComplianceInstanceID,
                                                   ScheduledOn = row.PerformerScheduledOn,
                                                   ScheduledOnID = row.ScheduledOnID,
                                                   ShortDescription = row.ShortDescription,
                                                   CustomerBranchID = row.CustomerBranchID,
                                                   ComplianceStatusID = row.ComplianceStatusID,                                                   
                                                   CType = row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based",
                                                   RoleID = row.RoleID,
                                                   Risk = row.Risk,
                                               }).ToList();



                var QueryInternalPerformer = (from row in QueryInternal                                             
                                              select new
                                              {
                                                  Interimdays = "",
                                                  UserID = row.UserID,
                                                  ComplianceInstanceID = row.InternalComplianceInstanceID,
                                                  ScheduledOn = row.InternalScheduledOn,
                                                  ScheduledOnID = row.InternalScheduledOnID,
                                                  ShortDescription = row.ShortDescription,
                                                  CustomerBranchID = row.CustomerBranchID,
                                                  ComplianceStatusID = row.InternalComplianceStatusID,
                                                  RoleID = row.RoleID,
                                                  CType = row.ComplianceType == 1 ? "Internal Checklist" : "Internal",
                                                  Risk = row.Risk,
                                              }).ToList();




                DataTable table = new DataTable();
                table.Columns.Add("Interimdays", typeof(string));
                table.Columns.Add("UserID", typeof(int));
                table.Columns.Add("ComplianceInstanceID", typeof(long));
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                table.Columns.Add("ShortDescription", typeof(string));
                table.Columns.Add("ComplianceStatusID", typeof(int));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("ScheduledOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(int));
                table.Columns.Add("CType", typeof(string));
                table.Columns.Add("Risk", typeof(int));
                if (QueryStatutoryPerformer.Count > 0)
                {
                    foreach (var item in QueryStatutoryPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;                        
                        table.Rows.Add(dr);
                    }
                }
                

                if (QueryInternalPerformer.Count > 0)
                {
                    foreach (var item in QueryInternalPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }
               
                return table;
            }
        }

        public DataTable fetchDataApprover(int customerid, int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime dt = Business.DateTimeExtensions.GetDateCalender(date);
                string Type = Request.QueryString["Type"].ToString();
                string isallorsi = string.Empty;
                if (Type == "0")
                {
                    isallorsi = "SI";
                }
                else
                {
                    isallorsi = "All";
                }
                #region Statutory
                entities.Database.CommandTimeout = 180;
                var QueryStatutory = (from row in entities.Sp_ComplianceInstanceTransactionApprover(customerid, userid, isallorsi)
                                      select row).ToList();
                if (QueryStatutory.Count > 0)
                {
                    QueryStatutory = QueryStatutory.Where(entry => entry.RoleID == 6
                    && entry.PerformerScheduledOn.Value.Year == dt.Year
                    && entry.PerformerScheduledOn.Value.Month == dt.Month
                    && entry.PerformerScheduledOn.Value.Day == dt.Day).ToList();

                    QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                #region Internal
                entities.Database.CommandTimeout = 180;
                var QueryInternal = (from row in entities.Sp_InternalComplianceInstanceTransactionApprover(customerid, userid, isallorsi)
                                     select row).ToList();
                if (QueryInternal.Count > 0)
                {
                    QueryInternal = QueryInternal.Where(entry => entry.RoleID == 6 && entry.InternalScheduledOn.Year == dt.Year
                                              && entry.InternalScheduledOn.Month == dt.Month
                                              && entry.InternalScheduledOn.Day == dt.Day).ToList();

                    QueryInternal = QueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                var QueryStatutoryPerformer = (from row in QueryStatutory
                                               select new
                                               {
                                                   Interimdays = row.Interimdays != null ? row.Interimdays.ToString() : "",
                                                   UserID = row.UserID,
                                                   ComplianceInstanceID = row.ComplianceInstanceID,
                                                   ScheduledOn = row.PerformerScheduledOn,
                                                   ScheduledOnID = row.ScheduledOnID,
                                                   ShortDescription = row.ShortDescription,
                                                   CustomerBranchID = row.CustomerBranchID,
                                                   ComplianceStatusID = row.ComplianceStatusID,
                                                   CType = row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based",
                                                   RoleID = row.RoleID,
                                                   Risk = row.Risk,
                                               }).ToList();



                var QueryInternalPerformer = (from row in QueryInternal
                                              select new
                                              {
                                                  Interimdays = "",
                                                  UserID = row.UserID,
                                                  ComplianceInstanceID = row.InternalComplianceInstanceID,
                                                  ScheduledOn = row.InternalScheduledOn,
                                                  ScheduledOnID = row.InternalScheduledOnID,
                                                  ShortDescription = row.ShortDescription,
                                                  CustomerBranchID = row.CustomerBranchID,
                                                  ComplianceStatusID = row.InternalComplianceStatusID,
                                                  RoleID = row.RoleID,
                                                  CType = row.ComplianceType == 1 ? "Internal Checklist" : "Internal",
                                                  Risk = row.Risk,
                                              }).ToList();




                DataTable table = new DataTable();
                table.Columns.Add("Interimdays", typeof(string));
                table.Columns.Add("UserID", typeof(int));
                table.Columns.Add("ComplianceInstanceID", typeof(long));
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                table.Columns.Add("ShortDescription", typeof(string));
                table.Columns.Add("ComplianceStatusID", typeof(int));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("ScheduledOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(int));
                table.Columns.Add("CType", typeof(string));
                table.Columns.Add("Risk", typeof(int));
                if (QueryStatutoryPerformer.Count > 0)
                {
                    foreach (var item in QueryStatutoryPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }


                if (QueryInternalPerformer.Count > 0)
                {
                    foreach (var item in QueryInternalPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }

                return table;
            }
        }

        public DataTable fetchDataManagement(int customerid, int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime dt = Business.DateTimeExtensions.GetDateCalender(date);
                string Type = Request.QueryString["Type"].ToString();
                string isallorsi = string.Empty;
                if (Type == "0")
                {
                    isallorsi = "SI";
                }
                else
                {
                    isallorsi = "All";
                }
                                
                #region Statutory
                entities.Database.CommandTimeout = 180;
                var mgmtQueryStatutory = (from row in entities.ComplianceInstanceTransactionViewCustomerWiseManagement(customerid, userid, isallorsi)
                                          select row).ToList();
                if (mgmtQueryStatutory.Count > 0)
                {
                    mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                #endregion

                #region Internal
                entities.Database.CommandTimeout = 180;
                var mgmtQueryInternal = (from row in entities.InternalComplianceInstanceTransactionmanagement(customerid, userid, isallorsi)
                                         select row).ToList();

                if (mgmtQueryInternal.Count > 0)
                {
                    mgmtQueryInternal = mgmtQueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                #endregion

               

                var QueryStatutoryPerformer = (from row in mgmtQueryStatutory
                                               where row.PerformerScheduledOn == dt 
                                               select new
                                               {
                                                   Interimdays = row.Interimdays != null ? row.Interimdays.ToString() : "",
                                                   UserID = row.UserID,
                                                   ComplianceInstanceID = row.ComplianceInstanceID,
                                                   ScheduledOn = row.PerformerScheduledOn,
                                                   ScheduledOnID = row.ScheduledOnID,
                                                   ShortDescription = row.ShortDescription,
                                                   CustomerBranchID = row.CustomerBranchID,
                                                   ComplianceStatusID = row.ComplianceStatusID,
                                                   CType = row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based",
                                                   RoleID = row.RoleID,
                                                   Risk = row.Risk,
                                               }).ToList();


                var QueryInternalPerformer = (from row in mgmtQueryInternal
                                              where row.InternalScheduledOn.Year == dt.Year
                                             && row.InternalScheduledOn.Month == dt.Month
                                             && row.InternalScheduledOn.Day == dt.Day 
                                              select new
                                              {
                                                  Interimdays = "",
                                                  UserID = row.UserID,
                                                  ComplianceInstanceID = row.InternalComplianceInstanceID,
                                                  ScheduledOn = row.InternalScheduledOn,
                                                  ScheduledOnID = row.InternalScheduledOnID,
                                                  ShortDescription = row.ShortDescription,
                                                  CustomerBranchID = row.CustomerBranchID,
                                                  ComplianceStatusID = row.InternalComplianceStatusID,
                                                  RoleID = row.RoleID,
                                                  CType = row.ComplianceType == 1 ? "Internal Checklist" : "Internal",
                                                  Risk = row.Risk,
                                              }).ToList();


                if (mgmtQueryStatutory.Count > 0)
                {
                    mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                DataTable table = new DataTable();
                table.Columns.Add("Interimdays", typeof(string));
                table.Columns.Add("UserID", typeof(int));
                table.Columns.Add("ComplianceInstanceID", typeof(long));
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                table.Columns.Add("ShortDescription", typeof(string));
                table.Columns.Add("ComplianceStatusID", typeof(int));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("ScheduledOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(int));
                table.Columns.Add("CType", typeof(string));
                table.Columns.Add("Risk", typeof(int));

                if (QueryStatutoryPerformer.Count > 0)
                {
                    foreach (var item in QueryStatutoryPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }
                if (QueryInternalPerformer.Count > 0)
                {
                    foreach (var item in QueryInternalPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }
                return table;
            }
        }
        public DataTable fetchDataReviewer(int customerid, int userid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DateTime dt = Business.DateTimeExtensions.GetDateCalender(date);
                string Type = Request.QueryString["Type"].ToString();
                string isallorsi = string.Empty;
                if (Type == "0")
                {
                    isallorsi = "SI";
                }
                else
                {
                    isallorsi = "All";
                }
                #region Statutory
                entities.Database.CommandTimeout = 180;
                var QueryStatutory = (from row in entities.ComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                      select row).ToList();  
                if (QueryStatutory.Count > 0)
                {
                    //QueryStatutory= QueryStatutory.Where(entry=>entry.RoleID == 4 && entry.ScheduledOn == dt).ToList();
                    QueryStatutory = QueryStatutory.Where(entry => entry.RoleID == 4
                    && entry.PerformerScheduledOn.Value.Year == dt.Year
                    && entry.PerformerScheduledOn.Value.Month == dt.Month
                    && entry.PerformerScheduledOn.Value.Day == dt.Day).ToList();

                    QueryStatutory = QueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                #region Internal
                entities.Database.CommandTimeout = 180;
                var QueryInternal = (from row in entities.InternalComplianceInstanceTransactionView_UserWise(customerid, userid, isallorsi)
                                     select row).ToList();
                if (QueryInternal.Count > 0)
                {
                    QueryInternal = QueryInternal.Where(entry => entry.RoleID == 4 && entry.InternalScheduledOn.Year == dt.Year
                                              && entry.InternalScheduledOn.Month == dt.Month
                                              && entry.InternalScheduledOn.Day == dt.Day).ToList();

                    QueryInternal = QueryInternal.GroupBy(entity => entity.InternalScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }

                #endregion

                var QueryStatutoryPerformer = (from row in QueryStatutory                                             
                                               select new
                                               {
                                                   Interimdays = row.Interimdays != null ? row.Interimdays.ToString() : "",
                                                   UserID = row.UserID,
                                                   ComplianceInstanceID = row.ComplianceInstanceID,
                                                   ScheduledOn = row.PerformerScheduledOn,
                                                   ScheduledOnID = row.ScheduledOnID,
                                                   ShortDescription = row.ShortDescription,
                                                   CustomerBranchID = row.CustomerBranchID,
                                                   ComplianceStatusID = row.ComplianceStatusID,
                                                   CType = row.ComplianceType == 1 ? "Statutory CheckList" : row.EventFlag == null ? "Statutory" : "Event Based",
                                                   RoleID = row.RoleID,
                                                   Risk = row.Risk,
                                               }).ToList();



                var QueryInternalPerformer = (from row in QueryInternal                                             
                                              select new
                                              {
                                                  Interimdays = "",
                                                  UserID = row.UserID,
                                                  ComplianceInstanceID = row.InternalComplianceInstanceID,
                                                  ScheduledOn = row.InternalScheduledOn,
                                                  ScheduledOnID = row.InternalScheduledOnID,
                                                  ShortDescription = row.ShortDescription,
                                                  CustomerBranchID = row.CustomerBranchID,
                                                  ComplianceStatusID = row.InternalComplianceStatusID,
                                                  RoleID = row.RoleID,
                                                  CType = row.ComplianceType == 1 ? "Internal Checklist" : "Internal",
                                                  Risk = row.Risk,
                                              }).ToList();

                DataTable table = new DataTable();
                table.Columns.Add("Interimdays", typeof(string));
                table.Columns.Add("UserID", typeof(int));
                table.Columns.Add("ComplianceInstanceID", typeof(long));
                table.Columns.Add("ScheduledOn", typeof(DateTime));
                table.Columns.Add("ShortDescription", typeof(string));
                table.Columns.Add("ComplianceStatusID", typeof(int));
                table.Columns.Add("RoleID", typeof(int));
                table.Columns.Add("ScheduledOnID", typeof(long));
                table.Columns.Add("CustomerBranchID", typeof(int));
                table.Columns.Add("CType", typeof(string));
                table.Columns.Add("Risk", typeof(int));
                if (QueryStatutoryPerformer.Count > 0)
                {
                    foreach (var item in QueryStatutoryPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }

                if (QueryInternalPerformer.Count > 0)
                {
                    foreach (var item in QueryInternalPerformer)
                    {
                        DataRow dr = table.NewRow();
                        dr["Interimdays"] = item.Interimdays;
                        dr["UserID"] = item.UserID;
                        dr["ComplianceInstanceID"] = item.ComplianceInstanceID;
                        dr["ShortDescription"] = item.ShortDescription;
                        dr["ScheduledOn"] = item.ScheduledOn;
                        dr["ComplianceStatusID"] = item.ComplianceStatusID;
                        dr["RoleID"] = item.RoleID;
                        dr["ScheduledOnID"] = item.ScheduledOnID;
                        dr["CustomerBranchID"] = item.CustomerBranchID;
                        dr["CType"] = item.CType;
                        dr["Risk"] = item.Risk;
                        table.Rows.Add(dr);
                    }
                }              
                return table;
            }
        }        
        protected void grdCompliancePerformer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ImageButton ImgBtnOverView = (ImageButton)e.Row.FindControl("lblOverView");
                    Image imtemplat = (Image)e.Row.FindControl("imtemplat");                    
                    Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledOn");
                    Label lblComplianceStatusID = (Label)e.Row.FindControl("lblComplianceStatusID");
                    if (lblComplianceStatusID.Text == "5")
                    {
                        imtemplat.ImageUrl = "~/Images/delayed.png"; //delayed
                        imtemplat.Attributes["data-original-title"] = "One or more compliances are completed after due date";                        
                    }
                    else if (lblComplianceStatusID.Text == "4")
                    {
                        imtemplat.ImageUrl = "~/Images/completed.png"; //in time
                        imtemplat.Attributes["data-original-title"] = "All compliances are completed";
                    }
                    else if (lblComplianceStatusID.Text == "7")
                    {
                        imtemplat.ImageUrl = "~/Images/completed.png"; //in time
                        imtemplat.Attributes["data-original-title"] = "All compliances are completed";
                    }
                    else if ((lblComplianceStatusID.Text != "4" || lblComplianceStatusID.Text != "5"))
                    {
                        DateTime scodate = Convert.ToDateTime(lblScheduledOn.Text);
                        if (scodate.Date < DateTime.Today.Date)
                        {
                            imtemplat.ImageUrl = "~/Images/overdue.png"; //overdue
                            imtemplat.Attributes["data-original-title"] = "One or more compliances are overdue";
                        }
                        else if (scodate.Date >= DateTime.Today.Date)
                        {
                            imtemplat.ImageUrl = "~/Images/upcoming.png";  //upcoming
                            imtemplat.Attributes["data-original-title"] = "Upcoming";
                        }
                    }
                    if (Overviewflag == "Managment")
                    {
                        ImgBtnOverView.Visible = true;
                    }                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdComplianceReviewer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Image imtemplat = (Image)e.Row.FindControl("imtemplat");                    
                    Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledOn");
                    Label lblComplianceStatusID = (Label)e.Row.FindControl("lblComplianceStatusID");
                    if (lblComplianceStatusID.Text == "5")
                    {
                        imtemplat.ImageUrl = "~/Images/delayed.png"; //delayed
                        imtemplat.Attributes["data-original-title"] = "One or more compliances are completed after due date";                        
                    }
                    else if (lblComplianceStatusID.Text == "4")
                    {
                        imtemplat.ImageUrl = "~/Images/completed.png"; //in time
                        imtemplat.Attributes["data-original-title"] = "All compliances are completed";
                    }
                    else if ((lblComplianceStatusID.Text != "4" || lblComplianceStatusID.Text != "5"))
                    {
                        DateTime scodate = Convert.ToDateTime(lblScheduledOn.Text);
                        if (scodate.Date < DateTime.Today.Date)
                        {
                            imtemplat.ImageUrl = "~/Images/overdue.png"; //overdue
                            imtemplat.Attributes["data-original-title"] = "One or more compliances are overdue";
                        }
                        else if (scodate.Date >= DateTime.Today.Date)
                        {
                            imtemplat.ImageUrl = "~/Images/upcoming.png";  //upcoming
                            imtemplat.Attributes["data-original-title"] = "Upcoming";
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        protected bool CanChangeStatus(long userID, int RoleID, int statusID,string Type)
        {
            try
            {
                bool result = false;
                if (RoleID == 3 && (Type == "Statutory" || Type == "Event Based"))
                {
                    result = CanChangePerformerStatus(userID, RoleID, statusID);
                }
                else if (RoleID == 4 && (Type == "Statutory" || Type == "Event Based"))
                {
                    result = CanChangeReviewerStatus(userID, RoleID, statusID);
                }
                else if (RoleID == 3 && Type == "Statutory CheckList")
                {
                    result = CanChangePerformerStatus(userID, RoleID, statusID);
                }
                else if (RoleID == 3 && Type == "Internal")
                {
                    result = CanChangeInternalPerformerStatus(userID, RoleID, statusID);
                }
                else if (RoleID == 4 && Type == "Internal")
                {
                    result = CanChangeInternalReviewerStatus(userID, RoleID, statusID);
                }
                else if (RoleID == 3 && Type == "Internal Checklist")
                {
                    result = CanChangeInternalPerformerStatus(userID, RoleID, statusID);
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }
        protected bool CanChangeReviewerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3 || statusID == 11 || statusID == 12;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }
        protected bool CanChangeInternalPerformerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1 || statusID == 10;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }
        protected bool CanChangeInternalReviewerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3 || statusID == 11;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }
        protected bool CanChangePerformerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1 || statusID == 6 || statusID == 10 || statusID == 12;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
            return false;
        }
  
    }
}