﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UsersSummaryCompanyAdmin.ascx.cs"
    Inherits="com.VirtuosoITech.ComplianceManagement.Portal.Controls.UsersSummaryCompanyAdmin" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<div style="float: left; clear: both; margin-top: 5px; margin-left: 50px">
    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Chart ID="chrtCustomers" runat="server" Width="500px" Height="500px">
                <Titles>
                    <asp:Title Text="Customers" Alignment="TopCenter" Font="Tahoma, 12pt, style=Bold" />
                </Titles>
                <Legends>
                    <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                        LegendStyle="Table" />
                </Legends>
                <Series>
                    <asp:Series ChartType="Doughnut" XValueMember="Name" YValueMembers="Quantity"  IsValueShownAsLabel="true">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Area3DStyle-Enable3D="false">
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
            <div style="margin-bottom: 7px; clear: both">
                <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                    Location:</label>
                <asp:DropDownList runat="server" ID="ddlLocation" Style="padding: 0px; margin: 0px;
                    height: 22px; width: 200px;" DataValueField="ID" DataTextField="Name" AutoPostBack="true"
                    OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
