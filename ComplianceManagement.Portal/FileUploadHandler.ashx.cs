﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    /// <summary>
    /// Summary description for FileUploadHandler
    /// </summary>
    public class FileUploadHandler : IHttpHandler
    {
        Boolean sendmailresult = false;
        public void ProcessRequest(HttpContext context)
        {

            string Recepient = context.Request.Form["txttomail"];
            int sender = Convert.ToInt32(context.Request.Form["Sender"]);
            string ListMail = System.Configuration.ConfigurationManager.AppSettings["ImpliMailList"];
            string[] AllmailList2 = new string[] { };
            if (!string.IsNullOrEmpty(Recepient))
            {
                AllmailList2 = ListMail.Split(',');
            }
            if (!string.IsNullOrEmpty(Recepient))
            {
                string[] arryval = Recepient.Split(',');//split values with ‘,’  
                int j = arryval.Length;
                foreach (string v in arryval)
                {
                    try
                    {
                        Email n = new Email();
                        n.Recepient = v;
                        n.Sender = sender;
                        n.Subjecttext = context.Request.Form["txtsub"];
                        n.MessageBody = context.Request.Form["txtmsgbody"];
                        n.CreatedDate = DateTime.Now;
                        var custdetails = UserManagement.GetByID(sender);
                        if (custdetails != null)
                        {
                            int? customerID = custdetails.CustomerID;
                            List<string> CustList = Business.ComplianceManagement.GetMailList(Convert.ToInt32(customerID));
                            if (CustList.Any(v.Contains) || ListMail.Contains("," + v + ","))
                            {
                                if (context.Request.Files.Count > 0)
                                {
                                    using (SmtpClient email = new SmtpClient())
                                    using (MailMessage mailMessage = new MailMessage())
                                    {
                                        email.DeliveryMethod = SmtpDeliveryMethod.Network;
                                        email.UseDefaultCredentials = false;
                                        NetworkCredential credential = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);
                                        email.Credentials = credential;
                                        email.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                                        email.Host = ConfigurationManager.AppSettings["MailServerDomain"];
                                        email.Timeout = 60000;
                                        email.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSslFlag"]);
                                        MailMessage oMessage = new MailMessage();
                                        string FromEmailId = ConfigurationManager.AppSettings["SenderEmailAddress"];
                                        mailMessage.From = new MailAddress(FromEmailId);
                                        mailMessage.Subject = context.Request.Form["txtsub"];
                                        mailMessage.Body = context.Request.Form["txtmsgbody"];
                                        mailMessage.IsBodyHtml = true;
                                        mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                                        mailMessage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ReplyEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"]));

                                        HttpFileCollection files = context.Request.Files;
                                        for (int i = 0; i < files.Count; i++)
                                        {
                                            HttpPostedFile file = files[i];
                                            string fname;
                                            if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                                            {
                                                string[] testfiles = file.FileName.Split(new char[] { '\\' });
                                                fname = testfiles[testfiles.Length - 1];
                                            }
                                            else
                                            {
                                                fname = file.FileName;
                                            }
                                            fname = Path.Combine(context.Server.MapPath("~/UploadedFile/"), fname);
                                            file.SaveAs(fname);
                                            Attachment data = new Attachment(fname);
                                            mailMessage.Attachments.Add(data);
                                            n.Attachment = fname;
                                        }
                                        mailMessage.To.Add(context.Request["txttomail"]);
                                        email.Send(mailMessage);
                                        sendmailresult = true;
                                    }
                                }
                                else
                                {

                                    using (SmtpClient email = new SmtpClient())
                                    using (MailMessage mailMessage = new MailMessage())
                                    {
                                        email.DeliveryMethod = SmtpDeliveryMethod.Network;
                                        email.UseDefaultCredentials = false;
                                        NetworkCredential credential = new NetworkCredential(ConfigurationManager.AppSettings["UserName"], ConfigurationManager.AppSettings["Password"]);
                                        email.Credentials = credential;
                                        email.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
                                        email.Host = ConfigurationManager.AppSettings["MailServerDomain"];
                                        email.Timeout = 60000;
                                        email.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSslFlag"]);
                                        MailMessage oMessage = new MailMessage();
                                        string FromEmailId = ConfigurationManager.AppSettings["SenderEmailAddress"];
                                        mailMessage.From = new MailAddress(FromEmailId);
                                        mailMessage.Subject = context.Request.Form["txtsub"];
                                        mailMessage.Body = context.Request.Form["txtmsgbody"];
                                        mailMessage.IsBodyHtml = true;
                                        mailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                                        mailMessage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings["ReplyEmailAddress"], ConfigurationManager.AppSettings["ReplyEmailAddressName"]));


                                        mailMessage.To.Add(context.Request["txttomail"]);
                                        email.Send(mailMessage);
                                        sendmailresult = true;
                                    }
                                }
                                com.VirtuosoITech.ComplianceManagement.Business.ComplianceManagement.Addemaildata(n);
                            }

                        }
                        context.Response.ContentType = "text/plain";
                    }
                    catch
                    {
                        sendmailresult = false;
                    }
                }
            }
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Write(sendmailresult.ToString());
            HttpContext.Current.Response.End();
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}