﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="CustomerList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Customers.CustomerList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>



    <script type="text/javascript">
        function fopenpopup() {
            $('#divCustomersDialog').modal('show');
        }
    </script>

    <script type="text/javascript">
        function initializeDatePicker(date) {
            var startDate = new Date();
            $("#<%= txtStartDate.ClientID %>").datepicker({
                dateFormat: 'dd-mm-yy',
                defaultDate: startDate,
                numberOfMonths: 1,
                minDate: startDate,
                onClose: function (startDate) {
                    $("#<%= txtEndDate.ClientID %>").datepicker("option", "minDate", startDate);
                }
            });

                $("#<%= txtEndDate.ClientID %>").datepicker({
                dateFormat: 'dd-mm-yy',
                defaultDate: startDate,
                numberOfMonths: 1,
                minDate: startDate,
                onClose: function (startDate) {
                    $("#<%= txtStartDate.ClientID %>").datepicker("option", "maxDate", startDate);
                }
                });


                if (date != null) {
                    $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date);
                    $("#<%= txtEndDate.ClientID %>").datepicker("option", "defaultDate", date);
                }
            }

            function initializeCombobox() {
                $("#<%= ddlCustomerStatus.ClientID %>").combobox();
              <%--  $("#<%= ddlComplianceApplicable.ClientID %>").combobox();--%>
            }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upCustomerList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                        <div style="margin-bottom: 4px"/> 
                    <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                    <div class="col-md-2 colpadding0">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                        <asp:ListItem Text="5" Selected="True" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                    <div class="col-md-2 colpadding0">
                        <p style="color: #999; margin-top: 5px;"> Filter : </p>
                    </div>
                     <asp:TextBox runat="server" ID="tbxFilter" class="form-control m-bot15" Style="Width:250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />

                    </div>
                    <div style="text-align:right">
                        <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" OnClientClick="fopenpopup()" ID="btnAddCustomer" OnClick="btnAddCustomer_Click" />
                    </div>                                       
                    <div style="margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            </div>
                <div style="margin-bottom: 4px">   
            <%--<asp:Panel ID="Panel1" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">--%>
                <asp:GridView runat="server" ID="grdCustomer" AutoGenerateColumns="false" OnRowCreated="grdCustomer_RowCreated" OnSorting="grdCustomer_Sorting"
                    DataKeyNames="ID" OnRowCommand="grdCustomer_RowCommand" OnPageIndexChanging="grdCustomer_PageIndexChanging"
                     PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" AllowSorting="true"
                    OnPreRender="grdCustomer_OnPreRender">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                         <ItemTemplate>
                                          <%#Container.DataItemIndex+1 %>
                         </ItemTemplate>
                         </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="60px">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkCompliancesHeader" Text="All" runat="server" onclick="javascript:SelectheaderCheckboxes(this)" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkCustomer" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Name" HeaderText="Name" />
                        <asp:BoundField DataField="BuyerName" HeaderText="Buyer Name" />
                        <asp:BoundField DataField="BuyerContactNumber" HeaderText="Buyer Contact" />
                        <asp:BoundField DataField="BuyerEmail" HeaderText="Buyer Email" SortExpression="BuyerEmail" />
                        <asp:BoundField DataField="Status" HeaderText="Status" />
                        <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# Eval("StartDate") != null ? ((DateTime)Eval("StartDate")).ToString("dd-MMM-yyyy")  : " "%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="End Date" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# Eval("EndDate") != null ? ((DateTime)Eval("EndDate")).ToString("dd-MMM-yyyy") : " "%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sub Unit">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" CommandName="VIEW_COMPANIES" CommandArgument='<%# Eval("ID") %>'>sub-entities</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Justify" HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_CUSTOMER" CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon_new.png" alt="Edit Customer" title="Edit Customer" /></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_CUSTOMER" CommandArgument='<%# Eval("ID") %>'
                                    OnClientClick="return confirm('This will also delete all the sub-entities associated with customer. Are you certain you want to delete this customer entry?');"><img src="../Images/delete_icon_new.png" alt="Disable Customer" title="Disable Customer" /></asp:LinkButton>
                            </ItemTemplate>                            
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />            
                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                      </PagerTemplate>
                    </asp:GridView>
                    </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0" style="margin-left: 765px;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>                                  
                                    <div class="table-paging-text">
                                        <p>
                                            <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                        </p>
                                    </div>
                                    <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
            <%--</asp:Panel>--%>
            <div style="margin-bottom: 7px; margin-left: 10px; margin-top: 10px; margin-right: 20px; text-align: right; float: right">
                <asp:Button Text="Notify Software Update" runat="server" ID="btnSendNotification" Width="100%" ToolTip="Click here to send server down notifications." OnClick="btnSendNotification_Click"
                    CssClass="btn btn-primary" ValidationGroup="ModifyAsignmentValidationGroup" CausesValidation="false" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divNotificationTime">
        <asp:UpdatePanel ID="upServerDownTimeDetails" runat="server" UpdateMode="Conditional" OnLoad="upServerDownTimeDetails_Load">
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="ComplianceCategoryValidationGroup1" />
                        <asp:CustomValidator ID="CustomValidator1" runat="server" EnableClientScript="False"
                            ValidationGroup="ComplianceCategoryValidationGroup1" Display="none" class="alert alert-block alert-danger fade in" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Date</label>
                        <asp:TextBox runat="server" ID="txtDate" CssClass="form-control" Style="width: 200px;" ReadOnly="true" MaxLength="200" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Date can not be empty." ControlToValidate="txtDate"
                            runat="server" ValidationGroup="ComplianceCategoryValidationGroup1" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Time
                        </label>
                        <asp:DropDownList runat="server" ID="ddlStartTime"></asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator1" ErrorMessage="Please select start time." ControlToValidate="ddlStartTime"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceCategoryValidationGroup1"
                            Display="None" />
                        To
                        <asp:DropDownList runat="server" ID="ddlEndTime"></asp:DropDownList>
                        <asp:CompareValidator ID="CompareValidator2" ErrorMessage="Please select end time." ControlToValidate="ddlEndTime"
                            runat="server" ValueToCompare="-1" Operator="NotEqual" ValidationGroup="ComplianceCategoryValidationGroup1"
                            Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; float: right; margin-right: 66px; margin-top: 10px;">
                        <asp:Button Text="Send" runat="server" ID="btnSend" CssClass="btn btn-primary" OnClick="btnSend_click"
                            ValidationGroup="ComplianceCategoryValidationGroup1" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" OnClientClick="$('#divNotificationTime').dialog('close');" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div class="modal fade" id="divCustomersDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 730px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="dvAuditorDialog">
                        <asp:UpdatePanel ID="upCustomers" runat="server" UpdateMode="Conditional" OnLoad="upCustomers_Load">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="CustomerValidationGroup" />
                                        <asp:CustomValidator ID="CustomValidator2" runat="server" EnableClientScript="False"
                                            ValidationGroup="CustomerValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                        <asp:Label runat="server" ID="lblErrorMassage" Style="color: Red"></asp:Label>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Name</label>
                                        <asp:TextBox runat="server" ID="tbxName" CssClass="form-control" Style="width: 250px;" MaxLength="50" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Name can not be empty."
                                            ControlToValidate="tbxName" runat="server" ValidationGroup="CustomerValidationGroup"
                                            Display="None" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                                            ErrorMessage="Please enter a valid name." ControlToValidate="tbxName" ValidationExpression="^[a-zA-Z_&]+[a-zA-Z0-9&_ .-]*$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Address</label>
                                        <asp:TextBox runat="server" ID="tbxAddress" CssClass="form-control" Style="width: 250px;" MaxLength="500"
                                            TextMode="MultiLine" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Buyer Name</label>
                                        <asp:TextBox runat="server" ID="tbxBuyerName" CssClass="form-control" Style="width: 250px;" MaxLength="50" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Buyer Name can not be empty."
                                            ControlToValidate="tbxBuyerName" runat="server" ValidationGroup="CustomerValidationGroup"
                                            Display="None" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Buyer Contact No</label>
                                        <asp:TextBox runat="server" ID="tbxBuyerContactNo" CssClass="form-control" Style="width: 250px;" MaxLength="15" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Buyer Contact Number can not be empty."
                                            ControlToValidate="tbxBuyerContactNo" runat="server" ValidationGroup="CustomerValidationGroup"
                                            Display="None" />
                                        <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                                            ErrorMessage="Please enter a valid contact number." ControlToValidate="tbxBuyerContactNo"
                                            ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Buyer Email</label>
                                        <asp:TextBox runat="server" ID="tbxBuyerEmail" CssClass="form-control" Style="width: 250px;" MaxLength="200" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Buyer Email can not be empty."
                                            ControlToValidate="tbxBuyerEmail" runat="server" ValidationGroup="CustomerValidationGroup"
                                            Display="None" />
                                        <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                                            ErrorMessage="Please enter a valid email." ControlToValidate="tbxBuyerEmail"
                                            ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Customer Status</label>
                                        <asp:DropDownList runat="server" ID="ddlCustomerStatus" class="form-control m-bot15" Style="Width:250px" MaxLength="50" />
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Start Date</label>
                                        <asp:TextBox runat="server" ID="txtStartDate" CssClass="form-control" Style="width: 250px;" MaxLength="200" ReadOnly="true"/>
                                    </div>
                                    <div style="margin-bottom: 7px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            End Date</label>
                                        <asp:TextBox runat="server" ID="txtEndDate" CssClass="form-control" Style="width: 250px;" MaxLength="200" ReadOnly="true" />
                                    </div>
                                    <div style="margin-bottom: 7px; float: right; margin-right: 257px; margin-top: 10px;">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="CustomerValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="Button1" CssClass="btn btn-primary" />
                                    </div>
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <script type="text/javascript">
         function fopenpopup() {
             $('#divNotificationTime').modal('show');
         }
    </script>

    <script type="text/javascript">
        //$(function () {
        //    $('#divNotificationTime').dialog({
        //        height: 300,
        //        width: 500,
        //        autoOpen: false,
        //        showOn: true,
        //        draggable: true,
        //        title: "Software Update Details",
        //        open: function (type, data) {
        //            $(this).parent().appendTo("form");
        //        }
        //    });
        //});

        function initializeDowntimeDatePicker(date) {
            var startDate = new Date();
            $('#<%= txtDate.ClientID %>').datepicker({
                                dateFormat: 'dd-mm-yy',
                                numberOfMonths: 1,
                                minDate: startDate
                            });

                            if (date != null) {
                                $("#<%= txtDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }

        function SelectheaderCheckboxes(headerchk) {
            var rolecolumn;

            var chkheaderid = headerchk.id.split("_");

            var gvcheck = document.getElementById("<%=grdCustomer.ClientID %>");
            var i;

            if (headerchk.checked) {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }

            else {
                for (i = 0; i < gvcheck.rows.length; i++) {
                    gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked = headerchk.checked;
                }
            }
        }

        function Selectchildcheckboxes(header) {
            var i;
            var count = 0;
            var rolecolumn;
            var gvcheck = document.getElementById("<%=grdCustomer.ClientID %>");
            var headerchk = document.getElementById(header);
            var chkheaderid = header.split("_");

            var rowcount = gvcheck.rows.length;

            for (i = 1; i < gvcheck.rows.length - 1; i++) {
                if (gvcheck.rows[i + 1].cells[0].getElementsByTagName("INPUT")[0].checked) {
                    count++;
                }
            }

            if (count == gvcheck.rows.length - 2) {
                headerchk.checked = true;
            }
            else {
                headerchk.checked = false;
            }
        }

    </script>
</asp:Content>
