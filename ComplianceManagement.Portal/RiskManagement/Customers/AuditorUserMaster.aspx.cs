﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Portal.Users;
using System.Configuration;


namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Customers
{
    public partial class AuditorUserMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Name";
                BindAuditor();               
                BindFilterAuditor();
                //GetPageDisplaySummary();
                BindAuditorList(Convert.ToInt32(ddlFilterAuditor.SelectedValue));
                ddlFilterAuditor_SelectedIndexChanged(sender, e);
            }
        }


        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdAuditor.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindAuditorList(Convert.ToInt32(ddlFilterAuditor.SelectedValue));

        }


        protected void ddlFilterAuditor_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAuditorList(Convert.ToInt32(ddlFilterAuditor.SelectedValue));
            bindPageNumber();
        }
        private void BindAuditor()
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                ddlAuditor.DataTextField = "Name";
                ddlAuditor.DataValueField = "ID";
                ddlAuditor.DataSource = ProcessManagement.GetAllAuditorMaster(customerID);
                ddlAuditor.DataBind();
                ddlAuditor.Items.Insert(0, new ListItem("Select Auditor", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindFilterAuditor()
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                ddlFilterAuditor.DataTextField = "Name";
                ddlFilterAuditor.DataValueField = "ID";
                ddlFilterAuditor.DataSource = ProcessManagement.GetAllAuditorMaster(customerID);
                ddlFilterAuditor.DataBind();
                ddlFilterAuditor.Items.Insert(0, new ListItem("Select Auditor", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void ddlAuditor_SelectedIndexChanged(object sender, EventArgs e)
        {            
        }
        protected void upPromotor_Load(object sender, EventArgs e)
        {
            try
            {                
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditor_RowDataBound(object sender, GridViewRowEventArgs e)
        {            
        }
        private void BindAuditorList(int AuditorID)
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var AuditorMasterList = ProcessManagement.GetAllAuditorTeamUser(customerID, AuditorID);
                grdAuditor.DataSource = AuditorMasterList;
                Session["TotalRows"] = AuditorMasterList.Count;
                grdAuditor.DataBind();
                upPromotorList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(ddlAuditor.SelectedValue))
                {
                    #region Auditor User
                    int AuditorId = -1;
                    if (!string.IsNullOrEmpty(ddlAuditor.SelectedValue))
                    {
                        if (ddlAuditor.SelectedValue !="-1")
                        {
                            AuditorId = Convert.ToInt32(ddlAuditor.SelectedValue); 
                        }                               
                    }                                                  
                    Mst_AuditorUserMaster mstauditorusermaster = new Mst_AuditorUserMaster()
                    {
                        AuditorId = AuditorId,
                        FirstName = txtFName.Text,
                        LastName = txtLName.Text,
                        Address = txtAddress.Text,
                        EmailID = txtEmail.Text,                      
                        ContactNo = txtContactNo.Text,
                        CreatedBy = null,
                        CreatedOn = DateTime.Now,
                        IsActive = false,                        
                    };                   
                    if (!String.IsNullOrEmpty(rblAuditRole.SelectedValue))
                    {
                        if (rblAuditRole.SelectedItem.Text == "Is Audit Head")
                        {
                            mstauditorusermaster.IsAuditHeadOrMgr = "AH";
                        }
                        else if (rblAuditRole.SelectedItem.Text == "Is Audit Manager")
                        {
                            mstauditorusermaster.IsAuditHeadOrMgr = "AM";
                        }
                    }
                    #endregion
                    #region Compliance User
                    User user = new User()
                    {
                        AuditorID = AuditorId,
                        FirstName = txtFName.Text,
                        LastName = txtLName.Text,
                        Designation = "External Auditor",
                        Email = txtEmail.Text,
                        ContactNumber = txtContactNo.Text,
                        Address = txtAddress.Text,
                        RoleID = 7,
                        CustomerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID),
                        IsHead = false,
                        EnType = "A"
                    };
                    List<UserParameterValue> parameters = new List<UserParameterValue>();
                    foreach (var item in repParameters.Items)
                    {
                        RepeaterItem entry = item as RepeaterItem;
                        TextBox tbxValue = ((TextBox)entry.FindControl("tbxValue"));
                        HiddenField hdnEntityParameterID = ((HiddenField)entry.FindControl("hdnEntityParameterID"));
                        HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
                        parameters.Add(new UserParameterValue()
                        {
                            ID = Convert.ToInt32(hdnID.Value),
                            UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                            Value = tbxValue.Text
                        });
                    }
                    if (!String.IsNullOrEmpty(rblAuditRole.SelectedValue))
                    {
                        if (rblAuditRole.SelectedItem.Text == "Is Audit Head")
                        {
                            user.IsAuditHeadOrMgr = "AH";
                        }
                        if (rblAuditRole.SelectedItem.Text == "Is Audit Manager")
                        {
                            user.IsAuditHeadOrMgr = "AM";
                        }
                    }
                    #endregion
                    #region Risk User
                    mst_User mstuser = new mst_User()
                    {
                        AuditorID = AuditorId,
                        FirstName = txtFName.Text,
                        LastName = txtLName.Text,
                        Designation = "External Auditor",
                        Email = txtEmail.Text,                        
                        ContactNumber = txtContactNo.Text,
                        Address = txtAddress.Text,
                        RoleID = 7,
                        CustomerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID),
                        IsHead = false,
                    };
                    List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk> parametersRisk = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk>();
                    foreach (var item in repParameters.Items)
                    {
                        RepeaterItem entry = item as RepeaterItem;
                        TextBox tbxValue = ((TextBox)entry.FindControl("tbxValue"));
                        HiddenField hdnEntityParameterID = ((HiddenField)entry.FindControl("hdnEntityParameterID"));
                        HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));
                        parameters.Add(new UserParameterValue()
                        {
                            ID = Convert.ToInt32(hdnID.Value),
                            UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                            Value = tbxValue.Text
                        });
                    }
                    if (!String.IsNullOrEmpty(rblAuditRole.SelectedValue))
                    {
                        if (rblAuditRole.SelectedItem.Text == "Is Audit Head")
                        {
                            mstuser.IsAuditHeadOrMgr = "AH";
                        }
                        else if (rblAuditRole.SelectedItem.Text == "Is Audit Manager")
                        {
                            mstuser.IsAuditHeadOrMgr = "AM";
                        }
                    }
                    #endregion

                    if ((int)ViewState["Mode"] == 1)
                    {
                        user.ID = Convert.ToInt32(ViewState["AuditorUserID"]);
                        mstuser.ID = Convert.ToInt32(ViewState["AuditorUserID"]);                        
                    }                                      
                    bool result = false;
                    if ((int)ViewState["Mode"] == 0)
                    {
                        bool emailExists;
                        UserManagement.Exists(user, out emailExists);
                        if (emailExists)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "User with same email already exists";
                            return;
                        }                        
                        UserManagementRisk.Exists(mstuser, out emailExists);
                        if (emailExists)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "User with same email already exists";
                            return;
                        }
                        if (ProcessManagement.AuditorTeamUserExists(mstauditorusermaster.EmailID, Convert.ToInt32(ViewState["AuditorUserID"])))
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "User with same email already exists";
                            return;
                        }
                        user.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        user.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;

                        string passwordText = Util.CreateRandomPassword(10);
                        user.Password = Util.CalculateMD5Hash(passwordText);
                        string message = SendNotificationEmail(user, passwordText);

                        mstuser.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                        mstuser.CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User;
                        mstuser.Password = Util.CalculateMD5Hash(passwordText);

                        result = UserManagement.Create(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                        result = UserManagementRisk.Create(mstuser, parametersRisk, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                        ProcessManagement.CreateAuditorUserMaster(mstauditorusermaster);
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        result = UserManagement.Update(user, parameters);
                        result = UserManagementRisk.Update(mstuser, parametersRisk);                        
                    }
                    if (result)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "User save successfully";
                    }                   
                    BindAuditorList(AuditorId);
                    bindPageNumber();
                    int count = Convert.ToInt32(GetTotalPagesCount());
                    if (count > 0)
                    {
                        int gridindex = grdAuditor.PageIndex;
                        string chkcindition = (gridindex + 1).ToString();
                        DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private string SendNotificationEmail(User user, string passwordText)
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@Password", passwordText)
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    ;
                return message;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {
            try
            {               
                ViewState["Mode"] = 0;
                txtFName.Text = string.Empty;
                txtLName.Text = string.Empty;
                txtAddress.Text = string.Empty;
                txtEmail.Text = string.Empty;
                txtContactNo.Text = string.Empty;
                rblAuditRole.ClearSelection();
                upPromotor.Update();
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAuditorDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        
        protected void grdAuditor_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var AuditorMasterList = ProcessManagement.GetAllAuditorUserMasterList(customerID,Convert.ToInt32(ddlAuditor.SelectedValue));
                if (direction == SortDirection.Ascending)
                {
                    AuditorMasterList = AuditorMasterList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    AuditorMasterList = AuditorMasterList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                foreach (DataControlField field in grdAuditor.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdAuditor.Columns.IndexOf(field);
                    }
                }
                grdAuditor.DataSource = AuditorMasterList;
                grdAuditor.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int AuditorUserID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_PROMOTER"))
                {
                    rblAuditRole.ClearSelection();
                    ViewState["Mode"] = 1;
                    ViewState["AuditorUserID"] = AuditorUserID;
                    int customerID = -1;
                    //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    mst_User mstusermaster = ProcessManagement.AuditorTeamUserGetByID(customerID,AuditorUserID);
                    ddlAuditor.SelectedValue = Convert.ToString(mstusermaster.AuditorID);
                    txtFName.Text = mstusermaster.FirstName;
                    txtLName.Text = mstusermaster.LastName;
                    txtAddress.Text = mstusermaster.Address;
                    txtEmail.Text = mstusermaster.Email;
                    txtContactNo.Text = mstusermaster.ContactNumber;

                    if (mstusermaster.IsAuditHeadOrMgr == "AH")
                        rblAuditRole.Items.FindByValue("IAH").Selected = true;

                    if (mstusermaster.IsAuditHeadOrMgr == "AM")
                        rblAuditRole.Items.FindByValue("IAM").Selected = true;

                    upPromotor.Update();
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAuditorDialog\").dialog('open')", true);
                }

                else if (e.CommandName.Equals("DELETE_PROMOTER"))
                {

                    if (UserManagementRisk.HasAuditsAssigned(AuditorUserID))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deleted. One or more Compliances are assigned to user, please re-assign to other user.');", true);
                    }
                    else if (UserManagementRisk.HasInternalControlAuditsAssigned(AuditorUserID))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deleted. One or more Compliances are assigned to user, please re-assign to other user.');", true);
                    }
                    else
                    {
                        int customerID = -1;
                        //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        ProcessManagement.DeleteAuditorTeamUser(customerID, AuditorUserID);
                        UserManagement.DeleteAuditorTeamUser(AuditorUserID);
                        if (!string.IsNullOrEmpty(ddlFilterAuditor.SelectedValue))
                        {
                            int a = Convert.ToInt32(ddlFilterAuditor.SelectedValue);
                            BindAuditorList(a);
                        }
                    }                                       
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdAuditor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                int AuditorId = -1;
                if (!String.IsNullOrEmpty(ddlFilterAuditor.SelectedValue))
                {
                    AuditorId = Convert.ToInt32(ddlFilterAuditor.SelectedValue);
                }               
                grdAuditor.PageIndex = e.NewPageIndex;
                BindAuditorList(AuditorId);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //protected void grdAuditor_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {
        //        int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
        //        if (sortColumnIndex != -1)
        //        {
        //            AddSortImage(sortColumnIndex, e.Row);
        //        }
        //    }
        //}
        //protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        //{
        //    System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
        //    sortImage.ImageAlign = ImageAlign.AbsMiddle;
        //    if (direction == SortDirection.Ascending)
        //    {
        //        sortImage.ImageUrl = "../../Images/SortAsc.gif";
        //        sortImage.AlternateText = "Ascending Order";
        //    }
        //    else
        //    {
        //        sortImage.ImageUrl = "../../Images/SortDesc.gif";
        //        sortImage.AlternateText = "Descending Order";
        //    }
        //    headerRow.Cells[columnIndex].Controls.Add(sortImage);
        //}
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //if (!IsValid()) { return; };

                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}
                //else
                //{

                //}
                //Reload the Grid
                BindAuditorList(Convert.ToInt32(ddlFilterAuditor.SelectedValue));
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdAuditor.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }


        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {

        //        }
        //        //Reload the Grid                
        //        BindAuditorList(Convert.ToInt32(ddlFilterAuditor.SelectedValue));
        //    }
        //    catch (Exception ex)
        //    {
        //        //ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdAuditor.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdAuditor.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {
        //        }
        //        //Reload the Grid
        //        BindAuditorList(Convert.ToInt32(ddlFilterAuditor.SelectedValue));
        //    }
        //    catch (Exception ex)
        //    {
        //        //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        //private bool IsValid()
        //{
        //    try
        //    {
        //        if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
        //        {
        //            SelectedPageNo.Text = "1";
        //            return false;
        //        }
        //        else if (!IsNumeric(SelectedPageNo.Text))
        //        {
        //            //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
        //            return false;
        //        }
        //        else
        //        {
        //            return true;
        //        }
        //    }
        //    catch (FormatException)
        //    {
        //        return false;
        //    }
        //}

    }
}