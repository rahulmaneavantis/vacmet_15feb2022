﻿<%@ Page Title="Risk Category" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" CodeBehind="RiskCategory.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Customers.RiskCategory" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls"  TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #progressbar .ui-progressbar-value {
            background-color: #ccc;
        }
    </style>
       <style type="text/css">
        .dd_chk_select {
            
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc!important;        
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family:Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }
         .chosen-results {  max-height: 75px !important
         }
        .chosen-container-single .chosen-single{background:none !important;border:1px solid #c7c7cc   !important;height: 32px  !important;padding: 3px 0px 0px 14px !important; font-family:'Roboto', sans-serif !important; }
        .modal-body label {
    padding: 2px;
}
    </style> 

    <script type="text/javascript">
        function fopenpopup() {
            $('#RiskCategory').modal('show');
        }
    </script>

    <script type="text/javascript">

        function initializeCombobox() {

        }

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        $(document).ready(function () {
            //setactivemenu('Risk Category');
            fhead('Risk Category');
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upProcessList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                 
                        <div style="margin-bottom: 4px"/> 
                            <div class="col-md-2 colpadding0 entrycount" style="margin-top: 5px;">
                    <div class="col-md-2 colpadding0"  style="width:23%">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px; float: left"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                        <asp:ListItem Text="5" Selected="True" />
                        <asp:ListItem Text="10" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>
                 <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;">                     
                       <div class="col-md-2 colpadding0" >
                        <p style="color: #999; margin-top: 5px;">Filter</p>
                    </div>                         
                     <asp:TextBox runat="server" ID="tbxFilter" MaxLength="50" AutoPostBack="true" CssClass="form-control"  Style="width: 150px;" 
                            OnTextChanged="tbxFilter_TextChanged" />
                 </div>
                <div style="text-align:right">
                     <asp:LinkButton Text="Add New" runat="server"   OnClientClick="fopenpopup()" ID="btnAddProcess" CssClass="btn btn-primary" OnClick="btnAddProcess_Click" Visible="false" />
                </div>                
             <div style="margin-bottom: 4px">  
                 &nbsp;          
                <asp:GridView runat="server" ID="grdRiskcategoryList" AutoGenerateColumns="false" OnRowDataBound="grdRiskcategoryList_RowDataBound"
                    AllowSorting="true" OnSorting="grdRiskcategoryList_Sorting"
                    PageSize="5" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" 
                    DataKeyNames="ID" OnRowCommand="grdRiskcategoryList_RowCommand" OnPageIndexChanging="grdRiskcategoryList_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr">
                         <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                         </ItemTemplate>
                      </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="Name" SortExpression="Name">--%>
                            <asp:TemplateField HeaderText="Name" >
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">
                                    <asp:Label ID="Label1" runat="server" data-toggle="tooltip" data-placement="top" Text='<%# Eval("Name") %>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 400px">                                   
                                    <asp:Label ID="lblApprover" runat="server" Text='<%# ShowProcessNonProcess((string)Eval("IsICRRisk")) %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtEdit" runat="server" OnClientClick="fopenpopup()" CommandName="EDIT_Category" CommandArgument='<%# Eval("ID") %>' data-toggle="tooltip" data-placement="top" ToolTip="Edit Category"><img src="../../Images/edit_icon_new.png"/></asp:LinkButton>
                                <asp:LinkButton ID="lbtDelete" runat="server" CommandName="DELETE_Category" CommandArgument='<%# Eval("ID") %>' Visible="false" data-toggle="tooltip" data-placement="top" ToolTip="Delete Category"
                                    OnClientClick="return confirm('Are you certain you want to delete this Category?');"><img src="../../Images/delete_icon_new.png"/></asp:LinkButton>
                            </ItemTemplate>                            
                        </asp:TemplateField>
                    </Columns>
                     <RowStyle CssClass="clsROWgrid" />
                     <HeaderStyle CssClass="clsheadergrid" /> 
                    <HeaderStyle BackColor="#ECF0F1" />
                      <PagerSettings Visible="false" />                   
                    <PagerTemplate>
                                       <%-- <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>--%>
                      </PagerTemplate> 
                      </asp:GridView>
                  <div style="float: right;">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                        class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                        </asp:DropDownListChosen>  
                    </div> 
                        </div>
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0"  style="float:right">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                    <%--<asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick ="lBPrevious_Click"/>--%>                                  
                                    <div class="table-paging-text" style="float: right;">
                                        <p>Page
                                         <%--   <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                            <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>--%>
                                        </p>
                                    </div>
                                    <%--<asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick ="lBNext_Click" />--%>                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>
                       </section>
                    </div>
                </div>
            </div>
            <%--</asp:Panel>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="RiskCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 85%">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div id="divRiskCategoryDialog">
                        <asp:UpdatePanel ID="upProcess" runat="server" UpdateMode="Conditional" OnLoad="upProcess_Load">
                            <ContentTemplate>
                                <div>
                                    <div style="margin-bottom: 7px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                            ValidationGroup="EventValidationGroup" />
                                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                            ValidationGroup="EventValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                    </div>
                                    <div style="margin-bottom: 5px">
                                        <asp:RadioButtonList runat="server" ID="rdRiskProcess" RepeatDirection="Horizontal" AutoPostBack="true" Style="margin-left: 10%" ForeColor="GrayText"
                                            RepeatLayout="Flow" OnSelectedIndexChanged="rdRiskProcess_SelectedIndexChanged">
                                            <asp:ListItem Text="Internal Financial Control" Value="IFC" Selected="True" />
                                            <asp:ListItem Text="Enterprise Risk Management" Value="Risk Management" />
                                        </asp:RadioButtonList>
                                    </div>
                                    <div style="margin-bottom: 5px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Name</label>
                                        <asp:TextBox runat="server" ID="tbxName" autocomplete="off" Style="width: 50%;" CssClass="form-control" MaxLength="100" ToolTip="Name" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty." ControlToValidate="tbxName"
                                            runat="server" ValidationGroup="EventValidationGroup" Display="None" />
                                    </div>
                                    <div style="margin-bottom: 5px">
                                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                                            Description</label>
                                        <asp:TextBox runat="server" ID="txtDescription" CssClass="form-control" Style="height: 50px; width: 50%;" MaxLength="100" ToolTip="" TextMode="MultiLine" />
                                    </div>
                                    <div style="margin-bottom: 7px; margin-left: 42%; margin-top: 10px">
                                        <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="btn btn-primary"
                                            ValidationGroup="EventValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" />
                                    </div>
                                    <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
                                    </div>
                                    <div class="clearfix" style="height: 50px">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
