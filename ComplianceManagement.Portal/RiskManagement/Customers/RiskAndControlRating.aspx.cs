﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Customers
{
    public partial class RiskAndControlRating : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Value";
                BindRiskCategory("", "A");
                //GetPageDisplaySummary();
                bindPageNumber();
                btnAddProcess.Visible = true;
                rdRiskProcess_SelectedIndexChanged(null, null);
                
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdRiskcategoryList.PageIndex = chkSelectedPage - 1;
                        
            grdRiskcategoryList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            
            BindRiskCategory("", "A");

        }

        private void BindRiskCategory(string Rating, string FlagAll)
        {
            try
            {
                var CategoryList = ProcessManagement.GetAllRiskControlRating(Rating, FlagAll);

                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "Value")
                    {
                        CategoryList = CategoryList.OrderBy(entry => entry.Value).ToList();
                    }

                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "Value")
                    {
                        CategoryList = CategoryList.OrderByDescending(entry => entry.Value).ToList();
                    }
                    direction = SortDirection.Ascending;
                }
                grdRiskcategoryList.DataSource = CategoryList;
                Session["TotalRows"] = CategoryList.Count;
                grdRiskcategoryList.DataBind();
                upProcessList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string ShowProcessNonProcess(string flag)
        {
            string processnonprocess = "";
            string cc = ProcessManagement.Get_Risk_ControlRating(flag);
            if (cc == "R")
            {
                processnonprocess = "Risk Rating";
            }
            else
            {
                processnonprocess = "Control Rating";
            }
            return processnonprocess;
        }
        protected void grdRiskcategoryList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int CategoryID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_Category"))
                {
                    ViewState["Mode"] = 1;
                    ViewState["CategoryID"] = CategoryID;
                    string IsICRRisk = "";
                    mst_Risk_ControlRating eventData = ProcessManagement.RiskControlRatingGetByID(CategoryID);
                    tbxName.Text = eventData.Name;
                    tbxName.ToolTip = eventData.Name;
                    tbxrating.Text = Convert.ToString(eventData.Value);        
                    IsICRRisk = eventData.IsRiskControl;
                    if (IsICRRisk == "R")
                    {
                        rdRiskProcess.SelectedIndex = 0;
                    }
                    else
                    {
                        rdRiskProcess.SelectedIndex = 1;
                    }                  
                    upProcess.Update();
                 

                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divRiskCategoryDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_Category"))
                {
                    ProcessManagement.RiskControlRatingDelete(CategoryID);

                    string IsICRRisk = "";
                    mst_Risk_ControlRating eventData = ProcessManagement.RiskControlRatingGetByIDFetchDetail(CategoryID);                    
                    IsICRRisk = eventData.IsRiskControl;
                    if (IsICRRisk == "R")
                    {
                        BindRiskCategory("R", "R");
                    }
                    else if (IsICRRisk == "C")
                    {
                        BindRiskCategory("C", "C");
                    }
                    else
                    {
                        BindRiskCategory("A", "A");
                    }
                }          
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdRiskcategoryList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdRiskcategoryList.PageIndex = e.NewPageIndex;

                List<mst_Risk_ControlRating> CategoryList = new List<mst_Risk_ControlRating>();
                if (ddlFilterCategory.SelectedItem.Text == "Risk Rating")
                {
                    BindRiskCategory("R", "R");                    
                }
                else if (ddlFilterCategory.SelectedItem.Text == "Control Rating")
                {
                    BindRiskCategory("C", "C");                    
                }
                else
                {
                    BindRiskCategory("A", "A");                  
                }                                 
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdRiskcategoryList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                List<mst_Risk_ControlRating> CategoryList = new List<mst_Risk_ControlRating>();
                if (ddlFilterCategory.SelectedItem.Text == "Risk Rating")
                {
                    CategoryList = ProcessManagement.GetAllRiskControlRating("R", "R");
                   
                }
                else if (ddlFilterCategory.SelectedItem.Text == "Control Rating")
                {
                    CategoryList = ProcessManagement.GetAllRiskControlRating("C", "C");
                }
                else
                {
                    CategoryList = ProcessManagement.GetAllRiskControlRating("A", "A");
                }
                 
                if (direction == SortDirection.Ascending)
                {
                    CategoryList = CategoryList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    CategoryList = CategoryList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }

                foreach (DataControlField field in grdRiskcategoryList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdRiskcategoryList.Columns.IndexOf(field);
                    }
                }

                grdRiskcategoryList.DataSource = CategoryList;
                grdRiskcategoryList.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }        

        protected void grdRiskcategoryList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.Footer || e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role.Equals("SADMN"))
                //    {
                //        e.Row.Cells[6].Visible = true;
                //    }
                //    else
                //    {
                //        e.Row.Cells[6].Visible = false;
                //    }
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void btnAddProcess_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                tbxName.Text = string.Empty;
                tbxrating.Text = string.Empty;  
                upProcess.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divRiskCategoryDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

       

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string IsICRRisk = "";               
                if (rdRiskProcess.SelectedItem.Text == "Risk Rating")
                {
                    IsICRRisk = "R";
                   
                }
                else
                {
                    IsICRRisk = "C";
                }
               
                mst_Risk_ControlRating CategoryData = new mst_Risk_ControlRating()
                {
                    Name = tbxName.Text,                   
                    IsActive = false,
                    IsRiskControl = IsICRRisk,
                    Value=Convert.ToInt32(tbxrating.Text)
                };

                if ((int)ViewState["Mode"] == 1)
                {
                    CategoryData.Id = Convert.ToInt32(ViewState["CategoryID"]);
                }

                if ((int)ViewState["Mode"] == 0)
                {
                    if (ProcessManagement.RiskControlRatingExists(CategoryData.Name, IsICRRisk))
                    {
                        cvDuplicateEntry.ErrorMessage = "Category name already exists.";
                        cvDuplicateEntry.IsValid = false;
                        return;
                    }
                    else {
                        ProcessManagement.RiskControlRatingCreate(CategoryData);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Rating Details Add Succssfully";
                    }
  
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    ProcessManagement.RiskControlRatingUpdate(CategoryData);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Rating Details Update Succssfully";
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divRiskCategoryDialog\").dialog('close')", true);
                if (IsICRRisk == "R")
                {
                    BindRiskCategory("R","R");
                }
                else if (IsICRRisk == "R")
                {
                    BindRiskCategory("C", "C");
                }
                else
                {
                    BindRiskCategory("A", "A");
                }
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdRiskcategoryList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upProcess_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlFilterCategory.SelectedItem.Text == "Risk Rating")
                {
                    BindRiskCategory("R", "R");
                }
                else if (ddlFilterCategory.SelectedItem.Text == "Control Rating")                
                {
                    BindRiskCategory("C", "C");
                }
                else
                {
                    BindRiskCategory("A", "A");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void rdRiskProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (rdRiskProcess.SelectedItem.Text == "IFC")
            //{
            //    BindProcess("P");

            //}
            //else
            //{
            //    BindProcess("N");
            //}
        }
        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
        }


        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdRiskcategoryList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                ////if (!IsValid()) { return; };

                //SelectedPageNo.Text = "1";
                //int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();
                //    grdRiskcategoryList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                //    grdRiskcategoryList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                //}
                //else
                //{

                //}
                //Reload the Grid
                BindRiskCategory("", "A");
                //GetPageDisplaySummary();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdRiskcategoryList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }


        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
        //            grdRiskcategoryList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdRiskcategoryList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {

        //        }
        //        //Reload the Grid
        //        BindRiskCategory("", "A");
        //    }
        //    catch (Exception ex)
        //    {
        //        //ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();
        //            grdRiskcategoryList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //            grdRiskcategoryList.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //        }
        //        else
        //        {
        //        }
        //        //Reload the Grid
        //        BindRiskCategory("", "A");
        //    }
        //    catch (Exception ex)
        //    {
        //        //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
        //    }
        //}

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
        //private bool IsValid()
        //{
        //    try
        //    {
        //        if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
        //        {
        //            SelectedPageNo.Text = "1";
        //            return false;
        //        }
        //        else if (!IsNumeric(SelectedPageNo.Text))
        //        {
        //            //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
        //            return false;
        //        }
        //        else
        //        {
        //            return true;
        //        }
        //    }
        //    catch (FormatException)
        //    {
        //        return false;
        //    }
        //}
    }
}