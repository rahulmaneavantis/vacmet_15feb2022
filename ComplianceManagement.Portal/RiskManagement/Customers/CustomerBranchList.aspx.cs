﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Customers
{
    public partial class CustomerBranchList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["CustomerID"] = Session["CustomerID"];
                    ViewState["ParentID"] = null;

                    BindCustomerBranches();
                    BindCustomerStatus();
                    BindIndustry();
                    BindStates();
                  
                    BindLegalEntityType();
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";

                }
            }
        }

        private void BindLegalRelationShips(bool parent)
        {
            try
            {
                ddlLegalRelationShip.DataTextField = "Name";
                ddlLegalRelationShip.DataValueField = "ID";

                var legalRelationShips = Enumerations.GetAll<LegalRelationship>();
                if (!parent)
                {
                    legalRelationShips.RemoveAt(0);
                }
                ddlLegalRelationShip.DataSource = legalRelationShips;
                ddlLegalRelationShip.DataBind();

                ddlLegalRelationShip.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchTypes()
        {
            try
            {
                //ddlType.DataSource = null;
                //ddlType.DataBind();
                //ddlType.ClearSelection();

                //ddlType.DataTextField = "Name";
                //ddlType.DataValueField = "ID";

                //var dataSource = CustomerBranchManagement.GetAllNodeTypes();
                //if (ViewState["ParentID"] == null)
                //{
                //    dataSource = dataSource.Where(entry => entry.ID == 1).ToList();
                //}
                //ddlType.DataSource = dataSource;

                //ddlType.DataBind();
                //ddlType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindIndustry()
        {
            try
            {
                //ddlIndustry.DataTextField = "Name";
                //ddlIndustry.DataValueField = "ID";

                //ddlIndustry.DataSource = CustomerBranchManagement.GetAllIndustry();
                //ddlIndustry.DataBind();
                //ddlIndustry.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void dlBreadcrumb_ItemCommand(object source, DataListCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ITEM_CLICKED")
                {
                    if (e.Item.ItemIndex == 0)
                    {
                        ViewState["ParentID"] = null;
                    }
                    else
                    {
                        ViewState["ParentID"] = e.CommandArgument.ToString();
                    }
                    BindCustomerBranches();
                    upCustomerBranchList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                long parentID = -1;
                ddlLegalRelationShipOrStatus.Items.Clear();
                if (ViewState["ParentID"] != null)
                {
                    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                    BindLegalRelationShipOrStatus(Convert.ToInt32(ViewState["IndustryId"].ToString()));
                }
                else
                {
                    ViewState["IndustryId"] = null;
                }

                //dlBreadcrumb.DataSource = CustomerBranchManagement.GetHierarchy(Convert.ToInt32(ViewState["CustomerID"]), parentID);
                //dlBreadcrumb.DataBind();

                //grdCustomerBranch.DataSource = CustomerBranchManagement.GetAll(Convert.ToInt32(ViewState["CustomerID"]), parentID, tbxFilter.Text);
                //grdCustomerBranch.DataBind();
                //upCustomerBranchList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divLegalRelationship.Visible = divIndustry.Visible = ddlType.SelectedValue == "1";
                string industryId = string.Empty;

                if (ViewState["IndustryId"] != null)
                {
                    BindLegalRelationShipOrStatus(Convert.ToInt32(ViewState["IndustryId"].ToString()));
                    ddlIndustry.SelectedValue = ViewState["IndustryId"].ToString();
                }
                if (!divIndustry.Visible)
                {
                    ddlIndustry.SelectedIndex = -1;
                    if (ViewState["IndustryId"] == null)
                        BindLegalRelationShipOrStatus(0);
                }
                if (!divLegalRelationship.Visible)
                {
                    ddlLegalRelationShip.SelectedIndex = -1;
                }
                if (ddlType.SelectedValue == "1")
                {
                    divLegalEntityType.Visible = true;
                }
                else
                {
                    divLegalEntityType.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlIndustry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindLegalRelationShipOrStatus(Convert.ToInt32(ddlIndustry.SelectedValue));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindLegalRelationShipOrStatus(int industryId)
        {
            try
            {
                //ddlLegalRelationShipOrStatus.DataSource = null;
                //ddlLegalRelationShipOrStatus.DataBind();
                //ddlLegalRelationShipOrStatus.ClearSelection();

                //ddlLegalRelationShipOrStatus.DataTextField = "Name";
                //ddlLegalRelationShipOrStatus.DataValueField = "ID";


                //ddlLegalRelationShipOrStatus.DataSource = CustomerBranchManagement.GetAllLegalStatus(industryId, ddlType.SelectedValue.Length > 0 ? Convert.ToInt32(ddlType.SelectedValue) : -1);
                //ddlLegalRelationShipOrStatus.DataBind();

                //ddlLegalRelationShipOrStatus.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCities()
        {
            try
            {
                //ddlCity.DataSource = null;
                //ddlCity.DataBind();
                //ddlCity.ClearSelection();

                //ddlCity.DataTextField = "Name";
                //ddlCity.DataValueField = "ID";

                //ddlCity.DataSource = AddressManagement.GetAllCitiesByState(Convert.ToInt32(ddlState.SelectedValue));
                //ddlCity.DataBind();

                //ddlCity.Items.Insert(0, new ListItem("< Other >", "0"));
                //ddlCity.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindStates()
        {
            try
            {
                //ddlState.DataTextField = "Name";
                //ddlState.DataValueField = "ID";

                //ddlState.DataSource = AddressManagement.GetAllStates();
                //ddlState.DataBind();

                //ddlState.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindCities();
            ddlCity.SelectedValue = "-1";
        }

        protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCity.SelectedValue == "0" && divOther.Visible == false)
                {
                    divOther.Visible = true;
                    tbxOther.Text = string.Empty;
                }
                else if (ddlCity.SelectedValue != "0")
                {
                    divOther.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomerBranch_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerBranchID = Convert.ToInt32(e.CommandArgument);

                if (e.CommandName.Equals("EDIT_CUSTOMER_BRANCH"))
                {
                    //ViewState["Mode"] = 1;
                    //ViewState["CustomerBranchID"] = customerBranchID;

                    //mst_CustomerBranch customerBranch = CustomerBranchManagement.GetByID(customerBranchID);

                    //PopulateInputForm();

                    //tbxName.Text = customerBranch.Name;
                    //ddlType.SelectedValue = customerBranch.Type.ToString();
                    //ddlType_SelectedIndexChanged(null, null);

                    //ddlLegalRelationShip.SelectedValue = (customerBranch.LegalRelationShipID ?? -1).ToString();

                    //ddlLegalEntityType.SelectedValue = (customerBranch.LegalEntityTypeID ?? -1).ToString();

                    //ddlIndustry.SelectedValue = customerBranch.Industry.ToString();
                    //ddlIndustry_SelectedIndexChanged(null, null);

                    //if (customerBranch.Industry == -1 || string.IsNullOrEmpty(customerBranch.Industry.ToString()))
                    //{
                    //    int industryID = -1;
                    //    if (ViewState["IndustryId"] == null)
                    //    {
                    //        if (customerBranch.ParentID != null)//Added by Rahul on 9 FEB 2016
                    //        {
                    //            industryID = Convert.ToInt32(CustomerBranchManagement.GetByID(Convert.ToInt32(customerBranch.ParentID)).Industry);

                    //        }

                    //    }
                    //    else
                    //    {
                    //        industryID = Convert.ToInt32(ViewState["IndustryId"]);
                    //    }
                    //    BindLegalRelationShipOrStatus(industryID);
                    //}

                   
                    //ddlLegalRelationShipOrStatus.SelectedValue = customerBranch.LegalRelationShipOrStatus != 0 ? customerBranch.LegalRelationShipOrStatus.ToString() : "-1";
                    //tbxAddressLine1.Text = customerBranch.AddressLine1;
                    //tbxAddressLine2.Text = customerBranch.AddressLine2;

                    //ddlState.SelectedValue = customerBranch.StateID.ToString();
                    //ddlState_SelectedIndexChanged(null, null);

                    //ddlCity.SelectedValue = customerBranch.CityID.ToString();
                    //ddlCity_SelectedIndexChanged(null, null);

                    //tbxOther.Text = customerBranch.Others;
                    //tbxPinCode.Text = customerBranch.PinCode;

                    ////tbxIndustry.Text = customerBranch.Industry;
                    //tbxContactPerson.Text = customerBranch.ContactPerson;
                    //tbxLandline.Text = customerBranch.Landline;
                    //tbxMobile.Text = customerBranch.Mobile;
                    //tbxEmail.Text = customerBranch.EmailID;
                    //if (customerBranch.Status != null)
                    //{
                    //    ddlCustomerStatus.SelectedValue = Convert.ToString(customerBranch.Status);
                    //}

                    //ddlIndustry.Enabled = false;
                    //ddlType.Enabled = false;
                    //ddlLegalRelationShipOrStatus.Enabled = false;
                    //ddlLegalRelationShip.Enabled = false;

                    //upCustomerBranches.Update();
                    //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divCustomerBranchesDialog\").dialog('open')", true);
                }
                else if (e.CommandName.Equals("DELETE_CUSTOMER_BRANCH"))
                {
                    //CustomerBranchManagement.Delete(customerBranchID);
                    //BindCustomerBranches();
                }
                else if (e.CommandName.Equals("VIEW_CHILDREN"))
                {
                    try
                    {

                        //ViewState["ParentID"] = customerBranchID;
                        //if (ViewState["IndustryId"] == null)
                        //{
                        //    ViewState["IndustryId"] = CustomerBranchManagement.GetIndustryIDByCustomerId(customerBranchID);
                        //}

                        //BindCustomerBranches();
                      
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomerBranch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdCustomerBranch.PageIndex = e.NewPageIndex;
                BindCustomerBranches();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddCustomerBranch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;

                ddlIndustry.Enabled = true;
                ddlType.Enabled = true;
                ddlLegalRelationShipOrStatus.Enabled = true;
                ddlLegalRelationShip.Enabled = true;

                tbxName.Text = tbxAddressLine1.Text = tbxAddressLine2.Text = tbxOther.Text = tbxPinCode.Text = tbxContactPerson.Text = tbxLandline.Text = tbxMobile.Text = tbxEmail.Text = string.Empty;
                PopulateInputForm();

                ddlIndustry.SelectedValue = "-1";
                ddlType.SelectedValue = "-1";
                ddlType_SelectedIndexChanged(null, null);


                ddlState.SelectedValue = "-1";
                ddlState_SelectedIndexChanged(null, null);
                ddlCity_SelectedIndexChanged(null, null);

                ddlCustomerStatus.SelectedIndex = -1;

                upCustomerBranches.Update();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divCustomerBranchesDialog\").dialog('open')", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void PopulateInputForm()
        {
            try
            {
                //BindBranchTypes();
                //BindLegalRelationShips(ViewState["ParentID"] == null);
                //divParent.Visible = ViewState["ParentID"] != null;

                //litCustomer.Text = CustomerManagement.GetByID(Convert.ToInt32(ViewState["CustomerID"])).Name;

                //if (ViewState["ParentID"] != null)
                //{
                //    litParent.Text = CustomerBranchManagement.GetByID(Convert.ToInt32(ViewState["ParentID"])).Name;
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdCustomerBranch.PageIndex = 0;
                BindCustomerBranches();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                //mst_CustomerBranch customerBranch = new mst_CustomerBranch()
                //{
                //    Name = tbxName.Text,
                //    Type = Convert.ToByte(ddlType.SelectedValue),
                //    AddressLine1 = tbxAddressLine1.Text,
                //    AddressLine2 = tbxAddressLine2.Text,
                //    StateID = Convert.ToInt32(ddlState.SelectedValue),
                //    CityID = Convert.ToInt32(ddlCity.SelectedValue),
                //    Others = tbxOther.Text,
                //    PinCode = tbxPinCode.Text,
                //    Industry = Convert.ToInt32(ddlIndustry.SelectedValue),
                //    ContactPerson = tbxContactPerson.Text,
                //    Landline = tbxLandline.Text,
                //    Mobile = tbxMobile.Text,
                //    EmailID = tbxEmail.Text,
                //    CustomerID = Convert.ToInt32(ViewState["CustomerID"]),
                //    ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                //    Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                //};

                //if (ddlType.SelectedValue == "1")
                //{
                //    customerBranch.LegalRelationShipID = Convert.ToInt32(ddlLegalRelationShip.SelectedValue);
                //    customerBranch.LegalEntityTypeID = Convert.ToInt32(ddlLegalEntityType.SelectedValue);

                //}
                //else
                //{
                //    customerBranch.LegalRelationShipID = null;
                //    customerBranch.LegalEntityTypeID = null;
                //}

                //if (ddlLegalRelationShipOrStatus.SelectedValue != "-1")
                //    customerBranch.LegalRelationShipOrStatus = Convert.ToByte(ddlLegalRelationShipOrStatus.SelectedValue);
                //else
                //    customerBranch.LegalRelationShipOrStatus = 0;

                //if ((int)ViewState["Mode"] == 1)
                //{
                //    customerBranch.ID = Convert.ToInt32(ViewState["CustomerBranchID"]);
                //}

                //if (CustomerBranchManagement.Exists(customerBranch, Convert.ToInt32(ViewState["CustomerID"])))
                //{
                //    cvDuplicateEntry.ErrorMessage = "Customer branch name already exists.";
                //    cvDuplicateEntry.IsValid = false;
                //    return;
                //}

                //if ((int)ViewState["Mode"] == 0)
                //{
                //    CustomerBranchManagement.Create(customerBranch);
                //}
                //else if ((int)ViewState["Mode"] == 1)
                //{
                //    CustomerBranchManagement.Update(customerBranch);
                //}

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divCustomerBranchesDialog\").dialog('close')", true);
                BindCustomerBranches();
                upCustomerBranches.Update();
              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upCustomerBranches_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }



        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdCustomerBranch_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

                //long parentID = -1;
                //if (ViewState["ParentID"] != null)
                //{
                //    long.TryParse(ViewState["ParentID"].ToString(), out parentID);
                //}

                //var customerBranchList = CustomerBranchManagement.GetAll(Convert.ToInt32(ViewState["CustomerID"]), parentID, tbxFilter.Text);
                //if (direction == SortDirection.Ascending)
                //{
                //    customerBranchList = customerBranchList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //    direction = SortDirection.Descending;
                //}
                //else
                //{
                //    customerBranchList = customerBranchList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                //    direction = SortDirection.Ascending;
                //}


                //foreach (DataControlField field in grdCustomerBranch.Columns)
                //{
                //    if (field.SortExpression == e.SortExpression)
                //    {
                //        ViewState["SortIndex"] = grdCustomerBranch.Columns.IndexOf(field);
                //    }
                //}

                //grdCustomerBranch.DataSource = customerBranchList;
                //grdCustomerBranch.DataBind();


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdCustomerBranch_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        public void BindCustomerStatus()
        {
            try
            {

                ddlCustomerStatus.DataTextField = "Name";
                ddlCustomerStatus.DataValueField = "ID";

                ddlCustomerStatus.DataSource = Enumerations.GetAll<CustomerStatus>();
                ddlCustomerStatus.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        //added by Manisha
        private void BindLegalEntityType()
        {
            try
            {

                //ddlLegalEntityType.DataTextField = "EntityTypeName";
                //ddlLegalEntityType.DataValueField = "ID";
                //ddlLegalEntityType.DataSource = CustomerBranchManagement.GetAllLegalEntityType();
                //ddlLegalEntityType.DataBind();
                //ddlLegalEntityType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }


    }
}