﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Controls;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Saplin.Controls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DropDownListChosen;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class RiskControlMatrix : System.Web.UI.Page
    {
        public static List<int> IndustryIdProcess = new List<int>();
        public static List<int> RiskIdProcess = new List<int>();
        public static List<int> AssertionsIdProcess = new List<int>();
        public static List<long> Branchlist = new List<long>();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Session["TotalRows"] = null;
                BindLegalEntityData();
                BindLocationType();
                BindProcess("P");
                BindGridData();
                bindPageNumber();
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                DropDownListPageNo.DataBind();
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdRiskActivityMatrix.PageIndex = chkSelectedPage - 1;

            //SelectedPageNo.Text = (chkSelectedPage).ToString();
            grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            //grdUser.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

            BindGridData();
        }
        public void BindLegalEntityData()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, customerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.Items.Count > 0)
                ddlSubEntity1.Items.Clear();

            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                    {
                        BindProcess("P");
                    }
                    else
                    {
                        BindProcess("N");
                    }
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                    {
                        BindProcess("P");
                    }
                    else
                    {
                        BindProcess("N");
                    }

                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                    {
                        BindProcess("P");
                    }
                    else
                    {
                        BindProcess("N");
                    }
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.ClearSelection();

            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                    {
                        BindProcess("P");
                    }
                    else
                    {
                        BindProcess("N");
                    }
                    BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindProcess("P");
            }
            else
            {
                BindProcess("N");
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        public void BindGridData()
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P");
            }
            else if (rdRiskActivityProcess.SelectedItem.Text == "Others")
            {
                BindData("N");
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {


            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
        private void BindProcessPopPup(string flag)
        {
            try
            {
                int customerID = -1;               
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int CustomerBranchId = -1;

                if (flag == "P")
                {
                    if (!String.IsNullOrEmpty(ddlClientProcess.SelectedValue))
                    {
                        if (ddlClientProcess.SelectedValue != "")
                            CustomerBranchId = Convert.ToInt32(ddlClientProcess.SelectedValue);
                    }

                    ddlSubProcess.Items.Clear();

                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = ProcessManagement.FillProcess("P", customerID, CustomerBranchId);
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Process", "-1"));

                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcessPoPup1(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                    }
                }
                else
                {
                    var process = ProcessManagement.FillProcess("N", Convert.ToInt32(customerID));

                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = process;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Process", "-1"));
                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcessPoPup1(Convert.ToInt32(ddlProcess.SelectedValue), "N");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindProcessPopPup(int Branchid, int Processid, string flag)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);


                if (flag == "P")
                {
                    var process = ProcessManagement.FillProcess("P", customerID, Convert.ToInt32(Branchid));

                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = process;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Process", "-1"));

                    BindSubProcessPoPup1(Convert.ToInt32(Processid), "P");

                }
                else
                {
                    var process = ProcessManagement.FillProcess("N", Convert.ToInt32(customerID));

                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = process;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("Process", "-1"));
                    BindSubProcessPoPup1(Convert.ToInt32(Processid), "N");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindProcess(string flag)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int CustomerBranchId = -1;

                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }


                if (flag == "P")
                {
                    //var process = ProcessManagement.FillProcess("P", customerID, Convert.ToInt32(CustomerBranchId)); 
                    ddlFilterSubProcess.Items.Clear();
                    ddlFilterProcess.Items.Clear();
                    ddlFilterProcess.DataTextField = "Name";
                    ddlFilterProcess.DataValueField = "Id";
                    ddlFilterProcess.DataSource = ProcessManagement.FillProcess("P", customerID, Convert.ToInt32(CustomerBranchId)); ;
                    ddlFilterProcess.DataBind();
                    ddlFilterProcess.Items.Insert(0, new ListItem("Process", "-1"));

                    if (!String.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                    {
                        BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "P");
                    }
                }
                else
                {
                    //var process = ProcessManagement.FillProcess("N", customerID, Convert.ToInt32(CustomerBranchId));
                    ddlFilterSubProcess.Items.Clear();
                    ddlFilterProcess.Items.Clear();
                    ddlFilterProcess.DataTextField = "Name";
                    ddlFilterProcess.DataValueField = "Id";
                    ddlFilterProcess.DataSource = ProcessManagement.FillProcess("N", customerID, Convert.ToInt32(CustomerBranchId));
                    ddlFilterProcess.DataBind();
                    ddlFilterProcess.Items.Insert(0, new ListItem("Process", "-1"));
                    if (!String.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                    {
                        BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "N");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindSubProcess(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlFilterSubProcess.Items.Clear();
                    ddlFilterSubProcess.DataTextField = "Name";
                    ddlFilterSubProcess.DataValueField = "Id";
                    ddlFilterSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlFilterSubProcess.DataBind();
                    ddlFilterSubProcess.Items.Insert(0, new ListItem("Sub Process", "-1"));
                }
                else
                {
                    ddlFilterSubProcess.Items.Clear();
                    ddlFilterSubProcess.DataTextField = "Name";
                    ddlFilterSubProcess.DataValueField = "Id";
                    ddlFilterSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlFilterSubProcess.DataBind();
                    ddlFilterSubProcess.Items.Insert(0, new ListItem("Sub Process", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindSubProcessPoPup1(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("Process", "-1"));
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("Process", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindSubProcessActivity(long Processid, long SubProcessid)
        {
            try
            {

                ddlActivity.Items.Clear();
                ddlActivity.DataTextField = "Name";
                ddlActivity.DataValueField = "Id";
                ddlActivity.DataSource = ProcessManagement.FillSubProcessActivity(Processid, SubProcessid);
                ddlActivity.DataBind();
                ddlActivity.Items.Insert(0, new ListItem("Process", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindLocationType()
        {
            long CustomerID = Portal.Common.AuthenticationHelper.CustomerID;
            ddlFilterLocationType.DataTextField = "Name";
            ddlFilterLocationType.DataValueField = "ID";
            ddlFilterLocationType.Items.Clear();
            ddlFilterLocationType.DataSource = ProcessManagement.FillLocationType(CustomerID);
            ddlFilterLocationType.DataBind();
            ddlFilterLocationType.Items.Insert(0, new ListItem("All", "-1"));
        }
        public void BindLocationTypePopPup()
        {
            long CustomerID = Portal.Common.AuthenticationHelper.CustomerID;
            ddlLocationType.DataTextField = "Name";
            ddlLocationType.DataValueField = "ID";
            ddlLocationType.Items.Clear();
            ddlLocationType.DataSource = ProcessManagement.FillLocationType(CustomerID);
            ddlLocationType.DataBind();
            ddlLocationType.Items.Insert(0, new ListItem(" Select Location Type ", "-1"));
        }
        protected void ddlClientProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindProcessPopPup("P");
            }
            else
            {
                BindProcessPopPup("N");
            }
        }
        protected void ddlFilterProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                if (!string.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                {
                    if (ddlFilterProcess.SelectedValue != "-1")
                    {
                        BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "P");
                    }
                    else
                    {
                        if (ddlFilterSubProcess.Items.Count > 0)
                            ddlFilterSubProcess.Items.Clear();
                    }
                }
                else
                {
                    if (ddlFilterSubProcess.Items.Count > 0)
                        ddlFilterSubProcess.Items.Clear();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                {
                    if (ddlFilterProcess.SelectedValue != "-1")
                    {
                        BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "N");
                    }
                }
                else
                {
                    if (ddlFilterSubProcess.Items.Count > 0)
                        ddlFilterSubProcess.Items.Clear();
                }
            }

            ddlPageSize_SelectedIndexChanged(sender, e);
        }
        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        BindSubProcessPoPup1(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
                {
                    if (ddlProcess.SelectedValue != "-1")
                    {
                        BindSubProcessPoPup1(Convert.ToInt32(ddlProcess.SelectedValue), "N");
                    }
                }
            }
            ddlSubProcess.ClearSelection();
        }
        protected void ddlFilterSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                this.BindData("P");
            }
            else
            {
                this.BindData("N");
            }
        }
        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlProcess.SelectedValue))
            {
                if (ddlProcess.SelectedValue != "-1")
                {
                    if (!string.IsNullOrEmpty(ddlSubProcess.SelectedValue))
                    {
                        if (ddlSubProcess.SelectedValue != "-1")
                        {
                            BindSubProcessActivity(Convert.ToInt32(ddlProcess.SelectedValue), Convert.ToInt32(ddlSubProcess.SelectedValue));
                        }
                    }
                }
            }
        }
    

        protected void ddlActivity_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void ddlFilterRiskCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                this.BindData("P");
            }
            else
            {
                this.BindData("N");
            }
        }
        protected void ddlFilterIndustry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                this.BindData("P");
            }
            else
            {
                this.BindData("N");
            }
        }
        protected void ddlFilterLocationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPageSize_SelectedIndexChanged(sender, e);
        }

        protected void rdRiskActivityProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindProcess("P");
                BindData("P");
            }
            else
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindProcess("N");
                BindData("N");
            }
        }
        public string ShowIndustryName(long categoryid)
        {
            List<IndustryName_Result> a = new List<IndustryName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetIndustryNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdRiskActivityMatrix.Rows.Count > 0)
                {
                    bool suucess = false;
                    List<RiskActivityTransaction> riskactivitytransactionlist = new List<RiskActivityTransaction>();
                    List<RiskActivityTransaction> riskactivitytransactionlist1 = new List<RiskActivityTransaction>();
                    foreach (GridViewRow g1 in grdRiskActivityMatrix.Rows)
                    {
                        string taskid = (g1.FindControl("lbltaskidItemTemplate") as Label).Text;
                        string Assertions = (g1.FindControl("lblAssertions") as Label).Text;
                        string ExistingControlDescription = (g1.FindControl("txtExistingControlDescriptionItemTemplate") as TextBox).Text;
                        string MControlDescription = (g1.FindControl("txtMControlDescriptionItemTemplate") as TextBox).Text;
                        string PersonResponsible = (g1.FindControl("ddlPersonResponsibleItemTemplate") as DropDownList).SelectedItem.Value;
                        string EffectiveDate = (g1.FindControl("txtEffectiveDateItemTemplate") as TextBox).Text;
                        string Key = (g1.FindControl("ddlKeyItemTemplate") as DropDownList).SelectedItem.Value;
                        string Preventive = (g1.FindControl("ddlPreventiveItemTemplate") as DropDownList).SelectedItem.Value;
                        string Automated = (g1.FindControl("ddlAutomatedItemTemplate") as DropDownList).SelectedItem.Value;
                        string Frequency = (g1.FindControl("ddlFrequencytemTemplate") as DropDownList).SelectedItem.Value;
                        string lblClient = (g1.FindControl("lblClient") as Label).Text;
                        string GapDescriptionItemTemplate = (g1.FindControl("txtGapDescriptionItemTemplate") as TextBox).Text;
                        string RecommendationsItemTemplate = (g1.FindControl("txtRecommendationsItemTemplate") as TextBox).Text;
                        string ActionRemediationplanItemTemplate = (g1.FindControl("txtActionRemediationplanItemTemplate") as TextBox).Text;
                        string IPEItemTemplate = (g1.FindControl("txtIPEItemTemplate") as TextBox).Text;
                        string ERPsystemItemTemplate = (g1.FindControl("txtERPsystemItemTemplate") as TextBox).Text;
                        string FRCItemTemplate = (g1.FindControl("txtFRCItemTemplate") as TextBox).Text;
                        string UniqueReferredItemTemplate = (g1.FindControl("txtUniqueReferredItemTemplate") as TextBox).Text;
                        
                        int customerbranchid = RiskCategoryManagement.GetCustomerBranchIDByName(lblClient);
                        if (!String.IsNullOrEmpty(taskid))
                        {
                            RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction();
                            riskactivitytransaction.ProcessId = Convert.ToInt32(ddlFilterProcess.SelectedValue);
                            riskactivitytransaction.SubProcessId = Convert.ToInt32(ddlFilterSubProcess.SelectedValue);
                            riskactivitytransaction.RiskCreationId = Convert.ToInt32(taskid);
                            riskactivitytransaction.Assertion = Convert.ToInt32(Assertions);
                            riskactivitytransaction.ControlDescription = ExistingControlDescription;
                            riskactivitytransaction.MControlDescription = MControlDescription;
                            riskactivitytransaction.PersonResponsible = Convert.ToInt32(PersonResponsible);
                            riskactivitytransaction.EffectiveDate = Convert.ToDateTime(EffectiveDate);
                            riskactivitytransaction.Key_Value = Convert.ToInt32(Key);
                            riskactivitytransaction.PrevationControl = Convert.ToInt32(Preventive);
                            riskactivitytransaction.AutomatedControl = Convert.ToInt32(Automated);
                            riskactivitytransaction.Frequency = Convert.ToInt32(Frequency);
                            riskactivitytransaction.CustomerBranchId = customerbranchid;
                            riskactivitytransaction.GapDescription = GapDescriptionItemTemplate;
                            riskactivitytransaction.Recommendations = RecommendationsItemTemplate;
                            riskactivitytransaction.ActionRemediationplan = ActionRemediationplanItemTemplate;
                            riskactivitytransaction.IPE = IPEItemTemplate;
                            riskactivitytransaction.ERPsystem = ERPsystemItemTemplate;
                            riskactivitytransaction.FRC = FRCItemTemplate;
                            riskactivitytransaction.UniqueReferred = UniqueReferredItemTemplate;
                            riskactivitytransaction.SpecialAuditFlag = false;
                            //riskactivitytransaction.TestStrategy = TestStrategyItemTemplate;
                            //riskactivitytransaction.DocumentsExamined = DocumentsExaminedItemTemplate;
                            riskactivitytransactionlist.Add(riskactivitytransaction);
                        }
                    }
                    riskactivitytransactionlist1 = riskactivitytransactionlist.Where(entry => entry.Frequency == null).ToList();
                    if (riskactivitytransactionlist1.Count == 0)
                    {
                        suucess = RiskCategoryManagement.CreateRiskActivityTransaction(riskactivitytransactionlist);
                        if (suucess == true)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Data Save successfully.";
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Error.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string ShowAssertionsName(long categoryid)
        {
            List<AssertionsName_Result> a = new List<AssertionsName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetAssertionsNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }
        public string ShowPersonResposibleName(long categoryid)
        {
            List<PersonResponsibleName_Result> a = new List<PersonResponsibleName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetPersonResponsibleNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }
        public string ShowKeyName(long categoryid)
        {
            List<KeyName_Result> a = new List<KeyName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetKeyNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }
            return processnonprocess.Trim(',');
        }
        public string ShowControlName(long categoryid)
        {
            List<ControlName_Result> a = new List<ControlName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetControlNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }
            return processnonprocess.Trim(',');
        }
        public string ShowFrequencyName(long categoryid)
        {
            List<FrequencyName_Result> a = new List<FrequencyName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetFrequencyNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }
            return processnonprocess.Trim(',');
        }
        public string ShowCustomerBranchName(long categoryid, long branchid)
        {
            List<CustomerBranchNameLocationWise_Result> a = new List<CustomerBranchNameLocationWise_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetCustomerBranchNameProcedureLocationWise(Convert.ToInt32(categoryid), Convert.ToInt32(branchid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }
        public string ShowRiskCategoryName(long categoryid)
        {
            List<RiskCategoryName_Result> a = new List<RiskCategoryName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetRiskCategoryNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }
            return processnonprocess.Trim(',');
        }
        public string ShowClientName(long branchid)
        {
            string processnonprocess = "";
            processnonprocess = ProcessManagement.GetClientnameName(branchid);
            return processnonprocess;
        }
        public string ShowProcessName(long processId)
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);


            string processnonprocess = "";
            processnonprocess = ProcessManagement.GetProcessName(processId, customerID);
            return processnonprocess;
        }
        public string ShowSubProcessName(long processId, long SubprocessId)
        {
            string processnonprocess = "";
            processnonprocess = ProcessManagement.GetSubProcessName(processId, SubprocessId);
            return processnonprocess;
        }
        public string ShowRiskRatingName(int? ValueID)
        {
            string processnonprocess = "";
            if (!string.IsNullOrEmpty(Convert.ToString(ValueID)))
            {
                processnonprocess = GetRiskControlRating(Convert.ToInt32(ValueID), "R");
            }             
            return processnonprocess.Trim();
        }
        public string GetRiskControlRating(int ValueID , string flag)
        {            
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = "";
                if (flag =="R")
                {
                     transactionsQuery = (from row in entities.mst_Risk_ControlRating
                                             where row.Value == ValueID && row.IsActive == false
                                             && row.IsRiskControl == "R"
                                             select row.Name).FirstOrDefault();
                }
                else if (flag=="C")
                {
                    transactionsQuery = (from row in entities.mst_Risk_ControlRating
                                             where row.Value == ValueID && row.IsActive == false
                                             && row.IsRiskControl == "C"
                                             select row.Name).FirstOrDefault();
                }
               
                return transactionsQuery;
               
            }
        }
        public string ShowControlRatingName(int? ValueID)
        {
            string processnonprocess = "";
            if (!string.IsNullOrEmpty(Convert.ToString(ValueID)))
            {
                processnonprocess = GetRiskControlRating(Convert.ToInt32(ValueID), "C"); 
            }                  
            return processnonprocess.Trim();
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        private List<FilterRiskControlMatrixForUpdate> BindDataExport(string Flag)
        {
            
                int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            int CustomerBranchId = -1;
                int processid = -1;
                int subprocessid = -1;
                int LocationType = -1; 

                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                {
                    if (ddlFilterProcess.SelectedValue != "-1")
                    {
                        processid = Convert.ToInt32(ddlFilterProcess.SelectedValue);
                    }
                }

                if (!string.IsNullOrEmpty(ddlFilterSubProcess.SelectedValue))
                {
                    if (ddlFilterSubProcess.SelectedValue != "-1")
                    {
                        subprocessid = Convert.ToInt32(ddlFilterSubProcess.SelectedValue);
                    }
                }               

                if (!string.IsNullOrEmpty(ddlFilterLocationType.SelectedValue))
                {
                    if (ddlFilterLocationType.SelectedValue != "-1")
                    {
                        LocationType = Convert.ToInt32(ddlFilterLocationType.SelectedValue);
                    }
                }

                Branchlist.Clear();
                GetAllHierarchy(customerID, CustomerBranchId);
                Branchlist.ToList();

                List<FilterRiskControlMatrixForUpdate> RCMExportList = new List<FilterRiskControlMatrixForUpdate>();

                if (Flag == "P")
                {
                    RCMExportList = RiskControlMatrixClass.GetRiskControlMatrixDataForUpdate(customerID, Branchlist.ToList(), processid, subprocessid, LocationType, "");                   
                }
                else
                {
                    RCMExportList = RiskControlMatrixClass.GetRiskControlMatrixDataForUpdate(customerID, Branchlist.ToList(), processid, subprocessid, LocationType, "");
                }

            return RCMExportList;            
        }
        private void BindData(string Flag)
        {
            try
            {
                int customerID = -1;
                //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                int CustomerBranchId = -1;
                int processid = -1;
                int subprocessid = -1;
                int LocationType = -1;
                int RiskCategory = -1;
                int Industry = -1;                
                if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);                        
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);                        
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);                        
                    }
                }
                if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);                        
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);                        
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                {
                    if (ddlFilterProcess.SelectedValue != "-1")
                    {
                        processid = Convert.ToInt32(ddlFilterProcess.SelectedValue);                        
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterSubProcess.SelectedValue))
                {
                    if (ddlFilterSubProcess.SelectedValue != "-1")
                    {
                        subprocessid = Convert.ToInt32(ddlFilterSubProcess.SelectedValue);                        
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocationType.SelectedValue))
                {
                    if (ddlFilterLocationType.SelectedValue != "-1")
                    {
                        LocationType = Convert.ToInt32(ddlFilterLocationType.SelectedValue);                        
                    }
                }

                Branchlist.Clear();
                GetAllHierarchy(customerID, CustomerBranchId);
                Branchlist.ToList();

                if (Flag == "P")
                {
                    var Riskcategorymanagementlist = RiskControlMatrixClass.GetAllRiskControlMatrix(customerID, Branchlist.ToList(), processid, subprocessid, LocationType);
                    grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = null;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;                   
                    grdRiskActivityMatrix.DataBind();

                    //Session["grdControlTestingAssignment"] = (grdRiskActivityMatrix.DataSource as List<FilterRiskControlMatrix>).ToDataTable();

                    Riskcategorymanagementlist.Clear();
                    Riskcategorymanagementlist = null;
                }
                else
                {
                    var Riskcategorymanagementlist = RiskControlMatrixClass.GetAllRiskControlMatrix(customerID, Branchlist.ToList(), processid, subprocessid, LocationType);
                    grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                    Session["TotalRows"] = null;
                    Session["TotalRows"] = Riskcategorymanagementlist.Count;                    
                    grdRiskActivityMatrix.DataBind();

                    //Session["grdControlTestingAssignment"] = (grdRiskActivityMatrix.DataSource as List<FilterRiskControlMatrix>).ToDataTable();

                    Riskcategorymanagementlist.Clear();
                    Riskcategorymanagementlist = null;
                }
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string ShowDate(DateTime dt)
        {
            try
            {
                DateTime dt1 = dt;
                if (dt1 == null)
                    return String.Empty;
                else
                    return dt1.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {      
            try
            {
                ViewState["Mode"] = 0;
                tbxControl.Text = string.Empty;
                tbxRiskDesc.Text = string.Empty;
                tbxControlObj.Text = string.Empty;
                ddlRiskCategoryCheck.ClearSelection();
                ddlAssertions.ClearSelection();               
                ddlClientProcess.ClearSelection();
                tbxControlDesc.Text = string.Empty;
                txtMitigation.Text = string.Empty;
                ddlPersonResponsible.ClearSelection();
                txtStartDate.Text = string.Empty;
                ddlKey.ClearSelection();
                ddlPreventive.ClearSelection();
                ddlAutomated.ClearSelection();
                ddlFrequency.ClearSelection();
                tbxGapDesc.Text = string.Empty;
                tbxRecom.Text = string.Empty;
                tbxActionPlan.Text = string.Empty;
                tbxIPE.Text = string.Empty;
                tbxERPSystem.Text = string.Empty;
                tbxFRC.Text = string.Empty;
                tbxUnique.Text = string.Empty;
                ddlRiskRating.ClearSelection();
                ddlControlRating.ClearSelection();
                ddlVerticalBranch.ClearSelection();               
                ddlLocationType.ClearSelection();
                ddlProcess.ClearSelection();
                ddlSubProcess.ClearSelection();

                BindLocationTypePopPup();
                BindDropDown();
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    BindProcessPopPup("P");
                }
                else
                {
                    BindProcessPopPup("N");
                }
               
                upComplianceDetails.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSavePop_Click(object sender, EventArgs e)
        {
            int flagAssertion = 0;
            int flagIndustry = 0;
            int flagRiskCategory = 0;
            try
            {

                for (int i = 0; i < ddlAssertions.Items.Count; i++)
                {
                    if (ddlAssertions.Items[i].Selected)
                    {
                        flagAssertion = 1;
                    }
                }
                for (int i = 0; i < ddlIndultory.Items.Count; i++)
                {
                    if (ddlIndultory.Items[i].Selected)
                    {
                        flagIndustry = 1;
                    }
                }
                for (int i = 0; i < ddlRiskCategoryCheck.Items.Count; i++)
                {
                    if (ddlRiskCategoryCheck.Items[i].Selected)
                    {
                        flagRiskCategory = 1;
                    }
                }

                if (flagAssertion > 0)
                {
                    if (flagIndustry > 0)
                    {
                        if (flagRiskCategory > 0)
                        {
                            int customerID = -1;
                            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                            if ((int)ViewState["Mode"] == 0)
                            {
                                #region  Creation
                                string Controno = tbxControl.Text;
                                string ActivityDescription = tbxRiskDesc.Text;
                                string controlObjective = tbxControlObj.Text;
                                string MControlDescriptionFooterTemplate = txtMitigation.Text;
                                string ExistingControlDescriptionFooterTemplate = tbxControlDesc.Text;
                                string GapDescriptionFooterTemplate = tbxGapDesc.Text;
                                string RecommendationsFooterTemplate = tbxRecom.Text;
                                string ActionRemediationplanFooterTemplate = tbxActionPlan.Text;
                                string IPEFooterTemplate = tbxIPE.Text;
                                string ERPsystemFooterTemplate = tbxERPSystem.Text;
                                string FRCFooterTemplate = tbxFRC.Text;
                                string UniqueReferredFooterTemplate = tbxUnique.Text;

                                string ClientProcess = ddlClientProcess.SelectedValue;
                                long pid = -1;
                                long psid = -1;
                                long Aid = -1;
                                if (ddlProcess.SelectedValue != "")
                                {
                                    if (ddlProcess.SelectedValue != "All")
                                    {
                                        pid = Convert.ToInt32(ddlProcess.SelectedValue);
                                    }
                                }
                                if (ddlSubProcess.SelectedValue != "")
                                {
                                    if (ddlSubProcess.SelectedValue != "All")
                                    {
                                        psid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                                    }
                                }
                                if (ddlActivity.SelectedValue != "")
                                {
                                    if (ddlActivity.SelectedValue != "All")
                                    {
                                        Aid = Convert.ToInt32(ddlActivity.SelectedValue);
                                    }
                                }
                                if (ActivityDescription != "" || ActivityDescription != null)
                                {
                                    if (controlObjective != "" || controlObjective != null)
                                    {
                                        RiskCategoryCreation riskcategorycreation = new RiskCategoryCreation()
                                        {
                                            ControlNo = Controno,
                                            ActivityDescription = ActivityDescription,
                                            ControlObjective = controlObjective,
                                            ProcessId = pid,
                                            SubProcessId = psid,
                                            IsInternalAudit = "N",
                                            CustomerId = customerID,
                                            CustomerBranchId = Convert.ToInt32(ddlClientProcess.SelectedValue),
                                            LocationType = Convert.ToInt32(ddlLocationType.SelectedValue),
                                            SpecialAuditFlag = false,
                                        };
                                        if (RiskCategoryManagement.Exists(riskcategorycreation))
                                        {
                                            CustomValidator1.ErrorMessage = "Risk already exists.";
                                            CustomValidator1.IsValid = false;
                                            return;
                                        }
                                        using (AuditControlEntities entities = new AuditControlEntities())
                                        {
                                            entities.RiskCategoryCreations.Add(riskcategorycreation);
                                            entities.SaveChanges();
                                        }
                                        RiskIdProcess.Clear();
                                        for (int i = 0; i < ddlRiskCategoryCheck.Items.Count; i++)
                                        {
                                            if (ddlRiskCategoryCheck.Items[i].Selected)
                                            {
                                                RiskIdProcess.Add(Convert.ToInt32(ddlRiskCategoryCheck.Items[i].Value));
                                            }
                                        }
                                        AssertionsIdProcess.Clear();
                                        for (int i = 0; i < ddlAssertions.Items.Count; i++)
                                        {
                                            if (ddlAssertions.Items[i].Selected)
                                            {
                                                AssertionsIdProcess.Add(Convert.ToInt32(ddlAssertions.Items[i].Value));
                                            }
                                        }

                                        IndustryIdProcess.Clear();
                                        for (int i = 0; i < ddlIndultory.Items.Count; i++)
                                        {
                                            if (ddlIndultory.Items[i].Selected)
                                            {
                                                IndustryIdProcess.Add(Convert.ToInt32(ddlIndultory.Items[i].Value));
                                            }
                                        }

                                        if (IndustryIdProcess.Count > 0)
                                        {
                                            foreach (var aItem in IndustryIdProcess)
                                            {
                                                IndustryMapping IndustryMapping = new IndustryMapping()
                                                {
                                                    RiskCategoryCreationId = riskcategorycreation.Id,
                                                    IndustryID = Convert.ToInt32(aItem),
                                                    IsActive = true,
                                                    EditedDate = DateTime.Now,
                                                    EditedBy = Convert.ToInt32(Session["userID"]),
                                                    ProcessId = pid,
                                                    SubProcessId = psid,
                                                };
                                                RiskCategoryManagement.CreateIndustryMapping(IndustryMapping);
                                            }
                                            IndustryIdProcess.Clear();
                                        }
                                        if (RiskIdProcess.Count > 0)
                                        {
                                            foreach (var aItem in RiskIdProcess)
                                            {
                                                RiskCategoryMapping RiskCategoryMapping = new RiskCategoryMapping()
                                                {
                                                    RiskCategoryCreationId = riskcategorycreation.Id,
                                                    RiskCategoryId = Convert.ToInt32(aItem),
                                                    ProcessId = pid,
                                                    SubProcessId = psid,
                                                    IsActive = true,
                                                };
                                                RiskCategoryManagement.CreateRiskCategoryMapping(RiskCategoryMapping);
                                            }
                                            RiskIdProcess.Clear();
                                        }
                                        if (AssertionsIdProcess.Count > 0)
                                        {
                                            foreach (var aItem in AssertionsIdProcess)
                                            {
                                                AssertionsMapping AssertionsMapping = new AssertionsMapping()
                                                {
                                                    RiskCategoryCreationId = riskcategorycreation.Id,
                                                    AssertionId = Convert.ToInt32(aItem),
                                                    ProcessId = pid,
                                                    SubProcessId = psid,
                                                    IsActive = true,
                                                };
                                                RiskCategoryManagement.CreateAssertionsMapping(AssertionsMapping);
                                            }
                                            AssertionsIdProcess.Clear();
                                        }

                                        RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction()
                                        {
                                            ControlDescription = ExistingControlDescriptionFooterTemplate,
                                            MControlDescription = MControlDescriptionFooterTemplate,
                                            ProcessId = pid,
                                            SubProcessId = psid,
                                            ActivityID = Aid,
                                            RiskCreationId = riskcategorycreation.Id,
                                            PersonResponsible = Convert.ToInt32(ddlPersonResponsible.SelectedValue),
                                            ControlOwner = Convert.ToInt32(ddlControlOwner.SelectedValue),
                                            Key_Value = Convert.ToInt32(ddlKey.SelectedValue),
                                            KC2 = Convert.ToInt32(ddlKC1.SelectedValue),
                                            AutomatedControl = Convert.ToInt32(ddlAutomated.SelectedValue),
                                            Primary_Secondary = Convert.ToInt32(ddlPrimarySecondary.SelectedValue),
                                            Frequency = Convert.ToInt32(ddlFrequency.SelectedValue),
                                            PrevationControl = Convert.ToInt32(ddlPreventive.SelectedValue),
                                            EffectiveDate = DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                            IsDeleted = false,
                                            CustomerBranchId = Convert.ToInt32(ddlClientProcess.SelectedValue),
                                            GapDescription = GapDescriptionFooterTemplate,
                                            Recommendations = RecommendationsFooterTemplate,
                                            ActionRemediationplan = ActionRemediationplanFooterTemplate,
                                            IPE = IPEFooterTemplate,
                                            ERPsystem = ERPsystemFooterTemplate,
                                            FRC = FRCFooterTemplate,
                                            UniqueReferred = UniqueReferredFooterTemplate,
                                            RiskRating = Convert.ToInt32(ddlRiskRating.SelectedValue),
                                            ControlRating = Convert.ToInt32(ddlControlRating.SelectedValue),
                                            VerticalsId = Convert.ToInt32(ddlVerticalBranch.SelectedValue),
                                            ProcessScore = 0,
                                            SpecialAuditFlag = false,
                                        };
                                        if (RiskCategoryManagement.RiskActivityTransactionExists(riskactivitytransaction))
                                        {
                                            cvDuplicateEntry.ErrorMessage = "Risk with Same Control Description already exists.";
                                            cvDuplicateEntry.IsValid = false;
                                            return;
                                        }
                                        using (AuditControlEntities entities = new AuditControlEntities())
                                        {
                                            entities.RiskActivityTransactions.Add(riskactivitytransaction);
                                            entities.SaveChanges();
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region Updation
                                long pid = -1;
                                long psid = -1;
                                try
                                {
                                    if (ddlProcess.SelectedValue != "-1" && ddlSubProcess.SelectedValue != "-1")
                                    {
                                        if (ddlProcess.SelectedItem.Text != "All")
                                        {
                                            pid = Convert.ToInt32(ddlProcess.SelectedValue);
                                        }
                                        if (ddlSubProcess.SelectedItem.Text != "All")
                                        {
                                            psid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                                        }
                                        long RiskCreationId = Convert.ToInt32(ViewState["RRID"]);
                                        int riskactivityid = Convert.ToInt32(ViewState["RCMID"]);
                                        RiskCategoryCreation RiskData = new RiskCategoryCreation()
                                        {
                                            Id = RiskCreationId,
                                            ActivityDescription = tbxRiskDesc.Text,
                                            ControlObjective = tbxControlObj.Text,
                                            LocationType = Convert.ToInt32(ddlLocationType.SelectedValue),
                                            SpecialAuditFlag = false,
                                        };
                                        RiskCategoryManagement.RiskUpdateRCM(RiskData);
                                        RiskCategoryManagement.UpdateRiskCategoryMapping(RiskCreationId);
                                        RiskCategoryManagement.UpdateAssertionsMapping(RiskCreationId);
                                        RiskCategoryManagement.UpdateRiskIndustryMapping(RiskCreationId);
                                        RiskIdProcess.Clear();
                                        for (int i = 0; i < ddlRiskCategoryCheck.Items.Count; i++)
                                        {
                                            if (ddlRiskCategoryCheck.Items[i].Selected)
                                            {
                                                RiskIdProcess.Add(Convert.ToInt32(ddlRiskCategoryCheck.Items[i].Value));
                                            }
                                        }
                                        AssertionsIdProcess.Clear();
                                        for (int i = 0; i < ddlAssertions.Items.Count; i++)
                                        {
                                            if (ddlAssertions.Items[i].Selected)
                                            {
                                                AssertionsIdProcess.Add(Convert.ToInt32(ddlAssertions.Items[i].Value));
                                            }
                                        }
                                        IndustryIdProcess.Clear();
                                        for (int i = 0; i < ddlIndultory.Items.Count; i++)
                                        {
                                            if (ddlIndultory.Items[i].Selected)
                                            {
                                                IndustryIdProcess.Add(Convert.ToInt32(ddlIndultory.Items[i].Value));
                                            }
                                        }
                                        if (IndustryIdProcess.Count > 0)
                                        {
                                            foreach (var aItem in IndustryIdProcess)
                                            {
                                                IndustryMapping IndustryMapping = new IndustryMapping()
                                                {
                                                    RiskCategoryCreationId = RiskCreationId,
                                                    IndustryID = Convert.ToInt32(aItem),
                                                    IsActive = true,
                                                    EditedDate = DateTime.Now,
                                                    EditedBy = Convert.ToInt32(Session["userID"]),
                                                    ProcessId = pid,
                                                    SubProcessId = psid,
                                                };
                                                RiskCategoryManagement.CreateIndustryMapping(IndustryMapping);
                                            }
                                            IndustryIdProcess.Clear();
                                        }
                                        if (RiskIdProcess.Count > 0)
                                        {
                                            foreach (var aItem in RiskIdProcess)
                                            {
                                                if (RiskCategoryManagement.RiskCategoryMappingExists(Convert.ToInt64(aItem), RiskCreationId))
                                                {
                                                    RiskCategoryManagement.EditRiskCategoryMapping(aItem, RiskCreationId);
                                                }
                                                else
                                                {
                                                    if (pid != -1 && psid != -1)
                                                    {
                                                        RiskCategoryMapping RiskCategoryMapping = new RiskCategoryMapping()
                                                        {
                                                            RiskCategoryCreationId = RiskCreationId,
                                                            RiskCategoryId = Convert.ToInt32(aItem),
                                                            ProcessId = pid,
                                                            SubProcessId = psid,
                                                            IsActive = true,
                                                        };
                                                        RiskCategoryManagement.CreateRiskCategoryMapping(RiskCategoryMapping);
                                                    }
                                                }
                                            }
                                            RiskIdProcess.Clear();
                                        }
                                        if (AssertionsIdProcess.Count > 0)
                                        {
                                            foreach (var aItem in AssertionsIdProcess)
                                            {
                                                if (RiskCategoryManagement.AssertionMappingExists(Convert.ToInt64(aItem), RiskCreationId))
                                                {
                                                    RiskCategoryManagement.EditAssertionsMapping(aItem, RiskCreationId);
                                                }
                                                else
                                                {
                                                    if (pid != -1 && psid != -1)
                                                    {
                                                        AssertionsMapping AssertionsMapping = new AssertionsMapping()
                                                        {
                                                            RiskCategoryCreationId = RiskCreationId,
                                                            AssertionId = Convert.ToInt32(aItem),
                                                            ProcessId = pid,
                                                            SubProcessId = psid,
                                                            IsActive = true,
                                                        };
                                                        RiskCategoryManagement.CreateAssertionsMapping(AssertionsMapping);
                                                    }
                                                }
                                            }
                                            AssertionsIdProcess.Clear();
                                        }
                                        RiskActivityTransaction RiskActivityTxnData = new RiskActivityTransaction()
                                        {
                                            Id = riskactivityid,
                                            ControlDescription = tbxControlDesc.Text,
                                            MControlDescription = txtMitigation.Text,
                                            ProcessId = pid,
                                            SubProcessId = psid,
                                            RiskCreationId = RiskCreationId,
                                            PersonResponsible = Convert.ToInt32(ddlPersonResponsible.SelectedValue),
                                            Key_Value = Convert.ToInt32(ddlKey.SelectedValue),
                                            AutomatedControl = Convert.ToInt32(ddlAutomated.SelectedValue),
                                            Frequency = Convert.ToInt32(ddlFrequency.SelectedValue),
                                            PrevationControl = Convert.ToInt32(ddlPreventive.SelectedValue),
                                            EffectiveDate = DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                            IsDeleted = false,
                                            GapDescription = tbxGapDesc.Text,
                                            Recommendations = tbxRecom.Text,
                                            ActionRemediationplan = tbxActionPlan.Text,
                                            IPE = tbxIPE.Text,
                                            ERPsystem = tbxERPSystem.Text,
                                            FRC = tbxFRC.Text,
                                            UniqueReferred = tbxUnique.Text,
                                            RiskRating = Convert.ToInt32(ddlRiskRating.SelectedValue),
                                            ControlRating = Convert.ToInt32(ddlControlRating.SelectedValue),
                                            CustomerBranchId = Convert.ToInt32(ddlClientProcess.SelectedValue),
                                            VerticalsId = Convert.ToInt32(ddlVerticalBranch.SelectedValue),
                                            ProcessScore = 0,
                                            SpecialAuditFlag = false,
                                        };
                                        RiskCategoryManagement.RiskActivityTxnUpdate(RiskActivityTxnData);

                                        grdRiskActivityMatrix.EditIndex = -1;
                                    }
                                    else
                                    {
                                        rfvProcess.IsValid = false;
                                        rfvSubProcess.IsValid = false;
                                        cvDuplicateEntry.IsValid = false;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                                }
                                #endregion
                            }
                            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                            {
                                this.BindData("P");
                            }
                            else
                            {
                                this.BindData("N");
                            }
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:CloseWin()", true);
                            upCompliance.Update();
                        }
                        else
                        {
                            CustomValidator1.ErrorMessage = "Plase Select RiskCategory.";
                            CustomValidator1.IsValid = false;
                            return;
                        }
                    }
                    else
                    {
                        CustomValidator1.ErrorMessage = "Plase select Industry.";
                        CustomValidator1.IsValid = false;
                        return;
                    }
                }
                else
                {
                    CustomValidator1.ErrorMessage = "Plase select Assertions.";
                    CustomValidator1.IsValid = false;
                    return;
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void checkBoxesProcess_SelcetedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Saplin.Controls.DropDownCheckBoxes DrpIndustryProcess = (DropDownCheckBoxes)sender;
                IndustryIdProcess.Clear();
                for (int i = 0; i < DrpIndustryProcess.Items.Count; i++)
                {
                    if (DrpIndustryProcess.Items[i].Selected)
                    {
                        IndustryIdProcess.Add(Convert.ToInt32(DrpIndustryProcess.Items[i].Value));
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void checkBoxesAssertionsProcess_SelcetedIndexChanged(object sender, EventArgs e)
        {
            Saplin.Controls.DropDownCheckBoxes ddlAssertionsProcess = (DropDownCheckBoxes)sender;
            AssertionsIdProcess.Clear();
            for (int i = 0; i < ddlAssertionsProcess.Items.Count; i++)
            {
                if (ddlAssertionsProcess.Items[i].Selected)
                {
                    AssertionsIdProcess.Add(Convert.ToInt32(ddlAssertionsProcess.Items[i].Value));
                }
            }
        }
        protected void checkBoxesRiskCategoryProcess_SelcetedIndexChanged(object sender, EventArgs e)
        {
            Saplin.Controls.DropDownCheckBoxes DrpRiskCategoryProcess = (DropDownCheckBoxes)sender;
            RiskIdProcess.Clear();
            for (int i = 0; i < DrpRiskCategoryProcess.Items.Count; i++)
            {
                if (DrpRiskCategoryProcess.Items[i].Selected)
                {
                    RiskIdProcess.Add(Convert.ToInt32(DrpRiskCategoryProcess.Items[i].Value));
                }
            }
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
        }
        private void BindBranchesHierarchy(TreeNode parent, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
     
        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter1", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter1", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);

                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePicker", "initializeDatePicker(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static FilterRiskControlMatrix GetRiskControlMatrixByRCMID(int RCMID,int RRID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var FilterRiskControl = (from row in entities.FilterRiskControlMatrices
                                         where //row.RATID == RCMID &&
                                         row.RiskCreationId == RRID
                                         select row).FirstOrDefault();

                return FilterRiskControl;
            }
        }
        protected void grdRiskActivityMatrix_SelectedIndexChanged(object sender, EventArgs e)
        {

            //tbxControl.Text = grdRiskActivityMatrix.SelectedRow.Cells[1].Text;
            //tbxRiskDesc.Text = grdRiskActivityMatrix.SelectedRow.Cells[2].Text;
            //tbxControlObj.Text = grdRiskActivityMatrix.SelectedRow.Cells[3].Text;
            //ddlClientProcessEdit.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[4].Text;
            //ddlRiskCategory.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[5].Text;
            //ddlAssertionsFooterTemplate.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[6].Text;
            //tbxControlDesc.Text = grdRiskActivityMatrix.SelectedRow.Cells[7].Text;
            //txtMitigation.Text = grdRiskActivityMatrix.SelectedRow.Cells[8].Text;
            //ddlPersonResponsibleItemTemplate.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[9].Text;
            //txtStartDate.Text = grdRiskActivityMatrix.SelectedRow.Cells[10].Text;
            //ddlKeyItemTemplate.Text = grdRiskActivityMatrix.SelectedRow.Cells[11].Text;
            //ddlPreventiveItemTemplate.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[12].Text;
            //ddlAutomatedItemTemplate.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[13].Text;
            //ddlFrequencytemTemplate.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[14].Text;
            //tbxGapDesc.Text = grdRiskActivityMatrix.SelectedRow.Cells[15].Text;
            //tbxRecom.Text = grdRiskActivityMatrix.SelectedRow.Cells[16].Text;
            //tbxActionPlan.Text = grdRiskActivityMatrix.SelectedRow.Cells[17].Text;
            //tbxIPE.Text = grdRiskActivityMatrix.SelectedRow.Cells[18].Text;
            //tbxERPSystem.Text = grdRiskActivityMatrix.SelectedRow.Cells[19].Text;
            //tbxFRC.Text = grdRiskActivityMatrix.SelectedRow.Cells[20].Text;
            //tbxUnique.Text = grdRiskActivityMatrix.SelectedRow.Cells[21].Text;
            //ddlRiskEdit.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[22].Text;
            //ddlControlEdit.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[23].Text;

        }
        protected void grdRiskActivityMatrix_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_RCM"))
                {

                    int customerID = -1;
                    //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    //countrybindClient(ddlClient, customerID);

                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int RCMID = Convert.ToInt32(commandArgs[0]);
                    int RRID = Convert.ToInt32(commandArgs[1]);
                    ViewState["RCMID"] = RCMID;
                    ViewState["RRID"] = RRID;
                    ViewState["Mode"] = 1;
                    var RCMInfo = GetRiskControlMatrixByRCMID(RCMID, RRID);
                    if (RCMInfo !=null)
                    {
                        if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                        {
                            BindProcessPopPup(Convert.ToInt32(RCMInfo.BranchId), Convert.ToInt32(RCMInfo.ProcessId), "P");
                        }
                        else
                        {
                            BindProcessPopPup(Convert.ToInt32(RCMInfo.BranchId), Convert.ToInt32(RCMInfo.ProcessId),"N");
                        }
                        tbxControl.Text = RCMInfo.ControlNo;
                        tbxRiskDesc.Text = RCMInfo.ActivityDescription;
                        tbxControlObj.Text = RCMInfo.ControlObjective;
                        SplitString(ShowRiskCategoryName(RRID), ddlRiskCategoryCheck);
                        SplitString(ShowAssertionsName(RRID), ddlAssertions);
                        SplitString(ShowIndustryName(RRID), ddlIndultory);
                        ddlClientProcess.SelectedValue = Convert.ToString(RCMInfo.BranchId);
                        ddlProcess.SelectedValue = Convert.ToString(RCMInfo.ProcessId);
                        ddlSubProcess.SelectedValue = Convert.ToString(RCMInfo.SubProcessId);
                        ddlLocationType.SelectedValue = Convert.ToString(RCMInfo.LocationType);
                        tbxControlDesc.Text = RCMInfo.ControlDescription;
                        txtMitigation.Text = RCMInfo.MControlDescription;
                        ddlPersonResponsible.SelectedValue =Convert.ToString(RCMInfo.PersonResponsible);
                        txtStartDate.Text = Convert.ToDateTime(RCMInfo.EffectiveDate).ToString("dd-MM-yyyy");
                        ddlKey.SelectedValue = Convert.ToString(RCMInfo.Key);
                        ddlPreventive.SelectedValue = Convert.ToString(RCMInfo.PrevationControl);
                        ddlAutomated.SelectedValue = Convert.ToString(RCMInfo.AutomatedControl);
                        ddlFrequency.SelectedValue = Convert.ToString(RCMInfo.Frequency);
                        tbxGapDesc.Text = RCMInfo.GapDescription;
                        tbxRecom.Text = RCMInfo.Recommendations;
                        tbxActionPlan.Text = RCMInfo.ActionRemediationplan;
                        tbxIPE.Text = RCMInfo.IPE;
                        tbxERPSystem.Text = RCMInfo.ERPsystem;
                        tbxFRC.Text = RCMInfo.FRC;
                        tbxUnique.Text = RCMInfo.UniqueReferred;
                        ddlRiskRating.SelectedValue = Convert.ToString(RCMInfo.RiskRating);
                        ddlControlRating.SelectedValue = Convert.ToString(RCMInfo.ControlRating);
                        //ddlVerticalBranch.SelectedValue = Convert.ToString(RCMInfo.VerticalsId);
                        upComplianceDetails.Update();
                    }
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdRiskActivityMatrix_RowDataBound(object sender, GridViewRowEventArgs e)
        {           
        }
      
        protected void grdRiskActivityMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdRiskActivityMatrix.PageIndex = e.NewPageIndex;
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    this.BindData("P");
                }
                else
                {
                    this.BindData("N");
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void setDateToGridView()
        {
            try
            {
              
                DateTime date = DateTime.Now;
                
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string ShowCustomerBranchName(long categoryid)
        {
            List<CustomerBranchName_Result> a = new List<CustomerBranchName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetCustomerBranchNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }

        public void SplitString(string StrToSplit, Saplin.Controls.DropDownCheckBoxes DrpToFill)
        {
            string[] commandArgs = StrToSplit.Split(new char[] { ',' });
            string a = string.Empty;
            if (commandArgs.Length > 0)
            {
                for (int i = 0; i < commandArgs.Length; i++)
                {
                    a = commandArgs[i];
                    if (!string.IsNullOrEmpty(a))
                    {
                        DrpToFill.Items.FindByText(a.Trim()).Selected = true;
                    }
                }
            }
        }
        private void countrybindIndustory(Saplin.Controls.DropDownCheckBoxes DrpIndustry)
        {
            DrpIndustry.DataTextField = "Name";
            DrpIndustry.DataValueField = "Id";
            DrpIndustry.DataSource = ProcessManagement.FilliNDUSTRY();
            DrpIndustry.DataBind();
        }
        private void countrybindAssertions(Saplin.Controls.DropDownCheckBoxes DrpAssertions)
        {
            DrpAssertions.DataTextField = "Name";
            DrpAssertions.DataValueField = "ID";
            DrpAssertions.DataSource = RiskCategoryManagement.FillAssertions();
            DrpAssertions.DataBind();
        }
        private void BindddlVerticalBranch(int Customerid)
        {
            ddlVerticalBranch.DataTextField = "Name";
            ddlVerticalBranch.DataValueField = "ID";
            ddlVerticalBranch.DataSource = UserManagementRisk.GetVerticalBranchList(Customerid);
            ddlVerticalBranch.DataBind();
            ddlVerticalBranch.Items.Insert(0, new ListItem(" Vertical ", "-1"));
        }
        private void countrybindRisk(Saplin.Controls.DropDownCheckBoxes Drpriskcategory, string Flag)
        {
            if (Flag == "P")
            {
                Drpriskcategory.DataTextField = "Name";
                Drpriskcategory.DataValueField = "ID";
                Drpriskcategory.DataSource = RiskCategoryManagement.FillRiskCategoryProcess("P");
                Drpriskcategory.DataBind();
            }
            else
            {
                Drpriskcategory.DataTextField = "Name";
                Drpriskcategory.DataValueField = "ID";
                Drpriskcategory.DataSource = RiskCategoryManagement.FillRiskCategoryProcess("N");
                Drpriskcategory.DataBind();
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("RiskControlMatrixUpdate");
                    DataTable ExcelData = null;

                    DataView view = new System.Data.DataView(BindDataExport("P").ToDataTable());
                    //DataView view = new System.Data.DataView((DataTable) Session["grdControlTestingAssignment"]);  

                    ExcelData = view.ToTable("Selected", false, "Branch", "VerticalName", "ProcessName", "SubProcessName", "ActivityName", "ControlNo", "ActivityDescription", "ControlObjective", "ControlDescription", "MControlDescription", "PersonResponsible", "Email", "ControlOwnerName", "ControlOwnerEmail", "KC1Name", "KC2Name", "TestStrategy", "RiskRating", "ControlRating", "ProcessScore", "RiskID", "RiskActivityID", "CustomerBranchId", "VerticalsId", "ProcessId", "SubProcessId", "ControlOwner", "Key_Value" , "KC2"); //"Branch", "VerticalName", "ProcessName", "SubProcessName"); // //"Frequency",

                    //"ControlOwnerName", "ControlOwnerEmail", "KC1Name", "KC2Name"
                    foreach (DataRow item in ExcelData.Rows)
                    {
                        if (item["ControlOwner"].ToString() !=null)
                        {
                            var userdetails = UserManagementRisk.GetByID(Convert.ToInt32(item["ControlOwner"].ToString()));
                            if (userdetails != null)
                            {
                                item["ControlOwnerName"] = userdetails.FirstName + " " + userdetails.LastName;
                                item["ControlOwnerEmail"] = userdetails.Email;
                            }
                        }

                        if (item["KC2"].ToString() != null)
                        {
                            if (item["KC2"].ToString() == "1")
                            {
                                item["KC1Name"] = "YES";
                            }
                            else if (item["KC2"].ToString() == "2")                                
                            {
                                item["KC1Name"] = "NO";
                            }
                        }
                        if (item["Key_Value"].ToString() != null)
                        {
                            if (item["Key_Value"].ToString() == "1")
                            {
                                item["KC2Name"] = "YES";
                            }
                            else if (item["Key_Value"].ToString() == "2")
                            {
                                item["KC2Name"] = "NO";
                            }
                        }                            
                    }
                    ExcelData.Columns.Remove("ControlOwner");
                    ExcelData.Columns.Remove("Key_Value");
                    ExcelData.Columns.Remove("KC2");
                    view = null;

                    exWorkSheet.Cells["A1"].LoadFromDataTable(ExcelData, true);

                    exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["A1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["A1"].Value = "Branch";
                    exWorkSheet.Cells["A1"].AutoFitColumns(20);

                    exWorkSheet.Cells["B1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["B1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["B1"].Value = "Vertical";
                    exWorkSheet.Cells["B1"].AutoFitColumns(20);

                    exWorkSheet.Cells["C1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["C1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["C1"].Value = "Process";
                    exWorkSheet.Cells["C1"].AutoFitColumns(30);

                    exWorkSheet.Cells["D1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["D1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["D1"].Value = "Sub Process";
                    exWorkSheet.Cells["D1"].AutoFitColumns(30);

                    exWorkSheet.Cells["E1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["E1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["E1"].Value = "Activity";
                    exWorkSheet.Cells["E1"].AutoFitColumns(30);

                    exWorkSheet.Cells["F1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["F1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["F1"].Value = "ControlNo";
                    exWorkSheet.Cells["F1"].AutoFitColumns(15);

                    exWorkSheet.Cells["G1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["G1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["G1"].Value = "Risk Description";
                    exWorkSheet.Cells["G1"].AutoFitColumns(30);

                    exWorkSheet.Cells["H1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H1"].Value = "Control Objective";
                    exWorkSheet.Cells["H1"].AutoFitColumns(30);

                    exWorkSheet.Cells["I1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["I1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["I1"].Value = "Control Description";
                    exWorkSheet.Cells["I1"].AutoFitColumns(30);

                    exWorkSheet.Cells["J1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["J1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["J1"].Value = "Mitigating Control Description";
                    exWorkSheet.Cells["J1"].AutoFitColumns(15);

                    exWorkSheet.Cells["K1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["K1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["K1"].Value = "Person Responsible";
                    exWorkSheet.Cells["K1"].AutoFitColumns(20);

                    exWorkSheet.Cells["L1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["L1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["L1"].Value = "Email";
                    exWorkSheet.Cells["L1"].AutoFitColumns(30);                

                    exWorkSheet.Cells["M1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["M1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["M1"].Value = "Control Owner";
                    exWorkSheet.Cells["M1"].AutoFitColumns(20);

                    exWorkSheet.Cells["N1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["N1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["N1"].Value = "Email";
                    exWorkSheet.Cells["N1"].AutoFitColumns(30);

                    exWorkSheet.Cells["O1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["O1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["O1"].Value = "KC -1";
                    exWorkSheet.Cells["O1"].AutoFitColumns(10);

                    exWorkSheet.Cells["P1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["P1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["P1"].Value = "KC -2";
                    exWorkSheet.Cells["P1"].AutoFitColumns(10);

                    exWorkSheet.Cells["Q1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["Q1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["Q1"].Value = "Test Strategy";
                    exWorkSheet.Cells["Q1"].AutoFitColumns(20);

                    exWorkSheet.Cells["R1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["R1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["R1"].Value = "Risk Rating";
                    exWorkSheet.Cells["R1"].AutoFitColumns(10);

                    exWorkSheet.Cells["S1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["S1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["S1"].Value = "Control Rating";
                    exWorkSheet.Cells["S1"].AutoFitColumns(10);

                    exWorkSheet.Cells["T1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["T1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["T1"].Value = "Process Score";
                    exWorkSheet.Cells["T1"].AutoFitColumns(5);

                    exWorkSheet.Cells["U1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["U1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["U1"].Value = "RiskID";
                    exWorkSheet.Cells["U1"].AutoFitColumns(5);

                    exWorkSheet.Cells["V1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["V1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["V1"].Value = "RATID";
                    exWorkSheet.Cells["V1"].AutoFitColumns(5);

                       
                    exWorkSheet.Cells["W1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["W1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["W1"].Value = "BID";
                    exWorkSheet.Cells["W1"].AutoFitColumns(5);

                    exWorkSheet.Cells["X1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["X1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["X1"].Value = "VID";
                    exWorkSheet.Cells["X1"].AutoFitColumns(5);

                    exWorkSheet.Cells["Y1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["Y1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["Y1"].Value = "PID";
                    exWorkSheet.Cells["Y1"].AutoFitColumns(5);

                    exWorkSheet.Cells["Z1"].Style.Font.Bold = true;
                    exWorkSheet.Cells["Z1"].Style.Font.Size = 12;
                    exWorkSheet.Cells["Z1"].Value = "SPID";
                    exWorkSheet.Cells["Z1"].AutoFitColumns(5);

                    

                    using (ExcelRange col = exWorkSheet.Cells[1, 1, 1 + ExcelData.Rows.Count, 26])
                    {
                        col.Style.WrapText = true;
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        //col.Style.Numberformat.Format = "dd/MMM/yyyy";
                        //col.AutoFitColumns();

                        //Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }

                    //exWorkSheet.Protection.IsProtected = true;
                    //exWorkSheet.Protection.AllowSort= true;
                    //exWorkSheet.Protection.AllowAutoFilter = true;
                    //exWorkSheet.Protection.AllowDeleteRows = true;

                    //exWorkSheet.Cells["A:L"].Style.Locked = false;
                    //exWorkSheet.Cells["A:L"].AutoFilter = true;

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=RiskControlMatrixExportForUpdate.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw;
            }
        }


        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                           
                BindGridData();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdRiskActivityMatrix.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                // GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

       
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        protected void lbtEdit_Click(object sender, EventArgs e)
        {

        }

        public void BindDropDown()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            if (ddlClientProcess != null)
            {
                ddlClientProcess.DataTextField = "Name";
                ddlClientProcess.DataValueField = "ID";
                ddlClientProcess.DataSource = UserManagementRisk.FillCustomerNew(customerID);
                ddlClientProcess.DataBind();
                ddlClientProcess.Items.Insert(0, new ListItem(" Select Branch ", "-1"));
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomerBranchName"])))
                {
                    ddlClientProcess.Items.FindByText(ViewState["CustomerBranchName"].ToString().Trim()).Selected = true;
                }
            }
            this.countrybindRisk(ddlRiskCategoryCheck, "P");
            this.countrybindAssertions(ddlAssertions);
            this.countrybindIndustory(ddlIndultory);
            BindddlVerticalBranch(customerID);
            if (ddlPersonResponsible != null)
            {
                var userdata= RiskCategoryManagement.FillUsers(customerID);
                if (userdata !=null)
                {
                    ddlPersonResponsible.DataTextField = "Name";
                    ddlPersonResponsible.DataValueField = "ID";
                    ddlPersonResponsible.DataSource = userdata;
                    ddlPersonResponsible.DataBind();
                    ddlPersonResponsible.Items.Insert(0, new ListItem(" Select User ", "-1"));



                    ddlControlOwner.DataTextField = "Name";
                    ddlControlOwner.DataValueField = "ID";
                    ddlControlOwner.DataSource = userdata;
                    ddlControlOwner.DataBind();
                    ddlControlOwner.Items.Insert(0, new ListItem(" Select User ", "-1"));
                }
              
            }
            if (ddlKey != null)
            {
                var data = RiskCategoryManagement.FillKey();
                if (data !=null)
                {
                    ddlKey.DataTextField = "Name";
                    ddlKey.DataValueField = "ID";
                    ddlKey.DataSource = data;
                    ddlKey.DataBind();
                    ddlKey.Items.Insert(0, new ListItem(" Select KC2 ", "-1"));

                    ddlKC1.DataTextField = "Name";
                    ddlKC1.DataValueField = "ID";
                    ddlKC1.DataSource = data;
                    ddlKC1.DataBind();
                    ddlKC1.Items.Insert(0, new ListItem(" Select KC1 ", "-1"));

                }
               
            }
            if (ddlPreventive != null)
            {
                ddlPreventive.DataTextField = "Name";
                ddlPreventive.DataValueField = "ID";
                ddlPreventive.DataSource = RiskCategoryManagement.FillPreventiveControl();
                ddlPreventive.DataBind();
                ddlPreventive.Items.Insert(0, new ListItem(" Select Control ", "-1"));
            }
            if (ddlAutomated != null)
            {
                ddlAutomated.DataTextField = "Name";
                ddlAutomated.DataValueField = "ID";
                ddlAutomated.DataSource = RiskCategoryManagement.FillAutomatedControl();
                ddlAutomated.DataBind();
                ddlAutomated.Items.Insert(0, new ListItem(" Select Control ", "-1"));
            }
            if (ddlPrimarySecondary != null)
            {
                ddlPrimarySecondary.DataTextField = "Name";
                ddlPrimarySecondary.DataValueField = "ID";
                ddlPrimarySecondary.DataSource = RiskCategoryManagement.FillPrimarySecondary();
                ddlPrimarySecondary.DataBind();
                ddlPrimarySecondary.Items.Insert(0, new ListItem(" Select Primary/Secondary ", "-1"));
            }
            
            if (ddlFrequency != null)
            {
                ddlFrequency.DataTextField = "Name";
                ddlFrequency.DataValueField = "ID";
                ddlFrequency.DataSource = RiskCategoryManagement.FillFrequency();
                ddlFrequency.DataBind();
                ddlFrequency.Items.Insert(0, new ListItem(" Select Frequency ", "-1"));
            }
            if (ddlRiskRating != null)
            {
                ddlRiskRating.Items.Clear();
                ddlRiskRating.DataTextField = "Name";
                ddlRiskRating.DataValueField = "ID";
                ddlRiskRating.DataSource = RiskCategoryManagement.FillRiskRating();
                ddlRiskRating.DataBind();
                ddlRiskRating.Items.Insert(0, new ListItem(" Select Risk Rating ", "-1"));
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Risk"])))
                {
                    ddlRiskRating.Items.FindByText(ViewState["Risk"].ToString().Trim()).Selected = true;
                }
            }
            if (ddlControlRating != null)
            {
                ddlControlRating.Items.Clear();
                ddlControlRating.DataTextField = "Name";
                ddlControlRating.DataValueField = "ID";
                ddlControlRating.DataSource = RiskCategoryManagement.FillControlRating();
                ddlControlRating.DataBind();
                ddlControlRating.Items.Insert(0, new ListItem(" Select Control Rating ", "-1"));
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Control"])))
                {
                    ddlControlRating.Items.FindByText(ViewState["Control"].ToString().Trim()).Selected = true;
                }
            }
        }    
    }
}