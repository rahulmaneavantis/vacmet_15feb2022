﻿<%@ Page Title="My Workspace" Language="C#" MasterPageFile="~/AuditTool.Master" AutoEventWireup="true" Async="true" EnableEventValidation="false" CodeBehind="AuditStatusUI.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.AuditStatusUI" %>


<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        span#ContentPlaceHolder1_rdRiskActivityProcess > Label {
            margin-left: 5px;
            font-family: 'Roboto',sans-serif;
            color: #8e8e93;
        }

        input#ContentPlaceHolder1_rdRiskActivityProcess_1 {
            margin-left: 12px;
            font-family: 'Roboto',sans-serif;
            color: #8e8e93;
        }
    </style>

    <style type="text/css">
        .dd_chk_select {
            height: 81px;
            height: 3px !important;
            /*text-align: center;*/
            border-radius: 4px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            line-height: 1.428571429;
            color: #8e8e93 !important;
            background-color: #fff !important;
            border: 1px solid #c7c7cc !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            font-family: Roboto sans-serif !important;
            margin-bottom: 0px !important;
        }

        .chosen-results {
            max-height: 75px !important;
        }

        .chosen-container-single .chosen-single {
            background: none !important;
            border: 1px solid #c7c7cc !important;
            height: 32px !important;
            padding: 3px 0px 0px 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script type="text/javascript">
        function ShowDialog(RID, UID, AuditID) {
            $('#divPreShowDialog').modal('show');
            $('#Preshowdetails').attr('width', '98%');
            $('#Preshowdetails').attr('height', '600px');
            $('.modal-dialog').css('width', '98%');
            $('#Preshowdetails').attr('src', "../AuditTool/CheckListPreReqsite.aspx?RoID=" + RID + "&USID=" + UID + "&AuditID=" + AuditID);
        };
        $(document).ready(function () {
            setactivemenu('leftworkspacemenu');
            var filterbytype = ReadQuerySt('Status');
            if (filterbytype == '') {
                fhead('My Workspace');
            } else {
                //$('#pagetype').css("font-size", "20px")
                if (filterbytype == 'Open') {
                    filterbytype = 'Open Audits';
                } else if (filterbytype == 'Closed') {
                    filterbytype = 'Closed Audits';
                }
                fhead('My Workspace/ ' + filterbytype);
            }
        });

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            BindControls();
        });
        function InitializeRequest(sender, args) { }
        function EndRequest(sender, args) { BindControls(); }

        function BindControls() {
            // JQUERY DATE PICKER.
            var now = new Date();
            var firstDayPrevMonth = new Date(now.getFullYear(), now.getMonth() - 1, 1);
            var diffDays = now.getDate() - firstDayPrevMonth.getDate();
            if (diffDays > 15) {
                var firstDayPrevMonth = new Date(now.getFullYear(), now.getMonth(), 1);
            }

            //
            $(function () {
                $('input[id*=txtStartDate]').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd-mm-yy',
                    numberOfMonths: 1,
                    //minDate: firstDayPrevMonth,
                });
            });

            $(function () {
                $('input[id*=txtEndDate]').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd-mm-yy',
                    //minDate: firstDayPrevMonth,
                });
            });
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upComplianceTypeList" runat="server">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">
                    <div class="col-lg-12 col-md-12 ">
                        <section class="panel">                              
                             <div class="clearfix"></div>                              

                                 <header class="panel-heading tab-bg-primary ">
                                      <ul id="rblRole1" class="nav nav-tabs">
                                           <%if (roles.Contains(3))%>
                                           <%{%>
                                        <li class="active" id="liPerformer" runat="server">
                                            <asp:LinkButton ID="lnkPerformer" OnClick="ShowPerformer" CausesValidation="false" runat="server">Performer</asp:LinkButton>                                           
                                        </li>
                                           <%}%>
                                            <%if (roles.Contains(4) || roles.Contains(5))%>
                                           <%{%>
                                        <li class=""  id="liReviewer" runat="server">
                                            <asp:LinkButton ID="lnkReviewer" OnClick="ShowReviewer" CausesValidation="false" runat="server">Reviewer</asp:LinkButton>                                        
                                        </li>
                                          <%}%>
                                                                                 
                                    </ul>
                                </header> 
                              
                                 <div class="clearfix"></div> 

                            <div class="panel-body">

                                 <div class="col-md-12 colpadding0">
                                   <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-block alert-danger fade in" 
                                       ValidationGroup="ComplianceInstanceValidationGroup" />
                                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                        ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                                    <asp:Label ID="lblErrorMessage" runat="server" Style="color: Red"></asp:Label>
                                 </div>

                                 <div class="col-md-12 colpadding0">
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%;">
                                        <div class="col-md-2 colpadding0" style="width: 20%;">
                                        <p style="color: #999; margin-top: 5px;">Show </p>
                                        </div>
                                        <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 70px;" 
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" >                        
                                        <asp:ListItem Text="5" />
                                        <asp:ListItem Text="10" />
                                        <asp:ListItem Text="20" Selected="True" />
                                        <asp:ListItem Text="50" />
                                    </asp:DropDownList>
                                    </div>  
                                    <div class="col-md-3 colpadding0" style="color: #999; padding-top: 9px;width: 24%; display:none;" >
                                    <asp:RadioButtonList runat="server" ID="rdRiskActivityProcess" RepeatDirection="Horizontal" AutoPostBack="true" ForeColor="Black"
                                        RepeatLayout="Flow" OnSelectedIndexChanged="rdRiskActivityProcess_SelectedIndexChanged" Visible="false">
                                        <asp:ListItem Text="Process"  Value="Process" Selected="True" />
                                        <asp:ListItem Text="Non Process" Value="Non Process" />
                                    </asp:RadioButtonList>
                                </div>

                                      <div class="col-md-3 colpadding0 entrycount" style="margin-top:5px;width: 20%;">                                    
                                     <asp:DropDownListChosen runat="server" ID="ddlLegalEntity" DataPlaceHolder="Unit" Width="95%"
                                          AllowSingleDeselect="false" DisableSearchThreshold="3" class="form-control m-bot15 select_location" Style="float: left; width:90%;" AutoPostBack="true" OnSelectedIndexChanged="ddlLegalEntity_SelectedIndexChanged">
                                            </asp:DropDownListChosen>
                                </div>

                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%">                                    
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity1" DataPlaceHolder="Sub Unit" Width="95%"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3" class="form-control m-bot15 select_location" Style="float: left; width:90%;" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity1_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                    
                                </div>
                                      
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%">                                   
                                    <asp:DropDownListChosen runat="server" ID="ddlSubEntity2" DataPlaceHolder="Sub Unit" Width="95%"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  class="form-control m-bot15 select_location" Style="float: left; width:90%;" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity2_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                      
                                </div>  
                                     
                                     <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%">                                    
                                     <asp:DropDownListChosen runat="server" ID="ddlSubEntity3" class="form-control m-bot15 select_location" Width="95%" DataPlaceHolder="Sub Unit"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3"  Style="float: left; width:90%;" AutoPostBack="true" OnSelectedIndexChanged="ddlSubEntity3_SelectedIndexChanged">
                                        </asp:DropDownListChosen>                                    
                                </div>                      
                                 </div>   
                                                           
                                <div class="clearfix"></div> 

                                <div class="col-md-12 colpadding0">                                       
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlFilterLocation" AutoPostBack="true" Width="95%" DataPlaceHolder="Location"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlFilterLocation_SelectedIndexChanged" class="form-control m-bot15 select_location" Style="float: left; width:90%;">
                                        </asp:DropDownListChosen>
                                    </div> 
                                               
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%">                     
                                        <asp:DropDownListChosen ID="ddlFinancialYear" runat="server" AutoPostBack="true" Width="95%" DataPlaceHolder="Financial Year"
                                          AllowSingleDeselect="false" DisableSearchThreshold="3"  class="form-control m-bot15 select_location" Style="float: left; width:90%;" OnSelectedIndexChanged="ddlFinancialYear_SelectedIndexChanged">
                                        </asp:DropDownListChosen>
                                        <asp:CompareValidator ErrorMessage="Please Select Financial Year." ControlToValidate="ddlFinancialYear"
                                        runat="server" ValueToCompare="-1" Operator="NotEqual" Display="None" />                    
                                    </div>    
                                    
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlSchedulingType" AutoPostBack="true" Width="95%" DataPlaceHolder="Scheduling Type"
                                       AllowSingleDeselect="false" DisableSearchThreshold="3" OnSelectedIndexChanged="ddlSchedulingType_SelectedIndexChanged" class="form-control m-bot15 select_location" Style="float: left; width:90%;">
                                        </asp:DropDownListChosen>
                                    </div>
                                    
                                    <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%">                     
                                        <asp:DropDownListChosen runat="server" ID="ddlPeriod" AutoPostBack="true" Width="95%" DataPlaceHolder="Period"
                                      AllowSingleDeselect="false" DisableSearchThreshold="3"  OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged" class="form-control m-bot15 select_location" Style="float: left; width:90%;">
                                        </asp:DropDownListChosen>
                                    </div> 
                                      <% if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 1)%>
                                      <%{%>
                                            <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px;width: 20%">
                                                <asp:DropDownListChosen runat="server" ID="ddlVertical" AutoPostBack="true" Width="95%" DataPlaceHolder="Vertical"
                                                AllowSingleDeselect="false" DisableSearchThreshold="3"    OnSelectedIndexChanged="ddlVertical_SelectedIndexChanged" class="form-control m-bot15 select_location" Style="float: left; width:90%;" >
                                                </asp:DropDownListChosen>  
                                            </div>   
                                     <%}%>                                                      
                                </div>  

                             <div class="clearfix"></div>                                                                 
                                <div style="margin-bottom: 4px">   
                                    &nbsp;                                    
                                      <asp:GridView runat="server" ID="grdRiskActivityMatrix" AutoGenerateColumns="false" 
                                     PageSize="20" 
                                    AllowPaging="true" AutoPostBack="true" ShowHeaderWhenEmpty="true"
                                    CssClass="table" GridLines="None" Width="100%" AllowSorting="true"
                                    OnPageIndexChanging="grdRiskActivityMatrix_PageIndexChanging" ShowFooter="true"
                                         OnRowCommand="grdRiskActivityMatrix_RowCommand" OnRowDataBound="grdRiskActivityMatrix_RowBound">
                                    <Columns>                                       
                                        <asp:TemplateField HeaderText="AuditName">
                                            <ItemTemplate>                                              
                                                 <div class="text_NlinesusingCSS" style="width: 200px;">                                                
                                                <asp:Label ID="lblAuditName" runat="server" Text='<%# Eval("AuditName") %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# Eval("AuditName") %>'></asp:Label>
                                                   <asp:Label ID="AuditId" Visible="false" runat="server" Text='<%# Eval("AuditId") %>'></asp:Label>
                                                       </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                          <asp:TemplateField HeaderText="Process" >
                                            <ItemTemplate>                                                  
                                                <div class="text_NlinesusingCSS" style="width: 200px;">
                                                <asp:Label ID="lblProcess" runat="server" Text='<%# GetProcessName(Convert.ToInt32(Eval("AuditID"))) %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# GetProcessName(Convert.ToInt32(Eval("AuditID"))) %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SubProcess" >
                                            <ItemTemplate>                                                  
                                                <div class="text_NlinesusingCSS" style="width: 200px;">
                                                <asp:Label ID="lblSubProcess" runat="server" Text='<%# GetSubProcessName(Convert.ToInt32(Eval("AuditID"))) %>' data-toggle="tooltip" data-placement="top" ToolTip='<%# GetSubProcessName(Convert.ToInt32(Eval("AuditID"))) %>'></asp:Label>
                                                     </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                                                                                                                     
                                        <asp:TemplateField HeaderText="Audit Start Date">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtStartDate" class="form-control" style="width: 110px; text-align: center;" runat="server" Enabled='<%# CheckEnable(Eval("Role").ToString(),Eval("ExpectedStartDate")!=null?Eval("ExpectedStartDate").ToString():"",Eval("ExpectedEndDate")!=null?Eval("ExpectedEndDate").ToString():"") %>' 
                                                    Text='<%# Eval("ExpectedStartDate")!=null? Convert.ToDateTime(Eval("ExpectedStartDate")).ToString("dd-MMM-yyyy"):"" %>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>  

                                        <asp:TemplateField HeaderText="Expected End Date">
                                            <ItemTemplate>
                                               <asp:TextBox ID="txtEndDate" class="form-control" runat="server" style="width: 110px; text-align: center;" Enabled='<%# CheckEnable(Eval("Role").ToString(),Eval("ExpectedStartDate")!=null?Eval("ExpectedStartDate").ToString():"",Eval("ExpectedEndDate")!=null?Eval("ExpectedEndDate").ToString():"") %>' 
                                                   Text='<%# Eval("ExpectedStartDate")!=null?Convert.ToDateTime(Eval("ExpectedEndDate")).ToString("dd-MMM-yyyy"):"" %>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                             

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%" HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSave" runat="server" CommandName="SaveDate"  CommandArgument='<%# Eval("CustomerBranchID") + "," + Eval("ForMonth") + "," + Eval("FinancialYear") +"," + Eval("RoleID") +"," + Eval("UserID")+","+Eval("VerticalId")+","+Eval("AuditID")  %>'
                                                    data-toggle="tooltip" data-placement="top" ToolTip="Save Audit Date Details"
                                                    Visible='<%# CheckEnable(Eval("Role").ToString(),Eval("ExpectedStartDate")!=null?Eval("ExpectedStartDate").ToString():"",Eval("ExpectedEndDate")!=null?Eval("ExpectedEndDate").ToString():"") %>' 
                                                    CausesValidation="false"><img src="../../Images/Save-icon.png" /></asp:LinkButton>     <%--  --%>                         
                                                
                                                  <asp:LinkButton ID="lnkprerequsiteDetails" runat="server" CommandName="ViewprerequsiteSummary" CommandArgument='<%# Eval("RoleID") +"," + Eval("UserID")+","+Eval("AuditID")  %>'
                                                      data-toggle="tooltip" data-placement="top" ToolTip="View Pre-Requsite Details"
                                                    CausesValidation="false"><img src="../../Images/icon_viewA.png" /></asp:LinkButton> 

                                                <asp:LinkButton ID="lnkAuditDetails" runat="server" CommandName="ViewAuditStatusSummary" CommandArgument='<%# Eval("CustomerBranchID") + "," + Eval("ForMonth") + "," + Eval("FinancialYear") +"," + Eval("RoleID") +"," + Eval("UserID")+","+Eval("VerticalId")+","+Eval("AuditID")  %>'
                                                    data-toggle="tooltip" data-placement="top" ToolTip="View Audit Details"
                                                    CausesValidation="false"  Visible='<%# CheckActionButtonEnable(Eval("Role").ToString(),Eval("ExpectedStartDate")!=null?Eval("ExpectedStartDate").ToString():"",Eval("ExpectedEndDate")!=null?Eval("ExpectedEndDate").ToString():"") %>' >
                                                    <img src="../../Images/View-icon-new.png" /></asp:LinkButton> <%--CommandArgument='<%# Eval("InternalAuditInstanceID") %>' --%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="clsROWgrid" />
                                    <HeaderStyle CssClass="clsheadergrid" />     
                                    <HeaderStyle BackColor="#ECF0F1" />
                                           <PagerSettings Visible="false" />                 
                                    <PagerTemplate>                                      
                                    </PagerTemplate>
                                     <EmptyDataTemplate>
                                          No Records Found.
                                     </EmptyDataTemplate> 
                                </asp:GridView>
                                <div style="float: right;">
                                    <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true"
                                    class="form-control m-bot15"  Width="120%" Height="30px"  OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged">                                   
                                    </asp:DropDownListChosen>  
                                </div>
                                </div>
                              
                            <div class="col-md-12 colpadding0">
                            <div class="col-md-5 colpadding0">
                                <div class="table-Selecteddownload">
                                    <div class="table-Selecteddownload-text">
                                        <p><asp:Label runat="server" ID="lblTotalSelected" Text="" style="color: #999; margin-right: 10px;"></asp:Label></p>
                                    </div>                                   
                                </div>
                            </div>

                            <div class="col-md-6 colpadding0" style="float:right;">
                                <div class="table-paging" style="margin-bottom: 10px;">
                                   
                                    <div class="table-paging-text" style="float: right;">
                                        <p>
                                            Page
                                      
                                        </p>
                                    </div>
                                   
                                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                                </div>
                            </div>
                        </div>           
                       </section>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div class="modal fade" id="divPreShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" onclick="javascript:window.location.reload()" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <iframe id="Preshowdetails" src="about:blank" width="95%" height="100%" frameborder="0" style="margin-left: 9px;"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
