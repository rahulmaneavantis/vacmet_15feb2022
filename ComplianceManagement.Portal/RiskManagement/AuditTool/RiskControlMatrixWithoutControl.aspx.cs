﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using Saplin.Controls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class RiskControlMatrixWithoutControl : System.Web.UI.Page
    {
        public static List<int> IndustryIdProcess = new List<int>();
        public static List<int> RiskIdProcess = new List<int>();
        public static List<int> AssertionsIdProcess = new List<int>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomer();               
                BindLocationType();            
                GetPageDisplaySummary();
                BindGridData();
                BindDropDown();
                BindLocationTypePopPup();
            }
        }

        public void BindGridData()
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                    && ddlFilterSubProcess.SelectedItem.Text != "All"/* && ddlFilterRiskCategory.SelectedItem.Text != "All"*/
                        /*&& ddlFilterIndustry.SelectedItem.Text != "All"*/ && ddlFilterLocationType.SelectedItem.Text != "All")
                {
                    BindData("P", "L");
                }
                //else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                //&& ddlFilterSubProcess.SelectedItem.Text != "All" && ddlFilterRiskCategory.SelectedItem.Text != "All"
                //&& ddlFilterIndustry.SelectedItem.Text != "All")
                //{
                //    BindData("P", "I");
                //}
                //else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                //&& ddlFilterSubProcess.SelectedItem.Text != "All" && ddlFilterRiskCategory.SelectedItem.Text != "All")
                //{
                //    BindData("P", "R");
                //}
                else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                && ddlFilterSubProcess.SelectedItem.Text != "All")
                {
                    BindData("P", "S");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All")
                {
                    BindData("P", "P");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "All")
                {
                    BindData("P", "B");
                }
                else
                {
                    BindData("P", "A");
                }
            }
            else if (rdRiskActivityProcess.SelectedItem.Text == "Others")
            {
                if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
               && ddlFilterSubProcess.SelectedItem.Text != "All" /*&& ddlFilterRiskCategory.SelectedItem.Text != "All"*/
                  /* && ddlFilterIndustry.SelectedItem.Text != "All"*/ && ddlFilterLocationType.SelectedItem.Text != "All")
                {
                    BindData("N", "L");
                }
                //else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                //&& ddlFilterSubProcess.SelectedItem.Text != "All" && ddlFilterRiskCategory.SelectedItem.Text != "All"
                //&& ddlFilterIndustry.SelectedItem.Text != "All")
                //{
                //    BindData("N", "I");
                //}
                //else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                //&& ddlFilterSubProcess.SelectedItem.Text != "All" && ddlFilterRiskCategory.SelectedItem.Text != "All")
                //{
                //    BindData("N", "R");
                //}
                else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                && ddlFilterSubProcess.SelectedItem.Text != "All")
                {
                    BindData("N", "S");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All")
                {
                    BindData("N", "P");
                }
                else if (ddlFilterLocation.SelectedItem.Text != "All")
                {
                    BindData("N", "B");
                }
                else
                {
                    BindData("N", "A");
                }
            }
        }
        public void BindCustomer()
        {
            int customerID = -1;
            //customerID = UserManagementRisk.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlFilterLocation.DataTextField = "Name";
            ddlFilterLocation.DataValueField = "ID";
            ddlFilterLocation.Items.Clear();
            ddlFilterLocation.DataSource = UserManagementRisk.FillCustomerNew(customerID);
            ddlFilterLocation.DataBind();
            ddlFilterLocation.Items.Insert(0, new ListItem("All", "-1"));
            ddlFilterProcess.Items.Insert(0, new ListItem("All", "-1"));
            ddlFilterSubProcess.Items.Insert(0, new ListItem("All", "-1"));
        }
        private void BindProcessPopPup(string flag)
        {
            try
            {
                if (flag == "P")
                {
                    var process = ProcessManagement.FillProcess("P", Convert.ToInt32(ddlClientProcess.SelectedValue));

                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = process;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("All", "-1"));
                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcessPoPup(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                    }
                }
                else
                {
                    var process = ProcessManagement.FillProcess("N", Convert.ToInt32(ddlClientProcess.SelectedValue));

                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = process;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("All", "-1"));
                    if (!String.IsNullOrEmpty(ddlProcess.SelectedValue))
                    {
                        BindSubProcessPoPup(Convert.ToInt32(ddlProcess.SelectedValue), "N");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindProcessPopPup(int Branchid,int Processid,string flag)
        {
            try
            {
                if (flag == "P")
                {
                    var process = ProcessManagement.FillProcess("P", Convert.ToInt32(Branchid));

                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = process;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("All", "-1"));

                    BindSubProcessPoPup(Convert.ToInt32(Processid), "P");

                }
                else
                {
                    var process = ProcessManagement.FillProcess("N", Convert.ToInt32(Branchid));

                    ddlSubProcess.Items.Clear();
                    ddlProcess.Items.Clear();
                    ddlProcess.DataTextField = "Name";
                    ddlProcess.DataValueField = "Id";
                    ddlProcess.DataSource = process;
                    ddlProcess.DataBind();
                    ddlProcess.Items.Insert(0, new ListItem("All", "-1"));
                    BindSubProcessPoPup(Convert.ToInt32(Processid), "N");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindProcess(string flag)
        {
            try
            {
                if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1)
                {
                    if (flag == "P")
                    {
                        var process= ProcessManagement.FillProcess("P", Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        ddlFilterSubProcess.Items.Clear();
                        ddlFilterProcess.Items.Clear();
                        ddlFilterProcess.DataTextField = "Name";
                        ddlFilterProcess.DataValueField = "Id";
                        ddlFilterProcess.DataSource = process;
                        ddlFilterProcess.DataBind();
                        ddlFilterProcess.Items.Insert(0, new ListItem("All", "-1"));
                        if (!String.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                        {
                            BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "P");
                        }                       
                    }
                    else
                    {
                        var process = ProcessManagement.FillProcess("N", Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        ddlFilterSubProcess.Items.Clear();
                        ddlFilterProcess.Items.Clear();
                        ddlFilterProcess.DataTextField = "Name";
                        ddlFilterProcess.DataValueField = "Id";
                        ddlFilterProcess.DataSource = process;
                        ddlFilterProcess.DataBind();
                        ddlFilterProcess.Items.Insert(0, new ListItem("All", "-1"));
                        if (!String.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                        {
                            BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "N");
                        }                        
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindSubProcess(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlFilterSubProcess.Items.Clear();
                    ddlFilterSubProcess.DataTextField = "Name";
                    ddlFilterSubProcess.DataValueField = "Id";
                    ddlFilterSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlFilterSubProcess.DataBind();
                    ddlFilterSubProcess.Items.Insert(0, new ListItem("All", "-1"));
                }
                else
                {
                    ddlFilterSubProcess.Items.Clear();
                    ddlFilterSubProcess.DataTextField = "Name";
                    ddlFilterSubProcess.DataValueField = "Id";
                    ddlFilterSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlFilterSubProcess.DataBind();
                    ddlFilterSubProcess.Items.Insert(0, new ListItem("All", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindSubProcessPoPup(long Processid, string flag)
        {
            try
            {
                if (flag == "P")
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("All", "-1"));
                }
                else
                {
                    ddlSubProcess.Items.Clear();
                    ddlSubProcess.DataTextField = "Name";
                    ddlSubProcess.DataValueField = "Id";
                    ddlSubProcess.DataSource = ProcessManagement.FillSubProcess(Processid, flag);
                    ddlSubProcess.DataBind();
                    ddlSubProcess.Items.Insert(0, new ListItem("All", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public void BindLocationType()
        {
            long CustomerID = Portal.Common.AuthenticationHelper.CustomerID;
            ddlFilterLocationType.DataTextField = "Name";
            ddlFilterLocationType.DataValueField = "ID";
            ddlFilterLocationType.Items.Clear();
            ddlFilterLocationType.DataSource = ProcessManagement.FillLocationType(CustomerID);
            ddlFilterLocationType.DataBind();
            ddlFilterLocationType.Items.Insert(0, new ListItem("All", "-1"));
        }
        public void BindLocationTypePopPup()
        {
            long CustomerID = Portal.Common.AuthenticationHelper.CustomerID;
            ddlLocationType.DataTextField = "Name";
            ddlLocationType.DataValueField = "ID";
            ddlLocationType.Items.Clear();
            ddlLocationType.DataSource = ProcessManagement.FillLocationType(CustomerID);
            ddlLocationType.DataBind();
            ddlLocationType.Items.Insert(0, new ListItem(" Select Location Type ", "-1"));
        }
   

        protected void ddlClientProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindProcessPopPup("P");               
            }
            else
            {               
                BindProcessPopPup("N");               
            }
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindProcess("P");
                BindData("P", "B");              
            }
            else
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindProcess("N");
                BindData("N", "B");               
            }
        }

        protected void ddlFilterProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                if (ddlFilterProcess.SelectedValue != "All" || ddlFilterProcess.SelectedValue != "" || ddlFilterProcess.SelectedValue != null)
                {
                    BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "P");
                }

                BindData("P", "P");
            }
            else
            {
                if (ddlFilterProcess.SelectedValue != "All" || ddlFilterProcess.SelectedValue != "" || ddlFilterProcess.SelectedValue != null)
                {
                    BindSubProcess(Convert.ToInt32(ddlFilterProcess.SelectedValue), "N");
                }
                BindData("N", "P");
            }
        }
        protected void ddlProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                if (ddlProcess.SelectedValue != "All" || ddlProcess.SelectedValue != "" || ddlProcess.SelectedValue != null)
                {
                    BindSubProcessPoPup(Convert.ToInt32(ddlProcess.SelectedValue), "P");
                }                
            }
            else
            {
                if (ddlProcess.SelectedValue != "All" || ddlProcess.SelectedValue != "" || ddlProcess.SelectedValue != null)
                {
                    BindSubProcessPoPup(Convert.ToInt32(ddlProcess.SelectedValue), "N");
                }             
            }
        }
        protected void ddlFilterSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                BindData("P", "S");               
            }
            else
            {
                BindData("N", "S");               
            }
        }
        protected void ddlSubProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        protected void ddlFilterRiskCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                this.BindData("P", "R");
            }
            else
            {
                this.BindData("N", "R");
            }
        }
        protected void ddlFilterIndustry_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                this.BindData("P", "I");
            }
            else
            {
                this.BindData("N", "I");
            }
        }
        protected void ddlFilterLocationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                this.BindData("P", "L");
            }
            else
            {
                this.BindData("N", "L");
            }
        }

        protected void rdRiskActivityProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindProcess("P");
                BindData("P", "A");
            }
            else
            {
                grdRiskActivityMatrix.DataSource = null;
                grdRiskActivityMatrix.DataBind();
                BindProcess("N");
                BindData("N", "A");
            }
        }
        public string ShowIndustryName(long categoryid)
        {
            List<IndustryName_Result> a = new List<IndustryName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetIndustryNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (grdRiskActivityMatrix.Rows.Count > 0)
                {
                    bool suucess = false;
                    List<RiskActivityTransaction> riskactivitytransactionlist = new List<RiskActivityTransaction>();
                    List<RiskActivityTransaction> riskactivitytransactionlist1 = new List<RiskActivityTransaction>();
                    foreach (GridViewRow g1 in grdRiskActivityMatrix.Rows)
                    {
                        string taskid = (g1.FindControl("lbltaskidItemTemplate") as Label).Text;
                        string Assertions = (g1.FindControl("lblAssertions") as Label).Text;
                        string ExistingControlDescription = (g1.FindControl("txtExistingControlDescriptionItemTemplate") as TextBox).Text;
                        string MControlDescription = (g1.FindControl("txtMControlDescriptionItemTemplate") as TextBox).Text;
                        string PersonResponsible = (g1.FindControl("ddlPersonResponsibleItemTemplate") as DropDownList).SelectedItem.Value;
                        string EffectiveDate = (g1.FindControl("txtEffectiveDateItemTemplate") as TextBox).Text;
                        string Key = (g1.FindControl("ddlKeyItemTemplate") as DropDownList).SelectedItem.Value;
                        string Preventive = (g1.FindControl("ddlPreventiveItemTemplate") as DropDownList).SelectedItem.Value;
                        string Automated = (g1.FindControl("ddlAutomatedItemTemplate") as DropDownList).SelectedItem.Value;
                        string Frequency = (g1.FindControl("ddlFrequencytemTemplate") as DropDownList).SelectedItem.Value;
                        string lblClient = (g1.FindControl("lblClient") as Label).Text;
                        string GapDescriptionItemTemplate = (g1.FindControl("txtGapDescriptionItemTemplate") as TextBox).Text;
                        string RecommendationsItemTemplate = (g1.FindControl("txtRecommendationsItemTemplate") as TextBox).Text;
                        string ActionRemediationplanItemTemplate = (g1.FindControl("txtActionRemediationplanItemTemplate") as TextBox).Text;
                        string IPEItemTemplate = (g1.FindControl("txtIPEItemTemplate") as TextBox).Text;
                        string ERPsystemItemTemplate = (g1.FindControl("txtERPsystemItemTemplate") as TextBox).Text;
                        string FRCItemTemplate = (g1.FindControl("txtFRCItemTemplate") as TextBox).Text;
                        string UniqueReferredItemTemplate = (g1.FindControl("txtUniqueReferredItemTemplate") as TextBox).Text;
                        
                        int customerbranchid = RiskCategoryManagement.GetCustomerBranchIDByName(lblClient);
                        if (!String.IsNullOrEmpty(taskid))
                        {
                            RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction();
                            riskactivitytransaction.ProcessId = Convert.ToInt32(ddlFilterProcess.SelectedValue);
                            riskactivitytransaction.SubProcessId = Convert.ToInt32(ddlFilterSubProcess.SelectedValue);
                            riskactivitytransaction.RiskCreationId = Convert.ToInt32(taskid);
                            riskactivitytransaction.Assertion = Convert.ToInt32(Assertions);
                            riskactivitytransaction.ControlDescription = ExistingControlDescription;
                            riskactivitytransaction.MControlDescription = MControlDescription;
                            riskactivitytransaction.PersonResponsible = Convert.ToInt32(PersonResponsible);
                            riskactivitytransaction.EffectiveDate = Convert.ToDateTime(EffectiveDate);
                            riskactivitytransaction.Key_Value = Convert.ToInt32(Key);
                            riskactivitytransaction.PrevationControl = Convert.ToInt32(Preventive);
                            riskactivitytransaction.AutomatedControl = Convert.ToInt32(Automated);
                            riskactivitytransaction.Frequency = Convert.ToInt32(Frequency);
                            riskactivitytransaction.CustomerBranchId = customerbranchid;
                            riskactivitytransaction.GapDescription = GapDescriptionItemTemplate;
                            riskactivitytransaction.Recommendations = RecommendationsItemTemplate;
                            riskactivitytransaction.ActionRemediationplan = ActionRemediationplanItemTemplate;
                            riskactivitytransaction.IPE = IPEItemTemplate;
                            riskactivitytransaction.ERPsystem = ERPsystemItemTemplate;
                            riskactivitytransaction.FRC = FRCItemTemplate;
                            riskactivitytransaction.UniqueReferred = UniqueReferredItemTemplate;
                            //riskactivitytransaction.TestStrategy = TestStrategyItemTemplate;
                            //riskactivitytransaction.DocumentsExamined = DocumentsExaminedItemTemplate;
                            riskactivitytransactionlist.Add(riskactivitytransaction);
                        }
                    }
                    riskactivitytransactionlist1 = riskactivitytransactionlist.Where(entry => entry.Frequency == null).ToList();
                    if (riskactivitytransactionlist1.Count == 0)
                    {
                        suucess = RiskCategoryManagement.CreateRiskActivityTransaction(riskactivitytransactionlist);
                        if (suucess == true)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Data Save successfully.";
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Error.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string ShowAssertionsName(long categoryid)
        {
            List<AssertionsName_Result> a = new List<AssertionsName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetAssertionsNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }
        public string ShowPersonResposibleName(long categoryid)
        {
            List<PersonResponsibleName_Result> a = new List<PersonResponsibleName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetPersonResponsibleNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }
        public string ShowKeyName(long categoryid)
        {
            List<KeyName_Result> a = new List<KeyName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetKeyNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }
            return processnonprocess.Trim(',');
        }
        public string ShowControlName(long categoryid)
        {
            List<ControlName_Result> a = new List<ControlName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetControlNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }
            return processnonprocess.Trim(',');
        }
        public string ShowFrequencyName(long categoryid)
        {
            List<FrequencyName_Result> a = new List<FrequencyName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetFrequencyNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }
            return processnonprocess.Trim(',');
        }
        public string ShowCustomerBranchName(long categoryid, long branchid)
        {
            List<CustomerBranchNameLocationWise_Result> a = new List<CustomerBranchNameLocationWise_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetCustomerBranchNameProcedureLocationWise(Convert.ToInt32(categoryid), Convert.ToInt32(branchid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }
        public string ShowRiskCategoryName(long categoryid)
        {
            List<RiskCategoryName_Result> a = new List<RiskCategoryName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetRiskCategoryNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }
            return processnonprocess.Trim(',');
        }
        public string ShowClientName(long branchid)
        {
            string processnonprocess = "";
            processnonprocess = ProcessManagement.GetClientnameName(branchid);
            return processnonprocess;
        }
        public string ShowProcessName(long processId)
        {
            int customerID = -1;
            customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;

            string processnonprocess = "";
            processnonprocess = ProcessManagement.GetProcessName(processId,customerID);
            return processnonprocess;
        }
        public string ShowSubProcessName(long processId, long SubprocessId)
        {
            string processnonprocess = "";
            processnonprocess = ProcessManagement.GetSubProcessName(processId, SubprocessId);
            return processnonprocess;
        }
        public string ShowRiskRatingName(int? ValueID)
        {
            string processnonprocess = "";
            if (!string.IsNullOrEmpty(Convert.ToString(ValueID)))
            {
                processnonprocess = GetRiskControlRating(Convert.ToInt32(ValueID), "R");
            }             
            return processnonprocess.Trim();
        }
        public string GetRiskControlRating(int ValueID , string flag)
        {            
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var transactionsQuery = "";
                if (flag =="R")
                {
                     transactionsQuery = (from row in entities.mst_Risk_ControlRating
                                             where row.Value == ValueID && row.IsActive == false
                                             && row.IsRiskControl == "R"
                                             select row.Name).FirstOrDefault();
                }
                else if (flag=="C")
                {
                    transactionsQuery = (from row in entities.mst_Risk_ControlRating
                                             where row.Value == ValueID && row.IsActive == false
                                             && row.IsRiskControl == "C"
                                             select row.Name).FirstOrDefault();
                }
               
                return transactionsQuery;
               
            }
        }
        public string ShowControlRatingName(int? ValueID)
        {
            string processnonprocess = "";
            if (!string.IsNullOrEmpty(Convert.ToString(ValueID)))
            {
                processnonprocess = GetRiskControlRating(Convert.ToInt32(ValueID), "C"); 
            }                  
            return processnonprocess.Trim();
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
        private void BindDataExport(string Flag, string Flag2)
        {
            try
            {
                int customerID = -1;
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                int CustomerBranchId = -1;
                int processid = -1;
                int subprocessid = -1;
                int LocationType = -1;
                int RiskCategory = -1;
                int Industry = -1;
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                {
                    if (ddlFilterProcess.SelectedValue != "-1")
                    {
                        processid = Convert.ToInt32(ddlFilterProcess.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterSubProcess.SelectedValue))
                {
                    if (ddlFilterSubProcess.SelectedValue != "-1")
                    {
                        subprocessid = Convert.ToInt32(ddlFilterSubProcess.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocationType.SelectedValue))
                {
                    if (ddlFilterLocationType.SelectedValue != "-1")
                    {
                        LocationType = Convert.ToInt32(ddlFilterLocationType.SelectedValue);
                    }
                }
                //if (!string.IsNullOrEmpty(ddlFilterRiskCategory.SelectedValue))
                //{
                //    if (ddlFilterRiskCategory.SelectedValue != "-1")
                //    {
                //        RiskCategory = Convert.ToInt32(ddlFilterRiskCategory.SelectedValue);
                //    }
                //}
                //if (!string.IsNullOrEmpty(ddlFilterIndustry.SelectedValue))
                //{
                //    if (ddlFilterIndustry.SelectedValue != "-1")
                //    {
                //        Industry = Convert.ToInt32(ddlFilterIndustry.SelectedValue);
                //    }
                //}
                if (Flag == "P")
                {
                    if (Flag2 == "A")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "All");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "B")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "Location");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "P")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "Process");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "S")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "SubProcess");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "R")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "RiskCategory");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "I")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "Industry");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "L")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "LocationType");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        GridExportExcel.DataBind();
                    }

                }
                else
                {
                    if (Flag2 == "A")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "All");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "B")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "Location");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "P")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "Process");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "S")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "SubProcess");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "R")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "RiskCategory");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "I")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "Industry");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        GridExportExcel.DataBind();
                    }
                    else if (Flag2 == "L")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "LocationType");
                        GridExportExcel.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        GridExportExcel.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void BindData(string Flag, string Flag2)
        {
            try
            {
                int customerID = -1;
                //if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role == "CADMN")
                //{
                customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                //}
                int CustomerBranchId = -1;
                int processid = -1;
                int subprocessid = -1;
                int LocationType = -1;
                int RiskCategory = -1;
                int Industry = -1;
                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustomerBranchId = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterProcess.SelectedValue))
                {
                    if (ddlFilterProcess.SelectedValue != "-1")
                    {
                        processid = Convert.ToInt32(ddlFilterProcess.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterSubProcess.SelectedValue))
                {
                    if (ddlFilterSubProcess.SelectedValue != "-1")
                    {
                        subprocessid = Convert.ToInt32(ddlFilterSubProcess.SelectedValue);
                    }
                }
                if (!string.IsNullOrEmpty(ddlFilterLocationType.SelectedValue))
                {
                    if (ddlFilterLocationType.SelectedValue != "-1")
                    {
                        LocationType = Convert.ToInt32(ddlFilterLocationType.SelectedValue);
                    }
                }
                //if (!string.IsNullOrEmpty(ddlFilterRiskCategory.SelectedValue))
                //{
                //    if (ddlFilterRiskCategory.SelectedValue != "-1")
                //    {
                //        RiskCategory = Convert.ToInt32(ddlFilterRiskCategory.SelectedValue);
                //    }
                //}
                //if (!string.IsNullOrEmpty(ddlFilterIndustry.SelectedValue))
                //{
                //    if (ddlFilterIndustry.SelectedValue != "-1")
                //    {
                //        Industry = Convert.ToInt32(ddlFilterIndustry.SelectedValue);
                //    }
                //}
                if (Flag == "P")
                {
                    if (Flag2 == "A")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "All");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "B")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "Location");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "P")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "Process");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "S")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "SubProcess");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "R")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "RiskCategory");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "I")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "Industry");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "L")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "P", "LocationType");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        grdRiskActivityMatrix.DataBind();
                    }
                    Saplin.Controls.DropDownCheckBoxes DRPIndultory = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlIndultory");
                    if (DRPIndultory != null)
                    {
                        this.countrybindIndustory(DRPIndultory);
                    }
                    Saplin.Controls.DropDownCheckBoxes DrpRiskCategoryProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlRiskCategoryCheck");
                    if (DrpRiskCategoryProcess != null)
                    {
                        this.countrybindRisk(DrpRiskCategoryProcess, "P");
                    }

                    Saplin.Controls.DropDownCheckBoxes ddlAssertionsProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlAssertions");// (e.Row.FindControl("ddlAssertionsProcess") as DropDownCheckBoxes);
                    if (ddlAssertionsProcess != null)
                    {
                        this.countrybindAssertions(ddlAssertionsProcess);
                    }                   
                }
                else
                {
                    if (Flag2 == "A")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "All");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "B")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "Location");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "P")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "Process");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "S")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "SubProcess");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "R")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "RiskCategory");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "I")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "Industry");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        grdRiskActivityMatrix.DataBind();
                    }
                    else if (Flag2 == "L")
                    {
                        var Riskcategorymanagementlist = RiskCategoryManagement.GetAllRiskControlMatrix(processid, subprocessid, CustomerBranchId, LocationType, RiskCategory, Industry, customerID, "N", "LocationType");
                        grdRiskActivityMatrix.DataSource = Riskcategorymanagementlist;
                        Session["TotalRows"] = Riskcategorymanagementlist.Count;
                        grdRiskActivityMatrix.DataBind();
                    }
                   
                    Saplin.Controls.DropDownCheckBoxes DRPIndultory = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlIndultory");
                    if (DRPIndultory != null)
                    {
                        this.countrybindIndustory(DRPIndultory);
                    }
                    Saplin.Controls.DropDownCheckBoxes DrpRiskCategoryProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlRiskCategoryCheck");
                    if (DrpRiskCategoryProcess != null)
                    {
                        this.countrybindRisk(DrpRiskCategoryProcess, "N");
                    }

                    Saplin.Controls.DropDownCheckBoxes ddlAssertionsProcess = (DropDownCheckBoxes)grdRiskActivityMatrix.FindControl("ddlAssertions");// (e.Row.FindControl("ddlAssertionsProcess") as DropDownCheckBoxes);
                    if (ddlAssertionsProcess != null)
                    {
                        this.countrybindAssertions(ddlAssertionsProcess);
                    }
                }
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string ShowDate(DateTime dt)
        {
            try
            {
                DateTime dt1 = dt;
                if (dt1 == null)
                    return String.Empty;
                else
                    return dt1.ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected void btnAddPromotor_Click(object sender, EventArgs e)
        {      
            try
            {
                ViewState["Mode"] = 0;
                tbxControl.Text = string.Empty;
                tbxRiskDesc.Text = string.Empty;
                tbxControlObj.Text = string.Empty;
                ddlRiskCategoryCheck.ClearSelection();
                ddlAssertions.ClearSelection();               
                ddlClientProcess.ClearSelection();
                tbxControlDesc.Text = string.Empty;
                txtMitigation.Text = string.Empty;
                ddlPersonResponsible.ClearSelection();
                txtStartDate.Text = string.Empty;
                ddlKey.ClearSelection();
                ddlPreventive.ClearSelection();
                ddlAutomated.ClearSelection();
                ddlFrequency.ClearSelection();
                tbxGapDesc.Text = string.Empty;
                tbxRecom.Text = string.Empty;
                tbxActionPlan.Text = string.Empty;
                tbxIPE.Text = string.Empty;
                tbxERPSystem.Text = string.Empty;
                tbxFRC.Text = string.Empty;
                tbxUnique.Text = string.Empty;
                ddlRiskRating.ClearSelection();
                ddlControlRating.ClearSelection();
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    BindProcessPopPup("P");
                }
                else
                {
                    BindProcessPopPup("N");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void btnSavePop_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                if ((int)ViewState["Mode"] == 0)
                {
                    #region  Creation    
                    string Controno = tbxControl.Text;
                    string ActivityDescription = tbxRiskDesc.Text;
                    string controlObjective = tbxControlObj.Text;
                    string MControlDescriptionFooterTemplate = txtMitigation.Text;
                    string ExistingControlDescriptionFooterTemplate = tbxControlDesc.Text;
                    string GapDescriptionFooterTemplate = tbxGapDesc.Text;
                    string RecommendationsFooterTemplate = tbxRecom.Text;
                    string ActionRemediationplanFooterTemplate = tbxActionPlan.Text;
                    string IPEFooterTemplate = tbxIPE.Text;
                    string ERPsystemFooterTemplate = tbxERPSystem.Text;
                    string FRCFooterTemplate = tbxFRC.Text;
                    string UniqueReferredFooterTemplate = tbxUnique.Text;

                    string ClientProcess = ddlClientProcess.SelectedValue;
                    long pid = -1;
                    long psid = -1;

                    if (ddlProcess.SelectedValue != "")
                    {
                        if (ddlProcess.SelectedValue != "All")
                        {
                            pid = Convert.ToInt32(ddlProcess.SelectedValue);
                        }
                    }
                    if (ddlSubProcess.SelectedValue != "")
                    {
                        if (ddlSubProcess.SelectedValue != "All")
                        {
                            psid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                        }
                    }

                    if (ActivityDescription != "" || ActivityDescription != null)
                    {
                        if (controlObjective != "" || controlObjective != null)
                        {
                            RiskCategoryCreation riskcategorycreation = new RiskCategoryCreation()
                            {
                                ControlNo = Controno,
                                ActivityDescription = ActivityDescription,
                                ControlObjective = controlObjective,
                                ProcessId = pid,
                                SubProcessId = psid,
                                IsInternalAudit = "N",
                                CustomerId = customerID,
                                CustomerBranchId = Convert.ToInt32(ddlClientProcess.SelectedValue),
                                LocationType = Convert.ToInt32(ddlLocationType.SelectedValue),
                            };
                            if (RiskCategoryManagement.Exists(riskcategorycreation))
                            {
                                cvDuplicateEntry.ErrorMessage = "Risk Category already exists.";
                                cvDuplicateEntry.IsValid = false;
                                return;
                            }
                            using (AuditControlEntities entities = new AuditControlEntities())
                            {
                                entities.RiskCategoryCreations.Add(riskcategorycreation);
                                entities.SaveChanges();
                            }
                            RiskIdProcess.Clear();
                            for (int i = 0; i < ddlRiskCategoryCheck.Items.Count; i++)
                            {
                                if (ddlRiskCategoryCheck.Items[i].Selected)
                                {
                                    RiskIdProcess.Add(Convert.ToInt32(ddlRiskCategoryCheck.Items[i].Value));
                                }
                            }
                            AssertionsIdProcess.Clear();
                            for (int i = 0; i < ddlAssertions.Items.Count; i++)
                            {
                                if (ddlAssertions.Items[i].Selected)
                                {
                                    AssertionsIdProcess.Add(Convert.ToInt32(ddlAssertions.Items[i].Value));
                                }
                            }
                            IndustryIdProcess.Clear();
                            for (int i = 0; i < ddlIndultory.Items.Count; i++)
                            {
                                if (ddlIndultory.Items[i].Selected)
                                {
                                    IndustryIdProcess.Add(Convert.ToInt32(ddlIndultory.Items[i].Value));
                                }
                            }

                            if (IndustryIdProcess.Count > 0)
                            {
                                foreach (var aItem in IndustryIdProcess)
                                {
                                    IndustryMapping IndustryMapping = new IndustryMapping()
                                    {
                                        RiskCategoryCreationId = riskcategorycreation.Id,
                                        IndustryID = Convert.ToInt32(aItem),
                                        IsActive = true,
                                        EditedDate = DateTime.Now,
                                        EditedBy = Convert.ToInt32(Session["userID"]),
                                        ProcessId = pid,
                                        SubProcessId = psid,
                                    };
                                    RiskCategoryManagement.CreateIndustryMapping(IndustryMapping);
                                }
                                IndustryIdProcess.Clear();
                            }
                            if (RiskIdProcess.Count > 0)
                            {
                                foreach (var aItem in RiskIdProcess)
                                {
                                    RiskCategoryMapping RiskCategoryMapping = new RiskCategoryMapping()
                                    {
                                        RiskCategoryCreationId = riskcategorycreation.Id,
                                        RiskCategoryId = Convert.ToInt32(aItem),
                                        ProcessId = pid,
                                        SubProcessId = psid,
                                        IsActive = true,
                                    };
                                    RiskCategoryManagement.CreateRiskCategoryMapping(RiskCategoryMapping);
                                }
                                RiskIdProcess.Clear();
                            }
                            if (AssertionsIdProcess.Count > 0)
                            {
                                foreach (var aItem in AssertionsIdProcess)
                                {
                                    AssertionsMapping AssertionsMapping = new AssertionsMapping()
                                    {
                                        RiskCategoryCreationId = riskcategorycreation.Id,
                                        AssertionId = Convert.ToInt32(aItem),
                                        ProcessId = pid,
                                        SubProcessId = psid,
                                        IsActive = true,
                                    };
                                    RiskCategoryManagement.CreateAssertionsMapping(AssertionsMapping);
                                }
                                AssertionsIdProcess.Clear();
                            }
                            RiskActivityTransaction riskactivitytransaction = new RiskActivityTransaction()
                            {
                                ControlDescription = ExistingControlDescriptionFooterTemplate,
                                MControlDescription = MControlDescriptionFooterTemplate,
                                ProcessId = pid,
                                SubProcessId = psid,
                                RiskCreationId = riskcategorycreation.Id,
                                PersonResponsible = Convert.ToInt32(ddlPersonResponsible.SelectedValue),
                                Key_Value = Convert.ToInt32(ddlKey.SelectedValue),
                                AutomatedControl = Convert.ToInt32(ddlAutomated.SelectedValue),
                                Frequency = Convert.ToInt32(ddlFrequency.SelectedValue),
                                PrevationControl = Convert.ToInt32(ddlPreventive.SelectedValue),
                                EffectiveDate = DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                IsDeleted = false,
                                CustomerBranchId = Convert.ToInt32(ddlClientProcess.SelectedValue),
                                GapDescription = GapDescriptionFooterTemplate,
                                Recommendations = RecommendationsFooterTemplate,
                                ActionRemediationplan = ActionRemediationplanFooterTemplate,
                                IPE = IPEFooterTemplate,
                                ERPsystem = ERPsystemFooterTemplate,
                                FRC = FRCFooterTemplate,
                                UniqueReferred = UniqueReferredFooterTemplate,
                                RiskRating = Convert.ToInt32(ddlRiskRating.SelectedValue),
                                ControlRating = Convert.ToInt32(ddlControlRating.SelectedValue),
                            };
                            if (RiskCategoryManagement.RiskActivityTransactionExists(riskactivitytransaction))
                            {
                                cvDuplicateEntry.ErrorMessage = "Risk Transaction already exists.";
                                cvDuplicateEntry.IsValid = false;
                                return;
                            }
                            using (AuditControlEntities entities = new AuditControlEntities())
                            {
                                entities.RiskActivityTransactions.Add(riskactivitytransaction);
                                entities.SaveChanges();
                            }

                        }
                    }
                    if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                    {
                        this.BindData("P", "A");
                    }
                    else
                    {
                        this.BindData("N", "A");
                    }
                    #endregion
                }
                else
                {
                    #region Updation
                    long pid = -1;
                    long psid = -1;
                    try
                    {
                        if (ddlProcess.SelectedValue != "-1" && ddlSubProcess.SelectedValue != "-1")
                        {
                            if (ddlProcess.SelectedItem.Text != "All")
                            {
                                pid = Convert.ToInt32(ddlProcess.SelectedValue);
                            }
                            if (ddlSubProcess.SelectedItem.Text != "All")
                            {
                                psid = Convert.ToInt32(ddlSubProcess.SelectedValue);
                            }
                            long RiskCreationId = Convert.ToInt32(ViewState["RRID"]);
                            int riskactivityid = Convert.ToInt32(ViewState["RCMID"]);
                            RiskCategoryCreation RiskData = new RiskCategoryCreation()
                            {
                                Id = RiskCreationId,
                                ActivityDescription = tbxRiskDesc.Text,
                                ControlObjective = tbxControlObj.Text,
                                LocationType = Convert.ToInt32(ddlLocationType.SelectedValue),
                            };
                            RiskCategoryManagement.RiskUpdateRCM(RiskData);
                            RiskCategoryManagement.UpdateRiskCategoryMapping(RiskCreationId);
                            RiskCategoryManagement.UpdateAssertionsMapping(RiskCreationId);
                            RiskCategoryManagement.UpdateRiskIndustryMapping(RiskCreationId);
                            RiskIdProcess.Clear();
                            for (int i = 0; i < ddlRiskCategoryCheck.Items.Count; i++)
                            {
                                if (ddlRiskCategoryCheck.Items[i].Selected)
                                {
                                    RiskIdProcess.Add(Convert.ToInt32(ddlRiskCategoryCheck.Items[i].Value));
                                }
                            }
                            AssertionsIdProcess.Clear();
                            for (int i = 0; i < ddlAssertions.Items.Count; i++)
                            {
                                if (ddlAssertions.Items[i].Selected)
                                {
                                    AssertionsIdProcess.Add(Convert.ToInt32(ddlAssertions.Items[i].Value));
                                }
                            }
                            IndustryIdProcess.Clear();
                            for (int i = 0; i < ddlIndultory.Items.Count; i++)
                            {
                                if (ddlIndultory.Items[i].Selected)
                                {
                                    IndustryIdProcess.Add(Convert.ToInt32(ddlIndultory.Items[i].Value));
                                }
                            }
                            if (IndustryIdProcess.Count > 0)
                            {
                                foreach (var aItem in IndustryIdProcess)
                                {
                                    IndustryMapping IndustryMapping = new IndustryMapping()
                                    {
                                        RiskCategoryCreationId = RiskCreationId,
                                        IndustryID = Convert.ToInt32(aItem),
                                        IsActive = true,
                                        EditedDate = DateTime.Now,
                                        EditedBy = Convert.ToInt32(Session["userID"]),
                                        ProcessId = pid,
                                        SubProcessId = psid,                                      
                                    };
                                    RiskCategoryManagement.CreateIndustryMapping(IndustryMapping);
                                }
                                IndustryIdProcess.Clear();
                            }
                            if (RiskIdProcess.Count > 0)
                            {
                                foreach (var aItem in RiskIdProcess)
                                {
                                    if (RiskCategoryManagement.RiskCategoryMappingExists(Convert.ToInt64(aItem), RiskCreationId))
                                    {
                                        RiskCategoryManagement.EditRiskCategoryMapping(aItem, RiskCreationId);
                                    }
                                    else
                                    {
                                        if (pid != -1 && psid != -1)
                                        {
                                            RiskCategoryMapping RiskCategoryMapping = new RiskCategoryMapping()
                                            {
                                                RiskCategoryCreationId = RiskCreationId,
                                                RiskCategoryId = Convert.ToInt32(aItem),
                                                ProcessId = pid,
                                                SubProcessId = psid,
                                                IsActive = true,
                                            };
                                            RiskCategoryManagement.CreateRiskCategoryMapping(RiskCategoryMapping);
                                        }
                                    }
                                }
                                RiskIdProcess.Clear();
                            }
                            if (AssertionsIdProcess.Count > 0)
                            {
                                foreach (var aItem in AssertionsIdProcess)
                                {
                                    if (RiskCategoryManagement.AssertionMappingExists(Convert.ToInt64(aItem), RiskCreationId))
                                    {
                                        RiskCategoryManagement.EditAssertionsMapping(aItem, RiskCreationId);
                                    }
                                    else
                                    {
                                        if (pid != -1 && psid != -1)
                                        {
                                            AssertionsMapping AssertionsMapping = new AssertionsMapping()
                                            {
                                                RiskCategoryCreationId = RiskCreationId,
                                                AssertionId = Convert.ToInt32(aItem),
                                                ProcessId = pid,
                                                SubProcessId = psid,
                                                IsActive = true,
                                            };
                                            RiskCategoryManagement.CreateAssertionsMapping(AssertionsMapping);
                                        }
                                    }
                                }
                                AssertionsIdProcess.Clear();
                            }
                            RiskActivityTransaction RiskActivityTxnData = new RiskActivityTransaction()
                            {
                                Id = riskactivityid,
                                ControlDescription = tbxControlDesc.Text,
                                MControlDescription = txtMitigation.Text,
                                ProcessId = pid,
                                SubProcessId = psid,
                                RiskCreationId = RiskCreationId,
                                PersonResponsible = Convert.ToInt32(ddlPersonResponsible.SelectedValue),
                                Key_Value = Convert.ToInt32(ddlKey.SelectedValue),
                                AutomatedControl = Convert.ToInt32(ddlAutomated.SelectedValue),
                                Frequency = Convert.ToInt32(ddlFrequency.SelectedValue),
                                PrevationControl = Convert.ToInt32(ddlPreventive.SelectedValue),
                                EffectiveDate = DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                                IsDeleted = false,
                                GapDescription = tbxGapDesc.Text,
                                Recommendations = tbxRecom.Text,
                                ActionRemediationplan = tbxActionPlan.Text,
                                IPE = tbxIPE.Text,
                                ERPsystem = tbxERPSystem.Text,
                                FRC = tbxFRC.Text,
                                UniqueReferred = tbxUnique.Text,
                                RiskRating = Convert.ToInt32(ddlRiskRating.SelectedValue),
                                ControlRating = Convert.ToInt32(ddlControlRating.SelectedValue),
                                CustomerBranchId = Convert.ToInt32(ddlClientProcess.SelectedValue),
                            };

                            RiskCategoryManagement.RiskActivityTxnUpdate(RiskActivityTxnData);

                            grdRiskActivityMatrix.EditIndex = -1;
                            if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                            {

                                if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                                && ddlFilterSubProcess.SelectedItem.Text != "All" /*&& ddlFilterRiskCategory.SelectedItem.Text != "All"*/
                                    /*&& ddlFilterIndustry.SelectedItem.Text != "All"*/ && ddlFilterLocationType.SelectedItem.Text != "All")
                                {
                                    BindData("P", "L");
                                }
                                //else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                                //&& ddlFilterSubProcess.SelectedItem.Text != "All" && ddlFilterRiskCategory.SelectedItem.Text != "All"
                                //&& ddlFilterIndustry.SelectedItem.Text != "All")
                                //{
                                //    BindData("P", "I");
                                //}
                                //else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                                //&& ddlFilterSubProcess.SelectedItem.Text != "All" && ddlFilterRiskCategory.SelectedItem.Text != "All")
                                //{
                                //    BindData("P", "R");
                                //}
                                else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                                && ddlFilterSubProcess.SelectedItem.Text != "All")
                                {
                                    BindData("P", "S");
                                }
                                else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All")
                                {
                                    BindData("P", "P");
                                }
                                else if (ddlFilterLocation.SelectedItem.Text != "All")
                                {
                                    BindData("P", "B");
                                }
                                else
                                {
                                    BindData("P", "A");
                                }
                            }
                            else if (rdRiskActivityProcess.SelectedItem.Text == "Others")
                            {
                                if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                               && ddlFilterSubProcess.SelectedItem.Text != "All" /*&& ddlFilterRiskCategory.SelectedItem.Text != "All"*/
                                   /*&& ddlFilterIndustry.SelectedItem.Text != "All" */&& ddlFilterLocationType.SelectedItem.Text != "All")
                                {
                                    BindData("N", "L");
                                }
                                //else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                                //&& ddlFilterSubProcess.SelectedItem.Text != "All" && ddlFilterRiskCategory.SelectedItem.Text != "All"
                                //&& ddlFilterIndustry.SelectedItem.Text != "All")
                                //{
                                //    BindData("N", "I");
                                //}
                                //else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                                //&& ddlFilterSubProcess.SelectedItem.Text != "All" && ddlFilterRiskCategory.SelectedItem.Text != "All")
                                //{
                                //    BindData("N", "R");
                                //}
                                else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                                && ddlFilterSubProcess.SelectedItem.Text != "All")
                                {
                                    BindData("N", "S");
                                }
                                else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All")
                                {
                                    BindData("N", "P");
                                }
                                else if (ddlFilterLocation.SelectedItem.Text != "All")
                                {
                                    BindData("N", "B");
                                }
                                else
                                {
                                    BindData("N", "A");
                                }
                            }

                        }
                        else
                        {
                            rfvProcess.IsValid = false;
                            rfvSubProcess.IsValid = false;
                            cvDuplicateEntry.IsValid = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                    #endregion
                }

                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {
                    BindData("P","A");
                }
                else
                {
                    BindData("N", "A");
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "javascript:CloseWin()", true);

            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void checkBoxesProcess_SelcetedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Saplin.Controls.DropDownCheckBoxes DrpIndustryProcess = (DropDownCheckBoxes)sender;
                IndustryIdProcess.Clear();
                for (int i = 0; i < DrpIndustryProcess.Items.Count; i++)
                {
                    if (DrpIndustryProcess.Items[i].Selected)
                    {
                        IndustryIdProcess.Add(Convert.ToInt32(DrpIndustryProcess.Items[i].Value));
                    }
                }
            }
            catch (Exception ex)
            {

                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void checkBoxesAssertionsProcess_SelcetedIndexChanged(object sender, EventArgs e)
        {
            Saplin.Controls.DropDownCheckBoxes ddlAssertionsProcess = (DropDownCheckBoxes)sender;
            AssertionsIdProcess.Clear();
            for (int i = 0; i < ddlAssertionsProcess.Items.Count; i++)
            {
                if (ddlAssertionsProcess.Items[i].Selected)
                {
                    AssertionsIdProcess.Add(Convert.ToInt32(ddlAssertionsProcess.Items[i].Value));
                }
            }
        }
        protected void checkBoxesRiskCategoryProcess_SelcetedIndexChanged(object sender, EventArgs e)
        {
            Saplin.Controls.DropDownCheckBoxes DrpRiskCategoryProcess = (DropDownCheckBoxes)sender;
            RiskIdProcess.Clear();
            for (int i = 0; i < DrpRiskCategoryProcess.Items.Count; i++)
            {
                if (DrpRiskCategoryProcess.Items[i].Selected)
                {
                    RiskIdProcess.Add(Convert.ToInt32(DrpRiskCategoryProcess.Items[i].Value));
                }
            }
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
        }
        private void BindBranchesHierarchy(TreeNode parent, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
     
        protected void upCompliance_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "initializeDatePicker", "initializeDatePicker(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public static FilterRiskControlMatrix GetRiskControlMatrixByRCMID(int RCMID,int RRID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {

                var FilterRiskControl = (from row in entities.FilterRiskControlMatrices
                                         where //row.RATID == RCMID && 
                                         row.RiskCreationId == RRID
                                         select row).FirstOrDefault();

                return FilterRiskControl;
            }
        }
        protected void grdRiskActivityMatrix_SelectedIndexChanged(object sender, EventArgs e)
        {

            //tbxControl.Text = grdRiskActivityMatrix.SelectedRow.Cells[1].Text;
            //tbxRiskDesc.Text = grdRiskActivityMatrix.SelectedRow.Cells[2].Text;
            //tbxControlObj.Text = grdRiskActivityMatrix.SelectedRow.Cells[3].Text;
            //ddlClientProcessEdit.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[4].Text;
            //ddlRiskCategory.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[5].Text;
            //ddlAssertionsFooterTemplate.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[6].Text;
            //tbxControlDesc.Text = grdRiskActivityMatrix.SelectedRow.Cells[7].Text;
            //txtMitigation.Text = grdRiskActivityMatrix.SelectedRow.Cells[8].Text;
            //ddlPersonResponsibleItemTemplate.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[9].Text;
            //txtStartDate.Text = grdRiskActivityMatrix.SelectedRow.Cells[10].Text;
            //ddlKeyItemTemplate.Text = grdRiskActivityMatrix.SelectedRow.Cells[11].Text;
            //ddlPreventiveItemTemplate.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[12].Text;
            //ddlAutomatedItemTemplate.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[13].Text;
            //ddlFrequencytemTemplate.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[14].Text;
            //tbxGapDesc.Text = grdRiskActivityMatrix.SelectedRow.Cells[15].Text;
            //tbxRecom.Text = grdRiskActivityMatrix.SelectedRow.Cells[16].Text;
            //tbxActionPlan.Text = grdRiskActivityMatrix.SelectedRow.Cells[17].Text;
            //tbxIPE.Text = grdRiskActivityMatrix.SelectedRow.Cells[18].Text;
            //tbxERPSystem.Text = grdRiskActivityMatrix.SelectedRow.Cells[19].Text;
            //tbxFRC.Text = grdRiskActivityMatrix.SelectedRow.Cells[20].Text;
            //tbxUnique.Text = grdRiskActivityMatrix.SelectedRow.Cells[21].Text;
            //ddlRiskEdit.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[22].Text;
            //ddlControlEdit.SelectedItem.Text = grdRiskActivityMatrix.SelectedRow.Cells[23].Text;

        }
        protected void grdRiskActivityMatrix_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EDIT_RCM"))
                {

                    int customerID = -1;
                    customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
                    //countrybindClient(ddlClient, customerID);

                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int RCMID = Convert.ToInt32(commandArgs[0]);
                    int RRID = Convert.ToInt32(commandArgs[1]);
                    ViewState["RCMID"] = RCMID;
                    ViewState["RRID"] = RRID;
                    ViewState["Mode"] = 1;
                    var RCMInfo = GetRiskControlMatrixByRCMID(RCMID, RRID);
                    if (RCMInfo !=null)
                    {
                        if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                        {
                            BindProcessPopPup(Convert.ToInt32(RCMInfo.BranchId), Convert.ToInt32(RCMInfo.ProcessId), "P");
                        }
                        else
                        {
                            BindProcessPopPup(Convert.ToInt32(RCMInfo.BranchId), Convert.ToInt32(RCMInfo.ProcessId),"N");
                        }
                        tbxControl.Text = RCMInfo.ControlNo;
                        tbxRiskDesc.Text = RCMInfo.ActivityDescription;
                        tbxControlObj.Text = RCMInfo.ControlObjective;
                        SplitString(ShowRiskCategoryName(RRID), ddlRiskCategoryCheck);
                        SplitString(ShowAssertionsName(RRID), ddlAssertions);
                        SplitString(ShowIndustryName(RRID), ddlIndultory);
                        ddlClientProcess.SelectedValue = Convert.ToString(RCMInfo.BranchId);
                        ddlProcess.SelectedValue = Convert.ToString(RCMInfo.ProcessId);
                        ddlSubProcess.SelectedValue = Convert.ToString(RCMInfo.SubProcessId);
                        ddlLocationType.SelectedValue = Convert.ToString(RCMInfo.LocationType);
                        tbxControlDesc.Text = RCMInfo.ControlDescription;
                        txtMitigation.Text = RCMInfo.MControlDescription;
                        ddlPersonResponsible.SelectedValue =Convert.ToString(RCMInfo.PersonResponsible);
                        txtStartDate.Text = Convert.ToDateTime(RCMInfo.EffectiveDate).ToString("dd-MM-yyyy");
                        ddlKey.SelectedValue = Convert.ToString(RCMInfo.Key);
                        ddlPreventive.SelectedValue = Convert.ToString(RCMInfo.PrevationControl);
                        ddlAutomated.SelectedValue = Convert.ToString(RCMInfo.AutomatedControl);
                        ddlFrequency.SelectedValue = Convert.ToString(RCMInfo.Frequency);
                        tbxGapDesc.Text = RCMInfo.GapDescription;
                        tbxRecom.Text = RCMInfo.Recommendations;
                        tbxActionPlan.Text = RCMInfo.ActionRemediationplan;
                        tbxIPE.Text = RCMInfo.IPE;
                        tbxERPSystem.Text = RCMInfo.ERPsystem;
                        tbxFRC.Text = RCMInfo.FRC;
                        tbxUnique.Text = RCMInfo.UniqueReferred;
                        ddlRiskRating.SelectedValue = Convert.ToString(RCMInfo.RiskRating);
                        ddlControlRating.SelectedValue = Convert.ToString(RCMInfo.ControlRating);
                        upComplianceDetails.Update();
                    }
                }                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void grdRiskActivityMatrix_RowDataBound(object sender, GridViewRowEventArgs e)
        {           
        }
      
        protected void grdRiskActivityMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdRiskActivityMatrix.PageIndex = e.NewPageIndex;
                if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                {

                    if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                    && ddlFilterSubProcess.SelectedItem.Text != "All" /*&& ddlFilterRiskCategory.SelectedItem.Text != "All"*/
                      /*  && ddlFilterIndustry.SelectedItem.Text != "All"*/ && ddlFilterLocationType.SelectedItem.Text != "All")
                    {
                        BindData("P", "L");
                    }
                    //else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                    //&& ddlFilterSubProcess.SelectedItem.Text != "All" && ddlFilterRiskCategory.SelectedItem.Text != "All"
                    //&& ddlFilterIndustry.SelectedItem.Text != "All")
                    //{
                    //    BindData("P", "I");
                    //}
                    //else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                    //&& ddlFilterSubProcess.SelectedItem.Text != "All" && ddlFilterRiskCategory.SelectedItem.Text != "All")
                    //{
                    //    BindData("P", "R");
                    //}
                    else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                    && ddlFilterSubProcess.SelectedItem.Text != "All")
                    {
                        BindData("P", "S");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All")
                    {
                        BindData("P", "P");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "All")
                    {
                        BindData("P", "B");
                    }
                    else
                    {
                        BindData("P", "A");
                    }
                }
                else if (rdRiskActivityProcess.SelectedItem.Text == "Others")
                {
                    if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                   && ddlFilterSubProcess.SelectedItem.Text != "All" /*&& ddlFilterRiskCategory.SelectedItem.Text != "All"*/
                      /* && ddlFilterIndustry.SelectedItem.Text != "All"*/ && ddlFilterLocationType.SelectedItem.Text != "All")
                    {
                        BindData("N", "L");
                    }
                    //else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                    //&& ddlFilterSubProcess.SelectedItem.Text != "All"/* && ddlFilterRiskCategory.SelectedItem.Text != "All"*/
                    //&& ddlFilterIndustry.SelectedItem.Text != "All")
                    //{
                    //    BindData("N", "I");
                    //}
                    //else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                    //&& ddlFilterSubProcess.SelectedItem.Text != "All" && ddlFilterRiskCategory.SelectedItem.Text != "All")
                    //{
                    //    BindData("N", "R");
                    //}
                    else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All"
                    && ddlFilterSubProcess.SelectedItem.Text != "All")
                    {
                        BindData("N", "S");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "All" && ddlFilterProcess.SelectedItem.Text != "All")
                    {
                        BindData("N", "P");
                    }
                    else if (ddlFilterLocation.SelectedItem.Text != "All")
                    {
                        BindData("N", "B");
                    }
                    else
                    {
                        BindData("N", "A");
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void setDateToGridView()
        {
            try
            {
              
                DateTime date = DateTime.Now;
                
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public string ShowCustomerBranchName(long categoryid)
        {
            List<CustomerBranchName_Result> a = new List<CustomerBranchName_Result>();
            string processnonprocess = "";
            a = ProcessManagement.GetCustomerBranchNameProcedure(Convert.ToInt32(categoryid)).ToList();
            var remindersummary = a.OrderBy(entry => entry.Name).ToList();
            if (remindersummary.Count > 0)
            {
                foreach (var row in remindersummary)
                {
                    processnonprocess += row.Name + " ,";
                }
            }

            return processnonprocess.Trim(',');
        }

        public void SplitString(string StrToSplit, Saplin.Controls.DropDownCheckBoxes DrpToFill)
        {
            string[] commandArgs = StrToSplit.Split(new char[] { ',' });
            string a = string.Empty;
            if (commandArgs.Length > 0)
            {
                for (int i = 0; i < commandArgs.Length; i++)
                {
                    a = commandArgs[i];
                    if (!string.IsNullOrEmpty(a))
                    {
                        DrpToFill.Items.FindByText(a.Trim()).Selected = true;
                    }
                }
            }
        }
        private void countrybindIndustory(Saplin.Controls.DropDownCheckBoxes DrpIndustry)
        {
            DrpIndustry.DataTextField = "Name";
            DrpIndustry.DataValueField = "Id";
            DrpIndustry.DataSource = ProcessManagement.FilliNDUSTRY();
            DrpIndustry.DataBind();
        }
        private void countrybindAssertions(Saplin.Controls.DropDownCheckBoxes DrpAssertions)
        {
            DrpAssertions.DataTextField = "Name";
            DrpAssertions.DataValueField = "ID";
            DrpAssertions.DataSource = RiskCategoryManagement.FillAssertions();
            DrpAssertions.DataBind();
        }
        private void countrybindRisk(Saplin.Controls.DropDownCheckBoxes Drpriskcategory, string Flag)
        {
            if (Flag == "P")
            {
                Drpriskcategory.DataTextField = "Name";
                Drpriskcategory.DataValueField = "ID";
                Drpriskcategory.DataSource = RiskCategoryManagement.FillRiskCategoryProcess("P");
                Drpriskcategory.DataBind();
            }
            else
            {
                Drpriskcategory.DataTextField = "Name";
                Drpriskcategory.DataValueField = "ID";
                Drpriskcategory.DataSource = RiskCategoryManagement.FillRiskCategoryProcess("N");
                Drpriskcategory.DataBind();
            }
        }
       
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=RiskCategoryTransaction.xls");
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";
                using (StringWriter sw = new StringWriter())
                {
                    HtmlTextWriter hw = new HtmlTextWriter(sw);
                    //To Export Allpages
                    GridExportExcel.AllowPaging = false;
                    if (rdRiskActivityProcess.SelectedItem.Text == "Process")
                    {
                        this.BindDataExport("P", "A");
                    }
                    else
                    {
                        this.BindDataExport("N", "A");
                    }
                    //GridExportExcel.HeaderRow.BackColor = Color.Blue;
                    //foreach (TableCell cell in GridExportExcel.HeaderRow.Cells)
                    //{
                    //    cell.BackColor = GridExportExcel.HeaderStyle.BackColor;
                    //}
                    //GridExportExcel.RenderControl(hw);
                    if (GridExportExcel.Rows.Count > 0)
                    {
                        GridExportExcel.HeaderRow.BackColor = Color.Blue;
                        foreach (TableCell cell in GridExportExcel.HeaderRow.Cells)
                        {
                            cell.BackColor = GridExportExcel.HeaderStyle.BackColor;
                        }
                        GridExportExcel.RenderControl(hw);
                    }
                    string style = @"<style> .textmode { } </style>";
                    Response.Write(style);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo <= GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                    grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdRiskActivityMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                BindGridData();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                // ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                    grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdRiskActivityMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {

                }
                //Reload the Grid
                BindGridData();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void lBNext_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //if (!IsValid()) { return; };

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                    grdRiskActivityMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdRiskActivityMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }
                else
                {
                }
                //Reload the Grid
                BindGridData();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        private void GetPageDisplaySummary()
        {
            try
            {
                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "")
                        SelectedPageNo.Text = "1";

                    if (SelectedPageNo.Text == "0")
                        SelectedPageNo.Text = "1";
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>

        private bool IsValid()
        {
            try
            {
                if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                {
                    SelectedPageNo.Text = "1";
                    return false;
                }
                else if (!IsNumeric(SelectedPageNo.Text))
                {
                    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }

        protected void lbtEdit_Click(object sender, EventArgs e)
        {

        }

        public void BindDropDown()
        {
            int customerID = -1;
            customerID = UserManagement.GetByID(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID).CustomerID ?? 0;
            if (ddlClientProcess != null)
            {
                ddlClientProcess.DataTextField = "Name";
                ddlClientProcess.DataValueField = "ID";
                ddlClientProcess.DataSource = UserManagementRisk.FillCustomerNew(customerID);
                ddlClientProcess.DataBind();
                ddlClientProcess.Items.Insert(0, new ListItem(" Select Branch ", "-1"));
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomerBranchName"])))
                {
                    ddlClientProcess.Items.FindByText(ViewState["CustomerBranchName"].ToString().Trim()).Selected = true;
                }
            }
            this.countrybindRisk(ddlRiskCategoryCheck, "P");
            this.countrybindAssertions(ddlAssertions);
            this.countrybindIndustory(ddlIndultory);
            if (ddlPersonResponsible != null)
            {
                ddlPersonResponsible.DataTextField = "Name";
                ddlPersonResponsible.DataValueField = "ID";
                ddlPersonResponsible.DataSource = RiskCategoryManagement.FillUsers(customerID);
                ddlPersonResponsible.DataBind();
                ddlPersonResponsible.Items.Insert(0, new ListItem(" Select User ", "-1"));
            }
            if (ddlKey != null)
            {
                ddlKey.DataTextField = "Name";
                ddlKey.DataValueField = "ID";
                ddlKey.DataSource = RiskCategoryManagement.FillKey();
                ddlKey.DataBind();
                ddlKey.Items.Insert(0, new ListItem(" Select Key ", "-1"));
            }
            if (ddlPreventive != null)
            {
                ddlPreventive.DataTextField = "Name";
                ddlPreventive.DataValueField = "ID";
                ddlPreventive.DataSource = RiskCategoryManagement.FillPreventiveControl();
                ddlPreventive.DataBind();
                ddlPreventive.Items.Insert(0, new ListItem(" Select Control ", "-1"));
            }
            if (ddlAutomated != null)
            {
                ddlAutomated.DataTextField = "Name";
                ddlAutomated.DataValueField = "ID";
                ddlAutomated.DataSource = RiskCategoryManagement.FillAutomatedControl();
                ddlAutomated.DataBind();
                ddlAutomated.Items.Insert(0, new ListItem(" Select Control ", "-1"));
            }
            if (ddlFrequency != null)
            {
                ddlFrequency.DataTextField = "Name";
                ddlFrequency.DataValueField = "ID";
                ddlFrequency.DataSource = RiskCategoryManagement.FillFrequency();
                ddlFrequency.DataBind();
                ddlFrequency.Items.Insert(0, new ListItem(" Select Frequency ", "-1"));
            }
            if (ddlRiskRating != null)
            {
                ddlRiskRating.Items.Clear();
                ddlRiskRating.DataTextField = "Name";
                ddlRiskRating.DataValueField = "ID";
                ddlRiskRating.DataSource = RiskCategoryManagement.FillRiskRating();
                ddlRiskRating.DataBind();
                ddlRiskRating.Items.Insert(0, new ListItem(" Select Risk Rating ", "-1"));
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Risk"])))
                {
                    ddlRiskRating.Items.FindByText(ViewState["Risk"].ToString().Trim()).Selected = true;
                }
            }
            if (ddlControlRating != null)
            {
                ddlControlRating.Items.Clear();
                ddlControlRating.DataTextField = "Name";
                ddlControlRating.DataValueField = "ID";
                ddlControlRating.DataSource = RiskCategoryManagement.FillControlRating();
                ddlControlRating.DataBind();
                ddlControlRating.Items.Insert(0, new ListItem(" Select Control Rating ", "-1"));
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Control"])))
                {
                    ddlControlRating.Items.FindByText(ViewState["Control"].ToString().Trim()).Selected = true;
                }
            }
        }    
    }
}