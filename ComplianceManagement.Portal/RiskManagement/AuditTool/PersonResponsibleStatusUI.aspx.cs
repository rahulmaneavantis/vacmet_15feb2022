﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class PersonResponsibleStatusUI : System.Web.UI.Page
    {
        public List<InternalAuditTransactionView> MasterInternalAuditTransactionView = new List<InternalAuditTransactionView>();
        public List<InternalControlAuditAssignment> MasterInternalControlAuditAssignment = new List<InternalControlAuditAssignment>();

        protected List<Int32> roles;
        public static List<long> Branchlist = new List<long>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindFinancialYear();
                BindLegalEntityData();
                BindVertical();                
                if (!String.IsNullOrEmpty(Request.QueryString["FinYear"]))
                {
                    string FinancialYear = Convert.ToString(Request.QueryString["FinYear"]);
                    ddlFinancialYear.ClearSelection();
                    ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));                    
                }
                else
                {
                    string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                    if (FinancialYear != null)
                    {
                        ddlFinancialYear.ClearSelection();
                        ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                    }
                }
                if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                {
                    if (Request.QueryString["Status"] == "RS")
                    {
                        ViewState["StatusID"] = 1;
                    }
                    if (Request.QueryString["Status"] == "AS")
                    {
                        ViewState["StatusID"] = 1;
                    }
                    if (Request.QueryString["Status"] == "RA")
                    {
                        ViewState["StatusID"] = 1;
                    }
                    else if (Request.QueryString["Status"] == "Closed")
                    {
                        ViewState["StatusID"] = 3;
                    }
                }

                if (!String.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    if (Request.QueryString["Type"] == "Process")
                    {
                        ShowProcessGrid(sender, e);
                    }
                    else if (Request.QueryString["Type"] == "Implementation")
                    {
                        ShowImplementationGrid(sender, e);
                    }
                    else
                    {
                        ShowProcessGrid(sender, e);
                    }
                }
                else
                    ShowProcessGrid(sender, e);

            }
        }
        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }
        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                    DropDownListPageNo.Items.Clear();

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(Convert.ToString(DropDownListPageNo.SelectedItem));

            if (ViewState["pagingflag"] != null)
            {
                if (Convert.ToString(ViewState["Type"]) == "Process")
                {
                    grdDrillDown.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdDrillDown.PageIndex = chkSelectedPage - 1;
                    BindData();
                }
                else if (Convert.ToString(ViewState["Type"]) == "Implementation")
                {
                    grdImplementationAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    grdImplementationAudits.PageIndex = chkSelectedPage - 1;
                    BindData();
                }
                bindPageNumber();
            }
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            //ddlFinancialYear.Items.Insert(0, new ListItem("Select Financial Year", "-1"));
        }

        public void BindLegalEntityData()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityData(customerID);
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Select Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            DRP.DataSource = AuditKickOff_NewDetails.PersonResponsibleFillSubEntityData(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, customerID, ParentId);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Unit", "-1"));
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else if (flag == "S")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, "Select Period");
                    ddlPeriod.Items.Insert(1, "Special Audit");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Select Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            //ddlSchedulingType.Items.Insert(0, new ListItem("Select Scheduling Type", "-1"));
        }

        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (branchid == -1)
                {
                    branchid = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }

                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                //ddlVertical.Items.Insert(0, new ListItem("Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, long customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities = new com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, com.VirtuosoITech.ComplianceManagement.Business.Data.ComplianceDBEntities entities)
        {
            IQueryable<com.VirtuosoITech.ComplianceManagement.Business.Data.CustomerBranch> query = (from row in entities.CustomerBranches
                                                                                                     where row.IsDeleted == false && row.CustomerID == customerid
                                                                                                      && row.ParentID == nvp.ID
                                                                                                     select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void BindData()
        {
            try
            {
                int branchid = -1;
                string FinYear = String.Empty;
                string Period = String.Empty;
                string Flag = String.Empty;
                int VerticalID = -1;
                string Status = string.Empty;
                int StatusID = 1;
                int CustomerId = -1;
                string tag = string.Empty;
                int UserID = Portal.Common.AuthenticationHelper.UserID;
                if (!String.IsNullOrEmpty(Convert.ToString(ViewState["Type"])))
                {
                    Flag = Convert.ToString(ViewState["Type"]);
                }
                if (!String.IsNullOrEmpty(Convert.ToString(Request.QueryString["tag"])))
                {
                    tag = Convert.ToString(Request.QueryString["tag"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["StatusID"])))
                {
                    StatusID = Convert.ToInt32(ViewState["StatusID"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["Status"])))
                {
                    if (Convert.ToString(Request.QueryString["Status"]) == "Open")
                    {
                        Status="RS";
                    }
                    else
                    {
                        Status = Convert.ToString(Request.QueryString["Status"]);
                    }
                }

                if (Status == "AC")
                {
                    StatusID = 3;
                }
                //if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                //{
                //    if (ddlStatus.SelectedValue != "-1")
                //    {
                //        Status = ddlStatus.SelectedValue;
                //    }
                //}
                //new
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }
                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }
                if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        Period = ddlPeriod.SelectedItem.Text;
                    }
                }               
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        VerticalID = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                    {
                        if (ddlVertical.SelectedValue != "-1")
                        {
                            VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                        }
                    }
                }
                Branchlist.Clear();
                GetAllHierarchy(CustomerId, branchid);
                Branchlist.ToList();

                //End
                if (Flag == "Process")
                {
                    grdDrillDown.DataSource = null;
                    grdDrillDown.DataBind();

                    grdImplementationAudits.Visible = false;
                    grdDrillDown.Visible = true;
                    var detailView = InternalControlManagementDashboardRisk.GetPersonResponsibleAuditSteps(UserID, StatusID, Status);

                    if (CustomerId != -1)
                        detailView = detailView.Where(Entry => Entry.CustomerID == CustomerId).ToList();
                    if (Branchlist.Count > 0)
                        detailView = detailView.Where(Entry => Branchlist.Contains((int)Entry.CustomerBranchID)).ToList();
                    if (VerticalID != -1)
                        detailView = detailView.Where(Entry => Entry.VerticalId == VerticalID).ToList();
                    if (FinYear != "")
                        detailView = detailView.Where(Entry => Entry.FinancialYear == FinYear).ToList();
                    if (Period != "")
                        detailView = detailView.Where(Entry => Entry.ForMonth == Period).ToList();

                    grdDrillDown.DataSource = detailView;
                    Session["TotalRows"] = detailView.Count;
                    grdDrillDown.DataBind();
                }
                else if (Flag == "Implementation")
                {
                    grdDrillDown.DataSource = null;
                    grdDrillDown.DataBind();

                    grdImplementationAudits.Visible = true;
                    grdDrillDown.Visible = false;

                    var detailView = InternalControlManagementDashboardRisk.GetPersonResponsibleAuditTotalCountUserWiseIMP(UserID, 3, CustomerId, StatusID, FinYear, Branchlist, Period, VerticalID, tag);

                    Session["TotalRows"] = detailView.Rows.Count;
                    grdImplementationAudits.DataSource = detailView;
                    grdImplementationAudits.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.Items.Count > 0)
                ddlSubEntity1.Items.Clear();

            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.Items.Count > 0)
                ddlSubEntity2.Items.Clear();

            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.Items.Clear();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.Items.Clear();

            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();

            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.Items.Count > 0)
                ddlSubEntity3.ClearSelection();

            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.ClearSelection();

            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();

            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterLocation.Items.Count > 0)
                ddlFilterLocation.ClearSelection();

            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
            }

            BindSchedulingType();
            BindVertical();
            BindData();

            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (Convert.ToInt32(ddlFilterLocation.SelectedValue) != -1)
                {
                    BindSchedulingType();
                    BindVertical();
                    BindData();

                    bindPageNumber();
                    //GetPageDisplaySummary();
                }
            }
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlFinancialYear.SelectedItem.Text != "< Select Financial Year >")
            //{
            if (ddlSchedulingType.SelectedItem.Text == "Annually")
            {
                BindAuditSchedule("A", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
            {
                BindAuditSchedule("H", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
            {
                BindAuditSchedule("Q", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
            {
                BindAuditSchedule("M", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Special Audit")
            {
                BindAuditSchedule("S", 0);
            }
            else if (ddlSchedulingType.SelectedItem.Text == "Phase")
            {
                //BindAuditSchedule("P", 5);

                if (!string.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        int count = 0;
                        count = UserManagementRisk.GetPhaseCount(Convert.ToInt32(ddlFilterLocation.SelectedValue));
                        BindAuditSchedule("P", count);
                    }
                }
            }
            //}
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
            bindPageNumber();
            //GetPageDisplaySummary();
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //SelectedPageNo.Text = "1";
                int chkSelectedPage = Convert.ToInt32(Convert.ToString(DropDownListPageNo.SelectedItem));

                //if (currentPageNo <= GetTotalPagesCount())
                //{
                //    SelectedPageNo.Text = (currentPageNo).ToString();

                if (!String.IsNullOrEmpty(Convert.ToString(ViewState["Type"])))
                {
                    if (Convert.ToString(ViewState["Type"]) == "Process")
                    {
                        grdDrillDown.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdDrillDown.PageIndex = chkSelectedPage - 1;
                    }
                    else if (Convert.ToString(ViewState["Type"]) == "Implementation")
                    {
                        grdImplementationAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                        grdImplementationAudits.PageIndex = chkSelectedPage - 1;
                    }
                }

                //Reload the Grid
                BindData();
                bindPageNumber();
                if (!String.IsNullOrEmpty(Convert.ToString(ViewState["Type"])))
                {
                    if (Convert.ToString(ViewState["Type"]) == "Process")
                    {
                        int count = Convert.ToInt32(GetTotalPagesCount());
                        if (count > 0)
                        {
                            int gridindex = grdDrillDown.PageIndex;
                            string chkcindition = Convert.ToString((gridindex + 1));
                            DropDownListPageNo.SelectedValue = Convert.ToString((chkcindition));
                        }
                    }
                    else if (Convert.ToString(ViewState["Type"]) == "Implementation")
                    {
                        int count = Convert.ToInt32(GetTotalPagesCount());
                        if (count > 0)
                        {
                            int gridindex = grdImplementationAudits.PageIndex;
                            string chkcindition = Convert.ToString((gridindex + 1));
                            DropDownListPageNo.SelectedValue = Convert.ToString((chkcindition));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DateTime GetDate(string date)
        {
            string date1 = "";
            if (date.Contains("/"))
            {
                date1 = date.Trim().Substring(date.IndexOf("/") + 4, 4) + "-" + date.Substring(date.IndexOf("/") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains("-"))
            {
                date1 = date.Substring(date.IndexOf("-") + 4, 4) + "-" + date.Substring(date.IndexOf("-") + 1, 2) + "-" + date.Substring(0, 2);
            }
            else if (date.Trim().Contains(" "))
            {
                date1 = date.Substring(date.IndexOf(" ") + 4, 4) + "-" + date.Substring(date.IndexOf(" ") + 1, 2) + "-" + date.Substring(0, 2);
            }
            return Convert.ToDateTime(date1);
        }

        protected void grdProcessAudits_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("ViewAuditStatusSummary"))
                {
                    string[] commandArgs = Convert.ToString(e.CommandArgument).Split(new char[] { ',' });
                    if (commandArgs.Length > 0)
                    {
                        //int CustomerBranchID = Convert.ToInt32(commandArgs[0]);
                        //string ForMonth = Convert.ToString(commandArgs[1]);
                        //string FinancialYear = Convert.ToString(commandArgs[2]);
                        //int Verticalid = Convert.ToInt32(commandArgs[3]);
                        long AuditID = Convert.ToInt32(commandArgs[4]);


                        if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                        {
                            Response.Redirect("~/RiskManagement/InternalAuditTool/PersonResponsibleStatusSummary.aspx?Type=Process" + "&ReturnUrl1=Status@" + Convert.ToString(Request.QueryString["Status"]) + "&AuditID=" + AuditID, false);
                        }
                        else
                        {
                            Response.Redirect("~/RiskManagement/InternalAuditTool/PersonResponsibleStatusSummary.aspx?Type=Process" + "&ReturnUrl1=" + "&AuditID=" + AuditID, false);
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "gridPageIndexChanged", "gridPageIndexChanged();", true);
                        // }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdImplementationAudits_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //try
            //{
            //    if (e.CommandName.Equals("ViewAuditStatusSummary"))
            //    {
            //        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            //        if (commandArgs.Length > 0)
            //        {
            //            //int CustomerBranchID = Convert.ToInt32(commandArgs[0]);
            //            //string ForMonth = Convert.ToString(commandArgs[1]);
            //            //string FinancialYear = Convert.ToString(commandArgs[2]);
            //            //int Verticalid = Convert.ToInt32(commandArgs[3]);
            //            long AuditID = Convert.ToInt64(commandArgs[4]);


            //            if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
            //            {
            //                Response.Redirect("~/RiskManagement/InternalAuditTool/PersonResponsibleIMPStatusSummary.aspx?Type=Implementation" + "&ReturnUrl1=Status@" + Request.QueryString["Status"].ToString() + "&AuditID=" + AuditID, false);
            //            }
            //            else
            //            {
            //                Response.Redirect("~/RiskManagement/InternalAuditTool/PersonResponsibleIMPStatusSummary.aspx?Type=Implementation" + "&ReturnUrl1=" + "&AuditID=" + AuditID, false);
            //            }
            //            // }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            //    cvDuplicateEntry.IsValid = false;
            //    cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            //}


            //if (!string.IsNullOrEmpty(Request.QueryString["AuditID"]))
            //{
            //    AuditID = Convert.ToInt32(Request.QueryString["AuditID"]);
            //}
            //else
            //{
            //    AuditID = Convert.ToInt32(ViewState["AuditID"]);
            //}
            //String Args = e.CommandArgument.ToString();

            //string[] arg = Args.ToString().Split(';');

            //string URLStr = string.Empty;
            //string url = "";
            //if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
            //{
            //    url = "&returnUrl1=" + Request.QueryString["ReturnUrl1"];
            //}
            //string url2 = HttpContext.Current.Request.Url.AbsolutePath;
            //if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl1"]))
            //{
            //    URLStr = "~/RiskManagement/InternalAuditTool/PersonResponsibleMainUI_IMP.aspx?Status=" + arg[0] + "&ID=" + arg[1] + "&SID=" + arg[2] + "&CustBranchID=" + arg[3] + "&FinYear=" + arg[4] + "&Period=" + arg[5] + "&VID=" + arg[6] + "&AuditID=" + AuditID + url;
            //}
            //else
            //{
            //    URLStr = "~/RiskManagement/InternalAuditTool/PersonResponsibleMainUI_IMP.aspx?Status=" + arg[0] + "&ID=" + arg[1] + "&SID=" + arg[2] + "&CustBranchID=" + arg[3] + "&FinYear=" + arg[4] + "&Period=" + arg[5] + "&VID=" + arg[6] + "&AuditID=" + AuditID;
            //}
            //if (Args != "")
            //    Response.Redirect(URLStr, false);
            try
            {
                if (e.CommandName.Equals("DRILLDOWN"))
                {
                    String Args = Convert.ToString(e.CommandArgument);

                    string[] arg = Convert.ToString(Args).Split(';');
                    String URLStr = "";
                    string url2 = HttpContext.Current.Request.Url.AbsolutePath;
                    string tag = string.Empty;
                    if (!String.IsNullOrEmpty(Convert.ToString(Request.QueryString["tag"])))
                    {
                        tag = Convert.ToString(Request.QueryString["tag"]);
                    }

                    URLStr = "~/RiskManagement/InternalAuditTool/PersonResponsibleMainUI_IMP.aspx?Status=" + arg[0] + "&ID=" + arg[1] + "&SID=" + arg[2] + "&CustBranchID=" + arg[3] + "&FinYear=" + arg[4] + "&Period=" + arg[5] + "&RoleID=" + arg[6] + "&VID=" + arg[7] + "&AuditID=" + arg[8] + "&ReturnUrl1=Status@" + Request.QueryString["Status"] + "&tag=" + tag + "&Type=Implementation&Audittee=" + arg[9];

                    if (Args != "")
                        Response.Redirect(URLStr, false);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //protected void lBPrevious_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        //if (!IsValid()) { return; };

        //        if (Convert.ToInt32(SelectedPageNo.Text) > 1)
        //        {
        //            SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();

        //            if (!String.IsNullOrEmpty(ViewState["Type"].ToString()))
        //            {
        //                if (ViewState["Type"].ToString() == "Process")
        //                {
        //                    grdProcessAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //                    grdProcessAudits.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //                }
        //                else if (ViewState["Type"].ToString() == "Implementation")
        //                {
        //                    grdImplementationAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //                    grdImplementationAudits.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //                }
        //            }
        //        }
        //        else
        //        {

        //        }
        //        //Reload the Grid
        //        BindData();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        //protected void lBNext_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

        //        if (currentPageNo < GetTotalPagesCount())
        //        {
        //            SelectedPageNo.Text = (currentPageNo + 1).ToString();

        //            if (!String.IsNullOrEmpty(ViewState["Type"].ToString()))
        //            {
        //                if (ViewState["Type"].ToString() == "Process")
        //                {
        //                    grdProcessAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //                    grdProcessAudits.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //                }
        //                else if (ViewState["Type"].ToString() == "Implementation")
        //                {
        //                    grdImplementationAudits.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
        //                    grdImplementationAudits.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
        //                }
        //            }
        //        }
        //        else
        //        {
        //        }
        //        //Reload the Grid
        //        BindData();
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        protected void ShowImplementationGrid(object sender, EventArgs e)
        {
            liImplementation.Attributes.Add("class", "active");
            liProcess.Attributes.Add("class", "");
            ViewState["Type"] = "Implementation";

            BindData();
            bindPageNumber();
        }

        protected void ShowProcessGrid(object sender, EventArgs e)
        {
            liProcess.Attributes.Add("class", "active");
            liImplementation.Attributes.Add("class", "");
            ViewState["Type"] = "Process";

            BindData();
            bindPageNumber();
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Convert.ToString(Session["TotalRows"]);

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        /// <summary>
        /// Gets the page display summary.
        /// </summary>
        //private void GetPageDisplaySummary()
        //{
        //    try
        //    {
        //        lTotalCount.Text = GetTotalPagesCount().ToString();

        //        if (lTotalCount.Text != "0")
        //        {
        //            if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
        //                SelectedPageNo.Text = "1";

        //            if (SelectedPageNo.Text == "0")
        //                SelectedPageNo.Text = "1";
        //        }
        //        else if (lTotalCount.Text == "0")
        //        {
        //            SelectedPageNo.Text = "0";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// Determines whether the specified page no is numeric.
        /// </summary>
        /// <param name="PageNo">The page no.</param>
        /// <returns><c>true</c> if the specified page no is numeric; otherwise, <c>false</c>.</returns>
        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }


        #region  Drill Down Grid
        public string ShowMgmtResponse(int ATBDID, int VerticalId, long ScheduledOnID, long AuditID)
        {
            string Observation = string.Empty;
            var MstRiskResult = RiskCategoryManagement.GetInternalControlAuditResultbyID(Convert.ToInt32(ScheduledOnID), ATBDID, VerticalId, Convert.ToInt32(AuditID));
            if (MstRiskResult != null)
            {
                Observation = MstRiskResult.ManagementResponse;
            }
            return Observation;
        }

        public string ShowObservation(int ATBDID, int VerticalId, long ScheduledOnID, long AuditID)
        {
            string workdone = string.Empty;
            var MstRiskResult = RiskCategoryManagement.GetInternalControlAuditResultbyID(Convert.ToInt32(ScheduledOnID), ATBDID, VerticalId, Convert.ToInt32(AuditID));
            if (MstRiskResult != null)
            {
                workdone = MstRiskResult.Observation;
            }
            return workdone;
        }

        public string ShowTimeline(int ATBDID, int VerticalId, long ScheduledOnID, long AuditID)
        {
            string timeline = string.Empty;
            var MstRiskResult = RiskCategoryManagement.GetInternalControlAuditResultbyID(Convert.ToInt32(ScheduledOnID), ATBDID, VerticalId, Convert.ToInt32(AuditID));
            if (MstRiskResult != null)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResult.TimeLine)))
                {
                    timeline = Convert.ToDateTime(MstRiskResult.TimeLine).ToString("dd-MM-yyyy");
                }                
            }
            return timeline;
        }

        protected void grdDrillDown_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkAuditDetails = (LinkButton)e.Row.FindControl("btnChangeStatus") as LinkButton;

                if (lnkAuditDetails != null)
                {
                    string[] arg = Convert.ToString(lnkAuditDetails.CommandArgument).Split(',');

                    string statusidasd = Convert.ToString(arg[8]);
                    int ScheduledOnID = Convert.ToInt32(arg[7]);
                    int AuditID = Convert.ToInt32(arg[6]);

                    if (ScheduledOnID > 0 && AuditID > 0)
                    {
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            MasterInternalAuditTransactionView = (from row in entities.InternalAuditTransactionViews
                                                                  where row.AuditScheduleOnID == ScheduledOnID
                                                                  && row.AuditID == AuditID
                                                                  && row.AuditStatusID != null
                                                                  && row.ATBDId != null
                                                                  select row).ToList();

                            MasterInternalControlAuditAssignment = (from row in entities.InternalControlAuditAssignments
                                                                    where row.AuditID == AuditID
                                                                    select row).ToList();
                        }
                    }

                    if (statusidasd == "Team Review")
                    {
                        int ATBTID = 0;
                        if (!string.IsNullOrEmpty(Convert.ToString(grdDrillDown.DataKeys[e.Row.RowIndex])))
                        {
                            ATBTID = Convert.ToInt32(grdDrillDown.DataKeys[e.Row.RowIndex].Value);
                        }
                        Label lblcolor = (Label)e.Row.FindControl("lblObservation");
                        var User = (from row in MasterInternalAuditTransactionView
                                    where row.AuditScheduleOnID == ScheduledOnID
                                    && row.AuditID == AuditID
                                    && row.ATBDId == ATBTID
                                    select row).OrderByDescending(el => el.Dated).FirstOrDefault();

                        var ListPerformer = (from row in MasterInternalControlAuditAssignment where row.RoleID == 3 select row.UserID).FirstOrDefault();
                        var ListReviewer = (from row in MasterInternalControlAuditAssignment where row.RoleID == 4 select row.UserID).FirstOrDefault();
                        if (User != null)
                        {
                            if (ListPerformer == User.CreatedBy)
                            {
                                lblcolor.Style.Add("color", "black");
                            }
                            else if (ListReviewer == User.CreatedBy)
                            {
                                lblcolor.Style.Add("color", "red");
                            }
                        }
                    }
                    else if (statusidasd == "Auditee Review")
                    {
                        int ATBTID = 0;
                        if (!string.IsNullOrEmpty(Convert.ToString(grdDrillDown.DataKeys[e.Row.RowIndex])))
                        {
                            ATBTID = Convert.ToInt32(grdDrillDown.DataKeys[e.Row.RowIndex].Value);
                        }
                        TextBox txtstepId = (TextBox)e.Row.FindControl("ATBDID");
                        Label lblcolor = (Label)e.Row.FindControl("lblObservation");
                        var User = (from row in MasterInternalAuditTransactionView
                                    where row.AuditScheduleOnID == ScheduledOnID
                                    && row.AuditID == AuditID
                                    && row.ATBDId == ATBTID
                                    select row).OrderByDescending(el => el.Dated).FirstOrDefault();
                        var ListReviewer = (from row in MasterInternalControlAuditAssignment where row.RoleID == 4 select row.UserID).FirstOrDefault();
                        if (User != null)
                        {
                            if (ListReviewer == User.CreatedBy)
                            {
                                lblcolor.Style.Add("color", "blue");
                            }
                            else
                            {
                                lblcolor.Style.Add("color", "black");
                            }
                        }
                    }
                }
            }
        }

        #endregion

        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                int atbdid = -1;
                int custbranchid = -1;
                string FinancialYear = string.Empty;
                string Forperiod = string.Empty;
                int VerticalId = -1;
                int AuditStatusID = -1;
                int AuditID = -1;
                string AuditName = string.Empty;
                LinkButton btn = (LinkButton)(sender);
                string[] commandArgs = Convert.ToString(btn.CommandArgument).Split(new char[] { ',' });
                if (!string.IsNullOrEmpty(commandArgs[0]))
                {
                    atbdid = Convert.ToInt32(commandArgs[0]);
                    ViewState["ATBDID"] = null;
                    ViewState["ATBDID"] = atbdid;
                }
                if (!string.IsNullOrEmpty(commandArgs[1]))
                {
                    custbranchid = Convert.ToInt32(commandArgs[1]);
                    ViewState["custbranchid"] = null;
                    ViewState["custbranchid"] = custbranchid;
                }
                if (!string.IsNullOrEmpty(commandArgs[2]))
                {
                    FinancialYear = Convert.ToString(commandArgs[2]);
                    ViewState["FinancialYear"] = null;
                    ViewState["FinancialYear"] = FinancialYear;
                }
                if (!string.IsNullOrEmpty(commandArgs[3]))
                {
                    Forperiod = Convert.ToString(commandArgs[3]);
                    ViewState["Forperiod"] = null;
                    ViewState["Forperiod"] = Forperiod;
                }
                if (!string.IsNullOrEmpty(commandArgs[4]))
                {
                    VerticalId = Convert.ToInt32(commandArgs[4]);
                    ViewState["VerticalID"] = null;
                    ViewState["VerticalID"] = VerticalId;
                }
                if (!string.IsNullOrEmpty(commandArgs[5]))
                {
                    AuditStatusID = Convert.ToInt32(commandArgs[5]);
                    ViewState["AuditStatusID"] = null;
                    ViewState["AuditStatusID"] = AuditStatusID;
                }
                if (!string.IsNullOrEmpty(commandArgs[6]))
                {
                    AuditID = Convert.ToInt32(commandArgs[6]);
                }

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + atbdid + "," + custbranchid + ",'" + FinancialYear + "','" + Forperiod + "','" + VerticalId + "'," + AuditStatusID + "," + AuditID + ");", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdDrillDown_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdDrillDown.PageIndex = e.NewPageIndex;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdDrillDown_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData(); bindPageNumber();
        }

        protected void grdImplementationAudits_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdImplementationAudits_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }


        protected void btnChangeStatusIMP_Click(object sender, EventArgs e)
        {
            try
            {
                int ScheduledonID = -1;
                int ResultID = -1;
                int AuditStatusID = -1;
                int custbranchid = -1;
                string FinancialYear = string.Empty;
                string Forperiod = string.Empty;
                int VerticalId = -1;
                long AuditID = -1;
                LinkButton btn = (LinkButton)(sender);
                string[] commandArgs = Convert.ToString(btn.CommandArgument).Split(new char[] { ',' });

                if (!string.IsNullOrEmpty(commandArgs[0]))
                {
                    ResultID = Convert.ToInt32(commandArgs[0]);
                    ViewState["ResultID"] = null;
                    ViewState["ResultID"] = ResultID;
                }
                if (!string.IsNullOrEmpty(commandArgs[1]))
                {
                    custbranchid = Convert.ToInt32(commandArgs[1]);
                    ViewState["custbranchid"] = null;
                    ViewState["custbranchid"] = custbranchid;
                }
                if (!string.IsNullOrEmpty(commandArgs[2]))
                {
                    FinancialYear = Convert.ToString(commandArgs[2]);
                    ViewState["FinancialYear"] = null;
                    ViewState["FinancialYear"] = FinancialYear;
                }
                if (!string.IsNullOrEmpty(commandArgs[3]))
                {
                    Forperiod = Convert.ToString(commandArgs[3]);
                    ViewState["Forperiod"] = null;
                    ViewState["Forperiod"] = Forperiod;
                }
                if (!string.IsNullOrEmpty(commandArgs[4]))
                {
                    VerticalId = Convert.ToInt32(commandArgs[4]);
                    ViewState["VerticalID"] = null;
                    ViewState["VerticalID"] = VerticalId;
                }
                if (!string.IsNullOrEmpty(commandArgs[5]))
                {
                    ScheduledonID = Convert.ToInt32(commandArgs[5]);
                    ViewState["ScheduledonID"] = null;
                    ViewState["ScheduledonID"] = ScheduledonID;
                }
                if (!string.IsNullOrEmpty(commandArgs[6]))
                {
                    AuditStatusID = Convert.ToInt32(commandArgs[6]);
                    ViewState["AuditStatusID"] = null;
                    ViewState["AuditStatusID"] = AuditStatusID;
                }
                if (!string.IsNullOrEmpty(commandArgs[7]))
                {
                    AuditID = Convert.ToInt64(commandArgs[7]);
                }
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowIMPDialog(" + ResultID + "," + custbranchid + ",'" + FinancialYear + "','" + Forperiod + "'," + VerticalId + "," + ScheduledonID + "," + AuditStatusID + "," + AuditID + ");", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAllSavechk_Click(object sender, EventArgs e)
        {
            try
            {
                int chkValidationflag = 0;
                foreach (GridViewRow rw in grdDrillDown.Rows)
                {
                    CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                    if (chkBx != null && chkBx.Checked)
                    {
                        chkValidationflag = chkValidationflag + 1;
                    }
                }
                if (chkValidationflag > 0)
                {
                    bool Success1 = false;
                    bool Success2 = false;
                    bool validationError = false;
                    foreach (GridViewRow rw in grdDrillDown.Rows)
                    {
                        CheckBox chkBx = (CheckBox)rw.FindControl("CheckBox1");
                        if (chkBx != null && chkBx.Checked)
                        {
                            TextBox tbxMgmtResponse = ((TextBox)rw.FindControl("tbxMgmtResponse"));
                            TextBox tbxTimeLine = ((TextBox)rw.FindControl("tbxTimeLine"));
                            LinkButton btnChangeStatus = ((LinkButton)rw.FindControl("btnChangeStatus"));

                            if (!string.IsNullOrEmpty(tbxMgmtResponse.Text))
                            {
                                if (!string.IsNullOrEmpty(tbxTimeLine.Text))
                                {
                                    int customerID = -1;
                                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                                    long roleid = 4;
                                    String Args = Convert.ToString(btnChangeStatus.CommandArgument);
                                    string[] arg = Convert.ToString(Args).Split(',');
                                    int ATBDID = Convert.ToInt32(arg[0]);
                                    int BranchID = Convert.ToInt32(arg[1]);
                                    string FinYear = Convert.ToString(arg[2]);
                                    string Period = Convert.ToString(arg[3]);
                                    int VerticalID = Convert.ToInt32(arg[4]);
                                    int StatusID = Convert.ToInt32(arg[5]);
                                    int AuditID = Convert.ToInt32(arg[6]);
                                    int ScheduledOnID = Convert.ToInt32(arg[7]);

                                    DateTime b = DateTime.ParseExact(DateTime.Now.ToString("dd-MM-yyyy"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                    var MstRiskResultObj = RiskCategoryManagement.GetInternalControlAuditResultbyID(ScheduledOnID, ATBDID, StatusID, VerticalID, AuditID);
                                    //var RecentComplianceTransaction = RiskCategoryManagement.GetCurrentStatusByInternalAuditComplianceID(Convert.ToInt32(ScheduledOnID), Atbtid);

                                    var getscheduleondetails = RiskCategoryManagement.GetInternalAuditScheduleOnByDetails(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID, 3, FinYear, Period, BranchID, ATBDID, VerticalID, AuditID);
                                    if (getscheduleondetails != null)
                                    {

                                        InternalControlAuditResult MstRiskResult = new InternalControlAuditResult()
                                        {
                                            AuditScheduleOnID = getscheduleondetails.ID,
                                            ProcessId = getscheduleondetails.ProcessId,
                                            FinancialYear = FinYear,
                                            ForPerid = Period,
                                            CustomerBranchId = BranchID,
                                            IsDeleted = false,
                                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            ATBDId = ATBDID,
                                            RoleID = roleid,
                                            InternalAuditInstance = getscheduleondetails.InternalAuditInstance,
                                            VerticalID = VerticalID,
                                            AuditID = AuditID,
                                            AuditeeResponse = "AS"
                                        };
                                        InternalAuditTransaction transaction = new InternalAuditTransaction()
                                        {
                                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            CreatedByText = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.User,
                                            StatusChangedOn = GetDate(b.ToString("dd/MM/yyyy")),
                                            AuditScheduleOnID = getscheduleondetails.ID,
                                            FinancialYear = FinYear,
                                            CustomerBranchId = BranchID,
                                            InternalAuditInstance = getscheduleondetails.InternalAuditInstance,
                                            ProcessId = getscheduleondetails.ProcessId,
                                            ForPeriod = Period,
                                            ATBDId = ATBDID,
                                            RoleID = roleid,
                                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            VerticalID = VerticalID,
                                            AuditID = AuditID
                                        };
                                        DateTime dt1 = new DateTime();
                                        ObservationHistory objHistory = new ObservationHistory()
                                        {
                                            UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            RoleID = roleid,
                                            CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            ATBTID = ATBDID
                                        };
                                        objHistory.ObservationOld = MstRiskResultObj.Observation;
                                        objHistory.ProcessWalkthroughOld = MstRiskResultObj.ProcessWalkthrough;
                                        objHistory.ActualWorkDoneOld = MstRiskResultObj.ActivityToBeDone;
                                        objHistory.PopulationOld = MstRiskResultObj.Population;
                                        objHistory.SampleOld = MstRiskResultObj.Sample;
                                        objHistory.ObservationTitleOld = MstRiskResultObj.ObservationTitle;
                                        objHistory.RiskOld = MstRiskResultObj.Risk;
                                        objHistory.RootCauseOld = MstRiskResultObj.RootCost;
                                        objHistory.FinancialImpactOld = Convert.ToString(MstRiskResultObj.FinancialImpact);
                                        objHistory.RecommendationOld = MstRiskResultObj.Recomendation;
                                        objHistory.ManagementResponseOld = MstRiskResultObj.ManagementResponse;
                                        objHistory.RemarksOld = MstRiskResultObj.FixRemark;
                                        objHistory.ScoreOld = Convert.ToString(MstRiskResultObj.AuditScores);

                                        objHistory.OldBodyContent = MstRiskResultObj.BodyContent;
                                        objHistory.OldUHComment = MstRiskResultObj.UHComment;
                                        objHistory.OldPRESIDENTComment = MstRiskResultObj.PRESIDENTComment;
                                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultObj.UHPersonResponsible)))
                                        {
                                            objHistory.OldUHPersonResponsible = Convert.ToInt32(MstRiskResultObj.UHPersonResponsible);
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(MstRiskResultObj.PRESIDENTPersonResponsible)))
                                        {
                                            objHistory.OldPRESIDENTPersonResponsible = Convert.ToInt32(MstRiskResultObj.PRESIDENTPersonResponsible);
                                        }

                                        MstRiskResult.BodyContent = MstRiskResultObj.BodyContent;
                                        objHistory.BodyContent = MstRiskResultObj.BodyContent;
                                        transaction.BodyContent = MstRiskResultObj.BodyContent;

                                        MstRiskResult.UHComment = MstRiskResultObj.UHComment;
                                        objHistory.UHComment = MstRiskResultObj.UHComment;
                                        transaction.UHComment = MstRiskResultObj.UHComment;

                                        MstRiskResult.PRESIDENTComment = MstRiskResultObj.PRESIDENTComment;
                                        objHistory.PRESIDENTComment = MstRiskResultObj.PRESIDENTComment;
                                        transaction.PRESIDENTComment = MstRiskResultObj.PRESIDENTComment;

                                        MstRiskResult.UHPersonResponsible = MstRiskResultObj.UHPersonResponsible;
                                        objHistory.UHPersonResponsible = MstRiskResultObj.UHPersonResponsible;
                                        transaction.UHPersonResponsible = MstRiskResultObj.UHPersonResponsible;

                                        MstRiskResult.PRESIDENTPersonResponsible = MstRiskResultObj.PRESIDENTPersonResponsible;
                                        objHistory.PRESIDENTPersonResponsible = MstRiskResultObj.PRESIDENTPersonResponsible;
                                        transaction.PRESIDENTPersonResponsible = MstRiskResultObj.PRESIDENTPersonResponsible;

                                        objHistory.PersonResponsibleOld = MstRiskResultObj.PersonResponsible;
                                        objHistory.ObservationRatingOld = MstRiskResultObj.ObservationRating;
                                        objHistory.ObservationCategoryOld = MstRiskResultObj.ObservationCategory;
                                        objHistory.ObservationSubCategoryOld = MstRiskResultObj.ObservationSubCategory;
                                        objHistory.OwnerOld = MstRiskResultObj.Owner;
                                        objHistory.UserOld = MstRiskResultObj.UserID;
                                        objHistory.ISACPORMISOld = MstRiskResultObj.ISACPORMIS;
                                        objHistory.ISACPORMISOld = MstRiskResultObj.ISACPORMIS;

                                        if (!string.IsNullOrEmpty(tbxTimeLine.Text))
                                        {
                                            DateTime DtTimeline = DateTimeExtensions.GetDate(tbxTimeLine.Text);
                                            //DateTime d1=Convert.ToDateTime(tbxTimeLine.Text);
                                            //dt = DateTime.ParseExact(d1, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                            objHistory.TimeLineOld = DtTimeline;
                                        }
                                        objHistory.Remarks = MstRiskResultObj.FixRemark;
                                        dt1 = new DateTime();
                                        if (!string.IsNullOrEmpty(tbxTimeLine.Text.Trim()))
                                        {
                                            DateTime DtTimeline = DateTimeExtensions.GetDate(tbxTimeLine.Text);
                                            objHistory.TimeLine = DtTimeline;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(AuditID)))
                                        {
                                            objHistory.AuditId = AuditID;
                                        }
                                        MstRiskResult.ISACPORMIS = MstRiskResultObj.ISACPORMIS;
                                        objHistory.ISACPORMIS = MstRiskResultObj.ISACPORMIS;
                                        string remark = string.Empty;
                                        transaction.StatusId = 6;
                                        MstRiskResult.AStatusId = 6;
                                        remark = "Audit Steps Under Auditee Review.";
                                        MstRiskResult.AuditObjective = MstRiskResultObj.AuditObjective;
                                        MstRiskResult.AuditSteps = MstRiskResultObj.AuditSteps;
                                        MstRiskResult.AnalysisToBePerofrmed = MstRiskResultObj.AuditSteps;
                                        MstRiskResult.ProcessWalkthrough = MstRiskResultObj.ProcessWalkthrough;
                                        objHistory.ProcessWalkthrough = MstRiskResultObj.ProcessWalkthrough;
                                        MstRiskResult.ActivityToBeDone = MstRiskResultObj.ActivityToBeDone;
                                        objHistory.ActualWorkDone = MstRiskResultObj.ActivityToBeDone;
                                        MstRiskResult.Population = MstRiskResultObj.Population;
                                        objHistory.Population = MstRiskResultObj.Population;
                                        MstRiskResult.Sample = MstRiskResultObj.Sample;
                                        MstRiskResult.ObservationNumber = MstRiskResultObj.ObservationNumber;
                                        MstRiskResult.ObservationTitle = MstRiskResultObj.ObservationTitle;
                                        objHistory.ObservationTitle = MstRiskResultObj.ObservationTitle;
                                        MstRiskResult.Observation = MstRiskResultObj.Observation;
                                        objHistory.Observation = MstRiskResultObj.Observation;
                                        MstRiskResult.Risk = MstRiskResultObj.Risk;
                                        objHistory.Risk = MstRiskResultObj.Risk;
                                        MstRiskResult.RootCost = MstRiskResultObj.RootCost;
                                        objHistory.RootCause = MstRiskResultObj.RootCost;
                                        MstRiskResult.AuditScores = MstRiskResultObj.AuditScores;
                                        objHistory.Score = Convert.ToString(MstRiskResultObj.AuditScores);
                                        MstRiskResult.FinancialImpact = MstRiskResultObj.FinancialImpact;
                                        objHistory.FinancialImpact = Convert.ToString(MstRiskResultObj.FinancialImpact);
                                        MstRiskResult.Recomendation = MstRiskResultObj.Recomendation;
                                        objHistory.Recommendation = MstRiskResultObj.Recomendation;
                                        if (!string.IsNullOrEmpty(tbxMgmtResponse.Text.Trim()))
                                        {
                                            MstRiskResult.ManagementResponse = tbxMgmtResponse.Text.Trim();
                                            objHistory.ManagementResponse = tbxMgmtResponse.Text.Trim();
                                        }
                                        else
                                        {
                                            MstRiskResult.ManagementResponse = "";
                                            objHistory.ManagementResponse = "";
                                        }
                                        MstRiskResult.FixRemark = MstRiskResultObj.FixRemark;

                                        if (!string.IsNullOrEmpty(tbxTimeLine.Text.Trim()))
                                        {
                                            DateTime DtTimeline = DateTimeExtensions.GetDate(tbxTimeLine.Text);
                                            MstRiskResult.TimeLine = DtTimeline;
                                        }
                                        else
                                            MstRiskResult.TimeLine = null;
                                        
                                        MstRiskResult.PersonResponsible = MstRiskResultObj.PersonResponsible;
                                        transaction.PersonResponsible = MstRiskResultObj.PersonResponsible;
                                        objHistory.PersonResponsible = MstRiskResultObj.PersonResponsible;
                                        MstRiskResult.Owner = MstRiskResultObj.Owner;
                                        transaction.Owner = MstRiskResultObj.Owner;
                                        objHistory.Owner = MstRiskResultObj.Owner;
                                        transaction.ObservatioRating = MstRiskResultObj.ObservationRating;
                                        MstRiskResult.ObservationRating = MstRiskResultObj.ObservationRating;
                                        objHistory.ObservationRating = MstRiskResultObj.ObservationRating;
                                        transaction.ObservationCategory = MstRiskResultObj.ObservationCategory;
                                        MstRiskResult.ObservationCategory = MstRiskResultObj.ObservationCategory;
                                        objHistory.ObservationCategory = MstRiskResultObj.ObservationCategory;
                                        transaction.ObservationSubCategory = MstRiskResultObj.ObservationSubCategory;
                                        MstRiskResult.ObservationSubCategory = MstRiskResultObj.ObservationSubCategory;
                                        objHistory.ObservationSubCategory = MstRiskResultObj.ObservationSubCategory;
                                        MstRiskResult.BriefObservation = MstRiskResultObj.BriefObservation;
                                        MstRiskResult.ObjBackground = MstRiskResultObj.ObjBackground;
                                        transaction.Remarks = remark.Trim();
                                        MstRiskResult.ResponseDueDate = MstRiskResultObj.ResponseDueDate;
                                        transaction.ResponseDueDate = MstRiskResultObj.ResponseDueDate;
                                        objHistory.ResponseDueDate = MstRiskResultObj.ResponseDueDate;
                                        objHistory.ResponseDueDateOld = MstRiskResultObj.ResponseDueDate;

                                        // transaction.ReviewerRiskRating = MstRiskResultObj.revi;

                                        // added by sagar more on 10-01-2020
                                        MstRiskResult.AnnexueTitle = MstRiskResultObj.AnnexueTitle;
                                        
                                        if (RiskCategoryManagement.InternalControlResultExistsCheckLastestNullWithStatus(MstRiskResult))
                                        {
                                            if (MstRiskResult.AStatusId == 6)
                                            {
                                                MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                MstRiskResult.CreatedOn = DateTime.Now;
                                                Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                            }
                                            else
                                            {
                                                if (RiskCategoryManagement.InternalControlResultExists(MstRiskResult))
                                                {
                                                    MstRiskResult.UpdatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                                    MstRiskResult.UpdatedOn = DateTime.Now;
                                                    Success1 = RiskCategoryManagement.UpdateInternalControlResult(MstRiskResult);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            MstRiskResult.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            MstRiskResult.CreatedOn = DateTime.Now;
                                            Success1 = RiskCategoryManagement.CreateInternalControlResult(MstRiskResult);
                                        }
                                        if (RiskCategoryManagement.InternalAuditTxnExists(transaction))
                                        {
                                            transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            transaction.CreatedOn = DateTime.Now;
                                            Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                                        }
                                        else
                                        {
                                            transaction.CreatedBy = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
                                            transaction.CreatedOn = DateTime.Now;
                                            Success2 = RiskCategoryManagement.CreateInternalAuditTxn(transaction);
                                        }
                                        if (RiskCategoryManagement.CheckExistObservationHistory(objHistory))
                                        {
                                            RiskCategoryManagement.AddObservationHistory(objHistory);
                                        }
                                    }
                                }
                                else
                                {
                                    validationError = true;
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Timeline should not be blank.";
                                }
                            }
                            else
                            {
                                validationError = true;
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Management Response should not be blank.";
                            }
                        }
                    }
                    if (Success1 == true && Success2 == true)
                    {
                        BindData();
                        bindPageNumber();
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Audit Steps Submitted Successfully";
                    }
                    else if(validationError == true)
                    {
                        cvDuplicateEntry.IsValid = false;
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}