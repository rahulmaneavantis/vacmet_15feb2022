﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Saplin.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class PreRequsiteLogReport : System.Web.UI.Page
    {
        protected List<int> Branchlist = new List<int>();
        protected List<string> Quarterlist = new List<string>();
        protected static string AuditHeadOrManagerReport = null;
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        protected int CustomerId = 0;
        protected bool DepartmentHead = false;
        protected static bool ManagementFlag = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (CustomerManagementRisk.CheckIsManagement(Portal.Common.AuthenticationHelper.UserID) == 8)
            {
                ManagementFlag = true;
            }
            if (!IsPostBack)
            {
                BindLegalEntityData();
                BindFinancialYear();
                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFilterFinancial.ClearSelection();
                    ddlFilterFinancial.SelectedValue = FinancialYear;
                }
                if (AuditHeadOrManagerReport != null)
                {
                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        PerformerFlag = false;
                        ReviewerFlag = false;
                        if (roles.Contains(3) && roles.Contains(4))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(3))
                        {
                            PerformerFlag = true;
                        }
                        else if (roles.Contains(4))
                        {
                            ReviewerFlag = true;
                        }
                    }
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                    }
                }
            }
        }
        protected void ddlFilterFinancial_SelectedIndexChanged(object sender, EventArgs e)
        {
            upComplianceTypeList.Update();
        }
        protected void ShowReviewer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "active");
            liPerformer.Attributes.Add("class", "");
            ReviewerFlag = true;
            PerformerFlag = false;
        }        
        protected void ShowPerformer(object sender, EventArgs e)
        {
            liReviewer.Attributes.Add("class", "");
            liPerformer.Attributes.Add("class", "active");
            ReviewerFlag = false;
            PerformerFlag = true;
        }
        protected string GetTestResult(string TOD, string TOE, string AuditStatusID)
        {
            try
            {
                string returnvalue = string.Empty;
                if ((TOD == "2" || TOD == "-1") && AuditStatusID == "3")
                {
                    returnvalue = "Design Failure (TOD)";
                }
                else if ((TOE == "2" || TOE == "-1") && AuditStatusID == "3")
                {
                    returnvalue = "Effectiveness Failure (TOE)";
                }
                else if ((TOD == null || TOE == "") || (TOE == null || TOE == ""))
                {
                    returnvalue = "Not Tested";
                }
                else if ((TOD == "2" || TOD == "-1") && AuditStatusID == "2")
                {
                    returnvalue = "Not Tested";
                }
                else
                {
                    returnvalue = "Pass";
                }
                return returnvalue;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected string GetFrequencyName(long ID)
        {
            try
            {
                using (AuditControlEntities entities = new AuditControlEntities())
                {
                    var transactionsQuery = (from row in entities.mst_Frequency
                                             where row.Id == ID
                                             select row).FirstOrDefault();

                    return transactionsQuery.Name.Trim();
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        public void BindFinancialYear()
        {
            ddlFilterFinancial.DataTextField = "Name";
            ddlFilterFinancial.DataValueField = "ID";
            ddlFilterFinancial.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFilterFinancial.DataBind();
        }
        public void BindLegalEntityData()
        {
            int CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
            mst_User UserDetails = UserManagementRisk.GetByID(UserID);
            bool IsAuditManager = false;
            bool IsDepartmentHead = false;
            bool IsManagement = false;
            if (UserDetails != null)
            {
                if (!string.IsNullOrEmpty(UserDetails.IsAuditHeadOrMgr))
                {
                    IsAuditManager = true;
                }
                else if (UserDetails.RoleID == 8)
                {
                    IsManagement = true;
                }
                else if(UserDetails.IsHead == true)
                {
                    IsDepartmentHead = true;
                }
                ddlLegalEntity.DataTextField = "Name";
                ddlLegalEntity.DataValueField = "ID";
                ddlLegalEntity.Items.Clear();
                if (IsAuditManager)
                {
                    ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(CustomerId, UserID);
                }
                else if (IsDepartmentHead)
                {
                    ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataDepartmentHead(CustomerId, UserID);
                }
                else if (IsManagement)
                {
                    ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataManagement(CustomerId, UserID);
                }
                else
                {
                    ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataPerformerReviewer(CustomerId, UserID);
                }
                ddlLegalEntity.DataBind();
            }
        }
        public void BindSubEntityData(DropDownList DRP, Int64 ParentId)
        {
            int CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
            int UserID = Portal.Common.AuthenticationHelper.UserID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                    DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);                
            }
            else if (DepartmentHead)
            {
                DRP.DataSource = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else
            {
                DRP.DataSource = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Select Sub Entity", "-1"));
        }
        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void BindSchedulingType()
        {
            int branchid = -1;
            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }
            if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
            {
                if (ddlSubEntity4.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                }
            }
            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Scheduling Type", "-1"));
        }
        public void BindVertical()
        {
            try
            {
                int branchid = -1;
                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                int CustomerId = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);
                //ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(CustomerId);
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasaBYCustomer(CustomerId);
                ddlVertical.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        
        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            using (ExcelPackage exportPackge = new ExcelPackage())
            {
                try
                {
                    int UserId = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);                    
                    if (1 == 1)
                    {
                        int RoleID = -1;
                        ArrayList ItemList = new ArrayList();
                        DataTable Observationcategorytable = new DataTable();
                        Observationcategorytable.Columns.Add("ObservationCategory", typeof(string));
                        Observationcategorytable.Columns.Add("ObservationRating", typeof(int));
                        Observationcategorytable.Columns.Add("ProcessName", typeof(string));
                        if (PerformerFlag)
                            RoleID = 3;
                        if (ReviewerFlag)
                            RoleID = 4;

                        int CustomerBranchId = -1;
                        string CustomerBranchName = "";
                        string ForPeriod = "";
                        string FnancialYear = "";
                        string Flag = String.Empty;
                        int VerticalID = -1;
                        string departmentheadFlag = string.Empty;
                        if (DepartmentHead)
                        {
                            departmentheadFlag = "DH";
                        }
                        if (CustomerId == 0)
                        {
                            CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                        }
                        if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                        {
                            if (ddlLegalEntity.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                                CustomerBranchName = ddlLegalEntity.SelectedItem.Text;
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                        {
                            if (ddlSubEntity1.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                                CustomerBranchName = ddlSubEntity1.SelectedItem.Text;
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                        {
                            if (ddlSubEntity2.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                                CustomerBranchName = ddlSubEntity2.SelectedItem.Text;
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                        {
                            if (ddlSubEntity3.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                                CustomerBranchName = ddlSubEntity3.SelectedItem.Text;
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                        {
                            if (ddlSubEntity4.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                                CustomerBranchName = ddlSubEntity4.SelectedItem.Text;
                            }
                        }
                        if (!String.IsNullOrEmpty(ddlFilterFinancial.SelectedValue))
                        {
                            if (ddlFilterFinancial.SelectedValue != "-1")
                            {
                                FnancialYear = Convert.ToString(ddlFilterFinancial.SelectedItem.Text);
                            }
                        }
                        else
                        {
                            FnancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                        }
                        if (Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                        {
                            int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                            if (vid != -1)
                            {
                                VerticalID = vid;
                            }
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(ddlVertical.SelectedValue))
                            {
                                if (ddlVertical.SelectedValue != "-1")
                                {
                                    VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                        {
                            if (ddlSubEntity4.SelectedValue != "-1")
                            {
                                CustomerBranchId = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                                CustomerBranchName = ddlSubEntity4.SelectedItem.Text;
                            }
                        }
                        if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                        {
                            if (ddlPeriod.SelectedValue != "-1")
                            {
                                ForPeriod = ddlPeriod.SelectedItem.Text;
                            }
                        }

                        string IsAuditHeadFlag = string.Empty;
                        if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                        {
                            IsAuditHeadFlag = "AM";
                            if (RoleID == -1)
                            {
                                RoleID = 4;
                            }
                        }
                        else
                        {
                            if (Portal.Common.AuthenticationHelper.Role.Equals("CADMN"))
                            {
                                IsAuditHeadFlag = "CMA";
                            }
                            else if (DepartmentHead)
                            {
                                IsAuditHeadFlag = "DH";
                            }
                            else if (CustomerManagementRisk.CheckIsManagement(UserId) == 8)
                            {
                                IsAuditHeadFlag = "MA";
                            }
                            else
                            {
                                IsAuditHeadFlag = "FE";
                            }
                        }
                        using (AuditControlEntities entities = new AuditControlEntities())
                        {
                            List<sp_PreRequsiteLogReport_Result> table = new List<sp_PreRequsiteLogReport_Result>();

                            table = (from row in entities.sp_PreRequsiteLogReport(CustomerId)
                                     select row).ToList();
                            if (CustomerBranchId != -1)
                            {
                                table = table.Where(Entry => Entry.BranchId == CustomerBranchId).ToList();
                            }
                            if (FnancialYear != "")
                            {
                                table = table.Where(entry => entry.FinancialYear == FnancialYear).ToList();
                            }
                            if (ForPeriod != "")
                            {
                                table = table.Where(entry => entry.Period == ForPeriod).ToList();
                            }
                            if (table.Count > 0)
                            {
                                string FileName = string.Empty;
                                FileName = "Pre-Requsite Log Report";
                                var CustomerName = UserManagementRisk.GetCustomerName(CustomerId);
                                List<sp_PreRequsiteLogReport_Result> table1 = new List<sp_PreRequsiteLogReport_Result>();
                                long? ATBTId = null;
                                long? checkListId = null;
                                DateTime? timeLineDate = null;
                                foreach (sp_PreRequsiteLogReport_Result record in table)
                                {
                                    if (record.Atbdid == ATBTId && record.ChecklistId == checkListId && record.TimelineDuedate != timeLineDate)
                                    {
                                        record.CustomerBranch = "";
                                        record.FinancialYear = "";
                                        record.Period = "";
                                        record.AuditeeName = "";
                                        record.ReportingManager = "";
                                        record.DocumentName = "";
                                        record.RequestedDate = "";
                                        table1.Add(record);
                                    }
                                    else
                                    {
                                        ATBTId = record.Atbdid;
                                        checkListId = record.ChecklistId;
                                        timeLineDate = record.TimelineDuedate;
                                        table1.Add(record);
                                    }
                                }
                                
                                ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("Pre-Requsite Log Report");
                                DataTable ExcelData1 = null;
                                DataView view1 = new System.Data.DataView((table1 as List<sp_PreRequsiteLogReport_Result>).ToDataTable());

                                ExcelData1 = view1.ToTable(false, "CustomerName", "CustomerBranch", "FinancialYear", "Period", "AuditeeName", "ReportingManager", "DocumentName", "RequestedDate", "TimelineDuedate", "Status", "Remark", "BranchId", "Createdby", "Atbdid", "ChecklistId");

                                ExcelData1.Columns["CustomerName"].ColumnName = "Customer Name";
                                ExcelData1.Columns["CustomerBranch"].ColumnName = "Customer Branch";
                                ExcelData1.Columns["FinancialYear"].ColumnName = "Financial Year";
                                ExcelData1.Columns["Period"].ColumnName = "For Period";
                                ExcelData1.Columns["AuditeeName"].ColumnName = "Auditee Name";
                                ExcelData1.Columns["ReportingManager"].ColumnName = "Reporting Manager";
                                ExcelData1.Columns["DocumentName"].ColumnName = "Document Name";
                                ExcelData1.Columns["RequestedDate"].ColumnName = "Requested Date";
                                ExcelData1.Columns["TimelineDuedate"].ColumnName = "Timeline (Due date)";

                                string MonthToDisplay = string.Empty;

                                ExcelData1.Columns.Remove("BranchId");
                                ExcelData1.Columns.Remove("Createdby");
                                ExcelData1.Columns.Remove("Customer Name");
                                ExcelData1.Columns.Remove("Atbdid");
                                ExcelData1.Columns.Remove("ChecklistId");

                                exWorkSheet1.Cells["A1"].Value = "Report Generated On:";
                                exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["A1"].Style.Font.Size = 12;

                                exWorkSheet1.Cells["B1"].Value = DateTime.Now.ToString("dd-MMM-yyyy");
                                exWorkSheet1.Cells["B1"].Style.Font.Size = 12;

                                exWorkSheet1.Cells["A2"].Value = CustomerName;
                                exWorkSheet1.Cells["A2"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["A2"].Style.Font.Size = 12;

                                exWorkSheet1.Cells["A3"].Value = FileName;
                                exWorkSheet1.Cells["A3"].Style.Font.Bold = true;
                                exWorkSheet1.Cells["A3"].Style.Font.Size = 12;
                                exWorkSheet1.Cells["A3"].AutoFitColumns(50);

                                exWorkSheet1.Cells["A5"].Value = "Customer Branch";
                                exWorkSheet1.Cells["A5:A5"].Merge = true;
                                exWorkSheet1.Cells["A5"].AutoFitColumns(25);
                                exWorkSheet1.Cells["A5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["A5"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["A5"].Style.WrapText = true;
                                exWorkSheet1.Cells["A5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                                exWorkSheet1.Cells["B5"].Value = "Financial Year";
                                exWorkSheet1.Cells["B5:B5"].Merge = true;
                                exWorkSheet1.Cells["B5"].AutoFitColumns(25);
                                exWorkSheet1.Cells["B5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["B5"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["B5"].Style.WrapText = true;
                                exWorkSheet1.Cells["B5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                                exWorkSheet1.Cells["C5"].Value = "For Period";
                                exWorkSheet1.Cells["C5:C5"].Merge = true;
                                exWorkSheet1.Cells["C5"].AutoFitColumns(15);
                                exWorkSheet1.Cells["C5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["C5"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["C5"].Style.WrapText = true;

                                //exWorkSheet1.Cells["D5"].Value = "Review Period";
                                //exWorkSheet1.Cells["D5:D5"].Merge = true;
                                //exWorkSheet1.Cells["D5"].AutoFitColumns(15);
                                //exWorkSheet1.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                //exWorkSheet1.Cells["D5"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                //exWorkSheet1.Cells["D5"].Style.WrapText = true;

                                exWorkSheet1.Cells["D5"].Value = "Auditee Name";
                                exWorkSheet1.Cells["D5:D5"].Merge = true;
                                exWorkSheet1.Cells["D5"].AutoFitColumns(30);
                                exWorkSheet1.Cells["D5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["D5"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["D5"].Style.WrapText = true;

                                exWorkSheet1.Cells["E5"].Value = "Reporting Manager";
                                exWorkSheet1.Cells["E5:E5"].Merge = true;
                                exWorkSheet1.Cells["E5"].AutoFitColumns(30);
                                exWorkSheet1.Cells["E5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["E5"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["E5"].Style.WrapText = true;

                                exWorkSheet1.Cells["F5"].Value = "Document Name";
                                exWorkSheet1.Cells["F5:F5"].Merge = true;
                                exWorkSheet1.Cells["F5"].AutoFitColumns(45);
                                exWorkSheet1.Cells["F5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["F5"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["F5"].Style.WrapText = true;

                                exWorkSheet1.Cells["G5"].Value = "Requested Date";
                                exWorkSheet1.Cells["G5:G5"].Merge = true;
                                exWorkSheet1.Cells["G5"].AutoFitColumns(15);
                                exWorkSheet1.Cells["G5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["G5"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["G5"].Style.WrapText = true;

                                exWorkSheet1.Cells["H5"].Value = "Timeline (Due date)";
                                exWorkSheet1.Cells["H5:H5"].Merge = true;
                                exWorkSheet1.Cells["H5"].AutoFitColumns(25);
                                exWorkSheet1.Cells["H5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["H5"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["H5"].Style.WrapText = true;

                                exWorkSheet1.Cells["I5"].Value = "Status";
                                exWorkSheet1.Cells["I5:I5"].Merge = true;
                                exWorkSheet1.Cells["I5"].AutoFitColumns(15);
                                exWorkSheet1.Cells["I5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["I5"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["I5"].Style.WrapText = true;

                                exWorkSheet1.Cells["J5"].Value = "Remark";
                                exWorkSheet1.Cells["J5:J5"].Merge = true;
                                exWorkSheet1.Cells["J5"].AutoFitColumns(25);
                                exWorkSheet1.Cells["J5"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                exWorkSheet1.Cells["J5"].Style.Fill.BackgroundColor.SetColor(Color.Gainsboro);
                                exWorkSheet1.Cells["J5"].Style.WrapText = true;

                                exWorkSheet1.Cells["A6"].LoadFromDataTable(ExcelData1, false);

                                using (ExcelRange col = exWorkSheet1.Cells[5, 1, 5 + ExcelData1.Rows.Count, 10 + ItemList.Count])
                                {
                                    col.Style.WrapText = true;
                                    using (ExcelRange col1 = exWorkSheet1.Cells[5, 1, 5 + ExcelData1.Rows.Count, 10])
                                    {
                                        col1.Style.Numberformat.Format = "dd-MMM-yyyy";
                                    }
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                                    Response.ClearContent();
                                    Response.Buffer = true;
                                    Response.AddHeader("content-disposition", "attachment;filename=PreRequsite Log Report.xlsx");
                                    Response.Charset = "";
                                    Response.ContentType = "application/vnd.ms-excel";
                                    StringWriter sw = new StringWriter();
                                    Response.BinaryWrite(fileBytes);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "No Audit Available for selected period.";
                            }
                        }
                    }
                    
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                }
            }
        }        
        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }
        public List<NameValueHierarchy> GetAllHierarchy(long customerID, int customerbranchid)
        {
            List<NameValueHierarchy> hierarchy = null;
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public void LoadSubEntities(long customerid, NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {
            IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
                                                    where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                    select row);
            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }        
        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
        }
        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity1, Convert.ToInt64(ddlLegalEntity.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity1.Items.Count > 0)
                        ddlSubEntity1.Items.Clear();

                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
            upComplianceTypeList.Update();
        }
        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity2.Items.Count > 0)
                        ddlSubEntity2.Items.Clear();

                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
            upComplianceTypeList.Update();
        }
        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity3.Items.Count > 0)
                        ddlSubEntity3.Items.Clear();

                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
        }
        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    BindSubEntityData(ddlSubEntity4, Convert.ToInt32(ddlSubEntity3.SelectedValue));
                }
                else
                {
                    if (ddlSubEntity4.Items.Count > 0)
                        ddlSubEntity4.Items.Clear();
                }
            }
            BindSchedulingType(); BindVertical();
        }
        protected void ddlSubEntity4_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSchedulingType(); BindVertical();
        }        
        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }
                    if (!String.IsNullOrEmpty(ddlSubEntity4.SelectedValue))
                    {
                        if (ddlSubEntity4.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity4.SelectedValue);
                        }
                    }
                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                }
            }
        }
    }
}