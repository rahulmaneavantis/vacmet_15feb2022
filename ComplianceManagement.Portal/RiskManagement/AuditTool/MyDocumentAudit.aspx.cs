﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Common;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Collections;
using System.Data;
using System.IO;
using Ionic.Zip;
using Spire.Presentation.Drawing.Animation;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool
{
    public partial class MyDocumentAudit : System.Web.UI.Page
    {
        public static List<int?> Branchlist = new List<int?>();
        protected List<Int32> roles;
        static bool PerformerFlag;
        static bool ReviewerFlag;
        static bool auditHeadFlag;
        static bool departmenthead;
        public static string linkclick;
        protected static string AuditHeadOrManagerReport = null;
        protected bool DepartmentHead = false;
        protected int CustomerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            AuditHeadOrManagerReport = CustomerManagementRisk.GetAuditHeadOrManagerid(Portal.Common.AuthenticationHelper.UserID);
            roles = CustomerManagementRisk.GetAssignedRolesARS(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            if (!IsPostBack)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                BindVertical();
                BindFinancialYear();
                BindLegalEntityData();

                string FinancialYear = GetCurrentFinancialYear(DateTime.Now.Date);
                if (FinancialYear != null)
                {
                    ddlFinancialYear.ClearSelection();
                    ddlFinancialYear.SelectedValue = Convert.ToString(GetCurrentFinancialYearValue(FinancialYear));
                }

                if (AuditHeadOrManagerReport != null)
                {
                    if (AuditHeadOrManagerReport == "AM" || AuditHeadOrManagerReport == "AH")
                    {
                        auditHeadFlag = true;
                        ShowAuditHead(sender, e);
                    }
                }
                else if (DepartmentHead)
                {
                    departmenthead = true;
                    ShowDepartmentHead(sender, e);
                }
                else
                {
                    if (roles.Contains(3))
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                    else if (roles.Contains(4))
                    {
                        ReviewerFlag = true;
                        ShowReviewer(sender, e);
                    }
                    else
                    {
                        PerformerFlag = true;
                        ShowPerformer(sender, e);
                    }
                }
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }
                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";
                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //throw ex;
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;

            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.mst_CustomerBranch
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, AuditControlEntities entities)
        {
            //IQueryable<mst_CustomerBranch> query = (from row in entities.mst_CustomerBranch
            //                                        where row.IsDeleted == false && row.CustomerID == customerid
            //                                         && row.ParentID == nvp.ID
            //                                        select row);

            int UserID = Convert.ToInt32(Portal.Common.AuthenticationHelper.UserID);
            List<AuditManagerClass> query;
            if (departmenthead)
            {
                query = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerid, nvp.ID);
            }
            else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                query = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, customerid, nvp.ID);
            }
            else if (auditHeadFlag)
            {
                query = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, customerid, nvp.ID);
            }
            else
            {
                query = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, customerid, nvp.ID);
            }

            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdSummaryDetailsAuditCoverage.PageIndex = chkSelectedPage - 1;

            grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindData();
        }

        protected void ShowPerformer(object sender, EventArgs e)
        {
            liPerformer.Attributes.Add("class", "active");
            liReviewer.Attributes.Add("class", "");
            liDepartmentHead.Attributes.Add("class", "");
            liAuditHead.Attributes.Add("class", "");

            PerformerFlag = true;
            ReviewerFlag = false;
            departmenthead = false;
            auditHeadFlag = false;
            BindData();
        }

        protected void ShowReviewer(object sender, EventArgs e)
        {
            liPerformer.Attributes.Add("class", "");
            liReviewer.Attributes.Add("class", "active");
            liDepartmentHead.Attributes.Add("class", "");
            liAuditHead.Attributes.Add("class", "");

            PerformerFlag = false;
            ReviewerFlag = true;
            departmenthead = false;
            auditHeadFlag = false;
            BindData();
        }

        protected void ShowAuditHead(object sender, EventArgs e)
        {
            liPerformer.Attributes.Add("class", "");
            liReviewer.Attributes.Add("class", "");
            liDepartmentHead.Attributes.Add("class", "");
            liAuditHead.Attributes.Add("class", "active");

            ReviewerFlag = false;
            PerformerFlag = false;
            departmenthead = false;
            auditHeadFlag = true;
            BindData();
        }
        protected void ShowDepartmentHead(object sender, EventArgs e)
        {
            liPerformer.Attributes.Add("class", "");
            liReviewer.Attributes.Add("class", "");
            liDepartmentHead.Attributes.Add("class", "active");
            liAuditHead.Attributes.Add("class", "");

            PerformerFlag = false;
            ReviewerFlag = false;
            departmenthead = true;
            auditHeadFlag = false;
            BindData();
        }

        public void BindVertical()
        {
            try
            {
                int branchid = -1;

                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }


                ddlVertical.DataTextField = "VerticalName";
                ddlVertical.DataValueField = "VerticalsId";
                ddlVertical.Items.Clear();
                ddlVertical.DataSource = UserManagementRisk.FillVerticalListFromRiskActTrasa(branchid);
                ddlVertical.DataBind();
                ddlVertical.Items.Insert(0, new ListItem("Vertical", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindFinancialYear()
        {
            ddlFinancialYear.Items.Clear();
            ddlFinancialYear.DataTextField = "Name";
            ddlFinancialYear.DataValueField = "ID";
            ddlFinancialYear.DataSource = UserManagementRisk.FillFnancialYear();
            ddlFinancialYear.DataBind();
            ddlFinancialYear.Items.Insert(0, new ListItem("Financial Year", "-1"));
        }

        public void BindLegalEntityData()
        {
            int userID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            mst_User user = UserManagementRisk.GetByID(Convert.ToInt32(userID));

            string role = RoleManagementRisk.GetByID(user.RoleID).Code;
            DepartmentHead = CustomerManagementRisk.GetDepartMentHeadId(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);
            string AuditHeadOrManager = "";
            AuditHeadOrManager = CustomerManagementRisk.GetAuditHeadOrManagerid(Convert.ToInt32(userID));

            ddlLegalEntity.DataTextField = "Name";
            ddlLegalEntity.DataValueField = "ID";
            ddlLegalEntity.Items.Clear();
            if (DepartmentHead)
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataDepartmentHead(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID, userID);
            }
            else if (role.Equals("MGMT"))
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataManagement(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID, userID);
            }
            else if (AuditHeadOrManager == "AM" || AuditHeadOrManager == "AH")
            {
                ddlLegalEntity.DataSource = AuditKickOff_NewDetails.FillLegalEntityDataAuditManager(CustomerId, userID);
            }
            ddlLegalEntity.DataBind();
            ddlLegalEntity.Items.Insert(0, new ListItem("Unit", "-1"));
        }

        public void BindSubEntityData(DropDownList DRP, int ParentId)
        {
            if (CustomerId == 0)
            {
                CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            }
            int UserID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID;
            DRP.DataTextField = "Name";
            DRP.DataValueField = "ID";
            DRP.Items.Clear();
            if (departmenthead)
            {
                DRP.DataSource = AuditKickOff_NewDetails.DepartmentHeadFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (CustomerManagementRisk.CheckIsManagement(UserID) == 8)
            {
                DRP.DataSource = AuditKickOff_NewDetails.ManagementFillSubEntityData(Portal.Common.AuthenticationHelper.UserID, CustomerId, ParentId);
            }
            else if (auditHeadFlag)
            {
                DRP.DataSource = AuditKickOff_NewDetails.AuditManagerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            else
            {
                DRP.DataSource = AuditKickOff_NewDetails.PerformerReviewerFillSubEntityData(UserID, CustomerId, ParentId);
            }
            //DRP.DataSource = AuditKickOff_NewDetails.FillSubEntityData(ParentId, com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
            DRP.DataBind();
            DRP.Items.Insert(0, new ListItem("Sub Unit", "-1"));
        }

        public void BindSchedulingType()
        {
            int branchid = -1;

            if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
            {
                if (ddlLegalEntity.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
            {
                if (ddlSubEntity1.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
            {
                if (ddlSubEntity2.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
            {
                if (ddlSubEntity3.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                }
            }

            if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
            {
                if (ddlFilterLocation.SelectedValue != "-1")
                {
                    branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                }
            }

            ddlSchedulingType.DataTextField = "Name";
            ddlSchedulingType.DataValueField = "ID";
            ddlSchedulingType.DataSource = UserManagementRisk.FillSchedulingTypeRahul(branchid);
            ddlSchedulingType.DataBind();
            ddlSchedulingType.Items.Insert(0, new ListItem("Scheduling Type", "-1"));
        }

        public void BindAuditSchedule(string flag, int count)
        {
            try
            {
                if (flag == "A")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Annually");
                }
                else if (flag == "H")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Sep");
                    ddlPeriod.Items.Insert(2, "Oct-Mar");
                }
                else if (flag == "Q")
                {
                    ddlPeriod.DataSource = null;
                    ddlPeriod.DataBind();
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr-Jun");
                    ddlPeriod.Items.Insert(2, "Jul-Sep");
                    ddlPeriod.Items.Insert(3, "Oct-Dec");
                    ddlPeriod.Items.Insert(4, "Jan-Mar");

                }
                else if (flag == "M")
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                    ddlPeriod.Items.Insert(1, "Apr");
                    ddlPeriod.Items.Insert(2, "May");
                    ddlPeriod.Items.Insert(3, "Jun");
                    ddlPeriod.Items.Insert(4, "Jul");
                    ddlPeriod.Items.Insert(5, "Aug");
                    ddlPeriod.Items.Insert(6, "Sep");
                    ddlPeriod.Items.Insert(7, "Oct");
                    ddlPeriod.Items.Insert(8, "Nov");
                    ddlPeriod.Items.Insert(9, "Dec");
                    ddlPeriod.Items.Insert(10, "Jan");
                    ddlPeriod.Items.Insert(11, "Feb");
                    ddlPeriod.Items.Insert(12, "Mar");
                }
                else
                {
                    if (count == 1)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                    }
                    else if (count == 2)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                    }
                    else if (count == 3)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                    }
                    else if (count == 4)
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                    }
                    else
                    {
                        ddlPeriod.Items.Clear();
                        ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                        ddlPeriod.Items.Insert(1, "Phase1");
                        ddlPeriod.Items.Insert(2, "Phase2");
                        ddlPeriod.Items.Insert(3, "Phase3");
                        ddlPeriod.Items.Insert(4, "Phase4");
                        ddlPeriod.Items.Insert(5, "Phase5");
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //throw;
            }
        }

        protected void grdSummaryDetailsAuditCoverage_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                int CustomerBranchId = Convert.ToInt32(commandArgs[0]);
                int VerticalId = Convert.ToInt32(commandArgs[1]);
                string FinancialYear = Convert.ToString(commandArgs[2]);
                string ForMonth = Convert.ToString(commandArgs[3]);
                int AuditID = Convert.ToInt32(commandArgs[4]);
                if (e.CommandName.Equals("DocumentDownLoadfile"))
                {
                    GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    int RowIndex = gvr.RowIndex;

                    DropDownList chkvalue = (DropDownList)grdSummaryDetailsAuditCoverage.Rows[RowIndex].FindControl("ddlDocumentFile");
                    int chkFileTtype = Convert.ToInt32(chkvalue.SelectedValue);

                    #region Working Document
                    if (chkFileTtype == 1)//Working
                    {
                        List<SP_MyDocumentAuditNewView_Working_Result> WorkingDocument = new List<SP_MyDocumentAuditNewView_Working_Result>();
                        if (WorkingDocument != null)
                        {
                            using (ZipFile AuditZip = new ZipFile())
                            {
                                WorkingDocument = DashboardManagementRisk.GetFileDataWorkingDocument(AuditID);

                                if (WorkingDocument.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in WorkingDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            //string[] filename = file.Name.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];

                                            int idx = file.Name.LastIndexOf('.');
                                            string str = file.Name.Substring(0, idx) + "_" + i + "." + file.Name.Substring(idx + 1);
                                            if (!AuditZip.ContainsEntry(FinancialYear + "" + file.ForMonth + "/" + file.Version + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    AuditZip.AddEntry(FinancialYear + "_" + file.ForMonth + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    AuditZip.AddEntry(FinancialYear + "_" + file.ForMonth + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                    var zipMs = new MemoryStream();
                                    AuditZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=WorkingDocuments.zip");
                                    Response.BinaryWrite(Filedata);
                                    Response.Flush();
                                    //Response.End();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Document available to Download.";
                                }
                            }
                        }
                    }
                    #endregion

                    #region Review Document
                    else if (chkFileTtype == 2)//Review Document
                    {
                        List<MyDocumentAuditView_ReviewNew> ReviewDocument = new List<MyDocumentAuditView_ReviewNew>();
                        if (ReviewDocument != null)
                        {
                            using (ZipFile AuditZip = new ZipFile())
                            {
                                ReviewDocument = DashboardManagementRisk.GetFileDataReviewDocument(AuditID);

                                if (ReviewDocument.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in ReviewDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            //string[] filename = file.Name.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];

                                            int idx = file.Name.LastIndexOf('.');
                                            string str = file.Name.Substring(0, idx) + "_" + i + "." + file.Name.Substring(idx + 1);
                                            if (!AuditZip.ContainsEntry(FinancialYear + "/" + file.ForMonth + "/" + file.Version + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    AuditZip.AddEntry(FinancialYear + "_" + file.ForMonth + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    AuditZip.AddEntry(FinancialYear + "_" + file.ForMonth + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    AuditZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=ReviewDocuments.zip");
                                    Response.BinaryWrite(Filedata);
                                    Response.Flush();
                                    //Response.End();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Document available to Download.";
                                }
                            }
                        }
                    }
                    #endregion

                    #region Annexure Document
                    else if (chkFileTtype == 3)//annexures
                    {
                        List<Tran_AnnxetureUpload> AnnxetureDocument = new List<Tran_AnnxetureUpload>();

                        if (AnnxetureDocument != null)
                        {
                            using (ZipFile AuditZip = new ZipFile())
                            {
                                AnnxetureDocument = DashboardManagementRisk.GetFileDataGetTran_AnnxetureUploadDocument(AuditID);

                                if (AnnxetureDocument.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in AnnxetureDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            //string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + "." + filename[1];

                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);
                                            if (!AuditZip.ContainsEntry(FinancialYear + "/" + file.Period + "/" + file.Version + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    AuditZip.AddEntry(FinancialYear + "_" + file.Period + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    AuditZip.AddEntry(FinancialYear + "_" + file.Period + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                    var zipMs = new MemoryStream();
                                    AuditZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=AnnexureDocuments.zip");
                                    Response.BinaryWrite(Filedata);
                                    Response.Flush();
                                    //Response.End();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Document available to Download.";
                                }
                            }
                        }
                    }
                    #endregion

                    #region Audit Committee Presentation
                    else if (chkFileTtype == 5)//Audit Committee Presentation
                    {
                        List<Tran_AuditCommitePresantionUpload> AuditCommitteeDocument = new List<Tran_AuditCommitePresantionUpload>();
                        if (AuditCommitteeDocument != null)
                        {
                            using (ZipFile AuditZip = new ZipFile())
                            {

                                AuditCommitteeDocument = DashboardManagementRisk.GetFileDataAuditCommiteDocument(AuditID);  /*customerbranchId*/

                                if (AuditCommitteeDocument.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in AuditCommitteeDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            //string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];

                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);

                                            if (!AuditZip.ContainsEntry(FinancialYear + "/" + file.Period + "/" + file.Version + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    AuditZip.AddEntry(FinancialYear + "_" + file.Period + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    AuditZip.AddEntry(FinancialYear + "_" + file.Period + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                    var zipMs = new MemoryStream();
                                    AuditZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=AuditCommitteeDocuments.zip");
                                    Response.BinaryWrite(Filedata);
                                    Response.Flush();
                                    //Response.End();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Document available to Download.";//No Document available to Download.
                                }
                            }
                        }
                    }
                    #endregion

                    #region Final Deliverables
                    else if (chkFileTtype == 6)//Final Deliverables
                    {
                        List<Tran_FinalDeliverableUpload> FinalDeliverableDocument = new List<Tran_FinalDeliverableUpload>();
                        if (FinalDeliverableDocument != null)
                        {
                            using (ZipFile AuditZip = new ZipFile())
                            {

                                FinalDeliverableDocument = DashboardManagementRisk.GetFileDataFinalDelivrablesDocument(AuditID);

                                if (FinalDeliverableDocument.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in FinalDeliverableDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            //string[] filename = file.FileName.Split('.');
                                            //string str = filename[0] + i + "." + filename[1];

                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);

                                            if (!AuditZip.ContainsEntry(FinancialYear + "/" + file.Period + "/" + file.Version + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    AuditZip.AddEntry(FinancialYear + "_" + file.Period + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    AuditZip.AddEntry(FinancialYear + "_" + file.Period + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    AuditZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=FinalDeliverableDocuments.zip");
                                    Response.BinaryWrite(Filedata);
                                    Response.Flush();
                                    //Response.End();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Document available to Download.";
                                }
                            }
                        }
                    }
                    #endregion

                    #region Deleted Document
                    if (chkFileTtype == 7)//Deleted Document
                    {
                        List<MyDocumentAuditNewView_Deleted> lstDeletedDocuments = new List<MyDocumentAuditNewView_Deleted>();
                        if (lstDeletedDocuments != null)
                        {
                            using (ZipFile AuditZip = new ZipFile())
                            {
                                lstDeletedDocuments = DashboardManagementRisk.GetFileDataDeletedDocument(AuditID);

                                if (lstDeletedDocuments.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in lstDeletedDocuments)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {

                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);

                                            if (!AuditZip.ContainsEntry(FinancialYear + "/" + file.ForMonth + "/" + file.Version + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    AuditZip.AddEntry(FinancialYear + "_" + file.ForMonth + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    AuditZip.AddEntry(FinancialYear + "_" + file.ForMonth + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }
                                    var zipMs = new MemoryStream();
                                    AuditZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=DeletedDocuments.zip");
                                    Response.BinaryWrite(Filedata);
                                    Response.Flush();
                                    //Response.End();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Document available to Download.";
                                }
                            }
                        }
                    }
                    #endregion

                    #region feedback document
                    if (chkFileTtype == 8)//Deleted Document
                    {

                        List<FeedbackFormUpload> FeedbackFormUploadDocument = new List<FeedbackFormUpload>();
                        if (FeedbackFormUploadDocument != null)
                        {
                            using (ZipFile AuditZip = new ZipFile())
                            {
                                FeedbackFormUploadDocument = DashboardManagementRisk.GetFileDataFeedbackFormUploadDocument(AuditID);

                                if (FeedbackFormUploadDocument.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in FeedbackFormUploadDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.Name));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            int idx = file.Name.LastIndexOf('.');
                                            string str = file.Name.Substring(0, idx) + "_" + i + "." + file.Name.Substring(idx + 1);

                                            if (!AuditZip.ContainsEntry(FinancialYear + "/" + ForMonth + "/" + file.Version + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    AuditZip.AddEntry(FinancialYear + "_" + ForMonth + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    AuditZip.AddEntry(FinancialYear + "_" + ForMonth + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    AuditZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=FeedbackForm.zip");
                                    Response.BinaryWrite(Filedata);
                                    Response.Flush();
                                    //Response.End();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "No Document available to Download.";
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindData()
        {
            try
            {
                String FinancialYear = String.Empty;
                String Period = String.Empty;
                string auditHeadManagerFlag = string.Empty;
                string departmentheadFlag = string.Empty;
                int Statusid = -1;
                int userID = -1;
                int RoleID = -1;
                int CustBranchID = -1;
                int VerticalID = -1;

                if (CustomerId == 0)
                {
                    CustomerId = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                }
                userID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.UserID);

                if (PerformerFlag)
                    RoleID = 3;
                if (ReviewerFlag)
                    RoleID = 4;
                if (auditHeadFlag)
                {
                    RoleID = 4;
                    auditHeadManagerFlag = AuditHeadOrManagerReport;
                }
                if (departmenthead)
                {
                    RoleID = 4;
                    departmentheadFlag = "DH";
                }
                
                if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                {
                    if (ddlLegalEntity.SelectedValue != "-1")
                    {
                        Session["BranchList"] = 0;
                        CustBranchID = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                    }
                }
                else
                {
                    List<long> branchs = new List<long>();
                    for (int i = 0; i < ddlLegalEntity.Items.Count; i++)
                    {
                        if (ddlLegalEntity.Items[i].Value != "-1")
                        {
                            if (ddlLegalEntity.Items[i].Value != "")
                            {
                                branchs.Add(Convert.ToInt64(ddlLegalEntity.Items[i].Value));
                                Session["BranchList"] = branchs;
                            }
                        }

                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                {
                    if (ddlSubEntity1.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                {
                    if (ddlSubEntity2.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                {
                    if (ddlSubEntity3.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                {
                    if (ddlFilterLocation.SelectedValue != "-1")
                    {
                        CustBranchID = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                    }
                }

                if (!String.IsNullOrEmpty(ddlFinancialYear.SelectedValue))
                {
                    if (ddlFinancialYear.SelectedValue != "-1")
                    {
                        FinancialYear = ddlFinancialYear.SelectedItem.Text;
                    }
                }

                if (!String.IsNullOrEmpty(ddlPeriod.SelectedValue))
                {
                    if (ddlPeriod.SelectedValue != "-1")
                    {
                        Period = ddlPeriod.SelectedItem.Text;
                    }
                }                              
                if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.IsVerticalApplicable == 0)
                {
                    int vid = UserManagementRisk.VerticalgetBycustomerid(Portal.Common.AuthenticationHelper.CustomerID);
                    if (vid != -1)
                    {
                        VerticalID = vid;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlVertical.SelectedValue))
                    {
                        if (ddlVertical.SelectedValue != "-1")
                        {
                            VerticalID = Convert.ToInt32(ddlVertical.SelectedValue);
                        }
                    }
                }

                if (!String.IsNullOrEmpty(ddlStatusDocument.SelectedValue))
                {
                    if (ddlStatusDocument.SelectedValue != "-1")
                    {
                        Statusid = Convert.ToInt32(ddlStatusDocument.SelectedValue);
                    }
                }

                Branchlist.Clear();
                List<long> branch = new List<long>();
                branch = Session["BranchList"] as List<long>;
                if (branch != null)
                {
                    for (int i = 0; i < branch.Count; i++)
                    {
                        CustBranchID = Convert.ToInt32(branch[i]);
                        GetAllHierarchy(CustomerId, CustBranchID);
                    }
                }
                else
                {
                    var bracnhes = GetAllHierarchy(CustomerId, CustBranchID);
                }

                var Branchlistloop = Branchlist.ToList();

                var detailViewtest = UserManagementRisk.getAllMyDocumentFile(CustomerId, Branchlist, VerticalID, FinancialYear, Period, RoleID, userID, Statusid, auditHeadManagerFlag, departmentheadFlag);
                grdSummaryDetailsAuditCoverage.DataSource = detailViewtest;
                Session["TotalRows"] = detailViewtest.Count;
                grdSummaryDetailsAuditCoverage.DataBind();

                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdSummaryDetailsAuditCoverage.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public int GetCurrentFinancialYearValue(string FinancialYear)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var query = (from row in entities.Mst_FinancialYear
                             where row.FinancialYear == FinancialYear
                             select row.Id).FirstOrDefault();
                return query;
            }
        }

        public String GetCurrentFinancialYear(DateTime ForDate)
        {
            String FinYear = String.Empty;

            if (ForDate != null)
            {
                if (ForDate.Month <= 3)
                    FinYear = (ForDate.Year) - 1 + "-" + ForDate.Year;
                else
                    FinYear = (ForDate.Year) + "-" + (ForDate.Year + 1);
            }

            return FinYear;
        }

        protected void ddlStatusDocument_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }       
        protected void ddlVertical_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void ddlLegalEntity_SelectedIndexChanged(object sender, EventArgs e)
        {            
            if (ddlLegalEntity.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity1, Convert.ToInt32(ddlLegalEntity.SelectedValue));
            }
            else
            {
                if (ddlSubEntity1.Items.Count > 0)
                {
                    ddlSubEntity1.Items.Clear();
                    ddlSubEntity1.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }
            }            
            BindVertical();
            BindSchedulingType();
            BindData();            
        }

        protected void ddlSubEntity1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity1.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity2, Convert.ToInt32(ddlSubEntity1.SelectedValue));
            }
            else
            {
                if (ddlSubEntity2.Items.Count > 0)
                {
                    ddlSubEntity2.Items.Clear();
                    ddlSubEntity2.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }
            }            
            BindVertical();
            BindSchedulingType();
            BindData();            
        }

        protected void ddlSubEntity2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity2.SelectedValue != "-1")
            {
                BindSubEntityData(ddlSubEntity3, Convert.ToInt32(ddlSubEntity2.SelectedValue));
            }
            else
            {
                if (ddlSubEntity3.Items.Count > 0)
                {
                    ddlSubEntity3.Items.Clear();
                    ddlSubEntity3.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }

                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }
            }


            BindVertical();
            BindSchedulingType();
            BindData();            
        }

        protected void ddlSubEntity3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSubEntity3.SelectedValue != "-1")
            {
                BindSubEntityData(ddlFilterLocation, Convert.ToInt32(ddlSubEntity3.SelectedValue));
            }
            else
            {
                if (ddlFilterLocation.Items.Count > 0)
                {
                    ddlFilterLocation.Items.Clear();
                    ddlFilterLocation.Items.Insert(0, new ListItem("Sub Unit", "-1"));
                }
            }


            BindVertical();
            BindSchedulingType();
            BindData();            
        }

        protected void ddlFilterLocation_SelectedIndexChanged(object sender, EventArgs e)
        {

            BindVertical();
            BindSchedulingType();
            BindData();
        }

        protected void ddlFinancialYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFinancialYear.SelectedValue != "-1")
            {
                BindData();
            }
        }

        protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e) 
        {
            BindData();
        }

        protected void ddlSchedulingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedulingType.SelectedValue != "-1")
            {
                if (ddlSchedulingType.SelectedItem.Text == "Annually")
                {
                    BindAuditSchedule("A", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Half Yearly")
                {
                    BindAuditSchedule("H", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Quarterly")
                {
                    BindAuditSchedule("Q", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Monthly")
                {
                    BindAuditSchedule("M", 0);
                }
                else if (ddlSchedulingType.SelectedItem.Text == "Phase")
                {
                    int branchid = -1;

                    if (!String.IsNullOrEmpty(ddlLegalEntity.SelectedValue))
                    {
                        if (ddlLegalEntity.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlLegalEntity.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity1.SelectedValue))
                    {
                        if (ddlSubEntity1.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity1.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity2.SelectedValue))
                    {
                        if (ddlSubEntity2.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity2.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlSubEntity3.SelectedValue))
                    {
                        if (ddlSubEntity3.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlSubEntity3.SelectedValue);
                        }
                    }

                    if (!String.IsNullOrEmpty(ddlFilterLocation.SelectedValue))
                    {
                        if (ddlFilterLocation.SelectedValue != "-1")
                        {
                            branchid = Convert.ToInt32(ddlFilterLocation.SelectedValue);
                        }
                    }

                    int count = 0;
                    count = UserManagementRisk.GetPhaseCount(branchid);
                    BindAuditSchedule("P", count);
                }
            }
            else
            {
                if (ddlPeriod.Items.Count > 0)
                {
                    ddlPeriod.Items.Clear();
                    ddlPeriod.Items.Insert(0, new ListItem("Period", "-1"));
                }
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdSummaryDetailsAuditCoverage.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);                
                BindData();                                         
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                
            }
        }
          
        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }
        public string GetProcessName(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Processlist = (from row in entities.ProcessCommaSaperates
                                   where row.AuditID == AuditID
                                   select row.ProcessName).FirstOrDefault();

                return Processlist;
            }
        }

        public string GetSubProcessName(int AuditID)
        {
            using (AuditControlEntities entities = new AuditControlEntities())
            {
                var Processlist = (from row in entities.SubProcessCommaSeparates
                                   where row.AuditID == AuditID
                                   select row.SubProcessName).FirstOrDefault();

                return Processlist;
            }
        }
    }
}