﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InternalAuditControlAuditManager.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.AuditTool.InternalAuditControlAuditManager" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../Content/2018.3.1017/css/kendo.default.min.css" rel="stylesheet" />
    <link href="../../Content/2018.3.1017/css/kendo.common.min.css" rel="stylesheet" />
    <%--<link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />--%>
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <script src="../../Newjs/jquery-3.6.0.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jquery-1.12.4.min.js"></script>--%>
    <%--<script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>--%>
    <script src="../../Content/2018.3.1017/js/kendo.all.min.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jszip.min.js"></script>--%>
    <script src="../../Content/2018.3.1017/js/jszip.min.js"></script>
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>

    <style>
        .k-dropzone {
            padding: unset !important;
        }

        div#tabstrip-1 {
            height: 100% !important;
        }

        div#tabstrip-2 {
            height: 100% !important;
        }

        div#tabstrip-3 {
            height: 100% !important;
        }

        .bootRBL input {
            display: inline;
            margin-right: 0.25em;
        }

        .bootRBL label {
            display: inline;
            margin-right: 1em;
        }

        button, html input[type=button], input[type=reset], input[type=submit] {
            -webkit-appearance: button;
            cursor: pointer;
        }

        .modal, .modal-backdrop {
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .modal-content, .popover {
            background-clip: padding-box;
        }

        .modal-open .navbar-fixed-bottom, .modal-open .navbar-fixed-top, body.modal-open {
            margin-right: 15px;
        }

        .modal {
            display: none;
            overflow: auto;
            overflow-y: scroll;
            position: fixed;
            z-index: 1040;
        }

            .modal.fade .modal-dialog {
                -webkit-transform: translate(0,-25%);
                -ms-transform: translate(0,-25%);
                transform: translate(0,-25%);
                -webkit-transition: -webkit-transform .3s ease-out;
                -moz-transition: -moz-transform .3s ease-out;
                -o-transition: -o-transform .3s ease-out;
                transition: transform .3s ease-out;
            }

            .modal.in .modal-dialog {
                -webkit-transform: translate(0,0);
                -ms-transform: translate(0,0);
                transform: translate(0,0);
            }

        .modal-dialog {
            margin-left: auto;
            margin-right: auto;
            width: auto;
            padding: 10px;
            z-index: 1050;
        }

        .modal-content {
            position: relative;
            background-color: #fff;
            border: 1px solid #999;
            border: 1px solid rgba(0,0,0,.2);
            border-radius: 6px;
            -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
            box-shadow: 0 3px 9px rgba(0,0,0,.5);
            outline: 0;
        }

        .modal-backdrop {
            position: fixed;
            z-index: 1030;
            background-color: #000;
        }

            .modal-backdrop.fade {
                opacity: 0;
                filter: alpha(opacity=0);
            }

            .carousel-control, .modal-backdrop.in {
                opacity: .5;
                filter: alpha(opacity=50);
            }

        .modal-header {
            padding: 15px;
            border-bottom: 1px solid #e5e5e5;
            min-height: 16.43px;
        }

            .modal-header .close {
                margin-top: -2px;
            }

        .modal-title {
            margin: 0;
            line-height: 1.428571429;
        }

        .modal-body {
            position: relative;
            padding: 20px;
        }


        .modal-footer {
            margin-top: 15px;
            padding: 19px 20px 20px;
            text-align: right;
            border-top: 1px solid #e5e5e5;
        }



            .modal-footer:after, .modal-footer:before {
                content: " ";
                display: table;
            }

            .modal-footer .btn + .btn {
                margin-left: 5px;
                margin-bottom: 0;
            }

            .modal-footer .btn-group .btn + .btn {
                margin-left: -1px;
            }

            .modal-footer .btn-block + .btn-block {
                margin-left: 0;
            }

        @media screen and (min-width:768px) {
            .modal-dialog {
                left: 50%;
                right: auto;
                width: 600px;
                padding-top: 30px;
                padding-bottom: 30px;
            }

            .modal-content {
                -webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);
                box-shadow: 0 5px 15px rgba(0,0,0,.5);
            }
        }

        .btn-group-vertical > .btn-group:after, .btn-toolbar:after, .clearfix:after, .container:after, .dropdown-menu > li > a, .form-horizontal .form-group:after, .modal-footer:after, .nav:after, .navbar-collapse:after, .navbar-header:after, .navbar:after, .pager:after, .panel-body:after, .row:after {
            clear: both;
        }

        .media, .media-body, .modal-open, .progress {
            overflow: hidden;
        }

        .close, .list-group-item > .badge {
            float: right;
        }

        .close {
            font-size: 21px;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            opacity: .2;
            filter: alpha(opacity=20);
        }

            .close:focus, .close:hover {
                color: #000;
                text-decoration: none;
                cursor: pointer;
                opacity: .5;
                filter: alpha(opacity=50);
            }

        button.close {
            padding: 0;
            cursor: pointer;
            background: 0 0;
            border: 0;
            -webkit-appearance: none;
        }
    </style>
    <style type="text/css">
        .col-md-12 {
            width: 100%;
            margin-bottom: 2%;
        }

        .MainWrapper {
            width: 15%;
            display: block;
            float: left;
            font-weight: 500;
            margin-left: 2.2%;
        }

        button, html input[type=button], input[type=reset], input[type=submit] {
            -webkit-appearance: button;
            cursor: pointer;
        }

        .modal, .modal-backdrop {
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .modal-content, .popover {
            background-clip: padding-box;
        }

        .modal-open .navbar-fixed-bottom, .modal-open .navbar-fixed-top, body.modal-open {
            margin-right: 15px;
        }

        .modal {
            display: none;
            overflow: auto;
            overflow-y: scroll;
            position: fixed;
            z-index: 1040;
        }

            .modal.fade .modal-dialog {
                -webkit-transform: translate(0,-25%);
                -ms-transform: translate(0,-25%);
                transform: translate(0,-25%);
                -webkit-transition: -webkit-transform .3s ease-out;
                -moz-transition: -moz-transform .3s ease-out;
                -o-transition: -o-transform .3s ease-out;
                transition: transform .3s ease-out;
            }

            .modal.in .modal-dialog {
                -webkit-transform: translate(0,0);
                -ms-transform: translate(0,0);
                transform: translate(0,0);
            }

        .modal-dialog {
            margin-left: auto;
            margin-right: auto;
            width: auto;
            padding: 10px;
            z-index: 1050;
        }

        .modal-content {
            position: relative;
            background-color: #fff;
            border: 1px solid #999;
            border: 1px solid rgba(0,0,0,.2);
            border-radius: 6px;
            -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
            box-shadow: 0 3px 9px rgba(0,0,0,.5);
            outline: 0;
        }

        .modal-backdrop {
            position: fixed;
            z-index: 1030;
            background-color: #000;
        }

            .modal-backdrop.fade {
                opacity: 0;
                filter: alpha(opacity=0);
            }

            .carousel-control, .modal-backdrop.in {
                opacity: .5;
                filter: alpha(opacity=50);
            }

        .modal-header {
            padding: 15px;
            border-bottom: 1px solid #e5e5e5;
            min-height: 16.43px;
        }

            .modal-header .close {
                margin-top: -2px;
            }

        .modal-title {
            margin: 0;
            line-height: 1.428571429;
        }

        .modal-body {
            position: relative;
            padding: 20px;
        }


        .modal-footer {
            margin-top: 15px;
            padding: 19px 20px 20px;
            text-align: right;
            border-top: 1px solid #e5e5e5;
        }



            .modal-footer:after, .modal-footer:before {
                content: " ";
                display: table;
            }

            .modal-footer .btn + .btn {
                margin-left: 5px;
                margin-bottom: 0;
            }

            .modal-footer .btn-group .btn + .btn {
                margin-left: -1px;
            }

            .modal-footer .btn-block + .btn-block {
                margin-left: 0;
            }

        @media screen and (min-width:768px) {
            .modal-dialog {
                left: 50%;
                right: auto;
                width: 600px;
                padding-top: 30px;
                padding-bottom: 30px;
            }

            .modal-content {
                -webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);
                box-shadow: 0 5px 15px rgba(0,0,0,.5);
            }
        }

        .btn-group-vertical > .btn-group:after, .btn-toolbar:after, .clearfix:after, .container:after, .dropdown-menu > li > a, .form-horizontal .form-group:after, .modal-footer:after, .nav:after, .navbar-collapse:after, .navbar-header:after, .navbar:after, .pager:after, .panel-body:after, .row:after {
            clear: both;
        }

        .media, .media-body, .modal-open, .progress {
            overflow: hidden;
        }

        .close, .list-group-item > .badge {
            float: right;
        }

        .close {
            font-size: 21px;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            opacity: .2;
            filter: alpha(opacity=20);
        }

            .close:focus, .close:hover {
                color: #000;
                text-decoration: none;
                cursor: pointer;
                opacity: .5;
                filter: alpha(opacity=50);
            }

        button.close {
            padding: 0;
            cursor: pointer;
            background: 0 0;
            border: 0;
            -webkit-appearance: none;
        }

        input[type=checkbox], input[type=radio] {
            margin: 4px 6px 0;
            margin-top: 1px\9;
            line-height: normal;
        }

        .k-list > .k-state-focused.k-state-selected, .k-listview > .k-state-focused.k-state-selected, .k-state-focused.k-state-selected, td.k-state-focused.k-state-selected {
            -webkit-box-shadow: inset 0 0 3px 1px #14699f;
            box-shadow: inset 0 0 1px 1px #14699f;
        }

        .k-grid-header th.k-state-focused, .k-list > .k-state-focused, .k-listview > .k-state-focused, .k-state-focused, td.k-state-focused {
            -webkit-box-shadow: inset 0 0 3px 1px #d7dae0;
            box-shadow: inset 0 0 1px 1px white;
        }

        .div.k-grid-footer, div.k-grid-header {
            border-top-width: 1px;
            margin-right: 0px;
        }

        .k-grid-footer-wrap, .k-grid-header-wrap {
            position: relative;
            width: 100%;
            overflow: hidden;
            border-style: solid;
            border-width: 0 1px 0 0;
            zoom: 1;
        }

        .k-grid-content {
            overflow: hidden;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .myKendoCustomClass {
            z-index: 999 !important;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 2.0em;
            border-bottom-width: 1px;
            background-color: white;
            border-width: 0 1px 1px 0px;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
            background-color: white;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            border-color: #1fd9e1;
            background-color: white;
        }

        .k-pager-wrap > .k-link > .k-icon {
            margin-top: -2px;
            color: inherit;
        }

        html .k-grid tr:hover {
            background: white;
        }

        html .k-grid tr.k-alt:hover {
            background: white;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            background: white;
            border: none;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
            margin-right: 2px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: white;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 0px 1px 0px;
            text-align: center;
        }

        .k-grid-pager {
            margin-top: -1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 0px;
        }

        .col-md-2 {
            width: 20%;
        }

        .k-i-arrow-60-down, .k-i-arrow-60-left, .k-i-arrow-60-right, .k-i-arrow-60-up {
            cursor: pointer;
            margin-top: px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
            background: #E9EAEA;
            font-weight: bold;
            margin-right: 18px;
            font-family: 'Roboto',sans-serif;
            font-size: 15px;
            color: rgba(0, 0, 0, 0.5);
            height: 20px;
            vertical-align: middle;
        }

        .k-dropdown-wrap.k-state-default {
            background-color: white;
        }

        .k-popup.k-calendar-container, .k-popup.k-list-container {
            background-color: white;
        }

        label.k-label {
            font-family: roboto,sans-serif !important;
            color: #515967;
            /* font-stretch: 100%; */
            font-style: normal;
            font-weight: 400;
            min-width: 362px;
            white-space: pre-wrap;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: flex;
            margin-bottom: 0px;
        }

        .k-state-default > .k-select {
            border-color: #ceced2;
            margin-top: 0px;
        }

        .k-grid-norecords {
            width: 100%;
            height: 100%;
            text-align: center;
            margin-top: 2%;
        }

        .k-grouping-header {
            font-style: italic;
            background-color: white;
        }

        .k-grid table {
            width: 100.5%;
        }

        .k-active-filter, .k-state-active, .k-state-active:hover {
            background-color: #E9EAEA;
            border-color: #a6a6ad;
            color: #535b6a;
        }

        .k-multiselect-wrap .k-input {
            /*padding-top:6px;*/
            display: inherit !important;
        }

        html .k-upload {
            position: relative;
        }

        .k-clear-selected, .k-upload-selected {
            display: none !important;
        }
    </style>

    <script>
        sessionStorage.setItem('Observation', null)

        $(document).ready(function () {
            $("input").attr("autocomplete", "off");
            $("#tabstrip").kendoTabStrip({
                animation: {
                    open: {
                        effects: "fadeIn"
                    }
                }
            });
            var presentDate = new Date();
            $("#txtTimeLine").kendoDatePicker({                
                format: "dd/MM/yyyy",
                value: new Date(),
                min: presentDate,
                month: {
                    empty: '<div class="k-state-disabled">#= data.value #</div>'}
                //disableDates: function (date) {
                //    if (date < new Date()) {
                //        return true;
                //    } else {
                //        return false;
                //    }
                //}
            });
            $("#txtResponseDueDate").kendoDatePicker({
                format: "dd/MM/yyyy",
                value: new Date(),
                min: presentDate,
                month: {
                    empty: '<div class="k-state-disabled">#= data.value #</div>'}
                //disableDates: function (date) {
                //    if (date < new Date()) {
                //        return true;
                //    } else {
                //        return false;
                //    }
                //}
            });

            var oldValue =$("#txtObservation").val();
            //sessionStorage.setItem('Observation', oldValue);

            $("#editor").kendoEditor({
                tools: [
                    "tableWizard",
                    "createTable",
                    "addRowAbove",
                    "addRowBelow",
                    "addColumnLeft",
                    "addColumnRight",
                    "deleteRow",
                    "deleteColumn",
                    "mergeCellsHorizontally",
                    "mergeCellsVertically",
                    "splitCellHorizontally",
                    "splitCellVertically",
                    "tableAlignLeft",
                    "tableAlignCenter",
                    "tableAlignRight"
                ],
                encoded: false
            });

            $("#ddlObservationSubCategory").kendoDropDownList({
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                change: function () {
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: null,
                        }
                    }
                }
            });
            BindAuditDetialReviewPeriod(); 
            BindGroupLevelObj();
            BindOjbReport();
            BindPersonResponsible();
            BindOwner();
            BindCategory();
            BindWorkingDocuments();
            BindAnnexureDocuments();
            BindImageFiles();
            BindWorkingDocumentsTwo();
            BindAuditLogGrid();
            BindReviewHistoryGrid();
            BindData();
            UploadWorkingDocument();
            UploadAnnexureDocument();
            UploadWorkingDocumentTwo();
            UploadImageFile();
            UploadReviewHistoryFile();
            //Nextbutton 1
            $("#btn1").click(function (event) {
                btnSubmit_click(event);
            });
            $("#btn2").click(function (event) {
                btnSubmit2_click(event);
            });
            $("#btn3").click(function (event) {
                btnSubmit3_click(event);
            });

            $("#btnSave").click(function (event) {  
                btnSave_click(event);
            });

            //working file 1 Upload File
            $("#btnWFUpload").click(function (e) {
                e.preventDefault();
                var upload = $("#WFUpload").data("kendoUpload"),
                    files = upload.getFiles();
                if (files.length > 0) {
                    $(".k-upload-selected").click();
                }
                else {
                    alert("Select Document to Upload");
                }
            });

            //Annexture File Upload
            $("#btnAFUpload").click(function (e) {
                e.preventDefault();
                var upload = $("#AFUpload").data("kendoUpload"),
                    files = upload.getFiles();
                if (files.length > 0) {
                    $(".k-upload-selected").click();
                }
                else {
                    alert("Select Document to Upload");
                }
            });

            //working file 2 Upload File
            $("#btnWFUploadObj").click(function (e) {
                e.preventDefault();
                var upload = $("#WFUploadObj").data("kendoUpload"),
                    files = upload.getFiles();
                if (files.length > 0) {
                    $(".k-upload-selected").click();
                }
                else {
                    alert("Select Document to Upload");
                }
            });

            $("#btnImgUpload").click(function (e) {
                e.preventDefault();
                var upload = $("#ImageUpload").data("kendoUpload"),
                    files = upload.getFiles();
                if (files.length > 0) {
                    $(".k-upload-selected").click();
                }
                else {
                    alert("Select Document to Upload");
                }
            });

            $("#btnRHUpload").click(function (e) {
                e.preventDefault();
                var upload = $("#RVUpload").data("kendoUpload"),
                    files = upload.getFiles();
                if (files.length > 0) {
                    $(".k-upload-selected").click();
                }
                else {
                    alert("Select Document to Upload");
                }
            });

            if (<%=StatusID%> == 5) {
                EnableCotrol(false);
            }
            else {
                EnableCotrol(true);
            }
        });

        function OpenUserPopup() {
            $('#AddUserModelPopup').modal('show');
            $('#IUser').attr('width', '100%');
            $('#IUser').attr('height', '400px');
            $('.modal-dialog').css('width', '60%');
            $('.modal-dialog').css('height', '500px');
            $('#IUser').attr('src', "../../RiskManagement/AuditTool/AuditUser.aspx");
        }

        function CloseUserPage() {
            $('#AddUserModelPopup').modal('hide');
            BindPersonResponsible();
        }

        $(document).on("click", "#btnObjListing", function (e) {    
            
            var obs = true;
            var oldValue =$("#txtObservation").val();
            //sessionStorage.setItem('Observation', oldValue);
            var sessionvalue =sessionStorage.getItem('Observation');
            
            if (sessionvalue != undefined || sessionvalue != null || sessionvalue != '') {
                $.ajax({
                    type: "POST",
                    url: "../InternalAuditTool/DetailedObservationList.aspx/SetSessionObj",
                    data: '{name: "' + sessionvalue + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        
                    },
                    failure: function (response) {
                        //alert(response.responseText);
                    },
                    error: function (response) {
                        // alert(response.responseText);
                    }
                });
            }

            if (obs != 'false') {
                var value = sessionStorage.getItem('auditIdatbdId');
                sessionStorage.setItem('auditIdatbdId', value);
                $('#divPREDOCShowDialog').modal('show');
                $('#DetObsshowdetails').attr('width', '100%');
                $('#DetObsshowdetails').attr('height', '600px');
                $('.modal-dialog').css('width', '95%');
                // $('.modal-dialog').css('margin-left', '-2%');
                $('#DetObsshowdetails').attr('src', "../InternalAuditTool/DetailedObservationList.aspx?ProcessId=" + <%=ProcessID%> + "&SubProcessId=" + <%=SubProcessId%> + "&AuditStepid=" + <%=AuditStepID%> + "&BranchId=" + <%=BranchID%> + "&StatusId=" + <%=StatusID%> + "&Redirect=WS");
            }
            else {
                $('#divPREDOCShowDialog').modal('show');
                $('#DetObsshowdetails').attr('width', '100%');
                $('#DetObsshowdetails').attr('height', '600px');
                $('.modal-dialog').css('width', '104%');
                $('.modal-dialog').css('margin-left', '-2%');
                $('#DetObsshowdetails').attr('src', "../InternalAuditTool/DetailedObservationList.aspx?ProcessId=" + <%=ProcessID%> + "&SubProcessId=" + <%=SubProcessId%> + "&AuditStepid=" + <%=AuditStepID%> + "&BranchId=" + <%=BranchID%> + "&StatusId=" + <%=StatusID%> + "&Redirect=WS");
            }
        });


        function BindAuditDetialReviewPeriod() {
            $.ajax({
                type: 'POST',
                url: "../../AuditStatus/BindAuditDetialReviewPeriod?AuditID=<%=AuditID%>&ATBDID=<%=ATBDID%>&ProcessID=<%=ProcessID%>",
                dataType: "json",
                success: function (result) {
                    if (result != null) {
                        $('#lblAuditName1').text(result.AuditName);
                        //  $('#lblReviewPeriod1').text(result.ReviewPeriod);

                        $('#lblAuditName2').text(result.AuditName);
                        // $('#lblReviewPeriod2').text(result.ReviewPeriod);

                        $('#lblAuditName3').text(result.AuditName);
                        //  $('#lblReviewPeriod3').text(result.ReviewPeriod);

                        $('#lblAuditName4').text(result.AuditName);
                        //  $('#lblReviewPeriod4').text(result.ReviewPeriod);
                    }
                },
                error: function (result) {
                    // notify the data source that the request failed
                    console.log(result);
                }
            });
        }


        function HistoryMethod() {
            $('#divPREDOCShowDialog').modal('hide');
            var value = sessionStorage.getItem('auditIdatbdId');
            var resultset = value.split(",");
            $.ajax({
                type: 'POST',
                url: "../../AuditStatus/AuditClosureDetailsforHistoricalObj?AuditID="+resultset[0]+"&ATBDID="+resultset[1],
                dataType: "json",
                success: function (result) {
                    if (result != null) {
                        $("#txtObservationTitle").val(result.ObservationTitle);
                        $("#txtObservation").val(result.Observation);
                        $("#tbxBriefObservation").val(result.BriefObservation);
                        $("#tbxObjBackground").val(result.ObjBackground);
                        $("#txtRisk").val(result.Risk);
                        $("#txtRootcost").val(result.RootCost);
                        $("#txtfinancialImpact").val(result.FinancialImpact);
                        $("#txtRecommendation").val(result.Recomendation);
                        $("#txtMgtResponse").val(result.ManagementResponse);
                        $("#ddlPersonresponsible").data("kendoDropDownList").value(result.PersonResponsible);
                        $("#ddlobservationRating").data("kendoDropDownList").value(result.ObservationRating);
                        $("#ddlObservationCategory").data("kendoDropDownList").value(result.ObservationCategory);
                        var oldValue =$("#txtObservation").val();
                        sessionStorage.setItem('Observation', oldValue);
                    }
                },
                error: function (result) {
                    // notify the data source that the request failed
                    console.log(result);
                }
            });
        }

        function RemoveData() {
            $('#divPREDOCShowDialog').modal('hide');
            $('#txtObservationTitle').val(" ");
            $('#txtObservation').val(" ");
            $('#tbxBriefObservation').val(" ");
            $('#tbxObjBackground').val(" ");
            $('#txtRisk').val(" ");
            $('#txtRootcost').val(" ");
            $('#txtfinancialImpact').val(" ");
            $('#txtRecommendation').val(" ");
            $('#txtMgtResponse').val(" ");
            $('#ddlPersonresponsible').data("kendoDropDownList").select(0);
            $('#ddlobservationRating').data("kendoDropDownList").select(0);
            $('#ddlObservationCategory').data("kendoDropDownList").select(0);
            $('#ddlOwnerName').data("kendoDropDownList").select(0);
            $('#ddlObservationSubCategory').data("kendoDropDownList").select(0);
        }


        function RedirectEditModeOnAdd() {
            // $scope.tabstrip.select(1);
        }

        function UploadWorkingDocument() {
            $("#WFUpload").kendoUpload({
                async: {
                    saveUrl: "../../AuditStatus/UpladWorkingFile?AuditID=<% =AuditID%>&ATBDID=<%=ATBDID%>&CustomerId=<%=CustomerId%>&UserID=<%=UserID%>",
                    autoUpload: false
                },
                multiple: true,
                upload: function (e) {
                    e.sender.options.async.saveUrl = "../../AuditStatus/UpladWorkingFile?AuditID=<%=AuditID%>&ATBDID=<%=ATBDID%>&CustomerId=<%=CustomerId%>&UserID=<%=UserID%>";
                },
                success: function (e) {
                    if (e.response != null) {
                        $(".k-upload-files.k-reset").find("li").remove();
                        BindWorkingDocuments();
                        //document.getElementById('divDocumentAdd').scrollIntoView();
                    }
                    else {
                        $("#lblDocDetails").text('Error at Uploading Documents');
                        //$("#divDocDetails").show();
                    }
                    // setTimeout(function () { $("#divDocDetails").hide(); }, 3000);
                    //***Give here error message 
                }
            });
            }

            //Annexure File upload
            function UploadAnnexureDocument() {
                $("#AFUpload").kendoUpload({
                    async: {
                        saveUrl: "../../AuditStatus/UpladAnnexureFile?AuditID=<% =AuditID%>&ATBDID=<%=ATBDID%>&CustomerId=<%=CustomerId%>&UserID=<%=UserID%>",
                        autoUpload: false
                    },
                    multiple: true,
                    upload: function (e) {
                        e.sender.options.async.saveUrl = "../../AuditStatus/UpladAnnexureFile?AuditID=<%=AuditID%>&ATBDID=<%=ATBDID%>&CustomerId=<%=CustomerId%>&UserID=<%=UserID%>";
                    },
                    success: function (e) {
                        if (e.response != null) {
                            $(".k-upload-files.k-reset").find("li").remove();
                            BindAnnexureDocuments();
                            //document.getElementById('divDocumentAdd').scrollIntoView();
                        }
                        else {
                            $("#lblDocDetails").text('Error at Uploading Documents');
                            //$("#divDocDetails").show();
                        }
                        // setTimeout(function () { $("#divDocDetails").hide(); }, 3000);
                        //***Give here error message 
                    }
                });
                }

                //Working File Upload Two
                function UploadWorkingDocumentTwo() {
                    $("#WFUploadObj").kendoUpload({
                        async: {
                            saveUrl: "../../AuditStatus/UploadWorkingDocumentTwo?AuditID=<%=AuditID%>&ATBDID=<%=ATBDID%>&CustomerId=<%=CustomerId%>&UserID=<%=UserID%>",
                            autoUpload: false
                        },
                        multiple: true,
                        upload: function (e) {
                            e.sender.options.async.saveUrl = "../../AuditStatus/UploadWorkingDocumentTwo?AuditID=<%=AuditID%>&ATBDID=<%=ATBDID%>&CustomerId=<%=CustomerId%>&UserID=<%=UserID%>";
                        },
                        success: function (e) {
                            if (e.response != null) {
                                $(".k-upload-files.k-reset").find("li").remove();
                                BindWorkingDocumentsTwo();
                                //document.getElementById('divDocumentAdd').scrollIntoView();
                            }
                            else {
                                $("#lblDocDetails").text('Error at Uploading Documents');
                                //$("#divDocDetails").show();
                            }
                            // setTimeout(function () { $("#divDocDetails").hide(); }, 3000);
                            //***Give here error message 
                        }
                    });
                    }

                    //Image Upload
                    function UploadImageFile() {
                        $("#ImageUpload").kendoUpload({
                            async: {
                                saveUrl: "../../AuditStatus/UploadImageFile?AuditID=<% =AuditID%>&ATBDID=<%=ATBDID%>&CustomerId=<%=CustomerId%>&UserID=<%=UserID%>",
                                autoUpload: false
                            },
                            multiple: true,
                            upload: function (e) {
                                e.sender.options.async.saveUrl = "../../AuditStatus/UploadImageFile?AuditID=<%=AuditID%>&ATBDID=<%=ATBDID%>&CustomerId=<%=CustomerId%>&UserID=<%=UserID%>";
                            },
                            success: function (e) {
                                if (e.response != null) {
                                    $(".k-upload-files.k-reset").find("li").remove();
                                    BindImageFiles();
                                    //document.getElementById('divDocumentAdd').scrollIntoView();
                                }
                                else {
                                    $("#lblDocDetails").text('Error at Uploading Documents');
                                    //$("#divDocDetails").show();
                                }
                                // setTimeout(function () { $("#divDocDetails").hide(); }, 3000);
                                //***Give here error message 
                            }
                        });
                        }

                        function BindData() {
                            $("#txtAuditObjective").val("<%=AuditObjective%>");
                            $("#txtAuditSteps").val("<%= AuditSteps%>");
                            $("#txtAnalysisToBePerformed").val("<%=AnalysisToBePerofrmed%>");
                            $("#txtWalkthrough").val("<%=ProcessWalkthrough%>");
                            $("#txtActualWorkDone").val("<%=ActivityToBeDone%>");
                            $("#txtpopulation").val("<%=Population%>");
                            $("#txtSample").val("<%=Sample%>");
                            $("#tbxRemarks").val("<%=FixRemark%>");
                            $("#txtObservationTitle").val("<%=ObservationTitle%>");
                            $("#txtObservation").val("<%=Observation%>");
                            $("#tbxBriefObservation").val("<%=BriefObservation%>");
                            $("#tbxObjBackground").val("<%=ObjBackground%>");
                            $("#txtAnnexueTitle").val("<%=AnnexueTitle%>");                                
                            $("#txtMultilineVideolink").val("<%=AudioVideoLink%>");
                            $("#ObjReport1").data("kendoDropDownList").value("<%=ObjReport1%>");
                            $("#txtRisk").val("<%=Risk%>");
                            $("#txtRootcost").val("<%=RootCost%>");
                            $("#txtfinancialImpact").val("<%=FinancialImpact%>");
                            $("#txtRecommendation").val("<%=Recomendation%>");
                            $("#txtResponseDueDate").val("<%=ResponseDueDate%>");
                            $("#txtMgtResponse").val("<%=ManagementResponse%>");
                            $("#txtTimeLine").val("<%=TimeLine%>");
                            $("#ddlPersonresponsible").data("kendoDropDownList").value("<%=PersonResponsible%>");
                            $("#ddlOwnerName").data("kendoDropDownList").value("<%=Owner%>");
                            $("#ddlobservationRating").data("kendoDropDownList").value("<%=ObservationRating%>");
                            $("#ddlObservationCategory").data("kendoDropDownList").value("<%=ObservationCategory%>");
                            //$('#ddlObservationCategory').data("kendoDropDownList").trigger('change');
                            if ("<%=ObservationCategory%>" != '') {
                                f1("<%=ObservationCategory%>");
                            }
                            $("#ddlObservationSubCategory").data("kendoDropDownList").value("<%=ObservationSubCategory%>");
                            
                            var editor = $("#editor").data("kendoEditor");
                            editor.value('<%=BodyContent%>');
                            
                            //$("#editor").data("kendoEditor").value('<%=BodyContent%>');
                            //$("#editor").data("kendoEditor").body.innerHTML = '<%=BodyContent%>';
                            //$($("#editor").data().kendoEditor.body).innerHTML = "<%=BodyContent%>";
                            //$("#editor").val('<%=BodyContent%>');
                            //$($("#txtNarrative").data().kendoEditor.body).attr("contenteditable", $scope.formData.edit);
                        }

        function EnableCotrol(result)
        {
            $("#form1").find(".k-button").each(function(index)
            {
                $(this).prop( "disabled", result );
            });
            //$(document).ready(function(){
            //    $("#form1 :input").prop("disabled", result);
            //});
            $("#form1 :input").each(function(index)
            {
                if (this.id == "txtAuditObjective" || this.id == "txtAuditSteps" || this.id == "txtAnalysisToBePerformed") {
    
                }
                else{
                    $(this).prop( "disabled", result );
                }
            });

            if (!result) {
                $('#txtTimeLine').data('kendoDatePicker').enable(true);
                $('#txtResponseDueDate').data('kendoDatePicker').enable(true);
                var dropdownlist1 = $("#ddlPersonresponsible").data("kendoDropDownList");
                dropdownlist1.enable(true);
                var dropdownlist2 = $("#ddlOwnerName").data("kendoDropDownList");
                dropdownlist2.enable(true);
                var dropdownlist3 = $("#ddlGroupLevelObservation").data("kendoDropDownList");
                dropdownlist3.enable(true);
                var dropdownlist4 = $("#ObjReport1").data("kendoDropDownList");
                dropdownlist4.enable(true);
                var dropdownlist5 = $("#ddlobservationRating").data("kendoDropDownList");
                dropdownlist5.enable(true);
                var dropdownlist6 = $("#ddlObservationCategory").data("kendoDropDownList");
                dropdownlist6.enable(true);
                var dropdownlist7 = $("#ddlObservationSubCategory").data("kendoDropDownList");
                dropdownlist7.enable(true);
            }
            else {
                $('#txtTimeLine').data('kendoDatePicker').enable(false);
                $('#txtResponseDueDate').data('kendoDatePicker').enable(false);
                var dropdownlist1 = $("#ddlPersonresponsible").data("kendoDropDownList");
                dropdownlist1.enable(false);
                var dropdownlist2 = $("#ddlOwnerName").data("kendoDropDownList");
                dropdownlist2.enable(false);
                var dropdownlist3 = $("#ddlGroupLevelObservation").data("kendoDropDownList");
                dropdownlist3.enable(false);
                var dropdownlist4 = $("#ObjReport1").data("kendoDropDownList");
                dropdownlist4.enable(false);
                var dropdownlist5 = $("#ddlobservationRating").data("kendoDropDownList");
                dropdownlist5.enable(false);
                var dropdownlist6 = $("#ddlObservationCategory").data("kendoDropDownList");
                dropdownlist6.enable(false);
                var dropdownlist7 = $("#ddlObservationSubCategory").data("kendoDropDownList");
                dropdownlist7.enable(false);
            }
                            
            //var datepicker = $("#txtTimeLine").data("kendoDatePicker");
            //var datepicker1 = $("#txtResponseDueDate").data("kendoDatePicker");

            //datepicker.enable(result);
            //datepicker1.enable(result);

            //$("#form1").find(".k-textbox").each(function(index)
            //{
            //    $(this).prop( "disabled", result );
            //});
            //$("#form1").find(".k-dropdown").each(function(index)
            //{
            //    $(this).prop( "disabled", result );
            //});
        }


        function BindGroupLevelObj() {
            $("#ddlGroupLevelObservation").kendoDropDownList({
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Grop Level Observation",
                change: function () {
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '../../AuditStatus/GetGroupLevelObj?CustomerId=<%=CustomerId%>',
                        }
                    }
                }
            });
        }

        function BindPersonResponsible() {
            $("#ddlPersonresponsible").kendoDropDownList({
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select PersonResponsible",
                change: function () {
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '../../AuditStatus/GetPersonResponsible?CustomerId=<% =CustomerId%>',
                        }
                    }
                }
            });
        }

        function BindOwner() {
            $("#ddlOwnerName").kendoDropDownList({
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Owner",
                change: function () {
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '../../AuditStatus/GetPersonResponsible?CustomerId=<% =CustomerId%>',
                        }
                    }
                }
            });
        }

        function BindCategory() {
                                
            $("#ddlObservationCategory").kendoDropDownList({
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Category",
                change: function () {
                                        
                    f1($("#ddlObservationCategory").val());
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '../../AuditStatus/GetObservationCategory?CustomerId=<% =CustomerId%>',
                        }
                    }
                }
            });
        }


        function f1(CategoryID){
            
            $("#ddlObservationSubCategory").kendoDropDownList({
                autoWidth: true,
                dataTextField: "Name",
                dataValueField: "ID",
                optionLabel: "Select Sub Category",
                change: function () {
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '../../AuditStatus/GetSubObservationCategory?CustomerId=<% =CustomerId%>&CategoryID=' + CategoryID,
                        }
                    }
                }
            });
        }

        function BindOjbReport() {
            $("#ObjReport1").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "value",
                dataSource: [
                  { Name: "None of the above", value: "0" },
                  { Name: "Audit Committee", value: "1" }

                ]
            });

            $("#ddlobservationRating").kendoDropDownList({
                dataTextField: "Name",
                dataValueField: "value",
                optionLabel: "Select Rating",
                dataSource: [
                  { Name: "Major", value: "1" },
                  { Name: "Moderate", value: "2" },
                  { Name: "Minor", value: "3" }
                ]
            });
        }

        //Working Documents view and download
        function BindWorkingDocuments() {
            var grid = $('#WorkinFile1grid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#WorkinFile1grid').empty();

            var grid = $("#WorkinFile1grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: '../../AuditStatus/BindWorkingFileGrid?AuditID=<% =AuditID%>&ATBDID=<%=ATBDID%>'
                                        },
                                        pageSize: 10
                                    },
                                    excel: {
                                        allPages: true,
                                    },
                                    noRecords: true,
                                    messages: {
                                        noRecords: "No records found"
                                    },
                                    sortable: true,
                                    filterable: true,
                                    columnMenu: true,
                                    pageable: true,
                                    reorderable: true,
                                    resizable: true,
                                    multi: true,
                                    selectable: true,
                                    messages: {
                                        noRecords: "No records found"
                                    },
                                    dataBinding: function () {
                                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                                    },
                                    dataBound: function () {
                                        for (var i = 0; i < this.columns.length; i++) {
                                            this.autoWidth;
                                        }
                                    },
                                    columns: [{
                                        title: "Sr. No.",
                                        template: "#= ++record #",
                                        width: "70px",
                                    },
                                    {
                                        field: "FileName", title: 'Working Document',
                                        width: "55%",
                                        type: "string",
                                        attributes: {
                                            style: 'white-space: nowrap;'
                                        },
                                        filterable: {
                                            multi: true,
                                            extra: false,
                                            search: true,
                                            operators: {
                                                string: {
                                                    eq: "Is equal to",
                                                    neq: "Is not equal to",
                                                    contains: "Contains"
                                                }
                                            }
                                        }
                                    },
                                    {
                                        command: [
                                            //{ name: "view", text: "", iconClass: "k-icon k-i-eye", className: "ob-view" },
                                            { name: "download", text: "", iconClass: "k-icon k-i-download", className: "ob-download" }
                                            <%if (StatusID == 5)
        { %> 
                        ,{ name: "delete", text: "", iconClass: "k-icon k-i-delete k-i-trash", className: "ob-delete" }
                    <%}%>
                    ], title: "Action", lock: true, width: "15%;", headerAttributes: {
                        style: "text-align: center;"
                    }
                }
                ]
                                });

                                //$(document).on("click", "#WorkinFile1grid tbody tr .ob-view", function (e) {
                                //    $('#divViewDocument').modal('show');
                                //    $('.modal-dialog').css('width', '98%');
                                //    var item = $("#WorkinFile1grid").data("kendoGrid").dataItem($(this).closest("tr"));
                                //    $('#OverViews').attr('src', '../../AuditStatus/Common/docviewer.aspx?FileID=' + item.FileID);
                                //    e.preventDefault();
                                //});

            $(document).on("click", "#WorkinFile1grid tbody tr .ob-download", function (e) {
                var item = $("#WorkinFile1grid").data("kendoGrid").dataItem($(this).closest("tr"));
                $('#downloadfile').attr('src', '../../RiskManagement/AuditTool/DownloadFiles.aspx?FileID=' + item.FileID + '&FileType=WF1');
                e.preventDefault();
                return false;
            });

            $("#WorkinFile1grid").kendoTooltip({
                filter: ".k-grid-view",
                content: function (e) {
                    return "View Documents";
                }
            });

            $("#WorkinFile1grid").kendoTooltip({
                filter: ".k-grid-download",
                content: function (e) {
                    return "Download Documents";
                }
            });
        }

        //Delete File Working file
        $(document).on("click", "#WorkinFile1grid tbody tr .ob-delete", function (e) {
            var retVal = confirm("Are you certain you want to delete this file?");
            if (retVal == true) {
                var item = $("#WorkinFile1grid").data("kendoGrid").dataItem($(this).closest("tr"));
                var $tr = $(this).closest("tr");
                $.ajax({
                    type: 'POST',
                    url: "../../AuditStatus/DeleteWorkingFile?FileID=" + item.FileID,
                    dataType: "json",
                    success: function (result) {
                        // notify the data source that the request succeeded
                        grid = $("#WorkinFile1grid").data("kendoGrid");
                        grid.removeRow($tr);
                    },
                    error: function (result) {
                        // notify the data source that the request failed
                        console.log(result);
                    }
                });
            }
            return true;
        });

        function btnSubmit_click(e) {
            e.preventDefault();
            var AuditObjective = $("#txtAuditObjective").val();
            var AuditSteps = $("#txtAuditSteps").val();
            var AnalysisToBePerofrmed = $("#txtAnalysisToBePerformed").val();
            var ProcessWalkthrough = $("#txtWalkthrough").val();
            var ActivityToBeDone = $("#txtActualWorkDone").val();
            var Population = $("#txtpopulation").val();
            var Sample = $("#txtSample").val();
            var FixRemark = $("#tbxRemarks").val();
            var ObservationTitle = $("#txtObservationTitle").val();
            var Observation = $("#txtObservation").val();
            var BriefObservation = $("#tbxBriefObservation").val();
            var ObjBackground = $("#tbxObjBackground").val();
            //var GroupObservationCategoryID = $("#ddlGroupLevelObservation").val();
            var AnnexueTitle = $("#txtAnnexueTitle").val();
            var BodyContent = $("#editor").val();
            var AudioVideoLink = $("#txtMultilineVideolink").val();
            var ISACPORMIS = $("#ObjReport1").val();
            var Risk = $("#txtRisk").val();//Business Implication
            var RootCost = $("#txtRootcost").val();
            var FinancialImpact = $("#txtfinancialImpact").val();
            var Recomendation = $("#txtRecommendation").val();
            //var ResponseDueDate = $("#txtResponseDueDate").val();
            var ManagementResponse = $("#txtMgtResponse").val();
            //var TimeLine = $("#txtTimeLine").val();
            if ($("#txtResponseDueDate").val() != '') {
                var ResponseDueDate =kendo.toString($("#txtResponseDueDate").data("kendoDatePicker").value(), 'd');
                if (ResponseDueDate == null) {
                    ResponseDueDate=$("#txtResponseDueDate").val();
                }
            }
            if ($("#txtTimeLine").val() != '') {
                var TimeLine =kendo.toString($("#txtTimeLine").data("kendoDatePicker").value(), 'd');
                if (TimeLine == null) {
                    TimeLine=$("#txtTimeLine").val();
                }
            }
            var PersonResponsible = $("#ddlPersonresponsible").val();
            var Owner = $("#ddlOwnerName").val();
            var ObservatioRating = $("#ddlobservationRating").val();
            var ObservationCategory = $("#ddlObservationCategory").val();
            var ObservationSubCategory = $("#ddlObservationSubCategory").val();
            if (true) {
                var datastringRecord = { AuditObjective:AuditObjective , AuditSteps:AuditSteps, AnalysisToBePerofrmed:AnalysisToBePerofrmed ,ProcessWalkthrough:ProcessWalkthrough,
                    ActivityToBeDone:ActivityToBeDone, Population:Population ,Sample:Sample,FixRemark:FixRemark ,ObservationTitle:ObservationTitle,Observation:Observation,BriefObservation:BriefObservation ,ObjBackground:ObjBackground,AnnexueTitle:AnnexueTitle,
                    BodyContent:BodyContent ,AudioVideoLink:AudioVideoLink,ISACPORMIS:ISACPORMIS,Risk:Risk,RootCost:RootCost,FinancialImpact:FinancialImpact,Recomendation:Recomendation,ResponseDueDate:ResponseDueDate,ManagementResponse:ManagementResponse,TimeLine:TimeLine,PersonResponsible:PersonResponsible,Owner:Owner,ObservatioRating:ObservatioRating,ObservationCategory:ObservationCategory,
                    ObservationSubCategory:ObservationSubCategory,VerticalID:<%=VerticalID%>,AuditID:<%=AuditID%>,ATBDID:<%=ATBDID%>,ProcessID:<%=ProcessID%>,SubProcessId:<%=SubProcessId%>,Period:'<%=Period%>',FinYear:'<%=FinYear%>',BranchID:<%=BranchID%>,StatusID:<%=StatusID%>,ScheduleOnID:<%=ScheduleOnID%>,InstanceID:<%=InstanceID%>}
                //,GroupObservationCategoryID:GroupObservationCategoryID 
          $.ajax({
              type: "POST",
              url: "../../AuditStatus/AuditHeadObservationTabOne",
              data: JSON.stringify(datastringRecord),
              dataType: 'json',
              contentType: "application/json",
              cache: false,
              success: function (result) {
                  if (result != null) {
                      if (result== "success") {
                          var tabToActivate = $("#SecondTab");
                          $("#tabstrip").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);                                         
                      }
                      else {
                          resultNew = "<p1><strong style='margin-left:22%'>"+result+" </strong></p1>";
                          $('#confirmbox').kendoDialog({
                              width: "430px",
                              closable: false,
                              modal: true,
                              content: resultNew,
                              actions: [
                                  { text: 'OK', primary: true, action: RedirectEditModeOnAdd }
                              ]
                          }).data("kendoDialog").open(); 
                      }
                  }
                  else {
                      var resultNew = "";
                      resultNew = "<p1><strong style='margin-left:22%'>Something went wrong. </strong></p1>";
                      $('#confirmbox').kendoDialog({
                          width: "430px",
                          closable: false,
                          modal: true,
                          content: resultNew,
                          actions: [
                              { text: 'OK', primary: true, action: RedirectEditModeOnAdd }
                          ]
                      }).data("kendoDialog").open();
                  }
                  //setTimeout(function () { $('#divDetails').hide(); }, 3000);
              },
              error: function (result) {
                  console.log(result);
              }
          });
      }
  }

  //button 2
  function btnSubmit2_click(e) {
      e.preventDefault();
      var AuditObjective = $("#txtAuditObjective").val();
      var AuditSteps = $("#txtAuditSteps").val();
      var AnalysisToBePerofrmed = $("#txtAnalysisToBePerformed").val();
      var ProcessWalkthrough = $("#txtWalkthrough").val();
      var ActivityToBeDone = $("#txtActualWorkDone").val();
      var Population = $("#txtpopulation").val();
      var Sample = $("#txtSample").val();
      var FixRemark = $("#tbxRemarks").val();
      var ObservationTitle = $("#txtObservationTitle").val();
      var Observation = $("#txtObservation").val();
      var BriefObservation = $("#tbxBriefObservation").val();
      var ObjBackground = $("#tbxObjBackground").val();
      //var GroupObservationCategoryID = $("#ddlGroupLevelObservation").val();
      var AnnexueTitle = $("#txtAnnexueTitle").val();
      var BodyContent = $("#editor").val();
      var AudioVideoLink = $("#txtMultilineVideolink").val();
      var ISACPORMIS = $("#ObjReport1").val();
      var Risk = $("#txtRisk").val();//Business Implication
      var RootCost = $("#txtRootcost").val();
      var FinancialImpact = $("#txtfinancialImpact").val();
      var Recomendation = $("#txtRecommendation").val();
      //var ResponseDueDate = $("#txtResponseDueDate").val();
      var ManagementResponse = $("#txtMgtResponse").val();
      //var TimeLine = $("#txtTimeLine").val();
      if ($("#txtResponseDueDate").val() != '') {    
          var ResponseDueDate =kendo.toString($("#txtResponseDueDate").data("kendoDatePicker").value(), 'd');
          if (ResponseDueDate == null) {
              ResponseDueDate=$("#txtResponseDueDate").val();
          }
      }
      if ($("#txtTimeLine").val() != '') {
          var TimeLine =kendo.toString($("#txtTimeLine").data("kendoDatePicker").value(), 'd');
          if (TimeLine == null) {
              TimeLine=$("#txtTimeLine").val();
          }
      }
      var PersonResponsible = $("#ddlPersonresponsible").val();
      var Owner = $("#ddlOwnerName").val();
      var ObservatioRating = $("#ddlobservationRating").val();
      var ObservationCategory = $("#ddlObservationCategory").val();
      var ObservationSubCategory = $("#ddlObservationSubCategory").val();
      if (true) {
          var datastringRecord = { AuditObjective:AuditObjective , AuditSteps:AuditSteps, AnalysisToBePerofrmed:AnalysisToBePerofrmed ,ProcessWalkthrough:ProcessWalkthrough,
              ActivityToBeDone:ActivityToBeDone, Population:Population ,Sample:Sample,FixRemark:FixRemark ,ObservationTitle:ObservationTitle,Observation:Observation,BriefObservation:BriefObservation ,ObjBackground:ObjBackground ,AnnexueTitle:AnnexueTitle,
              BodyContent:BodyContent ,AudioVideoLink:AudioVideoLink,ISACPORMIS:ISACPORMIS,Risk:Risk,RootCost:RootCost,FinancialImpact:FinancialImpact,Recomendation:Recomendation,ResponseDueDate:ResponseDueDate,ManagementResponse:ManagementResponse,TimeLine:TimeLine,PersonResponsible:PersonResponsible,Owner:Owner,ObservatioRating:ObservatioRating,ObservationCategory:ObservationCategory,
              ObservationSubCategory:ObservationSubCategory,VerticalID:<%=VerticalID%>,AuditID:<%=AuditID%>,ATBDID:<%=ATBDID%>,ProcessID:<%=ProcessID%>,SubProcessId:<%=SubProcessId%>,Period:'<%=Period%>',FinYear:'<%=FinYear%>',BranchID:<%=BranchID%>,StatusID:<%=StatusID%>,ScheduleOnID:<%=ScheduleOnID%>,InstanceID:<%=InstanceID%>}
          //,GroupObservationCategoryID:GroupObservationCategoryID
          $.ajax({
              type: "POST",
              url: "../../AuditStatus/AuditHeadObservationTabOne",
              data: JSON.stringify(datastringRecord),
              dataType: 'json',
              contentType: "application/json",
              cache: false,
              success: function (result) {
                  if (result != null) {
                      if (result== "success") {
                          var tabToActivate = $("#ThirdTab");
                          $("#tabstrip").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);                                          
                      }
                      else {
                          resultNew = "<p1><strong style='margin-left:22%'>"+result+" </strong></p1>";
                          $('#confirmbox').kendoDialog({
                              width: "430px",
                              closable: false,
                              modal: true,
                              content: resultNew,
                              actions: [
                                  { text: 'OK', primary: true, action: RedirectEditModeOnAdd }
                              ]
                          }).data("kendoDialog").open(); 
                      }                                  
                  }
                  else {
                      var resultNew = "";
                      resultNew = "<p1><strong style='margin-left:22%'>Something went wrong. </strong></p1>";
                      $('#confirmbox').kendoDialog({
                          width: "430px",
                          closable: false,
                          modal: true,
                          content: resultNew,
                          actions: [
                              { text: 'OK', primary: true, action: RedirectEditModeOnAdd }
                          ]
                      }).data("kendoDialog").open();                      
                  }
                  //setTimeout(function () { $('#divDetails').hide(); }, 3000);
              },
              error: function (result) {
                  console.log(result);
              }
          });
      }
  }

  //button 3
  function btnSubmit3_click(e) {
      e.preventDefault();
      var AuditObjective = $("#txtAuditObjective").val();
      var AuditSteps = $("#txtAuditSteps").val();
      var AnalysisToBePerofrmed = $("#txtAnalysisToBePerformed").val();
      var ProcessWalkthrough = $("#txtWalkthrough").val();
      var ActivityToBeDone = $("#txtActualWorkDone").val();
      var Population = $("#txtpopulation").val();
      var Sample = $("#txtSample").val();
      var FixRemark = $("#tbxRemarks").val();
      var ObservationTitle = $("#txtObservationTitle").val();
      var Observation = $("#txtObservation").val();
      var BriefObservation = $("#tbxBriefObservation").val();
      var ObjBackground = $("#tbxObjBackground").val();
      //var GroupObservationCategoryID = $("#ddlGroupLevelObservation").val();
      var AnnexueTitle = $("#txtAnnexueTitle").val();
      var BodyContent = $("#editor").val();
      var AudioVideoLink = $("#txtMultilineVideolink").val();
      var ISACPORMIS = $("#ObjReport1").val();
      var Risk = $("#txtRisk").val();//Business Implication
      var RootCost = $("#txtRootcost").val();
      var FinancialImpact = $("#txtfinancialImpact").val();
      var Recomendation = $("#txtRecommendation").val();
      //var ResponseDueDate = $("#txtResponseDueDate").val();
      var ManagementResponse = $("#txtMgtResponse").val();
      //var TimeLine = $("#txtTimeLine").val();
      if ($("#txtResponseDueDate").val() != '') {
          var ResponseDueDate =kendo.toString($("#txtResponseDueDate").data("kendoDatePicker").value(), 'd');
          if (ResponseDueDate == null) {
              ResponseDueDate=$("#txtResponseDueDate").val();
          }
      }
      if ($("#txtTimeLine").val() != '') {
          var TimeLine =kendo.toString($("#txtTimeLine").data("kendoDatePicker").value(), 'd');
          if (TimeLine == null) {
              TimeLine=$("#txtTimeLine").val();
          }
      }
      var PersonResponsible = $("#ddlPersonresponsible").val();
      var Owner = $("#ddlOwnerName").val();
      var ObservatioRating = $("#ddlobservationRating").val();
      var ObservationCategory = $("#ddlObservationCategory").val();
      var ObservationSubCategory = $("#ddlObservationSubCategory").val();
      if (true) {
          var datastringRecord = { AuditObjective:AuditObjective , AuditSteps:AuditSteps, AnalysisToBePerofrmed:AnalysisToBePerofrmed ,ProcessWalkthrough:ProcessWalkthrough,
              ActivityToBeDone:ActivityToBeDone, Population:Population ,Sample:Sample,FixRemark:FixRemark ,ObservationTitle:ObservationTitle,Observation:Observation,BriefObservation:BriefObservation ,ObjBackground:ObjBackground ,AnnexueTitle:AnnexueTitle,
              BodyContent:BodyContent ,AudioVideoLink:AudioVideoLink,ISACPORMIS:ISACPORMIS,Risk:Risk,RootCost:RootCost,FinancialImpact:FinancialImpact,Recomendation:Recomendation,ResponseDueDate:ResponseDueDate,ManagementResponse:ManagementResponse,TimeLine:TimeLine,PersonResponsible:PersonResponsible,Owner:Owner,ObservatioRating:ObservatioRating,ObservationCategory:ObservationCategory,
              ObservationSubCategory:ObservationSubCategory,VerticalID:<%=VerticalID%>,AuditID:<%=AuditID%>,ATBDID:<%=ATBDID%>,ProcessID:<%=ProcessID%>,SubProcessId:<%=SubProcessId%>,Period:'<%=Period%>',FinYear:'<%=FinYear%>',BranchID:<%=BranchID%>,StatusID:<%=StatusID%>,ScheduleOnID:<%=ScheduleOnID%>,InstanceID:<%=InstanceID%>}
                      //,GroupObservationCategoryID:GroupObservationCategoryID
                      $.ajax({
                          type: "POST",
                          url: "../../AuditStatus/AuditHeadObservationTabOne",
                          data: JSON.stringify(datastringRecord),
                          dataType: 'json',
                          contentType: "application/json",
                          cache: false,
                          success: function (result) {
                              if (result != null) {
                                  if (result== "success") {
                                      var tabToActivate = $("#FourthTab");
                                      $("#tabstrip").kendoTabStrip().data("kendoTabStrip").activateTab(tabToActivate);                                  
                                  }
                                  else {
                                      resultNew = "<p1><strong style='margin-left:22%'>"+result+" </strong></p1>";
                                      $('#confirmbox').kendoDialog({
                                          width: "430px",
                                          closable: false,
                                          modal: true,
                                          content: resultNew,
                                          actions: [
                                              { text: 'OK', primary: true, action: RedirectEditModeOnAdd }
                                          ]
                                      }).data("kendoDialog").open(); 
                                  }
                              }
                              else {
                                  var resultNew = "";
                                  resultNew = "<p1><strong style='margin-left:22%'>Something went wrong. </strong></p1>";
                                  $('#confirmbox').kendoDialog({
                                      width: "430px",
                                      closable: false,
                                      modal: true,
                                      content: resultNew,
                                      actions: [
                                          { text: 'OK', primary: true, action: RedirectEditModeOnAdd }
                                      ]
                                  }).data("kendoDialog").open();                      
                              }
                              //setTimeout(function () { $('#divDetails').hide(); }, 3000);
                          },
                          error: function (result) {
                              console.log(result);
                          }
                      });
                  }
              }

              function btnSave_click(e) {
                  e.preventDefault();
                  var AuditObjective = $("#txtAuditObjective").val();
                  var AuditSteps = $("#txtAuditSteps").val();
                  var AnalysisToBePerofrmed = $("#txtAnalysisToBePerformed").val();
                  var ProcessWalkthrough = $("#txtWalkthrough").val();
                  var ActivityToBeDone = $("#txtActualWorkDone").val();
                  var Population = $("#txtpopulation").val();
                  var Sample = $("#txtSample").val();
                  var FixRemark = $("#tbxRemarks").val();
                  var ObservationTitle = $("#txtObservationTitle").val();
                  var Observation = $("#txtObservation").val();
                  var BriefObservation = $("#tbxBriefObservation").val();
                  var ObjBackground = $("#tbxObjBackground").val();
                  //var GroupObservationCategoryID = $("#ddlGroupLevelObservation").val();
                  var AnnexueTitle = $("#txtAnnexueTitle").val();
                  var BodyContent = $("#editor").val();
                  var AudioVideoLink = $("#txtMultilineVideolink").val();
                  var ISACPORMIS = $("#ObjReport1").val();
                  var Risk = $("#txtRisk").val();//Business Implication
                  var RootCost = $("#txtRootcost").val();
                  var FinancialImpact = $("#txtfinancialImpact").val();
                  var Recomendation = $("#txtRecommendation").val();
                  //var ResponseDueDate = $("#txtResponseDueDate").val();
                  var ManagementResponse = $("#txtMgtResponse").val();
                  //var TimeLine = $("#txtTimeLine").val();
                  if ($("#txtResponseDueDate").val() != '') {
                      var ResponseDueDate =kendo.toString($("#txtResponseDueDate").data("kendoDatePicker").value(), 'd');
                      if (ResponseDueDate == null) {
                          ResponseDueDate=$("#txtResponseDueDate").val();
                      }
                  }
                  if ($("#txtTimeLine").val() != '') {
                      var TimeLine =kendo.toString($("#txtTimeLine").data("kendoDatePicker").value(), 'd');
                      if (TimeLine == null) {
                          TimeLine=$("#txtTimeLine").val();
                      }
                  }
                  var PersonResponsible = $("#ddlPersonresponsible").val();
                  var Owner = $("#ddlOwnerName").val();
                  var ObservatioRating = $("#ddlobservationRating").val();
                  var ObservationCategory = $("#ddlObservationCategory").val();
                  var ObservationSubCategory = $("#ddlObservationSubCategory").val();
                  var StatusID=$("input[name=rdbtnStatus]:checked").val();  
                  var txtRemark= $("#txtRemark").val();
                  if ( StatusID=="undefined") {
                      StatusID=<%=CurrentStatusID%> ;
              }
              //var hidt=$("#hidTimeLine").val();
              //var hidTimeLine=kendo.toString(hidt.data("kendoDatePicker").value(), 'd');
              if (true) {
                  var datastringRecord = { AuditObjective:AuditObjective , AuditSteps:AuditSteps, AnalysisToBePerofrmed:AnalysisToBePerofrmed ,ProcessWalkthrough:ProcessWalkthrough,
                      ActivityToBeDone:ActivityToBeDone, Population:Population ,Sample:Sample,FixRemark:FixRemark ,ObservationTitle:ObservationTitle,Observation:Observation,BriefObservation:BriefObservation ,ObjBackground:ObjBackground ,AnnexueTitle:AnnexueTitle,
                      BodyContent:BodyContent ,AudioVideoLink:AudioVideoLink,ISACPORMIS:ISACPORMIS,Risk:Risk,RootCost:RootCost,FinancialImpact:FinancialImpact,Recomendation:Recomendation,ResponseDueDate:ResponseDueDate,ManagementResponse:ManagementResponse,TimeLine:TimeLine,PersonResponsible:PersonResponsible,Owner:Owner,ObservatioRating:ObservatioRating,ObservationCategory:ObservationCategory,
                      ObservationSubCategory:ObservationSubCategory,VerticalID:<%=VerticalID%>,AuditID:<%=AuditID%>,ATBDID:<%=ATBDID%>,ProcessID:<%=ProcessID%>,SubProcessId:<%=SubProcessId%>,Period:'<%=Period%>',FinYear:'<%=FinYear%>',BranchID:<%=BranchID%>,StatusID:<%=StatusID%>,ScheduleOnID:<%=ScheduleOnID%>,InstanceID:<%=InstanceID%>,hidObservation: $("#hidObservation").val(),hidAuditObjective:$("#hidAuditObjective").val(),hidAuditSteps:$("#hidAuditSteps").val(),hidAnalysisToBePerformed:$("#hidAnalysisToBePerformed").val(),
              hidWalkthrough:$("#hidWalkthrough").val(),hidActualWorkDone:$("#hidActualWorkDone").val(),hidpopulation:$("#hidpopulation").val(),hidSample:$("#hidSample").val(),hidObservationNumber:$("#hidObservationNumber").val(),hidObservationTitle:$("#hidObservationTitle").val(),hidRisk:$("#hidRisk").val(),hidRootcost:$("#hidRootcost").val(),hidfinancialImpact:$("#hidfinancialImpact").val(),hidRecommendation:$("#hidRecommendation").val(),
              hidMgtResponse:$("#hidMgtResponse").val(),hidRemarks:$("#hidRemarks").val(),hidAuditStepScore:$("#hidAuditStepScore").val(),hidTimeLine:$("#hidTimeLine").val(),hidISACPORMIS:$("#hidISACPORMIS").val(),hidPersonResponsible:$("#hidPersonResponsible").val(),hidObservationRating:$("#hidObservationRating").val(),hidObservationCategory:$("#hidObservationCategory").val(),hidObservationSubCategory:$("#hidObservationSubCategory").val(),hidOwner:$("#hidOwner").val(),hidUserID:$("#hidUserID").val(),hidGroupObservationCategory:$("#hidGroupObservationCategory").val(),
              hidUHPersonResponsible:$("#hidUHPersonResponsible").val(),hidPRESIDENTPersonResponsible:$("#hidPRESIDENTPersonResponsible").val(),hidUHComment:$("#hidUHComment").val(),hidPRESIDENTComment:$("#hidPRESIDENTComment").val(),hidBodyContent:$("#hidBodyContent").val(),hidResponseDueDate:$("#hidResponseDueDate").val(),hidAnnaxureTitle:$("#hidAnnaxureTitle").val()
                    ,CurrentStatusID:StatusID,txtRemark:txtRemark
          }
          //,GroupObservationCategoryID:GroupObservationCategoryID
          $.ajax({
              type: "POST",
              url: "../../AuditStatus/AuditHeadSave",
              data: JSON.stringify(datastringRecord),
              dataType: 'json',
              contentType: "application/json",
              cache: false,
              success: function (result) {
                  if (result != null)
                  {
                      if (result== "success") {
                          resultNew = "<p1><strong style='margin-left:22%'>Audit Step Submitted Successfully.</strong></p1>";
                          $('#confirmbox').kendoDialog({
                              width: "430px",
                              closable: false,
                              modal: true,
                              content: resultNew,
                              actions: [
                                  { text: 'OK', primary: true, action: RedirectEditModeOnAdd }
                              ]
                          }).data("kendoDialog").open();                                
                      }
                      else {
                          resultNew = "<p1><strong style='margin-left:22%'>"+result+" </strong></p1>";
                          $('#confirmbox').kendoDialog({
                              width: "430px",
                              closable: false,
                              modal: true,
                              content: resultNew,
                              actions: [
                                  { text: 'OK', primary: true, action: RedirectEditModeOnAdd }
                              ]
                          }).data("kendoDialog").open(); 
                      }
                      BindAuditLogGrid();
                      BindReviewHistoryGrid();
                  }
                  else {
                      var resultNew = "";
                      resultNew = "<p1><strong style='margin-left:22%'>Something went wrong. </strong></p1>";
                      $('#confirmbox').kendoDialog({
                          width: "430px",
                          closable: false,
                          modal: true,
                          content: result,
                          actions: [
                              { text: 'OK', primary: true, action: RedirectEditModeOnAdd }
                          ]
                      }).data("kendoDialog").open();
                      BindAuditLogGrid();
                      BindReviewHistoryGrid();
                      setTimeout(function () { $('#divDetails').hide(); }, 3000);
                  }
              },
              error: function (result) {
                  console.log(result);
              }
          });
      }
  }

  //Review History File Upload
  function UploadReviewHistoryFile() {
      $("#RVUpload").kendoUpload({
          async: {
              saveUrl: "../../AuditStatus/UploadReviewHistoryDocument?AuditID=<% =AuditID%>&ATBDID=<%=ATBDID%>&CustomerId=<%=CustomerId%>&UserID=<%=UserID%>&InstanceID=<%=InstanceID%>&TestRemark="+$("#txtRemark").val(),
                    autoUpload: false
                },
                multiple: false,
                upload: function (e) {
                    e.sender.options.async.saveUrl = "../../AuditStatus/UploadReviewHistoryDocument?AuditID=<%=AuditID%>&ATBDID=<%=ATBDID%>&CustomerId=<%=CustomerId%>&UserID=<%=UserID%>&InstanceID=<%=InstanceID%>&TestRemark=" + $("#txtRemark").val();
                      },
                      success: function (e) {
                          if (e.response != null) {
                              $(".k-upload-files.k-reset").find("li").remove();
                              BindAuditLogGrid();
                              BindReviewHistoryGrid();
                              //document.getElementById('divDocumentAdd').scrollIntoView();
                          }
                          else {
                              //$("#lblDocDetails").text('Error at Uploading Documents');
                              BindAuditLogGrid();
                              BindReviewHistoryGrid();
                              //$("#divDocDetails").show();
                          }
                          // setTimeout(function () { $("#divDocDetails").hide(); }, 3000);
                          //***Give here error message 
                      }
            });
                  }
      

                  //Annexure Documents view and download
                  function BindAnnexureDocuments() {
                      var grid = $('#AnnexureFilegrid').data("kendoGrid");
                      if (grid != undefined || grid != null)
                          $('#AnnexureFilegrid').empty();

                      var grid = $("#AnnexureFilegrid").kendoGrid({
                          dataSource: {
                              transport: {
                                  read: '../../AuditStatus/BindAnnexureFileGrid?AuditID=<% =AuditID%>&ATBDID=<%=ATBDID%>'
                          },
                          pageSize: 10
                      },
                      excel: {
                          allPages: true,
                      },
                      noRecords: true,
                      messages: {
                          noRecords: "No records found"
                      },
                      sortable: true,
                      filterable: true,
                      columnMenu: true,
                      pageable: true,
                      reorderable: true,
                      resizable: true,
                      multi: true,
                      selectable: true,
                      messages: {
                          noRecords: "No records found"
                      },
                      dataBinding: function () {
                          record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                      },
                      dataBound: function () {
                          for (var i = 0; i < this.columns.length; i++) {
                              this.autoWidth;
                          }
                      },
                      columns: [{
                          title: "Sr. No.",
                          template: "#= ++record #",
                          width: "70px",
                      },
                      {
                          field: "FileName", title: 'Annexure Document',
                          width: "55%",
                          type: "string",
                          attributes: {
                              style: 'white-space: nowrap;'
                          },
                          filterable: {
                              multi: true,
                              extra: false,
                              search: true,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                      },
                      {
                          command: [
                              //{ name: "view", text: "", iconClass: "k-icon k-i-eye", className: "ob-view" },
                              { name: "download", text: "", iconClass: "k-icon k-i-download", className: "ob-download" }
                              <%if (StatusID == 5)
        { %> 
                        ,{ name: "delete", text: "", iconClass: "k-icon k-i-delete k-i-trash", className: "ob-delete" }
                    <%}%>
                        ], title: "Action", lock: true, width: "15%;", headerAttributes: {
                            style: "text-align: center;"
                        }
                    }
                    ]
                  });

                  //$(document).on("click", "#AnnexureFilegrid tbody tr .ob-view", function (e) {
                  //    $('#divViewDocument').modal('show');
                  //    $('.modal-dialog').css('width', '98%');
                  //    var item = $("#AnnexureFilegrid").data("kendoGrid").dataItem($(this).closest("tr"));
                  //    $('#OverViews').attr('src', '../../AuditStatus/Common/docviewer.aspx?FileID=' + item.FileID);
                  //    e.preventDefault();
                  //});

                $(document).on("click", "#AnnexureFilegrid tbody tr .ob-download", function (e) {
                    var item = $("#AnnexureFilegrid").data("kendoGrid").dataItem($(this).closest("tr"));
                    $('#downloadfile').attr('src', '../../RiskManagement/AuditTool/DownloadFiles.aspx?FileID=' + item.Id + '&FileType=ANX');
                    e.preventDefault();
                    return false;
                });

                  //$("#AnnexureFilegrid").kendoTooltip({
                  //    filter: "td",
                  //    position: "down",
                  //    content: function (e) {
                  //        var content = e.target.context.textContent;
                  //        if (content != "") {
                  //            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
                  //        }
                  //        else
                  //            e.preventDefault();
                  //    }
                  //}).data("kendoTooltip");

                $("#AnnexureFilegrid").kendoTooltip({
                    filter: ".k-grid-view",
                    content: function (e) {
                        return "View Documents";
                    }
                });

                $("#AnnexureFilegrid").kendoTooltip({
                    filter: ".k-grid-download",
                    content: function (e) {
                        return "Download Documents";
                    }
                });
            }

            //Delete File Annexure file
            $(document).on("click", "#AnnexureFilegrid tbody tr .ob-delete", function (e) {
                var retVal = confirm("Are you certain you want to delete this file?");
                if (retVal == true) {
                    var item = $("#AnnexureFilegrid").data("kendoGrid").dataItem($(this).closest("tr"));
                    var $tr = $(this).closest("tr");
                    $.ajax({
                        type: 'POST',
                        url: "../../AuditStatus/DeleteAnnexureFile?FileID=" + item.Id,
                        dataType: "json",
                        success: function (result) {
                            // notify the data source that the request succeeded
                            grid = $("#AnnexureFilegrid").data("kendoGrid");
                            grid.removeRow($tr);
                        },
                        error: function (result) {
                            // notify the data source that the request failed
                            console.log(result);
                        }
                    });
                }
                return true;
            });
            //Annexure File End

            //Working Documents Two view and download
            function BindWorkingDocumentsTwo() {
                var grid = $('#WorkingFileTwoFilegrid').data("kendoGrid");
                if (grid != undefined || grid != null)
                    $('#WorkingFileTwoFilegrid').empty();

                var grid = $("#WorkingFileTwoFilegrid").kendoGrid({
                    dataSource: {
                        transport: {
                            read: '../../AuditStatus/BindWorkingFileTwoFilegrid?AuditID=<% =AuditID%>&ATBDID=<%=ATBDID%>'
              },
              pageSize: 10
          },
          excel: {
              allPages: true,
          },
          noRecords: true,
          messages: {
              noRecords: "No records found"
          },
          sortable: true,
          filterable: true,
          columnMenu: true,
          pageable: true,
          reorderable: true,
          resizable: true,
          multi: true,
          selectable: true,
          messages: {
              noRecords: "No records found"
          },
          dataBinding: function () {
              record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
          },
          dataBound: function () {
              for (var i = 0; i < this.columns.length; i++) {
                  this.autoWidth;
              }
          },
          columns: [{
              title: "Sr. No.",
              template: "#= ++record #",
              width: "70px",
          },
          {
              field: "FileName", title: 'Working Document',
              width: "55%",
              type: "string",
              attributes: {
                  style: 'white-space: nowrap;'
              },
              filterable: {
                  multi: true,
                  extra: false,
                  search: true,
                  operators: {
                      string: {
                          eq: "Is equal to",
                          neq: "Is not equal to",
                          contains: "Contains"
                      }
                  }
              }
          },
          {
              command: [
                  //{ name: "view", text: "", iconClass: "k-icon k-i-eye", className: "ob-view" },
                  { name: "download", text: "", iconClass: "k-icon k-i-download", className: "ob-download" }
                  <%if (StatusID == 5)
        { %> 
                        ,{ name: "delete", text: "", iconClass: "k-icon k-i-delete k-i-trash", className: "ob-delete" }
                    <%}%>
                    
                    ], title: "Action", lock: true, width: "15%;", headerAttributes: {
                        style: "text-align: center;"
                    }
                }
                ]
      });

            $(document).on("click", "#WorkingFileTwoFilegrid tbody tr .ob-download", function (e) {
                var item = $("#WorkingFileTwoFilegrid").data("kendoGrid").dataItem($(this).closest("tr"));
                $('#downloadfile').attr('src', '../../RiskManagement/AuditTool/DownloadFiles.aspx?FileID=' + item.FileID + '&FileType=WF1');
                e.preventDefault();
                return false;
            });

      //$("#WorkingFileTwoFilegrid").kendoTooltip({
      //    filter: "td",
      //    position: "down",
      //    content: function (e) {
      //        var content = e.target.context.textContent;
      //        if (content != "") {
      //            return '<div style="width: ' + content.length * .6 + 'em; max-width: 20em;white-space: pre-wrap;">' + content + '</div>';
      //        }
      //        else
      //            e.preventDefault();
      //    }
      //}).data("kendoTooltip");

            $("#WorkingFileTwoFilegrid").kendoTooltip({
                filter: ".k-grid-view",
                content: function (e) {
                    return "View Documents";
                }
            });

            $("#WorkingFileTwoFilegrid").kendoTooltip({
                filter: ".k-grid-download",
                content: function (e) {
                    return "Download Documents";
                }
            });
        }

        //Delete File Working file Two
        $(document).on("click", "#WorkingFileTwoFilegrid tbody tr .ob-delete", function (e) {
            var retVal = confirm("Are you certain you want to delete this file?");
            if (retVal == true) {
                var item = $("#WorkingFileTwoFilegrid").data("kendoGrid").dataItem($(this).closest("tr"));
                var $tr = $(this).closest("tr");
                $.ajax({
                    type: 'POST',
                    url: "../../AuditStatus/DeleteWorkingFile?FileID=" + item.FileID,
                    dataType: "json",
                    success: function (result) {
                        // notify the data source that the request succeeded
                        grid = $("#WorkingFileTwoFilegrid").data("kendoGrid");
                        grid.removeRow($tr);
                    },
                    error: function (result) {
                        // notify the data source that the request failed
                        console.log(result);
                    }
                });
            }
            return true;
        });
        //Delete File Working file Two End


        //Image grid view and download
        function BindImageFiles() {
            var grid = $('#UploadImagegrid').data("kendoGrid");
            if (grid != undefined || grid != null)
                $('#UploadImagegrid').empty();

            var grid = $("#UploadImagegrid").kendoGrid({
                dataSource: {
                    transport: {
                        read: '../../AuditStatus/BindUploadImagegrid?AuditID=<% =AuditID%>&ATBDID=<%=ATBDID%>'
                },
                pageSize: 10
            },
            excel: {
                allPages: true,
            },
            noRecords: true,
            messages: {
                noRecords: "No records found"
            },
            sortable: true,
            filterable: true,
            columnMenu: true,
            pageable: true,
            reorderable: true,
            resizable: true,
            multi: true,
            selectable: true,
            messages: {
                noRecords: "No records found"
            },
            dataBinding: function () {
                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            dataBound: function () {
                for (var i = 0; i < this.columns.length; i++) {
                    this.autoWidth;
                }
            },
            columns: [{
                title: "Sr. No.",
                template: "#= ++record #",
                width: "70px",
            },
            {
                field: "ImageName", title: 'Image',
                width: "55%",
                type: "string",
                attributes: {
                    style: 'white-space: nowrap;'
                },
                filterable: {
                    multi: true,
                    extra: false,
                    search: true,
                    operators: {
                        string: {
                            eq: "Is equal to",
                            neq: "Is not equal to",
                            contains: "Contains"
                        }
                    }
                }
            },
            {
                command: [
                    { name: "view", text: "", iconClass: "k-icon k-i-eye", className: "ob-view" }
                    <%if (StatusID == 5)
        { %> 
                        ,{ name: "delete", text: "", iconClass: "k-icon k-i-delete k-i-trash", className: "ob-delete" }
                    <%}%>
            
            ], title: "Action", lock: true, width: "15%;", headerAttributes: {
                style: "text-align: center;"
            }
        }
        ]
        });

    $(document).on("click", "#UploadImagegrid tbody tr .ob-view", function (e) {
        $('#divViewDocument').modal('show');
        $('.modal-dialog').css('width', '98%');
        var item = $("#UploadImagegrid").data("kendoGrid").dataItem($(this).closest("tr"));
        //$('#OverViews').attr('src', '../../AuditStatus/Common/docviewer.aspx?FileID=' + item.ID);
        $('#OverViews').attr('src', '../../RiskManagement/AuditTool/Auditdocviewer.aspx?FileID=' + item.ID + '&IMG=IMG');
        e.preventDefault();
    });


    $("#UploadImagegrid").kendoTooltip({
        filter: ".k-grid-view",
        content: function (e) {
            return "View Documents";
        }
    });

    $("#UploadImagegrid").kendoTooltip({
        filter: ".k-grid-download",
        content: function (e) {
            return "Download Documents";
        }
    });
}
//Delete File Image grid
$(document).on("click", "#UploadImagegrid tbody tr .ob-delete", function (e) {
    var retVal = confirm("Are you certain you want to delete this file?");
    if (retVal == true) {
        var item = $("#UploadImagegrid").data("kendoGrid").dataItem($(this).closest("tr"));
        var $tr = $(this).closest("tr");
        $.ajax({
            type: 'POST',
            url: "../../AuditStatus/DeleteUploadedImage?FileID=" + item.ID,
            dataType: "json",
            success: function (result) {
                // notify the data source that the request succeeded
                grid = $("#UploadImagegrid").data("kendoGrid");
                grid.removeRow($tr);
            },
            error: function (result) {
                // notify the data source that the request failed
                console.log(result);
            }
        });
    }
    return true;
});
//Image grid End

//Review History Grid Start
function BindReviewHistoryGrid() {
    var grid = $('#ReviewHistoryGrid').data("kendoGrid");
    if (grid != undefined || grid != null)
        $('#ReviewHistoryGrid').empty();

    var grid = $("#ReviewHistoryGrid").kendoGrid({
        dataSource: {
            transport: {
                read: '../../AuditStatus/BindReviewHistoryGrid?AuditID=<% =AuditID%>&ATBDID=<%=ATBDID%>&ScheduleOnID=<%=ScheduleOnID%>&ProcessID=<%=ProcessID%>'
            },
            pageSize: 10
        },
        excel: {
            allPages: true,
        },
        noRecords: true,
        messages: {
            noRecords: "No records found"
        },
        sortable: true,
        filterable: true,
        columnMenu: true,
        pageable: true,
        reorderable: true,
        resizable: true,
        multi: true,
        selectable: true,
        messages: {
            noRecords: "No records found"
        },
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        dataBound: function () {
            for (var i = 0; i < this.columns.length; i++) {
                this.autoWidth;
            }
        },
        columns: [
        {
            field: "CreatedByText", title: 'Created By',
            width: "55%",
            type: "string",
            attributes: {
                style: 'white-space: nowrap;'
            },
            filterable: {
                multi: true,
                extra: false,
                search: true,
                operators: {
                    string: {
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        contains: "Contains"
                    }
                }
            }
        },
        {
            field: "Remarks", title: 'Remarks',
            width: "55%",
            type: "string",
            attributes: {
                style: 'white-space: nowrap;'
            },
            filterable: {
                multi: true,
                extra: false,
                search: true,
                operators: {
                    string: {
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        contains: "Contains"
                    }
                }
            }
        },
        {
            field: "Dated", title: 'Date',
            width: "55%",
            type: "string",
            attributes: {
                style: 'white-space: nowrap;'
            },
            filterable: {
                multi: true,
                extra: false,
                search: true,
                operators: {
                    string: {
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        contains: "Contains"
                    }
                }
            }
        },
        {
            command: [
                { name: "view", text: "", iconClass: "k-icon k-i-eye", className: "ob-view" },
                { name: "download", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
            ],
            title: "Action", lock: true, width: "15%;", headerAttributes: {
                style: "text-align: center;"
            }
        }
        ]
    });

    $(document).on("click", "#ReviewHistoryGrid tbody tr .ob-view", function (e) {
        $('#divViewDocument').modal('show');
        $('.modal-dialog').css('width', '98%');
        var item = $("#ReviewHistoryGrid").data("kendoGrid").dataItem($(this).closest("tr"));
        $('#OverViews').attr('src', '../../RiskManagement/AuditTool/Auditdocviewer.aspx?FileID='+ item.ID);
        e.preventDefault();
    });

    $(document).on("click", "#ReviewHistoryGrid tbody tr .ob-download", function (e) {
        var item = $("#ReviewHistoryGrid").data("kendoGrid").dataItem($(this).closest("tr"));
        $('#downloadfile').attr('src', '../../RiskManagement/AuditTool/DownloadFiles.aspx?FileID=' + item.ID + '&FileType=RHist');
        e.preventDefault();
        return false;
    });

    $("#ReviewHistoryGrid").kendoTooltip({
        filter: ".k-grid-view",
        content: function (e) {
            return "View Documents";
        }
    });

    $("#ReviewHistoryGrid").kendoTooltip({
        filter: ".k-grid-download",
        content: function (e) {
            return "Download Documents";
        }
    });
}
//Review History Grid End
//Audit Log Grid Start
//Image grid view and download
function BindAuditLogGrid() {
    var grid = $('#AuditLogGrid').data("kendoGrid");
    if (grid != undefined || grid != null)
        $('#AuditLogGrid').empty();
    var grid = $("#AuditLogGrid").kendoGrid({
        dataSource: {
            transport: {
                read: '../../AuditStatus/BindAuditLogGrid?AuditID=<% =AuditID%>&ATBDID=<%=ATBDID%>&ScheduleOnID=<%=ScheduleOnID%>'
            },
            pageSize: 10
        },
        excel: {
            allPages: true,
        },
        noRecords: true,
        messages: {
            noRecords: "No records found"
        },
        sortable: true,
        filterable: true,
        columnMenu: true,
        pageable: true,
        reorderable: true,
        resizable: true,
        multi: true,
        selectable: true,
        messages: {
            noRecords: "No records found"
        },
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        dataBound: function () {
            for (var i = 0; i < this.columns.length; i++) {
                this.autoWidth;
            }
        },
        columns: [
        {
            field: "CreatedByText", title: 'Created By',
            width: "25%",
            type: "string",
            attributes: {
                style: 'white-space: nowrap;'
            },
            filterable: {
                multi: true,
                extra: false,
                search: true,
                operators: {
                    string: {
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        contains: "Contains"
                    }
                }
            }
        },
        {
            field: "Remarks", title: 'Remarks',
            width: "45%",
            type: "string",
            attributes: {
                style: 'white-space: nowrap;'
            },
            filterable: {
                multi: true,
                extra: false,
                search: true,
                operators: {
                    string: {
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        contains: "Contains"
                    }
                }
            }
        },
        {
            field: "Dated", title: 'Dated',
            width: "15%",
            type: "string",
            attributes: {
                style: 'white-space: nowrap;'
            },
            filterable: {
                multi: true,
                extra: false,
                search: true,
                operators: {
                    string: {
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        contains: "Contains"
                    }
                }
            }
        },
        {
            field: "Status", title: 'Status',
            width: "15%",
            type: "string",
            attributes: {
                style: 'white-space: nowrap;'
            },
            filterable: {
                multi: true,
                extra: false,
                search: true,
                operators: {
                    string: {
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        contains: "Contains"
                    }
                }
            }
        }
        ]
    });
}
//Audit Log grid End
    </script>
</head>
<body>
    <form id="form1" runat="server"> 
        <div>
            <div id="example">
                <div class="demo-section k-content">
                    <div id="tabstrip">
                        <ul>
                            <li id="FirstTab" class="k-state-active">Audit Coverage
                            </li>
                            <li id="SecondTab">Actual Testing/ Work Done
                            </li>
                            <li id="ThirdTab">Observations
                            </li>
                            <li id="FourthTab">Review History & Log
                            </li>
                        </ul>
                        <%--First Tab--%>
                        <div>
                            <div class="col-md-12">
                                <div style="width:50%; display:inline-block;">
                                <label id="lblAuditName1" style="float:left; display:inline-block;"/>
                                    </div>
                                <div style="width:45%; display:inline-block;">
                                <label id="lblReviewPeriod1" style="float:right; display:inline-block; margin-right:-5px;"/>
                                    </div>
                            </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">&nbsp;</span>Audit Methodology</label>                            
                            <textarea id="txtAuditObjective" style="width: 80%; height:70px;" disabled="disabled"></textarea>
                                </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">&nbsp;</span>Audit Steps</label>                            
                            <textarea id="txtAuditSteps" style="width: 80%; height:70px" disabled="disabled"></textarea>
                                </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">&nbsp;</span>Analyis To Be Performed</label>                            
                            <textarea id="txtAnalysisToBePerformed" style="width: 80%; height:70px" disabled="disabled"></textarea>
                                </div>
                            <div class="col-md-12">
                                <button id="btn1" type="submit" class="k-button" style="width: 70px; margin-right: 6px;">Next</button>
                            </div>
                        </div>
                        <%--Second Tab--%>
                        <div>
                            <div class="col-md-12">
                                <div style="width:47%; display:inline-block;">
                                <label id="lblAuditName2" style="float:left; display:inline-block;"/>
                                    </div>
                                <div style="width:50%; display:inline-block;">
                                <label id="lblReviewPeriod2" style="float:right; display:inline-block; margin-right:-5px;"/>
                                    </div>
                            </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">&nbsp;</span>Process Walkthrough</label>                            
                            <textarea id="txtWalkthrough" style="width: 80%; height:70px"></textarea>
                                </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">&nbsp;</span>Actual Work Done</label>                            
                            <textarea id="txtActualWorkDone" style="width: 80%; height:70px""></textarea>
                                </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">&nbsp;</span>Population</label>                            
                            <textarea id="txtpopulation" style="width: 80%; height:70px""></textarea>
                                </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">&nbsp;</span>Sample</label>                            
                            <textarea id="txtSample" style="width: 80%; height:70px""></textarea>
                                </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">&nbsp;</span>Remarks</label>                            
                            <textarea id="tbxRemarks" style="width: 80%; height:70px""></textarea>
                                </div>
                            <div class="col-md-12" style="display:none">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">&nbsp;</span>Working File Upload</label> 
                                <div style="float: left; width: 53%;">
                                    <input name="WFUpload" id="WFUpload" type="file" />
                                </div>
                                <button id="btnWFUpload" type="submit" class="k-button" style="width: 70px; margin-right: 6px;">Upload</button>
                                </div> 
                                <div class="col-md-12">
                                    <div id="WorkinFile1grid" style="margin:4px;"></div>
                                </div>
                            <div class="col-md-12" style="display:none">   
                            <label class="MainWrapper"><span style="color: red; display:inline-block">&nbsp;</span>Annexure Files</label> 
                                <div style="float: left; width: 53%;">
                                    <input name="AFUpload" id="AFUpload" type="file" />
                                </div>
                                <button id="btnAFUpload" type="submit" class="k-button" style="width: 70px; margin-right: 6px;">Upload</button>
                                </div>  
                            <div class="col-md-12">
                                    <div id="AnnexureFilegrid" style="margin:4px;"></div>
                                </div>    
                            <div class="col-md-12">
                                <button id="btn2" type="submit" class="k-button" style="width: 70px; margin-right: 6px;">Next</button>
                            </div>                  
                        </div>
                        <%--Third Tab--%>
                        <div>
                            <div class="col-md-12">
                                <div style="width:47%; display:inline-block;">
                                <label id="lblAuditName3" style="float:left; display:inline-block;"/>
                                    </div>
                                <div style="width:50%; display:inline-block;">
                                <label id="lblReviewPeriod3" style="float:right; display:inline-block; margin-right:-5px;"/>
                                    </div>
                            </div>
                            <div class="col-md-12" style="margin-bottom:5%; display:none;">
                                 <label class="MainWrapper" style="width:50%"><span style="color: red; display: inline-block">&nbsp;</span>&nbsp;</label>
                                <input type="button" id="btnObjListing" value="Historical Observations" class="btn" style="float:right; width: 185px; margin-right: 6px;"/>
                            </div>
                            <div class="col-md-12">
                                <label class="MainWrapper"><span style="color: red; display: inline-block">*</span>Observation Title</label>
                                <input id="txtObservationTitle"  style="width: 80%; height: 25px;" />
                            </div>

                            <div class="col-md-12">
                                <label class="MainWrapper"><span style="color: red; display: inline-block">*</span>Observation</label>
                                <textarea id="txtObservation"  style="width: 80%; height: 70px;"></textarea>
                            </div>

                            <div class="col-md-12">
                                <label class="MainWrapper"><span style="color: red; display: inline-block">&nbsp;</span>Brief Observation</label>
                                <textarea id="tbxBriefObservation" style="width: 80%; height: 70px;"></textarea>
                            </div>

                            <div class="col-md-12">
                                <label class="MainWrapper"><span style="color: red; display: inline-block">&nbsp;</span>Observation Background</label>
                                <textarea id="tbxObjBackground" style="width: 80%; height: 70px;"></textarea>
                            </div>

                            <div class="col-md-12" style="display:none;">
                                <label class="MainWrapper"><span style="color: red; display: inline-block">&nbsp;</span>Group Level Observation</label>
                                <input id="ddlGroupLevelObservation" />
                            </div>
                            
                            <%--Working File Upload  and grid--%>
                            <div class="col-md-12"  style="display:none">
                                <label class="MainWrapper"><span style="color: red; display: inline-block">&nbsp;</span>Working File Upload</label>
                                <div style="float: left; width: 53%;">
                                    <input name="WFUploadObj" id="WFUploadObj" type="file" />
                                </div>
                                <button id="btnWFUploadObj" type="submit" class="k-button" style="width: 70px; margin-right: 6px;">Upload</button>
                            </div>
                            <div class="col-md-12">
                                    <div id="WorkingFileTwoFilegrid" style="margin:4px;"></div>
                                </div>   
                            
                            <div class="col-md-12">
                                <label class="MainWrapper"><span style="color: red; display: inline-block">&nbsp;</span>Annexure Title</label>
                                <input id="txtAnnexueTitle" style="width: 80%; height: 25px;" />
                            </div> 
                            <%--Table--%> 
                            <div class="col-md-12">
                                <label class="MainWrapper"><span style="color: red; display: inline-block">&nbsp;</span>Table</label>
                                <div class="demo-containers">
                                    <textarea id="editor"  style="width:740px;height:440px"></textarea>        
                                </div>
                            </div>                
                            <%--Upload image and grid--%>                             
                            <div class="col-md-12"  style="display:none">
                                <label class="MainWrapper"><span style="color: red; display: inline-block">&nbsp;</span>Upload Image</label>
                                <div style="float: left; width: 53%;">
                                    <input name="ImageUpload" id="ImageUpload" type="file" />
                                </div>
                                <button id="btnImgUpload" type="submit" class="k-button" style="width: 70px; margin-right: 6px;">Upload</button>
                            </div>
                            <div class="col-md-12">
                                    <div id="UploadImagegrid" style="margin:4px;"></div>
                                </div> 
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">&nbsp;</span>Audio/Video Link (i)</label>     
                            <textarea id="txtMultilineVideolink" style="width: 80%; height:70px""></textarea>
                                </div>
                            <div class="col-md-12">
                                <label class="MainWrapper"><span style="color: red; display: inline-block">*</span>Observation Report</label>
                                <input id="ObjReport1" />
                            </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">*</span>Business Implication</label>
                            <textarea id="txtRisk" style="width: 80%; height:70px""></textarea>
                                </div>  
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">*</span>Root Cause</label>
                            <textarea id="txtRootcost" style="width: 80%; height:70px""></textarea>
                                </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">*</span>Financial Impact</label>
                            <input id="txtfinancialImpact"  maxlength="45" style="width: 80%; height: 25px;"/>
                                </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">*</span>Recommendation</label>
                            <textarea id="txtRecommendation" style="width: 80%; height:70px""></textarea>
                                </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">&nbsp;</span>Response Due Date</label>
                            <input id="txtResponseDueDate" />
                                </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">*</span>Management Response</label>
                            <textarea id="txtMgtResponse" style="width: 80%; height:70px""></textarea>
                                </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">*</span>Time Line</label>
                            <input id="txtTimeLine" /> 
                                </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">*</span>Person Responsible</label>
                            <input id="ddlPersonresponsible" />
                                <a class="k-button" id="btnAddUser" href="#" style="height:30px" onclick="OpenUserPopup()">
                                <span class="k-icon k-i-plus-circle"></span>
                             </a>
                                </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">*</span>Owner Name</label>
                            <input id="ddlOwnerName" />
                                </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">*</span>Observation Rating</label>
                            <input id="ddlobservationRating" />
                                </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">*</span>Observation Category</label>
                            <input id="ddlObservationCategory" />
                                </div>
                            <div class="col-md-12">
                            <label class="MainWrapper"><span style="color: red; display:inline-block">&nbsp;</span>Observation SubCategory</label>
                            <input id="ddlObservationSubCategory" />
                            </div>
                            <div class="col-md-12">
                                <button id="btn3" type="submit" class="k-button" style="width: 70px; margin-right: 6px;">Next</button>
                            </div>
                        </div>
                        <%--Fourth Tab--%>
                        <div>
                            <div class="col-md-12">
                                <div style="width:47%; display:inline-block;">
                                <label id="lblAuditName4" style="float:left; display:inline-block;"/>
                                    </div>
                                <div style="width:50%; display:inline-block;">
                                <label id="lblReviewPeriod4" style="float:right; display:inline-block; margin-right:-5px;"/>
                                    </div>
                            </div>
                            <fieldset style="margin-top: 5px; border: 1px solid #dddddd">
                                <legend>Review History</legend>
                                <%--IFC link and dropdown--%>
                                <div class="col-md-12">
                                    <div id="ReviewHistoryGrid" style="margin:4px;"></div>
                                    </div>
                                <div class="col-md-12">
                                    <label class="MainWrapper"><span style="color: red; display: inline-block">&nbsp;</span>Review Remark</label>
                                    <textarea id="txtRemark" style="width: 80%; height: 70px;"></textarea>                                    
                                </div>
                                <div class="col-md-12" style="max-height:100px !important; overflow-y:scroll;">
                                <label class="MainWrapper"><span style="color: red; display: inline-block">&nbsp;</span>Upload File</label>
                                <div style="float: left; width: 53%;">
                                    <input name="RVUpload" id="RVUpload" type="file" />
                                </div>
                                <button id="btnRHUpload" type="submit" class="k-button" style="width: 70px; margin-right: 6px; margin-left: 20px;">Upload</button>
                            </div>
                                <div class="col-md-12">
                                    <label class="MainWrapper"><span style="color: red; display: inline-block">&nbsp;</span>Status</label>
                                    <%--<div id="rdbtnStatus"></div>--%>
                                    <div style="width:70% !important">
                                     <asp:RadioButtonList ID="rdbtnStatus" style="display: inline-block" RepeatLayout="Flow"  CssClass="form-inline bootRBL" runat="server" RepeatDirection="Horizontal" AutoPostBack="false">
                                      </asp:RadioButtonList>
                                        </div>
                                </div>
                                <div class="col-md-12">
                                <button id="btnSave" type="submit" class="k-button" style="width: 70px; margin-right: 6px;">Submit</button>
                            </div>
                                <%--FileUpload--%>
                            </fieldset>
                            <fieldset style="margin-top: 5px; border: 1px solid #dddddd">
                                <legend>Audit Log</legend>
                                <div class="col-md-12">
                                    <div id="AuditLogGrid" style="margin:4px;"></div>
                                    </div>
                                </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="confirmbox"></div>

        <div style="display:none">
        <asp:HiddenField runat="server" ID="hidObservation" />
        <asp:HiddenField runat="server" ID="hidAuditObjective" />
        <asp:HiddenField runat="server" ID="hidAuditSteps" />
        <asp:HiddenField runat="server" ID="hidAnalysisToBePerformed" />
        <asp:HiddenField runat="server" ID="hidWalkthrough" />
        <asp:HiddenField runat="server" ID="hidActualWorkDone" />
        <asp:HiddenField runat="server" ID="hidpopulation" />
        <asp:HiddenField runat="server" ID="hidSample" />
        <asp:HiddenField runat="server" ID="hidObservationNumber" />
        <asp:HiddenField runat="server" ID="hidObservationTitle" />
        <asp:HiddenField runat="server" ID="hidRisk" />
        <asp:HiddenField runat="server" ID="hidRootcost" />
        <asp:HiddenField runat="server" ID="hidfinancialImpact" />
        <asp:HiddenField runat="server" ID="hidRecommendation" />
        <asp:HiddenField runat="server" ID="hidMgtResponse" />
        <asp:HiddenField runat="server" ID="hidRemarks" />
        <asp:HiddenField runat="server" ID="hidAuditStepScore" />
        <asp:HiddenField runat="server" ID="hidTimeLine" />
        <asp:HiddenField runat="server" ID="hidISACPORMIS" />
        <asp:HiddenField runat="server" ID="hidPersonResponsible" />
        <asp:HiddenField runat="server" ID="hidObservationRating" />
        <asp:HiddenField runat="server" ID="hidObservationCategory" />
        <asp:HiddenField runat="server" ID="hidObservationSubCategory" />
        <asp:HiddenField runat="server" ID="hidOwner" />
        <asp:HiddenField runat="server" ID="hidUserID" />
        <asp:HiddenField runat="server" ID="hidGroupObservationCategory" />
        <asp:HiddenField runat="server" ID="hidUHPersonResponsible" />
        <asp:HiddenField runat="server" ID="hidPRESIDENTPersonResponsible" />
        <asp:HiddenField runat="server" ID="hidUHComment" />
        <asp:HiddenField runat="server" ID="hidPRESIDENTComment" />
        <asp:HiddenField runat="server" ID="hidBodyContent" />
        <asp:HiddenField runat="server" ID="hidResponseDueDate" />
        <asp:HiddenField runat="server" ID="hidAnnaxureTitle" />
            </div>
        <iframe id="downloadfile" src="about:blank" width="0" height="0" style="display: none;"></iframe>
        <div class="modal fade" id="divPREDOCShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <iframe id="DetObsshowdetails" src="about:blank" height="100%" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog" style="width: 70%;">
                    <div class="modal-content">
                        <div class="modal-header" style="border: none;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body" style="height: 530px; margin-top: -2%;">
                            <div style="width: 100%;">
                                <div class="col-md-12 colpadding0">
                                    <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                    <fieldset style="height: 550px; width: 100%;">
                                        <iframe src="about:blank" id="OverViews" width="100%" height="100%" frameborder="0"></iframe>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <div class="modal fade" id="AddUserModelPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog p0" style="width: 50%; overflow: hidden;">
                <div class="modal-content">
                    <div class="modal-header" style="padding: 30px !important;">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="modal-header-custom" id="Usermmodel">
                            Add User</label>
                    </div>
                    <div class="modal-body Dashboard-white-widget" style="width: 100%; margin-top: -2px;">
                        <iframe src="about:blank" id="IUser" frameborder="0" runat="server" width="100%" height="400px"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>